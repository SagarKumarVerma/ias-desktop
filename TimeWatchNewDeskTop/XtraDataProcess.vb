﻿Imports System.Resources
Imports System.Globalization
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports System.ComponentModel
Imports DevExpress.XtraGrid.Columns
Imports System.Data.SqlClient
Imports System.Text
Imports System.Data.OleDb
Imports DevExpress.XtraSplashScreen
Imports System.Threading
Imports System.Threading.Tasks

Public Class XtraDataProcess
    'Dim res_man As ResourceManager     'declare Resource manager to access to specific cultureinfo
    'Dim cul As CultureInfo     'declare culture info
    Dim servername As String
    Dim ConnectionString As String
    Dim ulf As UserLookAndFeel
    Dim con As SqlConnection
    Dim con1 As OleDbConnection
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    Dim adap, adap1, adap2 As SqlDataAdapter
    Dim adapA, adapA1 As OleDbDataAdapter
    Dim ds, ds1, ds2 As DataSet
    Public Sub New()
        InitializeComponent()
       
        Dim fs As FileStream = New FileStream("db.txt", FileMode.Open, FileAccess.Read)
        Dim sr As StreamReader = New StreamReader(fs)
        Dim str As String
        Dim str1() As String
        Do While sr.Peek <> -1
            str = sr.ReadLine
            str1 = str.Split(",")
            servername = str1(0)
        Loop
        sr.Close()
        fs.Close()

        If Common.servername = "Access" Then
            'ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\TimeWatch.mdb;Persist Security Info=True;Jet OLEDB:Database Password=SSS"
            con1 = New OleDbConnection(ConnectionString)
        Else
            'ConnectionString = "Data Source=" & servername & ";Initial Catalog=SSSDB;Integrated Security=True"
            con = New SqlConnection(ConnectionString)
            TblCompanyTableAdapter.Connection.ConnectionString = Common.ConnectionString '"Data Source=DESKTOP-PG6NDV8;Initial Catalog=SSSDB;Integrated Security=True"
            TblDepartmentTableAdapter.Connection.ConnectionString = Common.ConnectionString
            TblbranchTableAdapter.Connection.ConnectionString = Common.ConnectionString
            TblEmployeeTableAdapter.Connection.ConnectionString = Common.ConnectionString
        End If
        Common.SetGridFont(GridViewComp, New Font("Tahoma", 10))
        Common.SetGridFont(GridViewDept, New Font("Tahoma", 10))
        Common.SetGridFont(GridViewLocation, New Font("Tahoma", 10))
        Common.SetGridFont(GridViewEmp, New Font("Tahoma", 10))
    End Sub
    Private Sub XtraDataProcess_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True


        'Me.Width = My.Computer.Screen.WorkingArea.Width
        'SplitContainerControl1.Width = My.Computer.Screen.WorkingArea.Width
        'SplitContainerControl1.SplitterPosition = (My.Computer.Screen.WorkingArea.Width) * 90 / 100

        Me.Width = Common.NavWidth 'Me.Parent.Width
        Me.Height = Common.NavHeight 'Me.Parent.Height
        'SplitContainerControl1.Width = Common.splitforMasterMenuWidth 'SplitContainerControl1.Parent.Width
        SplitContainerControl1.SplitterPosition = Common.SplitterPosition '(SplitContainerControl1.Parent.Width) * 85 / 100


        If GroupControl4.Width > SplitContainerControl1.Panel1.Width Then
            GroupControl4.Width = SplitContainerControl1.Panel1.Width - 50
            GroupControl1.Width = GroupControl4.Width / 3 - 5
            GroupControl3.Location = New Point(GroupControl1.Location.X + GroupControl1.Width + 5, GroupControl1.Location.Y)
            GroupControl3.Width = GroupControl4.Width / 3 - 5
            GroupControl2.Location = New Point(GroupControl3.Location.X + GroupControl3.Width + 5, GroupControl1.Location.Y)
            GroupControl2.Width = GroupControl4.Width / 3 - 5
            TextEditUploadLogs.Width = GroupControl2.Width - 10
        End If

        ToggleEmp.IsOn = False

        If Common.servername = "Access" Then
            Me.TblCompany1TableAdapter1.Fill(Me.SSSDBDataSet.tblCompany1)
            Me.TblDepartment1TableAdapter1.Fill(Me.SSSDBDataSet.tblDepartment1)
            Me.Tblbranch1TableAdapter1.Fill(Me.SSSDBDataSet.tblbranch1)
            'Me.TblEmployee1TableAdapter1.Fill(Me.SSSDBDataSet.TblEmployee1)
            GridControlComp.DataSource = SSSDBDataSet.tblCompany1
            GridControlDept.DataSource = SSSDBDataSet.tblDepartment1
            'GridControlLocation.DataSource = SSSDBDataSet.tblbranch1
            'GridControlEmp.DataSource = SSSDBDataSet.TblEmployee1
        Else
            Me.TblCompanyTableAdapter.Fill(Me.SSSDBDataSet.tblCompany)
            Me.TblDepartmentTableAdapter.Fill(Me.SSSDBDataSet.tblDepartment)
            'Me.TblbranchTableAdapter.Fill(Me.SSSDBDataSet.tblbranch)
            'Me.TblEmployeeTableAdapter.Fill(Me.SSSDBDataSet.TblEmployee)
            GridControlComp.DataSource = SSSDBDataSet.tblCompany
            GridControlDept.DataSource = SSSDBDataSet.tblDepartment
            'GridControlLocation.DataSource = SSSDBDataSet.tblbranch
            'GridControlEmp.DataSource = SSSDBDataSet.TblEmployee
        End If
        GridControlLocation.DataSource = Common.LocationNonAdmin
        GridControlEmp.DataSource = Common.EmpNonAdmin

        If Common.USERTYPE <> "A" Then
            CheckSelective.Checked = True
            CheckAll.Visible = False
            ComboBoxEdit1.Properties.Items.Remove("Company")
            ComboBoxEdit1.Properties.Items.Remove("Department")
            ComboBoxEdit1.Properties.Items.Remove("Location")
        End If

        'Me.Width = Common.NavWidth 'Me.Parent.Width
        'Me.Height = Common.NavHeight 'Me.Parent.Height
        ''SplitContainerControl1.Width = Common.splitforMasterMenuWidth 'SplitContainerControl1.Parent.Width
        'SplitContainerControl1.SplitterPosition = Common.SplitterPosition '(SplitContainerControl1.Parent.Width) * 85 / 100

        If CheckAll.Checked = True Then
            LabelControl5.Visible = False
            LabelControl7.Visible = False
            ComboBoxEdit1.Visible = False
            PopupContainerEdit1.Visible = False
        End If

        If Common.RegisterCreation = "N" Then
            GroupControl1.Enabled = False
        Else
            GroupControl1.Enabled = True
        End If


        If Common.RegisterUpdation = "N" Then
            GroupControl3.Enabled = False
        Else
            GroupControl3.Enabled = True
        End If


        If Common.BackDateProcess = "N" Then
            GroupControl2.Enabled = False
        Else
            GroupControl2.Enabled = True
        End If

        If Common.Verification = "N" Then
            GroupControl5.Enabled = False
        Else
            GroupControl5.Enabled = True
        End If

        If Common.IsCompliance = True Then
            GroupControl2.Visible = False
        End If
        TextEditUploadLogs.Text = ""
        LoadVerification()

        DateEdit3.DateTime = Now.ToString("yyyy-01-01")
        DateEditVerification.DateTime = Now.ToString("yyyy-MM-01")
        If Common.IsNepali = "Y" Then
            DateEdit3.Visible = False
            DateEditVerification.Visible = False
            ComboNepaliYear.Visible = True
            ComboNEpaliMonth.Visible = True
            ComboNepaliDate.Visible = True

            ComboNEpaliMonthVal.Visible = True
            ComboNepaliYearVal.Visible = True

            Dim DC As New DateConverter()
            Dim doj As String = DC.ToBS(New Date(DateEdit3.DateTime.Year, DateEdit3.DateTime.Month, 1))
            Dim dojTmp() As String = doj.Split("-")
            ComboNepaliYear.EditValue = dojTmp(0)
            ComboNEpaliMonth.SelectedIndex = dojTmp(1) - 1
            ComboNepaliDate.EditValue = dojTmp(2)

            doj = DC.ToBS(New Date(DateEditVerification.DateTime.Year, DateEditVerification.DateTime.Month, 1))
            dojTmp = doj.Split("-")
            ComboNEpaliMonthVal.SelectedIndex = dojTmp(1) - 1
            ComboNepaliYearVal.EditValue = dojTmp(0)
        Else
            DateEdit3.Visible = True
            DateEditVerification.Visible = True

            ComboNepaliYear.Visible = False
            ComboNEpaliMonth.Visible = False
            ComboNepaliDate.Visible = False

            ComboNEpaliMonthVal.Visible = False
            ComboNepaliYearVal.Visible = False
        End If
        'selectpopupdropdown()
        CheckDuplicate.Checked = False
        DateFromLeave.DateTime = Now
        DateToLeave.DateTime = Now
        LookUpEditDevice.Properties.DataSource = Common.MachineNonAdminAll
        LookUpEditDevice.EditValue = ""
    End Sub
    Private Sub CheckEdit5_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckAll.CheckedChanged
        If CheckAll.Checked = True Then
            LabelControl5.Visible = False
            LabelControl7.Visible = False
            ComboBoxEdit1.Visible = False
            PopupContainerEdit1.Visible = False
        End If
    End Sub
    Private Sub CheckEdit6_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckSelective.CheckedChanged
        If CheckSelective.Checked = True Then
            ComboBoxEdit1.EditValue = "Paycode"
            LabelControl5.Visible = True
            LabelControl7.Visible = True
            ComboBoxEdit1.Visible = True
            PopupContainerEdit1.Visible = True
            selectpopupdropdown()
        End If
    End Sub
    Private Sub SimpleButton1_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton1.Click
        If Common.IsNepali = "Y" Then
            Dim DC As New DateConverter()
            Try
                'DateEdit3.DateTime = DC.ToAD(New Date(ComboNepaliYear.EditValue, ComboNEpaliMonth.SelectedIndex + 1, ComboNepaliDate.EditValue))
                DateEdit3.DateTime = DC.ToAD(ComboNepaliYear.EditValue & "-" & ComboNEpaliMonth.SelectedIndex + 1 & "-" & ComboNepaliDate.EditValue)
            Catch ex As Exception
                XtraMessageBox.Show(ulf, "<size=10>Invalid Date</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                ComboNepaliDate.Select()
                Exit Sub
            End Try
        End If
        If DateEdit3.ToString = "" Then
            SplashScreenManager.CloseForm(False)
            XtraMessageBox.Show(ulf, "<size=10>Select date</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            DateEdit3.Select()
            Exit Sub
        End If
        'ProgressBarControl1.Visible = True
        DevExpress.XtraSplashScreen.SplashScreenManager.ShowForm(Me, GetType(WaitForm1), True, True, False)
        Me.Cursor = Cursors.WaitCursor
        Dim mmin_date As String = DateEdit3.DateTime.ToString("dd/MMM/yyyy")
        'If Common.servername = "Access" Then
        '    adapA = New OleDbDataAdapter("select * from tblSetup where SETUPID= (select MAX(SETUPID) from tblSetup) ", Common.con1)
        '    ds = New DataSet
        '    adapA.Fill(ds)
        'Else
        '    adap = New SqlDataAdapter("select * from tblSetup where SETUPID= (select MAX(SETUPID) from tblSetup) ", Common.con)
        '    ds = New DataSet
        '    adap.Fill(ds)
        'End If
        'Dim g_Wo_Include As String = ds.Tables(0).Rows(0).Item("WOInclude").ToString.Trim
        Dim sSql1 As String
        Dim comclass As Common = New Common
        Common.LogPost("Roster Creation; From Date:" & DateEdit3.DateTime.ToString("dd/MM/yyyy"))
        If CheckAll.Checked = True Then
            sSql1 = "Select  PAYCODE, dateofjoin, Id from tblEmployee, EmployeeGroup Where tblEmployee.active = 'Y' and tblEmployee.EmployeeGroupId=EmployeeGroup.GroupId  Order By Paycode"
            ds1 = New DataSet
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql1, Common.con1)
                adapA.Fill(ds1)
            Else
                adap1 = New SqlDataAdapter(sSql1, Common.con)
                adap1.Fill(ds1)
            End If

            For i As Integer = 0 To ds1.Tables(0).Rows.Count - 1
                XtraMasterTest.LabelControlCount.Text = "Processing " & i + 1 & " of " & ds1.Tables(0).Rows.Count
                Application.DoEvents()

                Dim g_Wo_Include As String = Common.EmpGrpArr(ds1.Tables(0).Rows(i).Item("Id")).g_Wo_Include
                If Convert.ToDateTime(ds1.Tables(0).Rows(i).Item("dateofjoin")) > Convert.ToDateTime(mmin_date) Then
                    mmin_date = Convert.ToDateTime(ds1.Tables(0).Rows(i).Item("dateofjoin")).ToString("dd/MMM/yyyy")
                Else
                    mmin_date = DateEdit3.DateTime.ToString("dd/MMM/yyyy")
                End If
                comclass.DutyRoster(ds1.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, mmin_date, g_Wo_Include)
                'End If
            Next
        Else
            If ComboBoxEdit1.EditValue = "Paycode" Then
                Dim paycode() As String = PopupContainerEdit1.EditValue.ToString.Split(",")
                If PopupContainerEdit1.EditValue.ToString.Length = 0 Then
                    DevExpress.XtraSplashScreen.SplashScreenManager.CloseForm(False)
                    Me.Cursor = Cursors.Default
                    XtraMessageBox.Show(ulf, "<size=10>Please Select Paycode.</size>", "iAS")
                    PopupContainerEdit1.Select()
                    Exit Sub
                End If
                For i As Integer = 0 To paycode.Length - 1
                    XtraMasterTest.LabelControlCount.Text = "Processing " & i + 1 & " of " & paycode.Length
                    Application.DoEvents()
                    Dim adap As SqlDataAdapter
                    Dim adapA As OleDbDataAdapter
                    Dim ds As DataSet = New DataSet
                    sSql1 = "Select PAYCODE, dateofjoin, Id from tblEmployee, EmployeeGroup Where tblEmployee.active = 'Y' and PayCode = '" & paycode(i).Trim & "' and tblEmployee.EmployeeGroupId=EmployeeGroup.GroupId  Order By Paycode"
                    If Common.servername = "Access" Then
                        adapA = New OleDbDataAdapter(sSql1, Common.con1)
                        adapA.Fill(ds)
                    Else
                        adap = New SqlDataAdapter(sSql1, Common.con)
                        adap.Fill(ds)
                    End If
                    'MsgBox(ds.Tables(0).Rows.Count)
                    If ds.Tables(0).Rows.Count > 0 Then
                        Dim g_Wo_Include As String = Common.EmpGrpArr(ds.Tables(0).Rows(0).Item("Id")).g_Wo_Include
                        If Convert.ToDateTime(ds.Tables(0).Rows(0).Item("dateofjoin")) > Convert.ToDateTime(mmin_date) Then
                            mmin_date = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("dateofjoin")).ToString("dd/MMM/yyyy")
                        Else
                            mmin_date = DateEdit3.DateTime.ToString("dd/MMM/yyyy")
                        End If
                        comclass.DutyRoster(ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, mmin_date, g_Wo_Include)
                    End If
                Next
            ElseIf ComboBoxEdit1.EditValue = "Company" Then
                Dim com() As String = PopupContainerEdit1.EditValue.ToString.Split(",")
                If PopupContainerEdit1.EditValue.ToString.Length = 0 Then
                    DevExpress.XtraSplashScreen.SplashScreenManager.CloseForm(False)
                    Me.Cursor = Cursors.Default
                    XtraMessageBox.Show(ulf, "<size=10>Please Select Company.</size>", "iAS")
                    PopupContainerEdit1.Select()
                    Exit Sub
                End If
                Dim ls As New List(Of String)()
                For x As Integer = 0 To com.Length - 1
                    ls.Add(com(x).Trim)
                Next
                'select PAYCODE from TblEmployee where COMPANYCODE IN ('01')  and ACTIVE = 'Y'
                sSql1 = "select PAYCODE, dateofjoin, Id from tblEmployee, EmployeeGroup where COMPANYCODE IN ('" & String.Join("', '", ls.ToArray()) & "')  and ACTIVE = 'Y'  and tblEmployee.EmployeeGroupId=EmployeeGroup.GroupId "
                'sSql1 = "select TblEmployee.PAYCODE,TblEmployee.dateofjoin, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK from TblEmployee, tblEmployeeShiftMaster where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.COMPANYCODE IN ('" & String.Join("', '", ls.ToArray()) & "')  and TblEmployee.ACTIVE = 'Y'"
                Dim adap As SqlDataAdapter
                Dim adapA As OleDbDataAdapter
                Dim ds As DataSet = New DataSet
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql1, Common.con1)
                    adapA.Fill(ds)
                Else
                    adap = New SqlDataAdapter(sSql1, Common.con)
                    adap.Fill(ds)
                End If
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    XtraMasterTest.LabelControlCount.Text = "Processing " & i + 1 & " of " & ds.Tables(0).Rows.Count
                    Application.DoEvents()
                    Dim g_Wo_Include As String = Common.EmpGrpArr(ds.Tables(0).Rows(i).Item("Id")).g_Wo_Include
                    If Convert.ToDateTime(ds.Tables(0).Rows(i).Item("dateofjoin")) > Convert.ToDateTime(mmin_date) Then
                        mmin_date = Convert.ToDateTime(ds.Tables(0).Rows(i).Item("dateofjoin")).ToString("dd/MMM/yyyy")
                    Else
                        mmin_date = DateEdit3.DateTime.ToString("dd/MMM/yyyy")
                    End If
                    comclass.DutyRoster(ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, mmin_date, g_Wo_Include)
                Next
            ElseIf ComboBoxEdit1.EditValue = "Location" Then
                Dim branch() As String = PopupContainerEdit1.EditValue.ToString.Split(",")
                If PopupContainerEdit1.EditValue.ToString.Length = 0 Then
                    DevExpress.XtraSplashScreen.SplashScreenManager.CloseForm(False)
                    Me.Cursor = Cursors.Default
                    XtraMessageBox.Show(ulf, "<size=10>Please Select Location.</size>", "iAS")
                    PopupContainerEdit1.Select()
                    Exit Sub
                End If
                Dim ls As New List(Of String)()
                For x As Integer = 0 To branch.Length - 1
                    ls.Add(branch(x).Trim)
                Next
                'select PAYCODE from TblEmployee where COMPANYCODE IN ('01')  and ACTIVE = 'Y'
                sSql1 = "select PAYCODE, dateofjoin, Id from tblEmployee, EmployeeGroup where BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "')  and ACTIVE = 'Y'  and tblEmployee.EmployeeGroupId=EmployeeGroup.GroupId "
                'sSql1 = "select TblEmployee.PAYCODE,TblEmployee.dateofjoin, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK from TblEmployee, tblEmployeeShiftMaster where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.COMPANYCODE IN ('" & String.Join("', '", ls.ToArray()) & "')  and TblEmployee.ACTIVE = 'Y'"
                Dim adap As SqlDataAdapter
                Dim adapA As OleDbDataAdapter
                Dim ds As DataSet = New DataSet
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql1, Common.con1)
                    adapA.Fill(ds)
                Else
                    adap = New SqlDataAdapter(sSql1, Common.con)
                    adap.Fill(ds)
                End If
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    XtraMasterTest.LabelControlCount.Text = "Processing " & i + 1 & " of " & ds.Tables(0).Rows.Count
                    Application.DoEvents()
                    Dim g_Wo_Include As String = Common.EmpGrpArr(ds.Tables(0).Rows(i).Item("Id")).g_Wo_Include
                    If Convert.ToDateTime(ds.Tables(0).Rows(i).Item("dateofjoin")) > Convert.ToDateTime(mmin_date) Then
                        mmin_date = Convert.ToDateTime(ds.Tables(0).Rows(i).Item("dateofjoin")).ToString("dd/MMM/yyyy")
                    Else
                        mmin_date = DateEdit3.DateTime.ToString("dd/MMM/yyyy")
                    End If
                    comclass.DutyRoster(ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, mmin_date, g_Wo_Include)
                Next
            ElseIf ComboBoxEdit1.EditValue = "Department" Then
                Dim dept() As String = PopupContainerEdit1.EditValue.ToString.Split(",")
                If PopupContainerEdit1.EditValue.ToString.Length = 0 Then
                    DevExpress.XtraSplashScreen.SplashScreenManager.CloseForm(False)
                    Me.Cursor = Cursors.Default
                    XtraMessageBox.Show(ulf, "<size=10>Please Select Department.</size>", "iAS")
                    PopupContainerEdit1.Select()
                    Exit Sub
                End If
                Dim ls As New List(Of String)()
                For x As Integer = 0 To dept.Length - 1
                    ls.Add(dept(x).Trim)
                Next
                'select PAYCODE from TblEmployee where COMPANYCODE IN ('01')  and ACTIVE = 'Y'
                sSql1 = "select PAYCODE, dateofjoin, Id from tblEmployee, EmployeeGroup where DEPARTMENTCODE IN ('" & String.Join("', '", ls.ToArray()) & "')  and ACTIVE = 'Y' and tblEmployee.EmployeeGroupId=EmployeeGroup.GroupId"
                'sSql1 = "select TblEmployee.PAYCODE,TblEmployee.dateofjoin, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK from TblEmployee, tblEmployeeShiftMaster where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.COMPANYCODE IN ('" & String.Join("', '", ls.ToArray()) & "')  and TblEmployee.ACTIVE = 'Y'"
                Dim adap As SqlDataAdapter
                Dim adapA As OleDbDataAdapter
                Dim ds As DataSet = New DataSet
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql1, Common.con1)
                    adapA.Fill(ds)
                Else
                    adap = New SqlDataAdapter(sSql1, Common.con)
                    adap.Fill(ds)
                End If
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    XtraMasterTest.LabelControlCount.Text = "Processing " & i + 1 & " of " & ds.Tables(0).Rows.Count
                    Application.DoEvents()
                    Dim g_Wo_Include As String = Common.EmpGrpArr(ds.Tables(0).Rows(i).Item("Id")).g_Wo_Include
                    If Convert.ToDateTime(ds.Tables(0).Rows(i).Item("dateofjoin")) > Convert.ToDateTime(mmin_date) Then
                        mmin_date = Convert.ToDateTime(ds.Tables(0).Rows(i).Item("dateofjoin")).ToString("dd/MMM/yyyy")
                    Else
                        mmin_date = DateEdit3.DateTime.ToString("dd/MMM/yyyy")
                    End If
                    comclass.DutyRoster(ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, mmin_date, g_Wo_Include)
                Next
            End If

        End If

        XtraMasterTest.LabelControlCount.Text = ""
        Application.DoEvents()
        SplashScreenManager.CloseForm(False)
        Me.Cursor = Cursors.Default
        XtraMessageBox.Show(ulf, "<size=10>Created Successfully</size>", "Success")
        'ProgressBarControl1.Visible = False
    End Sub
    Private Sub SimpleButton2_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton2.Click
        If Common.IsNepali = "Y" Then
            Dim DC As New DateConverter()
            Try
                'DateEdit3.DateTime = DC.ToAD(New Date(ComboNepaliYear.EditValue, ComboNEpaliMonth.SelectedIndex + 1, ComboNepaliDate.EditValue))
                DateEdit3.DateTime = DC.ToAD(ComboNepaliYear.EditValue & "-" & ComboNEpaliMonth.SelectedIndex + 1 & "-" & ComboNepaliDate.EditValue)
            Catch ex As Exception
                XtraMessageBox.Show(ulf, "<size=10>Invalid Date</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                ComboNepaliDate.Select()
                Exit Sub
            End Try
        End If
        If DateEdit3.ToString = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Select date</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            DateEdit3.Select()
            Exit Sub
        End If
        'ProgressBarControl1.Visible = True
        Dim comclass As Common = New Common
        DevExpress.XtraSplashScreen.SplashScreenManager.ShowForm(Me, GetType(WaitForm1), True, True, False)
        Me.Cursor = Cursors.WaitCursor
        Dim mmin_date As String = DateEdit3.DateTime.ToString("dd/MMM/yyyy")
        'If Common.servername = "Access" Then
        '    adapA = New OleDbDataAdapter("select * from tblSetup where SETUPID= (select MAX(SETUPID) from tblSetup)", Common.con1)
        '    ds = New DataSet
        '    adapA.Fill(ds)
        'Else
        '    adap = New SqlDataAdapter("select * from tblSetup where SETUPID= (select MAX(SETUPID) from tblSetup)", Common.con)
        '    ds = New DataSet
        '    adap.Fill(ds)
        'End If
        'Dim g_Wo_Include As String = ds.Tables(0).Rows(0).Item("WOInclude").ToString.Trim

        Common.LogPost("Roster Updation; From Date:" & DateEdit3.DateTime.ToString("dd/MM/yyyy"))
        Dim sSql1 As String
        If CheckAll.Checked = True Then
            sSql1 = "Select  PAYCODE, dateofjoin, Id from tblEmployee, EmployeeGroup  Where tblEmployee.active = 'Y'  and tblEmployee.EmployeeGroupId=EmployeeGroup.GroupId  Order By Paycode"
            ds1 = New DataSet
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql1, Common.con1)
                adapA.Fill(ds1)
            Else
                adap1 = New SqlDataAdapter(sSql1, Common.con)
                adap1.Fill(ds1)
            End If

            For i As Integer = 0 To ds1.Tables(0).Rows.Count - 1
                XtraMasterTest.LabelControlCount.Text = "Processing " & i + 1 & " of " & ds1.Tables(0).Rows.Count
                Application.DoEvents()
                Dim g_Wo_Include As String = Common.EmpGrpArr(ds1.Tables(0).Rows(i).Item("Id")).g_Wo_Include
                If Convert.ToDateTime(ds1.Tables(0).Rows(i).Item("dateofjoin")) > Convert.ToDateTime(mmin_date) Then
                    mmin_date = Convert.ToDateTime(ds1.Tables(0).Rows(i).Item("dateofjoin")).ToString("dd/MMM/yyyy")
                Else
                    mmin_date = DateEdit3.DateTime.ToString("dd/MMM/yyyy")
                End If
                comclass.DutyRosterUpd(ds1.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, mmin_date, g_Wo_Include)
            Next
        Else
            'sSql1 = "Select PAYCODE, dateofjoin from tblEmployee Where tblEmployee.active = 'Y' and PayCode = '" & TextEdit2.Text & "'  Order By Paycode"

            If ComboBoxEdit1.EditValue = "Paycode" Then
                Dim paycode() As String = PopupContainerEdit1.EditValue.ToString.Split(",")
                If PopupContainerEdit1.EditValue.ToString.Length = 0 Then
                    DevExpress.XtraSplashScreen.SplashScreenManager.CloseForm(False)
                    Me.Cursor = Cursors.Default
                    XtraMessageBox.Show(ulf, "<size=10>Please Select Paycode.</size>", "iAS")
                    PopupContainerEdit1.Select()
                    Exit Sub
                End If
                For i As Integer = 0 To paycode.Length - 1
                    XtraMasterTest.LabelControlCount.Text = "Processing " & i + 1 & " of " & paycode.Length
                    Application.DoEvents()
                    Dim adap As SqlDataAdapter
                    Dim adapA As OleDbDataAdapter
                    Dim ds As DataSet = New DataSet
                    sSql1 = "Select PAYCODE, dateofjoin, Id from tblEmployee, EmployeeGroup  Where tblEmployee.active = 'Y' and PayCode = '" & paycode(i).Trim & "'  and tblEmployee.EmployeeGroupId=EmployeeGroup.GroupId  Order By Paycode"
                    If Common.servername = "Access" Then
                        adapA = New OleDbDataAdapter(sSql1, Common.con1)
                        adapA.Fill(ds)
                    Else
                        adap = New SqlDataAdapter(sSql1, Common.con)
                        adap.Fill(ds)
                    End If
                    'MsgBox(ds.Tables(0).Rows.Count)
                    Dim g_Wo_Include As String = Common.EmpGrpArr(ds.Tables(0).Rows(0).Item("Id")).g_Wo_Include
                    If Convert.ToDateTime(ds.Tables(0).Rows(0).Item("dateofjoin")) > Convert.ToDateTime(mmin_date) Then
                        mmin_date = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("dateofjoin")).ToString("dd/MMM/yyyy")
                    Else
                        mmin_date = DateEdit3.DateTime.ToString("dd/MMM/yyyy")
                    End If
                    comclass.DutyRosterUpd(ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, mmin_date, g_Wo_Include)
                Next
            ElseIf ComboBoxEdit1.EditValue = "Company" Then
                Dim com() As String = PopupContainerEdit1.EditValue.ToString.Split(",")
                If PopupContainerEdit1.EditValue.ToString.Length = 0 Then
                    DevExpress.XtraSplashScreen.SplashScreenManager.CloseForm(False)
                    Me.Cursor = Cursors.Default
                    XtraMessageBox.Show(ulf, "<size=10>Please Select Company.</size>", "iAS")
                    PopupContainerEdit1.Select()
                    Exit Sub
                End If
                Dim ls As New List(Of String)()
                For x As Integer = 0 To com.Length - 1
                    ls.Add(com(x).Trim)
                Next
                'select PAYCODE from TblEmployee where COMPANYCODE IN ('01')  and ACTIVE = 'Y'
                sSql1 = "select PAYCODE, dateofjoin, Id from tblEmployee, EmployeeGroup where COMPANYCODE IN ('" & String.Join("', '", ls.ToArray()) & "')  and ACTIVE = 'Y'  and tblEmployee.EmployeeGroupId=EmployeeGroup.GroupId "
                'sSql1 = "select TblEmployee.PAYCODE,TblEmployee.dateofjoin, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK from TblEmployee, tblEmployeeShiftMaster where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.COMPANYCODE IN ('" & String.Join("', '", ls.ToArray()) & "')  and TblEmployee.ACTIVE = 'Y'"
                Dim adap As SqlDataAdapter
                Dim adapA As OleDbDataAdapter
                Dim ds As DataSet = New DataSet
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql1, Common.con1)
                    adapA.Fill(ds)
                Else
                    adap = New SqlDataAdapter(sSql1, Common.con)
                    adap.Fill(ds)
                End If
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    XtraMasterTest.LabelControlCount.Text = "Processing " & i + 1 & " of " & ds.Tables(0).Rows.Count
                    Application.DoEvents()
                    Dim g_Wo_Include As String = Common.EmpGrpArr(ds.Tables(0).Rows(i).Item("Id")).g_Wo_Include
                    If Convert.ToDateTime(ds.Tables(0).Rows(i).Item("dateofjoin")) > Convert.ToDateTime(mmin_date) Then
                        mmin_date = Convert.ToDateTime(ds.Tables(0).Rows(i).Item("dateofjoin")).ToString("dd/MMM/yyyy")
                    Else
                        mmin_date = DateEdit3.DateTime.ToString("dd/MMM/yyyy")
                    End If
                    comclass.DutyRosterUpd(ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, mmin_date, g_Wo_Include)
                Next
            ElseIf ComboBoxEdit1.EditValue = "Location" Then
                Dim branch() As String = PopupContainerEdit1.EditValue.ToString.Split(",")
                If PopupContainerEdit1.EditValue.ToString.Length = 0 Then
                    DevExpress.XtraSplashScreen.SplashScreenManager.CloseForm(False)
                    Me.Cursor = Cursors.Default
                    XtraMessageBox.Show(ulf, "<size=10>Please Select Location.</size>", "iAS")
                    PopupContainerEdit1.Select()
                    Exit Sub
                End If
                Dim ls As New List(Of String)()
                For x As Integer = 0 To branch.Length - 1
                    ls.Add(branch(x).Trim)
                Next
                'select PAYCODE from TblEmployee where COMPANYCODE IN ('01')  and ACTIVE = 'Y'
                sSql1 = "select PAYCODE, dateofjoin, Id from tblEmployee, EmployeeGroup where BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "')  and ACTIVE = 'Y'  and tblEmployee.EmployeeGroupId=EmployeeGroup.GroupId "
                'sSql1 = "select TblEmployee.PAYCODE,TblEmployee.dateofjoin, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK from TblEmployee, tblEmployeeShiftMaster where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.COMPANYCODE IN ('" & String.Join("', '", ls.ToArray()) & "')  and TblEmployee.ACTIVE = 'Y'"
                Dim adap As SqlDataAdapter
                Dim adapA As OleDbDataAdapter
                Dim ds As DataSet = New DataSet
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql1, Common.con1)
                    adapA.Fill(ds)
                Else
                    adap = New SqlDataAdapter(sSql1, Common.con)
                    adap.Fill(ds)
                End If
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    XtraMasterTest.LabelControlCount.Text = "Processing " & i + 1 & " of " & ds.Tables(0).Rows.Count
                    Application.DoEvents()
                    Dim g_Wo_Include As String = Common.EmpGrpArr(ds.Tables(0).Rows(i).Item("Id")).g_Wo_Include
                    If Convert.ToDateTime(ds.Tables(0).Rows(i).Item("dateofjoin")) > Convert.ToDateTime(mmin_date) Then
                        mmin_date = Convert.ToDateTime(ds.Tables(0).Rows(i).Item("dateofjoin")).ToString("dd/MMM/yyyy")
                    Else
                        mmin_date = DateEdit3.DateTime.ToString("dd/MMM/yyyy")
                    End If
                    comclass.DutyRosterUpd(ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, mmin_date, g_Wo_Include)
                Next
            ElseIf ComboBoxEdit1.EditValue = "Department" Then
                Dim dept() As String = PopupContainerEdit1.EditValue.ToString.Split(",")
                If PopupContainerEdit1.EditValue.ToString.Length = 0 Then
                    DevExpress.XtraSplashScreen.SplashScreenManager.CloseForm(False)
                    Me.Cursor = Cursors.Default
                    XtraMessageBox.Show(ulf, "<size=10>Please Select Department.</size>", "iAS")
                    PopupContainerEdit1.Select()
                    Exit Sub
                End If
                Dim ls As New List(Of String)()
                For x As Integer = 0 To dept.Length - 1
                    ls.Add(dept(x).Trim)
                Next
                'select PAYCODE from TblEmployee where COMPANYCODE IN ('01')  and ACTIVE = 'Y'
                sSql1 = "select PAYCODE, dateofjoin, Id from tblEmployee, EmployeeGroup where DEPARTMENTCODE IN ('" & String.Join("', '", ls.ToArray()) & "')  and ACTIVE = 'Y'  and tblEmployee.EmployeeGroupId=EmployeeGroup.GroupId "
                'sSql1 = "select TblEmployee.PAYCODE,TblEmployee.dateofjoin, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK from TblEmployee, tblEmployeeShiftMaster where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.COMPANYCODE IN ('" & String.Join("', '", ls.ToArray()) & "')  and TblEmployee.ACTIVE = 'Y'"
                Dim adap As SqlDataAdapter
                Dim adapA As OleDbDataAdapter
                Dim ds As DataSet = New DataSet
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql1, Common.con1)
                    adapA.Fill(ds)
                Else
                    adap = New SqlDataAdapter(sSql1, Common.con)
                    adap.Fill(ds)
                End If
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    XtraMasterTest.LabelControlCount.Text = "Processing " & i + 1 & " of " & ds.Tables(0).Rows.Count
                    Application.DoEvents()
                    Dim g_Wo_Include As String = Common.EmpGrpArr(ds.Tables(0).Rows(i).Item("Id")).g_Wo_Include
                    If Convert.ToDateTime(ds.Tables(0).Rows(i).Item("dateofjoin")) > Convert.ToDateTime(mmin_date) Then
                        mmin_date = Convert.ToDateTime(ds.Tables(0).Rows(i).Item("dateofjoin")).ToString("dd/MMM/yyyy")
                    Else
                        mmin_date = DateEdit3.DateTime.ToString("dd/MMM/yyyy")
                    End If
                    comclass.DutyRosterUpd(ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, mmin_date, g_Wo_Include)
                Next
            End If
        End If

        XtraMasterTest.LabelControlCount.Text = ""
        Application.DoEvents()
        SplashScreenManager.CloseForm(False)
        Me.Cursor = Cursors.Default

        SimpleButtonProcess_Click(sender, e)
        'XtraMessageBox.Show(ulf, "<size=10>Updated Successfully</size>", "Success")
    End Sub
    Private Sub SimpleButtonProcess_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonProcess.Click
        If Common.IsNepali = "Y" Then
            Dim DC As New DateConverter()
            Try
                'DateEdit3.DateTime = DC.ToAD(New Date(ComboNepaliYear.EditValue, ComboNEpaliMonth.SelectedIndex + 1, ComboNepaliDate.EditValue))
                DateEdit3.DateTime = DC.ToAD(ComboNepaliYear.EditValue & "-" & ComboNEpaliMonth.SelectedIndex + 1 & "-" & ComboNepaliDate.EditValue)
            Catch ex As Exception
                XtraMessageBox.Show(ulf, "<size=10>Invalid Date</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                ComboNepaliDate.Select()
                Exit Sub
            End Try
        End If
        If DateEdit3.ToString = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Select date</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            DateEdit3.Select()
            Exit Sub
        End If
        Dim comclass As Common = New Common
        If TextEditUploadLogs.Text <> "" Then

            Dim ID_NO As String
            Try
                ID_NO = LookUpEditDevice.EditValue.ToString.Trim
            Catch ex As Exception
                XtraMessageBox.Show(ulf, "<size=10>Select Device</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                LookUpEditDevice.Select()
                Exit Sub
            End Try

            Me.Cursor = Cursors.WaitCursor
            XtraMasterTest.LabelControlStatus.Text = "Reading File ...."
            Application.DoEvents()

            Common.LogPost("Logs Upload")

            Dim sr As New StreamReader(TextEditUploadLogs.Text)
            Dim NumberOfLines As Integer
            Dim x As String
            Dim CARDNO As String
            Dim paycode As String
            Dim OFFICEPUNCH As DateTime
            Dim DeviceID As String
            Dim EmpName As String = ""
            'Dim P_DAY As String = "N"
            'Dim ISMANUAL As String = "N"
            Dim sSql As String
            Dim i As Integer = 0
            Dim deviceType As String = "UltraFace"
            Do While sr.Peek >= 0
                Dim IN_OUT As String = ""

                'MsgBox(Path.GetExtension(TextEditUploadLogs.Text))
                If Path.GetExtension(TextEditUploadLogs.Text) = ".dat" Then      'ZK
                    x = sr.ReadLine() '.Split(" ")
                    Dim tmp() As String = x.Split(vbTab)
                    Try
                        CARDNO = tmp(0).Trim 'x.Substring(0, 9)
                        If IsNumeric(CARDNO.Trim) = True Then
                            CARDNO = Convert.ToInt64(CARDNO).ToString("000000000000")
                        Else
                            'CARDNO = x(0)
                        End If
                        DeviceID = "1"
                        OFFICEPUNCH = Convert.ToDateTime(tmp(1).Trim) ' Convert.ToDateTime(x.Substring(10, 19))
                        XtraMasterTest.LabelControlStatus.Text = "Reading Punch " & CARDNO & " " & OFFICEPUNCH.ToString("yyyy-MM-dd HH:mm:ss")
                        Application.DoEvents()
                    Catch ex As Exception
                        XtraMasterTest.LabelControlStatus.Text = ""
                        Application.DoEvents()
                        Me.Cursor = Cursors.Default
                        XtraMessageBox.Show(ulf, "<size=10>File format incorrect</size>", "<size>Error</size>")
                        Exit Sub
                        'Continue Do
                    End Try
                ElseIf Path.GetExtension(TextEditUploadLogs.Text) = ".txt" Or Path.GetExtension(TextEditUploadLogs.Text) = ".TXT" Then   'BIO
                    If TextEditUploadLogs.Text.Contains("Logs_") Then  'ATF686n
                        If i = 0 Or i = 1 Then
                            i = i + 1
                            Continue Do
                        End If
                        x = sr.ReadLine() '.Split(" ")
                        If IsNumeric(x.Substring(0, 1)) = False Then
                            Continue Do
                        End If

                        Try
                            Dim x1() As String = x.Split(vbTab)
                            'MsgBox(x1(0))
                            CARDNO = x1(1) 'x.Substring(9, 10)
                            If IsNumeric(CARDNO.Trim) = True Then
                                CARDNO = Convert.ToInt64(CARDNO).ToString("000000000000")
                            Else
                                'CARDNO = x(0)
                            End If
                            DeviceID = x1(3)
                            OFFICEPUNCH = Convert.ToDateTime(x1(5).Replace("  ", " ")) 'Convert.ToDateTime(x.Substring(57, 19))
                            EmpName = x1(2).Trim
                            XtraMasterTest.LabelControlStatus.Text = "Reading Punch " & CARDNO & " " & OFFICEPUNCH.ToString("yyyy-MM-dd HH:mm:ss")
                            Application.DoEvents()
                        Catch ex As Exception
                            XtraMasterTest.LabelControlStatus.Text = ""
                            Application.DoEvents()
                            Me.Cursor = Cursors.Default
                            XtraMessageBox.Show(ulf, "<size=10>File format incorrect</size>", "<size>Error</size>")
                            Exit Sub
                            'Continue Do
                        End Try

                    Else  'BIO
                        If i = 0 Then
                            i = i + 1
                            Continue Do
                        End If
                        x = sr.ReadLine() '.Split(" ")
                        If IsNumeric(x.Substring(0, 1)) = False Then
                            Continue Do
                        End If
                        Try
                            Dim x1() As String = x.Split(vbTab)
                            'MsgBox(x1(0))
                            CARDNO = x1(2) 'x.Substring(9, 10)
                            If IsNumeric(CARDNO.Trim) = True Then
                                CARDNO = Convert.ToInt64(CARDNO).ToString("000000000000")
                            Else
                                'CARDNO = x(0)
                            End If
                            DeviceID = x1(1)
                            OFFICEPUNCH = Convert.ToDateTime(x1(6)) 'Convert.ToDateTime(x.Substring(57, 19))
                            EmpName = x1(3)
                            XtraMasterTest.LabelControlStatus.Text = "Reading Punch " & CARDNO & " " & OFFICEPUNCH.ToString("yyyy-MM-dd HH:mm:ss")
                            Application.DoEvents()
                        Catch ex As Exception
                            XtraMasterTest.LabelControlStatus.Text = ""
                            Application.DoEvents()
                            Me.Cursor = Cursors.Default
                            XtraMessageBox.Show(ulf, "<size=10>File format incorrect</size>", "<size>Error</size>")
                            Exit Sub
                            'Continue Do
                        End Try
                    End If
                ElseIf Path.GetExtension(TextEditUploadLogs.Text) = ".csv" Then   'Hikvision
                    If LookUpEditDevice.EditValue.ToString.Trim = "" Then
                        XtraMessageBox.Show(ulf, "<size=10>Select Device</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        LookUpEditDevice.Select()
                        Exit Sub
                    End If
                    sSql = "SELECT ID_NO, IN_OUT, LEDIP from tblMachine where ID_NO='" & LookUpEditDevice.EditValue.ToString.Trim & "'"
                    Dim ds As DataSet = New DataSet
                    If Common.servername = "Access" Then
                        adapA = New OleDbDataAdapter(sSql, Common.con1)
                        adapA.Fill(ds)
                    Else
                        adap = New SqlDataAdapter(sSql, Common.con)
                        adap.Fill(ds)
                    End If


                    If ds.Tables(0).Rows.Count > 0 Then
                        IN_OUT = ds.Tables(0).Rows(0).Item("IN_OUT").ToString.Trim
                    End If

                    If i = 0 Then
                        i = i + 1
                        x = sr.ReadLine()
                        Dim x1() As String = x.Split(",")
                        If x1(0).Trim.Contains("User") Then  'FP machine
                            deviceType = "UltraFP"
                        ElseIf x1(0).Trim.Contains("Employee ID") Then '331, 671
                            deviceType = "UltraFace"
                        End If
                        Continue Do
                    End If
                    x = sr.ReadLine() '.Split(" ")
                    If x.Trim = "" Then
                        Continue Do
                    End If
                    Try
                        Dim x1() As String = x.Split(",")
                        If deviceType = "UltraFace" Then
                            CARDNO = x1(0).Trim("'").Trim   'x.Substring(9, 10)
                        ElseIf deviceType = "UltraFP" Then
                            CARDNO = x1(1).Trim("'").Trim
                        End If
                        If CARDNO = "" Then
                            Continue Do
                        End If
                        If IsNumeric(CARDNO.Trim) = True Then
                            CARDNO = Convert.ToInt64(CARDNO).ToString("000000000000")
                        Else
                            'CARDNO = x(0)
                        End If

                        If deviceType = "UltraFace" Then
                            OFFICEPUNCH = Convert.ToDateTime(x1(3).Trim("'").Trim)
                        ElseIf deviceType = "UltraFP" Then
                            OFFICEPUNCH = Convert.ToDateTime(x1(3).Trim("'").Trim).ToString("yyyy-MM-dd") & " " & Convert.ToDateTime(x1(4).Trim("'").Trim).ToString("HH:mm:ss")
                        End If

                    Catch ex As Exception

                    End Try

                End If
                Dim sdwEnrollNumber As String
                If IsNumeric(CARDNO) Then
                    sdwEnrollNumber = Convert.ToDouble(CARDNO)
                Else
                    sdwEnrollNumber = CARDNO
                End If

                Dim paycodelist As New List(Of String)()
                Dim paycodelistForEmpMaster As New List(Of String)() ' for emp master  
                paycodelist.Add(sdwEnrollNumber)
                paycodelistForEmpMaster.Add(sdwEnrollNumber & ";" & EmpName)
                If ToggleEmp.IsOn = True Then
                    Dim payCodeArrEmpMaster() As String = paycodelistForEmpMaster.ToArray
                    Common.CreateEmployee(payCodeArrEmpMaster, "Text")
                End If
                sSql = "select TblEmployee.Paycode, EmployeeGroup.Id from TblEmployee, EmployeeGroup where TblEmployee.PRESENTCARDNO='" & CARDNO & "' and EmployeeGroup.GroupId=TblEmployee.EmployeeGroupId"
                '"select PAYCODE from TblEmployee where PRESENTCARDNO = '" & CARDNO & "'"
                ds = New DataSet
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(ds)
                Else
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(ds)
                End If
                If ds.Tables(0).Rows.Count > 0 Then
                    XtraMasterTest.LabelControlStatus.Text = "Uploading logs " & CARDNO & " " & OFFICEPUNCH.ToString("yyyy-MM-dd HH:mm:ss")
                    Application.DoEvents()

                    If IN_OUT = "B" Then
                        Dim ds2 As DataSet = New DataSet
                        Dim sSql2 = "select count (CARDNO) from MachineRawPunch where CARDNO = '" & CARDNO & "' and FORMAT(OFFICEPUNCH, 'yyyy-MM-dd') = '" & Convert.ToDateTime(OFFICEPUNCH).ToString("yyyy-MM-dd") & "'"
                        If Common.servername = "Access" Then
                            adapA = New OleDbDataAdapter(sSql2, Common.con1)
                            adapA.Fill(ds2)
                        Else
                            sSql2 = "select count (CARDNO) from MachineRawPunch where CARDNO = '" & CARDNO & "' and convert(varchar, OFFICEPUNCH, 23) = '" & Convert.ToDateTime(OFFICEPUNCH).ToString("yyyy-MM-dd") & "'"
                            adap = New SqlDataAdapter(sSql2, Common.con)
                            adap.Fill(ds2)
                        End If

                        If ds2.Tables(0).Rows(0).Item(0) = 0 Then
                            IN_OUT = "I"
                        Else
                            If ds2.Tables(0).Rows(0).Item(0) Mod 2 = 0 Then
                                IN_OUT = "I"
                            Else
                                IN_OUT = "O"
                            End If
                        End If
                    End If
                    sSql = "insert into MachineRawPunch ([CARDNO],[OFFICEPUNCH],[P_DAY],[ISMANUAL],[ReasonCode],[MC_NO],[INOUT],[PAYCODE]) values('" & CARDNO & "','" & OFFICEPUNCH.ToString("yyyy-MM-dd HH:mm:00") & "','N', 'N','','" & DeviceID & "','" & IN_OUT & "','" & ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim & "' ) "
                    Dim sSql1 As String = "insert into MachineRawPunchAll ([CARDNO],[OFFICEPUNCH],[P_DAY],[ISMANUAL],[ReasonCode],[MC_NO],[INOUT],[PAYCODE]) values('" & CARDNO & "','" & OFFICEPUNCH.ToString("yyyy-MM-dd HH:mm:00") & "','N', 'N','','" & DeviceID & "','" & IN_OUT & "','" & ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim & "' ) "
                    If servername = "Access" Then
                        If Common.con1.State <> ConnectionState.Open Then
                            Common.con1.Open()
                        End If
                        Try
                            cmd1 = New OleDbCommand(sSql, Common.con1)
                            cmd1.ExecuteNonQuery()
                            cmd1 = New OleDbCommand(sSql1, Common.con1)
                            cmd1.ExecuteNonQuery()
                        Catch ex As Exception
                            Continue Do
                        End Try

                        If Common.con1.State <> ConnectionState.Closed Then
                            Common.con1.Close()
                        End If
                    Else
                        If Common.con.State <> ConnectionState.Open Then
                            Common.con.Open()
                        End If
                        Try
                            cmd = New SqlCommand(sSql, Common.con)
                            cmd.ExecuteNonQuery()
                            cmd = New SqlCommand(sSql1, Common.con)
                            cmd.ExecuteNonQuery()
                        Catch ex As Exception
                            Continue Do
                        End Try
                        If Common.con.State <> ConnectionState.Closed Then
                            Common.con.Close()
                        End If
                    End If
                    comclass.Remove_Duplicate_Punches(OFFICEPUNCH, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("Id"))
                End If
                'NumberOfLines += 1
                i = i + 1
            Loop
            'MsgBox(NumberOfLines.ToString)
            sr.Close()
            sr.Dispose()
        End If
        If ToggleEmp.IsOn = True Then
            Common.loadEmp()
        End If
        Me.Cursor = Cursors.Default
        ''ProgressBarControl1.Visible = True
        DevExpress.XtraSplashScreen.SplashScreenManager.ShowForm(Me, GetType(WaitForm1), True, True, False)
        'comclass.Load_Corporate_PolicySql()
        Common.LogPost("Back Day Processing; From Date:" & DateEdit3.DateTime.ToString("dd/MM/yyyy"))
        If CheckAll.Checked = True Then
            Dim adap As SqlDataAdapter
            Dim adapA As OleDbDataAdapter
            Dim ds As DataSet = New DataSet
            Dim sSql As String = "select TblEmployee.PAYCODE,tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK,EmployeeGroup.ID from tblEmployeeShiftMaster, TblEmployee, EmployeeGroup where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.ACTIVE = 'Y' and TblEmployee.EmployeeGroupId = EmployeeGroup.GroupId order by TblEmployee.PRESENTCARDNO"
            '"select TblEmployee.PAYCODE, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK from TblEmployee, tblEmployeeShiftMaster where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.ACTIVE = 'Y'"
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
            If ds.Tables(0).Rows.Count > 0 Then
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    XtraMasterTest.LabelControlCount.Text = "Processing " & i + 1 & " of " & ds.Tables(0).Rows.Count
                    Application.DoEvents()

                    Dim pay As String = ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim
                    If CheckDuplicate.Checked = True Then
                        Dim mindate As DateTime = DateEdit3.DateTime
                        Dim maxdate As DateTime = Now
                        Do While mindate <= maxdate
                            comclass.Remove_Duplicate_Punches(mindate, pay, ds.Tables(0).Rows(i).Item("Id"))
                            mindate = mindate.AddDays(1)
                            If mindate > maxdate Then
                                Exit Do
                            End If
                        Loop
                    End If

                    'comclass.Remove_Duplicate_Punches(DateEdit3.DateTime, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim)
                    If ds.Tables(0).Rows(i).Item("ISROUNDTHECLOCKWORK").ToString = "Y" Then
                        If Common.PrcessMode = "M" Then
                            comclass.Process_AllnonRTCINOUT(DateEdit3.DateTime.AddDays(-1), Now.Date, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(i).Item("Id"), ds.Tables(0).Rows(i).Item("ISROUNDTHECLOCKWORK").ToString)
                        Else
                            comclass.Process_AllRTC(DateEdit3.DateTime.AddDays(-1), Now.Date, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(i).Item("Id"))
                        End If
                        'comclass.Process_AllRTC(DateEdit3.DateTime.AddDays(-1), Now.Date, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(i).Item("Id"))
                    Else
                        If Common.PrcessMode = "M" Then
                            comclass.Process_AllnonRTCINOUT(DateEdit3.DateTime, Now.Date, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(i).Item("Id"), ds.Tables(0).Rows(i).Item("ISROUNDTHECLOCKWORK").ToString)
                        Else
                            comclass.Process_AllnonRTC(DateEdit3.DateTime, Now.Date, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(i).Item("Id"))
                        End If
                        If Common.EmpGrpArr(ds.Tables(0).Rows(i).Item("Id")).SHIFTTYPE = "M" Then
                            comclass.Process_AllnonRTCMulti(DateEdit3.DateTime, Now.Date, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(i).Item("Id"))
                        End If
                    End If
                Next
            End If
        ElseIf CheckSelective.Checked = True Then
            Dim sSql As String
            If ComboBoxEdit1.EditValue = "Paycode" Then
                Dim paycode() As String = PopupContainerEdit1.EditValue.ToString.Split(",")
                If PopupContainerEdit1.EditValue.ToString.Length = 0 Then
                    DevExpress.XtraSplashScreen.SplashScreenManager.CloseForm(False)
                    Me.Cursor = Cursors.Default
                    XtraMessageBox.Show(ulf, "<size=10>Please Select Paycode.</size>", "iAS")
                    PopupContainerEdit1.Select()
                    Exit Sub
                End If
                For i As Integer = 0 To paycode.Length - 1
                    XtraMasterTest.LabelControlCount.Text = "Processing " & i + 1 & " of " & paycode.Length
                    Application.DoEvents()
                    Dim adap As SqlDataAdapter
                    Dim adapA As OleDbDataAdapter
                    Dim ds As DataSet = New DataSet
                    sSql = "select tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK,EmployeeGroup.ID from tblEmployeeShiftMaster, TblEmployee, EmployeeGroup where tblEmployeeShiftMaster.PAYCODE='" & paycode(i).Trim & "' and tblEmployeeShiftMaster.PAYCODE= TblEmployee.PAYCODE and TblEmployee.EmployeeGroupId = EmployeeGroup.GroupId"
                    ' "select ISROUNDTHECLOCKWORK from tblEmployeeShiftMaster where PAYCODE = '" & paycode(i).Trim & "'"
                    If Common.servername = "Access" Then
                        adapA = New OleDbDataAdapter(sSql, Common.con1)
                        adapA.Fill(ds)
                    Else
                        adap = New SqlDataAdapter(sSql, Common.con)
                        adap.Fill(ds)
                    End If
                    If ds.Tables(0).Rows.Count > 0 Then
                        If CheckDuplicate.Checked = True Then
                            Dim mindate As DateTime = DateEdit3.DateTime
                            Dim maxdate As DateTime = Now
                            Do While mindate <= maxdate
                                comclass.Remove_Duplicate_Punches(mindate, paycode(i).Trim, ds.Tables(0).Rows(0).Item("Id"))
                                mindate = mindate.AddDays(1)
                                If mindate > maxdate Then
                                    Exit Do
                                End If
                            Loop
                        End If
                        If ds.Tables(0).Rows(0).Item("ISROUNDTHECLOCKWORK").ToString = "Y" Then
                            comclass.Process_AllRTC(DateEdit3.DateTime.AddDays(-1), Now.Date, paycode(i).Trim, paycode(i).Trim, ds.Tables(0).Rows(0).Item("Id"))
                            If Common.PrcessMode = "M" Then
                                comclass.Process_AllnonRTCINOUT(DateEdit3.DateTime.AddDays(-1), Now.Date, paycode(i).Trim, paycode(i).Trim, ds.Tables(0).Rows(i).Item("Id"), ds.Tables(0).Rows(i).Item("ISROUNDTHECLOCKWORK").ToString)
                            Else
                                comclass.Process_AllRTC(DateEdit3.DateTime.AddDays(-1), Now.Date, paycode(i).Trim, paycode(i).Trim, ds.Tables(0).Rows(0).Item("Id"))
                            End If
                        Else
                            If Common.PrcessMode = "M" Then
                                comclass.Process_AllnonRTCINOUT(DateEdit3.DateTime, Now.Date, paycode(i).Trim, paycode(i).Trim, ds.Tables(0).Rows(0).Item("Id"), ds.Tables(0).Rows(i).Item("ISROUNDTHECLOCKWORK").ToString)
                            Else
                                comclass.Process_AllnonRTC(DateEdit3.DateTime, Now.Date, paycode(i).Trim, paycode(i).Trim, ds.Tables(0).Rows(0).Item("Id"))
                            End If
                            'comclass.Process_AllnonRTC(DateEdit3.DateTime, Now.Date, paycode(i).Trim, paycode(i).Trim, ds.Tables(0).Rows(0).Item("Id"))
                            If Common.EmpGrpArr(ds.Tables(0).Rows(0).Item("Id")).SHIFTTYPE = "M" Then
                                comclass.Process_AllnonRTCMulti(DateEdit3.DateTime, Now.Date, paycode(i).Trim, paycode(i).Trim, ds.Tables(0).Rows(0).Item("Id"))
                            End If
                        End If
                    End If
                Next
            ElseIf ComboBoxEdit1.EditValue = "Company" Then
                Dim com() As String = PopupContainerEdit1.EditValue.ToString.Split(",")
                If PopupContainerEdit1.EditValue.ToString.Length = 0 Then
                    DevExpress.XtraSplashScreen.SplashScreenManager.CloseForm(False)
                    Me.Cursor = Cursors.Default
                    XtraMessageBox.Show(ulf, "<size=10>Please Select Company.</size>", "iAS")
                    PopupContainerEdit1.Select()
                    Exit Sub
                End If
                Dim ls As New List(Of String)()
                For i As Integer = 0 To com.Length - 1
                    ls.Add(com(i).Trim)
                Next
                'select PAYCODE from TblEmployee where COMPANYCODE IN ('01')  and ACTIVE = 'Y'
                sSql = "select TblEmployee.PAYCODE, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK,EmployeeGroup.Id from TblEmployee, tblEmployeeShiftMaster, EmployeeGroup where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.COMPANYCODE IN ('" & String.Join("', '", ls.ToArray()) & "')  and TblEmployee.ACTIVE = 'Y' and EmployeeGroup.GroupId=TblEmployee.EmployeeGroupId"
                Dim adap As SqlDataAdapter
                Dim adapA As OleDbDataAdapter
                Dim ds As DataSet = New DataSet
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(ds)
                Else
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(ds)
                End If
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    XtraMasterTest.LabelControlCount.Text = "Processing " & i + 1 & " of " & ds.Tables(0).Rows.Count
                    Application.DoEvents()
                    If CheckDuplicate.Checked = True Then
                        Dim mindate As DateTime = DateEdit3.DateTime
                        Dim maxdate As DateTime = Now
                        Do While mindate <= maxdate
                            comclass.Remove_Duplicate_Punches(mindate, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(i).Item("Id"))
                            mindate = mindate.AddDays(1)
                            If mindate > maxdate Then
                                Exit Do
                            End If
                        Loop
                    End If
                    If ds.Tables(0).Rows(i).Item("ISROUNDTHECLOCKWORK").ToString = "Y" Then
                        'comclass.Process_AllRTC(DateEdit3.DateTime.AddDays(-1), Now.Date, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(i).Item("Id"))
                        If Common.PrcessMode = "M" Then
                            comclass.Process_AllnonRTCINOUT(DateEdit3.DateTime.AddDays(-1), Now.Date, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(i).Item("Id"), ds.Tables(0).Rows(i).Item("ISROUNDTHECLOCKWORK").ToString)
                        Else
                            comclass.Process_AllRTC(DateEdit3.DateTime.AddDays(-1), Now.Date, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(i).Item("Id"))
                        End If
                    Else
                        If Common.PrcessMode = "M" Then
                            comclass.Process_AllnonRTCINOUT(DateEdit3.DateTime, Now.Date, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(i).Item("Id"), ds.Tables(0).Rows(i).Item("ISROUNDTHECLOCKWORK").ToString)
                        Else
                            comclass.Process_AllnonRTC(DateEdit3.DateTime, Now.Date, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(i).Item("Id"))
                        End If
                        'comclass.Process_AllnonRTC(DateEdit3.DateTime, Now.Date, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(i).Item("Id"))
                        If Common.EmpGrpArr(ds.Tables(0).Rows(i).Item("Id")).SHIFTTYPE = "M" Then
                            comclass.Process_AllnonRTCMulti(DateEdit3.DateTime, Now.Date, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(i).Item("Id"))
                        End If
                    End If
                Next
            ElseIf ComboBoxEdit1.EditValue = "Department" Then
                Dim com() As String = PopupContainerEdit1.EditValue.ToString.Split(",")
                If PopupContainerEdit1.EditValue.ToString.Length = 0 Then
                    DevExpress.XtraSplashScreen.SplashScreenManager.CloseForm(False)
                    Me.Cursor = Cursors.Default
                    XtraMessageBox.Show(ulf, "<size=10>Please Select Department.</size>", "iAS")
                    PopupContainerEdit1.Select()
                    Exit Sub
                End If
                Dim ls As New List(Of String)()
                For i As Integer = 0 To com.Length - 1
                    ls.Add(com(i).Trim)
                Next
                'select PAYCODE from TblEmployee where COMPANYCODE IN ('01')  and ACTIVE = 'Y'
                sSql = "select TblEmployee.PAYCODE, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK, EmployeeGroup.Id from TblEmployee, tblEmployeeShiftMaster, EmployeeGroup where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.DEPARTMENTCODE IN ('" & String.Join("', '", ls.ToArray()) & "')  and TblEmployee.ACTIVE = 'Y' and EmployeeGroup.GroupId=TblEmployee.EmployeeGroupId"
                Dim adap As SqlDataAdapter
                Dim adapA As OleDbDataAdapter
                Dim ds As DataSet = New DataSet
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(ds)
                Else
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(ds)
                End If
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    XtraMasterTest.LabelControlCount.Text = "Processing " & i + 1 & " of " & ds.Tables(0).Rows.Count
                    Application.DoEvents()
                    If CheckDuplicate.Checked = True Then
                        Dim mindate As DateTime = DateEdit3.DateTime
                        Dim maxdate As DateTime = Now
                        Do While mindate <= maxdate
                            comclass.Remove_Duplicate_Punches(mindate, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(i).Item("Id"))
                            mindate = mindate.AddDays(1)
                            If mindate > maxdate Then
                                Exit Do
                            End If
                        Loop
                    End If
                    If ds.Tables(0).Rows(i).Item("ISROUNDTHECLOCKWORK").ToString = "Y" Then
                        'comclass.Process_AllRTC(DateEdit3.DateTime.AddDays(-1), Now.Date, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(i).Item("Id"))
                        If Common.PrcessMode = "M" Then
                            comclass.Process_AllnonRTCINOUT(DateEdit3.DateTime.AddDays(-1), Now.Date, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(i).Item("Id"), ds.Tables(0).Rows(i).Item("ISROUNDTHECLOCKWORK").ToString)
                        Else
                            comclass.Process_AllRTC(DateEdit3.DateTime.AddDays(-1), Now.Date, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(i).Item("Id"))
                        End If
                    Else
                        If Common.PrcessMode = "M" Then
                            comclass.Process_AllnonRTCINOUT(DateEdit3.DateTime, Now.Date, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(i).Item("Id"), ds.Tables(0).Rows(i).Item("ISROUNDTHECLOCKWORK").ToString)
                        Else
                            comclass.Process_AllnonRTC(DateEdit3.DateTime, Now.Date, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(i).Item("Id"))
                        End If
                        'comclass.Process_AllnonRTC(DateEdit3.DateTime, Now.Date, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(i).Item("Id"))
                        If Common.EmpGrpArr(ds.Tables(0).Rows(i).Item("Id")).SHIFTTYPE = "M" Then
                            comclass.Process_AllnonRTCMulti(DateEdit3.DateTime, Now.Date, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(i).Item("Id"))
                        End If
                    End If
                Next
            ElseIf ComboBoxEdit1.EditValue = "Location" Then
                Dim com() As String = PopupContainerEdit1.EditValue.ToString.Split(",")
                If PopupContainerEdit1.EditValue.ToString.Length = 0 Then
                    DevExpress.XtraSplashScreen.SplashScreenManager.CloseForm(False)
                    Me.Cursor = Cursors.Default
                    XtraMessageBox.Show(ulf, "<size=10>Please Select Location.</size>", "iAS")
                    PopupContainerEdit1.Select()
                    Exit Sub
                End If
                Dim ls As New List(Of String)()
                For i As Integer = 0 To com.Length - 1
                    ls.Add(com(i).Trim)
                Next
                'select PAYCODE from TblEmployee where COMPANYCODE IN ('01')  and ACTIVE = 'Y'
                sSql = "select TblEmployee.PAYCODE, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK, EmployeeGroup.ID from TblEmployee, tblEmployeeShiftMaster, EmployeeGroup where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "')  and TblEmployee.ACTIVE = 'Y' and TblEmployee.EmployeeGroupId = EmployeeGroup.GroupId"
                Dim adap As SqlDataAdapter
                Dim adapA As OleDbDataAdapter
                Dim ds As DataSet = New DataSet
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(ds)
                Else
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(ds)
                End If
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    XtraMasterTest.LabelControlCount.Text = "Processing " & i + 1 & " of " & ds.Tables(0).Rows.Count
                    Application.DoEvents()
                    If CheckDuplicate.Checked = True Then
                        Dim mindate As DateTime = DateEdit3.DateTime
                        Dim maxdate As DateTime = Now
                        Do While mindate <= maxdate
                            comclass.Remove_Duplicate_Punches(mindate, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(i).Item("Id"))
                            mindate = mindate.AddDays(1)
                            If mindate > maxdate Then
                                Exit Do
                            End If
                        Loop
                    End If
                    If ds.Tables(0).Rows(i).Item("ISROUNDTHECLOCKWORK").ToString = "Y" Then
                        'comclass.Process_AllRTC(DateEdit3.DateTime.AddDays(-1), Now.Date, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(i).Item("Id"))
                        If Common.PrcessMode = "M" Then
                            comclass.Process_AllnonRTCINOUT(DateEdit3.DateTime.AddDays(-1), Now.Date, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(i).Item("Id"), ds.Tables(0).Rows(i).Item("ISROUNDTHECLOCKWORK").ToString)
                        Else
                            comclass.Process_AllRTC(DateEdit3.DateTime.AddDays(-1), Now.Date, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(i).Item("Id"))
                        End If
                    Else
                        If Common.PrcessMode = "M" Then
                            comclass.Process_AllnonRTCINOUT(DateEdit3.DateTime, Now.Date, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(i).Item("Id"), ds.Tables(0).Rows(i).Item("ISROUNDTHECLOCKWORK").ToString)
                        Else
                            comclass.Process_AllnonRTC(DateEdit3.DateTime, Now.Date, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(i).Item("Id"))
                        End If
                        'comclass.Process_AllnonRTC(DateEdit3.DateTime, Now.Date, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(i).Item("Id"))
                        If Common.EmpGrpArr(ds.Tables(0).Rows(i).Item("Id")).SHIFTTYPE = "M" Then
                            comclass.Process_AllnonRTCMulti(DateEdit3.DateTime, Now.Date, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(i).Item("Id"))
                        End If
                    End If
                Next
            End If
        End If
        XtraMasterTest.LabelControlCount.Text = ""
        XtraMasterTest.LabelControlStatus.Text = ""
        Application.DoEvents()
        SplashScreenManager.CloseForm(False)
        Me.Cursor = Cursors.Default
        XtraMessageBox.Show(ulf, "<size=10>Processed Successfully</size>", "Success")
    End Sub
    Private Sub ComboBoxEdit1_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBoxEdit1.SelectedIndexChanged
        selectpopupdropdown()
    End Sub
    Private Sub selectpopupdropdown()
        PopupContainerEdit1.EditValue = ""
        If ComboBoxEdit1.EditValue = "Paycode" Then
            LabelControl5.Visible = True
            LabelControl5.Text = "Select Paycode"
            'TextEdit3.Visible = True
            PopupContainerEdit1.Visible = True
            PopupContainerEdit1.Properties.PopupControl = PopupContainerControlEmp
        ElseIf ComboBoxEdit1.EditValue = "Company" Then
            LabelControl5.Visible = True
            LabelControl5.Text = "Select Company"
            PopupContainerEdit1.Visible = True
            PopupContainerEdit1.Properties.PopupControl = PopupContainerControlCompany
        ElseIf ComboBoxEdit1.EditValue = "Location" Then
            LabelControl5.Visible = True
            LabelControl5.Text = "Select Location"
            PopupContainerEdit1.Visible = True
            PopupContainerEdit1.Properties.PopupControl = PopupContainerControlLocation
        ElseIf ComboBoxEdit1.EditValue = "Department" Then
            LabelControl5.Visible = True
            LabelControl5.Text = "Select Department"
            PopupContainerEdit1.Visible = True
            PopupContainerEdit1.Properties.PopupControl = PopupContainerControlDept
        End If
    End Sub

    Private Sub BtnOracle_Click(sender As Object, e As EventArgs) Handles BtnOracle.Click


        Dim comclass As Common = New Common

        Me.Cursor = Cursors.Default
        DevExpress.XtraSplashScreen.SplashScreenManager.ShowForm(Me, GetType(WaitForm1), True, True, False)
        Common.LogPost("Back Day Leave Processing; From Date:" & DateEdit3.DateTime.ToString("dd/MM/yyyy"))
        Dim ssql As String = ""
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim ds As DataSet = New DataSet
        If CheckAll.Checked = True Then
            If Common.servername = "Access" Then
                ssql = "select * from tbltimeregister where LEAVEAMOUNT> 0  and FORMAT(dateoffice,'yyyy-MM-dd HH:mm:ss') >='" & DateFromLeave.DateTime.ToString("yyyy-MM-dd 00:00:00") & "' and FORMAT(dateoffice,'yyyy-MM-dd HH:mm:ss') <='" & DateToLeave.DateTime.ToString("yyyy-MM-dd 00:00:00") & "'  order by  paycode, dateoffice"
            Else
                ssql = "select * from tbltimeregister where LEAVEAMOUNT> 0  and dateoffice >='" & DateFromLeave.DateTime.ToString("yyyy-MM-dd 00:00:00") & "' and dateoffice <='" & DateToLeave.DateTime.ToString("yyyy-MM-dd 00:00:00") & "'  order by  paycode, dateoffice"
            End If

        ElseIf CheckSelective.Checked = True Then
            If ComboBoxEdit1.EditValue = "Paycode" Then
                Dim paycode() As String = PopupContainerEdit1.EditValue.ToString.Split(",")
                If PopupContainerEdit1.EditValue.ToString.Length = 0 Then
                    DevExpress.XtraSplashScreen.SplashScreenManager.CloseForm(False)
                    Me.Cursor = Cursors.Default
                    XtraMessageBox.Show(ulf, "<size=10>Please Select Paycode.</size>", "iAS")
                    PopupContainerEdit1.Select()
                    Exit Sub
                End If
                If Common.servername = "Access" Then
                    ssql = "select * from tbltimeregister where LEAVEAMOUNT> 0  and FORMAT(dateoffice,'yyyy-MM-dd HH:mm:ss') >='" & DateFromLeave.DateTime.ToString("yyyy-MM-dd 00:00:00") & "' and FORMAT(dateoffice,'yyyy-MM-dd HH:mm:ss') <='" & DateToLeave.DateTime.ToString("yyyy-MM-dd 00:00:00") & "' and paycode in ('" & String.Join("', '", paycode.ToArray()) & "') order by  paycode, dateoffice"
                Else
                    ssql = "select * from tbltimeregister where LEAVEAMOUNT> 0  and dateoffice >='" & DateFromLeave.DateTime.ToString("yyyy-MM-dd 00:00:00") & "' and dateoffice <='" & DateToLeave.DateTime.ToString("yyyy-MM-dd 00:00:00") & "' and paycode in ('" & String.Join("', '", paycode.ToArray()) & "') order by  paycode, dateoffice"
                End If


            ElseIf ComboBoxEdit1.EditValue = "Company" Then
                'Dim com() As String = PopupContainerEdit1.EditValue.ToString.Split(",")
                'If PopupContainerEdit1.EditValue.ToString.Length = 0 Then
                '    DevExpress.XtraSplashScreen.SplashScreenManager.CloseForm(False)
                '    Me.Cursor = Cursors.Default
                '    XtraMessageBox.Show(ulf, "<size=10>Please Select Company.</size>", "iAS")
                '    PopupContainerEdit1.Select()
                '    Exit Sub
                'End If
                'Dim ls As New List(Of String)()
                'For i As Integer = 0 To com.Length - 1
                '    ls.Add(com(i).Trim)
                'Next
                ''select PAYCODE from TblEmployee where COMPANYCODE IN ('01')  and ACTIVE = 'Y'
                'sSql = "select TblEmployee.PAYCODE, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK,EmployeeGroup.Id from TblEmployee, tblEmployeeShiftMaster, EmployeeGroup where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.COMPANYCODE IN ('" & String.Join("', '", ls.ToArray()) & "')  and TblEmployee.ACTIVE = 'Y' and EmployeeGroup.GroupId=TblEmployee.EmployeeGroupId"

            ElseIf ComboBoxEdit1.EditValue = "Department" Then
                'Dim com() As String = PopupContainerEdit1.EditValue.ToString.Split(",")
                'If PopupContainerEdit1.EditValue.ToString.Length = 0 Then
                '    DevExpress.XtraSplashScreen.SplashScreenManager.CloseForm(False)
                '    Me.Cursor = Cursors.Default
                '    XtraMessageBox.Show(ulf, "<size=10>Please Select Department.</size>", "iAS")
                '    PopupContainerEdit1.Select()
                '    Exit Sub
                'End If
                'Dim ls As New List(Of String)()
                'For i As Integer = 0 To com.Length - 1
                '    ls.Add(com(i).Trim)
                'Next
            ElseIf ComboBoxEdit1.EditValue = "Location" Then
                'Dim com() As String = PopupContainerEdit1.EditValue.ToString.Split(",")
                'If PopupContainerEdit1.EditValue.ToString.Length = 0 Then
                '    DevExpress.XtraSplashScreen.SplashScreenManager.CloseForm(False)
                '    Me.Cursor = Cursors.Default
                '    XtraMessageBox.Show(ulf, "<size=10>Please Select Location.</size>", "iAS")
                '    PopupContainerEdit1.Select()
                '    Exit Sub
                'End If
                'Dim ls As New List(Of String)()
                'For i As Integer = 0 To com.Length - 1
                '    ls.Add(com(i).Trim)
                'Next            
            End If
        End If

        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(ssql, Common.con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(ssql, Common.con)
            adap.Fill(ds)
        End If
        If ds.Tables(0).Rows.Count > 0 Then
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                Dim fordate As DateTime = Convert.ToDateTime(ds.Tables(0).Rows(i)("dateoffice").ToString.Trim)
                XtraMasterTest.LabelControlStatus.Text = "Processing For " & ds.Tables(0).Rows(i)("paycode").ToString.Trim & " " & fordate.ToString("dd/MM/yyyy")
                XtraMasterTest.LabelControlCount.Text = "Processing " & i + 1 & " of " & ds.Tables(0).Rows.Count
                Application.DoEvents()
                ssql = "select * from leaveapplication where paycode='" & ds.Tables(0).Rows(i)("paycode").ToString.Trim & "' and ForDate='" & fordate.ToString("yyyy-MM-dd 00:00:00") & "'"
                Dim ds1 As DataSet = New DataSet
                If Common.servername = "Access" Then
                    ssql = "select * from leaveapplication where paycode='" & ds.Tables(0).Rows(i)("paycode").ToString.Trim & "' and FORMAT(ForDate,'yyyy-MM-dd HH:mm:ss')='" & fordate.ToString("yyyy-MM-dd 00:00:00") & "'"
                    adapA = New OleDbDataAdapter(ssql, Common.con1)
                    adapA.Fill(ds1)
                Else
                    adap = New SqlDataAdapter(ssql, Common.con)
                    adap.Fill(ds1)
                End If
                If ds1.Tables(0).Rows.Count = 0 Then
                    Dim Leavecodetmp As String = ""
                    Dim LEAVETYPE As String = ""
                    Dim Voucher_Notmp As String = ""
                    Dim LEAVETYPE1 As String = ""
                    Dim LEAVETYPE2 As String = ""
                    Dim FIRSTHALFLEAVECODE As String = ""
                    Dim SECONDHALFLEAVECODE As String = ""
                    Dim leaveamount = 0
                    Dim leaveamount1 = 0
                    Dim leaveamount2 = 0

                    If Common.servername = "Access" Then
                        ssql = "update tblTimeRegister set  Leavecode='" & Leavecodetmp & "', LEAVETYPE= '" & LEAVETYPE & "', Voucher_No='" & Voucher_Notmp & "', LEAVETYPE1='" & LEAVETYPE1 & "', LEAVETYPE2='" & LEAVETYPE2 & "', FIRSTHALFLEAVECODE='" & FIRSTHALFLEAVECODE & "', SECONDHALFLEAVECODE='" & SECONDHALFLEAVECODE & "', leaveamount=" & leaveamount & ", leaveamount1 = " & leaveamount1 & ", leaveamount2=" & leaveamount2 & " where PAYCODE='" & ds.Tables(0).Rows(i)("paycode").ToString.Trim & "' and FORMAT(DateOFFICE,'yyyy-MM-dd HH:mm:ss')='" & fordate.ToString("yyyy-MM-dd HH:mm:ss") & "'"
                        If Common.con1.State <> ConnectionState.Open Then
                            Common.con1.Open()
                        End If
                        cmd1 = New OleDbCommand(ssql, Common.con1)
                        cmd1.ExecuteNonQuery()
                        If Common.con1.State <> ConnectionState.Closed Then
                            Common.con1.Close()
                        End If
                    Else
                        ssql = "update tblTimeRegister set Leavecode='" & Leavecodetmp & "', LEAVETYPE= '" & LEAVETYPE & "', Voucher_No='" & Voucher_Notmp & "', LEAVETYPE1='" & LEAVETYPE1 & "', LEAVETYPE2='" & LEAVETYPE2 & "', FIRSTHALFLEAVECODE='" & FIRSTHALFLEAVECODE & "', SECONDHALFLEAVECODE='" & SECONDHALFLEAVECODE & "', leaveamount=" & leaveamount & ", leaveamount1 = " & leaveamount1 & ", leaveamount2=" & leaveamount2 & " where PAYCODE='" & ds.Tables(0).Rows(i)("paycode").ToString.Trim & "' and DateOFFICE='" & fordate.ToString("yyyy-MM-dd HH:mm:ss") & "'"
                        If Common.con.State <> ConnectionState.Open Then
                            Common.con.Open()
                        End If
                        cmd = New SqlCommand(ssql, Common.con)
                        cmd.ExecuteNonQuery()
                        If Common.con.State <> ConnectionState.Closed Then
                            Common.con.Close()
                        End If
                    End If
                End If
            Next
        End If


        XtraMasterTest.LabelControlCount.Text = ""
        XtraMasterTest.LabelControlStatus.Text = ""
        Application.DoEvents()
        SplashScreenManager.CloseForm(False)
        Me.Cursor = Cursors.Default
        XtraMessageBox.Show(ulf, "<size=10>Processed Successfully</size>", "Success")
    End Sub

    Private Sub PopupContainerEdit1_QueryPopUp(sender As System.Object, e As System.ComponentModel.CancelEventArgs) Handles PopupContainerEdit1.QueryPopUp
        If ComboBoxEdit1.EditValue = "Paycode" Then
            Dim val As Object = PopupContainerEdit1.EditValue
            If (val Is Nothing) Then
                GridViewEmp.ClearSelection()
            Else
                Dim texts() As String = val.ToString.Split(",")
                For Each text As String In texts
                    If text.Trim.Length = 1 Then
                        text = text.Trim & "  "
                    ElseIf text.Trim.Length = 2 Then
                        text = text.Trim & " "
                    End If
                    Dim rowHandle As Integer = GridViewComp.LocateByValue("PAYCODE", text)
                    GridViewEmp.SelectRow(rowHandle)
                Next
            End If
        ElseIf ComboBoxEdit1.EditValue = "Company" Then
            Dim val As Object = PopupContainerEdit1.EditValue
            If (val Is Nothing) Then
                GridViewComp.ClearSelection()
            Else
                Dim texts() As String = val.ToString.Split(",")
                For Each text As String In texts
                    If text.Trim.Length = 1 Then
                        text = text.Trim & "  "
                    ElseIf text.Trim.Length = 2 Then
                        text = text.Trim & " "
                    End If
                    Dim rowHandle As Integer = GridViewComp.LocateByValue("COMPANYCODE", text)
                    GridViewComp.SelectRow(rowHandle)
                Next
            End If
        ElseIf ComboBoxEdit1.EditValue = "Location" Then
            Dim val As Object = PopupContainerEdit1.EditValue
            If (val Is Nothing) Then
                GridViewLocation.ClearSelection()
            Else
                Dim texts() As String = val.ToString.Split(",")
                For Each text As String In texts
                    If text.Trim.Length = 1 Then
                        text = text.Trim & "  "
                    ElseIf text.Trim.Length = 2 Then
                        text = text.Trim & " "
                    End If
                    Dim rowHandle As Integer = GridViewDept.LocateByValue("BRANCHCODE", text)
                    GridViewLocation.SelectRow(rowHandle)
                Next
            End If
        ElseIf ComboBoxEdit1.EditValue = "Department" Then
            Dim val As Object = PopupContainerEdit1.EditValue
            If (val Is Nothing) Then
                GridViewDept.ClearSelection()
            Else
                Dim texts() As String = val.ToString.Split(",")
                For Each text As String In texts
                    If text.Trim.Length = 1 Then
                        text = text.Trim & "  "
                    ElseIf text.Trim.Length = 2 Then
                        text = text.Trim & " "
                    End If
                    Dim rowHandle As Integer = GridViewDept.LocateByValue("DEPARTMENTCODE", text)
                    GridViewDept.SelectRow(rowHandle)
                Next
            End If
        End If

    End Sub
    Private Sub PopupContainerEdit1_QueryResultValue(sender As System.Object, e As DevExpress.XtraEditors.Controls.QueryResultValueEventArgs) Handles PopupContainerEdit1.QueryResultValue
        If ComboBoxEdit1.EditValue = "Paycode" Then
            Dim selectedRows() As Integer = GridViewEmp.GetSelectedRows
            Dim sb As StringBuilder = New StringBuilder
            For Each selectionRow As Integer In selectedRows
                Dim a As System.Data.DataRowView = GridViewEmp.GetRow(selectionRow)
                If (sb.ToString.Length > 0) Then
                    sb.Append(", ")
                End If
                sb.Append(a.Item("Paycode"))
            Next
            e.Value = sb.ToString
        ElseIf ComboBoxEdit1.EditValue = "Company" Then
            Dim selectedRows() As Integer = GridViewComp.GetSelectedRows
            Dim sb As StringBuilder = New StringBuilder
            For Each selectionRow As Integer In selectedRows
                Dim a As System.Data.DataRowView = GridViewComp.GetRow(selectionRow)
                If (sb.ToString.Length > 0) Then
                    sb.Append(", ")
                End If
                sb.Append(a.Item("COMPANYCODE"))
            Next
            e.Value = sb.ToString
        ElseIf ComboBoxEdit1.EditValue = "Location" Then
            Dim selectedRows() As Integer = GridViewLocation.GetSelectedRows
            Dim sb As StringBuilder = New StringBuilder
            For Each selectionRow As Integer In selectedRows
                Dim a As System.Data.DataRowView = GridViewLocation.GetRow(selectionRow)
                If (sb.ToString.Length > 0) Then
                    sb.Append(", ")
                End If
                sb.Append(a.Item("BRANCHCODE"))
            Next
            e.Value = sb.ToString
        ElseIf ComboBoxEdit1.EditValue = "Department" Then
            Dim selectedRows() As Integer = GridViewDept.GetSelectedRows
            Dim sb As StringBuilder = New StringBuilder
            For Each selectionRow As Integer In selectedRows
                Dim a As System.Data.DataRowView = GridViewDept.GetRow(selectionRow)
                If (sb.ToString.Length > 0) Then
                    sb.Append(", ")
                End If
                sb.Append(a.Item("DEPARTMENTCODE"))
            Next
            e.Value = sb.ToString
        End If

    End Sub
    Private Sub LoadVerification()
        DateEditVerification.DateTime = Now
        'Dim adapS As SqlDataAdapter
        'Dim adapAc As OleDbDataAdapter
        'Dim Rs As DataSet = New DataSet
        'Dim sSql As String = " Select * from tblSetUp where setupid =(Select Convert(varchar(10),Max(Convert(int,Setupid))) from tb" & _
        '"lsetup )"
        'If servername = "Access" Then
        '    sSql = " Select * from tblSetUp where setupid =(Select CVar(Max(CInt(Setupid))) from tblsetup )"
        '    adapAc = New OleDbDataAdapter(sSql, Common.con1)
        '    adapAc.Fill(Rs)
        'Else
        '    adapS = New SqlDataAdapter(sSql, Common.con)
        '    adapS.Fill(Rs)
        'End If
        'If Rs.Tables(0).Rows.Count > 0 Then
        '    If Rs.Tables(0).Rows(0).Item("ISAUTOABSENT").ToString.Trim = "Y" Then
        '        CheckEditLate.Visible = True
        '        SimpleButtonVerification.Enabled = True
        '    Else
        '        'optVerify(0).Visible = False
        '    End If
        '    If Rs.Tables(0).Rows(0).Item("LateVerification").ToString.Trim = "Y" Then
        '        CheckEditWO.Visible = True
        '    Else
        '        CheckEditWO.Visible = False
        '    End If
        'End If

        CheckEditLate.Visible = True
        CheckEditWO.Visible = True
        SimpleButtonVerification.Enabled = True
    End Sub
    Private Sub SimpleButtonVerification_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonVerification.Click
        Dim mCode As String, iFlag As Boolean, iCtr As Integer, iD As Integer
        'Dim Rs As DataSet ' ADODB.Recordset
        Dim rsTime As DataSet 'ADODB.Recordset
        Dim rsTime1 As DataSet
        Dim rsLeaveMaster As DataSet 'ADODB.Recordset
        Dim rsLeaveMaster1 As DataSet 'ADODB.Recordset
        Dim mArr(365) As Object, iPrevalue As Integer, iLateValue As Double, iDeductValue As Double
        Dim mLeaveCode As String, mLeaveType As String
        Me.Cursor = Cursors.WaitCursor
        Dim adapS As SqlDataAdapter
        Dim adapAc As OleDbDataAdapter
        Dim Rs As DataSet = New DataSet
        'Dim rsSetup As DataSet = New DataSet
        Dim rsEmp As DataSet = New DataSet
        Dim mmin_date As DateTime
        Dim mmax_date As DateTime
        Dim cn As Common = New Common
        Dim con1 As OleDbConnection = Common.con1
        Dim con As SqlConnection = Common.con
        Dim Str As String
        Dim sSql As String

        Common.LogPost("Verification; Processing Month:" & DateEditVerification.DateTime.ToString("MM/yyyy"))
        If CheckEditMark.Checked = True Then
            If CheckEditWO.Checked = True Then
                rsEmp = New DataSet
                If CheckAll.Checked = True Then
                    'sSql = "Select a.*,b.companycode,b.departmentcode,d.*,x.Id from tblemployeeshiftmaster a,tblemployee b,tbldepartment c,tblcatagory d, EmployeeGroup x where b.departmentcode=c.departmentcode and b.cat=d.cat and d.LateVerification='Y' and b.paycode=a.paycode and b.EmployeeGroupId = x.GroupId and b.ACTIVE ='Y' ORDER BY A.PAYCODE "                   
                    sSql = "Select a.*,b.companycode,b.departmentcode,x.Id from " &
                            "tblemployeeshiftmaster a,tblemployee b, EmployeeGroup x " &
                            "where b.paycode=a.paycode and a.PAYCODE IN ('" & String.Join("', '", Common.EmpArr) & "')" &
                            "and b.EmployeeGroupId = x.GroupId and b.ACTIVE ='Y' ORDER BY A.PAYCODE "
                Else
                    If ComboBoxEdit1.EditValue = "Paycode" Then
                        Dim paycode() As String = PopupContainerEdit1.EditValue.ToString.Split(",")
                        paycode = paycode.Select(Function(s) s.Replace(" ", "")).ToArray()
                        sSql = "Select a.*,b.companycode,b.departmentcode,x.Id from " &
                           "tblemployeeshiftmaster a,tblemployee b, EmployeeGroup x " &
                           "where b.paycode=a.paycode and a.PAYCODE IN ('" & String.Join("', '", paycode) & "')" &
                           "and b.EmployeeGroupId = x.GroupId and b.ACTIVE ='Y' ORDER BY A.PAYCODE "
                    ElseIf ComboBoxEdit1.EditValue = "Company" Then
                        Dim com() As String = PopupContainerEdit1.EditValue.ToString.Split(",")
                        'com = com.Select(Function(s) s.Replace(" ", "")).ToArray()
                        Dim ls As New List(Of String)()
                        For i As Integer = 0 To com.Length - 1
                            ls.Add(com(i).Trim)
                        Next
                        sSql = "Select a.*,b.companycode,b.departmentcode,x.Id from " &
                          "tblemployeeshiftmaster a,tblemployee b, EmployeeGroup x " &
                          "where b.paycode=a.paycode and b.companycode IN ('" & String.Join("', '", ls.ToArray()) & "')" &
                          "and b.EmployeeGroupId = x.GroupId and b.ACTIVE ='Y' ORDER BY A.PAYCODE "
                    ElseIf ComboBoxEdit1.EditValue = "Department" Then
                        Dim com() As String = PopupContainerEdit1.EditValue.ToString.Split(",")
                        'com = com.Select(Function(s) s.Replace(" ", "")).ToArray()
                        Dim ls As New List(Of String)()
                        For i As Integer = 0 To com.Length - 1
                            ls.Add(com(i).Trim)
                        Next
                        sSql = "Select a.*,b.companycode,b.departmentcode,x.Id from " &
                          "tblemployeeshiftmaster a,tblemployee b, EmployeeGroup x " &
                          "where b.paycode=a.paycode and b.DEPARTMENTCODE IN ('" & String.Join("', '", ls.ToArray()) & "')" &
                          "and b.EmployeeGroupId = x.GroupId and b.ACTIVE ='Y' ORDER BY A.PAYCODE "
                    ElseIf ComboBoxEdit1.EditValue = "Location" Then
                        Dim com() As String = PopupContainerEdit1.EditValue.ToString.Split(",")
                        Dim ls As New List(Of String)()
                        For i As Integer = 0 To com.Length - 1
                            ls.Add(com(i).Trim)
                        Next
                        sSql = "Select a.*,b.companycode,b.departmentcode,x.Id from " &
                          "tblemployeeshiftmaster a,tblemployee b, EmployeeGroup x " &
                          "where b.paycode=a.paycode and b.BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "')" &
                          "and b.EmployeeGroupId = x.GroupId and b.ACTIVE ='Y' ORDER BY A.PAYCODE "
                    End If
                End If
                Rs = New DataSet
                If servername = "Access" Then
                    adapAc = New OleDbDataAdapter(sSql, con1)
                    adapAc.Fill(Rs)
                Else
                    adapS = New SqlDataAdapter(sSql, con)
                    adapS.Fill(Rs)
                End If

                'Dim str1 As String = ""
                Dim mindate As DateTime
                Dim maxdate As DateTime
                'Try
                'cmdVerification.Enabled = False
                Dim MsgBox As String = ""
                Dim dt As DateTime = DateTime.MinValue
                mmin_date = Convert.ToDateTime("01/" & DateEditVerification.DateTime.ToString("MM/yyyy")) '("01/" + TxtFromDate.Text)
                Try
                    mindate = mmin_date 'DateTime.ParseExact(mmin_date.ToString, "dd/MM/yyyy", CultureInfo.InvariantCulture)
                    maxdate = mindate.AddMonths(1).AddDays(-1)
                Catch ex As System.Exception
                    MsgBox = "Invalid Date"
                    Return
                End Try
                If (Rs.Tables(0).Rows.Count <= 0) Then
                    'cmdVerification.Enabled = True
                    MsgBox = "Employee code does not exist"
                    Return
                End If

                'Dim i As Integer = 0
                'Do While (i < Rs.Tables(0).Rows.Count)
                If Common.servername = "Access" Then
                    If Common.con1.State <> ConnectionState.Open Then
                        Common.con1.Open()
                    End If
                Else
                    If Common.con.State <> ConnectionState.Open Then
                        Common.con.Open()
                    End If
                End If
                For i As Integer = 0 To Rs.Tables(0).Rows.Count - 1

                    mCode = Rs.Tables(0).Rows(i)("Paycode").ToString.Trim
                    XtraMasterTest.LabelControlStatus.Text = "Verifying for Paycode " & mCode
                    Application.DoEvents()
                    iFlag = True
                    sSql = " Select Paycode,DateOffice,AbsentValue,PresentValue,Wo_Value,Holiday_Value,Status,LeaveValue,in1, ShiftAttended from tblTimeRegister Where paycode='" _
                                + Rs.Tables(0).Rows(i)("Paycode").ToString + "' and tblTimeRegister.DateOffice " + " Between '" _
                                + mindate.ToString("yyyy-MM-dd") + "' AND '" _
                                + maxdate.ToString("yyyy-MM-dd") + "' Order By DateOffice"
                    rsTime = New DataSet
                    'rsTime = cn.FillDataSet(sSql)
                    If servername = "Access" Then
                        sSql = " Select Paycode,DateOffice,AbsentValue,PresentValue,Wo_Value,Holiday_Value,Status,LeaveValue,in1, ShiftAttended from tblTimeRegister Where paycode='" _
                               + Rs.Tables(0).Rows(i)("Paycode").ToString + "' and FORMAT(tblTimeRegister.DateOffice,'yyyy-MM-dd') " + " Between '" _
                               + mindate.ToString("yyyy-MM-dd") + "' AND '" _
                               + maxdate.ToString("yyyy-MM-dd") + "' Order By DateOffice"
                        adapAc = New OleDbDataAdapter(sSql, con1)
                        adapAc.Fill(rsTime)
                    Else
                        adapS = New SqlDataAdapter(sSql, con)
                        adapS.Fill(rsTime)
                    End If

                    If (Common.EmpGrpArr(Rs.Tables(0).Rows(i).Item("Id")).g_AbsentOnWO = "Y") Then
                        Dim j As Integer = 0
                        Do While (j < rsTime.Tables(0).Rows.Count)
                            If (rsTime.Tables(0).Rows(j)("Status").ToString.Trim = "A1") Then
                                If iFlag Then
                                    iCtr = 0
                                    While (rsTime.Tables(0).Rows(j)("Status").ToString.Trim(Microsoft.VisualBasic.ChrW(32)) = "A1")
                                        mArr(iCtr) = j
                                        iCtr = (iCtr + 1)
                                        j = (j + 1)
                                        If (rsTime.Tables(0).Rows.Count = j) Then
                                            Exit While
                                        End If
                                    End While
                                    While (iCtr >= 1)
                                        j = mArr((iCtr - 1))
                                        If (rsTime.Tables(0).Rows(j)("ShiftAttended").ToString = "OFF") Then
                                            Try
                                                dt = Convert.ToDateTime(rsTime.Tables(0).Rows(j)("dateoffice").ToString.Trim.Substring(0, 10))
                                                Str = "update tbltimeregister set Status='WO',ABSENTVALUE = 0,WO_VALUE = 1,HOLIDAY_VALUE = 0 where paycode='" &
                                                 mCode.Trim & "' and dateoffice='" &
                                                             dt.ToString("yyyy-MM-dd") & "'"
                                                'cn.execute_NonQuery(Str)
                                                If Common.servername = "Access" Then
                                                    Str = "update tbltimeregister set Status='WO',ABSENTVALUE = 0,WO_VALUE = 1,HOLIDAY_VALUE = 0 where paycode='" &
                                                mCode.Trim & "' and FORMAT(dateoffice,'yyyy-MM-dd')='" &
                                                            dt.ToString("yyyy-MM-dd") & "'"
                                                    cmd1 = New OleDbCommand(Str, Common.con1)
                                                    cmd1.ExecuteNonQuery()
                                                Else
                                                    cmd = New SqlCommand(Str, Common.con)
                                                    cmd.ExecuteNonQuery()
                                                End If
                                            Catch ex As System.Exception

                                            End Try

                                        Else
                                            Try
                                                dt = Convert.ToDateTime(rsTime.Tables(0).Rows(j)("dateoffice").ToString.Trim.Substring(0, 10))
                                                Str = "update tbltimeregister set Status='HLD',ABSENTVALUE = 0,WO_VALUE = 0,HOLIDAY_VALUE = 1 where paycode='" &
                                                    mCode.Trim & "' and dateoffice='" &
                                                             dt.ToString("yyyy-MM-dd") & "'"
                                                'cn.execute_NonQuery(Str)
                                                If Common.servername = "Access" Then
                                                    Str = "update tbltimeregister set Status='HLD',ABSENTVALUE = 0,WO_VALUE = 0,HOLIDAY_VALUE = 1 where paycode='" &
                                                    mCode.Trim & "' and FORMAT(dateoffice,'yyyy-MM-dd')='" &
                                                             dt.ToString("yyyy-MM-dd") & "'"
                                                    cmd1 = New OleDbCommand(Str, Common.con1)
                                                    cmd1.ExecuteNonQuery()
                                                Else
                                                    cmd = New SqlCommand(Str, Common.con)
                                                    cmd.ExecuteNonQuery()
                                                End If
                                            Catch ex As System.Exception
                                            End Try
                                        End If
                                        iCtr = (iCtr - 1)
                                    End While
                                Else
                                    While (rsTime.Tables(0).Rows(j)("Status").ToString.Trim(Microsoft.VisualBasic.ChrW(32)) = "A1")
                                        If (rsTime.Tables(0).Rows(j)("ShiftAttended").ToString = "OFF") Then
                                            Try
                                                dt = Convert.ToDateTime(rsTime.Tables(0).Rows(j)("dateoffice").ToString.Trim.Substring(0, 10))
                                                Str = "update tbltimeregister set Status='WO',ABSENTVALUE = 0,WO_VALUE = 1,HOLIDAY_VALUE = 0 where paycode='" &
                                                    mCode.Trim & "' and dateoffice='" &
                                                            dt.ToString("yyyy-MM-dd") & "'"
                                                'cn.execute_NonQuery(Str)
                                                If Common.servername = "Access" Then
                                                    cmd1 = New OleDbCommand(Str, Common.con1)
                                                    cmd1.ExecuteNonQuery()
                                                Else
                                                    cmd = New SqlCommand(Str, Common.con)
                                                    cmd.ExecuteNonQuery()
                                                End If
                                            Catch ex As System.Exception
                                            End Try
                                        Else
                                            Try
                                                dt = Convert.ToDateTime(rsTime.Tables(0).Rows(j)("dateoffice").ToString.Trim.Substring(0, 10))
                                                Str = "update tbltimeregister set Status='HLD',ABSENTVALUE = 0,WO_VALUE = 0,HOLIDAY_VALUE = 1 where paycode='" &
                                                       mCode.Trim & "' and dateoffice='" &
                                                       dt.ToString("yyyy-MM-dd") & "'"
                                                'cn.execute_NonQuery(Str)
                                                If Common.servername = "Access" Then
                                                    cmd1 = New OleDbCommand(Str, Common.con1)
                                                    cmd1.ExecuteNonQuery()
                                                Else
                                                    cmd = New SqlCommand(Str, Common.con)
                                                    cmd.ExecuteNonQuery()
                                                End If
                                            Catch ex As System.Exception
                                            End Try
                                        End If
                                        j = (j + 1)
                                        If (rsTime.Tables(0).Rows.Count = j) Then
                                            Exit While
                                        End If
                                    End While
                                End If
                            End If
                            If (rsTime.Tables(0).Rows.Count = j) Then
                                Exit Do
                            End If
                            j = (j + 1)
                        Loop
                    Else
                        'MsgBox = "Please Select Auto Absent Option In Admin/TimeOffice Setup"
                        'Return
                    End If


                    ''AWA to AAA
                    'If (Common.EmpGrpArr(Rs.Tables(0).Rows(i).Item("Id")).g_ISAWA = "Y") Then
                    '    'Dim k As Integer = 0                           
                    '    iCtr = 0
                    '    iFlag = True
                    '    Dim p As Integer = 0
                    '    Do While (p < rsTime.Tables(0).Rows.Count)
                    '        If (Convert.ToInt32(rsTime.Tables(0).Rows(p)("AbsentValue")) = 1) Then
                    '            iFlag = True
                    '            p = (p + 1)
                    '        ElseIf (((Convert.ToInt32(rsTime.Tables(0).Rows(p)("Holiday_Value")) = 1) _
                    '                    OrElse (Convert.ToInt32(rsTime.Tables(0).Rows(p)("WO_Value")) = 1)) _
                    '                    AndAlso (String.IsNullOrEmpty(rsTime.Tables(0).Rows(p)("in1").ToString) AndAlso iFlag)) Then
                    '            p = (p - 1)
                    '            If (p <= 0) Then
                    '                p = (p + 1)
                    '            End If

                    '            If (Convert.ToInt32(rsTime.Tables(0).Rows(p)("AbsentValue")) = 1) Then
                    '                iCtr = (iCtr + 1)
                    '            Else
                    '                iCtr = 0
                    '            End If
                    '            p = (p + 1)
                    '            iFlag = True
                    '            If (iCtr > 0) Then
                    '                While ((Convert.ToInt32(rsTime.Tables(0).Rows(p)("Holiday_Value")) = 1) _
                    '                            OrElse (Convert.ToInt32(rsTime.Tables(0).Rows(p)("WO_Value")) = 1))
                    '                    mArr(iCtr) = p
                    '                    iCtr = (iCtr + 1)
                    '                    p = (p + 1)
                    '                    If (rsTime.Tables(0).Rows.Count = p) Then
                    '                        Exit While
                    '                    End If
                    '                End While
                    '                If (rsTime.Tables(0).Rows.Count = p) Then
                    '                    Exit Do
                    '                End If
                    '                If (Convert.ToInt32(rsTime.Tables(0).Rows(p)("AbsentValue")) = 1) Then
                    '                    iD = p
                    '                    While (iCtr >= 2)
                    '                        p = mArr((iCtr - 1))
                    '                        dt = Convert.ToDateTime(rsTime.Tables(0).Rows(p)("dateoffice").ToString.Trim.Substring(0, 10))
                    '                        Str = "update tbltimeregister set Status='A1',ABSENTVALUE = 1,WO_VALUE = 0,HOLIDAY_VALUE = 0 where paycode='" & _
                    '                         mCode.Trim & "' and dateoffice='" & _
                    '                                     dt.ToString("yyyy-MM-dd") & "'"
                    '                        'cn.execute_NonQuery(Str)
                    '                        If Common.servername = "Access" Then
                    '                            Common.con1.Open()
                    '                            cmd1 = New OleDbCommand(Str, Common.con1)
                    '                            cmd1.ExecuteNonQuery()
                    '                            Common.con1.Close()
                    '                        Else
                    '                            Common.con.Open()
                    '                            cmd = New SqlCommand(Str, Common.con)
                    '                            cmd.ExecuteNonQuery()
                    '                            Common.con.Close()
                    '                        End If
                    '                        iCtr = (iCtr - 1)
                    '                    End While
                    '                    iCtr = 0
                    '                Else
                    '                    iCtr = 0
                    '                End If
                    '            Else
                    '                p = (p + 1)
                    '            End If
                    '        Else
                    '            p = (p + 1)
                    '        End If
                    '        If (rsTime.Tables(0).Rows.Count = p) Then
                    '            Exit Do
                    '        End If
                    '        p = (p + 1)
                    '    Loop
                    '    'rsTime.Clear()
                    '    'k = (k + 1)
                    'End If

                    'AWA to AAA nava
                    If (Common.EmpGrpArr(Rs.Tables(0).Rows(i).Item("Id")).g_ISAWA = "Y") Then
                        For p As Integer = 0 To rsTime.Tables(0).Rows.Count - 1
                            If (p > 0 And p < rsTime.Tables(0).Rows.Count - 1) Then
                                Dim HOLIDAY_VALUE As Double
                                If rsTime.Tables(0).Rows(p)("HOLIDAY_VALUE").ToString.Trim = "" Then
                                    HOLIDAY_VALUE = 0
                                Else
                                    HOLIDAY_VALUE = Convert.ToDouble(rsTime.Tables(0).Rows(p)("HOLIDAY_VALUE"))
                                End If
                                If ((Convert.ToDouble(rsTime.Tables(0).Rows(p)("Wo_Value")) = 1.0 Or HOLIDAY_VALUE = 1.0) And Convert.ToDouble(rsTime.Tables(0).Rows(p - 1)("AbsentValue")) = 1.0 And Convert.ToDouble(rsTime.Tables(0).Rows(p + 1)("AbsentValue")) = 1.0) Then
                                    dt = Convert.ToDateTime(rsTime.Tables(0).Rows(p)("dateoffice").ToString())
                                    Str = "update tbltimeregister set Status='A1',ABSENTVALUE = 1,WO_VALUE = 0,HOLIDAY_VALUE = 0 where paycode='" & mCode.ToString() & "' and dateoffice='" & dt.ToString("yyyy-MM-dd") & "'"
                                    If Common.servername = "Access" Then
                                        Str = "update tbltimeregister set Status='A1',ABSENTVALUE = 1,WO_VALUE = 0,HOLIDAY_VALUE = 0 where paycode='" & mCode.ToString() & "' and FORMAT(dateoffice,'yyyy-MM-dd')='" & dt.ToString("yyyy-MM-dd") & "'"
                                        cmd1 = New OleDbCommand(Str, Common.con1)
                                        cmd1.ExecuteNonQuery()
                                    Else
                                        cmd = New SqlCommand(Str, Common.con)
                                        cmd.ExecuteNonQuery()
                                    End If
                                End If
                            End If
                        Next
                    End If
                    'AWA to AAA nava end 

                    If (Common.EmpGrpArr(Rs.Tables(0).Rows(i).Item("Id")).g_isprewo = "Y") Then
                        iPrevalue = 0
                        Dim j As Integer = 0
                        Do While (j < rsTime.Tables(0).Rows.Count)
                            If j >= Convert.ToInt32(Common.EmpGrpArr(Rs.Tables(0).Rows(i).Item("Id")).g_PREWO) - 1 Then
                                If (Convert.ToInt32(rsTime.Tables(0).Rows(j)("PresentValue")) > 0) Then
                                    iPrevalue = (iPrevalue + 1)
                                    iFlag = True
                                    'j = (j + 1)
                                    'ElseIf (((Convert.ToInt32(rsTime.Tables(0).Rows(j)("Holiday_Value")) = 1) _
                                    '            OrElse (Convert.ToInt32(rsTime.Tables(0).Rows(j)("WO_Value")) = 1)) _
                                    '            AndAlso (String.IsNullOrEmpty(rsTime.Tables(0).Rows(j)("in1").ToString) AndAlso iFlag)) Then
                                ElseIf (((Convert.ToInt32(rsTime.Tables(0).Rows(j)("WO_Value")) = 1)) _
                                       AndAlso (String.IsNullOrEmpty(rsTime.Tables(0).Rows(j)("in1").ToString) AndAlso iFlag)) Then
                                    iFlag = True
                                    iCtr = 0
                                    'While ((Convert.ToInt32(rsTime.Tables(0).Rows(j)("Holiday_Value")) = 1) _
                                    '            OrElse (Convert.ToInt32(rsTime.Tables(0).Rows(j)("WO_Value")) = 1))
                                    While ((Convert.ToInt32(rsTime.Tables(0).Rows(j)("WO_Value")) = 1))
                                        mArr(iCtr) = j
                                        iCtr = (iCtr + 1)
                                        j = (j + 1)
                                        If (rsTime.Tables(0).Rows.Count = j) Then
                                            Exit While
                                        End If
                                    End While
                                    If (rsTime.Tables(0).Rows.Count = j) Then
                                        Exit Do
                                    End If
                                    If (iPrevalue < Convert.ToInt32(Common.EmpGrpArr(Rs.Tables(0).Rows(i).Item("Id")).g_PREWO)) Then
                                        iD = j
                                        While (iCtr >= 1)
                                            j = mArr((iCtr - 1))
                                            dt = Convert.ToDateTime(rsTime.Tables(0).Rows(j)("dateoffice").ToString.Trim.Substring(0, 10))
                                            Str = "update tbltimeregister set Status='A1',ABSENTVALUE = 1,WO_VALUE = 0,HOLIDAY_VALUE = 0 where paycode='" &
                                                    mCode.Trim & "' and dateoffice='" &
                                                        dt.ToString("yyyy-MM-dd") & "'"
                                            'cn.execute_NonQuery(Str)
                                            If Common.servername = "Access" Then
                                                Str = "update tbltimeregister set Status='A1',ABSENTVALUE = 1,WO_VALUE = 0,HOLIDAY_VALUE = 0 where paycode='" &
                                                    mCode.Trim & "' and FORMAT(dateoffice,'yyyy-MM-dd')='" &
                                                        dt.ToString("yyyy-MM-dd") & "'"
                                                cmd1 = New OleDbCommand(Str, Common.con1)
                                                cmd1.ExecuteNonQuery()
                                            Else
                                                cmd = New SqlCommand(Str, Common.con)
                                                cmd.ExecuteNonQuery()
                                            End If
                                            iCtr = (iCtr - 1)
                                        End While
                                    End If
                                    iPrevalue = 0
                                Else
                                End If
                            End If
                            If (rsTime.Tables(0).Rows.Count = j) Then
                                Exit Do
                            End If
                            j = (j + 1)
                        Loop
                        rsTime.Clear()
                    End If

                    'For WA As AA
                    If (Common.EmpGrpArr(Rs.Tables(0).Rows(i).Item("Id")).g_ISWA = "Y") Then
                        For p As Integer = 0 To rsTime.Tables(0).Rows.Count - 1
                            If (p > 0) Then
                                Dim HOLIDAY_VALUE As Double
                                If rsTime.Tables(0).Rows(p)("HOLIDAY_VALUE").ToString.Trim = "" Then
                                    HOLIDAY_VALUE = 0
                                Else
                                    HOLIDAY_VALUE = Convert.ToDouble(rsTime.Tables(0).Rows(p)("HOLIDAY_VALUE"))
                                End If
                                If ((Convert.ToDouble(rsTime.Tables(0).Rows(p)("Wo_Value")) = 1.0 Or HOLIDAY_VALUE = 1.0) And Convert.ToDouble(rsTime.Tables(0).Rows(p - 1)("AbsentValue")) = 1.0) Then
                                    dt = Convert.ToDateTime(rsTime.Tables(0).Rows(p)("dateoffice").ToString())
                                    Str = "update tbltimeregister set Status='A1',ABSENTVALUE = 1,WO_VALUE = 0,HOLIDAY_VALUE = 0 where paycode='" & mCode.ToString() & "' and dateoffice='" & dt.ToString("yyyy-MM-dd") & "'"
                                    If Common.servername = "Access" Then
                                        cmd1 = New OleDbCommand(Str, Common.con1)
                                        cmd1.ExecuteNonQuery()
                                    Else
                                        cmd = New SqlCommand(Str, Common.con)
                                        cmd.ExecuteNonQuery()
                                    End If
                                End If
                            End If
                        Next
                    End If
                    'For PWA As AAA End    

                    'For AW As AA
                    If (Common.EmpGrpArr(Rs.Tables(0).Rows(i).Item("Id")).g_ISAW = "Y") Then
                        For p As Integer = 0 To rsTime.Tables(0).Rows.Count - 1
                            If (p < rsTime.Tables(0).Rows.Count - 1) Then
                                Dim HOLIDAY_VALUE As Double
                                If rsTime.Tables(0).Rows(p)("HOLIDAY_VALUE").ToString.Trim = "" Then
                                    HOLIDAY_VALUE = 0
                                Else
                                    HOLIDAY_VALUE = Convert.ToDouble(rsTime.Tables(0).Rows(p)("HOLIDAY_VALUE"))
                                End If
                                If ((Convert.ToDouble(rsTime.Tables(0).Rows(p)("Wo_Value")) = 1.0 Or HOLIDAY_VALUE = 1.0) And Convert.ToDouble(rsTime.Tables(0).Rows(p + 1)("AbsentValue")) = 1.0) Then
                                    dt = Convert.ToDateTime(rsTime.Tables(0).Rows(p)("dateoffice").ToString())
                                    Str = "update tbltimeregister set Status='A1',ABSENTVALUE = 1,WO_VALUE = 0,HOLIDAY_VALUE = 0 where paycode='" & mCode.ToString() & "' and dateoffice='" & dt.ToString("yyyy-MM-dd") + "'"
                                    If Common.servername = "Access" Then
                                        cmd1 = New OleDbCommand(Str, Common.con1)
                                        cmd1.ExecuteNonQuery()
                                    Else
                                        cmd = New SqlCommand(Str, Common.con)
                                        cmd.ExecuteNonQuery()
                                    End If
                                End If
                            End If
                        Next
                    End If
                Next
                If Common.servername = "Access" Then
                    If Common.con1.State <> ConnectionState.Closed Then
                        Common.con1.Close()
                    End If
                Else
                    If Common.con.State <> ConnectionState.Closed Then
                        Common.con.Close()
                    End If
                End If
                Rs.Clear()
                'Catch Ex As Exception
                '    XtraMasterTest.LabelControlStatus.Text = ""
                '    Application.DoEvents()
                '    XtraMessageBox.Show(ulf, "<size=10>" & Ex.Message & "</size>", "iAS")
                '    Return
                'End Try
            End If

            If CheckEditLate.Checked = True Then
                'whereclausesalreg = gcompclauseD & " and " & gdeptclauseD
                sSql = "Select a.*,b.companycode,b.departmentcode,d.*,x.Id from tblemployeeshiftmaster a,tblemployee b,tbldepartment c,tblcatagory d, EmployeeGroup x where b.departmentcode=c.departmentcode and b.cat=d.cat and d.LateVerification='Y' and b.paycode=a.paycode and b.EmployeeGroupId = x.GroupId ORDER BY A.PAYCODE "

                If CheckAll.Checked = True Then
                    'sSql = "Select a.*,b.companycode,b.departmentcode,x.Id from " &
                    '        "tblemployeeshiftmaster a,tblemployee b, EmployeeGroup x " &
                    '        "where b.paycode=a.paycode and a.PAYCODE IN ('" & String.Join("', '", Common.EmpArr) & "')" &
                    '        "and b.EmployeeGroupId = x.GroupId and b.ACTIVE ='Y' ORDER BY A.PAYCODE "
                    sSql = "Select a.*,b.companycode,b.departmentcode,d.*,x.Id from tblemployeeshiftmaster a,tblemployee b,tbldepartment c,tblcatagory d, EmployeeGroup x where b.departmentcode=c.departmentcode and b.cat=d.cat and d.LateVerification='Y' and b.paycode=a.paycode and a.PAYCODE IN ('" & String.Join("', '", Common.EmpArr) & "') and b.EmployeeGroupId = x.GroupId and b.ACTIVE ='Y' ORDER BY A.PAYCODE"
                Else
                    If ComboBoxEdit1.EditValue = "Paycode" Then
                        Dim paycode() As String = PopupContainerEdit1.EditValue.ToString.Split(",")
                        paycode = paycode.Select(Function(s) s.Replace(" ", "")).ToArray()
                        'sSql = "Select a.*,b.companycode,b.departmentcode,x.Id from " &
                        '   "tblemployeeshiftmaster a,tblemployee b, EmployeeGroup x " &
                        '   "where b.paycode=a.paycode and a.PAYCODE IN ('" & String.Join("', '", paycode) & "')" &
                        '   "and b.EmployeeGroupId = x.GroupId and b.ACTIVE ='Y' ORDER BY A.PAYCODE "

                        sSql = "Select a.*,b.companycode,b.departmentcode,d.*,x.Id from tblemployeeshiftmaster a,tblemployee b,tbldepartment c,tblcatagory d, EmployeeGroup x where b.departmentcode=c.departmentcode and b.cat=d.cat and d.LateVerification='Y' and b.paycode=a.paycode and a.PAYCODE IN ('" & String.Join("', '", paycode) & "') and b.EmployeeGroupId = x.GroupId and b.ACTIVE ='Y' ORDER BY A.PAYCODE"
                    ElseIf ComboBoxEdit1.EditValue = "Company" Then
                        Dim com() As String = PopupContainerEdit1.EditValue.ToString.Split(",")
                        'com = com.Select(Function(s) s.Replace(" ", "")).ToArray()
                        Dim ls As New List(Of String)()
                        For i As Integer = 0 To com.Length - 1
                            ls.Add(com(i).Trim)
                        Next
                        'sSql = "Select a.*,b.companycode,b.departmentcode,x.Id from " &
                        '  "tblemployeeshiftmaster a,tblemployee b, EmployeeGroup x " &
                        '  "where b.paycode=a.paycode and b.companycode IN ('" & String.Join("', '", ls.ToArray()) & "')" &
                        '  "and b.EmployeeGroupId = x.GroupId and b.ACTIVE ='Y' ORDER BY A.PAYCODE "
                        sSql = "Select a.*,b.companycode,b.departmentcode,d.*,x.Id from tblemployeeshiftmaster a,tblemployee b,tbldepartment c,tblcatagory d, EmployeeGroup x where b.departmentcode=c.departmentcode and b.cat=d.cat and d.LateVerification='Y' and b.paycode=a.paycode and b.companycode IN ('" & String.Join("', '", ls.ToArray()) & "') and b.EmployeeGroupId = x.GroupId and b.ACTIVE ='Y' ORDER BY A.PAYCODE"
                    ElseIf ComboBoxEdit1.EditValue = "Department" Then
                        Dim com() As String = PopupContainerEdit1.EditValue.ToString.Split(",")
                        'com = com.Select(Function(s) s.Replace(" ", "")).ToArray()
                        Dim ls As New List(Of String)()
                        For i As Integer = 0 To com.Length - 1
                            ls.Add(com(i).Trim)
                        Next
                        'sSql = "Select a.*,b.companycode,b.departmentcode,x.Id from " &
                        '  "tblemployeeshiftmaster a,tblemployee b, EmployeeGroup x " &
                        '  "where b.paycode=a.paycode and b.DEPARTMENTCODE IN ('" & String.Join("', '", ls.ToArray()) & "')" &
                        '  "and b.EmployeeGroupId = x.GroupId and b.ACTIVE ='Y' ORDER BY A.PAYCODE "
                        sSql = "Select a.*,b.companycode,b.departmentcode,d.*,x.Id from tblemployeeshiftmaster a,tblemployee b,tbldepartment c,tblcatagory d, EmployeeGroup x where b.departmentcode=c.departmentcode and b.cat=d.cat and d.LateVerification='Y' and b.paycode=a.paycode and b.DEPARTMENTCODE IN ('" & String.Join("', '", ls.ToArray()) & "') and b.EmployeeGroupId = x.GroupId and b.ACTIVE ='Y' ORDER BY A.PAYCODE"

                    ElseIf ComboBoxEdit1.EditValue = "Location" Then
                        Dim com() As String = PopupContainerEdit1.EditValue.ToString.Split(",")
                        Dim ls As New List(Of String)()
                        For i As Integer = 0 To com.Length - 1
                            ls.Add(com(i).Trim)
                        Next
                        'sSql = "Select a.*,b.companycode,b.departmentcode,x.Id from " &
                        '  "tblemployeeshiftmaster a,tblemployee b, EmployeeGroup x " &
                        '  "where b.paycode=a.paycode and b.BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "')" &
                        '  "and b.EmployeeGroupId = x.GroupId and b.ACTIVE ='Y' ORDER BY A.PAYCODE "
                        sSql = "Select a.*,b.companycode,b.departmentcode,d.*,x.Id from tblemployeeshiftmaster a,tblemployee b,tbldepartment c,tblcatagory d, EmployeeGroup x where b.departmentcode=c.departmentcode and b.cat=d.cat and d.LateVerification='Y' and b.paycode=a.paycode and b.BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "') and b.EmployeeGroupId = x.GroupId and b.ACTIVE ='Y' ORDER BY A.PAYCODE"
                    End If
                End If

                rsEmp = New DataSet
                If servername = "Access" Then
                    adapAc = New OleDbDataAdapter(sSql, con1)
                    adapAc.Fill(rsEmp)
                Else
                    adapS = New SqlDataAdapter(sSql, con)
                    adapS.Fill(rsEmp)
                End If
                'rsEmp = Cn.Execute(sSql)

                mmin_date = Convert.ToDateTime("01/" & DateEditVerification.DateTime.ToString("MM/yyyy")) 'Format("01/" & Format(dtpProcDate.Value, "Mmm/Yyyy"), "dd/Mmm/Yyyy")
                mmax_date = DateAdd("m", 1, mmin_date)
                mmax_date = DateAdd("d", -1, mmax_date)
                mmax_date = Format(mmax_date, "dd/MM/yyyy")
                Dim EmpGpId As Integer
                For i As Integer = 0 To rsEmp.Tables(0).Rows.Count - 1 ' Do While Not rsEmp.EOF
                    XtraMasterTest.LabelControlCount.Text = "Processing " & i + 1 & " of " & rsEmp.Tables(0).Rows.Count
                    Application.DoEvents()

                    EmpGpId = rsEmp.Tables(0).Rows(i).Item("Id")
                    mCode = rsEmp.Tables(0).Rows(i).Item("Paycode").ToString.Trim
                    'If mCode <> "10" Then
                    '    Continue For
                    'End If
                    XtraMasterTest.LabelControlStatus.Text = "Verifying for Paycode " & mCode
                    Application.DoEvents()
                    If rsEmp.Tables(0).Rows(i).Item("DeductFrom").ToString.Trim = "A" Then
                        'If rsEmp.Tables(0).Rows(i).Item("LateDays").ToString.Trim > 0 And rsEmp.Tables(0).Rows(i).Item("DeductDay").ToString.Trim > 0 Then
                        If rsEmp.Tables(0).Rows(i).Item("LateDays").ToString.Trim > -1 And rsEmp.Tables(0).Rows(i).Item("DeductDay").ToString.Trim > 0 Then  '-1 for testing 
                            mCode = rsEmp.Tables(0).Rows(i).Item("Paycode").ToString.Trim
                            If Common.servername = "Access" Then
                                sSql = " Select * from tblTimeRegister Where (REASON='LATE_D' or REASON='LATE_Rem') AND paycode='" & rsEmp.Tables(0).Rows(i).Item("Paycode").ToString.Trim & "' and FORMAT(tblTimeRegister.DateOffice,'yyyy-MM-dd') Between '" & mmin_date.ToString("yyyy-MM-dd") & "' AND '" & mmax_date.ToString("yyyy-MM-dd") & "' Order By DateOffice"
                            Else
                                sSql = " Select * from tblTimeRegister Where (REASON='LATE_D' or REASON='LATE_Rem') AND paycode='" & rsEmp.Tables(0).Rows(i).Item("Paycode").ToString.Trim & "' and tblTimeRegister.DateOffice Between '" & mmin_date.ToString("yyyy-MM-dd") & "' AND '" & mmax_date.ToString("yyyy-MM-dd") & "' Order By DateOffice"
                            End If
                            rsTime = New DataSet 'ADODB.Recordset
                            If servername = "Access" Then
                                adapAc = New OleDbDataAdapter(sSql, con1)
                                adapAc.Fill(rsTime)
                            Else
                                adapS = New SqlDataAdapter(sSql, con)
                                adapS.Fill(rsTime)
                            End If

                            'rsTime.Open(sSql, Cn, adOpenKeyset, adLockBatchOptimistic)
                            If rsTime.Tables(0).Rows.Count > 0 Then
                                'rsTime.MoveFirst()
                                Dim status As String
                                For k As Integer = 0 To rsTime.Tables(0).Rows.Count - 1 '  Do Until rsTime.EOF
                                    Dim leaveamount As Double = 0
                                    Try
                                        leaveamount = Convert.ToDouble(rsTime.Tables(0).Rows(k).Item("leaveamount").ToString.Trim)
                                    Catch ex As Exception
                                    End Try
                                    If leaveamount > 0 Then
                                        If rsTime.Tables(0).Rows(k).Item("FIRSTHALFLEAVECODE").ToString.Trim <> "" Or rsTime.Tables(0).Rows(k).Item("SECONDHALFLEAVECODE").ToString.Trim <> "" Then
                                            If rsTime.Tables(0).Rows(k).Item("FIRSTHALFLEAVECODE").ToString.Trim <> "" Then
                                                sSql = "select LeaveField from tblleavemaster where leavecode='" & rsTime.Tables(0).Rows(k).Item("FIRSTHALFLEAVECODE").ToString.Trim & "'"
                                                'rsLeaveMaster.Find(sSql)
                                                rsLeaveMaster = New DataSet
                                                If servername = "Access" Then
                                                    adapAc = New OleDbDataAdapter(sSql, con1)
                                                    adapAc.Fill(rsLeaveMaster)
                                                Else
                                                    adapS = New SqlDataAdapter(sSql, con)
                                                    adapS.Fill(rsLeaveMaster)
                                                End If
                                                sSql = "Update tblLeaveLedger Set " & rsLeaveMaster.Tables(0).Rows(0).Item("LeaveField").ToString.Trim & "=" &
                                                rsLeaveMaster.Tables(0).Rows(0).Item("LeaveField").ToString.Trim & " - " & rsTime.Tables(0).Rows(k).Item("Leaveamount1").ToString.Trim &
                                                " Where Paycode='" & rsTime.Tables(0).Rows(k).Item("Paycode").ToString.Trim & "' AND LYEAR=" & Convert.ToDateTime(rsTime.Tables(0).Rows(k).Item("DATEOFFICE").ToString.Trim).Year
                                                If Common.servername = "Access" Then
                                                    Common.con1.Open()
                                                    cmd1 = New OleDbCommand(sSql, Common.con1)
                                                    cmd1.ExecuteNonQuery()
                                                    Common.con1.Close()
                                                Else
                                                    Common.con.Open()
                                                    cmd = New SqlCommand(sSql, Common.con)
                                                    cmd.ExecuteNonQuery()
                                                    Common.con.Close()
                                                End If
                                                'Cn.Execute(sSql)
                                            End If
                                            If rsTime.Tables(0).Rows(k).Item("SECONDHALFLEAVECODE").ToString.Trim <> "" Then
                                                sSql = "leavecode='" & rsTime.Tables(0).Rows(k).Item("SECONDHALFLEAVECODE").ToString.Trim & "'"
                                                'rsLeaveMaster.Find(sSql)
                                                sSql = "Update tblLeaveLedger Set " & rsLeaveMaster.Tables(0).Rows(0).Item("LeaveField").ToString.Trim & "=" &
                                                rsLeaveMaster.Tables(0).Rows(0).Item("LeaveField").ToString.Trim & " - " & rsTime.Tables(0).Rows(k).Item("Leaveamount2").ToString.Trim &
                                                " Where Paycode='" & rsTime.Tables(0).Rows(k).Item("Paycode").ToString.Trim & "' AND LYEAR=" & Convert.ToDateTime(rsTime.Tables(0).Rows(k).Item("DATEOFFICE").ToString.Trim).Year
                                                'Cn.Execute(sSql)
                                                If Common.servername = "Access" Then
                                                    Common.con1.Open()
                                                    cmd1 = New OleDbCommand(sSql, Common.con1)
                                                    cmd1.ExecuteNonQuery()
                                                    Common.con1.Close()
                                                Else
                                                    Common.con.Open()
                                                    cmd = New SqlCommand(sSql, Common.con)
                                                    cmd.ExecuteNonQuery()
                                                    Common.con.Close()
                                                End If
                                            End If
                                        ElseIf rsTime.Tables(0).Rows(k).Item("LEAVECODE").ToString.Trim <> "" Then
                                            sSql = "leavecode='" & rsTime.Tables(0).Rows(k).Item("Leavecode").ToString.Trim & "'"
                                            'rsLeaveMaster.Find(sSql)
                                            sSql = "Update tblLeaveLedger Set " & rsLeaveMaster.Tables(0).Rows(0).Item("LeaveField").ToString.Trim & "=" &
                                            rsLeaveMaster.Tables(0).Rows(0).Item("LeaveField").ToString.Trim & " - " & rsTime.Tables(0).Rows(k).Item("Leaveamount").ToString.Trim &
                                            " Where Paycode='" & rsTime.Tables(0).Rows(k).Item("Paycode").ToString.Trim & "' AND LYEAR=" & Convert.ToDateTime(rsTime.Tables(0).Rows(k).Item("DATEOFFICE").ToString.Trim).Year
                                            'Cn.Execute(sSql)
                                            'Cn.Execute(sSql)
                                            If Common.servername = "Access" Then
                                                Common.con1.Open()
                                                cmd1 = New OleDbCommand(sSql, Common.con1)
                                                cmd1.ExecuteNonQuery()
                                                Common.con1.Close()
                                            Else
                                                Common.con.Open()
                                                cmd = New SqlCommand(sSql, Common.con)
                                                cmd.ExecuteNonQuery()
                                                Common.con.Close()
                                            End If
                                        End If
                                    End If
                                    Dim reason As String
                                    If rsTime.Tables(0).Rows(k).Item("reason").ToString.Trim = "LATE_D" Then
                                        'rsTime!reason = ""
                                        reason = ""
                                    Else
                                        reason = rsTime.Tables(0).Rows(k).Item("reason").ToString.Trim
                                    End If
                                    'rsTime!Leavecode = Null
                                    'rsTime!LEAVETYPE = Null
                                    'rsTime!Voucher_No = Null
                                    'rsTime!LEAVETYPE1 = Null
                                    'rsTime!LEAVETYPE2 = Null
                                    'rsTime!FIRSTHALFLEAVECODE = Null
                                    'rsTime!SECONDHALFLEAVECODE = Null
                                    'rsTime!leaveamount = 0
                                    'rsTime!leaveamount1 = 0
                                    'rsTime!leaveamount2 = 0
                                    'Dim cn As Common
                                    status = cn.Upd(rsTime, EmpGpId)
                                    'Cn.Execute(sSql)
                                    'Cn.Execute(sSql)
                                    If Common.servername = "Access" Then
                                        sSql = "Update tblTimeRegister Set Leavecode = '', LEAVETYPE='', LEAVETYPE1='',LEAVETYPE2='', FIRSTHALFLEAVECODE='' , SECONDHALFLEAVECODE='', leaveamount=0, leaveamount1=0, leaveamount2=0, STATUS='" & status & "', reason='" & reason & "' " &
                                          " Where Paycode='" & rsTime.Tables(0).Rows(k).Item("Paycode").ToString.Trim & "' AND FORMAT(DateOFFICE,'yyyy-MM-dd')=" & Convert.ToDateTime(rsTime.Tables(0).Rows(k).Item("DATEOFFICE").ToString.Trim).ToString("yyyy-MM-dd")
                                        Common.con1.Open()
                                        cmd1 = New OleDbCommand(sSql, Common.con1)
                                        cmd1.ExecuteNonQuery()
                                        Common.con1.Close()
                                    Else
                                        sSql = "Update tblTimeRegister Set Leavecode = '', LEAVETYPE='', LEAVETYPE1='',LEAVETYPE2='', FIRSTHALFLEAVECODE='' , SECONDHALFLEAVECODE='', leaveamount=0, leaveamount1=0, leaveamount2=0, STATUS='" & status & "', reason='" & reason & "' " &
                                          " Where Paycode='" & rsTime.Tables(0).Rows(k).Item("Paycode").ToString.Trim & "' AND DateOFFICE=" & Convert.ToDateTime(rsTime.Tables(0).Rows(k).Item("DATEOFFICE").ToString.Trim)
                                        Common.con.Open()
                                        cmd = New SqlCommand(sSql, Common.con)
                                        cmd.ExecuteNonQuery()
                                        Common.con.Close()
                                    End If
                                    'rsTime.MoveNext()
                                    'If rsTime.EOF Then
                                    '    Exit Do
                                    'End If
                                Next ' Loop
                                'rsTime.UpdateBatch()
                                'rsTime.Close()
                            End If
                            If Common.servername = "Access" Then
                                sSql = " Select Paycode,DateOffice,AbsentValue,PresentValue,Wo_Value,Holiday_Value,Status,LeaveValue,in1," &
                                " ShiftAttended,REASON,LATEARRIVAL from tblTimeRegister Where (REASON<>'LATE_Rem' or reason is null ) and paycode='" & rsEmp.Tables(0).Rows(i)("Paycode") & "' and FORMAT(tblTimeRegister.DateOffice,'yyyy-MM-dd') Between '" & mmin_date.ToString("yyyy-MM-dd") & "' AND '" & mmax_date.ToString("yyyy-MM-dd") & "' Order By DateOffice"
                            Else
                                sSql = " Select Paycode,DateOffice,AbsentValue,PresentValue,Wo_Value,Holiday_Value,Status,LeaveValue,in1," &
                                " ShiftAttended,REASON,LATEARRIVAL from tblTimeRegister Where (REASON<>'LATE_Rem' or reason is null ) and paycode='" & rsEmp.Tables(0).Rows(i)("Paycode") & "' and tblTimeRegister.DateOffice Between '" & mmin_date.ToString("yyyy-MM-dd") & "' AND '" & mmax_date.ToString("yyyy-MM-dd") & "' Order By DateOffice"
                            End If
                            'rsTime = New ADODB.Recordset
                            'rsTime.Open(sSql, Cn, adOpenKeyset, adLockBatchOptimistic)
                            rsTime = New DataSet
                            If servername = "Access" Then
                                adapAc = New OleDbDataAdapter(sSql, con1)
                                adapAc.Fill(rsTime)
                            Else
                                adapS = New SqlDataAdapter(sSql, con)
                                adapS.Fill(rsTime)
                            End If

                            If rsTime.Tables(0).Rows.Count > 0 Then '  If rsTime.RecordCount > 0 Then
                                'rsTime.MoveFirst()
                                iLateValue = 0
                                iDeductValue = 0
                                For j As Integer = 0 To rsTime.Tables(0).Rows.Count - 1 ' Do Until rsTime.EOF
                                    Dim ABSENTVALUE As String
                                    Dim reason As String
                                    Dim Status As String
                                    Dim PRESENTVALUE As String
                                    Dim maxlatedur
                                    Dim LATEARRIVAL
                                    If rsEmp.Tables(0).Rows(i)("maxlatedur").ToString.Trim = "" Then
                                        maxlatedur = "0"
                                    Else
                                        maxlatedur = rsEmp.Tables(0).Rows(i)("maxlatedur").ToString.Trim
                                    End If
                                    If rsTime.Tables(0).Rows(j)("LATEARRIVAL").ToString.Trim = "" Then
                                        LATEARRIVAL = "0"
                                    Else
                                        LATEARRIVAL = rsTime.Tables(0).Rows(j)("LATEARRIVAL").ToString.Trim
                                    End If
                                    If Convert.ToDouble(maxlatedur) > 0 And Convert.ToDouble(LATEARRIVAL) > maxlatedur Then
                                        iDeductValue = rsEmp.Tables(0).Rows(i)("DeductDay")
                                        If rsTime.Tables(0).Rows(j)("PRESENTVALUE") > 0 Then
                                            If iDeductValue > rsTime.Tables(0).Rows(j)("PRESENTVALUE") Then
                                                iFlag = True
                                                iDeductValue = iDeductValue - rsTime.Tables(0).Rows(j)("PRESENTVALUE")
                                                ABSENTVALUE = rsTime.Tables(0).Rows(j)("ABSENTVALUE") + rsTime.Tables(0).Rows(j)("PRESENTVALUE")
                                                reason = "LATE_D"
                                                Status = "LT_A" & Trim(rsTime.Tables(0).Rows(j)("PRESENTVALUE").ToString)
                                                PRESENTVALUE = 0
                                                'rsTime.Update()
                                            Else
                                                iFlag = True
                                                ABSENTVALUE = rsTime.Tables(0).Rows(j)("ABSENTVALUE") + iDeductValue
                                                PRESENTVALUE = rsTime.Tables(0).Rows(j)("PRESENTVALUE") - iDeductValue
                                                reason = "LATE_D"
                                                Status = "LT_A" & Trim(iDeductValue.ToString)
                                                iDeductValue = 0
                                                'rsTime.Update()
                                            End If
                                            sSql = "update tblTimeRegister set ABSENTVALUE ='" & ABSENTVALUE & "', reason='" & reason & "', Status='" & Status & "', PRESENTVALUE='" & PRESENTVALUE & "' " &
                                                    " Where paycode='" & rsEmp.Tables(0).Rows(i)("Paycode").ToString.Trim & "' and DateOffice = '" & Convert.ToDateTime(rsTime.Tables(0).Rows(j)("DateOffice")).ToString("yyyy-MM-dd") & "' "
                                            If Common.servername = "Access" Then
                                                sSql = sSql.Replace("and DateOffice", "and FORMAT(DateOffice,'yyyy-MM-dd')")
                                                If Common.con1.State <> ConnectionState.Open Then
                                                    Common.con1.Open()
                                                End If
                                                cmd1 = New OleDbCommand(sSql, Common.con1)
                                                cmd1.ExecuteNonQuery()
                                                If Common.con1.State <> ConnectionState.Closed Then
                                                    Common.con1.Close()
                                                End If
                                            Else
                                                If Common.con.State <> ConnectionState.Open Then
                                                    Common.con.Open()
                                                End If
                                                cmd = New SqlCommand(sSql, Common.con)
                                                cmd.ExecuteNonQuery()
                                                If Common.con.State <> ConnectionState.Closed Then
                                                    Common.con.Close()
                                                End If
                                            End If
                                        End If
                                    Else
                                        If LATEARRIVAL > 0 Then
                                            iLateValue = iLateValue + 1
                                        End If
                                    End If
                                    If rsEmp.Tables(0).Rows(i)("EveryInterval") = "Y" And iLateValue > rsEmp.Tables(0).Rows(i)("LateDays") Then
                                        iLateValue = 0
                                        iDeductValue = rsEmp.Tables(0).Rows(i)("DeductDay")
                                        If rsTime.Tables(0).Rows(j)("PRESENTVALUE") > 0 Then
                                            If iDeductValue > rsTime.Tables(0).Rows(j)("PRESENTVALUE") Then
                                                iFlag = True
                                                iDeductValue = iDeductValue - rsTime.Tables(0).Rows(j)("PRESENTVALUE")
                                                ABSENTVALUE = rsTime.Tables(0).Rows(j)("ABSENTVALUE") + rsTime.Tables(0).Rows(j)("PRESENTVALUE")
                                                reason = "LATE_D"
                                                'Status = "LT_A" & Trim(Str(rsTime.Tables(0).Rows(j)("PRESENTVALUE")))
                                                Status = "LT_A" & rsTime.Tables(0).Rows(j)("PRESENTVALUE").ToString.Trim
                                                PRESENTVALUE = 0
                                                'rsTime.Update()
                                            Else
                                                iFlag = True
                                                ABSENTVALUE = rsTime.Tables(0).Rows(j)("ABSENTVALUE") + iDeductValue
                                                PRESENTVALUE = rsTime.Tables(0).Rows(j)("PRESENTVALUE") - iDeductValue
                                                reason = "LATE_D"
                                                'Status = "LT_A" & Trim(Str(iDeductValue))
                                                Status = "LT_A" & iDeductValue.ToString
                                                iDeductValue = 0
                                                'rsTime.Update()
                                            End If
                                            sSql = "update tblTimeRegister set ABSENTVALUE ='" & ABSENTVALUE & "', reason='" & reason & "', Status='" & Status & "', PRESENTVALUE='" & PRESENTVALUE & "' " &
                                                    " Where paycode='" & rsEmp.Tables(0).Rows(i)("Paycode").ToString.Trim & "' and DateOffice = '" & Convert.ToDateTime(rsTime.Tables(0).Rows(j)("DateOffice").ToString.Trim).ToString("yyyy-MM-dd") & "' "
                                            If Common.servername = "Access" Then
                                                sSql = sSql.Replace("and DateOffice", "and FORMAT(DateOffice,'yyyy-MM-dd')")
                                                If Common.con1.State <> ConnectionState.Open Then
                                                    Common.con1.Open()
                                                End If
                                                cmd1 = New OleDbCommand(sSql, Common.con1)
                                                cmd1.ExecuteNonQuery()
                                                If Common.con1.State <> ConnectionState.Closed Then
                                                    Common.con1.Close()
                                                End If
                                            Else
                                                If Common.con.State <> ConnectionState.Open Then
                                                    Common.con.Open()
                                                End If
                                                cmd = New SqlCommand(sSql, Common.con)
                                                cmd.ExecuteNonQuery()
                                                If Common.con.State <> ConnectionState.Closed Then
                                                    Common.con.Close()
                                                End If
                                            End If

                                        End If
                                    End If
                                    If rsEmp.Tables(0).Rows(i)("EveryInterval") = "N" And iLateValue > rsEmp.Tables(0).Rows(i)("LateDays") Then
                                        iLateValue = rsEmp.Tables(0).Rows(i)("LateDays")
                                        iDeductValue = rsEmp.Tables(0).Rows(i)("DeductDay")
                                        If rsTime.Tables(0).Rows(j)("PRESENTVALUE") > 0 Then
                                            If iDeductValue > rsTime.Tables(0).Rows(j)("PRESENTVALUE") Then
                                                iFlag = True
                                                iDeductValue = iDeductValue - rsTime.Tables(0).Rows(j)("PRESENTVALUE")
                                                ABSENTVALUE = rsTime.Tables(0).Rows(j)("ABSENTVALUE") + rsTime.Tables(0).Rows(j)("PRESENTVALUE")
                                                reason = "LATE_D"
                                                Status = "LT_A" & Trim(rsTime.Tables(0).Rows(j)("PRESENTVALUE").ToString)
                                                PRESENTVALUE = 0
                                                'rsTime.Update()
                                            Else
                                                iFlag = True
                                                ABSENTVALUE = rsTime.Tables(0).Rows(j)("ABSENTVALUE") + iDeductValue
                                                PRESENTVALUE = rsTime.Tables(0).Rows(j)("PRESENTVALUE") - iDeductValue
                                                reason = "LATE_D"
                                                Status = "LT_A" & Trim(iDeductValue)
                                                iDeductValue = 0
                                                'rsTime.Update()
                                            End If
                                        End If
                                        sSql = "update tblTimeRegister set ABSENTVALUE ='" & ABSENTVALUE & "', reason='" & reason & "', Status='" & Status & "', PRESENTVALUE='" & PRESENTVALUE & "' " &
                                                    " Where paycode='" & rsEmp.Tables(0).Rows(i)("Paycode").ToString.Trim & "' and DateOffice = '" & Convert.ToDateTime(rsTime.Tables(0).Rows(j)("DateOffice")).ToString("yyyy-MM-dd") & "' "
                                        If Common.servername = "Access" Then
                                            sSql = sSql.Replace("and DateOffice", "and FORMAT(DateOffice,'yyyy-MM-dd')")
                                            If Common.con1.State <> ConnectionState.Open Then
                                                Common.con1.Open()
                                            End If
                                            cmd1 = New OleDbCommand(sSql, Common.con1)
                                            cmd1.ExecuteNonQuery()
                                            If Common.con1.State <> ConnectionState.Closed Then
                                                Common.con1.Close()
                                            End If
                                        Else
                                            If Common.con.State <> ConnectionState.Open Then
                                                Common.con.Open()
                                            End If
                                            cmd = New SqlCommand(sSql, Common.con)
                                            cmd.ExecuteNonQuery()
                                            If Common.con.State <> ConnectionState.Closed Then
                                                Common.con.Close()
                                            End If
                                        End If
                                    End If
                                    'rsTime.MoveNext()
                                    'If rsTime.EOF Then Exit Do
                                Next ' Loop
                                'If iFlag = True Then
                                '    rsTime.UpdateBatch()
                                'End If
                            End If
                        End If
                        'rsTime.Close()
                    Else
                        If rsEmp.Tables(0).Rows(i)("LateDays") > 0 And rsEmp.Tables(0).Rows(i)("DeductDay") > 0 And rsEmp.Tables(0).Rows(i)("FromLeave").ToString.Trim <> "" Then
                            sSql = "select * from tblleavemaster order by leavecode"
                            rsLeaveMaster = New DataSet
                            If servername = "Access" Then
                                adapAc = New OleDbDataAdapter(sSql, con1)
                                adapAc.Fill(rsLeaveMaster)
                            Else
                                adapS = New SqlDataAdapter(sSql, con)
                                adapS.Fill(rsLeaveMaster)
                            End If
                            'rsLeaveMaster = Cn.Execute("select * from tblleavemaster order by leavecode")

                            sSql = "select * from tblleavemaster order by leavefield"
                            rsLeaveMaster1 = New DataSet
                            If servername = "Access" Then
                                adapAc = New OleDbDataAdapter(sSql, con1)
                                adapAc.Fill(rsLeaveMaster1)
                            Else
                                adapS = New SqlDataAdapter(sSql, con)
                                adapS.Fill(rsLeaveMaster1)
                            End If
                            'rsLeaveMaster1 = Cn.Execute("select * from tblleavemaster order by leavefield")
                            sSql = "leavefield='" & rsEmp.Tables(0).Rows(i)("FromLeave") & "'"
                            Dim SerchRows() As Data.DataRow = rsLeaveMaster1.Tables(0).Select(sSql)
                            'rsLeaveMaster1.Find(sSql)

                            sSql = "Select * from tblLeaveLedger Where Paycode='" & rsEmp.Tables(0).Rows(i)("Paycode").ToString.Trim & "' and lyear=" & DateEditVerification.DateTime.ToString("yyyy")
                            'rsLeaveLedger = Cn.Execute(sSql)
                            Dim rsLeaveLedger As DataSet = New DataSet
                            If servername = "Access" Then
                                adapAc = New OleDbDataAdapter(sSql, con1)
                                adapAc.Fill(rsLeaveLedger)
                            Else
                                adapS = New SqlDataAdapter(sSql, con)
                                adapS.Fill(rsLeaveLedger)
                            End If
                            If rsLeaveLedger.Tables(0).Rows.Count = 0 Then
                                sSql = "Insert into tblleaveledger (paycode,lyear,L01,L02,L03,L04,L05,L06,L07,L08,L09,L10,L11,L12,L13,L14,L15,L16,L17,L18,L19,L20,L01_Add,L02_Add,L03_Add,L04_Add,L05_Add,L06_Add,L07_Add,L08_Add,L09_Add,L10_Add,L11_Add,L12_Add,L13_Add,L14_Add,L15_Add,L16_Add,L17_Add,L18_Add,L19_Add,L20_Add) values('" & rsEmp.Tables(0).Rows(i)("paycode").ToString.Trim & "'," & DateEditVerification.DateTime.ToString("yyyy") & ",0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)"
                                If Common.servername = "Access" Then
                                    If Common.con1.State <> ConnectionState.Open Then
                                        Common.con1.Open()
                                    End If
                                    cmd1 = New OleDbCommand(sSql, Common.con1)
                                    cmd1.ExecuteNonQuery()
                                    If Common.con1.State <> ConnectionState.Closed Then
                                        Common.con1.Close()
                                    End If
                                Else
                                    If Common.con.State <> ConnectionState.Open Then
                                        Common.con.Open()
                                    End If
                                    cmd = New SqlCommand(sSql, Common.con)
                                    cmd.ExecuteNonQuery()
                                    If Common.con.State <> ConnectionState.Closed Then
                                        Common.con.Close()
                                    End If
                                End If
                                'Cn.Execute(sSql)
                                sSql = "Select * from tblLeaveLedger Where Paycode='" & rsEmp.Tables(0).Rows(i)("Paycode").ToString.Trim & "' and lyear=" & DateEditVerification.DateTime.ToString("yyyy")
                                If servername = "Access" Then
                                    adapAc = New OleDbDataAdapter(sSql, con1)
                                    adapAc.Fill(rsLeaveLedger)
                                Else
                                    adapS = New SqlDataAdapter(sSql, con)
                                    adapS.Fill(rsLeaveLedger)
                                End If
                                'rsLeaveLedger = Cn.Execute(sSql)
                            End If
                            If rsLeaveLedger.Tables(0).Rows.Count > 0 Then
                                If rsLeaveLedger.Tables(0).Rows(0)(SerchRows(0)("LeaveField") & "_Add" & "") - rsLeaveLedger.Tables(0).Rows(0)(SerchRows(0)("LeaveField") & "") >= rsEmp.Tables(0).Rows(i)("DeductDay") Then
                                    mLeaveCode = SerchRows(0)("Leavecode").ToString.Trim
                                    mLeaveType = SerchRows(0)("LEAVETYPE").ToString.Trim
                                Else
                                    'rsLeaveMaster1.MoveFirst()
                                    sSql = "leavefield='" & rsEmp.Tables(0).Rows(i)("FromLeave1") & "'"
                                    'rsLeaveMaster1.Find(sSql)
                                    SerchRows = rsLeaveMaster1.Tables(0).Select(sSql)
                                    If rsLeaveLedger.Tables(0).Rows(0)(SerchRows(0)("LeaveField") & "_Add" & "") - rsLeaveLedger.Tables(0).Rows(0)(SerchRows(0)("LeaveField") & "") >= rsEmp.Tables(0).Rows(i)("DeductDay") Then
                                        mLeaveCode = SerchRows(0)("Leavecode").ToString.Trim
                                        mLeaveType = SerchRows(0)("LEAVETYPE").ToString.Trim
                                    Else
                                        'rsLeaveMaster1.MoveFirst()
                                        sSql = "leavefield='" & rsEmp.Tables(0).Rows(i)("FromLeave") & "'"
                                        'rsLeaveMaster1.Find(sSql)
                                        SerchRows = rsLeaveMaster1.Tables(0).Select(sSql)
                                        mLeaveCode = SerchRows(0)("Leavecode").ToString.Trim
                                        mLeaveType = SerchRows(0)("LEAVETYPE").ToString.Trim
                                    End If
                                End If

                            End If



                            If Common.servername = "Access" Then
                                sSql = " Select * from tblTimeRegister Where (REASON='LATE_D' or REASON='LATE_Rem') AND paycode='" & rsEmp.Tables(0).Rows(i)("Paycode").ToString.Trim & "' and FORMAT(tblTimeRegister.DateOffice,'yyyy-MM-dd') Between '" & Format(mmin_date, "yyyy-MM-dd") & "' AND '" & Format(mmax_date, "yyyy-MM-dd") & "' Order By DateOffice"
                            Else
                                sSql = " Select * from tblTimeRegister Where (REASON='LATE_D' or REASON='LATE_Rem') AND paycode='" & rsEmp.Tables(0).Rows(i)("Paycode").ToString.Trim & "' and tblTimeRegister.DateOffice Between '" & Format(mmin_date, "yyyy-MM-dd") & "' AND '" & Format(mmax_date, "yyyy-MM-dd") & "' Order By DateOffice"
                            End If
                            rsTime = New DataSet 'ADODB.Recordset
                            If servername = "Access" Then
                                adapAc = New OleDbDataAdapter(sSql, con1)
                                adapAc.Fill(rsTime)
                            Else
                                adapS = New SqlDataAdapter(sSql, con)
                                adapS.Fill(rsTime)
                            End If
                            'rsTime.Open(sSql, Cn, adOpenKeyset, adLockBatchOptimistic)
                            If rsTime.Tables(0).Rows.Count > 0 Then
                                'rsTime.MoveFirst()
                                For j As Integer = 0 To rsTime.Tables(0).Rows.Count - 1 '   Do Until rsTime.EOF
                                    If (rsTime.Tables(0).Rows(j)("leaveamount").ToString.Trim <> "" And rsTime.Tables(0).Rows(j)("leaveamount") > 0) Then
                                        If rsTime.Tables(0).Rows(j)("FIRSTHALFLEAVECODE").ToString.Trim <> "" Or rsTime.Tables(0).Rows(j)("SECONDHALFLEAVECODE").ToString.Trim <> "" Then
                                            If rsTime.Tables(0).Rows(j)("FIRSTHALFLEAVECODE").ToString.Trim <> "" Then
                                                sSql = "leavecode='" & rsTime.Tables(0).Rows(j)("FIRSTHALFLEAVECODE").ToString.Trim & "'"
                                                'rsLeaveMaster.Find(sSql) 
                                                SerchRows = rsLeaveMaster1.Tables(0).Select(sSql)
                                                sSql = "Update tblLeaveLedger Set " & SerchRows(0)("LeaveField").ToString.Trim & "=" &
                                                 SerchRows(0)("LeaveField").ToString.Trim & " - " & rsTime.Tables(0).Rows(j)("Leaveamount1") &
                                                " Where Paycode='" & rsTime.Tables(0).Rows(j)("Paycode").ToString.Trim & "' AND LYEAR=" & Year(rsTime.Tables(0).Rows(j)("DATEOFFICE"))
                                                'Cn.Execute(sSql)
                                                If Common.servername = "Access" Then
                                                    If Common.con1.State <> ConnectionState.Open Then
                                                        Common.con1.Open()
                                                    End If
                                                    cmd1 = New OleDbCommand(sSql, Common.con1)
                                                    cmd1.ExecuteNonQuery()
                                                    If Common.con1.State <> ConnectionState.Closed Then
                                                        Common.con1.Close()
                                                    End If
                                                Else
                                                    If Common.con.State <> ConnectionState.Open Then
                                                        Common.con.Open()
                                                    End If
                                                    cmd = New SqlCommand(sSql, Common.con)
                                                    cmd.ExecuteNonQuery()
                                                    If Common.con.State <> ConnectionState.Closed Then
                                                        Common.con.Close()
                                                    End If
                                                End If
                                            End If
                                            If rsTime.Tables(0).Rows(j)("SECONDHALFLEAVECODE").ToString.Trim <> "" Then
                                                sSql = "leavecode='" & rsTime.Tables(0).Rows(j)("SECONDHALFLEAVECODE").ToString.Trim & "'"
                                                'rsLeaveMaster.Find(sSql)
                                                SerchRows = rsLeaveMaster1.Tables(0).Select(sSql)
                                                sSql = "Update tblLeaveLedger Set " & SerchRows(0)("LeaveField") & "=" &
                                                SerchRows(0)("LeaveField") & " - " & (rsTime.Tables(0).Rows(j)("Leaveamount2").ToString.Trim) &
                                                " Where Paycode='" & rsTime.Tables(0).Rows(j)("Paycode").ToString.Trim & "' AND LYEAR=" & Year(rsTime.Tables(0).Rows(j)("DATEOFFICE"))
                                                'Cn.Execute(sSql)
                                                If Common.servername = "Access" Then
                                                    If Common.con1.State <> ConnectionState.Open Then
                                                        Common.con1.Open()
                                                    End If
                                                    cmd1 = New OleDbCommand(sSql, Common.con1)
                                                    cmd1.ExecuteNonQuery()
                                                    If Common.con1.State <> ConnectionState.Closed Then
                                                        Common.con1.Close()
                                                    End If
                                                Else
                                                    If Common.con.State <> ConnectionState.Open Then
                                                        Common.con.Open()
                                                    End If
                                                    cmd = New SqlCommand(sSql, Common.con)
                                                    cmd.ExecuteNonQuery()
                                                    If Common.con.State <> ConnectionState.Closed Then
                                                        Common.con.Close()
                                                    End If
                                                End If
                                            End If
                                        ElseIf rsTime.Tables(0).Rows(j)("LEAVECODE").ToString.Trim <> "" Then
                                            sSql = "leavecode='" & rsTime.Tables(0).Rows(j)("Leavecode").ToString.Trim & "'"
                                            'rsLeaveMaster.Find(sSql)
                                            SerchRows = rsLeaveMaster1.Tables(0).Select(sSql)
                                            sSql = "Update tblLeaveLedger Set " & SerchRows(0)("LeaveField") & "=" &
                                            SerchRows(0)("LeaveField") & " - " & (rsTime.Tables(0).Rows(j)("Leaveamount").ToString.Trim) &
                                            " Where Paycode='" & rsTime.Tables(0).Rows(j)("Paycode").ToString.Trim & "' AND LYEAR=" & Year(rsTime.Tables(0).Rows(j)("DATEOFFICE"))
                                            'Cn.Execute(sSql)
                                            If Common.servername = "Access" Then
                                                If Common.con1.State <> ConnectionState.Open Then
                                                    Common.con1.Open()
                                                End If
                                                cmd1 = New OleDbCommand(sSql, Common.con1)
                                                cmd1.ExecuteNonQuery()
                                                If Common.con1.State <> ConnectionState.Closed Then
                                                    Common.con1.Close()
                                                End If
                                            Else
                                                If Common.con.State <> ConnectionState.Open Then
                                                    Common.con.Open()
                                                End If
                                                cmd = New SqlCommand(sSql, Common.con)
                                                cmd.ExecuteNonQuery()
                                                If Common.con.State <> ConnectionState.Closed Then
                                                    Common.con.Close()
                                                End If
                                            End If
                                        End If
                                    End If
                                    Dim reason As String
                                    If Trim(rsTime.Tables(0).Rows(j)("reason").ToString) = "LATE_D" Then
                                        reason = ""
                                    Else
                                        reason = rsTime.Tables(0).Rows(j)("reason").ToString.Trim
                                    End If
                                    Dim Leavecode As String = ""
                                    Dim LEAVETYPE As String = ""
                                    Dim Voucher_No As String = ""
                                    Dim LEAVETYPE1 = ""
                                    Dim LEAVETYPE2 = ""
                                    Dim FIRSTHALFLEAVECODE = ""
                                    Dim SECONDHALFLEAVECODE = ""
                                    Dim leaveamount = 0
                                    Dim leaveamount1 = 0
                                    Dim leaveamount2 = 0

                                    sSql = "update tblTimeRegister set reason='" & reason & "', Leavecode='" & Leavecode & "', LEAVETYPE='" & LEAVETYPE & "', " &
                                        "Voucher_No='" & Voucher_No & "', LEAVETYPE1='" & LEAVETYPE1 & "', LEAVETYPE2='" & LEAVETYPE2 & "', FIRSTHALFLEAVECODE='" & FIRSTHALFLEAVECODE & "'" &
                                        ",SECONDHALFLEAVECODE='" & SECONDHALFLEAVECODE & "',leaveamount=" & leaveamount & ", leaveamount1=" & leaveamount1 & ", leaveamount2=" & leaveamount2 &
                                        " where paycode='" & rsEmp.Tables(0).Rows(i)("Paycode").ToString.Trim & "' and DateOffice ='" & Convert.ToDateTime(rsTime.Tables(0).Rows(j)("DATEOFFICE").ToString.Trim).ToString("yyyy-MM-dd") & "'"
                                    If Common.servername = "Access" Then
                                        sSql = sSql.Replace("and DateOffice", "and FORMAT(DateOffice,'yyyy-MM-dd')")
                                        If Common.con1.State <> ConnectionState.Open Then
                                            Common.con1.Open()
                                        End If
                                        cmd1 = New OleDbCommand(sSql, Common.con1)
                                        cmd1.ExecuteNonQuery()
                                        If Common.con1.State <> ConnectionState.Closed Then
                                            Common.con1.Close()
                                        End If
                                    Else
                                        If Common.con.State <> ConnectionState.Open Then
                                            Common.con.Open()
                                        End If
                                        cmd = New SqlCommand(sSql, Common.con)
                                        cmd.ExecuteNonQuery()
                                        If Common.con.State <> ConnectionState.Closed Then
                                            Common.con.Close()
                                        End If
                                    End If
                                    sSql = "Select * from tblTimeRegister  where paycode='" & rsEmp.Tables(0).Rows(i)("Paycode").ToString.Trim & "'" &
                                        " and DateOffice ='" & Convert.ToDateTime(rsTime.Tables(0).Rows(j)("DATEOFFICE").ToString.Trim).ToString("yyyy-MM-dd") & "'"

                                    rsTime1 = New DataSet 'ADODB.Recordset
                                    If servername = "Access" Then
                                        sSql = sSql.Replace("and DateOffice", "and FORMAT(DateOffice,'yyyy-MM-dd')")
                                        adapAc = New OleDbDataAdapter(sSql, con1)
                                        adapAc.Fill(rsTime1)
                                    Else
                                        adapS = New SqlDataAdapter(sSql, con)
                                        adapS.Fill(rsTime1)
                                    End If

                                    cn.Upd(rsTime1, EmpGpId)

                                    'rsTime.MoveNext()
                                    'If rsTime.EOF Then
                                    '    Exit Do
                                    'End If
                                Next ' Loop
                                'rsTime.UpdateBatch()
                                'rsTime.Close()
                            End If
                            If Common.servername = "Access" Then
                                sSql = "Select * from tblTimeRegister Where (REASON<>'LATE_Rem' or reason is null ) and paycode='" & rsEmp.Tables(0).Rows(i)("Paycode").ToString.Trim & "' and FORMAT(tblTimeRegister.DateOffice,'yyyy-MM-dd') Between '" & Format(mmin_date, "yyyy-MM-dd") & "' AND '" & Format(mmax_date, "yyyy-MM-dd") & "' Order By DateOffice"
                            Else
                                sSql = "Select * from tblTimeRegister Where (REASON<>'LATE_Rem' or reason is null ) and paycode='" & rsEmp.Tables(0).Rows(i)("Paycode").ToString.Trim & "' and tblTimeRegister.DateOffice Between '" & Format(mmin_date, "yyyy-MM-dd") & "' AND '" & Format(mmax_date, "yyyy-MM-dd") & "' Order By DateOffice"
                            End If
                            rsTime = New DataSet 'ADODB.Recordset
                            If servername = "Access" Then
                                adapAc = New OleDbDataAdapter(sSql, con1)
                                adapAc.Fill(rsTime)
                            Else
                                adapS = New SqlDataAdapter(sSql, con)
                                adapS.Fill(rsTime)
                            End If
                            'rsTime = New ADODB.Recordset
                            'rsTime.Open(sSql, Cn, adOpenKeyset, adLockBatchOptimistic)
                            If rsTime.Tables(0).Rows.Count > 0 Then
                                'rsTime.MoveFirst()
                                iLateValue = 0
                                iDeductValue = 0
                                '*************************
                                For j As Integer = 0 To rsTime.Tables(0).Rows.Count - 1 ' Do Until rsTime.EOF
                                    Dim maxlatedur, LATEARRIVAL
                                    If rsEmp.Tables(0).Rows(i)("maxlatedur").ToString.Trim = "" Then : maxlatedur = 0 : Else : maxlatedur = Convert.ToDouble(rsEmp.Tables(0).Rows(i)("maxlatedur").ToString.Trim) : End If
                                    If rsTime.Tables(0).Rows(j)("LATEARRIVAL").ToString.Trim = "" Then : LATEARRIVAL = 0 : Else : LATEARRIVAL = Convert.ToDouble(rsTime.Tables(0).Rows(j)("LATEARRIVAL").ToString.Trim) : End If
                                    'If rsEmp.Tables(0).Rows(i)("maxlatedur") > 0 And rsTime.Tables(0).Rows(j)("LATEARRIVAL") > rsEmp.Tables(0).Rows(i)("maxlatedur") Then
                                    If maxlatedur > 0 And LATEARRIVAL > maxlatedur Then
                                        iDeductValue = rsEmp.Tables(0).Rows(i)("DeductDay")
                                        If iDeductValue >= 1 Then
                                            iFlag = True
                                            Dim leaveamount = 1
                                            Dim LEAVETYPE = mLeaveType
                                            Dim Leavecode = mLeaveCode
                                            Dim reason = "LATE_D"
                                            iDeductValue = iDeductValue - 1

                                            sSql = "update tblTimeRegister set reason='" & reason & "',leaveamount=" & leaveamount & ", Leavecode='" & Leavecode & "', LEAVETYPE='" & LEAVETYPE & "' " &
                                        " where paycode='" & rsEmp.Tables(0).Rows(i)("Paycode").ToString.Trim & "' and DateOffice ='" & Convert.ToDateTime(rsTime.Tables(0).Rows(j)("DATEOFFICE").ToString.Trim).ToString("yyyy-MM-dd") & "'"
                                            If Common.servername = "Access" Then
                                                sSql = sSql.Replace("and DateOffice", "and FORMAT(DateOffice,'yyyy-MM-dd')")
                                                If Common.con1.State <> ConnectionState.Open Then
                                                    Common.con1.Open()
                                                End If
                                                cmd1 = New OleDbCommand(sSql, Common.con1)
                                                cmd1.ExecuteNonQuery()
                                                If Common.con1.State <> ConnectionState.Closed Then
                                                    Common.con1.Close()
                                                End If
                                            Else
                                                If Common.con.State <> ConnectionState.Open Then
                                                    Common.con.Open()
                                                End If
                                                cmd = New SqlCommand(sSql, Common.con)
                                                cmd.ExecuteNonQuery()
                                                If Common.con.State <> ConnectionState.Closed Then
                                                    Common.con.Close()
                                                End If
                                            End If

                                            sSql = "Select * from tblTimeRegister  where paycode='" & rsEmp.Tables(0).Rows(i)("Paycode").ToString.Trim & "'" &
                                        " and DateOffice ='" & Convert.ToDateTime(rsTime.Tables(0).Rows(j)("DATEOFFICE").ToString.Trim).ToString("yyyy-MM-dd") & "'"
                                            rsTime1 = New DataSet 'ADODB.Recordset
                                            If servername = "Access" Then
                                                sSql = sSql.Replace("and DateOffice", "and FORMAT(DateOffice,'yyyy-MM-dd')")
                                                adapAc = New OleDbDataAdapter(sSql, con1)
                                                adapAc.Fill(rsTime1)
                                            Else
                                                adapS = New SqlDataAdapter(sSql, con)
                                                adapS.Fill(rsTime1)
                                            End If
                                            cn.Upd(rsTime1, EmpGpId)

                                            sSql = "Update tblLeaveLedger Set " & SerchRows(0)("LeaveField") & "=" &
                                             SerchRows(0)("LeaveField") & " +1 " &
                                            " Where Paycode='" & rsTime1.Tables(0).Rows(0)("Paycode").ToString.Trim & "' AND LYEAR=" & Year(rsTime1.Tables(0).Rows(0)("DATEOFFICE"))
                                            If Common.servername = "Access" Then
                                                If Common.con1.State <> ConnectionState.Open Then
                                                    Common.con1.Open()
                                                End If
                                                cmd1 = New OleDbCommand(sSql, Common.con1)
                                                cmd1.ExecuteNonQuery()
                                                If Common.con1.State <> ConnectionState.Closed Then
                                                    Common.con1.Close()
                                                End If
                                            Else
                                                If Common.con.State <> ConnectionState.Open Then
                                                    Common.con.Open()
                                                End If
                                                cmd = New SqlCommand(sSql, Common.con)
                                                cmd.ExecuteNonQuery()
                                                If Common.con.State <> ConnectionState.Closed Then
                                                    Common.con.Close()
                                                End If
                                            End If
                                            'cn.Execute(sSql)
                                        ElseIf iDeductValue > 0 And iDeductValue < 1 Then
                                            iFlag = True
                                            Dim leaveamount = iDeductValue
                                            Dim LEAVETYPE = mLeaveType
                                            Dim Leavecode = mLeaveCode
                                            Dim leaveamount1 = iDeductValue
                                            Dim FIRSTHALFLEAVECODE = Trim(mLeaveCode)
                                            Dim reason = "LATE_D"

                                            sSql = "update tblTimeRegister set reason='" & reason & "', Leavecode='" & Leavecode & "', LEAVETYPE='" & LEAVETYPE & "', " &
                                        " FIRSTHALFLEAVECODE='" & FIRSTHALFLEAVECODE & "',leaveamount=" & leaveamount & ", leaveamount1=" & leaveamount1 &
                                        " where paycode='" & rsEmp.Tables(0).Rows(i)("Paycode").ToString.Trim & "' and DateOffice ='" & Convert.ToDateTime(rsTime.Tables(0).Rows(j)("DATEOFFICE").ToString.Trim).ToString("yyyy-MM-dd") & "'"
                                            If Common.servername = "Access" Then
                                                sSql = sSql.Replace("and DateOffice", "and FORMAT(DateOffice,'yyyy-MM-dd')")
                                                If Common.con1.State <> ConnectionState.Open Then
                                                    Common.con1.Open()
                                                End If
                                                cmd1 = New OleDbCommand(sSql, Common.con1)
                                                cmd1.ExecuteNonQuery()
                                                If Common.con1.State <> ConnectionState.Closed Then
                                                    Common.con1.Close()
                                                End If
                                            Else
                                                If Common.con.State <> ConnectionState.Open Then
                                                    Common.con.Open()
                                                End If
                                                cmd = New SqlCommand(sSql, Common.con)
                                                cmd.ExecuteNonQuery()
                                                If Common.con.State <> ConnectionState.Closed Then
                                                    Common.con.Close()
                                                End If
                                            End If
                                            sSql = "Select * from tblTimeRegister  where paycode='" & rsEmp.Tables(0).Rows(i)("Paycode").ToString.Trim & "'" &
                                                " and DateOffice ='" & Convert.ToDateTime(rsTime.Tables(0).Rows(j)("DATEOFFICE").ToString.Trim).ToString("yyyy-MM-dd") & "'"
                                            rsTime1 = New DataSet 'ADODB.Recordset
                                            If servername = "Access" Then
                                                sSql = sSql.Replace("and DateOffice", "and FORMAT(DateOffice,'yyyy-MM-dd')")
                                                adapAc = New OleDbDataAdapter(sSql, con1)
                                                adapAc.Fill(rsTime1)
                                            Else
                                                adapS = New SqlDataAdapter(sSql, con)
                                                adapS.Fill(rsTime1)
                                            End If
                                            cn.Upd(rsTime1, EmpGpId)
                                            sSql = "Update tblLeaveLedger Set " & SerchRows(0)("LeaveField") & "=" &
                                            SerchRows(0)("LeaveField") & " + " & iDeductValue &
                                            " Where Paycode='" & rsTime1.Tables(0).Rows(0)("Paycode").ToString.Trim & "' AND LYEAR=" & Year(rsTime1.Tables(0).Rows(0)("DATEOFFICE"))
                                            If Common.servername = "Access" Then
                                                If Common.con1.State <> ConnectionState.Open Then
                                                    Common.con1.Open()
                                                End If
                                                cmd1 = New OleDbCommand(sSql, Common.con1)
                                                cmd1.ExecuteNonQuery()
                                                If Common.con1.State <> ConnectionState.Closed Then
                                                    Common.con1.Close()
                                                End If
                                            Else
                                                If Common.con.State <> ConnectionState.Open Then
                                                    Common.con.Open()
                                                End If
                                                cmd = New SqlCommand(sSql, Common.con)
                                                cmd.ExecuteNonQuery()
                                                If Common.con.State <> ConnectionState.Closed Then
                                                    Common.con.Close()
                                                End If
                                            End If
                                            'cn.Execute(sSql)
                                            iDeductValue = 0
                                        End If
                                    Else
                                        Dim LATEARRIVALTmp
                                        If rsTime.Tables(0).Rows(j)("LATEARRIVAL").ToString.Trim = "" Then
                                            LATEARRIVALTmp = 0
                                        Else
                                            LATEARRIVALTmp = rsTime.Tables(0).Rows(j)("LATEARRIVAL").ToString.Trim
                                        End If
                                        If LATEARRIVALTmp > 0 Then ' If rsTime.Tables(0).Rows(j)("LATEARRIVAL") > 0 Then
                                            iLateValue = iLateValue + 1
                                        End If
                                    End If

                                    If rsEmp.Tables(0).Rows(i)("EveryInterval").ToString.Trim = "Y" And iLateValue > rsEmp.Tables(0).Rows(i)("LateDays") Then
                                        iLateValue = 0
                                        iDeductValue = rsEmp.Tables(0).Rows(i)("DeductDay")
                                        If rsTime.Tables(0).Rows(j)("PRESENTVALUE") = 1 Then
                                            If iDeductValue >= 1 Then
                                                iFlag = True
                                                Dim leaveamount = 1
                                                Dim LEAVETYPE = mLeaveType
                                                Dim Leavecode = mLeaveCode
                                                Dim reason = "LATE_D"
                                                iDeductValue = iDeductValue - 1

                                                sSql = "update tblTimeRegister set reason='" & reason & "',leaveamount=" & leaveamount & ", Leavecode='" & Leavecode & "', LEAVETYPE='" & LEAVETYPE & "', " &
                                        " where paycode='" & rsEmp.Tables(0).Rows(i)("Paycode").ToString.Trim & "' and DateOffice ='" & Convert.ToDateTime(rsTime.Tables(0).Rows(j)("DATEOFFICE").ToString.Trim).ToString("yyyy-MM-dd") & "'"
                                                If Common.servername = "Access" Then
                                                    sSql = sSql.Replace("and DateOffice", "and FORMAT(DateOffice,'yyyy-MM-dd')")
                                                    If Common.con1.State <> ConnectionState.Open Then
                                                        Common.con1.Open()
                                                    End If
                                                    cmd1 = New OleDbCommand(sSql, Common.con1)
                                                    cmd1.ExecuteNonQuery()
                                                    If Common.con1.State <> ConnectionState.Closed Then
                                                        Common.con1.Close()
                                                    End If
                                                Else
                                                    If Common.con.State <> ConnectionState.Open Then
                                                        Common.con.Open()
                                                    End If
                                                    cmd = New SqlCommand(sSql, Common.con)
                                                    cmd.ExecuteNonQuery()
                                                    If Common.con.State <> ConnectionState.Closed Then
                                                        Common.con.Close()
                                                    End If
                                                End If
                                                sSql = "Select * from tblTimeRegister  where paycode='" & rsEmp.Tables(0).Rows(i)("Paycode").ToString.Trim & "'" &
                                            " and DateOffice ='" & Convert.ToDateTime(rsTime.Tables(0).Rows(j)("DATEOFFICE").ToString.Trim).ToString("yyyy-MM-dd") & "'"
                                                rsTime1 = New DataSet 'ADODB.Recordset
                                                If servername = "Access" Then
                                                    sSql = sSql.Replace("and DateOffice", "and FORMAT(DateOffice,'yyyy-MM-dd')")
                                                    adapAc = New OleDbDataAdapter(sSql, con1)
                                                    adapAc.Fill(rsTime1)
                                                Else
                                                    adapS = New SqlDataAdapter(sSql, con)
                                                    adapS.Fill(rsTime1)
                                                End If
                                                cn.Upd(rsTime1, EmpGpId)

                                                'sSql = "Update tblLeaveLedger Set " & rsLeaveMaster1("LeaveField") & "=" & _
                                                'rsLeaveMaster1("LeaveField") & " +1 " & _
                                                '" Where Paycode='" & rsTime("Paycode") & "' AND LYEAR=" & Year(rsTime("DATEOFFICE"))
                                                sSql = "Update tblLeaveLedger Set " & SerchRows(0)("LeaveField") & "=" &
                                             SerchRows(0)("LeaveField") & " +1 " &
                                            " Where Paycode='" & rsTime1.Tables(0).Rows(0)("Paycode").ToString.Trim & "' AND LYEAR=" & Year(rsTime1.Tables(0).Rows(0)("DATEOFFICE"))
                                                If Common.servername = "Access" Then
                                                    If Common.con1.State <> ConnectionState.Open Then
                                                        Common.con1.Open()
                                                    End If
                                                    cmd1 = New OleDbCommand(sSql, Common.con1)
                                                    cmd1.ExecuteNonQuery()
                                                    If Common.con1.State <> ConnectionState.Closed Then
                                                        Common.con1.Close()
                                                    End If
                                                Else
                                                    If Common.con.State <> ConnectionState.Open Then
                                                        Common.con.Open()
                                                    End If
                                                    cmd = New SqlCommand(sSql, Common.con)
                                                    cmd.ExecuteNonQuery()
                                                    If Common.con.State <> ConnectionState.Closed Then
                                                        Common.con.Close()
                                                    End If
                                                End If
                                                'cn.Execute(sSql)
                                            ElseIf iDeductValue > 0 And iDeductValue < 1 Then
                                                iFlag = True
                                                Dim leaveamount = iDeductValue
                                                Dim LEAVETYPE = mLeaveType
                                                Dim Leavecode = mLeaveCode
                                                Dim leaveamount1 = iDeductValue
                                                Dim FIRSTHALFLEAVECODE = mLeaveCode
                                                Dim reason = "LATE_D"
                                                sSql = "update tblTimeRegister set reason='" & reason & "', Leavecode='" & Leavecode & "', LEAVETYPE='" & LEAVETYPE & "', " &
                                       " FIRSTHALFLEAVECODE='" & FIRSTHALFLEAVECODE & "',leaveamount=" & leaveamount & ", leaveamount1=" & leaveamount1 &
                                       " where paycode='" & rsEmp.Tables(0).Rows(i)("Paycode").ToString.Trim & "' and DateOffice ='" & Convert.ToDateTime(rsTime.Tables(0).Rows(j)("DATEOFFICE").ToString.Trim).ToString("yyyy-MM-dd") & "'"
                                                If Common.servername = "Access" Then
                                                    sSql = sSql.Replace("and DateOffice", "and FORMAT(DateOffice,'yyyy-MM-dd')")
                                                    If Common.con1.State <> ConnectionState.Open Then
                                                        Common.con1.Open()
                                                    End If
                                                    cmd1 = New OleDbCommand(sSql, Common.con1)
                                                    cmd1.ExecuteNonQuery()
                                                    If Common.con1.State <> ConnectionState.Closed Then
                                                        Common.con1.Close()
                                                    End If
                                                Else
                                                    If Common.con.State <> ConnectionState.Open Then
                                                        Common.con.Open()
                                                    End If
                                                    cmd = New SqlCommand(sSql, Common.con)
                                                    cmd.ExecuteNonQuery()
                                                    If Common.con.State <> ConnectionState.Closed Then
                                                        Common.con.Close()
                                                    End If
                                                End If

                                                sSql = "Select * from tblTimeRegister  where paycode='" & rsEmp.Tables(0).Rows(i)("Paycode").ToString.Trim & "'" &
                                                    " and DateOffice ='" & Convert.ToDateTime(rsTime.Tables(0).Rows(j)("DATEOFFICE").ToString.Trim).ToString("yyyy-MM-dd") & "'"
                                                rsTime1 = New DataSet 'ADODB.Recordset
                                                If servername = "Access" Then
                                                    sSql = sSql.Replace("and DateOffice", "and FORMAT(DateOffice,'yyyy-MM-dd')")
                                                    adapAc = New OleDbDataAdapter(sSql, con1)
                                                    adapAc.Fill(rsTime1)
                                                Else
                                                    adapS = New SqlDataAdapter(sSql, con)
                                                    adapS.Fill(rsTime1)
                                                End If

                                                'sSql = "Update tblLeaveLedger Set " & rsLeaveMaster1("LeaveField") & "=" & _
                                                'rsLeaveMaster1("LeaveField") & " + " & Str(iDeductValue) & _
                                                '" Where Paycode='" & rsTime("Paycode") & "' AND LYEAR=" & Year(rsTime("DATEOFFICE"))
                                                'cn.Execute(sSql)
                                                cn.Upd(rsTime1, EmpGpId)

                                                sSql = "Update tblLeaveLedger Set " & SerchRows(0)("LeaveField") & "=" &
                                                SerchRows(0)("LeaveField") & " + " & iDeductValue.ToString.Trim &
                                                " Where Paycode='" & rsTime1.Tables(0).Rows(0)("Paycode").ToString.Trim & "' AND LYEAR=" & Year(rsTime1.Tables(0).Rows(0)("DATEOFFICE"))
                                                If Common.servername = "Access" Then
                                                    If Common.con1.State <> ConnectionState.Open Then
                                                        Common.con1.Open()
                                                    End If
                                                    cmd1 = New OleDbCommand(sSql, Common.con1)
                                                    cmd1.ExecuteNonQuery()
                                                    If Common.con1.State <> ConnectionState.Closed Then
                                                        Common.con1.Close()
                                                    End If
                                                Else
                                                    If Common.con.State <> ConnectionState.Open Then
                                                        Common.con.Open()
                                                    End If
                                                    cmd = New SqlCommand(sSql, Common.con)
                                                    cmd.ExecuteNonQuery()
                                                    If Common.con.State <> ConnectionState.Closed Then
                                                        Common.con.Close()
                                                    End If
                                                End If



                                                iDeductValue = 0
                                            End If
                                        End If
                                    End If
                                    If rsEmp.Tables(0).Rows(i)("EveryInterval").ToString.Trim = "N" And iLateValue > rsEmp.Tables(0).Rows(i)("LateDays") Then
                                        iLateValue = rsEmp.Tables(0).Rows(i)("LateDays")
                                        iDeductValue = rsEmp.Tables(0).Rows(i)("DeductDay")
                                        If iDeductValue >= 1 Then
                                            iFlag = True
                                            Dim leaveamount = 1
                                            Dim LEAVETYPE = mLeaveType
                                            Dim Leavecode = mLeaveCode
                                            Dim reason = "LATE_D"
                                            iDeductValue = iDeductValue - 1
                                            sSql = "update tblTimeRegister set reason='" & reason & "',leaveamount=" & leaveamount & ", Leavecode='" & Leavecode & "', LEAVETYPE='" & LEAVETYPE & "', " &
                                        " where paycode='" & rsEmp.Tables(0).Rows(i)("Paycode").ToString.Trim & "' and DateOffice ='" & Convert.ToDateTime(rsTime.Tables(0).Rows(j)("DATEOFFICE").ToString.Trim).ToString("yyyy-MM-dd") & "'"
                                            If Common.servername = "Access" Then
                                                sSql = sSql.Replace("and DateOffice", "and FORMAT(DateOffice,'yyyy-MM-dd')")
                                                If Common.con1.State <> ConnectionState.Open Then
                                                    Common.con1.Open()
                                                End If
                                                cmd1 = New OleDbCommand(sSql, Common.con1)
                                                cmd1.ExecuteNonQuery()
                                                If Common.con1.State <> ConnectionState.Closed Then
                                                    Common.con1.Close()
                                                End If
                                            Else
                                                If Common.con.State <> ConnectionState.Open Then
                                                    Common.con.Open()
                                                End If
                                                cmd = New SqlCommand(sSql, Common.con)
                                                cmd.ExecuteNonQuery()
                                                If Common.con.State <> ConnectionState.Closed Then
                                                    Common.con.Close()
                                                End If
                                            End If
                                            sSql = "Select * from tblTimeRegister  where paycode='" & rsEmp.Tables(0).Rows(i)("Paycode").ToString.Trim & "'" &
                                        " and DateOffice ='" & Convert.ToDateTime(rsTime.Tables(0).Rows(j)("DATEOFFICE").ToString.Trim).ToString("yyyy-MM-dd") & "'"
                                            rsTime1 = New DataSet 'ADODB.Recordset
                                            If servername = "Access" Then
                                                sSql = sSql.Replace("and DateOffice", "and FORMAT(DateOffice,'yyyy-MM-dd')")
                                                adapAc = New OleDbDataAdapter(sSql, con1)
                                                adapAc.Fill(rsTime1)
                                            Else
                                                adapS = New SqlDataAdapter(sSql, con)
                                                adapS.Fill(rsTime1)
                                            End If
                                            cn.Upd(rsTime1, EmpGpId)

                                            '  sSql = "Update tblLeaveLedger Set " & rsLeaveMaster1("LeaveField") & "=" & _
                                            'rsLeaveMaster1("LeaveField") & " +1 " & _
                                            '" Where Paycode='" & rsTime("Paycode") & "' AND LYEAR=" & Year(rsTime("DATEOFFICE"))
                                            '  cn.Execute(sSql)
                                            sSql = "Update tblLeaveLedger Set " & SerchRows(0)("LeaveField") & "=" &
                                         SerchRows(0)("LeaveField") & " +1 " &
                                        " Where Paycode='" & rsTime1.Tables(0).Rows(0)("Paycode").ToString.Trim & "' AND LYEAR=" & Year(rsTime1.Tables(0).Rows(0)("DATEOFFICE"))
                                            If Common.servername = "Access" Then
                                                If Common.con1.State <> ConnectionState.Open Then
                                                    Common.con1.Open()
                                                End If
                                                cmd1 = New OleDbCommand(sSql, Common.con1)
                                                cmd1.ExecuteNonQuery()
                                                If Common.con1.State <> ConnectionState.Closed Then
                                                    Common.con1.Close()
                                                End If
                                            Else
                                                If Common.con.State <> ConnectionState.Open Then
                                                    Common.con.Open()
                                                End If
                                                cmd = New SqlCommand(sSql, Common.con)
                                                cmd.ExecuteNonQuery()
                                                If Common.con.State <> ConnectionState.Closed Then
                                                    Common.con.Close()
                                                End If
                                            End If

                                        ElseIf iDeductValue > 0 And iDeductValue < 1 Then
                                            iFlag = True
                                            Dim leaveamount = iDeductValue
                                            Dim LEAVETYPE = mLeaveType
                                            Dim Leavecode = mLeaveCode
                                            Dim leaveamount1 = iDeductValue
                                            Dim FIRSTHALFLEAVECODE = Trim(mLeaveCode)
                                            Dim reason = "LATE_D"
                                            sSql = "update tblTimeRegister set reason='" & reason & "', Leavecode='" & Leavecode & "', LEAVETYPE='" & LEAVETYPE & "', " &
                                        " FIRSTHALFLEAVECODE='" & FIRSTHALFLEAVECODE & "',leaveamount=" & leaveamount & ", leaveamount1=" & leaveamount1 &
                                        " where paycode='" & rsEmp.Tables(0).Rows(i)("Paycode").ToString.Trim & "' and DateOffice ='" & Convert.ToDateTime(rsTime.Tables(0).Rows(j)("DATEOFFICE").ToString.Trim).ToString("yyyy-MM-dd") & "'"
                                            If Common.servername = "Access" Then
                                                sSql = sSql.Replace("and DateOffice", "and FORMAT(DateOffice,'yyyy-MM-dd')")
                                                If Common.con1.State <> ConnectionState.Open Then
                                                    Common.con1.Open()
                                                End If
                                                cmd1 = New OleDbCommand(sSql, Common.con1)
                                                cmd1.ExecuteNonQuery()
                                                If Common.con1.State <> ConnectionState.Closed Then
                                                    Common.con1.Close()
                                                End If
                                            Else
                                                If Common.con.State <> ConnectionState.Open Then
                                                    Common.con.Open()
                                                End If
                                                cmd = New SqlCommand(sSql, Common.con)
                                                cmd.ExecuteNonQuery()
                                                If Common.con.State <> ConnectionState.Closed Then
                                                    Common.con.Close()
                                                End If
                                            End If
                                            sSql = "Select * from tblTimeRegister  where paycode='" & rsEmp.Tables(0).Rows(i)("Paycode").ToString.Trim & "'" &
                                                " and DateOffice ='" & Convert.ToDateTime(rsTime.Tables(0).Rows(j)("DATEOFFICE").ToString.Trim).ToString("yyyy-MM-dd") & "'"
                                            rsTime1 = New DataSet 'ADODB.Recordset
                                            If servername = "Access" Then
                                                sSql = sSql.Replace("and DateOffice", "and FORMAT(DateOffice,'yyyy-MM-dd')")
                                                adapAc = New OleDbDataAdapter(sSql, con1)
                                                adapAc.Fill(rsTime1)
                                            Else
                                                adapS = New SqlDataAdapter(sSql, con)
                                                adapS.Fill(rsTime1)
                                            End If
                                            cn.Upd(rsTime1, EmpGpId)
                                            'sSql = "Update tblLeaveLedger Set " & rsLeaveMaster1("LeaveField") & "=" & _
                                            'rsLeaveMaster1("LeaveField") & " + " & Str(iDeductValue) & _
                                            '" Where Paycode='" & rsTime("Paycode") & "' AND LYEAR=" & Year(rsTime("DATEOFFICE"))
                                            'cn.Execute(sSql)

                                            sSql = "Update tblLeaveLedger Set " & SerchRows(0)("LeaveField") & "=" &
                                            SerchRows(0)("LeaveField") & " + " & (iDeductValue.ToString.Trim) &
                                            " Where Paycode='" & rsTime1.Tables(0).Rows(0)("Paycode").ToString.Trim & "' AND LYEAR=" & Year(rsTime1.Tables(0).Rows(0)("DATEOFFICE"))
                                            If Common.servername = "Access" Then
                                                If Common.con1.State <> ConnectionState.Open Then
                                                    Common.con1.Open()
                                                End If
                                                cmd1 = New OleDbCommand(sSql, Common.con1)
                                                cmd1.ExecuteNonQuery()
                                                If Common.con1.State <> ConnectionState.Closed Then
                                                    Common.con1.Close()
                                                End If
                                            Else
                                                If Common.con.State <> ConnectionState.Open Then
                                                    Common.con.Open()
                                                End If
                                                cmd = New SqlCommand(sSql, Common.con)
                                                cmd.ExecuteNonQuery()
                                                If Common.con.State <> ConnectionState.Closed Then
                                                    Common.con.Close()
                                                End If
                                            End If
                                            iDeductValue = 0
                                        End If
                                    End If

                                    'rsTime.MoveNext()
                                    'If rsTime.EOF Then Exit Do
                                    'Ajitesh
                                    If rsEmp.Tables(0).Rows(i)("LateDays") > 0 And rsEmp.Tables(0).Rows(i)("DeductDay") > 0 And rsEmp.Tables(0).Rows(i)("FromLeave").ToString.Trim <> "" Then
                                        'rsLeaveMaster = cn.Execute("select * from tblleavemaster order by leavecode")
                                        sSql = "select * from tblleavemaster order by leavecode"
                                        rsLeaveMaster = New DataSet
                                        If servername = "Access" Then
                                            adapAc = New OleDbDataAdapter(sSql, con1)
                                            adapAc.Fill(rsLeaveMaster)
                                        Else
                                            adapS = New SqlDataAdapter(sSql, con)
                                            adapS.Fill(rsLeaveMaster)
                                        End If
                                        'rsLeaveMaster = Cn.Execute("select * from tblleavemaster order by leavecode")

                                        sSql = "select * from tblleavemaster order by leavefield"
                                        rsLeaveMaster1 = New DataSet
                                        If servername = "Access" Then
                                            adapAc = New OleDbDataAdapter(sSql, con1)
                                            adapAc.Fill(rsLeaveMaster1)
                                        Else
                                            adapS = New SqlDataAdapter(sSql, con)
                                            adapS.Fill(rsLeaveMaster1)
                                        End If
                                        'rsLeaveMaster1 = cn.Execute("select * from tblleavemaster order by leavefield")
                                        'sSql = "leavefield='" & rsEmp("FromLeave") & "'"
                                        'rsLeaveMaster1.Find(sSql)
                                        sSql = "leavefield='" & rsEmp.Tables(0).Rows(i)("FromLeave") & "'"
                                        SerchRows = rsLeaveMaster1.Tables(0).Select(sSql)

                                        sSql = "Select * from tblLeaveLedger Where Paycode='" & rsEmp.Tables(0).Rows(i)("Paycode").ToString.Trim & "' and lyear=" & DateEditVerification.DateTime.ToString("yyyy")
                                        rsLeaveLedger = New DataSet 'cn.Execute(sSql)
                                        If servername = "Access" Then
                                            adapAc = New OleDbDataAdapter(sSql, con1)
                                            adapAc.Fill(rsLeaveLedger)
                                        Else
                                            adapS = New SqlDataAdapter(sSql, con)
                                            adapS.Fill(rsLeaveLedger)
                                        End If

                                        If rsLeaveLedger.Tables(0).Rows.Count = 0 Then
                                            sSql = "Insert into tblleaveledger (paycode,lyear,L01,L02,L03,L04,L05,L06,L07,L08,L09,L10,L11,L12,L13,L14,L15,L16,L17,L18,L19,L20,L01_Add,L02_Add,L03_Add,L04_Add,L05_Add,L06_Add,L07_Add,L08_Add,L09_Add,L10_Add,L11_Add,L12_Add,L13_Add,L14_Add,L15_Add,L16_Add,L17_Add,L18_Add,L19_Add,L20_Add) values('" & rsEmp.Tables(0).Rows(i)("paycode").ToString.Trim & "'," & DateEditVerification.DateTime.ToString("yyyy") & ",0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)"
                                            'cn.Execute(sSql)
                                            If Common.servername = "Access" Then
                                                If Common.con1.State <> ConnectionState.Open Then
                                                    Common.con1.Open()
                                                End If
                                                cmd1 = New OleDbCommand(sSql, Common.con1)
                                                cmd1.ExecuteNonQuery()
                                                If Common.con1.State <> ConnectionState.Closed Then
                                                    Common.con1.Close()
                                                End If
                                            Else
                                                If Common.con.State <> ConnectionState.Open Then
                                                    Common.con.Open()
                                                End If
                                                cmd = New SqlCommand(sSql, Common.con)
                                                cmd.ExecuteNonQuery()
                                                If Common.con.State <> ConnectionState.Closed Then
                                                    Common.con.Close()
                                                End If
                                            End If
                                            sSql = "Select * from tblLeaveLedger Where Paycode='" & rsEmp.Tables(0).Rows(i)("Paycode").ToString.Trim & "' and lyear=" & DateEditVerification.DateTime.ToString("yyyy")
                                            rsLeaveLedger = New DataSet 'cn.Execute(sSql)
                                            If servername = "Access" Then
                                                adapAc = New OleDbDataAdapter(sSql, con1)
                                                adapAc.Fill(rsLeaveLedger)
                                            Else
                                                adapS = New SqlDataAdapter(sSql, con)
                                                adapS.Fill(rsLeaveLedger)
                                            End If

                                        End If
                                        If rsLeaveLedger.Tables(0).Rows.Count > 0 Then
                                            If rsLeaveLedger.Tables(0).Rows(0)(SerchRows(0)("LeaveField") & "_Add" & "") - rsLeaveLedger.Tables(0).Rows(0)(SerchRows(0)("LeaveField") & "") >= rsEmp.Tables(0).Rows(i)("DeductDay") Then
                                                mLeaveCode = SerchRows(0)("Leavecode")
                                                mLeaveType = SerchRows(0)("LEAVETYPE")
                                            Else
                                                'rsLeaveMaster1.MoveFirst()
                                                sSql = "leavefield='" & rsEmp.Tables(0).Rows(i)("FromLeave1") & "'"
                                                SerchRows = rsLeaveMaster1.Tables(0).Select(sSql)
                                                'rsLeaveMaster1.Find(sSql)
                                                If rsLeaveLedger.Tables(0).Rows(0)(SerchRows(0)("LeaveField") & "_Add" & "") - rsLeaveLedger.Tables(0).Rows(0)(SerchRows(0)("LeaveField") & "") >= rsEmp.Tables(0).Rows(i)("DeductDay") Then
                                                    mLeaveCode = SerchRows(0)("Leavecode")
                                                    mLeaveType = SerchRows(0)("LEAVETYPE")
                                                Else
                                                    'rsLeaveMaster1.MoveFirst()
                                                    sSql = "leavefield='" & rsEmp.Tables(0).Rows(i)("FromLeave") & "'"
                                                    SerchRows = rsLeaveMaster1.Tables(0).Select(sSql)
                                                    'rsLeaveMaster1.Find(sSql)
                                                    mLeaveCode = SerchRows(0)("Leavecode")
                                                    mLeaveType = SerchRows(0)("LEAVETYPE")
                                                End If
                                            End If

                                        End If

                                    End If

                                    'Ajitesh

                                Next '  Loop


                                If iFlag = True Then
                                    'rsTime.UpdateBatch()
                                End If

                            End If
                            'rsTime.Close()
                        End If
                    End If
                    'rsEmp.MoveNext()
                Next '  Loop
            End If
        Else
            ''unmark
            If CheckEditWO.Checked = True Then ' If optVerify(0).Value = True Then
                For i As Integer = 0 To rsEmp.Tables(0).Rows.Count - 1 ' Do While Not rsEmp.EOF
                    mCode = rsEmp.Tables(0).Rows(i)("Paycode").ToString.Trim
                    XtraMasterTest.LabelControlStatus.Text = "Verifying for Paycode " & mCode
                    XtraMasterTest.LabelControlCount.Text = "Processing " & i + 1 & " of " & rsEmp.Tables(0).Rows.Count
                    Application.DoEvents()
                    iFlag = True
                    If Common.servername = "Access" Then
                        sSql = " Select Paycode,DateOffice,AbsentValue,PresentValue,Wo_Value,Holiday_Value,Status,LeaveValue,in1," &
                        " ShiftAttended from tblTimeRegister Where paycode='" & rsEmp.Tables(0).Rows(i)("Paycode").ToString.Trim & "' and FORMAT(tblTimeRegister.DateOffice,'yyyy-MM-dd') Between '" & Format(mmin_date, "yyyy-MM-dd") & "' AND '" & Format(mmax_date, "yyyy-MM-dd") & "' Order By DateOffice"
                    Else
                        sSql = " Select Paycode,DateOffice,AbsentValue,PresentValue,Wo_Value,Holiday_Value,Status,LeaveValue,in1," &
                        " ShiftAttended from tblTimeRegister Where paycode='" & rsEmp.Tables(0).Rows(i)("Paycode").ToString.Trim & "' and tblTimeRegister.DateOffice Between '" & Format(mmin_date, "yyyy-MM-dd") & "' AND '" & Format(mmax_date, "yyyy-MM-dd") & "' Order By DateOffice"
                    End If
                    rsTime = New DataSet 'ADODB.Recordset
                    If servername = "Access" Then
                        adapAc = New OleDbDataAdapter(sSql, con1)
                        adapAc.Fill(rsTime)
                    Else
                        adapS = New SqlDataAdapter(sSql, con)
                        adapS.Fill(rsTime)
                    End If

                    'rsTime.Open(sSql, cn, adOpenKeyset, adLockBatchOptimistic)
                    For j As Integer = 0 To rsTime.Tables(0).Rows.Count - 1 'Do Until rsTime.EOF
                        If Trim(rsTime.Tables(0).Rows(j)("Status").ToString) = "A1" Then
                            If iFlag Then
                                iCtr = 0
                                Do While Trim(rsTime.Tables(0).Rows(j)("Status").ToString) = "A1"
                                    'mArr(iCtr) = rsTime.Bookmark
                                    iCtr = iCtr + 1
                                    'rsTime.MoveNext()
                                    'If rsTime.EOF Then
                                    '    Exit Do
                                    'End If
                                    If j = rsTime.Tables(0).Rows.Count - 1 Then
                                        Exit Do
                                    End If
                                Loop
                                'If rsTime("AbsentValue") <> 1 Then
                                Do While iCtr >= 1
                                    'rsTime.Bookmark = mArr(iCtr - 1)
                                    Dim Status
                                    Dim WO_VALUE = 0
                                    Dim HOLIDAY_VALUE = 0
                                    Dim ABSENTVALUE = 0
                                    If rsTime.Tables(0).Rows(j)("ShiftAttended") = "OFF" Then
                                        Status = "WO"
                                        WO_VALUE = 1
                                    Else
                                        Status = "HLD"
                                        HOLIDAY_VALUE = 1
                                    End If
                                    ABSENTVALUE = 0
                                    sSql = "update tblTimeRegister set Status='" & Status & "', WO_VALUE='" & WO_VALUE & "',HOLIDAY_VALUE=" & HOLIDAY_VALUE &
                                        ", ABSENTVALUE=" & ABSENTVALUE & " where paycode='" & rsEmp.Tables(0).Rows(i)("Paycode").ToString.Trim & "' and " &
                                        "DateOffice = '" & Convert.ToDateTime(rsTime.Tables(0).Rows(j)("DATEOFFICE").ToString.Trim).ToString("yyyy-MM-dd") & "' "

                                    If Common.servername = "Access" Then
                                        sSql = sSql.Replace("and DateOffice", "and FORMAT(DateOffice,'yyyy-MM-dd')")
                                        If Common.con1.State <> ConnectionState.Open Then
                                            Common.con1.Open()
                                        End If
                                        cmd1 = New OleDbCommand(sSql, Common.con1)
                                        cmd1.ExecuteNonQuery()
                                        If Common.con1.State <> ConnectionState.Closed Then
                                            Common.con1.Close()
                                        End If
                                    Else
                                        If Common.con.State <> ConnectionState.Open Then
                                            Common.con.Open()
                                        End If
                                        cmd = New SqlCommand(sSql, Common.con)
                                        cmd.ExecuteNonQuery()
                                        If Common.con.State <> ConnectionState.Closed Then
                                            Common.con.Close()
                                        End If
                                    End If
                                    'rsTime.Update()
                                    iCtr = iCtr - 1
                                Loop
                                'End If
                            Else
                                'Do While Trim(rsTime.Tables(0).Rows(j)("Status").ToString) = "A1"
                                If Trim(rsTime.Tables(0).Rows(j)("Status").ToString) = "A1" Then
                                    Dim Status
                                    Dim WO_VALUE = 0
                                    Dim HOLIDAY_VALUE = 0
                                    Dim ABSENTVALUE = 0
                                    If rsTime.Tables(0).Rows(j)("ShiftAttended").ToString = "OFF" Then
                                        Status = "WO"
                                        WO_VALUE = 1
                                        ABSENTVALUE = 0
                                    Else
                                        Status = "HLD"
                                        HOLIDAY_VALUE = 1
                                        ABSENTVALUE = 0
                                    End If
                                    sSql = "update tblTimeRegister set Status='" & Status & "', WO_VALUE='" & WO_VALUE & "',HOLIDAY_VALUE=" & HOLIDAY_VALUE &
                                        ", ABSENTVALUE=" & ABSENTVALUE & " where paycode='" & rsEmp.Tables(0).Rows(i)("Paycode").ToString.Trim & "' and " &
                                        "DateOffice = '" & Convert.ToDateTime(rsTime.Tables(0).Rows(j)("DATEOFFICE").ToString.Trim).ToString("yyyy-MM-dd") & "' "

                                    If Common.servername = "Access" Then
                                        sSql = sSql.Replace("and DateOffice", "and FORMAT(DateOffice,'yyyy-MM-dd')")
                                        If Common.con1.State <> ConnectionState.Open Then
                                            Common.con1.Open()
                                        End If
                                        cmd1 = New OleDbCommand(sSql, Common.con1)
                                        cmd1.ExecuteNonQuery()
                                        If Common.con1.State <> ConnectionState.Closed Then
                                            Common.con1.Close()
                                        End If
                                    Else
                                        If Common.con.State <> ConnectionState.Open Then
                                            Common.con.Open()
                                        End If
                                        cmd = New SqlCommand(sSql, Common.con)
                                        cmd.ExecuteNonQuery()
                                        If Common.con.State <> ConnectionState.Closed Then
                                            Common.con.Close()
                                        End If
                                    End If
                                    'rsTime.Update()
                                    'rsTime.MoveNext()
                                    'If rsTime.EOF Then
                                    '    Exit Do
                                    'End If                                       
                                End If ' Loop
                            End If
                        End If
                        'rsTime.MoveNext()
                        'If rsTime.EOF Then
                        '    Exit Do
                        'End If
                    Next ' Loop
                    'rsTime.UpdateBatch()
                    'rsTime.Close()
                    'rsEmp.MoveNext()
                    'If rsEmp.EOF Then
                    '    Exit Do
                    'End If
                Next ' Loop
            End If
            If CheckEditLate.Checked = True Then 'If optVerify(1).Value = True Then
                sSql = "Select a.*,b.companycode,b.departmentcode,d.*,x.Id from tblemployeeshiftmaster a,tblemployee b,tbldepartment c,tblcatagory d, EmployeeGroup x where b.departmentcode=c.departmentcode and b.cat=d.cat and d.LateVerification='Y' and b.paycode=a.paycode and b.EmployeeGroupId = x.GroupId ORDER BY A.PAYCODE "
                If CheckAll.Checked = True Then
                    'sSql = "Select a.*,b.companycode,b.departmentcode,x.Id from " &
                    '        "tblemployeeshiftmaster a,tblemployee b, EmployeeGroup x " &
                    '        "where b.paycode=a.paycode and a.PAYCODE IN ('" & String.Join("', '", Common.EmpArr) & "')" &
                    '        "and b.EmployeeGroupId = x.GroupId and b.ACTIVE ='Y' ORDER BY A.PAYCODE "
                    sSql = "Select a.*,b.companycode,b.departmentcode,d.*,x.Id from tblemployeeshiftmaster a,tblemployee b,tbldepartment c,tblcatagory d, EmployeeGroup x where b.departmentcode=c.departmentcode and b.cat=d.cat and d.LateVerification='Y' and b.paycode=a.paycode and a.PAYCODE IN ('" & String.Join("', '", Common.EmpArr) & "') and b.EmployeeGroupId = x.GroupId and b.ACTIVE ='Y' ORDER BY A.PAYCODE"
                Else
                    If ComboBoxEdit1.EditValue = "Paycode" Then
                        Dim paycode() As String = PopupContainerEdit1.EditValue.ToString.Split(",")
                        paycode = paycode.Select(Function(s) s.Replace(" ", "")).ToArray()
                        'sSql = "Select a.*,b.companycode,b.departmentcode,x.Id from " &
                        '   "tblemployeeshiftmaster a,tblemployee b, EmployeeGroup x " &
                        '   "where b.paycode=a.paycode and a.PAYCODE IN ('" & String.Join("', '", paycode) & "')" &
                        '   "and b.EmployeeGroupId = x.GroupId and b.ACTIVE ='Y' ORDER BY A.PAYCODE "

                        sSql = "Select a.*,b.companycode,b.departmentcode,d.*,x.Id from tblemployeeshiftmaster a,tblemployee b,tbldepartment c,tblcatagory d, EmployeeGroup x where b.departmentcode=c.departmentcode and b.cat=d.cat and d.LateVerification='Y' and b.paycode=a.paycode and a.PAYCODE IN ('" & String.Join("', '", paycode) & "') and b.EmployeeGroupId = x.GroupId and b.ACTIVE ='Y' ORDER BY A.PAYCODE"
                    ElseIf ComboBoxEdit1.EditValue = "Company" Then
                        Dim com() As String = PopupContainerEdit1.EditValue.ToString.Split(",")
                        'com = com.Select(Function(s) s.Replace(" ", "")).ToArray()
                        Dim ls As New List(Of String)()
                        For i As Integer = 0 To com.Length - 1
                            ls.Add(com(i).Trim)
                        Next
                        'sSql = "Select a.*,b.companycode,b.departmentcode,x.Id from " &
                        '  "tblemployeeshiftmaster a,tblemployee b, EmployeeGroup x " &
                        '  "where b.paycode=a.paycode and b.companycode IN ('" & String.Join("', '", ls.ToArray()) & "')" &
                        '  "and b.EmployeeGroupId = x.GroupId and b.ACTIVE ='Y' ORDER BY A.PAYCODE "
                        sSql = "Select a.*,b.companycode,b.departmentcode,d.*,x.Id from tblemployeeshiftmaster a,tblemployee b,tbldepartment c,tblcatagory d, EmployeeGroup x where b.departmentcode=c.departmentcode and b.cat=d.cat and d.LateVerification='Y' and b.paycode=a.paycode and b.companycode IN ('" & String.Join("', '", ls.ToArray()) & "') and b.EmployeeGroupId = x.GroupId and b.ACTIVE ='Y' ORDER BY A.PAYCODE"
                    ElseIf ComboBoxEdit1.EditValue = "Department" Then
                        Dim com() As String = PopupContainerEdit1.EditValue.ToString.Split(",")
                        'com = com.Select(Function(s) s.Replace(" ", "")).ToArray()
                        Dim ls As New List(Of String)()
                        For i As Integer = 0 To com.Length - 1
                            ls.Add(com(i).Trim)
                        Next
                        'sSql = "Select a.*,b.companycode,b.departmentcode,x.Id from " &
                        '  "tblemployeeshiftmaster a,tblemployee b, EmployeeGroup x " &
                        '  "where b.paycode=a.paycode and b.DEPARTMENTCODE IN ('" & String.Join("', '", ls.ToArray()) & "')" &
                        '  "and b.EmployeeGroupId = x.GroupId and b.ACTIVE ='Y' ORDER BY A.PAYCODE "
                        sSql = "Select a.*,b.companycode,b.departmentcode,d.*,x.Id from tblemployeeshiftmaster a,tblemployee b,tbldepartment c,tblcatagory d, EmployeeGroup x where b.departmentcode=c.departmentcode and b.cat=d.cat and d.LateVerification='Y' and b.paycode=a.paycode and b.DEPARTMENTCODE IN ('" & String.Join("', '", ls.ToArray()) & "') and b.EmployeeGroupId = x.GroupId and b.ACTIVE ='Y' ORDER BY A.PAYCODE"

                    ElseIf ComboBoxEdit1.EditValue = "Location" Then
                        Dim com() As String = PopupContainerEdit1.EditValue.ToString.Split(",")
                        Dim ls As New List(Of String)()
                        For i As Integer = 0 To com.Length - 1
                            ls.Add(com(i).Trim)
                        Next
                        'sSql = "Select a.*,b.companycode,b.departmentcode,x.Id from " &
                        '  "tblemployeeshiftmaster a,tblemployee b, EmployeeGroup x " &
                        '  "where b.paycode=a.paycode and b.BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "')" &
                        '  "and b.EmployeeGroupId = x.GroupId and b.ACTIVE ='Y' ORDER BY A.PAYCODE "
                        sSql = "Select a.*,b.companycode,b.departmentcode,d.*,x.Id from tblemployeeshiftmaster a,tblemployee b,tbldepartment c,tblcatagory d, EmployeeGroup x where b.departmentcode=c.departmentcode and b.cat=d.cat and d.LateVerification='Y' and b.paycode=a.paycode and b.BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "') and b.EmployeeGroupId = x.GroupId and b.ACTIVE ='Y' ORDER BY A.PAYCODE"
                    End If
                End If

                rsEmp = New DataSet 'ADODB.Recordset
                If servername = "Access" Then
                    adapAc = New OleDbDataAdapter(sSql, con1)
                    adapAc.Fill(rsEmp)
                Else
                    adapS = New SqlDataAdapter(sSql, con)
                    adapS.Fill(rsEmp)
                End If
                If rsEmp.Tables(0).Rows.Count > 0 Then
                    'mmin_date = Format("01/" & Format(dtpProcDate.Value, "Mmm/Yyyy"), "dd/Mmm/Yyyy")                  
                    mmin_date = Convert.ToDateTime("01/" & DateEditVerification.DateTime.ToString("MM/yyyy")) 'Format("01/" & Format(dtpProcDate.Value, "Mmm/Yyyy"), "dd/Mmm/Yyyy")
                    mmax_date = DateAdd("m", 1, mmin_date)
                    mmax_date = DateAdd("d", -1, mmax_date)
                    mmax_date = Format(mmax_date, "dd/MM/yyyy")

                    'rsLeaveMaster = cn.Execute("select * from tblleavemaster order by leavecode")
                    'rsLeaveMaster1 = cn.Execute("select * from tblleavemaster order by leavefield")
                    sSql = "select * from tblleavemaster order by leavecode"
                    rsLeaveMaster = New DataSet
                    If servername = "Access" Then
                        adapAc = New OleDbDataAdapter(sSql, con1)
                        adapAc.Fill(rsLeaveMaster)
                    Else
                        adapS = New SqlDataAdapter(sSql, con)
                        adapS.Fill(rsLeaveMaster)
                    End If
                    'rsLeaveMaster = Cn.Execute("select * from tblleavemaster order by leavecode")

                    sSql = "select * from tblleavemaster order by leavefield"
                    rsLeaveMaster1 = New DataSet
                    If servername = "Access" Then
                        adapAc = New OleDbDataAdapter(sSql, con1)
                        adapAc.Fill(rsLeaveMaster1)
                    Else
                        adapS = New SqlDataAdapter(sSql, con)
                        adapS.Fill(rsLeaveMaster1)
                    End If

                    'sSql = "leavefield='" & rsEmp("FromLeave") & "'"
                    'rsLeaveMaster1.Find(sSql)
                    sSql = "leavefield='" & rsEmp.Tables(0).Rows(0)("FromLeave") & "'"
                    Dim SerchRows() As Data.DataRow = rsLeaveMaster1.Tables(0).Select(sSql)
                    Dim EmpGpId As Integer
                    For i As Integer = 0 To rsEmp.Tables(0).Rows.Count - 1 ' Do While Not rsEmp.EOF
                        EmpGpId = rsEmp.Tables(0).Rows(i)("Id")
                        mCode = rsEmp.Tables(0).Rows(i)("Paycode").ToString.Trim
                        XtraMasterTest.LabelControlStatus.Text = "Verifying for Paycode " & mCode
                        Application.DoEvents()
                        If Common.servername = "Access" Then
                            sSql = " Select * from tblTimeRegister Where REASON='LATE_D' AND paycode='" & rsEmp.Tables(0).Rows(i)("Paycode").ToString.Trim & "' and FORMAT(tblTimeRegister.DateOffice,'yyyy-MM-dd') Between '" & Format(mmin_date, "yyyy-MM-dd") & "' AND '" & Format(mmax_date, "yyyy-MM-dd") & "' Order By DateOffice"
                        Else
                            sSql = " Select * from tblTimeRegister Where REASON='LATE_D' AND paycode='" & rsEmp.Tables(0).Rows(i)("Paycode").ToString.Trim & "' and tblTimeRegister.DateOffice Between '" & Format(mmin_date, "yyyy-MM-dd") & "' AND '" & Format(mmax_date, "yyyy-MM-dd") & "' Order By DateOffice"
                        End If
                        rsTime = New DataSet    'ADODB.Recordset
                        If servername = "Access" Then
                            adapAc = New OleDbDataAdapter(sSql, con1)
                            adapAc.Fill(rsTime)
                        Else
                            adapS = New SqlDataAdapter(sSql, con)
                            adapS.Fill(rsTime)
                        End If

                        'rsTime.Open(sSql, cn, adOpenKeyset, adLockBatchOptimistic)
                        If rsTime.Tables(0).Rows.Count > 0 Then
                            'rsTime.MoveFirst()
                            For j As Integer = 0 To rsTime.Tables(0).Rows.Count - 1 ' Do Until rsTime.EOF
                                Dim leaveamountT As Double = 0
                                Try
                                    leaveamountT = Convert.ToDouble(rsTime.Tables(0).Rows(j)("leaveamount").ToString.Trim)
                                Catch ex As Exception
                                End Try
                                If leaveamountT > 0 Then
                                    If rsTime.Tables(0).Rows(j)("FIRSTHALFLEAVECODE").ToString.Trim <> "" Or rsTime.Tables(0).Rows(j)("SECONDHALFLEAVECODE").ToString.Trim <> "" Then
                                        If rsTime.Tables(0).Rows(j)("FIRSTHALFLEAVECODE").ToString.Trim <> "" Then
                                            sSql = "leavecode='" & rsTime.Tables(0).Rows(j)("FIRSTHALFLEAVECODE") & "'"
                                            'rsLeaveMaster.Find(sSql)
                                            SerchRows = rsLeaveMaster.Tables(0).Select(sSql)
                                            sSql = "Update tblLeaveLedger Set " & SerchRows(0)("LeaveField") & "=" &
                                            SerchRows(0)("LeaveField") & " - " & rsTime.Tables(0).Rows(j)("Leaveamount1") &
                                            " Where Paycode='" & rsTime.Tables(0).Rows(j)("Paycode").ToString.Trim & "' AND LYEAR=" & Year(rsTime.Tables(0).Rows(j)("DATEOFFICE"))
                                            'cn.Execute(sSql)
                                            If Common.servername = "Access" Then
                                                If Common.con1.State <> ConnectionState.Open Then
                                                    Common.con1.Open()
                                                End If
                                                cmd1 = New OleDbCommand(sSql, Common.con1)
                                                cmd1.ExecuteNonQuery()
                                                If Common.con1.State <> ConnectionState.Closed Then
                                                    Common.con1.Close()
                                                End If
                                            Else
                                                If Common.con.State <> ConnectionState.Open Then
                                                    Common.con.Open()
                                                End If
                                                cmd = New SqlCommand(sSql, Common.con)
                                                cmd.ExecuteNonQuery()
                                                If Common.con.State <> ConnectionState.Closed Then
                                                    Common.con.Close()
                                                End If
                                            End If
                                        End If
                                        If rsTime.Tables(0).Rows(j)("SECONDHALFLEAVECODE").ToString.Trim <> "" Then
                                            sSql = "leavecode='" & rsTime.Tables(0).Rows(j)("SECONDHALFLEAVECODE") & "'"
                                            'rsLeaveMaster.Find(sSql)
                                            SerchRows = rsLeaveMaster.Tables(0).Select(sSql)
                                            sSql = "Update tblLeaveLedger Set " & SerchRows(0)("LeaveField") & "=" &
                                            SerchRows(0)("LeaveField") & " - " & (rsTime.Tables(0).Rows(j)("Leaveamount2").ToString.Trim) &
                                            " Where Paycode='" & rsTime.Tables(0).Rows(j)("Paycode").ToString.Trim & "' AND LYEAR=" & Year(rsTime.Tables(0).Rows(j)("DATEOFFICE"))
                                            'cn.Execute(sSql)
                                            If Common.servername = "Access" Then
                                                If Common.con1.State <> ConnectionState.Open Then
                                                    Common.con1.Open()
                                                End If
                                                cmd1 = New OleDbCommand(sSql, Common.con1)
                                                cmd1.ExecuteNonQuery()
                                                If Common.con1.State <> ConnectionState.Closed Then
                                                    Common.con1.Close()
                                                End If
                                            Else
                                                If Common.con.State <> ConnectionState.Open Then
                                                    Common.con.Open()
                                                End If
                                                cmd = New SqlCommand(sSql, Common.con)
                                                cmd.ExecuteNonQuery()
                                                If Common.con.State <> ConnectionState.Closed Then
                                                    Common.con.Close()
                                                End If
                                            End If
                                        End If
                                    ElseIf rsTime.Tables(0).Rows(j)("LEAVECODE").ToString.Trim <> "" Then
                                        sSql = "leavecode='" & rsTime.Tables(0).Rows(j)("Leavecode") & "'"
                                        'rsLeaveMaster.Find(sSql)
                                        SerchRows = rsLeaveMaster.Tables(0).Select(sSql)
                                        sSql = "Update tblLeaveLedger Set " & SerchRows(0)("LeaveField") & "=" &
                                        SerchRows(0)("LeaveField") & " - " & (rsTime.Tables(0).Rows(j)("Leaveamount").ToString.Trim) &
                                        " Where Paycode='" & rsTime.Tables(0).Rows(j)("Paycode").ToString.Trim & "' AND LYEAR=" & Year(rsTime.Tables(0).Rows(j)("DATEOFFICE"))
                                        'cn.Execute(sSql)
                                        If Common.servername = "Access" Then
                                            If Common.con1.State <> ConnectionState.Open Then
                                                Common.con1.Open()
                                            End If
                                            cmd1 = New OleDbCommand(sSql, Common.con1)
                                            cmd1.ExecuteNonQuery()
                                            If Common.con1.State <> ConnectionState.Closed Then
                                                Common.con1.Close()
                                            End If
                                        Else
                                            If Common.con.State <> ConnectionState.Open Then
                                                Common.con.Open()
                                            End If
                                            cmd = New SqlCommand(sSql, Common.con)
                                            cmd.ExecuteNonQuery()
                                            If Common.con.State <> ConnectionState.Closed Then
                                                Common.con.Close()
                                            End If
                                        End If
                                    End If
                                End If
                                Dim reason = ""
                                Dim Leavecode = ""
                                Dim LEAVETYPE = ""
                                Dim Voucher_No = ""
                                Dim LEAVETYPE1 = ""
                                Dim LEAVETYPE2 = ""
                                Dim FIRSTHALFLEAVECODE = ""
                                Dim SECONDHALFLEAVECODE = ""
                                Dim leaveamount = 0
                                Dim leaveamount1 = 0
                                Dim leaveamount2 = 0

                                sSql = "update tblTimeRegister set reason='" & reason & "', Leavecode='" & Leavecode & "', LEAVETYPE='" & LEAVETYPE & "', " &
                                            "Voucher_No='" & Voucher_No & "', LEAVETYPE1='" & LEAVETYPE1 & "', LEAVETYPE2='" & LEAVETYPE2 & "', FIRSTHALFLEAVECODE='" & FIRSTHALFLEAVECODE & "'" &
                                            ",SECONDHALFLEAVECODE='" & SECONDHALFLEAVECODE & "',leaveamount=" & leaveamount & ", leaveamount1=" & leaveamount1 & ", leaveamount2=" & leaveamount2 &
                                            " where paycode='" & rsEmp.Tables(0).Rows(i)("Paycode").ToString.Trim & "' and DateOffice ='" & Convert.ToDateTime(rsTime.Tables(0).Rows(j)("DATEOFFICE").ToString.Trim).ToString("yyyy-MM-dd") & "'"
                                If Common.servername = "Access" Then
                                    sSql = sSql.Replace("and DateOffice", "and FORMAT(DateOffice,'yyyy-MM-dd')")
                                    If Common.con1.State <> ConnectionState.Open Then
                                        Common.con1.Open()
                                    End If
                                    cmd1 = New OleDbCommand(sSql, Common.con1)
                                    cmd1.ExecuteNonQuery()
                                    If Common.con1.State <> ConnectionState.Closed Then
                                        Common.con1.Close()
                                    End If
                                Else
                                    If Common.con.State <> ConnectionState.Open Then
                                        Common.con.Open()
                                    End If
                                    cmd = New SqlCommand(sSql, Common.con)
                                    cmd.ExecuteNonQuery()
                                    If Common.con.State <> ConnectionState.Closed Then
                                        Common.con.Close()
                                    End If
                                End If
                                sSql = "Select * from tblTimeRegister  where paycode='" & rsEmp.Tables(0).Rows(i)("Paycode").ToString.Trim & "'" &
                                    " and DateOffice ='" & Convert.ToDateTime(rsTime.Tables(0).Rows(j)("DATEOFFICE").ToString.Trim).ToString("yyyy-MM-dd") & "'"

                                rsTime1 = New DataSet 'ADODB.Recordset
                                If servername = "Access" Then
                                    sSql = sSql.Replace("and DateOffice", "and FORMAT(DateOffice,'yyyy-MM-dd')")
                                    adapAc = New OleDbDataAdapter(sSql, con1)
                                    adapAc.Fill(rsTime1)
                                Else
                                    adapS = New SqlDataAdapter(sSql, con)
                                    adapS.Fill(rsTime1)
                                End If

                                cn.Upd(rsTime1, EmpGpId)

                                'Upd(rsTime)
                                'rsTime.MoveNext()
                                'If rsTime.EOF Then
                                '    Exit Do
                                'End If
                            Next ' Loop
                            'rsTime.UpdateBatch()
                            'rsTime.Close()
                        End If
                        'rsEmp.MoveNext()
                    Next ' Loop
                End If
            End If
        End If
        'End If
        XtraMasterTest.LabelControlCount.Text = ""
        XtraMasterTest.LabelControlStatus.Text = ""
        Application.DoEvents()
        SplashScreenManager.CloseForm(False)
        Me.Cursor = Cursors.Default
        XtraMessageBox.Show(ulf, "<size=10>Processed Successfully</size>", "Success")
    End Sub
    'Private Sub SimpleButtonVerification_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonVerification.Click

    '    DevExpress.XtraSplashScreen.SplashScreenManager.ShowForm(Me, GetType(WaitForm1), True, True, False)
    '    Me.Cursor = Cursors.WaitCursor

    '    If Common.IsNepali = "Y" Then
    '        Dim DC As New DateConverter()
    '        Try
    '            DateEditVerification.DateTime = DC.ToAD(New Date(ComboNepaliYearVal.EditValue, ComboNEpaliMonthVal.SelectedIndex + 1, 1))
    '        Catch ex As Exception
    '            XtraMessageBox.Show(ulf, "<size=10>Invalid Date</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
    '            ComboNEpaliMonthVal.Select()
    '            Exit Sub
    '        End Try
    '    End If

    '    Dim sSql1 As String = "select PAYCODE from TblEmployee where  ACTIVE = 'Y'"
    '    'sSql1 = "select TblEmployee.PAYCODE,TblEmployee.dateofjoin, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK from TblEmployee, tblEmployeeShiftMaster where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.COMPANYCODE IN ('" & String.Join("', '", ls.ToArray()) & "')  and TblEmployee.ACTIVE = 'Y'"
    '    Dim adap As SqlDataAdapter
    '    Dim adapA As OleDbDataAdapter
    '    Dim ds As DataSet = New DataSet
    '    If Common.servername = "Access" Then
    '        adapA = New OleDbDataAdapter(sSql1, Common.con1)
    '        adapA.Fill(ds)
    '    Else
    '        adap = New SqlDataAdapter(sSql1, Common.con)
    '        adap.Fill(ds)
    '    End If
    '    For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
    '        XtraMaster.LabelControl4.Text = "Verifing for paycode " & ds.Tables(0).Rows(i).Item("Paycode").ToString.Trim
    '        Application.DoEvents()
    '    Next
    '    SplashScreenManager.CloseForm(False)
    '    Me.Cursor = Cursors.Default
    '    XtraMaster.LabelControl4.Text = ""
    '    XtraMessageBox.Show(ulf, "<size=10>Verified Successfully</size>", "Success")
    'End Sub
    Private Sub TextEditUploadLogs_Click(sender As System.Object, e As System.EventArgs) Handles TextEditUploadLogs.Click
        Dim dlg As New OpenFileDialog()
        ' Filter by Office Files
        dlg.Filter = "Office Files|*.txt;*.dat;*.csv;"
        dlg.ShowDialog()
        TextEditUploadLogs.Text = dlg.FileName
    End Sub
End Class
