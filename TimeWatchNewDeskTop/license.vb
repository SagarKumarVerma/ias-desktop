﻿Imports System.IO
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports System.Text.RegularExpressions
Imports DevExpress.XtraEditors

Public Class license
    Public Shared HDDSerialNo As String
    Public Shared InstallDate As DateTime
    Public Shared NoOfUsers As Double
    Public Shared NoOfDevices As Double
    Public Shared TrialPerioad As Boolean
    Public Shared TrialExpired As Boolean
    Public Shared LicenseKey As String
    Public Shared DeviceSerialNo() As String = New String() {}
    Public Shared InstalledCompName As String = ""
    Public Shared Function GetHddSerialNo() As Boolean
        Dim objMan As New Management.ManagementClass("Win32_PhysicalMedia")
        Dim objCol As Management.ManagementObjectCollection = objMan.GetInstances
        Dim objItem As Management.ManagementObject
        For Each objItem In objCol
            Dim pi As Management.PropertyDataCollection = objItem.Properties
            HDDSerialNo = Convert.ToString(pi.Item("SerialNumber").Value).ToString.Trim
            If HDDSerialNo <> "" Then
                Return True
            End If
        Next
        'MsgBox(HDDSerialNo)
        If HDDSerialNo.Trim = "" Then
            HDDSerialNo = "1234567890"
            Return True
        End If
        Return False
    End Function
    Public Shared Sub checkLicenseFile()
        GetHddSerialNo()
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim ds As DataSet = New DataSet
        Dim cmd As New SqlCommand
        Dim cmd1 As New OleDbCommand
        If Not System.IO.File.Exists("License.lic") Then
            File.Create("License.lic").Dispose()
            Dim sSql As String = "select * from iASSystemInfo"
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
            If ds.Tables(0).Rows.Count = 0 Then 'for new installation
                Dim ret As Boolean = GetHddSerialNo()
                If ret Then
                    Dim insertiASSystemInfo As String
                    Dim fs As FileStream = New FileStream("License.lic", FileMode.Create, FileAccess.Write)
                    Dim sw As StreamWriter = New StreamWriter(fs)
                    Dim str As String = ""
                    str = EncyDcry.Encrypt(HDDSerialNo, True)
                    sw.WriteLine(str)

                    ds = New DataSet
                    sSql = "select InstalledDate from InstallSystemInfo "
                    ds = New DataSet
                    If Common.servername = "Access" Then
                        adapA = New OleDbDataAdapter(sSql, Common.con1)
                        adapA.Fill(ds)
                    Else
                        adap = New SqlDataAdapter(sSql, Common.con)
                        adap.Fill(ds)
                    End If

                    Dim InstalledDate As String = EncyDcry.Encrypt(Convert.ToDateTime(ds.Tables(0).Rows(0)(0).ToString.Trim).ToString("yyyy-MM-dd HH:mm:ss"), True)
                    sw.WriteLine(InstalledDate)

                    str = EncyDcry.Encrypt("500", True)
                    sw.WriteLine(str)

                    str = EncyDcry.Encrypt("5", True)
                    sw.WriteLine(str)

                    insertiASSystemInfo = "insert into iASSystemInfo (HSerialNo , ISD , NOU ,NOD ) values " &
                        "('" & EncyDcry.Encrypt(HDDSerialNo, True) & "', '" & InstalledDate & "','" & EncyDcry.Encrypt("500", True) & "','" & EncyDcry.Encrypt("5", True) & "')"

                    'check if application in installed today then it is trial perioad
                    If Convert.ToDateTime(ds.Tables(0).Rows(0)(0).ToString.Trim).ToString("yyyy-MM-dd") < Now.ToString("yyyy-MM-dd") Then
                        Dim UserKeyNum As Double = Num(HDDSerialNo)
                        Dim LicenseNum As Double = (UserKeyNum * 9999) + Convert.ToDouble(1234567890) * 12345
                        Dim licenseStr As String = Regex.Replace(LicenseNum.ToString, ".{5}", "$0-").TrimEnd(CChar("-"))
                        Dim lStrTmp() As String = licenseStr.Split("-")
                        Dim FullPayLicence As String = lStrTmp(0) & "P-" & lStrTmp(1) & "Y-" & lStrTmp(2) & "E"
                        Dim LitePayLicence As String = lStrTmp(0) & "P-" & lStrTmp(1) & "N-" & lStrTmp(2) & "O"
                        sw.WriteLine(LitePayLicence)

                        sSql = "select MAC_ADDRESS from tblmachine where MAC_ADDRESS<>''"
                        ds = New DataSet
                        If Common.servername = "Access" Then
                            adapA = New OleDbDataAdapter(sSql, Common.con1)
                            adapA.Fill(ds)
                        Else
                            adap = New SqlDataAdapter(sSql, Common.con)
                            adap.Fill(ds)
                        End If

                        Dim SerialNo As String = ""
                        Dim SerialNoEnc As String = ""
                        For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                            If ds.Tables(0).Rows(i)(0).ToString.Trim <> "" Then
                                SerialNoEnc = SerialNoEnc & "," & EncyDcry.Encrypt(ds.Tables(0).Rows(i)(0).ToString.Trim, True)
                            End If
                        Next
                        SerialNoEnc = SerialNoEnc.TrimStart(",")
                        sw.Write(SerialNoEnc) 'do not use writeline
                        TrialPerioad = False

                        insertiASSystemInfo = "insert into iASSystemInfo (HSerialNo,ISD,NOU,NOD,Lic,DSerialNo) values " &
                        "('" & EncyDcry.Encrypt(HDDSerialNo, True) & "', '" & InstalledDate & "','" & EncyDcry.Encrypt("500", True) & "', " &
                        "'" & EncyDcry.Encrypt("5", True) & "','" & LitePayLicence & "','" & SerialNoEnc & "')"


                    ElseIf Convert.ToDateTime(ds.Tables(0).Rows(0)(0).ToString.Trim).ToString("yyyy-MM-dd") = Now.ToString("yyyy-MM-dd") Then
                        TrialPerioad = True
                    End If
                    sw.Flush()
                    sw.Close()
                    fs.Close()

                    If Common.servername = "Access" Then
                        If Common.con1.State <> ConnectionState.Open Then
                            Common.con1.Open()
                        End If
                        cmd1 = New OleDbCommand(insertiASSystemInfo, Common.con1)
                        cmd1.ExecuteNonQuery()
                        If Common.con1.State <> ConnectionState.Closed Then
                            Common.con1.Close()
                        End If
                    Else
                        If Common.con.State <> ConnectionState.Open Then
                            Common.con.Open()
                        End If
                        cmd = New SqlCommand(insertiASSystemInfo, Common.con)
                        cmd.ExecuteNonQuery()
                        If Common.con.State <> ConnectionState.Open Then
                            Common.con.Close()
                        End If
                    End If

                End If
            Else 'old installation in case any one deleted License.lic
                Dim fs As FileStream = New FileStream("License.lic", FileMode.Create, FileAccess.Write)
                Dim sw As StreamWriter = New StreamWriter(fs)
                sw.WriteLine(ds.Tables(0).Rows(0)(0).ToString.Trim)
                sw.WriteLine(ds.Tables(0).Rows(0)(1).ToString.Trim)
                sw.WriteLine(ds.Tables(0).Rows(0)(2).ToString.Trim)
                sw.WriteLine(ds.Tables(0).Rows(0)(3).ToString.Trim)
                If ds.Tables(0).Rows(0)(4).ToString.Trim <> "" Then
                    sw.WriteLine(ds.Tables(0).Rows(0)(4).ToString.Trim)
                    If ds.Tables(0).Rows(0)(5).ToString.Trim <> "" Then
                        sw.Write(ds.Tables(0).Rows(0)(5).ToString.Trim)
                    End If
                End If
                sw.Flush()
                sw.Close()
                fs.Close()
            End If
        Else  'if any changes in License.lic then update DB (used for offline device serial no update)
            Dim sSql As String = "select * from iASSystemInfo"
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If

            If ds.Tables(0).Rows.Count > 0 Then
                Dim fs As FileStream = New FileStream("License.lic", FileMode.Open, FileAccess.Read)
                Dim sr As StreamReader = New StreamReader(fs)
                Dim str() As String
                'Dim str1 As String
                Dim list As New List(Of String)()
                While (sr.Peek <> -1)
                    list.Add(sr.ReadLine)
                End While
                sr.Close()
                fs.Close()
                str = list.ToArray
                If str(0) = ds.Tables(0).Rows(0)(0).ToString.Trim And str(1) = ds.Tables(0).Rows(0)(1).ToString.Trim Then
                    If str.Length > 4 Then
                        If str(4).Trim <> "" Then
                            Dim goAhedWithLis As Boolean = False

                            Dim UserKeyNum As Double = Num(HDDSerialNo)
                            Dim LicenseNum As Double = (UserKeyNum * 9999) + Convert.ToDouble(1234567890) * 12345
                            Dim licenseStr As String = Regex.Replace(LicenseNum.ToString, ".{5}", "$0-").TrimEnd(CChar("-"))
                            Dim lStrTmp() As String = licenseStr.Split("-")
                            Dim FullPayLicence As String = lStrTmp(0) & "P-" & lStrTmp(1) & "Y-" & lStrTmp(2) & "E"
                            Dim LitePayLicence As String = lStrTmp(0) & "P-" & lStrTmp(1) & "N-" & lStrTmp(2) & "O"

                            Dim SfrmFile() As String = str(4).Trim.Split("-")
                            If str(4).Trim <> "" Then
                                If (SfrmFile(0)) = (lStrTmp(0) & "P") Then
                                    If (SfrmFile(1)) = (lStrTmp(1) & "Y") And (SfrmFile(2)) = (lStrTmp(2) & "C") Then
                                        goAhedWithLis = True
                                    ElseIf (SfrmFile(1)) = (lStrTmp(1) & "Y") And (SfrmFile(2)) = (lStrTmp(2) & "O") Then
                                        goAhedWithLis = True
                                    ElseIf (SfrmFile(1)) = (lStrTmp(1) & "N") And (SfrmFile(2)) = (lStrTmp(2) & "C") Then
                                        goAhedWithLis = True
                                    ElseIf (SfrmFile(1)) = (lStrTmp(1) & "N") And (SfrmFile(2)) = (lStrTmp(2) & "O") Then
                                        goAhedWithLis = True
                                    Else
                                        If Common.ApplicationType = "S" Then
                                            XtraMessageBox.Show(XtraMasterTest.ulf, "<size=10>Incorrect License, Application will Close Now.</size>", "<size=9>Error</size>")
                                            Application.Exit()
                                        End If
                                    End If
                                Else
                                    goAhedWithLis = False
                                    If Common.ApplicationType = "C" Then
                                        XtraMessageBox.Show(XtraMasterTest.ulf, "<size=10>Incorrect License, Application will Close Now.</size>", "<size=9>Error</size>")
                                        Application.Exit()
                                    End If
                                End If
                            End If

                            If goAhedWithLis = True Then
                                If str.Length > 5 Then
                                    If str(5).Trim <> ds.Tables(0).Rows(0)(5).ToString.Trim Then
                                        sSql = "update iASSystemInfo set DSerialNo='" & str(5) & "', Lic='" & str(4).Trim & "'"
                                        If Common.servername = "Access" Then
                                            If Common.con1.State <> ConnectionState.Open Then
                                                Common.con1.Open()
                                            End If
                                            cmd1 = New OleDbCommand(sSql, Common.con1)
                                            cmd1.ExecuteNonQuery()
                                            If Common.con1.State <> ConnectionState.Closed Then
                                                Common.con1.Close()
                                            End If
                                        Else
                                            If Common.con.State <> ConnectionState.Open Then
                                                Common.con.Open()
                                            End If
                                            cmd = New SqlCommand(sSql, Common.con)
                                            cmd.ExecuteNonQuery()
                                            If Common.con.State <> ConnectionState.Open Then
                                                Common.con.Close()
                                            End If
                                        End If
                                    End If
                                End If
                            Else
                                'messagebox
                                If Common.ApplicationType = "S" Then
                                    XtraMessageBox.Show(XtraMasterTest.ulf, "<size=10>Incorrect License, Application will Close Now.</size>", "<size=9>Error</size>")
                                    Application.Exit()
                                End If
                            End If
                        End If
                    End If
                Else

                End If
                InstallDate = Convert.ToDateTime(EncyDcry.Decrypt(str(1), True))
                NoOfUsers = EncyDcry.Decrypt(str(2), True)
                NoOfDevices = EncyDcry.Decrypt(str(3), True)

                If str.Length <= 4 Then
                    TrialPerioad = True
                    LicenseKey = ""
                Else
                    TrialPerioad = False
                    'get license and device serial no
                    LicenseKey = str(4)
                    If LicenseKey = "" Then
                        TrialPerioad = True
                        LicenseKey = ""
                    End If
                    If str.Length = 6 Then
                        Dim srtmp() As String = str(5).Split(",")
                        Dim ls As New List(Of String)()
                        For i As Integer = 0 To srtmp.Length - 1
                            ls.Add(EncyDcry.Decrypt(srtmp(i), True))
                        Next
                        DeviceSerialNo = ls.Distinct.ToArray
                    End If
                End If

                If TrialPerioad = True And InstallDate.ToString("yyyy-MM-dd") < Now.AddDays(-30).ToString("yyyy-MM-dd") Then
                    TrialExpired = True
                Else
                    TrialExpired = False
                End If
            End If
        End If
    End Sub
    Private Shared Function Num(ByVal value As String) As Double
        Try
            If value.Length > 12 Then
                value = value.Substring(0, 12)
            End If
            Dim returnVal As String = String.Empty
            Dim collection As MatchCollection = Regex.Matches(value, "\d+")
            For Each m As Match In collection
                returnVal += m.ToString()
            Next
            Return Convert.ToDouble(returnVal)
        Catch ex As Exception
            Return 0
        End Try
    End Function
    Public Shared Sub getLicenseInfo()
        Dim fs As FileStream = New FileStream("License.lic", FileMode.Open, FileAccess.Read)
        Dim sr As StreamReader = New StreamReader(fs)
        Dim str() As String
        'Dim str1 As String
        Dim list As New List(Of String)()
        While (sr.Peek <> -1)
            list.Add(sr.ReadLine)
        End While
        sr.Close()
        fs.Close()
        str = list.ToArray
        HDDSerialNo = EncyDcry.Decrypt(str(0), True)
        InstallDate = Convert.ToDateTime(EncyDcry.Decrypt(str(1), True))
        NoOfUsers = EncyDcry.Decrypt(str(2), True)
        NoOfDevices = EncyDcry.Decrypt(str(3), True)
        If str.Length <= 4 Then
            TrialPerioad = True
            LicenseKey = ""
        Else
            TrialPerioad = False
            'get license and device serial no
            LicenseKey = str(4)
            If LicenseKey = "" Then
                TrialPerioad = True
                LicenseKey = ""
                GoTo trialCheck
            End If
            Dim UserKeyNum As Double = Num(HDDSerialNo)
            Dim LicenseNum As Double = (UserKeyNum * 9999) + Convert.ToDouble(1234567890) * 12345
            Dim licenseStr As String = Regex.Replace(LicenseNum.ToString, ".{5}", "$0-").TrimEnd(CChar("-"))
            Dim lStrTmp() As String = licenseStr.Split("-")
            Dim SfrmFile() As String = str(4).Trim.Split("-")

            Dim FullPayLicence As String = lStrTmp(0) & "P-" & lStrTmp(1) & "Y-" & lStrTmp(2) & "E"
            Dim LitePayLicence As String = lStrTmp(0) & "P-" & lStrTmp(1) & "N-" & lStrTmp(2) & "O"

            If (SfrmFile(0)) = (lStrTmp(0) & "P") Then
                If (SfrmFile(1)) = (lStrTmp(1) & "Y") And (SfrmFile(2)) = (lStrTmp(2) & "C") Then
                    Common.IsComplianceFromLic = True
                    Common.IsFullPayroll = True
                    LicenseKey = str(4)
                ElseIf (SfrmFile(1)) = (lStrTmp(1) & "Y") And (SfrmFile(2)) = (lStrTmp(2) & "O") Then
                    Common.IsComplianceFromLic = False
                    Common.IsFullPayroll = True
                    LicenseKey = str(4)
                ElseIf (SfrmFile(1)) = (lStrTmp(1) & "N") And (SfrmFile(2)) = (lStrTmp(2) & "C") Then
                    Common.IsComplianceFromLic = True
                    Common.IsFullPayroll = False
                    LicenseKey = str(4)
                ElseIf (SfrmFile(1)) = (lStrTmp(1) & "N") And (SfrmFile(2)) = (lStrTmp(2) & "O") Then
                    Common.IsComplianceFromLic = False
                    Common.IsFullPayroll = False
                    LicenseKey = str(4)
                Else
                    If Common.ApplicationType = "S" Then
                        XtraMessageBox.Show(XtraMasterTest.ulf, "<size=10>Incorrect License, Application will Close Now.</size>", "<size=9>Error</size>")
                        Application.Exit()
                    End If
                End If
            Else
                If Common.ApplicationType = "S" Then
                    XtraMessageBox.Show(XtraMasterTest.ulf, "<size=10>Incorrect License, Application will Close Now.</size>", "<size=9>Error</size>")
                    Application.Exit()
                End If
            End If
trialCheck: If str.Length = 6 Then
                Dim srtmp() As String = str(5).Split(",")
                Dim ls As New List(Of String)()
                For i As Integer = 0 To srtmp.Length - 1
                    ls.Add(EncyDcry.Decrypt(srtmp(i), True))
                Next
                DeviceSerialNo = ls.Distinct.ToArray
            End If
        End If

        If TrialPerioad = True And InstallDate.ToString("yyyy-MM-dd") < Now.AddDays(-30).ToString("yyyy-MM-dd") Then
            TrialExpired = True
        Else
            TrialExpired = False
        End If


    End Sub
End Class