﻿Imports System.Resources
Imports System.Globalization
Imports System.Data.SqlClient
Imports System.IO
Imports System.Data.OleDb
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.Net.NetworkInformation
Imports System.Text.RegularExpressions
Imports System.Net.Mail
Imports System.Net
Imports System.Threading
Imports Newtonsoft.Json

Public Class XtraCompanyInfo
    Dim ulf As UserLookAndFeel
    Dim tmpDeviceCount As String
    Dim TmpUserCount As String


    Public Class infolicense
        Public MAC
        Public ComName
        Public ComAdd
        Public ComContact
        Public ComEmailPublic
        Public AppName
        Public NoOfTime
        Public LastModifiedBy
        Public City
        Public State
        Public Pincode
        Public NoOfUsers
        Public NoOfDevices
    End Class

    'Dim MAC As String = "" ' getMacAddress()
    Private Sub SimpleButtonSave_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonSave.Click
        If TextEditCompanyName.Text.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Company Name cannot be empty</size>", "<size=9>Error</size>")
            TextEditCompanyName.Select()
            Exit Sub
        End If
        If MemoEditAddress.Text.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Company Address cannot be empty</size>", "<size=9>Error</size>")
            MemoEditAddress.Select()
            Exit Sub
        End If
        If TextEditCity.Text.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>City cannot be empty</size>", "<size=9>Error</size>")
            TextEditCity.Select()
            Exit Sub
        End If
        If TextEditState.Text.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>State cannot be empty</size>", "<size=9>Error</size>")
            TextEditState.Select()
            Exit Sub
        End If
        If TextEditPincode.Text.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Pincode cannot be empty</size>", "<size=9>Error</size>")
            TextEditPincode.Select()
            Exit Sub
        End If
        If TextEditContact.Text.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Company Contact cannot be empty</size>", "<size=9>Error</size>")
            TextEditContact.Select()
            Exit Sub
        End If
        If TextEditEmail.Text.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Company Email cannot be empty</size>", "<size=9>Error</size>")
            TextEditEmail.Select()
            Exit Sub
        End If

        If emailaddresscheck(TextEditEmail.Text.Trim) = False Then
            XtraMessageBox.Show(ulf, "<size=10>Invalid Email </size>", "<size=9>Error</size>")
            TextEditEmail.Select()
            Exit Sub
        End If

        If TextEditUserNo.Text <> TmpUserCount Then
            XtraMessageBox.Show(ulf, "<size=10>No of Users do not match with lic file </size>", "<size=9>Error</size>")
            TextEditUserNo.Select()
            Exit Sub
        End If

        If TextEditDeviceNO.Text <> tmpDeviceCount Then
            XtraMessageBox.Show(ulf, "<size=10>No of Devices do not match with lic file </size>", "<size=9>Error</size>")
            TextEditDeviceNO.Select()
            Exit Sub
        End If

        Dim cmd As New SqlCommand
        Dim cmd1 As New OleDbCommand
        'If TextEdit1.Text.Trim = "" Then
        Dim UserKeyNum As Double = Num(license.HDDSerialNo)
        Dim LicenseNum As Double = (UserKeyNum * 9999) + Convert.ToDouble(1234567890) * 12345
        Dim licenseStr As String = Regex.Replace(LicenseNum.ToString, ".{5}", "$0-").TrimEnd(CChar("-"))
        Dim lStrTmp() As String = licenseStr.Split("-")
        Dim FullPayLicence As String = lStrTmp(0) & "P-" & lStrTmp(1) & "Y-" & lStrTmp(2) & "E"
        Dim LitePayLicence As String = lStrTmp(0) & "P-" & lStrTmp(1) & "N-" & lStrTmp(2) & "O"


        If Common.CallCompInfoFromDevice Then
            TextEditLicense.Text = LitePayLicence
        End If

        Dim LicenseText As String = TextEditLicense.Text.Trim
        Dim SfrmFile() As String = LicenseText.Trim.Split("-")
        If (SfrmFile(0)) = (lStrTmp(0) & "P") Then
            If (SfrmFile(1)) = (lStrTmp(1) & "Y") And (SfrmFile(2)) = (lStrTmp(2) & "C") Then
                'Common.IsComplianceFromLic = True
                'Common.IsFullPayroll = True
                'LicenseKey = str(4)
            ElseIf (SfrmFile(1)) = (lStrTmp(1) & "Y") And (SfrmFile(2)) = (lStrTmp(2) & "O") Then
                'Common.IsComplianceFromLic = False
                'Common.IsFullPayroll = True
                'LicenseKey = str(4)
            ElseIf (SfrmFile(1)) = (lStrTmp(1) & "N") And (SfrmFile(2)) = (lStrTmp(2) & "C") Then
                'Common.IsComplianceFromLic = True
                'Common.IsFullPayroll = False
                'LicenseKey = str(4)
            ElseIf (SfrmFile(1)) = (lStrTmp(1) & "N") And (SfrmFile(2)) = (lStrTmp(2) & "O") Then
                'Common.IsComplianceFromLic = False
                'Common.IsFullPayroll = False
                'LicenseKey = str(4)
            Else
                XtraMessageBox.Show(XtraMasterTest.ulf, "<size=10>Invalid License</size>", "<size=9>Error</size>")
                Me.Cursor = Cursors.Default
                Exit Sub
            End If
        Else
            XtraMessageBox.Show(XtraMasterTest.ulf, "<size=10>Invalid License</size>", "<size=9>Error</size>")
            Me.Cursor = Cursors.Default
            Exit Sub
        End If

        Me.Cursor = Cursors.WaitCursor

        If TextEditUpload.Text.Trim = "" Then
            If Not VerifyOL() Then
                Exit Sub
            End If
        End If


        Dim ucount As String = TextEditUserNo.Text.Trim
        Dim Dcount As String = TextEditDeviceNO.Text.Trim
        'update license.lic file
        My.Computer.FileSystem.DeleteFile("License.lic")
        Dim fs As FileStream = New FileStream("License.lic", FileMode.Create, FileAccess.Write)
        Dim sw As StreamWriter = New StreamWriter(fs)
        Dim str As String = ""
        str = EncyDcry.Encrypt(license.HDDSerialNo, True)
        sw.WriteLine(str)
        sw.WriteLine(EncyDcry.Encrypt(Now.ToString("yyyy-MM-dd HH:mm:ss"), True))
        str = EncyDcry.Encrypt(ucount, True)
        sw.WriteLine(str)
        str = EncyDcry.Encrypt(Dcount, True)
        sw.WriteLine(str)
        sw.Write(TextEditLicense.Text.Trim)
        sw.Flush()
        sw.Close()
        fs.Close()
        'Else
        'Dim client As New Net.WebClient
        'client.DownloadFile(TextEdit1.Text, "License.lic")
        'End If


        Dim ComName As String = TextEditCompanyName.Text.Trim
        Dim ComAdd As String = MemoEditAddress.Text.Trim
        Dim ComContact As String = TextEditContact.Text.Trim
        Dim ComEmail As String = TextEditEmail.Text.Trim
        Dim UserKey As String = TextEditUserKey.Text

        'Dim sSql1 As String = "insert into InstallSystemInfo (MAC,ComName,ComAdd,ComContact,ComEmail,UserKey,License) values ('" & MAC & "','" & ComName & "','" & ComAdd & "','" & ComContact & "','" & ComEmail & "','" & UserKey & "','" & License & "')"
        Dim sSql1 As String = "Update InstallSystemInfo set MAC='" & license.HDDSerialNo & "',ComName='" & ComName & "',ComAdd='" & ComAdd & "', " &
            "ComContact='" & ComContact & "',ComEmail='" & ComEmail & "',UserKey='" & TextEditUserKey.Text.Trim & "',License='" & TextEditLicense.Text.Trim & "', " &
            "City='" & TextEditCity.Text.Trim & "', State='" & TextEditState.Text.Trim & "', Pincode='" & TextEditPincode.Text.Trim & "'"
        If Common.servername = "Access" Then
            Try
                If Common.con1.State <> ConnectionState.Open Then
                    Common.con1.Open()
                End If
                cmd1 = New OleDbCommand(sSql1, Common.con1)
                cmd1.ExecuteNonQuery()
                If Common.con1.State <> ConnectionState.Closed Then
                    Common.con1.Close()
                End If
            Catch ex As Exception
                'MsgBox(ex.Message.ToString)
                If Common.con1.State <> ConnectionState.Closed Then
                    Common.con1.Close()
                End If
            End Try
        Else
            Common.con.Open()
            cmd = New SqlCommand(sSql1, Common.con)
            cmd.ExecuteNonQuery()
            Common.con.Close()
        End If

        sSql1 = "update iASSystemInfo set Lic='" & TextEditLicense.Text.Trim & "'"
        If Common.servername = "Access" Then
            Try
                If Common.con1.State <> ConnectionState.Open Then
                    Common.con1.Open()
                End If
                cmd1 = New OleDbCommand(sSql1, Common.con1)
                cmd1.ExecuteNonQuery()
                If Common.con1.State <> ConnectionState.Closed Then
                    Common.con1.Close()
                End If
            Catch ex As Exception
                'MsgBox(ex.Message.ToString)
                If Common.con1.State <> ConnectionState.Closed Then
                    Common.con1.Close()
                End If
            End Try
        Else
            Common.con.Open()
            cmd = New SqlCommand(sSql1, Common.con)
            cmd.ExecuteNonQuery()
            Common.con.Close()
        End If


        license.checkLicenseFile() 'new
        license.getLicenseInfo() 'new

        Common.LogPost("License Information Save")
        Me.Cursor = Cursors.Default

        If PanelControl1.Visible = True Then
            XtraMessageBox.Show(ulf, "<size=10>Application will Restart to get effects</size>", "<size=10>iAS</size>")
            Me.Close()
            Application.Exit()
            Process.Start(Application.ExecutablePath)
        Else
            XtraMessageBox.Show(ulf, "<size=10>Information Saved Successfully</size>", "<size=10>iAS</size>")
        End If
    End Sub
    Private Sub XtraCompanyInfo_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True
        'MAC =  getMacAddress()
        GetDefault()
    End Sub
    Private Sub GetDefault()
        tmpDeviceCount = license.NoOfDevices
        TmpUserCount = license.NoOfUsers
        If Common.CallCompInfoFromDevice Then
            PanelControl1.Visible = False
            PanelControl2.Visible = False
            TextEditDeviceNO.Visible = False
            LabelControl15.Visible = False
            LabelControl16.Visible = False
            TextEditUserNo.Visible = False
            LabelControl13.Visible = False
            LabelControl14.Visible = False
            TextEditLicense.Visible = False
            LabelControl6.Visible = False
            LabelControl12.Visible = False
        Else
            PanelControl1.Visible = True
            PanelControl2.Visible = False
            TextEditDeviceNO.Visible = True
            LabelControl15.Visible = True
            LabelControl16.Visible = True
            TextEditUserNo.Visible = True
            LabelControl13.Visible = True
            LabelControl14.Visible = True
            TextEditLicense.Visible = True
            LabelControl6.Visible = True
            LabelControl12.Visible = True
        End If
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim dsL As DataSet = New DataSet
        Dim sSql As String = "select * from InstallSystemInfo"
        'If Common.checkOpenedFromLicense = "Admin" Then
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(dsL)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(dsL)
        End If

        If dsL.Tables(0).Rows.Count > 0 Then
            TextEditLicense.Text = dsL.Tables(0).Rows(0).Item("License").ToString.Trim
            TextEditCompanyName.Text = dsL.Tables(0).Rows(0).Item("ComName").ToString.Trim
            MemoEditAddress.Text = dsL.Tables(0).Rows(0).Item("ComAdd").ToString.Trim
            TextEditContact.Text = dsL.Tables(0).Rows(0).Item("ComContact").ToString.Trim
            TextEditEmail.Text = dsL.Tables(0).Rows(0).Item("ComEmail").ToString.Trim

            TextEditCity.Text = dsL.Tables(0).Rows(0).Item("City").ToString.Trim
            TextEditState.Text = dsL.Tables(0).Rows(0).Item("State").ToString.Trim
            TextEditPincode.Text = dsL.Tables(0).Rows(0).Item("Pincode").ToString.Trim
            'TextEditUserKey.Text = dsL.Tables(0).Rows(0).Item("UserKey").ToString.Trim
            'SimpleButtonSave.Visible = False
            'SimpleButtonVerify.Visible = False
        Else
            TextEditLicense.Text = ""
            TextEditCompanyName.Text = ""
            MemoEditAddress.Text = ""
            TextEditContact.Text = ""
            TextEditEmail.Text = ""

            TextEditCity.Text = ""
            TextEditState.Text = ""
            TextEditPincode.Text = ""
            'TextEditUserKey.Text = Common.SysyemMac
            'SimpleButtonSave.Visible = True
            'SimpleButtonVerify.Visible = True
        End If
        TextEditUserKey.Text = license.HDDSerialNo
        TextEditUserNo.Text = license.NoOfUsers
        TextEditDeviceNO.Text = license.NoOfDevices
        'Else
        '    TextEditLicense.Text = ""
        '    TextEditCompanyName.Text = ""
        '    MemoEditAddress.Text = ""
        '    TextEditContact.Text = ""
        '    TextEditEmail.Text = ""
        '    TextEditUserKey.Text = ""
        '    SimpleButtonSave.Visible = True
        '    SimpleButtonVerify.Visible = True
        'End If
    End Sub
    'Private Sub XtraCompanyInfo_FormClosing(sender As System.Object, e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
    '    If XtraMessageBox.Show(ulf, "<size=10>Are you sure to close the application?</size>", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) <> Windows.Forms.DialogResult.Yes Then
    '        e.Cancel = True
    '    End If
    'End Sub
    Private Sub XtraCompanyInfo_FormClosed(sender As System.Object, e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        'Application.Exit()
    End Sub
    Private Function emailaddresscheck(ByVal emailaddress As String) As Boolean
        Dim pattern As String = "^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"
        Dim emailAddressMatch As Match = Regex.Match(emailaddress, pattern)
        If emailAddressMatch.Success Then
            Return True
        Else
            Return False
        End If
    End Function
    Public Shared Function Num(ByVal value As String) As Double
        If value.Length > 12 Then
            value = value.Substring(0, 12)
        End If
        Dim returnVal As String = String.Empty
        Dim collection As MatchCollection = Regex.Matches(value, "\d+")
        For Each m As Match In collection
            returnVal += m.ToString()
        Next
        Return Convert.ToDouble(returnVal)
    End Function
    Private Sub SimpleButton1_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonClose.Click
        If license.TrialPerioad = True And license.TrialExpired = True Then
            Application.Exit()
        Else
            Me.Close()
        End If

    End Sub
    Public Class VersionInfo
        Public Property SetLicenseInfoResult As String
    End Class
    Public Function VerifyOL() As Boolean



#Region "APICall"
        Dim ComInfo As infolicense = New infolicense
        ComInfo.MAC = license.HDDSerialNo
        ComInfo.ComName = TextEditCompanyName.Text.Trim
        ComInfo.ComAdd = MemoEditAddress.Text.Trim.Replace(vbCrLf, " ").Replace(",", " ")
        ComInfo.ComContact = TextEditContact.Text.Trim
        ComInfo.ComEmailPublic = TextEditEmail.Text.Trim
        ComInfo.AppName = "iAS"
        ComInfo.NoOfTime = "1"
        ComInfo.LastModifiedBy = "admin"
        ComInfo.City = TextEditCity.Text.Trim
        ComInfo.State = TextEditState.Text.Trim
        ComInfo.Pincode = TextEditPincode.Text.Trim
        ComInfo.NoOfUsers = TextEditUserNo.Text.Trim
        ComInfo.NoOfDevices = TextEditDeviceNO.Text.Trim

        Try
            Dim proxy As WebClient = New WebClient
            Dim urlN As String = "http://137.59.201.60:5011/Service1.svc/SetLicenseInfo/" & ComInfo.MAC & "," & ComInfo.ComName & "," & ComInfo.ComAdd & "," & ComInfo.ComContact & "," & ComInfo.ComEmailPublic & "," & ComInfo.AppName & "," & ComInfo.NoOfTime & "," & ComInfo.LastModifiedBy & "," & ComInfo.City & "," & ComInfo.State & "," & ComInfo.Pincode & "," & ComInfo.NoOfUsers & "," & ComInfo.NoOfDevices
            Dim serviceURL As String = String.Format(urlN)
            Dim data() As Byte = proxy.DownloadData(serviceURL)
            Dim stream As Stream = New MemoryStream(data)
            Dim OutPut As String = System.Text.Encoding.UTF8.GetString(data)

            Dim JsonCardInfoCount As VersionInfo = New VersionInfo()
            JsonCardInfoCount = JsonConvert.DeserializeObject(Of VersionInfo)(OutPut)
            Dim ServerVersion As String = JsonCardInfoCount.SetLicenseInfoResult
            If ServerVersion.Trim.ToLower <> "true" Then
                'XtraMessageBox.Show(ulf, "<size=10>" & ServerVersion & "</size>", "<size=9>iAS</size>")
                Me.Cursor = Cursors.Default
                Return True
                'Me.Hide()

            End If

        Catch ex As Exception
            XtraMessageBox.Show(ulf, "<size=10>Please make sure that you are connected to Internet, Try Offline Mode.</size>", "<size=9>iAS</size>")
            Me.Cursor = Cursors.Default

            If Common.CallCompInfoFromDevice Then
                PanelControl1.Visible = True
                TextEditLicense.Visible = True
            End If
            Return False
        End Try
#End Region
        'Dim ConnectionString As String = "Data Source=137.59.201.60, 1433;Initial Catalog=iASVersionCheck;User Id=sa;Password=sss;"
        'Dim contmp As SqlConnection = New SqlConnection(ConnectionString)
        'Dim sSql As String = "Select * from Infolicense where MAC = '" & license.HDDSerialNo & "'"
        'Dim adap As SqlDataAdapter
        'Dim ds As DataSet = New DataSet
        'Try
        '    adap = New SqlDataAdapter(sSql, contmp)
        '    adap.Fill(ds)
        'Catch ex As Exception
        '    XtraMessageBox.Show(ulf, "<size=10>Please make sure that you are connected to Internet</size>", "<size=9>ULtra</size>")
        '    Me.Cursor = Cursors.Default
        '    Exit Sub
        'End Try
        'If ds.Tables(0).Rows.Count = 0 Then
        '    '    sSql = "update infolicense set NoOfTime = NoOfTime + 1, LastModifiedBy ='" & license.HDDSerialNo & "', LastModifiedDate=getdate() where MAC='" & license.HDDSerialNo & "'"
        '    'Else
        '    sSql = "insert into infolicense (MAC,ComName, ComAdd, ComContact, ComEmail, AppName, InstalledDate, NoOfTime, LastModifiedBy, LastModifiedDate, City, State, Pincode, NoOfUsers, NoOfDevices)VALUES " &
        '        "('" & license.HDDSerialNo & "' ,'" & TextEditCompanyName.Text.Trim & "', '" & MemoEditAddress.Text.Trim & "', '" & TextEditContact.Text.Trim & "', '" & TextEditEmail.Text.Trim & "', 'ULtra', getdate(), 1, '" & license.HDDSerialNo & "', getdate(),'" & TextEditCity.Text.Trim & "','" & TextEditState.Text.Trim & "','" & TextEditPincode.Text.Trim & "','" & TextEditUserNo.Text.Trim & "','" & TextEditDeviceNO.Text.Trim & "')"
        'End If
        'Try
        '    If contmp.State <> ConnectionState.Open Then
        '        contmp.Open()
        '    End If
        '    Dim cmd As SqlCommand = New SqlCommand(sSql, contmp)
        '    cmd.ExecuteNonQuery()
        '    If contmp.State <> ConnectionState.Closed Then
        '        contmp.Close()
        '    End If
        'Catch ex As Exception
        '    If contmp.State <> ConnectionState.Closed Then
        '        contmp.Close()
        '    End If
        '    XtraMessageBox.Show(ulf, "<size=10>Please make sure that you are connected to Internet</size>", "<size=9></size>")
        '    Me.Cursor = Cursors.Default
        '    Exit Sub
        'End Try





        Dim Server As String = "smtp.timewatchindia.com"
        Dim senderDB As String = "appliction@timewatchindia.com" ' "nitins@timewatchindia.com"
        Dim type As String = "IMAP"
        Dim username As String = "appliction@timewatchindia.com"
        Dim password As String = "ed%@Usn5"
        Dim IsEnable As Boolean = False
        Dim Port As String = "25"
        Dim SenderName As String = "iAS"
        Dim CCMail As String = "appliction@timewatchindia.com"

        Dim Smtp_Server As New SmtpClient
        Dim e_mail As New MailMessage()
        Dim attachment As System.Net.Mail.Attachment
        Smtp_Server.UseDefaultCredentials = False

        'Smtp_Server.Credentials = New Net.NetworkCredential(("nittslam@gmail.com"), ("umawidnitinbs4"))
        Smtp_Server.Credentials = New Net.NetworkCredential(senderDB, password)
        Smtp_Server.Port = Convert.ToInt16(Port) '587
        Smtp_Server.EnableSsl = IsEnable 'True
        Smtp_Server.Host = Server '"smtp.gmail.com"
        e_mail = New MailMessage()
        'e_mail.From = New MailAddress("nittslam@gmail.com")
        e_mail.From = New MailAddress(senderDB)
        'e_mail.To.Add("nittslam@gmail.com")
        Try
            e_mail.To.Add("nitin@timewatchindia.com,helpdesk@timewatchindia.com")
            If CCMail.Trim <> "" Then
                e_mail.Bcc.Add(CCMail)
            End If
            e_mail.IsBodyHtml = True
            e_mail.IsBodyHtml = True
            e_mail.Subject = "iAS License Activation Request"
            e_mail.Body = "Please find the below informtion for License Activation" & vbCrLf &
                "<table border=1><tr><td>Customer Name: </td><td>" & TextEditCompanyName.Text.Trim & "</td></tr>" & vbCrLf &
                "<tr><td>Address: </td><td>" & MemoEditAddress.Text.Trim & "</td></tr>" & vbCrLf &
                "<tr><td>Contact No: </td><td>" & TextEditContact.Text.Trim & "</td></tr>" & vbCrLf &
                "<tr><td>Email Id: </td><td>" & TextEditEmail.Text.Trim & "</td></tr>" & vbCrLf &
                "<tr><td>User Key: </td><td>" & TextEditUserKey.Text.Trim & "</td></tr>" & vbCrLf &
                "<tr><td>No of Users: </td><td>" & TextEditUserNo.Text.Trim & "</td></tr>" & vbCrLf &
                "<tr><td>No of Device: </td><td>" & TextEditDeviceNO.Text.Trim & "</td></tr></table>"

            Smtp_Server.Send(e_mail)
        Catch ex As Exception
        End Try

        Dim request As HttpWebRequest
        Try
            Dim Message As String = "Please Activate iAS License for Customer Name: " & TextEditCompanyName.Text.Trim & " Contact: " & TextEditContact.Text.Trim
            Dim url As String = "https://www.mobtexting.com/app/index.php/api?method=sms.normal&api_key=c666c755fd17adfaaa8382a836d6ee2e9ebffa26&to=8369499622,9599953914&sender=TMWTCH&message=" & Message & "&flash=0&unicode=1"
            Dim WebRequestObject As HttpWebRequest = CType(HttpWebRequest.Create(url), HttpWebRequest)
            WebRequestObject.Timeout = Timeout.Infinite
            WebRequestObject.KeepAlive = True
            'WebRequestObject.Timeout = 5000
            'WebRequestObject.ReadWriteTimeout = 5000
            Dim response As HttpWebResponse
            response = DirectCast(WebRequestObject.GetResponse(), HttpWebResponse)
            response.Close()
            'request = DirectCast(WebRequest.Create(url1), HttpWebRequest)
            'response = DirectCast(request.GetResponse(), HttpWebResponse)
        Catch ex As Exception
        End Try

        'XtraMessageBox.Show(ulf, "<size=10>Activation Request Sent</size>", "<size=9>Ultra</size>")
        Me.Cursor = Cursors.Default
        'Me.Hide()
    End Function
    Private Sub SimpleButtonDownload_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonDownload.Click
        'My.Computer.DownloadFile("License.lic", Path.GetFileName(SaveFileDialog1.FileName), SaveFileDialog1.FileName)
        'If license.LicenseKey <> "" Then
        '    XtraMessageBox.Show(ulf, "<size=10>License Already Activated</size>", "<size=9>iAS</size>")
        '    Exit Sub
        'End If
        Try
            If TextEditCompanyName.Text.Trim = "" Then
                XtraMessageBox.Show(ulf, "<size=10>Company Name cannot be empty</size>", "<size=9>Error</size>")
                TextEditCompanyName.Select()
                Exit Sub
            End If
            If MemoEditAddress.Text.Trim = "" Then
                XtraMessageBox.Show(ulf, "<size=10>Company Address cannot be empty</size>", "<size=9>Error</size>")
                MemoEditAddress.Select()
                Exit Sub
            End If
            If TextEditCity.Text.Trim = "" Then
                XtraMessageBox.Show(ulf, "<size=10>City cannot be empty</size>", "<size=9>Error</size>")
                TextEditCity.Select()
                Exit Sub
            End If
            If TextEditState.Text.Trim = "" Then
                XtraMessageBox.Show(ulf, "<size=10>State cannot be empty</size>", "<size=9>Error</size>")
                TextEditState.Select()
                Exit Sub
            End If
            If TextEditPincode.Text.Trim = "" Then
                XtraMessageBox.Show(ulf, "<size=10>Pincode cannot be empty</size>", "<size=9>Error</size>")
                TextEditPincode.Select()
                Exit Sub
            End If
            If TextEditContact.Text.Trim = "" Then
                XtraMessageBox.Show(ulf, "<size=10>Company Contact cannot be empty</size>", "<size=9>Error</size>")
                TextEditContact.Select()
                Exit Sub
            End If
            If TextEditEmail.Text.Trim = "" Then
                XtraMessageBox.Show(ulf, "<size=10>Company Email cannot be empty</size>", "<size=9>Error</size>")
                TextEditEmail.Select()
                Exit Sub
            End If

            If emailaddresscheck(TextEditEmail.Text.Trim) = False Then
                XtraMessageBox.Show(ulf, "<size=10>Invalid Email </size>", "<size=9>Error</size>")
                TextEditEmail.Select()
                Exit Sub
            End If

            If System.IO.File.Exists("UserKey.lic") Then
                My.Computer.FileSystem.DeleteFile("UserKey.lic")
            End If
            'File.Create("UserKey.lic").Dispose()
            Dim fs As FileStream = New FileStream("UserKey.lic", FileMode.Create, FileAccess.Write)
            Dim sw As StreamWriter = New StreamWriter(fs)
            Dim str As String = ""
            sw.WriteLine(TextEditCompanyName.Text.Trim)
            sw.WriteLine(MemoEditAddress.Text.Trim.Replace(vbCrLf, " "))
            sw.WriteLine(TextEditCity.Text.Trim)
            sw.WriteLine(TextEditState.Text.Trim)
            sw.WriteLine(TextEditPincode.Text.Trim)
            sw.WriteLine(TextEditContact.Text.Trim)
            sw.WriteLine(TextEditEmail.Text.Trim)

            Dim ucount As String = TextEditUserNo.Text.Trim
            Dim Dcount As String = TextEditDeviceNO.Text.Trim

            sw.WriteLine(EncyDcry.Encrypt(license.HDDSerialNo, True))
            sw.WriteLine(EncyDcry.Encrypt(license.InstallDate.ToString("yyyy-MM-dd HH:mm:ss"), True))
            sw.WriteLine(EncyDcry.Encrypt(ucount, True))
            sw.Write(EncyDcry.Encrypt(Dcount, True))
            sw.Flush()
            sw.Close()
            fs.Close()

            Dim client As New Net.WebClient
            Dim sfd As New SaveFileDialog
            sfd.FileName = "UserKey.lic"
            sfd.ShowDialog()
            client.DownloadFile("UserKey.lic", sfd.FileName)
            Try
                My.Computer.FileSystem.DeleteFile("UserKey.lic")
            Catch ex As Exception
            End Try
        Catch ex As Exception
            XtraMessageBox.Show(ulf, "<size=10>" & ex.Message.Trim & "</size>", "<size=9>iAS</size>")
        End Try
    End Sub
    Private Sub TextEdit1_Click(sender As System.Object, e As System.EventArgs) Handles TextEditUpload.Click
        Dim dlg As New OpenFileDialog()
        ' Filter by Office Files
        dlg.Filter = "Office Files|*.lic;"
        dlg.ShowDialog()
        TextEditUpload.Text = dlg.FileName
    End Sub
    Private Sub SimpleButtonUpload_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonUpload.Click
        If TextEditUpload.Text.Trim <> "" Then
            Dim client As New Net.WebClient
            client.DownloadFile(TextEditUpload.Text, "ActivationKey.lic")
            Dim fs As FileStream = New FileStream("ActivationKey.lic", FileMode.Open, FileAccess.Read)
            Dim sr As StreamReader = New StreamReader(fs)
            Dim str() As String
            'Dim str1 As String
            Dim list As New List(Of String)()
            While (sr.Peek <> -1)
                list.Add(sr.ReadLine)
            End While
            sr.Close()
            fs.Close()
            str = list.ToArray
            If str.Length < 12 Then
                XtraMessageBox.Show(ulf, "<size=10>Incorrect file</size>", "<size=9>iAS</size>")
                TextEditUpload.Select()
                Exit Sub
            Else
                If EncyDcry.Decrypt(str(7), True) <> license.HDDSerialNo Then
                    XtraMessageBox.Show(ulf, "<size=10>Incorrect User Key</size>", "<size=9>iAS</size>")
                    TextEditUpload.Select()
                    Exit Sub
                End If
                TextEditCompanyName.Text = str(0).Trim
                MemoEditAddress.Text = str(1).Trim
                TextEditCity.Text = str(2).Trim
                TextEditState.Text = str(3).Trim
                TextEditPincode.Text = str(4).Trim
                TextEditContact.Text = str(5).Trim
                TextEditEmail.Text = str(6).Trim
                TextEditLicense.Text = str(11)
                TextEditUserNo.Text = EncyDcry.Decrypt(str(9), True)
                TextEditDeviceNO.Text = EncyDcry.Decrypt(str(10), True)

                tmpDeviceCount = TextEditDeviceNO.Text
                TmpUserCount = TextEditUserNo.Text
            End If
        Else
            XtraMessageBox.Show(ulf, "<size=10>Please Select The file</size>", "<size=9>iAS</size>")
            TextEditUpload.Select()
        End If
    End Sub

    Private Sub SimpleButtonVerify_Click(sender As Object, e As EventArgs) Handles SimpleButtonVerify.Click
        ''Dim UserKeyNum As Double = Num(license.HDDSerialNo)
        ''Dim LicenseNum As Double = (UserKeyNum * 9999) + Convert.ToDouble(1234567890) * 12345
        ''Dim licenseStr As String = Regex.Replace(LicenseNum.ToString, ".{5}", "$0-").TrimEnd(CChar("-"))
        ''TextEditLicense.Text = licenseStr

        'If license.LicenseKey <> "" Then
        '    XtraMessageBox.Show(ulf, "<size=10>License Already Activated</size>", "<size=9>iAS</size>")
        '    Exit Sub
        'End If

        If TextEditCompanyName.Text.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Company Name cannot be empty</size>", "<size=9>Error</size>")
            TextEditCompanyName.Select()
            Exit Sub
        End If
        If MemoEditAddress.Text.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Company Address cannot be empty</size>", "<size=9>Error</size>")
            MemoEditAddress.Select()
            Exit Sub
        End If
        If TextEditCity.Text.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>City cannot be empty</size>", "<size=9>Error</size>")
            TextEditCity.Select()
            Exit Sub
        End If
        If TextEditState.Text.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>State cannot be empty</size>", "<size=9>Error</size>")
            TextEditState.Select()
            Exit Sub
        End If
        If TextEditPincode.Text.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Pincode cannot be empty</size>", "<size=9>Error</size>")
            TextEditPincode.Select()
            Exit Sub
        End If
        If TextEditContact.Text.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Company Contact cannot be empty</size>", "<size=9>Error</size>")
            TextEditContact.Select()
            Exit Sub
        End If
        If TextEditEmail.Text.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Company Email cannot be empty</size>", "<size=9>Error</size>")
            TextEditEmail.Select()
            Exit Sub
        End If

        If emailaddresscheck(TextEditEmail.Text.Trim) = False Then
            XtraMessageBox.Show(ulf, "<size=10>Invalid Email </size>", "<size=9>Error</size>")
            TextEditEmail.Select()
            Exit Sub
        End If
        If VerifyOL() Then
            Me.Hide()
        End If
    End Sub

    Private Sub HyperlinkLabelControl1_Click(sender As Object, e As EventArgs) Handles HyperlinkLabelControl1.Click
        Dim process As Process = New Process
        process.StartInfo.FileName = "http://crm.mytimewatch.com/"
        process.StartInfo.Verb = "open"
        process.StartInfo.WindowStyle = ProcessWindowStyle.Normal
        Try
            process.Start()
        Catch ex As System.Exception
            XtraMessageBox.Show(ulf, "<size=10>" & ex.Message.ToString & "</size>", "<size=9>Error</size>")
            Exit Sub
        End Try
    End Sub
End Class