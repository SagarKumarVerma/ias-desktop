﻿Imports System.Resources
Imports System.Globalization
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports System.ComponentModel
Imports DevExpress.XtraGrid.Columns
Imports System.Data.SqlClient
Imports System.Text
Imports System.Data.OleDb
Imports DevExpress.XtraGrid.Views.Base
Imports Riss.Devices
Imports System.Threading
Imports iAS.HSeriesSampleCSharp
Imports System.Runtime.InteropServices
Imports CMITech.UMXClient
Imports System.Net
Imports Newtonsoft.Json
Imports System.Drawing.Imaging

Public Class XtraFringerDataMgmt

    ' ultra800
    Public Class pageInfo
        Public index As Integer
        Public length As Integer
        Public size As Integer
        Public total As Integer
    End Class
    Public Class records
        Public aliveType As Integer
        Public id As String
        Public idcardNum As String
        Public identifyType As Integer
        Public isImgDeleted As Integer
        Public isPass As Boolean
        Public model As Integer
        Public name As String
        Public passTimeType As Integer
        Public path As String
        Public permissionTimeType As Integer
        Public personId As String
        Public recModeType As Integer
        Public recType As Integer
        Public standard As String = ""
        Public state As Integer
        Public tempUnit As String = ""
        Public temperature As String = ""
        Public temperatureState As String
        Public time As String
        Public type As Integer
    End Class
    Public Class data
        Public pageInfo As pageInfo
        Public records() As records = New records() {}
    End Class
    Public Class AttlogUltra800
        Public code As String
        Public data As data
        Public msg As String
        Public result As Integer
        Public success As Boolean
    End Class
    Public Class FaceUltra800
        Public code As String
        Public data() As Fdata
        Public msg As String
        Public result As Integer
        Public success As Boolean
    End Class
    Public Class Fdata
        Public croplmgPath As String
        Public faceId As String
        Public feature As String
        Public featureKey As String
        Public path As String
        Public personId As String
        Public rect As Object
    End Class
    Public Class UserDelete
        Public pass As String
        Public id As String
    End Class
    Public Class Ulra800UserUpload
        Public pass As String
        Public person As person = New person
    End Class
    Public Class person
        Public id As String
        Public name As String
        Public idcardNum As String
        Public iDNumber As String = ""
        'Public facePermission As Integer = 2
        'Public idCardPermission As Integer = 2
        'Public faceAndCardPermission As Integer = 2
        'Public iDPermission As Integer = 2
        'Public tag As String = ""
        'Public phone As String = ""
        'Public password As String
        'Public passwordPermission As Integer = 2
    End Class
    Public Class UltraFaceUpload
        Public pass As String
        Public personId As String
        Public faceId As String
        Public imgBase64 As String
    End Class

    Dim ulf As UserLookAndFeel
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand

    Const PWD_DATA_SIZE = 40
    Const FP_DATA_SIZE = 1680
    Const FACE_DATA_SIZE = 20080
    Const VEIN_DATA_SIZE = 3080
    Private mbytEnrollData(FACE_DATA_SIZE - 1) As Byte
    Private mbytPhotoImage() As Byte

        'Private mnCommHandleIndex As Long
        Private mbGetState As Boolean
        Private mlngPasswordData As Long

        Dim vpszIPAddress As String
        Dim vpszNetPort As Long
        Dim vpszNetPassword As Long
        Dim vnTimeOut As Long
        Dim vnProtocolType As Long
        Dim strDateTime As String
        Dim vnResult As Long
        Dim vnLicense As Long
        '//=============== Backup Number Constant ===============//
        'Public Const BACKUP_FP_0 = 0                  ' Finger 0
        'Public Const BACKUP_FP_1 = 1                  ' Finger 1
        'Public Const BACKUP_FP_2 = 2                  ' Finger 2
        'Public Const BACKUP_FP_3 = 3                  ' Finger 3
        'Public Const BACKUP_FP_4 = 4                  ' Finger 4
        'Public Const BACKUP_FP_5 = 5                  ' Finger 5
        'Public Const BACKUP_FP_6 = 6                  ' Finger 6
        'Public Const BACKUP_FP_7 = 7                  ' Finger 7
        'Public Const BACKUP_FP_8 = 8                  ' Finger 8
        'Public Const BACKUP_FP_9 = 9                  ' Finger 9
        'Public Const BACKUP_PSW = 10                  ' Password
        Public Const BACKUP_CARD = 11                 ' Card
        Public Const BACKUP_FACE = 12                 ' FaceF:\developement\TimeWatchNewDeskTop\TimeWatchNewDeskTop\XtraGrade.vb
        Public Const BACKUP_VEIN_0 = 20               ' Vein 0


    Private Structure Bio2EnrollData
        Dim EMachineNumber As String
        Dim EnrollNumber As String
        Dim UserName As String
        Dim Privilege As Integer
        Dim Password As String
        Dim Cardnumber As String
        Dim ExtInfo As String 'Comment
        Dim Enable As Integer
        Dim Template() As String
        'userAttTypeIdNumericUpDown.Value = user1.AttType
        'userAccessControlComboBox.SelectedIndex = user1.AccessControl
        'userPassZoneNumericUpDown.Value = user1.AccessTimeZone
        'userDeptIdNumericUpDown.Value = user1.Department
        'userGroupIdNumericUpDown.Value = user1.UserGroup
        'userValidityPeriodComboBox.SelectedIndex = Convert.ToInt32(user1.ValidityPeriod)
        'userStartDateTimePicker.Value = user1.ValidDate
        'userEndDateTimePicker.Value = user1.InvalidDate
        'Dim fpBytes As Byte()
        'Array.Copy(user1.Enrolls(0).Fingerprint, fpBytes, Zd2911Utils.MaxFingerprintLength)
        'txt_UserFpData.Text = ConvertObject.ConvertByteToHex(fpBytes).Replace("-", " ")
    End Structure
    Public Sub New()
        InitializeComponent()
        Common.SetGridFont(GridView1, New Font("Tahoma", 10))
        Common.SetGridFont(GridView2, New Font("Tahoma", 10))
    End Sub
    Private Sub setDefault()
        ToggleDownloadAll.IsOn = True
        If ToggleDownloadAll.IsOn = True Then
            LabelControl1.Visible = False
            TextSelectUser.Visible = False
        Else
            LabelControl1.Visible = True
            TextSelectUser.Visible = True
        End If
        TextSelectUser.Text = ""
        ToggleCreateEmp.IsOn = False
        GridView1.ClearSelection()
        GridView2.ClearSelection()
        TextPassword.Text = ""
        For Each c As Control In Me.Controls
            c.Enabled = True
        Next
    End Sub
    Private Sub XtraFringerDataMgmt_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True

        If Common.servername = "Access" Then
            'Me.TblMachine1TableAdapter1.Fill(Me.SSSDBDataSet.tblMachine1)
            ''GridControl1.DataSource = SSSDBDataSet.tblMachine1
            'Me.Fptable1TableAdapter1.Fill(Me.SSSDBDataSet.fptable1)
            'GridControl2.DataSource = SSSDBDataSet.fptable1

            Me.TblDepartment1TableAdapter1.Fill(Me.SSSDBDataSet.tblDepartment1)
            GridControlDept.DataSource = SSSDBDataSet.tblDepartment1

        Else
            'TblMachineTableAdapter.Connection.ConnectionString = Common.ConnectionString
            'FptableTableAdapter.Connection.ConnectionString = Common.ConnectionString
            'Me.TblMachineTableAdapter.Fill(Me.SSSDBDataSet.tblMachine)
            ''GridControl1.DataSource = SSSDBDataSet.tblMachine
            'Me.FptableTableAdapter.Fill(Me.SSSDBDataSet.fptable)
            'GridControl2.DataSource = SSSDBDataSet.fptable

            TblDepartmentTableAdapter.Connection.ConnectionString = Common.ConnectionString
            Me.TblDepartmentTableAdapter.Fill(Me.SSSDBDataSet.tblDepartment)
            GridControlDept.DataSource = SSSDBDataSet.tblDepartment
        End If
        LoadFPGrid()
        GridControl1.DataSource = Common.MachineNonAdmin
        GridControlBranch.DataSource = Common.LocationNonAdmin
        Common.SetGridFont(GridViewBranch, New System.Drawing.Font("Tahoma", 10))
        Common.SetGridFont(GridViewDept, New System.Drawing.Font("Tahoma", 10))
        PopupContainerEditDept.EditValue = ""
        PopupContainerEditDept.EditValue = ""
        setDefault()
    End Sub
    Private Sub GridView2_CustomColumnDisplayText(sender As System.Object, e As DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs) Handles GridView2.CustomColumnDisplayText
        Try
            Dim row As System.Data.DataRow = GridView2.GetDataRow(GridView2.FocusedRowHandle)
            Dim view As ColumnView = TryCast(sender, ColumnView)
            If e.Column.FieldName = "FingerNumber" Then
                If view.GetListSourceRowCellValue(e.ListSourceRowIndex, "FingerNumber").ToString().Trim >= 0 And
                    view.GetListSourceRowCellValue(e.ListSourceRowIndex, "FingerNumber").ToString().Trim < 10 Then
                    e.DisplayText = "FP-" & view.GetListSourceRowCellValue(e.ListSourceRowIndex, "FingerNumber").ToString().Trim + 1
                ElseIf view.GetListSourceRowCellValue(e.ListSourceRowIndex, "FingerNumber").ToString().Trim = 12 Or view.GetListSourceRowCellValue(e.ListSourceRowIndex, "FingerNumber").ToString().Trim = 50 Or view.GetListSourceRowCellValue(e.ListSourceRowIndex, "FingerNumber").ToString().Trim = 22 Or view.GetListSourceRowCellValue(e.ListSourceRowIndex, "FingerNumber").ToString().Trim = 20 Then
                    e.DisplayText = "FACE"
                ElseIf view.GetListSourceRowCellValue(e.ListSourceRowIndex, "FingerNumber").ToString().Trim = 11 Or view.GetListSourceRowCellValue(e.ListSourceRowIndex, "FingerNumber").ToString().Trim = 21 Then
                    e.DisplayText = "CARD"
                ElseIf view.GetListSourceRowCellValue(e.ListSourceRowIndex, "FingerNumber").ToString().Trim = 10 Or view.GetListSourceRowCellValue(e.ListSourceRowIndex, "FingerNumber").ToString().Trim = 20 Then
                    e.DisplayText = "PASSWORD"
                ElseIf view.GetListSourceRowCellValue(e.ListSourceRowIndex, "FingerNumber").ToString().Trim = 99 Then
                    e.DisplayText = "Iris"
                End If
                If view.GetListSourceRowCellValue(e.ListSourceRowIndex, "DeviceType").ToString().Trim = "ATF686n" And view.GetListSourceRowCellValue(e.ListSourceRowIndex, "FingerNumber").ToString().Trim = 20 Then
                    e.DisplayText = "PASSWORD"
                End If
                e.DisplayText = (view.GetListSourceRowCellValue(e.ListSourceRowIndex, "SHIFTDURATION").ToString().Trim \ 60).ToString("00") & ":" & (view.GetListSourceRowCellValue(e.ListSourceRowIndex, "SHIFTDURATION").ToString().Trim Mod 60).ToString("00")
                End If
        Catch
        End Try
        Try
            Dim adapA As OleDbDataAdapter
            Dim adap As SqlDataAdapter
            Dim ds As DataSet
            Dim sSql As String
            Dim row As System.Data.DataRow = GridView2.GetDataRow(GridView2.FocusedRowHandle)
            Dim view As ColumnView = TryCast(sender, ColumnView)
            If e.Column.FieldName = "UserName" Then
                sSql = "SELECT EMPNAME from TblEmployee where PRESENTCARDNO = '" & view.GetListSourceRowCellValue(e.ListSourceRowIndex, "EnrollNumber").ToString().Trim & "'"
                ds = New DataSet
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(ds)
                Else
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(ds)
                End If
                If ds.Tables(0).Rows.Count > 0 Then
                    e.DisplayText = ds.Tables(0).Rows(0).Item(0).ToString
                Else
                    'e.DisplayText = ""
                End If
            End If

            'If e.Column.Caption = "Device Type" Then
            '    If view.GetListSourceRowCellValue(e.ListSourceRowIndex, "Template").ToString().Trim = "" Then
            '        If view.GetListSourceRowCellValue(e.ListSourceRowIndex, "Cardnumber").ToString().Trim <> "" Or view.GetListSourceRowCellValue(e.ListSourceRowIndex, "Password").ToString().Trim <> "" Then
            '            e.DisplayText = "ZK/Bio"
            '        Else
            '            e.DisplayText = "Bio"
            '        End If
            '    ElseIf view.GetListSourceRowCellValue(e.ListSourceRowIndex, "Template_Tw").ToString().Trim = "" Then
            '        e.DisplayText = "ZK"
            '    End If
            'End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub GridView1_CustomColumnDisplayText(sender As System.Object, e As DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs) Handles GridView1.CustomColumnDisplayText
        Try
            Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
            Dim view As ColumnView = TryCast(sender, ColumnView)
            If e.Column.FieldName = "DeviceType" Then
                If view.GetListSourceRowCellValue(e.ListSourceRowIndex, "DeviceType").ToString().Trim = "Bio-1/3/4/5/6/7/ATF-203/ATF-395/ACR-872" Then
                    e.DisplayText = "Bio"
                ElseIf view.GetListSourceRowCellValue(e.ListSourceRowIndex, "DeviceType").ToString().Trim = "ZK(TFT)" Or view.GetListSourceRowCellValue(e.ListSourceRowIndex, "DeviceType").ToString().Trim = "Bio-1Pro/ATF305Pro/ATF686Pro" Then
                    e.DisplayText = "ZK"
                ElseIf view.GetListSourceRowCellValue(e.ListSourceRowIndex, "DeviceType").ToString().Trim = "F9" Then
                    e.DisplayText = "F9"
                End If
                e.DisplayText = (view.GetListSourceRowCellValue(e.ListSourceRowIndex, "SHIFTDURATION").ToString().Trim \ 60).ToString("00") & ":" & (view.GetListSourceRowCellValue(e.ListSourceRowIndex, "SHIFTDURATION").ToString().Trim Mod 60).ToString("00")
            End If
        Catch
        End Try
    End Sub
    Private Function DisableDevice(vnMachineNumber) As Boolean

        Dim bRet As Boolean = AxFP_CLOCK1.EnableDevice(vnMachineNumber, 0)
        If bRet Then
            Return True
        Else
            Return False
        End If
    End Function
    Private Sub btnDownloadFinger_Click(sender As System.Object, e As System.EventArgs) Handles btnDownloadFinger.Click
        If GridView1.GetSelectedRows.Count = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>Please select Device</size>", "<size=9>Error</size>")
            Exit Sub
        Else
            If ToggleDownloadAll.IsOn = False Then
                If TextSelectUser.Text = "" Then
                    XtraMessageBox.Show(ulf, "<size=10>Please Enter User</size>", "<size=9>Error</size>")
                    Exit Sub
                End If
            End If
            Me.Cursor = Cursors.WaitCursor
            For Each c As Control In Me.Controls
                c.Enabled = False
            Next

            Dim paycodelist As New List(Of String)()
            Dim paycodeArray '= paycodelist.Distinct.ToArray ' paycodelist.ToArray
            Dim failIP As New List(Of String)()
            Dim commkey As Integer
            Dim selectedRows As Integer() = GridView1.GetSelectedRows()
            Dim result As Object() = New Object(selectedRows.Length - 1) {}
            For i As Integer = 0 To selectedRows.Length - 1
                XtraMasterTest.LabelControlStatus.Text = ""
                XtraMasterTest.LabelControlCount.Text = ""
                Application.DoEvents()
                Dim rowHandle As Integer = selectedRows(i)
                If Not GridView1.IsGroupRow(rowHandle) Then
                    result(i) = GridView1.GetRowCellValue(rowHandle, "ID_NO")
                    vpszIPAddress = Trim(GridView1.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim)
                    XtraMasterTest.LabelControlStatus.Text = "Connecting " & vpszIPAddress & "..."
                    Application.DoEvents()
                    Common.LogPost("Device User Download; Device ID: " & result(i))
                    Dim Emachinenumber As Integer = result(i)
                    Dim A_R As String = GridView1.GetRowCellValue(rowHandle, "A_R") 'GridView1.GetRowCellValue(rowHandle, GridView1.Columns("A_R"))
                    If GridView1.GetRowCellValue(rowHandle, "DeviceType") = "Bio-1/3/4/5/6/7/ATF-203/ATF-395/ACR-872" Then
                        vpszNetPort = CLng("5005")
                        vpszNetPassword = CLng("0")
                        vnTimeOut = CLng("5000")
                        vnProtocolType = 0
                        vnLicense = 1261
                        If A_R = "S" Then
                            vnResult = FK_ConnectUSB(GridView1.GetRowCellValue(rowHandle, "ID_NO").ToString.Trim, vnLicense)
                        Else
                            vnResult = FK_ConnectNet(GridView1.GetRowCellValue(rowHandle, "ID_NO").ToString.Trim, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
                        End If
                        If vnResult > 0 Then
                            Dim vstrData As String = New String(CType(ChrW(32), Char), 256)
                            Dim ret = FK_GetProductData(vnResult, CType(1, Integer), vstrData)
                            If license.LicenseKey <> "" Then
                                If license.DeviceSerialNo.Contains(vstrData.Trim) Then
                                Else
                                    FK_DisConnect(vnResult)
                                    'Return "Invalid Serial number " & vstrData
                                    XtraMessageBox.Show(ulf, "<size=10>Invalid Serial number " & vstrData & "</size>", "Failed")
                                    Continue For
                                End If
                            End If
                            If ToggleDownloadAll.IsOn = False Then
                                cmdGetAllEnrollData_BIO_ONE(result(i), TextSelectUser.Text.Trim, paycodelist, vnResult)
                            Else
                                cmdGetAllEnrollData_BIO_all(result(i), vnResult, paycodelist)
                            End If
                            Dim vRet = FK_EnableDevice(vnResult, 1)
                            FK_DisConnect(vnResult)
                        Else
                            failIP.Add(vpszIPAddress.ToString)
                        End If
                    ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "ZK(TFT)" Or GridView1.GetRowCellValue(rowHandle, "DeviceType") = "Bio-1Pro/ATF305Pro/ATF686Pro" Then

                        'Dim sdwEnrollNumber As Integer
                        Dim sdwEnrollNumber As String
                        'Dim idwVerifyMode As Integer
                        'Dim idwWorkcode As Integer
                        commkey = Convert.ToInt32(GridView1.GetRowCellValue(rowHandle, "commkey"))
                        Dim bIsConnected = False
                        Dim iMachineNumber As Integer = result(i)
                        Dim idwErrorCode As Integer
                        Dim com As Common = New Common
                        Dim axCZKEM1 As New zkemkeeper.CZKEM

                        Dim sName As String = ""
                        Dim sPassword As String = ""
                        Dim iPrivilege As Integer
                        Dim bEnabled As Boolean = False
                        Dim idwFingerIndex As Integer
                        Dim sTmpData As String = ""
                        Dim iTmpLength As Integer
                        Dim iFlag As Integer
                        Dim flagenabled As String = ""
                        Dim adap As SqlDataAdapter
                        Dim adapA As OleDbDataAdapter
                        Dim dsRecord As DataSet = New DataSet
                        axCZKEM1.SetCommPassword(commkey)  'to check device commkey and db commkey matches
                        bIsConnected = axCZKEM1.Connect_Net(vpszIPAddress, 4370)
                        'MsgBox(bIsConnected.ToString)
                        If bIsConnected = True Then
                            Dim lvDownload As System.Windows.Forms.ListView = New System.Windows.Forms.ListView
                            lvDownload.Items.Clear()
                            lvDownload.BeginUpdate()
                            axCZKEM1.EnableDevice(iMachineNumber, False)

                            Dim lvItem As New ListViewItem("Items", 0)
                            Dim sSql As String
                            If Common.servername = "Access" Then
                                If Common.con1.State <> ConnectionState.Open Then
                                    Common.con1.Open()
                                End If
                            Else
                                If Common.con.State <> ConnectionState.Open Then
                                    Common.con.Open()
                                End If
                            End If
                            If ToggleDownloadAll.IsOn = False Then
                                sdwEnrollNumber = TextSelectUser.Text.Trim
                                While axCZKEM1.SSR_GetUserInfo(iMachineNumber, sdwEnrollNumber, sName, sPassword, iPrivilege, bEnabled) = True
                                    saveTemplate(iMachineNumber, sdwEnrollNumber, sName, sPassword, iPrivilege, bEnabled, axCZKEM1, lvItem, lvDownload, Emachinenumber)
                                    paycodelist.Add(sdwEnrollNumber & ";" & sName)
                                    Exit While
                                End While
                            Else
                                XtraMasterTest.LabelControlStatus.Text = "Reading all data from " & vpszIPAddress & "..."
                                Application.DoEvents()
                                Dim userCount As Integer = 0
                                axCZKEM1.GetDeviceStatus(iMachineNumber, 2, userCount)
                                axCZKEM1.ReadAllUserID(iMachineNumber) 'read all the user information to the memory
                                axCZKEM1.ReadAllTemplate(iMachineNumber) 'read all the users' fingerprint templates to the memory
                                Dim count As Integer = 0
                                While axCZKEM1.SSR_GetAllUserInfo(iMachineNumber, sdwEnrollNumber, sName, sPassword, iPrivilege, bEnabled) = True  'get all the users' information from the memory
                                    XtraMasterTest.LabelControlStatus.Text = "Downloading Template " & sdwEnrollNumber & " from Device No. " & Emachinenumber
                                    XtraMasterTest.LabelControlCount.Text = "Downloading " & count + 1 & " of " & userCount
                                    Application.DoEvents()
                                    saveTemplate(iMachineNumber, sdwEnrollNumber, sName, sPassword, iPrivilege, bEnabled, axCZKEM1, lvItem, lvDownload, Emachinenumber)
                                    paycodelist.Add(sdwEnrollNumber & ";" & sName)
                                    count = count + 1
                                End While
                            End If

                            If Common.servername = "Access" Then
                                If Common.con1.State <> ConnectionState.Closed Then
                                    Common.con1.Close()
                                End If
                            Else
                                If Common.con.State <> ConnectionState.Closed Then
                                    Common.con.Close()
                                End If
                            End If
                            axCZKEM1.EnableDevice(iMachineNumber, True)
                            axCZKEM1.Disconnect()
                        Else
                            axCZKEM1.GetLastError(idwErrorCode)
                            failIP.Add(vpszIPAddress.ToString)
                            Continue For
                        End If
                    ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "Bio2+" Then
                        'XtraMasterTest.LabelControlStatus.Text = "Connecting " & vpszIPAddress & "..."
                        'Application.DoEvents()
                        commkey = Convert.ToInt32(GridView1.GetRowCellValue(rowHandle, "commkey"))
                        Dim deviceG2 As Riss.Devices.Device
                        Dim deviceConnectionG2 As Riss.Devices.DeviceConnection
                        deviceG2 = New Riss.Devices.Device()
                        deviceG2.DN = 1 'CInt(nud_DN.Value)
                        deviceG2.Password = commkey ' TextEditComm.Text.Trim ' nud_Pwd.Value.ToString()
                        deviceG2.Model = "ZDC2911"
                        deviceG2.ConnectionModel = 5
                        If A_R = "S" Then
                            deviceG2.CommunicationType = CommunicationType.Usb
                        Else
                            deviceG2.IpAddress = vpszIPAddress 'TextEdit2.Text.Trim()
                            deviceG2.IpPort = 5500 'CInt(nud_Port.Value)
                            deviceG2.CommunicationType = CommunicationType.Tcp
                        End If
                        deviceConnectionG2 = DeviceConnection.CreateConnection(deviceG2)
                        If deviceConnectionG2.Open() > 0 Then
                            XtraMasterTest.LabelControlStatus.Text = "Reading all data from " & vpszIPAddress & "..."
                            Application.DoEvents()

                            Dim extraProperty As New Object()
                            Dim extraData As New Object()
                            extraData = 1 ' Globalss.DeviceBusy
                            Try
                                Dim result1 As Boolean = deviceConnectionG2.SetProperty(DeviceProperty.Enable, extraProperty, deviceG2, extraData)
                                extraProperty = 0UL
                                extraData = UInt16.MaxValue
                                result1 = deviceConnectionG2.GetProperty(DeviceProperty.Enrolls, extraProperty, deviceG2, extraData)
                                If result1 Then
                                    'If btn_Select.Text.Equals("Deselect All") Then
                                    '    btn_Select.Text = "Select All"
                                    'End If
                                    'lvw_GLogList.Items.Clear()
                                    Dim userList As List(Of User) = DirectCast(extraData, List(Of User))
                                    Dim no As Integer = 1

                                    If Common.servername = "Access" Then
                                        If Common.con1.State <> System.Data.ConnectionState.Open Then
                                            Common.con1.Open()
                                        End If
                                    Else
                                        If Common.con.State <> System.Data.ConnectionState.Open Then
                                            Common.con.Open()
                                        End If
                                    End If

                                    For Each user As User In userList  'get all users
                                        Dim enroll As Enroll = user.Enrolls(0)
                                        'Dim item As New ListViewItem(no.ToString())
                                        'item.SubItems.Add(user.DIN.ToString())
                                        'item.SubItems.Add(user.UserName)

                                        Try 'get users all details
                                            Dim extraProperty1 As Object = New Object
                                            Dim extraData1 As Object = New Object
                                            Dim user1 As User = New User
                                            user1.DIN = user.DIN 'CType(nud_DIN.Value, UInt64)
                                            'Thread.Sleep(100)
                                            result1 = deviceConnectionG2.GetProperty(UserProperty.Enroll, extraProperty1, user1, extraData1)
                                            'Dim result As Boolean = deviceConnectionG2.GetProperty(UserProperty.UserEnroll, extraProperty, user, extraData)
                                            If result1 Then
                                                Dim Bio2 As Bio2EnrollData = New Bio2EnrollData
                                                Bio2.EnrollNumber = user1.DIN
                                                Bio2.Privilege = (user1.Privilege / 2)
                                                Bio2.Password = user1.Enrolls(0).Password
                                                Bio2.Cardnumber = user1.Enrolls(0).CardID
                                                Bio2.UserName = user1.UserName
                                                'ExtInfoTextBox.Text = user1.Comment
                                                Bio2.Enable = Convert.ToInt32(user1.Enable)
                                                'userAttTypeIdNumericUpDown.Value = user1.AttType
                                                'userAccessControlComboBox.SelectedIndex = user1.AccessControl
                                                'userPassZoneNumericUpDown.Value = user1.AccessTimeZone
                                                'userDeptIdNumericUpDown.Value = user1.Department
                                                'userGroupIdNumericUpDown.Value = user1.UserGroup
                                                'userValidityPeriodComboBox.SelectedIndex = Convert.ToInt32(user1.ValidityPeriod)
                                                'userStartDateTimePicker.Value = user1.ValidDate
                                                'userEndDateTimePicker.Value = user1.InvalidDate


                                                Dim extraProperty2 As New Object()  'get finger template
                                                Dim extraData2 As New Object()
                                                extraData2 = ConvertObject.DeviceBusy

                                                Try
                                                    Dim result2 As Boolean = deviceConnectionG2.SetProperty(DeviceProperty.Enable, extraProperty2, deviceG2, extraData2)
                                                    Dim user2 As New User()
                                                    Dim enroll2 As New Enroll()
                                                    user2.DIN = user.DIN '.ToString()
                                                    enroll2.DIN = user.DIN
                                                    Dim TemplateLst As New List(Of String)()
                                                    For f As Integer = 0 To 9
                                                        enroll2.EnrollType = DirectCast(f, EnrollType)
                                                        enroll2.Fingerprint = New Byte(Zd2911Utils.MaxFingerprintLength - 1) {}
                                                        user2.Enrolls.Add(enroll2)
                                                        extraProperty2 = UserEnrollCommand.ReadFingerprint
                                                        result2 = deviceConnectionG2.GetProperty(UserProperty.UserEnroll, extraProperty2, user2, extraData2)
                                                        If result2 Then
                                                            Dim fpBytes As Byte()
                                                            fpBytes = user2.Enrolls(0).Fingerprint
                                                            TemplateLst.Add(ConvertObject.ConvertByteToHex(fpBytes).Replace("-", " ") & ";" & f)
                                                        Else
                                                            'MessageBox.Show("Read FP Data Fail", "Prompt", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                                                        End If
                                                    Next
                                                    Bio2.Template = TemplateLst.ToArray
                                                Catch ex As Exception
                                                    'MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.[Error])
                                                Finally
                                                    'extraData = ConvertObject.DeviceIdle
                                                    'deviceConnectionG2.SetProperty(DeviceProperty.Enable, extraProperty, deviceG2, extraData)
                                                End Try
                                                'Dim fpBytes As Byte()
                                                'Array.Copy(user1.Enrolls(0).Fingerprint, fpBytes, Zd2911Utils.MaxFingerprintLength)
                                                'Bio2.Template = ConvertObject.ConvertByteToHex(fpBytes).Replace("-", " ")
                                                Bio2.EMachineNumber = Emachinenumber
                                                SaveBio2FPData(Bio2)
                                                'shareUser = user1
                                            Else
                                                MessageBox.Show("Get user detail info. Fail", "Prompt", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                                                'Return
                                            End If

                                        Catch ex As Exception
                                            'MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                                        End Try
                                        'Dim privilege As String = ConvertObject.GetUserPrivilege(DirectCast(user.Privilege, UserPrivilege))
                                        'item.SubItems.Add(privilege)
                                        'lvw_GLogList.Items.Add(item)
                                        no += 1
                                    Next

                                    If Common.servername = "Access" Then
                                        If Common.con1.State <> System.Data.ConnectionState.Closed Then
                                            Common.con1.Close()
                                        End If
                                    Else
                                        If Common.con.State <> System.Data.ConnectionState.Open Then
                                            Common.con.Close()
                                        End If
                                    End If
                                Else
                                    deviceConnectionG2.Close()
                                    'Continue For
                                    'MessageBox.Show("Get All Enroll Information Fail", "Prompt", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                                End If
                            Catch ex As Exception
                                deviceConnectionG2.Close()
                                'Continue For
                                'MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.[Error])
                            Finally
                                extraData = 0 ' Globalss.DeviceIdle
                                deviceConnectionG2.SetProperty(DeviceProperty.Enable, extraProperty, deviceG2, extraData)
                            End Try
                            deviceConnectionG2.Close()
                        Else
                            failIP.Add(vpszIPAddress.ToString)
                            Continue For
                        End If

                    ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "Bio1Eco" Then
                        'XtraMasterTest.LabelControlStatus.Text = "Connecting " & vpszIPAddress & "..."
                        'Application.DoEvents()
                        Dim adap, adap1 As SqlDataAdapter
                        Dim adapA, adapA1 As OleDbDataAdapter
                        Dim ds, ds1 As DataSet
                        commkey = Convert.ToInt32(GridView1.GetRowCellValue(rowHandle, GridView1.Columns("commkey")))
                        Dim mOpenFlag As Boolean
                        Dim mMK8001Device As Boolean 'TRUE:MK8001/8002Device;False:else

                        mMK8001Device = False
                        Dim si As SySystemInfo = New SySystemInfo
                        si.cSerialNum = New Byte((20) - 1) {}
                        si.cManuName = New Byte((24) - 1) {}
                        si.cDevName = New Byte((24) - 1) {}
                        si.cAlgVer = New Byte((16) - 1) {}
                        si.cFirmwareVer = New Byte((24) - 1) {}
                        Dim rret As Integer = 66
                        Try
                            rret = SyFunctions.CmdGetSystemInfo(si)
                        Catch ex As Exception

                        End Try

                        If (rret = CType(SyLastError.sleSuss, Int32)) Then
                            Dim sFirmwareVer As String = System.Text.Encoding.Default.GetString(si.cFirmwareVer)
                            If sFirmwareVer.Contains("MK8001/8002") Then
                                mMK8001Device = True
                            Else
                                mMK8001Device = False
                            End If
                        End If

                        Dim ret As Int32 = CType(SyLastError.sleInvalidParam, Int32)
                        Dim equNo As UInt32 = Convert.ToUInt32(Emachinenumber)
                        Dim gEquNo As UInt32 = equNo
                        If A_R = "T" Then
                            Dim netCfg As SyNetCfg
                            Dim pswd As UInt32 = 0
                            Dim tmpPswd As String = commkey
                            If (tmpPswd = "0") Then
                                tmpPswd = "FFFFFFFF"
                            End If
                            Try
                                pswd = UInt32.Parse(tmpPswd, NumberStyles.HexNumber)
                            Catch ex As Exception
                                XtraMessageBox.Show(ulf, "<size=10>Incorrect Comm Key</size>", "Failed")
                                ret = CType(SyLastError.slePasswordError, Int32)
                            End Try
                            If (ret <> CType(SyLastError.slePasswordError, Int32)) Then
                                netCfg.mIsTCP = 1
                                netCfg.mPortNo = 8000 'Convert.ToUInt16(txtPortNo.Text)
                                netCfg.mIPAddr = New Byte((4) - 1) {}
                                Dim sArray() As String = vpszIPAddress.Split(Microsoft.VisualBasic.ChrW(46))
                                Dim j As Byte = 0
                                For Each i1 As String In sArray
                                    Try
                                        netCfg.mIPAddr(j) = Convert.ToByte(i1.Trim)
                                    Catch ex As System.Exception
                                        netCfg.mIPAddr(j) = 255
                                    End Try
                                    j = (j + 1)
                                    If j > 3 Then
                                        Exit For
                                    End If

                                Next
                            End If

                            Dim pnt As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(netCfg))
                            Try
                                Marshal.StructureToPtr(netCfg, pnt, False)
                                '        If optWifiDevice.Checked Then
                                'ret = SyFunctions.CmdOpenDevice(3, pnt, equNo, pswd, Convert.ToByte(chkTranceIO.Checked?1, :, 0))
                                '        Else
                                ret = SyFunctions.CmdOpenDevice(2, pnt, equNo, pswd, Convert.ToByte(1))
                                '        End If
                                '                'TODO: Warning!!!, inline IF is not supported ?
                                '                chkTranceIO.Checked()
                                '0:
                                '                '
                            Finally
                                Marshal.FreeHGlobal(pnt)
                            End Try
                        ElseIf A_R = "S" Then
                            Dim equtype As String = "Finger Module USB Device"
                            Dim pswd As UInt32 = UInt32.Parse("FFFFFFFF", NumberStyles.HexNumber)
                            ret = SyFunctions.CmdOpenDeviceByUsb(equtype, equNo, pswd, Convert.ToByte(1))
                        End If
                        If (ret = CType(SyLastError.sleSuss, Int32)) Then
                            ret = SyFunctions.CmdTestConn2Device

                            Dim maxUserCnt As System.UInt16 = 0
                            ret = SyFunctions.CmdGetCount(maxUserCnt)
                            If ((maxUserCnt > 0) And (ret = CType(SyLastError.sleSuss, Int32))) Then
                                If Common.servername = "Access" Then
                                    If Common.con1.State <> System.Data.ConnectionState.Open Then
                                        Common.con1.Open()
                                    End If
                                Else
                                    If Common.con.State <> System.Data.ConnectionState.Open Then
                                        Common.con.Open()
                                    End If
                                End If
                                Dim aID() As UInt16 = New UInt16((maxUserCnt) - 1) {}
                                Array.Clear(aID, 0, aID.Length)
                                ret = SyFunctions.CmdGetAllValidUserID(aID, maxUserCnt)
                                Dim uie As SyUserInfoExt = New SyUserInfoExt
                                'Dim i As Integer = 1
                                If ToggleDownloadAll.IsOn = True Then
                                    For Each userid As UInt16 In aID
                                        If ((userid < 65535) And (userid > 0)) Then
                                            ret = SyFunctions.CmdGetUserInfo(uie, CType(userid, UInt16))
                                            'user info
                                            If (ret = CType(SyLastError.sleSuss, Int32)) Then
                                                SaveBio1EcoFPData(uie, Emachinenumber, mMK8001Device)
                                            Else
                                                'TODO: Warning!!! continue Else
                                            End If
                                        End If
                                        If ToggleCreateEmp.IsOn Then
                                            paycodelist.Add(userid & ";" & uie.sUserInfo.name)
                                        End If
                                        If (userid = 0) Then
                                            Exit For
                                        End If
                                        'Application.DoEvents()
                                    Next
                                Else
                                    Dim userid As Integer
                                    Try
                                        userid = Convert.ToInt32(TextSelectUser.Text.Trim)
                                    Catch ex As Exception
                                        XtraMessageBox.Show(ulf, "<size=10>Invalid User</size>", "<size=9>Error</size>")
                                        XtraMasterTest.LabelControlStatus.Text = ""
                                        Application.DoEvents()
                                        GoTo conclose
                                    End Try
                                    If ((userid < 65535) And (userid > 0)) Then
                                        ret = SyFunctions.CmdGetUserInfo(uie, CType(userid, UInt16))
                                        'user info
                                        If (ret = CType(SyLastError.sleSuss, Int32)) Then
                                            SaveBio1EcoFPData(uie, Emachinenumber, mMK8001Device)
                                        Else
                                            'TODO: Warning!!! continue Else
                                        End If
                                        If ToggleCreateEmp.IsOn Then
                                            paycodelist.Add(userid & ";" & uie.sUserInfo.name)
                                        End If
                                    End If
                                    If (userid = 0) Then
                                        Exit For
                                    End If
                                    'Application.DoEvents()

                                End If

conclose:                       If Common.servername = "Access" Then
                                    If Common.con1.State <> System.Data.ConnectionState.Closed Then
                                        Common.con1.Close()
                                    End If
                                Else
                                    If Common.con.State <> System.Data.ConnectionState.Closed Then
                                        Common.con.Close()
                                    End If
                                End If
                            End If

                            SyFunctions.CloseCom()
                            ' close com 
                            SyFunctions.CmdUnLockDevice()
                            SyFunctions.CloseCom()
                        Else
                            failIP.Add(vpszIPAddress.ToString)
                            'XtraMessageBox.Show(ulf, "<size=10>Connection Failed</size>", "Failed")
                            Continue For
                        End If

                    ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "EF45" Then
                        'XtraMasterTest.LabelControlStatus.Text = "Connecting " & vpszIPAddress & "..."
                        'Application.DoEvents()
                        Dim cn As Common = New Common
                        Dim _client As Client = Nothing
                        Dim connectOk As Boolean = False
                        Try
                            _client = cn.initClientEF45(vpszIPAddress)
                            connectOk = _client.StealConnect '_client.Connect()
                            If connectOk Then
                                Dim sSql As String
                                Dim adapA As OleDbDataAdapter
                                Dim adap As SqlDataAdapter
                                Dim dsRecord As DataSet
                                'Dim temp As String
                                Dim sdwEnrollNumber As String
                                If ToggleDownloadAll.IsOn = False Then
                                    sdwEnrollNumber = TextSelectUser.Text.Trim
                                    Dim userInfo As CMITech.UMXClient.Entities.UserInfo = New CMITech.UMXClient.Entities.UserInfo
                                    userInfo = _client.GetUserInfoByUUID(sdwEnrollNumber)
                                    Try
                                        Dim UserName As String = ""
                                        SaveEF45Template(userInfo, Emachinenumber, _client, UserName)
                                        If ToggleCreateEmp.IsOn Then
                                            paycodelist.Add(userInfo.UUID & ";" & UserName)
                                        End If
                                    Catch ex As Exception

                                    End Try
                                Else
                                    Dim allUserInfo As List(Of CMITech.UMXClient.Entities.UserInfo) = _client.GetAllUserInfo
                                    For Each userinfo As CMITech.UMXClient.Entities.UserInfo In allUserInfo
                                        Try
                                            Dim UserName As String = ""
                                            SaveEF45Template(userinfo, Emachinenumber, _client, UserName)
                                            If ToggleCreateEmp.IsOn Then
                                                paycodelist.Add(userinfo.UUID & ";" & UserName)
                                            End If
                                        Catch ex As Exception

                                        End Try
                                    Next
                                End If

                                _client.Disconnect()
                            Else
                                failIP.Add(vpszIPAddress.ToString)
                                Continue For
                            End If
                        Catch ex As Exception
                            failIP.Add(vpszIPAddress.ToString)
                            Continue For
                        End Try
                    ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "F9" Then
                        vpszNetPort = CLng("5005")
                        vpszNetPassword = CLng("0")
                        vnTimeOut = CLng("5000")
                        vnProtocolType = 0
                        vnLicense = 7881
                        Dim F9 As mdlPublic_f9 = New mdlPublic_f9
                        If A_R = "S" Then
                            vnResult = F9.FK_ConnectUSB(GridView1.GetRowCellValue(rowHandle, "ID_NO").ToString.Trim, vnLicense)
                        Else
                            vnResult = F9.FK_ConnectNet(GridView1.GetRowCellValue(rowHandle, "ID_NO").ToString.Trim, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
                        End If
                        If vnResult > 0 Then
                            If ToggleDownloadAll.IsOn = False Then
                                cmdGetAllEnrollData_F9_ONE(result(i), TextSelectUser.Text.Trim, paycodelist, F9, vnResult)
                            Else
                                cmdGetAllEnrollData_F9_all(result(i), vnResult, paycodelist, F9)
                            End If

                        Else
                            failIP.Add(vpszIPAddress.ToString)
                        End If

                    ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "ATF686n" Then
                        vpszNetPort = CLng("5005")
                        vpszNetPassword = CLng("0")
                        vnTimeOut = CLng("5000")
                        vnProtocolType = 0
                        vnLicense = 7881
                        Dim atf686n As mdlFunction_Atf686n = New mdlFunction_Atf686n
                        If A_R = "S" Then
                            vnResult = atf686n.ST_ConnectUSB(GridView1.GetRowCellValue(rowHandle, "ID_NO").ToString.Trim, vnLicense)
                        Else
                            vnResult = atf686n.ConnectNet(vpszIPAddress)
                        End If
                        If vnResult > 0 Then
                            If ToggleDownloadAll.IsOn = False Then
                                cmdGetAllEnrollData_ATF686n_ONE(result(i), TextSelectUser.Text.Trim, atf686n, vnResult)
                            Else
                                cmdGetAllEnrollData_ATF686n_ONE(result(i), "", atf686n, vnResult)
                            End If

                        Else
                            failIP.Add(vpszIPAddress.ToString)
                        End If
                        atf686n.ST_DisConnect(vnResult)
                    ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "TF-01" Then
                        vpszNetPort = CLng("5005")
                        vpszNetPassword = CLng("0")
                        vnTimeOut = CLng("5000")
                        vnProtocolType = 0
                        vnLicense = 7881
                        Dim bRet As Boolean
                        Dim vnMachineNumber As Integer = 1

                        bRet = AxFP_CLOCK1.SetIPAddress(vpszIPAddress, vpszNetPort, vpszNetPassword)
                        Me.AxFP_CLOCK1.OpenCommPort(vnMachineNumber)
                        If Not bRet Then
                            failIP.Add(vpszIPAddress.ToString)
                            XtraMessageBox.Show(ulf, "<size=10>Connection Failed</size>", "Failed")
                            Me.Cursor = Cursors.Default
                            Exit Sub
                        Else

                            bRet = AxFP_CLOCK1.EnableDevice(vnMachineNumber, False)
                            If bRet Then
                                If ToggleDownloadAll.IsOn = False Then
                                    cmdGetAllEnrollData_TF01_ONE(vnMachineNumber, "", paycodelist, vnResult)
                                    'cmdGetAllEnrollData_ATF686n_ONE(result(i), TextSelectUser.Text.Trim, atf686n, vnResult)
                                Else
                                    cmdGetAllEnrollData_TF01_ONE(vnMachineNumber, TextSelectUser.Text.Trim, paycodelist, vnResult)
                                End If

                            End If
                        End If
                        bRet = AxFP_CLOCK1.EnableDevice(vnMachineNumber, True)
                        AxFP_CLOCK1.CloseCommPort()
                    ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "Ultra800" Then
                        If license.LicenseKey <> "" Then
                            If license.DeviceSerialNo.Contains(GridView1.GetRowCellValue(rowHandle, GridView1.Columns("MAC_ADDRESS")).ToString.Trim) Then
                            Else
                                XtraMessageBox.Show(ulf, "<size=10>Invalid Serial number " & GridView1.GetRowCellValue(rowHandle, GridView1.Columns("MAC_ADDRESS")).ToString.Trim & "</size>", "Failed")
                                Continue For
                            End If
                        End If
                        Dim commKey1 As String = GridView1.GetRowCellValue(rowHandle, GridView1.Columns("commkey")).ToString.Trim
                        Dim paycodelistTmp As New List(Of String)()
                        Dim index As Integer = 0
                        'get all users by logs
                        XtraMasterTest.LabelControlStatus.Text = "Reading Users from Device No. " & Emachinenumber
                        Application.DoEvents()

                        If ToggleDownloadAll.IsOn = False Then
                            paycodelistTmp.Add(TextSelectUser.Text.Trim)
                            GoTo faceDwnload
                        End If

calllink:               Dim link As String = "http://" & vpszIPAddress & ":8090/newFindRecords?pass=" & commKey1 & "&personId=-1&startTime=0&endTime=0&index=" & index & "&length=100&order=1"
                        Dim proxy As WebClient = New WebClient
                        Dim serviceURL As String = String.Format(link)
                        Dim data() As Byte = proxy.DownloadData(serviceURL)
                        Dim stream As Stream = New MemoryStream(data)
                        Dim OutPut As String = System.Text.Encoding.UTF8.GetString(data)
                        Dim AttD As AttlogUltra800 = New AttlogUltra800
                        AttD = JsonConvert.DeserializeObject(Of AttlogUltra800)(OutPut)
                        If AttD.success = True Then
                            For v As Integer = 0 To AttD.data.records.Count - 1
                                XtraMasterTest.LabelControlStatus.Text = "Reading Users from Device No. " & Emachinenumber
                                Application.DoEvents()
                                If AttD.data.records(v).personId <> "" And AttD.data.records(v).personId <> "STRANGERBABY" Then
                                    paycodelistTmp.Add(AttD.data.records(v).personId)
                                    paycodelist.Add(AttD.data.records(v).personId & ";" & "")
                                End If
                            Next
                        End If
                        If AttD.data.pageInfo.index < AttD.data.pageInfo.size - 1 Then
                            index = index + 1
                            GoTo calllink
                        End If
                        'get face for each user
faceDwnload:            Dim paycodeUltra800() As String = paycodelistTmp.Distinct.ToArray
                        For v As Integer = 0 To paycodeUltra800.Count - 1
#Region "Get Face"
                            XtraMasterTest.LabelControlStatus.Text = "Downloading Template " & paycodeUltra800(v) & " from Device No. " & Emachinenumber
                            XtraMasterTest.LabelControlCount.Text = "Downloading " & v + 1 & " of " & paycodeUltra800.Count
                            Application.DoEvents()

                            link = "http://" & vpszIPAddress & ":8090/face/find?pass=" & commKey1 & "&personId=" & paycodeUltra800(v)
                            Dim httpWebRequest As WebRequest = WebRequest.Create(link) 'Live
                            httpWebRequest.ContentType = "application/json"
                            httpWebRequest.Method = "POST"
                            Dim httpResponse = CType(httpWebRequest.GetResponse(), HttpWebResponse)
                            Using streamReader = New StreamReader(httpResponse.GetResponseStream())
                                'Dim streamReader = New StreamReader(httpResponse.GetResponseStream)
                                Dim result1 = streamReader.ReadToEnd()

                                Dim FData As FaceUltra800 = New FaceUltra800
                                FData = JsonConvert.DeserializeObject(Of FaceUltra800)(result1)

                                If FData.msg = "Photo query success" Then
                                    'Dim bArr As Byte() = Nothing
                                    If FData.data(0).feature <> "" Then
                                        'bArr = Convert.FromBase64String(FData.data(0).feature)
                                        Dim sdwEnrollNumber As String = ""
                                        Dim iFaceIndex As Integer = 50  'the only possible parameter value
                                        Dim idwFingerIndex As Integer = iFaceIndex
                                        'axCZKEM1.GetUserFaceStr(iMachineNumber, sEnrollNumber, iFaceIndex, sTmpData, iLength)


                                        Dim reqFTP As FtpWebRequest
                                        Dim filePath As String = My.Application.Info.DirectoryPath & "\EmpImages" & "\" & FData.data(0).personId & "_" & Now.ToString("HH") & ".jpg"
                                        Dim outputStream As FileStream = New FileStream(filePath, FileMode.Create)
                                        reqFTP = CType(FtpWebRequest.Create(New Uri(FData.data(0).path)), FtpWebRequest) 'Hikvision
                                        reqFTP.Method = WebRequestMethods.Ftp.DownloadFile
                                        reqFTP.UseBinary = True
                                        reqFTP.Credentials = New NetworkCredential("admin", commKey1)
                                        Dim response As FtpWebResponse = CType(reqFTP.GetResponse, FtpWebResponse)
                                        Dim ftpStream As Stream = response.GetResponseStream
                                        Dim cl As Long = response.ContentLength
                                        Dim bufferSize As Integer = 25000 ' 2048
                                        Dim readCount As Integer
                                        Dim buffer() As Byte = New Byte((bufferSize) - 1) {}
                                        readCount = ftpStream.Read(buffer, 0, bufferSize)

                                        While (readCount > 0)
                                            outputStream.Write(buffer, 0, readCount)
                                            readCount = ftpStream.Read(buffer, 0, bufferSize)
                                            Application.DoEvents()
                                        End While
                                        'Dim x As Integer = 0
                                        ftpStream.Close()
                                        outputStream.Close()
                                        response.Close()

                                        'Dim destFilePath As String = My.Application.Info.DirectoryPath & "\EmpImages" & "\" & FData.data(0).personId & "_" & Now.ToString("HH") & "_.jpg"
                                        'Dim selectedImage As Image = Image.FromFile(filePath) 'Image.FromFile(OpenFileDialog1.FileName)
                                        'CompressAndSaveImage(selectedImage, destFilePath, 50)

                                        Dim imageArray() As Byte = System.IO.File.ReadAllBytes(filePath)
                                        Dim base64ImageRepresentation As String = Convert.ToBase64String(imageArray)

                                        If IsNumeric(FData.data(0).personId) = True Then
                                            sdwEnrollNumber = Convert.ToDouble(FData.data(0).personId)
                                            sdwEnrollNumber = Convert.ToDouble(sdwEnrollNumber).ToString("000000000000")
                                        End If


                                        Dim sSql As String = "select Enrollnumber from fptable where Enrollnumber='" & sdwEnrollNumber & "' and FingerNumber=" & idwFingerIndex & " and DeviceType ='Ultra800' and EMachineNumber=" & Emachinenumber & " and Ultra800FId='" & FData.data(0).faceId & "'"
                                        Dim dsRecord As DataSet = New DataSet
                                        If Common.servername = "Access" Then
                                            Dim adapA As OleDbDataAdapter = New OleDbDataAdapter(sSql, Common.con1)
                                            adapA.Fill(dsRecord)
                                        Else
                                            Dim adap As SqlDataAdapter = New SqlDataAdapter(sSql, Common.con)
                                            adap.Fill(dsRecord)
                                        End If
                                        If dsRecord.Tables(0).Rows.Count > 0 Then
                                            sSql = "Update FPTABLE set Template = '" & base64ImageRepresentation & "', UserName='', Privilege=0 where Enrollnumber = '" & sdwEnrollNumber & "' and FingerNumber=" & idwFingerIndex.ToString() & " and DeviceType ='Ultra800' and EMachineNumber=" & Emachinenumber & ""
                                        Else
                                            sSql = "insert into fptable (Emachinenumber, EnrollNumber, UserName, Template, FingerNumber, Privilege, DeviceType, Ultra800FId) values (" & Emachinenumber & ",'" & sdwEnrollNumber & "', '','" & base64ImageRepresentation & "', " & idwFingerIndex & ", 0,'Ultra800','" & FData.data(0).faceId & "')"
                                        End If
                                        If Common.servername = "Access" Then
                                            If Common.con1.State <> ConnectionState.Open Then
                                                Common.con1.Open()
                                            End If
                                            cmd1 = New OleDbCommand(sSql, Common.con1)
                                            cmd1.ExecuteNonQuery()
                                            If Common.con1.State <> ConnectionState.Closed Then
                                                Common.con1.Close()
                                            End If
                                        Else
                                            If Common.con.State <> ConnectionState.Open Then
                                                Common.con.Open()
                                            End If
                                            cmd = New SqlCommand(sSql, Common.con)
                                            cmd.ExecuteNonQuery()
                                            If Common.con.State <> ConnectionState.Closed Then
                                                Common.con.Close()
                                            End If
                                        End If


                                    End If
                                End If
                                Try
                                    Dim fs As FileStream = New FileStream("JsonResponse.txt", FileMode.OpenOrCreate, FileAccess.Write)
                                    Dim sw As StreamWriter = New StreamWriter(fs)
                                    sw.BaseStream.Seek(0, SeekOrigin.End)
                                    sw.WriteLine(vbCrLf & link & ", " & FData.data(0).personId & "::" & DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"))
                                    sw.Flush()
                                    sw.Close()
                                Catch ex As Exception

                                End Try

                            End Using


#End Region
                        Next
                    Else
                            result(0) = -1
                        End If
                    End If
                Next

            If ToggleCreateEmp.IsOn = True Then
                paycodeArray = paycodelist.Distinct.ToArray ' paycodelist.ToArray
                Common.CreateEmployee(paycodeArray, "Device")
                Common.loadEmp()
            End If

            For Each c As Control In Me.Controls
                c.Enabled = True
            Next
            XtraMasterTest.LabelControlCount.Text = ""
            XtraMasterTest.LabelControlStatus.Text = ""
            Application.DoEvents()

            Me.Cursor = Cursors.Default
            'If Common.servername = "Access" Then
            '    Me.Fptable1TableAdapter1.Fill(Me.SSSDBDataSet.fptable1)
            '    GridControl2.DataSource = SSSDBDataSet.fptable1
            'Else
            '    FptableTableAdapter.Connection.ConnectionString = Common.ConnectionString
            '    Me.FptableTableAdapter.Fill(Me.SSSDBDataSet.fptable)
            '    GridControl2.DataSource = SSSDBDataSet.fptable
            'End If
            LoadFPGrid()
            Dim failedIpArr() As String = failIP.ToArray
            Dim failIpStr As String = ""
            If failedIpArr.Length > 0 Then
                For i As Integer = 0 To failedIpArr.Length - 1
                    failIpStr = failIpStr & "," & failedIpArr(i)
                Next
                failIpStr = failIpStr.TrimStart(",")
                XtraMessageBox.Show(ulf, "<size=10>" & failIpStr & " failed to download data</size>", "Failed")
            Else
                XtraMessageBox.Show(ulf, "<size=10>Task Completed</size>", "Success")
            End If

            'XtraMessageBox.Show(ulf, "<size=10>Task Completed</size>", "<size=9>Information</size>")
        End If
    End Sub
    Private Sub cmdGetAllEnrollData_ATF686n_ONE(mMachine_ID As Long, mEnrollNumber As String, atf686n As mdlFunction_Atf686n, MachineIndex As Long)
        Dim vnResultCode As Integer
        Dim vStrEnrollNumber As String = ""
        Dim vnInfoSize As Integer = Marshal.SizeOf(GetType(ENROLL_DATA_STRING_ID))
        Dim bytEnrollData(vnInfoSize - 1) As Byte

        Dim paycodelist As New List(Of String)()
        Dim paycodelistCrateImp As New List(Of String)()
        If mEnrollNumber = "" Then
            vnResultCode = atf686n.ST_ReadAllUserID(MachineIndex)
            If vnResultCode <> RUN_SUCCESS Then
                atf686n.ST_EnableDevice(MachineIndex, 1)
            End If
            If atf686n.ST_GetIsSupportStringID(MachineIndex) = RUN_SUCCESS Then
                Dim vEnrollNumber As Integer, vBackupNumber As Integer, vPrivilege As Integer, vnEnableFlag As Integer
                Do
                    vnResultCode = atf686n.ST_GetAllUserID_SID(MachineIndex, vStrEnrollNumber, vBackupNumber, vPrivilege, vnEnableFlag)
                    If vnResultCode <> RUN_SUCCESS Then
                        If vnResultCode = RUNERR_DATAARRAY_END Then
                            vnResultCode = RUN_SUCCESS
                        End If
                        Exit Do
                    End If
                    Try
                        vStrEnrollNumber = vStrEnrollNumber.Substring(0, vStrEnrollNumber.IndexOf(" ")).Trim
                    Catch ex As Exception

                    End Try
                    If vStrEnrollNumber = "" Then
                        Continue Do
                    End If
                    paycodelist.Add(vStrEnrollNumber)
                Loop
            End If
        Else
            paycodelist.Add(mEnrollNumber)
        End If
        Dim paycodeArr() As String = paycodelist.Distinct.ToArray()

        For i As Integer = 0 To paycodeArr.Length - 1

            'mEnrollNumber = paycodeArr(i).Trim
            mEnrollNumber = paycodeArr(i) '.Substring(0, paycodeArr(i).IndexOf(" "))
            XtraMasterTest.LabelControlStatus.Text = "Downloading Template " & mEnrollNumber & " From Device No. " & mMachine_ID
            Application.DoEvents()
            vnResultCode = atf686n.ST_EnableDevice(MachineIndex, 0)
            If vnResultCode <> CInt(enumErrorCode.RETURN_SUCCESS) Then
                'lblMessage.Text = GlobalConstants.gstrNoDevice
                'cmdGetEnrollData.Enabled = true;
                Return
            End If
            vStrEnrollNumber = mEnrollNumber
            If atf686n.ST_GetIsSupportStringID(MachineIndex) = CInt(enumErrorCode.RETURN_SUCCESS) Then
                vnResultCode = atf686n.ST_GetEnrollMainData_SID(MachineIndex, vStrEnrollNumber, bytEnrollData)
            End If
            If vnResultCode = CInt(enumErrorCode.RETURN_SUCCESS) Then
                Dim vobj As Object = ConvertByteArrayToStructure(bytEnrollData, GetType(ENROLL_DATA_STRING_ID))
                If vobj Is Nothing Then
                    Return
                End If

                Dim Emachinenumber As String = mMachine_ID
                'Dim EnrollNumber As String
                Dim UserName As String = ""
                'Dim Template_Tw As String
                Dim Template_Tw() As Byte
                'Dim FingerNumber As String
                Dim Privilege As String
                Dim Verifymode As String
                Dim Template_Face As String
                Dim Cardnumber As String = ""
                Dim Password As String = ""

                Dim mEnrollInfo As ENROLL_DATA_STRING_ID
                mEnrollInfo = DirectCast(vobj, ENROLL_DATA_STRING_ID)

                Privilege = mEnrollInfo.Privilege
                'If mEnrollInfo.Privilege = CInt(enumMachinePrivilege.MP_ALL) Then
                '    cmbPrivilege.SelectedIndex = 1
                'ElseIf mEnrollInfo.Privilege = CInt(enumMachinePrivilege.MP_NONE) Then
                '    cmbPrivilege.SelectedIndex = 0
                'ElseIf mEnrollInfo.Privilege = CInt(enumMachinePrivilege.MP_ROLE1) Then
                '    cmbPrivilege.SelectedIndex = 2
                'ElseIf mEnrollInfo.Privilege = CInt(enumMachinePrivilege.MP_ROLE2) Then
                '    cmbPrivilege.SelectedIndex = 3
                'ElseIf mEnrollInfo.Privilege = CInt(enumMachinePrivilege.MP_ROLE3) Then
                '    cmbPrivilege.SelectedIndex = 4
                'ElseIf mEnrollInfo.Privilege = CInt(enumMachinePrivilege.MP_ROLE4) Then
                '    cmbPrivilege.SelectedIndex = 5
                'ElseIf mEnrollInfo.Privilege = CInt(enumMachinePrivilege.MP_ROLE5) Then
                '    cmbPrivilege.SelectedIndex = 6
                'End If


                UserName = ByteArrayUtf16ToString(mEnrollInfo.UserName)
                paycodelistCrateImp.Add(mEnrollNumber & ";" & UserName)
                'lblMessage.Text = MainForm.ReturnResultPrint(vnResultCode)
                Dim vEnrollNumber As String = mEnrollNumber
                If IsNumeric(mEnrollNumber) Then
                    vEnrollNumber = Convert.ToDouble(mEnrollNumber).ToString("000000000000")
                End If
                Dim vBackupNumber As Integer = 0
#Region "CardPassword"
                Cardnumber = mEnrollInfo.Card.ToString()
                If Cardnumber.Trim <> "" And Cardnumber.Trim <> "0" Then
                    vBackupNumber = 21
                    Dim adap As SqlDataAdapter
                    Dim adapA As OleDbDataAdapter
                    Dim sSql As String = "select * from fptable where Enrollnumber='" & vEnrollNumber & "' and FingerNumber=" & vBackupNumber & " and DeviceType ='ATF686n' and EMachineNumber='" & mMachine_ID & "'"
                    'RsFp = Cn.Execute(sSql)
                    Dim RsFp As DataSet = New DataSet
                    If Common.servername = "Access" Then
                        sSql = "select * from fptable where Enrollnumber='" & vEnrollNumber & "' and FingerNumber=" & vBackupNumber & " and DeviceType ='ATF686n' and EMachineNumber=" & mMachine_ID & ""
                        adapA = New OleDbDataAdapter(sSql, Common.con1)
                        adapA.Fill(RsFp)
                    Else
                        adap = New SqlDataAdapter(sSql, Common.con)
                        adap.Fill(RsFp)
                    End If
                    If RsFp.Tables(0).Rows.Count > 0 Then
                        sSql = "DELETE FROM FPTABLE WHERE Enrollnumber='" & vEnrollNumber & "' and FingerNumber=" & vBackupNumber & " and DeviceType ='ATF686n' and EMachineNumber='" & mMachine_ID & "'"
                        'Cn.Execute(sSql)
                        If Common.servername = "Access" Then
                            sSql = "DELETE FROM FPTABLE WHERE Enrollnumber='" & vEnrollNumber & "' and FingerNumber=" & vBackupNumber & " and DeviceType ='ATF686n' and EMachineNumber=" & mMachine_ID & ""
                            If Common.con1.State <> ConnectionState.Open Then
                                Common.con1.Open()
                            End If
                            cmd1 = New OleDbCommand(sSql, Common.con1)
                            cmd1.ExecuteNonQuery()
                            If Common.con1.State <> ConnectionState.Closed Then
                                Common.con1.Close()
                            End If
                        Else
                            If Common.con.State <> ConnectionState.Open Then
                                Common.con.Open()
                            End If
                            cmd = New SqlCommand(sSql, Common.con)
                            cmd.ExecuteNonQuery()
                            If Common.con.State <> ConnectionState.Closed Then
                                Common.con.Close()
                            End If
                        End If

                    End If

                    Dim sqlinsert As String = "insert into fptable (Emachinenumber, EnrollNumber, UserName,  FingerNumber, Privilege, Cardnumber,  DeviceType) values (" & Emachinenumber & ",'" & vEnrollNumber & "', '" & UserName & "', '" & vBackupNumber & "', '" & Privilege & "', '" & Cardnumber & "','ATF686n')"
                    If Common.servername = "Access" Then
                        If Common.con1.State <> ConnectionState.Open Then
                            Common.con1.Open()
                        End If
                        cmd1 = New OleDbCommand(sqlinsert, Common.con1)
                        cmd1.ExecuteNonQuery()
                        If Common.con1.State <> ConnectionState.Closed Then
                            Common.con1.Close()
                        End If
                    Else
                        If Common.con.State <> ConnectionState.Open Then
                            Common.con.Open()
                        End If
                        cmd = New SqlCommand(sqlinsert, Common.con)
                        cmd.ExecuteNonQuery()
                        If Common.con.State <> ConnectionState.Closed Then
                            Common.con.Close()
                        End If
                    End If
                End If
                Password = mEnrollInfo.Password.ToString()
                If Password.Trim <> "" And Password.Trim <> "0" Then
                    vBackupNumber = 20
                    Dim adap As SqlDataAdapter
                    Dim adapA As OleDbDataAdapter
                    Dim sSql As String = "select * from fptable where Enrollnumber='" & vEnrollNumber & "' and FingerNumber=" & vBackupNumber & " and DeviceType ='ATF686n' and EMachineNumber='" & mMachine_ID & "'"
                    'RsFp = Cn.Execute(sSql)
                    Dim RsFp As DataSet = New DataSet
                    If Common.servername = "Access" Then
                        sSql = "select * from fptable where Enrollnumber='" & vEnrollNumber & "' and FingerNumber=" & vBackupNumber & " and DeviceType ='ATF686n' and EMachineNumber=" & mMachine_ID & ""
                        adapA = New OleDbDataAdapter(sSql, Common.con1)
                        adapA.Fill(RsFp)
                    Else
                        adap = New SqlDataAdapter(sSql, Common.con)
                        adap.Fill(RsFp)
                    End If
                    If RsFp.Tables(0).Rows.Count > 0 Then
                        sSql = "DELETE FROM FPTABLE WHERE Enrollnumber='" & vEnrollNumber & "' and FingerNumber=" & vBackupNumber & " and DeviceType ='ATF686n' and EMachineNumber='" & mMachine_ID & "'"
                        'Cn.Execute(sSql)
                        If Common.servername = "Access" Then
                            sSql = "DELETE FROM FPTABLE WHERE Enrollnumber='" & vEnrollNumber & "' and FingerNumber=" & vBackupNumber & " and DeviceType ='ATF686n' and EMachineNumber=" & mMachine_ID & ""
                            If Common.con1.State <> ConnectionState.Open Then
                                Common.con1.Open()
                            End If
                            cmd1 = New OleDbCommand(sSql, Common.con1)
                            cmd1.ExecuteNonQuery()
                            If Common.con1.State <> ConnectionState.Closed Then
                                Common.con1.Close()
                            End If
                        Else
                            If Common.con.State <> ConnectionState.Open Then
                                Common.con.Open()
                            End If
                            cmd = New SqlCommand(sSql, Common.con)
                            cmd.ExecuteNonQuery()
                            If Common.con.State <> ConnectionState.Closed Then
                                Common.con.Close()
                            End If
                        End If

                    End If

                    Dim sqlinsert As String = "insert into fptable (Emachinenumber, EnrollNumber, UserName,  FingerNumber, Privilege, Password,  DeviceType) values (" & Emachinenumber & ",'" & vEnrollNumber & "', '" & UserName & "', " & vBackupNumber & ", " & Privilege & ", '" & Password & "','ATF686n')"
                    If Common.servername = "Access" Then
                        If Common.con1.State <> ConnectionState.Open Then
                            Common.con1.Open()
                        End If
                        cmd1 = New OleDbCommand(sqlinsert, Common.con1)
                        cmd1.ExecuteNonQuery()
                        If Common.con1.State <> ConnectionState.Closed Then
                            Common.con1.Close()
                        End If
                    Else
                        If Common.con.State <> ConnectionState.Open Then
                            Common.con.Open()
                        End If
                        cmd = New SqlCommand(sqlinsert, Common.con)
                        cmd.ExecuteNonQuery()
                        If Common.con.State <> ConnectionState.Closed Then
                            Common.con.Close()
                        End If
                    End If
                End If
#End Region

                'txtFpIndex.Text = mEnrollInfo.FpCount.ToString()
                Dim VFACE_DATA_SIZE As Integer = 40080
                Dim mbytCurEnrollData(VFACE_DATA_SIZE - 1) As Byte
                Dim mnCurPassword As Integer = 0
                ' fp enroll data ===========================================================================
                If mEnrollInfo.FpCount > 0 Then
                    vBackupNumber = 0
                    Do While vBackupNumber <= CInt(enumBackupNumberType.BACKUP_FP_19)
                        Dim vPrivilege As Integer = 0
                        Dim nRet As Integer = atf686n.ST_GetEnrollData_SID(MachineIndex, vStrEnrollNumber, vBackupNumber, vPrivilege, mbytCurEnrollData, mnCurPassword)
                        If nRet = CInt(enumErrorCode.RETURN_SUCCESS) Then
                            saveEnrollDataToDB_Atf686n(vEnrollNumber, 0, vBackupNumber, vPrivilege, UserName, mbytCurEnrollData, Emachinenumber)
                        End If
                        vBackupNumber += 1
                    Loop
                End If
                '==============================================================================================

                ' face enroll data ===========================================================================
                If mEnrollInfo.FaceIndex > 0 Then
                    Dim vPrivilege As Integer = 0
                    Dim nRet As Integer = atf686n.ST_GetEnrollData_SID(MachineIndex, vStrEnrollNumber, CInt(enumBackupNumberType.BACKUP_FACE), vPrivilege, mbytCurEnrollData, mnCurPassword)
                    If nRet = CInt(enumErrorCode.RETURN_SUCCESS) Then
                        'saveEnrollDataToDB_Atf686n(vStrEnrollNumber, 0, CInt(enumBackupNumberType.BACKUP_FACE), vPrivilege, UserName, mbytCurEnrollData, Emachinenumber)
                        saveEnrollDataToDB_Atf686n(vEnrollNumber, 0, 22, vPrivilege, UserName, mbytCurEnrollData, Emachinenumber)
                    End If
                End If

                '==============================================================================================
            Else
                'lblMessage.Text = MainForm.ReturnResultPrint(vnResultCode)
            End If
        Next

        atf686n.ST_EnableDevice(MachineIndex, 1)

        Dim paycodeArrayCrateImp() As String
        If ToggleCreateEmp.IsOn = True Then
            paycodeArrayCrateImp = paycodelistCrateImp.ToArray ' paycodelist.ToArray
            Common.CreateEmployee(paycodeArrayCrateImp, "Device")
            Common.loadEmp()
        End If

    End Sub
    Private Sub saveEnrollDataToDB_Atf686n(ByVal vEnrollNumber As String, ByVal anEnrollNumber As Integer, ByVal anBackupNumber As Integer, ByVal anPrivilege As Integer, ByVal astrEnrollName As String, ByVal mbytCurEnrollData() As Byte, mMachine_ID As String)
        Dim FP_DATA_SIZE As Integer = 1680
        Dim FACE_DATA_SIZE As Integer = 20080
        Dim PWD_DATA_SIZE As Integer = 40
        Dim PALMVEIN_DATA_SIZE As Integer = 20080
        Dim VFACE_DATA_SIZE As Integer = 40080
        Dim vstrFind As String = ""
        Dim vnCount As Long
        Dim bytEnrollData() As Byte

        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim sSql As String = "select * from fptable where Enrollnumber='" & vEnrollNumber & "' and FingerNumber=" & anBackupNumber & " and DeviceType ='ATF686n' and EMachineNumber='" & mMachine_ID & "'"
        'RsFp = Cn.Execute(sSql)
        Dim RsFp As DataSet = New DataSet
        If Common.servername = "Access" Then
            sSql = "select * from fptable where Enrollnumber='" & vEnrollNumber & "' and FingerNumber=" & anBackupNumber & " and DeviceType ='ATF686n' and EMachineNumber=" & mMachine_ID & ""
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(RsFp)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(RsFp)
        End If
        If RsFp.Tables(0).Rows.Count > 0 Then
            sSql = "DELETE FROM FPTABLE WHERE Enrollnumber='" & vEnrollNumber & "' and FingerNumber=" & anBackupNumber & " and DeviceType ='ATF686n' and EMachineNumber='" & mMachine_ID & "'"
            'Cn.Execute(sSql)
            If Common.servername = "Access" Then
                sSql = "DELETE FROM FPTABLE WHERE Enrollnumber='" & vEnrollNumber & "' and FingerNumber=" & anBackupNumber & " and DeviceType ='ATF686n' and EMachineNumber=" & mMachine_ID & ""
                If Common.con1.State <> ConnectionState.Open Then
                    Common.con1.Open()
                End If
                cmd1 = New OleDbCommand(sSql, Common.con1)
                cmd1.ExecuteNonQuery()
                If Common.con1.State <> ConnectionState.Closed Then
                    Common.con1.Close()
                End If
            Else
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If

        End If

        Dim vbytConvFpData(0) As Byte
        If (anBackupNumber = CInt(enumBackupNumberType.BACKUP_PSW)) OrElse (anBackupNumber = CInt(enumBackupNumberType.BACKUP_CARD)) Then
            bytEnrollData = New Byte(PWD_DATA_SIZE - 1) {}
            Array.Copy(mbytCurEnrollData, bytEnrollData, PWD_DATA_SIZE)
            'mAdoRstEnroll.Fields("BackupData").Value = bytEnrollData
        ElseIf (anBackupNumber >= CInt(enumBackupNumberType.BACKUP_FP_0)) AndAlso (anBackupNumber <= CInt(enumBackupNumberType.BACKUP_FP_19)) Then
            bytEnrollData = New Byte(FP_DATA_SIZE - 1) {}
            Array.Copy(mbytCurEnrollData, bytEnrollData, FP_DATA_SIZE)
            '{ convert fpdata for compatibility with old version database
            convFpDataToSaveInDbForCompatibility_ATF686n(bytEnrollData, vbytConvFpData)
            '}
            'mAdoRstEnroll.Fields("BackupData").Value = vbytConvFpData
        ElseIf anBackupNumber = CInt(enumBackupNumberType.BACKUP_FACE) Then
            bytEnrollData = New Byte(FACE_DATA_SIZE - 1) {}
            Array.Copy(mbytCurEnrollData, bytEnrollData, FACE_DATA_SIZE)
            'mAdoRstEnroll.Fields("BackupData").Value = bytEnrollData
        ElseIf anBackupNumber >= CInt(enumBackupNumberType.BACKUP_PALMVEIN_0) AndAlso anBackupNumber <= CInt(enumBackupNumberType.BACKUP_PALMVEIN_3) Then
            bytEnrollData = New Byte(PALMVEIN_DATA_SIZE - 1) {}
            Array.Copy(mbytCurEnrollData, bytEnrollData, PALMVEIN_DATA_SIZE)
            'mAdoRstEnroll.Fields("BackupData").Value = bytEnrollData
        ElseIf anBackupNumber >= CInt(enumBackupNumberType.BACKUP_VFACE) Then
            bytEnrollData = New Byte(VFACE_DATA_SIZE - 1) {}
            Array.Copy(mbytCurEnrollData, bytEnrollData, VFACE_DATA_SIZE)
            'mAdoRstEnroll.Fields("BackupData").Value = bytEnrollData
        End If

        'lblMessage.Text = anEnrollNumber.ToString("0000#") + "-" + anBackupNumber;
        'txtEnrollNumber.Text = anEnrollNumber.ToString().Trim();
        'cmbBackupNumber.SelectedIndex = getComboItemIndexFromBackupNumber(anBackupNumber);

        Dim sqlinsert As String = "insert into fptable (Emachinenumber, EnrollNumber, UserName, Template_Tw, FingerNumber, Privilege, DeviceType) values (" & mMachine_ID & ",'" & vEnrollNumber & "', '" & astrEnrollName & "', @Template_Tw, '" & anBackupNumber & "', '" & anPrivilege & "','ATF686n')"

        If Common.servername = "Access" Then
            If Common.con1.State <> ConnectionState.Open Then
                Common.con1.Open()
            End If
            cmd1 = New OleDbCommand(sqlinsert, Common.con1)
            'Using picture As Image = Image.FromFile("./fb")
            Using stream As New IO.MemoryStream
                'picture.Save(stream, Imaging.ImageFormat.Jpeg)
                If anBackupNumber < 20 Then   'nitin... for FP
                    cmd1.Parameters.Add("@Template_Tw", OleDb.OleDbType.VarBinary).Value = vbytConvFpData 'stream.GetBuffer()
                Else  ' FOR face
                    cmd1.Parameters.Add("@Template_Tw", OleDb.OleDbType.VarBinary).Value = bytEnrollData 'stream.GetBuffer()
                End If
                cmd1.ExecuteNonQuery()
            End Using
            If Common.con1.State <> ConnectionState.Closed Then
                Common.con1.Close()
            End If
            'End Using
        Else
            If Common.con.State <> ConnectionState.Open Then
                Common.con.Open()
            End If
            cmd = New SqlCommand(sqlinsert, Common.con)
            Using stream As New IO.MemoryStream
                If anBackupNumber < 20 Then   'nitin... for FP
                    cmd.Parameters.Add("@Template_Tw", SqlDbType.VarBinary).Value = vbytConvFpData 'vbytEnrollData
                Else
                    cmd.Parameters.Add("@Template_Tw", SqlDbType.VarBinary).Value = bytEnrollData 'vbytEnrollData
                End If

                cmd.ExecuteNonQuery()
            End Using
            If Common.con.State <> ConnectionState.Closed Then
                Common.con.Close()
            End If
        End If
        Application.DoEvents()
    End Sub
    'Private Sub saveTemplate(iMachineNumber As Integer, sdwEnrollNumber As String, sName As String, sPassword As String, iPrivilege As Integer, bEnabled As Boolean, axCZKEM1 As zkemkeeper.CZKEM, lvItem As ListViewItem, lvDownload As System.Windows.Forms.ListView, Emachinenumber As Integer)
    '    XtraMasterTest.LabelControlStatus.Text = "Downloading Template " & sdwEnrollNumber
    '    Application.DoEvents()

    '    Dim sSql As String = ""
    '    Dim idwFingerIndex As Integer
    '    Dim sTmpData As String = ""
    '    Dim iTmpLength As Integer
    '    Dim iFlag As Integer
    '    Dim flagenabled As String = ""
    '    Dim adap As SqlDataAdapter
    '    Dim adapA As OleDbDataAdapter
    '    Dim dsRecord As DataSet = New DataSet

    '    Dim sEnrollNumber As String = ""
    '    Dim iFaceIndex As Integer = 50  'the only possible parameter value
    '    Dim sTmpData1 As String = ""
    '    Dim iLength As String = 0
    '    'For idwFingerIndex = 0 To 20
    '    idwFingerIndex = 50
    '    iTmpLength = 0
    '    ''axCZKEM1.GetUserFaceStr(iMachineNumber, sEnrollNumber, iFaceIndex, sTmpData, iLength)
    '    Dim strCardno As String = ""
    '    'strCardno = axCZKEM1.CardNumber(0)
    '    axCZKEM1.GetStrCardNumber(strCardno)
    '    If IsNumeric(sdwEnrollNumber) = True Then
    '        sdwEnrollNumber = Convert.ToDouble(sdwEnrollNumber).ToString("000000000000")
    '    End If
    '    If strCardno <> 0 Then
    '        sSql = "select * from fptable where Enrollnumber='" & sdwEnrollNumber & "' and CardNumber='" & strCardno & "' and DeviceType ='ZK(TFT)' and EMachineNumber=" & Emachinenumber & ""
    '        dsRecord = New DataSet
    '        If Common.servername = "Access" Then
    '            sSql = "select * from fptable where Enrollnumber='" & sdwEnrollNumber & "' and CardNumber='" & strCardno & "' and DeviceType ='ZK(TFT)' and EMachineNumber=" & Emachinenumber & ""
    '            adapA = New OleDbDataAdapter(sSql, Common.con1)
    '            adapA.Fill(dsRecord)
    '        Else
    '            adap = New SqlDataAdapter(sSql, Common.con)
    '            adap.Fill(dsRecord)
    '        End If
    '        If dsRecord.Tables(0).Rows.Count > 0 Then
    '            sSql = "update FPTABLE set Enrollnumber='" & sdwEnrollNumber & "' where CardNumber= '" & strCardno & "' and DeviceType ='ZK(TFT)' and EMachineNumber=" & Emachinenumber & ""
    '            'sSql = "DELETE FROM FPTABLE WHERE Enrollnumber='" & sdwEnrollNumber.ToString("000000000000") & "' and CardNumber=" & strCardno
    '            If Common.servername = "Access" Then
    '                sSql = "update FPTABLE set Enrollnumber='" & sdwEnrollNumber & "' where CardNumber= '" & strCardno & "' and DeviceType ='ZK(TFT)' and EMachineNumber=" & Emachinenumber & ""
    '                cmd1 = New OleDbCommand(sSql, Common.con1)
    '                cmd1.ExecuteNonQuery()
    '            Else
    '                cmd = New SqlCommand(sSql, Common.con)
    '                cmd.ExecuteNonQuery()
    '            End If
    '        Else
    '            Dim sqlinsert As String = "insert into fptable (Emachinenumber, EnrollNumber, UserName, CardNumber, FingerNumber, Privilege, DeviceType) values (" & Emachinenumber & ",'" & sdwEnrollNumber & "', '" & sName.ToString() & "','" & strCardno & "', '11', '" & iPrivilege.ToString() & "','ZK(TFT)')"
    '            If Common.servername = "Access" Then
    '                cmd1 = New OleDbCommand(sqlinsert, Common.con1)
    '                cmd1.ExecuteNonQuery()
    '            Else
    '                cmd = New SqlCommand(sqlinsert, Common.con)
    '                cmd.ExecuteNonQuery()
    '            End If
    '            'Strsql = "insert into tblDownloadData(UserID,EmpName,Finger,TmpData,Previlege,Password,Enabled,Flag)values('" + sdwEnrollNumber.ToString("00000000") + "','" + Name.ToString() + "','" + idwFingerIndex.ToString() + "','" + sTmpData.ToString() + "','" + iPrivilege.ToString() + "','" + sPassword.ToString() + "','" + flagenabled.ToString() + "','" + iFlag.ToString() + "')"
    '        End If
    '    End If
    '    'for password
    '    If sPassword <> "" Then
    '        sSql = "select * from fptable where Enrollnumber='" & sdwEnrollNumber & "' and Password='" & sPassword & "'  and DeviceType ='ZK(TFT)' and EMachineNumber=" & Emachinenumber & ""
    '        dsRecord = New DataSet
    '        If Common.servername = "Access" Then
    '            adapA = New OleDbDataAdapter(sSql, Common.con1)
    '            adapA.Fill(dsRecord)
    '        Else
    '            adap = New SqlDataAdapter(sSql, Common.con)
    '            adap.Fill(dsRecord)
    '        End If
    '        If dsRecord.Tables(0).Rows.Count > 0 Then
    '            sSql = "update FPTABLE set Enrollnumber='" & sdwEnrollNumber & "' where Password= '" & sPassword & "'  and DeviceType ='ZK(TFT)' and EMachineNumber=" & Emachinenumber & ""
    '            'sSql = "DELETE FROM FPTABLE WHERE Enrollnumber='" & sdwEnrollNumber.ToString("000000000000") & "' and CardNumber=" & strCardno
    '            If Common.servername = "Access" Then
    '                cmd1 = New OleDbCommand(sSql, Common.con1)
    '                cmd1.ExecuteNonQuery()
    '            Else
    '                cmd = New SqlCommand(sSql, Common.con)
    '                cmd.ExecuteNonQuery()
    '            End If
    '        Else
    '            Dim sqlinsert As String = "insert into fptable ([Emachinenumber], [EnrollNumber], [UserName], [Password], [FingerNumber], [Privilege],[DeviceType] ) values (" & Emachinenumber & ", '" & sdwEnrollNumber & "', '" & sName.ToString() & "', '" & sPassword & "', 10, " & iPrivilege & ", 'ZK(TFT)')"
    '            If Common.servername = "Access" Then
    '                cmd1 = New OleDbCommand(sqlinsert, Common.con1)
    '                cmd1.ExecuteNonQuery()
    '            Else
    '                cmd = New SqlCommand(sqlinsert, Common.con)
    '                cmd.ExecuteNonQuery()
    '            End If
    '            'Strsql = "insert into tblDownloadData(UserID,EmpName,Finger,TmpData,Previlege,Password,Enabled,Flag)values('" + sdwEnrollNumber.ToString("00000000") + "','" + Name.ToString() + "','" + idwFingerIndex.ToString() + "','" + sTmpData.ToString() + "','" + iPrivilege.ToString() + "','" + sPassword.ToString() + "','" + flagenabled.ToString() + "','" + iFlag.ToString() + "')"
    '        End If
    '    End If
    '    'Next


    '    'finger
    '    iTmpLength = 0
    '    sEnrollNumber = ""
    '    iFaceIndex = 50  'the only possible parameter value
    '    sTmpData1 = ""
    '    iLength = 0
    '    If IsNumeric(sdwEnrollNumber) = True Then
    '        sdwEnrollNumber = Convert.ToDouble(sdwEnrollNumber)
    '    End If
    '    For idwFingerIndex = 0 To 10
    '        If axCZKEM1.GetUserTmpExStr(iMachineNumber, sdwEnrollNumber, idwFingerIndex, iFlag, sTmpData, iTmpLength) Then 'get the corresponding templates string and length from the memory
    '            lvItem = lvDownload.Items.Add(sdwEnrollNumber.ToString())
    '            lvItem.SubItems.Add(sName)
    '            lvItem.SubItems.Add(idwFingerIndex.ToString())
    '            lvItem.SubItems.Add(sTmpData)
    '            lvItem.SubItems.Add(iPrivilege.ToString())
    '            lvItem.SubItems.Add(sPassword)
    '            If sTmpData.ToString.Trim = "" Then
    '                Continue For
    '            End If
    '            Dim adap1 As SqlDataAdapter
    '            Dim adapA1 As OleDbDataAdapter
    '            Dim rsEmp As DataSet = New DataSet
    '            If IsNumeric(sdwEnrollNumber) = True Then
    '                sdwEnrollNumber = Convert.ToDouble(sdwEnrollNumber).ToString("000000000000")
    '            End If
    '            sSql = "select empname from tblemployee where presentcardno='" & sdwEnrollNumber & "' "
    '            'rsEmp = Cn.Execute(sSql)
    '            If Common.servername = "Access" Then
    '                adapA1 = New OleDbDataAdapter(sSql, Common.con1)
    '                adapA1.Fill(rsEmp)
    '            Else
    '                adap1 = New SqlDataAdapter(sSql, Common.con)
    '                adap1.Fill(rsEmp)
    '            End If
    '            If rsEmp.Tables(0).Rows.Count > 0 Then
    '                sName = rsEmp.Tables(0).Rows(0).Item("empname").ToString.Trim
    '            Else
    '                'sName = ""
    '            End If

    '            If bEnabled = True Then
    '                lvItem.SubItems.Add("true")
    '                flagenabled = "true"
    '            Else
    '                lvItem.SubItems.Add("false")
    '                flagenabled = "false"
    '            End If
    '            lvItem.SubItems.Add(iFlag.ToString())

    '            'sSql = "select * from tblDownloadData where UserID='" & sdwEnrollNumber.ToString().Trim() & "' and  Finger='" & idwFingerIndex.ToString() & "'"
    '            sSql = "select * from fptable where Enrollnumber='" & sdwEnrollNumber & "' and FingerNumber=" & idwFingerIndex.ToString() & " and DeviceType ='ZK(TFT)' and EMachineNumber=" & Emachinenumber & ""
    '            dsRecord = New DataSet
    '            If Common.servername = "Access" Then
    '                sSql = "select * from fptable where Enrollnumber='" & sdwEnrollNumber & "' and FingerNumber=" & idwFingerIndex.ToString() & " and DeviceType ='ZK(TFT)' and EMachineNumber=" & Emachinenumber & ""
    '                adapA = New OleDbDataAdapter(sSql, Common.con1)
    '                adapA.Fill(dsRecord)
    '            Else
    '                adap = New SqlDataAdapter(sSql, Common.con)
    '                adap.Fill(dsRecord)
    '            End If
    '            If dsRecord.Tables(0).Rows.Count > 0 Then
    '                sSql = "DELETE FROM FPTABLE WHERE Enrollnumber='" & sdwEnrollNumber & "' and FingerNumber=" & idwFingerIndex.ToString() & " and DeviceType ='ZK(TFT)' and EMachineNumber=" & Emachinenumber & ""
    '                If Common.servername = "Access" Then
    '                    sSql = "DELETE FROM FPTABLE WHERE Enrollnumber='" & sdwEnrollNumber & "' and FingerNumber=" & idwFingerIndex.ToString() & " and DeviceType ='ZK(TFT)' and EMachineNumber=" & Emachinenumber & ""
    '                    cmd1 = New OleDbCommand(sSql, Common.con1)
    '                    cmd1.ExecuteNonQuery()
    '                Else
    '                    cmd = New SqlCommand(sSql, Common.con)
    '                    cmd.ExecuteNonQuery()
    '                End If
    '                Dim sqlinsert As String = "insert into fptable (Emachinenumber, EnrollNumber, UserName, Template, FingerNumber, Privilege, DeviceType) values ('" & Emachinenumber & "','" & sdwEnrollNumber & "', '" & sName.ToString() & "','" & sTmpData.ToString() & "', '" & idwFingerIndex & "', '" & iPrivilege.ToString() & "','ZK(TFT)')"
    '                If Common.servername = "Access" Then
    '                    cmd1 = New OleDbCommand(sqlinsert, Common.con1)
    '                    cmd1.ExecuteNonQuery()
    '                Else
    '                    cmd = New SqlCommand(sqlinsert, Common.con)
    '                    cmd.ExecuteNonQuery()
    '                End If
    '            Else
    '                Dim sqlinsert As String = "insert into fptable (Emachinenumber, EnrollNumber, UserName, Template, FingerNumber, Privilege, DeviceType) values (" & Emachinenumber & ",'" & sdwEnrollNumber & "', '" & sName.ToString() & "','" & sTmpData.ToString() & "', '" & idwFingerIndex & "', '" & iPrivilege.ToString() & "','ZK(TFT)')"
    '                If Common.servername = "Access" Then
    '                    cmd1 = New OleDbCommand(sqlinsert, Common.con1)
    '                    cmd1.ExecuteNonQuery()
    '                Else
    '                    cmd = New SqlCommand(sqlinsert, Common.con)
    '                    cmd.ExecuteNonQuery()
    '                End If
    '                'Strsql = "insert into tblDownloadData(UserID,EmpName,Finger,TmpData,Previlege,Password,Enabled,Flag)values('" + sdwEnrollNumber.ToString("00000000") + "','" + Name.ToString() + "','" + idwFingerIndex.ToString() + "','" + sTmpData.ToString() + "','" + iPrivilege.ToString() + "','" + sPassword.ToString() + "','" + flagenabled.ToString() + "','" + iFlag.ToString() + "')"
    '            End If
    '        End If
    '        Try
    '            sdwEnrollNumber = Convert.ToDouble(sdwEnrollNumber)
    '        Catch ex As Exception
    '            sdwEnrollNumber = sdwEnrollNumber
    '        End Try
    '    Next


    '    'face
    '    sEnrollNumber = ""
    '    iFaceIndex = 50  'the only possible parameter value
    '    idwFingerIndex = iFaceIndex
    '    sTmpData1 = ""
    '    iLength = 0
    '    'axCZKEM1.GetUserFaceStr(iMachineNumber, sEnrollNumber, iFaceIndex, sTmpData, iLength)
    '    If IsNumeric(sdwEnrollNumber) = True Then
    '        sdwEnrollNumber = Convert.ToDouble(sdwEnrollNumber)
    '    End If
    '    If axCZKEM1.GetUserFaceStr(iMachineNumber, sdwEnrollNumber, iFaceIndex, sTmpData1, iLength) Then
    '        lvItem = lvDownload.Items.Add(sdwEnrollNumber.ToString())
    '        lvItem.SubItems.Add(sName)
    '        lvItem.SubItems.Add(idwFingerIndex.ToString())
    '        lvItem.SubItems.Add(sTmpData)
    '        lvItem.SubItems.Add(iPrivilege.ToString())
    '        lvItem.SubItems.Add(sPassword)

    '        Dim adap1 As SqlDataAdapter
    '        Dim adapA1 As OleDbDataAdapter
    '        Dim rsEmp As DataSet = New DataSet
    '        If IsNumeric(sdwEnrollNumber) = True Then
    '            sdwEnrollNumber = Convert.ToDouble(sdwEnrollNumber).ToString("000000000000")
    '        End If
    '        sSql = "select empname from tblemployee where presentcardno='" & sdwEnrollNumber & "'"
    '        'rsEmp = Cn.Execute(sSql)
    '        If Common.servername = "Access" Then
    '            adapA1 = New OleDbDataAdapter(sSql, Common.con1)
    '            adapA1.Fill(rsEmp)
    '        Else
    '            adap1 = New SqlDataAdapter(sSql, Common.con)
    '            adap1.Fill(rsEmp)
    '        End If
    '        If rsEmp.Tables(0).Rows.Count > 0 Then
    '            sName = rsEmp.Tables(0).Rows(0).Item("empname").ToString.Trim
    '        Else
    '            sName = ""
    '        End If

    '        If bEnabled = True Then
    '            lvItem.SubItems.Add("true")
    '            flagenabled = "true"
    '        Else
    '            lvItem.SubItems.Add("false")
    '            flagenabled = "false"
    '        End If
    '        lvItem.SubItems.Add(iFlag.ToString())

    '        'sSql = "select * from tblDownloadData where UserID='" & sdwEnrollNumber.ToString().Trim() & "' and  Finger='" & idwFingerIndex.ToString() & "'"
    '        sSql = "select * from fptable where Enrollnumber='" & sdwEnrollNumber & "' and FingerNumber=" & idwFingerIndex.ToString() & " and DeviceType ='ZK(TFT)' and EMachineNumber=" & Emachinenumber & ""
    '        dsRecord = New DataSet
    '        If Common.servername = "Access" Then
    '            adapA = New OleDbDataAdapter(sSql, Common.con1)
    '            adapA.Fill(dsRecord)
    '        Else
    '            adap = New SqlDataAdapter(sSql, Common.con)
    '            adap.Fill(dsRecord)
    '        End If
    '        If dsRecord.Tables(0).Rows.Count > 0 Then
    '            'sSql = "DELETE FROM FPTABLE WHERE Enrollnumber='" & sdwEnrollNumber.ToString("000000000000") & "' and FingerNumber=" & idwFingerIndex.ToString()
    '            sSql = "Update FPTABLE set Template = '" & sTmpData1.ToString() & "', Emachinenumber= " & Emachinenumber & ", Privilege=" & iPrivilege & " where Enrollnumber = '" & sdwEnrollNumber & "' and FingerNumber=" & idwFingerIndex.ToString() & " and DeviceType ='ZK(TFT)' and EMachineNumber=" & Emachinenumber & ""
    '            If Common.servername = "Access" Then
    '                cmd1 = New OleDbCommand(sSql, Common.con1)
    '                cmd1.ExecuteNonQuery()
    '            Else
    '                cmd = New SqlCommand(sSql, Common.con)
    '                cmd.ExecuteNonQuery()
    '            End If
    '        Else
    '            Dim sqlinsert As String = "insert into fptable (Emachinenumber, EnrollNumber, UserName, Template, FingerNumber, Privilege, DeviceType) values (" & Emachinenumber & ",'" & sdwEnrollNumber & "', '" & sName.ToString() & "','" & sTmpData1.ToString() & "', '" & idwFingerIndex & "', '" & iPrivilege.ToString() & "','ZK(TFT)')"
    '            If Common.servername = "Access" Then
    '                cmd1 = New OleDbCommand(sqlinsert, Common.con1)
    '                cmd1.ExecuteNonQuery()
    '            Else
    '                cmd = New SqlCommand(sqlinsert, Common.con)
    '                cmd.ExecuteNonQuery()
    '            End If
    '            'Strsql = "insert into tblDownloadData(UserID,EmpName,Finger,TmpData,Previlege,Password,Enabled,Flag)values('" + sdwEnrollNumber.ToString("00000000") + "','" + Name.ToString() + "','" + idwFingerIndex.ToString() + "','" + sTmpData.ToString() + "','" + iPrivilege.ToString() + "','" + sPassword.ToString() + "','" + flagenabled.ToString() + "','" + iFlag.ToString() + "')"
    '        End If
    '    End If
    'End Sub
    Private Sub saveTemplate(iMachineNumber As Integer, sdwEnrollNumber As String, sName As String, sPassword As String, iPrivilege As Integer, bEnabled As Boolean, axCZKEM1 As zkemkeeper.CZKEM, lvItem As ListViewItem, lvDownload As System.Windows.Forms.ListView, Emachinenumber As Integer)
        Dim sSql As String = ""
        Dim idwFingerIndex As Integer
        Dim sTmpData As String = ""
        Dim iTmpLength As Integer
        Dim iFlag As Integer
        Dim flagenabled As String = ""
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim dsRecord As DataSet = New DataSet

        Dim sEnrollNumber As String = ""
        Dim iFaceIndex As Integer = 50  'the only possible parameter value
        Dim sTmpData1 As String = ""
        Dim iLength As String = 0
        'For idwFingerIndex = 0 To 20
        idwFingerIndex = 50
        iTmpLength = 0
        ''axCZKEM1.GetUserFaceStr(iMachineNumber, sEnrollNumber, iFaceIndex, sTmpData, iLength)
        Dim strCardno As String = ""
        'strCardno = axCZKEM1.CardNumber(0)
        axCZKEM1.GetStrCardNumber(strCardno)
        If IsNumeric(sdwEnrollNumber) = True Then
            sdwEnrollNumber = Convert.ToDouble(sdwEnrollNumber).ToString("000000000000")
        End If
        If strCardno <> 0 Then
            sSql = "select CardNumber from fptable where Enrollnumber='" & sdwEnrollNumber & "' and FingerNumber=11 and DeviceType ='ZK(TFT)' and EMachineNumber=" & Emachinenumber & ""
            dsRecord = New DataSet
            If Common.servername = "Access" Then
                'sSql = "select * from fptable where Enrollnumber='" & sdwEnrollNumber & "' and CardNumber='" & strCardno & "' and DeviceType ='ZK(TFT)' and EMachineNumber=" & Emachinenumber & ""
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(dsRecord)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(dsRecord)
            End If
            If dsRecord.Tables(0).Rows.Count > 0 Then
                sSql = "update FPTABLE set CardNumber='" & strCardno & "', UserName='" & sName & "', Privilege=" & iPrivilege & " where Enrollnumber= '" & sdwEnrollNumber & "' and FingerNumber=11 and DeviceType ='ZK(TFT)' and EMachineNumber=" & Emachinenumber & ""
                'sSql = "DELETE FROM FPTABLE WHERE Enrollnumber='" & sdwEnrollNumber.ToString("000000000000") & "' and CardNumber=" & strCardno                
            Else
                sSql = "insert into fptable (Emachinenumber, EnrollNumber, UserName, CardNumber, FingerNumber, Privilege, DeviceType) values (" & Emachinenumber & ",'" & sdwEnrollNumber & "', '" & sName & "','" & strCardno & "', 11, '" & iPrivilege.ToString() & "','ZK(TFT)')"
            End If
            If Common.servername = "Access" Then
                cmd1 = New OleDbCommand(sSql, Common.con1)
                cmd1.ExecuteNonQuery()
            Else
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
            End If
        End If
        'for password
        If sPassword <> "" Then
            sSql = "select Password from fptable where Enrollnumber='" & sdwEnrollNumber & "' and FingerNumber=10 and DeviceType ='ZK(TFT)' and EMachineNumber=" & Emachinenumber & ""
            dsRecord = New DataSet
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(dsRecord)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(dsRecord)
            End If
            If dsRecord.Tables(0).Rows.Count > 0 Then
                sSql = "update FPTABLE set Password='" & sPassword & "', UserName='" & sName & "', Privilege=" & iPrivilege & " where Enrollnumber= '" & sdwEnrollNumber & "' and FingerNumber=10 and DeviceType ='ZK(TFT)' and EMachineNumber=" & Emachinenumber & ""
            Else
                sSql = "insert into fptable ([Emachinenumber], [EnrollNumber], [UserName], [Password], [FingerNumber], [Privilege],[DeviceType] ) values (" & Emachinenumber & ", '" & sdwEnrollNumber & "', '" & sName.ToString() & "', '" & sPassword & "', 10, " & iPrivilege & ", 'ZK(TFT)')"
            End If
            If Common.servername = "Access" Then
                cmd1 = New OleDbCommand(sSql, Common.con1)
                cmd1.ExecuteNonQuery()
            Else
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
            End If
        End If
        'Next


        'finger
        iTmpLength = 0
        sEnrollNumber = ""
        iFaceIndex = 50  'the only possible parameter value
        sTmpData1 = ""
        iLength = 0
        If IsNumeric(sdwEnrollNumber) = True Then
            sdwEnrollNumber = Convert.ToDouble(sdwEnrollNumber)
        End If
        For idwFingerIndex = 0 To 10
            If axCZKEM1.GetUserTmpExStr(iMachineNumber, sdwEnrollNumber, idwFingerIndex, iFlag, sTmpData, iTmpLength) Then 'get the corresponding templates string and length from the memory
                'lvItem = lvDownload.Items.Add(sdwEnrollNumber.ToString())
                'lvItem.SubItems.Add(sName)
                'lvItem.SubItems.Add(idwFingerIndex.ToString())
                'lvItem.SubItems.Add(sTmpData)
                'lvItem.SubItems.Add(iPrivilege.ToString())
                'lvItem.SubItems.Add(sPassword)
                If sTmpData.ToString.Trim = "" Then
                    Continue For
                End If
                'Dim adap1 As SqlDataAdapter
                'Dim adapA1 As OleDbDataAdapter
                'Dim rsEmp As DataSet = New DataSet
                If IsNumeric(sdwEnrollNumber) = True Then
                    sdwEnrollNumber = Convert.ToDouble(sdwEnrollNumber).ToString("000000000000")
                End If
                'sSql = "select empname from tblemployee where presentcardno='" & sdwEnrollNumber & "' "
                ''rsEmp = Cn.Execute(sSql)
                'If Common.servername = "Access" Then
                '    adapA1 = New OleDbDataAdapter(sSql, Common.con1)
                '    adapA1.Fill(rsEmp)
                'Else
                '    adap1 = New SqlDataAdapter(sSql, Common.con)
                '    adap1.Fill(rsEmp)
                'End If
                'If rsEmp.Tables(0).Rows.Count > 0 Then
                '    sName = rsEmp.Tables(0).Rows(0).Item("empname").ToString.Trim
                'Else
                '    'sName = ""
                'End If

                If bEnabled = True Then
                    'lvItem.SubItems.Add("true")
                    flagenabled = "true"
                Else
                    'lvItem.SubItems.Add("false")
                    flagenabled = "false"
                End If
                lvItem.SubItems.Add(iFlag.ToString())

                'sSql = "select * from tblDownloadData where UserID='" & sdwEnrollNumber.ToString().Trim() & "' and  Finger='" & idwFingerIndex.ToString() & "'"
                sSql = "select Enrollnumber from fptable where Enrollnumber='" & sdwEnrollNumber & "' and FingerNumber=" & idwFingerIndex.ToString() & " and DeviceType ='ZK(TFT)' and EMachineNumber=" & Emachinenumber & ""
                dsRecord = New DataSet
                If Common.servername = "Access" Then
                    sSql = "select Enrollnumber from fptable where Enrollnumber='" & sdwEnrollNumber & "' and FingerNumber=" & idwFingerIndex & " and DeviceType ='ZK(TFT)' and EMachineNumber=" & Emachinenumber & ""
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(dsRecord)
                Else
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(dsRecord)
                End If
                If dsRecord.Tables(0).Rows.Count > 0 Then
                    sSql = "update FPTABLE set Template='" & sTmpData & "', UserName='" & sName & "', Privilege=" & iPrivilege & " WHERE Enrollnumber='" & sdwEnrollNumber & "' and FingerNumber=" & idwFingerIndex & " and DeviceType ='ZK(TFT)' and EMachineNumber=" & Emachinenumber & ""
                Else
                    sSql = "insert into fptable (Emachinenumber, EnrollNumber, UserName, Template, FingerNumber, Privilege, DeviceType) values (" & Emachinenumber & ",'" & sdwEnrollNumber & "', '" & sName & "','" & sTmpData.ToString() & "', " & idwFingerIndex & ", " & iPrivilege & ",'ZK(TFT)')"
                End If
                If Common.servername = "Access" Then
                    cmd1 = New OleDbCommand(sSql, Common.con1)
                    cmd1.ExecuteNonQuery()
                Else
                    cmd = New SqlCommand(sSql, Common.con)
                    cmd.ExecuteNonQuery()
                End If
            End If
            Try
                sdwEnrollNumber = Convert.ToDouble(sdwEnrollNumber)
            Catch ex As Exception
                sdwEnrollNumber = sdwEnrollNumber
            End Try
        Next


        'face
        sEnrollNumber = ""
        iFaceIndex = 50  'the only possible parameter value
        idwFingerIndex = iFaceIndex
        sTmpData1 = ""
        iLength = 0
        'axCZKEM1.GetUserFaceStr(iMachineNumber, sEnrollNumber, iFaceIndex, sTmpData, iLength)
        If IsNumeric(sdwEnrollNumber) = True Then
            sdwEnrollNumber = Convert.ToDouble(sdwEnrollNumber)
        End If
        If axCZKEM1.GetUserFaceStr(iMachineNumber, sdwEnrollNumber, iFaceIndex, sTmpData1, iLength) Then
            'lvItem = lvDownload.Items.Add(sdwEnrollNumber.ToString())
            'lvItem.SubItems.Add(sName)
            'lvItem.SubItems.Add(idwFingerIndex.ToString())
            'lvItem.SubItems.Add(sTmpData)
            'lvItem.SubItems.Add(iPrivilege.ToString())
            'lvItem.SubItems.Add(sPassword)

            'Dim adap1 As SqlDataAdapter
            'Dim adapA1 As OleDbDataAdapter
            'Dim rsEmp As DataSet = New DataSet
            If IsNumeric(sdwEnrollNumber) = True Then
                sdwEnrollNumber = Convert.ToDouble(sdwEnrollNumber).ToString("000000000000")
            End If
            'sSql = "select empname from tblemployee where presentcardno='" & sdwEnrollNumber & "'"
            ''rsEmp = Cn.Execute(sSql)
            'If Common.servername = "Access" Then
            '    adapA1 = New OleDbDataAdapter(sSql, Common.con1)
            '    adapA1.Fill(rsEmp)
            'Else
            '    adap1 = New SqlDataAdapter(sSql, Common.con)
            '    adap1.Fill(rsEmp)
            'End If
            'If rsEmp.Tables(0).Rows.Count > 0 Then
            '    sName = rsEmp.Tables(0).Rows(0).Item("empname").ToString.Trim
            'Else
            '    sName = ""
            'End If

            If bEnabled = True Then
                'lvItem.SubItems.Add("true")
                flagenabled = "true"
            Else
                'lvItem.SubItems.Add("false")
                flagenabled = "false"
            End If
            'lvItem.SubItems.Add(iFlag.ToString())

            'sSql = "select * from tblDownloadData where UserID='" & sdwEnrollNumber.ToString().Trim() & "' and  Finger='" & idwFingerIndex.ToString() & "'"
            sSql = "select Enrollnumber from fptable where Enrollnumber='" & sdwEnrollNumber & "' and FingerNumber=" & idwFingerIndex & " and DeviceType ='ZK(TFT)' and EMachineNumber=" & Emachinenumber & ""
            dsRecord = New DataSet
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(dsRecord)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(dsRecord)
            End If
            If dsRecord.Tables(0).Rows.Count > 0 Then
                'sSql = "DELETE FROM FPTABLE WHERE Enrollnumber='" & sdwEnrollNumber.ToString("000000000000") & "' and FingerNumber=" & idwFingerIndex.ToString()
                sSql = "Update FPTABLE set Template = '" & sTmpData1 & "', UserName='" & sName & "', Privilege=" & iPrivilege & " where Enrollnumber = '" & sdwEnrollNumber & "' and FingerNumber=" & idwFingerIndex.ToString() & " and DeviceType ='ZK(TFT)' and EMachineNumber=" & Emachinenumber & ""
            Else
                sSql = "insert into fptable (Emachinenumber, EnrollNumber, UserName, Template, FingerNumber, Privilege, DeviceType) values (" & Emachinenumber & ",'" & sdwEnrollNumber & "', '" & sName & "','" & sTmpData1 & "', " & idwFingerIndex & ", " & iPrivilege & ",'ZK(TFT)')"
            End If
            If Common.servername = "Access" Then
                cmd1 = New OleDbCommand(sSql, Common.con1)
                cmd1.ExecuteNonQuery()
            Else
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
            End If
        End If
    End Sub
    'Public Declare Sub CopyMemory Lib "kernel32" Alias "RtlMoveMemory" (Destination As Byte, Source As Byte, ByVal Length As Long)
    Public Declare Sub CopyMemory Lib "kernel32" Alias "RtlMoveMemory" (ByVal Destination As Long, ByVal Source As Long, ByVal Length As Long)
    Public Declare Sub ZeroMemory Lib "kernel32" Alias "RtlZeroMemory" (Destination As Byte, ByVal Length As Long)
    Public Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)
    Public Const gstrNoDevice = "No Device"
    Private Sub SaveBio1EcoFPData(ByVal uie As SyUserInfoExt, Emachinenumber As String, mMK8001Device As Boolean)
        Dim sSql As String
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim ds As DataSet = New DataSet
        Dim cmd As SqlCommand
        Dim cmd1 As OleDbCommand
        Dim EnrollNumber As String = uie.sUserInfo.wUserID
        If IsNumeric(EnrollNumber) Then
            EnrollNumber = Convert.ToDouble(EnrollNumber).ToString("000000000000")
        End If
        XtraMasterTest.LabelControlStatus.Text = "Downloading Template " & EnrollNumber
        Application.DoEvents()

        If (uie.dwCardID.ToString <> "" And uie.dwCardID <> 0) Then
            sSql = "select EMachineNumber from fptable where EMachineNumber = " & Emachinenumber & " and EnrollNumber ='" & EnrollNumber & "'  and Cardnumber <> '' and DeviceType='Bio1Eco'"
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
            If ds.Tables(0).Rows.Count > 0 Then
                sSql = "update fptable set Cardnumber ='" & uie.dwCardID & "', UserName='" & uie.sUserInfo.name & "'  where EMachineNumber = " & Emachinenumber & " and EnrollNumber ='" & EnrollNumber & "' and DeviceType='Bio1Eco'"
            Else
                sSql = "insert into fptable (Emachinenumber, EnrollNumber, UserName, CardNumber, FingerNumber, Privilege,  DeviceType) values (" & Emachinenumber & ",'" & EnrollNumber & "', '" & uie.sUserInfo.name & "','" & uie.dwCardID & "', '11', '" & uie.sUserInfo.sUserType & "','Bio1Eco')"
            End If
            If Common.servername = "Access" Then
                cmd1 = New OleDbCommand(sSql, Common.con1)
                cmd1.ExecuteNonQuery()
            Else
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
            End If
        End If

        If uie.dwPws <> 0 And uie.dwPws.ToString <> "" Then
            sSql = "select EMachineNumber from fptable where EMachineNumber = " & Emachinenumber & " and EnrollNumber ='" & EnrollNumber & "'  and Password <> '' and DeviceType='Bio1Eco'"
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
            If ds.Tables(0).Rows.Count > 0 Then
                sSql = "update fptable set  UserName='" & uie.sUserInfo.name & "',[Password]='" & uie.dwPws & "'  where EMachineNumber = " & Emachinenumber & " and EnrollNumber ='" & EnrollNumber & "' and DeviceType='Bio1Eco'"
            Else
                sSql = "insert into fptable (Emachinenumber, EnrollNumber, UserName, FingerNumber, Privilege, [Password], DeviceType) values (" & Emachinenumber & ",'" & EnrollNumber & "', '" & uie.sUserInfo.name & "', 10, " & uie.sUserInfo.sUserType & ",'" & uie.dwPws & "','Bio1Eco')"
            End If
            If Common.servername = "Access" Then
                cmd1 = New OleDbCommand(sSql, Common.con1)
                cmd1.ExecuteNonQuery()
            Else
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
            End If
        End If

        'If Bio2.Template.Length > 0 Then
        Dim iFingerSize As Integer
        Dim fts As SyFingerTemplate = New SyFingerTemplate
        Dim ftsEx As SyFingerTemplateEx = New SyFingerTemplateEx
        If mMK8001Device Then
            iFingerSize = 768
        Else
            iFingerSize = 512
        End If
        For x As Integer = 0 To 5 - 1
            If (uie.sUserInfo.MbIndex(x) > 0) Then
                Dim ret As Integer
                Dim sb As StringBuilder = New StringBuilder
                If (iFingerSize = 768) Then
                    ret = SyFunctions.CmdGetUserFingerPrintEx(ftsEx, uie.sUserInfo.wUserID, CType((x + 1), UInt16))
                    Dim j As Integer = 0
                    Do While (j < fts.data.Length)
                        sb.Append(Convert.ToString(ftsEx.data(j), 16).PadLeft(2, Microsoft.VisualBasic.ChrW(48)))
                        j = (j + 1)
                    Loop
                Else
                    ret = SyFunctions.CmdGetUserFingerPrint(fts, uie.sUserInfo.wUserID, CType((x + 1), UInt16))
                    Dim j As Integer = 0
                    Do While (j < fts.data.Length)
                        sb.Append(Convert.ToString(fts.data(j), 16).PadLeft(2, Microsoft.VisualBasic.ChrW(48)))
                        j = (j + 1)
                    Loop
                End If
                If sb.ToString.Trim <> "" Then
                    sSql = "select EMachineNumber from fptable where EMachineNumber = " & Emachinenumber & " and EnrollNumber ='" & EnrollNumber & "'  and FingerNumber = " & x & " and DeviceType='Bio1Eco'"
                    ds = New DataSet
                    If Common.servername = "Access" Then
                        adapA = New OleDbDataAdapter(sSql, Common.con1)
                        adapA.Fill(ds)
                    Else
                        adap = New SqlDataAdapter(sSql, Common.con)
                        adap.Fill(ds)
                    End If
                    If ds.Tables(0).Rows.Count > 0 Then
                        sSql = "update fptable set Template='" & sb.ToString & "' where EMachineNumber = " & Emachinenumber & " and EnrollNumber ='" & EnrollNumber & "'  and FingerNumber = " & x & " and DeviceType='Bio1Eco'"
                    Else
                        sSql = "insert into fptable (Emachinenumber, EnrollNumber, UserName, Template, FingerNumber, Privilege, DeviceType) values (" & Emachinenumber & ",'" & EnrollNumber & "', '" & uie.sUserInfo.name & "','" & sb.ToString & "', " & x & ", " & uie.sUserInfo.sUserType & ",'Bio1Eco')"
                    End If
                    If Common.servername = "Access" Then
                        cmd1 = New OleDbCommand(sSql, Common.con1)
                        cmd1.ExecuteNonQuery()
                    Else
                        cmd = New SqlCommand(sSql, Common.con)
                        cmd.ExecuteNonQuery()
                    End If
                End If
            End If
        Next
        'End If
    End Sub
    Private Sub SaveBio2FPData(ByVal Bio2 As Bio2EnrollData)
        Dim sSql As String
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim ds As DataSet = New DataSet
        Dim cmd As SqlCommand
        Dim cmd1 As OleDbCommand
        Dim EnrollNumber As String = Bio2.EnrollNumber
        If IsNumeric(EnrollNumber) Then
            EnrollNumber = Convert.ToDouble(EnrollNumber).ToString("000000000000")
        End If
        XtraMasterTest.LabelControlStatus.Text = "Downloading Template " & EnrollNumber
        Application.DoEvents()

        If Bio2.Cardnumber <> "" Then
            sSql = "select EMachineNumber from fptable where EMachineNumber = '" & Bio2.EMachineNumber & "' and EnrollNumber ='" & EnrollNumber & "'  and Cardnumber <> '' and DeviceType='Bio2+'"
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
            If ds.Tables(0).Rows.Count > 0 Then
                sSql = "update EMachineNumber set Cardnumber ='" & Bio2.Cardnumber & "', Password='" & Bio2.Password & "', UserName='" & Bio2.UserName & "' where EMachineNumber = '" & Bio2.EMachineNumber & "' and EnrollNumber ='" & EnrollNumber & "' and DeviceType='Bio2+'"
            Else
                sSql = "insert into fptable (Emachinenumber, EnrollNumber, UserName, CardNumber, FingerNumber, Privilege, Password, DeviceType) values ('" & Bio2.EMachineNumber & "','" & EnrollNumber & "', '" & Bio2.UserName & "','" & Bio2.Cardnumber & "', '11', '" & Bio2.Privilege & "','" & Bio2.Password & "','Bio2+')"
            End If
            If Common.servername = "Access" Then
                cmd1 = New OleDbCommand(sSql, Common.con1)
                cmd1.ExecuteNonQuery()
            Else
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
            End If
        End If
        If Bio2.Template.Length > 0 Then
            For x As Integer = 0 To Bio2.Template.Length - 1
                Dim TemplateArr() As String = Bio2.Template(x).Split(";")
                sSql = "select EMachineNumber from fptable where EMachineNumber = '" & Bio2.EMachineNumber & "' and EnrollNumber ='" & EnrollNumber & "'  and FingerNumber = '" & TemplateArr(1) & "' and DeviceType='Bio2+'"
                ds = New DataSet
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(ds)
                Else
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(ds)
                End If
                If ds.Tables(0).Rows.Count > 0 Then
                    sSql = "update fptable set Template='" & TemplateArr(0) & "' where EMachineNumber = '" & Bio2.EMachineNumber & "' and EnrollNumber ='" & EnrollNumber & "'  and FingerNumber = '" & TemplateArr(1) & "' and DeviceType='Bio2+'"
                Else
                    sSql = "insert into fptable (Emachinenumber, EnrollNumber, UserName, Template, FingerNumber, Privilege, DeviceType) values ('" & Bio2.EMachineNumber & "','" & EnrollNumber & "', '" & Bio2.UserName & "','" & TemplateArr(0) & "', '" & TemplateArr(1) & "', '" & Bio2.Privilege & "','Bio2+')"
                End If
                If Common.servername = "Access" Then
                    cmd1 = New OleDbCommand(sSql, Common.con1)
                    cmd1.ExecuteNonQuery()
                Else
                    cmd = New SqlCommand(sSql, Common.con)
                    cmd.ExecuteNonQuery()
                End If
            Next
        End If
    End Sub
    Private Sub SaveEF45Template(ByVal userinfo As CMITech.UMXClient.Entities.UserInfo, ByVal Emachinenumber As Integer, ByVal _client As Client, ByRef UserName As String)
        Try
            Dim sdwEnrollNumber As String
            If IsNumeric(userinfo.UUID) Then
                sdwEnrollNumber = Convert.ToInt64(userinfo.UUID).ToString("000000000000")
            Else
                sdwEnrollNumber = userinfo.UUID
            End If
            XtraMasterTest.LabelControlStatus.Text = "Downloading Template " & sdwEnrollNumber
            Application.DoEvents()

            Dim sSql As String = "select * from fptable where Enrollnumber='" & sdwEnrollNumber & "' and DeviceType ='EF45' and EMachineNumber=" & Emachinenumber & ""
            Dim dsRecord As DataSet = New DataSet
            Dim adapA As OleDbDataAdapter
            Dim adap As SqlDataAdapter
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(dsRecord)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(dsRecord)
            End If
            Dim subject As CMITech.UMXClient.Entities.Subject = _client.GetSubject(userinfo.UUID)
            UserName = subject.LastName
            Dim FingerNumber As Integer
            If subject.EnrolTemplate.LeftEyeTemplate IsNot Nothing Or subject.EnrolTemplate.RightEyeTemplate IsNot Nothing Then
                FingerNumber = 99
            Else
                FingerNumber = 11
            End If
            If dsRecord.Tables(0).Rows.Count = 0 Then
                    sSql = "insert into fptable([EnrollNumber],[LeftEye],[RightEye],[EMachineNumber],[UserName], [DeviceType],[Cardnumber],[Password],[Privilege],[FingerNumber]) values " &
               "('" & sdwEnrollNumber & "',@LeftEye,@RightEye," & Emachinenumber & ",'" & subject.LastName & "', 'EF45','" & userinfo.Card & "','" & userinfo.Pin & "', " & userinfo.Admin & ", " & FingerNumber & ")"
            Else
                sSql = "Update fptable set LeftEye=@LeftEye,RightEye=@RightEye,UserName='" & subject.LastName & "', DeviceType='EF45',Cardnumber='" & userinfo.Card & "',Password='" & userinfo.Pin & "',Privilege=" & userinfo.Admin & ", FingerNumber=" & FingerNumber & " where EnrollNumber='" & sdwEnrollNumber & "' and EMachineNumber=" & Emachinenumber & ""
            End If

            'temp = System.Text.ASCIIEncoding.ASCII.GetString(subject.EnrolTemplate.LeftEyeTemplate)
            If subject.EnrolTemplate.LeftEyeTemplate Is Nothing Then
                sSql = sSql.Replace("[LeftEye]=@LeftEye,", "").Replace("@LeftEye,", "").Replace("[LeftEye],", "")
            End If
            If subject.EnrolTemplate.RightEyeTemplate Is Nothing Then
                sSql = sSql.Replace("[RightEye]=@RightEye,", "").Replace("@RightEye,", "").Replace("[RightEye],", "")
            End If

            If Common.servername = "Access" Then
                If Common.con1.State <> System.Data.ConnectionState.Open Then
                    Common.con1.Open()
                End If
                cmd1 = New OleDbCommand(sSql, Common.con1)
                'cmd1.CommandTimeout = 1800
                If subject.EnrolTemplate.LeftEyeTemplate IsNot Nothing Then
                    cmd1.Parameters.Add("@LeftEye", OleDbType.VarBinary, subject.EnrolTemplate.LeftEyeTemplate.Length).Value = subject.EnrolTemplate.LeftEyeTemplate
                End If
                If subject.EnrolTemplate.RightEyeTemplate IsNot Nothing Then
                    cmd1.Parameters.Add("@RightEye", OleDbType.VarBinary, subject.EnrolTemplate.RightEyeTemplate.Length).Value = subject.EnrolTemplate.RightEyeTemplate
                End If
                Dim RowsCount As Integer = cmd1.ExecuteNonQuery()
                If Common.con1.State <> System.Data.ConnectionState.Closed Then
                    Common.con1.Close()
                End If
            Else
                If Common.con.State <> System.Data.ConnectionState.Open Then
                    Common.con.Open()
                End If

                cmd = New SqlCommand(sSql, Common.con)
                cmd.CommandTimeout = 1800
                cmd.Parameters.AddWithValue("@EnrollNumber", sdwEnrollNumber)
                If subject.EnrolTemplate.LeftEyeTemplate IsNot Nothing Then
                    cmd.Parameters.AddWithValue("@LeftEye", subject.EnrolTemplate.LeftEyeTemplate)
                End If
                If subject.EnrolTemplate.RightEyeTemplate IsNot Nothing Then
                    cmd.Parameters.AddWithValue("@RightEye", subject.EnrolTemplate.RightEyeTemplate)
                End If
                If subject.EnrolTemplate.LeftEyeTemplate IsNot Nothing Or subject.EnrolTemplate.RightEyeTemplate IsNot Nothing Then
                    cmd.Parameters.AddWithValue("@FingerNumber", 99)
                Else
                    cmd.Parameters.AddWithValue("@FingerNumber", 11)
                End If
                cmd.Parameters.AddWithValue("@EMachineNumber", Emachinenumber)
                cmd.Parameters.AddWithValue("@UserName", subject.LastName)
                cmd.Parameters.AddWithValue("@DeviceType", "EF45")
                cmd.Parameters.AddWithValue("@Cardnumber", userinfo.Card)
                cmd.Parameters.AddWithValue("@Password", userinfo.Pin)
                cmd.Parameters.AddWithValue("@Privilege", userinfo.Admin)
                cmd.CommandType = CommandType.Text
                Dim RowsCount As Integer = cmd.ExecuteNonQuery()
                If Common.con.State <> System.Data.ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If

        Catch ex As Exception

        End Try
    End Sub
    Private Sub cmdGetAllEnrollData_BIO_all(mMachine_ID As Long, vResult As Long, ByRef paycodelist As List(Of String))
        Dim sSql As String = ""
        Dim vEnrollNumber As Long
        Dim vStrEnrollNumber As String = ""
        Dim vEMachineNumber As Long
        Dim vFingerNumber As Long
        Dim vPrivilege As Long
        Dim vEnable As Long
        Dim vFlag As Boolean
        Dim vRet As Boolean
        Dim vErrorCode As Long
        Dim vStr As String
        ' Dim vRet As Boolean
        Dim recFP As New DataSet 'ADODB.Recordset
        Dim i As Long
        Dim vTitle As String
        Dim rsEmp As New DataSet 'ADODB.Recordset
        Dim RsFp As New DataSet 'ADODB.Recordset
        Dim sEcardnumber As String
        Dim mMachineNumber As Long
        Dim vVerifyMode As Long

        Dim nPhotoLen As Long
        Dim nFKRetCode As Long
        Dim vbytEnrollData() As Byte
        Dim vbytConvFpData() As Byte

        Dim vBackupNumber As Long

        Dim vEnrollName As String
        Dim vnEnableFlag As Long
        Dim vnMessRet As Long

        Dim vnResultCode As Long
        Dim vnii As Long
        Dim vnLen As Long
        'Dim vbytEnrollData() As Byte
        'Dim vbytConvFpData() As Byte
        Dim mnCommHandleIndex As Long


        'On Local Error Resume Next
        mMachineNumber = mMachine_ID
        'vRet = FK.EnableDevice(False)
        mnCommHandleIndex = vResult
        'sleep (3500)
        vnResultCode = FK_EnableDevice(mnCommHandleIndex, 0)
        If vnResultCode < 0 Then
            'MsgBox("Unable To connect " & mMachine_ID & "", vbOKOnly)
            XtraMessageBox.Show(ulf, "<size=10>Unable to connect Device Id " & mMachine_ID & "</size>", "<size=9>Information</size>")
            Exit Sub
        End If
        'Sleep (3500)
        vnResultCode = FK_ReadAllUserID(mnCommHandleIndex)
        If vnResultCode >= 1 Then
            '    lblMessage.Caption = "ReadAllUserID OK"
        Else
            'MsgBox("Unable to Read users from Device", vbOKOnly) 'FK_GetLastError vErrorCode
            XtraMessageBox.Show(ulf, "<size=10>Unable to Read users from Device</size>", "<size=9>Information</size>")
            'MsgBox "", vErrorCode
            vnResultCode = FK_EnableDevice(mnCommHandleIndex, 1)
            FK_DisConnect(mnCommHandleIndex)
            Exit Sub
        End If
        Dim UserCount As Integer = 0
        Dim vnValue As Integer
        Dim count As Integer = 0
        vnResultCode = FK_GetDeviceStatus(mnCommHandleIndex, GET_USERS, vnValue)
        If vnResultCode = RUN_SUCCESS Then
            UserCount = vnValue
        End If
        '---- Get Enroll data and save into database -------------
        Me.Cursor = Cursors.WaitCursor
        vFlag = False
        'With datEnroll
        'gGetState = True
        '.RecordSource = "select * from " & "tblEnroll"
        '.Refresh
        Dim isStringId As Boolean
        If FK_GetIsSupportStringID(mnCommHandleIndex) = RUN_SUCCESS Then
            isStringId = True
        Else
            isStringId = False
        End If
        Do
            Dim EnrollNumber As String
            Try
                If isStringId Then
FFFS:
                    vnResultCode = FK_GetAllUserID_StringID(mnCommHandleIndex, _
                                                         vStrEnrollNumber, _
                                                         vBackupNumber, _
                                                         vPrivilege, _
                                                         vnEnableFlag)
                    If vnResultCode <> RUN_SUCCESS Then
                        If vnResultCode = RUNERR_DATAARRAY_END Then
                            vnResultCode = RUN_SUCCESS
                        End If
                        Exit Do
                    End If
EEES:
                    vnResultCode = FK_GetEnrollData_StringID(mnCommHandleIndex, _
                                                          vStrEnrollNumber.Trim, _
                                                          vBackupNumber, _
                                                          vPrivilege, _
                                                          mbytEnrollData, _
                                                          mlngPasswordData)
                    vEnrollNumber = vStrEnrollNumber.Trim
                Else
FFF:
                    vnResultCode = FK_GetAllUserID(mnCommHandleIndex, _
                                                         vEnrollNumber, _
                                                         vBackupNumber, _
                                                         vPrivilege, _
                                                         vnEnableFlag)
                    If vnResultCode <> RUN_SUCCESS Then
                        If vnResultCode = RUNERR_DATAARRAY_END Then
                            vnResultCode = RUN_SUCCESS
                        End If
                        Exit Do
                    End If
EEE:
                    vnResultCode = FK_GetEnrollData(mnCommHandleIndex, _
                                                          vEnrollNumber, _
                                                          vBackupNumber, _
                                                          vPrivilege, _
                                                          mbytEnrollData, _
                                                          mlngPasswordData)
                End If


                If vnResultCode <> RUN_SUCCESS Then
                    GoTo LLL
                End If
                Dim CardNo As String
                If vBackupNumber = 11 Then
                    'mbytEnrollData
                    CardNo = System.Text.Encoding.UTF8.GetString(mbytEnrollData)
                End If
                XtraMasterTest.LabelControlStatus.Text = "Downloading Template " & vEnrollNumber & " From Device No. " & mMachine_ID
                XtraMasterTest.LabelControlCount.Text = "Downloading " & count + 1 & " of " & UserCount
                Application.DoEvents()

                Dim adap As SqlDataAdapter
                Dim adapA As OleDbDataAdapter
                rsEmp = New DataSet
                sSql = "select empname from tblemployee where presentcardno='" & vEnrollNumber.ToString("000000000000") & "'"
                'rsEmp = Cn.Execute(sSql)
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(rsEmp)
                Else
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(rsEmp)
                End If

                sSql = "select * from fptable where Enrollnumber='" & vEnrollNumber.ToString("000000000000") & "' and FingerNumber=" & vBackupNumber & " and DeviceType ='Bio' and EMachineNumber='" & mMachine_ID & "'"
                'RsFp = Cn.Execute(sSql)
                RsFp = New DataSet
                If Common.servername = "Access" Then
                    sSql = "select * from fptable where Enrollnumber='" & vEnrollNumber.ToString("000000000000") & "' and FingerNumber=" & vBackupNumber & " and DeviceType ='Bio' and EMachineNumber=" & mMachine_ID & ""
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(RsFp)
                Else
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(RsFp)
                End If


                If RsFp.Tables(0).Rows.Count > 0 Then
                    sSql = "DELETE FROM FPTABLE WHERE Enrollnumber='" & vEnrollNumber.ToString("000000000000") & "' and FingerNumber=" & vBackupNumber & " and DeviceType ='Bio' and EMachineNumber='" & mMachine_ID & "'"
                    'Cn.Execute(sSql)
                    If Common.servername = "Access" Then
                        sSql = "DELETE FROM FPTABLE WHERE Enrollnumber='" & vEnrollNumber.ToString("000000000000") & "' and FingerNumber=" & vBackupNumber & " and DeviceType ='Bio' and EMachineNumber=" & mMachine_ID & ""
                        If Common.con1.State <> ConnectionState.Open Then
                            Common.con1.Open()
                        End If
                        cmd1 = New OleDbCommand(sSql, Common.con1)
                        cmd1.ExecuteNonQuery()
                        If Common.con1.State <> ConnectionState.Closed Then
                            Common.con1.Close()
                        End If
                    Else
                        If Common.con.State <> ConnectionState.Open Then
                            Common.con.Open()
                        End If
                        cmd = New SqlCommand(sSql, Common.con)
                        cmd.ExecuteNonQuery()
                        If Common.con.State <> ConnectionState.Closed Then
                            Common.con.Close()
                        End If
                    End If
                End If
                'RsFp.Close()
                'RsFp = Nothing
                RsFp = New DataSet
                recFP = New DataSet

                'recFP.Open("select * from fptable", Cn, adOpenKeyset, adLockOptimistic)
                'recFP.AddNew()
                Dim s As String = "select * from fptable"
                Dim Emachinenumber As String
                'Dim EnrollNumber As String
                Dim UserName As String
                'Dim Template_Tw As String
                Dim Template_Tw() As Byte
                Dim FingerNumber As String
                Dim Privilege As String
                Dim Verifymode As String
                Dim Template_Face As String


                'recFP.Fields("Emachinenumber") = CLng(cboMachineNo.Text)
                Emachinenumber = mMachine_ID
                'recFP.Fields("EnrollNumber") = Format(CStr(vEnrollNumber), "00000000")
                'EnrollNumber = Format(CStr(vEnrollNumber), "00000000")
                EnrollNumber = vEnrollNumber.ToString("000000000000")
                If rsEmp.Tables(0).Rows.Count > 0 Then
                    'recFP.Fields("UserName") = IIf(IsNull(rsEmp(0)), " ", rsEmp(0))
                    UserName = rsEmp.Tables(0).Rows(0).Item("empname").ToString.Trim
                Else
                    'recFP.Fields("UserName") = ""
                    UserName = ""
                End If

                If vBackupNumber = BACKUP_PSW Then
                    ReDim vbytEnrollData(PWD_DATA_SIZE - 1)
                    ReDim Template_Tw(PWD_DATA_SIZE - 1)
                    'CopyMemory(vbytEnrollData(0), mbytEnrollData(0), PWD_DATA_SIZE)
                    Array.Copy(mbytEnrollData, vbytEnrollData, PWD_DATA_SIZE)
                    'recFP.Fields("Template_Tw") = vbytEnrollData
                    Array.Copy(vbytEnrollData, Template_Tw, PWD_DATA_SIZE)
                    'Template_Tw = Encoding.UTF8.GetString(vbytEnrollData, 0, vbytEnrollData.Length)
                ElseIf vBackupNumber = BACKUP_PSW Or vBackupNumber = BACKUP_CARD Then
                    ReDim vbytEnrollData(PWD_DATA_SIZE - 1)
                    ReDim Template_Tw(PWD_DATA_SIZE - 1)
                    Array.Copy(mbytEnrollData, vbytEnrollData, PWD_DATA_SIZE)
                    Array.Copy(vbytEnrollData, Template_Tw, PWD_DATA_SIZE)
                    'Template_Tw = Encoding.UTF8.GetString(vbytEnrollData, 0, vbytEnrollData.Length)
                    'Template_Tw = UnicodeBytesToString(vbytEnrollData)
                ElseIf vBackupNumber >= BACKUP_FP_0 And vBackupNumber <= BACKUP_FP_9 Then
                    ReDim vbytEnrollData(FP_DATA_SIZE - 1)
                    ReDim vbytConvFpData(FP_DATA_SIZE - 1)
                    ReDim Template_Tw(FP_DATA_SIZE - 1)
                    Array.Copy(mbytEnrollData, vbytEnrollData, FP_DATA_SIZE)
                    ConvFpDataToSaveInDbForCompatibility(vbytEnrollData, vbytConvFpData)
                    'Template_Tw = Encoding.UTF8.GetString(vbytConvFpData, 0, vbytConvFpData.Length)
                    Array.Copy(vbytConvFpData, Template_Tw, FP_DATA_SIZE)
                ElseIf vBackupNumber = BACKUP_FACE Then
                    ReDim vbytEnrollData(FACE_DATA_SIZE - 1)
                    ReDim Template_Tw(FACE_DATA_SIZE - 1)
                    Array.Copy(mbytEnrollData, vbytEnrollData, FACE_DATA_SIZE)
                    Array.Copy(vbytEnrollData, Template_Tw, FACE_DATA_SIZE)
                    'Template_Tw = Encoding.UTF8.GetString(vbytEnrollData, 0, vbytEnrollData.Length)
                ElseIf vBackupNumber = BACKUP_VEIN_0 Then
                    ReDim vbytEnrollData(VEIN_DATA_SIZE - 1)
                    ReDim Template_Tw(VEIN_DATA_SIZE - 1)
                    Array.Copy(mbytEnrollData, vbytEnrollData, VEIN_DATA_SIZE)
                    Array.Copy(vbytEnrollData, Template_Tw, VEIN_DATA_SIZE)
                    'Template_Tw = Encoding.UTF8.GetString(vbytEnrollData, 0, vbytEnrollData.Length)
                End If

                'recFP.Fields("FingerNumber") = vBackupNumber
                'recFP.Fields("Privilege") = vPrivilege
                'recFP.Fields("Verifymode") = vVerifyMode

                FingerNumber = vBackupNumber
                Privilege = vPrivilege
                Verifymode = vVerifyMode

                'By yogesh for bio 7

                If vBackupNumber = 0 Then
                    ReDim mbytPhotoImage(4)
                    nPhotoLen = 0
                    nFKRetCode = FK_GetEnrollPhoto(mnCommHandleIndex, _
                                            vEnrollNumber, _
                                            mbytPhotoImage, _
                                            nPhotoLen)
                    If nFKRetCode <> RUN_SUCCESS Then
                        'lblMessage.Caption = ReturnResultPrint(nFKRetCode)
                        FK_EnableDevice(mnCommHandleIndex, 1)
                    Else
                        ReDim mbytPhotoImage(nPhotoLen - 1)
                        nFKRetCode = FK_GetEnrollPhoto(mnCommHandleIndex, _
                                            vEnrollNumber, _
                                            mbytPhotoImage, _
                                            nPhotoLen)
                        nPhotoLen = UBound(mbytPhotoImage) + 1
                        If nPhotoLen > 1 Then
                            'recFP.Fields("Template_Face") = mbytPhotoImage
                            Template_Face = mbytPhotoImage(0)
                        End If
                    End If
                End If
                'end
                'recFP.Update()
                'recFP.Close()
                recFP = Nothing

                Dim sqlinsert As String = "insert into fptable (Emachinenumber, EnrollNumber, UserName, Template_Tw, FingerNumber, Privilege, Verifymode, Template_Face, DeviceType) values (" & Emachinenumber & ",'" & EnrollNumber & "', '" & UserName & "', @Template_Tw, '" & FingerNumber & "', '" & Privilege & "', '" & Verifymode & "', '" & Template_Face & "','Bio')"

                If Common.servername = "Access" Then
                    If Common.con1.State <> ConnectionState.Open Then
                        Common.con1.Open()
                    End If
                    cmd1 = New OleDbCommand(sqlinsert, Common.con1)
                    'Using picture As Image = Image.FromFile("./fb")
                    Using stream As New IO.MemoryStream
                        'picture.Save(stream, Imaging.ImageFormat.Jpeg)
                        cmd1.Parameters.Add("@Template_Tw", OleDb.OleDbType.VarBinary).Value = Template_Tw 'stream.GetBuffer()
                        cmd1.ExecuteNonQuery()
                    End Using
                    If Common.con1.State <> ConnectionState.Closed Then
                        Common.con1.Close()
                    End If
                    'End Using
                Else
                    If Common.con.State <> ConnectionState.Open Then
                        Common.con.Open()
                    End If
                    cmd = New SqlCommand(sqlinsert, Common.con)
                    Using stream As New IO.MemoryStream
                        cmd.Parameters.Add("@Template_Tw", SqlDbType.VarBinary).Value = Template_Tw 'vbytEnrollData
                        cmd.ExecuteNonQuery()
                    End Using
                    If Common.con.State <> ConnectionState.Closed Then
                        Common.con.Close()
                    End If
                End If
LLL:
            Catch ex As Exception

            End Try
            If IsNumeric(EnrollNumber) Then
                EnrollNumber = Convert.ToDouble(EnrollNumber)
            End If
            paycodelist.Add(EnrollNumber & ";" & "")
            count = count + 1
        Loop
        'gGetState = False
        'vRet = FK_EnableDevice(mnCommHandleIndex, 1)
        'FK_DisConnect(mnCommHandleIndex)

        Me.Cursor = Cursors.Default
    End Sub
    '    Private Sub cmdGetAllEnrollData_BIO_ONE(mMachine_ID As Long, mEnrollNumber As String, ByRef paycodelist As List(Of String), mnCommHandleIndex As Long)
    '        'Dim paycodelist As New List(Of String)()
    '        'Dim paycodeArray '= paycodelist.Distinct.ToArray ' paycodelist.ToArray

    '        Dim sSql As String = ""
    '        Dim vEnrollNumber As Long
    '        Dim vStrEnrollNumber As String
    '        Dim vEMachineNumber As Long
    '        Dim vFingerNumber As Long
    '        Dim vPrivilege As Long
    '        Dim vEnable As Long
    '        Dim vFlag As Boolean
    '        Dim vRet As Boolean
    '        Dim vErrorCode As Long
    '        Dim vStr As String
    '        ' Dim vRet As Boolean
    '        Dim recFP As New DataSet 'ADODB.Recordset
    '        Dim i As Long
    '        Dim vTitle As String
    '        Dim rsEmp As New DataSet 'ADODB.Recordset
    '        Dim RsFp As New DataSet 'ADODB.Recordset
    '        Dim sEcardnumber As String
    '        Dim mMachineNumber As Long
    '        Dim vVerifyMode As Long

    '        Dim vBackupNumber As Long

    '        Dim vEnrollName As String
    '        Dim vnEnableFlag As Long
    '        Dim vnMessRet As Long

    '        '    Dim vnii As Long
    '        '   Dim vnLen As Long
    '        Dim vbytEnrollData() As Byte
    '        Dim vbytConvFpData() As Byte

    '        Dim vnResultCode As Long
    '        Dim vnii As Long
    '        Dim vnLen As Long

    '        vEnrollNumber = mEnrollNumber
    '        vStrEnrollNumber = mEnrollNumber
    '        mMachineNumber = mMachine_ID
    '        vRet = FK_EnableDevice(mnCommHandleIndex, 0)
    '        If vRet = False Then
    '            XtraMessageBox.Show(ulf, "<size=10>Unable to connect Device Id " & mMachine_ID & "</size>", "<size=9>Information</size>")
    '            'MsgBox("Unable to connect Device Id " & mMachine_ID & "", vbOKOnly)
    '            Exit Sub
    '        End If

    '        Dim k As Integer

    '        '---- Get Enroll data and save into database -------------
    '        Me.Cursor = Cursors.WaitCursor
    '        vFlag = False

    '        Dim isStringId As Boolean
    '        If FK_GetIsSupportStringID(mnCommHandleIndex) = RUN_SUCCESS Then
    '            isStringId = True
    '        Else
    '            isStringId = False
    '        End If

    '        For k = 0 To 11
    '            vBackupNumber = k
    '            'If isStringId Then
    '            '    vnResultCode = FK_GetEnrollData_StringID(mnCommHandleIndex, _
    '            '                             vStrEnrollNumber, _
    '            '                             vBackupNumber, _
    '            '                             vPrivilege, _
    '            '                             mbytEnrollData, _
    '            '                             mlngPasswordData)
    '            '    vEnrollNumber = vStrEnrollNumber.Trim

    '            'Else
    '            '    vnResultCode = FK_GetEnrollData(mnCommHandleIndex, _
    '            '                             vEnrollNumber, _
    '            '                             vBackupNumber, _
    '            '                             vPrivilege, _
    '            '                             mbytEnrollData, _
    '            '                             mlngPasswordData)

    '            'End If



    '            'test
    '            If isStringId Then
    'FFFS:
    '                vnResultCode = FK_GetAllUserID_StringID(mnCommHandleIndex, _
    '                                                     vStrEnrollNumber, _
    '                                                     vBackupNumber, _
    '                                                     vPrivilege, _
    '                                                     vnEnableFlag)
    '                If vnResultCode <> RUN_SUCCESS Then
    '                    If vnResultCode = RUNERR_DATAARRAY_END Then
    '                        vnResultCode = RUN_SUCCESS
    '                    End If                   
    '                End If
    'EEES:
    '                vnResultCode = FK_GetEnrollData_StringID(mnCommHandleIndex, _
    '                                                      vStrEnrollNumber.Trim, _
    '                                                      vBackupNumber, _
    '                                                      vPrivilege, _
    '                                                      mbytEnrollData, _
    '                                                      mlngPasswordData)
    '                vEnrollNumber = vStrEnrollNumber.Trim
    '            Else
    '                'FFF:
    '                vnResultCode = FK_GetAllUserID(mnCommHandleIndex, _
    '                                                     vEnrollNumber, _
    '                                                     vBackupNumber, _
    '                                                     vPrivilege, _
    '                                                     vnEnableFlag)
    '                If vnResultCode <> RUN_SUCCESS Then
    '                    If vnResultCode = RUNERR_DATAARRAY_END Then
    '                        vnResultCode = RUN_SUCCESS
    '                    End If                   
    '                End If
    'EEE:
    '                vnResultCode = FK_GetEnrollData(mnCommHandleIndex, _
    '                                                      vEnrollNumber, _
    '                                                      vBackupNumber, _
    '                                                      vPrivilege, _
    '                                                      mbytEnrollData, _
    '                                                      mlngPasswordData)
    '            End If
    '            'end test


    '            If vnResultCode <> RUN_SUCCESS Then
    '                GoTo FFF
    '            End If

    '            XtraMasterTest.LabelControlStatus.Text = "Downloading Template " & vEnrollNumber & " From Device No. " & mMachine_ID
    '            Application.DoEvents()

    '            Dim adap As SqlDataAdapter
    '            Dim adapA As OleDbDataAdapter
    '            rsEmp = New DataSet

    '            sSql = "select empname from tblemployee where presentcardno='" & vEnrollNumber.ToString("000000000000") & "'"
    '            'rsEmp = Cn.Execute(sSql)
    '            If Common.servername = "Access" Then
    '                adapA = New OleDbDataAdapter(sSql, Common.con1)
    '                adapA.Fill(rsEmp)
    '            Else
    '                adap = New SqlDataAdapter(sSql, Common.con)
    '                adap.Fill(rsEmp)
    '            End If

    '            sSql = "select * from fptable where Enrollnumber='" & vEnrollNumber.ToString("000000000000") & "' and FingerNumber=" & vBackupNumber & " and DeviceType ='Bio' and EMachineNumber='" & mMachine_ID & "'"
    '            'RsFp = Cn.Execute(sSql)
    '            RsFp = New DataSet
    '            If Common.servername = "Access" Then
    '                adapA = New OleDbDataAdapter(sSql, Common.con1)
    '                adapA.Fill(RsFp)
    '            Else
    '                adap = New SqlDataAdapter(sSql, Common.con)
    '                adap.Fill(RsFp)
    '            End If

    '            If RsFp.Tables(0).Rows.Count > 0 Then
    '                sSql = "DELETE FROM FPTABLE WHERE Enrollnumber='" & vEnrollNumber.ToString("000000000000") & "' and FingerNumber=" & vBackupNumber & " and DeviceType ='Bio' and EMachineNumber='" & mMachine_ID & "'"
    '                'Cn.Execute(sSql)
    '                If Common.servername = "Access" Then
    '                    If Common.con1.State <> ConnectionState.Open Then
    '                        Common.con1.Open()
    '                    End If
    '                    cmd1 = New OleDbCommand(sSql, Common.con1)
    '                    cmd1.ExecuteNonQuery()
    '                    If Common.con1.State <> ConnectionState.Closed Then
    '                        Common.con1.Close()
    '                    End If
    '                Else
    '                    If Common.con.State <> ConnectionState.Open Then
    '                        Common.con.Open()
    '                    End If
    '                    cmd = New SqlCommand(sSql, Common.con)
    '                    cmd.ExecuteNonQuery()
    '                    If Common.con.State <> ConnectionState.Closed Then
    '                        Common.con.Close()
    '                    End If
    '                End If

    '            End If
    '            RsFp = New DataSet
    '            recFP = New DataSet
    '            Dim s As String = "select * from fptable"
    '            Dim Emachinenumber As String
    '            Dim EnrollNumber As String
    '            Dim UserName As String
    '            'Dim Template_Tw As String
    '            Dim FingerNumber As String
    '            Dim Privilege As String
    '            Dim Verifymode As String
    '            Dim Template_Face As String


    '            'recFP.Fields("Emachinenumber") = CLng(cboMachineNo.Text)
    '            Emachinenumber = mMachine_ID
    '            'recFP.Fields("EnrollNumber") = Format(CStr(vEnrollNumber), "00000000")
    '            EnrollNumber = Format(CStr(vEnrollNumber), "000000000000")
    '            EnrollNumber = vEnrollNumber.ToString("000000000000")
    '            If rsEmp.Tables(0).Rows.Count > 0 Then
    '                'recFP.Fields("UserName") = IIf(IsNull(rsEmp(0)), " ", rsEmp(0))
    '                UserName = rsEmp.Tables(0).Rows(0).Item("empname").ToString.Trim
    '            Else
    '                'recFP.Fields("UserName") = ""
    '                UserName = ""
    '            End If


    '            'If vBackupNumber = BACKUP_PSW Or vBackupNumber = BACKUP_CARD Then
    '            '    ReDim vbytEnrollData(PWD_DATA_SIZE - 1)
    '            '    Array.Copy(mbytEnrollData, vbytEnrollData, PWD_DATA_SIZE)
    '            '    Template_Tw = Encoding.UTF8.GetString(vbytEnrollData, 0, vbytEnrollData.Length)
    '            '    'Template_Tw = UnicodeBytesToString(vbytEnrollData)
    '            'ElseIf vBackupNumber >= BACKUP_FP_0 And vBackupNumber <= BACKUP_FP_9 Then
    '            '    ReDim vbytEnrollData(FP_DATA_SIZE - 1)
    '            '    ReDim vbytConvFpData(FP_DATA_SIZE - 1)
    '            '    Array.Copy(mbytEnrollData, vbytEnrollData, FP_DATA_SIZE)
    '            '    ConvFpDataToSaveInDbForCompatibility(vbytEnrollData, vbytConvFpData)
    '            '    Template_Tw = Encoding.UTF8.GetString(vbytEnrollData, 0, vbytEnrollData.Length)
    '            'ElseIf vBackupNumber = BACKUP_FACE Then
    '            '    ReDim vbytEnrollData(FACE_DATA_SIZE - 1)
    '            '    Array.Copy(mbytEnrollData, vbytEnrollData, FACE_DATA_SIZE)
    '            '    Template_Tw = Encoding.UTF8.GetString(vbytEnrollData, 0, vbytEnrollData.Length)
    '            'ElseIf vBackupNumber = BACKUP_VEIN_0 Then
    '            '    ReDim vbytEnrollData(VEIN_DATA_SIZE - 1)
    '            '    Array.Copy(mbytEnrollData, vbytEnrollData, VEIN_DATA_SIZE)
    '            '    Template_Tw = Encoding.UTF8.GetString(vbytEnrollData, 0, vbytEnrollData.Length)
    '            'End If

    '            'test
    '            Dim Template_Tw() As Byte
    '            If vBackupNumber = BACKUP_PSW Then
    '                ReDim vbytEnrollData(PWD_DATA_SIZE - 1)
    '                'CopyMemory(vbytEnrollData(0), mbytEnrollData(0), PWD_DATA_SIZE)
    '                Array.Copy(mbytEnrollData, vbytEnrollData, PWD_DATA_SIZE)
    '                'recFP.Fields("Template_Tw") = vbytEnrollData
    '                Array.Copy(vbytEnrollData, Template_Tw, PWD_DATA_SIZE)
    '                'Template_Tw = Encoding.UTF8.GetString(vbytEnrollData, 0, vbytEnrollData.Length)
    '            ElseIf vBackupNumber = BACKUP_PSW Or vBackupNumber = BACKUP_CARD Then
    '                ReDim vbytEnrollData(PWD_DATA_SIZE - 1)
    '                ReDim Template_Tw(PWD_DATA_SIZE - 1)
    '                Array.Copy(mbytEnrollData, vbytEnrollData, PWD_DATA_SIZE)
    '                Array.Copy(vbytEnrollData, Template_Tw, PWD_DATA_SIZE)
    '                'Template_Tw = Encoding.UTF8.GetString(vbytEnrollData, 0, vbytEnrollData.Length)
    '                'Template_Tw = UnicodeBytesToString(vbytEnrollData)
    '            ElseIf vBackupNumber >= BACKUP_FP_0 And vBackupNumber <= BACKUP_FP_9 Then
    '                ReDim vbytEnrollData(FP_DATA_SIZE - 1)
    '                ReDim vbytConvFpData(FP_DATA_SIZE - 1)
    '                ReDim Template_Tw(FP_DATA_SIZE - 1)
    '                Array.Copy(mbytEnrollData, vbytEnrollData, FP_DATA_SIZE)
    '                ConvFpDataToSaveInDbForCompatibility(vbytEnrollData, vbytConvFpData)
    '                'Template_Tw = Encoding.UTF8.GetString(vbytConvFpData, 0, vbytConvFpData.Length)
    '                Array.Copy(vbytConvFpData, Template_Tw, FP_DATA_SIZE)
    '            ElseIf vBackupNumber = BACKUP_FACE Then
    '                ReDim vbytEnrollData(FACE_DATA_SIZE - 1)
    '                ReDim Template_Tw(FACE_DATA_SIZE - 1)
    '                Array.Copy(mbytEnrollData, vbytEnrollData, FACE_DATA_SIZE)
    '                Array.Copy(vbytEnrollData, Template_Tw, FACE_DATA_SIZE)
    '                'Template_Tw = Encoding.UTF8.GetString(vbytEnrollData, 0, vbytEnrollData.Length)
    '            ElseIf vBackupNumber = BACKUP_VEIN_0 Then
    '                ReDim vbytEnrollData(VEIN_DATA_SIZE - 1)
    '                ReDim Template_Tw(VEIN_DATA_SIZE - 1)
    '                Array.Copy(mbytEnrollData, vbytEnrollData, VEIN_DATA_SIZE)
    '                Array.Copy(vbytEnrollData, Template_Tw, VEIN_DATA_SIZE)
    '                'Template_Tw = Encoding.UTF8.GetString(vbytEnrollData, 0, vbytEnrollData.Length)
    '            End If
    '            'end test

    '            'recFP.Fields("FingerNumber") = vBackupNumber
    '            'recFP.Fields("Privilege") = vPrivilege
    '            'recFP.Fields("Verifymode") = vVerifyMode

    '            FingerNumber = vBackupNumber
    '            Privilege = vPrivilege
    '            Verifymode = vVerifyMode

    '            'end
    '            'recFP.Update()
    '            'recFP.Close()
    '            recFP = Nothing

    '            Dim sqlinsert As String = "insert into fptable (Emachinenumber, EnrollNumber, UserName, Template_Tw, FingerNumber, Privilege, Verifymode, Template_Face, DeviceType) values (" & Emachinenumber & ",'" & EnrollNumber & "', '" & UserName & "', @Template_Tw, '" & FingerNumber & "', '" & Privilege & "', '" & Verifymode & "', '" & Template_Face & "','Bio')"

    '            If Common.servername = "Access" Then
    '                Common.con1.Open()
    '                cmd1 = New OleDbCommand(sqlinsert, Common.con1)
    '                'Using picture As Image = Image.FromFile("~/fb")
    '                Using stream As New IO.MemoryStream
    '                    'picture.Save(stream, Imaging.ImageFormat.Jpeg)
    '                    cmd1.Parameters.Add("@Template_Tw", OleDb.OleDbType.VarBinary).Value = Template_Tw 'vbytEnrollData 
    '                    cmd1.ExecuteNonQuery()
    '                    Common.con1.Close()
    '                End Using
    '            Else
    '                Common.con.Open()
    '                cmd = New SqlCommand(sqlinsert, Common.con)
    '                Using stream As New IO.MemoryStream
    '                    cmd.Parameters.Add("@Template_Tw", SqlDbType.VarBinary).Value = Template_Tw 'vbytEnrollData
    '                    cmd.ExecuteNonQuery()
    '                End Using
    '                Common.con.Close()
    '            End If
    'FFF:
    '            If IsNumeric(EnrollNumber) Then
    '                EnrollNumber = Convert.ToDouble(EnrollNumber)
    '            End If
    '            paycodelist.Add(EnrollNumber & ";" & "")
    '        Next k

    '        'vRet = FK_EnableDevice(mnCommHandleIndex, 1)
    '        'FK_DisConnect(mnCommHandleIndex)
    '        Me.Cursor = Cursors.Default
    '    End Sub
    Private Sub cmdGetAllEnrollData_BIO_ONE(mMachine_ID As Long, mEnrollNumber As String, ByRef paycodelist As List(Of String), vResult As Long)
        Dim sSql As String = ""
        Dim vEnrollNumber As Long = Convert.ToInt64(mEnrollNumber)
        Dim vStrEnrollNumber As String = mEnrollNumber ' ""
        Dim vEMachineNumber As Long
        Dim vFingerNumber As Long
        Dim vPrivilege As Long
        Dim vEnable As Long
        Dim vFlag As Boolean
        Dim vRet As Boolean
        Dim vErrorCode As Long
        Dim vStr As String
        ' Dim vRet As Boolean
        Dim recFP As New DataSet 'ADODB.Recordset
        Dim i As Long
        Dim vTitle As String
        Dim rsEmp As New DataSet 'ADODB.Recordset
        Dim RsFp As New DataSet 'ADODB.Recordset
        Dim sEcardnumber As String
        Dim mMachineNumber As Long
        Dim vVerifyMode As Long

        Dim nPhotoLen As Long
        Dim nFKRetCode As Long
        Dim vbytEnrollData() As Byte
        Dim vbytConvFpData() As Byte

        Dim vBackupNumber As Long

        Dim vEnrollName As String
        Dim vnEnableFlag As Long
        Dim vnMessRet As Long

        Dim vnResultCode As Long
        Dim vnii As Long
        Dim vnLen As Long
        'Dim vbytEnrollData() As Byte
        'Dim vbytConvFpData() As Byte
        Dim mnCommHandleIndex As Long


        'On Local Error Resume Next
        mMachineNumber = mMachine_ID
        'vRet = FK.EnableDevice(False)
        mnCommHandleIndex = vResult
        'sleep (3500)
        vnResultCode = FK_EnableDevice(mnCommHandleIndex, 0)
        If vnResultCode < 0 Then
            'MsgBox("Unable To connect " & mMachine_ID & "", vbOKOnly)
            XtraMessageBox.Show(ulf, "<size=10>Unable to connect Device Id " & mMachine_ID & "</size>", "<size=9>Information</size>")
            Exit Sub
        End If
        'Sleep (3500)
        vnResultCode = FK_ReadAllUserID(mnCommHandleIndex)
        If vnResultCode >= 1 Then
            '    lblMessage.Caption = "ReadAllUserID OK"
        Else
            'MsgBox("Unable to Read users from Device", vbOKOnly) 'FK_GetLastError vErrorCode
            XtraMessageBox.Show(ulf, "<size=10>Unable to Read users from Device</size>", "<size=9>Information</size>")
            'MsgBox "", vErrorCode
            vnResultCode = FK_EnableDevice(mnCommHandleIndex, 1)
            FK_DisConnect(mnCommHandleIndex)
            Exit Sub
        End If
        Dim UserCount As Integer = 0
        Dim vnValue As Integer
        Dim count As Integer = 0
        vnResultCode = FK_GetDeviceStatus(mnCommHandleIndex, GET_USERS, vnValue)
        If vnResultCode = RUN_SUCCESS Then
            UserCount = vnValue
        End If
        '---- Get Enroll data and save into database -------------
        Me.Cursor = Cursors.WaitCursor
        vFlag = False
        'With datEnroll
        'gGetState = True
        '.RecordSource = "select * from " & "tblEnroll"
        '.Refresh
        Dim isStringId As Boolean
        If FK_GetIsSupportStringID(mnCommHandleIndex) = RUN_SUCCESS Then
            isStringId = True
        Else
            isStringId = False
        End If
        'Do
        For k = 0 To 12
            vBackupNumber = k
            Dim EnrollNumber As String
            Try
                If isStringId Then
FFFS:
                    'vnResultCode = FK_GetAllUserID_StringID(mnCommHandleIndex, _
                    '                                     vStrEnrollNumber, _
                    '                                     vBackupNumber, _
                    '                                     vPrivilege, _
                    '                                     vnEnableFlag)
                    'If vnResultCode <> RUN_SUCCESS Then
                    '    If vnResultCode = RUNERR_DATAARRAY_END Then
                    '        vnResultCode = RUN_SUCCESS
                    '    End If
                    '    'Exit Do
                    'End If
EEES:
                    vnResultCode = FK_GetEnrollData_StringID(mnCommHandleIndex,
                                                          vStrEnrollNumber.Trim,
                                                          vBackupNumber,
                                                          vPrivilege,
                                                          mbytEnrollData,
                                                          mlngPasswordData)
                    vEnrollNumber = vStrEnrollNumber.Trim
                Else
FFF:
                    'vnResultCode = FK_GetAllUserID(mnCommHandleIndex, _
                    '                                     vEnrollNumber, _
                    '                                     vBackupNumber, _
                    '                                     vPrivilege, _
                    '                                     vnEnableFlag)
                    'If vnResultCode <> RUN_SUCCESS Then
                    '    If vnResultCode = RUNERR_DATAARRAY_END Then
                    '        vnResultCode = RUN_SUCCESS
                    '    End If
                    '    'Exit Do
                    'End If
EEE:
                    vnResultCode = FK_GetEnrollData(mnCommHandleIndex,
                                                          vEnrollNumber,
                                                          vBackupNumber,
                                                          vPrivilege,
                                                          mbytEnrollData,
                                                          mlngPasswordData)
                End If


                If vnResultCode <> RUN_SUCCESS Then
                    GoTo LLL
                End If
                XtraMasterTest.LabelControlStatus.Text = "Downloading Template " & vEnrollNumber & " From Device No. " & mMachine_ID
                XtraMasterTest.LabelControlCount.Text = "Downloading " & count + 1 & " of " & UserCount
                Application.DoEvents()

                Dim adap As SqlDataAdapter
                Dim adapA As OleDbDataAdapter
                rsEmp = New DataSet
                sSql = "select empname from tblemployee where presentcardno='" & vEnrollNumber.ToString("000000000000") & "'"
                'rsEmp = Cn.Execute(sSql)
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(rsEmp)
                Else
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(rsEmp)
                End If

                sSql = "select * from fptable where Enrollnumber='" & vEnrollNumber.ToString("000000000000") & "' and FingerNumber=" & vBackupNumber & " and DeviceType ='Bio' and EMachineNumber='" & mMachine_ID & "'"
                'RsFp = Cn.Execute(sSql)
                RsFp = New DataSet
                If Common.servername = "Access" Then
                    sSql = "select * from fptable where Enrollnumber='" & vEnrollNumber.ToString("000000000000") & "' and FingerNumber=" & vBackupNumber & " and DeviceType ='Bio' and EMachineNumber=" & mMachine_ID & ""
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(RsFp)
                Else
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(RsFp)
                End If


                If RsFp.Tables(0).Rows.Count > 0 Then
                    sSql = "DELETE FROM FPTABLE WHERE Enrollnumber='" & vEnrollNumber.ToString("000000000000") & "' and FingerNumber=" & vBackupNumber & " and DeviceType ='Bio' and EMachineNumber='" & mMachine_ID & "'"
                    'Cn.Execute(sSql)
                    If Common.servername = "Access" Then
                        sSql = "DELETE FROM FPTABLE WHERE Enrollnumber='" & vEnrollNumber.ToString("000000000000") & "' and FingerNumber=" & vBackupNumber & " and DeviceType ='Bio' and EMachineNumber=" & mMachine_ID & ""
                        If Common.con1.State <> ConnectionState.Open Then
                            Common.con1.Open()
                        End If
                        cmd1 = New OleDbCommand(sSql, Common.con1)
                        cmd1.ExecuteNonQuery()
                        If Common.con1.State <> ConnectionState.Closed Then
                            Common.con1.Close()
                        End If
                    Else
                        If Common.con.State <> ConnectionState.Open Then
                            Common.con.Open()
                        End If
                        cmd = New SqlCommand(sSql, Common.con)
                        cmd.ExecuteNonQuery()
                        If Common.con.State <> ConnectionState.Closed Then
                            Common.con.Close()
                        End If
                    End If
                End If
                'RsFp.Close()
                'RsFp = Nothing
                RsFp = New DataSet
                recFP = New DataSet

                'recFP.Open("select * from fptable", Cn, adOpenKeyset, adLockOptimistic)
                'recFP.AddNew()
                Dim s As String = "select * from fptable"
                Dim Emachinenumber As String
                'Dim EnrollNumber As String
                Dim UserName As String
                'Dim Template_Tw As String
                Dim Template_Tw() As Byte
                Dim FingerNumber As String
                Dim Privilege As String
                Dim Verifymode As String
                Dim Template_Face As String


                'recFP.Fields("Emachinenumber") = CLng(cboMachineNo.Text)
                Emachinenumber = mMachine_ID
                'recFP.Fields("EnrollNumber") = Format(CStr(vEnrollNumber), "00000000")
                'EnrollNumber = Format(CStr(vEnrollNumber), "00000000")
                EnrollNumber = vEnrollNumber.ToString("000000000000")
                If rsEmp.Tables(0).Rows.Count > 0 Then
                    'recFP.Fields("UserName") = IIf(IsNull(rsEmp(0)), " ", rsEmp(0))
                    UserName = rsEmp.Tables(0).Rows(0).Item("empname").ToString.Trim
                Else
                    'recFP.Fields("UserName") = ""
                    UserName = ""
                End If

                If vBackupNumber = BACKUP_PSW Then
                    ReDim vbytEnrollData(PWD_DATA_SIZE - 1)
                    ReDim Template_Tw(PWD_DATA_SIZE - 1)
                    'CopyMemory(vbytEnrollData(0), mbytEnrollData(0), PWD_DATA_SIZE)
                    Array.Copy(mbytEnrollData, vbytEnrollData, PWD_DATA_SIZE)
                    'recFP.Fields("Template_Tw") = vbytEnrollData
                    Array.Copy(vbytEnrollData, Template_Tw, PWD_DATA_SIZE)
                    'Template_Tw = Encoding.UTF8.GetString(vbytEnrollData, 0, vbytEnrollData.Length)
                    Dim pwd As String = Encoding.ASCII.GetString(Template_Tw)
                    Dim len As Integer = pwd.Replace(vbNullChar, "").Trim.Length
                    If len <= 24 Then
                        GoTo LLL
                    End If
                ElseIf vBackupNumber = BACKUP_PSW Or vBackupNumber = BACKUP_CARD Then
                    ReDim vbytEnrollData(PWD_DATA_SIZE - 1)
                    ReDim Template_Tw(PWD_DATA_SIZE - 1)
                    Array.Copy(mbytEnrollData, vbytEnrollData, PWD_DATA_SIZE)
                    Array.Copy(vbytEnrollData, Template_Tw, PWD_DATA_SIZE)
                    'Template_Tw = Encoding.UTF8.GetString(vbytEnrollData, 0, vbytEnrollData.Length)
                    'Template_Tw = UnicodeBytesToString(vbytEnrollData)
                    Dim card As String = Encoding.ASCII.GetString(Template_Tw)
                    Dim len As Integer = card.Replace(vbNullChar, "").Trim.Length
                    If len <= 17 Then
                        GoTo LLL
                    End If
                ElseIf vBackupNumber >= BACKUP_FP_0 And vBackupNumber <= BACKUP_FP_9 Then
                    ReDim vbytEnrollData(FP_DATA_SIZE - 1)
                    ReDim vbytConvFpData(FP_DATA_SIZE - 1)
                    ReDim Template_Tw(FP_DATA_SIZE - 1)
                    Array.Copy(mbytEnrollData, vbytEnrollData, FP_DATA_SIZE)
                    ConvFpDataToSaveInDbForCompatibility(vbytEnrollData, vbytConvFpData)
                    'Template_Tw = Encoding.UTF8.GetString(vbytConvFpData, 0, vbytConvFpData.Length)
                    Array.Copy(vbytConvFpData, Template_Tw, FP_DATA_SIZE)
                ElseIf vBackupNumber = BACKUP_FACE Then
                    ReDim vbytEnrollData(FACE_DATA_SIZE - 1)
                    ReDim Template_Tw(FACE_DATA_SIZE - 1)
                    Array.Copy(mbytEnrollData, vbytEnrollData, FACE_DATA_SIZE)
                    Array.Copy(vbytEnrollData, Template_Tw, FACE_DATA_SIZE)
                    'Template_Tw = Encoding.UTF8.GetString(vbytEnrollData, 0, vbytEnrollData.Length)
                ElseIf vBackupNumber = BACKUP_VEIN_0 Then
                    ReDim vbytEnrollData(VEIN_DATA_SIZE - 1)
                    ReDim Template_Tw(VEIN_DATA_SIZE - 1)
                    Array.Copy(mbytEnrollData, vbytEnrollData, VEIN_DATA_SIZE)
                    Array.Copy(vbytEnrollData, Template_Tw, VEIN_DATA_SIZE)
                    'Template_Tw = Encoding.UTF8.GetString(vbytEnrollData, 0, vbytEnrollData.Length)
                End If

                'recFP.Fields("FingerNumber") = vBackupNumber
                'recFP.Fields("Privilege") = vPrivilege
                'recFP.Fields("Verifymode") = vVerifyMode

                FingerNumber = vBackupNumber
                Privilege = vPrivilege
                Verifymode = vVerifyMode

                'By yogesh for bio 7

                If vBackupNumber = 0 Then
                    ReDim mbytPhotoImage(4)
                    nPhotoLen = 0
                    nFKRetCode = FK_GetEnrollPhoto(mnCommHandleIndex,
                                            vEnrollNumber,
                                            mbytPhotoImage,
                                            nPhotoLen)
                    If nFKRetCode <> RUN_SUCCESS Then
                        'lblMessage.Caption = ReturnResultPrint(nFKRetCode)
                        FK_EnableDevice(mnCommHandleIndex, 1)
                    Else
                        ReDim mbytPhotoImage(nPhotoLen - 1)
                        nFKRetCode = FK_GetEnrollPhoto(mnCommHandleIndex,
                                            vEnrollNumber,
                                            mbytPhotoImage,
                                            nPhotoLen)
                        nPhotoLen = UBound(mbytPhotoImage) + 1
                        If nPhotoLen > 1 Then
                            'recFP.Fields("Template_Face") = mbytPhotoImage
                            Template_Face = mbytPhotoImage(0)
                        End If
                    End If
                End If
                'end
                'recFP.Update()
                'recFP.Close()
                recFP = Nothing

                Dim sqlinsert As String = "insert into fptable (Emachinenumber, EnrollNumber, UserName, Template_Tw, FingerNumber, Privilege, Verifymode, Template_Face, DeviceType) values (" & Emachinenumber & ",'" & EnrollNumber & "', '" & UserName & "', @Template_Tw, '" & FingerNumber & "', '" & Privilege & "', '" & Verifymode & "', '" & Template_Face & "','Bio')"

                If Common.servername = "Access" Then
                    If Common.con1.State <> ConnectionState.Open Then
                        Common.con1.Open()
                    End If
                    cmd1 = New OleDbCommand(sqlinsert, Common.con1)
                    'Using picture As Image = Image.FromFile("./fb")
                    Using stream As New IO.MemoryStream
                        'picture.Save(stream, Imaging.ImageFormat.Jpeg)
                        cmd1.Parameters.Add("@Template_Tw", OleDb.OleDbType.VarBinary).Value = Template_Tw 'stream.GetBuffer()
                        cmd1.ExecuteNonQuery()
                    End Using
                    If Common.con1.State <> ConnectionState.Closed Then
                        Common.con1.Close()
                    End If
                    'End Using
                Else
                    If Common.con.State <> ConnectionState.Open Then
                        Common.con.Open()
                    End If
                    cmd = New SqlCommand(sqlinsert, Common.con)
                    Using stream As New IO.MemoryStream
                        cmd.Parameters.Add("@Template_Tw", SqlDbType.VarBinary).Value = Template_Tw 'vbytEnrollData
                        cmd.ExecuteNonQuery()
                    End Using
                    If Common.con.State <> ConnectionState.Closed Then
                        Common.con.Close()
                    End If
                End If
LLL:
            Catch ex As Exception
                'MsgBox(ex.Message)
            End Try
            If IsNumeric(EnrollNumber) Then
                EnrollNumber = Convert.ToDouble(EnrollNumber)
            End If
            paycodelist.Add(EnrollNumber & ";" & "")
            count = count + 1
            'Loop
        Next k
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub cmdGetAllEnrollData_TF01_ONE(mMachine_ID As Long, mEnrollNumber As String, ByRef paycodelist As List(Of String), vResult As Long)
        Dim sSql As String = ""
        ' Dim vEnrollNumber As Long = Convert.ToInt64(mEnrollNumber)
        Dim vStrEnrollNumber As String = mEnrollNumber ' ""
        Dim vPrivilege As Long
        Dim vFlag As Boolean
        Dim recFP As New DataSet 'ADODB.Recordset
        Dim rsEmp As New DataSet 'ADODB.Recordset
        Dim RsFp As New DataSet 'ADODB.Recordset
        Dim mMachineNumber As Long
        Dim vVerifyMode As Long
        Dim nPhotoLen As Long
        Dim nFKRetCode As Long
        Dim vbytEnrollData() As Byte
        Dim vbytConvFpData() As Byte
        Dim vBackupNumber As Long
        Dim vnResultCode As Long
        Dim mnCommHandleIndex As Long

        'For TF-01
        Dim dwEnrollNumber As Integer = 0
        Dim dwEnMachineID As Integer = 0
        Dim dwBackupNum As Integer = 0
        Dim dwPrivilegeNum As Integer = 0
        Dim dwEnable As Integer = 0
        Dim dwPassWord As Integer = 0
        Dim bBreakFail As Boolean = False
        Dim bRet As Boolean
        mMachineNumber = mMachine_ID
        bRet = DisableDevice(mMachine_ID)
        If Not bRet Then
            XtraMessageBox.Show(ulf, "<size=10>Unable to connect Device Id " & mMachine_ID & "</size>", "<size=9>Information</size>")
            Exit Sub
        End If
        bRet = AxFP_CLOCK1.ReadAllUserID(mMachine_ID)
        If Not bRet Then
            XtraMessageBox.Show(ulf, "<size=10>Unable to Read users from Device</size>", "<size=9>Information</size>")
            AxFP_CLOCK1.EnableDevice(mMachine_ID, 1)
            AxFP_CLOCK1.CloseCommPort()
            Exit Sub
        End If


        Dim UserCount As Integer = 0
        Dim vnValue As Integer
        Dim count As Integer = 0
        bRet = AxFP_CLOCK1.GetDeviceStatus(mMachine_ID, 2, vnValue)
        If bRet Then
            UserCount = vnValue
        End If
        '---- Get Enroll data and save into database -------------
        Me.Cursor = Cursors.WaitCursor

        Do

            Dim dwData((1420 \ 4) - 1) As Integer
            'object obj = new System.Runtime.InteropServices.VariantWrapper(dwData);
            Dim FacedwData((1888 \ 4) - 1) As Integer
            Dim obj As Object = New System.Runtime.InteropServices.VariantWrapper(FacedwData)

            bRet = AxFP_CLOCK1.GetAllUserID(mMachine_ID, dwEnrollNumber, dwEnMachineID, dwBackupNum, dwPrivilegeNum, dwEnable)

            'read finished
            If bRet = False Then
                'EnableDevice();
                bBreakFail = True
                'labelInfo.Text = "fail on GetAllUserID";
                Exit Do
            End If

            bRet = AxFP_CLOCK1.GetEnrollData(mMachine_ID, dwEnrollNumber, dwEnMachineID, dwBackupNum, dwPrivilegeNum, obj, dwPassWord)
            If Not bRet Then
                Exit Sub
            End If

            dwData = DirectCast(obj, Integer())
            Dim _indexData(1419) As Byte
            '分配内存
            Dim _ptrIndex As IntPtr = Marshal.AllocHGlobal(_indexData.Length)
            'int[]  转成 byte[]
            Marshal.Copy(dwData, 0, _ptrIndex, 1420 \ 4) 'be careful
            Marshal.Copy(_ptrIndex, _indexData, 0, 1420)
            Marshal.FreeHGlobal(_ptrIndex)

            FacedwData = DirectCast(obj, Integer())
            Dim _indexDataFace(1887) As Byte
            '分配内存
            Dim _ptrIndexFace As IntPtr = Marshal.AllocHGlobal(_indexDataFace.Length)
            'int[]  转成 byte[]
            Marshal.Copy(FacedwData, 0, _ptrIndexFace, 1888 \ 4) 'be careful
            Marshal.Copy(_ptrIndexFace, _indexDataFace, 0, 1888)
            Marshal.FreeHGlobal(_ptrIndexFace)
            XtraMasterTest.LabelControlStatus.Text = "Downloading Template " & dwEnrollNumber & " From Device No. " & mMachine_ID
            XtraMasterTest.LabelControlCount.Text = "Downloading " & count + 1 & " of " & UserCount
            Application.DoEvents()
            Dim UserName As String = ""
            Dim adap As SqlDataAdapter
            Dim adapA As OleDbDataAdapter
            rsEmp = New DataSet
            sSql = "select empname from tblemployee where presentcardno='" & dwEnrollNumber.ToString("000000000000") & "'"
            'rsEmp = Cn.Execute(sSql)
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(rsEmp)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(rsEmp)
            End If
            If rsEmp.Tables(0).Rows.Count > 0 Then
                UserName = rsEmp.Tables(0).Rows(0)(0).ToString.Trim
            End If


            sSql = "select * from fptable where Enrollnumber='" & dwEnrollNumber.ToString("000000000000") & "' and FingerNumber=" & dwBackupNum & " and DeviceType ='TF-01' and EMachineNumber='" & mMachine_ID & "'"
            'RsFp = Cn.Execute(sSql)
            RsFp = New DataSet
            If Common.servername = "Access" Then
                sSql = "select * from fptable where Enrollnumber='" & dwEnrollNumber.ToString("000000000000") & "' and FingerNumber=" & dwBackupNum & " and DeviceType ='TF-01' and EMachineNumber=" & mMachine_ID & ""
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(RsFp)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(RsFp)
            End If

            If RsFp.Tables(0).Rows.Count > 0 Then
                sSql = "DELETE FROM FPTABLE WHERE Enrollnumber='" & dwEnrollNumber.ToString("000000000000") & "' and FingerNumber=" & dwBackupNum & " and DeviceType ='TF-01' and EMachineNumber='" & mMachine_ID & "'"
                'Cn.Execute(sSql)
                If Common.servername = "Access" Then
                    sSql = "DELETE FROM FPTABLE WHERE Enrollnumber='" & dwEnrollNumber.ToString("000000000000") & "' and FingerNumber=" & dwBackupNum & " and DeviceType ='TF-01' and EMachineNumber=" & mMachine_ID & ""
                    If Common.con1.State <> ConnectionState.Open Then
                        Common.con1.Open()
                    End If
                    cmd1 = New OleDbCommand(sSql, Common.con1)
                    cmd1.ExecuteNonQuery()
                    If Common.con1.State <> ConnectionState.Closed Then
                        Common.con1.Close()
                    End If
                Else
                    If Common.con.State <> ConnectionState.Open Then
                        Common.con.Open()
                    End If
                    cmd = New SqlCommand(sSql, Common.con)
                    cmd.ExecuteNonQuery()
                    If Common.con.State <> ConnectionState.Closed Then
                        Common.con.Close()
                    End If
                End If
            End If

            RsFp = New DataSet
            recFP = New DataSet

            Dim sql As String

            sql = " insert into fptable(Emachinenumber, EnrollNumber, UserName, Template_Tw, FingerNumber, Privilege,DeviceType,Cardnumber) 
                   values('" & dwEnMachineID & "','" & dwEnrollNumber.ToString("000000000000") & "','" & UserName & "',@Template_Tw,'" & dwBackupNum & "','" & dwPrivilegeNum & "','TF-01','" & dwPassWord & "')"

            If Common.servername = "Access" Then
                If Common.con1.State <> ConnectionState.Open Then
                    Common.con1.Open()
                End If
                cmd1 = New OleDbCommand(sql, Common.con1)
                Using stream As New IO.MemoryStream
                    cmd1.Parameters.Add("@Template_Tw", OleDbType.VarBinary).Value = _indexDataFace  'vbytEnrollData
                    cmd1.ExecuteNonQuery()
                End Using
                If Common.con1.State <> ConnectionState.Closed Then
                    Common.con1.Close()
                End If
            Else

                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                cmd = New SqlCommand(sql, Common.con)
                Using stream As New IO.MemoryStream
                        cmd.Parameters.Add("@Template_Tw", SqlDbType.VarBinary).Value = _indexDataFace  'vbytEnrollData
                        cmd.ExecuteNonQuery()
                    End Using
                    If Common.con.State <> ConnectionState.Closed Then
                        Common.con.Close()
                    End If

                End If

            dwPassWord = 0

        Loop While bRet


        Me.Cursor = Cursors.Default
    End Sub

    Private Sub cmdGetAllEnrollData_F9_all(mMachine_ID As Long, vResult As Long, ByRef paycodelist As List(Of String), F9 As mdlPublic_f9)
        Dim sSql As String = ""
        Dim vEnrollNumber As Long
        Dim vStrEnrollNumber As String = ""
        Dim vEMachineNumber As Long
        Dim vFingerNumber As Long
        Dim vPrivilege As Long
        Dim vEnable As Long
        Dim vFlag As Boolean
        Dim vRet As Boolean
        Dim vErrorCode As Long
        Dim vStr As String
        ' Dim vRet As Boolean
        Dim recFP As New DataSet 'ADODB.Recordset
        Dim i As Long
        Dim vTitle As String
        Dim rsEmp As New DataSet 'ADODB.Recordset
        Dim RsFp As New DataSet 'ADODB.Recordset
        Dim sEcardnumber As String
        Dim mMachineNumber As Long
        Dim vVerifyMode As Long

        Dim nPhotoLen As Long
        Dim nFKRetCode As Long
        Dim vbytEnrollData() As Byte
        Dim vbytConvFpData() As Byte

        Dim vBackupNumber As Long

        Dim vEnrollName As String
        Dim vnEnableFlag As Long
        Dim vnMessRet As Long

        Dim vnResultCode As Long
        Dim vnii As Long
        Dim vnLen As Long
        'Dim vbytEnrollData() As Byte
        'Dim vbytConvFpData() As Byte
        Dim mnCommHandleIndex As Long


        'On Local Error Resume Next
        mMachineNumber = mMachine_ID
        'vRet = FK.EnableDevice(False)
        mnCommHandleIndex = vResult
        'sleep (3500)
        vnResultCode = F9.FK_EnableDevice(mnCommHandleIndex, 0)
        If vnResultCode < 0 Then
            'MsgBox("Unable To connect " & mMachine_ID & "", vbOKOnly)
            XtraMessageBox.Show(ulf, "<size=10>Unable to connect Device Id " & mMachine_ID & "</size>", "<size=9>Information</size>")
            Exit Sub
        End If
        'Sleep (3500)
        vnResultCode = F9.FK_ReadAllUserID(mnCommHandleIndex)
        If vnResultCode >= 1 Then
            '    lblMessage.Caption = "ReadAllUserID OK"
        Else
            'MsgBox("Unable to Read users from Device", vbOKOnly) 'FK_GetLastError vErrorCode
            XtraMessageBox.Show(ulf, "<size=10>Unable to Read users from Device</size>", "<size=9>Information</size>")
            'MsgBox "", vErrorCode
            vnResultCode = F9.FK_EnableDevice(mnCommHandleIndex, 1)
            F9.FK_DisConnect(mnCommHandleIndex)
            Exit Sub
        End If
        Dim UserCount As Integer = 0
        Dim vnValue As Integer
        Dim count As Integer = 0
        vnResultCode = F9.FK_GetDeviceStatus(mnCommHandleIndex, GET_USERS, vnValue)
        If vnResultCode = RUN_SUCCESS Then
            UserCount = vnValue
        End If
        '---- Get Enroll data and save into database -------------
        Me.Cursor = Cursors.WaitCursor
        vFlag = False
        'With datEnroll
        'gGetState = True
        '.RecordSource = "select * from " & "tblEnroll"
        '.Refresh
        Dim isStringId As Boolean
        If F9.FK_GetIsSupportStringID(mnCommHandleIndex) = RUN_SUCCESS Then
            isStringId = True
        Else
            isStringId = False
        End If
        Do
            If isStringId Then
FFFS:
                vnResultCode = F9.FK_GetAllUserID_StringID(mnCommHandleIndex, _
                                                     vStrEnrollNumber, _
                                                     vBackupNumber, _
                                                     vPrivilege, _
                                                     vnEnableFlag)
                If vnResultCode <> RUN_SUCCESS Then
                    If vnResultCode = RUNERR_DATAARRAY_END Then
                        vnResultCode = RUN_SUCCESS
                    End If
                    Exit Do
                End If
EEES:
                vnResultCode = F9.FK_GetEnrollData_StringID(mnCommHandleIndex, _
                                                      vStrEnrollNumber.Trim, _
                                                      vBackupNumber, _
                                                      vPrivilege, _
                                                      mbytEnrollData, _
                                                      mlngPasswordData)
                vEnrollNumber = vStrEnrollNumber.Trim
            Else
FFF:
                vnResultCode = F9.FK_GetAllUserID(mnCommHandleIndex, _
                                                     vEnrollNumber, _
                                                     vBackupNumber, _
                                                     vPrivilege, _
                                                     vnEnableFlag)
                If vnResultCode <> RUN_SUCCESS Then
                    If vnResultCode = RUNERR_DATAARRAY_END Then
                        vnResultCode = RUN_SUCCESS
                    End If
                    Exit Do
                End If
EEE:
                vnResultCode = F9.FK_GetEnrollData(mnCommHandleIndex, _
                                                      vEnrollNumber, _
                                                      vBackupNumber, _
                                                      vPrivilege, _
                                                      mbytEnrollData, _
                                                      mlngPasswordData)
            End If


            If vnResultCode <> RUN_SUCCESS Then
                GoTo LLL
            End If
            XtraMasterTest.LabelControlStatus.Text = "Downloading Template " & vEnrollNumber & " From Device No. " & mMachine_ID
            XtraMasterTest.LabelControlCount.Text = "Downloading " & count + 1 & " of " & UserCount
            Application.DoEvents()

            Dim adap As SqlDataAdapter
            Dim adapA As OleDbDataAdapter
            rsEmp = New DataSet
            sSql = "select empname from tblemployee where presentcardno='" & vEnrollNumber.ToString("000000000000") & "'"
            'rsEmp = Cn.Execute(sSql)
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(rsEmp)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(rsEmp)
            End If

            sSql = "select * from fptable where Enrollnumber='" & vEnrollNumber.ToString("000000000000") & "' and FingerNumber=" & vBackupNumber & " and DeviceType ='F9' and EMachineNumber='" & mMachine_ID & "'"
            'RsFp = Cn.Execute(sSql)
            RsFp = New DataSet
            If Common.servername = "Access" Then
                sSql = "select * from fptable where Enrollnumber='" & vEnrollNumber.ToString("000000000000") & "' and FingerNumber=" & vBackupNumber & " and DeviceType ='F9' and EMachineNumber=" & mMachine_ID & ""
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(RsFp)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(RsFp)
            End If


            If RsFp.Tables(0).Rows.Count > 0 Then
                sSql = "DELETE FROM FPTABLE WHERE Enrollnumber='" & vEnrollNumber.ToString("000000000000") & "' and FingerNumber=" & vBackupNumber & " and DeviceType ='F9' and EMachineNumber='" & mMachine_ID & "'"
                'Cn.Execute(sSql)
                If Common.servername = "Access" Then
                    sSql = "DELETE FROM FPTABLE WHERE Enrollnumber='" & vEnrollNumber.ToString("000000000000") & "' and FingerNumber=" & vBackupNumber & " and DeviceType ='F9' and EMachineNumber=" & mMachine_ID & ""
                    If Common.con1.State <> ConnectionState.Open Then
                        Common.con1.Open()
                    End If
                    cmd1 = New OleDbCommand(sSql, Common.con1)
                    cmd1.ExecuteNonQuery()
                    If Common.con1.State <> ConnectionState.Closed Then
                        Common.con1.Close()
                    End If
                Else
                    If Common.con.State <> ConnectionState.Open Then
                        Common.con.Open()
                    End If
                    cmd = New SqlCommand(sSql, Common.con)
                    cmd.ExecuteNonQuery()
                    If Common.con.State <> ConnectionState.Closed Then
                        Common.con.Close()
                    End If
                End If
            End If
            'RsFp.Close()
            'RsFp = Nothing
            RsFp = New DataSet
            recFP = New DataSet

            'recFP.Open("select * from fptable", Cn, adOpenKeyset, adLockOptimistic)
            'recFP.AddNew()
            Dim s As String = "select * from fptable"
            Dim Emachinenumber As String
            Dim EnrollNumber As String
            Dim UserName As String
            'Dim Template_Tw As String
            Dim Template_Tw() As Byte
            Dim FingerNumber As String
            Dim Privilege As String
            Dim Verifymode As String
            Dim Template_Face As String


            'recFP.Fields("Emachinenumber") = CLng(cboMachineNo.Text)
            Emachinenumber = mMachine_ID
            'recFP.Fields("EnrollNumber") = Format(CStr(vEnrollNumber), "00000000")
            'EnrollNumber = Format(CStr(vEnrollNumber), "00000000")
            EnrollNumber = vEnrollNumber.ToString("000000000000")
            If rsEmp.Tables(0).Rows.Count > 0 Then
                'recFP.Fields("UserName") = IIf(IsNull(rsEmp(0)), " ", rsEmp(0))
                UserName = rsEmp.Tables(0).Rows(0).Item("empname").ToString.Trim
            Else
                'recFP.Fields("UserName") = ""
                UserName = ""
            End If

            If vBackupNumber = BACKUP_PSW Then
                ReDim vbytEnrollData(PWD_DATA_SIZE - 1)
                'CopyMemory(vbytEnrollData(0), mbytEnrollData(0), PWD_DATA_SIZE)
                Array.Copy(mbytEnrollData, vbytEnrollData, PWD_DATA_SIZE)
                'recFP.Fields("Template_Tw") = vbytEnrollData
                Array.Copy(vbytEnrollData, Template_Tw, PWD_DATA_SIZE)
                'Template_Tw = Encoding.UTF8.GetString(vbytEnrollData, 0, vbytEnrollData.Length)
            ElseIf vBackupNumber = BACKUP_PSW Or vBackupNumber = BACKUP_CARD Then
                ReDim vbytEnrollData(PWD_DATA_SIZE - 1)
                ReDim Template_Tw(PWD_DATA_SIZE - 1)
                Array.Copy(mbytEnrollData, vbytEnrollData, PWD_DATA_SIZE)
                Array.Copy(vbytEnrollData, Template_Tw, PWD_DATA_SIZE)
                'Template_Tw = Encoding.UTF8.GetString(vbytEnrollData, 0, vbytEnrollData.Length)
                'Template_Tw = UnicodeBytesToString(vbytEnrollData)
            ElseIf vBackupNumber >= BACKUP_FP_0 And vBackupNumber <= BACKUP_FP_9 Then
                ReDim vbytEnrollData(FP_DATA_SIZE - 1)
                ReDim vbytConvFpData(FP_DATA_SIZE - 1)
                ReDim Template_Tw(FP_DATA_SIZE - 1)
                Array.Copy(mbytEnrollData, vbytEnrollData, FP_DATA_SIZE)
                ConvFpDataToSaveInDbForCompatibility(vbytEnrollData, vbytConvFpData)
                'Template_Tw = Encoding.UTF8.GetString(vbytConvFpData, 0, vbytConvFpData.Length)
                Array.Copy(vbytConvFpData, Template_Tw, FP_DATA_SIZE)
            ElseIf vBackupNumber = BACKUP_FACE Then
                ReDim vbytEnrollData(FACE_DATA_SIZE - 1)
                ReDim Template_Tw(FACE_DATA_SIZE - 1)
                Array.Copy(mbytEnrollData, vbytEnrollData, FACE_DATA_SIZE)
                Array.Copy(vbytEnrollData, Template_Tw, FACE_DATA_SIZE)
                'Template_Tw = Encoding.UTF8.GetString(vbytEnrollData, 0, vbytEnrollData.Length)
            ElseIf vBackupNumber = BACKUP_VEIN_0 Then
                ReDim vbytEnrollData(VEIN_DATA_SIZE - 1)
                ReDim Template_Tw(VEIN_DATA_SIZE - 1)
                Array.Copy(mbytEnrollData, vbytEnrollData, VEIN_DATA_SIZE)
                Array.Copy(vbytEnrollData, Template_Tw, VEIN_DATA_SIZE)
                'Template_Tw = Encoding.UTF8.GetString(vbytEnrollData, 0, vbytEnrollData.Length)
            End If

            'recFP.Fields("FingerNumber") = vBackupNumber
            'recFP.Fields("Privilege") = vPrivilege
            'recFP.Fields("Verifymode") = vVerifyMode

            FingerNumber = vBackupNumber
            Privilege = vPrivilege
            Verifymode = vVerifyMode

            'By yogesh for bio 7

            If vBackupNumber = 0 Then
                ReDim mbytPhotoImage(4)
                nPhotoLen = 0
                nFKRetCode = F9.FK_GetEnrollPhoto(mnCommHandleIndex, _
                                        vEnrollNumber, _
                                        mbytPhotoImage, _
                                        nPhotoLen)
                If nFKRetCode <> RUN_SUCCESS Then
                    'lblMessage.Caption = ReturnResultPrint(nFKRetCode)
                    F9.FK_EnableDevice(mnCommHandleIndex, 1)
                Else
                    ReDim mbytPhotoImage(nPhotoLen - 1)
                    nFKRetCode = F9.FK_GetEnrollPhoto(mnCommHandleIndex, _
                                        vEnrollNumber, _
                                        mbytPhotoImage, _
                                        nPhotoLen)
                    nPhotoLen = UBound(mbytPhotoImage) + 1
                    If nPhotoLen > 1 Then
                        'recFP.Fields("Template_Face") = mbytPhotoImage
                        Template_Face = mbytPhotoImage(0)
                    End If
                End If
            End If
            'end
            'recFP.Update()
            'recFP.Close()
            recFP = Nothing

            Dim sqlinsert As String = "insert into fptable (Emachinenumber, EnrollNumber, UserName, Template_Tw, FingerNumber, Privilege, Verifymode, Template_Face, DeviceType) values (" & Emachinenumber & ",'" & EnrollNumber & "', '" & UserName & "', @Template_Tw, '" & FingerNumber & "', '" & Privilege & "', '" & Verifymode & "', '" & Template_Face & "','F9')"

            If Common.servername = "Access" Then
                If Common.con1.State <> ConnectionState.Open Then
                    Common.con1.Open()
                End If
                cmd1 = New OleDbCommand(sqlinsert, Common.con1)
                'Using picture As Image = Image.FromFile("./fb")
                Using stream As New IO.MemoryStream
                    'picture.Save(stream, Imaging.ImageFormat.Jpeg)
                    cmd1.Parameters.Add("@Template_Tw", OleDb.OleDbType.VarBinary).Value = Template_Tw 'stream.GetBuffer()
                    cmd1.ExecuteNonQuery()
                End Using
                If Common.con1.State <> ConnectionState.Closed Then
                    Common.con1.Close()
                End If
                'End Using
            Else
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                cmd = New SqlCommand(sqlinsert, Common.con)
                Using stream As New IO.MemoryStream
                    cmd.Parameters.Add("@Template_Tw", SqlDbType.VarBinary).Value = Template_Tw 'vbytEnrollData
                    cmd.ExecuteNonQuery()
                End Using
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
LLL:
            If IsNumeric(EnrollNumber) Then
                EnrollNumber = Convert.ToDouble(EnrollNumber)
            End If
            paycodelist.Add(EnrollNumber & ";" & "")
            count = count + 1
        Loop
        'gGetState = False
        vRet = F9.FK_EnableDevice(mnCommHandleIndex, 1)
        F9.FK_DisConnect(mnCommHandleIndex)

        Me.Cursor = Cursors.Default
    End Sub
    Private Sub cmdGetAllEnrollData_F9_ONE(mMachine_ID As Long, mEnrollNumber As String, ByRef paycodelist As List(Of String), F9 As mdlPublic_f9, mnCommHandleIndex As Long)
        'Dim paycodelist As New List(Of String)()
        'Dim paycodeArray '= paycodelist.Distinct.ToArray ' paycodelist.ToArray

        Dim sSql As String = ""
        Dim vEnrollNumber As Long
        Dim vStrEnrollNumber As String
        Dim vEMachineNumber As Long
        Dim vFingerNumber As Long
        Dim vPrivilege As Long
        Dim vEnable As Long
        Dim vFlag As Boolean
        Dim vRet As Boolean
        Dim vErrorCode As Long
        Dim vStr As String
        ' Dim vRet As Boolean
        Dim recFP As New DataSet 'ADODB.Recordset
        Dim i As Long
        Dim vTitle As String
        Dim rsEmp As New DataSet 'ADODB.Recordset
        Dim RsFp As New DataSet 'ADODB.Recordset
        Dim sEcardnumber As String
        Dim mMachineNumber As Long
        Dim vVerifyMode As Long

        Dim vBackupNumber As Long

        Dim vEnrollName As String
        Dim vnEnableFlag As Long
        Dim vnMessRet As Long

        '    Dim vnii As Long
        '   Dim vnLen As Long
        Dim vbytEnrollData() As Byte
        Dim vbytConvFpData() As Byte

        Dim vnResultCode As Long
        Dim vnii As Long
        Dim vnLen As Long

        vEnrollNumber = mEnrollNumber
        vStrEnrollNumber = mEnrollNumber
        mMachineNumber = mMachine_ID
        vRet = F9.FK_EnableDevice(mnCommHandleIndex, 0)
        If vRet = False Then
            XtraMessageBox.Show(ulf, "<size=10>Unable to connect Device Id " & mMachine_ID & "</size>", "<size=9>Information</size>")
            'MsgBox("Unable to connect Device Id " & mMachine_ID & "", vbOKOnly)
            Exit Sub
        End If

        Dim k As Integer

        '---- Get Enroll data and save into database -------------
        Me.Cursor = Cursors.WaitCursor
        vFlag = False

        Dim isStringId As Boolean
        If F9.FK_GetIsSupportStringID(mnCommHandleIndex) = RUN_SUCCESS Then
            isStringId = True
        Else
            isStringId = False
        End If

        For k = 0 To 11
            vBackupNumber = k
            If isStringId Then
                vnResultCode = F9.FK_GetEnrollData_StringID(mnCommHandleIndex, _
                                         vStrEnrollNumber, _
                                         vBackupNumber, _
                                         vPrivilege, _
                                         mbytEnrollData, _
                                         mlngPasswordData)
                vEnrollNumber = vStrEnrollNumber.Trim

            Else
                vnResultCode = F9.FK_GetEnrollData(mnCommHandleIndex, _
                                         vEnrollNumber, _
                                         vBackupNumber, _
                                         vPrivilege, _
                                         mbytEnrollData, _
                                         mlngPasswordData)

            End If


            If vnResultCode <> RUN_SUCCESS Then
                GoTo FFF
            End If

            XtraMasterTest.LabelControlStatus.Text = "Downloading Template " & vEnrollNumber & " From Device No. " & mMachine_ID
            Application.DoEvents()

            Dim adap As SqlDataAdapter
            Dim adapA As OleDbDataAdapter
            rsEmp = New DataSet

            sSql = "select empname from tblemployee where presentcardno='" & vEnrollNumber.ToString("000000000000") & "'"
            'rsEmp = Cn.Execute(sSql)
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(rsEmp)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(rsEmp)
            End If

            sSql = "select * from fptable where Enrollnumber='" & vEnrollNumber.ToString("000000000000") & "' and FingerNumber=" & vBackupNumber & " and DeviceType ='F9' and EMachineNumber='" & mMachine_ID & "'"
            'RsFp = Cn.Execute(sSql)
            RsFp = New DataSet
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(RsFp)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(RsFp)
            End If

            If RsFp.Tables(0).Rows.Count > 0 Then
                sSql = "DELETE FROM FPTABLE WHERE Enrollnumber='" & vEnrollNumber.ToString("000000000000") & "' and FingerNumber=" & vBackupNumber & " and DeviceType ='F9' and EMachineNumber='" & mMachine_ID & "'"
                'Cn.Execute(sSql)
                If Common.servername = "Access" Then
                    If Common.con1.State <> ConnectionState.Open Then
                        Common.con1.Open()
                    End If
                    cmd1 = New OleDbCommand(sSql, Common.con1)
                    cmd1.ExecuteNonQuery()
                    If Common.con1.State <> ConnectionState.Closed Then
                        Common.con1.Close()
                    End If
                Else
                    If Common.con.State <> ConnectionState.Open Then
                        Common.con.Open()
                    End If
                    cmd = New SqlCommand(sSql, Common.con)
                    cmd.ExecuteNonQuery()
                    If Common.con.State <> ConnectionState.Closed Then
                        Common.con.Close()
                    End If
                End If

            End If
            RsFp = New DataSet
            recFP = New DataSet
            Dim s As String = "select * from fptable"
            Dim Emachinenumber As String
            Dim EnrollNumber As String
            Dim UserName As String
            Dim Template_Tw As String
            Dim FingerNumber As String
            Dim Privilege As String
            Dim Verifymode As String
            Dim Template_Face As String


            'recFP.Fields("Emachinenumber") = CLng(cboMachineNo.Text)
            Emachinenumber = mMachine_ID
            'recFP.Fields("EnrollNumber") = Format(CStr(vEnrollNumber), "00000000")
            EnrollNumber = Format(CStr(vEnrollNumber), "000000000000")
            EnrollNumber = vEnrollNumber.ToString("000000000000")
            If rsEmp.Tables(0).Rows.Count > 0 Then
                'recFP.Fields("UserName") = IIf(IsNull(rsEmp(0)), " ", rsEmp(0))
                UserName = rsEmp.Tables(0).Rows(0).Item("empname").ToString.Trim
            Else
                'recFP.Fields("UserName") = ""
                UserName = ""
            End If
            'If vBackupNumber = BACKUP_PSW Then
            '    ReDim vbytEnrollData(PWD_DATA_SIZE - 1)
            '    CopyMemory(vbytEnrollData(0), mbytEnrollData(0), PWD_DATA_SIZE)
            '    'recFP.Fields("Template_Tw") = vbytEnrollData
            '    Template_Tw = Encoding.UTF8.GetString(vbytEnrollData, 0, vbytEnrollData.Length)
            'Else

            If vBackupNumber = BACKUP_PSW Or vBackupNumber = BACKUP_CARD Then
                ReDim vbytEnrollData(PWD_DATA_SIZE - 1)
                Array.Copy(mbytEnrollData, vbytEnrollData, PWD_DATA_SIZE)
                Template_Tw = Encoding.UTF8.GetString(vbytEnrollData, 0, vbytEnrollData.Length)
                'Template_Tw = UnicodeBytesToString(vbytEnrollData)
            ElseIf vBackupNumber >= BACKUP_FP_0 And vBackupNumber <= BACKUP_FP_9 Then
                ReDim vbytEnrollData(FP_DATA_SIZE - 1)
                ReDim vbytConvFpData(FP_DATA_SIZE - 1)
                Array.Copy(mbytEnrollData, vbytEnrollData, FP_DATA_SIZE)
                ConvFpDataToSaveInDbForCompatibility(vbytEnrollData, vbytConvFpData)
                Template_Tw = Encoding.UTF8.GetString(vbytEnrollData, 0, vbytEnrollData.Length)
            ElseIf vBackupNumber = BACKUP_FACE Then
                ReDim vbytEnrollData(FACE_DATA_SIZE - 1)
                Array.Copy(mbytEnrollData, vbytEnrollData, FACE_DATA_SIZE)
                Template_Tw = Encoding.UTF8.GetString(vbytEnrollData, 0, vbytEnrollData.Length)
            ElseIf vBackupNumber = BACKUP_VEIN_0 Then
                ReDim vbytEnrollData(VEIN_DATA_SIZE - 1)
                Array.Copy(mbytEnrollData, vbytEnrollData, VEIN_DATA_SIZE)
                Template_Tw = Encoding.UTF8.GetString(vbytEnrollData, 0, vbytEnrollData.Length)
            End If


            'If vBackupNumber = BACKUP_PSW Then
            '    ReDim vbytEnrollData(PWD_DATA_SIZE - 1)
            '    CopyMemory(vbytEnrollData(0), mbytEnrollData(0), PWD_DATA_SIZE)
            '    'recFP.Fields("Template_Tw") = vbytEnrollData
            '    Array.Copy(vbytEnrollData, Template_Tw, PWD_DATA_SIZE)
            '    'Template_Tw = Encoding.UTF8.GetString(vbytEnrollData, 0, vbytEnrollData.Length)
            'ElseIf vBackupNumber = BACKUP_PSW Or vBackupNumber = BACKUP_CARD Then
            '    ReDim vbytEnrollData(PWD_DATA_SIZE - 1)
            '    ReDim Template_Tw(PWD_DATA_SIZE - 1)
            '    Array.Copy(mbytEnrollData, vbytEnrollData, PWD_DATA_SIZE)
            '    Array.Copy(vbytEnrollData, Template_Tw, PWD_DATA_SIZE)
            '    'Template_Tw = Encoding.UTF8.GetString(vbytEnrollData, 0, vbytEnrollData.Length)
            '    'Template_Tw = UnicodeBytesToString(vbytEnrollData)
            'ElseIf vBackupNumber >= BACKUP_FP_0 And vBackupNumber <= BACKUP_FP_9 Then
            '    ReDim vbytEnrollData(FP_DATA_SIZE - 1)
            '    ReDim vbytConvFpData(FP_DATA_SIZE - 1)
            '    ReDim Template_Tw(FP_DATA_SIZE - 1)
            '    Array.Copy(mbytEnrollData, vbytEnrollData, FP_DATA_SIZE)
            '    ConvFpDataToSaveInDbForCompatibility(vbytEnrollData, vbytConvFpData)
            '    'Template_Tw = Encoding.UTF8.GetString(vbytConvFpData, 0, vbytConvFpData.Length)
            '    Array.Copy(vbytConvFpData, Template_Tw, FP_DATA_SIZE)
            'ElseIf vBackupNumber = BACKUP_FACE Then
            '    ReDim vbytEnrollData(FACE_DATA_SIZE - 1)
            '    ReDim Template_Tw(FACE_DATA_SIZE - 1)
            '    Array.Copy(mbytEnrollData, vbytEnrollData, FACE_DATA_SIZE)
            '    Array.Copy(vbytEnrollData, Template_Tw, FACE_DATA_SIZE)
            '    'Template_Tw = Encoding.UTF8.GetString(vbytEnrollData, 0, vbytEnrollData.Length)
            'ElseIf vBackupNumber = BACKUP_VEIN_0 Then
            '    ReDim vbytEnrollData(VEIN_DATA_SIZE - 1)
            '    ReDim Template_Tw(VEIN_DATA_SIZE - 1)
            '    Array.Copy(mbytEnrollData, vbytEnrollData, VEIN_DATA_SIZE)
            '    Array.Copy(vbytEnrollData, Template_Tw, VEIN_DATA_SIZE)
            '    'Template_Tw = Encoding.UTF8.GetString(vbytEnrollData, 0, vbytEnrollData.Length)
            'End If


            'recFP.Fields("FingerNumber") = vBackupNumber
            'recFP.Fields("Privilege") = vPrivilege
            'recFP.Fields("Verifymode") = vVerifyMode

            FingerNumber = vBackupNumber
            Privilege = vPrivilege
            Verifymode = vVerifyMode

            'end
            'recFP.Update()
            'recFP.Close()
            recFP = Nothing

            Dim sqlinsert As String = "insert into fptable (Emachinenumber, EnrollNumber, UserName, Template_Tw, FingerNumber, Privilege, Verifymode, Template_Face, DeviceType) values (" & Emachinenumber & ",'" & EnrollNumber & "', '" & UserName & "', @Template_Tw, '" & FingerNumber & "', '" & Privilege & "', '" & Verifymode & "', '" & Template_Face & "','F9')"

            If Common.servername = "Access" Then
                Common.con1.Open()
                cmd1 = New OleDbCommand(sqlinsert, Common.con1)
                'Using picture As Image = Image.FromFile("~/fb")
                Using stream As New IO.MemoryStream
                    'picture.Save(stream, Imaging.ImageFormat.Jpeg)
                    cmd1.Parameters.Add("@Template_Tw", OleDb.OleDbType.VarBinary).Value = vbytEnrollData 'stream.GetBuffer()
                    cmd1.ExecuteNonQuery()
                    Common.con1.Close()
                End Using
            Else
                Common.con.Open()
                cmd = New SqlCommand(sqlinsert, Common.con)
                Using stream As New IO.MemoryStream
                    cmd.Parameters.Add("@Template_Tw", SqlDbType.VarBinary).Value = vbytEnrollData
                    cmd.ExecuteNonQuery()
                End Using
                Common.con.Close()
            End If
FFF:
            If IsNumeric(EnrollNumber) Then
                EnrollNumber = Convert.ToDouble(EnrollNumber)
            End If
            paycodelist.Add(EnrollNumber & ";" & "")
        Next k

        vRet = F9.FK_EnableDevice(mnCommHandleIndex, 1)
        F9.FK_DisConnect(mnCommHandleIndex)
        Me.Cursor = Cursors.Default
    End Sub
    Private Sub ConvFpDataToSaveInDbForCompatibility(ByRef abytSrc() As Byte, ByRef abytDest() As Byte)
        Dim nTempLen As Long, lenConvFpData As Long
        Dim bytConvFpData() As Byte
        Dim k As Int32, m As Int32
        Dim bytTemp() As Byte

        nTempLen = abytSrc.Length / 4
        lenConvFpData = nTempLen * 5
        ReDim bytConvFpData(lenConvFpData)

        For k = 0 To nTempLen - 1
            ReDim bytTemp(3)
            bytTemp(0) = abytSrc(k * 4)
            bytTemp(1) = abytSrc(k * 4 + 1)
            bytTemp(2) = abytSrc(k * 4 + 2)
            bytTemp(3) = abytSrc(k * 4 + 3)

            m = BitConverter.ToInt32(bytTemp, 0)
            bytConvFpData(k * 5) = 1
            If m < 0 Then
                If m = -2147483648 Then
                    bytConvFpData(k * 5) = 2
                    m = 2147483647
                Else
                    bytConvFpData(k * 5) = 0
                    m = -m
                End If
            End If
            bytTemp = BitConverter.GetBytes(m)
            bytConvFpData(k * 5 + 1) = bytTemp(3)
            bytConvFpData(k * 5 + 2) = bytTemp(2)
            bytConvFpData(k * 5 + 3) = bytTemp(1)
            bytConvFpData(k * 5 + 4) = bytTemp(0)
        Next

        abytDest = bytConvFpData
    End Sub
    Private Sub convFpDataToSaveInDbForCompatibility_ATF686n(ByVal abytSrc() As Byte, ByRef abytDest() As Byte)
        Dim nTempLen As Integer = abytSrc.Length \ 4
        Dim lenConvFpData As Integer = nTempLen * 5
        Dim byteConvFpData(lenConvFpData - 1) As Byte
        Dim byteTemp(3) As Byte
        Dim k, m1 As Integer

        For k = 0 To nTempLen - 1
            byteTemp(0) = abytSrc(k * 4)
            byteTemp(1) = abytSrc(k * 4 + 1)
            byteTemp(2) = abytSrc(k * 4 + 2)
            byteTemp(3) = abytSrc(k * 4 + 3)
            m1 = BitConverter.ToInt32(byteTemp, 0)

            byteConvFpData(k * 5) = 1
            If m1 < 0 Then
                If m1 = -2147483648 Then
                    byteConvFpData(k * 5) = 2
                    m1 = 2147483647
                Else
                    byteConvFpData(k * 5) = 0
                    m1 = -m1
                End If
            End If
            byteTemp = BitConverter.GetBytes(m1)
            byteConvFpData(k * 5 + 1) = byteTemp(3)
            byteConvFpData(k * 5 + 2) = byteTemp(2)
            byteConvFpData(k * 5 + 3) = byteTemp(1)
            byteConvFpData(k * 5 + 4) = byteTemp(0)
        Next k
        abytDest = byteConvFpData
    End Sub
    Private Sub ToggleSwitch1_Toggled(sender As System.Object, e As System.EventArgs) Handles ToggleDownloadAll.Toggled
        If ToggleDownloadAll.IsOn = True Then
            LabelControl1.Visible = False
            TextSelectUser.Visible = False
        Else
            LabelControl1.Visible = True
            TextSelectUser.Visible = True
        End If
    End Sub
    Private Sub btnDeleteFrmDB_Click(sender As System.Object, e As System.EventArgs) Handles btnDeleteFrmDB.Click
        If GridView2.GetSelectedRows.Count = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>Please select User from Fring print table</size>", "<size=9>Error</size>")
            Exit Sub
        Else
            If TextPassword.Text.Trim = "" Then
                XtraMessageBox.Show(ulf, "<size=10>Please Enter Admin Password</size>", "<size=9>Error</size>")
                TextPassword.Select()
                Exit Sub
            Else
                If TextPassword.Text.ToLower <> Common.AdminPASSWORD.ToLower Then
                    XtraMessageBox.Show(ulf, "<size=10>Admin Password Incorrect</size>", "<size=9>Error</size>")
                    TextPassword.Select()
                    Exit Sub
                End If
            End If
            If XtraMessageBox.Show(ulf, "<size=10>" & Common.res_man.GetString("askdelete", Common.cul) & "</size>", Common.res_man.GetString("confirmdeleteion", Common.cul), _
                              MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                Me.Cursor = Cursors.WaitCursor
                For Each c As Control In Me.Controls
                    c.Enabled = False
                Next
                If Common.servername = "Access" Then
                    If Common.con1.State <> ConnectionState.Open Then
                        Common.con1.Open()
                    End If
                Else
                    If Common.con.State <> ConnectionState.Open Then
                        Common.con.Open()
                    End If
                End If
                Dim sSql As String = ""
                Dim selectedRows As Integer() = GridView2.GetSelectedRows()
                Dim result As Object() = New Object(selectedRows.Length - 1) {}
                Dim FingerNumber As String
                Dim EnrollNumber As String
                Dim EMachineNumber As String
                For i As Integer = 0 To selectedRows.Length - 1
                    Dim rowHandle As Integer = selectedRows(i)
                    If Not GridView2.IsGroupRow(rowHandle) Then
                        EnrollNumber = GridView2.GetRowCellValue(rowHandle, "EnrollNumber").ToString.Trim
                        EMachineNumber = GridView2.GetRowCellValue(rowHandle, "EMachineNumber").ToString.Trim
                        XtraMasterTest.LabelControlStatus.Text = "Deleting Template " & EnrollNumber
                        Application.DoEvents()
                        Common.LogPost("Device User Upload from DB; Device ID: " & EMachineNumber)
                        FingerNumber = GridView2.GetRowCellValue(rowHandle, "FingerNumber")
                        sSql = "delete from fptable where EnrollNumber='" & EnrollNumber & "' and FingerNumber='" & FingerNumber & "' and EMachineNumber=" & EMachineNumber & ""
                        If Common.servername = "Access" Then
                            sSql = "delete from fptable where EnrollNumber='" & EnrollNumber & "' and FingerNumber=" & FingerNumber & " and EMachineNumber= " & EMachineNumber & ""
                            cmd1 = New OleDbCommand(sSql, Common.con1)
                            cmd1.ExecuteNonQuery()
                        Else
                            cmd = New SqlCommand(sSql, Common.con)
                            cmd.ExecuteNonQuery()
                        End If
                    End If
                Next
                If Common.servername = "Access" Then
                    If Common.con1.State <> ConnectionState.Closed Then
                        Common.con1.Close()
                    End If
                Else
                    If Common.con.State <> ConnectionState.Closed Then
                        Common.con.Close()
                    End If
                End If
                For Each c As Control In Me.Controls
                    c.Enabled = True
                Next
                Me.Cursor = Cursors.Default
                XtraMasterTest.LabelControlStatus.Text = ""
                Application.DoEvents()
                'If Common.servername = "Access" Then
                '    Me.Fptable1TableAdapter1.Fill(Me.SSSDBDataSet.fptable1)
                '    GridControl2.DataSource = SSSDBDataSet.fptable1
                'Else
                '    FptableTableAdapter.Connection.ConnectionString = Common.ConnectionString
                '    Me.FptableTableAdapter.Fill(Me.SSSDBDataSet.fptable)
                '    GridControl2.DataSource = SSSDBDataSet.fptable
                'End If
                LoadFPGrid()
                TextPassword.Text = ""
                XtraMasterTest.LabelControlStatus.Text = ""
                Application.DoEvents()
                XtraMessageBox.Show(ulf, "<size=10>Task Completed</size>", "<size=9>Information</size>")
            End If
        End If
    End Sub
    Private Sub cmdSetAllEnrollData_bio_All(mMachine_ID As Long, mUser_ID As String, FpNo As Long, LstMachineIdDest As Long, isStringId As Boolean, mnCommHandleIndex As Long)
        'MsgBox(mUser_ID)
        Dim vEnrollNumber As Long
        Dim vStrEnrollNumber As String = ""
        Dim vEMachineNumber As Long
        Dim vFingerNumber As Long
        Dim vPrivilege As Long
        'Dim vEnable As Long
        Dim vFlag As Boolean
        Dim vRet As Long
        'Dim vErrorCode As Long
        'Dim vStr As String
        'Dim vByte() As Byte
        'Dim i As Long
        'Dim vTitle As String
        'Dim vConvResult As Long
        Dim vnIsSuppoterd As Long
        Dim RsFp As New DataSet 'ADODB.Recordset
        Dim vbytConvFpData() As Byte
        Dim vbytEnrollData() As Byte
        ' Dim Verify As Long
        Dim vnResultCode As Long
        Dim verifymode As Long
        'Dim vnii As Long
        'Dim vnLen As Long
        Dim Uname As String
        '    lstEnrollData.Clear
        '    vTitle = frmEnroll.Caption
        '    lblMessage.Caption = "Working..."
        '    DoEvents
        Dim mMachineNumber As String = mMachine_ID
        vFlag = False
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        RsFp = New DataSet

        Dim sSql As String = "select * from fptable where enrollnumber='" & mUser_ID & "' and FingerNumber=" & FpNo & " and DeviceType='Bio' and EMachineNumber=" & LstMachineIdDest & " order by FingerNumber"
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(RsFp)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(RsFp)
        End If
        'RsFp = Cn.Execute(sSql)
        If RsFp.Tables(0).Rows.Count > 0 Then
            'With RsFp
            If RsFp.Tables(0).Rows.Count = 0 Then GoTo EEE
            vEMachineNumber = RsFp.Tables(0).Rows(0).Item("EMachineNumber").ToString.Trim '!EMachineNumber
            vEnrollNumber = RsFp.Tables(0).Rows(0).Item("EnrollNumber").ToString.Trim '!EnrollNumber
            If IsNumeric(RsFp.Tables(0).Rows(0).Item("EnrollNumber").ToString.Trim) Then
                vStrEnrollNumber = Convert.ToDouble(RsFp.Tables(0).Rows(0).Item("EnrollNumber").ToString.Trim)
            Else
                vStrEnrollNumber = RsFp.Tables(0).Rows(0).Item("EnrollNumber").ToString.Trim
            End If
            '!EnrollNumber
            vFingerNumber = RsFp.Tables(0).Rows(0).Item("FingerNumber").ToString.Trim '!FingerNumber
            If ToggleMakeAdmin.IsOn = True Then
                vPrivilege = 1
            Else
                vPrivilege = RsFp.Tables(0).Rows(0).Item("privilege").ToString.Trim ' !privilege
            End If

            'verifymode = IIf(IsNull(!verifymode), 0, !verifymode)
            If RsFp.Tables(0).Rows(0).Item("verifymode").ToString.Trim = "" Then
                verifymode = 0
            Else
                verifymode = RsFp.Tables(0).Rows(0).Item("verifymode").ToString.Trim
            End If

            'ZeroMemory(mbytEnrollData(0), UBound(mbytEnrollData) + 1)
            If vFingerNumber = BACKUP_PSW Or vFingerNumber = BACKUP_CARD Then
                vbytEnrollData = RsFp.Tables(0).Rows(0).Item("Template_Tw") 'System.Text.Encoding.Unicode.GetBytes(RsFp.Tables(0).Rows(0).Item("Template_Tw").ToString.Trim) '.Fields("FPdata").Value
                'Array.Copy(vbytEnrollData, mbytEnrollData, PWD_DATA_SIZE)
                Array.Copy(vbytEnrollData, mbytEnrollData, PWD_DATA_SIZE)
            ElseIf vFingerNumber >= BACKUP_FP_0 And vFingerNumber <= BACKUP_FP_9 Then
                vbytConvFpData = RsFp.Tables(0).Rows(0).Item("Template_Tw")
                ReDim vbytEnrollData(FP_DATA_SIZE - 1)
                ConvFpDataAfterReadFromDbForCompatibility(vbytConvFpData, vbytEnrollData)
                Array.Copy(vbytEnrollData, mbytEnrollData, FP_DATA_SIZE)
            ElseIf vFingerNumber = BACKUP_FACE Then
                vbytEnrollData = RsFp.Tables(0).Rows(0).Item("Template_Tw") 'System.Text.Encoding.Unicode.GetBytes(RsFp.Tables(0).Rows(0).Item("Template_Tw").ToString.Trim) '.Fields("FPdata").Value
                Array.Copy(vbytEnrollData, mbytEnrollData, FACE_DATA_SIZE)
            ElseIf vFingerNumber = BACKUP_VEIN_0 Then
                vbytEnrollData = RsFp.Tables(0).Rows(0).Item("Template_Tw") 'System.Text.Encoding.Unicode.GetBytes(RsFp.Tables(0).Rows(0).Item("Template_Tw").ToString.Trim) ''.Fields("FPdata").Value
                Array.Copy(vbytEnrollData, mbytEnrollData, VEIN_DATA_SIZE)
            End If


            vnResultCode = FK_EnableDevice(mnCommHandleIndex, 0)
            If vnResultCode <> RUN_SUCCESS Then
                Exit Sub
            End If

            FK_IsSupportedEnrollData(mnCommHandleIndex, vFingerNumber, vnIsSuppoterd)
            If vnIsSuppoterd = 0 Then
                XtraMessageBox.Show(ulf, "<size=10>Template Not supported</size>", "<size=9>Information</size>")
                vRet = FK_EnableDevice(mnCommHandleIndex, 1)
                GoTo EEE
            End If

            If isStringId Then
                vnResultCode = FK_PutEnrollData_StringID(mnCommHandleIndex, _
                                                 vStrEnrollNumber, _
                                                 vFingerNumber, _
                                                 vPrivilege, _
                                                 mbytEnrollData, _
                                                 mlngPasswordData)
            Else
                vnResultCode = FK_PutEnrollData(mnCommHandleIndex, _
                                                 vEnrollNumber, _
                                                 vFingerNumber, _
                                                 vPrivilege, _
                                                 mbytEnrollData, _
                                                 mlngPasswordData)
            End If

            If vnResultCode <> RUN_SUCCESS Then
                'vStr = "SetAllEnrollData Error"
                'vnMessRet = MsgBox(ReturnResultPrint(vnResultCode) & ": Continue ?", vbYesNoCancel, "SetEnrollData")
                'If vnMessRet = vbYes Then GoTo fff
                'If vnMessRet = vbCancel Then Exit Do
            End If


            'need to do in future for face
            'If Len(!Template_Face) > 0 Then
            '    mbytPhotoImage = !Template_Face
            '    Dim nPhotoLen As Long
            '    nPhotoLen = UBound(mbytPhotoImage) + 1
            '    If nPhotoLen > 1 Then
            '        nFKRetCode = FK_EnableDevice(mnCommHandleIndex, 0)
            '        If nFKRetCode <> RUN_SUCCESS Then
            '            'lblMessage.Caption = gstrNoDevice
            '        Else
            '            nFKRetCode = FK_SetEnrollPhoto(mnCommHandleIndex, _
            '                            vEnrollNumber, _
            '                            mbytPhotoImage(0), _
            '                            nPhotoLen)
            '        End If
            '    End If
            'End If

            '**************End**********


            'vnResultCode = FK_SetUserName(mnCommHandleIndex, _
            'vEnrollNumber, Uname)
            If vnResultCode <> RUN_SUCCESS Then
                GoTo LLL
            End If

            If vnResultCode = RUN_SUCCESS Then
                'lblMessage.Caption = "Saving..."
                'DoEvents
                vnResultCode = FK_SaveEnrollData(mnCommHandleIndex)
                If vnResultCode = RUN_SUCCESS Then
                    'lblMessage.Caption = "SetAllEnrollData OK"
                Else
                    'lblMessage.Caption = ReturnResultPrint(vnResultCode)
                End If
            Else
                'lblMessage.Caption = vStr & " : " & ReturnResultPrint(vnResultCode)
            End If

            vnResultCode = 0
            'vnResultCode = FK623Attend.SetUserName(vEnrollNumber, Trim(!UserName))
            Uname = RsFp.Tables(0).Rows(0).Item("Username") 'Trim(!Username)
            vStrEnrollNumber = vEnrollNumber
            If FK_GetIsSupportStringID(mnCommHandleIndex) = RUN_SUCCESS Then
                vnResultCode = FK_SetUserName_StringID(mnCommHandleIndex, _
                                            vStrEnrollNumber, Uname)
            Else
                vnResultCode = FK_SetUserName(mnCommHandleIndex, _
                                            vEnrollNumber, Uname)
            End If



LLL:
            ' lblMessage.Caption = "EMachine = " & Format(vEMachineNumber, "00#") & ", ID = " & Format(vEnrollNumber, "000#") & ", FpNo = " & vFingerNumber _
            '& ", Count = " & (.AbsolutePosition + 1)

            'frmEnroll.Caption = (.AbsolutePosition + 1)
            'DoEvents()
            '.MoveNext()
            'Loop
            'End With
        End If
EEE:
        ' vTitle = frmEnroll.Caption
        FK_EnableDevice(mnCommHandleIndex, 1)
        'Screen.MousePointer = vbNormal
        'gGetState = False
        'ProgressBar1.Value = 0
        ''If ProgressBar1.Value > 99 Then ProgressBar1.Value = 0
        'sbFields.Panels(1).Text = "Updated user details successfully"
        '' lblMessage.Caption = "SetAllUserData OK"
        'DoEvents()

        'FK623Attend.EnableDevice True

    End Sub

    Private Sub cmdSetAllEnrollData_TF01_All(mMachine_ID As Long, mUser_ID As String, FpNo As Long, LstMachineIdDest As Long)
        Dim vEnrollNumber As Long
        Dim vStrEnrollNumber As String = ""
        Dim vEMachineNumber As Long
        Dim vFingerNumber As Long
        Dim vPrivilege As Long
        Dim vPass As Long = 0
        Dim vFlag As Boolean
        Dim vRet As Long
        Dim vnIsSuppoterd As Long
        Dim RsFp As New DataSet 'ADODB.Recordset
        Dim dwFPData((1420 \ 4) - 1) As Integer
        Dim FacedwFPData((1888 \ 4) - 1) As Integer

        Dim vnResultCode As Long
        Dim verifymode As Long
        Dim Uname As String
        Dim mMachineNumber As String = mMachine_ID
        vFlag = False
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        RsFp = New DataSet
        Dim obj As Object = 0
        Dim bRet As Boolean
        'DisableDevice(LstMachineIdDest)
        AxFP_CLOCK1.EnableDevice(LstMachineIdDest, 0)
        Dim sSql As String = "select * from fptable where enrollnumber='" & mUser_ID & "' and FingerNumber=" & FpNo & " and DeviceType='TF-01' and EMachineNumber=" & LstMachineIdDest & " order by FingerNumber"
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(RsFp)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(RsFp)
        End If

        If RsFp.Tables(0).Rows.Count > 0 Then
            If RsFp.Tables(0).Rows.Count = 0 Then GoTo EEE
            vEMachineNumber = RsFp.Tables(0).Rows(0).Item("EMachineNumber").ToString.Trim '!EMachineNumber
            vEnrollNumber = RsFp.Tables(0).Rows(0).Item("EnrollNumber").ToString.Trim '!EnrollNumber
            If IsNumeric(RsFp.Tables(0).Rows(0).Item("EnrollNumber").ToString.Trim) Then
                vStrEnrollNumber = Convert.ToDouble(RsFp.Tables(0).Rows(0).Item("EnrollNumber").ToString.Trim)
            Else
                vStrEnrollNumber = RsFp.Tables(0).Rows(0).Item("EnrollNumber").ToString.Trim
            End If
            vFingerNumber = RsFp.Tables(0).Rows(0).Item("FingerNumber").ToString.Trim '!FingerNumber
            '!FingerNumber

            If RsFp.Tables(0).Rows(0).Item("cardnumber").ToString.Trim <> "" Then
                vPass = Convert.ToDouble(RsFp.Tables(0).Rows(0).Item("cardnumber").ToString.Trim)
            ElseIf RsFp.Tables(0).Rows(0).Item("Password").ToString.Trim <> "" Then
                vPass = Convert.ToDouble(RsFp.Tables(0).Rows(0).Item("Password").ToString.Trim)
            End If


            'If ToggleMakeAdmin.IsOn = True Then
            '    vPrivilege = 1
            'Else
            '    vPrivilege = RsFp.Tables(0).Rows(0).Item("privilege").ToString.Trim ' !privilege
            'End If

            If FpNo < 10 Then
                obj = New System.Runtime.InteropServices.VariantWrapper(RsFp.Tables(0).Rows(0).Item("Template_Tw"))
            ElseIf FpNo > 19 Then
                obj = New System.Runtime.InteropServices.VariantWrapper(RsFp.Tables(0).Rows(0).Item("Template_Tw"))
            Else
                obj = New System.Runtime.InteropServices.VariantWrapper(dwFPData)
            End If
            'AxFP_CLOCK1.EnableDevice(LstMachineIdDest, 0)
            bRet = AxFP_CLOCK1.SetEnrollData(LstMachineIdDest, vStrEnrollNumber, LstMachineIdDest, FpNo, vPrivilege, obj, vPass)
            If Not bRet Then

            End If

            Uname = RsFp.Tables(0).Rows(0).Item("Username")
            obj = New System.Runtime.InteropServices.VariantWrapper(Uname)
            vnResultCode = 0
            vStrEnrollNumber = vEnrollNumber
            bRet = AxFP_CLOCK1.SetUserName(0, LstMachineIdDest, vStrEnrollNumber, LstMachineIdDest, obj)
            If ToggleMakeAdmin.IsOn = True Then
                bRet = AxFP_CLOCK1.ModifyPrivilege(LstMachineIdDest, vStrEnrollNumber, LstMachineIdDest, FpNo, 1)

            End If




LLL:
        End If
EEE:

        AxFP_CLOCK1.EnableDevice(LstMachineIdDest, 1)
        'Dim tmp As String = "test"

    End Sub


    Private Sub cmdSetAllEnrollData_F9_All(mMachine_ID As Long, mUser_ID As String, FpNo As Long, LstMachineIdDest As Long, isStringId As Boolean, F9 As mdlPublic_f9, mnCommHandleIndex As Long)
        'MsgBox(mUser_ID)
        Dim vEnrollNumber As Long
        Dim vStrEnrollNumber As String = ""
        Dim vEMachineNumber As Long
        Dim vFingerNumber As Long
        Dim vPrivilege As Long
        'Dim vEnable As Long
        Dim vFlag As Boolean
        Dim vRet As Long
        'Dim vErrorCode As Long
        'Dim vStr As String
        'Dim vByte() As Byte
        'Dim i As Long
        'Dim vTitle As String
        'Dim vConvResult As Long
        Dim vnIsSuppoterd As Long
        Dim RsFp As New DataSet 'ADODB.Recordset
        Dim vbytConvFpData() As Byte
        Dim vbytEnrollData() As Byte
        ' Dim Verify As Long
        Dim vnResultCode As Long
        Dim verifymode As Long
        'Dim vnii As Long
        'Dim vnLen As Long
        Dim Uname As String
        '    lstEnrollData.Clear
        '    vTitle = frmEnroll.Caption
        '    lblMessage.Caption = "Working..."
        '    DoEvents
        Dim mMachineNumber As String = mMachine_ID
        vFlag = False
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        RsFp = New DataSet

        Dim sSql As String = "select * from fptable where enrollnumber='" & mUser_ID & "' and FingerNumber=" & FpNo & " and DeviceType='F9' and EMachineNumber=" & LstMachineIdDest & " order by FingerNumber"
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(RsFp)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(RsFp)
        End If
        'RsFp = Cn.Execute(sSql)
        If RsFp.Tables(0).Rows.Count > 0 Then
            'With RsFp
            If RsFp.Tables(0).Rows.Count = 0 Then GoTo EEE
            vEMachineNumber = RsFp.Tables(0).Rows(0).Item("EMachineNumber").ToString.Trim '!EMachineNumber
            vEnrollNumber = RsFp.Tables(0).Rows(0).Item("EnrollNumber").ToString.Trim '!EnrollNumber
            If IsNumeric(RsFp.Tables(0).Rows(0).Item("EnrollNumber").ToString.Trim) Then
                vStrEnrollNumber = Convert.ToDouble(RsFp.Tables(0).Rows(0).Item("EnrollNumber").ToString.Trim)
            Else
                vStrEnrollNumber = RsFp.Tables(0).Rows(0).Item("EnrollNumber").ToString.Trim
            End If
            '!EnrollNumber
            vFingerNumber = RsFp.Tables(0).Rows(0).Item("FingerNumber").ToString.Trim '!FingerNumber
            If ToggleMakeAdmin.IsOn = True Then
                vPrivilege = 1
            Else
                vPrivilege = RsFp.Tables(0).Rows(0).Item("privilege").ToString.Trim ' !privilege
            End If

            'verifymode = IIf(IsNull(!verifymode), 0, !verifymode)
            If RsFp.Tables(0).Rows(0).Item("verifymode").ToString.Trim = "" Then
                verifymode = 0
            Else
                verifymode = RsFp.Tables(0).Rows(0).Item("verifymode").ToString.Trim
            End If

            'ZeroMemory(mbytEnrollData(0), UBound(mbytEnrollData) + 1)
            If vFingerNumber = BACKUP_PSW Or vFingerNumber = BACKUP_CARD Then
                vbytEnrollData = RsFp.Tables(0).Rows(0).Item("Template_Tw") 'System.Text.Encoding.Unicode.GetBytes(RsFp.Tables(0).Rows(0).Item("Template_Tw").ToString.Trim) '.Fields("FPdata").Value
                'Array.Copy(vbytEnrollData, mbytEnrollData, PWD_DATA_SIZE)
                Array.Copy(vbytEnrollData, mbytEnrollData, PWD_DATA_SIZE)
            ElseIf vFingerNumber >= BACKUP_FP_0 And vFingerNumber <= BACKUP_FP_9 Then
                vbytConvFpData = RsFp.Tables(0).Rows(0).Item("Template_Tw")
                ReDim vbytEnrollData(FP_DATA_SIZE - 1)
                ConvFpDataAfterReadFromDbForCompatibility(vbytConvFpData, vbytEnrollData)
                Array.Copy(vbytEnrollData, mbytEnrollData, FP_DATA_SIZE)
            ElseIf vFingerNumber = BACKUP_FACE Then
                vbytEnrollData = RsFp.Tables(0).Rows(0).Item("Template_Tw") 'System.Text.Encoding.Unicode.GetBytes(RsFp.Tables(0).Rows(0).Item("Template_Tw").ToString.Trim) '.Fields("FPdata").Value
                Array.Copy(vbytEnrollData, mbytEnrollData, FACE_DATA_SIZE)
            ElseIf vFingerNumber = BACKUP_VEIN_0 Then
                vbytEnrollData = RsFp.Tables(0).Rows(0).Item("Template_Tw") 'System.Text.Encoding.Unicode.GetBytes(RsFp.Tables(0).Rows(0).Item("Template_Tw").ToString.Trim) ''.Fields("FPdata").Value
                Array.Copy(vbytEnrollData, mbytEnrollData, VEIN_DATA_SIZE)
            End If


            vnResultCode = F9.FK_EnableDevice(mnCommHandleIndex, 0)
            If vnResultCode <> RUN_SUCCESS Then
                Exit Sub
            End If

            F9.FK_IsSupportedEnrollData(mnCommHandleIndex, vFingerNumber, vnIsSuppoterd)
            If vnIsSuppoterd = 0 Then
                XtraMessageBox.Show(ulf, "<size=10>Template Not supported</size>", "<size=9>Information</size>")
                vRet = F9.FK_EnableDevice(mnCommHandleIndex, 1)
                GoTo EEE
            End If

            If isStringId Then
                vnResultCode = F9.FK_PutEnrollData_StringID(mnCommHandleIndex, _
                                                 vStrEnrollNumber, _
                                                 vFingerNumber, _
                                                 vPrivilege, _
                                                 mbytEnrollData, _
                                                 mlngPasswordData)
            Else
                vnResultCode = F9.FK_PutEnrollData(mnCommHandleIndex, _
                                                 vEnrollNumber, _
                                                 vFingerNumber, _
                                                 vPrivilege, _
                                                 mbytEnrollData, _
                                                 mlngPasswordData)
            End If

            If vnResultCode <> RUN_SUCCESS Then
                'vStr = "SetAllEnrollData Error"
                'vnMessRet = MsgBox(ReturnResultPrint(vnResultCode) & ": Continue ?", vbYesNoCancel, "SetEnrollData")
                'If vnMessRet = vbYes Then GoTo fff
                'If vnMessRet = vbCancel Then Exit Do
            End If


            'need to do in future for face
            'If Len(!Template_Face) > 0 Then
            '    mbytPhotoImage = !Template_Face
            '    Dim nPhotoLen As Long
            '    nPhotoLen = UBound(mbytPhotoImage) + 1
            '    If nPhotoLen > 1 Then
            '        nFKRetCode = FK_EnableDevice(mnCommHandleIndex, 0)
            '        If nFKRetCode <> RUN_SUCCESS Then
            '            'lblMessage.Caption = gstrNoDevice
            '        Else
            '            nFKRetCode = FK_SetEnrollPhoto(mnCommHandleIndex, _
            '                            vEnrollNumber, _
            '                            mbytPhotoImage(0), _
            '                            nPhotoLen)
            '        End If
            '    End If
            'End If

            '**************End**********


            'vnResultCode = FK_SetUserName(mnCommHandleIndex, _
            'vEnrollNumber, Uname)
            If vnResultCode <> RUN_SUCCESS Then
                GoTo LLL
            End If

            If vnResultCode = RUN_SUCCESS Then
                'lblMessage.Caption = "Saving..."
                'DoEvents
                vnResultCode = F9.FK_SaveEnrollData(mnCommHandleIndex)
                If vnResultCode = RUN_SUCCESS Then
                    'lblMessage.Caption = "SetAllEnrollData OK"
                Else
                    'lblMessage.Caption = ReturnResultPrint(vnResultCode)
                End If
            Else
                'lblMessage.Caption = vStr & " : " & ReturnResultPrint(vnResultCode)
            End If

            vnResultCode = 0
            'vnResultCode = FK623Attend.SetUserName(vEnrollNumber, Trim(!UserName))
            Uname = RsFp.Tables(0).Rows(0).Item("Username") 'Trim(!Username)
            vStrEnrollNumber = vEnrollNumber
            If F9.FK_GetIsSupportStringID(mnCommHandleIndex) = RUN_SUCCESS Then
                vnResultCode = F9.FK_SetUserName_StringID(mnCommHandleIndex, _
                                            vStrEnrollNumber, Uname)
            Else
                vnResultCode = F9.FK_SetUserName(mnCommHandleIndex, _
                                            vEnrollNumber, Uname)
            End If
LLL:
        End If
EEE:
        ' vTitle = frmEnroll.Caption
        F9.FK_EnableDevice(mnCommHandleIndex, 1)
    End Sub
    Private Sub btnUploadFinger_Click(sender As System.Object, e As System.EventArgs) Handles btnUploadFinger.Click
        Dim m As Integer
        'Dim e As Integer
        Dim lngMachineNum As Long
        Dim mCommKey As Long
        Dim lpszIPAddress As String
        Dim mKey As String
        Dim vnMachineNumber As Long
        Dim vnCommPort As Long
        Dim vnCommBaudrate As Long
        Dim vstrTelNumber As String
        Dim vnWaitDialTime As Long
        Dim vnLicense As Long
        Dim vpszIPAddress As String
        Dim vpszNetPort As Long
        Dim vpszNetPassword As Long
        Dim vnTimeOut As Long
        Dim vnProtocolType As Long
        Dim strDateTime As String
        Dim vRet As Long
        Dim vPrivilege As Long

        If GridView1.GetSelectedRows.Count = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>Please select the Machine</size>", "<size=9>Error</size>")
            Exit Sub
        End If

        If GridView2.GetSelectedRows.Count = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>Please select Employee</size>", "<size=9>Error</size>")
            Exit Sub
        End If
        Dim sSql As String = ""
        Me.Cursor = Cursors.WaitCursor

        For Each c As Control In Me.Controls
            c.Enabled = False
        Next
        Dim failIP As New List(Of String)()
        Dim selectedRows As Integer() = GridView1.GetSelectedRows()
        Dim result As Object() = New Object(selectedRows.Length - 1) {}
        Dim LstMachineId As String
        For i = 0 To selectedRows.Length - 1
            Dim rowHandle As Integer = selectedRows(i)
            If Not GridView1.IsGroupRow(rowHandle) Then
                LstMachineId = GridView1.GetRowCellValue(rowHandle, "ID_NO").ToString.Trim

                lpszIPAddress = GridView1.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim 'Trim(rstm!Location)
                vpszIPAddress = Trim(lpszIPAddress)
                XtraMasterTest.LabelControlStatus.Text = "Connecting " & vpszIPAddress & "..."
                Application.DoEvents()
                Common.LogPost("Device User Upload; Device ID: " & LstMachineId)
                If GridView1.GetRowCellValue(rowHandle, "DeviceType") = "Bio-1/3/4/5/6/7/ATF-203/ATF-395/ACR-872" Then
                    sSql = "select * from tblmachine where ID_NO='" & LstMachineId & "'"
                    Dim bConn As Boolean = False
                    vnMachineNumber = LstMachineId
                    vnLicense = 1261 '1789 '                    
                    vpszNetPort = CLng("5005")
                    vpszNetPassword = CLng("0")
                    vnTimeOut = CLng("5000")
                    vnProtocolType = 0 'PROTOCOL_TCPIP
                    'bConn = FK623Attend.ConnectNet(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
                    If GridView1.GetRowCellValue(rowHandle, "A_R").ToString.Trim = "S" Then
                        vRet = FK_ConnectUSB(vnMachineNumber, vnLicense)
                    Else
                        vpszIPAddress = Trim(lpszIPAddress)
                        vpszNetPort = CLng("5005")
                        vpszNetPassword = CLng("0")
                        vnTimeOut = CLng("5000")
                        vnProtocolType = 0
                        vRet = FK_ConnectNet(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
                    End If
                    If vRet > 0 Then
                        bConn = True
                    Else
                        bConn = False
                    End If
                    If bConn Then
                        Dim vstrData As String = New String(CType(ChrW(32), Char), 256)
                        Dim ret = FK_GetProductData(vRet, CType(1, Integer), vstrData)
                        If license.LicenseKey <> "" Then
                            If license.DeviceSerialNo.Contains(vstrData.Trim) Then
                            Else
                                FK_DisConnect(vRet)
                                'Return "Invalid Serial number " & vstrData
                                XtraMessageBox.Show(ulf, "<size=10>Invalid Serial number " & vstrData & "</size>", "Failed")
                                Continue For
                            End If
                        End If

                        Dim isStringId As Boolean
                        If FK_GetIsSupportStringID(vRet) = RUN_SUCCESS Then
                            isStringId = True
                        Else
                            isStringId = False
                        End If
                        Dim selectedRowsEmp As Integer() = GridView2.GetSelectedRows()
                        Dim resultEmp As Object() = New Object(selectedRowsEmp.Length - 1) {}
                        For x As Integer = 0 To selectedRowsEmp.Length - 1
                            'LstEmployeesTarget.ListIndex = e
                            Dim rowHandleEmp As Long = selectedRowsEmp(x)
                            If Not GridView2.IsGroupRow(rowHandleEmp) Then
                                Dim EnrollNumber As String = GridView2.GetRowCellValue(rowHandleEmp, "EnrollNumber").ToString.Trim
                                Dim FingerNumber As Long = GridView2.GetRowCellValue(rowHandleEmp, "FingerNumber").ToString.Trim
                                If GridView2.GetRowCellValue(rowHandleEmp, "DeviceType").ToString.Trim = "Bio" Then
                                    XtraMasterTest.LabelControlStatus.Text = "Uploading Template " & EnrollNumber
                                    XtraMasterTest.LabelControlCount.Text = "Uploading " & x + 1 & " of " & selectedRowsEmp.Length
                                    Application.DoEvents()
                                    Dim LstMachineIdDest As String = GridView2.GetRowCellValue(rowHandleEmp, "EMachineNumber").ToString.Trim()
                                    cmdSetAllEnrollData_bio_All(vnMachineNumber, EnrollNumber, FingerNumber, LstMachineIdDest, isStringId, vRet)
                                End If
                            End If
                        Next x
                    Else
                        'MsgBox("Device No: " & LstMachineId & " Not connected..")
                        'XtraMessageBox.Show(ulf, "<size=10>Device No: " & LstMachineId & " Not connected..</size>", "Information")
                        failIP.Add(vpszIPAddress.ToString)
                        Continue For
                    End If
                    'FK623Attend.Disconnect
                    FK_DisConnect(vRet)
                ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "ZK(TFT)" Or GridView1.GetRowCellValue(rowHandle, "DeviceType") = "Bio-1Pro/ATF305Pro/ATF686Pro" Then

                    Dim sdwEnrollNumber As String
                    Dim sName As String = ""
                    Dim sPassword As String = ""
                    Dim iPrivilege As Integer
                    Dim idwFingerIndex As Integer
                    Dim sTmpData As String = ""
                    Dim sEnabled As String = ""
                    Dim bEnabled As Boolean = False
                    Dim iflag As Integer
                    Dim commkey As Integer
                    commkey = Convert.ToInt32(GridView1.GetRowCellValue(rowHandle, "commkey").ToString.Trim)
                    'lpszIPAddress = GridView1.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim 'Trim(rstm!Location)
                    'vpszIPAddress = Trim(lpszIPAddress)

                    'XtraMasterTest.LabelControlStatus.Text = "Connecting " & vpszIPAddress & "..."
                    'Application.DoEvents()

                    Dim bIsConnected = False
                    Dim iMachineNumber As Integer = 1 ' result(i)
                    Dim idwErrorCode As Integer
                    Dim com As Common = New Common
                    Dim axCZKEM1 As New zkemkeeper.CZKEM
                    axCZKEM1.SetCommPassword(Convert.ToInt32(commkey))  'to check device commkey and db commkey matches
                    bIsConnected = axCZKEM1.Connect_Net(vpszIPAddress, 4370)
                    If bIsConnected = True Then
                        axCZKEM1.EnableDevice(iMachineNumber, False)
                        Dim Ret
                        'loop for fingerprint
                        Dim selectedRowsEmp As Integer() = GridView2.GetSelectedRows()
                        Dim resultEmp As Object() = New Object(selectedRowsEmp.Length - 1) {}
                        'If axCZKEM1.BeginBatchUpdate(iMachineNumber, 1) Then
                        For x As Integer = 0 To selectedRowsEmp.Length - 1
                            'LstEmployeesTarget.ListIndex = e
                            Dim rowHandleEmp As Long = selectedRowsEmp(x)
                            If Not GridView2.IsGroupRow(rowHandleEmp) Then
                                Dim adap As SqlDataAdapter
                                Dim adapA As OleDbDataAdapter
                                'Dim RsFp As DataSet = New DataSet
                                Dim mUser_ID As String = GridView2.GetRowCellValue(rowHandleEmp, "EnrollNumber").ToString.Trim()
                                XtraMasterTest.LabelControlStatus.Text = "Uploading Template " & mUser_ID
                                XtraMasterTest.LabelControlCount.Text = "Uploading " & x + 1 & " of " & selectedRowsEmp.Length
                                Application.DoEvents()
                                'If mUser_ID = "000075000104" Then
                                '    Dim tmp = mUser_ID
                                'End If
                                Dim FpNo As String = GridView2.GetRowCellValue(rowHandleEmp, "FingerNumber").ToString.Trim()
                                Dim LstMachineIdDest As String = GridView2.GetRowCellValue(rowHandleEmp, "EMachineNumber").ToString.Trim()
                                If IsNumeric(mUser_ID) = True Then
                                    sdwEnrollNumber = Convert.ToDouble(mUser_ID)
                                Else
                                    sdwEnrollNumber = mUser_ID
                                End If
                                sName = GridView2.GetRowCellDisplayText(rowHandleEmp, "UserName").ToString.Trim()
                                idwFingerIndex = FpNo ' Convert.ToInt32(RsFp.Tables(0).Rows(0)("FingerNumber").ToString().Trim())
                                sTmpData = GridView2.GetRowCellValue(rowHandleEmp, "Template").ToString().Trim()
                                iPrivilege = Convert.ToInt32(GridView2.GetRowCellValue(rowHandleEmp, "Privilege").ToString().Trim()) 'told by ajitesh(used for admin)
                                If ToggleMakeAdmin.IsOn = True Then
                                    iPrivilege = 1
                                Else
                                    iPrivilege = GridView2.GetRowCellValue(rowHandleEmp, "Privilege").ToString.Trim ' !privilege
                                End If
                                sPassword = GridView2.GetRowCellValue(rowHandleEmp, "Password").ToString().Trim()
                                sEnabled = "true" 'RsFp.Tables(0).Rows(0)(6).ToString().Trim()
                                iflag = 0 'Convert.ToInt32(RsFp.Tables(0).Rows(0)(7).ToString())' as per old s/w
                                Dim icard As String = GridView2.GetRowCellValue(rowHandleEmp, "Cardnumber").ToString().Trim()

                                If sEnabled.ToString().ToLower() = "true" Then
                                    bEnabled = True
                                Else
                                    bEnabled = False
                                End If
                                Dim iBackupNumber As String
                                If icard <> "" Then
                                    iBackupNumber = idwFingerIndex
                                    'axCZKEM1.RefreshData(iMachineNumber)
                                    'xxen = True
                                    'axCZKEM1.CardNumber(0) = icard
                                    Ret = axCZKEM1.SetStrCardNumber(icard)
                                    Ret = axCZKEM1.SSR_SetUserInfo(iMachineNumber, sdwEnrollNumber, sName, sPassword, iPrivilege, bEnabled)
                                    'Sleep(100)
                                    'TZS = "1:2:3"
                                    'Ret = axCZKEM1.SetUserTmpExStr(iMachineNumber, sdwEnrollNumber, 0, 0, "")
                                    ''Sleep (200)
                                    'If vRet = False Then
                                    '    Sleep(200)
                                    'Ret = axCZKEM1.SetUserTmpExStr(iMachineNumber, sdwEnrollNumber, iBackupNumber, 0, sTmpData)
                                    'End If
                                    If Ret = False Then
                                    End If
                                    'axCZKEM1.RefreshData(iMachineNumber)
                                Else
                                    Dim sName1, sPassword1, iPrivilege1, bEnabled1
                                    Ret = axCZKEM1.SSR_GetUserInfo(iMachineNumber, sdwEnrollNumber, sName1, sPassword1, iPrivilege1, bEnabled1)  'get the user info if user is already in device
                                    If Ret = True Then
                                        axCZKEM1.GetStrCardNumber(icard)
                                        If icard = "0" Then
                                            icard = ""
                                        End If
                                    End If
                                    Ret = axCZKEM1.SetStrCardNumber(icard)
                                    If axCZKEM1.SSR_SetUserInfo(iMachineNumber, sdwEnrollNumber, sName, sPassword, iPrivilege, bEnabled) Then 'upload user information to the device
                                        If idwFingerIndex = 50 Then
                                            axCZKEM1.SetUserFaceStr(iMachineNumber, sdwEnrollNumber, idwFingerIndex, sTmpData, sTmpData.Length)  'face
                                        Else
                                            axCZKEM1.SetUserTmpExStr(iMachineNumber, sdwEnrollNumber, idwFingerIndex, iflag, sTmpData) 'upload templates information to the device
                                        End If
                                    Else
                                        axCZKEM1.GetLastError(idwErrorCode)
                                        'MsgBox("Operation failed,ErrorCode=" & idwErrorCode.ToString(), MsgBoxStyle.Exclamation, "Error")
                                        'axCZKEM1.EnableDevice(iMachineNumber, True)
                                        'Cursor = Cursors.Default
                                        ' Return
                                        Continue For
                                    End If
                                End If
                            End If
                        Next
                        'End If
                        'axCZKEM1.BatchUpdate(iMachineNumber) 'upload all the information in the memory
                        axCZKEM1.RefreshData(iMachineNumber)
                        axCZKEM1.EnableDevice(iMachineNumber, True)
                        axCZKEM1.Disconnect()
                    Else
                        axCZKEM1.GetLastError(idwErrorCode)
                        failIP.Add(vpszIPAddress.ToString)
                        Continue For
                    End If
                ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "Bio2+" Then
                    Dim commkey As String = Convert.ToInt32(GridView1.GetRowCellValue(rowHandle, "commkey").ToString.Trim)
                    Dim deviceG2 As Riss.Devices.Device
                    Dim deviceConnectionG2 As Riss.Devices.DeviceConnection
                    deviceG2 = New Riss.Devices.Device()
                    deviceG2.DN = 1 'CInt(nud_DN.Value)
                    deviceG2.Password = commkey
                    deviceG2.Model = "ZDC2911"
                    deviceG2.ConnectionModel = 5
                    lpszIPAddress = GridView1.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim
                    If GridView1.GetRowCellValue(rowHandle, "A_R").ToString.Trim = "S" Then
                        deviceG2.CommunicationType = CommunicationType.Usb
                    Else
                        deviceG2.IpAddress = lpszIPAddress ' TextEdit2.Text.Trim()
                        deviceG2.IpPort = 5500 'CInt(nud_Port.Value)
                        deviceG2.CommunicationType = CommunicationType.Tcp
                    End If
                    deviceConnectionG2 = DeviceConnection.CreateConnection(deviceG2)
                    If deviceConnectionG2.Open() > 0 Then

                        'loop for fingerprint
                        Dim selectedRowsEmp As Integer() = GridView2.GetSelectedRows()
                        Dim resultEmp As Object() = New Object(selectedRowsEmp.Length - 1) {}
                        'If axCZKEM1.BeginBatchUpdate(iMachineNumber, 1) Then
                        For x As Integer = 0 To selectedRowsEmp.Length - 1
                            'LstEmployeesTarget.ListIndex = e
                            Dim rowHandleEmp As Long = selectedRowsEmp(x)
                            If Not GridView2.IsGroupRow(rowHandleEmp) Then
                                Dim adap As SqlDataAdapter
                                Dim adapA As OleDbDataAdapter
                                Dim RsFp As DataSet = New DataSet
                                Dim mUser_ID As String = GridView2.GetRowCellValue(rowHandleEmp, "EnrollNumber").ToString.Trim()
                                XtraMasterTest.LabelControlStatus.Text = "Uploading Template " & mUser_ID
                                XtraMasterTest.LabelControlCount.Text = "Uploading " & x + 1 & " of " & selectedRowsEmp.Length
                                Application.DoEvents()

                                Dim extraProperty As Object = New Object
                                Dim extraData As Object = New Object
                                extraData = False

                                'for userinformation 
                                Dim shareUser As User = New User()  'for userinformation 
                                shareUser.Enrolls = New List(Of Enroll)
                                Dim enroll As Enroll = New Enroll
                                shareUser.Enrolls.Add(enroll)

                                Dim enrollType As Integer = 0
                                shareUser.DIN = Convert.ToInt64(GridView2.GetRowCellValue(rowHandleEmp, "EnrollNumber").ToString.Trim()) ', UInt64)
                                If GridView2.GetRowCellValue(rowHandleEmp, "Privilege").ToString.Trim() = "4" Then
                                    shareUser.Privilege = 8 ' GridView2.GetRowCellValue(rowHandleEmp, "Privilege").ToString.Trim() ' GetPrivilege
                                Else
                                    shareUser.Privilege = 0
                                End If

                                shareUser.Enrolls(0).DIN = shareUser.DIN
                                shareUser.Enrolls(0).Password = GridView2.GetRowCellValue(rowHandleEmp, "Password").ToString.Trim() 'txt_Pwd.Text
                                enrollType = (enrollType + Zd2911Utils.SetBit(0, 10))
                                'password is 10, fp0-fp9, card is 11
                                If GridView2.GetRowCellValue(rowHandleEmp, "Cardnumber").ToString.Trim() <> "" Then
                                    shareUser.Enrolls(0).CardID = GridView2.GetRowCellValue(rowHandleEmp, "Cardnumber").ToString.Trim() 'txt_Card.Text
                                End If

                                enrollType = (enrollType + Zd2911Utils.SetBit(0, 11))
                                'password is 10, fp0-fp9, card is 11
                                shareUser.Enrolls(0).EnrollType = CType(enrollType, EnrollType)

                                Dim s As String = "select EMPNAME from TblEmployee where PRESENTCARDNO = '" & mUser_ID & "'"
                                Dim adapZ As SqlDataAdapter
                                Dim adapAZ As OleDbDataAdapter
                                'Dim dstmp As DataSet = New DataSet
                                'If Common.servername = "Access" Then
                                '    adapAZ = New OleDbDataAdapter(s, Common.con1)
                                '    adapAZ.Fill(dstmp)
                                'Else
                                '    adapZ = New SqlDataAdapter(s, Common.con)
                                '    adapZ.Fill(dstmp)
                                'End If
                                'If dstmp.Tables(0).Rows.Count > 0 Then
                                '    shareUser.UserName = dstmp.Tables(0).Rows(0).Item(0).ToString.Trim ' txt_UserName.Text
                                'Else
                                shareUser.UserName = GridView2.GetRowCellDisplayText(rowHandleEmp, "UserName").ToString.Trim()
                                'End If
                                shareUser.Comment = "" 'ExtInfoTextBox.Text
                                shareUser.Enable = True 'Convert.ToBoolean(userEnableComboBox.SelectedIndex)
                                shareUser.AttType = 0 'CType(userAttTypeIdNumericUpDown.Value, Integer)
                                shareUser.AccessControl = -1 'userAccessControlComboBox.SelectedIndex
                                shareUser.AccessTimeZone = 0 'CType(userPassZoneNumericUpDown.Value, Integer)
                                shareUser.Department = 0 'CType(userDeptIdNumericUpDown.Value, Integer)
                                shareUser.UserGroup = 0 'CType(userGroupIdNumericUpDown.Value, Integer)
                                shareUser.ValidityPeriod = False 'Convert.ToBoolean(userValidityPeriodComboBox.SelectedIndex)
                                'shareUser.ValidDate = userStartDateTimePicker.Value
                                'shareUser.InvalidDate = userEndDateTimePicker.Value
                                shareUser.Res = 0 ' CType(userResNumericUpDown.Value, UInteger)

                                Dim result1 As Boolean = deviceConnectionG2.SetProperty(UserProperty.Enroll, extraProperty, shareUser, extraData)
                                ' end for userinformation 

                                'for finger template
                                Dim extraPropertyfp As New Object()
                                Dim extraDatafp As New Object()
                                extraDatafp = ConvertObject.DeviceBusy

                                Try
                                    Dim result2 As Boolean = deviceConnectionG2.SetProperty(DeviceProperty.Enable, extraPropertyfp, deviceG2, extraDatafp)
                                    Dim user As New User()
                                    Dim enrollfp As New Enroll()
                                    user.DIN = shareUser.DIN
                                    If GridView2.GetRowCellValue(rowHandleEmp, "Privilege").ToString.Trim() = "4" Then
                                        user.Privilege = 8 ' GridView2.GetRowCellValue(rowHandleEmp, "Privilege").ToString.Trim() ' GetPrivilege
                                    Else
                                        user.Privilege = 0
                                    End If
                                    enrollfp.DIN = user.DIN
                                    enrollfp.EnrollType = DirectCast(GridView2.GetRowCellValue(rowHandleEmp, "FingerNumber"), EnrollType)
                                    Dim fingerprint As Byte() = New Byte(Zd2911Utils.MaxFingerprintLength * (GridView2.GetRowCellValue(rowHandleEmp, "FingerNumber") + 1) - 1) {}
                                    Dim fpBytes As Byte() = Common.HexToByte(GridView2.GetRowCellValue(rowHandleEmp, "Template").ToString.Trim)
                                    Array.Copy(fpBytes, 0, fingerprint, GridView2.GetRowCellValue(rowHandleEmp, "FingerNumber") * Zd2911Utils.MaxFingerprintLength, fpBytes.Length)
                                    enrollfp.Fingerprint = fingerprint
                                    user.Enrolls.Add(enrollfp)
                                    extraPropertyfp = UserEnrollCommand.WriteFingerprint
                                    result2 = deviceConnectionG2.SetProperty(UserProperty.UserEnroll, extraPropertyfp, user, extraDatafp)
                                    If result2 Then
                                        'MessageBox.Show("Write FP Data Success", "Prompt", MessageBoxButtons.OK, MessageBoxIcon.Information)
                                    Else
                                        'MessageBox.Show("Write FP Data Fail", "Prompt", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                                    End If
                                Catch ex As Exception
                                    'MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.[Error])
                                Finally
                                    extraDatafp = ConvertObject.DeviceIdle
                                    deviceConnectionG2.SetProperty(DeviceProperty.Enable, extraPropertyfp, deviceG2, extraDatafp)
                                End Try

                                'end for finger template
                            End If
                        Next

                        deviceConnectionG2.Close()
                    Else
                        'XtraMessageBox.Show(ulf, "<size=10>Connection Failed</size>", "Failed")
                        'Me.Cursor = Cursors.Default
                        'Exit Sub
                        failIP.Add(lpszIPAddress.ToString)
                        Continue For
                    End If
                ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "Bio1Eco" Then
                    'vpszIPAddress = GridView1.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim()
                    'XtraMasterTest.LabelControlStatus.Text = "Connecting " & vpszIPAddress & "..."
                    'Application.DoEvents()
                    Dim adap, adap1 As SqlDataAdapter
                    Dim adapA, adapA1 As OleDbDataAdapter
                    Dim ds, ds1 As DataSet
                    Dim commkey As Integer
                    commkey = Convert.ToInt32(GridView1.GetRowCellValue(rowHandle, GridView1.Columns("commkey")))
                    Dim mOpenFlag As Boolean
                    Dim mMK8001Device As Boolean 'TRUE:MK8001/8002Device;False:else

                    mMK8001Device = False
                    Dim si As SySystemInfo = New SySystemInfo
                    si.cSerialNum = New Byte((20) - 1) {}
                    si.cManuName = New Byte((24) - 1) {}
                    si.cDevName = New Byte((24) - 1) {}
                    si.cAlgVer = New Byte((16) - 1) {}
                    si.cFirmwareVer = New Byte((24) - 1) {}
                    Dim rret As Integer = 66
                    Try
                        rret = SyFunctions.CmdGetSystemInfo(si)
                    Catch ex As Exception

                    End Try

                    If (rret = CType(SyLastError.sleSuss, Int32)) Then
                        Dim sFirmwareVer As String = System.Text.Encoding.Default.GetString(si.cFirmwareVer)
                        If sFirmwareVer.Contains("MK8001/8002") Then
                            mMK8001Device = True
                        Else
                            mMK8001Device = False
                        End If
                    End If
                    Dim iFingerSize
                    If mMK8001Device Then
                        iFingerSize = 768
                    Else
                        iFingerSize = 512
                    End If
                    Dim ret As Int32 = CType(SyLastError.sleInvalidParam, Int32)
                    Dim equNo As UInt32 = Convert.ToUInt32(LstMachineId)
                    Dim gEquNo As UInt32 = equNo
                    If GridView1.GetRowCellValue(rowHandle, "A_R").ToString.Trim = "T" Then
                        Dim netCfg As SyNetCfg
                        Dim pswd As UInt32 = 0
                        Dim tmpPswd As String = commkey
                        If (tmpPswd = "0") Then
                            tmpPswd = "FFFFFFFF"
                        End If
                        Try
                            pswd = UInt32.Parse(tmpPswd, NumberStyles.HexNumber)
                        Catch ex As Exception
                            failIP.Add(vpszIPAddress.ToString)
                            'XtraMessageBox.Show(ulf, "<size=10>Incorrect Comm Key</size>", "Failed")
                            ret = CType(SyLastError.slePasswordError, Int32)
                            Continue For
                        End Try
                        If (ret <> CType(SyLastError.slePasswordError, Int32)) Then
                            netCfg.mIsTCP = 1
                            netCfg.mPortNo = 8000 'Convert.ToUInt16(txtPortNo.Text)
                            netCfg.mIPAddr = New Byte((4) - 1) {}
                            Dim sArray() As String = vpszIPAddress.Split(Microsoft.VisualBasic.ChrW(46))
                            Dim j As Byte = 0
                            For Each i1 As String In sArray
                                Try
                                    netCfg.mIPAddr(j) = Convert.ToByte(i1.Trim)
                                Catch ex As System.Exception
                                    netCfg.mIPAddr(j) = 255
                                End Try
                                j = (j + 1)
                                If j > 3 Then
                                    Exit For
                                End If

                            Next
                        End If

                        Dim pnt As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(netCfg))
                        Try
                            Marshal.StructureToPtr(netCfg, pnt, False)
                            '        If optWifiDevice.Checked Then
                            'ret = SyFunctions.CmdOpenDevice(3, pnt, equNo, pswd, Convert.ToByte(chkTranceIO.Checked?1, :, 0))
                            '        Else
                            ret = SyFunctions.CmdOpenDevice(2, pnt, equNo, pswd, Convert.ToByte(1))
                            '        End If
                            '                'TODO: Warning!!!, inline IF is not supported ?
                            '                chkTranceIO.Checked()
                            '0:
                            '                '
                        Finally
                            Marshal.FreeHGlobal(pnt)
                        End Try
                    ElseIf GridView1.GetRowCellValue(rowHandle, "A_R").ToString.Trim = "S" Then
                        Dim equtype As String = "Finger Module USB Device"
                        Dim pswd As UInt32 = UInt32.Parse("FFFFFFFF", NumberStyles.HexNumber)
                        ret = SyFunctions.CmdOpenDeviceByUsb(equtype, equNo, pswd, Convert.ToByte(1))
                    End If
                    If (ret = CType(SyLastError.sleSuss, Int32)) Then
                        ret = SyFunctions.CmdTestConn2Device

                        If Common.servername = "Access" Then
                            If Common.con1.State <> System.Data.ConnectionState.Open Then
                                Common.con1.Open()
                            End If
                        Else
                            If Common.con.State <> System.Data.ConnectionState.Open Then
                                Common.con.Open()
                            End If
                        End If


                        Dim selectedRowsEmp As Integer() = GridView2.GetSelectedRows()
                        Dim resultEmp As Object() = New Object(selectedRowsEmp.Length - 1) {}
                        For x As Integer = 0 To selectedRowsEmp.Length - 1
                            Dim rowHandleEmp As Long = selectedRowsEmp(x)
                            If Not GridView2.IsGroupRow(rowHandleEmp) Then
                                If GridView2.GetRowCellValue(rowHandleEmp, "DeviceType").ToString.Trim() = "Bio1Eco" Then
                                    XtraMasterTest.LabelControlStatus.Text = "Uploading Template " & GridView2.GetRowCellValue(rowHandleEmp, "EnrollNumber").ToString().Trim
                                    XtraMasterTest.LabelControlCount.Text = "Uploading " & x + 1 & " of " & selectedRowsEmp.Length
                                    Application.DoEvents()
                                    Dim uie As SyUserInfoExt = New SyUserInfoExt
                                    Dim t1 As Byte = 0, t2 = 0, t3 = 0, t5 = 0, kind = 0
                                    uie.dwCardID = 0
                                    uie.dwPws = 0
                                    uie.sUserInfo.name = GridView2.GetRowCellDisplayText(rowHandleEmp, "UserName").ToString.Trim()
                                    uie.sUserInfo.cValidTime = New Byte((5) - 1) {}
                                    uie.sUserInfo.MbIndex = New System.UInt16((5) - 1) {}
                                    Dim sdwEnrollNumber
                                    If IsNumeric(GridView2.GetRowCellValue(rowHandleEmp, "EnrollNumber").ToString().Trim) = True Then
                                        sdwEnrollNumber = Convert.ToInt64(GridView2.GetRowCellValue(rowHandleEmp, "EnrollNumber").ToString().Trim)
                                    Else
                                        sdwEnrollNumber = GridView2.GetRowCellValue(rowHandleEmp, "EnrollNumber").ToString().Trim
                                    End If

                                    uie.sUserInfo.wUserID = Convert.ToUInt16(sdwEnrollNumber)
                                    uie.sUserInfo.cVerifyType = Convert.ToByte(0)
                                    'uie.sUserInfo.sUserType = CType(1, Byte) 'privilege
                                    If ToggleMakeAdmin.IsOn = True Then
                                        uie.sUserInfo.sUserType = CType(1, Byte) 'privilege                                       
                                    Else
                                        uie.sUserInfo.sUserType = CType(GridView2.GetRowCellValue(rowHandleEmp, "Privilege").ToString().Trim, Byte) 'privilege                                       
                                    End If

                                    Try
                                        uie.sUserInfo.cValidTime(0) = Convert.ToByte("")
                                    Catch ex As Exception

                                    End Try

                                    Try
                                        uie.sUserInfo.cValidTime(1) = Convert.ToByte("")
                                    Catch ex As Exception

                                    End Try

                                    Try
                                        uie.sUserInfo.cValidTime(2) = Convert.ToByte("")
                                    Catch ex As Exception

                                    End Try

                                    Try
                                        uie.dwCardID = Convert.ToInt32(GridView2.GetRowCellValue(rowHandleEmp, "Cardnumber").ToString().Trim)
                                    Catch ex As Exception
                                    End Try

                                    Try
                                        uie.dwPws = Convert.ToInt32(GridView2.GetRowCellValue(rowHandleEmp, "Password").ToString().Trim)
                                    Catch ex As Exception
                                    End Try

                                    Dim strEnrollData As String = GridView2.GetRowCellValue(rowHandleEmp, "Template").ToString().Trim
                                    'If strEnrollData <> "" Then
                                    Dim ft As SyFingerTemplate = New SyFingerTemplate
                                    Dim ftEx As SyFingerTemplateEx = New SyFingerTemplateEx
                                    If (iFingerSize = 768) Then
                                        ftEx.data = New Byte((768) - 1) {}
                                        For n As Integer = 0 To 768 - 1
                                            If (strEnrollData.Length >= (n + 1) * 2) Then
                                                ftEx.data(n) = Convert.ToByte(strEnrollData.Substring(n * 2, 2), 16)
                                            Else
                                                ftEx.data(n) = 0
                                            End If
                                        Next
                                        ret = SyFunctions.CmdSetUserFingerPrint(ftEx, CType(uie.sUserInfo.wUserID, System.UInt16), CType((1), System.UInt16))
                                    Else
                                        ft.data = New Byte((512) - 1) {}
                                        For n As Integer = 0 To 512 - 1
                                            If (strEnrollData.Length >= (n + 1) * 2) Then
                                                ft.data(n) = Convert.ToByte(strEnrollData.Substring(n * 2, 2), 16)
                                            Else
                                                ft.data(n) = 0
                                            End If
                                        Next
                                        ret = SyFunctions.CmdSetUserFingerPrint(ft, CType(uie.sUserInfo.wUserID, System.UInt16), CType((1), System.UInt16))
                                    End If
                                    'End If
                                    't1 = CType(lstEnrollData.Items.Count, Byte)
                                    t1 = CType(1, Byte)
                                    t2 = 0
                                    If (uie.dwPws > 0) Then
                                        t2 = 1
                                        kind = 2
                                    End If

                                    If (uie.dwCardID > 0) Then
                                        t3 = 1
                                        kind = 3
                                    End If

                                    t5 = 3
                                    'If (lstEnrollData.Items.Count < 1) Then
                                    '    ret = SyFunctions.CmdReg(uie, CType(kind, Byte))
                                    '    Thread.Sleep(50)
                                    'End If

                                    uie.sUserInfo.sEnrollStatus = CType((t1 _
                                                Or ((t2 * 16) _
                                                Or ((t3 * 32) _
                                                Or (t5 * 64)))), Byte)
                                    ret = SyFunctions.CmdSetUserInfo(uie, uie.sUserInfo.wUserID)
                                    If (ret = CType(SyLastError.sleSuss, Int32)) Then
                                        'lblMessage.Text = "SetEnrollData OK"
                                    Else
                                        'lblMessage.Text = util.ErrorPrint(ret)
                                    End If
                                End If
                            End If
                        Next
                        'End loop for fingerprint
conclose:               If Common.servername = "Access" Then
                            If Common.con1.State <> System.Data.ConnectionState.Closed Then
                                Common.con1.Close()
                            End If
                        Else
                            If Common.con.State <> System.Data.ConnectionState.Closed Then
                                Common.con.Close()
                            End If
                        End If


                        SyFunctions.CloseCom()
                        ' close com 
                        SyFunctions.CmdUnLockDevice()
                        SyFunctions.CloseCom()
                    Else
                        'XtraMessageBox.Show(ulf, "<size=10>Connection Failed</size>", "Failed")
                        failIP.Add(vpszIPAddress.ToString)
                        Continue For
                    End If

                ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "EF45" Then
                    'lpszIPAddress = GridView1.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim 'Trim(rstm!Location)
                    'vpszIPAddress = Trim(lpszIPAddress)
                    'XtraMasterTest.LabelControlStatus.Text = "Connecting " & vpszIPAddress & "..."
                    'Application.DoEvents()
                    Dim cn As Common = New Common
                    Dim _client As Client = Nothing
                    Dim connectOk As Boolean = False
                    Try
                        _client = cn.initClientEF45(vpszIPAddress)
                        connectOk = _client.StealConnect '_client.Connect()
                        If connectOk Then
                            ' loop for fingerprint
                            Dim selectedRowsEmp As Integer() = GridView2.GetSelectedRows()
                            Dim resultEmp As Object() = New Object(selectedRowsEmp.Length - 1) {}
                            For x As Integer = 0 To selectedRowsEmp.Length - 1
                                Dim rowHandleEmp As Long = selectedRowsEmp(x)
                                If Not GridView2.IsGroupRow(rowHandleEmp) Then
                                    If GridView2.GetRowCellValue(rowHandleEmp, "DeviceType").ToString.Trim() = "EF45" Then
                                        XtraMasterTest.LabelControlStatus.Text = "Uploading Template " & GridView2.GetRowCellValue(rowHandleEmp, "EnrollNumber").ToString().Trim
                                        XtraMasterTest.LabelControlCount.Text = "Uploading " & x + 1 & " of " & selectedRowsEmp.Length
                                        Application.DoEvents()
                                        Dim sdwEnrollNumber
                                        If IsNumeric(GridView2.GetRowCellValue(rowHandleEmp, "EnrollNumber").ToString().Trim) = True Then
                                            sdwEnrollNumber = Convert.ToInt64(GridView2.GetRowCellValue(rowHandleEmp, "EnrollNumber").ToString().Trim)
                                        Else
                                            sdwEnrollNumber = GridView2.GetRowCellValue(rowHandleEmp, "EnrollNumber").ToString().Trim
                                        End If

                                        Dim adap As SqlDataAdapter
                                        Dim adapA As OleDbDataAdapter
                                        Dim Rs As DataSet = New DataSet
                                        sSql = "select LeftEye,RightEye from fptable where EnrollNumber='" & GridView2.GetRowCellValue(rowHandleEmp, "EnrollNumber").ToString().Trim & "' and EMachineNumber=" & LstMachineId & " and DeviceType='EF45'"
                                        If Common.servername = "Access" Then
                                            adapA = New OleDbDataAdapter(sSql, Common.con1)
                                            adapA.Fill(Rs)
                                        Else
                                            adap = New SqlDataAdapter(sSql, Common.con)
                                            adap.Fill(Rs)
                                        End If
                                        Dim LeftEyeTemplate() As Byte
                                        Dim RightEyeTemplate() As Byte
                                        Dim subject As CMITech.UMXClient.Entities.Subject
                                        Dim userInfo As Entities.UserInfo = New Entities.UserInfo()
                                        Dim admin As Integer
                                        If ToggleMakeAdmin.IsOn = True Then
                                            admin = 1
                                        Else
                                            admin = GridView2.GetRowCellValue(rowHandleEmp, "Privilege").ToString().Trim
                                        End If
                                        Try
                                            subject = _client.GetSubject(sdwEnrollNumber) 'check for duplicate 
                                            'important to rite this part in both cases add and update 
                                            'becayse _client.GetSubject("9999") will overrite all values
                                            Try
                                                Dim Template As CMITech.UMXClient.Entities.EnrolTemplate '= New CMITech.UMXClient.Entities.EnrolTemplate(Rs.Tables(0).Rows(0).Item("LeftEye"), Rs.Tables(0).Rows(0).Item("RightEye"))
                                                subject = New CMITech.UMXClient.Entities.Subject
                                                If GridView2.GetRowCellValue(rowHandleEmp, "FingerNumber").ToString.Trim() = "99" Then
                                                    If Rs.Tables(0).Rows(0).Item("RightEye") Is DBNull.Value Then
                                                        Template = New CMITech.UMXClient.Entities.EnrolTemplate(Rs.Tables(0).Rows(0).Item("LeftEye"), Nothing)
                                                    ElseIf Rs.Tables(0).Rows(0).Item("LeftEye") Is DBNull.Value Then
                                                        Template = New CMITech.UMXClient.Entities.EnrolTemplate(Nothing, Rs.Tables(0).Rows(0).Item("RightEye"))
                                                    Else
                                                        Template = New CMITech.UMXClient.Entities.EnrolTemplate(Rs.Tables(0).Rows(0).Item("LeftEye"), Rs.Tables(0).Rows(0).Item("RightEye"))
                                                    End If
                                                    subject.EnrolTemplate = Template
                                                End If
                                                subject.AccessAllowed = True
                                                subject.LastName = GridView2.GetRowCellDisplayText(rowHandleEmp, "UserName").ToString.Trim()
                                                subject.SubjectUID = sdwEnrollNumber
                                                subject.MatchUntil = ""
                                                subject.FirstName = ""
                                                subject.WiegandCustom = "Iamtestingnow"
                                                subject.WiegandCode = -1
                                                subject.WiegandFacility = -1
                                                subject.WiegandSite = -1
                                                _client.UpdateSubject(subject)  'update if exists
                                            Catch ex As Exception

                                            End Try
                                            Try
                                                userInfo.UUID = sdwEnrollNumber
                                                userInfo.Card = GridView2.GetRowCellValue(rowHandleEmp, "Cardnumber").ToString().Trim ' /* Card CSN : 9A 99 1F 3E ...*/
                                                userInfo.Pin = GridView2.GetRowCellValue(rowHandleEmp, "Password").ToString().Trim
                                                userInfo.Admin = admin
                                                userInfo.GroupIndex = 0
                                                userInfo.ByPassCard = 0
                                                userInfo.Indivisual = 0
                                                userInfo.ThreeOutStatus = 0
                                                userInfo.ThreeOutAccessAllowed = 1
                                                _client.UpdateUserInfoByUUID(userInfo) 'update if exists
                                            Catch ex As Exception

                                            End Try
                                        Catch ex As Exception
                                            If ex.Message.ToString = "Subject " & sdwEnrollNumber & " does not exist" Then
                                                Try
                                                    Dim Template As CMITech.UMXClient.Entities.EnrolTemplate '= New CMITech.UMXClient.Entities.EnrolTemplate(Rs.Tables(0).Rows(0).Item("LeftEye"), Rs.Tables(0).Rows(0).Item("RightEye"))
                                                    subject = New CMITech.UMXClient.Entities.Subject
                                                    If GridView2.GetRowCellValue(rowHandleEmp, "FingerNumber").ToString.Trim() = "99" Then
                                                        If Rs.Tables(0).Rows(0).Item("RightEye") Is DBNull.Value Then
                                                            Template = New CMITech.UMXClient.Entities.EnrolTemplate(Rs.Tables(0).Rows(0).Item("LeftEye"), Nothing)
                                                        ElseIf Rs.Tables(0).Rows(0).Item("LeftEye") Is DBNull.Value Then
                                                            Template = New CMITech.UMXClient.Entities.EnrolTemplate(Nothing, Rs.Tables(0).Rows(0).Item("RightEye"))
                                                        Else
                                                            Template = New CMITech.UMXClient.Entities.EnrolTemplate(Rs.Tables(0).Rows(0).Item("LeftEye"), Rs.Tables(0).Rows(0).Item("RightEye"))
                                                        End If
                                                        subject.EnrolTemplate = Template
                                                    End If
                                                    subject.AccessAllowed = True
                                                    subject.LastName = GridView2.GetRowCellDisplayText(rowHandleEmp, "UserName").ToString.Trim()
                                                    subject.SubjectUID = sdwEnrollNumber
                                                    subject.MatchUntil = ""
                                                    subject.FirstName = ""
                                                    subject.WiegandCustom = "Iamtestingnow"
                                                    subject.WiegandCode = -1
                                                    subject.WiegandFacility = -1
                                                    subject.WiegandSite = -1
                                                    _client.AddSubject(subject) 'add if not exists
                                                Catch ex1 As Exception
                                                End Try
                                                Try
                                                    userInfo.UUID = sdwEnrollNumber
                                                    userInfo.Card = GridView2.GetRowCellValue(rowHandleEmp, "Cardnumber").ToString().Trim ' /* Card CSN : 9A 99 1F 3E ...*/
                                                    userInfo.Pin = GridView2.GetRowCellValue(rowHandleEmp, "Password").ToString().Trim
                                                    userInfo.Admin = admin
                                                    userInfo.GroupIndex = 0
                                                    userInfo.ByPassCard = 0
                                                    userInfo.Indivisual = 0
                                                    userInfo.ThreeOutStatus = 0
                                                    userInfo.ThreeOutAccessAllowed = 1
                                                    _client.AddUserInfo(userInfo) 'add if not exists 'to save user info in device. needed while adding new user
                                                Catch ex1 As Exception

                                                End Try
                                            End If
                                        End Try
                                    End If
                                End If
                            Next
                            'End loop for fingerprint

                            _client.Disconnect()
                        Else
                            'XtraMessageBox.Show(ulf, "<size=10>Connection Failed</size>", "Failed")
                            'Me.Cursor = Cursors.Default
                            'Exit Sub
                            failIP.Add(vpszIPAddress.ToString)
                            Continue For
                        End If
                    Catch ex As Exception
                        'XtraMessageBox.Show(ulf, "<size=10>Connection Failed</size>", "Failed")
                        'Me.Cursor = Cursors.Default
                        'Exit Sub
                        Continue For
                    End Try
                ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "F9" Then
                    sSql = "select * from tblmachine where ID_NO='" & LstMachineId & "'"
                    Dim bConn As Boolean = False
                    vnMachineNumber = LstMachineId
                    vnLicense = 7881 '1789 '                    
                    vpszNetPort = CLng("5005")
                    vpszNetPassword = CLng("0")
                    vnTimeOut = CLng("5000")
                    vnProtocolType = 0 'PROTOCOL_TCPIP
                    Dim F9 As mdlPublic_f9 = New mdlPublic_f9
                    'bConn = FK623Attend.ConnectNet(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
                    If GridView1.GetRowCellValue(rowHandle, "A_R").ToString.Trim = "S" Then
                        vRet = F9.FK_ConnectUSB(vnMachineNumber, vnLicense)
                    Else
                        vpszIPAddress = Trim(lpszIPAddress)
                        vpszNetPort = CLng("5005")
                        vpszNetPassword = CLng("0")
                        vnTimeOut = CLng("5000")
                        vnProtocolType = 0
                        vRet = F9.FK_ConnectNet(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
                    End If
                    If vRet > 0 Then
                        bConn = True
                    Else
                        bConn = False
                    End If
                    If bConn Then
                        Dim isStringId As Boolean
                        If F9.FK_GetIsSupportStringID(vRet) = RUN_SUCCESS Then
                            isStringId = True
                        Else
                            isStringId = False
                        End If
                        Dim selectedRowsEmp As Integer() = GridView2.GetSelectedRows()
                        Dim resultEmp As Object() = New Object(selectedRowsEmp.Length - 1) {}
                        For x As Integer = 0 To selectedRowsEmp.Length - 1
                            'LstEmployeesTarget.ListIndex = e
                            Dim rowHandleEmp As Long = selectedRowsEmp(x)
                            If Not GridView2.IsGroupRow(rowHandleEmp) Then
                                Dim EnrollNumber As String = GridView2.GetRowCellValue(rowHandleEmp, "EnrollNumber").ToString.Trim
                                Dim FingerNumber As Long = GridView2.GetRowCellValue(rowHandleEmp, "FingerNumber").ToString.Trim
                                If GridView2.GetRowCellValue(rowHandleEmp, "DeviceType").ToString.Trim = "F9" Then
                                    XtraMasterTest.LabelControlStatus.Text = "Uploading Template " & EnrollNumber
                                    XtraMasterTest.LabelControlCount.Text = "Uploading " & x + 1 & " of " & selectedRowsEmp.Length
                                    Application.DoEvents()
                                    Dim LstMachineIdDest As String = GridView2.GetRowCellValue(rowHandleEmp, "EMachineNumber").ToString.Trim()
                                    cmdSetAllEnrollData_F9_All(vnMachineNumber, EnrollNumber, FingerNumber, LstMachineIdDest, isStringId, F9, vRet)
                                End If
                            End If
                        Next x
                    Else
                        'MsgBox("Device No: " & LstMachineId & " Not connected..")
                        'XtraMessageBox.Show(ulf, "<size=10>Device No: " & LstMachineId & " Not connected..</size>", "Information")
                        failIP.Add(vpszIPAddress.ToString)
                        Continue For
                    End If
                    'FK623Attend.Disconnect
                    FK_DisConnect(vRet)
                ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "ATF686n" Then
                    vpszNetPort = CLng("5005")
                    vpszNetPassword = CLng("0")
                    vnTimeOut = CLng("5000")
                    vnProtocolType = 0
                    vnLicense = 7881
                    Dim atf686n As mdlFunction_Atf686n = New mdlFunction_Atf686n
                    If GridView1.GetRowCellValue(rowHandle, "A_R").ToString.Trim = "S" Then
                        vnResult = atf686n.ST_ConnectUSB(GridView1.GetRowCellValue(rowHandle, "ID_NO").ToString.Trim, vnLicense)
                    Else
                        vnResult = atf686n.ConnectNet(vpszIPAddress)
                    End If
                    Dim vnResultCode As Integer = atf686n.ST_EnableDevice(vnResult, 0)
                        If vnResultCode <> RUN_SUCCESS Then
                        atf686n.ST_DisConnect(vnResult)
                        'Return gstrNoDevice
                        Continue For
                    End If

                    Dim selectedRowsEmp As Integer() = GridView2.GetSelectedRows()
                    Dim resultEmp As Object() = New Object(selectedRowsEmp.Length - 1) {}
                    For x As Integer = 0 To selectedRowsEmp.Length - 1
                        'LstEmployeesTarget.ListIndex = e
                        Dim rowHandleEmp As Long = selectedRowsEmp(x)
                        If Not GridView2.IsGroupRow(rowHandleEmp) Then
                            Dim EnrollNumber As String = GridView2.GetRowCellValue(rowHandleEmp, "EnrollNumber").ToString.Trim
                            Dim FingerNumber As Long = GridView2.GetRowCellValue(rowHandleEmp, "FingerNumber").ToString.Trim
                            Dim cardnumber As String = GridView2.GetRowCellValue(rowHandleEmp, "Cardnumber").ToString.Trim
                            Dim Password As String = GridView2.GetRowCellValue(rowHandleEmp, "Password").ToString.Trim
                            If GridView2.GetRowCellValue(rowHandleEmp, "DeviceType").ToString.Trim = "ATF686n" Then
                                XtraMasterTest.LabelControlStatus.Text = "Uploading Template " & EnrollNumber
                                XtraMasterTest.LabelControlCount.Text = "Uploading " & x + 1 & " of " & selectedRowsEmp.Length
                                Application.DoEvents()
                                Dim LstMachineIdDest As String = GridView2.GetRowCellValue(rowHandleEmp, "EMachineNumber").ToString.Trim()

                                Dim vStrEnrollNumber As String
                                vPrivilege = GridView2.GetRowCellValue(rowHandleEmp, "Privilege").ToString.Trim
                                Dim szUsername As String = GridView2.GetRowCellValue(rowHandleEmp, "UserName").ToString.Trim

                                Dim vnInfoSize As Integer = Marshal.SizeOf(GetType(ENROLL_DATA_STRING_ID))
                                Dim bytEnrollData(vnInfoSize - 1) As Byte
                                Dim bytGetEnrollData(vnInfoSize - 1) As Byte
                                vStrEnrollNumber = EnrollNumber
                                If IsNumeric(EnrollNumber) Then
                                    vStrEnrollNumber = Convert.ToDouble(EnrollNumber)
                                End If


                                If vPrivilege = 1 Then
                                    vPrivilege = CInt(enumMachinePrivilege.MP_ALL)
                                ElseIf vPrivilege = 0 Then
                                    vPrivilege = CInt(enumMachinePrivilege.MP_NONE)
                                ElseIf vPrivilege = 2 Then
                                    vPrivilege = CInt(enumMachinePrivilege.MP_ROLE1)
                                ElseIf vPrivilege = 3 Then
                                    vPrivilege = CInt(enumMachinePrivilege.MP_ROLE2)
                                ElseIf vPrivilege = 4 Then
                                    vPrivilege = CInt(enumMachinePrivilege.MP_ROLE3)
                                ElseIf vPrivilege = 5 Then
                                    vPrivilege = CInt(enumMachinePrivilege.MP_ROLE4)
                                ElseIf vPrivilege = 6 Then
                                    vPrivilege = CInt(enumMachinePrivilege.MP_ROLE5)
                                End If

                                Dim mEnrollInfo As ENROLL_DATA_STRING_ID
                                If atf686n.ST_GetIsSupportStringID(vnResult) = CInt(enumErrorCode.RETURN_SUCCESS) Then
                                    mEnrollInfo.Init()
                                    Try
                                        mEnrollInfo.Card = Convert.ToUInt32(cardnumber)
                                    Catch
                                        mEnrollInfo.Card = 0
                                    End Try
                                    Try
                                        mEnrollInfo.Password = Convert.ToUInt32(Password)
                                    Catch
                                        mEnrollInfo.Password = 0
                                    End Try
                                    mEnrollInfo.Privilege = Convert.ToByte(vPrivilege)
                                    StringToByteArrayUtf16(mEnrollInfo.UserName, szUsername) ' convert string to byte array as locale setting ()

                                    'ConvertStructureToByteArray(DirectCast(mEnrollInfo, Object), bytEnrollData)
                                    ConvertStructureToByteArray_ATF686n(DirectCast(mEnrollInfo, Object), bytEnrollData)
                                    atf686n.ST_GetEnrollMainData_SID(vnResult, vStrEnrollNumber, bytGetEnrollData) 'never delete
                                    vnResultCode = atf686n.ST_SetEnrollMainData_SID(vnResult, vStrEnrollNumber, bytEnrollData)

                                    If FingerNumber <> "20" And FingerNumber <> "21" Then
                                        ' Set Enroll Data ===================================================================================
                                        If vnResultCode = CInt(enumErrorCode.RETURN_SUCCESS) Then
                                            Dim vBackupNumber As Integer = 0
                                            Do While vBackupNumber <= CInt(enumBackupNumberType.BACKUP_FACE)
                                                If vBackupNumber = CInt(enumBackupNumberType.BACKUP_PSW) OrElse vBackupNumber = CInt(enumBackupNumberType.BACKUP_CARD) Then
                                                    vBackupNumber += 1
                                                    Continue Do
                                                End If
                                                Application.DoEvents()
                                                'Dim vstrFind As String = "select count(*) from EnrollTable where EnrollId='" & vStrEnrollNumber & "' and BackupIndex=" & vBackupNumber.ToString()
                                                'If mAdoRstTmp.State = CInt(ADODB.ObjectStateEnum.adStateOpen) Then
                                                '    mAdoRstTmp.Close()
                                                'End If
                                                'mAdoRstTmp.Open(vstrFind, mAdoConnEnroll, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockPessimistic, -1)
                                                'Dim vnCount As Integer = 0
                                                'Try
                                                '    vnCount = Convert.ToInt32(mAdoRstTmp.Fields(0).Value)
                                                'Catch
                                                '    vnCount = 0
                                                'End Try
                                                'mAdoRstTmp.Close()
                                                'If vnCount > 0 Then
                                                Dim VFACE_DATA_SIZE As Integer = 40080
                                                Dim mbytCurEnrollData(VFACE_DATA_SIZE - 1) As Byte
                                                readEnrollDataFromDBByID(vStrEnrollNumber, FingerNumber, GridView2.GetRowCellValue(rowHandleEmp, "Template_Tw"), mbytCurEnrollData)
                                                Dim mnCurPassword As Integer = 0
                                                vnResultCode = atf686n.ST_SetEnrollData_SID(vnResult, vStrEnrollNumber, FingerNumber, vPrivilege, mbytCurEnrollData, mnCurPassword)
                                                If vnResultCode <> CInt(enumErrorCode.RETURN_SUCCESS) Then
                                                    Continue Do
                                                End If
                                                'End If
                                                vBackupNumber += 1
                                            Loop

                                        End If
                                        '===================================================================================                                    
                                        '}
                                    End If
                                End If

                                'if (vnResultCode != (int)enumErrorCode.RETURN_SUCCESS)


                            End If
                        End If
                    Next
                    atf686n.ST_EnableDevice(vnResult, 1)
                    atf686n.ST_DisConnect(vnResult)
                ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "TF-01" Then
                    Dim sdwEnrollNumber As String
                    Dim sName As String = ""
                    Dim sPassword As String = ""
                    Dim iPrivilege As Integer
                    Dim idwFingerIndex As Integer
                    Dim sTmpData As String = ""
                    Dim sEnabled As String = ""
                    Dim bEnabled As Boolean = False
                    Dim iflag As Integer
                    Dim commkey As Integer
                    '   commkey = Convert.ToInt32(GridView1.GetRowCellValue(rowHandle, "commkey").ToString.Trim)
                    Dim bIsConnected = False
                    Dim iMachineNumber As Integer = 1 ' result(i)
                    Dim idwErrorCode As Integer
                    Dim com As Common = New Common
                    Dim bRet As Boolean
                    Dim axCZKEM1 As New zkemkeeper.CZKEM
                    ' Me.AxFP_CLOCK1.OpenCommPort(LstMachineId)
                    bIsConnected = AxFP_CLOCK1.SetIPAddress(vpszIPAddress, 5005, 0)
                    Me.AxFP_CLOCK1.OpenCommPort(LstMachineId)
                    If bIsConnected = True Then


                        Dim LstMachineIdDest As String
                        LstMachineIdDest = LstMachineId

                        'EnbResult = AxFP_CLOCK1.EnableDevice(LstMachineIdDest, 0)

                        Dim selectedRowsEmp As Integer() = GridView2.GetSelectedRows()
                        Dim resultEmp As Object() = New Object(selectedRowsEmp.Length - 1) {}
                        For x As Integer = 0 To selectedRowsEmp.Length - 1
                            'LstEmployeesTarget.ListIndex = e
                            Dim rowHandleEmp As Long = selectedRowsEmp(x)
                            If Not GridView2.IsGroupRow(rowHandleEmp) Then
                                Dim EnrollNumber As String = GridView2.GetRowCellValue(rowHandleEmp, "EnrollNumber").ToString.Trim
                                Dim FingerNumber As Long = GridView2.GetRowCellValue(rowHandleEmp, "FingerNumber").ToString.Trim
                                If GridView2.GetRowCellValue(rowHandleEmp, "DeviceType").ToString.Trim = "TF-01" Then
                                    XtraMasterTest.LabelControlStatus.Text = "Uploading Template " & EnrollNumber
                                    XtraMasterTest.LabelControlCount.Text = "Uploading " & x + 1 & " of " & selectedRowsEmp.Length
                                    Application.DoEvents()

                                    cmdSetAllEnrollData_TF01_All(vnMachineNumber, EnrollNumber, FingerNumber, LstMachineIdDest)
                                End If
                            End If
                        Next x
                        'End If
                        'axCZKEM1.BatchUpdate(iMachineNumber) 'upload all the information in the memory

                        AxFP_CLOCK1.EnableDevice(LstMachineIdDest, 1)
                        AxFP_CLOCK1.CloseCommPort()
                    Else
                        failIP.Add(vpszIPAddress.ToString)
                        Continue For
                    End If

                    ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "Ultra800" Then

                    If license.LicenseKey <> "" Then
                        If license.DeviceSerialNo.Contains(GridView1.GetRowCellValue(rowHandle, GridView1.Columns("MAC_ADDRESS")).ToString.Trim) Then
                        Else
                            XtraMessageBox.Show(ulf, "<size=10>Invalid Serial number " & GridView1.GetRowCellValue(rowHandle, GridView1.Columns("MAC_ADDRESS")).ToString.Trim & "</size>", "Failed")
                            Continue For
                        End If
                    End If

                    Dim sdwEnrollNumber As String
                        Dim sName As String = ""
                        Dim sPassword As String = ""
                        Dim iPrivilege As Integer
                        Dim idwFingerIndex As Integer
                        Dim sTmpData As String = ""
                        Dim sEnabled As String = ""
                        Dim bEnabled As Boolean = False
                        Dim iflag As Integer
                        Dim commkey As String
                        commkey = GridView1.GetRowCellValue(rowHandle, "commkey").ToString.Trim
                        'loop for fingerprint
                        Dim selectedRowsEmp As Integer() = GridView2.GetSelectedRows()
                    Dim resultEmp As Object() = New Object(selectedRowsEmp.Length - 1) {}
                    'If axCZKEM1.BeginBatchUpdate(iMachineNumber, 1) Then
                    For x As Integer = 0 To selectedRowsEmp.Length - 1
                        'LstEmployeesTarget.ListIndex = e
                        Dim rowHandleEmp As Long = selectedRowsEmp(x)
                        If Not GridView2.IsGroupRow(rowHandleEmp) Then
                            If GridView2.GetRowCellValue(rowHandleEmp, "DeviceType").ToString().Trim() <> "Ultra800" Then
                                Continue For
                            End If
                            Dim mUser_ID As String = GridView2.GetRowCellValue(rowHandleEmp, "EnrollNumber").ToString.Trim()
                            XtraMasterTest.LabelControlStatus.Text = "Uploading Template " & mUser_ID
                            XtraMasterTest.LabelControlCount.Text = "Uploading " & x + 1 & " of " & selectedRowsEmp.Length
                            Application.DoEvents()
                            Dim FpNo As String = GridView2.GetRowCellValue(rowHandleEmp, "FingerNumber").ToString.Trim()
                            Dim LstMachineIdDest As String = GridView2.GetRowCellValue(rowHandleEmp, "EMachineNumber").ToString.Trim()
                            If IsNumeric(mUser_ID) = True Then
                                sdwEnrollNumber = Convert.ToDouble(mUser_ID)
                            Else
                                sdwEnrollNumber = mUser_ID
                            End If
                            sName = GridView2.GetRowCellDisplayText(rowHandleEmp, "UserName").ToString.Trim()
                            idwFingerIndex = FpNo ' Convert.ToInt32(RsFp.Tables(0).Rows(0)("FingerNumber").ToString().Trim())
                            sTmpData = GridView2.GetRowCellValue(rowHandleEmp, "Template").ToString().Trim()
                            iPrivilege = Convert.ToInt32(GridView2.GetRowCellValue(rowHandleEmp, "Privilege").ToString().Trim()) 'told by ajitesh(used for admin)
                            If ToggleMakeAdmin.IsOn = True Then
                                iPrivilege = 1
                            Else
                                iPrivilege = GridView2.GetRowCellValue(rowHandleEmp, "Privilege").ToString.Trim ' !privilege
                            End If
                            sPassword = GridView2.GetRowCellValue(rowHandleEmp, "Password").ToString().Trim()
                            sEnabled = "true" 'RsFp.Tables(0).Rows(0)(6).ToString().Trim()
                            iflag = 0 'Convert.ToInt32(RsFp.Tables(0).Rows(0)(7).ToString())' as per old s/w
                            Dim icard As String = GridView2.GetRowCellValue(rowHandleEmp, "Cardnumber").ToString().Trim()

                            If sEnabled.ToString().ToLower() = "true" Then
                                bEnabled = True
                            Else
                                bEnabled = False
                            End If

                            Dim UA As Ulra800UserUpload = New Ulra800UserUpload
                            UA.pass = commkey
                            UA.person.id = sdwEnrollNumber
                            UA.person.name = sName
                            UA.person.idcardNum = icard
                            'UA.person.password = sPassword
                            Dim APILink As String = "http://" & vpszIPAddress & ":8090/person/create"
                            Dim jsons As String = JsonConvert.SerializeObject(UA)
                            Dim result1 As String = ""
callApiUpdate:              Dim httpWebRequest As WebRequest = WebRequest.Create(APILink) 'Live
                            httpWebRequest.ContentType = "application/json"
                            httpWebRequest.Method = "POST"
                            Using streamWriter = New StreamWriter(httpWebRequest.GetRequestStream())
                                streamWriter.Write(jsons)
                                streamWriter.Flush()
                                streamWriter.Close()
                            End Using
                            Dim httpResponse = CType(httpWebRequest.GetResponse(), HttpWebResponse)
                            Using streamReader = New StreamReader(httpResponse.GetResponseStream())
                                'Dim streamReader = New StreamReader(httpResponse.GetResponseStream)
                                result1 = streamReader.ReadToEnd()
                                If result1.Contains("Staff ID already exists") Then
                                    APILink = "http://" & vpszIPAddress & ":8090/person/update"
                                    GoTo callApiUpdate
                                End If
                                Dim fs As FileStream = New FileStream("JsonResponse.txt", FileMode.OpenOrCreate, FileAccess.Write)
                                Dim sw As StreamWriter = New StreamWriter(fs)
                                sw.BaseStream.Seek(0, SeekOrigin.End)
                                sw.WriteLine(vbCrLf & jsons & ", " & result1 & "::" & DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"))
                                sw.Flush()
                                sw.Close()
                            End Using

                            'face copy
                            If result1.Contains("""success"":true") Then
                                Dim Ultra800FId As String = GridView2.GetRowCellValue(rowHandleEmp, "Ultra800FId").ToString().Trim()
                                Dim imgBase64 As String = GridView2.GetRowCellValue(rowHandleEmp, "Template").ToString().Trim()
                                Dim UF As UltraFaceUpload = New UltraFaceUpload
                                UF.personId = sdwEnrollNumber
                                UF.faceId = Ultra800FId
                                UF.imgBase64 = imgBase64
                                UF.pass = commkey
                                jsons = JsonConvert.SerializeObject(UF)
                                APILink = "http://" & vpszIPAddress & ":8090/face/create"
callApiFaceUpdate:              result1 = ""
                                httpWebRequest = WebRequest.Create(APILink) 'Live
                                httpWebRequest.ContentType = "application/json"
                                httpWebRequest.Method = "POST"
                                Using streamWriter = New StreamWriter(httpWebRequest.GetRequestStream())
                                    streamWriter.Write(jsons)
                                    streamWriter.Flush()
                                    streamWriter.Close()
                                End Using
                                httpResponse = CType(httpWebRequest.GetResponse(), HttpWebResponse)
                                Using streamReader = New StreamReader(httpResponse.GetResponseStream())
                                    'Dim streamReader = New StreamReader(httpResponse.GetResponseStream)
                                    result1 = streamReader.ReadToEnd()
                                    If result1.Contains("Photo ID already exists") Then
                                        APILink = "http://" & vpszIPAddress & ":8090/face/update"
                                        GoTo callApiFaceUpdate
                                    End If
                                    Dim fs As FileStream = New FileStream("JsonResponse.txt", FileMode.OpenOrCreate, FileAccess.Write)
                                    Dim sw As StreamWriter = New StreamWriter(fs)
                                    sw.BaseStream.Seek(0, SeekOrigin.End)
                                    sw.WriteLine(vbCrLf & jsons & ", " & result1 & "::" & DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"))
                                    sw.Flush()
                                    sw.Close()
                                End Using

                            End If
                        End If
                    Next

                End If
            End If
        Next i
        For Each c As Control In Me.Controls
            c.Enabled = True
        Next
        Me.Cursor = Cursors.Default
        XtraMasterTest.LabelControlCount.Text = ""
        XtraMasterTest.LabelControlStatus.Text = ""
        Application.DoEvents()
        ' Screen.MousePointer = vbNormal
        'MsgBox("Task Completed", vbOKOnly)

        Dim failedIpArr() As String = failIP.ToArray
        Dim failIpStr As String = ""
        If failedIpArr.Length > 0 Then
            For i As Integer = 0 To failedIpArr.Length - 1
                failIpStr = failIpStr & "," & failedIpArr(i)
            Next
            failIpStr = failIpStr.TrimStart(",")
            XtraMessageBox.Show(ulf, "<size=10>" & failIpStr & " failed to download data</size>", "<size=9>Failed</size>")
        Else
            XtraMessageBox.Show(ulf, "<size=10>Task Completed</size>", "<size=9>Success</size>")
        End If


        'XtraMessageBox.Show(ulf, "<size=10>Task Completed</size>", "<size=9>Information</size>")
    End Sub
    Private Function readEnrollDataFromDBByID(ByVal astrStringID As String, ByVal anBackupNumber As Integer, BackupData As Object, ByRef mbytCurEnrollData As Byte()) As Boolean
        Application.DoEvents()
        'Dim vstrFind As String = "select * from EnrollTable where EnrollId='" & astrStringID & "' and BackupIndex=" & anBackupNumber.ToString()
        'If mAdoRstTmp.State = CInt(ADODB.ObjectStateEnum.adStateOpen) Then
        '    mAdoRstTmp.Close()
        'End If
        'mAdoRstTmp.Open(vstrFind, mAdoConnEnroll, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockPessimistic, -1)
        Dim PALMVEIN_DATA_SIZE As Integer = 20080
        If (anBackupNumber >= CInt(enumBackupNumberType.BACKUP_FP_0)) AndAlso (anBackupNumber <= CInt(enumBackupNumberType.BACKUP_FP_19)) Then
            Dim bytConvFpData() As Byte = DirectCast(BackupData, Byte())
            Dim bytFpData(FP_DATA_SIZE - 1) As Byte
            If bytConvFpData.Length < FP_DATA_SIZE Then
                Array.Clear(mbytCurEnrollData, 0, mbytCurEnrollData.Length)
            Else
                ConvFpDataAfterReadFromDbForCompatibility_ATF686n(bytConvFpData, bytFpData) ' convert fpdata for compatibility with old version database
                Array.Copy(bytFpData, mbytCurEnrollData, FP_DATA_SIZE)
            End If
        ElseIf (anBackupNumber = CInt(enumBackupNumberType.BACKUP_PSW)) OrElse (anBackupNumber = CInt(enumBackupNumberType.BACKUP_CARD)) Then
            Dim bytPwdData(-1) As Byte
            Try
                bytPwdData = DirectCast(BackupData, Byte())
            Catch
                ' If the value of "FPdata" field is empty, this database probably was built by old version DLL.
                ' With old version DLL the password or IDCard data is saved in "Password" field(int type).
                'bytPwdData = new byte[0];
                'mnCurPassword = (int)mAdoRstTmp.Fields["Password"].Value;
            End Try

            If bytPwdData.Length < PWD_DATA_SIZE Then
                Array.Clear(mbytCurEnrollData, 0, mbytCurEnrollData.Length)
            Else
                Array.Copy(bytPwdData, mbytCurEnrollData, PWD_DATA_SIZE)
            End If
        ElseIf anBackupNumber = CInt(enumBackupNumberType.BACKUP_FACE) Then
            Dim bytFaceData() As Byte = DirectCast(BackupData, Byte())
            If bytFaceData.Length < FACE_DATA_SIZE Then
                Array.Clear(mbytCurEnrollData, 0, mbytCurEnrollData.Length)
            Else
                Array.Copy(bytFaceData, mbytCurEnrollData, FACE_DATA_SIZE)
            End If

        ElseIf anBackupNumber >= CInt(enumBackupNumberType.BACKUP_PALMVEIN_0) AndAlso anBackupNumber <= CInt(enumBackupNumberType.BACKUP_PALMVEIN_3) Then
            Dim bytFaceData() As Byte = DirectCast(BackupData, Byte())
            If bytFaceData.Length < PALMVEIN_DATA_SIZE Then
                Array.Clear(mbytCurEnrollData, 0, mbytCurEnrollData.Length)
            Else
                Array.Copy(bytFaceData, mbytCurEnrollData, PALMVEIN_DATA_SIZE)
            End If

        End If
        Return True
    End Function
    Private Sub ConvFpDataAfterReadFromDbForCompatibility(ByRef abytSrc() As Byte, ByRef abytDest() As Byte)
        Dim nTempLen As Long, lenConvFpData As Long
        Dim bytConvFpData() As Byte
        Dim k As Long, m As Long
        Dim bytTemp() As Byte

        nTempLen = abytSrc.Length / 5
        lenConvFpData = nTempLen * 4

        If lenConvFpData < FP_DATA_SIZE Then lenConvFpData = FP_DATA_SIZE
        ReDim bytConvFpData(lenConvFpData)

        For k = 0 To nTempLen - 1
            ReDim bytTemp(3)
            bytTemp(0) = abytSrc(k * 5 + 4)
            bytTemp(1) = abytSrc(k * 5 + 3)
            bytTemp(2) = abytSrc(k * 5 + 2)
            bytTemp(3) = abytSrc(k * 5 + 1)

            m = BitConverter.ToInt32(bytTemp, 0)
            If abytSrc(k * 5) = 0 Then
                m = -m
            ElseIf abytSrc(k * 5) = 2 Then
                m = -2147483648
            End If
            bytTemp = BitConverter.GetBytes(m)

            bytConvFpData(k * 4 + 3) = bytTemp(3)
            bytConvFpData(k * 4 + 2) = bytTemp(2)
            bytConvFpData(k * 4 + 1) = bytTemp(1)
            bytConvFpData(k * 4 + 0) = bytTemp(0)
        Next

        abytDest = bytConvFpData
    End Sub
    Private Sub ConvFpDataAfterReadFromDbForCompatibility_ATF686n(ByVal abytSrc() As Byte, ByRef abytDest() As Byte)
        Dim nTempLen As Integer = abytSrc.Length \ 5
        Dim lenConvFpData As Integer = nTempLen * 4

        If lenConvFpData < FP_DATA_SIZE Then
            lenConvFpData = FP_DATA_SIZE
        End If

        Dim byteConvFpData(lenConvFpData - 1) As Byte
        Dim byteTemp(3) As Byte
        Dim k, m As Integer

        For k = 0 To nTempLen - 1
            byteTemp(0) = abytSrc(k * 5 + 4)
            byteTemp(1) = abytSrc(k * 5 + 3)
            byteTemp(2) = abytSrc(k * 5 + 2)
            byteTemp(3) = abytSrc(k * 5 + 1)
            m = BitConverter.ToInt32(byteTemp, 0)
            If abytSrc(k * 5) = 0 Then
                m = -m
            ElseIf abytSrc(k * 5) = 2 Then
                m = -2147483648
            End If
            byteTemp = BitConverter.GetBytes(m)

            byteConvFpData(k * 4 + 3) = byteTemp(3)
            byteConvFpData(k * 4 + 2) = byteTemp(2)
            byteConvFpData(k * 4 + 1) = byteTemp(1)
            byteConvFpData(k * 4 + 0) = byteTemp(0)
        Next k
        abytDest = byteConvFpData
    End Sub
    Private Sub btnDeleteFrmDevice_Click(sender As System.Object, e As System.EventArgs) Handles btnDeleteFrmDevice.Click

        Dim m As Integer
        'Dim e As Integer
        Dim lngMachineNum As Long
        Dim mCommKey As Long
        Dim lpszIPAddress As String
        Dim mKey As String
        Dim vnMachineNumber As Long
        Dim vnCommPort As Long
        Dim vnCommBaudrate As Long
        Dim vstrTelNumber As String
        Dim vnWaitDialTime As Long
        Dim vnLicense As Long
        Dim vpszIPAddress As String
        Dim vpszNetPort As Long
        Dim vpszNetPassword As Long
        Dim vnTimeOut As Long
        Dim vnProtocolType As Long
        Dim strDateTime As String
        Dim vRet As Long
        Dim vPrivilege As Long
        Dim mEmp As Integer
        Dim vFingerNumber As Long
        Dim vnResultCode As Long

        If GridView1.GetSelectedRows.Count = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>Please select the Machine</size>", "<size=9>Error</size>")
            Exit Sub
        End If
        If GridView2.GetSelectedRows.Count = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>Please select Employee</size>", "<size=9>Error</size>")
            Exit Sub
        End If
        If TextPassword.Text.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Please Enter Admin Password</size>", "<size=9>Error</size>")
            TextPassword.Select()
            Exit Sub
        Else
            If TextPassword.Text.ToLower <> Common.AdminPASSWORD.ToLower Then
                XtraMessageBox.Show(ulf, "<size=10>Admin Password Incorrect</size>", "<size=9>Error</size>")
                TextPassword.Select()
                Exit Sub
            End If
        End If

        If XtraMessageBox.Show(ulf, "<size=10>" & Common.res_man.GetString("askdelete", Common.cul) & "</size>", Common.res_man.GetString("confirmdeleteion", Common.cul),
                             MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
            For Each c As Control In Me.Controls
                c.Enabled = False
            Next
            Me.Cursor = Cursors.WaitCursor
            Dim sSql As String = ""
            Dim commkey As Integer
            Dim selectedRows As Integer() = GridView1.GetSelectedRows()
            Dim result As Object() = New Object(selectedRows.Length - 1) {}
            Dim LstMachineId As String
            Dim failIP As New List(Of String)()
            For i = 0 To selectedRows.Length - 1
                Dim rowHandle As Integer = selectedRows(i)
                If Not GridView1.IsGroupRow(rowHandle) Then
                    LstMachineId = GridView1.GetRowCellValue(rowHandle, "ID_NO").ToString.Trim
                    'sSql = "select * from tblmachine where ID_NO='" & LstMachineId & "'"
                    Dim bConn As Boolean = False
                    vnMachineNumber = LstMachineId
                    vnLicense = 1261 '1789 '
                    lpszIPAddress = GridView1.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim 'Trim(rstm!Location)
                    Common.LogPost("Device User Delete; Device ID: " & LstMachineId)
                    vpszIPAddress = Trim(lpszIPAddress)
                    If GridView1.GetRowCellValue(rowHandle, "DeviceType") = "Bio-1/3/4/5/6/7/ATF-203/ATF-395/ACR-872" Then
                        vpszNetPort = CLng("5005")
                        vpszNetPassword = CLng("0")
                        vnTimeOut = CLng("5000")
                        vnProtocolType = 0 'PROTOCOL_TCPIP
                        vnLicense = 1261
                        'bConn = FK623Attend.ConnectNet(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
                        If GridView1.GetRowCellValue(rowHandle, "A_R").ToString.Trim = "S" Then
                            vRet = FK_ConnectUSB(vnMachineNumber, vnLicense)
                            '    vstrTelNumber = ""
                            'vnWaitDialTime = 0
                            'vnCommPort = GridView1.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim 'Val(Trim(rstm!Location))
                            'vnCommBaudrate = 38400
                            ''*************************
                        Else
                            vpszIPAddress = Trim(lpszIPAddress)
                            vpszNetPort = CLng("5005")
                            vpszNetPassword = CLng("0")
                            vnTimeOut = CLng("5000")
                            vnProtocolType = 0
                            vRet = FK_ConnectNet(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
                        End If
                        If vRet > 0 Then
                            bConn = True
                        Else
                            bConn = False
                            failIP.Add(vpszIPAddress)
                        End If

                        Dim IsString As Boolean
                        If FK_GetIsSupportStringID(vRet) = RUN_SUCCESS Then
                            IsString = True
                        Else
                            IsString = False
                        End If
                        If vRet > 0 Then
                            Dim vstrData As String = New String(CType(ChrW(32), Char), 256)
                            Dim ret = FK_GetProductData(vRet, CType(1, Integer), vstrData)
                            If license.LicenseKey <> "" Then
                                If license.DeviceSerialNo.Contains(vstrData.Trim) Then
                                Else
                                    FK_DisConnect(vRet)
                                    'Return "Invalid Serial number " & vstrData
                                    XtraMessageBox.Show(ulf, "<size=10>Invalid Serial number " & vstrData & "</size>", "Failed")
                                    Continue For
                                End If
                            End If

                            'mnCommHandleIndex = 1
                            Dim selectedRowsEmp As Integer() = GridView2.GetSelectedRows()
                            Dim resultemp As Object() = New Object(selectedRowsEmp.Length - 1) {}

                            For x As Integer = 0 To selectedRowsEmp.Length - 1
                                Dim rowHandleemp As Integer = selectedRowsEmp(x)
                                If Not GridView2.IsGroupRow(rowHandleemp) Then
                                    mEmp = GridView2.GetRowCellValue(rowHandleemp, "EnrollNumber").ToString.Trim ' CInt(Val(Left(LstEmployeesTarget.Text, 8)))
                                    Dim mEmpStr As String = GridView2.GetRowCellValue(rowHandleemp, "EnrollNumber").ToString.Trim
                                    XtraMasterTest.LabelControlStatus.Text = "Deleting Template " & mEmp
                                    Application.DoEvents()
                                    'If GridView2.GetRowCellValue(rowHandleemp, "FingerNumber").ToString.Trim = "11" Then
                                    '    vFingerNumber = 11
                                    '    'ElseIf Trim(Right(LstEmployeesTarget.Text, 3)) = "PWD" Or Trim(Right(LstEmployeesTarget.Text, 4)) = "FACE" Then
                                    'ElseIf GridView2.GetRowCellValue(rowHandleemp, "FingerNumber").ToString.Trim = "10" Then
                                    '    vFingerNumber = 10
                                    'ElseIf GridView2.GetRowCellValue(rowHandleemp, "FingerNumber").ToString.Trim = "12" Then
                                    '    vFingerNumber = 12
                                    'Else
                                    '    'vFingerNumber = Trim(Right(LstEmployeesTarget.Text, 1)) - 1
                                    '    vFingerNumber = 0
                                    'End If
                                    vFingerNumber = GridView2.GetRowCellValue(rowHandleemp, "FingerNumber").ToString.Trim
                                    If IsString Then
                                        vnResultCode = FK_DeleteEnrollData_StringID(vnResultCode,
                                                       mEmpStr,
                                                       vFingerNumber)
                                    Else
                                        vnResultCode = FK_DeleteEnrollData(vnResultCode,
                                                       mEmp,
                                                       vFingerNumber)
                                    End If

                                    'bdel = FK623Attend.DeleteEnrollData(CInt(Val(Left(LstEmployeesTarget.Text, 8))), vFingerNumber)
                                    'If vnResultCode = 1 And Pdel = False Then
                                    '    Pdel = True
                                    'End If
                                    'Next k
                                    If vnResultCode = 1 Then
                                        ' MsgBox "User has been deleted successfully", vbOKOnly
                                    Else
                                        'MsgBox("Unable to delete the user " & mEmp & " from device " & vnMachineNumber & "", vbOKOnly)                                       
                                        XtraMessageBox.Show(ulf, "<size=10>Unable to delete the user " & mEmp & " from device " & vnMachineNumber & "</size>", "<size=9>Information</size>")
                                    End If
                                End If
                            Next
                        Else
                            'MsgBox("Device No: " & vnMachineNumber & " Not connected..")
                            failIP.Add(vpszIPAddress)
                            'XtraMessageBox.Show(ulf, "<size=10>Device No: " & vnMachineNumber & " Not connected..</size>", "<size=9>Information</size>")
                        End If
                        'FK623Attend.Disconnect
                        FK_DisConnect(1)
                    ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "ZK(TFT)" Or GridView1.GetRowCellValue(rowHandle, "DeviceType") = "Bio-1Pro/ATF305Pro/ATF686Pro" Then
                        Dim sdwEnrollNumber As Integer
                        Dim idwVerifyMode As Integer
                        Dim idwWorkcode As Integer

                        Dim bIsConnected = False
                        Dim iMachineNumber As Integer = result(i)
                        Dim idwErrorCode As Integer
                        Dim com As Common = New Common
                        Dim axCZKEM1 As New zkemkeeper.CZKEM
                        commkey = Convert.ToInt32(GridView1.GetRowCellValue(rowHandle, GridView1.Columns("commkey")))
                        Dim sName As String = ""
                        Dim sPassword As String = ""
                        Dim iPrivilege As Integer
                        Dim bEnabled As Boolean = False
                        Dim idwFingerIndex As Integer
                        Dim sTmpData As String = ""
                        Dim iTmpLength As Integer
                        Dim iFlag As Integer
                        Dim flagenabled As String = ""
                        Dim adap As SqlDataAdapter
                        Dim adapA As OleDbDataAdapter
                        Dim dsRecord As DataSet = New DataSet
                        axCZKEM1.SetCommPassword(Convert.ToInt32(commkey))  'to check device commkey and db commkey matches
                        bIsConnected = axCZKEM1.Connect_Net(vpszIPAddress, 4370)
                        If bIsConnected = True Then
                            Dim lvDownload As System.Windows.Forms.ListView = New System.Windows.Forms.ListView
                            lvDownload.Items.Clear()
                            lvDownload.BeginUpdate()
                            axCZKEM1.EnableDevice(iMachineNumber, False)
                            Dim selectedRowsEmp As Integer() = GridView2.GetSelectedRows()
                            Dim resultemp As Object() = New Object(selectedRowsEmp.Length - 1) {}
                            Dim fingerNum As Integer
                            Dim tmp As Boolean
                            For x As Integer = 0 To selectedRowsEmp.Length - 1
                                Dim rowHandleemp As Integer = selectedRowsEmp(x)
                                If Not GridView2.IsGroupRow(rowHandleemp) Then
                                    Dim mEmp1 As String = GridView2.GetRowCellValue(rowHandleemp, "EnrollNumber").ToString.Trim
                                    If IsNumeric(mEmp1) Then
                                        mEmp1 = Convert.ToDouble(mEmp1)
                                    End If
                                    XtraMasterTest.LabelControlStatus.Text = "Deleting Template " & mEmp1
                                    Application.DoEvents()
                                    fingerNum = 12 'GridView2.GetRowCellValue(rowHandleemp, "FingerNumber").ToString.Trim
                                    tmp = axCZKEM1.SSR_DeleteEnrollData(vnMachineNumber, mEmp1, fingerNum)
                                    'MsgBox(tmp.ToString)
                                    'If fingerNum = 11 Or fingerNum = 10 Then
                                    '    'fingerNum = 12
                                    'End If
                                    'If fingerNum = 50 Then    'ful delete
                                    '    tmp = axCZKEM1.DelUserTmpExt(vnMachineNumber, mEmp, vnMachineNumber, fingerNum)
                                    'ElseIf fingerNum = 10 Then
                                    '    tmp = axCZKEM1.SSR_DeleteEnrollData(vnMachineNumber, mEmp, fingerNum)
                                    'Else   'indexed value delete
                                    '    tmp = axCZKEM1.SSR_DelUserTmpExt(vnMachineNumber, mEmp, fingerNum)
                                    'End If
                                End If
                            Next
                            axCZKEM1.EnableDevice(iMachineNumber, True)
                            axCZKEM1.Disconnect()
                        Else
                            failIP.Add(vpszIPAddress)
                        End If
                    ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "Bio1Eco" Then
                        vpszIPAddress = GridView1.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim()
                        XtraMasterTest.LabelControlStatus.Text = "Connecting " & vpszIPAddress & "..."
                        Application.DoEvents()
                        Dim adap, adap1 As SqlDataAdapter
                        Dim adapA, adapA1 As OleDbDataAdapter
                        Dim ds, ds1 As DataSet
                        commkey = Convert.ToInt32(GridView1.GetRowCellValue(rowHandle, GridView1.Columns("commkey")))
                        Dim mOpenFlag As Boolean
                        Dim mMK8001Device As Boolean 'TRUE:MK8001/8002Device;False:else

                        mMK8001Device = False
                        Dim si As SySystemInfo = New SySystemInfo
                        si.cSerialNum = New Byte((20) - 1) {}
                        si.cManuName = New Byte((24) - 1) {}
                        si.cDevName = New Byte((24) - 1) {}
                        si.cAlgVer = New Byte((16) - 1) {}
                        si.cFirmwareVer = New Byte((24) - 1) {}
                        Dim rret As Integer = 66
                        Try
                            rret = SyFunctions.CmdGetSystemInfo(si)
                        Catch ex As Exception

                        End Try

                        If (rret = CType(SyLastError.sleSuss, Int32)) Then
                            Dim sFirmwareVer As String = System.Text.Encoding.Default.GetString(si.cFirmwareVer)
                            If sFirmwareVer.Contains("MK8001/8002") Then
                                mMK8001Device = True
                            Else
                                mMK8001Device = False
                            End If
                        End If
                        Dim iFingerSize
                        If mMK8001Device Then
                            iFingerSize = 768
                        Else
                            iFingerSize = 512
                        End If
                        Dim ret As Int32 = CType(SyLastError.sleInvalidParam, Int32)
                        Dim equNo As UInt32 = Convert.ToUInt32(LstMachineId)
                        Dim gEquNo As UInt32 = equNo
                        If GridView1.GetRowCellValue(rowHandle, "A_R").ToString.Trim = "T" Then
                            Dim netCfg As SyNetCfg
                            Dim pswd As UInt32 = 0
                            Dim tmpPswd As String = commkey
                            If (tmpPswd = "0") Then
                                tmpPswd = "FFFFFFFF"
                            End If
                            Try
                                pswd = UInt32.Parse(tmpPswd, NumberStyles.HexNumber)
                            Catch ex As Exception
                                XtraMessageBox.Show(ulf, "<size=10>Incorrect Comm Key</size>", "Failed")
                                ret = CType(SyLastError.slePasswordError, Int32)
                            End Try
                            If (ret <> CType(SyLastError.slePasswordError, Int32)) Then
                                netCfg.mIsTCP = 1
                                netCfg.mPortNo = 8000 'Convert.ToUInt16(txtPortNo.Text)
                                netCfg.mIPAddr = New Byte((4) - 1) {}
                                Dim sArray() As String = vpszIPAddress.Split(Microsoft.VisualBasic.ChrW(46))
                                Dim j As Byte = 0
                                For Each i1 As String In sArray
                                    Try
                                        netCfg.mIPAddr(j) = Convert.ToByte(i1.Trim)
                                    Catch ex As System.Exception
                                        netCfg.mIPAddr(j) = 255
                                    End Try
                                    j = (j + 1)
                                    If j > 3 Then
                                        Exit For
                                    End If

                                Next
                            End If

                            Dim pnt As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(netCfg))
                            Try
                                Marshal.StructureToPtr(netCfg, pnt, False)
                                '        If optWifiDevice.Checked Then
                                'ret = SyFunctions.CmdOpenDevice(3, pnt, equNo, pswd, Convert.ToByte(chkTranceIO.Checked?1, :, 0))
                                '        Else
                                ret = SyFunctions.CmdOpenDevice(2, pnt, equNo, pswd, Convert.ToByte(1))
                                '        End If
                                '                'TODO: Warning!!!, inline IF is not supported ?
                                '                chkTranceIO.Checked()
                                '0:
                                '                '
                            Finally
                                Marshal.FreeHGlobal(pnt)
                            End Try
                        ElseIf GridView1.GetRowCellValue(rowHandle, "A_R").ToString.Trim = "S" Then
                            Dim equtype As String = "Finger Module USB Device"
                            Dim pswd As UInt32 = UInt32.Parse("FFFFFFFF", NumberStyles.HexNumber)
                            ret = SyFunctions.CmdOpenDeviceByUsb(equtype, equNo, pswd, Convert.ToByte(1))
                        End If
                        If (ret = CType(SyLastError.sleSuss, Int32)) Then
                            ret = SyFunctions.CmdTestConn2Device
                            Dim selectedRowsEmp As Integer() = GridView2.GetSelectedRows()
                            Dim resultEmp As Object() = New Object(selectedRowsEmp.Length - 1) {}
                            For x As Integer = 0 To selectedRowsEmp.Length - 1
                                Dim rowHandleEmp As Long = selectedRowsEmp(x)
                                If Not GridView2.IsGroupRow(rowHandleEmp) Then
                                    If GridView2.GetRowCellValue(rowHandleEmp, "DeviceType").ToString.Trim() = "Bio1Eco" Then
                                        Dim sdwEnrollNumber
                                        If IsNumeric(GridView2.GetRowCellValue(rowHandleEmp, "EnrollNumber").ToString().Trim) = True Then
                                            sdwEnrollNumber = Convert.ToInt64(GridView2.GetRowCellValue(rowHandleEmp, "EnrollNumber").ToString().Trim)
                                        Else
                                            sdwEnrollNumber = GridView2.GetRowCellValue(rowHandleEmp, "EnrollNumber").ToString().Trim
                                        End If
                                        XtraMasterTest.LabelControlStatus.Text = "Deleting Template " & sdwEnrollNumber
                                        Application.DoEvents()
                                        Dim vEnrollNumber As Integer = Convert.ToInt32(sdwEnrollNumber)
                                        ret = SyFunctions.CmdDeleteUserInfo(CType(vEnrollNumber, System.UInt16), 0)

                                    End If
                                End If
                            Next
                            'End loop for fingerprint

                            SyFunctions.CloseCom()
                            ' close com 
                            SyFunctions.CmdUnLockDevice()
                            SyFunctions.CloseCom()
                        Else
                            'XtraMessageBox.Show(ulf, "<size=10>Connection Failed</size>", "Failed")
                            Continue For
                        End If
                    ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "EF45" Then
                        XtraMasterTest.LabelControlStatus.Text = "Connecting " & vpszIPAddress & "..."
                        Application.DoEvents()
                        Dim cn As Common = New Common
                        Dim _client As Client = Nothing
                        Dim connectOk As Boolean = False
                        Try
                            _client = cn.initClientEF45(vpszIPAddress)
                            connectOk = _client.StealConnect '_client.Connect()
                            If connectOk Then
                                ' loop for fingerprint
                                Dim selectedRowsEmp As Integer() = GridView2.GetSelectedRows()
                                Dim resultEmp As Object() = New Object(selectedRowsEmp.Length - 1) {}
                                For x As Integer = 0 To selectedRowsEmp.Length - 1
                                    Dim rowHandleEmp As Long = selectedRowsEmp(x)
                                    If Not GridView2.IsGroupRow(rowHandleEmp) Then
                                        If GridView2.GetRowCellValue(rowHandleEmp, "DeviceType").ToString.Trim() = "EF45" Then
                                            Dim sdwEnrollNumber
                                            If IsNumeric(GridView2.GetRowCellValue(rowHandleEmp, "EnrollNumber").ToString().Trim) = True Then
                                                sdwEnrollNumber = Convert.ToInt64(GridView2.GetRowCellValue(rowHandleEmp, "EnrollNumber").ToString().Trim)
                                            Else
                                                sdwEnrollNumber = GridView2.GetRowCellValue(rowHandleEmp, "EnrollNumber").ToString().Trim
                                            End If
                                            XtraMasterTest.LabelControlStatus.Text = "Deleting Template " & sdwEnrollNumber
                                            Application.DoEvents()
                                            If IsNumeric(sdwEnrollNumber) Then
                                                sdwEnrollNumber = Convert.ToInt64(sdwEnrollNumber)
                                            End If
                                            Try
                                                _client.DeleteSubject(sdwEnrollNumber)
                                            Catch ex As Exception

                                            End Try
                                            Try
                                                _client.DeleteFace(sdwEnrollNumber, CType(Client.UMXFace.FarOffFace, Integer))
                                            Catch ex As Exception

                                            End Try
                                            Try
                                                _client.DeleteFace(sdwEnrollNumber, CType(Client.UMXFace.FarOnFace, Integer))
                                            Catch ex As Exception

                                            End Try
                                            Try
                                                _client.DeleteFace(sdwEnrollNumber, CType(Client.UMXFace.NearOffFace, Integer))
                                            Catch ex As Exception

                                            End Try
                                            Try
                                                _client.DeleteFace(sdwEnrollNumber, CType(Client.UMXFace.NearOnFace, Integer))
                                            Catch ex As Exception

                                            End Try
                                            Try
                                                _client.DeleteFace(sdwEnrollNumber, CType(Client.UMXFace.UpOffFace, Integer))
                                            Catch ex As Exception

                                            End Try
                                            Try
                                                _client.DeleteFace(sdwEnrollNumber, CType(Client.UMXFace.UpOnFace, Integer))
                                            Catch ex As Exception

                                            End Try
                                            Try
                                                _client.DeleteUserInfoByUUID(sdwEnrollNumber)
                                            Catch ex As Exception

                                            End Try

                                        End If
                                    End If
                                Next
                                'End loop for fingerprint

                                _client.Disconnect()
                            Else
                                'failIP.Add(vpszIPAddress.ToString)
                                failIP.Add(vpszIPAddress)
                                Continue For
                            End If
                        Catch ex As Exception
                            'failIP.Add(vpszIPAddress.ToString)
                            Continue For
                        End Try
                    ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "F9" Then
                        vpszNetPort = CLng("5005")
                        vpszNetPassword = CLng("0")
                        vnTimeOut = CLng("5000")
                        vnProtocolType = 0 'PROTOCOL_TCPIP
                        vnLicense = 7881
                        Dim F9 As mdlPublic_f9 = New mdlPublic_f9
                        If GridView1.GetRowCellValue(rowHandle, "A_R").ToString.Trim = "S" Then
                            vRet = F9.FK_ConnectUSB(vnMachineNumber, vnLicense)
                        Else
                            vpszIPAddress = Trim(lpszIPAddress)
                            vpszNetPort = CLng("5005")
                            vpszNetPassword = CLng("0")
                            vnTimeOut = CLng("5000")
                            vnProtocolType = 0
                            vRet = F9.FK_ConnectNet(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
                        End If
                        If vRet > 0 Then
                            bConn = True
                        Else
                            bConn = False
                            failIP.Add(vpszIPAddress)
                        End If

                        Dim IsString As Boolean
                        If F9.FK_GetIsSupportStringID(vRet) = RUN_SUCCESS Then
                            IsString = True
                        Else
                            IsString = False
                        End If
                        If vRet > 0 Then
                            'mnCommHandleIndex = 1
                            Dim selectedRowsEmp As Integer() = GridView2.GetSelectedRows()
                            Dim resultemp As Object() = New Object(selectedRowsEmp.Length - 1) {}
                            Dim mEmpStr As String = ""
                            For x As Integer = 0 To selectedRowsEmp.Length - 1
                                Dim rowHandleemp As Integer = selectedRowsEmp(x)
                                If Not GridView2.IsGroupRow(rowHandleemp) Then
                                    mEmp = GridView2.GetRowCellValue(rowHandleemp, "EnrollNumber").ToString.Trim ' CInt(Val(Left(LstEmployeesTarget.Text, 8)))
                                    mEmpStr = GridView2.GetRowCellValue(rowHandleemp, "EnrollNumber").ToString.Trim
                                    XtraMasterTest.LabelControlStatus.Text = "Deleting Template " & mEmp
                                    Application.DoEvents()
                                    vFingerNumber = GridView2.GetRowCellValue(rowHandleemp, "FingerNumber").ToString.Trim

                                    If IsString Then
                                        vnResultCode = F9.FK_DeleteEnrollData_StringID(vRet,
                                                       mEmp.ToString,
                                                       vFingerNumber)
                                    Else
                                        vnResultCode = F9.FK_DeleteEnrollData(vRet,
                                                       mEmp,
                                                       vFingerNumber)
                                    End If

                                    If vnResultCode = 1 Then
                                        ' MsgBox "User has been deleted successfully", vbOKOnly
                                    Else
                                        XtraMessageBox.Show(ulf, "<size=10>Unable to delete the user " & mEmp & " from device " & vnMachineNumber & "</size>", "<size=9>Information</size>")
                                    End If
                                End If
                            Next
                        Else
                            failIP.Add(vpszIPAddress)
                            'XtraMessageBox.Show(ulf, "<size=10>Device No: " & vnMachineNumber & " Not connected..</size>", "<size=9>Information</size>")
                        End If
                        F9.FK_DisConnect(1)

                    ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "ATF686n" Then
                        vpszNetPort = CLng("5005")
                        vpszNetPassword = CLng("0")
                        vnTimeOut = CLng("5000")
                        vnProtocolType = 0
                        vnLicense = 7881
                        Dim atf686n As mdlFunction_Atf686n = New mdlFunction_Atf686n
                        If GridView1.GetRowCellValue(rowHandle, "A_R").ToString.Trim = "S" Then
                            vnResult = atf686n.ST_ConnectUSB(GridView1.GetRowCellValue(rowHandle, "ID_NO").ToString.Trim, vnLicense)
                        Else
                            vnResult = atf686n.ConnectNet(vpszIPAddress)
                        End If
                        If vnResult <= 0 Then
                            failIP.Add(vpszIPAddress)
                        End If
                        vnResultCode = atf686n.ST_EnableDevice(vnResult, 0)
                        If vnResultCode <> RUN_SUCCESS Then
                            failIP.Add(vpszIPAddress)
                            atf686n.ST_DisConnect(vnResult)
                            'Return gstrNoDevice
                            Continue For
                        End If
                        Dim selectedRowsEmp As Integer() = GridView2.GetSelectedRows()
                        Dim resultEmp As Object() = New Object(selectedRowsEmp.Length - 1) {}
                        For x As Integer = 0 To selectedRowsEmp.Length - 1
                            'LstEmployeesTarget.ListIndex = e
                            Dim rowHandleEmp As Long = selectedRowsEmp(x)
                            If Not GridView2.IsGroupRow(rowHandleEmp) Then
                                Dim EnrollNumber As String = GridView2.GetRowCellValue(rowHandleEmp, "EnrollNumber").ToString.Trim
                                Dim FingerNumber As Long = GridView2.GetRowCellValue(rowHandleEmp, "FingerNumber").ToString.Trim
                                Dim vStrEnrollNumber As String = EnrollNumber
                                If IsNumeric(EnrollNumber) Then
                                    vStrEnrollNumber = Convert.ToDouble(EnrollNumber)
                                End If
                                If GridView2.GetRowCellValue(rowHandleEmp, "DeviceType").ToString.Trim = "ATF686n" Then
                                    If atf686n.ST_GetIsSupportStringID(vnResult) = CInt(enumErrorCode.RETURN_SUCCESS) Then
                                        Dim abEnableFlag As Integer = 1
                                        XtraMasterTest.LabelControlStatus.Text = "Deleting User " & EnrollNumber & " In Device " & vpszIPAddress
                                        Application.DoEvents()
                                        vnResultCode = atf686n.ST_DeleteEnrollData_SID(vnResult, vStrEnrollNumber, True)
                                    End If
                                End If
                            End If
                        Next
                        atf686n.ST_EnableDevice(vnResult, 1)
                        atf686n.ST_DisConnect(vnResult)
                    ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "TF-01" Then
                        vpszNetPort = CLng("5005")
                        vpszNetPassword = CLng("0")
                        vnTimeOut = CLng("5000")
                        vnProtocolType = 0 'PROTOCOL_TCPIP
                        vnLicense = 7881
                        'Dim F9 As mdlPublic_f9 = New mdlPublic_f9
                        Dim bRet As Boolean
                        ' AxFP_CLOCK1.OpenCommPort(vnMachineNumber)

                        vpszIPAddress = Trim(lpszIPAddress)
                        vpszNetPort = CLng("5005")
                        vpszNetPassword = CLng("0")
                        vnTimeOut = CLng("5000")
                        vnProtocolType = 0
                        bRet = AxFP_CLOCK1.SetIPAddress(vpszIPAddress, vpszNetPort, vpszNetPort)
                        AxFP_CLOCK1.OpenCommPort(vnMachineNumber)
                        If bRet Then
                            bConn = True
                            ' AxFP_CLOCK1.EnableDevice(vnMachineNumber, 0)
                        Else
                            bConn = False
                            failIP.Add(vpszIPAddress)
                        End If

                        'Dim IsString As Boolean
                        'If F9.FK_GetIsSupportStringID(vRet) = RUN_SUCCESS Then
                        '    IsString = True
                        'Else
                        '    IsString = False
                        'End If
                        If bConn Then

                            'mnCommHandleIndex = 1
                            Dim selectedRowsEmp As Integer() = GridView2.GetSelectedRows()
                            Dim resultemp As Object() = New Object(selectedRowsEmp.Length - 1) {}
                            Dim mEmpStr As String = ""
                            For x As Integer = 0 To selectedRowsEmp.Length - 1
                                Dim rowHandleemp As Integer = selectedRowsEmp(x)
                                If Not GridView2.IsGroupRow(rowHandleemp) Then
                                    mEmp = GridView2.GetRowCellValue(rowHandleemp, "EnrollNumber").ToString.Trim ' CInt(Val(Left(LstEmployeesTarget.Text, 8)))
                                    mEmpStr = GridView2.GetRowCellValue(rowHandleemp, "EnrollNumber").ToString.Trim
                                    XtraMasterTest.LabelControlStatus.Text = "Deleting Template " & mEmp
                                    Application.DoEvents()
                                    vFingerNumber = GridView2.GetRowCellValue(rowHandleemp, "FingerNumber").ToString.Trim
                                    bRet = AxFP_CLOCK1.DeleteEnrollData(vnMachineNumber, mEmp, vnMachineNumber, vFingerNumber)
                                    If bRet Then
                                        ' MsgBox "User has been deleted successfully", vbOKOnly
                                    Else
                                        XtraMessageBox.Show(ulf, "<size=10>Unable to delete the user " & mEmp & " from device " & vnMachineNumber & "</size>", "<size=9>Information</size>")
                                    End If
                                End If
                            Next
                        Else
                            failIP.Add(vpszIPAddress)
                            'XtraMessageBox.Show(ulf, "<size=10>Device No: " & vnMachineNumber & " Not connected..</size>", "<size=9>Information</size>")
                        End If
                        AxFP_CLOCK1.EnableDevice(vnMachineNumber, 1)
                        AxFP_CLOCK1.CloseCommPort()

                    ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "Ultra800" Then

                        If license.LicenseKey <> "" Then
                            If license.DeviceSerialNo.Contains(GridView1.GetRowCellValue(rowHandle, GridView1.Columns("MAC_ADDRESS")).ToString.Trim) Then
                            Else
                                XtraMessageBox.Show(ulf, "<size=10>Invalid Serial number " & GridView1.GetRowCellValue(rowHandle, GridView1.Columns("MAC_ADDRESS")).ToString.Trim & "</size>", "Failed")
                                Continue For
                            End If
                        End If

                        Dim commKey1 As String = GridView1.GetRowCellValue(rowHandle, GridView1.Columns("commkey")).ToString.Trim
                            vpszIPAddress = Trim(lpszIPAddress)
                            Dim selectedRowsEmp As Integer() = GridView2.GetSelectedRows()
                            Dim resultemp As Object() = New Object(selectedRowsEmp.Length - 1) {}
                            Dim mEmpStr As String = ""
                            For x As Integer = 0 To selectedRowsEmp.Length - 1
                                Dim rowHandleemp As Integer = selectedRowsEmp(x)
                                If Not GridView2.IsGroupRow(rowHandleemp) Then
                                    mEmp = GridView2.GetRowCellValue(rowHandleemp, "EnrollNumber").ToString.Trim ' CInt(Val(Left(LstEmployeesTarget.Text, 8)))
                                    mEmpStr = GridView2.GetRowCellValue(rowHandleemp, "EnrollNumber").ToString.Trim
                                    XtraMasterTest.LabelControlStatus.Text = "Deleting Template " & mEmp
                                    Application.DoEvents()
                                    Dim UD As UserDelete = New UserDelete
                                    UD.pass = commKey1

                                    If IsNumeric(mEmpStr) Then
                                        UD.id = Convert.ToDouble(mEmpStr)
                                    End If
                                    Dim jsons As String = JsonConvert.SerializeObject(UD)
                                    Dim link As String = "http://" & vpszIPAddress & ":8090/person/delete"
                                    Dim httpWebRequest As WebRequest = WebRequest.Create(link) 'Live
                                    httpWebRequest.ContentType = "application/json"
                                    httpWebRequest.Method = "POST"
                                    Using streamWriter = New StreamWriter(httpWebRequest.GetRequestStream())
                                        streamWriter.Write(jsons)
                                        streamWriter.Flush()
                                        streamWriter.Close()
                                    End Using
                                    Dim httpResponse = CType(httpWebRequest.GetResponse(), HttpWebResponse)
                                    Using streamReader = New StreamReader(httpResponse.GetResponseStream())
                                        'Dim streamReader = New StreamReader(httpResponse.GetResponseStream)
                                        Dim result1 = streamReader.ReadToEnd()
                                        Dim fs As FileStream = New FileStream("JsonResponse.txt", FileMode.OpenOrCreate, FileAccess.Write)
                                        Dim sw As StreamWriter = New StreamWriter(fs)
                                        sw.BaseStream.Seek(0, SeekOrigin.End)
                                        sw.WriteLine(vbCrLf & link & ", " & result1 & "::" & DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"))
                                        sw.Flush()
                                        sw.Close()
                                    End Using
                                End If
                            Next

                        End If
                    End If
                Next
                For Each c As Control In Me.Controls
                    c.Enabled = True
                Next

                TextPassword.Text = ""
                XtraMasterTest.LabelControlStatus.Text = ""
                Application.DoEvents()
                Me.Cursor = Cursors.Default
                Dim failedIpArr() As String = failIP.Distinct.ToArray
                Dim failIpStr As String = ""
                If failedIpArr.Length > 0 Then
                    For i As Integer = 0 To failedIpArr.Length - 1
                        failIpStr = failIpStr & "," & failedIpArr(i)
                    Next
                    failIpStr = failIpStr.TrimStart(",")
                    XtraMessageBox.Show(ulf, "<size=10>" & failIpStr & " failed to download data</size>", "Failed")
                Else
                    XtraMessageBox.Show(ulf, "<size=10>Task Completed</size>", "Success")
                End If


                XtraMessageBox.Show(ulf, "<size=10>Task Completed</size>", "<size=9>Information</size>")
            End If
        End Sub
        Private Sub btnClearAdmin_Click(sender As System.Object, e As System.EventArgs) Handles btnClearAdmin.Click
            Dim bConn As Boolean
            Dim lngMachineNum As Long
            Dim mCommKey As Long
            Dim lpszIPAddress As String
            Dim vEMachineNumber As Long
            Dim vEnrollNumber As Long
            Dim vFingerNumber As Long
            Dim vPrivilege As Long
            Dim vEnable As Long
            Dim vRet As Boolean
            Dim mKey As String
            Dim vnResultCode As Long
            Dim RsDevice As New DataSet 'ADODB.Recordset
            vnResultCode = 0
            Dim vnMachineNumber As Long

        Dim vstrTelNumber As String
        Dim vnWaitDialTime As Long
        Dim vnLicense As Long
        Dim vpszIPAddress As String
        Dim vpszNetPort As Long
        Dim vpszNetPassword As Long
        Dim vnCommPort As Long
        Dim vnCommBaudrate As Long

        If GridView1.GetSelectedRows.Count = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>Please select the Machine</size>", "<size=9>Error</size>")
            Exit Sub
        End If
        If TextPassword.Text.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Please Enter Admin Password</size>", "<size=9>Error</size>")
            TextPassword.Select()
            Exit Sub
        Else
            If TextPassword.Text.ToLower <> Common.AdminPASSWORD.ToLower Then
                XtraMessageBox.Show(ulf, "<size=10>Admin Password Incorrect</size>", "<size=9>Error</size>")
                TextPassword.Select()
                Exit Sub
            End If
        End If
        If XtraMessageBox.Show(ulf, "<size=10>Are you sure to clear admin for selected machine</size>", Common.res_man.GetString("confirmdeleteion", Common.cul), _
                             MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then

            Me.Cursor = Cursors.WaitCursor
            For Each c As Control In Me.Controls
                c.Enabled = False
            Next
            Dim sSql As String = ""
            Dim selectedRows As Integer() = GridView1.GetSelectedRows()
            Dim result As Object() = New Object(selectedRows.Length - 1) {}
            Dim LstMachineId As String
            Dim mMachineNumber As Long
            Dim commkey As Integer
            Dim failIP As New List(Of String)()
            For i = 0 To selectedRows.Length - 1
                Dim rowHandle As Integer = selectedRows(i)
                If Not GridView1.IsGroupRow(rowHandle) Then
                    LstMachineId = GridView1.GetRowCellValue(rowHandle, "ID_NO").ToString.Trim
                    Common.LogPost("Device Clear Admin; Device ID: " & LstMachineId)
                    'sSql = "select * from tblmachine where ID_NO='" & LstMachineId & "'"

                    bConn = False
                    vnMachineNumber = LstMachineId
                    vnLicense = 1261 '1789 '
                    lpszIPAddress = GridView1.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim 'Trim(rstm!Location)
                    vpszIPAddress = Trim(lpszIPAddress)

                    If GridView1.GetRowCellValue(rowHandle, "DeviceType") = "Bio-1/3/4/5/6/7/ATF-203/ATF-395/ACR-872" Then

                        vpszNetPort = CLng("5005")
                        vpszNetPassword = CLng("0")
                        vnTimeOut = CLng("5000")
                        vnProtocolType = 0 'PROTOCOL_TCPIP
                        vnLicense = 1261
                        'bConn = FK623Attend.ConnectNet(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
                        If GridView1.GetRowCellValue(rowHandle, "A_R").ToString.Trim = "S" Then
                            vnResultCode = FK_ConnectUSB(vnMachineNumber, vnLicense)
                            '    vstrTelNumber = ""
                            'vnWaitDialTime = 0
                            'vnCommPort = GridView1.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim 'Val(Trim(rstm!Location))
                            'vnCommBaudrate = 38400
                        Else
                            vpszIPAddress = Trim(lpszIPAddress)
                            vpszNetPort = CLng("5005")
                            vpszNetPassword = CLng("0")
                            vnTimeOut = CLng("5000")
                            vnProtocolType = 0
                            vnResultCode = FK_ConnectNet(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
                            'vnResultCode = FK_ConnectNet(mMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
                        End If
                        If vnResultCode > 0 Then
                            bConn = True
                        Else
                            bConn = False
                            failIP.Add(vpszIPAddress)
                        End If
                        If vnResultCode > 0 Then
                            Dim vstrData As String = New String(CType(ChrW(32), Char), 256)
                            Dim ret = FK_GetProductData(vnResultCode, CType(1, Integer), vstrData)
                            If license.LicenseKey <> "" Then
                                If license.DeviceSerialNo.Contains(vstrData.Trim) Then
                                Else
                                    FK_DisConnect(vnResultCode)
                                    'Return "Invalid Serial number " & vstrData
                                    XtraMessageBox.Show(ulf, "<size=10>Invalid Serial number " & vstrData & "</size>", "Failed")
                                    Continue For
                                End If
                            End If
                            vnResultCode = FK_EnableDevice(vnResultCode, 0)
                            'vnResultCode = FK623Attend.BenumbAllManager()
                            vnResultCode = FK_BenumbAllManager(1)
                            If vnResultCode <> RUN_SUCCESS Then
                                'MsgBox("Unable to clear Admin")
                                XtraMessageBox.Show(ulf, "<size=10>Unable to clear Admin</size>", "<size=9>Information</size>")
                            End If
                        Else
                            'MsgBox("Unable to connect Device No - " & mMachineNumber)
                            failIP.Add(vpszIPAddress)
                            'XtraMessageBox.Show(ulf, "<size=10>Unable to connect Device No - " & mMachineNumber & "</size>", "<size=9>Information</size>")
                        End If
                        'FK623Attend.Disconnect
                        vnResultCode = FK_EnableDevice(vnResultCode, 1)
                        FK_DisConnect(vnResultCode)
                    ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "ZK(TFT)" Or GridView1.GetRowCellValue(rowHandle, "DeviceType") = "Bio-1Pro/ATF305Pro/ATF686Pro" Then
                        Dim sdwEnrollNumber As Integer
                        Dim idwVerifyMode As Integer
                        Dim idwWorkcode As Integer

                        Dim bIsConnected = False
                        Dim iMachineNumber As Integer = result(i)
                        Dim idwErrorCode As Integer
                        Dim com As Common = New Common
                        Dim axCZKEM1 As New zkemkeeper.CZKEM
                        commkey = Convert.ToInt32(GridView1.GetRowCellValue(rowHandle, GridView1.Columns("commkey")))
                        Dim sName As String = ""
                        Dim sPassword As String = ""
                        Dim iPrivilege As Integer
                        Dim bEnabled As Boolean = False
                        Dim idwFingerIndex As Integer
                        Dim sTmpData As String = ""
                        Dim iTmpLength As Integer
                        Dim iFlag As Integer
                        Dim flagenabled As String = ""
                        Dim adap As SqlDataAdapter
                        Dim adapA As OleDbDataAdapter
                        Dim dsRecord As DataSet = New DataSet
                        axCZKEM1.SetCommPassword(Convert.ToInt32(commkey))  'to check device commkey and db commkey matches
                        bIsConnected = axCZKEM1.Connect_Net(vpszIPAddress, 4370)
                        If bIsConnected = True Then
                            Dim lvDownload As System.Windows.Forms.ListView = New System.Windows.Forms.ListView
                            lvDownload.Items.Clear()
                            lvDownload.BeginUpdate()
                            axCZKEM1.EnableDevice(iMachineNumber, False)
                            axCZKEM1.ClearAdministrators(iMachineNumber)
                            axCZKEM1.EnableDevice(iMachineNumber, True)
                            axCZKEM1.Disconnect()
                        End If
                        'axCZKEM1.Disconnect()
                    ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "Bio1Eco" Then
                        vpszIPAddress = GridView1.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim()
                        XtraMasterTest.LabelControlStatus.Text = "Connecting " & vpszIPAddress & "..."
                        Application.DoEvents()
                        Dim adap, adap1 As SqlDataAdapter
                        Dim adapA, adapA1 As OleDbDataAdapter
                        Dim ds, ds1 As DataSet
                        commkey = Convert.ToInt32(GridView1.GetRowCellValue(rowHandle, GridView1.Columns("commkey")))
                        Dim mOpenFlag As Boolean
                        Dim mMK8001Device As Boolean 'TRUE:MK8001/8002Device;False:else

                        mMK8001Device = False
                        Dim si As SySystemInfo = New SySystemInfo
                        si.cSerialNum = New Byte((20) - 1) {}
                        si.cManuName = New Byte((24) - 1) {}
                        si.cDevName = New Byte((24) - 1) {}
                        si.cAlgVer = New Byte((16) - 1) {}
                        si.cFirmwareVer = New Byte((24) - 1) {}
                        Dim rret As Integer = 66
                        Try
                            rret = SyFunctions.CmdGetSystemInfo(si)
                        Catch ex As Exception

                        End Try

                        If (rret = CType(SyLastError.sleSuss, Int32)) Then
                            Dim sFirmwareVer As String = System.Text.Encoding.Default.GetString(si.cFirmwareVer)
                            If sFirmwareVer.Contains("MK8001/8002") Then
                                mMK8001Device = True
                            Else
                                mMK8001Device = False
                            End If
                        End If
                        Dim iFingerSize
                        If mMK8001Device Then
                            iFingerSize = 768
                        Else
                            iFingerSize = 512
                        End If
                        Dim ret As Int32 = CType(SyLastError.sleInvalidParam, Int32)
                        Dim equNo As UInt32 = Convert.ToUInt32(LstMachineId)
                        Dim gEquNo As UInt32 = equNo
                        If GridView1.GetRowCellValue(rowHandle, "A_R").ToString.Trim = "T" Then
                            Dim netCfg As SyNetCfg
                            Dim pswd As UInt32 = 0
                            Dim tmpPswd As String = commkey
                            If (tmpPswd = "0") Then
                                tmpPswd = "FFFFFFFF"
                            End If
                            Try
                                pswd = UInt32.Parse(tmpPswd, NumberStyles.HexNumber)
                            Catch ex As Exception
                                XtraMessageBox.Show(ulf, "<size=10>Incorrect Comm Key</size>", "Failed")
                                ret = CType(SyLastError.slePasswordError, Int32)
                            End Try
                            If (ret <> CType(SyLastError.slePasswordError, Int32)) Then
                                netCfg.mIsTCP = 1
                                netCfg.mPortNo = 8000 'Convert.ToUInt16(txtPortNo.Text)
                                netCfg.mIPAddr = New Byte((4) - 1) {}
                                Dim sArray() As String = vpszIPAddress.Split(Microsoft.VisualBasic.ChrW(46))
                                Dim j As Byte = 0
                                For Each i1 As String In sArray
                                    Try
                                        netCfg.mIPAddr(j) = Convert.ToByte(i1.Trim)
                                    Catch ex As System.Exception
                                        netCfg.mIPAddr(j) = 255
                                    End Try
                                    j = (j + 1)
                                    If j > 3 Then
                                        Exit For
                                    End If

                                Next
                            End If

                            Dim pnt As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(netCfg))
                            Try
                                Marshal.StructureToPtr(netCfg, pnt, False)
                                '        If optWifiDevice.Checked Then
                                'ret = SyFunctions.CmdOpenDevice(3, pnt, equNo, pswd, Convert.ToByte(chkTranceIO.Checked?1, :, 0))
                                '        Else
                                ret = SyFunctions.CmdOpenDevice(2, pnt, equNo, pswd, Convert.ToByte(1))
                                '        End If
                                '                'TODO: Warning!!!, inline IF is not supported ?
                                '                chkTranceIO.Checked()
                                '0:
                                '                '
                            Finally
                                Marshal.FreeHGlobal(pnt)
                            End Try
                        ElseIf GridView1.GetRowCellValue(rowHandle, "A_R").ToString.Trim = "S" Then
                            Dim equtype As String = "Finger Module USB Device"
                            Dim pswd As UInt32 = UInt32.Parse("FFFFFFFF", NumberStyles.HexNumber)
                            ret = SyFunctions.CmdOpenDeviceByUsb(equtype, equNo, pswd, Convert.ToByte(1))
                        End If
                        If (ret = CType(SyLastError.sleSuss, Int32)) Then
                            ret = SyFunctions.CmdTestConn2Device

                            ret = SyFunctions.CmdClearAdmin()

                            SyFunctions.CloseCom()
                            ' close com 
                            SyFunctions.CmdUnLockDevice()
                            SyFunctions.CloseCom()
                        Else
                            'XtraMessageBox.Show(ulf, "<size=10>Connection Failed</size>", "Failed")
                            Continue For
                        End If
                    ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "F9" Then
                        vpszNetPort = CLng("5005")
                        vpszNetPassword = CLng("0")
                        vnTimeOut = CLng("5000")
                        vnProtocolType = 0 'PROTOCOL_TCPIP
                        vnLicense = 7881
                        Dim F9 As mdlPublic_f9 = New mdlPublic_f9
                        If GridView1.GetRowCellValue(rowHandle, "A_R").ToString.Trim = "S" Then
                            vnResultCode = F9.FK_ConnectUSB(vnMachineNumber, vnLicense)
                        Else
                            vnResultCode = F9.FK_ConnectNet(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
                        End If
                        If vnResultCode > 0 Then
                            bConn = True
                        Else
                            failIP.Add(vpszIPAddress)
                            bConn = False
                        End If
                        If vnResultCode > 0 Then
                            vnResultCode = F9.FK_EnableDevice(vnResultCode, 0)
                            'vnResultCode = FK623Attend.BenumbAllManager()
                            vnResultCode = F9.FK_BenumbAllManager(1)
                            If vnResultCode <> RUN_SUCCESS Then
                                'MsgBox("Unable to clear Admin")
                                XtraMessageBox.Show(ulf, "<size=10>Unable to clear Admin</size>", "<size=9>Information</size>")
                            End If
                        Else
                            'MsgBox("Unable to connect Device No - " & mMachineNumber)
                            failIP.Add(vpszIPAddress)
                            'XtraMessageBox.Show(ulf, "<size=10>Unable to connect Device No - " & mMachineNumber & "</size>", "<size=9>Information</size>")
                        End If
                        'FK623Attend.Disconnect
                        vnResultCode = F9.FK_EnableDevice(vnResultCode, 1)
                        F9.FK_DisConnect(vnResultCode)
                    ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "ATF686n" Then
                        vpszNetPort = CLng("5005")
                        vpszNetPassword = CLng("0")
                        vnTimeOut = CLng("5000")
                        vnProtocolType = 0
                        vnLicense = 7881
                        Dim atf686n As mdlFunction_Atf686n = New mdlFunction_Atf686n
                        If GridView1.GetRowCellValue(rowHandle, "A_R").ToString.Trim = "S" Then
                            vnResult = atf686n.ST_ConnectUSB(GridView1.GetRowCellValue(rowHandle, "ID_NO").ToString.Trim, vnLicense)
                        Else
                            vnResult = atf686n.ConnectNet(vpszIPAddress)
                        End If
                        If vnResult <= 0 Then
                                failIP.Add(vpszIPAddress)
                                Continue For
                            End If
                            vnResultCode = atf686n.ST_BenumbAllManager(vnResult)

                        atf686n.ST_EnableDevice(vnResult, 1)
                        atf686n.ST_DisConnect(vnResult)
                    ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "TF-01" Then
                        vpszNetPort = CLng("5005")
                        vpszNetPassword = CLng("0")
                        vnTimeOut = CLng("5000")
                        vnProtocolType = 0 'PROTOCOL_TCPIP
                        vnLicense = 7881
                        Dim bRet As Boolean

                        bRet = AxFP_CLOCK1.SetIPAddress(vpszIPAddress, vpszNetPort, vpszNetPassword)
                        AxFP_CLOCK1.OpenCommPort(vnMachineNumber)
                        If bRet Then
                            bConn = True
                        Else
                            bConn = False
                            failIP.Add(vpszIPAddress)
                        End If
                        If bConn Then
                            AxFP_CLOCK1.EnableDevice(vnMachineNumber, False)
                            bConn = AxFP_CLOCK1.BenumbAllManager(vnMachineNumber)
                            If Not bConn Then
                                'MsgBox("Unable to clear Admin")
                                XtraMessageBox.Show(ulf, "<size=10>Unable to Clear Device Data</size>", "<size=9>Information</size>")
                            End If
                        Else
                            failIP.Add(vpszIPAddress)
                            'XtraMessageBox.Show(ulf, "<size=10>Unable to connect Device No - " & mMachineNumber & "</size>", "<size=9>Information</size>")
                        End If
                        AxFP_CLOCK1.EnableDevice(vnMachineNumber, True)
                        AxFP_CLOCK1.CloseCommPort()
                    End If
                End If
            Next
            For Each c As Control In Me.Controls
                c.Enabled = True
            Next

            Dim failedIpArr() As String = failIP.Distinct.ToArray
            Dim failIpStr As String = ""
            If failedIpArr.Length > 0 Then
                For i As Integer = 0 To failedIpArr.Length - 1
                    failIpStr = failIpStr & "," & failedIpArr(i)
                Next
                failIpStr = failIpStr.TrimStart(",")
                XtraMessageBox.Show(ulf, "<size=10>" & failIpStr & " failed to download data</size>", "Failed")
            Else
                XtraMessageBox.Show(ulf, "<size=10>Task Completed</size>", "Success")
            End If

            TextPassword.Text = ""
            Me.Cursor = Cursors.Default
            XtraMasterTest.LabelControlStatus.Text = ""
            Application.DoEvents()
            XtraMessageBox.Show(ulf, "<size=10>Task Completed</size>", "<size=9>Information</size>")
        End If
    End Sub
    Private Sub SimpleButton1_Click_1(sender As System.Object, e As System.EventArgs) Handles SimpleButton1.Click
        Me.Close()
    End Sub
    Private Sub btnClearDeviceData_Click(sender As System.Object, e As System.EventArgs) Handles btnClearDeviceData.Click
        Dim bConn As Boolean
        Dim lngMachineNum As Long
        Dim mCommKey As Long
        Dim lpszIPAddress As String
        Dim vEMachineNumber As Long
        Dim vEnrollNumber As Long
        Dim vFingerNumber As Long
        Dim vPrivilege As Long
        Dim vEnable As Long
        Dim vRet As Boolean
        Dim mKey As String
        Dim vnResultCode As Long
        Dim RsDevice As New DataSet 'ADODB.Recordset
        vnResultCode = 0
        Dim vnMachineNumber As Long

        Dim vstrTelNumber As String
        Dim vnWaitDialTime As Long
        Dim vnLicense As Long
        Dim vpszIPAddress As String
        Dim vpszNetPort As Long
        Dim vpszNetPassword As Long
        Dim vnCommPort As Long
        Dim vnCommBaudrate As Long

        If GridView1.GetSelectedRows.Count = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>Please select the Machine</size>", "<size=9>Error</size>")
            Exit Sub
        End If
        If TextPassword.Text.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Please Enter Admin Password</size>", "<size=9>Error</size>")
            TextPassword.Select()
            Exit Sub
        Else
            If TextPassword.Text.ToLower <> Common.AdminPASSWORD.ToLower Then
                XtraMessageBox.Show(ulf, "<size=10>Admin Password Incorrect</size>", "<size=9>Error</size>")
                TextPassword.Select()
                Exit Sub
            End If
        End If
        If XtraMessageBox.Show(ulf, "<size=10>Are you sure to Clear Device Data for selected machine</size>", Common.res_man.GetString("confirmdeleteion", Common.cul), _
                             MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
            Me.Cursor = Cursors.WaitCursor
            For Each c As Control In Me.Controls
                c.Enabled = False
            Next
            Dim sSql As String = ""
            Dim selectedRows As Integer() = GridView1.GetSelectedRows()
            Dim result As Object() = New Object(selectedRows.Length - 1) {}
            Dim LstMachineId As String
            Dim mMachineNumber As Long
            Dim commkey As Integer
            Dim failIP As New List(Of String)()
            For i = 0 To selectedRows.Length - 1
                Dim rowHandle As Integer = selectedRows(i)
                If Not GridView1.IsGroupRow(rowHandle) Then
                    LstMachineId = GridView1.GetRowCellValue(rowHandle, "ID_NO").ToString.Trim
                    Common.LogPost("Device Clear Data; Device ID: " & LstMachineId)
                    bConn = False
                    vnMachineNumber = LstMachineId
                    vnLicense = 1261 '1789 '
                    lpszIPAddress = GridView1.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim 'Trim(rstm!Location)
                    vpszIPAddress = Trim(lpszIPAddress)

                    If GridView1.GetRowCellValue(rowHandle, "DeviceType") = "Bio-1/3/4/5/6/7/ATF-203/ATF-395/ACR-872" Then
                        vpszNetPort = CLng("5005")
                        vpszNetPassword = CLng("0")
                        vnTimeOut = CLng("5000")
                        vnProtocolType = 0 'PROTOCOL_TCPIP
                        vnLicense = 1261
                        'bConn = FK623Attend.ConnectNet(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
                        If GridView1.GetRowCellValue(rowHandle, "A_R").ToString.Trim = "S" Then
                            vnResultCode = FK_ConnectUSB(vnMachineNumber, vnLicense)
                        Else
                            vpszIPAddress = Trim(lpszIPAddress)
                            vpszNetPort = CLng("5005")
                            vpszNetPassword = CLng("0")
                            vnTimeOut = CLng("5000")
                            vnProtocolType = 0
                            vnResultCode = FK_ConnectNet(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
                            'vnResultCode = FK_ConnectNet(mMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
                        End If
                        If vnResultCode > 0 Then
                            bConn = True
                        Else
                            bConn = False
                            failIP.Add(vpszIPAddress)
                        End If
                        If vnResultCode > 0 Then
                            Dim vstrData As String = New String(CType(ChrW(32), Char), 256)
                            Dim ret = FK_GetProductData(vnResultCode, CType(1, Integer), vstrData)
                            If license.LicenseKey <> "" Then
                                If license.DeviceSerialNo.Contains(vstrData.Trim) Then
                                Else
                                    FK_DisConnect(vnResultCode)
                                    'Return "Invalid Serial number " & vstrData
                                    XtraMessageBox.Show(ulf, "<size=10>Invalid Serial number " & vstrData & "</size>", "Failed")
                                    Continue For
                                End If
                            End If
                            vnResultCode = FK_EnableDevice(vnResultCode, 0)
                            'vnResultCode = FK623Attend.BenumbAllManager()
                            'vnResultCode = FK_BenumbAllManager(1)
                            vnResultCode = FK_ClearKeeperData(vnResultCode)
                            If vnResultCode <> RUN_SUCCESS Then
                                'MsgBox("Unable to clear Admin")
                                XtraMessageBox.Show(ulf, "<size=10>Unable to Clear Device Data</size>", "<size=9>Information</size>")
                            End If
                        Else
                            'MsgBox("Unable to connect Device No - " & mMachineNumber)
                            failIP.Add(vpszIPAddress)
                            'XtraMessageBox.Show(ulf, "<size=10>Unable to connect Device No - " & mMachineNumber & "</size>", "<size=9>Information</size>")
                        End If
                        'FK623Attend.Disconnect
                        vnResultCode = FK_EnableDevice(vnResultCode, 1)
                        FK_DisConnect(vnResultCode)
                    ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "ZK(TFT)" Or GridView1.GetRowCellValue(rowHandle, "DeviceType") = "Bio-1Pro/ATF305Pro/ATF686Pro" Then
                        Dim sdwEnrollNumber As Integer
                        Dim idwVerifyMode As Integer
                        Dim idwWorkcode As Integer

                        Dim bIsConnected = False
                        Dim iMachineNumber As Integer = result(i)
                        Dim idwErrorCode As Integer
                        Dim com As Common = New Common
                        Dim axCZKEM1 As New zkemkeeper.CZKEM
                        commkey = Convert.ToInt32(GridView1.GetRowCellValue(rowHandle, GridView1.Columns("commkey")))
                        Dim sName As String = ""
                        Dim sPassword As String = ""
                        Dim iPrivilege As Integer
                        Dim bEnabled As Boolean = False
                        Dim idwFingerIndex As Integer
                        Dim sTmpData As String = ""
                        Dim iTmpLength As Integer
                        Dim iFlag As Integer
                        Dim flagenabled As String = ""
                        Dim adap As SqlDataAdapter
                        Dim adapA As OleDbDataAdapter
                        Dim dsRecord As DataSet = New DataSet
                        axCZKEM1.SetCommPassword(Convert.ToInt32(commkey))  'to check device commkey and db commkey matches
                        bIsConnected = axCZKEM1.Connect_Net(vpszIPAddress, 4370)
                        If bIsConnected = True Then
                            Dim lvDownload As System.Windows.Forms.ListView = New System.Windows.Forms.ListView
                            lvDownload.Items.Clear()
                            lvDownload.BeginUpdate()
                            axCZKEM1.EnableDevice(iMachineNumber, False)
                            axCZKEM1.ClearKeeperData(iMachineNumber)
                            'axCZKEM1.ClearAdministrators(iMachineNumber)
                            axCZKEM1.EnableDevice(iMachineNumber, True)
                            axCZKEM1.Disconnect()
                        End If
                        'axCZKEM1.Disconnect()
                    ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "Bio1Eco" Then
                        vpszIPAddress = GridView1.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim()
                        XtraMasterTest.LabelControlStatus.Text = "Connecting " & vpszIPAddress & "..."
                        Application.DoEvents()
                        Dim adap, adap1 As SqlDataAdapter
                        Dim adapA, adapA1 As OleDbDataAdapter
                        Dim ds, ds1 As DataSet
                        commkey = Convert.ToInt32(GridView1.GetRowCellValue(rowHandle, GridView1.Columns("commkey")))
                        Dim mOpenFlag As Boolean
                        Dim mMK8001Device As Boolean 'TRUE:MK8001/8002Device;False:else

                        mMK8001Device = False
                        Dim si As SySystemInfo = New SySystemInfo
                        si.cSerialNum = New Byte((20) - 1) {}
                        si.cManuName = New Byte((24) - 1) {}
                        si.cDevName = New Byte((24) - 1) {}
                        si.cAlgVer = New Byte((16) - 1) {}
                        si.cFirmwareVer = New Byte((24) - 1) {}
                        Dim rret As Integer = 66
                        Try
                            rret = SyFunctions.CmdGetSystemInfo(si)
                        Catch ex As Exception

                        End Try

                        If (rret = CType(SyLastError.sleSuss, Int32)) Then
                            Dim sFirmwareVer As String = System.Text.Encoding.Default.GetString(si.cFirmwareVer)
                            If sFirmwareVer.Contains("MK8001/8002") Then
                                mMK8001Device = True
                            Else
                                mMK8001Device = False
                            End If
                        End If
                        Dim iFingerSize
                        If mMK8001Device Then
                            iFingerSize = 768
                        Else
                            iFingerSize = 512
                        End If
                        Dim ret As Int32 = CType(SyLastError.sleInvalidParam, Int32)
                        Dim equNo As UInt32 = Convert.ToUInt32(LstMachineId)
                        Dim gEquNo As UInt32 = equNo
                        If GridView1.GetRowCellValue(rowHandle, "A_R").ToString.Trim = "T" Then
                            Dim netCfg As SyNetCfg
                            Dim pswd As UInt32 = 0
                            Dim tmpPswd As String = commkey
                            If (tmpPswd = "0") Then
                                tmpPswd = "FFFFFFFF"
                            End If
                            Try
                                pswd = UInt32.Parse(tmpPswd, NumberStyles.HexNumber)
                            Catch ex As Exception
                                XtraMessageBox.Show(ulf, "<size=10>Incorrect Comm Key</size>", "Failed")
                                ret = CType(SyLastError.slePasswordError, Int32)
                            End Try
                            If (ret <> CType(SyLastError.slePasswordError, Int32)) Then
                                netCfg.mIsTCP = 1
                                netCfg.mPortNo = 8000 'Convert.ToUInt16(txtPortNo.Text)
                                netCfg.mIPAddr = New Byte((4) - 1) {}
                                Dim sArray() As String = vpszIPAddress.Split(Microsoft.VisualBasic.ChrW(46))
                                Dim j As Byte = 0
                                For Each i1 As String In sArray
                                    Try
                                        netCfg.mIPAddr(j) = Convert.ToByte(i1.Trim)
                                    Catch ex As System.Exception
                                        netCfg.mIPAddr(j) = 255
                                    End Try
                                    j = (j + 1)
                                    If j > 3 Then
                                        Exit For
                                    End If

                                Next
                            End If

                            Dim pnt As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(netCfg))
                            Try
                                Marshal.StructureToPtr(netCfg, pnt, False)
                                '        If optWifiDevice.Checked Then
                                'ret = SyFunctions.CmdOpenDevice(3, pnt, equNo, pswd, Convert.ToByte(chkTranceIO.Checked?1, :, 0))
                                '        Else
                                ret = SyFunctions.CmdOpenDevice(2, pnt, equNo, pswd, Convert.ToByte(1))
                                '        End If
                                '                'TODO: Warning!!!, inline IF is not supported ?
                                '                chkTranceIO.Checked()
                                '0:
                                '                '
                            Finally
                                Marshal.FreeHGlobal(pnt)
                            End Try
                        ElseIf GridView1.GetRowCellValue(rowHandle, "A_R").ToString.Trim = "S" Then
                            Dim equtype As String = "Finger Module USB Device"
                            Dim pswd As UInt32 = UInt32.Parse("FFFFFFFF", NumberStyles.HexNumber)
                            ret = SyFunctions.CmdOpenDeviceByUsb(equtype, equNo, pswd, Convert.ToByte(1))
                        End If
                        If (ret = CType(SyLastError.sleSuss, Int32)) Then
                            ret = SyFunctions.CmdTestConn2Device

                            ret = SyFunctions.CmdClearAllUser()

                            SyFunctions.CloseCom()
                            ' close com 
                            SyFunctions.CmdUnLockDevice()
                            SyFunctions.CloseCom()
                        Else
                            'XtraMessageBox.Show(ulf, "<size=10>Connection Failed</size>", "Failed")
                            Continue For
                        End If
                    ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "EF45" Then
                        XtraMasterTest.LabelControlStatus.Text = "Connecting " & vpszIPAddress & "..."
                        Application.DoEvents()
                        Dim cn As Common = New Common
                        Dim _client As Client = Nothing
                        Dim connectOk As Boolean = False
                        Try
                            _client = cn.initClientEF45(vpszIPAddress)
                            connectOk = _client.StealConnect '_client.Connect()
                            If connectOk Then
                                Try
                                    _client.DeleteAllUsers()
                                Catch ex As Exception

                                End Try
                                Try
                                    _client.DeleteLogDB()
                                Catch ex As Exception

                                End Try
                                _client.Disconnect()
                            Else
                                'failIP.Add(vpszIPAddress.ToString)
                                Continue For
                            End If
                        Catch ex As Exception
                            'failIP.Add(vpszIPAddress.ToString)
                            Continue For
                        End Try
                    ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "F9" Then
                        vpszNetPort = CLng("5005")
                        vpszNetPassword = CLng("0")
                        vnTimeOut = CLng("5000")
                        vnProtocolType = 0 'PROTOCOL_TCPIP
                        vnLicense = 7881
                        Dim F9 As mdlPublic_f9 = New mdlPublic_f9
                        If GridView1.GetRowCellValue(rowHandle, "A_R").ToString.Trim = "S" Then
                            vnResultCode = F9.FK_ConnectUSB(vnMachineNumber, vnLicense)
                        Else
                            vnResultCode = F9.FK_ConnectNet(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
                        End If
                        If vnResultCode > 0 Then
                            bConn = True
                        Else
                            bConn = False
                            failIP.Add(vpszIPAddress)
                        End If
                        If vnResultCode > 0 Then
                            vnResultCode = F9.FK_EnableDevice(vnResultCode, 0)
                            vnResultCode = F9.FK_ClearKeeperData(vnResultCode)
                            If vnResultCode <> RUN_SUCCESS Then
                                'MsgBox("Unable to clear Admin")
                                XtraMessageBox.Show(ulf, "<size=10>Unable to Clear Device Data</size>", "<size=9>Information</size>")
                            End If
                        Else
                            failIP.Add(vpszIPAddress)
                            'XtraMessageBox.Show(ulf, "<size=10>Unable to connect Device No - " & mMachineNumber & "</size>", "<size=9>Information</size>")
                        End If
                        vnResultCode = F9.FK_EnableDevice(vnResultCode, 1)
                        F9.FK_DisConnect(vnResultCode)
                    ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "ATF686n" Then
                        vpszNetPort = CLng("5005")
                        vpszNetPassword = CLng("0")
                        vnTimeOut = CLng("5000")
                        vnProtocolType = 0
                        vnLicense = 7881
                        Dim atf686n As mdlFunction_Atf686n = New mdlFunction_Atf686n
                        If GridView1.GetRowCellValue(rowHandle, "A_R").ToString.Trim = "S" Then
                            vnResult = atf686n.ST_ConnectUSB(GridView1.GetRowCellValue(rowHandle, "ID_NO").ToString.Trim, vnLicense)
                        Else
                            vnResult = atf686n.ConnectNet(vpszIPAddress)
                        End If
                        If vnResult <= 0 Then
                            failIP.Add(vpszIPAddress)
                        End If
                        vnResultCode = atf686n.ST_ClearAllData(vnResult)

                        atf686n.ST_EnableDevice(vnResult, 1)
                        atf686n.ST_DisConnect(vnResult)

                    ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "TF-01" Then
                        vpszNetPort = CLng("5005")
                        vpszNetPassword = CLng("0")
                        vnTimeOut = CLng("5000")
                        vnProtocolType = 0 'PROTOCOL_TCPIP
                        vnLicense = 7881
                        Dim bRet As Boolean
                        AxFP_CLOCK1.OpenCommPort(vnMachineNumber)
                        bRet = AxFP_CLOCK1.SetIPAddress(vpszIPAddress, vpszNetPort, vpszNetPassword)
                        If bRet Then
                            bConn = True
                        Else
                            bConn = False
                            failIP.Add(vpszIPAddress)
                        End If
                        If bConn Then
                            AxFP_CLOCK1.EnableDevice(vnMachineNumber, 0)
                            bConn = AxFP_CLOCK1.ClearKeeperData(vnMachineNumber)
                            If Not bConn Then
                                'MsgBox("Unable to clear Admin")
                                XtraMessageBox.Show(ulf, "<size=10>Unable to Clear Device Data</size>", "<size=9>Information</size>")
                            End If
                        Else
                            failIP.Add(vpszIPAddress)
                            'XtraMessageBox.Show(ulf, "<size=10>Unable to connect Device No - " & mMachineNumber & "</size>", "<size=9>Information</size>")
                        End If
                        AxFP_CLOCK1.EnableDevice(vnMachineNumber, 1)
                        AxFP_CLOCK1.CloseCommPort()
                    ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "Ultra800" Then
                        If license.LicenseKey <> "" Then
                            If license.DeviceSerialNo.Contains(GridView1.GetRowCellValue(rowHandle, GridView1.Columns("MAC_ADDRESS")).ToString.Trim) Then
                            Else
                                XtraMessageBox.Show(ulf, "<size=10>Invalid Serial number " & GridView1.GetRowCellValue(rowHandle, GridView1.Columns("MAC_ADDRESS")).ToString.Trim & "</size>", "Failed")
                                Continue For
                            End If
                        End If
                        Dim commKey1 As String = GridView1.GetRowCellValue(rowHandle, GridView1.Columns("commkey")).ToString.Trim
                        vpszIPAddress = Trim(lpszIPAddress)
                        'XtraMasterTest.LabelControlStatus.Text = "Deleting Template " & mEmp
                        'Application.DoEvents()
                        Dim UD As UserDelete = New UserDelete
                        UD.pass = commKey1
                        UD.id = "-1"
                        Dim jsons As String = JsonConvert.SerializeObject(UD)
                        Dim link As String = "http://" & vpszIPAddress & ":8090/person/delete"
                        Dim httpWebRequest As WebRequest = WebRequest.Create(link) 'Live
                        httpWebRequest.ContentType = "application/json"
                        httpWebRequest.Method = "POST"
                        Using streamWriter = New StreamWriter(httpWebRequest.GetRequestStream())
                            streamWriter.Write(jsons)
                            streamWriter.Flush()
                            streamWriter.Close()
                        End Using
                        Dim httpResponse = CType(httpWebRequest.GetResponse(), HttpWebResponse)
                            Using streamReader = New StreamReader(httpResponse.GetResponseStream())
                                'Dim streamReader = New StreamReader(httpResponse.GetResponseStream)
                                Dim result1 = streamReader.ReadToEnd()
                                Dim fs As FileStream = New FileStream("JsonResponse.txt", FileMode.OpenOrCreate, FileAccess.Write)
                                Dim sw As StreamWriter = New StreamWriter(fs)
                                sw.BaseStream.Seek(0, SeekOrigin.End)
                                sw.WriteLine(vbCrLf & link & ", " & result1 & "::" & DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"))
                                sw.Flush()
                                sw.Close()
                            End Using
                        End If
                    End If
                Next
                For Each c As Control In Me.Controls
                    c.Enabled = True
                Next
                TextPassword.Text = ""
                Me.Cursor = Cursors.Default
                XtraMasterTest.LabelControlStatus.Text = ""
                Application.DoEvents()
                Dim failedIpArr() As String = failIP.Distinct.ToArray
                Dim failIpStr As String = ""
                If failedIpArr.Length > 0 Then
                    For i As Integer = 0 To failedIpArr.Length - 1
                        failIpStr = failIpStr & "," & failedIpArr(i)
                    Next
                    failIpStr = failIpStr.TrimStart(",")
                    XtraMessageBox.Show(ulf, "<size=10>" & failIpStr & " failed to download data</size>", "Failed")
                Else
                    XtraMessageBox.Show(ulf, "<size=10>Task Completed</size>", "Success")
                End If


                'XtraMessageBox.Show(ulf, "<size=10>Task Completed</size>", "<size=9>Information</size>")
            End If
        End Sub
        Private Sub btnLockOpen_Click(sender As System.Object, e As System.EventArgs) Handles btnLockOpen.Click
            Dim bConn As Boolean
            Dim lngMachineNum As Long
            Dim mCommKey As Long
            Dim lpszIPAddress As String
            Dim vEMachineNumber As Long
            Dim vEnrollNumber As Long
            Dim vFingerNumber As Long
            Dim vPrivilege As Long
            Dim vEnable As Long
            Dim vRet As Boolean
            Dim mKey As String
            Dim vnResultCode As Long
            Dim RsDevice As New DataSet 'ADODB.Recordset
            vnResultCode = 0
            Dim vnMachineNumber As Long

        Dim vstrTelNumber As String
        Dim vnWaitDialTime As Long
        Dim vnLicense As Long
        Dim vpszIPAddress As String
        Dim vpszNetPort As Long
        Dim vpszNetPassword As Long
        Dim vnCommPort As Long
        Dim vnCommBaudrate As Long

        If GridView1.GetSelectedRows.Count = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>Please select the Machine</size>", "<size=9>Error</size>")
            Exit Sub
        End If
        If TextPassword.Text.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Please Enter Admin Password</size>", "<size=9>Error</size>")
            TextPassword.Select()
            Exit Sub
        Else
            If TextPassword.Text.ToLower <> Common.PASSWORD.ToLower Then
                XtraMessageBox.Show(ulf, "<size=10>Admin Password Incorrect</size>", "<size=9>Error</size>")
                TextPassword.Select()
                Exit Sub
            End If
        End If
        If XtraMessageBox.Show(ulf, "<size=10>Are you sure to Open Door Lock for selected machine</size>", Common.res_man.GetString("confirmdeleteion", Common.cul), _
                             MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
            Me.Cursor = Cursors.WaitCursor
            For Each c As Control In Me.Controls
                c.Enabled = False
            Next
            Dim sSql As String = ""
            Dim selectedRows As Integer() = GridView1.GetSelectedRows()
            Dim result As Object() = New Object(selectedRows.Length - 1) {}
            Dim LstMachineId As String
            Dim mMachineNumber As Long
            Dim commkey As Integer
            For i = 0 To selectedRows.Length - 1
                Dim rowHandle As Integer = selectedRows(i)
                If Not GridView1.IsGroupRow(rowHandle) Then
                    LstMachineId = GridView1.GetRowCellValue(rowHandle, "ID_NO").ToString.Trim
                    Common.LogPost("Device Door Open; Device ID: " & LstMachineId)
                    bConn = False
                    vnMachineNumber = LstMachineId
                    vnLicense = 1261 '1789 '
                    lpszIPAddress = GridView1.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim 'Trim(rstm!Location)
                    vpszIPAddress = Trim(lpszIPAddress)

                    If GridView1.GetRowCellValue(rowHandle, "DeviceType") = "Bio-1/3/4/5/6/7/ATF-203/ATF-395/ACR-872" Then
                        'vpszNetPort = CLng("5005")
                        'vpszNetPassword = CLng("0")
                        'vnTimeOut = CLng("5000")
                        'vnProtocolType = 0 'PROTOCOL_TCPIP
                        ''bConn = FK623Attend.ConnectNet(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
                        'If GridView1.GetRowCellValue(rowHandle, "A_R").ToString.Trim = "S" Then
                        '    vnResultCode = FK_ConnectUSB(vnMachineNumber, vnLicense)
                        'Else
                        '    vpszIPAddress = Trim(lpszIPAddress)
                        '    vpszNetPort = CLng("5005")
                        '    vpszNetPassword = CLng("0")
                        '    vnTimeOut = CLng("5000")
                        '    vnProtocolType = 0
                        '    vnResultCode = FK_ConnectNet(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
                        '    'vnResultCode = FK_ConnectNet(mMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
                        'End If
                        'If vnResultCode > 0 Then
                        '    bConn = True
                        'Else
                        '    bConn = False
                        'End If
                        'If vnResultCode > 0 Then
                        '    vnResultCode = FK_EnableDevice(vnResultCode, 0)
                        '    'vnResultCode = FK623Attend.BenumbAllManager()
                        '    'vnResultCode = FK_BenumbAllManager(1)
                        '    vnResultCode = FK_ClearKeeperData(vnResultCode)
                        '    If vnResultCode <> RUN_SUCCESS Then
                        '        'MsgBox("Unable to clear Admin")
                        '        XtraMessageBox.Show(ulf, "<size=10>Unable to Clear Device Data</size>", "<size=9>Information</size>")
                        '    End If
                        'Else
                        '    'MsgBox("Unable to connect Device No - " & mMachineNumber)
                        '    XtraMessageBox.Show(ulf, "<size=10>Unable to connect Device No - " & mMachineNumber & "</size>", "<size=9>Information</size>")
                        'End If
                        ''FK623Attend.Disconnect
                        'vnResultCode = FK_EnableDevice(vnResultCode, 1)
                        'FK_DisConnect(vnResultCode)
                    ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "ZK(TFT)" Or GridView1.GetRowCellValue(rowHandle, "DeviceType") = "Bio-1Pro/ATF305Pro/ATF686Pro" Then
                        Dim sdwEnrollNumber As Integer
                        Dim idwVerifyMode As Integer
                        Dim idwWorkcode As Integer

                        Dim bIsConnected = False
                        Dim iMachineNumber As Integer = result(i)
                        Dim idwErrorCode As Integer
                        Dim com As Common = New Common
                        Dim axCZKEM1 As New zkemkeeper.CZKEM
                        commkey = Convert.ToInt32(GridView1.GetRowCellValue(rowHandle, GridView1.Columns("commkey")))
                        Dim sName As String = ""
                        Dim sPassword As String = ""
                        Dim iPrivilege As Integer
                        Dim bEnabled As Boolean = False
                        Dim idwFingerIndex As Integer
                        Dim sTmpData As String = ""
                        Dim iTmpLength As Integer
                        Dim iFlag As Integer
                        Dim flagenabled As String = ""
                        Dim adap As SqlDataAdapter
                        Dim adapA As OleDbDataAdapter
                        Dim dsRecord As DataSet = New DataSet
                        axCZKEM1.SetCommPassword(Convert.ToInt32(commkey))  'to check device commkey and db commkey matches
                        bIsConnected = axCZKEM1.Connect_Net(vpszIPAddress, 4370)
                        If bIsConnected = True Then
                            Dim lvDownload As System.Windows.Forms.ListView = New System.Windows.Forms.ListView
                            lvDownload.Items.Clear()
                            lvDownload.BeginUpdate()
                            axCZKEM1.EnableDevice(iMachineNumber, False)
                            axCZKEM1.ACUnlock(iMachineNumber, 10)
                            'axCZKEM1.ClearAdministrators(iMachineNumber)
                            axCZKEM1.EnableDevice(iMachineNumber, True)
                            axCZKEM1.Disconnect()
                        End If
                        'axCZKEM1.Disconnect()
                    ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "Bio1Eco" Then
                        vpszIPAddress = GridView1.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim()
                        XtraMasterTest.LabelControlStatus.Text = "Connecting " & vpszIPAddress & "..."
                        Application.DoEvents()
                        Dim adap, adap1 As SqlDataAdapter
                        Dim adapA, adapA1 As OleDbDataAdapter
                        Dim ds, ds1 As DataSet
                        commkey = Convert.ToInt32(GridView1.GetRowCellValue(rowHandle, GridView1.Columns("commkey")))
                        Dim mOpenFlag As Boolean
                        Dim mMK8001Device As Boolean 'TRUE:MK8001/8002Device;False:else

                        mMK8001Device = False
                        Dim si As SySystemInfo = New SySystemInfo
                        si.cSerialNum = New Byte((20) - 1) {}
                        si.cManuName = New Byte((24) - 1) {}
                        si.cDevName = New Byte((24) - 1) {}
                        si.cAlgVer = New Byte((16) - 1) {}
                        si.cFirmwareVer = New Byte((24) - 1) {}
                        Dim rret As Integer = 66
                        Try
                            rret = SyFunctions.CmdGetSystemInfo(si)
                        Catch ex As Exception

                        End Try

                        If (rret = CType(SyLastError.sleSuss, Int32)) Then
                            Dim sFirmwareVer As String = System.Text.Encoding.Default.GetString(si.cFirmwareVer)
                            If sFirmwareVer.Contains("MK8001/8002") Then
                                mMK8001Device = True
                            Else
                                mMK8001Device = False
                            End If
                        End If
                        Dim iFingerSize
                        If mMK8001Device Then
                            iFingerSize = 768
                        Else
                            iFingerSize = 512
                        End If
                        Dim ret As Int32 = CType(SyLastError.sleInvalidParam, Int32)
                        Dim equNo As UInt32 = Convert.ToUInt32(LstMachineId)
                        Dim gEquNo As UInt32 = equNo
                        If GridView1.GetRowCellValue(rowHandle, "A_R").ToString.Trim = "T" Then
                            Dim netCfg As SyNetCfg
                            Dim pswd As UInt32 = 0
                            Dim tmpPswd As String = commkey
                            If (tmpPswd = "0") Then
                                tmpPswd = "FFFFFFFF"
                            End If
                            Try
                                pswd = UInt32.Parse(tmpPswd, NumberStyles.HexNumber)
                            Catch ex As Exception
                                XtraMessageBox.Show(ulf, "<size=10>Incorrect Comm Key</size>", "Failed")
                                ret = CType(SyLastError.slePasswordError, Int32)
                            End Try
                            If (ret <> CType(SyLastError.slePasswordError, Int32)) Then
                                netCfg.mIsTCP = 1
                                netCfg.mPortNo = 8000 'Convert.ToUInt16(txtPortNo.Text)
                                netCfg.mIPAddr = New Byte((4) - 1) {}
                                Dim sArray() As String = vpszIPAddress.Split(Microsoft.VisualBasic.ChrW(46))
                                Dim j As Byte = 0
                                For Each i1 As String In sArray
                                    Try
                                        netCfg.mIPAddr(j) = Convert.ToByte(i1.Trim)
                                    Catch ex As System.Exception
                                        netCfg.mIPAddr(j) = 255
                                    End Try
                                    j = (j + 1)
                                    If j > 3 Then
                                        Exit For
                                    End If

                                Next
                            End If

                            Dim pnt As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(netCfg))
                            Try
                                Marshal.StructureToPtr(netCfg, pnt, False)
                                '        If optWifiDevice.Checked Then
                                'ret = SyFunctions.CmdOpenDevice(3, pnt, equNo, pswd, Convert.ToByte(chkTranceIO.Checked?1, :, 0))
                                '        Else
                                ret = SyFunctions.CmdOpenDevice(2, pnt, equNo, pswd, Convert.ToByte(1))
                                '        End If
                                '                'TODO: Warning!!!, inline IF is not supported ?
                                '                chkTranceIO.Checked()
                                '0:
                                '                '
                            Finally
                                Marshal.FreeHGlobal(pnt)
                            End Try
                        ElseIf GridView1.GetRowCellValue(rowHandle, "A_R").ToString.Trim = "S" Then
                            Dim equtype As String = "Finger Module USB Device"
                            Dim pswd As UInt32 = UInt32.Parse("FFFFFFFF", NumberStyles.HexNumber)
                            ret = SyFunctions.CmdOpenDeviceByUsb(equtype, equNo, pswd, Convert.ToByte(1))
                        End If
                        If (ret = CType(SyLastError.sleSuss, Int32)) Then
                            ret = SyFunctions.CmdTestConn2Device

                            ret = SyFunctions.CmdOpenDoor()

                            SyFunctions.CloseCom()
                            ' close com 
                            SyFunctions.CmdUnLockDevice()
                            SyFunctions.CloseCom()
                        Else
                            'XtraMessageBox.Show(ulf, "<size=10>Connection Failed</size>", "Failed")
                            Continue For
                        End If
                    End If
                End If
            Next
            For Each c As Control In Me.Controls
                c.Enabled = True
            Next
            TextPassword.Text = ""
            Me.Cursor = Cursors.Default
            XtraMasterTest.LabelControlStatus.Text = ""
            Application.DoEvents()
            XtraMessageBox.Show(ulf, "<size=10>Task Completed</size>", "<size=9>Information</size>")
        End If
    End Sub
    Private Sub EnableDiableUser(ByVal enbleValue As Boolean)
        Dim m As Integer
        'Dim e As Integer
        Dim lngMachineNum As Long
        Dim mCommKey As Long
        Dim lpszIPAddress As String
        Dim mKey As String
        Dim vnMachineNumber As Long
        Dim vnCommPort As Long
        Dim vnCommBaudrate As Long
        Dim vstrTelNumber As String
        Dim vnWaitDialTime As Long
        Dim vnLicense As Long
        Dim vpszIPAddress As String
        Dim vpszNetPort As Long
        Dim vpszNetPassword As Long
        Dim vnTimeOut As Long
        Dim vnProtocolType As Long
        Dim strDateTime As String
        Dim vRet As Long
        Dim vPrivilege As Long

        If GridView1.GetSelectedRows.Count = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>Please select the Machine</size>", "<size=9>Error</size>")
            Exit Sub
        End If

        If GridView2.GetSelectedRows.Count = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>Please select Employee</size>", "<size=9>Error</size>")
            Exit Sub
        End If
        Dim sSql As String = ""
        Me.Cursor = Cursors.WaitCursor

        For Each c As Control In Me.Controls
            c.Enabled = False
        Next

        Dim selectedRows As Integer() = GridView1.GetSelectedRows()
        Dim result As Object() = New Object(selectedRows.Length - 1) {}
        Dim LstMachineId As String
        Dim failIP As New List(Of String)()
        For i = 0 To selectedRows.Length - 1
            Dim rowHandle As Integer = selectedRows(i)
            If Not GridView1.IsGroupRow(rowHandle) Then
                LstMachineId = GridView1.GetRowCellValue(rowHandle, "ID_NO").ToString.Trim
                If enbleValue = True Then
                    Common.LogPost("Device User Enable; Device ID: " & LstMachineId)
                ElseIf enbleValue = False Then
                    Common.LogPost("Device User Disable; Device ID: " & LstMachineId)
                End If
                If GridView1.GetRowCellValue(rowHandle, "DeviceType") = "Bio-1/3/4/5/6/7/ATF-203/ATF-395/ACR-872" Then
                    sSql = "select * from tblmachine where ID_NO='" & LstMachineId & "'"
                    Dim bConn As Boolean = False
                    vnMachineNumber = LstMachineId
                    vnLicense = 1261 '1789 '
                    lpszIPAddress = GridView1.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim 'Trim(rstm!Location)
                    vpszIPAddress = Trim(lpszIPAddress)

                    XtraMasterTest.LabelControlStatus.Text = "Connecting " & vpszIPAddress & "..."
                    Application.DoEvents()

                    vpszNetPort = CLng("5005")
                    vpszNetPassword = CLng("0")
                    vnTimeOut = CLng("5000")
                    vnProtocolType = 0 'PROTOCOL_TCPIP
                    'bConn = FK623Attend.ConnectNet(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
                    If GridView1.GetRowCellValue(rowHandle, "A_R").ToString.Trim = "S" Then
                        vRet = FK_ConnectUSB(vnMachineNumber, vnLicense)
                    Else
                        vpszIPAddress = Trim(lpszIPAddress)
                        vpszNetPort = CLng("5005")
                        vpszNetPassword = CLng("0")
                        vnTimeOut = CLng("5000")
                        vnProtocolType = 0
                        vRet = FK_ConnectNet(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
                    End If
                    If vRet > 0 Then
                        bConn = True
                    Else
                        bConn = False
                    End If
                    If bConn Then
                        Dim vstrData As String = New String(CType(ChrW(32), Char), 256)
                        Dim ret = FK_GetProductData(vRet, CType(1, Integer), vstrData)
                        If license.LicenseKey <> "" Then
                            If license.DeviceSerialNo.Contains(vstrData.Trim) Then
                            Else
                                FK_DisConnect(vRet)
                                'Return "Invalid Serial number " & vstrData
                                XtraMessageBox.Show(ulf, "<size=10>Invalid Serial number " & vstrData & "</size>", "Failed")
                                Continue For
                            End If
                        End If

                        Dim selectedRowsEmp As Integer() = GridView2.GetSelectedRows()
                        Dim resultEmp As Object() = New Object(selectedRowsEmp.Length - 1) {}
                        For x As Integer = 0 To selectedRowsEmp.Length - 1
                            'LstEmployeesTarget.ListIndex = e
                            Dim rowHandleEmp As Long = selectedRowsEmp(x)
                            If Not GridView2.IsGroupRow(rowHandleEmp) Then
                                Dim EnrollNumber As String = GridView2.GetRowCellValue(rowHandleEmp, "EnrollNumber").ToString.Trim
                                Dim FingerNumber As Long = GridView2.GetRowCellValue(rowHandleEmp, "FingerNumber").ToString.Trim
                                If GridView2.GetRowCellValue(rowHandleEmp, "DeviceType").ToString.Trim = "Bio" Then
                                    If enbleValue = True Then
                                        XtraMasterTest.LabelControlStatus.Text = "Enabling User " & EnrollNumber & " In Device " & vpszIPAddress
                                        Application.DoEvents()
                                    Else
                                        XtraMasterTest.LabelControlStatus.Text = "Disabling   User " & EnrollNumber & " In Device " & vpszIPAddress
                                        Application.DoEvents()
                                    End If

                                    Dim vnResultCode As Integer
                                    If FK_GetIsSupportStringID(vRet) = RUN_SUCCESS Then
                                        vnResultCode = FK_EnableUser_StringID(vRet, EnrollNumber, FingerNumber, enbleValue)
                                    Else
                                        vnResultCode = FK_EnableUser(vRet, EnrollNumber, FingerNumber, enbleValue)
                                    End If
                                    'cmdSetAllEnrollData_bio_All(vnMachineNumber, EnrollNumber, FingerNumber)
                                End If
                            End If
                        Next x
                    Else
                        'MsgBox("Device No: " & LstMachineId & " Not connected..")
                        XtraMessageBox.Show(ulf, "<size=10>Device No: " & LstMachineId & " Not connected..</size>", "Information")
                    End If
                    'FK623Attend.Disconnect
                    FK_DisConnect(vRet)
                ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "ZK(TFT)" Or GridView1.GetRowCellValue(rowHandle, "DeviceType") = "Bio-1Pro/ATF305Pro/ATF686Pro" Then

                    Dim sdwEnrollNumber As String
                    Dim sName As String = ""
                    Dim sPassword As String = ""
                    Dim iPrivilege As Integer
                    Dim idwFingerIndex As Integer
                    Dim sTmpData As String = ""
                    'Dim sEnabled As String = ""
                    'Dim bEnabled As Boolean = False
                    Dim iflag As Integer
                    Dim commkey As Integer
                    commkey = Convert.ToInt32(GridView1.GetRowCellValue(rowHandle, "commkey").ToString.Trim)
                    lpszIPAddress = GridView1.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim 'Trim(rstm!Location)
                    vpszIPAddress = Trim(lpszIPAddress)

                    XtraMasterTest.LabelControlStatus.Text = "Connecting " & vpszIPAddress & "..."
                    Application.DoEvents()

                    Dim bIsConnected = False
                    Dim iMachineNumber As Integer = result(i)
                    Dim idwErrorCode As Integer
                    Dim com As Common = New Common
                    Dim axCZKEM1 As New zkemkeeper.CZKEM
                    axCZKEM1.SetCommPassword(Convert.ToInt32(commkey))  'to check device commkey and db commkey matches
                    bIsConnected = axCZKEM1.Connect_Net(vpszIPAddress, 4370)
                    If bIsConnected = True Then
                        axCZKEM1.EnableDevice(iMachineNumber, False)

                        'loop for fingerprint
                        Dim selectedRowsEmp As Integer() = GridView2.GetSelectedRows()
                        Dim resultEmp As Object() = New Object(selectedRowsEmp.Length - 1) {}
                        'If axCZKEM1.BeginBatchUpdate(iMachineNumber, 1) Then
                        For x As Integer = 0 To selectedRowsEmp.Length - 1
                            'LstEmployeesTarget.ListIndex = e
                            Dim rowHandleEmp As Long = selectedRowsEmp(x)
                            If Not GridView2.IsGroupRow(rowHandleEmp) Then
                                Dim adap As SqlDataAdapter
                                Dim adapA As OleDbDataAdapter
                                Dim RsFp As DataSet = New DataSet
                                Dim mUser_ID As String = GridView2.GetRowCellValue(rowHandleEmp, "EnrollNumber").ToString.Trim()
                                If enbleValue = True Then
                                    XtraMasterTest.LabelControlStatus.Text = "Enabling User " & mUser_ID & " In Device " & vpszIPAddress
                                    Application.DoEvents()
                                Else
                                    XtraMasterTest.LabelControlStatus.Text = "Disabling   User " & mUser_ID & " In Device " & vpszIPAddress
                                    Application.DoEvents()
                                End If
                                'Dim s As String = "select EMPNAME from TblEmployee where PRESENTCARDNO = '" & mUser_ID & "'"
                                'Dim adapZ As SqlDataAdapter
                                'Dim adapAZ As OleDbDataAdapter
                                'Dim dstmp As DataSet = New DataSet
                                'If Common.servername = "Access" Then
                                '    adapAZ = New OleDbDataAdapter(s, Common.con1)
                                '    adapAZ.Fill(dstmp)
                                'Else
                                '    adapZ = New SqlDataAdapter(s, Common.con)
                                '    adapZ.Fill(dstmp)
                                'End If
                                'Dim FpNo As String = GridView2.GetRowCellValue(rowHandleEmp, "FingerNumber").ToString.Trim()
                                'sSql = "select * from fptable where enrollnumber='" & mUser_ID & "' and FingerNumber=" & FpNo & " and EMachineNumber='" & LstMachineId & "' and DeviceType='ZK(TFT)' order by FingerNumber"
                                'If Common.servername = "Access" Then
                                '    sSql = "select * from fptable where enrollnumber='" & mUser_ID & "' and FingerNumber=" & FpNo & " and EMachineNumber=" & LstMachineId & " and DeviceType='ZK(TFT)' order by FingerNumber"
                                '    adapA = New OleDbDataAdapter(sSql, Common.con1)
                                '    adapA.Fill(RsFp)
                                'Else
                                '    adap = New SqlDataAdapter(sSql, Common.con)
                                '    adap.Fill(RsFp)
                                'End If


                                If IsNumeric(mUser_ID) = True Then
                                    sdwEnrollNumber = Convert.ToDouble(mUser_ID)
                                Else
                                    sdwEnrollNumber = mUser_ID
                                End If
                                'If dstmp.Tables(0).Rows.Count > 0 Then
                                '    sName = dstmp.Tables(0).Rows(0)("EMPNAME").ToString().Trim()
                                'Else
                                '    sName = ""
                                'End If
                                'idwFingerIndex = Convert.ToInt32(RsFp.Tables(0).Rows(0)("FingerNumber").ToString().Trim())
                                'sTmpData = RsFp.Tables(0).Rows(0)("Template").ToString().Trim()
                                'iPrivilege = Convert.ToInt32(RsFp.Tables(0).Rows(0)("Privilege").ToString().Trim()) 'told by ajitesh(used for admin)
                                'If ToggleMakeAdmin.IsOn = True Then
                                '    iPrivilege = 1
                                'Else
                                '    iPrivilege = RsFp.Tables(0).Rows(0).Item("privilege").ToString.Trim ' !privilege
                                'End If
                                'sPassword = RsFp.Tables(0).Rows(0)("Password").ToString().Trim()
                                'iflag = 0 'Convert.ToInt32(RsFp.Tables(0).Rows(0)(7).ToString())' as per old s/w
                                'Dim icard As String = RsFp.Tables(0).Rows(0)("CardNumber").ToString().Trim()
                                Dim expires As Integer
                                If enbleValue = False Then
                                    expires = 3
                                Else
                                    expires = 0
                                End If
                                Dim iBackupNumber As String
                                Dim strtV As DateTime
                                Dim endV As DateTime
                                If axCZKEM1.SetUserValidDate(iMachineNumber, sdwEnrollNumber, expires, 0, strtV, endV) Then  'add valdity
                                    'If axCZKEM1.SSR_SetUserInfo(iMachineNumber, sdwEnrollNumber, sName, sPassword, iPrivilege, enbleValue) Then 'upload user information to the device
                                    'If idwFingerIndex = 50 Then
                                    '    axCZKEM1.SetUserFaceStr(iMachineNumber, sdwEnrollNumber, idwFingerIndex, sTmpData, sTmpData.Length)  'face
                                    'Else
                                    '    axCZKEM1.SetUserTmpExStr(iMachineNumber, sdwEnrollNumber, idwFingerIndex, iflag, sTmpData) 'upload templates information to the device
                                    'End If
                                Else
                                    axCZKEM1.GetLastError(idwErrorCode)
                                    axCZKEM1.EnableDevice(iMachineNumber, True)
                                    Continue For
                                End If

                            End If
                        Next
                        'End If
                        'axCZKEM1.BatchUpdate(iMachineNumber) 'upload all the information in the memory
                        axCZKEM1.RefreshData(iMachineNumber)
                        axCZKEM1.EnableDevice(iMachineNumber, True)
                        axCZKEM1.Disconnect()
                    Else
                        axCZKEM1.GetLastError(idwErrorCode)
                        Continue For
                    End If
                ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "Bio2+" Then
                    'Dim commkey As String = Convert.ToInt32(GridView1.GetRowCellValue(rowHandle, "commkey").ToString.Trim)
                    'Dim deviceG2 As Riss.Devices.Device
                    'Dim deviceConnectionG2 As Riss.Devices.DeviceConnection
                    'deviceG2 = New Riss.Devices.Device()
                    'deviceG2.DN = 1 'CInt(nud_DN.Value)
                    'deviceG2.Password = commkey
                    'deviceG2.Model = "ZDC2911"
                    'deviceG2.ConnectionModel = 5
                    'lpszIPAddress = GridView1.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim
                    'If GridView1.GetRowCellValue(rowHandle, "A_R").ToString.Trim = "S" Then
                    '    deviceG2.CommunicationType = CommunicationType.Usb
                    'Else
                    '    deviceG2.IpAddress = lpszIPAddress ' TextEdit2.Text.Trim()
                    '    deviceG2.IpPort = 5500 'CInt(nud_Port.Value)
                    '    deviceG2.CommunicationType = CommunicationType.Tcp
                    'End If
                    'deviceConnectionG2 = DeviceConnection.CreateConnection(deviceG2)
                    'If deviceConnectionG2.Open() > 0 Then

                    '    'loop for fingerprint
                    '    Dim selectedRowsEmp As Integer() = GridView2.GetSelectedRows()
                    '    Dim resultEmp As Object() = New Object(selectedRowsEmp.Length - 1) {}
                    '    'If axCZKEM1.BeginBatchUpdate(iMachineNumber, 1) Then
                    '    For x As Integer = 0 To selectedRowsEmp.Length - 1
                    '        'LstEmployeesTarget.ListIndex = e
                    '        Dim rowHandleEmp As Long = selectedRowsEmp(x)
                    '        If Not GridView2.IsGroupRow(rowHandleEmp) Then
                    '            Dim adap As SqlDataAdapter
                    '            Dim adapA As OleDbDataAdapter
                    '            Dim RsFp As DataSet = New DataSet
                    '            Dim mUser_ID As String = GridView2.GetRowCellValue(rowHandleEmp, "EnrollNumber").ToString.Trim()
                    '            XtraMaster.LabelControl4.Text = "Uploading Template " & mUser_ID
                    '            Application.DoEvents()

                    '            Dim extraProperty As Object = New Object
                    '            Dim extraData As Object = New Object
                    '            extraData = False

                    '            'for userinformation 
                    '            Dim shareUser As User = New User()  'for userinformation 
                    '            shareUser.Enrolls = New List(Of Enroll)
                    '            Dim enroll As Enroll = New Enroll
                    '            shareUser.Enrolls.Add(enroll)

                    '            Dim enrollType As Integer = 0
                    '            shareUser.DIN = Convert.ToInt64(GridView2.GetRowCellValue(rowHandleEmp, "EnrollNumber").ToString.Trim()) ', UInt64)
                    '            If GridView2.GetRowCellValue(rowHandleEmp, "Privilege").ToString.Trim() = "4" Then
                    '                shareUser.Privilege = 8 ' GridView2.GetRowCellValue(rowHandleEmp, "Privilege").ToString.Trim() ' GetPrivilege
                    '            Else
                    '                shareUser.Privilege = 0
                    '            End If

                    '            shareUser.Enrolls(0).DIN = shareUser.DIN
                    '            shareUser.Enrolls(0).Password = GridView2.GetRowCellValue(rowHandleEmp, "Password").ToString.Trim() 'txt_Pwd.Text
                    '            enrollType = (enrollType + Zd2911Utils.SetBit(0, 10))
                    '            'password is 10, fp0-fp9, card is 11
                    '            If GridView2.GetRowCellValue(rowHandleEmp, "Cardnumber").ToString.Trim() <> "" Then
                    '                shareUser.Enrolls(0).CardID = GridView2.GetRowCellValue(rowHandleEmp, "Cardnumber").ToString.Trim() 'txt_Card.Text
                    '            End If

                    '            enrollType = (enrollType + Zd2911Utils.SetBit(0, 11))
                    '            'password is 10, fp0-fp9, card is 11
                    '            shareUser.Enrolls(0).EnrollType = CType(enrollType, EnrollType)

                    '            Dim s As String = "select EMPNAME from TblEmployee where PRESENTCARDNO = '" & mUser_ID & "'"
                    '            Dim adapZ As SqlDataAdapter
                    '            Dim adapAZ As OleDbDataAdapter
                    '            Dim dstmp As DataSet = New DataSet
                    '            If Common.servername = "Access" Then
                    '                adapAZ = New OleDbDataAdapter(s, Common.con1)
                    '                adapAZ.Fill(dstmp)
                    '            Else
                    '                adapZ = New SqlDataAdapter(s, Common.con)
                    '                adapZ.Fill(dstmp)
                    '            End If
                    '            If dstmp.Tables(0).Rows.Count > 0 Then
                    '                shareUser.UserName = dstmp.Tables(0).Rows(0).Item(0).ToString.Trim ' txt_UserName.Text
                    '            Else
                    '                shareUser.UserName = GridView2.GetRowCellValue(rowHandleEmp, "UserName").ToString.Trim()
                    '            End If
                    '            shareUser.Comment = "" 'ExtInfoTextBox.Text
                    '            shareUser.Enable = True 'Convert.ToBoolean(userEnableComboBox.SelectedIndex)
                    '            shareUser.AttType = 0 'CType(userAttTypeIdNumericUpDown.Value, Integer)
                    '            shareUser.AccessControl = -1 'userAccessControlComboBox.SelectedIndex
                    '            shareUser.AccessTimeZone = 0 'CType(userPassZoneNumericUpDown.Value, Integer)
                    '            shareUser.Department = 0 'CType(userDeptIdNumericUpDown.Value, Integer)
                    '            shareUser.UserGroup = 0 'CType(userGroupIdNumericUpDown.Value, Integer)
                    '            shareUser.ValidityPeriod = False 'Convert.ToBoolean(userValidityPeriodComboBox.SelectedIndex)
                    '            'shareUser.ValidDate = userStartDateTimePicker.Value
                    '            'shareUser.InvalidDate = userEndDateTimePicker.Value
                    '            shareUser.Res = 0 ' CType(userResNumericUpDown.Value, UInteger)

                    '            Dim result1 As Boolean = deviceConnectionG2.SetProperty(UserProperty.Enroll, extraProperty, shareUser, extraData)
                    '            ' end for userinformation 

                    '            'for finger template
                    '            Dim extraPropertyfp As New Object()
                    '            Dim extraDatafp As New Object()
                    '            extraDatafp = ConvertObject.DeviceBusy

                    '            Try
                    '                Dim result2 As Boolean = deviceConnectionG2.SetProperty(DeviceProperty.Enable, extraPropertyfp, deviceG2, extraDatafp)
                    '                Dim user As New User()
                    '                Dim enrollfp As New Enroll()
                    '                user.DIN = shareUser.DIN
                    '                If GridView2.GetRowCellValue(rowHandleEmp, "Privilege").ToString.Trim() = "4" Then
                    '                    user.Privilege = 8 ' GridView2.GetRowCellValue(rowHandleEmp, "Privilege").ToString.Trim() ' GetPrivilege
                    '                Else
                    '                    user.Privilege = 0
                    '                End If
                    '                enrollfp.DIN = user.DIN
                    '                enrollfp.EnrollType = DirectCast(GridView2.GetRowCellValue(rowHandleEmp, "FingerNumber"), EnrollType)
                    '                Dim fingerprint As Byte() = New Byte(Zd2911Utils.MaxFingerprintLength * (GridView2.GetRowCellValue(rowHandleEmp, "FingerNumber") + 1) - 1) {}
                    '                Dim fpBytes As Byte() = Common.HexToByte(GridView2.GetRowCellValue(rowHandleEmp, "Template").ToString.Trim)
                    '                Array.Copy(fpBytes, 0, fingerprint, GridView2.GetRowCellValue(rowHandleEmp, "FingerNumber") * Zd2911Utils.MaxFingerprintLength, fpBytes.Length)
                    '                enrollfp.Fingerprint = fingerprint
                    '                user.Enrolls.Add(enrollfp)
                    '                extraPropertyfp = UserEnrollCommand.WriteFingerprint
                    '                result2 = deviceConnectionG2.SetProperty(UserProperty.UserEnroll, extraPropertyfp, user, extraDatafp)
                    '                If result2 Then
                    '                    'MessageBox.Show("Write FP Data Success", "Prompt", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    '                Else
                    '                    'MessageBox.Show("Write FP Data Fail", "Prompt", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    '                End If
                    '            Catch ex As Exception
                    '                'MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.[Error])
                    '            Finally
                    '                extraDatafp = ConvertObject.DeviceIdle
                    '                deviceConnectionG2.SetProperty(DeviceProperty.Enable, extraPropertyfp, deviceG2, extraDatafp)
                    '            End Try

                    '            'end for finger template
                    '        End If
                    '    Next

                    '    deviceConnectionG2.Close()
                    'Else
                    '    XtraMessageBox.Show(ulf, "<size=10>Connection Failed</size>", "Failed")
                    '    Me.Cursor = Cursors.Default
                    '    Exit Sub
                    'End If
                ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "F9" Then
                    sSql = "select * from tblmachine where ID_NO='" & LstMachineId & "'"
                    Dim bConn As Boolean = False
                    vnMachineNumber = LstMachineId
                    vnLicense = 7881 '1789 '
                    lpszIPAddress = GridView1.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim 'Trim(rstm!Location)
                    vpszIPAddress = Trim(lpszIPAddress)

                    XtraMasterTest.LabelControlStatus.Text = "Connecting " & vpszIPAddress & "..."
                    Application.DoEvents()

                    vpszNetPort = CLng("5005")
                    vpszNetPassword = CLng("0")
                    vnTimeOut = CLng("5000")
                    vnProtocolType = 0 'PROTOCOL_TCPIP
                    Dim F9 As mdlPublic_f9 = New mdlPublic_f9
                    'bConn = FK623Attend.ConnectNet(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
                    If GridView1.GetRowCellValue(rowHandle, "A_R").ToString.Trim = "S" Then
                        vRet = F9.FK_ConnectUSB(vnMachineNumber, vnLicense)
                    Else
                        vRet = F9.FK_ConnectNet(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
                    End If
                    If vRet > 0 Then
                        bConn = True
                    Else
                        bConn = False
                    End If
                    If bConn Then
                        Dim selectedRowsEmp As Integer() = GridView2.GetSelectedRows()
                        Dim resultEmp As Object() = New Object(selectedRowsEmp.Length - 1) {}
                        For x As Integer = 0 To selectedRowsEmp.Length - 1
                            'LstEmployeesTarget.ListIndex = e
                            Dim rowHandleEmp As Long = selectedRowsEmp(x)
                            If Not GridView2.IsGroupRow(rowHandleEmp) Then
                                Dim EnrollNumber As String = GridView2.GetRowCellValue(rowHandleEmp, "EnrollNumber").ToString.Trim
                                Dim FingerNumber As Long = GridView2.GetRowCellValue(rowHandleEmp, "FingerNumber").ToString.Trim
                                If GridView2.GetRowCellValue(rowHandleEmp, "DeviceType").ToString.Trim = "F9" Then
                                    If enbleValue = True Then
                                        XtraMasterTest.LabelControlStatus.Text = "Enabling User " & EnrollNumber & " In Device " & vpszIPAddress
                                        Application.DoEvents()
                                    Else
                                        XtraMasterTest.LabelControlStatus.Text = "Disabling   User " & EnrollNumber & " In Device " & vpszIPAddress
                                        Application.DoEvents()
                                    End If

                                    Dim vnResultCode As Integer
                                    If F9.FK_GetIsSupportStringID(vRet) = RUN_SUCCESS Then
                                        vnResultCode = F9.FK_EnableUser_StringID(vRet, EnrollNumber, FingerNumber, enbleValue)
                                    Else
                                        vnResultCode = F9.FK_EnableUser(vRet, EnrollNumber, FingerNumber, enbleValue)
                                    End If
                                    'cmdSetAllEnrollData_bio_All(vnMachineNumber, EnrollNumber, FingerNumber)
                                End If
                            End If
                        Next x
                    Else
                        'MsgBox("Device No: " & LstMachineId & " Not connected..")
                        XtraMessageBox.Show(ulf, "<size=10>Device No: " & LstMachineId & " Not connected..</size>", "Information")
                    End If
                    F9.FK_DisConnect(vRet)
                ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "ATF686n" Then
                    vpszIPAddress = Trim(GridView1.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim)
                    vpszNetPort = CLng("5005")
                    vpszNetPassword = CLng("0")
                    vnTimeOut = CLng("5000")
                    vnProtocolType = 0
                    vnLicense = 7881
                    Dim atf686n As mdlFunction_Atf686n = New mdlFunction_Atf686n

                    If GridView1.GetRowCellValue(rowHandle, "A_R").ToString.Trim = "S" Then
                        vnResult = atf686n.ST_ConnectUSB(GridView1.GetRowCellValue(rowHandle, "ID_NO").ToString.Trim, vnLicense)
                    Else
                        vnResult = atf686n.ConnectNet(vpszIPAddress)
                    End If
                    If vnResult <= 0 Then
                            failIP.Add(vpszIPAddress)
                        End If
                        Dim vnResultCode As Integer = atf686n.ST_EnableDevice(vnResult, 0)
                    If vnResultCode <> RUN_SUCCESS Then
                        failIP.Add(vpszIPAddress)
                        atf686n.ST_DisConnect(vnResult)
                        'Return gstrNoDevice
                        Continue For
                    End If
                    Dim selectedRowsEmp As Integer() = GridView2.GetSelectedRows()
                    Dim resultEmp As Object() = New Object(selectedRowsEmp.Length - 1) {}
                    For x As Integer = 0 To selectedRowsEmp.Length - 1
                        'LstEmployeesTarget.ListIndex = e
                        Dim rowHandleEmp As Long = selectedRowsEmp(x)
                        If Not GridView2.IsGroupRow(rowHandleEmp) Then
                            Dim EnrollNumber As String = GridView2.GetRowCellValue(rowHandleEmp, "EnrollNumber").ToString.Trim
                            Dim FingerNumber As Long = GridView2.GetRowCellValue(rowHandleEmp, "FingerNumber").ToString.Trim
                            Dim vStrEnrollNumber As String = EnrollNumber
                            If IsNumeric(EnrollNumber) Then
                                vStrEnrollNumber = Convert.ToDouble(EnrollNumber)
                            End If
                            If GridView2.GetRowCellValue(rowHandleEmp, "DeviceType").ToString.Trim = "ATF686n" Then
                                If atf686n.ST_GetIsSupportStringID(vnResult) = CInt(enumErrorCode.RETURN_SUCCESS) Then
                                    Dim abEnableFlag As Integer = 1
                                    If enbleValue = True Then
                                        abEnableFlag = 1
                                        XtraMasterTest.LabelControlStatus.Text = "Enabling User " & EnrollNumber & " In Device " & vpszIPAddress
                                        Application.DoEvents()
                                    Else
                                        abEnableFlag = 0
                                        XtraMasterTest.LabelControlStatus.Text = "Disabling   User " & EnrollNumber & " In Device " & vpszIPAddress
                                        Application.DoEvents()
                                    End If
                                    vnResultCode = atf686n.ST_EnableUser_SID(vnResult, vStrEnrollNumber, 0, abEnableFlag)
                                End If
                            End If
                        End If
                    Next
                    atf686n.ST_EnableDevice(vnResult, 1)
                    atf686n.ST_DisConnect(vnResult)
                End If
            End If
        Next i
        For Each c As Control In Me.Controls
            c.Enabled = True
        Next
            Me.Cursor = Cursors.Default
            XtraMasterTest.LabelControlStatus.Text = ""
            Application.DoEvents()
            Dim failedIpArr() As String = failIP.Distinct.ToArray
            Dim failIpStr As String = ""
            If failedIpArr.Length > 0 Then
                For i As Integer = 0 To failedIpArr.Length - 1
                    failIpStr = failIpStr & "," & failedIpArr(i)
                Next
                failIpStr = failIpStr.TrimStart(",")
                XtraMessageBox.Show(ulf, "<size=10>" & failIpStr & " failed to download data</size>", "Failed")
            Else
                XtraMessageBox.Show(ulf, "<size=10>Task Completed</size>", "Success")
            End If
            'XtraMessageBox.Show(ulf, "<size=10>Task Completed</size>", "<size=9>Information</size>")
        End Sub
        Private Sub BtnEnable_Click(sender As System.Object, e As System.EventArgs) Handles BtnEnable.Click
            EnableDiableUser(True)
        End Sub
        Private Sub BtnDisable_Click(sender As System.Object, e As System.EventArgs) Handles BtnDisable.Click
            EnableDiableUser(False)
        End Sub
        Private Sub SimpleButton2_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonRestart.Click
            Dim bConn As Boolean
            Dim lngMachineNum As Long
            Dim mCommKey As Long
            Dim lpszIPAddress As String
            Dim vEMachineNumber As Long
            Dim vEnrollNumber As Long
            Dim vFingerNumber As Long
            Dim vPrivilege As Long
            Dim vEnable As Long
            Dim vRet As Boolean
            Dim mKey As String
            Dim vnResultCode As Long
            Dim RsDevice As New DataSet 'ADODB.Recordset
            vnResultCode = 0
            Dim vnMachineNumber As Long

        Dim vstrTelNumber As String
        Dim vnWaitDialTime As Long
        Dim vnLicense As Long
        Dim vpszIPAddress As String
        Dim vpszNetPort As Long
        Dim vpszNetPassword As Long
        Dim vnCommPort As Long
        Dim vnCommBaudrate As Long

        If GridView1.GetSelectedRows.Count = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>Please select the Machine</size>", "<size=9>Error</size>")
            Exit Sub
        End If
        If TextPassword.Text.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Please Enter Admin Password</size>", "<size=9>Error</size>")
            TextPassword.Select()
            Exit Sub
        Else
            If TextPassword.Text.ToLower <> Common.PASSWORD.ToLower Then
                XtraMessageBox.Show(ulf, "<size=10>Admin Password Incorrect</size>", "<size=9>Error</size>")
                TextPassword.Select()
                Exit Sub
            End If
        End If
        If XtraMessageBox.Show(ulf, "<size=10>Are you sure to Restart selected machine</size>", Common.res_man.GetString("confirmdeleteion", Common.cul), _
                             MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then

            Me.Cursor = Cursors.WaitCursor
            For Each c As Control In Me.Controls
                c.Enabled = False
            Next
            Dim sSql As String = ""
            Dim selectedRows As Integer() = GridView1.GetSelectedRows()
            Dim result As Object() = New Object(selectedRows.Length - 1) {}
            Dim LstMachineId As String
            Dim mMachineNumber As Long
            Dim commkey As Integer
            Dim failIP As New List(Of String)()

            For i = 0 To selectedRows.Length - 1
                Dim rowHandle As Integer = selectedRows(i)
                If Not GridView1.IsGroupRow(rowHandle) Then
                    LstMachineId = GridView1.GetRowCellValue(rowHandle, "ID_NO").ToString.Trim
                    Common.LogPost("Device Restart; Device ID: " & LstMachineId)
                    'sSql = "select * from tblmachine where ID_NO='" & LstMachineId & "'"

                    bConn = False
                    vnMachineNumber = LstMachineId
                    vnLicense = 1261 '1789 '
                    lpszIPAddress = GridView1.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim 'Trim(rstm!Location)
                    vpszIPAddress = Trim(lpszIPAddress)

                    If GridView1.GetRowCellValue(rowHandle, "DeviceType") = "Bio-1/3/4/5/6/7/ATF-203/ATF-395/ACR-872" Then
                        vnLicense = 1261
                        vpszNetPort = CLng("5005")
                        vpszNetPassword = CLng("0")
                        vnTimeOut = CLng("5000")
                        vnProtocolType = 0 'PROTOCOL_TCPIP
                        'bConn = FK623Attend.ConnectNet(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
                        If GridView1.GetRowCellValue(rowHandle, "A_R").ToString.Trim = "S" Then
                            vnResultCode = FK_ConnectUSB(vnMachineNumber, vnLicense)
                            '    vstrTelNumber = ""
                            'vnWaitDialTime = 0
                            'vnCommPort = GridView1.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim 'Val(Trim(rstm!Location))
                            'vnCommBaudrate = 38400
                        Else
                            vpszIPAddress = Trim(lpszIPAddress)
                            vpszNetPort = CLng("5005")
                            vpszNetPassword = CLng("0")
                            vnTimeOut = CLng("5000")
                            vnProtocolType = 0
                            vnResultCode = FK_ConnectNet(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
                            'vnResultCode = FK_ConnectNet(mMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
                        End If
                        If vnResultCode > 0 Then
                            bConn = True
                        Else
                            bConn = False
                        End If
                        If vnResultCode > 0 Then
                            Dim vstrData As String = New String(CType(ChrW(32), Char), 256)
                            Dim ret = FK_GetProductData(vnResultCode, CType(1, Integer), vstrData)
                            If license.LicenseKey <> "" Then
                                If license.DeviceSerialNo.Contains(vstrData.Trim) Then
                                Else
                                    FK_DisConnect(vnResultCode)
                                    'Return "Invalid Serial number " & vstrData
                                    XtraMessageBox.Show(ulf, "<size=10>Invalid Serial number " & vstrData & "</size>", "Failed")
                                    Continue For
                                End If
                            End If

                            vnResultCode = FK_EnableDevice(vnResultCode, 0)
                            'vnResultCode = FK623Attend.BenumbAllManager()
                            vnResultCode = FK_BenumbAllManager(1)
                            If vnResultCode <> RUN_SUCCESS Then
                                'MsgBox("Unable to clear Admin")
                                XtraMessageBox.Show(ulf, "<size=10>Unable to clear Admin</size>", "<size=9>Information</size>")
                            End If
                        Else
                            'MsgBox("Unable to connect Device No - " & mMachineNumber)
                            XtraMessageBox.Show(ulf, "<size=10>Unable to connect Device No - " & mMachineNumber & "</size>", "<size=9>Information</size>")
                        End If
                        'FK623Attend.Disconnect
                        vnResultCode = FK_EnableDevice(vnResultCode, 1)
                        FK_DisConnect(vnResultCode)
                    ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "ZK(TFT)" Or GridView1.GetRowCellValue(rowHandle, "DeviceType") = "Bio-1Pro/ATF305Pro/ATF686Pro" Then
                        Dim sdwEnrollNumber As Integer
                        Dim idwVerifyMode As Integer
                        Dim idwWorkcode As Integer

                        Dim bIsConnected = False
                        Dim iMachineNumber As Integer = result(i)
                        Dim idwErrorCode As Integer
                        Dim com As Common = New Common
                        Dim axCZKEM1 As New zkemkeeper.CZKEM
                        commkey = Convert.ToInt32(GridView1.GetRowCellValue(rowHandle, GridView1.Columns("commkey")))
                        Dim sName As String = ""
                        Dim sPassword As String = ""
                        Dim iPrivilege As Integer
                        Dim bEnabled As Boolean = False
                        Dim idwFingerIndex As Integer
                        Dim sTmpData As String = ""
                        Dim iTmpLength As Integer
                        Dim iFlag As Integer
                        Dim flagenabled As String = ""
                        Dim adap As SqlDataAdapter
                        Dim adapA As OleDbDataAdapter
                        Dim dsRecord As DataSet = New DataSet
                        axCZKEM1.SetCommPassword(Convert.ToInt32(commkey))  'to check device commkey and db commkey matches
                        bIsConnected = axCZKEM1.Connect_Net(vpszIPAddress, 4370)
                        If bIsConnected = True Then
                            If axCZKEM1.RestartDevice(iMachineNumber) Then
                                axCZKEM1.EnableDevice(iMachineNumber, True)
                                axCZKEM1.Disconnect()
                            End If
                        End If
                        'axCZKEM1.Disconnect()
                    ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "Bio1Eco" Then
                        vpszIPAddress = GridView1.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim()
                        XtraMasterTest.LabelControlStatus.Text = "Connecting " & vpszIPAddress & "..."
                        Application.DoEvents()
                        Dim adap, adap1 As SqlDataAdapter
                        Dim adapA, adapA1 As OleDbDataAdapter
                        Dim ds, ds1 As DataSet
                        commkey = Convert.ToInt32(GridView1.GetRowCellValue(rowHandle, GridView1.Columns("commkey")))
                        Dim mOpenFlag As Boolean
                        Dim mMK8001Device As Boolean 'TRUE:MK8001/8002Device;False:else

                        mMK8001Device = False
                        Dim si As SySystemInfo = New SySystemInfo
                        si.cSerialNum = New Byte((20) - 1) {}
                        si.cManuName = New Byte((24) - 1) {}
                        si.cDevName = New Byte((24) - 1) {}
                        si.cAlgVer = New Byte((16) - 1) {}
                        si.cFirmwareVer = New Byte((24) - 1) {}
                        Dim rret As Integer = 66
                        Try
                            rret = SyFunctions.CmdGetSystemInfo(si)
                        Catch ex As Exception

                        End Try

                        If (rret = CType(SyLastError.sleSuss, Int32)) Then
                            Dim sFirmwareVer As String = System.Text.Encoding.Default.GetString(si.cFirmwareVer)
                            If sFirmwareVer.Contains("MK8001/8002") Then
                                mMK8001Device = True
                            Else
                                mMK8001Device = False
                            End If
                        End If
                        Dim iFingerSize
                        If mMK8001Device Then
                            iFingerSize = 768
                        Else
                            iFingerSize = 512
                        End If
                        Dim ret As Int32 = CType(SyLastError.sleInvalidParam, Int32)
                        Dim equNo As UInt32 = Convert.ToUInt32(LstMachineId)
                        Dim gEquNo As UInt32 = equNo
                        If GridView1.GetRowCellValue(rowHandle, "A_R").ToString.Trim = "T" Then
                            Dim netCfg As SyNetCfg
                            Dim pswd As UInt32 = 0
                            Dim tmpPswd As String = commkey
                            If (tmpPswd = "0") Then
                                tmpPswd = "FFFFFFFF"
                            End If
                            Try
                                pswd = UInt32.Parse(tmpPswd, NumberStyles.HexNumber)
                            Catch ex As Exception
                                XtraMessageBox.Show(ulf, "<size=10>Incorrect Comm Key</size>", "Failed")
                                ret = CType(SyLastError.slePasswordError, Int32)
                            End Try
                            If (ret <> CType(SyLastError.slePasswordError, Int32)) Then
                                netCfg.mIsTCP = 1
                                netCfg.mPortNo = 8000 'Convert.ToUInt16(txtPortNo.Text)
                                netCfg.mIPAddr = New Byte((4) - 1) {}
                                Dim sArray() As String = vpszIPAddress.Split(Microsoft.VisualBasic.ChrW(46))
                                Dim j As Byte = 0
                                For Each i1 As String In sArray
                                    Try
                                        netCfg.mIPAddr(j) = Convert.ToByte(i1.Trim)
                                    Catch ex As System.Exception
                                        netCfg.mIPAddr(j) = 255
                                    End Try
                                    j = (j + 1)
                                    If j > 3 Then
                                        Exit For
                                    End If

                                Next
                            End If

                            Dim pnt As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(netCfg))
                            Try
                                Marshal.StructureToPtr(netCfg, pnt, False)
                                '        If optWifiDevice.Checked Then
                                'ret = SyFunctions.CmdOpenDevice(3, pnt, equNo, pswd, Convert.ToByte(chkTranceIO.Checked?1, :, 0))
                                '        Else
                                ret = SyFunctions.CmdOpenDevice(2, pnt, equNo, pswd, Convert.ToByte(1))
                                '        End If
                                '                'TODO: Warning!!!, inline IF is not supported ?
                                '                chkTranceIO.Checked()
                                '0:
                                '                '
                            Finally
                                Marshal.FreeHGlobal(pnt)
                            End Try
                        ElseIf GridView1.GetRowCellValue(rowHandle, "A_R").ToString.Trim = "S" Then
                            Dim equtype As String = "Finger Module USB Device"
                            Dim pswd As UInt32 = UInt32.Parse("FFFFFFFF", NumberStyles.HexNumber)
                            ret = SyFunctions.CmdOpenDeviceByUsb(equtype, equNo, pswd, Convert.ToByte(1))
                        End If
                        If (ret = CType(SyLastError.sleSuss, Int32)) Then
                            ret = SyFunctions.CmdTestConn2Device

                            ret = SyFunctions.CmdRebootDevice()

                            SyFunctions.CloseCom()
                            ' close com 
                            SyFunctions.CmdUnLockDevice()
                            SyFunctions.CloseCom()
                        Else
                            'XtraMessageBox.Show(ulf, "<size=10>Connection Failed</size>", "Failed")
                            Continue For
                        End If
                    ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "ATF686n" Then
                        vpszNetPort = CLng("5005")
                        vpszNetPassword = CLng("0")
                        vnTimeOut = CLng("5000")
                        vnProtocolType = 0
                        vnLicense = 7881
                        Dim atf686n As mdlFunction_Atf686n = New mdlFunction_Atf686n
                        If GridView1.GetRowCellValue(rowHandle, "A_R").ToString.Trim = "S" Then
                            vnResult = atf686n.ST_ConnectUSB(GridView1.GetRowCellValue(rowHandle, "ID_NO").ToString.Trim, vnLicense)
                        Else
                            vnResult = atf686n.ConnectNet(vpszIPAddress)
                        End If
                        If vnResult <= 0 Then
                            failIP.Add(vpszIPAddress)
                        End If
                        vnResultCode = atf686n.ST_PowerOffDevice(vnResult)
                        'atf686n.ST_EnableDevice(vnResult, 1)
                        'atf686n.ST_DisConnect(vnResult)
                    ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "Ultra800" Then
                        If license.LicenseKey <> "" Then
                            If license.DeviceSerialNo.Contains(GridView1.GetRowCellValue(rowHandle, GridView1.Columns("MAC_ADDRESS")).ToString.Trim) Then
                            Else
                                XtraMessageBox.Show(ulf, "<size=10>Invalid Serial number " & GridView1.GetRowCellValue(rowHandle, GridView1.Columns("MAC_ADDRESS")).ToString.Trim & "</size>", "Failed")
                                Continue For
                            End If
                        End If
                        Dim commkey1 As String = GridView1.GetRowCellValue(rowHandle, GridView1.Columns("commkey")).ToString.Trim
                        Dim link As String = "http://" & vpszIPAddress & ":8090/restartDevice?pass=" & commkey1
                        Dim httpWebRequest As WebRequest = WebRequest.Create(link) 'Live
                        httpWebRequest.ContentType = "application/json"
                        httpWebRequest.Method = "POST"
                        Dim httpResponse = CType(httpWebRequest.GetResponse(), HttpWebResponse)
                        Using streamReader = New StreamReader(httpResponse.GetResponseStream())
                            'Dim streamReader = New StreamReader(httpResponse.GetResponseStream)
                            Dim result1 = streamReader.ReadToEnd()

                            Dim jsonResult = JsonConvert.DeserializeObject(Of Dictionary(Of String, Object))(result1)
                            Dim firstItem = jsonResult.Item("success")
                            If firstItem <> True Then
                                failIP.Add(vpszIPAddress)
                            End If
                            Dim fs As FileStream = New FileStream("JsonResponse.txt", FileMode.OpenOrCreate, FileAccess.Write)
                            Dim sw As StreamWriter = New StreamWriter(fs)
                            sw.BaseStream.Seek(0, SeekOrigin.End)
                            sw.WriteLine(vbCrLf & link & ", " & result1 & "::" & DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"))
                            sw.Flush()
                            sw.Close()
                        End Using
                    End If
                End If
            Next
            For Each c As Control In Me.Controls
                c.Enabled = True
            Next
            TextPassword.Text = ""
            Me.Cursor = Cursors.Default
            XtraMasterTest.LabelControlStatus.Text = ""
            Application.DoEvents()
            Dim failedIpArr() As String = failIP.Distinct.ToArray
            Dim failIpStr As String = ""
            If failedIpArr.Length > 0 Then
                For i As Integer = 0 To failedIpArr.Length - 1
                    failIpStr = failIpStr & "," & failedIpArr(i)
                Next
                failIpStr = failIpStr.TrimStart(",")
                XtraMessageBox.Show(ulf, "<size=10>" & failIpStr & " failed to download data</size>", "Failed")
            Else
                XtraMessageBox.Show(ulf, "<size=10>Task Completed</size>", "Success")
            End If
        End If
    End Sub
    Private Sub PopupContainerEditLocation_QueryResultValue(sender As System.Object, e As DevExpress.XtraEditors.Controls.QueryResultValueEventArgs) Handles PopupContainerEditLocation.QueryResultValue
            Dim selectedRows() As Integer = GridViewBranch.GetSelectedRows
            Dim sb As StringBuilder = New StringBuilder
            For Each selectionRow As Integer In selectedRows
                Dim a As System.Data.DataRowView = GridViewBranch.GetRow(selectionRow)
                If (sb.ToString.Length > 0) Then
                    sb.Append(", ")
                End If
                sb.Append(a.Item("BRANCHCODE"))
            Next
            e.Value = sb.ToString
        End Sub
        Private Sub PopupContainerEditDept_QueryResultValue(sender As System.Object, e As DevExpress.XtraEditors.Controls.QueryResultValueEventArgs) Handles PopupContainerEditDept.QueryResultValue
            Dim selectedRows() As Integer = GridViewDept.GetSelectedRows
            Dim sb As StringBuilder = New StringBuilder
            For Each selectionRow As Integer In selectedRows
                Dim a As System.Data.DataRowView = GridViewDept.GetRow(selectionRow)
                If (sb.ToString.Length > 0) Then
                    sb.Append(", ")
                End If
                sb.Append(a.Item("DEPARTMENTCODE"))
            Next
            e.Value = sb.ToString
        End Sub
        Private Sub btnSearch_Click(sender As System.Object, e As System.EventArgs) Handles btnSearch.Click
            Dim sSql As String = ""
            If PopupContainerEditLocation.EditValue <> "" Or PopupContainerEditDept.EditValue <> "" Then
                Dim selectQ As String = ""
                Dim selectDept As String = ""
                Dim selectLocation As String = ""
                If PopupContainerEditLocation.EditValue <> "" Then
                    selectLocation = " BRANCHCODE IN (" & PopupContainerEditLocation.EditValue & ") "
                    selectQ = selectQ & " and " & selectLocation
                End If
                If PopupContainerEditDept.EditValue <> "" Then
                    selectDept = " DEPARTMENTCODE IN (" & PopupContainerEditDept.EditValue & ")"
                    selectQ = selectQ & " and " & selectDept
                End If
                sSql = "SELECT * from fptable, TblEmployee where fptable.EnrollNumber= TblEmployee.PRESENTCARDNO " & selectQ
            Else
                sSql = "SELECT * from fptable"
            End If
            Dim adap As SqlDataAdapter
            Dim adapA As OleDbDataAdapter
            Dim ds As DataSet = New DataSet
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
            GridControl2.DataSource = ds.Tables(0)
        End Sub
        Private Sub CompressAndSaveImage(ByVal img As Image, ByVal fileName As String, ByVal quality As Long)
            Dim parameters As New EncoderParameters(1)
            parameters.Param(0) = New EncoderParameter(System.Drawing.Imaging.Encoder.Quality, quality)
            img.Save(fileName, GetCodecInfo("image/jpeg"), parameters)
        End Sub
    Private Shared Function GetCodecInfo(ByVal mimeType As String) As ImageCodecInfo
        For Each encoder As ImageCodecInfo In ImageCodecInfo.GetImageEncoders()
            If encoder.MimeType = mimeType Then
                Return encoder
            End If
        Next encoder
        Throw New ArgumentOutOfRangeException(String.Format("'{0}' not supported", mimeType))
    End Function
    Private Sub LoadFPGrid()
        Dim fptable As New DataTable("fptable")
        Dim gridselet As String '= "select val(tblMachine.ID_NO), tblMachine.A_R, tblMachine.IN_OUT,tblMachine.DeviceType,tblMachine.LOCATION,tblMachine.branch,tblMachine.commkey,tblMachine.MAC_ADDRESS,tblMachine.Purpose,tblMachine.LastModifiedBy,tblMachine.LastModifiedDate from tblMachine"
        gridselet = "select * from fptable where DeviceType<>'HKSeries'"
        If Common.servername = "Access" Then
            Dim dataAdapter As New OleDbDataAdapter(gridselet, Common.con1)
            dataAdapter.Fill(fptable)
        Else
            Dim dataAdapter As New SqlClient.SqlDataAdapter(gridselet, Common.con)
            dataAdapter.Fill(fptable)
        End If
        GridControl2.DataSource = fptable
    End Sub
End Class