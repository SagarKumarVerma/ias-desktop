﻿Imports System.Resources
Imports System.Globalization
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports System.ComponentModel
Imports DevExpress.XtraGrid.Columns
Imports System.Data.SqlClient
Imports System.Text
Imports System.Data.OleDb
Imports DevExpress.XtraGrid.Views.Base
Imports DevExpress.XtraSplashScreen
Imports iAS.HSeriesSampleCSharp
Imports System.Runtime.InteropServices
Imports CMITech.UMXClient
Imports System.Net
Imports Newtonsoft.Json
Imports iAS.AscDemo
Imports System.Threading
Imports Newtonsoft.Json.Linq

Public Class XtraEmployeeCopyFromDevice

    Public Delegate Sub invokeDelegate()
    Dim serialNo As String = ""
    'Dim m_lGetCardCfgHandle As Int32 = -1 'Hikvision
    'Dim g_fGetGatewayCardCallback As CHCNetSDK.RemoteConfigCallback = Nothing 'Hikvision

    'for getting user info (not having card number) added from staffmanagament.cs (Acs Demo)
    Dim m_lUserInfoSearchHandle As Integer = -1 'Hikvision
    Dim g_fUserInfoSearchCallback As CHCNetSDK.RemoteConfigCallback = Nothing 'Hikvision
    Dim m_JsonUserInfoSearch As CUserInfoSearch
    Public m_iSearchPosition As Integer = 0
    Public m_iUserCount As Integer = 0
    Public m_lUserInfoRecordHandle As Integer = -1
    Public m_lUserID As Int32 = -1
    Public m_lUserInfoDeleteHandle As Int32 = -1
    Public m_iUserInfoRecordIndex As Integer = 0
    Public m_bSearchAll As Boolean = True
    Public m_bDeleteAll As Boolean = True
    Public m_iUserInfoSettingHandle As Int32 = -1
    Dim lUserID As Integer = -1
    Dim paycodelist As New List(Of String)()
    Dim paycodeArray '= paycodelist.Distinct.ToArray ' paycodelist.ToArray
    'for getting user info (not having card number)


    Dim m_lGetFingerPrintCfgHandle As Integer = -1
    Dim HCardNos As List(Of String) = Nothing
    Dim iMaxCardNum As Integer = 1000
    Dim m_struFingerPrintOne As CHCNetSDK.NET_DVR_FINGER_PRINT_CFG_V50 = New CHCNetSDK.NET_DVR_FINGER_PRINT_CFG_V50
    Dim m_struDelFingerPrint As CHCNetSDK.NET_DVR_FINGER_PRINT_INFO_CTRL_BYCARD_V50 = New CHCNetSDK.NET_DVR_FINGER_PRINT_INFO_CTRL_BYCARD_V50
    Dim m_struRecordCardCfg() As CHCNetSDK.NET_DVR_FINGER_PRINT_CFG_V50 = New CHCNetSDK.NET_DVR_FINGER_PRINT_CFG_V50((iMaxCardNum) - 1) {}
    Dim m_struCardInfo() As CHCNetSDK.NET_DVR_CARD_CFG_V50 = New CHCNetSDK.NET_DVR_CARD_CFG_V50((iMaxCardNum) - 1) {}
    Dim g_fSetFingerPrintCallback As CHCNetSDK.RemoteConfigCallback = Nothing
    Dim g_fGetFingerPrintCallback As CHCNetSDK.RemoteConfigCallback = Nothing
    Dim g_fDelFingerPrintCallback As CHCNetSDK.RemoteConfigCallback = Nothing
    Dim g_fSetGatewayCardCallback As CHCNetSDK.RemoteConfigCallback = Nothing
    Dim m_lSetCardCfgHandle As Int32 = -1
    Dim m_bSendOne As Boolean = False
    Dim m_struSelSendCardCfg As CHCNetSDK.NET_DVR_CARD_CFG_V50
    Dim m_struNowSendCard As CHCNetSDK.NET_DVR_CARD_CFG_V50
    Dim m_BSendSel As Boolean = False
    Dim m_dwCardNum As UInteger = 0
    Dim m_dwSendIndex As UInteger = 0
    Dim m_lSetFingerPrintCfgHandle As Integer = -1
    Dim m_iSendIndex As Integer = -1

    Dim Downloading As Boolean = False

    Dim ulf As UserLookAndFeel
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    Public Class pageInfo
        Public index As Integer
        Public length As Integer
        Public size As Integer
        Public total As Integer
    End Class
    Public Class records
        Public aliveType As Integer
        Public id As String
        Public idcardNum As String
        Public identifyType As Integer
        Public isImgDeleted As Integer
        Public isPass As Boolean
        Public model As Integer
        Public name As String
        Public passTimeType As Integer
        Public path As String
        Public permissionTimeType As Integer
        Public personId As String
        Public recModeType As Integer
        Public recType As Integer
        Public standard As String = ""
        Public state As Integer
        Public tempUnit As String = ""
        Public temperature As String = ""
        Public temperatureState As String
        Public time As String
        Public type As Integer
    End Class
    Public Class data
        Public pageInfo As pageInfo
        Public records() As records = New records() {}
    End Class
    Public Class AttlogUltra800
        Public code As String
        Public data As data
        Public msg As String
        Public result As Integer
        Public success As Boolean
    End Class
    Public m_lGetUserCfgHandle As Int32 = -1
    Public m_lSetUserCfgHandle As Int32 = -1
    Public Sub New()
        InitializeComponent()
        Common.SetGridFont(GridView1, New Font("Tahoma", 10))
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True

        m_struFingerPrintOne.Init()
        m_struDelFingerPrint.struByCard.byFingerPrintID = New Byte((CHCNetSDK.MAX_FINGER_PRINT_NUM) - 1) {}
    End Sub
    Private Sub XtraEmployeeCopyFromDevice_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        'If Common.servername = "Access" Then
        '    Me.TblMachine1TableAdapter1.Fill(Me.SSSDBDataSet.tblMachine1)
        '    GridControl1.DataSource = SSSDBDataSet.tblMachine1
        'Else
        '    TblMachineTableAdapter.Connection.ConnectionString = Common.ConnectionString
        '    Me.TblMachineTableAdapter.Fill(Me.SSSDBDataSet.tblMachine)
        '    GridControl1.DataSource = SSSDBDataSet.tblMachine
        'End If

        GridControl1.DataSource = Common.MachineNonAdminAll
        GridView1.ClearSelection()
    End Sub
    Private Sub btnUploadFinger_Click(sender As System.Object, e As System.EventArgs) Handles btnUploadFinger.Click
        If GridView1.GetSelectedRows.Count = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>Please select the Machine</size>", "<size=9>Error</size>")
            Exit Sub
        End If
        Dim sdwEnrollNumber As String = ""
        Dim com As Common = New Common

        Dim vnMachineNumber As Integer
        Dim vnLicense As Long
        Dim vpszIPAddress As String
        Dim vpszNetPort As Integer
        Dim vpszNetPassword As Integer
        Dim vnTimeOut As Long
        Dim vnProtocolType As Long
        Dim vnReadMark As Integer

        vnReadMark = 0 'All logs
        Dim clearLog As Boolean
        clearLog = False

        Dim RowHandles() As Integer = GridView1.GetSelectedRows
        Dim IP, ID_NO, DeviceType, A_R, Purpose As Object
        Dim commkey As Integer
        Dim cn As Common = New Common
        Dim failIP As New List(Of String)()
        DevExpress.XtraSplashScreen.SplashScreenManager.ShowForm(Me, GetType(WaitForm1), True, True, False)
        Dim paycodelist As New List(Of String)()
        Dim paycodeArray '= paycodelist.Distinct.ToArray ' paycodelist.ToArray
        For Each rowHandle As Integer In RowHandles
            IP = GridView1.GetRowCellValue(rowHandle, GridView1.Columns("LOCATION"))
            ID_NO = GridView1.GetRowCellValue(rowHandle, GridView1.Columns("ID_NO"))
            DeviceType = GridView1.GetRowCellValue(rowHandle, GridView1.Columns("DeviceType"))
            A_R = GridView1.GetRowCellValue(rowHandle, GridView1.Columns("A_R"))
            Purpose = GridView1.GetRowCellValue(rowHandle, GridView1.Columns("Purpose"))
            XtraMasterTest.LabelControlStatus.Text = "Connecting " & IP & "..."
            Application.DoEvents()
            Common.LogPost("Import from Device; Device Id: " & ID_NO)
            If DeviceType.ToString.Trim = "Bio-1/3/4/5/6/7/ATF-203/ATF-395/ACR-872" Then
                vnMachineNumber = ID_NO.ToString  '1
                vpszIPAddress = IP.ToString '"192.168.0.111"
                vpszNetPort = 5005
                vpszNetPassword = 0
                vnTimeOut = 5000
                vnProtocolType = PROTOCOL_TCPIP
                vnLicense = 1261

                Dim gnCommHandleIndex As Long
                If A_R = "S" Then
                    gnCommHandleIndex = FK_ConnectUSB(vnMachineNumber, vnLicense)
                Else
                    gnCommHandleIndex = FK_ConnectNet(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
                End If
                If gnCommHandleIndex > 0 Then
                    'MsgBox("Connection success")
                Else
                    'Return "Connection fail"
                    Continue For
                End If
                Dim vnResultCode As Integer = FK_EnableDevice(gnCommHandleIndex, 0)
                If vnResultCode <> RUN_SUCCESS Then
                    FK_DisConnect(gnCommHandleIndex)
                    'Return gstrNoDevice
                    Continue For
                End If
                vnResultCode = FK_ReadAllUserID(gnCommHandleIndex)
                If vnResultCode <> RUN_SUCCESS Then
                    FK_EnableDevice(gnCommHandleIndex, 1)
                    Continue For
                End If
                Dim vEnrollNumber As Integer, vBackupNumber As Integer, vPrivilege As Integer, vnEnableFlag As Integer
                Dim vStrEnrollNumber As String = ""
                If FK_GetIsSupportStringID(gnCommHandleIndex) = RUN_SUCCESS Then
                    Do
                        vnResultCode = FK_GetAllUserID_StringID(gnCommHandleIndex, vStrEnrollNumber, vBackupNumber, vPrivilege, vnEnableFlag)
                        If vnResultCode <> RUN_SUCCESS Then
                            If vnResultCode = RUNERR_DATAARRAY_END Then
                                vnResultCode = RUN_SUCCESS
                            End If
                            Exit Do
                        End If
                        Dim vName As String = New String(ChrW(&H20), 256)
                        vnResultCode = FK_GetUserName_StringID(gnCommHandleIndex, vStrEnrollNumber.Trim, vName)
                        paycodelist.Add(vStrEnrollNumber & ";" & vName)
                    Loop
                Else
                    Do
                        vnResultCode = FK_GetAllUserID(gnCommHandleIndex, vEnrollNumber, vBackupNumber, vPrivilege, vnEnableFlag)
                        If vnResultCode <> RUN_SUCCESS Then
                            If vnResultCode = RUNERR_DATAARRAY_END Then
                                vnResultCode = RUN_SUCCESS
                            End If
                            Exit Do
                        End If
                        Dim vName As String = New String(ChrW(&H20), 256)
                        vnResultCode = FK_GetUserName(gnCommHandleIndex, vEnrollNumber, vName)
                        paycodelist.Add(vEnrollNumber & ";" & vName)
                    Loop
                End If



                FK_EnableDevice(gnCommHandleIndex, 1)
                FK_DisConnect(gnCommHandleIndex)
            ElseIf DeviceType.ToString.Trim = "ZK(TFT)" Or DeviceType.ToString.Trim = "Bio-1Pro/ATF305Pro/ATF686Pro" Then
                'Dim CreateEmpPunchDwnld As String
                commkey = Convert.ToInt32(GridView1.GetRowCellValue(rowHandle, GridView1.Columns("commkey")))
                Dim comZK As CommonZK = New CommonZK
                Dim bIsConnected = False
                Dim iMachineNumber As Integer
                Dim idwErrorCode As Integer
                Dim axCZKEM1 As New zkemkeeper.CZKEM
                axCZKEM1.SetCommPassword(commkey)  'to check device commkey and db commkey matches
                bIsConnected = axCZKEM1.Connect_Net(IP, 4370)
                If bIsConnected = True Then
                    iMachineNumber = 1 'In fact,when you are using the tcp/ip communication,this parameter will be ignored,that is any integer will all right.Here we use 1.
                    axCZKEM1.RegEvent(iMachineNumber, 65535) 'Here you can register the realtime events that you want to be triggered(the parameters 65535 means registering all)
                    axCZKEM1.EnableDevice(iMachineNumber, False) 'disable the device

                    Dim McSrno As String = ""
                    Dim vRet As Boolean = axCZKEM1.GetSerialNumber(iMachineNumber, McSrno)
                    'If McSrno.Substring(0, 3) = "AF6" Or McSrno.Substring(0, 3) = "AEH" Or McSrno.Substring(0, 3) = "AJV" Or McSrno.Substring(0, 3) = "AOO" Or McSrno.Substring(0, 3) = "719" Or McSrno.Substring(0, 3) = "ALM" Or McSrno.Substring(0, 3) = "701" Or McSrno.Substring(0, 3) = "BYR" Or McSrno.Substring(0, 3) = "OIN" Or McSrno.Substring(0, 3) = "AF6" Or McSrno.Substring(0, 3) = "BYX" Or McSrno.Substring(0, 3) = "AIJ" Or McSrno.Substring(0, 3) = "A6G" Or McSrno.Substring(0, 3) = "AEH" Or McSrno.Substring(0, 3) = "CCG" Or McSrno.Substring(0, 3) = "BJV" _
                    '    Or Mid(Trim(McSrno), 1, 1) = "A" Or Mid(Trim(McSrno), 1, 3) = "OIN" Or Mid(Trim(McSrno), 1, 5) = "FPCTA" Or Mid(Trim(McSrno), 1, 4) = "0000" Or Mid(Trim(McSrno), 1, 4) = "1808" Or Mid(Trim(McSrno), 1, 4) = "ATPL" Or Mid(Trim(McSrno), 1, 4) = "TIPL" Or Mid(Trim(McSrno), 1, 4) = "ZXJK" Or Mid(Trim(McSrno), 1, 8) = "10122013" Or Mid(Trim(McSrno), 1, 8) = "76140122" Or Mid(Trim(McSrno), 1, 8) = "17614012" Or Mid(Trim(McSrno), 1, 8) = "17214012" Or Mid(Trim(McSrno), 1, 8) = "27022014" Or Mid(Trim(McSrno), 1, 8) = "24042014" Or Mid(Trim(McSrno), 1, 8) = "25042014" _
                    '    Or Mid(Trim(McSrno), 1, 8) = "26042014" Or Mid(Trim(McSrno), 1, 7) = "SN:0000" Or Mid(Trim(McSrno), 1, 4) = "BOCK" Or McSrno.Substring(0, 1) = "B" Or McSrno.Substring(0, 4) = "2455" Or McSrno.Substring(0, 4) = "CDSL" _
                    '     Or McSrno = "6583151100400" Then
                    If license.DeviceSerialNo.Contains(McSrno.Trim) Then
                    Else
                        SplashScreenManager.CloseForm(False)
                        axCZKEM1.EnableDevice(iMachineNumber, True)
                        axCZKEM1.Disconnect()
                        XtraMessageBox.Show(ulf, "<size=10>Invalid Serial number " & McSrno & "</size>", "<size=9>Error</size>")
                        DevExpress.XtraSplashScreen.SplashScreenManager.ShowForm(Me, GetType(WaitForm1), True, True, False)
                        Continue For
                    End If
                    Dim LogResult As Boolean
                    'LogResult = axCZKEM1.ReadGeneralLogData(iMachineNumber)  'all logs
                    'If LogResult Then 'read all the attendance records to the memory
                    XtraMasterTest.LabelControlStatus.Text = "Downloading from " & IP & "..."
                    Application.DoEvents()
                    'get records from the memory
                    Dim x As Integer = 0
                    'Dim paycodelist As New List(Of String)()
                    'While axCZKEM1.SSR_GetGeneralLogData(iMachineNumber, sdwEnrollNumber, idwVerifyMode, idwInOutMode, idwYear, idwMonth, idwDay, idwHour, idwMinute, idwSecond, idwWorkcode)
                    Dim sName As String, sPassword As String
                    Dim iPrivilege As Integer
                    Dim bEnabled As Boolean
                    While axCZKEM1.SSR_GetAllUserInfo(iMachineNumber, sdwEnrollNumber, sName, sPassword, iPrivilege, bEnabled)
                        paycodelist.Add(sdwEnrollNumber & ";" & sName)
                        'com.funcGetGeneralLogDataZK(sdwEnrollNumber, idwVerifyMode, idwInOutMode, punchdate, idwWorkcode, Purpose)
                        x = x + 1
                    End While
                    axCZKEM1.EnableDevice(iMachineNumber, True)
                    If clearLog = True Then
                        axCZKEM1.ClearGLog(iMachineNumber)
                    End If
                Else
                    axCZKEM1.GetLastError(idwErrorCode)
                    failIP.Add(IP.ToString)
                    Continue For
                End If
                axCZKEM1.Disconnect()
            ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "Bio1Eco" Then
                vpszIPAddress = IP
                XtraMasterTest.LabelControlStatus.Text = "Connecting " & vpszIPAddress & "..."
                Application.DoEvents()
                Dim adap, adap1 As SqlDataAdapter
                Dim adapA, adapA1 As OleDbDataAdapter
                Dim ds, ds1 As DataSet
                commkey = Convert.ToInt32(GridView1.GetRowCellValue(rowHandle, GridView1.Columns("commkey")))
                Dim mOpenFlag As Boolean
                Dim mMK8001Device As Boolean 'TRUE:MK8001/8002Device;False:else

                mMK8001Device = False
                Dim si As SySystemInfo = New SySystemInfo
                si.cSerialNum = New Byte((20) - 1) {}
                si.cManuName = New Byte((24) - 1) {}
                si.cDevName = New Byte((24) - 1) {}
                si.cAlgVer = New Byte((16) - 1) {}
                si.cFirmwareVer = New Byte((24) - 1) {}
                Dim rret As Integer = 66
                Try
                    rret = SyFunctions.CmdGetSystemInfo(si)
                Catch ex As Exception

                End Try

                If (rret = CType(SyLastError.sleSuss, Int32)) Then
                    Dim sFirmwareVer As String = System.Text.Encoding.Default.GetString(si.cFirmwareVer)
                    If sFirmwareVer.Contains("MK8001/8002") Then
                        mMK8001Device = True
                    Else
                        mMK8001Device = False
                    End If
                End If

                Dim ret As Int32 = CType(SyLastError.sleInvalidParam, Int32)
                Dim equNo As UInt32 = Convert.ToUInt32(ID_NO)
                Dim gEquNo As UInt32 = equNo
                If A_R = "T" Then
                    Dim netCfg As SyNetCfg
                    Dim pswd As UInt32 = 0
                    Dim tmpPswd As String = commkey
                    If (tmpPswd = "0") Then
                        tmpPswd = "FFFFFFFF"
                    End If
                    Try
                        pswd = UInt32.Parse(tmpPswd, NumberStyles.HexNumber)
                    Catch ex As Exception
                        XtraMessageBox.Show(ulf, "<size=10>Incorrect Comm Key</size>", "Failed")
                        ret = CType(SyLastError.slePasswordError, Int32)
                    End Try
                    If (ret <> CType(SyLastError.slePasswordError, Int32)) Then
                        netCfg.mIsTCP = 1
                        netCfg.mPortNo = 8000 'Convert.ToUInt16(txtPortNo.Text)
                        netCfg.mIPAddr = New Byte((4) - 1) {}
                        Dim sArray() As String = vpszIPAddress.Split(Microsoft.VisualBasic.ChrW(46))
                        Dim j As Byte = 0
                        For Each i1 As String In sArray
                            Try
                                netCfg.mIPAddr(j) = Convert.ToByte(i1.Trim)
                            Catch ex As System.Exception
                                netCfg.mIPAddr(j) = 255
                            End Try
                            j = (j + 1)
                            If j > 3 Then
                                Exit For
                            End If

                        Next
                    End If

                    Dim pnt As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(netCfg))
                    Try
                        Marshal.StructureToPtr(netCfg, pnt, False)
                        '        If optWifiDevice.Checked Then
                        'ret = SyFunctions.CmdOpenDevice(3, pnt, equNo, pswd, Convert.ToByte(chkTranceIO.Checked?1, :, 0))
                        '        Else
                        ret = SyFunctions.CmdOpenDevice(2, pnt, equNo, pswd, Convert.ToByte(1))
                        '        End If
                        '                'TODO: Warning!!!, inline IF is not supported ?
                        '                chkTranceIO.Checked()
                        '0:
                        '                '
                    Finally
                        Marshal.FreeHGlobal(pnt)
                    End Try
                ElseIf A_R = "S" Then
                    Dim equtype As String = "Finger Module USB Device"
                    Dim pswd As UInt32 = UInt32.Parse("FFFFFFFF", NumberStyles.HexNumber)
                    ret = SyFunctions.CmdOpenDeviceByUsb(equtype, equNo, pswd, Convert.ToByte(1))
                End If
                If (ret = CType(SyLastError.sleSuss, Int32)) Then
                    ret = SyFunctions.CmdTestConn2Device

                    Dim maxUserCnt As System.UInt16 = 0
                    ret = SyFunctions.CmdGetCount(maxUserCnt)
                    If ((maxUserCnt > 0) And (ret = CType(SyLastError.sleSuss, Int32))) Then
                        If Common.servername = "Access" Then
                            If Common.con1.State <> System.Data.ConnectionState.Open Then
                                Common.con1.Open()
                            End If
                        Else
                            If Common.con.State <> System.Data.ConnectionState.Open Then
                                Common.con.Open()
                            End If
                        End If
                        Dim aID() As UInt16 = New UInt16((maxUserCnt) - 1) {}
                        Array.Clear(aID, 0, aID.Length)
                        ret = SyFunctions.CmdGetAllValidUserID(aID, maxUserCnt)
                        Dim uie As SyUserInfoExt = New SyUserInfoExt
                        'Dim i As Integer = 1

                        For Each userid As UInt16 In aID
                            paycodelist.Add(userid & ";" & uie.sUserInfo.name)
                            If (userid = 0) Then
                                Exit For
                            End If
                            'Application.DoEvents()
                        Next


conclose:               If Common.servername = "Access" Then
                            If Common.con1.State <> System.Data.ConnectionState.Closed Then
                                Common.con1.Close()
                            End If
                        Else
                            If Common.con.State <> System.Data.ConnectionState.Closed Then
                                Common.con.Close()
                            End If
                        End If
                    End If

                    SyFunctions.CloseCom()
                    ' close com 
                    SyFunctions.CmdUnLockDevice()
                    SyFunctions.CloseCom()
                Else
                    'XtraMessageBox.Show(ulf, "<size=10>Connection Failed</size>", "Failed")
                    Continue For
                End If

            ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "EF45" Then
                vpszIPAddress = IP
                XtraMasterTest.LabelControlStatus.Text = "Connecting " & vpszIPAddress & "..."
                Application.DoEvents()
                Dim _client As Client = Nothing
                Dim connectOk As Boolean = False
                Try
                    _client = cn.initClientEF45(vpszIPAddress)
                    connectOk = _client.StealConnect '_client.Connect()
                    If connectOk Then
                        Dim sSql As String
                        Dim adapA As OleDbDataAdapter
                        Dim adap As SqlDataAdapter
                        Dim dsRecord As DataSet
                        Dim allUserInfo As List(Of CMITech.UMXClient.Entities.UserInfo) = _client.GetAllUserInfo
                        For Each userinfo As CMITech.UMXClient.Entities.UserInfo In allUserInfo
                            Try
                                Dim subject As CMITech.UMXClient.Entities.Subject = _client.GetSubject(userinfo.UUID)
                                paycodelist.Add(userinfo.UUID & ";" & subject.LastName)
                            Catch ex As Exception

                            End Try
                        Next
                        _client.Disconnect()
                    Else
                        failIP.Add(vpszIPAddress.ToString)
                        Continue For
                    End If
                Catch ex As Exception
                    failIP.Add(vpszIPAddress.ToString)
                    Continue For
                End Try

            ElseIf DeviceType.ToString.Trim = "F9" Then
                vnMachineNumber = ID_NO.ToString  '1
                vpszIPAddress = IP.ToString '"192.168.0.111"
                vpszNetPort = 5005
                vpszNetPassword = 0
                vnTimeOut = 5000
                vnProtocolType = PROTOCOL_TCPIP
                vnLicense = 7881
                Dim F9 As mdlPublic_f9 = New mdlPublic_f9
                Dim gnCommHandleIndex As Long
                If A_R = "S" Then
                    gnCommHandleIndex = F9.FK_ConnectUSB(vnMachineNumber, vnLicense)
                Else
                    gnCommHandleIndex = F9.FK_ConnectNet(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
                End If
                If gnCommHandleIndex > 0 Then
                    'MsgBox("Connection success")
                Else
                    'Return "Connection fail"
                    Continue For
                End If
                Dim vnResultCode As Integer = F9.FK_EnableDevice(gnCommHandleIndex, 0)
                If vnResultCode <> RUN_SUCCESS Then
                    FK_DisConnect(gnCommHandleIndex)
                    'Return gstrNoDevice
                    Continue For
                End If
                vnResultCode = F9.FK_ReadAllUserID(gnCommHandleIndex)
                If vnResultCode <> RUN_SUCCESS Then
                    F9.FK_EnableDevice(gnCommHandleIndex, 1)
                    Continue For
                End If
                Dim vEnrollNumber As Integer, vBackupNumber As Integer, vPrivilege As Integer, vnEnableFlag As Integer
                Dim vStrEnrollNumber As String = ""
                If F9.FK_GetIsSupportStringID(gnCommHandleIndex) = RUN_SUCCESS Then
                    Do
                        vnResultCode = F9.FK_GetAllUserID_StringID(gnCommHandleIndex, vStrEnrollNumber, vBackupNumber, vPrivilege, vnEnableFlag)
                        If vnResultCode <> RUN_SUCCESS Then
                            If vnResultCode = RUNERR_DATAARRAY_END Then
                                vnResultCode = RUN_SUCCESS
                            End If
                            Exit Do
                        End If
                        Dim vName As String = New String(ChrW(&H20), 256)
                        vnResultCode = F9.FK_GetUserName_StringID(gnCommHandleIndex, vStrEnrollNumber.Trim, vName)
                        paycodelist.Add(vStrEnrollNumber & ";" & vName)
                    Loop
                Else
                    Do
                        vnResultCode = F9.FK_GetAllUserID(gnCommHandleIndex, vEnrollNumber, vBackupNumber, vPrivilege, vnEnableFlag)
                        If vnResultCode <> RUN_SUCCESS Then
                            If vnResultCode = RUNERR_DATAARRAY_END Then
                                vnResultCode = RUN_SUCCESS
                            End If
                            Exit Do
                        End If
                        Dim vName As String = New String(ChrW(&H20), 256)
                        vnResultCode = F9.FK_GetUserName(gnCommHandleIndex, vEnrollNumber, vName)
                        paycodelist.Add(vEnrollNumber & ";" & vName)
                    Loop
                End If
                '0:
                F9.FK_EnableDevice(gnCommHandleIndex, 1)
                F9.FK_DisConnect(gnCommHandleIndex)
            ElseIf DeviceType.ToString.Trim = "ATF686n" Then
                vnMachineNumber = ID_NO.ToString  '1
                vpszIPAddress = IP.ToString '"192.168.0.111"
                vpszNetPort = 5005
                vpszNetPassword = 0
                vnTimeOut = 5000
                vnProtocolType = PROTOCOL_TCPIP
                vnLicense = 7881
                Dim atf686n As mdlFunction_Atf686n = New mdlFunction_Atf686n
                Dim gnCommHandleIndex As Long

                'gnCommHandleIndex = atf686n.ConnectNet(vpszIPAddress)
                If A_R = "S" Then
                    gnCommHandleIndex = atf686n.ST_ConnectUSB(vnMachineNumber, vnLicense)
                Else
                    gnCommHandleIndex = atf686n.ConnectNet(vpszIPAddress)
                End If
                If gnCommHandleIndex > 0 Then
                    'MsgBox("Connection success")
                Else
                    'Return "Connection fail"
                    Continue For
                End If
                Dim vnResultCode As Integer = atf686n.ST_EnableDevice(gnCommHandleIndex, 0)
                If vnResultCode <> RUN_SUCCESS Then
                    atf686n.ST_DisConnect(gnCommHandleIndex)
                    'Return gstrNoDevice
                    Continue For
                End If
                vnResultCode = atf686n.ST_ReadAllUserID(gnCommHandleIndex)
                If vnResultCode <> RUN_SUCCESS Then
                    atf686n.ST_EnableDevice(gnCommHandleIndex, 1)
                    Continue For
                End If
                Dim vEnrollNumber As Integer, vBackupNumber As Integer, vPrivilege As Integer, vnEnableFlag As Integer
                Dim vStrEnrollNumber As String = ""
                If atf686n.ST_GetIsSupportStringID(gnCommHandleIndex) = RUN_SUCCESS Then
                    Do
                        vnResultCode = atf686n.ST_GetAllUserID_SID(gnCommHandleIndex, vStrEnrollNumber, vBackupNumber, vPrivilege, vnEnableFlag)
                        If vnResultCode <> RUN_SUCCESS Then
                            If vnResultCode = RUNERR_DATAARRAY_END Then
                                vnResultCode = RUN_SUCCESS
                            End If
                            Exit Do
                        End If
                        Try
                            vStrEnrollNumber = vStrEnrollNumber.Substring(0, vStrEnrollNumber.IndexOf(" ")).Trim
                        Catch ex As Exception

                        End Try

                        Dim vName As String = ""
                        Dim vnInfoSize As Integer = Marshal.SizeOf(GetType(USER_NAME_STRING_ID))
                        Dim bytNameData(vnInfoSize - 1) As Byte
                        Dim mUserNameInfo As USER_NAME_STRING_ID
                        vnResultCode = atf686n.ST_GetOnlyUserName_SID(gnCommHandleIndex, vStrEnrollNumber.Trim, bytNameData)
                        Dim vobj As Object = ConvertByteArrayToStructure(bytNameData, GetType(USER_NAME_STRING_ID))
                        If vobj Is Nothing Then
                            Return
                        End If
                        mUserNameInfo = DirectCast(vobj, USER_NAME_STRING_ID)
                        vName = ByteArrayUtf16ToString(mUserNameInfo.UserName)

                        paycodelist.Add(vStrEnrollNumber & ";" & vName.Trim)
                    Loop
                Else
                    'Do
                    '    vnResultCode = F9.FK_GetAllUserID(gnCommHandleIndex, vEnrollNumber, vBackupNumber, vPrivilege, vnEnableFlag)
                    '    If vnResultCode <> RUN_SUCCESS Then
                    '        If vnResultCode = RUNERR_DATAARRAY_END Then
                    '            vnResultCode = RUN_SUCCESS
                    '        End If
                    '        Exit Do
                    '    End If
                    '    Dim vName As String = New String(ChrW(&H20), 256)
                    '    vnResultCode = F9.FK_GetUserName(gnCommHandleIndex, vEnrollNumber, vName)
                    '    paycodelist.Add(vEnrollNumber & ";" & vName)
                    'Loop
                End If
                '0:
                atf686n.ST_EnableDevice(gnCommHandleIndex, 1)
                atf686n.ST_DisConnect(gnCommHandleIndex)
            ElseIf DeviceType.ToString.Trim = "TF-01" Then
                vnMachineNumber = ID_NO.ToString  '1
                vpszIPAddress = IP.ToString '"192.168.0.111"
                vpszNetPort = 5005
                vpszNetPassword = 0
                vnTimeOut = 5000
                vnProtocolType = PROTOCOL_TCPIP
                vnLicense = 7881
                Dim F9 As mdlPublic_f9 = New mdlPublic_f9

                Dim bRet As Boolean
                bRet = AxFP_CLOCK1.SetIPAddress(vpszIPAddress, vpszNetPort, vpszNetPassword)
                Me.AxFP_CLOCK1.OpenCommPort(vnMachineNumber)

                If Not bRet Then
                    Continue For

                End If

                bRet = AxFP_CLOCK1.EnableDevice(vnMachineNumber, False)

                If Not bRet Then
                    AxFP_CLOCK1.CloseCommPort()
                    Continue For
                End If
                bRet = AxFP_CLOCK1.ReadAllUserID(vnMachineNumber)
                If Not bRet Then
                    AxFP_CLOCK1.CloseCommPort()
                    Continue For
                End If
                Dim dwEnrollNumber As Integer = 0
                Dim dwMachineNumber As Integer = 0
                Dim dwBackupNumber As Integer = 0
                Dim dwUserPrivilege As Integer = 0
                Dim dwAttendenceEnable As Integer = 0
                Dim IsOver As Boolean = True

                Dim vStrEnrollNumber As String = ""
                While (IsOver)
                    IsOver = AxFP_CLOCK1.GetAllUserID(vnMachineNumber, dwEnrollNumber, dwMachineNumber, dwBackupNumber, dwUserPrivilege, dwAttendenceEnable)
                    Dim strName As String = ""
                    Dim obj As Object = New System.Runtime.InteropServices.VariantWrapper(strName)
                    Dim ob As New Object()
                    ob = strName

                    bRet = AxFP_CLOCK1.GetUserName(0, vnMachineNumber, dwEnrollNumber, dwEnrollNumber, obj)

                    If Not bRet Then
                        obj = dwEnrollNumber
                    End If
                    paycodelist.Add(dwEnrollNumber.ToString & ";" & obj)

                End While
                AxFP_CLOCK1.EnableDevice(vnMachineNumber, True)
                AxFP_CLOCK1.CloseCommPort()
            ElseIf DeviceType.ToString.Trim = "Ultra800" Then
                If license.LicenseKey <> "" Then
                    If license.DeviceSerialNo.Contains(GridView1.GetRowCellValue(rowHandle, GridView1.Columns("MAC_ADDRESS")).ToString.Trim) Then
                    Else
                        XtraMessageBox.Show(ulf, "<size=10>Invalid Serial number " & GridView1.GetRowCellValue(rowHandle, GridView1.Columns("MAC_ADDRESS")).ToString.Trim & "</size>", "Failed")
                        Continue For
                    End If
                End If
                Dim commKey1 As String = GridView1.GetRowCellValue(rowHandle, GridView1.Columns("commkey")).ToString.Trim
                Dim paycodelistTmp As New List(Of String)()
                Dim index As Integer = 0
calllink:       Dim link As String = "http://" & IP.ToString & ":8090/newFindRecords?pass=" & commKey1 & "&personId=-1&startTime=0&endTime=0&index=" & index & "&length=100&order=1"
                Dim proxy As WebClient = New WebClient
                Dim serviceURL As String = String.Format(link)
                Dim data() As Byte = proxy.DownloadData(serviceURL)
                Dim stream As Stream = New MemoryStream(data)
                Dim OutPut As String = System.Text.Encoding.UTF8.GetString(data)
                Dim AttD As AttlogUltra800 = New AttlogUltra800
                AttD = JsonConvert.DeserializeObject(Of AttlogUltra800)(OutPut)
                If AttD.success = True Then
                    For v As Integer = 0 To AttD.data.records.Count - 1
                        If AttD.data.records(v).personId <> "" And AttD.data.records(v).personId <> "STRANGERBABY" Then
                            paycodelistTmp.Add(AttD.data.records(v).personId)
                            paycodelist.Add(AttD.data.records(v).personId & ";" & "")
                        End If
                    Next
                End If
                If AttD.data.pageInfo.index < AttD.data.pageInfo.size - 1 Then
                    index = index + 1
                    GoTo calllink
                End If
                'get face for each user
                Dim paycodeUltra800() As String = paycodelistTmp.Distinct.ToArray
            ElseIf DeviceType.ToString.Trim = "HKSeries" Then
                m_iUserCount = 0
                m_iSearchPosition = 0
                Dim struDeviceInfoV40 As CHCNetSDK.NET_DVR_DEVICEINFO_V40 = New CHCNetSDK.NET_DVR_DEVICEINFO_V40
                Dim DeviceAdd As String = IP, userName As String = Trim(GridView1.GetRowCellValue(rowHandle, "HLogin").ToString.Trim), pwd As String = Trim(GridView1.GetRowCellValue(rowHandle, "HPassword").ToString.Trim)
                Dim failReason As String = ""
                Dim logistatus = cn.HikvisionLogin(DeviceAdd, userName, pwd, struDeviceInfoV40, lUserID, failReason)
                If logistatus = False Then
                    If failReason <> "" Then
                        XtraMessageBox.Show(ulf, "<size=10>" & failReason & "</size>", "Failed")
                        XtraMasterTest.LabelControlStatus.Text = ""
                        Application.DoEvents()
                        Continue For
                    End If
                Else
                    If failReason <> "" Then
                        XtraMessageBox.Show(ulf, "<size=10>" & failReason & "</size>", "Failed")
                        XtraMasterTest.LabelControlStatus.Text = ""
                        Application.DoEvents()
                        logistatus = cn.HikvisionLogOut(lUserID)
                        Continue For
                    End If
                    Downloading = True
                    m_lUserID = lUserID
                    If m_lGetUserCfgHandle <> -1 Then

                        If CHCNetSDK.NET_DVR_StopRemoteConfig(m_lGetUserCfgHandle) Then
                            m_lGetUserCfgHandle = -1
                        End If
                    End If
                    'MsgBox("1")
                    Dim sURL As String = "POST /ISAPI/AccessControl/UserInfo/Search?format=json"
                    Dim ptrURL As IntPtr = Marshal.StringToHGlobalAnsi(sURL)
                    m_lSetUserCfgHandle = CHCNetSDK.NET_DVR_StartRemoteConfig(lUserID, CHCNetSDK.NET_DVR_JSON_CONFIG, ptrURL, sURL.Length, Nothing, IntPtr.Zero)
                    'MsgBox("2")
                    If m_lSetUserCfgHandle < 0 Then
                        MessageBox.Show("NET_DVR_StartRemoteConfig fail [url:POST /ISAPI/AccessControl/UserInfo/Search?format=json] error:" & CHCNetSDK.NET_DVR_GetLastError())
                        Marshal.FreeHGlobal(ptrURL)
                        Return
                    Else
                        Dim getUser As Boolean = True
                        Dim userStartPos As Integer = 0
                        'ListUserInfo.Items.Clear()
                        'MsgBox("3")
                        While getUser
                            Dim JsonUserInfoSearchCond As CUserInfoSearchCondCfg = New CUserInfoSearchCondCfg()
                            JsonUserInfoSearchCond.UserInfoSearchCond = New CUserInfoSearchCond()
                            'Dim EmpNO As String = textBoxEmployeeNo.Text

                            'If EmpNO.Length = 0 Then
                            JsonUserInfoSearchCond.UserInfoSearchCond.searchID = "1"
                            JsonUserInfoSearchCond.UserInfoSearchCond.searchResultPosition = userStartPos
                            JsonUserInfoSearchCond.UserInfoSearchCond.maxResults = 10
                            'Else
                            '    JsonUserInfoSearchCond.UserInfoSearchCond.searchID = "1"
                            '    JsonUserInfoSearchCond.UserInfoSearchCond.searchResultPosition = 0
                            '    JsonUserInfoSearchCond.UserInfoSearchCond.maxResults = 10
                            '    JsonUserInfoSearchCond.UserInfoSearchCond.EmployeeNoList = New List(Of CEmployeeNoList)()
                            '    Dim singleEmployeeNoList As CEmployeeNoList = New CEmployeeNoList()
                            '    singleEmployeeNoList.employeeNo = textBoxEmployeeNo.Text
                            '    JsonUserInfoSearchCond.UserInfoSearchCond.EmployeeNoList.Add(singleEmployeeNoList)
                            'End If

                            Dim strUserInfoSearchCondCfg As String = JsonConvert.SerializeObject(JsonUserInfoSearchCond)
                            Dim ptrUserInfoSearchCondCfg As IntPtr = Marshal.StringToHGlobalAnsi(strUserInfoSearchCondCfg)
                            Dim ptrJsonData As IntPtr = Marshal.AllocHGlobal(1024 * 10)

                            For i As Integer = 0 To 1024 * 10 - 1
                                Marshal.WriteByte(ptrJsonData, i, 0)
                            Next

                            Dim dwState As Integer = CInt(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_SUCCESS)
                            Dim dwReturned As UInteger = 0

                            While True
                                dwState = CHCNetSDK.NET_DVR_SendWithRecvRemoteConfig(m_lSetUserCfgHandle, ptrUserInfoSearchCondCfg, CUInt(strUserInfoSearchCondCfg.Length), ptrJsonData, 1024 * 10, dwReturned)
                                Dim strJsonData As String = Marshal.PtrToStringAnsi(ptrJsonData)

                                If dwState = CInt(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_NEEDWAIT) Then
                                    Thread.Sleep(10)
                                    Continue While
                                ElseIf dwState = CInt(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_FAILED) Then
                                    'MessageBox.Show("Get User Fail error:" & CHCNetSDK.NET_DVR_GetLastError())
                                ElseIf dwState = CInt(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_SUCCESS) Then
                                    Dim JsonUserInfoSearchCfg As CUserInfoSearchCfg = New CUserInfoSearchCfg()
                                    JsonUserInfoSearchCfg = JsonConvert.DeserializeObject(Of CUserInfoSearchCfg)(strJsonData)

                                    If JsonUserInfoSearchCfg.UserInfoSearch Is Nothing Then
                                        Dim JsonResponseStatus As CResponseStatus = New CResponseStatus()
                                        JsonResponseStatus = JsonConvert.DeserializeObject(Of CResponseStatus)(strJsonData)

                                        If JsonResponseStatus.statusCode = 1 Then
                                            'MessageBox.Show("Get User Success")
                                        Else
                                            'MessageBox.Show("Get User Fail, ResponseStatus.statusCode" & JsonResponseStatus.statusCode)
                                            getUser = False
                                        End If
                                    Else

                                        If JsonUserInfoSearchCfg.UserInfoSearch.totalMatches = 0 Then
                                            'MessageBox.Show("no this EmployeeNo person")
                                            getUser = False
                                            Exit While
                                        End If

                                        If JsonUserInfoSearchCfg.UserInfoSearch.numOfMatches < 10 Then getUser = False

                                        For i As Integer = 0 To JsonUserInfoSearchCfg.UserInfoSearch.numOfMatches - 1
                                            paycodelist.Add(JsonUserInfoSearchCfg.UserInfoSearch.UserInfo(i).employeeNo & ";" & JsonUserInfoSearchCfg.UserInfoSearch.UserInfo(i).name)
                                            'Dim Items As ListViewItem = New ListViewItem()
                                            'Items.Text = Convert.ToString(i + 1)
                                            'Items.SubItems.Add(JsonUserInfoSearchCfg.UserInfoSearch.UserInfo(i).employeeNo)
                                            'Items.SubItems.Add(JsonUserInfoSearchCfg.UserInfoSearch.UserInfo(i).name)
                                            'Items.SubItems.Add(JsonUserInfoSearchCfg.UserInfoSearch.UserInfo(i).userType)
                                            'Items.SubItems.Add(JsonUserInfoSearchCfg.UserInfoSearch.UserInfo(i).Valid.beginTime)
                                            'Items.SubItems.Add(JsonUserInfoSearchCfg.UserInfoSearch.UserInfo(i).Valid.endTime)
                                            'Items.SubItems.Add(JsonUserInfoSearchCfg.UserInfoSearch.UserInfo(i).belongGroup)
                                            'Items.SubItems.Add(JsonUserInfoSearchCfg.UserInfoSearch.UserInfo(i).password)
                                            'Items.SubItems.Add(Convert.ToString(JsonUserInfoSearchCfg.UserInfoSearch.UserInfo(i).maxOpenDoorTime))
                                            'Items.SubItems.Add(Convert.ToString(JsonUserInfoSearchCfg.UserInfoSearch.UserInfo(i).openDoorTime))
                                            'ListUserInfo.Items.Add(Items)
                                        Next

                                        userStartPos = userStartPos + 10
                                    End If

                                    Exit While
                                ElseIf dwState = CInt(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_FINISH) Then
                                    'MessageBox.Show("Get User Finish")
                                    getUser = False
                                    Exit While
                                ElseIf dwState = CInt(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_EXCEPTION) Then
                                    'MessageBox.Show("Get User Exception error:" & CHCNetSDK.NET_DVR_GetLastError())
                                    getUser = False
                                    Exit While
                                Else
                                    'MessageBox.Show("unknown status error:" & CHCNetSDK.NET_DVR_GetLastError())
                                    getUser = False
                                    Exit While
                                End If
                            End While

                            Marshal.FreeHGlobal(ptrUserInfoSearchCondCfg)
                            Marshal.FreeHGlobal(ptrJsonData)
                        End While
                    End If

                    If m_lGetUserCfgHandle <> -1 Then

                        If CHCNetSDK.NET_DVR_StopRemoteConfig(m_lGetUserCfgHandle) Then
                            m_lSetUserCfgHandle = -1
                        End If
                    End If

                    Marshal.FreeHGlobal(ptrURL)
                    cn.HikvisionLogOut(lUserID)
                    'end new logic
                End If
            End If
        Next
        paycodeArray = paycodelist.Distinct.ToArray ' paycodelist.ToArray
        Common.CreateEmployee(paycodeArray, "Device")

        SplashScreenManager.CloseForm(False)
        XtraMasterTest.LabelControlStatus.Text = ""
        Application.DoEvents()
        Me.Close()
    End Sub
    Private Sub ProcessGetGatewayCardCallback(ByVal dwType As UInteger, ByVal lpBuffer As IntPtr, ByVal dwBufLen As UInteger, ByVal pUserData As IntPtr)
        If (pUserData = Nothing) Then
            Return
        End If
        If (dwType = CType(CHCNetSDK.NET_SDK_CALLBACK_TYPE.NET_SDK_CALLBACK_TYPE_DATA, UInteger)) Then
            Dim struCardCfg As CHCNetSDK.NET_DVR_CARD_CFG_V50 = New CHCNetSDK.NET_DVR_CARD_CFG_V50
            struCardCfg = CType(Marshal.PtrToStructure(lpBuffer, GetType(CHCNetSDK.NET_DVR_CARD_CFG_V50)), CHCNetSDK.NET_DVR_CARD_CFG_V50)
            Dim strCardNo As String = System.Text.Encoding.UTF8.GetString(struCardCfg.byCardNo)
            Dim pCardInfo As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(struCardCfg))
            Marshal.StructureToPtr(struCardCfg, pCardInfo, True)
            CHCNetSDK.PostMessage(pUserData, 1003, CType(pCardInfo, Integer), 0)
            'AddToCardList(struCardCfg, strCardNo);
        ElseIf (dwType = CType(CHCNetSDK.NET_SDK_CALLBACK_TYPE.NET_SDK_CALLBACK_TYPE_STATUS, UInteger)) Then
            Dim dwStatus As UInteger = CType(Marshal.ReadInt32(lpBuffer), UInteger)
            If (dwStatus = CType(CHCNetSDK.NET_SDK_CALLBACK_STATUS_NORMAL.NET_SDK_CALLBACK_STATUS_SUCCESS, UInteger)) Then
                Dim listItem As ListViewItem = New ListViewItem
                listItem.Text = "SUCC"
                listItem.SubItems.Add("NET_DVR_GET_CARD_CFG_V50 Get finish")
                'Me.AddList(listViewMessage, listItem, True)
                CHCNetSDK.PostMessage(pUserData, 1002, 0, 0)
            ElseIf (dwStatus = CType(CHCNetSDK.NET_SDK_CALLBACK_STATUS_NORMAL.NET_SDK_CALLBACK_STATUS_FAILED, UInteger)) Then
                Dim dwErrorCode As UInteger = CType(Marshal.ReadInt32((lpBuffer + 1)), UInteger)
                Dim cardNumber As String = Marshal.PtrToStringAnsi((lpBuffer + 2))
                Dim listItem As ListViewItem = New ListViewItem
                listItem.Text = "FAIL"
                listItem.SubItems.Add(String.Format("NET_DVR_GET_CARD_CFG_V50 Get Failed,ErrorCode:{0},CardNo:{1}", dwErrorCode, cardNumber))
                'Me.AddList(listViewMessage, listItem, True)
                CHCNetSDK.PostMessage(pUserData, 1002, 0, 0)
            End If
        End If
        Return
    End Sub
    'Public Delegate Sub DefWndProcCallback(ByRef m As System.Windows.Forms.Message)
    'Protected Overrides Sub DefWndProc(ByRef m As System.Windows.Forms.Message)
    '    If InvokeRequired = True Then
    '        Dim d As DefWndProcCallback = New DefWndProcCallback(AddressOf Me.DefWndProc)
    '        Me.Invoke(d, New Object() {m})
    '        'Dim tmp = IN_OUT
    '    Else
    '        Select Case (m.Msg)
    '            Case 1001
    '                Dim iErrorMsg As Integer = m.WParam.ToInt32
    '                If (-1 <> m_lSetCardCfgHandle) Then
    '                    If CHCNetSDK.NET_DVR_StopRemoteConfig(m_lSetCardCfgHandle) Then
    '                        m_lSetCardCfgHandle = -1
    '                    End If
    '                End If
    '                If (-1 <> m_lGetCardCfgHandle) Then
    '                    If CHCNetSDK.NET_DVR_StopRemoteConfig(m_lGetCardCfgHandle) Then
    '                        m_lGetCardCfgHandle = -1

    '                    End If
    '                End If
    '            Case 1002
    '                If Downloading = True Then
    '                    Downloading = False
    '                    'Dim msg As New Message("Success", "Download Finished")
    '                    'XtraMaster.AlertControl1.Show(XtraMaster, msg.Caption, msg.Text, "", msg.Image, msg)
    '                    'XtraMasterTest.LabelControlStatus.Text = ""
    '                    'Application.DoEvents()
    '                    'If Common.servername = "Access" Then
    '                    '    Me.Fptable1TableAdapter1.Fill(Me.SSSDBDataSet.fptable1)
    '                    '    GridControl2.DataSource = SSSDBDataSet.fptable1
    '                    'Else
    '                    '    FptableTableAdapter.Connection.ConnectionString = Common.ConnectionString
    '                    '    Me.FptableTableAdapter.Fill(Me.SSSDBDataSet.fptable)
    '                    '    GridControl2.DataSource = SSSDBDataSet.fptable
    '                    'End If
    '                    'GridView2.RefreshData()
    '                    'Application.DoEvents()
    '                    XtraMessageBox.Show(ulf, "<size=10>Task Completed</size>", "<size=9>Information</size>")
    '                End If

    '                'SendNextCard()
    '            Case 1003
    '                Dim pCardInfo As IntPtr = CType(m.WParam.ToInt32, IntPtr)
    '                Dim struCardCfg As CHCNetSDK.NET_DVR_CARD_CFG_V50 = New CHCNetSDK.NET_DVR_CARD_CFG_V50
    '                struCardCfg = CType(Marshal.PtrToStructure(pCardInfo, GetType(CHCNetSDK.NET_DVR_CARD_CFG_V50)), CHCNetSDK.NET_DVR_CARD_CFG_V50)
    '                Dim strCardNo As String = System.Text.Encoding.UTF8.GetString(struCardCfg.byCardNo)
    '                'AddToCardList(struCardCfg, strCardNo)
    '                'Thread.Sleep(200)
    '                saveHikvision(struCardCfg)
    '            Case Else
    '                MyBase.DefWndProc(m)
    '        End Select
    '    End If
    'End Sub
    Private Sub AddToCardList(ByVal struCardInfo As CHCNetSDK.NET_DVR_CARD_CFG_V50, ByVal strCardNo As String)
        Dim iItemIndex As Integer = 0 'GetExistItem(struCardInfo)
        If (-1 = iItemIndex) Then
            iItemIndex = 0 ' listViewGataManage.Items.Count
        End If
        'UpdateList(iItemIndex, strCardNo, struCardInfo)
        m_struCardInfo(iItemIndex) = struCardInfo
    End Sub
    'Public Delegate Sub SetTextCallbacksaveHikvision(ByVal struCardCfg As CHCNetSDK.NET_DVR_CARD_CFG_V50)
    'Private Sub saveHikvision(ByVal struCardCfg As CHCNetSDK.NET_DVR_CARD_CFG_V50)
    '    If InvokeRequired = True Then
    '        Dim d As SetTextCallbacksaveHikvision = New SetTextCallbacksaveHikvision(AddressOf Me.saveHikvision)
    '        Me.Invoke(d, New Object() {struCardCfg})
    '        'Dim tmp = IN_OUT
    '    Else
    '        Dim cn As Common = New Common
    '        Dim con1 As OleDbConnection
    '        Dim con As SqlConnection
    '        If Common.servername = "Access" Then
    '            con1 = New OleDbConnection(Common.ConnectionString)
    '        Else
    '            con = New SqlConnection(Common.ConnectionString)
    '        End If

    '        Dim EnrollNumber As String = System.Text.Encoding.UTF8.GetString(struCardCfg.byCardNo)
    '        'HCardNos.Add(EnrollNumber)
    '        GetHFp(EnrollNumber)
    '        If IsNumeric(EnrollNumber) Then
    '            EnrollNumber = Convert.ToDouble(EnrollNumber).ToString("000000000000")
    '        End If
    '        Dim DeviceType As String = "HKSeries"
    '        Dim Password As String = System.Text.Encoding.UTF8.GetString(struCardCfg.byCardPassword)

    '        Dim del As invokeDelegate = Function()
    '                                        XtraMasterTest.LabelControlStatus.Text = "Downloading Template " & EnrollNumber
    '                                    End Function
    '        Invoke(del)
    '        Dim paycodelist As New List(Of String)()
    '        paycodelist.Add(EnrollNumber & ";" & System.Text.Encoding.UTF8.GetString(struCardCfg.byName).Trim)
    '        Dim paycodeArray = paycodelist.Distinct.ToArray
    '        Common.CreateEmployee(paycodeArray)

    '        'Dim x As String = serialNo.ToString.Trim 'Replace("-", "")
    '        'Dim adap As SqlDataAdapter
    '        'Dim adapA As OleDbDataAdapter
    '        'Dim sSql As String = "select * from fptable where [EMachineNumber]='" & x & "' and EnrollNumber ='" & EnrollNumber & "' and FingerNumber=11 "
    '        'Dim dsRecord As DataSet = New DataSet
    '        'If Common.servername = "Access" Then
    '        '    adapA = New OleDbDataAdapter(sSql, con1)
    '        '    adapA.Fill(dsRecord)
    '        'Else
    '        '    adap = New SqlDataAdapter(sSql, con)
    '        '    adap.Fill(dsRecord)
    '        'End If
    '        'If dsRecord.Tables(0).Rows.Count = 0 Then
    '        '    sSql = "insert into fptable (Emachinenumber, EnrollNumber, UserName, CardNumber, FingerNumber, Privilege) values ('" & serialNo.Trim & "','" & EnrollNumber & "', '','', '11', '0')"
    '        '    If Common.servername = "Access" Then
    '        '        If Common.con1.State <> ConnectionState.Open Then
    '        '            Common.con1.Open()
    '        '        End If
    '        '        Dim cmd As OleDbCommand
    '        '        cmd = New OleDbCommand(sSql, con1)
    '        '        cmd.ExecuteNonQuery()

    '        '        If con1.State <> ConnectionState.Closed Then
    '        '            con1.Close()
    '        '        End If
    '        '    Else
    '        '        If con.State <> ConnectionState.Open Then
    '        '            con.Open()
    '        '        End If
    '        '        Dim cmd As SqlCommand
    '        '        cmd = New SqlCommand(sSql, con)
    '        '        cmd.ExecuteNonQuery()
    '        '        If con.State <> ConnectionState.Closed Then
    '        '            con.Close()
    '        '        End If
    '        '    End If

    '        '    'GridView2.RefreshData()
    '        '    If Common.servername = "Access" Then
    '        '        Me.Fptable1TableAdapter1.Fill(Me.SSSDBDataSet.fptable1)
    '        '        GridControl2.DataSource = SSSDBDataSet.fptable1
    '        '    Else
    '        '        FptableTableAdapter.Connection.ConnectionString = Common.ConnectionString
    '        '        Me.FptableTableAdapter.Fill(Me.SSSDBDataSet.fptable)
    '        '        GridControl2.DataSource = SSSDBDataSet.fptable
    '        '    End If
    '        '    Application.DoEvents()
    '        'End If

    '        'If IsNumeric(EnrollNumber) Then
    '        '    EnrollNumber = Convert.ToDouble(EnrollNumber)
    '        'End If
    '        'GetHFp(EnrollNumber)
    '    End If
    'End Sub
    Private Function SendNextCard() As Boolean
        If (-1 = m_lSetCardCfgHandle) Then
            Return False
        End If
        m_dwSendIndex = (m_dwSendIndex + 1)
        If (m_dwSendIndex >= m_dwCardNum) Then
            'CHCNetSDK.NET_DVR_StopRemoteConfig(m_lSetCardCfgHandle);
            'this.StopRemoteCfg(ref m_lSetCardCfgHandle);
            'm_lSetCardCfgHandle = -1;
            Dim listItem2 As ListViewItem = New ListViewItem
            listItem2.Text = "SUCC"
            Dim strTemp2 As String = Nothing
            strTemp2 = String.Format("Send {0} card(s) over", m_dwCardNum)
            listItem2.SubItems.Add(strTemp2)
            'Me.AddList(listViewMessage, listItem2, True)
            Return True
        End If

        m_struNowSendCard = m_struCardInfo(m_dwSendIndex)
        Dim dwSize As UInteger = CType(Marshal.SizeOf(m_struNowSendCard), UInteger)
        Dim ptrSendCard As IntPtr = Marshal.AllocHGlobal(CType(dwSize, Integer))
        Marshal.StructureToPtr(m_struNowSendCard, ptrSendCard, False)
        If Not CHCNetSDK.NET_DVR_SendRemoteConfig(m_lSetCardCfgHandle, 3, ptrSendCard, dwSize) Then
            Marshal.FreeHGlobal(ptrSendCard)
            Dim listItem3 As ListViewItem = New ListViewItem
            listItem3.Text = "FAIL"
            Dim strTemp3 As String = Nothing
            strTemp3 = String.Format("Send Fail,CardNO:{0}", System.Text.Encoding.UTF8.GetString(m_struNowSendCard.byCardNo).TrimEnd(Microsoft.VisualBasic.ChrW(92)))
            listItem3.SubItems.Add(strTemp3)
            'Me.AddList(listViewMessage, listItem3, True)           
            Return False
        End If

        Marshal.FreeHGlobal(ptrSendCard)
        Return True
    End Function
    'Private Sub GetHFp(ByVal CardNo As String)
    '    Thread.Sleep(500)
    '    If (m_lGetFingerPrintCfgHandle <> -1) Then
    '        CHCNetSDK.NET_DVR_StopRemoteConfig(CType(m_lGetFingerPrintCfgHandle, Integer))
    '    End If
    '    Dim struCond As CHCNetSDK.NET_DVR_FINGER_PRINT_INFO_COND_V50 = New CHCNetSDK.NET_DVR_FINGER_PRINT_INFO_COND_V50
    '    struCond.byCardNo = New Byte((32) - 1) {}
    '    struCond.byEmployeeNo = New Byte((32) - 1) {}
    '    struCond.dwSize = CType(Marshal.SizeOf(struCond), UInteger)
    '    'UInteger.TryParse(textBoxNumber.Text, struCond.dwFingerPrintNum)
    '    struCond.dwFingerPrintNum = 0
    '    Dim byTempEmployeeNo() As Byte = System.Text.Encoding.UTF8.GetBytes("")
    '    Dim i As Integer = 0
    '    Do While (i < byTempEmployeeNo.Length)
    '        struCond.byEmployeeNo(i) = byTempEmployeeNo(i)
    '        i = (i + 1)
    '    Loop
    '    Byte.TryParse("1", struCond.byFingerPrintID)
    '    Dim byTemp() As Byte = System.Text.Encoding.UTF8.GetBytes(CardNo)
    '    i = 0
    '    Do While (i < byTemp.Length)
    '        struCond.byCardNo(i) = byTemp(i)
    '        i = (i + 1)
    '    Loop
    '    GetTreeSel()
    '    struCond.byEnableCardReader = m_struFingerPrintOne.byEnableCardReader
    '    Dim dwSize As Integer = Marshal.SizeOf(struCond)
    '    Dim ptrStruCond As IntPtr = Marshal.AllocHGlobal(dwSize)
    '    Marshal.StructureToPtr(struCond, ptrStruCond, False)
    '    'Thread.Sleep(200)  'test
    '    g_fGetFingerPrintCallback = New CHCNetSDK.RemoteConfigCallback(AddressOf ProcessGetFingerPrintCfgCallbackData)
    '    m_lGetFingerPrintCfgHandle = CHCNetSDK.NET_DVR_StartRemoteConfig(m_lUserID, CHCNetSDK.NET_DVR_GET_FINGERPRINT_CFG_V50, ptrStruCond, dwSize, g_fGetFingerPrintCallback, Me.Handle)
    '    If (-1 = m_lGetFingerPrintCfgHandle) Then
    '        Dim listItem As ListViewItem = New ListViewItem
    '        listItem.Text = "FAIL"
    '        Dim strTemp As String = String.Format("NET_DVR_GET_FINGERPRINT_CFG_V50 FAIL, ERROR CODE {0}", CHCNetSDK.NET_DVR_GetLastError)
    '        listItem.SubItems.Add(strTemp)
    '        'Me.AddList(listViewMessage, listItem)
    '        Marshal.FreeHGlobal(ptrStruCond)
    '        Return
    '    Else
    '        Dim listItem As ListViewItem = New ListViewItem
    '        listItem.Text = "SUCC"
    '        listItem.SubItems.Add("NET_DVR_GET_FINGERPRINT_CFG_V50")
    '        'Me.AddList(listViewMessage, listItem)
    '        Marshal.FreeHGlobal(ptrStruCond)
    '    End If

    'End Sub
    'Public Delegate Sub ProcessGetFingerPrintCfgCallbackDataCallback(ByVal dwType As UInteger, ByVal lpBuffer As IntPtr, ByVal dwBufLen As UInteger, ByVal pUserData As IntPtr)
    'Private Sub ProcessGetFingerPrintCfgCallbackData(ByVal dwType As UInteger, ByVal lpBuffer As IntPtr, ByVal dwBufLen As UInteger, ByVal pUserData As IntPtr)
    '    'If InvokeRequired = True Then
    '    '    Dim d As ProcessGetFingerPrintCfgCallbackDataCallback = New ProcessGetFingerPrintCfgCallbackDataCallback(AddressOf Me.ProcessGetFingerPrintCfgCallbackData)
    '    '    Me.Invoke(d, New Object() {dwType, lpBuffer, dwBufLen, pUserData})
    '    '    'Dim tmp = IN_OUT
    '    'Else

    '    Thread.Sleep(50)
    '    If (pUserData = Nothing) Then
    '        Return
    '    End If
    '    If (dwType = CType(CHCNetSDK.NET_SDK_CALLBACK_TYPE.NET_SDK_CALLBACK_TYPE_DATA, UInteger)) Then
    '        Dim strFingerPrintCfg As CHCNetSDK.NET_DVR_FINGER_PRINT_CFG_V50 = New CHCNetSDK.NET_DVR_FINGER_PRINT_CFG_V50
    '        strFingerPrintCfg = CType(Marshal.PtrToStructure(lpBuffer, GetType(CHCNetSDK.NET_DVR_FINGER_PRINT_CFG_V50)), CHCNetSDK.NET_DVR_FINGER_PRINT_CFG_V50)
    '        'Marshal.PtrToStructure(lpBuffer, strCardCfg);
    '        Dim bSendOK As Boolean = False
    '        Dim i As Integer = 0
    '        Do While (i < strFingerPrintCfg.byEnableCardReader.Length)
    '            If (1 = strFingerPrintCfg.byEnableCardReader(i)) Then
    '                bSendOK = True
    '            End If
    '            i = (i + 1)
    '        Loop

    '        If Not bSendOK Then
    '        End If
    '        bSendOK = False
    '        i = 0
    '        Do While (i < strFingerPrintCfg.byLeaderFP.Length)
    '            If (1 = strFingerPrintCfg.byLeaderFP(i)) Then
    '                bSendOK = True
    '            End If
    '            i = (i + 1)
    '        Loop
    '        If Not bSendOK Then
    '        End If
    '        If (0 = strFingerPrintCfg.dwSize) Then
    '            Return
    '        End If
    '        'AddToFingerPrintList(strFingerPrintCfg, False)
    '        Dim con1 As OleDbConnection
    '        Dim con As SqlConnection
    '        If Common.servername = "Access" Then
    '            con1 = New OleDbConnection(Common.ConnectionString)
    '        Else
    '            con = New SqlConnection(Common.ConnectionString)
    '        End If
    '        Dim ds As DataSet = New DataSet
    '        Dim adap As SqlDataAdapter
    '        Dim adapA As OleDbDataAdapter
    '        Dim Template As String = Encoding.ASCII.GetString(strFingerPrintCfg.byFingerData)
    '        Dim EnrollNumber As String = Convert.ToDouble(System.Text.Encoding.UTF8.GetString(strFingerPrintCfg.byCardNo))

    '        XtraMasterTest.LabelControlStatus.Text = "Downloading Template " & EnrollNumber
    '        Application.DoEvents()

    '        Dim FingerNumber As String = strFingerPrintCfg.byFingerPrintID.ToString
    '        serialNo = serialNo.Trim
    '        Dim strPath As String = System.Environment.CurrentDirectory & "\FTemplate\" & EnrollNumber.ToString.Trim & "_" & serialNo.Trim.Substring(0, 5) & "fingerprint.dat"
    '        Dim fs As FileStream = New FileStream(strPath, FileMode.OpenOrCreate)
    '        If Not File.Exists(strPath) Then
    '            MessageBox.Show("Fingerprint storage file creat failed")
    '        End If
    '        Dim objBinaryWrite As BinaryWriter = New BinaryWriter(fs)
    '        fs.Write(strFingerPrintCfg.byFingerData, 0, CType(strFingerPrintCfg.dwFingerPrintLen, Integer))
    '        fs.Close()

    '        'Dim EMachineNumber As String = serialNo
    '        If IsNumeric(EnrollNumber) Then
    '            EnrollNumber = Convert.ToDouble(EnrollNumber).ToString("000000000000")
    '        End If
    '        'serialNo = "DS-K1T804MF-120170911V010100ENC13360348"
    '        Dim SrTmp As String = serialNo.Trim
    '        Dim Sql1 As String = "select * from fptable Where  [EMachineNumber] = '" & SrTmp.Trim & "' and EnrollNumber ='" & EnrollNumber & "' and FingerNumber=" & FingerNumber & ""
    '        Dim HTemplate As String
    '        If Common.servername = "Access" Then
    '            adapA = New OleDbDataAdapter(Sql1, con1)
    '            adapA.Fill(ds)
    '        Else
    '            adap = New SqlDataAdapter(Sql1, con)
    '            adap.Fill(ds)
    '        End If


    '        If ds.Tables(0).Rows.Count > 0 Then
    '            Sql1 = "update fptable set HTemplatePath='" & strPath & "' where EMachineNumber = '" & serialNo.Trim & "' and EnrollNumber ='" & EnrollNumber & "' and FingerNumber='" & FingerNumber & "' "
    '        Else
    '            Sql1 = "insert into fptable (Emachinenumber, EnrollNumber, UserName, CardNumber, FingerNumber, HTemplatePath) values ('" & serialNo.Trim & "','" & EnrollNumber & "', '','', '" & FingerNumber & "', '" & strPath & "')"
    '        End If
    '        If Common.servername = "Access" Then
    '            If con1.State <> ConnectionState.Open Then
    '                con1.Open()
    '            End If
    '            Dim cmd As OleDbCommand
    '            cmd = New OleDbCommand(Sql1, con1)
    '            cmd.ExecuteNonQuery()

    '            If con1.State <> ConnectionState.Closed Then
    '                con1.Close()
    '            End If
    '        Else
    '            If con.State <> ConnectionState.Open Then
    '                con.Open()
    '            End If
    '            Dim cmd As SqlCommand
    '            cmd = New SqlCommand(Sql1, con)
    '            cmd.ExecuteNonQuery()
    '            If con.State <> ConnectionState.Closed Then
    '                con.Close()
    '            End If
    '        End If
    '    ElseIf (dwType = CType(CHCNetSDK.NET_SDK_CALLBACK_TYPE.NET_SDK_CALLBACK_TYPE_STATUS, UInteger)) Then
    '        Dim dwStatus As UInteger = 0
    '        dwStatus = CType(Marshal.ReadInt32(lpBuffer), UInteger)
    '        If (dwStatus = CType(CHCNetSDK.NET_SDK_CALLBACK_STATUS_NORMAL.NET_SDK_CALLBACK_STATUS_SUCCESS, UInteger)) Then
    '            Dim listItem As ListViewItem = New ListViewItem
    '            listItem.Text = "SUCC"
    '            listItem.SubItems.Add("NET_DVR_GET_FINGERPRINT_CFG_V50 Get finish")
    '        ElseIf (dwStatus = CType(CHCNetSDK.NET_SDK_CALLBACK_STATUS_NORMAL.NET_SDK_CALLBACK_STATUS_FAILED, UInteger)) Then
    '            Dim listItem As ListViewItem = New ListViewItem
    '            listItem.Text = "FAIL"
    '            listItem.SubItems.Add("NET_DVR_GET_FINGERPRINT_CFG_V50 Get FAIL")
    '            'Me.AddList(listViewMessage, listItem)
    '        End If
    '    End If
    '    'End If
    'End Sub
    'Private Sub GetTreeSel()
    '    Dim i As Integer = 0
    '    Do While (i < 512)
    '        If i = 0 Then
    '            m_struFingerPrintOne.byEnableCardReader(i) = 1
    '        Else
    '            m_struFingerPrintOne.byEnableCardReader(i) = 0
    '        End If
    '        i = (i + 1)
    '    Loop
    '    i = 0
    '    Do While (i < 256)
    '        m_struFingerPrintOne.byLeaderFP(i) = 0
    '        i = (i + 1)
    '    Loop
    '    i = 0
    '    Do While (i < 10)
    '        m_struDelFingerPrint.struByCard.byFingerPrintID(i) = 0
    '        i = (i + 1)
    '    Loop
    'End Sub

    Private Sub SimpleButton1_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton1.Click
        Me.Close()
    End Sub
End Class