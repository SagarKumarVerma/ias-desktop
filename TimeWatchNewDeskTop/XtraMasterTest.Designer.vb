﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraMasterTest
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(XtraMasterTest))
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.SidePanelMainFormShow = New DevExpress.XtraEditors.SidePanel()
        Me.SidePanelTitle = New DevExpress.XtraEditors.SidePanel()
        Me.LabelTitle = New System.Windows.Forms.Label()
        Me.SidePanel1 = New DevExpress.XtraEditors.SidePanel()
        Me.SidePanel8 = New DevExpress.XtraEditors.SidePanel()
        Me.LabelControlCount = New DevExpress.XtraEditors.LabelControl()
        Me.SidePanel7 = New DevExpress.XtraEditors.SidePanel()
        Me.LabelControlUser = New DevExpress.XtraEditors.LabelControl()
        Me.SidePanel6 = New DevExpress.XtraEditors.SidePanel()
        Me.LabelControlStatus = New DevExpress.XtraEditors.LabelControl()
        Me.SidePanel5 = New DevExpress.XtraEditors.SidePanel()
        Me.LabelControlTime = New DevExpress.XtraEditors.LabelControl()
        Me.SidePanel4 = New DevExpress.XtraEditors.SidePanel()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.SidePanel3 = New DevExpress.XtraEditors.SidePanel()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.TimerShowClock = New System.Windows.Forms.Timer(Me.components)
        Me.TimerReportEmail = New System.Windows.Forms.Timer(Me.components)
        Me.AlertControl1 = New DevExpress.XtraBars.Alerter.AlertControl(Me.components)
        Me.ToastNotificationsManager1 = New DevExpress.XtraBars.ToastNotifications.ToastNotificationsManager(Me.components)
        Me.TimerDataProcess = New System.Windows.Forms.Timer(Me.components)
        Me.TimerSMS = New System.Windows.Forms.Timer(Me.components)
        Me.TimerCloud = New System.Windows.Forms.Timer(Me.components)
        Me.TimerAutoDownload = New System.Windows.Forms.Timer(Me.components)
        Me.TimerAutpBK = New System.Windows.Forms.Timer(Me.components)
        Me.BarManager1 = New DevExpress.XtraBars.BarManager(Me.components)
        Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl()
        Me.BarManager2 = New DevExpress.XtraBars.BarManager(Me.components)
        Me.Bar1 = New DevExpress.XtraBars.Bar()
        Me.BarButtonItemHome = New DevExpress.XtraBars.BarButtonItem()
        Me.BarSubItemDevice = New DevExpress.XtraBars.BarSubItem()
        Me.BarMenuDevice = New DevExpress.XtraBars.BarSubItem()
        Me.BarButtoniAS = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonUltra = New DevExpress.XtraBars.BarButtonItem()
        Me.BarMenuLog = New DevExpress.XtraBars.BarSubItem()
        Me.BarButtonLogiAS = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonLogUltra = New DevExpress.XtraBars.BarButtonItem()
        Me.BarMenuTemplate = New DevExpress.XtraBars.BarSubItem()
        Me.BarButtonTempiAS = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonTempUltra = New DevExpress.XtraBars.BarButtonItem()
        Me.BarMenuRealTime = New DevExpress.XtraBars.BarSubItem()
        Me.BarButtonRealiAS = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonRealUltra = New DevExpress.XtraBars.BarButtonItem()
        Me.BarSubItemMaster = New DevExpress.XtraBars.BarSubItem()
        Me.BarButtonItemComp = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItemLocation = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItemDept = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItemShift = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItemGrade = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItemEmpGrp = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItemBank = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItemDisp = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItemCat = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItemEmp = New DevExpress.XtraBars.BarButtonItem()
        Me.BarSubItemTrans = New DevExpress.XtraBars.BarSubItem()
        Me.BarButtonItemManual = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItemDataMaint = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItemHoliday = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItemOTMaint = New DevExpress.XtraBars.BarButtonItem()
        Me.BarSubItemLeaveMgmt = New DevExpress.XtraBars.BarSubItem()
        Me.BarButtonItemLMaster = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItemlAcc = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItemLApp = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItemLeaveIncr = New DevExpress.XtraBars.BarButtonItem()
        Me.BarSubItemAdmin = New DevExpress.XtraBars.BarSubItem()
        Me.BarButtonItemCSettings = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItemDPro = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItemDB = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItemSMS = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItemBSMS = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItemEmail = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItemUManage = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItemBackUp = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItemPDB = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem2 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem6 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonOTComp = New DevExpress.XtraBars.BarButtonItem()
        Me.BarSubItemReports = New DevExpress.XtraBars.BarSubItem()
        Me.BarButtonItemDReports = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItemMReports = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItemLReports = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItemCreports = New DevExpress.XtraBars.BarButtonItem()
        Me.BarSubItemPayRoll = New DevExpress.XtraBars.BarSubItem()
        Me.BarButtonItemPEmp = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItemPSetup = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItemFormula = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItemPayPro = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItemPerMain = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItemLoan = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItemPayReports = New DevExpress.XtraBars.BarButtonItem()
        Me.BarSubItemCanteen = New DevExpress.XtraBars.BarSubItem()
        Me.BarButtonItemSlab = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItemMealMenu = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItemCanReports = New DevExpress.XtraBars.BarButtonItem()
        Me.BarSubItemVisitor = New DevExpress.XtraBars.BarSubItem()
        Me.BarButtonItemVEntry = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItemVHistory = New DevExpress.XtraBars.BarButtonItem()
        Me.BarDockControl1 = New DevExpress.XtraBars.BarDockControl()
        Me.BarDockControl2 = New DevExpress.XtraBars.BarDockControl()
        Me.BarDockControl3 = New DevExpress.XtraBars.BarDockControl()
        Me.BarDockControl4 = New DevExpress.XtraBars.BarDockControl()
        Me.BarButtonHome = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem3 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarSubItem1 = New DevExpress.XtraBars.BarSubItem()
        Me.BarButtonItem4 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem5 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItemDMgmt = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem1 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarSubItem2 = New DevExpress.XtraBars.BarSubItem()
        Me.PopupMenu1 = New DevExpress.XtraBars.PopupMenu(Me.components)
        Me.TimerIOCL = New System.Windows.Forms.Timer(Me.components)
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        Me.SidePanelTitle.SuspendLayout()
        Me.SidePanel1.SuspendLayout()
        Me.SidePanel8.SuspendLayout()
        Me.SidePanel7.SuspendLayout()
        Me.SidePanel6.SuspendLayout()
        Me.SidePanel5.SuspendLayout()
        Me.SidePanel4.SuspendLayout()
        Me.SidePanel3.SuspendLayout()
        CType(Me.ToastNotificationsManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BarManager2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupMenu1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.SidePanelMainFormShow)
        Me.PanelControl1.Controls.Add(Me.SidePanelTitle)
        Me.PanelControl1.Controls.Add(Me.SidePanel1)
        Me.PanelControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelControl1.Location = New System.Drawing.Point(0, 84)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(1184, 627)
        Me.PanelControl1.TabIndex = 3
        '
        'SidePanelMainFormShow
        '
        Me.SidePanelMainFormShow.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SidePanelMainFormShow.Location = New System.Drawing.Point(2, 43)
        Me.SidePanelMainFormShow.Name = "SidePanelMainFormShow"
        Me.SidePanelMainFormShow.Size = New System.Drawing.Size(1180, 559)
        Me.SidePanelMainFormShow.TabIndex = 3
        Me.SidePanelMainFormShow.Text = "SidePanel2"
        '
        'SidePanelTitle
        '
        Me.SidePanelTitle.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.SidePanelTitle.Appearance.Options.UseBackColor = True
        Me.SidePanelTitle.Controls.Add(Me.LabelTitle)
        Me.SidePanelTitle.Dock = System.Windows.Forms.DockStyle.Top
        Me.SidePanelTitle.Location = New System.Drawing.Point(2, 2)
        Me.SidePanelTitle.Name = "SidePanelTitle"
        Me.SidePanelTitle.Size = New System.Drawing.Size(1180, 41)
        Me.SidePanelTitle.TabIndex = 2
        Me.SidePanelTitle.Text = "SidePanel2"
        '
        'LabelTitle
        '
        Me.LabelTitle.AutoSize = True
        Me.LabelTitle.Font = New System.Drawing.Font("Tahoma", 15.0!)
        Me.LabelTitle.ForeColor = System.Drawing.Color.SteelBlue
        Me.LabelTitle.Location = New System.Drawing.Point(4, 7)
        Me.LabelTitle.Name = "LabelTitle"
        Me.LabelTitle.Size = New System.Drawing.Size(94, 24)
        Me.LabelTitle.TabIndex = 0
        Me.LabelTitle.Text = "              "
        '
        'SidePanel1
        '
        Me.SidePanel1.AllowResize = False
        Me.SidePanel1.Controls.Add(Me.SidePanel8)
        Me.SidePanel1.Controls.Add(Me.SidePanel7)
        Me.SidePanel1.Controls.Add(Me.SidePanel6)
        Me.SidePanel1.Controls.Add(Me.SidePanel5)
        Me.SidePanel1.Controls.Add(Me.SidePanel4)
        Me.SidePanel1.Controls.Add(Me.SidePanel3)
        Me.SidePanel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.SidePanel1.Location = New System.Drawing.Point(2, 602)
        Me.SidePanel1.Name = "SidePanel1"
        Me.SidePanel1.Size = New System.Drawing.Size(1180, 23)
        Me.SidePanel1.TabIndex = 0
        Me.SidePanel1.Text = "SidePanel1"
        '
        'SidePanel8
        '
        Me.SidePanel8.Controls.Add(Me.LabelControlCount)
        Me.SidePanel8.Dock = System.Windows.Forms.DockStyle.Right
        Me.SidePanel8.Location = New System.Drawing.Point(459, 1)
        Me.SidePanel8.Name = "SidePanel8"
        Me.SidePanel8.Size = New System.Drawing.Size(270, 22)
        Me.SidePanel8.TabIndex = 5
        Me.SidePanel8.Text = "SidePanel8"
        '
        'LabelControlCount
        '
        Me.LabelControlCount.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControlCount.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.LabelControlCount.Appearance.Options.UseFont = True
        Me.LabelControlCount.Appearance.Options.UseForeColor = True
        Me.LabelControlCount.Location = New System.Drawing.Point(4, 4)
        Me.LabelControlCount.Name = "LabelControlCount"
        Me.LabelControlCount.Size = New System.Drawing.Size(48, 14)
        Me.LabelControlCount.TabIndex = 4
        Me.LabelControlCount.Text = "            "
        '
        'SidePanel7
        '
        Me.SidePanel7.Controls.Add(Me.LabelControlUser)
        Me.SidePanel7.Dock = System.Windows.Forms.DockStyle.Right
        Me.SidePanel7.Location = New System.Drawing.Point(729, 1)
        Me.SidePanel7.Name = "SidePanel7"
        Me.SidePanel7.Size = New System.Drawing.Size(74, 22)
        Me.SidePanel7.TabIndex = 4
        Me.SidePanel7.Text = "SidePanel7"
        '
        'LabelControlUser
        '
        Me.LabelControlUser.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControlUser.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.LabelControlUser.Appearance.Options.UseFont = True
        Me.LabelControlUser.Appearance.Options.UseForeColor = True
        Me.LabelControlUser.Location = New System.Drawing.Point(5, 3)
        Me.LabelControlUser.Name = "LabelControlUser"
        Me.LabelControlUser.Size = New System.Drawing.Size(56, 14)
        Me.LabelControlUser.TabIndex = 3
        Me.LabelControlUser.Text = "              "
        '
        'SidePanel6
        '
        Me.SidePanel6.Controls.Add(Me.LabelControlStatus)
        Me.SidePanel6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SidePanel6.Location = New System.Drawing.Point(124, 1)
        Me.SidePanel6.Name = "SidePanel6"
        Me.SidePanel6.Size = New System.Drawing.Size(679, 22)
        Me.SidePanel6.TabIndex = 3
        Me.SidePanel6.Text = "SidePanel6"
        '
        'LabelControlStatus
        '
        Me.LabelControlStatus.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControlStatus.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.LabelControlStatus.Appearance.Options.UseFont = True
        Me.LabelControlStatus.Appearance.Options.UseForeColor = True
        Me.LabelControlStatus.Location = New System.Drawing.Point(4, 4)
        Me.LabelControlStatus.Name = "LabelControlStatus"
        Me.LabelControlStatus.Size = New System.Drawing.Size(32, 14)
        Me.LabelControlStatus.TabIndex = 3
        Me.LabelControlStatus.Text = "        "
        '
        'SidePanel5
        '
        Me.SidePanel5.Controls.Add(Me.LabelControlTime)
        Me.SidePanel5.Dock = System.Windows.Forms.DockStyle.Right
        Me.SidePanel5.Location = New System.Drawing.Point(803, 1)
        Me.SidePanel5.Name = "SidePanel5"
        Me.SidePanel5.Size = New System.Drawing.Size(100, 22)
        Me.SidePanel5.TabIndex = 2
        Me.SidePanel5.Text = "SidePanel5"
        '
        'LabelControlTime
        '
        Me.LabelControlTime.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControlTime.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.LabelControlTime.Appearance.Options.UseFont = True
        Me.LabelControlTime.Appearance.Options.UseForeColor = True
        Me.LabelControlTime.Location = New System.Drawing.Point(4, 3)
        Me.LabelControlTime.Name = "LabelControlTime"
        Me.LabelControlTime.Size = New System.Drawing.Size(56, 14)
        Me.LabelControlTime.TabIndex = 3
        Me.LabelControlTime.Text = "              "
        '
        'SidePanel4
        '
        Me.SidePanel4.Controls.Add(Me.LabelControl2)
        Me.SidePanel4.Dock = System.Windows.Forms.DockStyle.Right
        Me.SidePanel4.Location = New System.Drawing.Point(903, 1)
        Me.SidePanel4.Name = "SidePanel4"
        Me.SidePanel4.Size = New System.Drawing.Size(277, 22)
        Me.SidePanel4.TabIndex = 1
        Me.SidePanel4.Text = "SidePanel4"
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl2.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.LabelControl2.Appearance.Options.UseFont = True
        Me.LabelControl2.Appearance.Options.UseForeColor = True
        Me.LabelControl2.Location = New System.Drawing.Point(4, 3)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(74, 14)
        Me.LabelControl2.TabIndex = 0
        Me.LabelControl2.Text = "LabelControl2"
        '
        'SidePanel3
        '
        Me.SidePanel3.Controls.Add(Me.LabelControl1)
        Me.SidePanel3.Dock = System.Windows.Forms.DockStyle.Left
        Me.SidePanel3.Location = New System.Drawing.Point(0, 1)
        Me.SidePanel3.Name = "SidePanel3"
        Me.SidePanel3.Size = New System.Drawing.Size(124, 22)
        Me.SidePanel3.TabIndex = 0
        Me.SidePanel3.Text = "SidePanel3"
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.LabelControl1.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Appearance.Options.UseForeColor = True
        Me.LabelControl1.Location = New System.Drawing.Point(3, 3)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(70, 16)
        Me.LabelControl1.TabIndex = 0
        Me.LabelControl1.Text = "iAS V 2.3.2"
        '
        'TimerShowClock
        '
        Me.TimerShowClock.Enabled = True
        Me.TimerShowClock.Interval = 1000
        '
        'TimerReportEmail
        '
        Me.TimerReportEmail.Enabled = True
        Me.TimerReportEmail.Interval = 60000
        '
        'AlertControl1
        '
        Me.AlertControl1.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold)
        Me.AlertControl1.AppearanceCaption.Options.UseFont = True
        Me.AlertControl1.AppearanceHotTrackedText.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Underline)
        Me.AlertControl1.AppearanceHotTrackedText.Options.UseFont = True
        Me.AlertControl1.AppearanceText.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.AlertControl1.AppearanceText.Options.UseFont = True
        Me.AlertControl1.LookAndFeel.SkinName = "iMaginary"
        Me.AlertControl1.LookAndFeel.UseDefaultLookAndFeel = False
        '
        'ToastNotificationsManager1
        '
        Me.ToastNotificationsManager1.ApplicationId = "70e1a3f0-8af9-450c-8446-955b2f839bb3"
        Me.ToastNotificationsManager1.ApplicationName = "TimeWatchNewDeskTop"
        Me.ToastNotificationsManager1.Notifications.AddRange(New DevExpress.XtraBars.ToastNotifications.IToastNotificationProperties() {New DevExpress.XtraBars.ToastNotifications.ToastNotification("4f8cfe13-3e07-48f8-bddf-9d5c77387978", Nothing, "Pellentesque lacinia tellus eget volutpat", "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor i" &
                    "ncididunt ut labore et dolore magna aliqua.", "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor i" &
                    "ncididunt ut labore et dolore magna aliqua.", DevExpress.XtraBars.ToastNotifications.ToastNotificationTemplate.Text01)})
        '
        'TimerDataProcess
        '
        Me.TimerDataProcess.Interval = 1000
        '
        'TimerSMS
        '
        Me.TimerSMS.Interval = 60000
        '
        'TimerCloud
        '
        '
        'TimerAutoDownload
        '
        Me.TimerAutoDownload.Interval = 60000
        '
        'TimerAutpBK
        '
        Me.TimerAutpBK.Enabled = True
        Me.TimerAutpBK.Interval = 30000
        '
        'BarManager1
        '
        Me.BarManager1.AllowShowToolbarsPopup = False
        Me.BarManager1.DockControls.Add(Me.barDockControlTop)
        Me.BarManager1.DockControls.Add(Me.barDockControlBottom)
        Me.BarManager1.DockControls.Add(Me.barDockControlLeft)
        Me.BarManager1.DockControls.Add(Me.barDockControlRight)
        Me.BarManager1.Form = Me
        Me.BarManager1.MaxItemId = 0
        '
        'barDockControlTop
        '
        Me.barDockControlTop.CausesValidation = False
        Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.barDockControlTop.Location = New System.Drawing.Point(0, 84)
        Me.barDockControlTop.Manager = Me.BarManager1
        Me.barDockControlTop.Size = New System.Drawing.Size(1184, 0)
        '
        'barDockControlBottom
        '
        Me.barDockControlBottom.CausesValidation = False
        Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.barDockControlBottom.Location = New System.Drawing.Point(0, 711)
        Me.barDockControlBottom.Manager = Me.BarManager1
        Me.barDockControlBottom.Size = New System.Drawing.Size(1184, 0)
        '
        'barDockControlLeft
        '
        Me.barDockControlLeft.CausesValidation = False
        Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
        Me.barDockControlLeft.Location = New System.Drawing.Point(0, 84)
        Me.barDockControlLeft.Manager = Me.BarManager1
        Me.barDockControlLeft.Size = New System.Drawing.Size(0, 627)
        '
        'barDockControlRight
        '
        Me.barDockControlRight.CausesValidation = False
        Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
        Me.barDockControlRight.Location = New System.Drawing.Point(1184, 84)
        Me.barDockControlRight.Manager = Me.BarManager1
        Me.barDockControlRight.Size = New System.Drawing.Size(0, 627)
        '
        'BarManager2
        '
        Me.BarManager2.AllowShowToolbarsPopup = False
        Me.BarManager2.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar1})
        Me.BarManager2.DockControls.Add(Me.BarDockControl1)
        Me.BarManager2.DockControls.Add(Me.BarDockControl2)
        Me.BarManager2.DockControls.Add(Me.BarDockControl3)
        Me.BarManager2.DockControls.Add(Me.BarDockControl4)
        Me.BarManager2.Form = Me
        Me.BarManager2.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.BarButtonHome, Me.BarButtonItem3, Me.BarSubItem1, Me.BarButtonItem4, Me.BarButtonItem5, Me.BarSubItemDevice, Me.BarSubItemMaster, Me.BarButtonItemComp, Me.BarButtonItemLocation, Me.BarButtonItemDept, Me.BarButtonItemShift, Me.BarButtonItemGrade, Me.BarButtonItemEmpGrp, Me.BarButtonItemBank, Me.BarButtonItemDisp, Me.BarButtonItemCat, Me.BarButtonItemEmp, Me.BarSubItemTrans, Me.BarButtonItemManual, Me.BarButtonItemDataMaint, Me.BarButtonItemHoliday, Me.BarSubItemLeaveMgmt, Me.BarButtonItemLMaster, Me.BarButtonItemlAcc, Me.BarButtonItemLApp, Me.BarSubItemAdmin, Me.BarButtonItemCSettings, Me.BarButtonItemDPro, Me.BarButtonItemDB, Me.BarButtonItemSMS, Me.BarButtonItemBSMS, Me.BarButtonItemEmail, Me.BarButtonItemUManage, Me.BarButtonItemBackUp, Me.BarButtonItemPDB, Me.BarButtonItemDMgmt, Me.BarButtonItem1, Me.BarSubItemReports, Me.BarButtonItemDReports, Me.BarButtonItemMReports, Me.BarButtonItemLReports, Me.BarButtonItemCreports, Me.BarSubItemPayRoll, Me.BarButtonItemPEmp, Me.BarButtonItemPSetup, Me.BarButtonItemFormula, Me.BarButtonItemPayPro, Me.BarButtonItemPerMain, Me.BarButtonItemLoan, Me.BarButtonItemPayReports, Me.BarButtonItemLeaveIncr, Me.BarSubItem2, Me.BarButtonItemHome, Me.BarButtonItem2, Me.BarButtonItem6, Me.BarSubItemCanteen, Me.BarButtonItemSlab, Me.BarButtonItemMealMenu, Me.BarButtonItemCanReports, Me.BarSubItemVisitor, Me.BarButtonItemVEntry, Me.BarButtonItemVHistory, Me.BarButtonOTComp, Me.BarButtonItemOTMaint, Me.BarMenuDevice, Me.BarButtoniAS, Me.BarButtonUltra, Me.BarMenuLog, Me.BarButtonLogiAS, Me.BarButtonLogUltra, Me.BarMenuTemplate, Me.BarButtonTempiAS, Me.BarButtonTempUltra, Me.BarMenuRealTime, Me.BarButtonRealiAS, Me.BarButtonRealUltra})
        Me.BarManager2.MaxItemId = 83
        '
        'Bar1
        '
        Me.Bar1.BarAppearance.Hovered.Font = New System.Drawing.Font("Tahoma", 17.0!)
        Me.Bar1.BarAppearance.Hovered.ForeColor = System.Drawing.Color.SteelBlue
        Me.Bar1.BarAppearance.Hovered.Options.UseFont = True
        Me.Bar1.BarAppearance.Hovered.Options.UseForeColor = True
        Me.Bar1.BarAppearance.Normal.Font = New System.Drawing.Font("Tahoma", 15.0!)
        Me.Bar1.BarAppearance.Normal.ForeColor = System.Drawing.Color.SteelBlue
        Me.Bar1.BarAppearance.Normal.Options.UseFont = True
        Me.Bar1.BarAppearance.Normal.Options.UseForeColor = True
        Me.Bar1.BarAppearance.Pressed.Font = New System.Drawing.Font("Tahoma", 15.0!)
        Me.Bar1.BarAppearance.Pressed.ForeColor = System.Drawing.Color.SteelBlue
        Me.Bar1.BarAppearance.Pressed.Options.UseFont = True
        Me.Bar1.BarAppearance.Pressed.Options.UseForeColor = True
        Me.Bar1.BarName = "Tools"
        Me.Bar1.DockCol = 0
        Me.Bar1.DockRow = 0
        Me.Bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.Bar1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemHome), New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItemDevice), New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItemMaster), New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItemTrans), New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItemLeaveMgmt), New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItemAdmin), New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItemReports), New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItemPayRoll), New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItemCanteen), New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItemVisitor)})
        Me.Bar1.OptionsBar.AllowQuickCustomization = False
        Me.Bar1.OptionsBar.DrawDragBorder = False
        Me.Bar1.OptionsBar.MultiLine = True
        Me.Bar1.OptionsBar.UseWholeRow = True
        Me.Bar1.Text = "Tools"
        '
        'BarButtonItemHome
        '
        Me.BarButtonItemHome.Caption = "Home"
        Me.BarButtonItemHome.Id = 59
        Me.BarButtonItemHome.ImageOptions.Image = CType(resources.GetObject("BarButtonItemHome.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItemHome.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItemHome.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItemHome.Name = "BarButtonItemHome"
        Me.BarButtonItemHome.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'BarSubItemDevice
        '
        Me.BarSubItemDevice.Caption = "Device"
        Me.BarSubItemDevice.Id = 6
        Me.BarSubItemDevice.ImageOptions.Image = CType(resources.GetObject("BarSubItemDevice.ImageOptions.Image"), System.Drawing.Image)
        Me.BarSubItemDevice.ImageOptions.LargeImage = CType(resources.GetObject("BarSubItemDevice.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarSubItemDevice.ItemAppearance.Hovered.Font = New System.Drawing.Font("Tahoma", 15.0!)
        Me.BarSubItemDevice.ItemAppearance.Hovered.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarSubItemDevice.ItemAppearance.Hovered.Options.UseFont = True
        Me.BarSubItemDevice.ItemAppearance.Hovered.Options.UseForeColor = True
        Me.BarSubItemDevice.ItemAppearance.Normal.Font = New System.Drawing.Font("Tahoma", 15.0!)
        Me.BarSubItemDevice.ItemAppearance.Normal.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarSubItemDevice.ItemAppearance.Normal.Options.UseFont = True
        Me.BarSubItemDevice.ItemAppearance.Normal.Options.UseForeColor = True
        Me.BarSubItemDevice.ItemAppearance.Pressed.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarSubItemDevice.ItemAppearance.Pressed.Options.UseForeColor = True
        Me.BarSubItemDevice.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarMenuDevice), New DevExpress.XtraBars.LinkPersistInfo(Me.BarMenuLog), New DevExpress.XtraBars.LinkPersistInfo(Me.BarMenuTemplate), New DevExpress.XtraBars.LinkPersistInfo(Me.BarMenuRealTime)})
        Me.BarSubItemDevice.MenuAppearance.AppearanceMenu.Hovered.Font = New System.Drawing.Font("Tahoma", 14.0!)
        Me.BarSubItemDevice.MenuAppearance.AppearanceMenu.Hovered.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarSubItemDevice.MenuAppearance.AppearanceMenu.Hovered.Options.UseFont = True
        Me.BarSubItemDevice.MenuAppearance.AppearanceMenu.Hovered.Options.UseForeColor = True
        Me.BarSubItemDevice.MenuAppearance.AppearanceMenu.Normal.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.BarSubItemDevice.MenuAppearance.AppearanceMenu.Normal.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarSubItemDevice.MenuAppearance.AppearanceMenu.Normal.Options.UseFont = True
        Me.BarSubItemDevice.MenuAppearance.AppearanceMenu.Normal.Options.UseForeColor = True
        Me.BarSubItemDevice.MenuAppearance.AppearanceMenu.Pressed.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarSubItemDevice.MenuAppearance.AppearanceMenu.Pressed.Options.UseForeColor = True
        Me.BarSubItemDevice.MenuAppearance.HeaderItemAppearance.Font = New System.Drawing.Font("Tahoma", 15.0!)
        Me.BarSubItemDevice.MenuAppearance.HeaderItemAppearance.Options.UseFont = True
        Me.BarSubItemDevice.MenuAppearance.MenuCaption.ForeColor = System.Drawing.Color.Blue
        Me.BarSubItemDevice.MenuAppearance.MenuCaption.Options.UseForeColor = True
        Me.BarSubItemDevice.Name = "BarSubItemDevice"
        Me.BarSubItemDevice.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'BarMenuDevice
        '
        Me.BarMenuDevice.Caption = "Device Management"
        Me.BarMenuDevice.Id = 71
        Me.BarMenuDevice.ImageOptions.Image = CType(resources.GetObject("BarMenuDevice.ImageOptions.Image"), System.Drawing.Image)
        Me.BarMenuDevice.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtoniAS), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonUltra)})
        Me.BarMenuDevice.MenuAppearance.AppearanceMenu.Hovered.Font = New System.Drawing.Font("Tahoma", 14.0!)
        Me.BarMenuDevice.MenuAppearance.AppearanceMenu.Hovered.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarMenuDevice.MenuAppearance.AppearanceMenu.Hovered.Options.UseFont = True
        Me.BarMenuDevice.MenuAppearance.AppearanceMenu.Hovered.Options.UseForeColor = True
        Me.BarMenuDevice.MenuAppearance.AppearanceMenu.Normal.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.BarMenuDevice.MenuAppearance.AppearanceMenu.Normal.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarMenuDevice.MenuAppearance.AppearanceMenu.Normal.Options.UseFont = True
        Me.BarMenuDevice.MenuAppearance.AppearanceMenu.Normal.Options.UseForeColor = True
        Me.BarMenuDevice.MenuAppearance.AppearanceMenu.Pressed.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarMenuDevice.MenuAppearance.AppearanceMenu.Pressed.Options.UseForeColor = True
        Me.BarMenuDevice.Name = "BarMenuDevice"
        '
        'BarButtoniAS
        '
        Me.BarButtoniAS.Caption = "iAS"
        Me.BarButtoniAS.Id = 72
        Me.BarButtoniAS.ImageOptions.Image = CType(resources.GetObject("BarButtoniAS.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtoniAS.Name = "BarButtoniAS"
        '
        'BarButtonUltra
        '
        Me.BarButtonUltra.Caption = "Ultra"
        Me.BarButtonUltra.Id = 73
        Me.BarButtonUltra.ImageOptions.Image = CType(resources.GetObject("BarButtonUltra.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonUltra.Name = "BarButtonUltra"
        '
        'BarMenuLog
        '
        Me.BarMenuLog.Caption = "Log Management"
        Me.BarMenuLog.Id = 74
        Me.BarMenuLog.ImageOptions.Image = CType(resources.GetObject("BarMenuLog.ImageOptions.Image"), System.Drawing.Image)
        Me.BarMenuLog.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonLogiAS), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonLogUltra)})
        Me.BarMenuLog.MenuAppearance.AppearanceMenu.Hovered.Font = New System.Drawing.Font("Tahoma", 14.0!)
        Me.BarMenuLog.MenuAppearance.AppearanceMenu.Hovered.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarMenuLog.MenuAppearance.AppearanceMenu.Hovered.Options.UseFont = True
        Me.BarMenuLog.MenuAppearance.AppearanceMenu.Hovered.Options.UseForeColor = True
        Me.BarMenuLog.MenuAppearance.AppearanceMenu.Normal.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.BarMenuLog.MenuAppearance.AppearanceMenu.Normal.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarMenuLog.MenuAppearance.AppearanceMenu.Normal.Options.UseFont = True
        Me.BarMenuLog.MenuAppearance.AppearanceMenu.Normal.Options.UseForeColor = True
        Me.BarMenuLog.MenuAppearance.AppearanceMenu.Pressed.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarMenuLog.MenuAppearance.AppearanceMenu.Pressed.Options.UseForeColor = True
        Me.BarMenuLog.Name = "BarMenuLog"
        '
        'BarButtonLogiAS
        '
        Me.BarButtonLogiAS.Caption = "iAS"
        Me.BarButtonLogiAS.Id = 75
        Me.BarButtonLogiAS.ImageOptions.Image = CType(resources.GetObject("BarButtonLogiAS.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonLogiAS.Name = "BarButtonLogiAS"
        '
        'BarButtonLogUltra
        '
        Me.BarButtonLogUltra.Caption = "Ultra"
        Me.BarButtonLogUltra.Id = 76
        Me.BarButtonLogUltra.ImageOptions.Image = CType(resources.GetObject("BarButtonLogUltra.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonLogUltra.Name = "BarButtonLogUltra"
        '
        'BarMenuTemplate
        '
        Me.BarMenuTemplate.Caption = "Template Management"
        Me.BarMenuTemplate.Id = 77
        Me.BarMenuTemplate.ImageOptions.Image = CType(resources.GetObject("BarMenuTemplate.ImageOptions.Image"), System.Drawing.Image)
        Me.BarMenuTemplate.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonTempiAS), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonTempUltra)})
        Me.BarMenuTemplate.MenuAppearance.AppearanceMenu.Hovered.Font = New System.Drawing.Font("Tahoma", 14.0!)
        Me.BarMenuTemplate.MenuAppearance.AppearanceMenu.Hovered.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarMenuTemplate.MenuAppearance.AppearanceMenu.Hovered.Options.UseFont = True
        Me.BarMenuTemplate.MenuAppearance.AppearanceMenu.Hovered.Options.UseForeColor = True
        Me.BarMenuTemplate.MenuAppearance.AppearanceMenu.Normal.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.BarMenuTemplate.MenuAppearance.AppearanceMenu.Normal.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarMenuTemplate.MenuAppearance.AppearanceMenu.Normal.Options.UseFont = True
        Me.BarMenuTemplate.MenuAppearance.AppearanceMenu.Normal.Options.UseForeColor = True
        Me.BarMenuTemplate.MenuAppearance.AppearanceMenu.Pressed.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarMenuTemplate.MenuAppearance.AppearanceMenu.Pressed.Options.UseForeColor = True
        Me.BarMenuTemplate.Name = "BarMenuTemplate"
        '
        'BarButtonTempiAS
        '
        Me.BarButtonTempiAS.Caption = "iAS"
        Me.BarButtonTempiAS.Id = 78
        Me.BarButtonTempiAS.ImageOptions.Image = CType(resources.GetObject("BarButtonTempiAS.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonTempiAS.Name = "BarButtonTempiAS"
        '
        'BarButtonTempUltra
        '
        Me.BarButtonTempUltra.Caption = "Ultra"
        Me.BarButtonTempUltra.Id = 79
        Me.BarButtonTempUltra.ImageOptions.Image = CType(resources.GetObject("BarButtonTempUltra.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonTempUltra.Name = "BarButtonTempUltra"
        '
        'BarMenuRealTime
        '
        Me.BarMenuRealTime.Caption = "Real Time Logs"
        Me.BarMenuRealTime.Id = 80
        Me.BarMenuRealTime.ImageOptions.Image = CType(resources.GetObject("BarMenuRealTime.ImageOptions.Image"), System.Drawing.Image)
        Me.BarMenuRealTime.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonRealiAS), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonRealUltra)})
        Me.BarMenuRealTime.MenuAppearance.AppearanceMenu.Hovered.Font = New System.Drawing.Font("Tahoma", 14.0!)
        Me.BarMenuRealTime.MenuAppearance.AppearanceMenu.Hovered.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarMenuRealTime.MenuAppearance.AppearanceMenu.Hovered.Options.UseFont = True
        Me.BarMenuRealTime.MenuAppearance.AppearanceMenu.Hovered.Options.UseForeColor = True
        Me.BarMenuRealTime.MenuAppearance.AppearanceMenu.Normal.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.BarMenuRealTime.MenuAppearance.AppearanceMenu.Normal.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarMenuRealTime.MenuAppearance.AppearanceMenu.Normal.Options.UseFont = True
        Me.BarMenuRealTime.MenuAppearance.AppearanceMenu.Normal.Options.UseForeColor = True
        Me.BarMenuRealTime.MenuAppearance.AppearanceMenu.Pressed.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarMenuRealTime.MenuAppearance.AppearanceMenu.Pressed.Options.UseForeColor = True
        Me.BarMenuRealTime.Name = "BarMenuRealTime"
        '
        'BarButtonRealiAS
        '
        Me.BarButtonRealiAS.Caption = "iAS"
        Me.BarButtonRealiAS.Id = 81
        Me.BarButtonRealiAS.ImageOptions.Image = CType(resources.GetObject("BarButtonRealiAS.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonRealiAS.Name = "BarButtonRealiAS"
        '
        'BarButtonRealUltra
        '
        Me.BarButtonRealUltra.Caption = "Ultra"
        Me.BarButtonRealUltra.Id = 82
        Me.BarButtonRealUltra.ImageOptions.Image = CType(resources.GetObject("BarButtonRealUltra.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonRealUltra.Name = "BarButtonRealUltra"
        '
        'BarSubItemMaster
        '
        Me.BarSubItemMaster.Caption = "Master"
        Me.BarSubItemMaster.Id = 10
        Me.BarSubItemMaster.ImageOptions.Image = CType(resources.GetObject("BarSubItemMaster.ImageOptions.Image"), System.Drawing.Image)
        Me.BarSubItemMaster.ImageOptions.LargeImage = CType(resources.GetObject("BarSubItemMaster.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarSubItemMaster.ItemAppearance.Hovered.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarSubItemMaster.ItemAppearance.Normal.Font = New System.Drawing.Font("Tahoma", 15.0!)
        Me.BarSubItemMaster.ItemAppearance.Normal.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarSubItemMaster.ItemAppearance.Pressed.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarSubItemMaster.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemComp), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.BarButtonItemLocation, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemDept), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemShift), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemGrade), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemEmpGrp), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemBank), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemDisp), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemCat), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemEmp)})
        Me.BarSubItemMaster.MenuAppearance.AppearanceMenu.Hovered.Font = New System.Drawing.Font("Tahoma", 14.0!)
        Me.BarSubItemMaster.MenuAppearance.AppearanceMenu.Hovered.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarSubItemMaster.MenuAppearance.AppearanceMenu.Hovered.Options.UseFont = True
        Me.BarSubItemMaster.MenuAppearance.AppearanceMenu.Hovered.Options.UseForeColor = True
        Me.BarSubItemMaster.MenuAppearance.AppearanceMenu.Normal.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.BarSubItemMaster.MenuAppearance.AppearanceMenu.Normal.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarSubItemMaster.MenuAppearance.AppearanceMenu.Normal.Options.UseFont = True
        Me.BarSubItemMaster.MenuAppearance.AppearanceMenu.Normal.Options.UseForeColor = True
        Me.BarSubItemMaster.MenuAppearance.AppearanceMenu.Pressed.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarSubItemMaster.MenuAppearance.AppearanceMenu.Pressed.Options.UseForeColor = True
        Me.BarSubItemMaster.Name = "BarSubItemMaster"
        Me.BarSubItemMaster.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'BarButtonItemComp
        '
        Me.BarButtonItemComp.Caption = "Company"
        Me.BarButtonItemComp.Id = 11
        Me.BarButtonItemComp.ImageOptions.Image = CType(resources.GetObject("BarButtonItemComp.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItemComp.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItemComp.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItemComp.ItemInMenuAppearance.Hovered.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarButtonItemComp.ItemInMenuAppearance.Hovered.Options.UseForeColor = True
        Me.BarButtonItemComp.ItemInMenuAppearance.Normal.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarButtonItemComp.ItemInMenuAppearance.Normal.Options.UseForeColor = True
        Me.BarButtonItemComp.ItemInMenuAppearance.Pressed.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarButtonItemComp.ItemInMenuAppearance.Pressed.Options.UseForeColor = True
        Me.BarButtonItemComp.Name = "BarButtonItemComp"
        Me.BarButtonItemComp.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'BarButtonItemLocation
        '
        Me.BarButtonItemLocation.Caption = "Location"
        Me.BarButtonItemLocation.Id = 12
        Me.BarButtonItemLocation.ImageOptions.Image = CType(resources.GetObject("BarButtonItemLocation.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItemLocation.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItemLocation.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItemLocation.ItemInMenuAppearance.Hovered.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarButtonItemLocation.ItemInMenuAppearance.Hovered.Options.UseForeColor = True
        Me.BarButtonItemLocation.ItemInMenuAppearance.Normal.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarButtonItemLocation.ItemInMenuAppearance.Normal.Options.UseForeColor = True
        Me.BarButtonItemLocation.ItemInMenuAppearance.Pressed.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarButtonItemLocation.ItemInMenuAppearance.Pressed.Options.UseForeColor = True
        Me.BarButtonItemLocation.Name = "BarButtonItemLocation"
        '
        'BarButtonItemDept
        '
        Me.BarButtonItemDept.Caption = "Department"
        Me.BarButtonItemDept.Id = 13
        Me.BarButtonItemDept.ImageOptions.Image = CType(resources.GetObject("BarButtonItemDept.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItemDept.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItemDept.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItemDept.ItemInMenuAppearance.Hovered.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarButtonItemDept.ItemInMenuAppearance.Hovered.Options.UseForeColor = True
        Me.BarButtonItemDept.ItemInMenuAppearance.Normal.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarButtonItemDept.ItemInMenuAppearance.Normal.Options.UseForeColor = True
        Me.BarButtonItemDept.ItemInMenuAppearance.Pressed.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarButtonItemDept.ItemInMenuAppearance.Pressed.Options.UseForeColor = True
        Me.BarButtonItemDept.Name = "BarButtonItemDept"
        '
        'BarButtonItemShift
        '
        Me.BarButtonItemShift.Caption = "Shift"
        Me.BarButtonItemShift.Id = 14
        Me.BarButtonItemShift.ImageOptions.Image = CType(resources.GetObject("BarButtonItemShift.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItemShift.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItemShift.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItemShift.ItemInMenuAppearance.Hovered.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarButtonItemShift.ItemInMenuAppearance.Hovered.Options.UseForeColor = True
        Me.BarButtonItemShift.ItemInMenuAppearance.Normal.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarButtonItemShift.ItemInMenuAppearance.Normal.Options.UseForeColor = True
        Me.BarButtonItemShift.ItemInMenuAppearance.Pressed.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarButtonItemShift.ItemInMenuAppearance.Pressed.Options.UseForeColor = True
        Me.BarButtonItemShift.Name = "BarButtonItemShift"
        '
        'BarButtonItemGrade
        '
        Me.BarButtonItemGrade.Caption = "Grade"
        Me.BarButtonItemGrade.Id = 15
        Me.BarButtonItemGrade.ImageOptions.Image = CType(resources.GetObject("BarButtonItemGrade.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItemGrade.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItemGrade.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItemGrade.ItemInMenuAppearance.Hovered.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarButtonItemGrade.ItemInMenuAppearance.Hovered.Options.UseForeColor = True
        Me.BarButtonItemGrade.ItemInMenuAppearance.Normal.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarButtonItemGrade.ItemInMenuAppearance.Normal.Options.UseForeColor = True
        Me.BarButtonItemGrade.ItemInMenuAppearance.Pressed.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarButtonItemGrade.ItemInMenuAppearance.Pressed.Options.UseForeColor = True
        Me.BarButtonItemGrade.Name = "BarButtonItemGrade"
        '
        'BarButtonItemEmpGrp
        '
        Me.BarButtonItemEmpGrp.Caption = "Employee Group"
        Me.BarButtonItemEmpGrp.Id = 16
        Me.BarButtonItemEmpGrp.ImageOptions.Image = CType(resources.GetObject("BarButtonItemEmpGrp.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItemEmpGrp.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItemEmpGrp.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItemEmpGrp.ItemInMenuAppearance.Hovered.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarButtonItemEmpGrp.ItemInMenuAppearance.Hovered.Options.UseForeColor = True
        Me.BarButtonItemEmpGrp.ItemInMenuAppearance.Normal.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarButtonItemEmpGrp.ItemInMenuAppearance.Normal.Options.UseForeColor = True
        Me.BarButtonItemEmpGrp.ItemInMenuAppearance.Pressed.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarButtonItemEmpGrp.ItemInMenuAppearance.Pressed.Options.UseForeColor = True
        Me.BarButtonItemEmpGrp.Name = "BarButtonItemEmpGrp"
        '
        'BarButtonItemBank
        '
        Me.BarButtonItemBank.Caption = "Bank"
        Me.BarButtonItemBank.Id = 17
        Me.BarButtonItemBank.ImageOptions.Image = CType(resources.GetObject("BarButtonItemBank.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItemBank.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItemBank.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItemBank.ItemInMenuAppearance.Hovered.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarButtonItemBank.ItemInMenuAppearance.Hovered.Options.UseForeColor = True
        Me.BarButtonItemBank.ItemInMenuAppearance.Normal.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarButtonItemBank.ItemInMenuAppearance.Normal.Options.UseForeColor = True
        Me.BarButtonItemBank.ItemInMenuAppearance.Pressed.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarButtonItemBank.ItemInMenuAppearance.Pressed.Options.UseForeColor = True
        Me.BarButtonItemBank.Name = "BarButtonItemBank"
        '
        'BarButtonItemDisp
        '
        Me.BarButtonItemDisp.Caption = "Dispensary"
        Me.BarButtonItemDisp.Id = 18
        Me.BarButtonItemDisp.ImageOptions.Image = CType(resources.GetObject("BarButtonItemDisp.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItemDisp.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItemDisp.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItemDisp.ItemInMenuAppearance.Hovered.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarButtonItemDisp.ItemInMenuAppearance.Hovered.Options.UseForeColor = True
        Me.BarButtonItemDisp.ItemInMenuAppearance.Normal.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarButtonItemDisp.ItemInMenuAppearance.Normal.Options.UseForeColor = True
        Me.BarButtonItemDisp.ItemInMenuAppearance.Pressed.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarButtonItemDisp.ItemInMenuAppearance.Pressed.Options.UseForeColor = True
        Me.BarButtonItemDisp.Name = "BarButtonItemDisp"
        '
        'BarButtonItemCat
        '
        Me.BarButtonItemCat.Caption = "Category"
        Me.BarButtonItemCat.Id = 19
        Me.BarButtonItemCat.ImageOptions.Image = CType(resources.GetObject("BarButtonItemCat.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItemCat.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItemCat.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItemCat.ItemInMenuAppearance.Hovered.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarButtonItemCat.ItemInMenuAppearance.Hovered.Options.UseForeColor = True
        Me.BarButtonItemCat.ItemInMenuAppearance.Normal.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarButtonItemCat.ItemInMenuAppearance.Normal.Options.UseForeColor = True
        Me.BarButtonItemCat.ItemInMenuAppearance.Pressed.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarButtonItemCat.ItemInMenuAppearance.Pressed.Options.UseForeColor = True
        Me.BarButtonItemCat.Name = "BarButtonItemCat"
        '
        'BarButtonItemEmp
        '
        Me.BarButtonItemEmp.Caption = "Employee"
        Me.BarButtonItemEmp.Id = 20
        Me.BarButtonItemEmp.ImageOptions.Image = CType(resources.GetObject("BarButtonItemEmp.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItemEmp.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItemEmp.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItemEmp.ItemInMenuAppearance.Hovered.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarButtonItemEmp.ItemInMenuAppearance.Hovered.Options.UseForeColor = True
        Me.BarButtonItemEmp.ItemInMenuAppearance.Normal.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarButtonItemEmp.ItemInMenuAppearance.Normal.Options.UseForeColor = True
        Me.BarButtonItemEmp.ItemInMenuAppearance.Pressed.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarButtonItemEmp.ItemInMenuAppearance.Pressed.Options.UseForeColor = True
        Me.BarButtonItemEmp.Name = "BarButtonItemEmp"
        '
        'BarSubItemTrans
        '
        Me.BarSubItemTrans.Caption = "Transaction"
        Me.BarSubItemTrans.Id = 21
        Me.BarSubItemTrans.ImageOptions.Image = CType(resources.GetObject("BarSubItemTrans.ImageOptions.Image"), System.Drawing.Image)
        Me.BarSubItemTrans.ImageOptions.LargeImage = CType(resources.GetObject("BarSubItemTrans.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarSubItemTrans.ItemAppearance.Hovered.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarSubItemTrans.ItemAppearance.Normal.Font = New System.Drawing.Font("Tahoma", 15.0!)
        Me.BarSubItemTrans.ItemAppearance.Normal.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarSubItemTrans.ItemAppearance.Pressed.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarSubItemTrans.ItemInMenuAppearance.Normal.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarSubItemTrans.ItemInMenuAppearance.Normal.Options.UseForeColor = True
        Me.BarSubItemTrans.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, Me.BarButtonItemManual, DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemDataMaint), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemHoliday), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemOTMaint)})
        Me.BarSubItemTrans.MenuAppearance.AppearanceMenu.Hovered.Font = New System.Drawing.Font("Tahoma", 14.0!)
        Me.BarSubItemTrans.MenuAppearance.AppearanceMenu.Hovered.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarSubItemTrans.MenuAppearance.AppearanceMenu.Hovered.Options.UseFont = True
        Me.BarSubItemTrans.MenuAppearance.AppearanceMenu.Hovered.Options.UseForeColor = True
        Me.BarSubItemTrans.MenuAppearance.AppearanceMenu.Normal.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.BarSubItemTrans.MenuAppearance.AppearanceMenu.Normal.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarSubItemTrans.MenuAppearance.AppearanceMenu.Normal.Options.UseFont = True
        Me.BarSubItemTrans.MenuAppearance.AppearanceMenu.Normal.Options.UseForeColor = True
        Me.BarSubItemTrans.MenuAppearance.AppearanceMenu.Pressed.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarSubItemTrans.MenuAppearance.AppearanceMenu.Pressed.Options.UseForeColor = True
        Me.BarSubItemTrans.Name = "BarSubItemTrans"
        Me.BarSubItemTrans.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'BarButtonItemManual
        '
        Me.BarButtonItemManual.Caption = "Manual Entry"
        Me.BarButtonItemManual.Id = 22
        Me.BarButtonItemManual.ImageOptions.Image = CType(resources.GetObject("BarButtonItemManual.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItemManual.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItemManual.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItemManual.Name = "BarButtonItemManual"
        '
        'BarButtonItemDataMaint
        '
        Me.BarButtonItemDataMaint.Caption = "Data Maintenance"
        Me.BarButtonItemDataMaint.Id = 23
        Me.BarButtonItemDataMaint.ImageOptions.Image = CType(resources.GetObject("BarButtonItemDataMaint.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItemDataMaint.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItemDataMaint.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItemDataMaint.Name = "BarButtonItemDataMaint"
        '
        'BarButtonItemHoliday
        '
        Me.BarButtonItemHoliday.Caption = "Holiday Entry"
        Me.BarButtonItemHoliday.Id = 24
        Me.BarButtonItemHoliday.ImageOptions.Image = CType(resources.GetObject("BarButtonItemHoliday.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItemHoliday.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItemHoliday.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItemHoliday.Name = "BarButtonItemHoliday"
        '
        'BarButtonItemOTMaint
        '
        Me.BarButtonItemOTMaint.Caption = "OT Maintenance"
        Me.BarButtonItemOTMaint.Id = 70
        Me.BarButtonItemOTMaint.ImageOptions.Image = CType(resources.GetObject("BarButtonItemOTMaint.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItemOTMaint.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItemOTMaint.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItemOTMaint.Name = "BarButtonItemOTMaint"
        '
        'BarSubItemLeaveMgmt
        '
        Me.BarSubItemLeaveMgmt.Caption = "Leave Management"
        Me.BarSubItemLeaveMgmt.Id = 25
        Me.BarSubItemLeaveMgmt.ImageOptions.Image = CType(resources.GetObject("BarSubItemLeaveMgmt.ImageOptions.Image"), System.Drawing.Image)
        Me.BarSubItemLeaveMgmt.ImageOptions.LargeImage = CType(resources.GetObject("BarSubItemLeaveMgmt.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarSubItemLeaveMgmt.ItemAppearance.Hovered.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarSubItemLeaveMgmt.ItemAppearance.Normal.Font = New System.Drawing.Font("Tahoma", 15.0!)
        Me.BarSubItemLeaveMgmt.ItemAppearance.Normal.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarSubItemLeaveMgmt.ItemAppearance.Pressed.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarSubItemLeaveMgmt.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemLMaster), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemlAcc), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemLApp), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemLeaveIncr)})
        Me.BarSubItemLeaveMgmt.MenuAppearance.AppearanceMenu.Hovered.Font = New System.Drawing.Font("Tahoma", 14.0!)
        Me.BarSubItemLeaveMgmt.MenuAppearance.AppearanceMenu.Hovered.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarSubItemLeaveMgmt.MenuAppearance.AppearanceMenu.Hovered.Options.UseFont = True
        Me.BarSubItemLeaveMgmt.MenuAppearance.AppearanceMenu.Hovered.Options.UseForeColor = True
        Me.BarSubItemLeaveMgmt.MenuAppearance.AppearanceMenu.Normal.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.BarSubItemLeaveMgmt.MenuAppearance.AppearanceMenu.Normal.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarSubItemLeaveMgmt.MenuAppearance.AppearanceMenu.Normal.Options.UseFont = True
        Me.BarSubItemLeaveMgmt.MenuAppearance.AppearanceMenu.Normal.Options.UseForeColor = True
        Me.BarSubItemLeaveMgmt.MenuAppearance.AppearanceMenu.Pressed.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarSubItemLeaveMgmt.MenuAppearance.AppearanceMenu.Pressed.Options.UseForeColor = True
        Me.BarSubItemLeaveMgmt.Name = "BarSubItemLeaveMgmt"
        Me.BarSubItemLeaveMgmt.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'BarButtonItemLMaster
        '
        Me.BarButtonItemLMaster.Caption = "Leave Master"
        Me.BarButtonItemLMaster.Id = 26
        Me.BarButtonItemLMaster.ImageOptions.Image = CType(resources.GetObject("BarButtonItemLMaster.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItemLMaster.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItemLMaster.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItemLMaster.Name = "BarButtonItemLMaster"
        '
        'BarButtonItemlAcc
        '
        Me.BarButtonItemlAcc.Caption = "Leave Accrual"
        Me.BarButtonItemlAcc.Id = 27
        Me.BarButtonItemlAcc.ImageOptions.Image = CType(resources.GetObject("BarButtonItemlAcc.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItemlAcc.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItemlAcc.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItemlAcc.Name = "BarButtonItemlAcc"
        '
        'BarButtonItemLApp
        '
        Me.BarButtonItemLApp.Caption = "Leave Application"
        Me.BarButtonItemLApp.Id = 28
        Me.BarButtonItemLApp.ImageOptions.Image = CType(resources.GetObject("BarButtonItemLApp.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItemLApp.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItemLApp.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItemLApp.Name = "BarButtonItemLApp"
        '
        'BarButtonItemLeaveIncr
        '
        Me.BarButtonItemLeaveIncr.Caption = "Leave Increment"
        Me.BarButtonItemLeaveIncr.Id = 54
        Me.BarButtonItemLeaveIncr.ImageOptions.Image = CType(resources.GetObject("BarButtonItemLeaveIncr.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItemLeaveIncr.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItemLeaveIncr.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItemLeaveIncr.Name = "BarButtonItemLeaveIncr"
        '
        'BarSubItemAdmin
        '
        Me.BarSubItemAdmin.Caption = "Admin"
        Me.BarSubItemAdmin.Id = 29
        Me.BarSubItemAdmin.ImageOptions.Image = CType(resources.GetObject("BarSubItemAdmin.ImageOptions.Image"), System.Drawing.Image)
        Me.BarSubItemAdmin.ImageOptions.LargeImage = CType(resources.GetObject("BarSubItemAdmin.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarSubItemAdmin.ItemAppearance.Hovered.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarSubItemAdmin.ItemAppearance.Normal.Font = New System.Drawing.Font("Tahoma", 15.0!)
        Me.BarSubItemAdmin.ItemAppearance.Normal.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarSubItemAdmin.ItemAppearance.Pressed.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarSubItemAdmin.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemCSettings), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemDPro), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemDB), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemSMS), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemBSMS), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemEmail), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemUManage), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemBackUp), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemPDB), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem2), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem6), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonOTComp)})
        Me.BarSubItemAdmin.MenuAppearance.AppearanceMenu.Hovered.Font = New System.Drawing.Font("Tahoma", 14.0!)
        Me.BarSubItemAdmin.MenuAppearance.AppearanceMenu.Hovered.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarSubItemAdmin.MenuAppearance.AppearanceMenu.Hovered.Options.UseFont = True
        Me.BarSubItemAdmin.MenuAppearance.AppearanceMenu.Hovered.Options.UseForeColor = True
        Me.BarSubItemAdmin.MenuAppearance.AppearanceMenu.Normal.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.BarSubItemAdmin.MenuAppearance.AppearanceMenu.Normal.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarSubItemAdmin.MenuAppearance.AppearanceMenu.Normal.Options.UseFont = True
        Me.BarSubItemAdmin.MenuAppearance.AppearanceMenu.Normal.Options.UseForeColor = True
        Me.BarSubItemAdmin.MenuAppearance.AppearanceMenu.Pressed.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarSubItemAdmin.MenuAppearance.AppearanceMenu.Pressed.Options.UseForeColor = True
        Me.BarSubItemAdmin.Name = "BarSubItemAdmin"
        Me.BarSubItemAdmin.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'BarButtonItemCSettings
        '
        Me.BarButtonItemCSettings.Caption = "Common Settings"
        Me.BarButtonItemCSettings.Id = 30
        Me.BarButtonItemCSettings.ImageOptions.Image = CType(resources.GetObject("BarButtonItemCSettings.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItemCSettings.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItemCSettings.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItemCSettings.Name = "BarButtonItemCSettings"
        '
        'BarButtonItemDPro
        '
        Me.BarButtonItemDPro.Caption = "Data Process"
        Me.BarButtonItemDPro.Id = 31
        Me.BarButtonItemDPro.ImageOptions.Image = CType(resources.GetObject("BarButtonItemDPro.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItemDPro.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItemDPro.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItemDPro.Name = "BarButtonItemDPro"
        '
        'BarButtonItemDB
        '
        Me.BarButtonItemDB.Caption = "Database Setting"
        Me.BarButtonItemDB.Id = 32
        Me.BarButtonItemDB.ImageOptions.Image = CType(resources.GetObject("BarButtonItemDB.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItemDB.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItemDB.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItemDB.Name = "BarButtonItemDB"
        '
        'BarButtonItemSMS
        '
        Me.BarButtonItemSMS.Caption = "SMS Setting"
        Me.BarButtonItemSMS.Id = 33
        Me.BarButtonItemSMS.ImageOptions.Image = CType(resources.GetObject("BarButtonItemSMS.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItemSMS.Name = "BarButtonItemSMS"
        '
        'BarButtonItemBSMS
        '
        Me.BarButtonItemBSMS.Caption = "Bulk SMS"
        Me.BarButtonItemBSMS.Id = 34
        Me.BarButtonItemBSMS.ImageOptions.Image = CType(resources.GetObject("BarButtonItemBSMS.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItemBSMS.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItemBSMS.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItemBSMS.Name = "BarButtonItemBSMS"
        '
        'BarButtonItemEmail
        '
        Me.BarButtonItemEmail.Caption = "Email Setting"
        Me.BarButtonItemEmail.Id = 35
        Me.BarButtonItemEmail.ImageOptions.Image = CType(resources.GetObject("BarButtonItemEmail.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItemEmail.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItemEmail.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItemEmail.Name = "BarButtonItemEmail"
        '
        'BarButtonItemUManage
        '
        Me.BarButtonItemUManage.Caption = "User Manage"
        Me.BarButtonItemUManage.Id = 36
        Me.BarButtonItemUManage.ImageOptions.Image = CType(resources.GetObject("BarButtonItemUManage.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItemUManage.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItemUManage.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItemUManage.Name = "BarButtonItemUManage"
        '
        'BarButtonItemBackUp
        '
        Me.BarButtonItemBackUp.Caption = "BackUp Setting"
        Me.BarButtonItemBackUp.Id = 37
        Me.BarButtonItemBackUp.ImageOptions.Image = CType(resources.GetObject("BarButtonItemBackUp.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItemBackUp.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItemBackUp.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItemBackUp.Name = "BarButtonItemBackUp"
        '
        'BarButtonItemPDB
        '
        Me.BarButtonItemPDB.Caption = "Parallel Database"
        Me.BarButtonItemPDB.Id = 38
        Me.BarButtonItemPDB.ImageOptions.Image = CType(resources.GetObject("BarButtonItemPDB.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItemPDB.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItemPDB.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItemPDB.Name = "BarButtonItemPDB"
        '
        'BarButtonItem2
        '
        Me.BarButtonItem2.Caption = "License Details"
        Me.BarButtonItem2.Id = 60
        Me.BarButtonItem2.ImageOptions.Image = CType(resources.GetObject("BarButtonItem2.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem2.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItem2.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItem2.Name = "BarButtonItem2"
        '
        'BarButtonItem6
        '
        Me.BarButtonItem6.Caption = "iAS Information"
        Me.BarButtonItem6.Id = 61
        Me.BarButtonItem6.ImageOptions.Image = CType(resources.GetObject("BarButtonItem6.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem6.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItem6.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItem6.Name = "BarButtonItem6"
        '
        'BarButtonOTComp
        '
        Me.BarButtonOTComp.Caption = "OT Setup"
        Me.BarButtonOTComp.Id = 69
        Me.BarButtonOTComp.ImageOptions.Image = CType(resources.GetObject("BarButtonOTComp.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonOTComp.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonOTComp.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonOTComp.Name = "BarButtonOTComp"
        '
        'BarSubItemReports
        '
        Me.BarSubItemReports.Caption = "Reports"
        Me.BarSubItemReports.Id = 41
        Me.BarSubItemReports.ImageOptions.Image = CType(resources.GetObject("BarSubItemReports.ImageOptions.Image"), System.Drawing.Image)
        Me.BarSubItemReports.ImageOptions.LargeImage = CType(resources.GetObject("BarSubItemReports.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarSubItemReports.ItemAppearance.Hovered.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarSubItemReports.ItemAppearance.Normal.Font = New System.Drawing.Font("Tahoma", 15.0!)
        Me.BarSubItemReports.ItemAppearance.Normal.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarSubItemReports.ItemAppearance.Pressed.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarSubItemReports.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemDReports), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemMReports), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemLReports), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemCreports)})
        Me.BarSubItemReports.MenuAppearance.AppearanceMenu.Hovered.Font = New System.Drawing.Font("Tahoma", 14.0!)
        Me.BarSubItemReports.MenuAppearance.AppearanceMenu.Hovered.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarSubItemReports.MenuAppearance.AppearanceMenu.Hovered.Options.UseFont = True
        Me.BarSubItemReports.MenuAppearance.AppearanceMenu.Hovered.Options.UseForeColor = True
        Me.BarSubItemReports.MenuAppearance.AppearanceMenu.Normal.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.BarSubItemReports.MenuAppearance.AppearanceMenu.Normal.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarSubItemReports.MenuAppearance.AppearanceMenu.Normal.Options.UseFont = True
        Me.BarSubItemReports.MenuAppearance.AppearanceMenu.Normal.Options.UseForeColor = True
        Me.BarSubItemReports.MenuAppearance.AppearanceMenu.Pressed.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarSubItemReports.MenuAppearance.AppearanceMenu.Pressed.Options.UseForeColor = True
        Me.BarSubItemReports.Name = "BarSubItemReports"
        Me.BarSubItemReports.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'BarButtonItemDReports
        '
        Me.BarButtonItemDReports.Caption = "Daily Reports"
        Me.BarButtonItemDReports.Id = 42
        Me.BarButtonItemDReports.ImageOptions.Image = CType(resources.GetObject("BarButtonItemDReports.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItemDReports.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItemDReports.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItemDReports.Name = "BarButtonItemDReports"
        '
        'BarButtonItemMReports
        '
        Me.BarButtonItemMReports.Caption = "Monthly Reports"
        Me.BarButtonItemMReports.Id = 43
        Me.BarButtonItemMReports.ImageOptions.Image = CType(resources.GetObject("BarButtonItemMReports.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItemMReports.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItemMReports.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItemMReports.Name = "BarButtonItemMReports"
        '
        'BarButtonItemLReports
        '
        Me.BarButtonItemLReports.Caption = "Leave Reports"
        Me.BarButtonItemLReports.Id = 44
        Me.BarButtonItemLReports.ImageOptions.Image = CType(resources.GetObject("BarButtonItemLReports.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItemLReports.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItemLReports.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItemLReports.Name = "BarButtonItemLReports"
        '
        'BarButtonItemCreports
        '
        Me.BarButtonItemCreports.Caption = "Customised Report"
        Me.BarButtonItemCreports.Id = 45
        Me.BarButtonItemCreports.ImageOptions.Image = CType(resources.GetObject("BarButtonItemCreports.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItemCreports.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItemCreports.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItemCreports.Name = "BarButtonItemCreports"
        '
        'BarSubItemPayRoll
        '
        Me.BarSubItemPayRoll.Caption = "Payroll"
        Me.BarSubItemPayRoll.Id = 46
        Me.BarSubItemPayRoll.ImageOptions.Image = CType(resources.GetObject("BarSubItemPayRoll.ImageOptions.Image"), System.Drawing.Image)
        Me.BarSubItemPayRoll.ImageOptions.LargeImage = CType(resources.GetObject("BarSubItemPayRoll.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarSubItemPayRoll.ItemInMenuAppearance.Hovered.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.BarSubItemPayRoll.ItemInMenuAppearance.Hovered.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarSubItemPayRoll.ItemInMenuAppearance.Hovered.Options.UseFont = True
        Me.BarSubItemPayRoll.ItemInMenuAppearance.Hovered.Options.UseForeColor = True
        Me.BarSubItemPayRoll.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemPEmp), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemPSetup), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemFormula), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemPayPro), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemPerMain), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemLoan), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemPayReports)})
        Me.BarSubItemPayRoll.MenuAppearance.AppearanceMenu.Hovered.Font = New System.Drawing.Font("Tahoma", 14.0!)
        Me.BarSubItemPayRoll.MenuAppearance.AppearanceMenu.Hovered.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarSubItemPayRoll.MenuAppearance.AppearanceMenu.Hovered.Options.UseFont = True
        Me.BarSubItemPayRoll.MenuAppearance.AppearanceMenu.Hovered.Options.UseForeColor = True
        Me.BarSubItemPayRoll.MenuAppearance.AppearanceMenu.Normal.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.BarSubItemPayRoll.MenuAppearance.AppearanceMenu.Normal.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarSubItemPayRoll.MenuAppearance.AppearanceMenu.Normal.Options.UseFont = True
        Me.BarSubItemPayRoll.MenuAppearance.AppearanceMenu.Normal.Options.UseForeColor = True
        Me.BarSubItemPayRoll.MenuAppearance.AppearanceMenu.Pressed.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarSubItemPayRoll.MenuAppearance.AppearanceMenu.Pressed.Options.UseForeColor = True
        Me.BarSubItemPayRoll.Name = "BarSubItemPayRoll"
        Me.BarSubItemPayRoll.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'BarButtonItemPEmp
        '
        Me.BarButtonItemPEmp.Caption = "Employee Setup"
        Me.BarButtonItemPEmp.Id = 47
        Me.BarButtonItemPEmp.ImageOptions.Image = CType(resources.GetObject("BarButtonItemPEmp.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItemPEmp.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItemPEmp.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItemPEmp.Name = "BarButtonItemPEmp"
        '
        'BarButtonItemPSetup
        '
        Me.BarButtonItemPSetup.Caption = "Payroll Setup"
        Me.BarButtonItemPSetup.Id = 48
        Me.BarButtonItemPSetup.ImageOptions.Image = CType(resources.GetObject("BarButtonItemPSetup.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItemPSetup.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItemPSetup.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItemPSetup.Name = "BarButtonItemPSetup"
        '
        'BarButtonItemFormula
        '
        Me.BarButtonItemFormula.Caption = "Formula Setup"
        Me.BarButtonItemFormula.Id = 49
        Me.BarButtonItemFormula.ImageOptions.Image = CType(resources.GetObject("BarButtonItemFormula.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItemFormula.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItemFormula.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItemFormula.Name = "BarButtonItemFormula"
        '
        'BarButtonItemPayPro
        '
        Me.BarButtonItemPayPro.Caption = "Payroll Processing"
        Me.BarButtonItemPayPro.Id = 50
        Me.BarButtonItemPayPro.ImageOptions.Image = CType(resources.GetObject("BarButtonItemPayPro.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItemPayPro.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItemPayPro.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItemPayPro.Name = "BarButtonItemPayPro"
        '
        'BarButtonItemPerMain
        '
        Me.BarButtonItemPerMain.Caption = "Performance Maintain"
        Me.BarButtonItemPerMain.Id = 51
        Me.BarButtonItemPerMain.ImageOptions.Image = CType(resources.GetObject("BarButtonItemPerMain.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItemPerMain.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItemPerMain.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItemPerMain.Name = "BarButtonItemPerMain"
        '
        'BarButtonItemLoan
        '
        Me.BarButtonItemLoan.Caption = "Loan/Advance Setup"
        Me.BarButtonItemLoan.Id = 52
        Me.BarButtonItemLoan.ImageOptions.Image = CType(resources.GetObject("BarButtonItemLoan.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItemLoan.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItemLoan.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItemLoan.Name = "BarButtonItemLoan"
        '
        'BarButtonItemPayReports
        '
        Me.BarButtonItemPayReports.Caption = "Reports"
        Me.BarButtonItemPayReports.Id = 53
        Me.BarButtonItemPayReports.ImageOptions.Image = CType(resources.GetObject("BarButtonItemPayReports.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItemPayReports.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItemPayReports.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItemPayReports.Name = "BarButtonItemPayReports"
        '
        'BarSubItemCanteen
        '
        Me.BarSubItemCanteen.Caption = "Canteen"
        Me.BarSubItemCanteen.Id = 62
        Me.BarSubItemCanteen.ImageOptions.Image = CType(resources.GetObject("BarSubItemCanteen.ImageOptions.Image"), System.Drawing.Image)
        Me.BarSubItemCanteen.ImageOptions.LargeImage = CType(resources.GetObject("BarSubItemCanteen.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarSubItemCanteen.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemSlab), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemMealMenu), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemCanReports)})
        Me.BarSubItemCanteen.MenuAppearance.AppearanceMenu.Hovered.Font = New System.Drawing.Font("Tahoma", 14.0!)
        Me.BarSubItemCanteen.MenuAppearance.AppearanceMenu.Hovered.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarSubItemCanteen.MenuAppearance.AppearanceMenu.Hovered.Options.UseFont = True
        Me.BarSubItemCanteen.MenuAppearance.AppearanceMenu.Hovered.Options.UseForeColor = True
        Me.BarSubItemCanteen.MenuAppearance.AppearanceMenu.Normal.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.BarSubItemCanteen.MenuAppearance.AppearanceMenu.Normal.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarSubItemCanteen.MenuAppearance.AppearanceMenu.Normal.Options.UseFont = True
        Me.BarSubItemCanteen.MenuAppearance.AppearanceMenu.Normal.Options.UseForeColor = True
        Me.BarSubItemCanteen.MenuAppearance.AppearanceMenu.Pressed.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.BarSubItemCanteen.MenuAppearance.AppearanceMenu.Pressed.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarSubItemCanteen.MenuAppearance.AppearanceMenu.Pressed.Options.UseFont = True
        Me.BarSubItemCanteen.MenuAppearance.AppearanceMenu.Pressed.Options.UseForeColor = True
        Me.BarSubItemCanteen.Name = "BarSubItemCanteen"
        Me.BarSubItemCanteen.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'BarButtonItemSlab
        '
        Me.BarButtonItemSlab.Caption = "Meal Time Slab"
        Me.BarButtonItemSlab.Id = 63
        Me.BarButtonItemSlab.ImageOptions.Image = CType(resources.GetObject("BarButtonItemSlab.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItemSlab.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItemSlab.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItemSlab.Name = "BarButtonItemSlab"
        '
        'BarButtonItemMealMenu
        '
        Me.BarButtonItemMealMenu.Caption = "Meal Menu"
        Me.BarButtonItemMealMenu.Id = 64
        Me.BarButtonItemMealMenu.ImageOptions.Image = CType(resources.GetObject("BarButtonItemMealMenu.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItemMealMenu.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItemMealMenu.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItemMealMenu.Name = "BarButtonItemMealMenu"
        '
        'BarButtonItemCanReports
        '
        Me.BarButtonItemCanReports.Caption = "Reports"
        Me.BarButtonItemCanReports.Id = 65
        Me.BarButtonItemCanReports.ImageOptions.Image = CType(resources.GetObject("BarButtonItemCanReports.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItemCanReports.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItemCanReports.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItemCanReports.Name = "BarButtonItemCanReports"
        '
        'BarSubItemVisitor
        '
        Me.BarSubItemVisitor.Caption = "Visitor"
        Me.BarSubItemVisitor.Id = 66
        Me.BarSubItemVisitor.ImageOptions.Image = CType(resources.GetObject("BarSubItemVisitor.ImageOptions.Image"), System.Drawing.Image)
        Me.BarSubItemVisitor.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemVEntry), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemVHistory)})
        Me.BarSubItemVisitor.MenuAppearance.AppearanceMenu.Hovered.Font = New System.Drawing.Font("Tahoma", 14.0!)
        Me.BarSubItemVisitor.MenuAppearance.AppearanceMenu.Hovered.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarSubItemVisitor.MenuAppearance.AppearanceMenu.Hovered.Options.UseFont = True
        Me.BarSubItemVisitor.MenuAppearance.AppearanceMenu.Hovered.Options.UseForeColor = True
        Me.BarSubItemVisitor.MenuAppearance.AppearanceMenu.Normal.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.BarSubItemVisitor.MenuAppearance.AppearanceMenu.Normal.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarSubItemVisitor.MenuAppearance.AppearanceMenu.Normal.Options.UseFont = True
        Me.BarSubItemVisitor.MenuAppearance.AppearanceMenu.Normal.Options.UseForeColor = True
        Me.BarSubItemVisitor.MenuAppearance.AppearanceMenu.Pressed.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.BarSubItemVisitor.MenuAppearance.AppearanceMenu.Pressed.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarSubItemVisitor.MenuAppearance.AppearanceMenu.Pressed.Options.UseFont = True
        Me.BarSubItemVisitor.MenuAppearance.AppearanceMenu.Pressed.Options.UseForeColor = True
        Me.BarSubItemVisitor.Name = "BarSubItemVisitor"
        Me.BarSubItemVisitor.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'BarButtonItemVEntry
        '
        Me.BarButtonItemVEntry.Caption = "Visitor Entry"
        Me.BarButtonItemVEntry.Id = 67
        Me.BarButtonItemVEntry.ImageOptions.Image = CType(resources.GetObject("BarButtonItemVEntry.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItemVEntry.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItemVEntry.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItemVEntry.Name = "BarButtonItemVEntry"
        '
        'BarButtonItemVHistory
        '
        Me.BarButtonItemVHistory.Caption = "Visitor History"
        Me.BarButtonItemVHistory.Id = 68
        Me.BarButtonItemVHistory.ImageOptions.Image = CType(resources.GetObject("BarButtonItemVHistory.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItemVHistory.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItemVHistory.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItemVHistory.Name = "BarButtonItemVHistory"
        '
        'BarDockControl1
        '
        Me.BarDockControl1.CausesValidation = False
        Me.BarDockControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.BarDockControl1.Location = New System.Drawing.Point(0, 0)
        Me.BarDockControl1.Manager = Me.BarManager2
        Me.BarDockControl1.Size = New System.Drawing.Size(1184, 84)
        '
        'BarDockControl2
        '
        Me.BarDockControl2.CausesValidation = False
        Me.BarDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.BarDockControl2.Location = New System.Drawing.Point(0, 711)
        Me.BarDockControl2.Manager = Me.BarManager2
        Me.BarDockControl2.Size = New System.Drawing.Size(1184, 0)
        '
        'BarDockControl3
        '
        Me.BarDockControl3.CausesValidation = False
        Me.BarDockControl3.Dock = System.Windows.Forms.DockStyle.Left
        Me.BarDockControl3.Location = New System.Drawing.Point(0, 84)
        Me.BarDockControl3.Manager = Me.BarManager2
        Me.BarDockControl3.Size = New System.Drawing.Size(0, 627)
        '
        'BarDockControl4
        '
        Me.BarDockControl4.CausesValidation = False
        Me.BarDockControl4.Dock = System.Windows.Forms.DockStyle.Right
        Me.BarDockControl4.Location = New System.Drawing.Point(1184, 84)
        Me.BarDockControl4.Manager = Me.BarManager2
        Me.BarDockControl4.Size = New System.Drawing.Size(0, 627)
        '
        'BarButtonHome
        '
        Me.BarButtonHome.Caption = "Home"
        Me.BarButtonHome.Id = 1
        Me.BarButtonHome.ImageOptions.Image = CType(resources.GetObject("BarButtonHome.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonHome.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonHome.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonHome.ItemAppearance.Normal.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BarButtonHome.ItemAppearance.Normal.Options.UseFont = True
        Me.BarButtonHome.Name = "BarButtonHome"
        Me.BarButtonHome.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'BarButtonItem3
        '
        Me.BarButtonItem3.Caption = "BarButtonItem3"
        Me.BarButtonItem3.Id = 2
        Me.BarButtonItem3.Name = "BarButtonItem3"
        '
        'BarSubItem1
        '
        Me.BarSubItem1.Caption = "BarSubItem1"
        Me.BarSubItem1.Id = 3
        Me.BarSubItem1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem4), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem5)})
        Me.BarSubItem1.Name = "BarSubItem1"
        '
        'BarButtonItem4
        '
        Me.BarButtonItem4.Caption = "BarButtonItem4"
        Me.BarButtonItem4.Id = 4
        Me.BarButtonItem4.Name = "BarButtonItem4"
        '
        'BarButtonItem5
        '
        Me.BarButtonItem5.Caption = "BarButtonItem5"
        Me.BarButtonItem5.Id = 5
        Me.BarButtonItem5.Name = "BarButtonItem5"
        '
        'BarButtonItemDMgmt
        '
        Me.BarButtonItemDMgmt.Caption = "Device Management"
        Me.BarButtonItemDMgmt.Id = 39
        Me.BarButtonItemDMgmt.ImageOptions.Image = CType(resources.GetObject("BarButtonItemDMgmt.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItemDMgmt.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItemDMgmt.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItemDMgmt.ItemInMenuAppearance.Hovered.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarButtonItemDMgmt.ItemInMenuAppearance.Hovered.Options.UseForeColor = True
        Me.BarButtonItemDMgmt.ItemInMenuAppearance.Pressed.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarButtonItemDMgmt.ItemInMenuAppearance.Pressed.Options.UseForeColor = True
        Me.BarButtonItemDMgmt.Name = "BarButtonItemDMgmt"
        '
        'BarButtonItem1
        '
        Me.BarButtonItem1.Id = 40
        Me.BarButtonItem1.Name = "BarButtonItem1"
        '
        'BarSubItem2
        '
        Me.BarSubItem2.Caption = "BarSubItem2"
        Me.BarSubItem2.Id = 58
        Me.BarSubItem2.Name = "BarSubItem2"
        '
        'PopupMenu1
        '
        Me.PopupMenu1.Manager = Me.BarManager2
        Me.PopupMenu1.Name = "PopupMenu1"
        '
        'TimerIOCL
        '
        Me.TimerIOCL.Interval = 1000
        '
        'XtraMasterTest
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1184, 711)
        Me.Controls.Add(Me.PanelControl1)
        Me.Controls.Add(Me.barDockControlLeft)
        Me.Controls.Add(Me.barDockControlRight)
        Me.Controls.Add(Me.barDockControlBottom)
        Me.Controls.Add(Me.barDockControlTop)
        Me.Controls.Add(Me.BarDockControl3)
        Me.Controls.Add(Me.BarDockControl4)
        Me.Controls.Add(Me.BarDockControl2)
        Me.Controls.Add(Me.BarDockControl1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.LookAndFeel.SkinName = "iMaginary"
        Me.LookAndFeel.TouchUIMode = DevExpress.Utils.DefaultBoolean.[False]
        Me.LookAndFeel.UseDefaultLookAndFeel = False
        Me.Name = "XtraMasterTest"
        Me.Text = "Integrated Attendance System"
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.SidePanelTitle.ResumeLayout(False)
        Me.SidePanelTitle.PerformLayout()
        Me.SidePanel1.ResumeLayout(False)
        Me.SidePanel8.ResumeLayout(False)
        Me.SidePanel8.PerformLayout()
        Me.SidePanel7.ResumeLayout(False)
        Me.SidePanel7.PerformLayout()
        Me.SidePanel6.ResumeLayout(False)
        Me.SidePanel6.PerformLayout()
        Me.SidePanel5.ResumeLayout(False)
        Me.SidePanel5.PerformLayout()
        Me.SidePanel4.ResumeLayout(False)
        Me.SidePanel4.PerformLayout()
        Me.SidePanel3.ResumeLayout(False)
        Me.SidePanel3.PerformLayout()
        CType(Me.ToastNotificationsManager1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BarManager2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupMenu1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents SidePanel1 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents TimerShowClock As System.Windows.Forms.Timer
    Friend WithEvents SidePanel3 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SidePanel4 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SidePanel5 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents LabelControlTime As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SidePanel6 As DevExpress.XtraEditors.SidePanel
    Public WithEvents LabelControlStatus As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TimerReportEmail As System.Windows.Forms.Timer
    Friend WithEvents AlertControl1 As DevExpress.XtraBars.Alerter.AlertControl
    Friend WithEvents ToastNotificationsManager1 As DevExpress.XtraBars.ToastNotifications.ToastNotificationsManager
    Friend WithEvents TimerDataProcess As System.Windows.Forms.Timer
    Friend WithEvents SidePanel7 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents LabelControlUser As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TimerSMS As System.Windows.Forms.Timer
    Friend WithEvents TimerCloud As System.Windows.Forms.Timer
    Friend WithEvents TimerAutoDownload As System.Windows.Forms.Timer
    Friend WithEvents TimerAutpBK As System.Windows.Forms.Timer
    Friend WithEvents SidePanel8 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents LabelControlCount As DevExpress.XtraEditors.LabelControl
    Friend WithEvents BarManager1 As DevExpress.XtraBars.BarManager
    Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
    Friend WithEvents BarDockControl3 As DevExpress.XtraBars.BarDockControl
    Friend WithEvents BarManager2 As DevExpress.XtraBars.BarManager
    Friend WithEvents Bar1 As DevExpress.XtraBars.Bar
    Friend WithEvents BarButtonHome As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem3 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarSubItemDevice As DevExpress.XtraBars.BarSubItem
    Friend WithEvents BarDockControl1 As DevExpress.XtraBars.BarDockControl
    Friend WithEvents BarDockControl2 As DevExpress.XtraBars.BarDockControl
    Friend WithEvents BarDockControl4 As DevExpress.XtraBars.BarDockControl
    Friend WithEvents BarSubItem1 As DevExpress.XtraBars.BarSubItem
    Friend WithEvents BarButtonItem4 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem5 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents PopupMenu1 As DevExpress.XtraBars.PopupMenu
    Friend WithEvents BarSubItemMaster As DevExpress.XtraBars.BarSubItem
    Friend WithEvents BarButtonItemComp As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItemLocation As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItemDept As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItemShift As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItemGrade As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItemEmpGrp As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItemBank As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItemDisp As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItemCat As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItemEmp As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents SidePanelTitle As DevExpress.XtraEditors.SidePanel
    Friend WithEvents LabelTitle As System.Windows.Forms.Label
    Friend WithEvents SidePanelMainFormShow As DevExpress.XtraEditors.SidePanel
    Friend WithEvents BarSubItemTrans As DevExpress.XtraBars.BarSubItem
    Friend WithEvents BarButtonItemManual As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItemDataMaint As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItemHoliday As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarSubItemLeaveMgmt As DevExpress.XtraBars.BarSubItem
    Friend WithEvents BarButtonItemLMaster As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItemlAcc As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItemLApp As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarSubItemAdmin As DevExpress.XtraBars.BarSubItem
    Friend WithEvents BarButtonItemCSettings As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItemDPro As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItemDB As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItemSMS As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItemBSMS As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItemEmail As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItemUManage As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItemBackUp As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItemPDB As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItemDMgmt As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem1 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarSubItemReports As DevExpress.XtraBars.BarSubItem
    Friend WithEvents BarButtonItemDReports As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItemMReports As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItemLReports As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItemCreports As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarSubItemPayRoll As DevExpress.XtraBars.BarSubItem
    Friend WithEvents BarButtonItemPEmp As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItemPSetup As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItemFormula As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItemPayPro As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItemPerMain As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItemLoan As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItemPayReports As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItemLeaveIncr As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarSubItem2 As DevExpress.XtraBars.BarSubItem
    Friend WithEvents BarButtonItemHome As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem2 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem6 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarSubItemCanteen As DevExpress.XtraBars.BarSubItem
    Friend WithEvents BarButtonItemSlab As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItemMealMenu As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItemCanReports As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarSubItemVisitor As DevExpress.XtraBars.BarSubItem
    Friend WithEvents BarButtonItemVEntry As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItemVHistory As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents TimerIOCL As System.Windows.Forms.Timer
    Friend WithEvents BarButtonOTComp As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItemOTMaint As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarMenuDevice As DevExpress.XtraBars.BarSubItem
    Friend WithEvents BarButtoniAS As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonUltra As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarMenuLog As DevExpress.XtraBars.BarSubItem
    Friend WithEvents BarButtonLogiAS As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonLogUltra As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarMenuTemplate As DevExpress.XtraBars.BarSubItem
    Friend WithEvents BarButtonTempiAS As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonTempUltra As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarMenuRealTime As DevExpress.XtraBars.BarSubItem
    Friend WithEvents BarButtonRealiAS As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonRealUltra As DevExpress.XtraBars.BarButtonItem
End Class
