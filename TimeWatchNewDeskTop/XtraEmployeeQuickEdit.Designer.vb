﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraEmployeeQuickEdit
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.SimpleButton2 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl()
        Me.CheckDOJ = New DevExpress.XtraEditors.CheckEdit()
        Me.ComboBoxEditNepaliYear = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboBoxEditNEpaliMonth = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboBoxEditNepaliDate = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.DateEditJoin = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl()
        Me.CheckGrd = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEmpGrp = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckCat = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckDept = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckLoc = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckCom = New DevExpress.XtraEditors.CheckEdit()
        Me.GridLookUpEditBr = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.TblbranchBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SSSDBDataSet = New iAS.SSSDBDataSet()
        Me.GridView7 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.LabelControl37 = New DevExpress.XtraEditors.LabelControl()
        Me.GridLookUpEditGrd = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.TblGradeBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GridView4 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colGradeCode = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colGradeName = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridLookUpEditEmpGrp = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.EmployeeGroupBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GridView3 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colGroupId = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colGroupName = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridLookUpEditCat = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.TblCatagoryBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colCAT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCATAGORYNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridLookUpEditDept = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.TblDepartmentBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colDEPARTMENTCODE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDEPARTMENTNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridLookUpEditCom = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.TblCompanyBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GridLookUpEdit1View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colCOMPANYCODE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCOMPANYNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.TblCompanyTableAdapter = New iAS.SSSDBDataSetTableAdapters.tblCompanyTableAdapter()
        Me.TblCompany1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.tblCompany1TableAdapter()
        Me.TblDepartmentTableAdapter = New iAS.SSSDBDataSetTableAdapters.tblDepartmentTableAdapter()
        Me.TblDepartment1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.tblDepartment1TableAdapter()
        Me.TblCatagoryTableAdapter = New iAS.SSSDBDataSetTableAdapters.tblCatagoryTableAdapter()
        Me.TblCatagory1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.tblCatagory1TableAdapter()
        Me.EmployeeGroupTableAdapter = New iAS.SSSDBDataSetTableAdapters.EmployeeGroupTableAdapter()
        Me.TblGrade1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.tblGrade1TableAdapter()
        Me.TblGradeTableAdapter = New iAS.SSSDBDataSetTableAdapters.tblGradeTableAdapter()
        Me.EmployeeGroup1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.EmployeeGroup1TableAdapter()
        Me.TblDESPANSARYBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TblDESPANSARYTableAdapter = New iAS.SSSDBDataSetTableAdapters.tblDESPANSARYTableAdapter()
        Me.TblBankBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TblBankTableAdapter = New iAS.SSSDBDataSetTableAdapters.tblBankTableAdapter()
        Me.TblbranchTableAdapter = New iAS.SSSDBDataSetTableAdapters.tblbranchTableAdapter()
        Me.TblBank1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.tblBank1TableAdapter()
        Me.TblDESPANSARY1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.tblDESPANSARY1TableAdapter()
        Me.Tblbranch1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.tblbranch1TableAdapter()
        Me.TblMachineBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TblMachineTableAdapter = New iAS.SSSDBDataSetTableAdapters.tblMachineTableAdapter()
        Me.TblMachine1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.tblMachine1TableAdapter()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl2.SuspendLayout()
        CType(Me.CheckDOJ.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEditNepaliYear.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEditNEpaliMonth.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEditNepaliDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEditJoin.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEditJoin.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckGrd.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEmpGrp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckCat.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckDept.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckLoc.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckCom.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEditBr.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblbranchBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEditGrd.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblGradeBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEditEmpGrp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmployeeGroupBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEditCat.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblCatagoryBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEditDept.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblDepartmentBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEditCom.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblCompanyBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEdit1View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblDESPANSARYBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblBankBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblMachineBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PanelControl1
        '
        Me.PanelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.PanelControl1.Controls.Add(Me.SimpleButton2)
        Me.PanelControl1.Controls.Add(Me.SimpleButton1)
        Me.PanelControl1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.PanelControl1.Location = New System.Drawing.Point(0, 205)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(350, 37)
        Me.PanelControl1.TabIndex = 0
        '
        'SimpleButton2
        '
        Me.SimpleButton2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButton2.Appearance.Options.UseFont = True
        Me.SimpleButton2.Location = New System.Drawing.Point(161, 6)
        Me.SimpleButton2.Name = "SimpleButton2"
        Me.SimpleButton2.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton2.TabIndex = 38
        Me.SimpleButton2.Text = "Cancel"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButton1.Appearance.Options.UseFont = True
        Me.SimpleButton1.Location = New System.Drawing.Point(80, 6)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton1.TabIndex = 37
        Me.SimpleButton1.Text = "Update"
        '
        'PanelControl2
        '
        Me.PanelControl2.Controls.Add(Me.CheckDOJ)
        Me.PanelControl2.Controls.Add(Me.ComboBoxEditNepaliYear)
        Me.PanelControl2.Controls.Add(Me.ComboBoxEditNEpaliMonth)
        Me.PanelControl2.Controls.Add(Me.ComboBoxEditNepaliDate)
        Me.PanelControl2.Controls.Add(Me.DateEditJoin)
        Me.PanelControl2.Controls.Add(Me.LabelControl17)
        Me.PanelControl2.Controls.Add(Me.CheckGrd)
        Me.PanelControl2.Controls.Add(Me.CheckEmpGrp)
        Me.PanelControl2.Controls.Add(Me.CheckCat)
        Me.PanelControl2.Controls.Add(Me.CheckDept)
        Me.PanelControl2.Controls.Add(Me.CheckLoc)
        Me.PanelControl2.Controls.Add(Me.CheckCom)
        Me.PanelControl2.Controls.Add(Me.GridLookUpEditBr)
        Me.PanelControl2.Controls.Add(Me.LabelControl37)
        Me.PanelControl2.Controls.Add(Me.GridLookUpEditGrd)
        Me.PanelControl2.Controls.Add(Me.GridLookUpEditEmpGrp)
        Me.PanelControl2.Controls.Add(Me.GridLookUpEditCat)
        Me.PanelControl2.Controls.Add(Me.GridLookUpEditDept)
        Me.PanelControl2.Controls.Add(Me.GridLookUpEditCom)
        Me.PanelControl2.Controls.Add(Me.LabelControl6)
        Me.PanelControl2.Controls.Add(Me.LabelControl7)
        Me.PanelControl2.Controls.Add(Me.LabelControl10)
        Me.PanelControl2.Controls.Add(Me.LabelControl8)
        Me.PanelControl2.Controls.Add(Me.LabelControl9)
        Me.PanelControl2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelControl2.Location = New System.Drawing.Point(0, 0)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(350, 205)
        Me.PanelControl2.TabIndex = 1
        '
        'CheckDOJ
        '
        Me.CheckDOJ.Location = New System.Drawing.Point(310, 162)
        Me.CheckDOJ.Name = "CheckDOJ"
        Me.CheckDOJ.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckDOJ.Properties.Appearance.Options.UseFont = True
        Me.CheckDOJ.Properties.Caption = ""
        Me.CheckDOJ.Size = New System.Drawing.Size(18, 19)
        Me.CheckDOJ.TabIndex = 53
        '
        'ComboBoxEditNepaliYear
        '
        Me.ComboBoxEditNepaliYear.Location = New System.Drawing.Point(233, 162)
        Me.ComboBoxEditNepaliYear.Name = "ComboBoxEditNepaliYear"
        Me.ComboBoxEditNepaliYear.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEditNepaliYear.Properties.Appearance.Options.UseFont = True
        Me.ComboBoxEditNepaliYear.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEditNepaliYear.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboBoxEditNepaliYear.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEditNepaliYear.Properties.Items.AddRange(New Object() {"2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037", "2038", "2039", "2040", "2041", "2042", "2043", "2044", "2045", "2046", "2047", "2048", "2049", "2050", "2051", "2052", "2053", "2054", "2055", "2056", "2057", "2058", "2059", "2060", "2061", "2062", "2063", "2064", "2065", "2066", "2067", "2068", "2069", "2070", "2071", "2072", "2073", "2074", "2075", "2076", "2077", "2078", "2079", "2080", "2081", "2082", "2083", "2084", "2085", "2086", "2087", "2088", "2089"})
        Me.ComboBoxEditNepaliYear.Size = New System.Drawing.Size(61, 20)
        Me.ComboBoxEditNepaliYear.TabIndex = 52
        '
        'ComboBoxEditNEpaliMonth
        '
        Me.ComboBoxEditNEpaliMonth.Location = New System.Drawing.Point(161, 162)
        Me.ComboBoxEditNEpaliMonth.Name = "ComboBoxEditNEpaliMonth"
        Me.ComboBoxEditNEpaliMonth.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEditNEpaliMonth.Properties.Appearance.Options.UseFont = True
        Me.ComboBoxEditNEpaliMonth.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEditNEpaliMonth.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboBoxEditNEpaliMonth.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEditNEpaliMonth.Properties.Items.AddRange(New Object() {"Baishakh", "Jestha", "Asar", "Shrawan", "Bhadau", "Aswin", "Kartik", "Mansir", "Poush", "Magh", "Falgun", "Chaitra"})
        Me.ComboBoxEditNEpaliMonth.Size = New System.Drawing.Size(66, 20)
        Me.ComboBoxEditNEpaliMonth.TabIndex = 51
        '
        'ComboBoxEditNepaliDate
        '
        Me.ComboBoxEditNepaliDate.Location = New System.Drawing.Point(113, 162)
        Me.ComboBoxEditNepaliDate.Name = "ComboBoxEditNepaliDate"
        Me.ComboBoxEditNepaliDate.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEditNepaliDate.Properties.Appearance.Options.UseFont = True
        Me.ComboBoxEditNepaliDate.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEditNepaliDate.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboBoxEditNepaliDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEditNepaliDate.Properties.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32"})
        Me.ComboBoxEditNepaliDate.Size = New System.Drawing.Size(42, 20)
        Me.ComboBoxEditNepaliDate.TabIndex = 50
        '
        'DateEditJoin
        '
        Me.DateEditJoin.EditValue = Nothing
        Me.DateEditJoin.Location = New System.Drawing.Point(126, 162)
        Me.DateEditJoin.Name = "DateEditJoin"
        Me.DateEditJoin.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.DateEditJoin.Properties.Appearance.Options.UseFont = True
        Me.DateEditJoin.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEditJoin.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEditJoin.Properties.Mask.EditMask = "dd/MM/yyyy"
        Me.DateEditJoin.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.DateEditJoin.Size = New System.Drawing.Size(135, 20)
        Me.DateEditJoin.TabIndex = 48
        '
        'LabelControl17
        '
        Me.LabelControl17.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl17.Appearance.Options.UseFont = True
        Me.LabelControl17.Location = New System.Drawing.Point(12, 165)
        Me.LabelControl17.Name = "LabelControl17"
        Me.LabelControl17.Size = New System.Drawing.Size(82, 14)
        Me.LabelControl17.TabIndex = 49
        Me.LabelControl17.Text = "Date of Joining"
        '
        'CheckGrd
        '
        Me.CheckGrd.Location = New System.Drawing.Point(310, 137)
        Me.CheckGrd.Name = "CheckGrd"
        Me.CheckGrd.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckGrd.Properties.Appearance.Options.UseFont = True
        Me.CheckGrd.Properties.Caption = ""
        Me.CheckGrd.Size = New System.Drawing.Size(18, 19)
        Me.CheckGrd.TabIndex = 47
        '
        'CheckEmpGrp
        '
        Me.CheckEmpGrp.Location = New System.Drawing.Point(310, 112)
        Me.CheckEmpGrp.Name = "CheckEmpGrp"
        Me.CheckEmpGrp.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEmpGrp.Properties.Appearance.Options.UseFont = True
        Me.CheckEmpGrp.Properties.Caption = ""
        Me.CheckEmpGrp.Size = New System.Drawing.Size(18, 19)
        Me.CheckEmpGrp.TabIndex = 46
        '
        'CheckCat
        '
        Me.CheckCat.Location = New System.Drawing.Point(310, 88)
        Me.CheckCat.Name = "CheckCat"
        Me.CheckCat.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckCat.Properties.Appearance.Options.UseFont = True
        Me.CheckCat.Properties.Caption = ""
        Me.CheckCat.Size = New System.Drawing.Size(18, 19)
        Me.CheckCat.TabIndex = 45
        '
        'CheckDept
        '
        Me.CheckDept.Location = New System.Drawing.Point(310, 64)
        Me.CheckDept.Name = "CheckDept"
        Me.CheckDept.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckDept.Properties.Appearance.Options.UseFont = True
        Me.CheckDept.Properties.Caption = ""
        Me.CheckDept.Size = New System.Drawing.Size(18, 19)
        Me.CheckDept.TabIndex = 44
        '
        'CheckLoc
        '
        Me.CheckLoc.Location = New System.Drawing.Point(310, 38)
        Me.CheckLoc.Name = "CheckLoc"
        Me.CheckLoc.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckLoc.Properties.Appearance.Options.UseFont = True
        Me.CheckLoc.Properties.Caption = ""
        Me.CheckLoc.Size = New System.Drawing.Size(18, 19)
        Me.CheckLoc.TabIndex = 43
        '
        'CheckCom
        '
        Me.CheckCom.Location = New System.Drawing.Point(310, 12)
        Me.CheckCom.Name = "CheckCom"
        Me.CheckCom.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckCom.Properties.Appearance.Options.UseFont = True
        Me.CheckCom.Properties.Caption = ""
        Me.CheckCom.Size = New System.Drawing.Size(18, 19)
        Me.CheckCom.TabIndex = 42
        '
        'GridLookUpEditBr
        '
        Me.GridLookUpEditBr.Location = New System.Drawing.Point(126, 38)
        Me.GridLookUpEditBr.Name = "GridLookUpEditBr"
        Me.GridLookUpEditBr.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEditBr.Properties.Appearance.Options.UseFont = True
        Me.GridLookUpEditBr.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GridLookUpEditBr.Properties.DataSource = Me.TblbranchBindingSource
        Me.GridLookUpEditBr.Properties.DisplayMember = "BRANCHCODE"
        Me.GridLookUpEditBr.Properties.NullText = ""
        Me.GridLookUpEditBr.Properties.ValueMember = "BRANCHCODE"
        Me.GridLookUpEditBr.Properties.View = Me.GridView7
        Me.GridLookUpEditBr.Size = New System.Drawing.Size(135, 20)
        Me.GridLookUpEditBr.TabIndex = 2
        '
        'TblbranchBindingSource
        '
        Me.TblbranchBindingSource.DataMember = "tblbranch"
        Me.TblbranchBindingSource.DataSource = Me.SSSDBDataSet
        '
        'SSSDBDataSet
        '
        Me.SSSDBDataSet.DataSetName = "SSSDBDataSet"
        Me.SSSDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GridView7
        '
        Me.GridView7.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn2})
        Me.GridView7.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView7.Name = "GridView7"
        Me.GridView7.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView7.OptionsView.ShowGroupPanel = False
        '
        'GridColumn1
        '
        Me.GridColumn1.FieldName = "BRANCHCODE"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 0
        '
        'GridColumn2
        '
        Me.GridColumn2.FieldName = "BRANCHNAME"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 1
        '
        'LabelControl37
        '
        Me.LabelControl37.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl37.Appearance.Options.UseFont = True
        Me.LabelControl37.Location = New System.Drawing.Point(14, 41)
        Me.LabelControl37.Name = "LabelControl37"
        Me.LabelControl37.Size = New System.Drawing.Size(50, 14)
        Me.LabelControl37.TabIndex = 41
        Me.LabelControl37.Text = "Location "
        '
        'GridLookUpEditGrd
        '
        Me.GridLookUpEditGrd.Location = New System.Drawing.Point(126, 136)
        Me.GridLookUpEditGrd.Name = "GridLookUpEditGrd"
        Me.GridLookUpEditGrd.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEditGrd.Properties.Appearance.Options.UseFont = True
        Me.GridLookUpEditGrd.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GridLookUpEditGrd.Properties.DataSource = Me.TblGradeBindingSource
        Me.GridLookUpEditGrd.Properties.DisplayMember = "GradeCode"
        Me.GridLookUpEditGrd.Properties.NullText = ""
        Me.GridLookUpEditGrd.Properties.ValueMember = "GradeCode"
        Me.GridLookUpEditGrd.Properties.View = Me.GridView4
        Me.GridLookUpEditGrd.Size = New System.Drawing.Size(135, 20)
        Me.GridLookUpEditGrd.TabIndex = 6
        '
        'TblGradeBindingSource
        '
        Me.TblGradeBindingSource.DataMember = "tblGrade"
        Me.TblGradeBindingSource.DataSource = Me.SSSDBDataSet
        '
        'GridView4
        '
        Me.GridView4.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colGradeCode, Me.colGradeName})
        Me.GridView4.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView4.Name = "GridView4"
        Me.GridView4.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView4.OptionsView.ShowGroupPanel = False
        '
        'colGradeCode
        '
        Me.colGradeCode.FieldName = "GradeCode"
        Me.colGradeCode.Name = "colGradeCode"
        Me.colGradeCode.Visible = True
        Me.colGradeCode.VisibleIndex = 0
        '
        'colGradeName
        '
        Me.colGradeName.FieldName = "GradeName"
        Me.colGradeName.Name = "colGradeName"
        Me.colGradeName.Visible = True
        Me.colGradeName.VisibleIndex = 1
        '
        'GridLookUpEditEmpGrp
        '
        Me.GridLookUpEditEmpGrp.Location = New System.Drawing.Point(126, 112)
        Me.GridLookUpEditEmpGrp.Name = "GridLookUpEditEmpGrp"
        Me.GridLookUpEditEmpGrp.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEditEmpGrp.Properties.Appearance.Options.UseFont = True
        Me.GridLookUpEditEmpGrp.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GridLookUpEditEmpGrp.Properties.DataSource = Me.EmployeeGroupBindingSource
        Me.GridLookUpEditEmpGrp.Properties.DisplayMember = "GroupId"
        Me.GridLookUpEditEmpGrp.Properties.NullText = ""
        Me.GridLookUpEditEmpGrp.Properties.ValueMember = "GroupId"
        Me.GridLookUpEditEmpGrp.Properties.View = Me.GridView3
        Me.GridLookUpEditEmpGrp.Size = New System.Drawing.Size(135, 20)
        Me.GridLookUpEditEmpGrp.TabIndex = 5
        '
        'EmployeeGroupBindingSource
        '
        Me.EmployeeGroupBindingSource.DataMember = "EmployeeGroup"
        Me.EmployeeGroupBindingSource.DataSource = Me.SSSDBDataSet
        '
        'GridView3
        '
        Me.GridView3.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colGroupId, Me.colGroupName})
        Me.GridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView3.Name = "GridView3"
        Me.GridView3.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView3.OptionsView.ShowGroupPanel = False
        '
        'colGroupId
        '
        Me.colGroupId.FieldName = "GroupId"
        Me.colGroupId.Name = "colGroupId"
        Me.colGroupId.Visible = True
        Me.colGroupId.VisibleIndex = 0
        '
        'colGroupName
        '
        Me.colGroupName.FieldName = "GroupName"
        Me.colGroupName.Name = "colGroupName"
        Me.colGroupName.Visible = True
        Me.colGroupName.VisibleIndex = 1
        '
        'GridLookUpEditCat
        '
        Me.GridLookUpEditCat.Location = New System.Drawing.Point(126, 88)
        Me.GridLookUpEditCat.Name = "GridLookUpEditCat"
        Me.GridLookUpEditCat.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEditCat.Properties.Appearance.Options.UseFont = True
        Me.GridLookUpEditCat.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GridLookUpEditCat.Properties.DataSource = Me.TblCatagoryBindingSource
        Me.GridLookUpEditCat.Properties.DisplayMember = "CAT"
        Me.GridLookUpEditCat.Properties.NullText = ""
        Me.GridLookUpEditCat.Properties.ValueMember = "CAT"
        Me.GridLookUpEditCat.Properties.View = Me.GridView2
        Me.GridLookUpEditCat.Size = New System.Drawing.Size(135, 20)
        Me.GridLookUpEditCat.TabIndex = 4
        '
        'TblCatagoryBindingSource
        '
        Me.TblCatagoryBindingSource.DataMember = "tblCatagory"
        Me.TblCatagoryBindingSource.DataSource = Me.SSSDBDataSet
        '
        'GridView2
        '
        Me.GridView2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colCAT, Me.colCATAGORYNAME})
        Me.GridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView2.Name = "GridView2"
        Me.GridView2.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView2.OptionsView.ShowGroupPanel = False
        '
        'colCAT
        '
        Me.colCAT.FieldName = "CAT"
        Me.colCAT.Name = "colCAT"
        Me.colCAT.Visible = True
        Me.colCAT.VisibleIndex = 0
        '
        'colCATAGORYNAME
        '
        Me.colCATAGORYNAME.FieldName = "CATAGORYNAME"
        Me.colCATAGORYNAME.Name = "colCATAGORYNAME"
        Me.colCATAGORYNAME.Visible = True
        Me.colCATAGORYNAME.VisibleIndex = 1
        '
        'GridLookUpEditDept
        '
        Me.GridLookUpEditDept.Location = New System.Drawing.Point(126, 64)
        Me.GridLookUpEditDept.Name = "GridLookUpEditDept"
        Me.GridLookUpEditDept.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEditDept.Properties.Appearance.Options.UseFont = True
        Me.GridLookUpEditDept.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GridLookUpEditDept.Properties.DataSource = Me.TblDepartmentBindingSource
        Me.GridLookUpEditDept.Properties.DisplayMember = "DEPARTMENTCODE"
        Me.GridLookUpEditDept.Properties.NullText = ""
        Me.GridLookUpEditDept.Properties.ValueMember = "DEPARTMENTCODE"
        Me.GridLookUpEditDept.Properties.View = Me.GridView1
        Me.GridLookUpEditDept.Size = New System.Drawing.Size(135, 20)
        Me.GridLookUpEditDept.TabIndex = 3
        '
        'TblDepartmentBindingSource
        '
        Me.TblDepartmentBindingSource.DataMember = "tblDepartment"
        Me.TblDepartmentBindingSource.DataSource = Me.SSSDBDataSet
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colDEPARTMENTCODE, Me.colDEPARTMENTNAME})
        Me.GridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView1.OptionsView.ShowGroupPanel = False
        '
        'colDEPARTMENTCODE
        '
        Me.colDEPARTMENTCODE.FieldName = "DEPARTMENTCODE"
        Me.colDEPARTMENTCODE.Name = "colDEPARTMENTCODE"
        Me.colDEPARTMENTCODE.Visible = True
        Me.colDEPARTMENTCODE.VisibleIndex = 0
        '
        'colDEPARTMENTNAME
        '
        Me.colDEPARTMENTNAME.FieldName = "DEPARTMENTNAME"
        Me.colDEPARTMENTNAME.Name = "colDEPARTMENTNAME"
        Me.colDEPARTMENTNAME.Visible = True
        Me.colDEPARTMENTNAME.VisibleIndex = 1
        '
        'GridLookUpEditCom
        '
        Me.GridLookUpEditCom.Location = New System.Drawing.Point(126, 12)
        Me.GridLookUpEditCom.Name = "GridLookUpEditCom"
        Me.GridLookUpEditCom.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEditCom.Properties.Appearance.Options.UseFont = True
        Me.GridLookUpEditCom.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GridLookUpEditCom.Properties.DataSource = Me.TblCompanyBindingSource
        Me.GridLookUpEditCom.Properties.DisplayMember = "COMPANYCODE"
        Me.GridLookUpEditCom.Properties.NullText = ""
        Me.GridLookUpEditCom.Properties.ValueMember = "COMPANYCODE"
        Me.GridLookUpEditCom.Properties.View = Me.GridLookUpEdit1View
        Me.GridLookUpEditCom.Size = New System.Drawing.Size(135, 20)
        Me.GridLookUpEditCom.TabIndex = 1
        '
        'TblCompanyBindingSource
        '
        Me.TblCompanyBindingSource.DataMember = "tblCompany"
        Me.TblCompanyBindingSource.DataSource = Me.SSSDBDataSet
        '
        'GridLookUpEdit1View
        '
        Me.GridLookUpEdit1View.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colCOMPANYCODE, Me.colCOMPANYNAME})
        Me.GridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridLookUpEdit1View.Name = "GridLookUpEdit1View"
        Me.GridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridLookUpEdit1View.OptionsView.ShowGroupPanel = False
        '
        'colCOMPANYCODE
        '
        Me.colCOMPANYCODE.FieldName = "COMPANYCODE"
        Me.colCOMPANYCODE.Name = "colCOMPANYCODE"
        Me.colCOMPANYCODE.Visible = True
        Me.colCOMPANYCODE.VisibleIndex = 0
        '
        'colCOMPANYNAME
        '
        Me.colCOMPANYNAME.FieldName = "COMPANYNAME"
        Me.colCOMPANYNAME.Name = "colCOMPANYNAME"
        Me.colCOMPANYNAME.Visible = True
        Me.colCOMPANYNAME.VisibleIndex = 1
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl6.Appearance.Options.UseFont = True
        Me.LabelControl6.Location = New System.Drawing.Point(14, 16)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(54, 14)
        Me.LabelControl6.TabIndex = 34
        Me.LabelControl6.Text = "Company "
        '
        'LabelControl7
        '
        Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl7.Appearance.Options.UseFont = True
        Me.LabelControl7.Location = New System.Drawing.Point(14, 68)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(70, 14)
        Me.LabelControl7.TabIndex = 36
        Me.LabelControl7.Text = "Department "
        '
        'LabelControl10
        '
        Me.LabelControl10.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl10.Appearance.Options.UseFont = True
        Me.LabelControl10.Location = New System.Drawing.Point(14, 140)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(32, 14)
        Me.LabelControl10.TabIndex = 40
        Me.LabelControl10.Text = "Grade"
        '
        'LabelControl8
        '
        Me.LabelControl8.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl8.Appearance.Options.UseFont = True
        Me.LabelControl8.Location = New System.Drawing.Point(14, 92)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(53, 14)
        Me.LabelControl8.TabIndex = 37
        Me.LabelControl8.Text = "Category "
        '
        'LabelControl9
        '
        Me.LabelControl9.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl9.Appearance.Options.UseFont = True
        Me.LabelControl9.Location = New System.Drawing.Point(14, 116)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(94, 14)
        Me.LabelControl9.TabIndex = 39
        Me.LabelControl9.Text = "Employee Group "
        '
        'TblCompanyTableAdapter
        '
        Me.TblCompanyTableAdapter.ClearBeforeFill = True
        '
        'TblCompany1TableAdapter1
        '
        Me.TblCompany1TableAdapter1.ClearBeforeFill = True
        '
        'TblDepartmentTableAdapter
        '
        Me.TblDepartmentTableAdapter.ClearBeforeFill = True
        '
        'TblDepartment1TableAdapter1
        '
        Me.TblDepartment1TableAdapter1.ClearBeforeFill = True
        '
        'TblCatagoryTableAdapter
        '
        Me.TblCatagoryTableAdapter.ClearBeforeFill = True
        '
        'TblCatagory1TableAdapter1
        '
        Me.TblCatagory1TableAdapter1.ClearBeforeFill = True
        '
        'EmployeeGroupTableAdapter
        '
        Me.EmployeeGroupTableAdapter.ClearBeforeFill = True
        '
        'TblGrade1TableAdapter1
        '
        Me.TblGrade1TableAdapter1.ClearBeforeFill = True
        '
        'TblGradeTableAdapter
        '
        Me.TblGradeTableAdapter.ClearBeforeFill = True
        '
        'EmployeeGroup1TableAdapter1
        '
        Me.EmployeeGroup1TableAdapter1.ClearBeforeFill = True
        '
        'TblDESPANSARYBindingSource
        '
        Me.TblDESPANSARYBindingSource.DataMember = "tblDESPANSARY"
        Me.TblDESPANSARYBindingSource.DataSource = Me.SSSDBDataSet
        '
        'TblDESPANSARYTableAdapter
        '
        Me.TblDESPANSARYTableAdapter.ClearBeforeFill = True
        '
        'TblBankBindingSource
        '
        Me.TblBankBindingSource.DataMember = "tblBank"
        Me.TblBankBindingSource.DataSource = Me.SSSDBDataSet
        '
        'TblBankTableAdapter
        '
        Me.TblBankTableAdapter.ClearBeforeFill = True
        '
        'TblbranchTableAdapter
        '
        Me.TblbranchTableAdapter.ClearBeforeFill = True
        '
        'TblBank1TableAdapter1
        '
        Me.TblBank1TableAdapter1.ClearBeforeFill = True
        '
        'TblDESPANSARY1TableAdapter1
        '
        Me.TblDESPANSARY1TableAdapter1.ClearBeforeFill = True
        '
        'Tblbranch1TableAdapter1
        '
        Me.Tblbranch1TableAdapter1.ClearBeforeFill = True
        '
        'TblMachineBindingSource
        '
        Me.TblMachineBindingSource.DataMember = "tblMachine"
        Me.TblMachineBindingSource.DataSource = Me.SSSDBDataSet
        '
        'TblMachineTableAdapter
        '
        Me.TblMachineTableAdapter.ClearBeforeFill = True
        '
        'TblMachine1TableAdapter1
        '
        Me.TblMachine1TableAdapter1.ClearBeforeFill = True
        '
        'XtraEmployeeQuickEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(350, 242)
        Me.ControlBox = False
        Me.Controls.Add(Me.PanelControl2)
        Me.Controls.Add(Me.PanelControl1)
        Me.LookAndFeel.SkinName = "iMaginary"
        Me.LookAndFeel.UseDefaultLookAndFeel = False
        Me.Name = "XtraEmployeeQuickEdit"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Employee Quick Edit"
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl2.ResumeLayout(False)
        Me.PanelControl2.PerformLayout()
        CType(Me.CheckDOJ.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEditNepaliYear.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEditNEpaliMonth.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEditNepaliDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEditJoin.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEditJoin.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckGrd.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEmpGrp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckCat.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckDept.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckLoc.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckCom.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEditBr.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblbranchBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEditGrd.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblGradeBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEditEmpGrp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmployeeGroupBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEditCat.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblCatagoryBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEditDept.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblDepartmentBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEditCom.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblCompanyBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEdit1View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblDESPANSARYBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblBankBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblMachineBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents SimpleButton2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents TblCompanyBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SSSDBDataSet As iAS.SSSDBDataSet
    Friend WithEvents TblCompanyTableAdapter As iAS.SSSDBDataSetTableAdapters.tblCompanyTableAdapter
    Friend WithEvents TblCompany1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.tblCompany1TableAdapter
    Friend WithEvents TblDepartmentBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblDepartmentTableAdapter As iAS.SSSDBDataSetTableAdapters.tblDepartmentTableAdapter
    Friend WithEvents TblDepartment1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.tblDepartment1TableAdapter
    Friend WithEvents TblCatagoryBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblCatagoryTableAdapter As iAS.SSSDBDataSetTableAdapters.tblCatagoryTableAdapter
    Friend WithEvents TblCatagory1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.tblCatagory1TableAdapter
    Friend WithEvents EmployeeGroupBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents EmployeeGroupTableAdapter As iAS.SSSDBDataSetTableAdapters.EmployeeGroupTableAdapter
    Friend WithEvents TblGrade1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.tblGrade1TableAdapter
    Friend WithEvents TblGradeTableAdapter As iAS.SSSDBDataSetTableAdapters.tblGradeTableAdapter
    Friend WithEvents TblGradeBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents EmployeeGroup1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.EmployeeGroup1TableAdapter
    Friend WithEvents TblDESPANSARYBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblDESPANSARYTableAdapter As iAS.SSSDBDataSetTableAdapters.tblDESPANSARYTableAdapter
    Friend WithEvents TblBankBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblBankTableAdapter As iAS.SSSDBDataSetTableAdapters.tblBankTableAdapter
    Friend WithEvents TblbranchTableAdapter As iAS.SSSDBDataSetTableAdapters.tblbranchTableAdapter
    Friend WithEvents TblbranchBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblBank1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.tblBank1TableAdapter
    Friend WithEvents TblDESPANSARY1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.tblDESPANSARY1TableAdapter
    Friend WithEvents Tblbranch1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.tblbranch1TableAdapter
    Friend WithEvents TblMachineBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblMachineTableAdapter As iAS.SSSDBDataSetTableAdapters.tblMachineTableAdapter
    Friend WithEvents TblMachine1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.tblMachine1TableAdapter
    Friend WithEvents GridLookUpEditBr As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridView7 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LabelControl37 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GridLookUpEditGrd As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridView4 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colGradeCode As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colGradeName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridLookUpEditEmpGrp As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridView3 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colGroupId As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colGroupName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridLookUpEditCat As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colCAT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCATAGORYNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridLookUpEditDept As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colDEPARTMENTCODE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDEPARTMENTNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridLookUpEditCom As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridLookUpEdit1View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colCOMPANYCODE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCOMPANYNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents CheckCom As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckLoc As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckGrd As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEmpGrp As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckCat As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckDept As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents ComboBoxEditNepaliYear As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboBoxEditNEpaliMonth As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboBoxEditNepaliDate As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents DateEditJoin As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents CheckDOJ As DevExpress.XtraEditors.CheckEdit
End Class
