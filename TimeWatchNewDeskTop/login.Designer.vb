﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class login
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(login))
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEdit1 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEdit2 = New DevExpress.XtraEditors.TextEdit()
        Me.SimpleButton2 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton3 = New DevExpress.XtraEditors.SimpleButton()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.ProgressBarControl1 = New DevExpress.XtraEditors.ProgressBarControl()
        Me.LabelStatus = New DevExpress.XtraEditors.LabelControl()
        Me.CheckClient = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckServer = New DevExpress.XtraEditors.CheckEdit()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProgressBarControl1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckClient.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckServer.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.LabelControl1.Appearance.ForeColor = System.Drawing.Color.Black
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Appearance.Options.UseForeColor = True
        Me.LabelControl1.Location = New System.Drawing.Point(14, 119)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(49, 18)
        Me.LabelControl1.TabIndex = 0
        Me.LabelControl1.Text = "User Id"
        '
        'TextEdit1
        '
        Me.TextEdit1.Location = New System.Drawing.Point(103, 116)
        Me.TextEdit1.Name = "TextEdit1"
        Me.TextEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.TextEdit1.Properties.Appearance.Options.UseFont = True
        Me.TextEdit1.Properties.MaxLength = 20
        Me.TextEdit1.Size = New System.Drawing.Size(180, 24)
        Me.TextEdit1.TabIndex = 1
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.LabelControl2.Appearance.ForeColor = System.Drawing.Color.Black
        Me.LabelControl2.Appearance.Options.UseFont = True
        Me.LabelControl2.Appearance.Options.UseForeColor = True
        Me.LabelControl2.Location = New System.Drawing.Point(14, 149)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(61, 18)
        Me.LabelControl2.TabIndex = 2
        Me.LabelControl2.Text = "Password"
        '
        'TextEdit2
        '
        Me.TextEdit2.Location = New System.Drawing.Point(103, 146)
        Me.TextEdit2.Name = "TextEdit2"
        Me.TextEdit2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.TextEdit2.Properties.Appearance.Options.UseFont = True
        Me.TextEdit2.Properties.MaxLength = 20
        Me.TextEdit2.Properties.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.TextEdit2.Size = New System.Drawing.Size(180, 24)
        Me.TextEdit2.TabIndex = 2
        '
        'SimpleButton2
        '
        Me.SimpleButton2.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.SimpleButton2.Appearance.Options.UseFont = True
        Me.SimpleButton2.Location = New System.Drawing.Point(196, 204)
        Me.SimpleButton2.Name = "SimpleButton2"
        Me.SimpleButton2.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.[True]
        Me.SimpleButton2.Size = New System.Drawing.Size(87, 25)
        Me.SimpleButton2.TabIndex = 4
        Me.SimpleButton2.Text = "SimpleButton2"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.SimpleButton1.Appearance.Options.UseFont = True
        Me.SimpleButton1.Location = New System.Drawing.Point(103, 204)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.[False]
        Me.SimpleButton1.Size = New System.Drawing.Size(87, 25)
        Me.SimpleButton1.TabIndex = 3
        Me.SimpleButton1.Text = "SimpleButton1"
        '
        'SimpleButton3
        '
        Me.SimpleButton3.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.SimpleButton3.Appearance.Options.UseFont = True
        Me.SimpleButton3.Location = New System.Drawing.Point(103, 84)
        Me.SimpleButton3.Name = "SimpleButton3"
        Me.SimpleButton3.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.[False]
        Me.SimpleButton3.Size = New System.Drawing.Size(175, 25)
        Me.SimpleButton3.TabIndex = 5
        Me.SimpleButton3.Text = "SimpleButton3"
        Me.SimpleButton3.Visible = False
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(35, 12)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(266, 76)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox1.TabIndex = 6
        Me.PictureBox1.TabStop = False
        '
        'ProgressBarControl1
        '
        Me.ProgressBarControl1.Location = New System.Drawing.Point(31, 269)
        Me.ProgressBarControl1.Name = "ProgressBarControl1"
        Me.ProgressBarControl1.Properties.Step = 3
        Me.ProgressBarControl1.Size = New System.Drawing.Size(306, 18)
        Me.ProgressBarControl1.TabIndex = 7
        Me.ProgressBarControl1.Visible = False
        '
        'LabelStatus
        '
        Me.LabelStatus.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.LabelStatus.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.LabelStatus.Appearance.Options.UseFont = True
        Me.LabelStatus.Appearance.Options.UseForeColor = True
        Me.LabelStatus.Location = New System.Drawing.Point(37, 246)
        Me.LabelStatus.Name = "LabelStatus"
        Me.LabelStatus.Size = New System.Drawing.Size(12, 16)
        Me.LabelStatus.TabIndex = 8
        Me.LabelStatus.Text = "   "
        '
        'CheckClient
        '
        Me.CheckClient.Location = New System.Drawing.Point(207, 176)
        Me.CheckClient.Name = "CheckClient"
        Me.CheckClient.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckClient.Properties.Appearance.Options.UseFont = True
        Me.CheckClient.Properties.Caption = "Client"
        Me.CheckClient.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckClient.Properties.RadioGroupIndex = 0
        Me.CheckClient.Size = New System.Drawing.Size(71, 19)
        Me.CheckClient.TabIndex = 17
        Me.CheckClient.TabStop = False
        '
        'CheckServer
        '
        Me.CheckServer.EditValue = True
        Me.CheckServer.Location = New System.Drawing.Point(103, 176)
        Me.CheckServer.Name = "CheckServer"
        Me.CheckServer.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckServer.Properties.Appearance.Options.UseFont = True
        Me.CheckServer.Properties.Caption = "Server"
        Me.CheckServer.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckServer.Properties.RadioGroupIndex = 0
        Me.CheckServer.Size = New System.Drawing.Size(79, 19)
        Me.CheckServer.TabIndex = 16
        '
        'login
        '
        Me.AcceptButton = Me.SimpleButton1
        Me.Appearance.ForeColor = System.Drawing.Color.Black
        Me.Appearance.Options.UseFont = True
        Me.Appearance.Options.UseForeColor = True
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.BackgroundImageLayoutStore = System.Windows.Forms.ImageLayout.Stretch
        Me.BackgroundImageStore = CType(resources.GetObject("$this.BackgroundImageStore"), System.Drawing.Image)
        Me.ClientSize = New System.Drawing.Size(672, 307)
        Me.ControlBox = False
        Me.Controls.Add(Me.CheckClient)
        Me.Controls.Add(Me.CheckServer)
        Me.Controls.Add(Me.LabelStatus)
        Me.Controls.Add(Me.ProgressBarControl1)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.SimpleButton3)
        Me.Controls.Add(Me.SimpleButton1)
        Me.Controls.Add(Me.SimpleButton2)
        Me.Controls.Add(Me.TextEdit2)
        Me.Controls.Add(Me.LabelControl2)
        Me.Controls.Add(Me.TextEdit1)
        Me.Controls.Add(Me.LabelControl1)
        Me.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.LookAndFeel.SkinName = "iMaginary"
        Me.LookAndFeel.UseDefaultLookAndFeel = False
        Me.MaximizeBox = False
        Me.Name = "login"
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProgressBarControl1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckClient.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckServer.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEdit1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEdit2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents SimpleButton2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton3 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents LabelStatus As DevExpress.XtraEditors.LabelControl
    Friend WithEvents CheckClient As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckServer As DevExpress.XtraEditors.CheckEdit
    Friend Shared WithEvents ProgressBarControl1 As DevExpress.XtraEditors.ProgressBarControl
End Class
