﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraPieceEntry
    Inherits DevExpress.XtraEditors.XtraUserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(XtraPieceEntry))
        Me.SplitContainerControl1 = New DevExpress.XtraEditors.SplitContainerControl()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.mskPieceNo = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.mskPrate = New DevExpress.XtraEditors.TextEdit()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.ComboNepaliYearTo = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboNEpaliMonthTo = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboNepaliDateTo = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboNepaliYearFrm = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboNEpaliMonthFrm = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboNepaliDateFrm = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LookUpEdit1 = New DevExpress.XtraEditors.LookUpEdit()
        Me.TblEmployeeBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SSSDBDataSet = New iAS.SSSDBDataSet()
        Me.SidePanel1 = New DevExpress.XtraEditors.SidePanel()
        Me.GridControlPiece = New DevExpress.XtraGrid.GridControl()
        Me.GridViewPiece = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridLookUpEditPiece = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.GridLookUpEdit1View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.LabelControl22 = New DevExpress.XtraEditors.LabelControl()
        Me.lblGrade = New DevExpress.XtraEditors.LabelControl()
        Me.lblEmpGrp = New DevExpress.XtraEditors.LabelControl()
        Me.lblCat = New DevExpress.XtraEditors.LabelControl()
        Me.lblDept = New DevExpress.XtraEditors.LabelControl()
        Me.lblComp = New DevExpress.XtraEditors.LabelControl()
        Me.lblDesi = New DevExpress.XtraEditors.LabelControl()
        Me.lblCardNum = New DevExpress.XtraEditors.LabelControl()
        Me.lblName = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.DateEdit2 = New DevExpress.XtraEditors.DateEdit()
        Me.DateEditFrom = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEdit2 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.MemoEdit1 = New DevExpress.XtraEditors.MemoEdit()
        Me.TblLeaveMasterBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.LeaveApplicationBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.LeaveApplicationTableAdapter = New iAS.SSSDBDataSetTableAdapters.LeaveApplicationTableAdapter()
        Me.LeaveApplication1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.LeaveApplication1TableAdapter()
        Me.TblLeaveMasterTableAdapter = New iAS.SSSDBDataSetTableAdapters.tblLeaveMasterTableAdapter()
        Me.TblLeaveMaster1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.tblLeaveMaster1TableAdapter()
        Me.TblEmployeeTableAdapter = New iAS.SSSDBDataSetTableAdapters.TblEmployeeTableAdapter()
        Me.TblEmployee1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.TblEmployee1TableAdapter()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerControl1.SuspendLayout()
        CType(Me.mskPieceNo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.mskPrate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboNepaliYearTo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboNEpaliMonthTo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboNepaliDateTo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboNepaliYearFrm.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboNEpaliMonthFrm.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboNepaliDateFrm.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LookUpEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblEmployeeBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SidePanel1.SuspendLayout()
        CType(Me.GridControlPiece, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewPiece, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEditPiece.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEdit1View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit2.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEditFrom.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEditFrom.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblLeaveMasterBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LeaveApplicationBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SplitContainerControl1
        '
        Me.SplitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainerControl1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainerControl1.LookAndFeel.SkinName = "iMaginary"
        Me.SplitContainerControl1.LookAndFeel.UseDefaultLookAndFeel = False
        Me.SplitContainerControl1.Name = "SplitContainerControl1"
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.LabelControl13)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.mskPieceNo)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.LabelControl4)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.mskPrate)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.SimpleButton1)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.ComboNepaliYearTo)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.ComboNEpaliMonthTo)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.ComboNepaliDateTo)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.ComboNepaliYearFrm)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.ComboNEpaliMonthFrm)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.ComboNepaliDateFrm)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.LookUpEdit1)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.SidePanel1)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.GridLookUpEditPiece)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.LabelControl22)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.lblGrade)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.lblEmpGrp)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.lblCat)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.lblDept)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.lblComp)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.lblDesi)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.lblCardNum)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.lblName)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.LabelControl12)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.LabelControl11)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.LabelControl10)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.LabelControl9)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.LabelControl8)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.LabelControl7)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.LabelControl6)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.LabelControl5)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.DateEdit2)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.DateEditFrom)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.LabelControl3)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.LabelControl2)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.TextEdit2)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.LabelControl1)
        Me.SplitContainerControl1.Panel1.Text = "Panel1"
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.MemoEdit1)
        Me.SplitContainerControl1.Panel2.Text = "Panel2"
        Me.SplitContainerControl1.Size = New System.Drawing.Size(1145, 568)
        Me.SplitContainerControl1.SplitterPosition = 1036
        Me.SplitContainerControl1.TabIndex = 4
        Me.SplitContainerControl1.Text = "SplitContainerControl1"
        '
        'LabelControl13
        '
        Me.LabelControl13.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl13.Appearance.Options.UseFont = True
        Me.LabelControl13.Location = New System.Drawing.Point(293, 111)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(67, 14)
        Me.LabelControl13.TabIndex = 36
        Me.LabelControl13.Text = "No. of Piece"
        '
        'mskPieceNo
        '
        Me.mskPieceNo.Location = New System.Drawing.Point(416, 109)
        Me.mskPieceNo.Name = "mskPieceNo"
        Me.mskPieceNo.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.mskPieceNo.Properties.Appearance.Options.UseFont = True
        Me.mskPieceNo.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.mskPieceNo.Properties.Mask.EditMask = "[0-9]*"
        Me.mskPieceNo.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.mskPieceNo.Properties.MaxLength = 12
        Me.mskPieceNo.Size = New System.Drawing.Size(129, 20)
        Me.mskPieceNo.TabIndex = 35
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl4.Appearance.Options.UseFont = True
        Me.LabelControl4.Location = New System.Drawing.Point(293, 85)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(25, 14)
        Me.LabelControl4.TabIndex = 34
        Me.LabelControl4.Text = "Rate"
        '
        'mskPrate
        '
        Me.mskPrate.Enabled = False
        Me.mskPrate.Location = New System.Drawing.Point(416, 83)
        Me.mskPrate.Name = "mskPrate"
        Me.mskPrate.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.mskPrate.Properties.Appearance.Options.UseFont = True
        Me.mskPrate.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.mskPrate.Properties.Mask.EditMask = "###.##"
        Me.mskPrate.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.mskPrate.Properties.MaxLength = 12
        Me.mskPrate.Size = New System.Drawing.Size(129, 20)
        Me.mskPrate.TabIndex = 33
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButton1.Appearance.Options.UseFont = True
        Me.SimpleButton1.Enabled = False
        Me.SimpleButton1.Location = New System.Drawing.Point(416, 248)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton1.TabIndex = 14
        Me.SimpleButton1.Text = "Save"
        '
        'ComboNepaliYearTo
        '
        Me.ComboNepaliYearTo.Location = New System.Drawing.Point(724, 13)
        Me.ComboNepaliYearTo.Name = "ComboNepaliYearTo"
        Me.ComboNepaliYearTo.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliYearTo.Properties.Appearance.Options.UseFont = True
        Me.ComboNepaliYearTo.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliYearTo.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNepaliYearTo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNepaliYearTo.Properties.Items.AddRange(New Object() {"2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037", "2038", "2039", "2040", "2041", "2042", "2043", "2044", "2045", "2046", "2047", "2048", "2049", "2050", "2051", "2052", "2053", "2054", "2055", "2056", "2057", "2058", "2059", "2060", "2061", "2062", "2063", "2064", "2065", "2066", "2067", "2068", "2069", "2070", "2071", "2072", "2073", "2074", "2075", "2076", "2077", "2078", "2079", "2080", "2081", "2082", "2083", "2084", "2085", "2086", "2087", "2088", "2089"})
        Me.ComboNepaliYearTo.Size = New System.Drawing.Size(61, 20)
        Me.ComboNepaliYearTo.TabIndex = 7
        Me.ComboNepaliYearTo.Visible = False
        '
        'ComboNEpaliMonthTo
        '
        Me.ComboNEpaliMonthTo.Location = New System.Drawing.Point(652, 13)
        Me.ComboNEpaliMonthTo.Name = "ComboNEpaliMonthTo"
        Me.ComboNEpaliMonthTo.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNEpaliMonthTo.Properties.Appearance.Options.UseFont = True
        Me.ComboNEpaliMonthTo.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNEpaliMonthTo.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNEpaliMonthTo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNEpaliMonthTo.Properties.Items.AddRange(New Object() {"Baishakh", "Jestha", "Asar", "Shrawan", "Bhadau", "Aswin", "Kartik", "Mansir", "Poush", "Magh", "Falgun", "Chaitra"})
        Me.ComboNEpaliMonthTo.Size = New System.Drawing.Size(66, 20)
        Me.ComboNEpaliMonthTo.TabIndex = 6
        Me.ComboNEpaliMonthTo.Visible = False
        '
        'ComboNepaliDateTo
        '
        Me.ComboNepaliDateTo.Location = New System.Drawing.Point(604, 13)
        Me.ComboNepaliDateTo.Name = "ComboNepaliDateTo"
        Me.ComboNepaliDateTo.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliDateTo.Properties.Appearance.Options.UseFont = True
        Me.ComboNepaliDateTo.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliDateTo.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNepaliDateTo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNepaliDateTo.Properties.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32"})
        Me.ComboNepaliDateTo.Size = New System.Drawing.Size(42, 20)
        Me.ComboNepaliDateTo.TabIndex = 5
        Me.ComboNepaliDateTo.Visible = False
        '
        'ComboNepaliYearFrm
        '
        Me.ComboNepaliYearFrm.Location = New System.Drawing.Point(480, 13)
        Me.ComboNepaliYearFrm.Name = "ComboNepaliYearFrm"
        Me.ComboNepaliYearFrm.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliYearFrm.Properties.Appearance.Options.UseFont = True
        Me.ComboNepaliYearFrm.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliYearFrm.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNepaliYearFrm.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNepaliYearFrm.Properties.Items.AddRange(New Object() {"2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037", "2038", "2039", "2040", "2041", "2042", "2043", "2044", "2045", "2046", "2047", "2048", "2049", "2050", "2051", "2052", "2053", "2054", "2055", "2056", "2057", "2058", "2059", "2060", "2061", "2062", "2063", "2064", "2065", "2066", "2067", "2068", "2069", "2070", "2071", "2072", "2073", "2074", "2075", "2076", "2077", "2078", "2079", "2080", "2081", "2082", "2083", "2084", "2085", "2086", "2087", "2088", "2089"})
        Me.ComboNepaliYearFrm.Size = New System.Drawing.Size(61, 20)
        Me.ComboNepaliYearFrm.TabIndex = 4
        '
        'ComboNEpaliMonthFrm
        '
        Me.ComboNEpaliMonthFrm.Location = New System.Drawing.Point(408, 13)
        Me.ComboNEpaliMonthFrm.Name = "ComboNEpaliMonthFrm"
        Me.ComboNEpaliMonthFrm.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNEpaliMonthFrm.Properties.Appearance.Options.UseFont = True
        Me.ComboNEpaliMonthFrm.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNEpaliMonthFrm.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNEpaliMonthFrm.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNEpaliMonthFrm.Properties.Items.AddRange(New Object() {"Baishakh", "Jestha", "Asar", "Shrawan", "Bhadau", "Aswin", "Kartik", "Mansir", "Poush", "Magh", "Falgun", "Chaitra"})
        Me.ComboNEpaliMonthFrm.Size = New System.Drawing.Size(66, 20)
        Me.ComboNEpaliMonthFrm.TabIndex = 3
        '
        'ComboNepaliDateFrm
        '
        Me.ComboNepaliDateFrm.Location = New System.Drawing.Point(360, 13)
        Me.ComboNepaliDateFrm.Name = "ComboNepaliDateFrm"
        Me.ComboNepaliDateFrm.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliDateFrm.Properties.Appearance.Options.UseFont = True
        Me.ComboNepaliDateFrm.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliDateFrm.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNepaliDateFrm.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNepaliDateFrm.Properties.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32"})
        Me.ComboNepaliDateFrm.Size = New System.Drawing.Size(42, 20)
        Me.ComboNepaliDateFrm.TabIndex = 2
        '
        'LookUpEdit1
        '
        Me.LookUpEdit1.Location = New System.Drawing.Point(102, 14)
        Me.LookUpEdit1.Name = "LookUpEdit1"
        Me.LookUpEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LookUpEdit1.Properties.Appearance.Options.UseFont = True
        Me.LookUpEdit1.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LookUpEdit1.Properties.AppearanceDropDown.Options.UseFont = True
        Me.LookUpEdit1.Properties.AppearanceDropDownHeader.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LookUpEdit1.Properties.AppearanceDropDownHeader.Options.UseFont = True
        Me.LookUpEdit1.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LookUpEdit1.Properties.AppearanceFocused.Options.UseFont = True
        Me.LookUpEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.LookUpEdit1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.LookUpEdit1.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("PAYCODE", "PAYCODE", 30, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("EMPNAME", "Name")})
        Me.LookUpEdit1.Properties.DataSource = Me.TblEmployeeBindingSource
        Me.LookUpEdit1.Properties.DisplayMember = "PAYCODE"
        Me.LookUpEdit1.Properties.MaxLength = 12
        Me.LookUpEdit1.Properties.NullText = ""
        Me.LookUpEdit1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
        Me.LookUpEdit1.Properties.ValueMember = "PAYCODE"
        Me.LookUpEdit1.Size = New System.Drawing.Size(129, 20)
        Me.LookUpEdit1.TabIndex = 1
        '
        'TblEmployeeBindingSource
        '
        Me.TblEmployeeBindingSource.DataMember = "TblEmployee"
        Me.TblEmployeeBindingSource.DataSource = Me.SSSDBDataSet
        '
        'SSSDBDataSet
        '
        Me.SSSDBDataSet.DataSetName = "SSSDBDataSet"
        Me.SSSDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SidePanel1
        '
        Me.SidePanel1.Controls.Add(Me.GridControlPiece)
        Me.SidePanel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.SidePanel1.Location = New System.Drawing.Point(0, 328)
        Me.SidePanel1.Name = "SidePanel1"
        Me.SidePanel1.Size = New System.Drawing.Size(1036, 240)
        Me.SidePanel1.TabIndex = 32
        Me.SidePanel1.Text = "SidePanel1"
        '
        'GridControlPiece
        '
        Me.GridControlPiece.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControlPiece.EmbeddedNavigator.Buttons.Append.Visible = False
        Me.GridControlPiece.EmbeddedNavigator.Buttons.CancelEdit.Visible = False
        Me.GridControlPiece.EmbeddedNavigator.Buttons.Edit.Visible = False
        Me.GridControlPiece.EmbeddedNavigator.Buttons.EndEdit.Visible = False
        Me.GridControlPiece.Location = New System.Drawing.Point(0, 1)
        Me.GridControlPiece.MainView = Me.GridViewPiece
        Me.GridControlPiece.Name = "GridControlPiece"
        Me.GridControlPiece.Size = New System.Drawing.Size(1036, 239)
        Me.GridControlPiece.TabIndex = 0
        Me.GridControlPiece.UseEmbeddedNavigator = True
        Me.GridControlPiece.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewPiece})
        '
        'GridViewPiece
        '
        Me.GridViewPiece.GridControl = Me.GridControlPiece
        Me.GridViewPiece.Name = "GridViewPiece"
        Me.GridViewPiece.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewPiece.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[True]
        Me.GridViewPiece.OptionsBehavior.Editable = False
        Me.GridViewPiece.OptionsView.ShowGroupPanel = False
        '
        'GridLookUpEditPiece
        '
        Me.GridLookUpEditPiece.Location = New System.Drawing.Point(416, 57)
        Me.GridLookUpEditPiece.Name = "GridLookUpEditPiece"
        Me.GridLookUpEditPiece.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEditPiece.Properties.Appearance.Options.UseFont = True
        Me.GridLookUpEditPiece.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEditPiece.Properties.AppearanceDisabled.Options.UseFont = True
        Me.GridLookUpEditPiece.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEditPiece.Properties.AppearanceDropDown.Options.UseFont = True
        Me.GridLookUpEditPiece.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEditPiece.Properties.AppearanceFocused.Options.UseFont = True
        Me.GridLookUpEditPiece.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GridLookUpEditPiece.Properties.NullText = ""
        Me.GridLookUpEditPiece.Properties.View = Me.GridLookUpEdit1View
        Me.GridLookUpEditPiece.Size = New System.Drawing.Size(166, 20)
        Me.GridLookUpEditPiece.TabIndex = 8
        '
        'GridLookUpEdit1View
        '
        Me.GridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridLookUpEdit1View.Name = "GridLookUpEdit1View"
        Me.GridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridLookUpEdit1View.OptionsView.ShowGroupPanel = False
        '
        'LabelControl22
        '
        Me.LabelControl22.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl22.Appearance.Options.UseFont = True
        Me.LabelControl22.Location = New System.Drawing.Point(293, 60)
        Me.LabelControl22.Name = "LabelControl22"
        Me.LabelControl22.Size = New System.Drawing.Size(99, 14)
        Me.LabelControl22.TabIndex = 27
        Me.LabelControl22.Text = "Select Piece Type"
        '
        'lblGrade
        '
        Me.lblGrade.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lblGrade.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.lblGrade.Appearance.Options.UseFont = True
        Me.lblGrade.Appearance.Options.UseForeColor = True
        Me.lblGrade.Location = New System.Drawing.Point(127, 257)
        Me.lblGrade.Name = "lblGrade"
        Me.lblGrade.Size = New System.Drawing.Size(20, 14)
        Me.lblGrade.TabIndex = 26
        Me.lblGrade.Text = "     "
        '
        'lblEmpGrp
        '
        Me.lblEmpGrp.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lblEmpGrp.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.lblEmpGrp.Appearance.Options.UseFont = True
        Me.lblEmpGrp.Appearance.Options.UseForeColor = True
        Me.lblEmpGrp.Location = New System.Drawing.Point(127, 228)
        Me.lblEmpGrp.Name = "lblEmpGrp"
        Me.lblEmpGrp.Size = New System.Drawing.Size(20, 14)
        Me.lblEmpGrp.TabIndex = 25
        Me.lblEmpGrp.Text = "     "
        '
        'lblCat
        '
        Me.lblCat.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lblCat.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.lblCat.Appearance.Options.UseFont = True
        Me.lblCat.Appearance.Options.UseForeColor = True
        Me.lblCat.Location = New System.Drawing.Point(127, 199)
        Me.lblCat.Name = "lblCat"
        Me.lblCat.Size = New System.Drawing.Size(20, 14)
        Me.lblCat.TabIndex = 24
        Me.lblCat.Text = "     "
        '
        'lblDept
        '
        Me.lblDept.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lblDept.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.lblDept.Appearance.Options.UseFont = True
        Me.lblDept.Appearance.Options.UseForeColor = True
        Me.lblDept.Location = New System.Drawing.Point(127, 170)
        Me.lblDept.Name = "lblDept"
        Me.lblDept.Size = New System.Drawing.Size(24, 14)
        Me.lblDept.TabIndex = 23
        Me.lblDept.Text = "      "
        '
        'lblComp
        '
        Me.lblComp.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lblComp.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.lblComp.Appearance.Options.UseFont = True
        Me.lblComp.Appearance.Options.UseForeColor = True
        Me.lblComp.Location = New System.Drawing.Point(127, 142)
        Me.lblComp.Name = "lblComp"
        Me.lblComp.Size = New System.Drawing.Size(20, 14)
        Me.lblComp.TabIndex = 22
        Me.lblComp.Text = "     "
        '
        'lblDesi
        '
        Me.lblDesi.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lblDesi.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.lblDesi.Appearance.Options.UseFont = True
        Me.lblDesi.Appearance.Options.UseForeColor = True
        Me.lblDesi.Location = New System.Drawing.Point(127, 113)
        Me.lblDesi.Name = "lblDesi"
        Me.lblDesi.Size = New System.Drawing.Size(20, 14)
        Me.lblDesi.TabIndex = 21
        Me.lblDesi.Text = "     "
        '
        'lblCardNum
        '
        Me.lblCardNum.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lblCardNum.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.lblCardNum.Appearance.Options.UseFont = True
        Me.lblCardNum.Appearance.Options.UseForeColor = True
        Me.lblCardNum.Location = New System.Drawing.Point(127, 86)
        Me.lblCardNum.Name = "lblCardNum"
        Me.lblCardNum.Size = New System.Drawing.Size(24, 14)
        Me.lblCardNum.TabIndex = 20
        Me.lblCardNum.Text = "      "
        '
        'lblName
        '
        Me.lblName.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.lblName.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.lblName.Appearance.Options.UseFont = True
        Me.lblName.Appearance.Options.UseForeColor = True
        Me.lblName.Location = New System.Drawing.Point(127, 60)
        Me.lblName.Name = "lblName"
        Me.lblName.Size = New System.Drawing.Size(20, 14)
        Me.lblName.TabIndex = 19
        Me.lblName.Text = "     "
        '
        'LabelControl12
        '
        Me.LabelControl12.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl12.Appearance.Options.UseFont = True
        Me.LabelControl12.Location = New System.Drawing.Point(15, 257)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(32, 14)
        Me.LabelControl12.TabIndex = 17
        Me.LabelControl12.Text = "Grade"
        '
        'LabelControl11
        '
        Me.LabelControl11.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl11.Appearance.Options.UseFont = True
        Me.LabelControl11.Location = New System.Drawing.Point(15, 228)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(90, 14)
        Me.LabelControl11.TabIndex = 16
        Me.LabelControl11.Text = "Employee Group"
        '
        'LabelControl10
        '
        Me.LabelControl10.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl10.Appearance.Options.UseFont = True
        Me.LabelControl10.Location = New System.Drawing.Point(15, 199)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(48, 14)
        Me.LabelControl10.TabIndex = 15
        Me.LabelControl10.Text = "Catagory"
        '
        'LabelControl9
        '
        Me.LabelControl9.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl9.Appearance.Options.UseFont = True
        Me.LabelControl9.Location = New System.Drawing.Point(15, 170)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(66, 14)
        Me.LabelControl9.TabIndex = 14
        Me.LabelControl9.Text = "Department"
        '
        'LabelControl8
        '
        Me.LabelControl8.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl8.Appearance.Options.UseFont = True
        Me.LabelControl8.Location = New System.Drawing.Point(15, 142)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(50, 14)
        Me.LabelControl8.TabIndex = 13
        Me.LabelControl8.Text = "Company"
        '
        'LabelControl7
        '
        Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl7.Appearance.Options.UseFont = True
        Me.LabelControl7.Location = New System.Drawing.Point(15, 113)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(63, 14)
        Me.LabelControl7.TabIndex = 12
        Me.LabelControl7.Text = "Designation"
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl6.Appearance.Options.UseFont = True
        Me.LabelControl6.Location = New System.Drawing.Point(15, 86)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(47, 14)
        Me.LabelControl6.TabIndex = 11
        Me.LabelControl6.Text = "Card No."
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl5.Appearance.Options.UseFont = True
        Me.LabelControl5.Location = New System.Drawing.Point(15, 60)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(31, 14)
        Me.LabelControl5.TabIndex = 10
        Me.LabelControl5.Text = "Name"
        '
        'DateEdit2
        '
        Me.DateEdit2.EditValue = Nothing
        Me.DateEdit2.Location = New System.Drawing.Point(622, 13)
        Me.DateEdit2.Name = "DateEdit2"
        Me.DateEdit2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.DateEdit2.Properties.Appearance.Options.UseFont = True
        Me.DateEdit2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit2.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit2.Size = New System.Drawing.Size(112, 20)
        Me.DateEdit2.TabIndex = 3
        Me.DateEdit2.Visible = False
        '
        'DateEditFrom
        '
        Me.DateEditFrom.EditValue = Nothing
        Me.DateEditFrom.Location = New System.Drawing.Point(360, 13)
        Me.DateEditFrom.Name = "DateEditFrom"
        Me.DateEditFrom.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.DateEditFrom.Properties.Appearance.Options.UseFont = True
        Me.DateEditFrom.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEditFrom.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEditFrom.Size = New System.Drawing.Size(112, 20)
        Me.DateEditFrom.TabIndex = 2
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl3.Appearance.Options.UseFont = True
        Me.LabelControl3.Location = New System.Drawing.Point(553, 16)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(45, 14)
        Me.LabelControl3.TabIndex = 6
        Me.LabelControl3.Text = "To Date"
        Me.LabelControl3.Visible = False
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl2.Appearance.Options.UseFont = True
        Me.LabelControl2.Location = New System.Drawing.Point(288, 16)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(57, 14)
        Me.LabelControl2.TabIndex = 5
        Me.LabelControl2.Text = "From Date"
        '
        'TextEdit2
        '
        Me.TextEdit2.Location = New System.Drawing.Point(675, 54)
        Me.TextEdit2.Name = "TextEdit2"
        Me.TextEdit2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEdit2.Properties.Appearance.Options.UseFont = True
        Me.TextEdit2.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextEdit2.Properties.MaxLength = 12
        Me.TextEdit2.Size = New System.Drawing.Size(129, 20)
        Me.TextEdit2.TabIndex = 1
        Me.TextEdit2.Visible = False
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Location = New System.Drawing.Point(15, 16)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(46, 14)
        Me.LabelControl1.TabIndex = 0
        Me.LabelControl1.Text = "Paycode"
        '
        'MemoEdit1
        '
        Me.MemoEdit1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.MemoEdit1.EditValue = resources.GetString("MemoEdit1.EditValue")
        Me.MemoEdit1.Location = New System.Drawing.Point(0, 0)
        Me.MemoEdit1.Name = "MemoEdit1"
        Me.MemoEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.MemoEdit1.Properties.Appearance.Options.UseFont = True
        Me.MemoEdit1.Properties.MaxLength = 100000
        Me.MemoEdit1.Properties.ReadOnly = True
        Me.MemoEdit1.Size = New System.Drawing.Size(101, 568)
        Me.MemoEdit1.TabIndex = 1
        '
        'TblLeaveMasterBindingSource
        '
        Me.TblLeaveMasterBindingSource.DataMember = "tblLeaveMaster"
        Me.TblLeaveMasterBindingSource.DataSource = Me.SSSDBDataSet
        '
        'LeaveApplicationBindingSource
        '
        Me.LeaveApplicationBindingSource.DataMember = "LeaveApplication"
        Me.LeaveApplicationBindingSource.DataSource = Me.SSSDBDataSet
        '
        'LeaveApplicationTableAdapter
        '
        Me.LeaveApplicationTableAdapter.ClearBeforeFill = True
        '
        'LeaveApplication1TableAdapter1
        '
        Me.LeaveApplication1TableAdapter1.ClearBeforeFill = True
        '
        'TblLeaveMasterTableAdapter
        '
        Me.TblLeaveMasterTableAdapter.ClearBeforeFill = True
        '
        'TblLeaveMaster1TableAdapter1
        '
        Me.TblLeaveMaster1TableAdapter1.ClearBeforeFill = True
        '
        'TblEmployeeTableAdapter
        '
        Me.TblEmployeeTableAdapter.ClearBeforeFill = True
        '
        'TblEmployee1TableAdapter1
        '
        Me.TblEmployee1TableAdapter1.ClearBeforeFill = True
        '
        'XtraPieceEntry
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.SplitContainerControl1)
        Me.Name = "XtraPieceEntry"
        Me.Size = New System.Drawing.Size(1145, 568)
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerControl1.ResumeLayout(False)
        CType(Me.mskPieceNo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.mskPrate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboNepaliYearTo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboNEpaliMonthTo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboNepaliDateTo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboNepaliYearFrm.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboNEpaliMonthFrm.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboNepaliDateFrm.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LookUpEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblEmployeeBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SidePanel1.ResumeLayout(False)
        CType(Me.GridControlPiece, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewPiece, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEditPiece.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEdit1View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit2.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEditFrom.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEditFrom.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblLeaveMasterBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LeaveApplicationBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SplitContainerControl1 As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents MemoEdit1 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents lblGrade As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblEmpGrp As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblCat As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblDept As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblComp As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblDesi As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblCardNum As DevExpress.XtraEditors.LabelControl
    Friend WithEvents lblName As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents DateEdit2 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents DateEditFrom As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEdit2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl22 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GridLookUpEditPiece As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridLookUpEdit1View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents SidePanel1 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents LeaveApplicationBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SSSDBDataSet As iAS.SSSDBDataSet
    Friend WithEvents LeaveApplicationTableAdapter As iAS.SSSDBDataSetTableAdapters.LeaveApplicationTableAdapter
    Friend WithEvents LeaveApplication1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.LeaveApplication1TableAdapter
    Friend WithEvents GridControlPiece As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridViewPiece As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents TblLeaveMasterBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblLeaveMasterTableAdapter As iAS.SSSDBDataSetTableAdapters.tblLeaveMasterTableAdapter
    Friend WithEvents TblLeaveMaster1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.tblLeaveMaster1TableAdapter
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents TblEmployeeBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblEmployeeTableAdapter As iAS.SSSDBDataSetTableAdapters.TblEmployeeTableAdapter
    Friend WithEvents LookUpEdit1 As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents TblEmployee1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.TblEmployee1TableAdapter
    Friend WithEvents ComboNepaliYearTo As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboNEpaliMonthTo As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboNepaliDateTo As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboNepaliYearFrm As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboNEpaliMonthFrm As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboNepaliDateFrm As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents mskPrate As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents mskPieceNo As DevExpress.XtraEditors.TextEdit

End Class
