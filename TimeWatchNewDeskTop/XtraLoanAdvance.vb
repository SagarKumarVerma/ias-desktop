﻿Imports System.Resources
Imports System.Globalization
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports System.ComponentModel
Imports DevExpress.XtraGrid.Columns
Imports System.Data.SqlClient
Imports System.Text
Imports System.Data.OleDb
Imports DevExpress.Utils
Imports DevExpress.XtraGrid.Views.Base

Public Class XtraLoanAdvance
    Dim ulf As UserLookAndFeel
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    Dim leaveFlage As Boolean = True
    Public Shared IDNO As String, loanType As String, LAPaycode As String
    Public Sub New()
        InitializeComponent()
        If Common.servername = "Access" Then
            'Me.TblEmployee1TableAdapter1.Fill(Me.SSSDBDataSet.TblEmployee1)
            Me.TbladvancE1TableAdapter1.Fill(Me.SSSDBDataSet.TBLADVANCE1)
        Else
            TblEmployeeTableAdapter.Connection.ConnectionString = Common.ConnectionString
            'Me.TblEmployeeTableAdapter.Fill(Me.SSSDBDataSet.TblEmployee)
            TBLADVANCETableAdapter.Connection.ConnectionString = Common.ConnectionString
            'Me.TBLADVANCETableAdapter.Fill(Me.SSSDBDataSet.TBLADVANCE)
        End If
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True
        Common.SetGridFont(GridView1, New Font("Tahoma", 11))
    End Sub
    Private Sub XtraLoanAdvance_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Me.Width = Common.NavWidth 'Me.Parent.Width
        Me.Height = Common.NavHeight 'Me.Parent.Height
        'SplitContainerControl1.Width = Common.splitforMasterMenuWidth 'SplitContainerControl1.Parent.Width
        SplitContainerControl1.SplitterPosition = Common.SplitterPosition '(SplitContainerControl1.Parent.Width) * 85 / 100
        If Common.servername = "Access" Then
            'Me.TblEmployee1TableAdapter1.Fill(Me.SSSDBDataSet.TblEmployee1)
            'LookUpEdit1.Properties.DataSource = SSSDBDataSet.TblEmployee1

            Me.TbladvancE1TableAdapter1.Fill(Me.SSSDBDataSet.TBLADVANCE1)
            'GridControl1.DataSource = SSSDBDataSet.TBLADVANCE1
        Else
            TblEmployeeTableAdapter.Connection.ConnectionString = Common.ConnectionString
            'Me.TblEmployeeTableAdapter.Fill(Me.SSSDBDataSet.TblEmployee)
            'LookUpEdit1.Properties.DataSource = SSSDBDataSet.TblEmployee
            'Me.TBLADVANCETableAdapter.Fill(Me.SSSDBDataSet.TBLADVANCE)
            'GridControl1.DataSource = SSSDBDataSet.TBLADVANCE
        End If
        LookUpEdit1.Properties.DataSource = Common.EmpNonAdmin
        LonAdvIndxChange()
        setDefault()
    End Sub
    Private Sub LookUpEdit1_Leave(sender As System.Object, e As System.EventArgs) Handles LookUpEdit1.Leave
        If Not leaveFlage Then Return
        leaveFlage = False
        Dim adap, adap1 As SqlDataAdapter
        Dim adapA, adapA1 As OleDbDataAdapter
        Dim ds As DataSet = New DataSet
        Dim ds1 As DataSet
        Dim sSql As String = "select PAYCODE from TblEmployee where PAYCODE = '" & LookUpEdit1.EditValue.ToString.Trim & "'"
        If LookUpEdit1.EditValue.ToString.Trim <> "" Then
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
            If ds.Tables(0).Rows.Count = 0 Then
                If LookUpEdit1.EditValue.ToString.Trim <> "" Then
                    XtraMessageBox.Show(ulf, "<size=10>No such Employee</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    LookUpEdit1.Select()
                    setDefault()
                    leaveFlage = True
                    Exit Sub
                End If
            Else
                Dim sSql1 As String = "select EMPNAME, PRESENTCARDNO, (select GroupName from EmployeeGroup where EmployeeGroup.GroupId = TblEmployee.EmployeeGroupId) as EmpGrp, (select DEPARTMENTNAME from tblDepartment where tblDepartment.DEPARTMENTCODE = TblEmployee.DEPARTMENTCODE) as DEPName, (select CATAGORYNAME from tblCatagory WHERE tblCatagory.CAT = TblEmployee.CAT) as CATName, DESIGNATION  from TblEmployee where PAYCODE = '" & LookUpEdit1.EditValue.ToString.Trim & "'"
                sSql1 = "select EMPNAME, PRESENTCARDNO, " & _
                       "(SELECT COMPANYNAME from tblCompany WHERE tblCompany.COMPANYCODE = TblEmployee.COMPANYCODE) as CompName, " & _
                       "(select GroupName from EmployeeGroup where EmployeeGroup.GroupId = TblEmployee.EmployeeGroupId) as EmpGrp," & _
                       "(select DEPARTMENTNAME from tblDepartment where tblDepartment.DEPARTMENTCODE = TblEmployee.DEPARTMENTCODE) as DEPName, " & _
                       "(select CATAGORYNAME from tblCatagory WHERE tblCatagory.CAT = TblEmployee.CAT) as CATName, " & _
                       "(select GradeName from tblGrade WHERE tblGrade.GradeCode = TblEmployee.GradeCode) as GrdName, DESIGNATION  from TblEmployee where PAYCODE = '" & LookUpEdit1.EditValue.ToString.Trim & "'"
                If Common.servername = "Access" Then
                    sSql1 = "select T.EMPNAME, T.PRESENTCARDNO, C.COMPANYNAME  as CompName,  T.DESIGNATION, E.GroupName as EmpGrp, D.DEPARTMENTNAME as DEPName, cat.CATAGORYNAME as CATName, grd.GradeName as GrdName  from TblEmployee T,  tblCompany C, EmployeeGroup E, tblDepartment D, tblCatagory cat, tblGrade grd where C.COMPANYCODE = T.COMPANYCODE  and E.GroupId = T.EmployeeGroupId and D.DEPARTMENTCODE = T.DEPARTMENTCODE and cat.CAT = T.CAT and grd.GradeCode = T.GradeCode and T.Paycode='" & LookUpEdit1.EditValue.ToString.Trim & "'"
                    adapA1 = New OleDbDataAdapter(sSql1, Common.con1)
                    ds1 = New DataSet
                    adapA1.Fill(ds1)
                Else
                    adap1 = New SqlDataAdapter(sSql1, Common.con)
                    ds1 = New DataSet
                    adap1.Fill(ds1)
                End If
                lblName.Text = ds1.Tables(0).Rows(0).Item("EMPNAME").ToString
                lblCardNum.Text = ds1.Tables(0).Rows(0).Item("PRESENTCARDNO").ToString
                lblComp.Text = ds1.Tables(0).Rows(0).Item("CompName").ToString
                lblCat.Text = ds1.Tables(0).Rows(0).Item("CATName").ToString
                lblDept.Text = ds1.Tables(0).Rows(0).Item("DEPName").ToString
                lblEmpGrp.Text = ds1.Tables(0).Rows(0).Item("EmpGrp").ToString
                lblDesi.Text = ds1.Tables(0).Rows(0).Item("DESIGNATION").ToString
                lblGrade.Text = ds1.Tables(0).Rows(0).Item("GrdName").ToString
            End If
            ds = New DataSet
            sSql = "select max(IDNO) from TBLADVANCE where paycode='" & LookUpEdit1.EditValue.ToString.Trim & "'"
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
            'MsgBox(ds.Tables(0).Rows(0).Item(0))
            If ds.Tables(0).Rows.Count = 0 Or ds.Tables(0).Rows(0).Item(0).ToString = "" Then
                lblVoucher.Text = "2018001"
            Else
                Dim voucher_num As Integer = Convert.ToInt32(ds.Tables(0).Rows(0).Item(0).ToString) + 1
                lblVoucher.Text = (Convert.ToInt64(ds.Tables(0).Rows(0).Item(0).ToString.Trim) + 1)
            End If
        End If

        
        leaveFlage = True
        loadAdvanceGrid()
    End Sub
    Private Sub setDefault()
        LookUpEdit1.EditValue = ""
        lblCardNum.Text = ""
        lblCat.Text = ""
        lblComp.Text = ""
        lblDept.Text = ""
        lblDesi.Text = ""
        lblGrade.Text = ""
        lblName.Text = ""
        lblEmpGrp.Text = ""
        DateEditL.DateTime = Now
        DateEditT.DateTime = Now
        GridControl1.DataSource = Nothing

        If Common.IsNepali = "Y" Then
            ComboNepaliYearL.Visible = True
            ComboNEpaliMonthL.Visible = True
            ComboNepaliYearT.Visible = True
            ComboNEpaliMonthT.Visible = True
            DateEditL.Visible = False
            DateEditT.Visible = False

            Dim DC As New DateConverter()
            Dim Vstart As String = DC.ToBS(New Date(DateEditL.DateTime.Year, DateEditL.DateTime.Month, 1))
            Dim dojTmp() As String = Vstart.Split("-")
            ComboNepaliYearL.EditValue = dojTmp(0)
            ComboNEpaliMonthL.SelectedIndex = dojTmp(1) - 1
          

            Dim VEnd As String = DC.ToBS(New Date(DateEditT.DateTime.Year, DateEditT.DateTime.Month, DateEditT.DateTime.Day))
            dojTmp = VEnd.Split("-")
            ComboNepaliYearT.EditValue = dojTmp(0)
            ComboNEpaliMonthT.SelectedIndex = dojTmp(1) - 1           

        Else
            ComboNepaliYearL.Visible = False
            ComboNEpaliMonthL.Visible = False
            ComboNepaliYearT.Visible = False
            ComboNEpaliMonthT.Visible = False
            DateEditL.Visible = True
            DateEditT.Visible = True
        End If

    End Sub
    Private Sub LonAdvIndxChange()
        If CheckEditAdvance.Checked = True Then
            LabelControlLonMonth.Text = "Advance Month"
            LabelControlTotal.Text = "Total Advance"
            LabelControl2.Visible = False
            LabelControl14.Visible = False
            TextEditIntRate.Visible = False
            'If LookUpEdit1.EditValue <> "" Then
            '    loadAdvanceGrid()
            'End If
        Else
            LabelControlLonMonth.Text = "Loan Month"
            LabelControlTotal.Text = "Total Loan"
            LabelControl2.Visible = True
            LabelControl14.Visible = True
            TextEditIntRate.Visible = True
            'If LookUpEdit1.EditValue <> "" Then
            '    loadLoanGrid()
            'End If
        End If
    End Sub
    Private Sub SimpleButtonSave_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonSave.Click
        Dim LoanAdvText As String

        If Common.IsNepali = "Y" Then
            Dim DC As New DateConverter()
            Try
                'DateEditL.DateTime = DC.ToAD(New Date(ComboNepaliYearL.EditValue, ComboNEpaliMonthL.SelectedIndex + 1, 1))
                DateEditL.DateTime = DC.ToAD(ComboNepaliYearL.EditValue & "-" & ComboNEpaliMonthL.SelectedIndex + 1 & "-" & 1)
            Catch ex As Exception
                XtraMessageBox.Show(ulf, "<size=10>Invalid Date Of Joining</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                ComboNEpaliMonthL.Select()
                Exit Sub
            End Try
            Try
                'DateEditT.DateTime = DC.ToAD(New Date(ComboNepaliYearT.EditValue, ComboNEpaliMonthT.SelectedIndex + 1, 1))
                DateEditT.DateTime = DC.ToAD(ComboNepaliYearT.EditValue & "-" & ComboNEpaliMonthT.SelectedIndex + 1 & "-" & 1)
            Catch ex As Exception
                XtraMessageBox.Show(ulf, "<size=10>Invalid Date Of Birth</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                ComboNEpaliMonthT.Select()
                Exit Sub
            End Try
        End If

        If CheckEditLoan.Checked = True Then
            LoanAdvText = "Loan "
            If TextEditIntRate.Text.Trim = "" Then
                XtraMessageBox.Show(ulf, "<size=10>Intrest Rate cannot be Empty</size>", "<size=9>Error</size>")
                TextEditIntRate.Select()
                Exit Sub
            End If
            If TextEditIntRate.Text.Trim > 100 Then
                XtraMessageBox.Show(ulf, "<size=10>Intrest Rate cannot be greater than 100</size>", "<size=9>Error</size>")
                TextEditIntRate.Select()
                Exit Sub
            End If
        Else
            LoanAdvText = "Advance "
        End If
        If TextEditTotal.Text.Trim = "" Or TextEditTotal.Text.Trim = "0" Then
            XtraMessageBox.Show(ulf, "<size=10>" & LoanAdvText & "Amount cannot be empty</size>", "<size=9>Error</size>")
            TextEditTotal.Select()
            Exit Sub
        End If
        If TextEditNoInst.Text.Trim = "" Or TextEditNoInst.Text.Trim = "0" Then
            XtraMessageBox.Show(ulf, "<size=10>Number Of Installment cannot be empty</size>", "<size=9>Error</size>")
            TextEditNoInst.Select()
            Exit Sub
        End If


        Dim bAdvanceApply As Boolean
        Dim A_L As String
        If CheckEditLoan.Checked = True Then
            A_L = "L"
        Else
            A_L = "A"
        End If
        Dim INTREST_RATE As Double
        If TextEditIntRate.Text.Trim <> "" Then
            INTREST_RATE = Val(TextEditIntRate.Text.Trim)
        Else
            INTREST_RATE = 0
        End If
        Dim rsChkdata As DataSet = New DataSet 'ADODB.Recordset,
        Dim nIdno As Long, minstno As Integer
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim sSql As String = "select max(idno) as midno from tbladvance where   year(MON_YEAR)='" & DateEditL.DateTime.Year & "'" 'Paycode='" & LookUpEdit1.EditValue.ToString.Trim & "' and
        'rsChkdata = Cn.Execute(sSql)
        rsChkdata = New DataSet
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(rsChkdata)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(rsChkdata)
        End If
        If rsChkdata.Tables(0).Rows.Count = 1 Then
            If (rsChkdata.Tables(0).Rows(0).Item("midno").ToString.Trim = "") Then
                nIdno = DateEditL.DateTime.Year & "001" ' Val(DateEdit1.DateTime.Year + 1)
            Else
                nIdno = Convert.ToInt64(rsChkdata.Tables(0).Rows(0).Item("midno").ToString.Trim) + 1 'Val(DateEdit1.DateTime.Year + Format(Val(Mid(Trim(Str(rsChkdata.Tables(0).Rows(0).Item("midno"))), 5, 3)) + 1, "000"))
            End If
        End If
        If CheckEditAdvance.Checked = True Then
            TextEditInstAmt.Text = Math.Round(TextEditTotal.Text / TextEditNoInst.Text)
        Else
            If TextEditIntRate.Text <> "" And TextEditNoInst.Text <> "" And TextEditTotal.Text <> "" Then
                TextEditInstAmt.Text = Math.Round(Pmt((Val(TextEditIntRate.Text.Trim) / 1200), Val(TextEditNoInst.Text.Trim), -Val(TextEditTotal.Text.Trim)), 2)
            End If
        End If
        If CheckEditAdvance.Checked = True Then
            sSql = "Insert into TBLADVANCE(PAYCODE,IDNO,MON_YEAR,ENTRY_DATE,TRAN_MONTH,ADV_AMT,INST_AMT,INSTNO,A_L,INTREST_RATE)" & _
                   " Values('" & LookUpEdit1.EditValue.ToString.Trim & "'," & nIdno & ",'" & DateEditL.DateTime.ToString("yyyy-MM-01") & "','" & Now().ToString("yyyy-MM-dd") & "','" & DateEditT.DateTime.ToString("yyyy-MM-01") & "'," & _
                   Val(TextEditTotal.Text.Trim) & "," & Val(TextEditInstAmt.Text.Trim) & "," & Val(TextEditNoInst.Text.Trim) & ",'" & A_L & "',0)"
        Else
            sSql = "Insert into TBLADVANCE(PAYCODE,IDNO,MON_YEAR,ENTRY_DATE,TRAN_MONTH,ADV_AMT,INST_AMT,INSTNO,A_L,INTREST_RATE)" & _
                   " Values('" & LookUpEdit1.EditValue.ToString.Trim & "'," & nIdno & ",'" & DateEditL.DateTime.ToString("yyyy-MM-01") & "','" & Now().ToString("yyyy-MM-dd") & "','" & DateEditT.DateTime.ToString("yyyy-MM-01") & "'," & _
                   Val(TextEditTotal.Text.Trim) & "," & Val(TextEditInstAmt.Text.Trim) & "," & Val(TextEditNoInst.Text.Trim) & ",'" & A_L & "'," & INTREST_RATE & ")"
        End If
        If Common.servername = "Access" Then
            If Common.con1.State <> ConnectionState.Open Then
                Common.con1.Open()
            End If
            cmd1 = New OleDbCommand(sSql, Common.con1)
            cmd1.ExecuteNonQuery()
            If Common.con1.State <> ConnectionState.Closed Then
                Common.con1.Close()
            End If
        Else
            If Common.con.State <> ConnectionState.Open Then
                Common.con.Open()
            End If
            cmd = New SqlCommand(sSql, Common.con)
            cmd.ExecuteNonQuery()
            If Common.con.State <> ConnectionState.Closed Then
                Common.con.Close()
            End If
        End If
        'Cn.Execute(sSql)
        Dim iTotNoOfInstallment As Integer = Val(TextEditNoInst.Text.Trim)

        Dim iRestAmount As Double = Val(TextEditTotal.Text)
        Dim TempRestAmt As Double = Val(TextEditTotal.Text)
        Dim dInstStartMonth As DateTime = DateEditT.DateTime.ToString("yyyy-MM-dd") 'Format(dtpTmonth, "DD/MM/YYYY")
        minstno = iTotNoOfInstallment
        For i = 1 To iTotNoOfInstallment
            If CheckEditAdvance.Checked = True Then
                iRestAmount = iRestAmount - (TextEditInstAmt.Text)
                If TempRestAmt < Val(TextEditInstAmt.Text.Trim) Then
                    TextEditInstAmt.Text = Format(TempRestAmt, "000000.00")
                    iRestAmount = 0
                End If
            Else
                iRestAmount = Math.Round(iRestAmount - (TextEditInstAmt.Text - Math.Round(IPmt((INTREST_RATE / 1200), i, Val(TextEditNoInst.Text.Trim), -Val(TextEditTotal.Text.Trim), 0), 2)), 2)
                If iRestAmount < Math.Round(IPmt((INTREST_RATE / 1200), i, Val(TextEditNoInst.Text.Trim), -Val(TextEditTotal.Text.Trim), 0), 2) Then
                    iRestAmount = 0
                End If
            End If

            'sMonth = Mid(dInstStartMonth, (InStr(1, CStr(dInstStartMonth), "/") + 1), 2)
            'sYear = Right(dInstStartMonth, 4)

            If CheckEditAdvance.Checked = True Then
                sSql = "Insert into TBLADVANCEDATA Values('" & LookUpEdit1.EditValue.ToString.Trim & "','" & A_L & "'," & nIdno & _
                        ",'" & dInstStartMonth.ToString("yyyy-MM-01") & _
                        "'," & TextEditInstAmt.Text.Trim & ",0," & iRestAmount & ",0,0," & minstno & ")"
            Else
                sSql = "Insert into TBLADVANCEDATA Values('" & LookUpEdit1.EditValue.ToString.Trim & "','" & A_L & "'," & nIdno & _
                        ",'" & dInstStartMonth.ToString("yyyy-MM-01") & _
                        "'," & TextEditInstAmt.Text.Trim & ",0," & iRestAmount & "," & (TextEditInstAmt.Text.Trim - Math.Round(IPmt((INTREST_RATE / 1200), i, Val(TextEditNoInst.Text.Trim), -Val(TextEditTotal.Text.Trim), 0), 2)) & "," & Math.Round(IPmt((INTREST_RATE / 1200), i, Val(TextEditNoInst.Text.Trim), -Val(TextEditTotal.Text.Trim), 0), 2) & "," & minstno & ")"
            End If
            If Common.servername = "Access" Then
                If Common.con1.State <> ConnectionState.Open Then
                    Common.con1.Open()
                End If
                cmd1 = New OleDbCommand(sSql, Common.con1)
                cmd1.ExecuteNonQuery()
                If Common.con1.State <> ConnectionState.Closed Then
                    Common.con1.Close()
                End If
            Else
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
            'Cn.Execute(sSql)
            TempRestAmt = iRestAmount
            dInstStartMonth = DateEditT.DateTime.AddMonths(i)
            bAdvanceApply = True
            minstno = minstno - 1
        Next i
        XtraMessageBox.Show(ulf, "<size=10>Saved Successfully</size>", "<size=9>Success</size>")
        loadAdvanceGrid()
    End Sub
    Private Sub CheckEditAdvance_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckEditAdvance.CheckedChanged
        LonAdvIndxChange()
    End Sub
    Private Sub CheckEditLoan_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckEditLoan.CheckedChanged
        LonAdvIndxChange()
    End Sub
    Private Sub TextEditNoInst_Leave(sender As System.Object, e As System.EventArgs) Handles TextEditNoInst.Leave
        'Dim instno As Integer
        If TextEditTotal.Text <> "" And TextEditNoInst.Text <> "0" And TextEditTotal.Text <> "0" And TextEditNoInst.Text <> "" Then
            If CheckEditAdvance.Checked = True Then
                If TextEditNoInst.Text.Trim <> "" And TextEditTotal.Text.Trim <> "" Then
                    TextEditInstAmt.Text = Math.Round(TextEditTotal.Text / TextEditNoInst.Text, 0)
                End If
            Else
                If TextEditIntRate.Text.Trim <> "" And TextEditNoInst.Text.Trim <> "" And TextEditTotal.Text <> "" Then
                    TextEditInstAmt.Text = Math.Round(Pmt((Val(TextEditIntRate.Text) / 1200), Val(TextEditNoInst.Text), -Val(TextEditTotal.Text)), 2)
                End If
            End If
        Else
            TextEditInstAmt.Text = ""
        End If
    End Sub
    Private Sub TextEditIntRate_Leave(sender As System.Object, e As System.EventArgs) Handles TextEditIntRate.Leave
        If TextEditIntRate.Text.Trim <> "" And TextEditIntRate.Text.Trim <> "." And Val(TextEditIntRate.Text.Trim) <> 0 Then
            If TextEditIntRate.Text.Trim > 100 Then
                XtraMessageBox.Show(ulf, "<size=10>Intrest Rate cannot be greater than 100</size>", "<size=9>Error</size>")
                TextEditIntRate.Select()
            End If
        End If
    End Sub
    Private Sub loadAdvanceGrid()
        GridControl1.DataSource = Nothing
        Dim gridselet As String = "select * from TBLADVANCE where PAYCODE = '" & LookUpEdit1.EditValue.ToString.Trim & "'"
        If Common.servername = "Access" Then
            Dim dataAdapter As New OleDbDataAdapter(gridselet, Common.con1)
            Dim WTDataTable As New DataTable("TBLADVANCE")
            dataAdapter.Fill(WTDataTable)
            GridControl1.DataSource = WTDataTable
        Else
            Dim dataAdapter As New SqlClient.SqlDataAdapter(gridselet, Common.con)
            Dim WTDataTable As New DataTable("TBLADVANCE")
            dataAdapter.Fill(WTDataTable)
            GridControl1.DataSource = WTDataTable
        End If
    End Sub
    Private Sub GridView1_CustomColumnDisplayText(sender As System.Object, e As DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs) Handles GridView1.CustomColumnDisplayText
        Try
            Dim view As ColumnView = TryCast(sender, ColumnView)
            If e.Column.FieldName = "A_L" Then
                Dim coll As String = view.GetListSourceRowCellValue(e.ListSourceRowIndex, "A_L")
                If coll = "L" Then
                    e.DisplayText = "Loan"
                ElseIf coll = "A" Then
                    e.DisplayText = "Advance"
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub
    Private Sub GridView1_EditFormShowing(sender As System.Object, e As DevExpress.XtraGrid.Views.Grid.EditFormShowingEventArgs) Handles GridView1.EditFormShowing
        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
        IDNO = row("IDNO").ToString.Trim
        loanType = row("A_L").ToString.Trim
        LAPaycode = LookUpEdit1.EditValue.ToString.Trim
        e.Allow = False
        XtraLoanAdvanceGrid.ShowDialog()
    End Sub
    Private Sub GridControl1_EmbeddedNavigator_ButtonClick(sender As System.Object, e As DevExpress.XtraEditors.NavigatorButtonClickEventArgs) Handles GridControl1.EmbeddedNavigator.ButtonClick
        If e.Button.ButtonType = DevExpress.XtraEditors.NavigatorButtonType.Remove Then
            If XtraMessageBox.Show(ulf, "<size=10>" & Common.res_man.GetString("askdelete", Common.cul) & "</size>", Common.res_man.GetString("confirmdeleteion", Common.cul), _
                              MessageBoxButtons.YesNo, MessageBoxIcon.Question) <> DialogResult.Yes Then
                Me.Validate()
                e.Handled = True
                'MsgBox("Your records have been saved and updated successfully!")
            Else
                e.Handled = True
                Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
                Dim CellId As String = row("IDNO").ToString.Trim

                Dim cmd As New SqlCommand
                Dim cmd1 As New OleDbCommand
                Dim sSql As String = "delete from TBLADVANCEDATA where IDNO = " & CellId
                Dim sSql1 As String = "delete from TBLADVANCE where IDNO = " & CellId
                If Common.servername = "Access" Then
                    If Common.con1.State <> ConnectionState.Open Then
                        Common.con1.Open()
                    End If
                    cmd1 = New OleDbCommand(sSql, Common.con1)
                    cmd1.ExecuteNonQuery()
                    cmd1 = New OleDbCommand(sSql1, Common.con1)
                    cmd1.ExecuteNonQuery()
                    Common.con1.Close()
                Else
                    If Common.con.State <> ConnectionState.Open Then
                        Common.con.Open()
                    End If

                    cmd = New SqlCommand(sSql, Common.con)
                    cmd.ExecuteNonQuery()
                    cmd = New SqlCommand(sSql1, Common.con)
                    cmd.ExecuteNonQuery()
                    Common.con.Close()
                End If
                loadAdvanceGrid()
            End If
        End If
    End Sub
End Class
