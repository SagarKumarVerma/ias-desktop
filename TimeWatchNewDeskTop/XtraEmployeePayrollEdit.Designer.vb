﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class S
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(S))
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.LookUpEdit1 = New DevExpress.XtraEditors.LookUpEdit()
        Me.TblEmployeeBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SSSDBDataSet = New iAS.SSSDBDataSet()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditCardNo = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.DateEditEffFrm = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControlName = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditGross = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControlDept = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.SimpleButtonCal = New DevExpress.XtraEditors.SimpleButton()
        Me.TextEditPf1 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEditPf = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditESI = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditPAN = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl33 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.ComboBoxEditPayBy = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboBoxEditEmpType = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.GridLookUpEditBank = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.TblBankBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GridView6 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colBANKCODE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBANKNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.LabelControl24 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditAccNo = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl25 = New DevExpress.XtraEditors.LabelControl()
        Me.ComboBoxEditDA = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboBoxEditConvence = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboBoxEditMedical = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboBoxEditHRA = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.TextEditOT = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl()
        Me.ComboBoxEditTDS = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.TextEditTDS = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl18 = New DevExpress.XtraEditors.LabelControl()
        Me.GroupControlAllow = New DevExpress.XtraEditors.GroupControl()
        Me.CheckEditBonus = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditVPF = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditGraduty = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditESI = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditUnderLim = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditPF = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditProfTax = New DevExpress.XtraEditors.CheckEdit()
        Me.GridLookUpEditFormula = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.PAYFORMULABindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.TblEmployeeTableAdapter = New iAS.SSSDBDataSetTableAdapters.TblEmployeeTableAdapter()
        Me.TblEmployee1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.TblEmployee1TableAdapter()
        Me.TblBankTableAdapter = New iAS.SSSDBDataSetTableAdapters.tblBankTableAdapter()
        Me.TblBank1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.tblBank1TableAdapter()
        Me.PAY_FORMULATableAdapter = New iAS.SSSDBDataSetTableAdapters.PAY_FORMULATableAdapter()
        Me.PaY_FORMULA1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.PAY_FORMULA1TableAdapter()
        Me.SidePanel1 = New DevExpress.XtraEditors.SidePanel()
        Me.ComboNepaliYear = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboNEpaliMonth = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.TextEditPaycode = New DevExpress.XtraEditors.TextEdit()
        Me.TextEditBasic = New DevExpress.XtraEditors.TextEdit()
        Me.TextEditHRA = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditMedical = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditConvence = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditDA = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.SidePanel3 = New DevExpress.XtraEditors.SidePanel()
        Me.XtraTabControl2 = New DevExpress.XtraTab.XtraTabControl()
        Me.XtraTabPage3 = New DevExpress.XtraTab.XtraTabPage()
        Me.PictureEdit15 = New DevExpress.XtraEditors.PictureEdit()
        Me.PictureEdit14 = New DevExpress.XtraEditors.PictureEdit()
        Me.PictureEdit13 = New DevExpress.XtraEditors.PictureEdit()
        Me.PictureEdit12 = New DevExpress.XtraEditors.PictureEdit()
        Me.PictureEdit11 = New DevExpress.XtraEditors.PictureEdit()
        Me.LabelControl38 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditRateE5 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEditDescpE5 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl39 = New DevExpress.XtraEditors.LabelControl()
        Me.GridLookUpEditFormulaE5 = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.GridView5 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn9 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn10 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.TextEditRateE4 = New DevExpress.XtraEditors.TextEdit()
        Me.GridLookUpEditFormulaE4 = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.GridView4 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn8 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.TextEditDescpE4 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl40 = New DevExpress.XtraEditors.LabelControl()
        Me.GridLookUpEditFormulaE3 = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.GridView3 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridLookUpEditFormulaE2 = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.GridView11 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.TextEditRateE3 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEditDescpE3 = New DevExpress.XtraEditors.TextEdit()
        Me.GridLookUpEditFormulaE1 = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.GridView12 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn21 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn22 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.LabelControl41 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditRateE2 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEditDescpE2 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl42 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl43 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl44 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditRateE1 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEditDescpE1 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl45 = New DevExpress.XtraEditors.LabelControl()
        Me.XtraTabPage4 = New DevExpress.XtraTab.XtraTabPage()
        Me.GridLookUpEditFormulaE10 = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.GridView10 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn19 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn20 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.PictureEdit20 = New DevExpress.XtraEditors.PictureEdit()
        Me.PictureEdit19 = New DevExpress.XtraEditors.PictureEdit()
        Me.GridLookUpEditFormulaE9 = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.GridView9 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn17 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn18 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.PictureEdit18 = New DevExpress.XtraEditors.PictureEdit()
        Me.PictureEdit17 = New DevExpress.XtraEditors.PictureEdit()
        Me.GridLookUpEditFormulaE8 = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.GridView8 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn15 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn16 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.PictureEdit16 = New DevExpress.XtraEditors.PictureEdit()
        Me.LabelControl46 = New DevExpress.XtraEditors.LabelControl()
        Me.GridLookUpEditFormulaE7 = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.GridView7 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn13 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn14 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.TextEditRateE10 = New DevExpress.XtraEditors.TextEdit()
        Me.GridLookUpEditFormulaE6 = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn11 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn12 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.TextEditDescpE10 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl47 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditRateE9 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEditDescpE9 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl48 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditRateE8 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEditDescpE8 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl49 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditRateE7 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEditDescpE7 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl50 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl51 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl52 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditRateE6 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEditDescpE6 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl53 = New DevExpress.XtraEditors.LabelControl()
        Me.SidePanel4 = New DevExpress.XtraEditors.SidePanel()
        Me.SimpleButton2 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButtonSave = New DevExpress.XtraEditors.SimpleButton()
        Me.XtraTabControl1 = New DevExpress.XtraTab.XtraTabControl()
        Me.XtraTabPage1 = New DevExpress.XtraTab.XtraTabPage()
        Me.PictureEdit5 = New DevExpress.XtraEditors.PictureEdit()
        Me.PictureEdit4 = New DevExpress.XtraEditors.PictureEdit()
        Me.PictureEdit3 = New DevExpress.XtraEditors.PictureEdit()
        Me.PictureEdit2 = New DevExpress.XtraEditors.PictureEdit()
        Me.PictureEdit1 = New DevExpress.XtraEditors.PictureEdit()
        Me.LabelControl28 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditRate5 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEditDescp5 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl27 = New DevExpress.XtraEditors.LabelControl()
        Me.GridLookUpEditFormulaD5 = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.GridView18 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn33 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn34 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.TextEditRate4 = New DevExpress.XtraEditors.TextEdit()
        Me.GridLookUpEditFormulaD4 = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.GridView19 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn35 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn36 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.TextEditDescp4 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl26 = New DevExpress.XtraEditors.LabelControl()
        Me.GridLookUpEditFormulaD3 = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.GridView20 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn37 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn38 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.TextEditRate3 = New DevExpress.XtraEditors.TextEdit()
        Me.GridLookUpEditFormulaD2 = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.GridView21 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn39 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn40 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.TextEditDescp3 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl23 = New DevExpress.XtraEditors.LabelControl()
        Me.GridLookUpEditFormulaD1 = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.GridView22 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn41 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn42 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.TextEditRate2 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEditDescp2 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl22 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl21 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl20 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditRate1 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEditDescp1 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl19 = New DevExpress.XtraEditors.LabelControl()
        Me.XtraTabPage2 = New DevExpress.XtraTab.XtraTabPage()
        Me.GridLookUpEditFormulaD10 = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.GridView13 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn23 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn24 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.PictureEdit10 = New DevExpress.XtraEditors.PictureEdit()
        Me.PictureEdit9 = New DevExpress.XtraEditors.PictureEdit()
        Me.GridLookUpEditFormulaD9 = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.GridView14 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn25 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn26 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.PictureEdit8 = New DevExpress.XtraEditors.PictureEdit()
        Me.PictureEdit7 = New DevExpress.XtraEditors.PictureEdit()
        Me.GridLookUpEditFormulaD8 = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.GridView15 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn27 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn28 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.PictureEdit6 = New DevExpress.XtraEditors.PictureEdit()
        Me.LabelControl29 = New DevExpress.XtraEditors.LabelControl()
        Me.GridLookUpEditFormulaD7 = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.GridView16 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn29 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn30 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.TextEditRate10 = New DevExpress.XtraEditors.TextEdit()
        Me.GridLookUpEditFormulaD6 = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.GridView17 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn31 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn32 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.TextEditDescp10 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl30 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditRate9 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEditDescp9 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl31 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditRate8 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEditDescp8 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl32 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditRate7 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEditDescp7 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl34 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl35 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl36 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditRate6 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEditDescp6 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl37 = New DevExpress.XtraEditors.LabelControl()
        Me.SidePanel2 = New DevExpress.XtraEditors.SidePanel()
        CType(Me.LookUpEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblEmployeeBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditCardNo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEditEffFrm.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEditEffFrm.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditGross.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditPf1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditPf.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditESI.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditPAN.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEditPayBy.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEditEmpType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEditBank.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblBankBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditAccNo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEditDA.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEditConvence.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEditMedical.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEditHRA.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditOT.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEditTDS.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditTDS.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControlAllow, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControlAllow.SuspendLayout()
        CType(Me.CheckEditBonus.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditVPF.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditGraduty.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditESI.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditUnderLim.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditPF.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditProfTax.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEditFormula.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PAYFORMULABindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SidePanel1.SuspendLayout()
        CType(Me.ComboNepaliYear.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboNEpaliMonth.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditPaycode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditBasic.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditHRA.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditMedical.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditConvence.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditDA.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SidePanel3.SuspendLayout()
        CType(Me.XtraTabControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabControl2.SuspendLayout()
        Me.XtraTabPage3.SuspendLayout()
        CType(Me.PictureEdit15.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureEdit14.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureEdit13.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureEdit12.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureEdit11.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditRateE5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditDescpE5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEditFormulaE5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditRateE4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEditFormulaE4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditDescpE4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEditFormulaE3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEditFormulaE2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditRateE3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditDescpE3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEditFormulaE1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView12, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditRateE2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditDescpE2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditRateE1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditDescpE1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage4.SuspendLayout()
        CType(Me.GridLookUpEditFormulaE10.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureEdit20.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureEdit19.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEditFormulaE9.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureEdit18.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureEdit17.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEditFormulaE8.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureEdit16.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEditFormulaE7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditRateE10.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEditFormulaE6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditDescpE10.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditRateE9.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditDescpE9.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditRateE8.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditDescpE8.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditRateE7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditDescpE7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditRateE6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditDescpE6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SidePanel4.SuspendLayout()
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabControl1.SuspendLayout()
        Me.XtraTabPage1.SuspendLayout()
        CType(Me.PictureEdit5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureEdit4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureEdit3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditRate5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditDescp5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEditFormulaD5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditRate4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEditFormulaD4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView19, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditDescp4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEditFormulaD3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView20, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditRate3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEditFormulaD2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditDescp3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEditFormulaD1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView22, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditRate2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditDescp2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditRate1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditDescp1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.XtraTabPage2.SuspendLayout()
        CType(Me.GridLookUpEditFormulaD10.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView13, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureEdit10.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureEdit9.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEditFormulaD9.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView14, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureEdit8.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureEdit7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEditFormulaD8.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView15, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureEdit6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEditFormulaD7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView16, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditRate10.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEditFormulaD6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView17, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditDescp10.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditRate9.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditDescp9.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditRate8.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditDescp8.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditRate7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditDescp7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditRate6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditDescp6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SidePanel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Location = New System.Drawing.Point(11, 6)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(46, 14)
        Me.LabelControl1.TabIndex = 1
        Me.LabelControl1.Text = "Paycode"
        '
        'LookUpEdit1
        '
        Me.LookUpEdit1.Location = New System.Drawing.Point(785, 54)
        Me.LookUpEdit1.Name = "LookUpEdit1"
        Me.LookUpEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LookUpEdit1.Properties.Appearance.Options.UseFont = True
        Me.LookUpEdit1.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LookUpEdit1.Properties.AppearanceDropDown.Options.UseFont = True
        Me.LookUpEdit1.Properties.AppearanceDropDownHeader.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LookUpEdit1.Properties.AppearanceDropDownHeader.Options.UseFont = True
        Me.LookUpEdit1.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LookUpEdit1.Properties.AppearanceFocused.Options.UseFont = True
        Me.LookUpEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.LookUpEdit1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.LookUpEdit1.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("PAYCODE", "PAYCODE", 30, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near, DevExpress.Data.ColumnSortOrder.Ascending), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("EMPNAME", "Name")})
        Me.LookUpEdit1.Properties.DataSource = Me.TblEmployeeBindingSource
        Me.LookUpEdit1.Properties.DisplayMember = "PAYCODE"
        Me.LookUpEdit1.Properties.MaxLength = 12
        Me.LookUpEdit1.Properties.NullText = ""
        Me.LookUpEdit1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
        Me.LookUpEdit1.Properties.ValueMember = "PAYCODE"
        Me.LookUpEdit1.Size = New System.Drawing.Size(129, 20)
        Me.LookUpEdit1.TabIndex = 2
        Me.LookUpEdit1.Visible = False
        '
        'TblEmployeeBindingSource
        '
        Me.TblEmployeeBindingSource.DataMember = "TblEmployee"
        Me.TblEmployeeBindingSource.DataSource = Me.SSSDBDataSet
        '
        'SSSDBDataSet
        '
        Me.SSSDBDataSet.DataSetName = "SSSDBDataSet"
        Me.SSSDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl2.Appearance.Options.UseFont = True
        Me.LabelControl2.Location = New System.Drawing.Point(274, 6)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(47, 14)
        Me.LabelControl2.TabIndex = 3
        Me.LabelControl2.Text = "Card No."
        '
        'TextEditCardNo
        '
        Me.TextEditCardNo.Location = New System.Drawing.Point(359, 6)
        Me.TextEditCardNo.Name = "TextEditCardNo"
        Me.TextEditCardNo.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditCardNo.Properties.Appearance.Options.UseFont = True
        Me.TextEditCardNo.Properties.MaxLength = 12
        Me.TextEditCardNo.Properties.ReadOnly = True
        Me.TextEditCardNo.Size = New System.Drawing.Size(127, 20)
        Me.TextEditCardNo.TabIndex = 4
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl3.Appearance.Options.UseFont = True
        Me.LabelControl3.Location = New System.Drawing.Point(11, 32)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(78, 14)
        Me.LabelControl3.TabIndex = 5
        Me.LabelControl3.Text = "Effected From"
        '
        'DateEditEffFrm
        '
        Me.DateEditEffFrm.EditValue = Nothing
        Me.DateEditEffFrm.Location = New System.Drawing.Point(106, 29)
        Me.DateEditEffFrm.Name = "DateEditEffFrm"
        Me.DateEditEffFrm.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.DateEditEffFrm.Properties.Appearance.Options.UseFont = True
        Me.DateEditEffFrm.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEditEffFrm.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEditEffFrm.Properties.Mask.EditMask = "MM/yyyy"
        Me.DateEditEffFrm.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.DateEditEffFrm.Size = New System.Drawing.Size(129, 20)
        Me.DateEditEffFrm.TabIndex = 6
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl4.Appearance.Options.UseFont = True
        Me.LabelControl4.Location = New System.Drawing.Point(274, 33)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(77, 14)
        Me.LabelControl4.TabIndex = 7
        Me.LabelControl4.Text = "Gross Amount"
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl5.Appearance.Options.UseFont = True
        Me.LabelControl5.Location = New System.Drawing.Point(577, 9)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(31, 14)
        Me.LabelControl5.TabIndex = 8
        Me.LabelControl5.Text = "Name"
        '
        'LabelControlName
        '
        Me.LabelControlName.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControlName.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.LabelControlName.Appearance.Options.UseFont = True
        Me.LabelControlName.Appearance.Options.UseForeColor = True
        Me.LabelControlName.Location = New System.Drawing.Point(660, 9)
        Me.LabelControlName.Name = "LabelControlName"
        Me.LabelControlName.Size = New System.Drawing.Size(31, 14)
        Me.LabelControlName.TabIndex = 9
        Me.LabelControlName.Text = "Name"
        '
        'TextEditGross
        '
        Me.TextEditGross.Location = New System.Drawing.Point(359, 32)
        Me.TextEditGross.Name = "TextEditGross"
        Me.TextEditGross.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditGross.Properties.Appearance.Options.UseFont = True
        Me.TextEditGross.Properties.Mask.EditMask = "########.##"
        Me.TextEditGross.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEditGross.Properties.MaxLength = 8
        Me.TextEditGross.Size = New System.Drawing.Size(127, 20)
        Me.TextEditGross.TabIndex = 10
        '
        'LabelControlDept
        '
        Me.LabelControlDept.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControlDept.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.LabelControlDept.Appearance.Options.UseFont = True
        Me.LabelControlDept.Appearance.Options.UseForeColor = True
        Me.LabelControlDept.Location = New System.Drawing.Point(660, 35)
        Me.LabelControlDept.Name = "LabelControlDept"
        Me.LabelControlDept.Size = New System.Drawing.Size(66, 14)
        Me.LabelControlDept.TabIndex = 12
        Me.LabelControlDept.Text = "Department"
        '
        'LabelControl7
        '
        Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl7.Appearance.Options.UseFont = True
        Me.LabelControl7.Location = New System.Drawing.Point(577, 35)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(66, 14)
        Me.LabelControl7.TabIndex = 11
        Me.LabelControl7.Text = "Department"
        '
        'SimpleButtonCal
        '
        Me.SimpleButtonCal.ImageOptions.Image = CType(resources.GetObject("SimpleButtonCal.ImageOptions.Image"), System.Drawing.Image)
        Me.SimpleButtonCal.Location = New System.Drawing.Point(493, 32)
        Me.SimpleButtonCal.Name = "SimpleButtonCal"
        Me.SimpleButtonCal.Size = New System.Drawing.Size(23, 20)
        Me.SimpleButtonCal.TabIndex = 13
        Me.SimpleButtonCal.ToolTip = "Bifercate Grosss as per Setup"
        '
        'TextEditPf1
        '
        Me.TextEditPf1.Location = New System.Drawing.Point(246, 108)
        Me.TextEditPf1.Name = "TextEditPf1"
        Me.TextEditPf1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditPf1.Properties.Appearance.Options.UseFont = True
        Me.TextEditPf1.Properties.Mask.EditMask = "#####"
        Me.TextEditPf1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEditPf1.Properties.MaxLength = 5
        Me.TextEditPf1.Properties.ReadOnly = True
        Me.TextEditPf1.Size = New System.Drawing.Size(54, 20)
        Me.TextEditPf1.TabIndex = 27
        '
        'TextEditPf
        '
        Me.TextEditPf.Location = New System.Drawing.Point(105, 108)
        Me.TextEditPf.Name = "TextEditPf"
        Me.TextEditPf.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditPf.Properties.Appearance.Options.UseFont = True
        Me.TextEditPf.Properties.Mask.EditMask = "[A-Z0-9]*"
        Me.TextEditPf.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEditPf.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditPf.Properties.MaxLength = 12
        Me.TextEditPf.Properties.ReadOnly = True
        Me.TextEditPf.Size = New System.Drawing.Size(135, 20)
        Me.TextEditPf.TabIndex = 26
        '
        'LabelControl14
        '
        Me.LabelControl14.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl14.Appearance.Options.UseFont = True
        Me.LabelControl14.Location = New System.Drawing.Point(11, 111)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(44, 14)
        Me.LabelControl14.TabIndex = 28
        Me.LabelControl14.Text = "PF No   "
        '
        'TextEditESI
        '
        Me.TextEditESI.Location = New System.Drawing.Point(105, 134)
        Me.TextEditESI.Name = "TextEditESI"
        Me.TextEditESI.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditESI.Properties.Appearance.Options.UseFont = True
        Me.TextEditESI.Properties.Mask.EditMask = "[A-Z0-9]*"
        Me.TextEditESI.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEditESI.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditESI.Properties.MaxLength = 12
        Me.TextEditESI.Properties.ReadOnly = True
        Me.TextEditESI.Size = New System.Drawing.Size(135, 20)
        Me.TextEditESI.TabIndex = 29
        '
        'LabelControl15
        '
        Me.LabelControl15.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl15.Appearance.Options.UseFont = True
        Me.LabelControl15.Location = New System.Drawing.Point(11, 137)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(49, 14)
        Me.LabelControl15.TabIndex = 30
        Me.LabelControl15.Text = "ESI No   "
        '
        'TextEditPAN
        '
        Me.TextEditPAN.Location = New System.Drawing.Point(105, 160)
        Me.TextEditPAN.Name = "TextEditPAN"
        Me.TextEditPAN.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditPAN.Properties.Appearance.Options.UseFont = True
        Me.TextEditPAN.Properties.Mask.EditMask = "[A-Z0-9]*"
        Me.TextEditPAN.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEditPAN.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditPAN.Properties.MaxLength = 10
        Me.TextEditPAN.Properties.ReadOnly = True
        Me.TextEditPAN.Size = New System.Drawing.Size(135, 20)
        Me.TextEditPAN.TabIndex = 31
        '
        'LabelControl33
        '
        Me.LabelControl33.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl33.Appearance.Options.UseFont = True
        Me.LabelControl33.Location = New System.Drawing.Point(11, 163)
        Me.LabelControl33.Name = "LabelControl33"
        Me.LabelControl33.Size = New System.Drawing.Size(23, 14)
        Me.LabelControl33.TabIndex = 32
        Me.LabelControl33.Text = "PAN"
        '
        'LabelControl9
        '
        Me.LabelControl9.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl9.Appearance.Options.UseFont = True
        Me.LabelControl9.Location = New System.Drawing.Point(11, 189)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(65, 14)
        Me.LabelControl9.TabIndex = 33
        Me.LabelControl9.Text = "Payment by"
        '
        'ComboBoxEditPayBy
        '
        Me.ComboBoxEditPayBy.EditValue = "Cheque"
        Me.ComboBoxEditPayBy.Location = New System.Drawing.Point(105, 186)
        Me.ComboBoxEditPayBy.Name = "ComboBoxEditPayBy"
        Me.ComboBoxEditPayBy.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEditPayBy.Properties.Appearance.Options.UseFont = True
        Me.ComboBoxEditPayBy.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEditPayBy.Properties.Items.AddRange(New Object() {"Cheque", "Cash"})
        Me.ComboBoxEditPayBy.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.ComboBoxEditPayBy.Size = New System.Drawing.Size(135, 20)
        Me.ComboBoxEditPayBy.TabIndex = 34
        '
        'ComboBoxEditEmpType
        '
        Me.ComboBoxEditEmpType.EditValue = "OnRoll"
        Me.ComboBoxEditEmpType.Location = New System.Drawing.Point(105, 212)
        Me.ComboBoxEditEmpType.Name = "ComboBoxEditEmpType"
        Me.ComboBoxEditEmpType.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEditEmpType.Properties.Appearance.Options.UseFont = True
        Me.ComboBoxEditEmpType.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEditEmpType.Properties.Items.AddRange(New Object() {"OnRoll", "Daily Wages", "Piece Rate", "Hourly"})
        Me.ComboBoxEditEmpType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.ComboBoxEditEmpType.Size = New System.Drawing.Size(135, 20)
        Me.ComboBoxEditEmpType.TabIndex = 35
        '
        'LabelControl10
        '
        Me.LabelControl10.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl10.Appearance.Options.UseFont = True
        Me.LabelControl10.Location = New System.Drawing.Point(11, 215)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(85, 14)
        Me.LabelControl10.TabIndex = 36
        Me.LabelControl10.Text = "Employee Type"
        '
        'GridLookUpEditBank
        '
        Me.GridLookUpEditBank.Location = New System.Drawing.Point(105, 238)
        Me.GridLookUpEditBank.Name = "GridLookUpEditBank"
        Me.GridLookUpEditBank.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEditBank.Properties.Appearance.Options.UseFont = True
        Me.GridLookUpEditBank.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GridLookUpEditBank.Properties.DataSource = Me.TblBankBindingSource
        Me.GridLookUpEditBank.Properties.DisplayMember = "BANKCODE"
        Me.GridLookUpEditBank.Properties.NullText = ""
        Me.GridLookUpEditBank.Properties.ReadOnly = True
        Me.GridLookUpEditBank.Properties.ValueMember = "BANKCODE"
        Me.GridLookUpEditBank.Properties.View = Me.GridView6
        Me.GridLookUpEditBank.Size = New System.Drawing.Size(135, 20)
        Me.GridLookUpEditBank.TabIndex = 38
        '
        'TblBankBindingSource
        '
        Me.TblBankBindingSource.DataMember = "tblBank"
        Me.TblBankBindingSource.DataSource = Me.SSSDBDataSet
        '
        'GridView6
        '
        Me.GridView6.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colBANKCODE, Me.colBANKNAME})
        Me.GridView6.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView6.Name = "GridView6"
        Me.GridView6.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView6.OptionsView.ShowGroupPanel = False
        '
        'colBANKCODE
        '
        Me.colBANKCODE.FieldName = "BANKCODE"
        Me.colBANKCODE.Name = "colBANKCODE"
        Me.colBANKCODE.Visible = True
        Me.colBANKCODE.VisibleIndex = 0
        '
        'colBANKNAME
        '
        Me.colBANKNAME.FieldName = "BANKNAME"
        Me.colBANKNAME.Name = "colBANKNAME"
        Me.colBANKNAME.Visible = True
        Me.colBANKNAME.VisibleIndex = 1
        '
        'LabelControl24
        '
        Me.LabelControl24.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl24.Appearance.Options.UseFont = True
        Me.LabelControl24.Location = New System.Drawing.Point(11, 241)
        Me.LabelControl24.Name = "LabelControl24"
        Me.LabelControl24.Size = New System.Drawing.Size(61, 14)
        Me.LabelControl24.TabIndex = 37
        Me.LabelControl24.Text = "Bank Name"
        '
        'TextEditAccNo
        '
        Me.TextEditAccNo.Location = New System.Drawing.Point(322, 238)
        Me.TextEditAccNo.Name = "TextEditAccNo"
        Me.TextEditAccNo.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditAccNo.Properties.Appearance.Options.UseFont = True
        Me.TextEditAccNo.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEditAccNo.Properties.MaxLength = 25
        Me.TextEditAccNo.Size = New System.Drawing.Size(135, 20)
        Me.TextEditAccNo.TabIndex = 40
        '
        'LabelControl25
        '
        Me.LabelControl25.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl25.Appearance.Options.UseFont = True
        Me.LabelControl25.Location = New System.Drawing.Point(266, 238)
        Me.LabelControl25.Name = "LabelControl25"
        Me.LabelControl25.Size = New System.Drawing.Size(38, 14)
        Me.LabelControl25.TabIndex = 39
        Me.LabelControl25.Text = "A/c No"
        '
        'ComboBoxEditDA
        '
        Me.ComboBoxEditDA.EditValue = "PDays"
        Me.ComboBoxEditDA.Location = New System.Drawing.Point(614, 82)
        Me.ComboBoxEditDA.Name = "ComboBoxEditDA"
        Me.ComboBoxEditDA.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEditDA.Properties.Appearance.Options.UseFont = True
        Me.ComboBoxEditDA.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEditDA.Properties.Items.AddRange(New Object() {"Fixed", "PDays"})
        Me.ComboBoxEditDA.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.ComboBoxEditDA.Size = New System.Drawing.Size(77, 20)
        Me.ComboBoxEditDA.TabIndex = 43
        '
        'ComboBoxEditConvence
        '
        Me.ComboBoxEditConvence.EditValue = "PDays"
        Me.ComboBoxEditConvence.Location = New System.Drawing.Point(614, 108)
        Me.ComboBoxEditConvence.Name = "ComboBoxEditConvence"
        Me.ComboBoxEditConvence.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEditConvence.Properties.Appearance.Options.UseFont = True
        Me.ComboBoxEditConvence.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEditConvence.Properties.Items.AddRange(New Object() {"Fixed", "PDays"})
        Me.ComboBoxEditConvence.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.ComboBoxEditConvence.Size = New System.Drawing.Size(77, 20)
        Me.ComboBoxEditConvence.TabIndex = 46
        '
        'ComboBoxEditMedical
        '
        Me.ComboBoxEditMedical.EditValue = "PDays"
        Me.ComboBoxEditMedical.Location = New System.Drawing.Point(614, 134)
        Me.ComboBoxEditMedical.Name = "ComboBoxEditMedical"
        Me.ComboBoxEditMedical.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEditMedical.Properties.Appearance.Options.UseFont = True
        Me.ComboBoxEditMedical.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEditMedical.Properties.Items.AddRange(New Object() {"Fixed", "PDays"})
        Me.ComboBoxEditMedical.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.ComboBoxEditMedical.Size = New System.Drawing.Size(77, 20)
        Me.ComboBoxEditMedical.TabIndex = 49
        '
        'ComboBoxEditHRA
        '
        Me.ComboBoxEditHRA.EditValue = "PDays"
        Me.ComboBoxEditHRA.Location = New System.Drawing.Point(614, 160)
        Me.ComboBoxEditHRA.Name = "ComboBoxEditHRA"
        Me.ComboBoxEditHRA.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEditHRA.Properties.Appearance.Options.UseFont = True
        Me.ComboBoxEditHRA.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEditHRA.Properties.Items.AddRange(New Object() {"Fixed", "PDays"})
        Me.ComboBoxEditHRA.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.ComboBoxEditHRA.Size = New System.Drawing.Size(77, 20)
        Me.ComboBoxEditHRA.TabIndex = 52
        '
        'TextEditOT
        '
        Me.TextEditOT.Location = New System.Drawing.Point(473, 186)
        Me.TextEditOT.Name = "TextEditOT"
        Me.TextEditOT.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditOT.Properties.Appearance.Options.UseFont = True
        Me.TextEditOT.Properties.Mask.EditMask = "########.##"
        Me.TextEditOT.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEditOT.Properties.MaxLength = 8
        Me.TextEditOT.Size = New System.Drawing.Size(135, 20)
        Me.TextEditOT.TabIndex = 54
        '
        'LabelControl17
        '
        Me.LabelControl17.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl17.Appearance.Options.UseFont = True
        Me.LabelControl17.Location = New System.Drawing.Point(359, 189)
        Me.LabelControl17.Name = "LabelControl17"
        Me.LabelControl17.Size = New System.Drawing.Size(106, 14)
        Me.LabelControl17.TabIndex = 53
        Me.LabelControl17.Text = "O.T. Rate Per Hour"
        '
        'ComboBoxEditTDS
        '
        Me.ComboBoxEditTDS.EditValue = ""
        Me.ComboBoxEditTDS.Enabled = False
        Me.ComboBoxEditTDS.Location = New System.Drawing.Point(614, 212)
        Me.ComboBoxEditTDS.Name = "ComboBoxEditTDS"
        Me.ComboBoxEditTDS.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEditTDS.Properties.Appearance.Options.UseFont = True
        Me.ComboBoxEditTDS.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEditTDS.Size = New System.Drawing.Size(77, 20)
        Me.ComboBoxEditTDS.TabIndex = 58
        '
        'TextEditTDS
        '
        Me.TextEditTDS.Location = New System.Drawing.Point(473, 212)
        Me.TextEditTDS.Name = "TextEditTDS"
        Me.TextEditTDS.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditTDS.Properties.Appearance.Options.UseFont = True
        Me.TextEditTDS.Properties.Mask.EditMask = "########.##"
        Me.TextEditTDS.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEditTDS.Properties.MaxLength = 8
        Me.TextEditTDS.Size = New System.Drawing.Size(135, 20)
        Me.TextEditTDS.TabIndex = 57
        '
        'LabelControl18
        '
        Me.LabelControl18.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl18.Appearance.Options.UseFont = True
        Me.LabelControl18.Location = New System.Drawing.Point(359, 215)
        Me.LabelControl18.Name = "LabelControl18"
        Me.LabelControl18.Size = New System.Drawing.Size(23, 14)
        Me.LabelControl18.TabIndex = 56
        Me.LabelControl18.Text = "TDS"
        '
        'GroupControlAllow
        '
        Me.GroupControlAllow.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GroupControlAllow.AppearanceCaption.ForeColor = System.Drawing.Color.Blue
        Me.GroupControlAllow.AppearanceCaption.Options.UseFont = True
        Me.GroupControlAllow.AppearanceCaption.Options.UseForeColor = True
        Me.GroupControlAllow.Controls.Add(Me.CheckEditBonus)
        Me.GroupControlAllow.Controls.Add(Me.CheckEditVPF)
        Me.GroupControlAllow.Controls.Add(Me.CheckEditGraduty)
        Me.GroupControlAllow.Controls.Add(Me.CheckEditESI)
        Me.GroupControlAllow.Controls.Add(Me.CheckEditUnderLim)
        Me.GroupControlAllow.Controls.Add(Me.CheckEditPF)
        Me.GroupControlAllow.Controls.Add(Me.CheckEditProfTax)
        Me.GroupControlAllow.Location = New System.Drawing.Point(697, 80)
        Me.GroupControlAllow.Name = "GroupControlAllow"
        Me.GroupControlAllow.Size = New System.Drawing.Size(200, 152)
        Me.GroupControlAllow.TabIndex = 59
        Me.GroupControlAllow.Text = "Allow"
        '
        'CheckEditBonus
        '
        Me.CheckEditBonus.EditValue = "N"
        Me.CheckEditBonus.Location = New System.Drawing.Point(106, 54)
        Me.CheckEditBonus.Name = "CheckEditBonus"
        Me.CheckEditBonus.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditBonus.Properties.Appearance.Options.UseFont = True
        Me.CheckEditBonus.Properties.Caption = "Bonus"
        Me.CheckEditBonus.Properties.DisplayValueChecked = "Y"
        Me.CheckEditBonus.Properties.DisplayValueGrayed = "N"
        Me.CheckEditBonus.Properties.DisplayValueUnchecked = "N"
        Me.CheckEditBonus.Properties.ValueChecked = "Y"
        Me.CheckEditBonus.Properties.ValueGrayed = "N"
        Me.CheckEditBonus.Properties.ValueUnchecked = "N"
        Me.CheckEditBonus.Size = New System.Drawing.Size(75, 19)
        Me.CheckEditBonus.TabIndex = 7
        '
        'CheckEditVPF
        '
        Me.CheckEditVPF.EditValue = "N"
        Me.CheckEditVPF.Location = New System.Drawing.Point(106, 29)
        Me.CheckEditVPF.Name = "CheckEditVPF"
        Me.CheckEditVPF.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditVPF.Properties.Appearance.Options.UseFont = True
        Me.CheckEditVPF.Properties.Caption = "VPF"
        Me.CheckEditVPF.Properties.DisplayValueChecked = "Y"
        Me.CheckEditVPF.Properties.DisplayValueGrayed = "N"
        Me.CheckEditVPF.Properties.DisplayValueUnchecked = "N"
        Me.CheckEditVPF.Properties.ValueChecked = "Y"
        Me.CheckEditVPF.Properties.ValueGrayed = "N"
        Me.CheckEditVPF.Properties.ValueUnchecked = "N"
        Me.CheckEditVPF.Size = New System.Drawing.Size(75, 19)
        Me.CheckEditVPF.TabIndex = 6
        '
        'CheckEditGraduty
        '
        Me.CheckEditGraduty.EditValue = "N"
        Me.CheckEditGraduty.Location = New System.Drawing.Point(106, 79)
        Me.CheckEditGraduty.Name = "CheckEditGraduty"
        Me.CheckEditGraduty.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditGraduty.Properties.Appearance.Options.UseFont = True
        Me.CheckEditGraduty.Properties.Caption = "Graduty"
        Me.CheckEditGraduty.Properties.DisplayValueChecked = "Y"
        Me.CheckEditGraduty.Properties.DisplayValueGrayed = "N"
        Me.CheckEditGraduty.Properties.DisplayValueUnchecked = "N"
        Me.CheckEditGraduty.Properties.ValueChecked = "Y"
        Me.CheckEditGraduty.Properties.ValueGrayed = "N"
        Me.CheckEditGraduty.Properties.ValueUnchecked = "N"
        Me.CheckEditGraduty.Size = New System.Drawing.Size(75, 19)
        Me.CheckEditGraduty.TabIndex = 5
        '
        'CheckEditESI
        '
        Me.CheckEditESI.EditValue = "N"
        Me.CheckEditESI.Location = New System.Drawing.Point(5, 104)
        Me.CheckEditESI.Name = "CheckEditESI"
        Me.CheckEditESI.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditESI.Properties.Appearance.Options.UseFont = True
        Me.CheckEditESI.Properties.Caption = "ESI"
        Me.CheckEditESI.Properties.DisplayValueChecked = "Y"
        Me.CheckEditESI.Properties.DisplayValueGrayed = "N"
        Me.CheckEditESI.Properties.DisplayValueUnchecked = "N"
        Me.CheckEditESI.Properties.ValueChecked = "Y"
        Me.CheckEditESI.Properties.ValueGrayed = "N"
        Me.CheckEditESI.Properties.ValueUnchecked = "N"
        Me.CheckEditESI.Size = New System.Drawing.Size(75, 19)
        Me.CheckEditESI.TabIndex = 4
        '
        'CheckEditUnderLim
        '
        Me.CheckEditUnderLim.EditValue = "N"
        Me.CheckEditUnderLim.Location = New System.Drawing.Point(5, 79)
        Me.CheckEditUnderLim.Name = "CheckEditUnderLim"
        Me.CheckEditUnderLim.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditUnderLim.Properties.Appearance.Options.UseFont = True
        Me.CheckEditUnderLim.Properties.Caption = "Under Limit"
        Me.CheckEditUnderLim.Properties.DisplayValueChecked = "Y"
        Me.CheckEditUnderLim.Properties.DisplayValueGrayed = "N"
        Me.CheckEditUnderLim.Properties.DisplayValueUnchecked = "N"
        Me.CheckEditUnderLim.Properties.ValueChecked = "Y"
        Me.CheckEditUnderLim.Properties.ValueGrayed = "N"
        Me.CheckEditUnderLim.Properties.ValueUnchecked = "N"
        Me.CheckEditUnderLim.Size = New System.Drawing.Size(95, 19)
        Me.CheckEditUnderLim.TabIndex = 3
        '
        'CheckEditPF
        '
        Me.CheckEditPF.EditValue = "N"
        Me.CheckEditPF.Location = New System.Drawing.Point(5, 54)
        Me.CheckEditPF.Name = "CheckEditPF"
        Me.CheckEditPF.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditPF.Properties.Appearance.Options.UseFont = True
        Me.CheckEditPF.Properties.Caption = "PF"
        Me.CheckEditPF.Properties.DisplayValueChecked = "Y"
        Me.CheckEditPF.Properties.DisplayValueGrayed = "N"
        Me.CheckEditPF.Properties.DisplayValueUnchecked = "N"
        Me.CheckEditPF.Properties.ValueChecked = "Y"
        Me.CheckEditPF.Properties.ValueGrayed = "N"
        Me.CheckEditPF.Properties.ValueUnchecked = "N"
        Me.CheckEditPF.Size = New System.Drawing.Size(75, 19)
        Me.CheckEditPF.TabIndex = 2
        '
        'CheckEditProfTax
        '
        Me.CheckEditProfTax.EditValue = "N"
        Me.CheckEditProfTax.Location = New System.Drawing.Point(5, 29)
        Me.CheckEditProfTax.Name = "CheckEditProfTax"
        Me.CheckEditProfTax.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditProfTax.Properties.Appearance.Options.UseFont = True
        Me.CheckEditProfTax.Properties.Caption = "Prof. Tax"
        Me.CheckEditProfTax.Properties.DisplayValueChecked = "Y"
        Me.CheckEditProfTax.Properties.DisplayValueGrayed = "N"
        Me.CheckEditProfTax.Properties.DisplayValueUnchecked = "N"
        Me.CheckEditProfTax.Properties.ValueChecked = "Y"
        Me.CheckEditProfTax.Properties.ValueGrayed = "N"
        Me.CheckEditProfTax.Properties.ValueUnchecked = "N"
        Me.CheckEditProfTax.Size = New System.Drawing.Size(75, 19)
        Me.CheckEditProfTax.TabIndex = 1
        '
        'GridLookUpEditFormula
        '
        Me.GridLookUpEditFormula.Location = New System.Drawing.Point(614, 186)
        Me.GridLookUpEditFormula.Name = "GridLookUpEditFormula"
        Me.GridLookUpEditFormula.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEditFormula.Properties.Appearance.Options.UseFont = True
        Me.GridLookUpEditFormula.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GridLookUpEditFormula.Properties.DataSource = Me.PAYFORMULABindingSource
        Me.GridLookUpEditFormula.Properties.DisplayMember = "CODE"
        Me.GridLookUpEditFormula.Properties.NullText = ""
        Me.GridLookUpEditFormula.Properties.ValueMember = "CODE"
        Me.GridLookUpEditFormula.Properties.View = Me.GridView1
        Me.GridLookUpEditFormula.Size = New System.Drawing.Size(77, 20)
        Me.GridLookUpEditFormula.TabIndex = 60
        '
        'PAYFORMULABindingSource
        '
        Me.PAYFORMULABindingSource.DataMember = "PAY_FORMULA"
        Me.PAYFORMULABindingSource.DataSource = Me.SSSDBDataSet
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn2})
        Me.GridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView1.OptionsView.ShowGroupPanel = False
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Code"
        Me.GridColumn1.FieldName = "CODE"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 0
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Formula"
        Me.GridColumn2.FieldName = "FORM"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 1
        '
        'TblEmployeeTableAdapter
        '
        Me.TblEmployeeTableAdapter.ClearBeforeFill = True
        '
        'TblEmployee1TableAdapter1
        '
        Me.TblEmployee1TableAdapter1.ClearBeforeFill = True
        '
        'TblBankTableAdapter
        '
        Me.TblBankTableAdapter.ClearBeforeFill = True
        '
        'TblBank1TableAdapter1
        '
        Me.TblBank1TableAdapter1.ClearBeforeFill = True
        '
        'PAY_FORMULATableAdapter
        '
        Me.PAY_FORMULATableAdapter.ClearBeforeFill = True
        '
        'PaY_FORMULA1TableAdapter1
        '
        Me.PaY_FORMULA1TableAdapter1.ClearBeforeFill = True
        '
        'SidePanel1
        '
        Me.SidePanel1.Controls.Add(Me.ComboNepaliYear)
        Me.SidePanel1.Controls.Add(Me.ComboNEpaliMonth)
        Me.SidePanel1.Controls.Add(Me.TextEditPaycode)
        Me.SidePanel1.Controls.Add(Me.TextEditBasic)
        Me.SidePanel1.Controls.Add(Me.GridLookUpEditFormula)
        Me.SidePanel1.Controls.Add(Me.LabelControl1)
        Me.SidePanel1.Controls.Add(Me.GroupControlAllow)
        Me.SidePanel1.Controls.Add(Me.LookUpEdit1)
        Me.SidePanel1.Controls.Add(Me.ComboBoxEditTDS)
        Me.SidePanel1.Controls.Add(Me.LabelControl2)
        Me.SidePanel1.Controls.Add(Me.TextEditTDS)
        Me.SidePanel1.Controls.Add(Me.TextEditCardNo)
        Me.SidePanel1.Controls.Add(Me.LabelControl18)
        Me.SidePanel1.Controls.Add(Me.LabelControl3)
        Me.SidePanel1.Controls.Add(Me.TextEditOT)
        Me.SidePanel1.Controls.Add(Me.DateEditEffFrm)
        Me.SidePanel1.Controls.Add(Me.LabelControl17)
        Me.SidePanel1.Controls.Add(Me.LabelControl4)
        Me.SidePanel1.Controls.Add(Me.ComboBoxEditHRA)
        Me.SidePanel1.Controls.Add(Me.LabelControl5)
        Me.SidePanel1.Controls.Add(Me.TextEditHRA)
        Me.SidePanel1.Controls.Add(Me.LabelControlName)
        Me.SidePanel1.Controls.Add(Me.LabelControl16)
        Me.SidePanel1.Controls.Add(Me.TextEditGross)
        Me.SidePanel1.Controls.Add(Me.ComboBoxEditMedical)
        Me.SidePanel1.Controls.Add(Me.LabelControl7)
        Me.SidePanel1.Controls.Add(Me.TextEditMedical)
        Me.SidePanel1.Controls.Add(Me.LabelControlDept)
        Me.SidePanel1.Controls.Add(Me.LabelControl13)
        Me.SidePanel1.Controls.Add(Me.SimpleButtonCal)
        Me.SidePanel1.Controls.Add(Me.ComboBoxEditConvence)
        Me.SidePanel1.Controls.Add(Me.LabelControl8)
        Me.SidePanel1.Controls.Add(Me.TextEditConvence)
        Me.SidePanel1.Controls.Add(Me.LabelControl14)
        Me.SidePanel1.Controls.Add(Me.LabelControl12)
        Me.SidePanel1.Controls.Add(Me.TextEditPf)
        Me.SidePanel1.Controls.Add(Me.ComboBoxEditDA)
        Me.SidePanel1.Controls.Add(Me.TextEditPf1)
        Me.SidePanel1.Controls.Add(Me.TextEditDA)
        Me.SidePanel1.Controls.Add(Me.LabelControl15)
        Me.SidePanel1.Controls.Add(Me.LabelControl11)
        Me.SidePanel1.Controls.Add(Me.TextEditESI)
        Me.SidePanel1.Controls.Add(Me.TextEditAccNo)
        Me.SidePanel1.Controls.Add(Me.LabelControl33)
        Me.SidePanel1.Controls.Add(Me.LabelControl25)
        Me.SidePanel1.Controls.Add(Me.TextEditPAN)
        Me.SidePanel1.Controls.Add(Me.GridLookUpEditBank)
        Me.SidePanel1.Controls.Add(Me.LabelControl9)
        Me.SidePanel1.Controls.Add(Me.LabelControl24)
        Me.SidePanel1.Controls.Add(Me.ComboBoxEditPayBy)
        Me.SidePanel1.Controls.Add(Me.LabelControl10)
        Me.SidePanel1.Controls.Add(Me.ComboBoxEditEmpType)
        Me.SidePanel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.SidePanel1.Location = New System.Drawing.Point(0, 0)
        Me.SidePanel1.Name = "SidePanel1"
        Me.SidePanel1.Size = New System.Drawing.Size(975, 276)
        Me.SidePanel1.TabIndex = 61
        Me.SidePanel1.Text = "SidePanel1"
        '
        'ComboNepaliYear
        '
        Me.ComboNepaliYear.Location = New System.Drawing.Point(178, 29)
        Me.ComboNepaliYear.Name = "ComboNepaliYear"
        Me.ComboNepaliYear.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliYear.Properties.Appearance.Options.UseFont = True
        Me.ComboNepaliYear.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliYear.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNepaliYear.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNepaliYear.Properties.Items.AddRange(New Object() {"2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037", "2038", "2039", "2040", "2041", "2042", "2043", "2044", "2045", "2046", "2047", "2048", "2049", "2050", "2051", "2052", "2053", "2054", "2055", "2056", "2057", "2058", "2059", "2060", "2061", "2062", "2063", "2064", "2065", "2066", "2067", "2068", "2069", "2070", "2071", "2072", "2073", "2074", "2075", "2076", "2077", "2078", "2079", "2080", "2081", "2082", "2083", "2084", "2085", "2086", "2087", "2088", "2089"})
        Me.ComboNepaliYear.Size = New System.Drawing.Size(61, 20)
        Me.ComboNepaliYear.TabIndex = 63
        '
        'ComboNEpaliMonth
        '
        Me.ComboNEpaliMonth.Location = New System.Drawing.Point(106, 29)
        Me.ComboNEpaliMonth.Name = "ComboNEpaliMonth"
        Me.ComboNEpaliMonth.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNEpaliMonth.Properties.Appearance.Options.UseFont = True
        Me.ComboNEpaliMonth.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNEpaliMonth.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNEpaliMonth.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNEpaliMonth.Properties.Items.AddRange(New Object() {"Baishakh", "Jestha", "Asar", "Shrawan", "Bhadau", "Aswin", "Kartik", "Mansir", "Poush", "Magh", "Falgun", "Chaitra"})
        Me.ComboNEpaliMonth.Size = New System.Drawing.Size(66, 20)
        Me.ComboNEpaliMonth.TabIndex = 62
        '
        'TextEditPaycode
        '
        Me.TextEditPaycode.Location = New System.Drawing.Point(106, 6)
        Me.TextEditPaycode.Name = "TextEditPaycode"
        Me.TextEditPaycode.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditPaycode.Properties.Appearance.Options.UseFont = True
        Me.TextEditPaycode.Properties.MaxLength = 12
        Me.TextEditPaycode.Properties.ReadOnly = True
        Me.TextEditPaycode.Size = New System.Drawing.Size(129, 20)
        Me.TextEditPaycode.TabIndex = 61
        '
        'TextEditBasic
        '
        Me.TextEditBasic.Location = New System.Drawing.Point(105, 82)
        Me.TextEditBasic.Name = "TextEditBasic"
        Me.TextEditBasic.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditBasic.Properties.Appearance.Options.UseFont = True
        Me.TextEditBasic.Properties.Mask.EditMask = "########.##"
        Me.TextEditBasic.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEditBasic.Properties.MaxLength = 8
        Me.TextEditBasic.Size = New System.Drawing.Size(135, 20)
        Me.TextEditBasic.TabIndex = 15
        '
        'TextEditHRA
        '
        Me.TextEditHRA.Location = New System.Drawing.Point(473, 160)
        Me.TextEditHRA.Name = "TextEditHRA"
        Me.TextEditHRA.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditHRA.Properties.Appearance.Options.UseFont = True
        Me.TextEditHRA.Properties.Mask.EditMask = "########.##"
        Me.TextEditHRA.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEditHRA.Properties.MaxLength = 8
        Me.TextEditHRA.Size = New System.Drawing.Size(135, 20)
        Me.TextEditHRA.TabIndex = 51
        '
        'LabelControl16
        '
        Me.LabelControl16.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl16.Appearance.Options.UseFont = True
        Me.LabelControl16.Location = New System.Drawing.Point(359, 163)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(23, 14)
        Me.LabelControl16.TabIndex = 50
        Me.LabelControl16.Text = "HRA"
        '
        'TextEditMedical
        '
        Me.TextEditMedical.Location = New System.Drawing.Point(473, 134)
        Me.TextEditMedical.Name = "TextEditMedical"
        Me.TextEditMedical.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditMedical.Properties.Appearance.Options.UseFont = True
        Me.TextEditMedical.Properties.Mask.EditMask = "########.##"
        Me.TextEditMedical.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEditMedical.Properties.MaxLength = 8
        Me.TextEditMedical.Size = New System.Drawing.Size(135, 20)
        Me.TextEditMedical.TabIndex = 48
        '
        'LabelControl13
        '
        Me.LabelControl13.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl13.Appearance.Options.UseFont = True
        Me.LabelControl13.Location = New System.Drawing.Point(359, 137)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(39, 14)
        Me.LabelControl13.TabIndex = 47
        Me.LabelControl13.Text = "Medical"
        '
        'LabelControl8
        '
        Me.LabelControl8.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl8.Appearance.Options.UseFont = True
        Me.LabelControl8.Location = New System.Drawing.Point(11, 85)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(26, 14)
        Me.LabelControl8.TabIndex = 14
        Me.LabelControl8.Text = "Basic"
        '
        'TextEditConvence
        '
        Me.TextEditConvence.Location = New System.Drawing.Point(473, 108)
        Me.TextEditConvence.Name = "TextEditConvence"
        Me.TextEditConvence.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditConvence.Properties.Appearance.Options.UseFont = True
        Me.TextEditConvence.Properties.Mask.EditMask = "########.##"
        Me.TextEditConvence.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEditConvence.Properties.MaxLength = 8
        Me.TextEditConvence.Size = New System.Drawing.Size(135, 20)
        Me.TextEditConvence.TabIndex = 45
        '
        'LabelControl12
        '
        Me.LabelControl12.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl12.Appearance.Options.UseFont = True
        Me.LabelControl12.Location = New System.Drawing.Point(359, 111)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(66, 14)
        Me.LabelControl12.TabIndex = 44
        Me.LabelControl12.Text = "Conveyance"
        '
        'TextEditDA
        '
        Me.TextEditDA.Location = New System.Drawing.Point(473, 82)
        Me.TextEditDA.Name = "TextEditDA"
        Me.TextEditDA.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditDA.Properties.Appearance.Options.UseFont = True
        Me.TextEditDA.Properties.Mask.EditMask = "########.##"
        Me.TextEditDA.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEditDA.Properties.MaxLength = 8
        Me.TextEditDA.Size = New System.Drawing.Size(135, 20)
        Me.TextEditDA.TabIndex = 42
        '
        'LabelControl11
        '
        Me.LabelControl11.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl11.Appearance.Options.UseFont = True
        Me.LabelControl11.Location = New System.Drawing.Point(359, 85)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(16, 14)
        Me.LabelControl11.TabIndex = 41
        Me.LabelControl11.Text = "DA"
        '
        'SidePanel3
        '
        Me.SidePanel3.Controls.Add(Me.XtraTabControl2)
        Me.SidePanel3.Location = New System.Drawing.Point(486, 276)
        Me.SidePanel3.Name = "SidePanel3"
        Me.SidePanel3.Size = New System.Drawing.Size(486, 204)
        Me.SidePanel3.TabIndex = 63
        Me.SidePanel3.Text = "SidePanel3"
        '
        'XtraTabControl2
        '
        Me.XtraTabControl2.AppearancePage.Header.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.XtraTabControl2.AppearancePage.Header.Options.UseFont = True
        Me.XtraTabControl2.AppearancePage.HeaderActive.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.XtraTabControl2.AppearancePage.HeaderActive.Options.UseFont = True
        Me.XtraTabControl2.AppearancePage.HeaderHotTracked.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.XtraTabControl2.AppearancePage.HeaderHotTracked.Options.UseFont = True
        Me.XtraTabControl2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.XtraTabControl2.Location = New System.Drawing.Point(0, 0)
        Me.XtraTabControl2.Name = "XtraTabControl2"
        Me.XtraTabControl2.SelectedTabPage = Me.XtraTabPage3
        Me.XtraTabControl2.Size = New System.Drawing.Size(486, 204)
        Me.XtraTabControl2.TabIndex = 1
        Me.XtraTabControl2.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage3, Me.XtraTabPage4})
        '
        'XtraTabPage3
        '
        Me.XtraTabPage3.Controls.Add(Me.PictureEdit15)
        Me.XtraTabPage3.Controls.Add(Me.PictureEdit14)
        Me.XtraTabPage3.Controls.Add(Me.PictureEdit13)
        Me.XtraTabPage3.Controls.Add(Me.PictureEdit12)
        Me.XtraTabPage3.Controls.Add(Me.PictureEdit11)
        Me.XtraTabPage3.Controls.Add(Me.LabelControl38)
        Me.XtraTabPage3.Controls.Add(Me.TextEditRateE5)
        Me.XtraTabPage3.Controls.Add(Me.TextEditDescpE5)
        Me.XtraTabPage3.Controls.Add(Me.LabelControl39)
        Me.XtraTabPage3.Controls.Add(Me.GridLookUpEditFormulaE5)
        Me.XtraTabPage3.Controls.Add(Me.TextEditRateE4)
        Me.XtraTabPage3.Controls.Add(Me.GridLookUpEditFormulaE4)
        Me.XtraTabPage3.Controls.Add(Me.TextEditDescpE4)
        Me.XtraTabPage3.Controls.Add(Me.LabelControl40)
        Me.XtraTabPage3.Controls.Add(Me.GridLookUpEditFormulaE3)
        Me.XtraTabPage3.Controls.Add(Me.GridLookUpEditFormulaE2)
        Me.XtraTabPage3.Controls.Add(Me.TextEditRateE3)
        Me.XtraTabPage3.Controls.Add(Me.TextEditDescpE3)
        Me.XtraTabPage3.Controls.Add(Me.GridLookUpEditFormulaE1)
        Me.XtraTabPage3.Controls.Add(Me.LabelControl41)
        Me.XtraTabPage3.Controls.Add(Me.TextEditRateE2)
        Me.XtraTabPage3.Controls.Add(Me.TextEditDescpE2)
        Me.XtraTabPage3.Controls.Add(Me.LabelControl42)
        Me.XtraTabPage3.Controls.Add(Me.LabelControl43)
        Me.XtraTabPage3.Controls.Add(Me.LabelControl44)
        Me.XtraTabPage3.Controls.Add(Me.TextEditRateE1)
        Me.XtraTabPage3.Controls.Add(Me.TextEditDescpE1)
        Me.XtraTabPage3.Controls.Add(Me.LabelControl45)
        Me.XtraTabPage3.Name = "XtraTabPage3"
        Me.XtraTabPage3.Size = New System.Drawing.Size(478, 174)
        Me.XtraTabPage3.Text = "Earning 1 to 5"
        '
        'PictureEdit15
        '
        Me.PictureEdit15.Cursor = System.Windows.Forms.Cursors.Default
        Me.PictureEdit15.EditValue = CType(resources.GetObject("PictureEdit15.EditValue"), Object)
        Me.PictureEdit15.Location = New System.Drawing.Point(363, 142)
        Me.PictureEdit15.Name = "PictureEdit15"
        Me.PictureEdit15.Properties.AllowFocused = False
        Me.PictureEdit15.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit15.Properties.Appearance.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit15.Properties.Appearance.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit15.Properties.Appearance.Options.UseBackColor = True
        Me.PictureEdit15.Properties.Appearance.Options.UseBorderColor = True
        Me.PictureEdit15.Properties.AppearanceFocused.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit15.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit15.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit15.Properties.AppearanceFocused.Options.UseBackColor = True
        Me.PictureEdit15.Properties.AppearanceFocused.Options.UseBorderColor = True
        Me.PictureEdit15.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PictureEdit15.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.[Auto]
        Me.PictureEdit15.Size = New System.Drawing.Size(23, 19)
        Me.PictureEdit15.TabIndex = 67
        '
        'PictureEdit14
        '
        Me.PictureEdit14.Cursor = System.Windows.Forms.Cursors.Default
        Me.PictureEdit14.EditValue = CType(resources.GetObject("PictureEdit14.EditValue"), Object)
        Me.PictureEdit14.Location = New System.Drawing.Point(363, 116)
        Me.PictureEdit14.Name = "PictureEdit14"
        Me.PictureEdit14.Properties.AllowFocused = False
        Me.PictureEdit14.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit14.Properties.Appearance.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit14.Properties.Appearance.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit14.Properties.Appearance.Options.UseBackColor = True
        Me.PictureEdit14.Properties.Appearance.Options.UseBorderColor = True
        Me.PictureEdit14.Properties.AppearanceFocused.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit14.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit14.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit14.Properties.AppearanceFocused.Options.UseBackColor = True
        Me.PictureEdit14.Properties.AppearanceFocused.Options.UseBorderColor = True
        Me.PictureEdit14.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PictureEdit14.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.[Auto]
        Me.PictureEdit14.Size = New System.Drawing.Size(23, 19)
        Me.PictureEdit14.TabIndex = 66
        '
        'PictureEdit13
        '
        Me.PictureEdit13.Cursor = System.Windows.Forms.Cursors.Default
        Me.PictureEdit13.EditValue = CType(resources.GetObject("PictureEdit13.EditValue"), Object)
        Me.PictureEdit13.Location = New System.Drawing.Point(363, 90)
        Me.PictureEdit13.Name = "PictureEdit13"
        Me.PictureEdit13.Properties.AllowFocused = False
        Me.PictureEdit13.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit13.Properties.Appearance.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit13.Properties.Appearance.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit13.Properties.Appearance.Options.UseBackColor = True
        Me.PictureEdit13.Properties.Appearance.Options.UseBorderColor = True
        Me.PictureEdit13.Properties.AppearanceFocused.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit13.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit13.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit13.Properties.AppearanceFocused.Options.UseBackColor = True
        Me.PictureEdit13.Properties.AppearanceFocused.Options.UseBorderColor = True
        Me.PictureEdit13.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PictureEdit13.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.[Auto]
        Me.PictureEdit13.Size = New System.Drawing.Size(23, 19)
        Me.PictureEdit13.TabIndex = 65
        '
        'PictureEdit12
        '
        Me.PictureEdit12.Cursor = System.Windows.Forms.Cursors.Default
        Me.PictureEdit12.EditValue = CType(resources.GetObject("PictureEdit12.EditValue"), Object)
        Me.PictureEdit12.Location = New System.Drawing.Point(363, 64)
        Me.PictureEdit12.Name = "PictureEdit12"
        Me.PictureEdit12.Properties.AllowFocused = False
        Me.PictureEdit12.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit12.Properties.Appearance.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit12.Properties.Appearance.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit12.Properties.Appearance.Options.UseBackColor = True
        Me.PictureEdit12.Properties.Appearance.Options.UseBorderColor = True
        Me.PictureEdit12.Properties.AppearanceFocused.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit12.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit12.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit12.Properties.AppearanceFocused.Options.UseBackColor = True
        Me.PictureEdit12.Properties.AppearanceFocused.Options.UseBorderColor = True
        Me.PictureEdit12.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PictureEdit12.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.[Auto]
        Me.PictureEdit12.Size = New System.Drawing.Size(23, 19)
        Me.PictureEdit12.TabIndex = 64
        '
        'PictureEdit11
        '
        Me.PictureEdit11.Cursor = System.Windows.Forms.Cursors.Default
        Me.PictureEdit11.EditValue = CType(resources.GetObject("PictureEdit11.EditValue"), Object)
        Me.PictureEdit11.Location = New System.Drawing.Point(363, 38)
        Me.PictureEdit11.Name = "PictureEdit11"
        Me.PictureEdit11.Properties.AllowFocused = False
        Me.PictureEdit11.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit11.Properties.Appearance.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit11.Properties.Appearance.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit11.Properties.Appearance.Options.UseBackColor = True
        Me.PictureEdit11.Properties.Appearance.Options.UseBorderColor = True
        Me.PictureEdit11.Properties.AppearanceFocused.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit11.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit11.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit11.Properties.AppearanceFocused.Options.UseBackColor = True
        Me.PictureEdit11.Properties.AppearanceFocused.Options.UseBorderColor = True
        Me.PictureEdit11.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PictureEdit11.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.[Auto]
        Me.PictureEdit11.Size = New System.Drawing.Size(23, 19)
        Me.PictureEdit11.TabIndex = 63
        '
        'LabelControl38
        '
        Me.LabelControl38.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl38.Appearance.Options.UseFont = True
        Me.LabelControl38.Location = New System.Drawing.Point(25, 144)
        Me.LabelControl38.Name = "LabelControl38"
        Me.LabelControl38.Size = New System.Drawing.Size(7, 14)
        Me.LabelControl38.TabIndex = 60
        Me.LabelControl38.Text = "5"
        '
        'TextEditRateE5
        '
        Me.TextEditRateE5.Location = New System.Drawing.Point(151, 141)
        Me.TextEditRateE5.Name = "TextEditRateE5"
        Me.TextEditRateE5.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditRateE5.Properties.Appearance.Options.UseFont = True
        Me.TextEditRateE5.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEditRateE5.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditRateE5.Properties.MaxLength = 8
        Me.TextEditRateE5.Properties.ReadOnly = True
        Me.TextEditRateE5.Size = New System.Drawing.Size(100, 20)
        Me.TextEditRateE5.TabIndex = 58
        '
        'TextEditDescpE5
        '
        Me.TextEditDescpE5.Location = New System.Drawing.Point(45, 141)
        Me.TextEditDescpE5.Name = "TextEditDescpE5"
        Me.TextEditDescpE5.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditDescpE5.Properties.Appearance.Options.UseFont = True
        Me.TextEditDescpE5.Properties.ReadOnly = True
        Me.TextEditDescpE5.Size = New System.Drawing.Size(100, 20)
        Me.TextEditDescpE5.TabIndex = 57
        '
        'LabelControl39
        '
        Me.LabelControl39.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl39.Appearance.Options.UseFont = True
        Me.LabelControl39.Location = New System.Drawing.Point(25, 118)
        Me.LabelControl39.Name = "LabelControl39"
        Me.LabelControl39.Size = New System.Drawing.Size(7, 14)
        Me.LabelControl39.TabIndex = 56
        Me.LabelControl39.Text = "4"
        '
        'GridLookUpEditFormulaE5
        '
        Me.GridLookUpEditFormulaE5.Location = New System.Drawing.Point(271, 141)
        Me.GridLookUpEditFormulaE5.Name = "GridLookUpEditFormulaE5"
        Me.GridLookUpEditFormulaE5.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.GridLookUpEditFormulaE5.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEditFormulaE5.Properties.Appearance.Options.UseFont = True
        Me.GridLookUpEditFormulaE5.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GridLookUpEditFormulaE5.Properties.DataSource = Me.PAYFORMULABindingSource
        Me.GridLookUpEditFormulaE5.Properties.DisplayMember = "CODE"
        Me.GridLookUpEditFormulaE5.Properties.NullText = ""
        Me.GridLookUpEditFormulaE5.Properties.ValueMember = "CODE"
        Me.GridLookUpEditFormulaE5.Properties.View = Me.GridView5
        Me.GridLookUpEditFormulaE5.Size = New System.Drawing.Size(77, 20)
        Me.GridLookUpEditFormulaE5.TabIndex = 72
        '
        'GridView5
        '
        Me.GridView5.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn9, Me.GridColumn10})
        Me.GridView5.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView5.Name = "GridView5"
        Me.GridView5.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView5.OptionsView.ShowGroupPanel = False
        '
        'GridColumn9
        '
        Me.GridColumn9.Caption = "Code"
        Me.GridColumn9.FieldName = "CODE"
        Me.GridColumn9.Name = "GridColumn9"
        Me.GridColumn9.Visible = True
        Me.GridColumn9.VisibleIndex = 0
        '
        'GridColumn10
        '
        Me.GridColumn10.Caption = "Formula"
        Me.GridColumn10.FieldName = "FORM"
        Me.GridColumn10.Name = "GridColumn10"
        Me.GridColumn10.Visible = True
        Me.GridColumn10.VisibleIndex = 1
        '
        'TextEditRateE4
        '
        Me.TextEditRateE4.Location = New System.Drawing.Point(151, 115)
        Me.TextEditRateE4.Name = "TextEditRateE4"
        Me.TextEditRateE4.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditRateE4.Properties.Appearance.Options.UseFont = True
        Me.TextEditRateE4.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEditRateE4.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditRateE4.Properties.MaxLength = 8
        Me.TextEditRateE4.Properties.ReadOnly = True
        Me.TextEditRateE4.Size = New System.Drawing.Size(100, 20)
        Me.TextEditRateE4.TabIndex = 54
        '
        'GridLookUpEditFormulaE4
        '
        Me.GridLookUpEditFormulaE4.Location = New System.Drawing.Point(271, 115)
        Me.GridLookUpEditFormulaE4.Name = "GridLookUpEditFormulaE4"
        Me.GridLookUpEditFormulaE4.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.GridLookUpEditFormulaE4.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEditFormulaE4.Properties.Appearance.Options.UseFont = True
        Me.GridLookUpEditFormulaE4.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GridLookUpEditFormulaE4.Properties.DataSource = Me.PAYFORMULABindingSource
        Me.GridLookUpEditFormulaE4.Properties.DisplayMember = "CODE"
        Me.GridLookUpEditFormulaE4.Properties.NullText = ""
        Me.GridLookUpEditFormulaE4.Properties.ValueMember = "CODE"
        Me.GridLookUpEditFormulaE4.Properties.View = Me.GridView4
        Me.GridLookUpEditFormulaE4.Size = New System.Drawing.Size(77, 20)
        Me.GridLookUpEditFormulaE4.TabIndex = 71
        '
        'GridView4
        '
        Me.GridView4.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn7, Me.GridColumn8})
        Me.GridView4.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView4.Name = "GridView4"
        Me.GridView4.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView4.OptionsView.ShowGroupPanel = False
        '
        'GridColumn7
        '
        Me.GridColumn7.Caption = "Code"
        Me.GridColumn7.FieldName = "CODE"
        Me.GridColumn7.Name = "GridColumn7"
        Me.GridColumn7.Visible = True
        Me.GridColumn7.VisibleIndex = 0
        '
        'GridColumn8
        '
        Me.GridColumn8.Caption = "Fromula"
        Me.GridColumn8.FieldName = "FORM"
        Me.GridColumn8.Name = "GridColumn8"
        Me.GridColumn8.Visible = True
        Me.GridColumn8.VisibleIndex = 1
        '
        'TextEditDescpE4
        '
        Me.TextEditDescpE4.Location = New System.Drawing.Point(45, 115)
        Me.TextEditDescpE4.Name = "TextEditDescpE4"
        Me.TextEditDescpE4.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditDescpE4.Properties.Appearance.Options.UseFont = True
        Me.TextEditDescpE4.Properties.ReadOnly = True
        Me.TextEditDescpE4.Size = New System.Drawing.Size(100, 20)
        Me.TextEditDescpE4.TabIndex = 53
        '
        'LabelControl40
        '
        Me.LabelControl40.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl40.Appearance.Options.UseFont = True
        Me.LabelControl40.Location = New System.Drawing.Point(25, 92)
        Me.LabelControl40.Name = "LabelControl40"
        Me.LabelControl40.Size = New System.Drawing.Size(7, 14)
        Me.LabelControl40.TabIndex = 52
        Me.LabelControl40.Text = "3"
        '
        'GridLookUpEditFormulaE3
        '
        Me.GridLookUpEditFormulaE3.Location = New System.Drawing.Point(271, 89)
        Me.GridLookUpEditFormulaE3.Name = "GridLookUpEditFormulaE3"
        Me.GridLookUpEditFormulaE3.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.GridLookUpEditFormulaE3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEditFormulaE3.Properties.Appearance.Options.UseFont = True
        Me.GridLookUpEditFormulaE3.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GridLookUpEditFormulaE3.Properties.DataSource = Me.PAYFORMULABindingSource
        Me.GridLookUpEditFormulaE3.Properties.DisplayMember = "CODE"
        Me.GridLookUpEditFormulaE3.Properties.NullText = ""
        Me.GridLookUpEditFormulaE3.Properties.ValueMember = "CODE"
        Me.GridLookUpEditFormulaE3.Properties.View = Me.GridView3
        Me.GridLookUpEditFormulaE3.Size = New System.Drawing.Size(77, 20)
        Me.GridLookUpEditFormulaE3.TabIndex = 70
        '
        'GridView3
        '
        Me.GridView3.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn5, Me.GridColumn6})
        Me.GridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView3.Name = "GridView3"
        Me.GridView3.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView3.OptionsView.ShowGroupPanel = False
        '
        'GridColumn5
        '
        Me.GridColumn5.Caption = "Code"
        Me.GridColumn5.FieldName = "CODE"
        Me.GridColumn5.Name = "GridColumn5"
        Me.GridColumn5.Visible = True
        Me.GridColumn5.VisibleIndex = 0
        '
        'GridColumn6
        '
        Me.GridColumn6.Caption = "Formula"
        Me.GridColumn6.FieldName = "FORM"
        Me.GridColumn6.Name = "GridColumn6"
        Me.GridColumn6.Visible = True
        Me.GridColumn6.VisibleIndex = 1
        '
        'GridLookUpEditFormulaE2
        '
        Me.GridLookUpEditFormulaE2.Location = New System.Drawing.Point(271, 63)
        Me.GridLookUpEditFormulaE2.Name = "GridLookUpEditFormulaE2"
        Me.GridLookUpEditFormulaE2.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.GridLookUpEditFormulaE2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEditFormulaE2.Properties.Appearance.Options.UseFont = True
        Me.GridLookUpEditFormulaE2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GridLookUpEditFormulaE2.Properties.DataSource = Me.PAYFORMULABindingSource
        Me.GridLookUpEditFormulaE2.Properties.DisplayMember = "CODE"
        Me.GridLookUpEditFormulaE2.Properties.NullText = ""
        Me.GridLookUpEditFormulaE2.Properties.ValueMember = "CODE"
        Me.GridLookUpEditFormulaE2.Properties.View = Me.GridView11
        Me.GridLookUpEditFormulaE2.Size = New System.Drawing.Size(77, 20)
        Me.GridLookUpEditFormulaE2.TabIndex = 69
        '
        'GridView11
        '
        Me.GridView11.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn3, Me.GridColumn4})
        Me.GridView11.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView11.Name = "GridView11"
        Me.GridView11.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView11.OptionsView.ShowGroupPanel = False
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "Code"
        Me.GridColumn3.FieldName = "CODE"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.Visible = True
        Me.GridColumn3.VisibleIndex = 0
        '
        'GridColumn4
        '
        Me.GridColumn4.Caption = "Formula"
        Me.GridColumn4.FieldName = "FORM"
        Me.GridColumn4.Name = "GridColumn4"
        Me.GridColumn4.Visible = True
        Me.GridColumn4.VisibleIndex = 1
        '
        'TextEditRateE3
        '
        Me.TextEditRateE3.Location = New System.Drawing.Point(151, 89)
        Me.TextEditRateE3.Name = "TextEditRateE3"
        Me.TextEditRateE3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditRateE3.Properties.Appearance.Options.UseFont = True
        Me.TextEditRateE3.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEditRateE3.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditRateE3.Properties.MaxLength = 8
        Me.TextEditRateE3.Properties.ReadOnly = True
        Me.TextEditRateE3.Size = New System.Drawing.Size(100, 20)
        Me.TextEditRateE3.TabIndex = 50
        '
        'TextEditDescpE3
        '
        Me.TextEditDescpE3.Location = New System.Drawing.Point(45, 89)
        Me.TextEditDescpE3.Name = "TextEditDescpE3"
        Me.TextEditDescpE3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditDescpE3.Properties.Appearance.Options.UseFont = True
        Me.TextEditDescpE3.Properties.ReadOnly = True
        Me.TextEditDescpE3.Size = New System.Drawing.Size(100, 20)
        Me.TextEditDescpE3.TabIndex = 49
        '
        'GridLookUpEditFormulaE1
        '
        Me.GridLookUpEditFormulaE1.Location = New System.Drawing.Point(271, 37)
        Me.GridLookUpEditFormulaE1.Name = "GridLookUpEditFormulaE1"
        Me.GridLookUpEditFormulaE1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.GridLookUpEditFormulaE1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEditFormulaE1.Properties.Appearance.Options.UseFont = True
        Me.GridLookUpEditFormulaE1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GridLookUpEditFormulaE1.Properties.DataSource = Me.PAYFORMULABindingSource
        Me.GridLookUpEditFormulaE1.Properties.DisplayMember = "CODE"
        Me.GridLookUpEditFormulaE1.Properties.NullText = ""
        Me.GridLookUpEditFormulaE1.Properties.ValueMember = "CODE"
        Me.GridLookUpEditFormulaE1.Properties.View = Me.GridView12
        Me.GridLookUpEditFormulaE1.Size = New System.Drawing.Size(77, 20)
        Me.GridLookUpEditFormulaE1.TabIndex = 68
        '
        'GridView12
        '
        Me.GridView12.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn21, Me.GridColumn22})
        Me.GridView12.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView12.Name = "GridView12"
        Me.GridView12.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView12.OptionsView.ShowGroupPanel = False
        '
        'GridColumn21
        '
        Me.GridColumn21.Caption = "Code"
        Me.GridColumn21.FieldName = "CODE"
        Me.GridColumn21.Name = "GridColumn21"
        Me.GridColumn21.Visible = True
        Me.GridColumn21.VisibleIndex = 0
        '
        'GridColumn22
        '
        Me.GridColumn22.Caption = "Formula"
        Me.GridColumn22.FieldName = "FORM"
        Me.GridColumn22.Name = "GridColumn22"
        Me.GridColumn22.Visible = True
        Me.GridColumn22.VisibleIndex = 1
        '
        'LabelControl41
        '
        Me.LabelControl41.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl41.Appearance.Options.UseFont = True
        Me.LabelControl41.Location = New System.Drawing.Point(25, 66)
        Me.LabelControl41.Name = "LabelControl41"
        Me.LabelControl41.Size = New System.Drawing.Size(7, 14)
        Me.LabelControl41.TabIndex = 48
        Me.LabelControl41.Text = "2"
        '
        'TextEditRateE2
        '
        Me.TextEditRateE2.Location = New System.Drawing.Point(151, 63)
        Me.TextEditRateE2.Name = "TextEditRateE2"
        Me.TextEditRateE2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditRateE2.Properties.Appearance.Options.UseFont = True
        Me.TextEditRateE2.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEditRateE2.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditRateE2.Properties.MaxLength = 8
        Me.TextEditRateE2.Properties.ReadOnly = True
        Me.TextEditRateE2.Size = New System.Drawing.Size(100, 20)
        Me.TextEditRateE2.TabIndex = 46
        '
        'TextEditDescpE2
        '
        Me.TextEditDescpE2.Location = New System.Drawing.Point(45, 63)
        Me.TextEditDescpE2.Name = "TextEditDescpE2"
        Me.TextEditDescpE2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditDescpE2.Properties.Appearance.Options.UseFont = True
        Me.TextEditDescpE2.Properties.ReadOnly = True
        Me.TextEditDescpE2.Size = New System.Drawing.Size(100, 20)
        Me.TextEditDescpE2.TabIndex = 45
        '
        'LabelControl42
        '
        Me.LabelControl42.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl42.Appearance.Options.UseFont = True
        Me.LabelControl42.Location = New System.Drawing.Point(25, 40)
        Me.LabelControl42.Name = "LabelControl42"
        Me.LabelControl42.Size = New System.Drawing.Size(7, 14)
        Me.LabelControl42.TabIndex = 44
        Me.LabelControl42.Text = "1"
        '
        'LabelControl43
        '
        Me.LabelControl43.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl43.Appearance.Options.UseFont = True
        Me.LabelControl43.Location = New System.Drawing.Point(287, 17)
        Me.LabelControl43.Name = "LabelControl43"
        Me.LabelControl43.Size = New System.Drawing.Size(42, 14)
        Me.LabelControl43.TabIndex = 42
        Me.LabelControl43.Text = "Formula"
        '
        'LabelControl44
        '
        Me.LabelControl44.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl44.Appearance.Options.UseFont = True
        Me.LabelControl44.Location = New System.Drawing.Point(167, 17)
        Me.LabelControl44.Name = "LabelControl44"
        Me.LabelControl44.Size = New System.Drawing.Size(61, 14)
        Me.LabelControl44.TabIndex = 41
        Me.LabelControl44.Text = "Rate / Amt"
        '
        'TextEditRateE1
        '
        Me.TextEditRateE1.Location = New System.Drawing.Point(151, 37)
        Me.TextEditRateE1.Name = "TextEditRateE1"
        Me.TextEditRateE1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditRateE1.Properties.Appearance.Options.UseFont = True
        Me.TextEditRateE1.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEditRateE1.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditRateE1.Properties.MaxLength = 8
        Me.TextEditRateE1.Properties.ReadOnly = True
        Me.TextEditRateE1.Size = New System.Drawing.Size(100, 20)
        Me.TextEditRateE1.TabIndex = 40
        '
        'TextEditDescpE1
        '
        Me.TextEditDescpE1.Location = New System.Drawing.Point(45, 37)
        Me.TextEditDescpE1.Name = "TextEditDescpE1"
        Me.TextEditDescpE1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditDescpE1.Properties.Appearance.Options.UseFont = True
        Me.TextEditDescpE1.Properties.ReadOnly = True
        Me.TextEditDescpE1.Size = New System.Drawing.Size(100, 20)
        Me.TextEditDescpE1.TabIndex = 39
        '
        'LabelControl45
        '
        Me.LabelControl45.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl45.Appearance.Options.UseFont = True
        Me.LabelControl45.Location = New System.Drawing.Point(64, 17)
        Me.LabelControl45.Name = "LabelControl45"
        Me.LabelControl45.Size = New System.Drawing.Size(60, 14)
        Me.LabelControl45.TabIndex = 38
        Me.LabelControl45.Text = "Description"
        '
        'XtraTabPage4
        '
        Me.XtraTabPage4.Controls.Add(Me.GridLookUpEditFormulaE10)
        Me.XtraTabPage4.Controls.Add(Me.PictureEdit20)
        Me.XtraTabPage4.Controls.Add(Me.PictureEdit19)
        Me.XtraTabPage4.Controls.Add(Me.GridLookUpEditFormulaE9)
        Me.XtraTabPage4.Controls.Add(Me.PictureEdit18)
        Me.XtraTabPage4.Controls.Add(Me.PictureEdit17)
        Me.XtraTabPage4.Controls.Add(Me.GridLookUpEditFormulaE8)
        Me.XtraTabPage4.Controls.Add(Me.PictureEdit16)
        Me.XtraTabPage4.Controls.Add(Me.LabelControl46)
        Me.XtraTabPage4.Controls.Add(Me.GridLookUpEditFormulaE7)
        Me.XtraTabPage4.Controls.Add(Me.TextEditRateE10)
        Me.XtraTabPage4.Controls.Add(Me.GridLookUpEditFormulaE6)
        Me.XtraTabPage4.Controls.Add(Me.TextEditDescpE10)
        Me.XtraTabPage4.Controls.Add(Me.LabelControl47)
        Me.XtraTabPage4.Controls.Add(Me.TextEditRateE9)
        Me.XtraTabPage4.Controls.Add(Me.TextEditDescpE9)
        Me.XtraTabPage4.Controls.Add(Me.LabelControl48)
        Me.XtraTabPage4.Controls.Add(Me.TextEditRateE8)
        Me.XtraTabPage4.Controls.Add(Me.TextEditDescpE8)
        Me.XtraTabPage4.Controls.Add(Me.LabelControl49)
        Me.XtraTabPage4.Controls.Add(Me.TextEditRateE7)
        Me.XtraTabPage4.Controls.Add(Me.TextEditDescpE7)
        Me.XtraTabPage4.Controls.Add(Me.LabelControl50)
        Me.XtraTabPage4.Controls.Add(Me.LabelControl51)
        Me.XtraTabPage4.Controls.Add(Me.LabelControl52)
        Me.XtraTabPage4.Controls.Add(Me.TextEditRateE6)
        Me.XtraTabPage4.Controls.Add(Me.TextEditDescpE6)
        Me.XtraTabPage4.Controls.Add(Me.LabelControl53)
        Me.XtraTabPage4.Name = "XtraTabPage4"
        Me.XtraTabPage4.Size = New System.Drawing.Size(478, 174)
        Me.XtraTabPage4.Text = "Earning 6 to 10"
        '
        'GridLookUpEditFormulaE10
        '
        Me.GridLookUpEditFormulaE10.Location = New System.Drawing.Point(271, 141)
        Me.GridLookUpEditFormulaE10.Name = "GridLookUpEditFormulaE10"
        Me.GridLookUpEditFormulaE10.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.GridLookUpEditFormulaE10.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEditFormulaE10.Properties.Appearance.Options.UseFont = True
        Me.GridLookUpEditFormulaE10.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GridLookUpEditFormulaE10.Properties.DataSource = Me.PAYFORMULABindingSource
        Me.GridLookUpEditFormulaE10.Properties.DisplayMember = "CODE"
        Me.GridLookUpEditFormulaE10.Properties.NullText = ""
        Me.GridLookUpEditFormulaE10.Properties.ValueMember = "CODE"
        Me.GridLookUpEditFormulaE10.Properties.View = Me.GridView10
        Me.GridLookUpEditFormulaE10.Size = New System.Drawing.Size(77, 20)
        Me.GridLookUpEditFormulaE10.TabIndex = 77
        '
        'GridView10
        '
        Me.GridView10.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn19, Me.GridColumn20})
        Me.GridView10.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView10.Name = "GridView10"
        Me.GridView10.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView10.OptionsView.ShowGroupPanel = False
        '
        'GridColumn19
        '
        Me.GridColumn19.Caption = "Code"
        Me.GridColumn19.FieldName = "CODE"
        Me.GridColumn19.Name = "GridColumn19"
        Me.GridColumn19.Visible = True
        Me.GridColumn19.VisibleIndex = 0
        '
        'GridColumn20
        '
        Me.GridColumn20.Caption = "Formula"
        Me.GridColumn20.FieldName = "FORM"
        Me.GridColumn20.Name = "GridColumn20"
        Me.GridColumn20.Visible = True
        Me.GridColumn20.VisibleIndex = 1
        '
        'PictureEdit20
        '
        Me.PictureEdit20.Cursor = System.Windows.Forms.Cursors.Default
        Me.PictureEdit20.EditValue = CType(resources.GetObject("PictureEdit20.EditValue"), Object)
        Me.PictureEdit20.Location = New System.Drawing.Point(363, 142)
        Me.PictureEdit20.Name = "PictureEdit20"
        Me.PictureEdit20.Properties.AllowFocused = False
        Me.PictureEdit20.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit20.Properties.Appearance.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit20.Properties.Appearance.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit20.Properties.Appearance.Options.UseBackColor = True
        Me.PictureEdit20.Properties.Appearance.Options.UseBorderColor = True
        Me.PictureEdit20.Properties.AppearanceFocused.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit20.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit20.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit20.Properties.AppearanceFocused.Options.UseBackColor = True
        Me.PictureEdit20.Properties.AppearanceFocused.Options.UseBorderColor = True
        Me.PictureEdit20.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PictureEdit20.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.[Auto]
        Me.PictureEdit20.Size = New System.Drawing.Size(23, 19)
        Me.PictureEdit20.TabIndex = 88
        '
        'PictureEdit19
        '
        Me.PictureEdit19.Cursor = System.Windows.Forms.Cursors.Default
        Me.PictureEdit19.EditValue = CType(resources.GetObject("PictureEdit19.EditValue"), Object)
        Me.PictureEdit19.Location = New System.Drawing.Point(363, 116)
        Me.PictureEdit19.Name = "PictureEdit19"
        Me.PictureEdit19.Properties.AllowFocused = False
        Me.PictureEdit19.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit19.Properties.Appearance.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit19.Properties.Appearance.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit19.Properties.Appearance.Options.UseBackColor = True
        Me.PictureEdit19.Properties.Appearance.Options.UseBorderColor = True
        Me.PictureEdit19.Properties.AppearanceFocused.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit19.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit19.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit19.Properties.AppearanceFocused.Options.UseBackColor = True
        Me.PictureEdit19.Properties.AppearanceFocused.Options.UseBorderColor = True
        Me.PictureEdit19.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PictureEdit19.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.[Auto]
        Me.PictureEdit19.Size = New System.Drawing.Size(23, 19)
        Me.PictureEdit19.TabIndex = 87
        '
        'GridLookUpEditFormulaE9
        '
        Me.GridLookUpEditFormulaE9.Location = New System.Drawing.Point(271, 115)
        Me.GridLookUpEditFormulaE9.Name = "GridLookUpEditFormulaE9"
        Me.GridLookUpEditFormulaE9.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.GridLookUpEditFormulaE9.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEditFormulaE9.Properties.Appearance.Options.UseFont = True
        Me.GridLookUpEditFormulaE9.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GridLookUpEditFormulaE9.Properties.DataSource = Me.PAYFORMULABindingSource
        Me.GridLookUpEditFormulaE9.Properties.DisplayMember = "CODE"
        Me.GridLookUpEditFormulaE9.Properties.NullText = ""
        Me.GridLookUpEditFormulaE9.Properties.ValueMember = "CODE"
        Me.GridLookUpEditFormulaE9.Properties.View = Me.GridView9
        Me.GridLookUpEditFormulaE9.Size = New System.Drawing.Size(77, 20)
        Me.GridLookUpEditFormulaE9.TabIndex = 76
        '
        'GridView9
        '
        Me.GridView9.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn17, Me.GridColumn18})
        Me.GridView9.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView9.Name = "GridView9"
        Me.GridView9.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView9.OptionsView.ShowGroupPanel = False
        '
        'GridColumn17
        '
        Me.GridColumn17.Caption = "Code"
        Me.GridColumn17.FieldName = "CODE"
        Me.GridColumn17.Name = "GridColumn17"
        Me.GridColumn17.Visible = True
        Me.GridColumn17.VisibleIndex = 0
        '
        'GridColumn18
        '
        Me.GridColumn18.Caption = "Formula"
        Me.GridColumn18.FieldName = "FORM"
        Me.GridColumn18.Name = "GridColumn18"
        Me.GridColumn18.Visible = True
        Me.GridColumn18.VisibleIndex = 1
        '
        'PictureEdit18
        '
        Me.PictureEdit18.Cursor = System.Windows.Forms.Cursors.Default
        Me.PictureEdit18.EditValue = CType(resources.GetObject("PictureEdit18.EditValue"), Object)
        Me.PictureEdit18.Location = New System.Drawing.Point(363, 90)
        Me.PictureEdit18.Name = "PictureEdit18"
        Me.PictureEdit18.Properties.AllowFocused = False
        Me.PictureEdit18.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit18.Properties.Appearance.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit18.Properties.Appearance.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit18.Properties.Appearance.Options.UseBackColor = True
        Me.PictureEdit18.Properties.Appearance.Options.UseBorderColor = True
        Me.PictureEdit18.Properties.AppearanceFocused.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit18.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit18.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit18.Properties.AppearanceFocused.Options.UseBackColor = True
        Me.PictureEdit18.Properties.AppearanceFocused.Options.UseBorderColor = True
        Me.PictureEdit18.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PictureEdit18.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.[Auto]
        Me.PictureEdit18.Size = New System.Drawing.Size(23, 19)
        Me.PictureEdit18.TabIndex = 86
        '
        'PictureEdit17
        '
        Me.PictureEdit17.Cursor = System.Windows.Forms.Cursors.Default
        Me.PictureEdit17.EditValue = CType(resources.GetObject("PictureEdit17.EditValue"), Object)
        Me.PictureEdit17.Location = New System.Drawing.Point(363, 64)
        Me.PictureEdit17.Name = "PictureEdit17"
        Me.PictureEdit17.Properties.AllowFocused = False
        Me.PictureEdit17.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit17.Properties.Appearance.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit17.Properties.Appearance.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit17.Properties.Appearance.Options.UseBackColor = True
        Me.PictureEdit17.Properties.Appearance.Options.UseBorderColor = True
        Me.PictureEdit17.Properties.AppearanceFocused.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit17.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit17.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit17.Properties.AppearanceFocused.Options.UseBackColor = True
        Me.PictureEdit17.Properties.AppearanceFocused.Options.UseBorderColor = True
        Me.PictureEdit17.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PictureEdit17.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.[Auto]
        Me.PictureEdit17.Size = New System.Drawing.Size(23, 19)
        Me.PictureEdit17.TabIndex = 85
        '
        'GridLookUpEditFormulaE8
        '
        Me.GridLookUpEditFormulaE8.Location = New System.Drawing.Point(271, 89)
        Me.GridLookUpEditFormulaE8.Name = "GridLookUpEditFormulaE8"
        Me.GridLookUpEditFormulaE8.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.GridLookUpEditFormulaE8.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEditFormulaE8.Properties.Appearance.Options.UseFont = True
        Me.GridLookUpEditFormulaE8.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GridLookUpEditFormulaE8.Properties.DataSource = Me.PAYFORMULABindingSource
        Me.GridLookUpEditFormulaE8.Properties.DisplayMember = "CODE"
        Me.GridLookUpEditFormulaE8.Properties.NullText = ""
        Me.GridLookUpEditFormulaE8.Properties.ValueMember = "CODE"
        Me.GridLookUpEditFormulaE8.Properties.View = Me.GridView8
        Me.GridLookUpEditFormulaE8.Size = New System.Drawing.Size(77, 20)
        Me.GridLookUpEditFormulaE8.TabIndex = 75
        '
        'GridView8
        '
        Me.GridView8.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn15, Me.GridColumn16})
        Me.GridView8.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView8.Name = "GridView8"
        Me.GridView8.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView8.OptionsView.ShowGroupPanel = False
        '
        'GridColumn15
        '
        Me.GridColumn15.Caption = "Code"
        Me.GridColumn15.FieldName = "CODE"
        Me.GridColumn15.Name = "GridColumn15"
        Me.GridColumn15.Visible = True
        Me.GridColumn15.VisibleIndex = 0
        '
        'GridColumn16
        '
        Me.GridColumn16.Caption = "Formula"
        Me.GridColumn16.FieldName = "FORM"
        Me.GridColumn16.Name = "GridColumn16"
        Me.GridColumn16.Visible = True
        Me.GridColumn16.VisibleIndex = 1
        '
        'PictureEdit16
        '
        Me.PictureEdit16.Cursor = System.Windows.Forms.Cursors.Default
        Me.PictureEdit16.EditValue = CType(resources.GetObject("PictureEdit16.EditValue"), Object)
        Me.PictureEdit16.Location = New System.Drawing.Point(363, 38)
        Me.PictureEdit16.Name = "PictureEdit16"
        Me.PictureEdit16.Properties.AllowFocused = False
        Me.PictureEdit16.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit16.Properties.Appearance.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit16.Properties.Appearance.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit16.Properties.Appearance.Options.UseBackColor = True
        Me.PictureEdit16.Properties.Appearance.Options.UseBorderColor = True
        Me.PictureEdit16.Properties.AppearanceFocused.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit16.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit16.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit16.Properties.AppearanceFocused.Options.UseBackColor = True
        Me.PictureEdit16.Properties.AppearanceFocused.Options.UseBorderColor = True
        Me.PictureEdit16.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PictureEdit16.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.[Auto]
        Me.PictureEdit16.Size = New System.Drawing.Size(23, 19)
        Me.PictureEdit16.TabIndex = 84
        '
        'LabelControl46
        '
        Me.LabelControl46.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl46.Appearance.Options.UseFont = True
        Me.LabelControl46.Location = New System.Drawing.Point(25, 144)
        Me.LabelControl46.Name = "LabelControl46"
        Me.LabelControl46.Size = New System.Drawing.Size(14, 14)
        Me.LabelControl46.TabIndex = 83
        Me.LabelControl46.Text = "10"
        '
        'GridLookUpEditFormulaE7
        '
        Me.GridLookUpEditFormulaE7.Location = New System.Drawing.Point(271, 63)
        Me.GridLookUpEditFormulaE7.Name = "GridLookUpEditFormulaE7"
        Me.GridLookUpEditFormulaE7.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.GridLookUpEditFormulaE7.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEditFormulaE7.Properties.Appearance.Options.UseFont = True
        Me.GridLookUpEditFormulaE7.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GridLookUpEditFormulaE7.Properties.DataSource = Me.PAYFORMULABindingSource
        Me.GridLookUpEditFormulaE7.Properties.DisplayMember = "CODE"
        Me.GridLookUpEditFormulaE7.Properties.NullText = ""
        Me.GridLookUpEditFormulaE7.Properties.ValueMember = "CODE"
        Me.GridLookUpEditFormulaE7.Properties.View = Me.GridView7
        Me.GridLookUpEditFormulaE7.Size = New System.Drawing.Size(77, 20)
        Me.GridLookUpEditFormulaE7.TabIndex = 74
        '
        'GridView7
        '
        Me.GridView7.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn13, Me.GridColumn14})
        Me.GridView7.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView7.Name = "GridView7"
        Me.GridView7.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView7.OptionsView.ShowGroupPanel = False
        '
        'GridColumn13
        '
        Me.GridColumn13.Caption = "Code"
        Me.GridColumn13.FieldName = "CODE"
        Me.GridColumn13.Name = "GridColumn13"
        Me.GridColumn13.Visible = True
        Me.GridColumn13.VisibleIndex = 0
        '
        'GridColumn14
        '
        Me.GridColumn14.Caption = "Formula"
        Me.GridColumn14.FieldName = "FORM"
        Me.GridColumn14.Name = "GridColumn14"
        Me.GridColumn14.Visible = True
        Me.GridColumn14.VisibleIndex = 1
        '
        'TextEditRateE10
        '
        Me.TextEditRateE10.Location = New System.Drawing.Point(151, 141)
        Me.TextEditRateE10.Name = "TextEditRateE10"
        Me.TextEditRateE10.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditRateE10.Properties.Appearance.Options.UseFont = True
        Me.TextEditRateE10.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEditRateE10.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditRateE10.Properties.MaxLength = 8
        Me.TextEditRateE10.Properties.ReadOnly = True
        Me.TextEditRateE10.Size = New System.Drawing.Size(100, 20)
        Me.TextEditRateE10.TabIndex = 81
        '
        'GridLookUpEditFormulaE6
        '
        Me.GridLookUpEditFormulaE6.Location = New System.Drawing.Point(271, 37)
        Me.GridLookUpEditFormulaE6.Name = "GridLookUpEditFormulaE6"
        Me.GridLookUpEditFormulaE6.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.GridLookUpEditFormulaE6.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEditFormulaE6.Properties.Appearance.Options.UseFont = True
        Me.GridLookUpEditFormulaE6.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GridLookUpEditFormulaE6.Properties.DataSource = Me.PAYFORMULABindingSource
        Me.GridLookUpEditFormulaE6.Properties.DisplayMember = "CODE"
        Me.GridLookUpEditFormulaE6.Properties.NullText = ""
        Me.GridLookUpEditFormulaE6.Properties.ValueMember = "CODE"
        Me.GridLookUpEditFormulaE6.Properties.View = Me.GridView2
        Me.GridLookUpEditFormulaE6.Size = New System.Drawing.Size(77, 20)
        Me.GridLookUpEditFormulaE6.TabIndex = 73
        '
        'GridView2
        '
        Me.GridView2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn11, Me.GridColumn12})
        Me.GridView2.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView2.Name = "GridView2"
        Me.GridView2.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView2.OptionsView.ShowGroupPanel = False
        '
        'GridColumn11
        '
        Me.GridColumn11.Caption = "Code"
        Me.GridColumn11.FieldName = "CODE"
        Me.GridColumn11.Name = "GridColumn11"
        Me.GridColumn11.Visible = True
        Me.GridColumn11.VisibleIndex = 0
        '
        'GridColumn12
        '
        Me.GridColumn12.Caption = "Formula"
        Me.GridColumn12.FieldName = "FORM"
        Me.GridColumn12.Name = "GridColumn12"
        Me.GridColumn12.Visible = True
        Me.GridColumn12.VisibleIndex = 1
        '
        'TextEditDescpE10
        '
        Me.TextEditDescpE10.Location = New System.Drawing.Point(45, 141)
        Me.TextEditDescpE10.Name = "TextEditDescpE10"
        Me.TextEditDescpE10.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditDescpE10.Properties.Appearance.Options.UseFont = True
        Me.TextEditDescpE10.Properties.ReadOnly = True
        Me.TextEditDescpE10.Size = New System.Drawing.Size(100, 20)
        Me.TextEditDescpE10.TabIndex = 80
        '
        'LabelControl47
        '
        Me.LabelControl47.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl47.Appearance.Options.UseFont = True
        Me.LabelControl47.Location = New System.Drawing.Point(25, 118)
        Me.LabelControl47.Name = "LabelControl47"
        Me.LabelControl47.Size = New System.Drawing.Size(7, 14)
        Me.LabelControl47.TabIndex = 79
        Me.LabelControl47.Text = "9"
        '
        'TextEditRateE9
        '
        Me.TextEditRateE9.Location = New System.Drawing.Point(151, 115)
        Me.TextEditRateE9.Name = "TextEditRateE9"
        Me.TextEditRateE9.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditRateE9.Properties.Appearance.Options.UseFont = True
        Me.TextEditRateE9.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEditRateE9.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditRateE9.Properties.MaxLength = 8
        Me.TextEditRateE9.Properties.ReadOnly = True
        Me.TextEditRateE9.Size = New System.Drawing.Size(100, 20)
        Me.TextEditRateE9.TabIndex = 77
        '
        'TextEditDescpE9
        '
        Me.TextEditDescpE9.Location = New System.Drawing.Point(45, 115)
        Me.TextEditDescpE9.Name = "TextEditDescpE9"
        Me.TextEditDescpE9.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditDescpE9.Properties.Appearance.Options.UseFont = True
        Me.TextEditDescpE9.Properties.ReadOnly = True
        Me.TextEditDescpE9.Size = New System.Drawing.Size(100, 20)
        Me.TextEditDescpE9.TabIndex = 76
        '
        'LabelControl48
        '
        Me.LabelControl48.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl48.Appearance.Options.UseFont = True
        Me.LabelControl48.Location = New System.Drawing.Point(25, 92)
        Me.LabelControl48.Name = "LabelControl48"
        Me.LabelControl48.Size = New System.Drawing.Size(7, 14)
        Me.LabelControl48.TabIndex = 75
        Me.LabelControl48.Text = "8"
        '
        'TextEditRateE8
        '
        Me.TextEditRateE8.Location = New System.Drawing.Point(151, 89)
        Me.TextEditRateE8.Name = "TextEditRateE8"
        Me.TextEditRateE8.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditRateE8.Properties.Appearance.Options.UseFont = True
        Me.TextEditRateE8.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEditRateE8.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditRateE8.Properties.MaxLength = 8
        Me.TextEditRateE8.Properties.ReadOnly = True
        Me.TextEditRateE8.Size = New System.Drawing.Size(100, 20)
        Me.TextEditRateE8.TabIndex = 73
        '
        'TextEditDescpE8
        '
        Me.TextEditDescpE8.Location = New System.Drawing.Point(45, 89)
        Me.TextEditDescpE8.Name = "TextEditDescpE8"
        Me.TextEditDescpE8.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditDescpE8.Properties.Appearance.Options.UseFont = True
        Me.TextEditDescpE8.Properties.ReadOnly = True
        Me.TextEditDescpE8.Size = New System.Drawing.Size(100, 20)
        Me.TextEditDescpE8.TabIndex = 72
        '
        'LabelControl49
        '
        Me.LabelControl49.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl49.Appearance.Options.UseFont = True
        Me.LabelControl49.Location = New System.Drawing.Point(25, 66)
        Me.LabelControl49.Name = "LabelControl49"
        Me.LabelControl49.Size = New System.Drawing.Size(7, 14)
        Me.LabelControl49.TabIndex = 71
        Me.LabelControl49.Text = "7"
        '
        'TextEditRateE7
        '
        Me.TextEditRateE7.Location = New System.Drawing.Point(151, 63)
        Me.TextEditRateE7.Name = "TextEditRateE7"
        Me.TextEditRateE7.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditRateE7.Properties.Appearance.Options.UseFont = True
        Me.TextEditRateE7.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEditRateE7.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditRateE7.Properties.MaxLength = 8
        Me.TextEditRateE7.Properties.ReadOnly = True
        Me.TextEditRateE7.Size = New System.Drawing.Size(100, 20)
        Me.TextEditRateE7.TabIndex = 69
        '
        'TextEditDescpE7
        '
        Me.TextEditDescpE7.Location = New System.Drawing.Point(45, 63)
        Me.TextEditDescpE7.Name = "TextEditDescpE7"
        Me.TextEditDescpE7.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditDescpE7.Properties.Appearance.Options.UseFont = True
        Me.TextEditDescpE7.Properties.ReadOnly = True
        Me.TextEditDescpE7.Size = New System.Drawing.Size(100, 20)
        Me.TextEditDescpE7.TabIndex = 68
        '
        'LabelControl50
        '
        Me.LabelControl50.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl50.Appearance.Options.UseFont = True
        Me.LabelControl50.Location = New System.Drawing.Point(25, 40)
        Me.LabelControl50.Name = "LabelControl50"
        Me.LabelControl50.Size = New System.Drawing.Size(7, 14)
        Me.LabelControl50.TabIndex = 67
        Me.LabelControl50.Text = "6"
        '
        'LabelControl51
        '
        Me.LabelControl51.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl51.Appearance.Options.UseFont = True
        Me.LabelControl51.Location = New System.Drawing.Point(287, 17)
        Me.LabelControl51.Name = "LabelControl51"
        Me.LabelControl51.Size = New System.Drawing.Size(42, 14)
        Me.LabelControl51.TabIndex = 65
        Me.LabelControl51.Text = "Formula"
        '
        'LabelControl52
        '
        Me.LabelControl52.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl52.Appearance.Options.UseFont = True
        Me.LabelControl52.Location = New System.Drawing.Point(167, 17)
        Me.LabelControl52.Name = "LabelControl52"
        Me.LabelControl52.Size = New System.Drawing.Size(61, 14)
        Me.LabelControl52.TabIndex = 64
        Me.LabelControl52.Text = "Rate / Amt"
        '
        'TextEditRateE6
        '
        Me.TextEditRateE6.Location = New System.Drawing.Point(151, 37)
        Me.TextEditRateE6.Name = "TextEditRateE6"
        Me.TextEditRateE6.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditRateE6.Properties.Appearance.Options.UseFont = True
        Me.TextEditRateE6.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEditRateE6.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditRateE6.Properties.MaxLength = 8
        Me.TextEditRateE6.Properties.ReadOnly = True
        Me.TextEditRateE6.Size = New System.Drawing.Size(100, 20)
        Me.TextEditRateE6.TabIndex = 63
        '
        'TextEditDescpE6
        '
        Me.TextEditDescpE6.Location = New System.Drawing.Point(45, 37)
        Me.TextEditDescpE6.Name = "TextEditDescpE6"
        Me.TextEditDescpE6.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditDescpE6.Properties.Appearance.Options.UseFont = True
        Me.TextEditDescpE6.Properties.ReadOnly = True
        Me.TextEditDescpE6.Size = New System.Drawing.Size(100, 20)
        Me.TextEditDescpE6.TabIndex = 62
        '
        'LabelControl53
        '
        Me.LabelControl53.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl53.Appearance.Options.UseFont = True
        Me.LabelControl53.Location = New System.Drawing.Point(64, 17)
        Me.LabelControl53.Name = "LabelControl53"
        Me.LabelControl53.Size = New System.Drawing.Size(60, 14)
        Me.LabelControl53.TabIndex = 61
        Me.LabelControl53.Text = "Description"
        '
        'SidePanel4
        '
        Me.SidePanel4.Controls.Add(Me.SimpleButton2)
        Me.SidePanel4.Controls.Add(Me.SimpleButtonSave)
        Me.SidePanel4.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.SidePanel4.Location = New System.Drawing.Point(0, 480)
        Me.SidePanel4.Name = "SidePanel4"
        Me.SidePanel4.Size = New System.Drawing.Size(975, 38)
        Me.SidePanel4.TabIndex = 64
        Me.SidePanel4.Text = "SidePanel4"
        '
        'SimpleButton2
        '
        Me.SimpleButton2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButton2.Appearance.Options.UseFont = True
        Me.SimpleButton2.Location = New System.Drawing.Point(888, 6)
        Me.SimpleButton2.Name = "SimpleButton2"
        Me.SimpleButton2.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton2.TabIndex = 106
        Me.SimpleButton2.Text = "Cancel"
        '
        'SimpleButtonSave
        '
        Me.SimpleButtonSave.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonSave.Appearance.Options.UseFont = True
        Me.SimpleButtonSave.Location = New System.Drawing.Point(807, 6)
        Me.SimpleButtonSave.Name = "SimpleButtonSave"
        Me.SimpleButtonSave.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButtonSave.TabIndex = 105
        Me.SimpleButtonSave.Text = "Save"
        '
        'XtraTabControl1
        '
        Me.XtraTabControl1.AppearancePage.Header.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.XtraTabControl1.AppearancePage.Header.Options.UseFont = True
        Me.XtraTabControl1.AppearancePage.HeaderActive.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.XtraTabControl1.AppearancePage.HeaderActive.Options.UseFont = True
        Me.XtraTabControl1.AppearancePage.HeaderHotTracked.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.XtraTabControl1.AppearancePage.HeaderHotTracked.Options.UseFont = True
        Me.XtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.XtraTabControl1.Location = New System.Drawing.Point(0, 0)
        Me.XtraTabControl1.Name = "XtraTabControl1"
        Me.XtraTabControl1.SelectedTabPage = Me.XtraTabPage1
        Me.XtraTabControl1.Size = New System.Drawing.Size(486, 204)
        Me.XtraTabControl1.TabIndex = 0
        Me.XtraTabControl1.TabPages.AddRange(New DevExpress.XtraTab.XtraTabPage() {Me.XtraTabPage1, Me.XtraTabPage2})
        '
        'XtraTabPage1
        '
        Me.XtraTabPage1.Controls.Add(Me.PictureEdit5)
        Me.XtraTabPage1.Controls.Add(Me.PictureEdit4)
        Me.XtraTabPage1.Controls.Add(Me.PictureEdit3)
        Me.XtraTabPage1.Controls.Add(Me.PictureEdit2)
        Me.XtraTabPage1.Controls.Add(Me.PictureEdit1)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl28)
        Me.XtraTabPage1.Controls.Add(Me.TextEditRate5)
        Me.XtraTabPage1.Controls.Add(Me.TextEditDescp5)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl27)
        Me.XtraTabPage1.Controls.Add(Me.GridLookUpEditFormulaD5)
        Me.XtraTabPage1.Controls.Add(Me.TextEditRate4)
        Me.XtraTabPage1.Controls.Add(Me.GridLookUpEditFormulaD4)
        Me.XtraTabPage1.Controls.Add(Me.TextEditDescp4)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl26)
        Me.XtraTabPage1.Controls.Add(Me.GridLookUpEditFormulaD3)
        Me.XtraTabPage1.Controls.Add(Me.TextEditRate3)
        Me.XtraTabPage1.Controls.Add(Me.GridLookUpEditFormulaD2)
        Me.XtraTabPage1.Controls.Add(Me.TextEditDescp3)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl23)
        Me.XtraTabPage1.Controls.Add(Me.GridLookUpEditFormulaD1)
        Me.XtraTabPage1.Controls.Add(Me.TextEditRate2)
        Me.XtraTabPage1.Controls.Add(Me.TextEditDescp2)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl22)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl21)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl20)
        Me.XtraTabPage1.Controls.Add(Me.TextEditRate1)
        Me.XtraTabPage1.Controls.Add(Me.TextEditDescp1)
        Me.XtraTabPage1.Controls.Add(Me.LabelControl19)
        Me.XtraTabPage1.Name = "XtraTabPage1"
        Me.XtraTabPage1.Size = New System.Drawing.Size(478, 174)
        Me.XtraTabPage1.Text = "Deduction 1 to 5"
        '
        'PictureEdit5
        '
        Me.PictureEdit5.Cursor = System.Windows.Forms.Cursors.Default
        Me.PictureEdit5.EditValue = CType(resources.GetObject("PictureEdit5.EditValue"), Object)
        Me.PictureEdit5.Location = New System.Drawing.Point(363, 142)
        Me.PictureEdit5.Name = "PictureEdit5"
        Me.PictureEdit5.Properties.AllowFocused = False
        Me.PictureEdit5.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit5.Properties.Appearance.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit5.Properties.Appearance.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit5.Properties.Appearance.Options.UseBackColor = True
        Me.PictureEdit5.Properties.Appearance.Options.UseBorderColor = True
        Me.PictureEdit5.Properties.AppearanceFocused.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit5.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit5.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit5.Properties.AppearanceFocused.Options.UseBackColor = True
        Me.PictureEdit5.Properties.AppearanceFocused.Options.UseBorderColor = True
        Me.PictureEdit5.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PictureEdit5.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.[Auto]
        Me.PictureEdit5.Size = New System.Drawing.Size(23, 19)
        Me.PictureEdit5.TabIndex = 66
        '
        'PictureEdit4
        '
        Me.PictureEdit4.Cursor = System.Windows.Forms.Cursors.Default
        Me.PictureEdit4.EditValue = CType(resources.GetObject("PictureEdit4.EditValue"), Object)
        Me.PictureEdit4.Location = New System.Drawing.Point(363, 116)
        Me.PictureEdit4.Name = "PictureEdit4"
        Me.PictureEdit4.Properties.AllowFocused = False
        Me.PictureEdit4.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit4.Properties.Appearance.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit4.Properties.Appearance.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit4.Properties.Appearance.Options.UseBackColor = True
        Me.PictureEdit4.Properties.Appearance.Options.UseBorderColor = True
        Me.PictureEdit4.Properties.AppearanceFocused.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit4.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit4.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit4.Properties.AppearanceFocused.Options.UseBackColor = True
        Me.PictureEdit4.Properties.AppearanceFocused.Options.UseBorderColor = True
        Me.PictureEdit4.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PictureEdit4.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.[Auto]
        Me.PictureEdit4.Size = New System.Drawing.Size(23, 19)
        Me.PictureEdit4.TabIndex = 65
        '
        'PictureEdit3
        '
        Me.PictureEdit3.Cursor = System.Windows.Forms.Cursors.Default
        Me.PictureEdit3.EditValue = CType(resources.GetObject("PictureEdit3.EditValue"), Object)
        Me.PictureEdit3.Location = New System.Drawing.Point(363, 90)
        Me.PictureEdit3.Name = "PictureEdit3"
        Me.PictureEdit3.Properties.AllowFocused = False
        Me.PictureEdit3.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit3.Properties.Appearance.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit3.Properties.Appearance.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit3.Properties.Appearance.Options.UseBackColor = True
        Me.PictureEdit3.Properties.Appearance.Options.UseBorderColor = True
        Me.PictureEdit3.Properties.AppearanceFocused.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit3.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit3.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit3.Properties.AppearanceFocused.Options.UseBackColor = True
        Me.PictureEdit3.Properties.AppearanceFocused.Options.UseBorderColor = True
        Me.PictureEdit3.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PictureEdit3.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.[Auto]
        Me.PictureEdit3.Size = New System.Drawing.Size(23, 19)
        Me.PictureEdit3.TabIndex = 64
        '
        'PictureEdit2
        '
        Me.PictureEdit2.Cursor = System.Windows.Forms.Cursors.Default
        Me.PictureEdit2.EditValue = CType(resources.GetObject("PictureEdit2.EditValue"), Object)
        Me.PictureEdit2.Location = New System.Drawing.Point(363, 64)
        Me.PictureEdit2.Name = "PictureEdit2"
        Me.PictureEdit2.Properties.AllowFocused = False
        Me.PictureEdit2.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit2.Properties.Appearance.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit2.Properties.Appearance.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit2.Properties.Appearance.Options.UseBackColor = True
        Me.PictureEdit2.Properties.Appearance.Options.UseBorderColor = True
        Me.PictureEdit2.Properties.AppearanceFocused.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit2.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit2.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit2.Properties.AppearanceFocused.Options.UseBackColor = True
        Me.PictureEdit2.Properties.AppearanceFocused.Options.UseBorderColor = True
        Me.PictureEdit2.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PictureEdit2.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.[Auto]
        Me.PictureEdit2.Size = New System.Drawing.Size(23, 19)
        Me.PictureEdit2.TabIndex = 63
        '
        'PictureEdit1
        '
        Me.PictureEdit1.Cursor = System.Windows.Forms.Cursors.Default
        Me.PictureEdit1.EditValue = CType(resources.GetObject("PictureEdit1.EditValue"), Object)
        Me.PictureEdit1.Location = New System.Drawing.Point(363, 38)
        Me.PictureEdit1.Name = "PictureEdit1"
        Me.PictureEdit1.Properties.AllowFocused = False
        Me.PictureEdit1.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit1.Properties.Appearance.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit1.Properties.Appearance.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit1.Properties.Appearance.Options.UseBackColor = True
        Me.PictureEdit1.Properties.Appearance.Options.UseBorderColor = True
        Me.PictureEdit1.Properties.AppearanceFocused.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit1.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit1.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit1.Properties.AppearanceFocused.Options.UseBackColor = True
        Me.PictureEdit1.Properties.AppearanceFocused.Options.UseBorderColor = True
        Me.PictureEdit1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PictureEdit1.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.[Auto]
        Me.PictureEdit1.Size = New System.Drawing.Size(23, 19)
        Me.PictureEdit1.TabIndex = 62
        '
        'LabelControl28
        '
        Me.LabelControl28.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl28.Appearance.Options.UseFont = True
        Me.LabelControl28.Location = New System.Drawing.Point(25, 144)
        Me.LabelControl28.Name = "LabelControl28"
        Me.LabelControl28.Size = New System.Drawing.Size(7, 14)
        Me.LabelControl28.TabIndex = 60
        Me.LabelControl28.Text = "5"
        '
        'TextEditRate5
        '
        Me.TextEditRate5.Location = New System.Drawing.Point(151, 141)
        Me.TextEditRate5.Name = "TextEditRate5"
        Me.TextEditRate5.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditRate5.Properties.Appearance.Options.UseFont = True
        Me.TextEditRate5.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEditRate5.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditRate5.Properties.MaxLength = 8
        Me.TextEditRate5.Properties.ReadOnly = True
        Me.TextEditRate5.Size = New System.Drawing.Size(100, 20)
        Me.TextEditRate5.TabIndex = 58
        '
        'TextEditDescp5
        '
        Me.TextEditDescp5.Location = New System.Drawing.Point(45, 141)
        Me.TextEditDescp5.Name = "TextEditDescp5"
        Me.TextEditDescp5.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditDescp5.Properties.Appearance.Options.UseFont = True
        Me.TextEditDescp5.Properties.AppearanceReadOnly.ForeColor = System.Drawing.Color.Black
        Me.TextEditDescp5.Properties.AppearanceReadOnly.Options.UseForeColor = True
        Me.TextEditDescp5.Properties.ReadOnly = True
        Me.TextEditDescp5.Size = New System.Drawing.Size(100, 20)
        Me.TextEditDescp5.TabIndex = 57
        '
        'LabelControl27
        '
        Me.LabelControl27.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl27.Appearance.Options.UseFont = True
        Me.LabelControl27.Location = New System.Drawing.Point(25, 118)
        Me.LabelControl27.Name = "LabelControl27"
        Me.LabelControl27.Size = New System.Drawing.Size(7, 14)
        Me.LabelControl27.TabIndex = 56
        Me.LabelControl27.Text = "4"
        '
        'GridLookUpEditFormulaD5
        '
        Me.GridLookUpEditFormulaD5.Location = New System.Drawing.Point(272, 141)
        Me.GridLookUpEditFormulaD5.Name = "GridLookUpEditFormulaD5"
        Me.GridLookUpEditFormulaD5.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.GridLookUpEditFormulaD5.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEditFormulaD5.Properties.Appearance.Options.UseFont = True
        Me.GridLookUpEditFormulaD5.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GridLookUpEditFormulaD5.Properties.DataSource = Me.PAYFORMULABindingSource
        Me.GridLookUpEditFormulaD5.Properties.DisplayMember = "CODE"
        Me.GridLookUpEditFormulaD5.Properties.NullText = ""
        Me.GridLookUpEditFormulaD5.Properties.ValueMember = "CODE"
        Me.GridLookUpEditFormulaD5.Properties.View = Me.GridView18
        Me.GridLookUpEditFormulaD5.Size = New System.Drawing.Size(77, 20)
        Me.GridLookUpEditFormulaD5.TabIndex = 93
        '
        'GridView18
        '
        Me.GridView18.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn33, Me.GridColumn34})
        Me.GridView18.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView18.Name = "GridView18"
        Me.GridView18.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView18.OptionsView.ShowGroupPanel = False
        '
        'GridColumn33
        '
        Me.GridColumn33.Caption = "Code"
        Me.GridColumn33.FieldName = "CODE"
        Me.GridColumn33.Name = "GridColumn33"
        Me.GridColumn33.Visible = True
        Me.GridColumn33.VisibleIndex = 0
        '
        'GridColumn34
        '
        Me.GridColumn34.Caption = "Formula"
        Me.GridColumn34.FieldName = "FORM"
        Me.GridColumn34.Name = "GridColumn34"
        Me.GridColumn34.Visible = True
        Me.GridColumn34.VisibleIndex = 1
        '
        'TextEditRate4
        '
        Me.TextEditRate4.Location = New System.Drawing.Point(151, 115)
        Me.TextEditRate4.Name = "TextEditRate4"
        Me.TextEditRate4.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditRate4.Properties.Appearance.Options.UseFont = True
        Me.TextEditRate4.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEditRate4.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditRate4.Properties.MaxLength = 8
        Me.TextEditRate4.Properties.ReadOnly = True
        Me.TextEditRate4.Size = New System.Drawing.Size(100, 20)
        Me.TextEditRate4.TabIndex = 54
        '
        'GridLookUpEditFormulaD4
        '
        Me.GridLookUpEditFormulaD4.Location = New System.Drawing.Point(272, 115)
        Me.GridLookUpEditFormulaD4.Name = "GridLookUpEditFormulaD4"
        Me.GridLookUpEditFormulaD4.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.GridLookUpEditFormulaD4.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEditFormulaD4.Properties.Appearance.Options.UseFont = True
        Me.GridLookUpEditFormulaD4.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GridLookUpEditFormulaD4.Properties.DataSource = Me.PAYFORMULABindingSource
        Me.GridLookUpEditFormulaD4.Properties.DisplayMember = "CODE"
        Me.GridLookUpEditFormulaD4.Properties.NullText = ""
        Me.GridLookUpEditFormulaD4.Properties.ValueMember = "CODE"
        Me.GridLookUpEditFormulaD4.Properties.View = Me.GridView19
        Me.GridLookUpEditFormulaD4.Size = New System.Drawing.Size(77, 20)
        Me.GridLookUpEditFormulaD4.TabIndex = 92
        '
        'GridView19
        '
        Me.GridView19.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn35, Me.GridColumn36})
        Me.GridView19.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView19.Name = "GridView19"
        Me.GridView19.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView19.OptionsView.ShowGroupPanel = False
        '
        'GridColumn35
        '
        Me.GridColumn35.Caption = "Code"
        Me.GridColumn35.FieldName = "CODE"
        Me.GridColumn35.Name = "GridColumn35"
        Me.GridColumn35.Visible = True
        Me.GridColumn35.VisibleIndex = 0
        '
        'GridColumn36
        '
        Me.GridColumn36.Caption = "Formula"
        Me.GridColumn36.FieldName = "FORM"
        Me.GridColumn36.Name = "GridColumn36"
        Me.GridColumn36.Visible = True
        Me.GridColumn36.VisibleIndex = 1
        '
        'TextEditDescp4
        '
        Me.TextEditDescp4.Location = New System.Drawing.Point(45, 115)
        Me.TextEditDescp4.Name = "TextEditDescp4"
        Me.TextEditDescp4.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditDescp4.Properties.Appearance.Options.UseFont = True
        Me.TextEditDescp4.Properties.AppearanceReadOnly.ForeColor = System.Drawing.Color.Black
        Me.TextEditDescp4.Properties.AppearanceReadOnly.Options.UseForeColor = True
        Me.TextEditDescp4.Properties.ReadOnly = True
        Me.TextEditDescp4.Size = New System.Drawing.Size(100, 20)
        Me.TextEditDescp4.TabIndex = 53
        '
        'LabelControl26
        '
        Me.LabelControl26.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl26.Appearance.Options.UseFont = True
        Me.LabelControl26.Location = New System.Drawing.Point(25, 92)
        Me.LabelControl26.Name = "LabelControl26"
        Me.LabelControl26.Size = New System.Drawing.Size(7, 14)
        Me.LabelControl26.TabIndex = 52
        Me.LabelControl26.Text = "3"
        '
        'GridLookUpEditFormulaD3
        '
        Me.GridLookUpEditFormulaD3.Location = New System.Drawing.Point(272, 89)
        Me.GridLookUpEditFormulaD3.Name = "GridLookUpEditFormulaD3"
        Me.GridLookUpEditFormulaD3.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.GridLookUpEditFormulaD3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEditFormulaD3.Properties.Appearance.Options.UseFont = True
        Me.GridLookUpEditFormulaD3.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GridLookUpEditFormulaD3.Properties.DataSource = Me.PAYFORMULABindingSource
        Me.GridLookUpEditFormulaD3.Properties.DisplayMember = "CODE"
        Me.GridLookUpEditFormulaD3.Properties.NullText = ""
        Me.GridLookUpEditFormulaD3.Properties.ValueMember = "CODE"
        Me.GridLookUpEditFormulaD3.Properties.View = Me.GridView20
        Me.GridLookUpEditFormulaD3.Size = New System.Drawing.Size(77, 20)
        Me.GridLookUpEditFormulaD3.TabIndex = 91
        '
        'GridView20
        '
        Me.GridView20.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn37, Me.GridColumn38})
        Me.GridView20.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView20.Name = "GridView20"
        Me.GridView20.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView20.OptionsView.ShowGroupPanel = False
        '
        'GridColumn37
        '
        Me.GridColumn37.Caption = "Code"
        Me.GridColumn37.FieldName = "CODE"
        Me.GridColumn37.Name = "GridColumn37"
        Me.GridColumn37.Visible = True
        Me.GridColumn37.VisibleIndex = 0
        '
        'GridColumn38
        '
        Me.GridColumn38.Caption = "Formula"
        Me.GridColumn38.FieldName = "FORM"
        Me.GridColumn38.Name = "GridColumn38"
        Me.GridColumn38.Visible = True
        Me.GridColumn38.VisibleIndex = 1
        '
        'TextEditRate3
        '
        Me.TextEditRate3.Location = New System.Drawing.Point(151, 89)
        Me.TextEditRate3.Name = "TextEditRate3"
        Me.TextEditRate3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditRate3.Properties.Appearance.Options.UseFont = True
        Me.TextEditRate3.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEditRate3.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditRate3.Properties.MaxLength = 8
        Me.TextEditRate3.Properties.ReadOnly = True
        Me.TextEditRate3.Size = New System.Drawing.Size(100, 20)
        Me.TextEditRate3.TabIndex = 50
        '
        'GridLookUpEditFormulaD2
        '
        Me.GridLookUpEditFormulaD2.Location = New System.Drawing.Point(272, 63)
        Me.GridLookUpEditFormulaD2.Name = "GridLookUpEditFormulaD2"
        Me.GridLookUpEditFormulaD2.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.GridLookUpEditFormulaD2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEditFormulaD2.Properties.Appearance.Options.UseFont = True
        Me.GridLookUpEditFormulaD2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GridLookUpEditFormulaD2.Properties.DataSource = Me.PAYFORMULABindingSource
        Me.GridLookUpEditFormulaD2.Properties.DisplayMember = "CODE"
        Me.GridLookUpEditFormulaD2.Properties.NullText = ""
        Me.GridLookUpEditFormulaD2.Properties.ValueMember = "CODE"
        Me.GridLookUpEditFormulaD2.Properties.View = Me.GridView21
        Me.GridLookUpEditFormulaD2.Size = New System.Drawing.Size(77, 20)
        Me.GridLookUpEditFormulaD2.TabIndex = 90
        '
        'GridView21
        '
        Me.GridView21.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn39, Me.GridColumn40})
        Me.GridView21.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView21.Name = "GridView21"
        Me.GridView21.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView21.OptionsView.ShowGroupPanel = False
        '
        'GridColumn39
        '
        Me.GridColumn39.Caption = "Code"
        Me.GridColumn39.FieldName = "CODE"
        Me.GridColumn39.Name = "GridColumn39"
        Me.GridColumn39.Visible = True
        Me.GridColumn39.VisibleIndex = 0
        '
        'GridColumn40
        '
        Me.GridColumn40.Caption = "Formula"
        Me.GridColumn40.FieldName = "FORM"
        Me.GridColumn40.Name = "GridColumn40"
        Me.GridColumn40.Visible = True
        Me.GridColumn40.VisibleIndex = 1
        '
        'TextEditDescp3
        '
        Me.TextEditDescp3.Location = New System.Drawing.Point(45, 89)
        Me.TextEditDescp3.Name = "TextEditDescp3"
        Me.TextEditDescp3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditDescp3.Properties.Appearance.Options.UseFont = True
        Me.TextEditDescp3.Properties.AppearanceReadOnly.ForeColor = System.Drawing.Color.Black
        Me.TextEditDescp3.Properties.AppearanceReadOnly.Options.UseForeColor = True
        Me.TextEditDescp3.Properties.ReadOnly = True
        Me.TextEditDescp3.Size = New System.Drawing.Size(100, 20)
        Me.TextEditDescp3.TabIndex = 49
        '
        'LabelControl23
        '
        Me.LabelControl23.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl23.Appearance.Options.UseFont = True
        Me.LabelControl23.Location = New System.Drawing.Point(25, 66)
        Me.LabelControl23.Name = "LabelControl23"
        Me.LabelControl23.Size = New System.Drawing.Size(7, 14)
        Me.LabelControl23.TabIndex = 48
        Me.LabelControl23.Text = "2"
        '
        'GridLookUpEditFormulaD1
        '
        Me.GridLookUpEditFormulaD1.Location = New System.Drawing.Point(272, 37)
        Me.GridLookUpEditFormulaD1.Name = "GridLookUpEditFormulaD1"
        Me.GridLookUpEditFormulaD1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.GridLookUpEditFormulaD1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEditFormulaD1.Properties.Appearance.Options.UseFont = True
        Me.GridLookUpEditFormulaD1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GridLookUpEditFormulaD1.Properties.DataSource = Me.PAYFORMULABindingSource
        Me.GridLookUpEditFormulaD1.Properties.DisplayMember = "CODE"
        Me.GridLookUpEditFormulaD1.Properties.NullText = ""
        Me.GridLookUpEditFormulaD1.Properties.ValueMember = "CODE"
        Me.GridLookUpEditFormulaD1.Properties.View = Me.GridView22
        Me.GridLookUpEditFormulaD1.Size = New System.Drawing.Size(77, 20)
        Me.GridLookUpEditFormulaD1.TabIndex = 89
        '
        'GridView22
        '
        Me.GridView22.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn41, Me.GridColumn42})
        Me.GridView22.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView22.Name = "GridView22"
        Me.GridView22.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView22.OptionsView.ShowGroupPanel = False
        '
        'GridColumn41
        '
        Me.GridColumn41.Caption = "Code"
        Me.GridColumn41.FieldName = "CODE"
        Me.GridColumn41.Name = "GridColumn41"
        Me.GridColumn41.Visible = True
        Me.GridColumn41.VisibleIndex = 0
        '
        'GridColumn42
        '
        Me.GridColumn42.Caption = "Formula"
        Me.GridColumn42.FieldName = "FORM"
        Me.GridColumn42.Name = "GridColumn42"
        Me.GridColumn42.Visible = True
        Me.GridColumn42.VisibleIndex = 1
        '
        'TextEditRate2
        '
        Me.TextEditRate2.Location = New System.Drawing.Point(151, 63)
        Me.TextEditRate2.Name = "TextEditRate2"
        Me.TextEditRate2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditRate2.Properties.Appearance.Options.UseFont = True
        Me.TextEditRate2.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEditRate2.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditRate2.Properties.MaxLength = 8
        Me.TextEditRate2.Properties.ReadOnly = True
        Me.TextEditRate2.Size = New System.Drawing.Size(100, 20)
        Me.TextEditRate2.TabIndex = 46
        '
        'TextEditDescp2
        '
        Me.TextEditDescp2.Location = New System.Drawing.Point(45, 63)
        Me.TextEditDescp2.Name = "TextEditDescp2"
        Me.TextEditDescp2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditDescp2.Properties.Appearance.Options.UseFont = True
        Me.TextEditDescp2.Properties.AppearanceReadOnly.ForeColor = System.Drawing.Color.Black
        Me.TextEditDescp2.Properties.AppearanceReadOnly.Options.UseForeColor = True
        Me.TextEditDescp2.Properties.ReadOnly = True
        Me.TextEditDescp2.Size = New System.Drawing.Size(100, 20)
        Me.TextEditDescp2.TabIndex = 45
        '
        'LabelControl22
        '
        Me.LabelControl22.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl22.Appearance.Options.UseFont = True
        Me.LabelControl22.Location = New System.Drawing.Point(25, 40)
        Me.LabelControl22.Name = "LabelControl22"
        Me.LabelControl22.Size = New System.Drawing.Size(7, 14)
        Me.LabelControl22.TabIndex = 44
        Me.LabelControl22.Text = "1"
        '
        'LabelControl21
        '
        Me.LabelControl21.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl21.Appearance.Options.UseFont = True
        Me.LabelControl21.Location = New System.Drawing.Point(287, 17)
        Me.LabelControl21.Name = "LabelControl21"
        Me.LabelControl21.Size = New System.Drawing.Size(42, 14)
        Me.LabelControl21.TabIndex = 42
        Me.LabelControl21.Text = "Formula"
        '
        'LabelControl20
        '
        Me.LabelControl20.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl20.Appearance.Options.UseFont = True
        Me.LabelControl20.Location = New System.Drawing.Point(167, 17)
        Me.LabelControl20.Name = "LabelControl20"
        Me.LabelControl20.Size = New System.Drawing.Size(61, 14)
        Me.LabelControl20.TabIndex = 41
        Me.LabelControl20.Text = "Rate / Amt"
        '
        'TextEditRate1
        '
        Me.TextEditRate1.Location = New System.Drawing.Point(151, 37)
        Me.TextEditRate1.Name = "TextEditRate1"
        Me.TextEditRate1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditRate1.Properties.Appearance.Options.UseFont = True
        Me.TextEditRate1.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEditRate1.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditRate1.Properties.MaxLength = 8
        Me.TextEditRate1.Properties.ReadOnly = True
        Me.TextEditRate1.Size = New System.Drawing.Size(100, 20)
        Me.TextEditRate1.TabIndex = 40
        '
        'TextEditDescp1
        '
        Me.TextEditDescp1.Location = New System.Drawing.Point(45, 37)
        Me.TextEditDescp1.Name = "TextEditDescp1"
        Me.TextEditDescp1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditDescp1.Properties.Appearance.Options.UseFont = True
        Me.TextEditDescp1.Properties.AppearanceReadOnly.ForeColor = System.Drawing.Color.Black
        Me.TextEditDescp1.Properties.AppearanceReadOnly.Options.UseForeColor = True
        Me.TextEditDescp1.Properties.ReadOnly = True
        Me.TextEditDescp1.Size = New System.Drawing.Size(100, 20)
        Me.TextEditDescp1.TabIndex = 39
        '
        'LabelControl19
        '
        Me.LabelControl19.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl19.Appearance.Options.UseFont = True
        Me.LabelControl19.Location = New System.Drawing.Point(64, 17)
        Me.LabelControl19.Name = "LabelControl19"
        Me.LabelControl19.Size = New System.Drawing.Size(60, 14)
        Me.LabelControl19.TabIndex = 38
        Me.LabelControl19.Text = "Description"
        '
        'XtraTabPage2
        '
        Me.XtraTabPage2.Controls.Add(Me.GridLookUpEditFormulaD10)
        Me.XtraTabPage2.Controls.Add(Me.PictureEdit10)
        Me.XtraTabPage2.Controls.Add(Me.PictureEdit9)
        Me.XtraTabPage2.Controls.Add(Me.GridLookUpEditFormulaD9)
        Me.XtraTabPage2.Controls.Add(Me.PictureEdit8)
        Me.XtraTabPage2.Controls.Add(Me.PictureEdit7)
        Me.XtraTabPage2.Controls.Add(Me.GridLookUpEditFormulaD8)
        Me.XtraTabPage2.Controls.Add(Me.PictureEdit6)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl29)
        Me.XtraTabPage2.Controls.Add(Me.GridLookUpEditFormulaD7)
        Me.XtraTabPage2.Controls.Add(Me.TextEditRate10)
        Me.XtraTabPage2.Controls.Add(Me.GridLookUpEditFormulaD6)
        Me.XtraTabPage2.Controls.Add(Me.TextEditDescp10)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl30)
        Me.XtraTabPage2.Controls.Add(Me.TextEditRate9)
        Me.XtraTabPage2.Controls.Add(Me.TextEditDescp9)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl31)
        Me.XtraTabPage2.Controls.Add(Me.TextEditRate8)
        Me.XtraTabPage2.Controls.Add(Me.TextEditDescp8)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl32)
        Me.XtraTabPage2.Controls.Add(Me.TextEditRate7)
        Me.XtraTabPage2.Controls.Add(Me.TextEditDescp7)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl34)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl35)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl36)
        Me.XtraTabPage2.Controls.Add(Me.TextEditRate6)
        Me.XtraTabPage2.Controls.Add(Me.TextEditDescp6)
        Me.XtraTabPage2.Controls.Add(Me.LabelControl37)
        Me.XtraTabPage2.Name = "XtraTabPage2"
        Me.XtraTabPage2.Size = New System.Drawing.Size(478, 174)
        Me.XtraTabPage2.Text = "Deduction 6 to 10"
        '
        'GridLookUpEditFormulaD10
        '
        Me.GridLookUpEditFormulaD10.Location = New System.Drawing.Point(272, 141)
        Me.GridLookUpEditFormulaD10.Name = "GridLookUpEditFormulaD10"
        Me.GridLookUpEditFormulaD10.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.GridLookUpEditFormulaD10.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEditFormulaD10.Properties.Appearance.Options.UseFont = True
        Me.GridLookUpEditFormulaD10.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GridLookUpEditFormulaD10.Properties.DataSource = Me.PAYFORMULABindingSource
        Me.GridLookUpEditFormulaD10.Properties.DisplayMember = "CODE"
        Me.GridLookUpEditFormulaD10.Properties.NullText = ""
        Me.GridLookUpEditFormulaD10.Properties.ValueMember = "CODE"
        Me.GridLookUpEditFormulaD10.Properties.View = Me.GridView13
        Me.GridLookUpEditFormulaD10.Size = New System.Drawing.Size(77, 20)
        Me.GridLookUpEditFormulaD10.TabIndex = 98
        '
        'GridView13
        '
        Me.GridView13.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn23, Me.GridColumn24})
        Me.GridView13.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView13.Name = "GridView13"
        Me.GridView13.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView13.OptionsView.ShowGroupPanel = False
        '
        'GridColumn23
        '
        Me.GridColumn23.Caption = "Code"
        Me.GridColumn23.FieldName = "CODE"
        Me.GridColumn23.Name = "GridColumn23"
        Me.GridColumn23.Visible = True
        Me.GridColumn23.VisibleIndex = 0
        '
        'GridColumn24
        '
        Me.GridColumn24.Caption = "Formula"
        Me.GridColumn24.FieldName = "FORM"
        Me.GridColumn24.Name = "GridColumn24"
        Me.GridColumn24.Visible = True
        Me.GridColumn24.VisibleIndex = 1
        '
        'PictureEdit10
        '
        Me.PictureEdit10.Cursor = System.Windows.Forms.Cursors.Default
        Me.PictureEdit10.EditValue = CType(resources.GetObject("PictureEdit10.EditValue"), Object)
        Me.PictureEdit10.Location = New System.Drawing.Point(363, 142)
        Me.PictureEdit10.Name = "PictureEdit10"
        Me.PictureEdit10.Properties.AllowFocused = False
        Me.PictureEdit10.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit10.Properties.Appearance.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit10.Properties.Appearance.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit10.Properties.Appearance.Options.UseBackColor = True
        Me.PictureEdit10.Properties.Appearance.Options.UseBorderColor = True
        Me.PictureEdit10.Properties.AppearanceFocused.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit10.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit10.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit10.Properties.AppearanceFocused.Options.UseBackColor = True
        Me.PictureEdit10.Properties.AppearanceFocused.Options.UseBorderColor = True
        Me.PictureEdit10.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PictureEdit10.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.[Auto]
        Me.PictureEdit10.Size = New System.Drawing.Size(23, 19)
        Me.PictureEdit10.TabIndex = 88
        '
        'PictureEdit9
        '
        Me.PictureEdit9.Cursor = System.Windows.Forms.Cursors.Default
        Me.PictureEdit9.EditValue = CType(resources.GetObject("PictureEdit9.EditValue"), Object)
        Me.PictureEdit9.Location = New System.Drawing.Point(363, 116)
        Me.PictureEdit9.Name = "PictureEdit9"
        Me.PictureEdit9.Properties.AllowFocused = False
        Me.PictureEdit9.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit9.Properties.Appearance.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit9.Properties.Appearance.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit9.Properties.Appearance.Options.UseBackColor = True
        Me.PictureEdit9.Properties.Appearance.Options.UseBorderColor = True
        Me.PictureEdit9.Properties.AppearanceFocused.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit9.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit9.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit9.Properties.AppearanceFocused.Options.UseBackColor = True
        Me.PictureEdit9.Properties.AppearanceFocused.Options.UseBorderColor = True
        Me.PictureEdit9.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PictureEdit9.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.[Auto]
        Me.PictureEdit9.Size = New System.Drawing.Size(23, 19)
        Me.PictureEdit9.TabIndex = 87
        '
        'GridLookUpEditFormulaD9
        '
        Me.GridLookUpEditFormulaD9.Location = New System.Drawing.Point(272, 115)
        Me.GridLookUpEditFormulaD9.Name = "GridLookUpEditFormulaD9"
        Me.GridLookUpEditFormulaD9.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.GridLookUpEditFormulaD9.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEditFormulaD9.Properties.Appearance.Options.UseFont = True
        Me.GridLookUpEditFormulaD9.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GridLookUpEditFormulaD9.Properties.DataSource = Me.PAYFORMULABindingSource
        Me.GridLookUpEditFormulaD9.Properties.DisplayMember = "CODE"
        Me.GridLookUpEditFormulaD9.Properties.NullText = ""
        Me.GridLookUpEditFormulaD9.Properties.ValueMember = "CODE"
        Me.GridLookUpEditFormulaD9.Properties.View = Me.GridView14
        Me.GridLookUpEditFormulaD9.Size = New System.Drawing.Size(77, 20)
        Me.GridLookUpEditFormulaD9.TabIndex = 97
        '
        'GridView14
        '
        Me.GridView14.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn25, Me.GridColumn26})
        Me.GridView14.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView14.Name = "GridView14"
        Me.GridView14.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView14.OptionsView.ShowGroupPanel = False
        '
        'GridColumn25
        '
        Me.GridColumn25.Caption = "Code"
        Me.GridColumn25.FieldName = "CODE"
        Me.GridColumn25.Name = "GridColumn25"
        Me.GridColumn25.Visible = True
        Me.GridColumn25.VisibleIndex = 0
        '
        'GridColumn26
        '
        Me.GridColumn26.Caption = "Formula"
        Me.GridColumn26.FieldName = "FORM"
        Me.GridColumn26.Name = "GridColumn26"
        Me.GridColumn26.Visible = True
        Me.GridColumn26.VisibleIndex = 1
        '
        'PictureEdit8
        '
        Me.PictureEdit8.Cursor = System.Windows.Forms.Cursors.Default
        Me.PictureEdit8.EditValue = CType(resources.GetObject("PictureEdit8.EditValue"), Object)
        Me.PictureEdit8.Location = New System.Drawing.Point(363, 90)
        Me.PictureEdit8.Name = "PictureEdit8"
        Me.PictureEdit8.Properties.AllowFocused = False
        Me.PictureEdit8.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit8.Properties.Appearance.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit8.Properties.Appearance.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit8.Properties.Appearance.Options.UseBackColor = True
        Me.PictureEdit8.Properties.Appearance.Options.UseBorderColor = True
        Me.PictureEdit8.Properties.AppearanceFocused.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit8.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit8.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit8.Properties.AppearanceFocused.Options.UseBackColor = True
        Me.PictureEdit8.Properties.AppearanceFocused.Options.UseBorderColor = True
        Me.PictureEdit8.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PictureEdit8.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.[Auto]
        Me.PictureEdit8.Size = New System.Drawing.Size(23, 19)
        Me.PictureEdit8.TabIndex = 86
        '
        'PictureEdit7
        '
        Me.PictureEdit7.Cursor = System.Windows.Forms.Cursors.Default
        Me.PictureEdit7.EditValue = CType(resources.GetObject("PictureEdit7.EditValue"), Object)
        Me.PictureEdit7.Location = New System.Drawing.Point(363, 64)
        Me.PictureEdit7.Name = "PictureEdit7"
        Me.PictureEdit7.Properties.AllowFocused = False
        Me.PictureEdit7.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit7.Properties.Appearance.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit7.Properties.Appearance.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit7.Properties.Appearance.Options.UseBackColor = True
        Me.PictureEdit7.Properties.Appearance.Options.UseBorderColor = True
        Me.PictureEdit7.Properties.AppearanceFocused.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit7.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit7.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit7.Properties.AppearanceFocused.Options.UseBackColor = True
        Me.PictureEdit7.Properties.AppearanceFocused.Options.UseBorderColor = True
        Me.PictureEdit7.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PictureEdit7.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.[Auto]
        Me.PictureEdit7.Size = New System.Drawing.Size(23, 19)
        Me.PictureEdit7.TabIndex = 85
        '
        'GridLookUpEditFormulaD8
        '
        Me.GridLookUpEditFormulaD8.Location = New System.Drawing.Point(272, 89)
        Me.GridLookUpEditFormulaD8.Name = "GridLookUpEditFormulaD8"
        Me.GridLookUpEditFormulaD8.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.GridLookUpEditFormulaD8.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEditFormulaD8.Properties.Appearance.Options.UseFont = True
        Me.GridLookUpEditFormulaD8.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GridLookUpEditFormulaD8.Properties.DataSource = Me.PAYFORMULABindingSource
        Me.GridLookUpEditFormulaD8.Properties.DisplayMember = "CODE"
        Me.GridLookUpEditFormulaD8.Properties.NullText = ""
        Me.GridLookUpEditFormulaD8.Properties.ValueMember = "CODE"
        Me.GridLookUpEditFormulaD8.Properties.View = Me.GridView15
        Me.GridLookUpEditFormulaD8.Size = New System.Drawing.Size(77, 20)
        Me.GridLookUpEditFormulaD8.TabIndex = 96
        '
        'GridView15
        '
        Me.GridView15.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn27, Me.GridColumn28})
        Me.GridView15.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView15.Name = "GridView15"
        Me.GridView15.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView15.OptionsView.ShowGroupPanel = False
        '
        'GridColumn27
        '
        Me.GridColumn27.Caption = "Code"
        Me.GridColumn27.FieldName = "CODE"
        Me.GridColumn27.Name = "GridColumn27"
        Me.GridColumn27.Visible = True
        Me.GridColumn27.VisibleIndex = 0
        '
        'GridColumn28
        '
        Me.GridColumn28.Caption = "Formula"
        Me.GridColumn28.FieldName = "FORM"
        Me.GridColumn28.Name = "GridColumn28"
        Me.GridColumn28.Visible = True
        Me.GridColumn28.VisibleIndex = 1
        '
        'PictureEdit6
        '
        Me.PictureEdit6.Cursor = System.Windows.Forms.Cursors.Default
        Me.PictureEdit6.EditValue = CType(resources.GetObject("PictureEdit6.EditValue"), Object)
        Me.PictureEdit6.Location = New System.Drawing.Point(363, 38)
        Me.PictureEdit6.Name = "PictureEdit6"
        Me.PictureEdit6.Properties.AllowFocused = False
        Me.PictureEdit6.Properties.Appearance.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit6.Properties.Appearance.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit6.Properties.Appearance.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit6.Properties.Appearance.Options.UseBackColor = True
        Me.PictureEdit6.Properties.Appearance.Options.UseBorderColor = True
        Me.PictureEdit6.Properties.AppearanceFocused.BackColor = System.Drawing.Color.Transparent
        Me.PictureEdit6.Properties.AppearanceFocused.BackColor2 = System.Drawing.Color.Transparent
        Me.PictureEdit6.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Transparent
        Me.PictureEdit6.Properties.AppearanceFocused.Options.UseBackColor = True
        Me.PictureEdit6.Properties.AppearanceFocused.Options.UseBorderColor = True
        Me.PictureEdit6.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PictureEdit6.Properties.ShowCameraMenuItem = DevExpress.XtraEditors.Controls.CameraMenuItemVisibility.[Auto]
        Me.PictureEdit6.Size = New System.Drawing.Size(23, 19)
        Me.PictureEdit6.TabIndex = 84
        '
        'LabelControl29
        '
        Me.LabelControl29.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl29.Appearance.Options.UseFont = True
        Me.LabelControl29.Location = New System.Drawing.Point(25, 144)
        Me.LabelControl29.Name = "LabelControl29"
        Me.LabelControl29.Size = New System.Drawing.Size(14, 14)
        Me.LabelControl29.TabIndex = 83
        Me.LabelControl29.Text = "10"
        '
        'GridLookUpEditFormulaD7
        '
        Me.GridLookUpEditFormulaD7.Location = New System.Drawing.Point(272, 63)
        Me.GridLookUpEditFormulaD7.Name = "GridLookUpEditFormulaD7"
        Me.GridLookUpEditFormulaD7.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.GridLookUpEditFormulaD7.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEditFormulaD7.Properties.Appearance.Options.UseFont = True
        Me.GridLookUpEditFormulaD7.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GridLookUpEditFormulaD7.Properties.DataSource = Me.PAYFORMULABindingSource
        Me.GridLookUpEditFormulaD7.Properties.DisplayMember = "CODE"
        Me.GridLookUpEditFormulaD7.Properties.NullText = ""
        Me.GridLookUpEditFormulaD7.Properties.ValueMember = "CODE"
        Me.GridLookUpEditFormulaD7.Properties.View = Me.GridView16
        Me.GridLookUpEditFormulaD7.Size = New System.Drawing.Size(77, 20)
        Me.GridLookUpEditFormulaD7.TabIndex = 95
        '
        'GridView16
        '
        Me.GridView16.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn29, Me.GridColumn30})
        Me.GridView16.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView16.Name = "GridView16"
        Me.GridView16.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView16.OptionsView.ShowGroupPanel = False
        '
        'GridColumn29
        '
        Me.GridColumn29.Caption = "Code"
        Me.GridColumn29.FieldName = "CODE"
        Me.GridColumn29.Name = "GridColumn29"
        Me.GridColumn29.Visible = True
        Me.GridColumn29.VisibleIndex = 0
        '
        'GridColumn30
        '
        Me.GridColumn30.Caption = "Formula"
        Me.GridColumn30.FieldName = "FORM"
        Me.GridColumn30.Name = "GridColumn30"
        Me.GridColumn30.Visible = True
        Me.GridColumn30.VisibleIndex = 1
        '
        'TextEditRate10
        '
        Me.TextEditRate10.Location = New System.Drawing.Point(151, 141)
        Me.TextEditRate10.Name = "TextEditRate10"
        Me.TextEditRate10.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditRate10.Properties.Appearance.Options.UseFont = True
        Me.TextEditRate10.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEditRate10.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditRate10.Properties.MaxLength = 8
        Me.TextEditRate10.Properties.ReadOnly = True
        Me.TextEditRate10.Size = New System.Drawing.Size(100, 20)
        Me.TextEditRate10.TabIndex = 81
        '
        'GridLookUpEditFormulaD6
        '
        Me.GridLookUpEditFormulaD6.Location = New System.Drawing.Point(272, 37)
        Me.GridLookUpEditFormulaD6.Name = "GridLookUpEditFormulaD6"
        Me.GridLookUpEditFormulaD6.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[True]
        Me.GridLookUpEditFormulaD6.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEditFormulaD6.Properties.Appearance.Options.UseFont = True
        Me.GridLookUpEditFormulaD6.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GridLookUpEditFormulaD6.Properties.DataSource = Me.PAYFORMULABindingSource
        Me.GridLookUpEditFormulaD6.Properties.DisplayMember = "CODE"
        Me.GridLookUpEditFormulaD6.Properties.NullText = ""
        Me.GridLookUpEditFormulaD6.Properties.ValueMember = "CODE"
        Me.GridLookUpEditFormulaD6.Properties.View = Me.GridView17
        Me.GridLookUpEditFormulaD6.Size = New System.Drawing.Size(77, 20)
        Me.GridLookUpEditFormulaD6.TabIndex = 94
        '
        'GridView17
        '
        Me.GridView17.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn31, Me.GridColumn32})
        Me.GridView17.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView17.Name = "GridView17"
        Me.GridView17.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView17.OptionsView.ShowGroupPanel = False
        '
        'GridColumn31
        '
        Me.GridColumn31.Caption = "Code"
        Me.GridColumn31.FieldName = "CODE"
        Me.GridColumn31.Name = "GridColumn31"
        Me.GridColumn31.Visible = True
        Me.GridColumn31.VisibleIndex = 0
        '
        'GridColumn32
        '
        Me.GridColumn32.Caption = "Formula"
        Me.GridColumn32.FieldName = "FORM"
        Me.GridColumn32.Name = "GridColumn32"
        Me.GridColumn32.Visible = True
        Me.GridColumn32.VisibleIndex = 1
        '
        'TextEditDescp10
        '
        Me.TextEditDescp10.Location = New System.Drawing.Point(45, 141)
        Me.TextEditDescp10.Name = "TextEditDescp10"
        Me.TextEditDescp10.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditDescp10.Properties.Appearance.Options.UseFont = True
        Me.TextEditDescp10.Properties.ReadOnly = True
        Me.TextEditDescp10.Size = New System.Drawing.Size(100, 20)
        Me.TextEditDescp10.TabIndex = 80
        '
        'LabelControl30
        '
        Me.LabelControl30.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl30.Appearance.Options.UseFont = True
        Me.LabelControl30.Location = New System.Drawing.Point(25, 118)
        Me.LabelControl30.Name = "LabelControl30"
        Me.LabelControl30.Size = New System.Drawing.Size(7, 14)
        Me.LabelControl30.TabIndex = 79
        Me.LabelControl30.Text = "9"
        '
        'TextEditRate9
        '
        Me.TextEditRate9.Location = New System.Drawing.Point(151, 115)
        Me.TextEditRate9.Name = "TextEditRate9"
        Me.TextEditRate9.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditRate9.Properties.Appearance.Options.UseFont = True
        Me.TextEditRate9.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEditRate9.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditRate9.Properties.MaxLength = 8
        Me.TextEditRate9.Properties.ReadOnly = True
        Me.TextEditRate9.Size = New System.Drawing.Size(100, 20)
        Me.TextEditRate9.TabIndex = 77
        '
        'TextEditDescp9
        '
        Me.TextEditDescp9.Location = New System.Drawing.Point(45, 115)
        Me.TextEditDescp9.Name = "TextEditDescp9"
        Me.TextEditDescp9.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditDescp9.Properties.Appearance.Options.UseFont = True
        Me.TextEditDescp9.Properties.ReadOnly = True
        Me.TextEditDescp9.Size = New System.Drawing.Size(100, 20)
        Me.TextEditDescp9.TabIndex = 76
        '
        'LabelControl31
        '
        Me.LabelControl31.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl31.Appearance.Options.UseFont = True
        Me.LabelControl31.Location = New System.Drawing.Point(25, 92)
        Me.LabelControl31.Name = "LabelControl31"
        Me.LabelControl31.Size = New System.Drawing.Size(7, 14)
        Me.LabelControl31.TabIndex = 75
        Me.LabelControl31.Text = "8"
        '
        'TextEditRate8
        '
        Me.TextEditRate8.Location = New System.Drawing.Point(151, 89)
        Me.TextEditRate8.Name = "TextEditRate8"
        Me.TextEditRate8.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditRate8.Properties.Appearance.Options.UseFont = True
        Me.TextEditRate8.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEditRate8.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditRate8.Properties.MaxLength = 8
        Me.TextEditRate8.Properties.ReadOnly = True
        Me.TextEditRate8.Size = New System.Drawing.Size(100, 20)
        Me.TextEditRate8.TabIndex = 73
        '
        'TextEditDescp8
        '
        Me.TextEditDescp8.Location = New System.Drawing.Point(45, 89)
        Me.TextEditDescp8.Name = "TextEditDescp8"
        Me.TextEditDescp8.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditDescp8.Properties.Appearance.Options.UseFont = True
        Me.TextEditDescp8.Properties.ReadOnly = True
        Me.TextEditDescp8.Size = New System.Drawing.Size(100, 20)
        Me.TextEditDescp8.TabIndex = 72
        '
        'LabelControl32
        '
        Me.LabelControl32.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl32.Appearance.Options.UseFont = True
        Me.LabelControl32.Location = New System.Drawing.Point(25, 66)
        Me.LabelControl32.Name = "LabelControl32"
        Me.LabelControl32.Size = New System.Drawing.Size(7, 14)
        Me.LabelControl32.TabIndex = 71
        Me.LabelControl32.Text = "7"
        '
        'TextEditRate7
        '
        Me.TextEditRate7.Location = New System.Drawing.Point(151, 63)
        Me.TextEditRate7.Name = "TextEditRate7"
        Me.TextEditRate7.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditRate7.Properties.Appearance.Options.UseFont = True
        Me.TextEditRate7.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEditRate7.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditRate7.Properties.MaxLength = 8
        Me.TextEditRate7.Properties.ReadOnly = True
        Me.TextEditRate7.Size = New System.Drawing.Size(100, 20)
        Me.TextEditRate7.TabIndex = 69
        '
        'TextEditDescp7
        '
        Me.TextEditDescp7.Location = New System.Drawing.Point(45, 63)
        Me.TextEditDescp7.Name = "TextEditDescp7"
        Me.TextEditDescp7.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditDescp7.Properties.Appearance.Options.UseFont = True
        Me.TextEditDescp7.Properties.ReadOnly = True
        Me.TextEditDescp7.Size = New System.Drawing.Size(100, 20)
        Me.TextEditDescp7.TabIndex = 68
        '
        'LabelControl34
        '
        Me.LabelControl34.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl34.Appearance.Options.UseFont = True
        Me.LabelControl34.Location = New System.Drawing.Point(25, 40)
        Me.LabelControl34.Name = "LabelControl34"
        Me.LabelControl34.Size = New System.Drawing.Size(7, 14)
        Me.LabelControl34.TabIndex = 67
        Me.LabelControl34.Text = "6"
        '
        'LabelControl35
        '
        Me.LabelControl35.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl35.Appearance.Options.UseFont = True
        Me.LabelControl35.Location = New System.Drawing.Point(287, 17)
        Me.LabelControl35.Name = "LabelControl35"
        Me.LabelControl35.Size = New System.Drawing.Size(42, 14)
        Me.LabelControl35.TabIndex = 65
        Me.LabelControl35.Text = "Formula"
        '
        'LabelControl36
        '
        Me.LabelControl36.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl36.Appearance.Options.UseFont = True
        Me.LabelControl36.Location = New System.Drawing.Point(167, 17)
        Me.LabelControl36.Name = "LabelControl36"
        Me.LabelControl36.Size = New System.Drawing.Size(61, 14)
        Me.LabelControl36.TabIndex = 64
        Me.LabelControl36.Text = "Rate / Amt"
        '
        'TextEditRate6
        '
        Me.TextEditRate6.Location = New System.Drawing.Point(151, 37)
        Me.TextEditRate6.Name = "TextEditRate6"
        Me.TextEditRate6.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditRate6.Properties.Appearance.Options.UseFont = True
        Me.TextEditRate6.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEditRate6.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditRate6.Properties.MaxLength = 8
        Me.TextEditRate6.Properties.ReadOnly = True
        Me.TextEditRate6.Size = New System.Drawing.Size(100, 20)
        Me.TextEditRate6.TabIndex = 63
        '
        'TextEditDescp6
        '
        Me.TextEditDescp6.Location = New System.Drawing.Point(45, 37)
        Me.TextEditDescp6.Name = "TextEditDescp6"
        Me.TextEditDescp6.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditDescp6.Properties.Appearance.Options.UseFont = True
        Me.TextEditDescp6.Properties.ReadOnly = True
        Me.TextEditDescp6.Size = New System.Drawing.Size(100, 20)
        Me.TextEditDescp6.TabIndex = 62
        '
        'LabelControl37
        '
        Me.LabelControl37.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl37.Appearance.Options.UseFont = True
        Me.LabelControl37.Location = New System.Drawing.Point(64, 17)
        Me.LabelControl37.Name = "LabelControl37"
        Me.LabelControl37.Size = New System.Drawing.Size(60, 14)
        Me.LabelControl37.TabIndex = 61
        Me.LabelControl37.Text = "Description"
        '
        'SidePanel2
        '
        Me.SidePanel2.Controls.Add(Me.XtraTabControl1)
        Me.SidePanel2.Location = New System.Drawing.Point(0, 276)
        Me.SidePanel2.Name = "SidePanel2"
        Me.SidePanel2.Size = New System.Drawing.Size(486, 204)
        Me.SidePanel2.TabIndex = 62
        Me.SidePanel2.Text = "SidePanel2"
        '
        'S
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(975, 518)
        Me.Controls.Add(Me.SidePanel4)
        Me.Controls.Add(Me.SidePanel3)
        Me.Controls.Add(Me.SidePanel2)
        Me.Controls.Add(Me.SidePanel1)
        Me.LookAndFeel.SkinName = "iMaginary"
        Me.LookAndFeel.UseDefaultLookAndFeel = False
        Me.Name = "S"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        CType(Me.LookUpEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblEmployeeBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditCardNo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEditEffFrm.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEditEffFrm.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditGross.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditPf1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditPf.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditESI.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditPAN.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEditPayBy.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEditEmpType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEditBank.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblBankBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditAccNo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEditDA.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEditConvence.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEditMedical.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEditHRA.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditOT.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEditTDS.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditTDS.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControlAllow, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControlAllow.ResumeLayout(False)
        CType(Me.CheckEditBonus.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditVPF.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditGraduty.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditESI.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditUnderLim.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditPF.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditProfTax.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEditFormula.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PAYFORMULABindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SidePanel1.ResumeLayout(False)
        Me.SidePanel1.PerformLayout()
        CType(Me.ComboNepaliYear.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboNEpaliMonth.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditPaycode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditBasic.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditHRA.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditMedical.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditConvence.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditDA.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SidePanel3.ResumeLayout(False)
        CType(Me.XtraTabControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabControl2.ResumeLayout(False)
        Me.XtraTabPage3.ResumeLayout(False)
        Me.XtraTabPage3.PerformLayout()
        CType(Me.PictureEdit15.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureEdit14.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureEdit13.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureEdit12.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureEdit11.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditRateE5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditDescpE5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEditFormulaE5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditRateE4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEditFormulaE4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditDescpE4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEditFormulaE3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEditFormulaE2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditRateE3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditDescpE3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEditFormulaE1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView12, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditRateE2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditDescpE2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditRateE1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditDescpE1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage4.ResumeLayout(False)
        Me.XtraTabPage4.PerformLayout()
        CType(Me.GridLookUpEditFormulaE10.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureEdit20.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureEdit19.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEditFormulaE9.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureEdit18.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureEdit17.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEditFormulaE8.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureEdit16.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEditFormulaE7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditRateE10.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEditFormulaE6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditDescpE10.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditRateE9.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditDescpE9.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditRateE8.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditDescpE8.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditRateE7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditDescpE7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditRateE6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditDescpE6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SidePanel4.ResumeLayout(False)
        CType(Me.XtraTabControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabControl1.ResumeLayout(False)
        Me.XtraTabPage1.ResumeLayout(False)
        Me.XtraTabPage1.PerformLayout()
        CType(Me.PictureEdit5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureEdit4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureEdit3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditRate5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditDescp5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEditFormulaD5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditRate4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEditFormulaD4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView19, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditDescp4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEditFormulaD3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView20, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditRate3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEditFormulaD2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditDescp3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEditFormulaD1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView22, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditRate2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditDescp2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditRate1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditDescp1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.XtraTabPage2.ResumeLayout(False)
        Me.XtraTabPage2.PerformLayout()
        CType(Me.GridLookUpEditFormulaD10.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView13, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureEdit10.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureEdit9.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEditFormulaD9.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView14, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureEdit8.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureEdit7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEditFormulaD8.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView15, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureEdit6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEditFormulaD7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView16, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditRate10.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEditFormulaD6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView17, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditDescp10.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditRate9.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditDescp9.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditRate8.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditDescp8.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditRate7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditDescp7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditRate6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditDescp6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SidePanel2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SSSDBDataSet As iAS.SSSDBDataSet
    Friend WithEvents TblEmployeeBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblEmployeeTableAdapter As iAS.SSSDBDataSetTableAdapters.TblEmployeeTableAdapter
    Friend WithEvents TblEmployee1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.TblEmployee1TableAdapter
    Friend WithEvents LookUpEdit1 As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditCardNo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents DateEditEffFrm As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControlName As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditGross As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControlDept As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SimpleButtonCal As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents TextEditPf1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditPf As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditESI As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditPAN As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl33 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ComboBoxEditPayBy As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboBoxEditEmpType As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TblBankBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblBankTableAdapter As iAS.SSSDBDataSetTableAdapters.tblBankTableAdapter
    Friend WithEvents TblBank1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.tblBank1TableAdapter
    Friend WithEvents GridLookUpEditBank As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridView6 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colBANKCODE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBANKNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LabelControl24 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditAccNo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl25 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ComboBoxEditDA As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboBoxEditConvence As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboBoxEditMedical As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboBoxEditHRA As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents TextEditOT As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ComboBoxEditTDS As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents TextEditTDS As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl18 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GroupControlAllow As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GridLookUpEditFormula As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents PAYFORMULABindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PAY_FORMULATableAdapter As iAS.SSSDBDataSetTableAdapters.PAY_FORMULATableAdapter
    Friend WithEvents CheckEditProfTax As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents PaY_FORMULA1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.PAY_FORMULA1TableAdapter
    Friend WithEvents CheckEditBonus As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditVPF As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditGraduty As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditESI As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditUnderLim As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditPF As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents SidePanel1 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents SidePanel3 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents XtraTabControl2 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage3 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents LabelControl38 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditRateE5 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditDescpE5 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl39 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditRateE4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditDescpE4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl40 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditRateE3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditDescpE3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl41 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditRateE2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditDescpE2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl42 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl43 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl44 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditRateE1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditDescpE1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl45 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents XtraTabPage4 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents LabelControl46 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditRateE10 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditDescpE10 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl47 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditRateE9 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditDescpE9 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl48 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditRateE8 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditDescpE8 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl49 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditRateE7 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditDescpE7 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl50 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl51 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl52 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditRateE6 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditDescpE6 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl53 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SidePanel4 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents XtraTabControl1 As DevExpress.XtraTab.XtraTabControl
    Friend WithEvents XtraTabPage1 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents LabelControl28 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditRate5 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditDescp5 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl27 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditRate4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditDescp4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl26 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditRate3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditDescp3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl23 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditRate2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditDescp2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl22 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl21 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl20 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditRate1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditDescp1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl19 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents XtraTabPage2 As DevExpress.XtraTab.XtraTabPage
    Friend WithEvents LabelControl29 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditRate10 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditDescp10 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl30 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditRate9 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditDescp9 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl31 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditRate8 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditDescp8 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl32 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditRate7 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditDescp7 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl34 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl35 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl36 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditRate6 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditDescp6 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl37 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SidePanel2 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents SimpleButton2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButtonSave As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents TextEditPaycode As DevExpress.XtraEditors.TextEdit
    Friend WithEvents ComboNepaliYear As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboNEpaliMonth As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents PictureEdit1 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents PictureEdit5 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents PictureEdit4 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents PictureEdit3 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents PictureEdit2 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents PictureEdit10 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents PictureEdit9 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents PictureEdit8 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents PictureEdit7 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents PictureEdit6 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents PictureEdit15 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents PictureEdit14 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents PictureEdit13 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents PictureEdit12 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents PictureEdit11 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents PictureEdit20 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents PictureEdit19 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents PictureEdit18 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents PictureEdit17 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents PictureEdit16 As DevExpress.XtraEditors.PictureEdit
    Friend WithEvents TextEditBasic As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditHRA As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditMedical As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditConvence As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditDA As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GridLookUpEditFormulaE5 As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridView5 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn9 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn10 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridLookUpEditFormulaE4 As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridView4 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn8 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridLookUpEditFormulaE3 As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridView3 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridLookUpEditFormulaE2 As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridView11 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridLookUpEditFormulaE1 As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridView12 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn21 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn22 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridLookUpEditFormulaE10 As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridView10 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn19 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn20 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridLookUpEditFormulaE9 As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridView9 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn17 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn18 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridLookUpEditFormulaE8 As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridView8 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn15 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn16 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridLookUpEditFormulaE7 As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridView7 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn13 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn14 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridLookUpEditFormulaE6 As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn11 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn12 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridLookUpEditFormulaD5 As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridView18 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn33 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn34 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridLookUpEditFormulaD4 As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridView19 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn35 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn36 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridLookUpEditFormulaD3 As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridView20 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn37 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn38 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridLookUpEditFormulaD2 As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridView21 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn39 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn40 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridLookUpEditFormulaD1 As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridView22 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn41 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn42 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridLookUpEditFormulaD10 As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridView13 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn23 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn24 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridLookUpEditFormulaD9 As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridView14 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn25 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn26 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridLookUpEditFormulaD8 As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridView15 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn27 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn28 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridLookUpEditFormulaD7 As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridView16 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn29 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn30 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridLookUpEditFormulaD6 As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridView17 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn31 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn32 As DevExpress.XtraGrid.Columns.GridColumn
End Class
