﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraLiveVideo
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(XtraLiveVideo))
        Me.streamPlayerControl1 = New WebEye.StreamPlayerControl()
        Me.SuspendLayout()
        '
        'streamPlayerControl1
        '
        Me.streamPlayerControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.streamPlayerControl1.Location = New System.Drawing.Point(0, 0)
        Me.streamPlayerControl1.Margin = New System.Windows.Forms.Padding(2)
        Me.streamPlayerControl1.Name = "streamPlayerControl1"
        Me.streamPlayerControl1.Size = New System.Drawing.Size(450, 351)
        Me.streamPlayerControl1.TabIndex = 1
        '
        'XtraLiveVideo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(450, 351)
        Me.Controls.Add(Me.streamPlayerControl1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.LookAndFeel.SkinName = "iMaginary"
        Me.LookAndFeel.UseDefaultLookAndFeel = False
        Me.Name = "XtraLiveVideo"
        Me.ResumeLayout(False)

    End Sub

    Private WithEvents streamPlayerControl1 As WebEye.StreamPlayerControl
End Class
