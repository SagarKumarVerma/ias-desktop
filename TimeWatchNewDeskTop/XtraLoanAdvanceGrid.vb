﻿Imports System.Data.OleDb
Imports DevExpress.XtraGrid.Views.Base
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.Data.SqlClient

Public Class XtraLoanAdvanceGrid
    Dim ulf As UserLookAndFeel
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    Public Sub New()
        InitializeComponent()
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True
        If Common.servername = "Access" Then
            Me.TbladvancedatA1TableAdapter1.Fill(Me.SSSDBDataSet.TBLADVANCEDATA1)
        Else
            TBLADVANCEDATATableAdapter.Connection.ConnectionString = Common.ConnectionString
            Me.TBLADVANCEDATATableAdapter.Fill(Me.SSSDBDataSet.TBLADVANCEDATA)
        End If
        Common.SetGridFont(GridView1, New Font("Tahoma", 11))
    End Sub
    Private Sub XtraLoanAdvanceGrid_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Me.Text = "ID No: " & XtraLoanAdvance.IDNO
        If Common.servername = "Access" Then
            Me.TbladvancedatA1TableAdapter1.Fill(Me.SSSDBDataSet.TBLADVANCEDATA1)
            'GridControl1.DataSource = SSSDBDataSet.TBLADVANCEDATA1
        Else
            TBLADVANCEDATATableAdapter.Connection.ConnectionString = Common.ConnectionString
            Me.TBLADVANCEDATATableAdapter.Fill(Me.SSSDBDataSet.TBLADVANCEDATA)
            'GridControl1.DataSource = SSSDBDataSet.TBLADVANCEDATA
        End If
        GridControl1.DataSource = Nothing
        Dim gridselet As String = "select * from TBLADVANCEDATA where IDNO = " & XtraLoanAdvance.IDNO & " and A_L = '" & XtraLoanAdvance.loanType & "' and paycode='" & XtraLoanAdvance.LAPaycode & "'"
        If Common.servername = "Access" Then
            Dim dataAdapter As New OleDbDataAdapter(gridselet, Common.con1)
            Dim WTDataTable As New DataTable("TBLADVANCE")
            dataAdapter.Fill(WTDataTable)
            GridControl1.DataSource = WTDataTable
        Else
            Dim dataAdapter As New SqlClient.SqlDataAdapter(gridselet, Common.con)
            Dim WTDataTable As New DataTable("TBLADVANCE")
            dataAdapter.Fill(WTDataTable)
            GridControl1.DataSource = WTDataTable
        End If
        TextEdit1.Text = ""
        CheckEditSalary.Checked = True
    End Sub
    Private Sub GridView1_CustomColumnDisplayText(sender As System.Object, e As DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs) Handles GridView1.CustomColumnDisplayText
        Try
            Dim view As ColumnView = TryCast(sender, ColumnView)
            If e.Column.FieldName = "A_L" Then
                Dim coll As String = view.GetListSourceRowCellValue(e.ListSourceRowIndex, "A_L")
                If coll = "L" Then
                    e.DisplayText = "Loan"
                ElseIf coll = "A" Then
                    e.DisplayText = "Advance"
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub
    Private Sub SimpleButtonSave_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonSave.Click

        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
        Dim MON_YEAR As DateTime = Convert.ToDateTime(row("MON_YEAR").ToString.Trim)
        Dim INST_AMT As Double = row("INST_AMT").ToString.Trim
        Dim CASH_AMT As Double = row("CASH_AMT").ToString.Trim
        Dim BALANCE_AMT As Double = row("BALANCE_AMT").ToString.Trim
        Dim INSTNO As Double = row("INSTNO").ToString.Trim
        Dim rsChkdata As DataSet = New DataSet 'ADODB.Recordset
        Dim txtAdInstallmentAmt As Double
        Dim iTotNoOfInstallment As Double
        Dim nIdno As Long, mTotalAdvance As Double, minstno As Integer, k As Integer, j As Integer
        'If chkType(0).Value = FLASE And optType(0).Value = True Then
        If XtraLoanAdvance.loanType = "L" And CheckEditSalary.Checked = True Then
            XtraMessageBox.Show(ulf, "<size=10>Balance Amount of Loan only Adjust through Cash</size>", "<size=9>Error</size>")
            Exit Sub
        End If
        Dim sSql As String = "Select * from tblAdvance Where Paycode='" & XtraLoanAdvance.LAPaycode & "' AND A_L='" & XtraLoanAdvance.loanType & "' and IDNO = " & XtraLoanAdvance.IDNO
        Dim sSql1 As String
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        rsChkdata = New DataSet
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(rsChkdata)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(rsChkdata)
        End If
        'rsChkdata = Cn.Execute(sSql)
        'sSql = "Select * from TBLADVANCEDATA Where Paycode='" & XtraLoanAdvance.LAPaycode & "' AND A_L='" & XtraLoanAdvance.loanType & "' and IDNO = " & XtraLoanAdvance.IDNO
        ''Dim tmp As DataSet = New DataSet
        'If Common.servername = "Access" Then
        '    adapA = New OleDbDataAdapter(sSql, Common.con1)
        '    adapA.Fill(tmp)
        'Else
        '    adap = New SqlDataAdapter(sSql, Common.con)
        '    adap.Fill(tmp)
        'End If
        '0=MON_YEAR
        '1 =INST_AMT
        '2 =PAMT
        '3=IAMT
        '4= CASH_AMT
        '5 = BALANCE_AMT
        '6=INSTNO

        ''adv
        '1 =INST_AMT
        '2 = CASH_AMT
        '3 = BALANCE_AMT
        '4 =INSTNO
        If rsChkdata.Tables(0).Rows.Count = 1 Then
            If XtraLoanAdvance.loanType = "A" Then
                'mTotalAdvance = Val(tmp.Tables(0).Rows(0).Item("INST_AMT").ToString.Trim) + Val(tmp.Tables(0).Rows(0).Item("PAMT").ToString.Trim) + Val(tmp.Tables(0).Rows(0).Item("IAMT").ToString.Trim)
                'mTotalAdvance = Val(tmp.Tables(0).Rows(0).Item("INST_AMT").ToString.Trim) + Val(tmp.Tables(0).Rows(0).Item("CASH_AMT").ToString.Trim) + Val(tmp.Tables(0).Rows(0).Item("BALANCE_AMT").ToString.Trim)
                mTotalAdvance = INST_AMT + CASH_AMT + BALANCE_AMT
                'mTotalAdvance = Val(tmp.Tables(0).Rows(0).Item("INST_AMT").ToString.Trim) + Val(tmp.Tables(0).Rows(0).Item("CASH_AMT").ToString.Trim) + Val(tmp.Tables(0).Rows(0).Item("BALANCE_AMT").ToString.Trim)
            End If
            If TextEdit1.Text.Trim = "" Then : txtAdInstallmentAmt = "0" : Else : txtAdInstallmentAmt = TextEdit1.Text.Trim : End If

            'ANJAN
            If CheckEditSalary.Checked = True Then
                If XtraLoanAdvance.loanType = "A" Then
                    'mTotalAdvance = mTotalAdvance - (Val(tmp.Tables(0).Rows(0).Item("CASH_AMT").ToString.Trim) + Val(txtAdInstallmentAmt))
                    mTotalAdvance = mTotalAdvance - (CASH_AMT + Val(txtAdInstallmentAmt))
                    'sSql = "update TBLADVANCEDATA set inst_amt=" & txtAdInstallmentAmt & ",Balance_amt=" & mTotalAdvance & " where Paycode='" & XtraLoanAdvance.LAPaycode & "' AND A_L='" & XtraLoanAdvance.loanType & "' and IDNO = " & XtraLoanAdvance.IDNO & " and mon_year='" & Convert.ToDateTime(tmp.Tables(0).Rows(0).Item("MON_YEAR").ToString.Trim).ToString("yyyy-MM-dd") & "'"
                    sSql = "update TBLADVANCEDATA set inst_amt=" & txtAdInstallmentAmt & ",Balance_amt=" & mTotalAdvance & " where Paycode='" & XtraLoanAdvance.LAPaycode & "' AND A_L='" & XtraLoanAdvance.loanType & "' and IDNO = " & XtraLoanAdvance.IDNO & " and mon_year='" & MON_YEAR.ToString("yyyy-MM-dd") & "'"
                    'Cn.Execute(sSql)
                    'sSql1 = "delete from TBLADVANCEDATA where Paycode='" & XtraLoanAdvance.LAPaycode & "' AND A_L='" & XtraLoanAdvance.loanType & "' and IDNO = " & XtraLoanAdvance.IDNO & " and mon_year>'" & Convert.ToDateTime(tmp.Tables(0).Rows(0).Item("MON_YEAR").ToString.Trim).ToString("yyyy-MM-dd") & "'"
                    sSql1 = "delete from TBLADVANCEDATA where Paycode='" & XtraLoanAdvance.LAPaycode & "' AND A_L='" & XtraLoanAdvance.loanType & "' and IDNO = " & XtraLoanAdvance.IDNO & " and mon_year>'" & MON_YEAR.ToString("yyyy-MM-dd") & "'"
                    'Cn.Execute(sSql)
                Else
                    'mTotalAdvance = mTotalAdvance - (Val(tmp.Tables(0).Rows(0).Item("CASH_AMT").ToString.Trim) + Val(txtAdInstallmentAmt))
                    mTotalAdvance = mTotalAdvance - CASH_AMT + Val(txtAdInstallmentAmt)
                    'sSql = "update TBLADVANCEDATA set inst_amt=" & txtAdInstallmentAmt & ",Balance_amt=" & mTotalAdvance & " where Paycode='" & XtraLoanAdvance.LAPaycode & "' AND A_L='" & XtraLoanAdvance.loanType & "' and IDNO = " & XtraLoanAdvance.IDNO & " and mon_year='" & Convert.ToDateTime(tmp.Tables(0).Rows(0).Item("MON_YEAR").ToString.Trim).ToString("yyyy-MM-dd") & "'"
                    sSql = "update TBLADVANCEDATA set inst_amt=" & txtAdInstallmentAmt & ",Balance_amt=" & mTotalAdvance & " where Paycode='" & XtraLoanAdvance.LAPaycode & "' AND A_L='" & XtraLoanAdvance.loanType & "' and IDNO = " & XtraLoanAdvance.IDNO & " and mon_year='" & MON_YEAR.ToString("yyyy-MM-dd") & "'"
                    'Cn.Execute(sSql)
                    'sSql1 = "delete from TBLADVANCEDATA where Paycode='" & XtraLoanAdvance.LAPaycode & "' AND A_L='" & XtraLoanAdvance.loanType & "' and IDNO = " & XtraLoanAdvance.IDNO & " and mon_year>'" & Convert.ToDateTime(tmp.Tables(0).Rows(0).Item("MON_YEAR").ToString.Trim).ToString("yyyy-MM-dd") & "'"
                    sSql1 = "delete from TBLADVANCEDATA where Paycode='" & XtraLoanAdvance.LAPaycode & "' AND A_L='" & XtraLoanAdvance.loanType & "' and IDNO = " & XtraLoanAdvance.IDNO & " and mon_year>'" & MON_YEAR.ToString("yyyy-MM-dd") & "'"

                    'Cn.Execute(sSql)
                End If
                If Common.servername = "Access" Then
                    If Common.con1.State <> ConnectionState.Open Then
                        Common.con1.Open()
                    End If
                    cmd1 = New OleDbCommand(sSql, Common.con1)
                    cmd1.ExecuteNonQuery()
                    cmd1 = New OleDbCommand(sSql1, Common.con1)
                    cmd1.ExecuteNonQuery()
                    If Common.con1.State <> ConnectionState.Closed Then
                        Common.con1.Close()
                    End If
                Else
                    If Common.con.State <> ConnectionState.Open Then
                        Common.con.Open()
                    End If
                    cmd = New SqlCommand(sSql, Common.con)
                    cmd.ExecuteNonQuery()
                    cmd = New SqlCommand(sSql1, Common.con)
                    cmd.ExecuteNonQuery()
                    If Common.con.State <> ConnectionState.Closed Then
                        Common.con.Close()
                    End If
                End If
            Else
                If XtraLoanAdvance.loanType = "L" And CheckEditCash.Checked = True Then
                    'If Val(txtAdInstallmentAmt) <> Val(tmp.Tables(0).Rows(0).Item("BALANCE_AMT").ToString.Trim) Then
                    If Val(txtAdInstallmentAmt) <> BALANCE_AMT Then
                        XtraMessageBox.Show(ulf, "<size=10>Adjust Amount not equal to Balance Amount</size>", "<size=9>Error</size>")
                        Exit Sub
                    End If
                End If
                'mTotalAdvance = Math.Round(mTotalAdvance, 2) - Math.Round(Val(tmp.Tables(0).Rows(0).Item("INST_AMT").ToString.Trim) + Val(txtAdInstallmentAmt), 2)
                mTotalAdvance = Math.Round(mTotalAdvance, 2) - Math.Round(INST_AMT + Val(txtAdInstallmentAmt), 2)
                If Common.servername = "Access" Then
                    'sSql = "update TBLADVANCEDATA set cash_amt=" & txtAdInstallmentAmt & ",Balance_amt=" & mTotalAdvance & " where Paycode='" & XtraLoanAdvance.LAPaycode & "' AND A_L='" & XtraLoanAdvance.loanType & "' and IDNO = " & XtraLoanAdvance.IDNO & " and FORMAT(mon_year,'yyyy-MM-dd')='" & Convert.ToDateTime(tmp.Tables(0).Rows(0).Item("MON_YEAR").ToString.Trim).ToString("yyyy-MM-dd") & "'"
                    sSql = "update TBLADVANCEDATA set cash_amt=" & txtAdInstallmentAmt & ",Balance_amt=" & mTotalAdvance & " where Paycode='" & XtraLoanAdvance.LAPaycode & "' AND A_L='" & XtraLoanAdvance.loanType & "' and IDNO = " & XtraLoanAdvance.IDNO & " and FORMAT(mon_year,'yyyy-MM-dd')='" & MON_YEAR.ToString("yyyy-MM-dd") & "'"
                Else
                    'sSql = "update TBLADVANCEDATA set cash_amt=" & txtAdInstallmentAmt & ",Balance_amt=" & mTotalAdvance & " where Paycode='" & XtraLoanAdvance.LAPaycode & "' AND A_L='" & XtraLoanAdvance.loanType & "' and IDNO = " & XtraLoanAdvance.IDNO & " and mon_year='" & Convert.ToDateTime(tmp.Tables(0).Rows(0).Item("MON_YEAR").ToString.Trim).ToString("yyyy-MM-dd") & "'"
                    sSql = "update TBLADVANCEDATA set cash_amt=" & txtAdInstallmentAmt & ",Balance_amt=" & mTotalAdvance & " where Paycode='" & XtraLoanAdvance.LAPaycode & "' AND A_L='" & XtraLoanAdvance.loanType & "' and IDNO = " & XtraLoanAdvance.IDNO & " and mon_year='" & MON_YEAR.ToString("yyyy-MM-dd") & "'"
                End If
                If Common.servername = "Access" Then
                    If Common.con1.State <> ConnectionState.Open Then
                        Common.con1.Open()
                    End If
                    cmd1 = New OleDbCommand(sSql, Common.con1)
                    cmd1.ExecuteNonQuery()
                    If Common.con1.State <> ConnectionState.Closed Then
                        Common.con1.Close()
                    End If
                Else
                    If Common.con.State <> ConnectionState.Open Then
                        Common.con.Open()
                    End If
                    cmd = New SqlCommand(sSql, Common.con)
                    cmd.ExecuteNonQuery()
                    If Common.con.State <> ConnectionState.Closed Then
                        Common.con.Close()
                    End If
                End If
                'Cn.Execute(sSql)
                If Common.servername = "Access" Then
                    'sSql = "delete from TBLADVANCEDATA where Paycode='" & XtraLoanAdvance.LAPaycode & "' AND A_L='" & XtraLoanAdvance.loanType & "' and IDNO = " & XtraLoanAdvance.IDNO & " and FORMAT(mon_year,'YYYY-MM-DD')>'" & Convert.ToDateTime(tmp.Tables(0).Rows(0).Item("MON_YEAR").ToString.Trim).ToString("yyyy-MM-dd") & "'"
                    sSql = "delete from TBLADVANCEDATA where Paycode='" & XtraLoanAdvance.LAPaycode & "' AND A_L='" & XtraLoanAdvance.loanType & "' and IDNO = " & XtraLoanAdvance.IDNO & " and FORMAT(mon_year,'YYYY-MM-DD')>'" & MON_YEAR.ToString("yyyy-MM-dd") & "'"
                Else
                    'sSql = "delete from TBLADVANCEDATA where Paycode='" & XtraLoanAdvance.LAPaycode & "' AND A_L='" & XtraLoanAdvance.loanType & "' and IDNO = " & XtraLoanAdvance.IDNO & " and mon_year>'" & Convert.ToDateTime(tmp.Tables(0).Rows(0).Item("MON_YEAR").ToString.Trim).ToString("yyyy-MM-dd") & "'"
                    sSql = "delete from TBLADVANCEDATA where Paycode='" & XtraLoanAdvance.LAPaycode & "' AND A_L='" & XtraLoanAdvance.loanType & "' and IDNO = " & XtraLoanAdvance.IDNO & " and mon_year>'" & MON_YEAR.ToString("yyyy-MM-dd") & "'"
                End If
                If Common.servername = "Access" Then
                    If Common.con1.State <> ConnectionState.Open Then
                        Common.con1.Open()
                    End If
                    cmd1 = New OleDbCommand(sSql, Common.con1)
                    cmd1.ExecuteNonQuery()
                    If Common.con1.State <> ConnectionState.Closed Then
                        Common.con1.Close()
                    End If
                Else
                    If Common.con.State <> ConnectionState.Open Then
                        Common.con.Open()
                    End If
                    cmd = New SqlCommand(sSql, Common.con)
                    cmd.ExecuteNonQuery()
                    If Common.con.State <> ConnectionState.Closed Then
                        Common.con.Close()
                    End If
                End If
                'Cn.Execute(sSql)
            End If
            If mTotalAdvance = 0 Then
                'Call cboIDno_Click()
                Exit Sub
            End If
            If XtraLoanAdvance.loanType = "A" Then
                iTotNoOfInstallment = Int(mTotalAdvance / rsChkdata.Tables(0).Rows(0).Item("inst_amt"))
                'iTotNoOfInstallment = INSTNO - 1
                If (mTotalAdvance / rsChkdata.Tables(0).Rows(0).Item("inst_amt")) - (Int(mTotalAdvance / rsChkdata.Tables(0).Rows(0).Item("inst_amt"))) > 0 Then
                    iTotNoOfInstallment = iTotNoOfInstallment + 1
                End If
                k = 1
                minstno = INSTNO 'iTotNoOfInstallment
            Else
                iTotNoOfInstallment = rsChkdata.Tables(0).Rows(0).Item("instno")
                'k = rsChkdata.Tables(0).Rows(0).Item("instno") - (Val(tmp.Tables(0).Rows(0).Item("INSTNO").ToString.Trim) - 2)
                'minstno = (Val(tmp.Tables(0).Rows(0).Item("INSTNO").ToString.Trim) - 1)
                k = rsChkdata.Tables(0).Rows(0).Item("instno") - INSTNO - 2
                minstno = INSTNO - 1
            End If
            'iTotNoOfInstallment = Val(txtAdInstallemtTimes)

            Dim iRestAmount As Double = (mTotalAdvance)
            Dim TempRestAmt As Double = (mTotalAdvance)
            'Dim dInstStartMonth As DateTime = Convert.ToDateTime(tmp.Tables(0).Rows(0).Item("MON_YEAR").ToString.Trim).AddMonths(1)
            Dim dInstStartMonth As DateTime = MON_YEAR.AddMonths(1)
            j = 1
            For i = k To iTotNoOfInstallment
                If XtraLoanAdvance.loanType = "A" Then
                    iRestAmount = iRestAmount - rsChkdata.Tables(0).Rows(0).Item("inst_amt")
                    If TempRestAmt < rsChkdata.Tables(0).Rows(0).Item("inst_amt") Then
                        txtAdInstallmentAmt = Format(TempRestAmt, "000000.00")
                        iRestAmount = 0
                    Else
                        txtAdInstallmentAmt = rsChkdata.Tables(0).Rows(0).Item("inst_amt")
                    End If
                Else
                    iRestAmount = Math.Round(iRestAmount - (rsChkdata.Tables(0).Rows(0).Item("inst_amt") - Math.Round(IPmt((rsChkdata.Tables(0).Rows(0).Item("intrest_rate") / 1200), i, rsChkdata.Tables(0).Rows(0).Item("instno"), -rsChkdata.Tables(0).Rows(0).Item("adv_amt"), 0), 2)), 2)
                    txtAdInstallmentAmt = rsChkdata.Tables(0).Rows(0).Item("inst_amt")
                    If iRestAmount < Math.Round(IPmt((rsChkdata.Tables(0).Rows(0).Item("intrest_rate") / 1200), i, iTotNoOfInstallment, -rsChkdata.Tables(0).Rows(0).Item("adv_amt"), 0), 2) Then
                        iRestAmount = 0
                    End If
                End If
                'sMonth = Mid(dInstStartMonth, (InStr(1, CStr(dInstStartMonth), "/") + 1), 2)
                'sYear = Right(dInstStartMonth, 4)
                If XtraLoanAdvance.LAPaycode = "A" Then
                    sSql = "Insert into TBLADVANCEDATA Values('" & XtraLoanAdvance.LAPaycode & "','" & XtraLoanAdvance.loanType & "'," & XtraLoanAdvance.IDNO & _
                            ",'" & dInstStartMonth.ToString("yyyy-MM-01") & _
                            "'," & txtAdInstallmentAmt & ",0," & iRestAmount & ",0,0," & minstno & ")"
                Else
                    sSql = "Insert into TBLADVANCEDATA Values('" & XtraLoanAdvance.LAPaycode & "','" & XtraLoanAdvance.loanType & "'," & XtraLoanAdvance.IDNO & _
                            ",'" & dInstStartMonth.ToString("yyyy-MM-01") & _
                            "'," & txtAdInstallmentAmt & ",0," & iRestAmount & "," & (txtAdInstallmentAmt - Math.Round(IPmt((rsChkdata.Tables(0).Rows(0).Item("intrest_rate") / 1200), i, iTotNoOfInstallment, -rsChkdata.Tables(0).Rows(0).Item("adv_amt"), 0), 2)) & "," & Math.Round(IPmt((rsChkdata.Tables(0).Rows(0).Item("intrest_rate") / 1200), i, iTotNoOfInstallment, -rsChkdata.Tables(0).Rows(0).Item("adv_amt"), 0), 2) & "," & minstno & ")"
                End If
                If Common.servername = "Access" Then
                    If Common.con1.State <> ConnectionState.Open Then
                        Common.con1.Open()
                    End If
                    cmd1 = New OleDbCommand(sSql, Common.con1)
                    cmd1.ExecuteNonQuery()
                    If Common.con1.State <> ConnectionState.Closed Then
                        Common.con1.Close()
                    End If
                Else
                    If Common.con.State <> ConnectionState.Open Then
                        Common.con.Open()
                    End If
                    cmd = New SqlCommand(sSql, Common.con)
                    cmd.ExecuteNonQuery()
                    If Common.con.State <> ConnectionState.Closed Then
                        Common.con.Close()
                    End If
                End If
                'Cn.Execute(sSql)
                TempRestAmt = iRestAmount
                dInstStartMonth = dInstStartMonth.AddMonths(j) ' DateAdd("m", j, Format(dInstStartMonth, "DD/MM/YYYY"))
                'bAdvanceApply = True
                minstno = minstno - 1
            Next
        End If
        XtraMessageBox.Show(ulf, "<size=10>Saved Successfully</size>", "<size=9>Success</size>")
        Me.Close()
    End Sub
End Class