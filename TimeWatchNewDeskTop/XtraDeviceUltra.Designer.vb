﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraDeviceUltra
    Inherits DevExpress.XtraEditors.XtraUserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(XtraDevice))
        Me.TblMachineBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SSSDBDataSet = New iAS.SSSDBDataSet()
        Me.TblMachineTableAdapter = New iAS.SSSDBDataSetTableAdapters.tblMachineTableAdapter()
        Me.BehaviorManager1 = New DevExpress.Utils.Behaviors.BehaviorManager(Me.components)
        Me.TblMachine1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.tblMachine1TableAdapter()
        Me.SplitContainerControl1 = New DevExpress.XtraEditors.SplitContainerControl()
        Me.SidePanelDevice = New DevExpress.XtraEditors.SidePanel()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colID_NO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTextEdit5 = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.colA_R = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.ProtocolComboBox1 = New DevExpress.XtraEditors.Repository.RepositoryItemComboBox()
        Me.colIN_OUT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.TypeComboBox1 = New DevExpress.XtraEditors.Repository.RepositoryItemComboBox()
        Me.colDeviceType = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.DeviceListComboBox1 = New DevExpress.XtraEditors.Repository.RepositoryItemComboBox()
        Me.colLOCATION = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTextEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.colbranch = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTextEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.colcommkey = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTextEdit3 = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.colMAC_ADDRESS = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTextEdit4 = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.colLastModifiedBy = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLastModifiedDate = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.Status = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colHLogin = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colHPassword = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colUserCount = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colFaceCount = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemComboBox1 = New DevExpress.XtraEditors.Repository.RepositoryItemComboBox()
        Me.RepositoryItemLookUpEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.TblbranchBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SidePanelSearch = New DevExpress.XtraEditors.SidePanel()
        Me.GridControlSearch = New DevExpress.XtraGrid.GridControl()
        Me.GridViewSearch = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.RepositoryItemComboBox4 = New DevExpress.XtraEditors.Repository.RepositoryItemComboBox()
        Me.RepositoryItemComboBox2 = New DevExpress.XtraEditors.Repository.RepositoryItemComboBox()
        Me.RepositoryItemComboBox3 = New DevExpress.XtraEditors.Repository.RepositoryItemComboBox()
        Me.RepositoryItemTextEdit7 = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.RepositoryItemTextEdit8 = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.RepositoryItemTextEdit9 = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.RepositoryItemTextEdit10 = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.RepositoryItemTextEdit6 = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.RepositoryItemComboBox5 = New DevExpress.XtraEditors.Repository.RepositoryItemComboBox()
        Me.RepositoryItemLookUpEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit()
        Me.SidePanelTitle = New DevExpress.XtraEditors.SidePanel()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.MemoEdit1 = New DevExpress.XtraEditors.MemoEdit()
        Me.BarManager1 = New DevExpress.XtraBars.BarManager(Me.components)
        Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl()
        Me.BarButtonItem1 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem2 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarManager2 = New DevExpress.XtraBars.BarManager(Me.components)
        Me.Bar1 = New DevExpress.XtraBars.Bar()
        Me.BarButtonItem3 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem4 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonNewLogs = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonOldLogs = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonDeviceStatus = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem6 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem7 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarDockControl1 = New DevExpress.XtraBars.BarDockControl()
        Me.BarDockControl2 = New DevExpress.XtraBars.BarDockControl()
        Me.BarDockControl3 = New DevExpress.XtraBars.BarDockControl()
        Me.BarDockControl4 = New DevExpress.XtraBars.BarDockControl()
        Me.TblbranchTableAdapter = New iAS.SSSDBDataSetTableAdapters.tblbranchTableAdapter()
        Me.Tblbranch1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.tblbranch1TableAdapter()
        CType(Me.TblMachineBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BehaviorManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerControl1.SuspendLayout()
        Me.SidePanelDevice.SuspendLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTextEdit5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProtocolComboBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TypeComboBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DeviceListComboBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTextEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTextEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTextEdit3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTextEdit4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemComboBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemLookUpEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblbranchBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SidePanelSearch.SuspendLayout()
        CType(Me.GridControlSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewSearch, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemComboBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemComboBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemComboBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTextEdit7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTextEdit8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTextEdit9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTextEdit10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTextEdit6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemComboBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemLookUpEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SidePanelTitle.SuspendLayout()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BarManager2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TblMachineBindingSource
        '
        Me.TblMachineBindingSource.DataMember = "tblMachine"
        Me.TblMachineBindingSource.DataSource = Me.SSSDBDataSet
        '
        'SSSDBDataSet
        '
        Me.SSSDBDataSet.DataSetName = "SSSDBDataSet"
        Me.SSSDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'TblMachineTableAdapter
        '
        Me.TblMachineTableAdapter.ClearBeforeFill = True
        '
        'TblMachine1TableAdapter1
        '
        Me.TblMachine1TableAdapter1.ClearBeforeFill = True
        '
        'SplitContainerControl1
        '
        Me.SplitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainerControl1.Location = New System.Drawing.Point(0, 31)
        Me.SplitContainerControl1.Name = "SplitContainerControl1"
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.SidePanelDevice)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.SidePanelSearch)
        Me.SplitContainerControl1.Panel1.Text = "Panel1"
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.MemoEdit1)
        Me.SplitContainerControl1.Panel2.Text = "Panel2"
        Me.SplitContainerControl1.Size = New System.Drawing.Size(1215, 462)
        Me.SplitContainerControl1.SplitterPosition = 1050
        Me.SplitContainerControl1.TabIndex = 3
        Me.SplitContainerControl1.Text = "SplitContainerControl1"
        '
        'SidePanelDevice
        '
        Me.SidePanelDevice.Controls.Add(Me.GridControl1)
        Me.SidePanelDevice.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SidePanelDevice.Location = New System.Drawing.Point(0, 0)
        Me.SidePanelDevice.Name = "SidePanelDevice"
        Me.SidePanelDevice.Size = New System.Drawing.Size(1050, 309)
        Me.SidePanelDevice.TabIndex = 3
        Me.SidePanelDevice.Text = "SidePanel1"
        '
        'GridControl1
        '
        Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControl1.EmbeddedNavigator.Buttons.Append.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.Edit.Enabled = False
        Me.GridControl1.EmbeddedNavigator.Buttons.Edit.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = False
        Me.GridControl1.EmbeddedNavigator.Cursor = System.Windows.Forms.Cursors.Default
        Me.GridControl1.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.GridControl1.Location = New System.Drawing.Point(0, 0)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.DeviceListComboBox1, Me.ProtocolComboBox1, Me.TypeComboBox1, Me.RepositoryItemTextEdit1, Me.RepositoryItemTextEdit2, Me.RepositoryItemTextEdit3, Me.RepositoryItemTextEdit4, Me.RepositoryItemTextEdit5, Me.RepositoryItemComboBox1, Me.RepositoryItemLookUpEdit1})
        Me.GridControl1.Size = New System.Drawing.Size(1050, 309)
        Me.GridControl1.TabIndex = 1
        Me.GridControl1.UseEmbeddedNavigator = True
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.Appearance.HeaderPanel.BackColor = System.Drawing.Color.LightSteelBlue
        Me.GridView1.Appearance.HeaderPanel.BackColor2 = System.Drawing.Color.LightSteelBlue
        Me.GridView1.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.Gray
        Me.GridView1.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black
        Me.GridView1.Appearance.HeaderPanel.Options.UseBackColor = True
        Me.GridView1.Appearance.HeaderPanel.Options.UseBorderColor = True
        Me.GridView1.Appearance.HeaderPanel.Options.UseForeColor = True
        Me.GridView1.Appearance.TopNewRow.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.GridView1.Appearance.TopNewRow.ForeColor = System.Drawing.Color.Blue
        Me.GridView1.Appearance.TopNewRow.Options.UseFont = True
        Me.GridView1.Appearance.TopNewRow.Options.UseForeColor = True
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colID_NO, Me.colA_R, Me.colIN_OUT, Me.colDeviceType, Me.colLOCATION, Me.colbranch, Me.colcommkey, Me.colMAC_ADDRESS, Me.colLastModifiedBy, Me.colLastModifiedDate, Me.GridColumn1, Me.Status, Me.colHLogin, Me.colHPassword, Me.colUserCount, Me.colFaceCount})
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.NewItemRowText = "Click here to add new Device"
        Me.GridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[True]
        Me.GridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[True]
        Me.GridView1.OptionsBehavior.EditingMode = DevExpress.XtraGrid.Views.Grid.GridEditingMode.EditForm
        Me.GridView1.OptionsEditForm.EditFormColumnCount = 1
        Me.GridView1.OptionsEditForm.PopupEditFormWidth = 400
        Me.GridView1.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridView1.OptionsSelection.MultiSelect = True
        Me.GridView1.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        Me.GridView1.OptionsView.ColumnAutoWidth = False
        Me.GridView1.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top
        Me.GridView1.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colID_NO, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colID_NO
        '
        Me.colID_NO.ColumnEdit = Me.RepositoryItemTextEdit5
        Me.colID_NO.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.colID_NO.FieldName = "ID_NO"
        Me.colID_NO.Name = "colID_NO"
        Me.colID_NO.SortMode = DevExpress.XtraGrid.ColumnSortMode.Value
        Me.colID_NO.Visible = True
        Me.colID_NO.VisibleIndex = 1
        Me.colID_NO.Width = 100
        '
        'RepositoryItemTextEdit5
        '
        Me.RepositoryItemTextEdit5.AutoHeight = False
        Me.RepositoryItemTextEdit5.Mask.EditMask = "[0-9]*"
        Me.RepositoryItemTextEdit5.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.RepositoryItemTextEdit5.MaxLength = 3
        Me.RepositoryItemTextEdit5.Name = "RepositoryItemTextEdit5"
        '
        'colA_R
        '
        Me.colA_R.ColumnEdit = Me.ProtocolComboBox1
        Me.colA_R.FieldName = "A_R"
        Me.colA_R.Name = "colA_R"
        Me.colA_R.Visible = True
        Me.colA_R.VisibleIndex = 2
        Me.colA_R.Width = 100
        '
        'ProtocolComboBox1
        '
        Me.ProtocolComboBox1.AutoHeight = False
        Me.ProtocolComboBox1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ProtocolComboBox1.Items.AddRange(New Object() {"TCP/IP", "USB"})
        Me.ProtocolComboBox1.MaxLength = 10
        Me.ProtocolComboBox1.Name = "ProtocolComboBox1"
        '
        'colIN_OUT
        '
        Me.colIN_OUT.ColumnEdit = Me.TypeComboBox1
        Me.colIN_OUT.FieldName = "IN_OUT"
        Me.colIN_OUT.Name = "colIN_OUT"
        Me.colIN_OUT.Visible = True
        Me.colIN_OUT.VisibleIndex = 3
        '
        'TypeComboBox1
        '
        Me.TypeComboBox1.AutoHeight = False
        Me.TypeComboBox1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.TypeComboBox1.Items.AddRange(New Object() {"IN", "OUT"})
        Me.TypeComboBox1.MaxLength = 10
        Me.TypeComboBox1.Name = "TypeComboBox1"
        Me.TypeComboBox1.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        '
        'colDeviceType
        '
        Me.colDeviceType.Caption = "Device Model"
        Me.colDeviceType.ColumnEdit = Me.DeviceListComboBox1
        Me.colDeviceType.FieldName = "DeviceType"
        Me.colDeviceType.Name = "colDeviceType"
        '
        'DeviceListComboBox1
        '
        Me.DeviceListComboBox1.AutoHeight = False
        Me.DeviceListComboBox1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DeviceListComboBox1.Items.AddRange(New Object() {"Face Id", "Bio-101/Bio-202/ATF-1002/ATF-3001/ATF-3003/TimeWatch (ATF-3/303/3011)", "F10-SF", "ZK(TFT)", "SecureEye", "Bio-1/3/4/5/6/7/ATF-203/ATF-395/ACR-872", "BioFace", "Bio-2/10/Bio201/Bio-203/Bio-204/Bio-909", "Bio-8", "Bio-9", "Bio-868", "Bio-868A", "Honeywell-WinPakPro", "ZK-GPRS", "ZK-4 Door Controller", "Porys 4 Door Controller", "TimeWatch Access Control"})
        Me.DeviceListComboBox1.Name = "DeviceListComboBox1"
        '
        'colLOCATION
        '
        Me.colLOCATION.Caption = "Location"
        Me.colLOCATION.ColumnEdit = Me.RepositoryItemTextEdit1
        Me.colLOCATION.FieldName = "LOCATION"
        Me.colLOCATION.Name = "colLOCATION"
        Me.colLOCATION.Visible = True
        Me.colLOCATION.VisibleIndex = 4
        Me.colLOCATION.Width = 120
        '
        'RepositoryItemTextEdit1
        '
        Me.RepositoryItemTextEdit1.AutoHeight = False
        Me.RepositoryItemTextEdit1.MaxLength = 15
        Me.RepositoryItemTextEdit1.Name = "RepositoryItemTextEdit1"
        '
        'colbranch
        '
        Me.colbranch.ColumnEdit = Me.RepositoryItemTextEdit2
        Me.colbranch.FieldName = "branch"
        Me.colbranch.Name = "colbranch"
        Me.colbranch.Visible = True
        Me.colbranch.VisibleIndex = 5
        Me.colbranch.Width = 120
        '
        'RepositoryItemTextEdit2
        '
        Me.RepositoryItemTextEdit2.AutoHeight = False
        Me.RepositoryItemTextEdit2.MaxLength = 50
        Me.RepositoryItemTextEdit2.Name = "RepositoryItemTextEdit2"
        '
        'colcommkey
        '
        Me.colcommkey.Caption = "CommKey"
        Me.colcommkey.ColumnEdit = Me.RepositoryItemTextEdit3
        Me.colcommkey.FieldName = "commkey"
        Me.colcommkey.Name = "colcommkey"
        '
        'RepositoryItemTextEdit3
        '
        Me.RepositoryItemTextEdit3.AutoHeight = False
        Me.RepositoryItemTextEdit3.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.RepositoryItemTextEdit3.Mask.EditMask = "##########"
        Me.RepositoryItemTextEdit3.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.RepositoryItemTextEdit3.MaxLength = 10
        Me.RepositoryItemTextEdit3.Name = "RepositoryItemTextEdit3"
        '
        'colMAC_ADDRESS
        '
        Me.colMAC_ADDRESS.Caption = "Serial No"
        Me.colMAC_ADDRESS.ColumnEdit = Me.RepositoryItemTextEdit4
        Me.colMAC_ADDRESS.FieldName = "MAC_ADDRESS"
        Me.colMAC_ADDRESS.Name = "colMAC_ADDRESS"
        Me.colMAC_ADDRESS.Visible = True
        Me.colMAC_ADDRESS.VisibleIndex = 7
        Me.colMAC_ADDRESS.Width = 200
        '
        'RepositoryItemTextEdit4
        '
        Me.RepositoryItemTextEdit4.AutoHeight = False
        Me.RepositoryItemTextEdit4.MaxLength = 50
        Me.RepositoryItemTextEdit4.Name = "RepositoryItemTextEdit4"
        '
        'colLastModifiedBy
        '
        Me.colLastModifiedBy.Caption = "Status"
        Me.colLastModifiedBy.FieldName = "LastModifiedBy"
        Me.colLastModifiedBy.Name = "colLastModifiedBy"
        Me.colLastModifiedBy.Width = 100
        '
        'colLastModifiedDate
        '
        Me.colLastModifiedDate.FieldName = "LastModifiedDate"
        Me.colLastModifiedDate.Name = "colLastModifiedDate"
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Purpose"
        Me.GridColumn1.FieldName = "Purpose"
        Me.GridColumn1.Name = "GridColumn1"
        '
        'Status
        '
        Me.Status.Caption = "Status"
        Me.Status.FieldName = "Status"
        Me.Status.Name = "Status"
        Me.Status.Visible = True
        Me.Status.VisibleIndex = 6
        Me.Status.Width = 100
        '
        'colHLogin
        '
        Me.colHLogin.Caption = "HLogin"
        Me.colHLogin.FieldName = "HLogin"
        Me.colHLogin.Name = "colHLogin"
        '
        'colHPassword
        '
        Me.colHPassword.Caption = "HPassword"
        Me.colHPassword.FieldName = "HPassword"
        Me.colHPassword.Name = "colHPassword"
        '
        'colUserCount
        '
        Me.colUserCount.Caption = "User Count"
        Me.colUserCount.FieldName = "UserCount"
        Me.colUserCount.Name = "colUserCount"
        Me.colUserCount.Visible = True
        Me.colUserCount.VisibleIndex = 8
        Me.colUserCount.Width = 100
        '
        'colFaceCount
        '
        Me.colFaceCount.Caption = "Face Count"
        Me.colFaceCount.FieldName = "FaceCount"
        Me.colFaceCount.Name = "colFaceCount"
        Me.colFaceCount.Visible = True
        Me.colFaceCount.VisibleIndex = 9
        Me.colFaceCount.Width = 100
        '
        'RepositoryItemComboBox1
        '
        Me.RepositoryItemComboBox1.AutoHeight = False
        Me.RepositoryItemComboBox1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemComboBox1.Name = "RepositoryItemComboBox1"
        '
        'RepositoryItemLookUpEdit1
        '
        Me.RepositoryItemLookUpEdit1.AutoHeight = False
        Me.RepositoryItemLookUpEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemLookUpEdit1.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("BRANCHNAME", "BRANCHNAME", 79, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near)})
        Me.RepositoryItemLookUpEdit1.DataSource = Me.TblbranchBindingSource
        Me.RepositoryItemLookUpEdit1.DisplayMember = "BRANCHNAME"
        Me.RepositoryItemLookUpEdit1.Name = "RepositoryItemLookUpEdit1"
        Me.RepositoryItemLookUpEdit1.NullText = ""
        Me.RepositoryItemLookUpEdit1.ValueMember = "BRANCHNAME"
        '
        'TblbranchBindingSource
        '
        Me.TblbranchBindingSource.DataMember = "tblbranch"
        Me.TblbranchBindingSource.DataSource = Me.SSSDBDataSet
        '
        'SidePanelSearch
        '
        Me.SidePanelSearch.Controls.Add(Me.GridControlSearch)
        Me.SidePanelSearch.Controls.Add(Me.SidePanelTitle)
        Me.SidePanelSearch.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.SidePanelSearch.Location = New System.Drawing.Point(0, 309)
        Me.SidePanelSearch.Name = "SidePanelSearch"
        Me.SidePanelSearch.Size = New System.Drawing.Size(1050, 153)
        Me.SidePanelSearch.TabIndex = 2
        Me.SidePanelSearch.Text = "SidePanel1"
        '
        'GridControlSearch
        '
        Me.GridControlSearch.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControlSearch.EmbeddedNavigator.Buttons.Append.Visible = False
        Me.GridControlSearch.EmbeddedNavigator.Buttons.CancelEdit.Visible = False
        Me.GridControlSearch.EmbeddedNavigator.Buttons.Edit.Enabled = False
        Me.GridControlSearch.EmbeddedNavigator.Buttons.Edit.Visible = False
        Me.GridControlSearch.EmbeddedNavigator.Buttons.EndEdit.Visible = False
        Me.GridControlSearch.EmbeddedNavigator.Buttons.Remove.Visible = False
        Me.GridControlSearch.EmbeddedNavigator.Cursor = System.Windows.Forms.Cursors.Default
        Me.GridControlSearch.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.GridControlSearch.Location = New System.Drawing.Point(0, 24)
        Me.GridControlSearch.MainView = Me.GridViewSearch
        Me.GridControlSearch.Name = "GridControlSearch"
        Me.GridControlSearch.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemComboBox4, Me.RepositoryItemComboBox2, Me.RepositoryItemComboBox3, Me.RepositoryItemTextEdit7, Me.RepositoryItemTextEdit8, Me.RepositoryItemTextEdit9, Me.RepositoryItemTextEdit10, Me.RepositoryItemTextEdit6, Me.RepositoryItemComboBox5, Me.RepositoryItemLookUpEdit2})
        Me.GridControlSearch.Size = New System.Drawing.Size(1050, 129)
        Me.GridControlSearch.TabIndex = 2
        Me.GridControlSearch.UseEmbeddedNavigator = True
        Me.GridControlSearch.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewSearch})
        '
        'GridViewSearch
        '
        Me.GridViewSearch.Appearance.HeaderPanel.BackColor = System.Drawing.Color.LightSteelBlue
        Me.GridViewSearch.Appearance.HeaderPanel.BackColor2 = System.Drawing.Color.LightSteelBlue
        Me.GridViewSearch.Appearance.HeaderPanel.BorderColor = System.Drawing.Color.Gray
        Me.GridViewSearch.Appearance.HeaderPanel.ForeColor = System.Drawing.Color.Black
        Me.GridViewSearch.Appearance.HeaderPanel.Options.UseBackColor = True
        Me.GridViewSearch.Appearance.HeaderPanel.Options.UseBorderColor = True
        Me.GridViewSearch.Appearance.HeaderPanel.Options.UseForeColor = True
        Me.GridViewSearch.Appearance.TopNewRow.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.GridViewSearch.Appearance.TopNewRow.ForeColor = System.Drawing.Color.Blue
        Me.GridViewSearch.Appearance.TopNewRow.Options.UseFont = True
        Me.GridViewSearch.Appearance.TopNewRow.Options.UseForeColor = True
        Me.GridViewSearch.GridControl = Me.GridControlSearch
        Me.GridViewSearch.Name = "GridViewSearch"
        Me.GridViewSearch.NewItemRowText = "Double click on row to add new Device"
        Me.GridViewSearch.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewSearch.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewSearch.OptionsBehavior.EditingMode = DevExpress.XtraGrid.Views.Grid.GridEditingMode.EditForm
        Me.GridViewSearch.OptionsEditForm.EditFormColumnCount = 1
        Me.GridViewSearch.OptionsEditForm.PopupEditFormWidth = 400
        Me.GridViewSearch.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridViewSearch.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        Me.GridViewSearch.OptionsView.ShowGroupPanel = False
        '
        'RepositoryItemComboBox4
        '
        Me.RepositoryItemComboBox4.AutoHeight = False
        Me.RepositoryItemComboBox4.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemComboBox4.Items.AddRange(New Object() {"Face Id", "Bio-101/Bio-202/ATF-1002/ATF-3001/ATF-3003/TimeWatch (ATF-3/303/3011)", "F10-SF", "ZK(TFT)", "SecureEye", "Bio-1/3/4/5/6/7/ATF-203/ATF-395/ACR-872", "BioFace", "Bio-2/10/Bio201/Bio-203/Bio-204/Bio-909", "Bio-8", "Bio-9", "Bio-868", "Bio-868A", "Honeywell-WinPakPro", "ZK-GPRS", "ZK-4 Door Controller", "Porys 4 Door Controller", "TimeWatch Access Control"})
        Me.RepositoryItemComboBox4.Name = "RepositoryItemComboBox4"
        '
        'RepositoryItemComboBox2
        '
        Me.RepositoryItemComboBox2.AutoHeight = False
        Me.RepositoryItemComboBox2.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemComboBox2.Items.AddRange(New Object() {"TCP/IP", "USB"})
        Me.RepositoryItemComboBox2.MaxLength = 10
        Me.RepositoryItemComboBox2.Name = "RepositoryItemComboBox2"
        '
        'RepositoryItemComboBox3
        '
        Me.RepositoryItemComboBox3.AutoHeight = False
        Me.RepositoryItemComboBox3.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemComboBox3.Items.AddRange(New Object() {"IN", "OUT"})
        Me.RepositoryItemComboBox3.MaxLength = 10
        Me.RepositoryItemComboBox3.Name = "RepositoryItemComboBox3"
        Me.RepositoryItemComboBox3.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        '
        'RepositoryItemTextEdit7
        '
        Me.RepositoryItemTextEdit7.AutoHeight = False
        Me.RepositoryItemTextEdit7.MaxLength = 15
        Me.RepositoryItemTextEdit7.Name = "RepositoryItemTextEdit7"
        '
        'RepositoryItemTextEdit8
        '
        Me.RepositoryItemTextEdit8.AutoHeight = False
        Me.RepositoryItemTextEdit8.MaxLength = 50
        Me.RepositoryItemTextEdit8.Name = "RepositoryItemTextEdit8"
        '
        'RepositoryItemTextEdit9
        '
        Me.RepositoryItemTextEdit9.AutoHeight = False
        Me.RepositoryItemTextEdit9.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.RepositoryItemTextEdit9.Mask.EditMask = "##########"
        Me.RepositoryItemTextEdit9.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.RepositoryItemTextEdit9.MaxLength = 10
        Me.RepositoryItemTextEdit9.Name = "RepositoryItemTextEdit9"
        '
        'RepositoryItemTextEdit10
        '
        Me.RepositoryItemTextEdit10.AutoHeight = False
        Me.RepositoryItemTextEdit10.MaxLength = 50
        Me.RepositoryItemTextEdit10.Name = "RepositoryItemTextEdit10"
        '
        'RepositoryItemTextEdit6
        '
        Me.RepositoryItemTextEdit6.AutoHeight = False
        Me.RepositoryItemTextEdit6.Mask.EditMask = "[0-9]*"
        Me.RepositoryItemTextEdit6.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.RepositoryItemTextEdit6.MaxLength = 3
        Me.RepositoryItemTextEdit6.Name = "RepositoryItemTextEdit6"
        '
        'RepositoryItemComboBox5
        '
        Me.RepositoryItemComboBox5.AutoHeight = False
        Me.RepositoryItemComboBox5.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemComboBox5.Name = "RepositoryItemComboBox5"
        '
        'RepositoryItemLookUpEdit2
        '
        Me.RepositoryItemLookUpEdit2.AutoHeight = False
        Me.RepositoryItemLookUpEdit2.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemLookUpEdit2.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("BRANCHNAME", "BRANCHNAME", 79, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near)})
        Me.RepositoryItemLookUpEdit2.DataSource = Me.TblbranchBindingSource
        Me.RepositoryItemLookUpEdit2.DisplayMember = "BRANCHNAME"
        Me.RepositoryItemLookUpEdit2.Name = "RepositoryItemLookUpEdit2"
        Me.RepositoryItemLookUpEdit2.NullText = ""
        Me.RepositoryItemLookUpEdit2.ValueMember = "BRANCHNAME"
        '
        'SidePanelTitle
        '
        Me.SidePanelTitle.Controls.Add(Me.LabelControl1)
        Me.SidePanelTitle.Dock = System.Windows.Forms.DockStyle.Top
        Me.SidePanelTitle.Location = New System.Drawing.Point(0, 1)
        Me.SidePanelTitle.Name = "SidePanelTitle"
        Me.SidePanelTitle.Size = New System.Drawing.Size(1050, 23)
        Me.SidePanelTitle.TabIndex = 0
        Me.SidePanelTitle.Text = "SidePanel1"
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl1.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Appearance.Options.UseForeColor = True
        Me.LabelControl1.Location = New System.Drawing.Point(4, 5)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(158, 14)
        Me.LabelControl1.TabIndex = 0
        Me.LabelControl1.Text = "Available Devices In Network"
        '
        'MemoEdit1
        '
        Me.MemoEdit1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.MemoEdit1.EditValue = resources.GetString("MemoEdit1.EditValue")
        Me.MemoEdit1.Location = New System.Drawing.Point(0, 0)
        Me.MemoEdit1.Name = "MemoEdit1"
        Me.MemoEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.MemoEdit1.Properties.Appearance.Options.UseFont = True
        Me.MemoEdit1.Properties.MaxLength = 100000
        Me.MemoEdit1.Properties.ReadOnly = True
        Me.MemoEdit1.Size = New System.Drawing.Size(160, 462)
        Me.MemoEdit1.TabIndex = 1
        '
        'BarManager1
        '
        Me.BarManager1.DockControls.Add(Me.barDockControlTop)
        Me.BarManager1.DockControls.Add(Me.barDockControlBottom)
        Me.BarManager1.DockControls.Add(Me.barDockControlLeft)
        Me.BarManager1.DockControls.Add(Me.barDockControlRight)
        Me.BarManager1.Form = Me
        Me.BarManager1.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.BarButtonItem1, Me.BarButtonItem2})
        Me.BarManager1.MaxItemId = 2
        '
        'barDockControlTop
        '
        Me.barDockControlTop.CausesValidation = False
        Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.barDockControlTop.Location = New System.Drawing.Point(0, 31)
        Me.barDockControlTop.Manager = Me.BarManager1
        Me.barDockControlTop.Size = New System.Drawing.Size(1215, 0)
        '
        'barDockControlBottom
        '
        Me.barDockControlBottom.CausesValidation = False
        Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.barDockControlBottom.Location = New System.Drawing.Point(0, 493)
        Me.barDockControlBottom.Manager = Me.BarManager1
        Me.barDockControlBottom.Size = New System.Drawing.Size(1215, 0)
        '
        'barDockControlLeft
        '
        Me.barDockControlLeft.CausesValidation = False
        Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
        Me.barDockControlLeft.Location = New System.Drawing.Point(0, 31)
        Me.barDockControlLeft.Manager = Me.BarManager1
        Me.barDockControlLeft.Size = New System.Drawing.Size(0, 462)
        '
        'barDockControlRight
        '
        Me.barDockControlRight.CausesValidation = False
        Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
        Me.barDockControlRight.Location = New System.Drawing.Point(1215, 31)
        Me.barDockControlRight.Manager = Me.BarManager1
        Me.barDockControlRight.Size = New System.Drawing.Size(0, 462)
        '
        'BarButtonItem1
        '
        Me.BarButtonItem1.Caption = "Get Logs"
        Me.BarButtonItem1.Id = 0
        Me.BarButtonItem1.ItemAppearance.Hovered.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.BarButtonItem1.ItemAppearance.Hovered.Options.UseFont = True
        Me.BarButtonItem1.ItemAppearance.Normal.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.BarButtonItem1.ItemAppearance.Normal.Options.UseFont = True
        Me.BarButtonItem1.ItemAppearance.Pressed.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.BarButtonItem1.ItemAppearance.Pressed.Options.UseFont = True
        Me.BarButtonItem1.ItemInMenuAppearance.Hovered.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.BarButtonItem1.ItemInMenuAppearance.Hovered.Options.UseFont = True
        Me.BarButtonItem1.Name = "BarButtonItem1"
        '
        'BarButtonItem2
        '
        Me.BarButtonItem2.Caption = "BarButtonItem2"
        Me.BarButtonItem2.Id = 1
        Me.BarButtonItem2.Name = "BarButtonItem2"
        '
        'BarManager2
        '
        Me.BarManager2.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar1})
        Me.BarManager2.DockControls.Add(Me.BarDockControl1)
        Me.BarManager2.DockControls.Add(Me.BarDockControl2)
        Me.BarManager2.DockControls.Add(Me.BarDockControl3)
        Me.BarManager2.DockControls.Add(Me.BarDockControl4)
        Me.BarManager2.Form = Me
        Me.BarManager2.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.BarButtonItem3, Me.BarButtonItem4, Me.BarButtonNewLogs, Me.BarButtonOldLogs, Me.BarButtonDeviceStatus, Me.BarButtonItem6, Me.BarButtonItem7})
        Me.BarManager2.MainMenu = Me.Bar1
        Me.BarManager2.MaxItemId = 8
        '
        'Bar1
        '
        Me.Bar1.BarAppearance.Hovered.Font = New System.Drawing.Font("Tahoma", 15.0!)
        Me.Bar1.BarAppearance.Hovered.ForeColor = System.Drawing.Color.SteelBlue
        Me.Bar1.BarAppearance.Hovered.Options.UseFont = True
        Me.Bar1.BarAppearance.Hovered.Options.UseForeColor = True
        Me.Bar1.BarAppearance.Normal.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.Bar1.BarAppearance.Normal.ForeColor = System.Drawing.Color.SteelBlue
        Me.Bar1.BarAppearance.Normal.Options.UseFont = True
        Me.Bar1.BarAppearance.Normal.Options.UseForeColor = True
        Me.Bar1.BarAppearance.Pressed.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.Bar1.BarAppearance.Pressed.ForeColor = System.Drawing.Color.SteelBlue
        Me.Bar1.BarAppearance.Pressed.Options.UseFont = True
        Me.Bar1.BarAppearance.Pressed.Options.UseForeColor = True
        Me.Bar1.BarName = "Main menu"
        Me.Bar1.DockCol = 0
        Me.Bar1.DockRow = 0
        Me.Bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.Bar1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem3), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem4), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonNewLogs), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonOldLogs), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonDeviceStatus), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem6), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem7)})
        Me.Bar1.OptionsBar.AllowQuickCustomization = False
        Me.Bar1.OptionsBar.DrawDragBorder = False
        Me.Bar1.OptionsBar.MultiLine = True
        Me.Bar1.OptionsBar.UseWholeRow = True
        Me.Bar1.Text = "Main menu"
        '
        'BarButtonItem3
        '
        Me.BarButtonItem3.Caption = "Logs Management"
        Me.BarButtonItem3.Id = 0
        Me.BarButtonItem3.ItemAppearance.Normal.Font = New System.Drawing.Font("Tahoma", 15.0!)
        Me.BarButtonItem3.ItemAppearance.Normal.Options.UseFont = True
        Me.BarButtonItem3.Name = "BarButtonItem3"
        Me.BarButtonItem3.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        '
        'BarButtonItem4
        '
        Me.BarButtonItem4.Caption = "Finger Print Setup"
        Me.BarButtonItem4.Id = 1
        Me.BarButtonItem4.ItemAppearance.Normal.Font = New System.Drawing.Font("Tahoma", 15.0!)
        Me.BarButtonItem4.ItemAppearance.Normal.Options.UseFont = True
        Me.BarButtonItem4.Name = "BarButtonItem4"
        Me.BarButtonItem4.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        '
        'BarButtonNewLogs
        '
        Me.BarButtonNewLogs.Caption = "Get New Logs"
        Me.BarButtonNewLogs.Id = 3
        Me.BarButtonNewLogs.ItemAppearance.Normal.Font = New System.Drawing.Font("Tahoma", 15.0!)
        Me.BarButtonNewLogs.ItemAppearance.Normal.Options.UseFont = True
        Me.BarButtonNewLogs.Name = "BarButtonNewLogs"
        Me.BarButtonNewLogs.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        '
        'BarButtonOldLogs
        '
        Me.BarButtonOldLogs.Caption = "Get All Logs"
        Me.BarButtonOldLogs.Id = 4
        Me.BarButtonOldLogs.ItemAppearance.Normal.Font = New System.Drawing.Font("Tahoma", 15.0!)
        Me.BarButtonOldLogs.ItemAppearance.Normal.Options.UseFont = True
        Me.BarButtonOldLogs.Name = "BarButtonOldLogs"
        Me.BarButtonOldLogs.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        '
        'BarButtonDeviceStatus
        '
        Me.BarButtonDeviceStatus.Caption = "Device Status"
        Me.BarButtonDeviceStatus.Id = 5
        Me.BarButtonDeviceStatus.ImageOptions.Image = CType(resources.GetObject("BarButtonDeviceStatus.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonDeviceStatus.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonDeviceStatus.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonDeviceStatus.Name = "BarButtonDeviceStatus"
        Me.BarButtonDeviceStatus.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'BarButtonItem6
        '
        Me.BarButtonItem6.Caption = "Live Video"
        Me.BarButtonItem6.Id = 6
        Me.BarButtonItem6.ImageOptions.Image = CType(resources.GetObject("BarButtonItem6.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem6.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItem6.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItem6.Name = "BarButtonItem6"
        Me.BarButtonItem6.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'BarButtonItem7
        '
        Me.BarButtonItem7.Caption = "Search Devices"
        Me.BarButtonItem7.Id = 7
        Me.BarButtonItem7.ImageOptions.Image = CType(resources.GetObject("BarButtonItem7.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem7.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItem7.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItem7.Name = "BarButtonItem7"
        Me.BarButtonItem7.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'BarDockControl1
        '
        Me.BarDockControl1.CausesValidation = False
        Me.BarDockControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.BarDockControl1.Location = New System.Drawing.Point(0, 0)
        Me.BarDockControl1.Manager = Me.BarManager2
        Me.BarDockControl1.Size = New System.Drawing.Size(1215, 31)
        '
        'BarDockControl2
        '
        Me.BarDockControl2.CausesValidation = False
        Me.BarDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.BarDockControl2.Location = New System.Drawing.Point(0, 493)
        Me.BarDockControl2.Manager = Me.BarManager2
        Me.BarDockControl2.Size = New System.Drawing.Size(1215, 0)
        '
        'BarDockControl3
        '
        Me.BarDockControl3.CausesValidation = False
        Me.BarDockControl3.Dock = System.Windows.Forms.DockStyle.Left
        Me.BarDockControl3.Location = New System.Drawing.Point(0, 31)
        Me.BarDockControl3.Manager = Me.BarManager2
        Me.BarDockControl3.Size = New System.Drawing.Size(0, 462)
        '
        'BarDockControl4
        '
        Me.BarDockControl4.CausesValidation = False
        Me.BarDockControl4.Dock = System.Windows.Forms.DockStyle.Right
        Me.BarDockControl4.Location = New System.Drawing.Point(1215, 31)
        Me.BarDockControl4.Manager = Me.BarManager2
        Me.BarDockControl4.Size = New System.Drawing.Size(0, 462)
        '
        'TblbranchTableAdapter
        '
        Me.TblbranchTableAdapter.ClearBeforeFill = True
        '
        'Tblbranch1TableAdapter1
        '
        Me.Tblbranch1TableAdapter1.ClearBeforeFill = True
        '
        'XtraDevice
        '
        Me.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.Appearance.Options.UseBackColor = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.SplitContainerControl1)
        Me.Controls.Add(Me.barDockControlLeft)
        Me.Controls.Add(Me.barDockControlRight)
        Me.Controls.Add(Me.barDockControlBottom)
        Me.Controls.Add(Me.barDockControlTop)
        Me.Controls.Add(Me.BarDockControl3)
        Me.Controls.Add(Me.BarDockControl4)
        Me.Controls.Add(Me.BarDockControl2)
        Me.Controls.Add(Me.BarDockControl1)
        Me.LookAndFeel.SkinMaskColor = System.Drawing.Color.Red
        Me.LookAndFeel.SkinMaskColor2 = System.Drawing.Color.Red
        Me.LookAndFeel.SkinName = "Black"
        Me.Name = "XtraDevice"
        Me.Size = New System.Drawing.Size(1215, 493)
        CType(Me.TblMachineBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BehaviorManager1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerControl1.ResumeLayout(False)
        Me.SidePanelDevice.ResumeLayout(False)
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTextEdit5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProtocolComboBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TypeComboBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DeviceListComboBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTextEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTextEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTextEdit3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTextEdit4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemComboBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemLookUpEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblbranchBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SidePanelSearch.ResumeLayout(False)
        CType(Me.GridControlSearch, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewSearch, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemComboBox4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemComboBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemComboBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTextEdit7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTextEdit8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTextEdit9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTextEdit10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTextEdit6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemComboBox5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemLookUpEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SidePanelTitle.ResumeLayout(False)
        Me.SidePanelTitle.PerformLayout()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BarManager1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BarManager2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TblMachineBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SSSDBDataSet As iAS.SSSDBDataSet
    Friend WithEvents TblMachineTableAdapter As iAS.SSSDBDataSetTableAdapters.tblMachineTableAdapter
    Friend WithEvents BehaviorManager1 As DevExpress.Utils.Behaviors.BehaviorManager
    Friend WithEvents TblMachine1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.tblMachine1TableAdapter
    Friend WithEvents SplitContainerControl1 As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colID_NO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemTextEdit5 As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents colA_R As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents ProtocolComboBox1 As DevExpress.XtraEditors.Repository.RepositoryItemComboBox
    Friend WithEvents colIN_OUT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents TypeComboBox1 As DevExpress.XtraEditors.Repository.RepositoryItemComboBox
    Friend WithEvents colDeviceType As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents DeviceListComboBox1 As DevExpress.XtraEditors.Repository.RepositoryItemComboBox
    Friend WithEvents colLOCATION As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemTextEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents colbranch As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemTextEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents colcommkey As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemTextEdit3 As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents colMAC_ADDRESS As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemTextEdit4 As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents colLastModifiedBy As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLastModifiedDate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents MemoEdit1 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents BarManager1 As DevExpress.XtraBars.BarManager
    Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
    Friend WithEvents BarDockControl3 As DevExpress.XtraBars.BarDockControl
    Friend WithEvents BarManager2 As DevExpress.XtraBars.BarManager
    Friend WithEvents Bar1 As DevExpress.XtraBars.Bar
    Friend WithEvents BarButtonItem3 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem4 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarDockControl1 As DevExpress.XtraBars.BarDockControl
    Friend WithEvents BarDockControl2 As DevExpress.XtraBars.BarDockControl
    Friend WithEvents BarDockControl4 As DevExpress.XtraBars.BarDockControl
    Friend WithEvents BarButtonItem1 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem2 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents RepositoryItemLookUpEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents TblbranchBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents RepositoryItemComboBox1 As DevExpress.XtraEditors.Repository.RepositoryItemComboBox
    Friend WithEvents TblbranchTableAdapter As iAS.SSSDBDataSetTableAdapters.tblbranchTableAdapter
    Friend WithEvents Tblbranch1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.tblbranch1TableAdapter
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents BarButtonNewLogs As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonOldLogs As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonDeviceStatus As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents Status As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colHLogin As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colHPassword As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents BarButtonItem6 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents colUserCount As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colFaceCount As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents SidePanelDevice As DevExpress.XtraEditors.SidePanel
    Friend WithEvents SidePanelSearch As DevExpress.XtraEditors.SidePanel
    Friend WithEvents SidePanelTitle As DevExpress.XtraEditors.SidePanel
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GridControlSearch As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridViewSearch As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents RepositoryItemComboBox4 As DevExpress.XtraEditors.Repository.RepositoryItemComboBox
    Friend WithEvents RepositoryItemComboBox2 As DevExpress.XtraEditors.Repository.RepositoryItemComboBox
    Friend WithEvents RepositoryItemComboBox3 As DevExpress.XtraEditors.Repository.RepositoryItemComboBox
    Friend WithEvents RepositoryItemTextEdit7 As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents RepositoryItemTextEdit8 As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents RepositoryItemTextEdit9 As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents RepositoryItemTextEdit10 As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents RepositoryItemTextEdit6 As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents RepositoryItemComboBox5 As DevExpress.XtraEditors.Repository.RepositoryItemComboBox
    Friend WithEvents RepositoryItemLookUpEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemLookUpEdit
    Friend WithEvents BarButtonItem7 As DevExpress.XtraBars.BarButtonItem
End Class
