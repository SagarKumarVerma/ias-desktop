﻿Imports System.Resources
Imports System.Globalization
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports System.ComponentModel
Imports DevExpress.XtraGrid.Columns
Imports System.Data.SqlClient
Imports System.Text
Imports System.Data.OleDb
Public Class XtraUserMgmtEdit
    Dim UsrId As String
    Dim ulf As UserLookAndFeel
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    Public Sub New()
        InitializeComponent()
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True
    End Sub
    Private Sub XtraUserMgmtEdit_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        UsrId = XtraUserMgmt.UserID
        XtraTabControl1.SelectedTabPage = XtraTabPrevileges

        'If Common.servername = "Access" Then
        '    Me.TblCompany1TableAdapter1.Fill(Me.SSSDBDataSet.tblCompany1)
        '    GridControlComp.DataSource = SSSDBDataSet.tblCompany1

        '    Me.Tblbranch1TableAdapter1.Fill(Me.SSSDBDataSet.tblbranch1)
        '    GridControlBranch.DataSource = SSSDBDataSet.tblbranch1
        'Else
        '    TblCompanyTableAdapter.Connection.ConnectionString = Common.ConnectionString
        '    Me.TblCompanyTableAdapter.Fill(Me.SSSDBDataSet.tblCompany)
        '    GridControlComp.DataSource = SSSDBDataSet.tblCompany

        '    TblbranchTableAdapter.Connection.ConnectionString = Common.ConnectionString
        '    Me.TblbranchTableAdapter.Fill(Me.SSSDBDataSet.tblbranch)
        '    GridControlBranch.DataSource = SSSDBDataSet.tblbranch
        'End If
        GridControlComp.DataSource = Common.CompanyNonAdmin
        GridControlBranch.DataSource = Common.LocationNonAdmin

        CheckEditMarlAll.Checked = False
        CheckEditMarlAll.Text = "Mark All"
        If UsrId.Length = 0 Then
            SetDefaultValue()
        Else
            SetFormValue()
        End If
        If Common.IsComplianceFromLic = True Then
            ToggleCompl.Visible = True
            LabelControl19.Visible = True
        Else
            ToggleCompl.Visible = False
            LabelControl19.Visible = False
            ToggleCompl.IsOn = False
        End If
    End Sub
    Private Sub SetDefaultValue()
        TextEditUsr.Enabled = True
        TextEditUsr.Text = ""
        TextEditPwd.Text = ""
        TextEditDes.Text = ""
        ToggleSwitchAdmin.IsOn = True
        ToggleSwitchMain.IsOn = False
        ToggleSwitchTransaction.IsOn = False
        ToggleSwitchPay.IsOn = False
        ToggleSwitchReports.IsOn = False
        ToggleSwitchAutoProcess.IsOn = False
        ToggleSwitchDataPtrocess.IsOn = False
        ToggleSwitchLeaveMgmt.IsOn = False
        PopupContainerEditLocation.EditValue = ""
        PopupContainerEditComp.EditValue = ""
        ToggleSwitchCanteen.IsOn = False
        ToggleSwitchVisitor.IsOn = False
        ToggleCompl.IsOn = False

        CheckEditComp.EditValue = "N"
        CheckEditDept.EditValue = "N"
        CheckEditSec.EditValue = "N"
        CheckEditGrade.EditValue = "N"
        CheckEditCat.EditValue = "N"
        CheckEditShift.EditValue = "N"
        CheckEditEmp.EditValue = "N"
        CheckEditPunchEntry.EditValue = "N"
        CheckEditOverStayToOT.EditValue = "N"
        CheckEditShiftShange.EditValue = "N"
        CheckEditHoliday.EditValue = "N"
        CheckEditLeaveMaster.EditValue = "N"
        CheckEditLeaveApp.EditValue = "N"
        CheckEditLeaveAcc.EditValue = "N"
        CheckEditCommonSetting.EditValue = "N"
        CheckEditUserPre.EditValue = "N"
        CheckEditVerification.EditValue = "N"
        CheckEditEmp.EditValue = "N"
        CheckEditTimeReport.EditValue = "N"
        CheckEditPayReport.EditValue = "N"
        CheckEditAttResCre.EditValue = "N"
        CheckEditAttResUpd.EditValue = "N"
        CheckEditBckDate.EditValue = "N"
        CheckEditCompAdd.EditValue = "N"
        CheckEditCompEdit.EditValue = "N"
        CheckEditCompDelete.EditValue = "N"
        CheckEditDeptAdd.EditValue = "N"
        CheckEditDeptEdit.EditValue = "N"
        CheckEditDeptDelete.EditValue = "N"
        CheckEditCatAdd.EditValue = "N"
        CheckEditCatEdit.EditValue = "N"
        CheckEditCatDelete.EditValue = "N"
        CheckEditSecAdd.EditValue = "N"
        CheckEditSecEdit.EditValue = "N"
        CheckEditSecDelete.EditValue = "N"
        CheckEditGrdAdd.EditValue = "N"
        CheckEditGrdEdit.EditValue = "N"
        CheckEditGrdDelete.EditValue = "N"
        CheckEditShiftAdd.EditValue = "N"
        CheckEditShiftEdit.EditValue = "N"
        CheckEditShiftDelete.EditValue = "N"
        CheckEditEmpAdd.EditValue = "N"
        CheckEditEmpEdit.EditValue = "N"
        CheckEditEmpDelete.EditValue = "N"
        CheckEditDataMaintenance.EditValue = "N"


        ToggleSwitchDevice.EditValue = "N"
        CheckEditDAdd.EditValue = "N"
        CheckEditDEdit.EditValue = "N"
        CheckEditDDelete.EditValue = "N"
        CheckEditLogMgmt.EditValue = "N"
        CheckEditUserSetup.EditValue = "N"
        CheckEditDBSetting.EditValue = "N"
        CheckEditSMS.EditValue = "N"
        CheckEditBulkSMS.EditValue = "N"
        CheckEditEmail.EditValue = "N"
        CheckEditBackup.EditValue = "N"
        CheckEditParallel.EditValue = "N"
        CheckEditBranch.EditValue = "N"
        CheckEditBranchAdd.EditValue = "N"
        CheckEditBranchEdit.EditValue = "N"
        CheckEditBranchDel.EditValue = "N"
        CheckEditMonthlyR.EditValue = "N"
        CheckEditDailyR.EditValue = "N"
        CheckEditLeaveR.EditValue = "N"
        CheckEditMleaveInc.EditValue = "N"
        visibiltyChange()
    End Sub
    Private Sub SetFormValue()
        TextEditUsr.Enabled = False

        Dim adapS As SqlDataAdapter
        Dim adapAc As OleDbDataAdapter
        Dim Rs As DataSet = New DataSet
        Dim sSql As String = " Select * from tblUser where USER_R = '" & UsrId & "'"
        If Common.servername = "Access" Then
            adapAc = New OleDbDataAdapter(sSql, Common.con1)
            adapAc.Fill(Rs)
        Else
            adapS = New SqlDataAdapter(sSql, Common.con)
            adapS.Fill(Rs)
        End If

        CheckEditComp.EditValue = Rs.Tables(0).Rows(0).Item("Company").ToString.Trim
        CheckEditDept.EditValue = Rs.Tables(0).Rows(0).Item("Department").ToString.Trim
        CheckEditSec.EditValue = Rs.Tables(0).Rows(0).Item("Section").ToString.Trim
        CheckEditGrade.EditValue = Rs.Tables(0).Rows(0).Item("Grade").ToString.Trim
        CheckEditCat.EditValue = Rs.Tables(0).Rows(0).Item("Category").ToString.Trim
        CheckEditShift.EditValue = Rs.Tables(0).Rows(0).Item("Shift").ToString.Trim
        CheckEditEmp.EditValue = Rs.Tables(0).Rows(0).Item("Employee").ToString.Trim
        CheckEditPunchEntry.EditValue = Rs.Tables(0).Rows(0).Item("Manual_Attendance").ToString.Trim
        CheckEditOverStayToOT.EditValue = Rs.Tables(0).Rows(0).Item("OstoOt").ToString.Trim
        CheckEditShiftShange.EditValue = Rs.Tables(0).Rows(0).Item("ShiftChange").ToString.Trim
        CheckEditHoliday.EditValue = Rs.Tables(0).Rows(0).Item("HoliDay").ToString.Trim
        CheckEditLeaveMaster.EditValue = Rs.Tables(0).Rows(0).Item("LeaveMaster").ToString.Trim
        CheckEditLeaveApp.EditValue = Rs.Tables(0).Rows(0).Item("LeaveApplication").ToString.Trim
        CheckEditLeaveAcc.EditValue = Rs.Tables(0).Rows(0).Item("LeaveAccural").ToString.Trim
        CheckEditCommonSetting.EditValue = Rs.Tables(0).Rows(0).Item("TimeOfficeSetup").ToString.Trim
        CheckEditUserPre.EditValue = Rs.Tables(0).Rows(0).Item("UserPrevilege").ToString.Trim
        CheckEditVerification.EditValue = Rs.Tables(0).Rows(0).Item("Verification").ToString.Trim
        CheckEditEmp.EditValue = Rs.Tables(0).Rows(0).Item("EmployeeSetup").ToString.Trim
        CheckEditTimeReport.EditValue = Rs.Tables(0).Rows(0).Item("TimeOfficeReport").ToString.Trim
        CheckEditPayReport.EditValue = Rs.Tables(0).Rows(0).Item("PayrollReport").ToString.Trim
        CheckEditAttResCre.EditValue = Rs.Tables(0).Rows(0).Item("RegisterCreation").ToString.Trim
        CheckEditAttResUpd.EditValue = Rs.Tables(0).Rows(0).Item("RegisterUpdation").ToString.Trim
        CheckEditBckDate.EditValue = Rs.Tables(0).Rows(0).Item("BackDateProcess").ToString.Trim
        CheckEditCompAdd.EditValue = Rs.Tables(0).Rows(0).Item("CompAdd").ToString.Trim
        CheckEditCompEdit.EditValue = Rs.Tables(0).Rows(0).Item("CompModi").ToString.Trim
        CheckEditCompDelete.EditValue = Rs.Tables(0).Rows(0).Item("CompDel").ToString.Trim
        CheckEditDeptAdd.EditValue = Rs.Tables(0).Rows(0).Item("DeptAdd").ToString.Trim
        CheckEditDeptEdit.EditValue = Rs.Tables(0).Rows(0).Item("DeptModi").ToString.Trim
        CheckEditDeptDelete.EditValue = Rs.Tables(0).Rows(0).Item("DeptDel").ToString.Trim
        CheckEditCatAdd.EditValue = Rs.Tables(0).Rows(0).Item("CatAdd").ToString.Trim
        CheckEditCatEdit.EditValue = Rs.Tables(0).Rows(0).Item("CatModi").ToString.Trim
        CheckEditCatDelete.EditValue = Rs.Tables(0).Rows(0).Item("CatDel").ToString.Trim
        CheckEditSecAdd.EditValue = Rs.Tables(0).Rows(0).Item("SecAdd").ToString.Trim
        CheckEditSecEdit.EditValue = Rs.Tables(0).Rows(0).Item("SecModi").ToString.Trim
        CheckEditSecDelete.EditValue = Rs.Tables(0).Rows(0).Item("SecDel").ToString.Trim
        CheckEditGrdAdd.EditValue = Rs.Tables(0).Rows(0).Item("GrdAdd").ToString.Trim
        CheckEditGrdEdit.EditValue = Rs.Tables(0).Rows(0).Item("GrdModi").ToString.Trim
        CheckEditGrdDelete.EditValue = Rs.Tables(0).Rows(0).Item("GrdDel").ToString.Trim
        CheckEditShiftAdd.EditValue = Rs.Tables(0).Rows(0).Item("SftAdd").ToString.Trim
        CheckEditShiftEdit.EditValue = Rs.Tables(0).Rows(0).Item("SftModi").ToString.Trim
        CheckEditShiftDelete.EditValue = Rs.Tables(0).Rows(0).Item("SftDel").ToString.Trim
        CheckEditEmpAdd.EditValue = Rs.Tables(0).Rows(0).Item("EmpAdd").ToString.Trim
        CheckEditEmpEdit.EditValue = Rs.Tables(0).Rows(0).Item("EmpModi").ToString.Trim
        CheckEditEmpDelete.EditValue = Rs.Tables(0).Rows(0).Item("EmpDel").ToString.Trim
        CheckEditDataMaintenance.EditValue = Rs.Tables(0).Rows(0).Item("DataMaintenance").ToString.Trim

        PopupContainerEditLocation.EditValue = Rs.Tables(0).Rows(0).Item("Auth_Branch").ToString.Trim
        PopupContainerEditComp.EditValue = Rs.Tables(0).Rows(0).Item("auth_comp").ToString.Trim

        TextEditUsr.Text = Rs.Tables(0).Rows(0).Item("USER_R").ToString.Trim
        TextEditDes.Text = Rs.Tables(0).Rows(0).Item("USERDESCRIPRION").ToString.Trim
        TextEditPwd.Text = Rs.Tables(0).Rows(0).Item("PASSWORD").ToString.Trim
        If Rs.Tables(0).Rows(0).Item("USERTYPE").ToString.Trim = "H" Then
            ComboBoxEdit2.SelectedIndex = 1
            PopupContainerEditLocation.Visible = True
            PopupContainerEditComp.Visible = True
        Else
            ComboBoxEdit2.SelectedIndex = 0
            PopupContainerEditLocation.Visible = False
            PopupContainerEditComp.Visible = False
        End If

        If Rs.Tables(0).Rows(0).Item("AutoProcess").ToString.Trim = "Y" Then
            ToggleSwitchAutoProcess.IsOn = True
        Else
            ToggleSwitchAutoProcess.IsOn = False
        End If

        If Rs.Tables(0).Rows(0).Item("DataProcess").ToString.Trim = "Y" Then
            ToggleSwitchDataPtrocess.IsOn = True
        Else
            ToggleSwitchDataPtrocess.IsOn = False
        End If

        If Rs.Tables(0).Rows(0).Item("Main").ToString.Trim = "Y" Then
            ToggleSwitchMain.IsOn = True
        Else
            ToggleSwitchMain.IsOn = False
        End If
        If Rs.Tables(0).Rows(0).Item("V_Transaction").ToString.Trim = "Y" Then
            ToggleSwitchTransaction.IsOn = True
        Else
            ToggleSwitchTransaction.IsOn = False
        End If
        If Rs.Tables(0).Rows(0).Item("Admin").ToString.Trim = "Y" Then
            ToggleSwitchAdmin.IsOn = True
        Else
            ToggleSwitchAdmin.IsOn = False
        End If
        If Rs.Tables(0).Rows(0).Item("Payroll").ToString.Trim = "Y" Then
            ToggleSwitchPay.IsOn = True
        Else
            ToggleSwitchPay.IsOn = False
        End If
        If Rs.Tables(0).Rows(0).Item("Reports").ToString.Trim = "Y" Then
            ToggleSwitchReports.IsOn = True
        Else
            ToggleSwitchReports.IsOn = False
        End If
        If Rs.Tables(0).Rows(0).Item("Leave").ToString.Trim = "Y" Then
            ToggleSwitchLeaveMgmt.IsOn = True
        Else
            ToggleSwitchLeaveMgmt.IsOn = False
        End If

        If Rs.Tables(0).Rows(0).Item("canteen").ToString.Trim = "Y" Then
            ToggleSwitchCanteen.IsOn = True
        Else
            ToggleSwitchCanteen.IsOn = False
        End If

        If Rs.Tables(0).Rows(0).Item("Visitor").ToString.Trim = "Y" Then
            ToggleSwitchVisitor.IsOn = True
        Else
            ToggleSwitchVisitor.IsOn = False
        End If

        If Rs.Tables(0).Rows(0).Item("IsCompliance").ToString.Trim = "Y" Then
            ToggleCompl.IsOn = True
        Else
            ToggleCompl.IsOn = False
        End If

        ToggleSwitchDevice.EditValue = Rs.Tables(0).Rows(0).Item("DeviceMgmt").ToString.Trim
        CheckEditDAdd.EditValue = Rs.Tables(0).Rows(0).Item("DeviceAdd").ToString.Trim
        CheckEditDEdit.EditValue = Rs.Tables(0).Rows(0).Item("DeviceModi").ToString.Trim
        CheckEditDDelete.EditValue = Rs.Tables(0).Rows(0).Item("DeviceDelete").ToString.Trim
        CheckEditLogMgmt.EditValue = Rs.Tables(0).Rows(0).Item("LogMgmt").ToString.Trim
        CheckEditUserSetup.EditValue = Rs.Tables(0).Rows(0).Item("UserSetupTemplate").ToString.Trim
        CheckEditDBSetting.EditValue = Rs.Tables(0).Rows(0).Item("DBSetting").ToString.Trim
        CheckEditSMS.EditValue = Rs.Tables(0).Rows(0).Item("SMSSetting").ToString.Trim
        CheckEditBulkSMS.EditValue = Rs.Tables(0).Rows(0).Item("BulkSMS").ToString.Trim
        CheckEditEmail.EditValue = Rs.Tables(0).Rows(0).Item("EmailSetting").ToString.Trim
        CheckEditBackup.EditValue = Rs.Tables(0).Rows(0).Item("BackUpSetting").ToString.Trim
        CheckEditParallel.EditValue = Rs.Tables(0).Rows(0).Item("ParallelSetting").ToString.Trim
        CheckEditBranch.EditValue = Rs.Tables(0).Rows(0).Item("Branch").ToString.Trim
        CheckEditBranchAdd.EditValue = Rs.Tables(0).Rows(0).Item("BranchAdd").ToString.Trim
        CheckEditBranchEdit.EditValue = Rs.Tables(0).Rows(0).Item("BranchModi").ToString.Trim
        CheckEditBranchDel.EditValue = Rs.Tables(0).Rows(0).Item("BranchDel").ToString.Trim
        CheckEditMonthlyR.EditValue = Rs.Tables(0).Rows(0).Item("MonthlyReport").ToString.Trim
        CheckEditDailyR.EditValue = Rs.Tables(0).Rows(0).Item("DalyReport").ToString.Trim
        CheckEditLeaveR.EditValue = Rs.Tables(0).Rows(0).Item("LeaveReport").ToString.Trim
        CheckEditMleaveInc.EditValue = Rs.Tables(0).Rows(0).Item("LeaveAccuralAuto").ToString.Trim
        visibiltyChange()
    End Sub
    Private Sub SimpleButton2_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton2.Click
        Me.Close()
    End Sub
    Private Sub visibiltyChange()
        If ToggleSwitchMain.IsOn = True Then
            XtraTabMaster.PageEnabled = True
        Else
            XtraTabMaster.PageEnabled = False
        End If
        If ToggleSwitchTransaction.IsOn = True Then
            XtraTabTransaction.PageEnabled = True
        Else
            XtraTabTransaction.PageEnabled = False
        End If
        If ToggleSwitchLeaveMgmt.IsOn = True Then
            XtraTabLeave.PageEnabled = True
        Else
            XtraTabLeave.PageEnabled = False
        End If
        If ToggleSwitchPay.IsOn = True Then
            XtraTabPayroll.PageEnabled = True
        Else
            XtraTabPayroll.PageEnabled = False
        End If
        If ToggleSwitchAdmin.IsOn = True Then
            XtraTabAdmin.PageEnabled = True
        Else
            XtraTabAdmin.PageEnabled = False
        End If
        If ToggleSwitchDataPtrocess.IsOn = True Then
            XtraTabDataProcess.PageEnabled = True
        Else
            XtraTabDataProcess.PageEnabled = False
        End If
        If ToggleSwitchReports.IsOn = True Then
            XtraTabReports.PageEnabled = True
        Else
            XtraTabReports.PageEnabled = False
        End If
        If ToggleSwitchDevice.IsOn = True Then
            XtraTabDevice.PageEnabled = True
        Else
            XtraTabDevice.PageEnabled = False
        End If
    End Sub
    Private Sub SimpleButton1_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton1.Click
        If TextEditUsr.Text = "" Then
            XtraMessageBox.Show(ulf, "<size=10>User name cannot be Empty</size>", "<size=9>Error</size>")
            TextEditUsr.Select()
            Exit Sub
        End If
        Dim USERTYPE As String
        Dim auth_comp As String = PopupContainerEditComp.EditValue.ToString.Trim
        Dim Auth_Branch As String = PopupContainerEditLocation.EditValue.ToString.Trim
        If ComboBoxEdit2.SelectedIndex = 1 Then
            USERTYPE = "H"
            If PopupContainerEditComp.EditValue.ToString.Trim = "" Then
                XtraMessageBox.Show(ulf, "<size=10>Please select Company</size>", "<size=9>Error</size>")
                PopupContainerEditComp.Select()
                Exit Sub
            End If
            If PopupContainerEditLocation.EditValue.ToString.Trim = "" Then
                XtraMessageBox.Show(ulf, "<size=10>Please select Location</size>", "<size=9>Error</size>")
                PopupContainerEditLocation.Select()
                Exit Sub
            End If
        Else
            USERTYPE = "A"
            auth_comp = ""
            Auth_Branch = ""
        End If


        Dim USER_R As String = TextEditUsr.Text.Trim
        Dim USERDESCRIPRION As String = TextEditDes.Text.Trim
        Dim PASSWORD As String = TextEditPwd.Text.Trim
        Dim AutoProcess As String
        If ToggleSwitchAutoProcess.IsOn = True Then
            AutoProcess = "Y"
        Else
            AutoProcess = "N"
        End If
        Dim DataProcess As String
        If ToggleSwitchDataPtrocess.IsOn = True Then
            DataProcess = "Y"
        Else
            DataProcess = "N"
        End If
        Dim Main As String
        If ToggleSwitchMain.IsOn = True Then
            Main = "Y"
        Else
            Main = "N"
        End If
        Dim V_Transaction As String
        If ToggleSwitchTransaction.IsOn = True Then
            V_Transaction = "Y"
        Else
            V_Transaction = "N"
        End If
        Dim Admin As String
        If ToggleSwitchAdmin.IsOn = True Then
            Admin = "Y"
        Else
            Admin = "N"
        End If
        Dim Payroll As String
        If ToggleSwitchPay.IsOn = True Then
            Payroll = "Y"
        Else
            Payroll = "N"
        End If
        Dim Reports As String
        If ToggleSwitchReports.IsOn = True Then
            Reports = "Y"
        Else
            Reports = "N"
        End If
        Dim Leave As String
        If ToggleSwitchLeaveMgmt.IsOn = True Then
            Leave = "Y"
        Else
            Leave = "N"
        End If
        Dim IsCompliance As String
        If ToggleCompl.IsOn = True Then
            IsCompliance = "Y"
        Else
            IsCompliance = "N"
        End If
        Dim Company As String = CheckEditComp.EditValue
        Dim Department As String = CheckEditDept.EditValue
        Dim Section As String = CheckEditSec.EditValue
        Dim Grade As String = CheckEditGrade.EditValue
        Dim Category As String = CheckEditCat.EditValue
        Dim Shift As String = CheckEditShift.EditValue
        Dim Employee As String = CheckEditEmp.EditValue
        Dim Visitor As String '= "N"
        Dim Reason_Card As String = "N"
        Dim Manual_Attendance As String = CheckEditPunchEntry.EditValue
        Dim OstoOt As String = CheckEditOverStayToOT.EditValue
        Dim ShiftChange As String = CheckEditShiftShange.EditValue
        Dim HoliDay As String = CheckEditHoliday.EditValue
        Dim LeaveMaster As String = CheckEditLeaveMaster.EditValue
        Dim LeaveApplication As String = CheckEditLeaveApp.EditValue
        Dim LeaveAccural As String = CheckEditLeaveAcc.EditValue
        Dim LeaveAccuralAuto As String = CheckEditMleaveInc.EditValue
        Dim TimeOfficeSetup As String = CheckEditCommonSetting.EditValue
        Dim UserPrevilege As String = CheckEditUserPre.EditValue
        Dim Verification As String = CheckEditVerification.EditValue
        Dim InstallationSetup As String = "Y"
        Dim EmployeeSetup As String = CheckEditEmp.EditValue
        Dim ArearEntry As String = "N"
        Dim Advance_Loan As String = "N"
        Dim Form16 As String = "N"
        Dim Form16Return As String = "N"
        Dim payrollFormula As String = "N"
        Dim PayrollSetup As String = "N"
        Dim LoanAdjustment As String = "N"
        Dim TimeOfficeReport As String = CheckEditTimeReport.EditValue
        Dim VisitorReport As String = "N"
        Dim PayrollReport As String = CheckEditPayReport.EditValue
        Dim RegisterCreation As String = CheckEditAttResCre.EditValue
        Dim RegisterUpdation As String = CheckEditAttResUpd.EditValue
        Dim BackDateProcess As String = CheckEditBckDate.EditValue
        Dim ReProcess As String = "Y"
        'Dim OTCAL As String
        'Dim auth_comp As String
        'Dim Auth_dept As String
        'Dim USERTYPE As String
        'Dim paycode As String
        Dim CompAdd As String = CheckEditCompAdd.EditValue
        Dim CompModi As String = CheckEditCompEdit.EditValue
        Dim CompDel As String = CheckEditCompDelete.EditValue
        Dim DeptAdd As String = CheckEditDeptAdd.EditValue
        Dim DeptModi As String = CheckEditDeptEdit.EditValue
        Dim DeptDel As String = CheckEditDeptDelete.EditValue
        Dim CatAdd As String = CheckEditCatAdd.EditValue
        Dim CatModi As String = CheckEditCatEdit.EditValue
        Dim CatDel As String = CheckEditCatDelete.EditValue
        Dim SecAdd As String = CheckEditSecAdd.EditValue
        Dim SecModi As String = CheckEditSecEdit.EditValue
        Dim SecDel As String = CheckEditSecDelete.EditValue
        Dim GrdAdd As String = CheckEditGrdAdd.EditValue
        Dim GrdModi As String = CheckEditGrdEdit.EditValue
        Dim GrdDel As String = CheckEditGrdDelete.EditValue
        Dim SftAdd As String = CheckEditShiftAdd.EditValue
        Dim SftModi As String = CheckEditShiftEdit.EditValue
        Dim SftDel As String = CheckEditShiftDelete.EditValue
        Dim EmpAdd As String = CheckEditEmpAdd.EditValue
        Dim EmpModi As String = CheckEditEmpEdit.EditValue
        Dim EmpDel As String = CheckEditEmpDelete.EditValue
        Dim DataMaintenance As String = CheckEditDataMaintenance.EditValue

        Dim Canteen As String
        If ToggleSwitchCanteen.IsOn = True Then
            Canteen = "Y"
        Else
            Canteen = "N"
        End If
        If ToggleSwitchVisitor.IsOn = True Then
            Visitor = "Y"
        Else
            Visitor = "N"
        End If

        Dim DeviceMgmt As String = ToggleSwitchDevice.EditValue
        Dim DeviceAdd As String = CheckEditDAdd.EditValue
        Dim DeviceModi As String = CheckEditDEdit.EditValue
        Dim DeviceDelete As String = CheckEditDDelete.EditValue
        Dim LogMgmt As String = CheckEditLogMgmt.EditValue
        Dim UserSetupTemplate As String = CheckEditUserSetup.EditValue
        Dim DBSetting As String = CheckEditDBSetting.EditValue
        Dim SMSSetting As String = CheckEditSMS.EditValue
        Dim BulkSMS As String = CheckEditBulkSMS.EditValue
        Dim EmailSetting As String = CheckEditEmail.EditValue
        Dim BackUpSetting As String = CheckEditBackup.EditValue
        Dim ParallelSetting As String = CheckEditParallel.EditValue
        Dim Branch As String = CheckEditBranch.EditValue
        Dim BranchAdd As String = CheckEditBranchAdd.EditValue
        Dim BranchModi As String = CheckEditBranchEdit.EditValue
        Dim BranchDel As String = CheckEditBranchDel.EditValue
        Dim MonthlyReport As String = CheckEditMonthlyR.EditValue
        Dim DalyReport As String = CheckEditDailyR.EditValue
        Dim LeaveReport As String = CheckEditLeaveR.EditValue


        Dim sSql As String
        If UsrId = "" Then
            sSql = "Insert into tblUser ([USER_R],[USERDESCRIPRION],[PASSWORD],[AutoProcess],[DataProcess],[Main],[V_Transaction],[Admin],[Payroll],[Reports],[Leave],[Company],[Department],[Section],[Grade],[Category] ,[Shift] ,[Employee] ,[Visitor] ,[Reason_Card] ,[Manual_Attendance] ,[OstoOt] ,[ShiftChange] ,[HoliDay] ,[LeaveMaster] ,[LeaveApplication] ,[LeaveAccural] ,[LeaveAccuralAuto] ,[TimeOfficeSetup] ,[UserPrevilege] ,[Verification],[InstallationSetup] ,[EmployeeSetup] ,[ArearEntry] ,[Advance_Loan],[Form16] ,[Form16Return] ,[payrollFormula] ,[PayrollSetup] ,[LoanAdjustment] ,[TimeOfficeReport] ,[VisitorReport] ,[PayrollReport] ,[RegisterCreation] ,[RegisterUpdation] ,[BackDateProcess] ,[ReProcess] ,[CompAdd] ,[CompModi] ,[CompDel] ,[DeptAdd] ,[DeptModi] ,[DeptDel] ,[CatAdd] ,[CatModi] ,[CatDel] ,[SecAdd] ,[SecModi] ,[SecDel] ,[GrdAdd] ,[GrdModi] ,[GrdDel] ,[SftAdd] ,[SftModi] ,[SftDel] ,[EmpAdd] ,[EmpModi] ,[EmpDel] ,[DataMaintenance],[auth_comp], [Auth_Branch],[USERTYPE], [Canteen]" &
                ",[DeviceMgmt],[LogMgmt],[UserSetupTemplate],[DBSetting],[SMSSetting],[BulkSMS],[EmailSetting],[BackUpSetting],[ParallelSetting],[Branch],[BranchAdd],[BranchModi],[BranchDel],[MonthlyReport],[DalyReport],[LeaveReport],[DeviceAdd],[DeviceModi],[DeviceDelete],[IsCompliance]) " &
            "values(  '" & USER_R & "','" & USERDESCRIPRION & "', '" & PASSWORD & "', '" & AutoProcess & "', '" & DataProcess & "', '" & Main & "', '" & V_Transaction & "', '" & Admin & "', '" & Payroll & "', '" & Reports & "', '" & Leave & "', '" & Company & "', '" & Department & "', '" & Section & "', '" & Grade & "', '" & Category & "', '" & Shift & "', '" & Employee & "', '" & Visitor & "', '" & Reason_Card & "', '" & Manual_Attendance & "', '" & OstoOt & "', '" & ShiftChange & "', '" & HoliDay & "', '" & LeaveMaster & "', '" & LeaveApplication & "', '" & LeaveAccural & "', '" & LeaveAccuralAuto & "', '" & TimeOfficeSetup & "', '" & UserPrevilege & "', '" & Verification & "', '" & InstallationSetup & "', '" & EmployeeSetup & "', '" & ArearEntry & "', '" & Advance_Loan & "', '" & Form16 & "', '" & Form16Return & "', '" & payrollFormula & "', '" & PayrollSetup & "', '" & LoanAdjustment & "', '" & TimeOfficeReport & "', '" & VisitorReport & "', '" & PayrollReport & "', '" & RegisterCreation & "', '" & RegisterUpdation & "', '" & BackDateProcess & "', '" & ReProcess & "', '" & CompAdd & "', '" & CompModi & "', '" & CompDel & "', '" & DeptAdd & "', '" & DeptModi & "', '" & DeptDel & "', '" & CatAdd & "', '" & CatModi & "', '" & CatDel & "', '" & SecAdd & "', '" & SecModi & "', '" & SecDel & "', '" & GrdAdd & "', '" & GrdModi & "', '" & GrdDel & "', '" & SftAdd & "', '" & SftModi & "', '" & SftDel & "', '" & EmpAdd & "', '" & EmpModi & "', '" & EmpDel & "', '" & DataMaintenance & "','" & auth_comp & "','" & Auth_Branch & "','" & USERTYPE & "','" & Canteen & "'" &
            " ,'" & DeviceMgmt & "','" & LogMgmt & "','" & UserSetupTemplate & "','" & DBSetting & "','" & SMSSetting & "','" & BulkSMS & "','" & EmailSetting & "','" & BackUpSetting & "','" & ParallelSetting & "','" & Branch & "','" & BranchAdd & "','" & BranchModi & "','" & BranchDel & "','" & MonthlyReport & "','" & DalyReport & "','" & LeaveReport & "','" & DeviceAdd & "','" & DeviceModi & "','" & DeviceDelete & "','" & IsCompliance & "')"
            Common.LogPost("User Add; User Name:" & USER_R)
        Else
            sSql = "update tblUser set [USERDESCRIPRION]='" & USERDESCRIPRION & "', [PASSWORD]='" & PASSWORD & "', [AutoProcess]='" & AutoProcess & "', [DataProcess]='" & DataProcess & "', [Main]='" & Main & "', [V_Transaction]='" & V_Transaction & "', [Admin]='" & Admin & "', [Payroll]='" & Payroll & "', [Reports]='" & Reports & "', [Leave]='" & Leave & "', [Company]='" & Company & "', [Department]='" & Department & "', [Section]='" & Section & "', [Grade]='" & Grade & "', [Category]='" & Category & "', [Shift]='" & Shift & "', [Employee]='" & Employee & "', [Visitor]='" & Visitor & "', [Reason_Card]='" & Reason_Card & "', [Manual_Attendance]='" & Manual_Attendance & "', [OstoOt]='" & OstoOt & "', [ShiftChange]='" & ShiftChange & "', [HoliDay]='" & HoliDay & "', [LeaveMaster]='" & LeaveMaster & "', [LeaveApplication]='" & LeaveApplication & "', [LeaveAccural]='" & LeaveAccural & "', [LeaveAccuralAuto]='" & LeaveAccuralAuto & "', [TimeOfficeSetup]='" & TimeOfficeSetup & "', " &
                "[UserPrevilege]='" & UserPrevilege & "', [Verification]='" & Verification & "', [InstallationSetup]='" & InstallationSetup & "', [EmployeeSetup]='" & EmployeeSetup & "', [ArearEntry]='" & ArearEntry & "', [Advance_Loan]='" & Advance_Loan & "', [Form16]='" & Form16 & "', [Form16Return]='" & Form16Return & "', [payrollFormula]='" & payrollFormula & "', [PayrollSetup]='" & PayrollSetup & "', [LoanAdjustment]='" & LoanAdjustment & "', [TimeOfficeReport]='" & TimeOfficeReport & "', [VisitorReport]='" & VisitorReport & "', [PayrollReport]='" & PayrollReport & "', " &
                "[RegisterCreation]='" & RegisterCreation & "', [RegisterUpdation]='" & RegisterUpdation & "', [BackDateProcess]='" & BackDateProcess & "', [ReProcess]='" & ReProcess & "', [CompAdd]='" & CompAdd & "', [CompModi]='" & CompModi & "', [CompDel]='" & CompDel & "', [DeptAdd]='" & DeptAdd & "', [DeptModi]='" & DeptModi & "', [DeptDel]='" & DeptDel & "', [CatAdd]='" & CatAdd & "', [CatModi]='" & CatModi & "', [CatDel]='" & CatDel & "', [SecAdd]='" & SecAdd & "', [SecModi]='" & SecModi & "', [SecDel]='" & SecDel & "', [GrdAdd]='" & GrdAdd & "', [GrdModi]='" & GrdModi & "', [GrdDel]='" & GrdDel & "', [SftAdd]='" & SftAdd & "', [SftModi]='" & SftModi & "', [SftDel]='" & SftDel & "', [EmpAdd]='" & EmpAdd & "', [EmpModi]='" & EmpModi & "', [EmpDel]='" & EmpDel & "', [DataMaintenance]='" & DataMaintenance & "', Auth_Branch='" & Auth_Branch & "', auth_comp='" & auth_comp & "', USERTYPE='" & USERTYPE & "', [Canteen]='" & Canteen & "'" &
                ",[DeviceMgmt]='" & DeviceMgmt & "'" &
                ",[LogMgmt]='" & LogMgmt & "' " &
                ",[UserSetupTemplate]='" & UserSetupTemplate & "' " &
                ",[DBSetting]='" & DBSetting & "' " &
                ",[SMSSetting]='" & SMSSetting & "'" &
                ",[BulkSMS]='" & BulkSMS & "' " &
                ",[EmailSetting]='" & EmailSetting & "' " &
                ",[BackUpSetting]='" & BackUpSetting & "' " &
                ",[ParallelSetting]='" & ParallelSetting & "' " &
                ",[Branch]='" & Branch & "' " &
                ",[BranchAdd]='" & BranchAdd & "'" &
                ",[BranchModi]='" & BranchModi & "' " &
                ",[BranchDel]='" & BranchDel & "' " &
                ",[MonthlyReport]='" & MonthlyReport & "' " &
                ",[DalyReport]='" & DalyReport & "' " &
                ",[LeaveReport]='" & LeaveReport & "' " &
                ",[DeviceAdd]='" & DeviceAdd & "' " &
                ",[DeviceModi]='" & DeviceModi & "'" &
                ",[DeviceDelete]='" & DeviceDelete & "'" &
                ",[IsCompliance]='" & IsCompliance & "'" &
                " where [USER_R]='" & TextEditUsr.Text.Trim & "'"

            Common.LogPost("User Edit; User Name:" & USER_R)
        End If
        If Common.servername = "Access" Then
            If Common.con1.State <> ConnectionState.Open Then
                Common.con1.Open()
            End If
            cmd1 = New OleDbCommand(sSql, Common.con1)
            cmd1.ExecuteNonQuery()
            If Common.con1.State <> ConnectionState.Closed Then
                Common.con1.Close()
            End If
        Else
            If Common.con.State <> ConnectionState.Open Then
                Common.con.Open()
            End If
            cmd = New SqlCommand(sSql, Common.con)
            cmd.ExecuteNonQuery()
            If Common.con.State <> ConnectionState.Closed Then
                Common.con.Close()
            End If
        End If
        If UsrId = Common.USER_R Then
            XtraMessageBox.Show(ulf, "<size=10>Saved Successfuly. Application will Restart Now.</size>", "<size=9>Success</size>")
            If Application.OpenForms().OfType(Of XtraRealTimePunches).Any Then  'to check real time is runing or not
                XtraRealTimePunches.Close()
                XtraRealTimePunchesUltra.Close()
            End If
            Application.Exit()
            Process.Start(Application.ExecutablePath)
        Else
            XtraMessageBox.Show(ulf, "<size=10>Saved Successfuly</size>", "<size=9>Success</size>")
        End If
        Me.Close()
        'restart application here with asking
    End Sub
    Private Sub ToggleSwitchMain_Toggled(sender As System.Object, e As System.EventArgs) Handles ToggleSwitchMain.Toggled
        visibiltyChange()
    End Sub
    Private Sub ToggleSwitchTransaction_Toggled(sender As System.Object, e As System.EventArgs) Handles ToggleSwitchTransaction.Toggled
        visibiltyChange()
    End Sub
    Private Sub ToggleSwitchDataPtrocess_Toggled(sender As System.Object, e As System.EventArgs) Handles ToggleSwitchDataPtrocess.Toggled
        visibiltyChange()
    End Sub
    Private Sub ToggleSwitchLeaveMgmt_Toggled(sender As System.Object, e As System.EventArgs) Handles ToggleSwitchLeaveMgmt.Toggled
        visibiltyChange()
    End Sub
    Private Sub ToggleSwitchPay_Toggled(sender As System.Object, e As System.EventArgs) Handles ToggleSwitchPay.Toggled
        visibiltyChange()
    End Sub
    Private Sub ToggleSwitchAdmin_Toggled(sender As System.Object, e As System.EventArgs) Handles ToggleSwitchAdmin.Toggled
        visibiltyChange()
    End Sub
    Private Sub ToggleSwitchAutoProcess_Toggled(sender As System.Object, e As System.EventArgs) Handles ToggleSwitchAutoProcess.Toggled
        visibiltyChange()
    End Sub
    Private Sub ToggleSwitchReports_Toggled(sender As System.Object, e As System.EventArgs) Handles ToggleSwitchReports.Toggled
        visibiltyChange()
    End Sub
    Private Sub PopupContainerEditComp_QueryResultValue(sender As System.Object, e As DevExpress.XtraEditors.Controls.QueryResultValueEventArgs) Handles PopupContainerEditComp.QueryResultValue
        Dim selectedRows() As Integer = GridViewComp.GetSelectedRows
        Dim sb As StringBuilder = New StringBuilder
        For Each selectionRow As Integer In selectedRows
            Dim a As System.Data.DataRowView = GridViewComp.GetRow(selectionRow)
            If (sb.ToString.Length > 0) Then
                sb.Append(", ")
            End If
            sb.Append(a.Item("COMPANYCODE"))
        Next
        e.Value = sb.ToString
    End Sub
    Private Sub PopupContainerEditComp_QueryPopUp(sender As System.Object, e As System.ComponentModel.CancelEventArgs) Handles PopupContainerEditComp.QueryPopUp
        Dim val As Object = PopupContainerEditComp.EditValue
        If (val Is Nothing) Then
            GridViewComp.ClearSelection()
        Else
            'Dim texts() As String = val.ToString.Split(Microsoft.VisualBasic.ChrW(44))
            Dim texts() As String = val.ToString.Split(",")
            For Each text As String In texts
                If text.Trim.Length = 1 Then
                    text = text.Trim & "  "
                ElseIf text.Trim.Length = 2 Then
                    text = text.Trim & " "
                End If
                'MsgBox(text & "  " & text.Length & " " & GridView1.LocateByValue("SHIFT", text))
                Dim rowHandle As Integer = GridViewComp.LocateByValue("COMPANYCODE", text.Trim)
                GridViewComp.SelectRow(rowHandle)
            Next
        End If
    End Sub
    Private Sub PopupContainerEditLocation_QueryResultValue(sender As System.Object, e As DevExpress.XtraEditors.Controls.QueryResultValueEventArgs) Handles PopupContainerEditLocation.QueryResultValue
        Dim selectedRows() As Integer = GridViewBranch.GetSelectedRows
        Dim sb As StringBuilder = New StringBuilder
        For Each selectionRow As Integer In selectedRows
            Dim a As System.Data.DataRowView = GridViewBranch.GetRow(selectionRow)
            If (sb.ToString.Length > 0) Then
                sb.Append(", ")
            End If
            sb.Append(a.Item("BRANCHCODE"))
        Next
        e.Value = sb.ToString
    End Sub
    Private Sub PopupContainerEditLocation_QueryPopUp(sender As System.Object, e As System.ComponentModel.CancelEventArgs) Handles PopupContainerEditLocation.QueryPopUp
        Dim val As Object = PopupContainerEditLocation.EditValue
        If (val Is Nothing) Then
            GridViewBranch.ClearSelection()
        Else
            Dim texts() As String = val.ToString.Split(",")
            For Each text As String In texts
                If text.Trim.Length = 1 Then
                    text = text.Trim & "  "
                ElseIf text.Trim.Length = 2 Then
                    text = text.Trim & " "
                End If
                Dim rowHandle As Integer = GridViewBranch.LocateByValue("BRANCHCODE", text.Trim)
                GridViewBranch.SelectRow(rowHandle)
            Next
        End If
    End Sub
    Private Sub ComboBoxEdit2_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBoxEdit2.SelectedIndexChanged
        If ComboBoxEdit2.SelectedIndex = 0 Then
            PopupContainerEditComp.Visible = False
            PopupContainerEditLocation.Visible = False
            LabelControl13.Visible = False
            LabelControl14.Visible = False
        ElseIf ComboBoxEdit2.SelectedIndex = 1 Then
            PopupContainerEditComp.Visible = True
            PopupContainerEditLocation.Visible = True
            LabelControl13.Visible = True
            LabelControl14.Visible = True
        End If
    End Sub

    Private Sub CheckEditMarlAll_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckEditMarlAll.CheckedChanged
        If CheckEditMarlAll.Checked = True Then
            CheckEditMarlAll.Text = "Unmark All"
            CheckEditComp.Checked = True
            CheckEditCompAdd.Checked = True
            CheckEditCompEdit.Checked = True
            CheckEditCompDelete.Checked = True

            CheckEditDept.Checked = True
            CheckEditDeptAdd.Checked = True
            CheckEditDeptEdit.Checked = True
            CheckEditDeptDelete.Checked = True

            CheckEditSec.Checked = True
            CheckEditSecAdd.Checked = True
            CheckEditSecEdit.Checked = True
            CheckEditSecDelete.Checked = True

            CheckEditGrade.Checked = True
            CheckEditGrdAdd.Checked = True
            CheckEditGrdEdit.Checked = True
            CheckEditGrdDelete.Checked = True

            CheckEditCat.Checked = True
            CheckEditCatEdit.Checked = True
            CheckEditCatAdd.Checked = True
            CheckEditCatDelete.Checked = True

            CheckEditShift.Checked = True
            CheckEditShiftAdd.Checked = True
            CheckEditShiftEdit.Checked = True
            CheckEditShiftDelete.Checked = True

            CheckEditEmp.Checked = True
            CheckEditEmpAdd.Checked = True
            CheckEditEmpEdit.Checked = True
            CheckEditEmpDelete.Checked = True

            CheckEditBranch.Checked = True
            CheckEditBranchAdd.Checked = True
            CheckEditBranchEdit.Checked = True
            CheckEditBranchDel.Checked = True
        Else
            CheckEditMarlAll.Text = "Mark All"
            CheckEditComp.Checked = False
            CheckEditCompAdd.Checked = False
            CheckEditCompEdit.Checked = False
            CheckEditCompDelete.Checked = False

            CheckEditDept.Checked = False
            CheckEditDeptAdd.Checked = False
            CheckEditDeptEdit.Checked = False
            CheckEditDeptDelete.Checked = False

            CheckEditSec.Checked = False
            CheckEditSecAdd.Checked = False
            CheckEditSecEdit.Checked = False
            CheckEditSecDelete.Checked = False

            CheckEditGrade.Checked = False
            CheckEditGrdAdd.Checked = False
            CheckEditGrdEdit.Checked = False
            CheckEditGrdDelete.Checked = False

            CheckEditCat.Checked = False
            CheckEditCatEdit.Checked = False
            CheckEditCatAdd.Checked = False
            CheckEditCatDelete.Checked = False

            CheckEditShift.Checked = False
            CheckEditShiftAdd.Checked = False
            CheckEditShiftEdit.Checked = False
            CheckEditShiftDelete.Checked = False

            CheckEditEmp.Checked = False
            CheckEditEmpAdd.Checked = False
            CheckEditEmpEdit.Checked = False
            CheckEditEmpDelete.Checked = False

            CheckEditBranch.Checked = False
            CheckEditBranchAdd.Checked = False
            CheckEditBranchEdit.Checked = False
            CheckEditBranchDel.Checked = False

        End If
    End Sub

    Private Sub ToggleSwitchDevice_Toggled(sender As System.Object, e As System.EventArgs) Handles ToggleSwitchDevice.Toggled
        visibiltyChange()
    End Sub
End Class