﻿Imports System.Resources
Imports System.Globalization
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports System.IO
Imports System.Text
Imports DevExpress.XtraSplashScreen
Imports DevExpress.XtraEditors
Imports DevExpress.LookAndFeel
Imports iAS.AscDemo
Imports System.Runtime.InteropServices
Imports iAS.AcsDemo
Imports System.Threading

Public Class CommunicationFormHikvision
    Public Delegate Sub invokeDelegate()
    Dim ConnectionString As String
    Dim ulf As UserLookAndFeel
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    Dim DC As New DateConverter()
    Dim datafile As String = "" '= System.Environment.CurrentDirectory & "\Data\" & Now.ToString("yyyyMMddHHmmss") & ".txt"
    Dim serialNo As String = "" 'Hikvision

    'Dim IN_OUT As String = ""
    'Dim MC_NO As String = ""
    'Dim ID_NO As String = ""
    Dim g_fGetAcsEventCallback As CHCNetSDK.RemoteConfigCallback = Nothing
    Dim m_lGetAcsEvent As Integer = 0

    Public Sub New()
        InitializeComponent()
        'MsgBox("")
        'If Common.servername = "Access" Then
        '    'ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\HTDB.mdb;Persist Security Info=True;Jet OLEDB:Database Password=SSS"
        '    Me.TblMachine1TableAdapter1.Fill(Me.SSSDBDataSet.tblMachine1)
        '    GridControl1.DataSource = SSSDBDataSet.tblMachine1
        'Else
        '    'ConnectionString = "Data Source=" & servername & ";Initial Catalog=SSSDB;Integrated Security=True"
        '    TblMachineTableAdapter1.Connection.ConnectionString = Common.ConnectionString '"Data Source=DESKTOP-PG6NDV8;Initial Catalog=SSSDB;Integrated Security=True"
        '    Me.TblMachineTableAdapter1.Fill(Me.SSSDBDataSet.tblMachine)
        '    GridControl1.DataSource = SSSDBDataSet.tblMachine
        'End If
        GridControl1.DataSource = Common.MachineNonAdminUltra
        Common.SetGridFont(GridView1, New Font("Tahoma", 9))
    End Sub
    Private Sub XtraCommunicationForm_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        'Me.Width = Common.NavWidth 'Me.Parent.Width
        'Me.Height = Common.NavHeight 'Me.Parent.Height
        'SplitContainerControl1.Width = SplitContainerControl1.Parent.Width  'Common.splitforMasterMenuWidth '
        'SplitContainerControl1.SplitterPosition = Common.SplitterPosition '(SplitContainerControl1.Parent.Width) * 85 / 100 
        'MsgBox(SplitContainerControl1.Width)
        'MsgBox(Common.SplitterPosition)
        'MsgBox(SplitContainerControl1.SplitterPosition)

        'res_man = New ResourceManager("TimeWatchNewDeskTop.Res", GetType(XtraDevice).Assembly)
        'cul = CultureInfo.CreateSpecificCulture("en")
        LabelControl1.Text = Common.res_man.GetString("selectdevice", Common.cul)
        LabelControl2.Text = Common.res_man.GetString("download_from_all", Common.cul)
        LabelControl3.Text = Common.res_man.GetString("delete_from_device", Common.cul)
        ToggleSwitch1.Properties.OffText = Common.res_man.GetString("off", Common.cul)
        ToggleSwitch1.Properties.OnText = Common.res_man.GetString("onn", Common.cul)
        ToggleSwitch2.Properties.OffText = Common.res_man.GetString("off", Common.cul)
        ToggleSwitch2.Properties.OnText = Common.res_man.GetString("onn", Common.cul)
        'CheckEdit1.Text = Common.res_man.GetString("oldlog", Common.cul)
        'CheckEdit2.Text = Common.res_man.GetString("newlog", Common.cul)
        SimpleButtonRead.Text = Common.res_man.GetString("readdate", Common.cul)
        SimpleButtonSet.Text = Common.res_man.GetString("setdate", Common.cul)
        SimpleButtonDownload.Text = Common.res_man.GetString("downloadlogs", Common.cul)
        colID_NO.Caption = Common.res_man.GetString("controller_id", Common.cul)
        colLOCATION.Caption = Common.res_man.GetString("device_ip", Common.cul)
        colbranch.Caption = Common.res_man.GetString("location", Common.cul)


        ''this is if device modified in device grid then immidiate update in device list of download device select 
        'If Common.servername = "Access" Then
        '    'ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\HTDB.mdb;Persist Security Info=True;Jet OLEDB:Database Password=SSS"
        '    Me.TblMachine1TableAdapter1.Fill(Me.SSSDBDataSet.tblMachine1)
        '    GridControl1.DataSource = SSSDBDataSet.tblMachine1
        'Else
        '    'ConnectionString = "Data Source=" & servername & ";Initial Catalog=SSSDB;Integrated Security=True"
        '    TblMachineTableAdapter1.Connection.ConnectionString = Common.ConnectionString '"Data Source=DESKTOP-PG6NDV8;Initial Catalog=SSSDB;Integrated Security=True"
        '    Me.TblMachineTableAdapter1.Fill(Me.SSSDBDataSet.tblMachine)
        '    GridControl1.DataSource = SSSDBDataSet.tblMachine
        'End If
        GridControl1.DataSource = Common.MachineNonAdminUltra
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True
        PopupContainerEdit1.EditValue = ""
        TextEdit1.Text = ""
        CheckEdit2.Checked = True
        ToggleSwitch2.IsOn = False
        CheckEditTimePerioad.Checked = False
        'DateEditFrmDate.Enabled = False
        'DateEditToDate.Enabled = False
        'LabelControl4.Enabled = False
        'LabelControl5.Enabled = False
        DateEditFrmDate.DateTime = Now.AddDays(-1)
        DateEditToDate.DateTime = Now

        'If CheckEditTimePerioad.Checked = True Then
        '    ComboNYearStr.Enabled = True
        '    ComboNMonthStr.Enabled = True
        '    ComboNDateStr.Enabled = True
        '    ComboNYearEnd.Enabled = True
        '    ComboNMonthEnd.Enabled = True
        '    ComboNDateEnd.Enabled = True
        '    LabelControl4.Enabled = True
        '    LabelControl5.Enabled = True
        'Else
        '    ComboNYearStr.Enabled = False
        '    ComboNMonthStr.Enabled = False
        '    ComboNDateStr.Enabled = False
        '    ComboNYearEnd.Enabled = False
        '    ComboNMonthEnd.Enabled = False
        '    ComboNDateEnd.Enabled = False
        '    LabelControl4.Enabled = False
        '    LabelControl5.Enabled = False
        'End If

        If Common.IsNepali = "Y" Then
            DateEditFrmDate.Visible = False
            DateEditToDate.Visible = False
            ComboNYearStr.Visible = True
            ComboNMonthStr.Visible = True
            ComboNDateStr.Visible = True
            ComboNYearEnd.Visible = True
            ComboNMonthEnd.Visible = True
            ComboNDateEnd.Visible = True

            Dim doj As DateTime = DC.ToBS(New Date(DateEditFrmDate.DateTime.Year, DateEditFrmDate.DateTime.Month, DateEditFrmDate.DateTime.Day))
            ComboNYearStr.EditValue = doj.Year
            ComboNMonthStr.SelectedIndex = doj.Month - 1
            ComboNDateStr.EditValue = doj.Day

            doj = DC.ToBS(New Date(DateEditToDate.DateTime.Year, DateEditToDate.DateTime.Month, DateEditToDate.DateTime.Day))
            ComboNYearEnd.EditValue = doj.Year
            ComboNMonthEnd.SelectedIndex = doj.Month - 1
            ComboNDateEnd.EditValue = doj.Day
        Else
            DateEditFrmDate.Visible = True
            DateEditToDate.Visible = True
            ComboNYearStr.Visible = False
            ComboNMonthStr.Visible = False
            ComboNDateStr.Visible = False
            ComboNYearEnd.Visible = False
            ComboNMonthEnd.Visible = False
            ComboNDateEnd.Visible = False
        End If
        GridView1.ClearSelection()
    End Sub
    Private Sub PopupContainerEdit1_QueryPopUp(sender As System.Object, e As System.ComponentModel.CancelEventArgs) Handles PopupContainerEdit1.QueryPopUp
        Dim val As Object = PopupContainerEdit1.EditValue
        If (val Is Nothing) Then
            GridView1.ClearSelection()
        Else
            'Dim texts() As String = val.ToString.Split(Microsoft.VisualBasic.ChrW(44))
            Dim texts() As String = val.ToString.Split(",")
            For Each text As String In texts
                If text.Trim.Length = 1 Then
                    text = text.Trim & "  "
                ElseIf text.Trim.Length = 2 Then
                    text = text.Trim & " "
                End If
                'MsgBox(text & "  " & text.Length & " " & GridView1.LocateByValue("SHIFT", text))
                Dim rowHandle As Integer = GridView1.LocateByValue("ID_NO", text)
                GridView1.SelectRow(rowHandle)
            Next
        End If
    End Sub
    Private Sub PopupContainerEdit1_QueryResultValue(sender As System.Object, e As DevExpress.XtraEditors.Controls.QueryResultValueEventArgs) Handles PopupContainerEdit1.QueryResultValue
        Dim selectedRows() As Integer = GridView1.GetSelectedRows
        Dim sb As StringBuilder = New StringBuilder
        For Each selectionRow As Integer In selectedRows
            Dim a As System.Data.DataRowView = GridView1.GetRow(selectionRow)
            If (sb.ToString.Length > 0) Then
                sb.Append(", ")
            End If
            sb.Append(a.Item("ID_NO"))
        Next
        e.Value = sb.ToString
    End Sub
    Private Sub SimpleButton4_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton4.Click
        'Dim comclass As Common = New Common
        'comclass.Load_Corporate_PolicySql()
        'Me.Cursor = Cursors.WaitCursor

        'comclass.Process_AllnonRTC(DateEdit3.DateTime, Now.Date, "000000000000", "zzzzzzzzzzzz")
        'comclass.Process_AllRTC(DateEdit3.DateTime, Now.Date, "000000000000", "zzzzzzzzzzzz")
        'Me.Cursor = Cursors.Default
        Me.Close()
    End Sub
    Private Sub SimpleButtonRead_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonRead.Click
        If PopupContainerEdit1.EditValue.ToString.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Please select device</size>", "Failed")
            PopupContainerEdit1.Select()
            Exit Sub
        End If
        Dim a As DateTime
        Dim c As Common = New Common()
        Dim vnMachineNumber As Integer
        Dim vnLicense As Long
        Dim vpszIPAddress As String
        Dim vpszNetPort As Integer
        Dim vpszNetPassword As Integer
        Dim vnTimeOut As Long
        Dim vnProtocolType As Long
        Dim vnReadMark As Integer

        Dim RowHandles() As Integer = GridView1.GetSelectedRows
        Dim IP, ID_NO, DeviceType, A_R As Object
        Dim cn As Common = New Common
        Dim failIP As New List(Of String)()
        Me.Cursor = Cursors.WaitCursor
        IP = GridView1.GetRowCellValue(RowHandles(0), GridView1.Columns("LOCATION"))
        Dim commkey As Integer '= Convert.ToInt32(GridView1.GetRowCellValue(RowHandles(0), GridView1.Columns("commkey")))
        ID_NO = GridView1.GetRowCellValue(RowHandles(0), GridView1.Columns("ID_NO"))
        DeviceType = GridView1.GetRowCellValue(RowHandles(0), GridView1.Columns("DeviceType"))
        A_R = GridView1.GetRowCellValue(RowHandles(0), GridView1.Columns("A_R"))
        If DeviceType = "Bio-1/3/4/5/6/7/ATF-203/ATF-395/ACR-872" Then
            
        ElseIf DeviceType = "ZK(TFT)" Or DeviceType.ToString.Trim = "Bio-1Pro/ATF305Pro/ATF686Pro" Then
            
        ElseIf DeviceType = "HKSeries" Then
            Dim struDeviceInfoV40 As CHCNetSDK.NET_DVR_DEVICEINFO_V40 = New CHCNetSDK.NET_DVR_DEVICEINFO_V40
            Dim DeviceAdd As String = IP
            Dim userName As String = GridView1.GetRowCellValue(RowHandles(0), GridView1.Columns("HLogin"))
            Dim pwd As String = GridView1.GetRowCellValue(RowHandles(0), GridView1.Columns("HPassword"))
            Dim lUserID As Integer = -1
            'MsgBox(DeviceAdd & vbCrLf & userName & vbCrLf & pwd)
            Dim failReason As String = ""
            Dim logistatus = cn.HikvisionLogin(DeviceAdd, userName, pwd, struDeviceInfoV40, lUserID, failReason)
            'MsgBox("lUserID " & lUserID.ToString)
            If logistatus = False Then
                'MsgBox(CHCNetSDK.NET_DVR_GetLastError.ToString & "  Login failed")
                XtraMessageBox.Show(ulf, "<size=10>" & failReason & "</size>", "Failed")
                XtraMasterTest.LabelControlStatus.Text = ""
                Application.DoEvents()
                Me.Cursor = Cursors.Default
                Exit Sub
            Else
                If failReason <> "" Then
                    XtraMessageBox.Show(ulf, "<size=10>" & failReason & "</size>", "Failed")
                    XtraMasterTest.LabelControlStatus.Text = ""
                    Application.DoEvents()
                    Me.Cursor = Cursors.Default
                    logistatus = cn.HikvisionLogOut(lUserID)
                    Exit Sub
                End If
                Dim struTimeCfg As CHCNetSDK.NET_DVR_TIME = New CHCNetSDK.NET_DVR_TIME
                Dim dwReturned As UInteger = 0
                Dim dwSize As UInteger = CType(Marshal.SizeOf(struTimeCfg), UInteger)
                Dim ptrTimeCfg As IntPtr = Marshal.AllocHGlobal(CType(dwSize, Int32))
                Marshal.StructureToPtr(struTimeCfg, ptrTimeCfg, False)
                If Not CHCNetSDK.NET_DVR_GetDVRConfig(lUserID, CHCNetSDK.NET_DVR_GET_TIMECFG, 1, ptrTimeCfg, dwSize, dwReturned) Then
                    MessageBox.Show((lUserID & vbCrLf & "Get device time error code :" + CHCNetSDK.NET_DVR_GetLastError.ToString))
                    Marshal.FreeHGlobal(ptrTimeCfg)
                    ptrTimeCfg = IntPtr.Zero
                    XtraMessageBox.Show(ulf, "<size=10>Connection Failed</size>", "Failed")
                    XtraMasterTest.LabelControlStatus.Text = ""
                    Application.DoEvents()
                    Me.Cursor = Cursors.Default
                    Return
                Else
                    struTimeCfg = CType(Marshal.PtrToStructure(ptrTimeCfg, GetType(CHCNetSDK.NET_DVR_TIME)), CHCNetSDK.NET_DVR_TIME)
                    TextEdit1.Text = struTimeCfg.dwYear.ToString("0000") & "-" & struTimeCfg.dwMonth.ToString("00") & "-" & struTimeCfg.dwDay.ToString("00") & _
                         " " & struTimeCfg.dwHour.ToString("00") & ":" & struTimeCfg.dwMinute.ToString("00") & ":" & struTimeCfg.dwSecond.ToString("00")
                End If
                'logistatus = cn.HikvisionLogOut(lUserID)
            End If

        End If
        Me.Cursor = Cursors.Default
    End Sub
    Private Sub SimpleButtonSet_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonSet.Click
        If PopupContainerEdit1.EditValue.ToString.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Please select device</size>", "Failed")
            PopupContainerEdit1.Select()
            Exit Sub
        End If
        Dim a As DateTime
        Dim c As Common = New Common()
        Dim vnMachineNumber As Integer
        Dim vnLicense As Long
        Dim vpszIPAddress As String
        Dim vpszNetPort As Integer
        Dim vpszNetPassword As Integer
        Dim vnTimeOut As Long
        Dim vnProtocolType As Long
        Dim vnReadMark As Integer
        Me.Cursor = Cursors.WaitCursor

        Dim RowHandles() As Integer = GridView1.GetSelectedRows
        Dim IP, ID_NO, DeviceType, A_R As Object
        Dim cn As Common = New Common
        Dim failIP As New List(Of String)()

        IP = GridView1.GetRowCellValue(RowHandles(0), GridView1.Columns("LOCATION"))
        ID_NO = GridView1.GetRowCellValue(RowHandles(0), GridView1.Columns("ID_NO"))
        DeviceType = GridView1.GetRowCellValue(RowHandles(0), GridView1.Columns("DeviceType"))
        A_R = GridView1.GetRowCellValue(RowHandles(0), GridView1.Columns("A_R"))
        Dim commkey As Integer '= Convert.ToInt32(GridView1.GetRowCellValue(RowHandles(0), GridView1.Columns("commkey")))
        If DeviceType = "Bio-1/3/4/5/6/7/ATF-203/ATF-395/ACR-872" Then

        ElseIf DeviceType = "ZK(TFT)" Or DeviceType.ToString.Trim = "Bio-1Pro/ATF305Pro/ATF686Pro" Then

        ElseIf DeviceType = "HKSeries" Then
            Dim struDeviceInfoV40 As CHCNetSDK.NET_DVR_DEVICEINFO_V40 = New CHCNetSDK.NET_DVR_DEVICEINFO_V40
            Dim DeviceAdd As String = IP, userName As String = GridView1.GetRowCellValue(RowHandles(0), GridView1.Columns("HLogin")), pwd As String = GridView1.GetRowCellValue(RowHandles(0), GridView1.Columns("HPassword"))
            Dim lUserID As Integer = -1
            Dim failReason As String = ""
            Dim logistatus = cn.HikvisionLogin(DeviceAdd, userName, pwd, struDeviceInfoV40, lUserID, failReason)
            If logistatus = False Then
                XtraMessageBox.Show(ulf, "<size=10>" & failReason & "</size>", "Failed")
                XtraMasterTest.LabelControlStatus.Text = ""
                Application.DoEvents()
                Me.Cursor = Cursors.Default
                Exit Sub
            Else
                If failReason <> "" Then
                    XtraMessageBox.Show(ulf, "<size=10>" & failReason & "</size>", "Failed")
                    XtraMasterTest.LabelControlStatus.Text = ""
                    Application.DoEvents()
                    Me.Cursor = Cursors.Default
                    logistatus = cn.HikvisionLogOut(lUserID)
                    Exit Sub
                End If

                Dim struTimeCfg As CHCNetSDK.NET_DVR_TIME = New CHCNetSDK.NET_DVR_TIME
                struTimeCfg.dwYear = Now.Year
                struTimeCfg.dwMonth = Now.Month
                struTimeCfg.dwDay = Now.Day
                struTimeCfg.dwHour = Now.Hour
                struTimeCfg.dwMinute = Now.Minute
                struTimeCfg.dwSecond = Now.Second
                Dim dwReturned As UInteger = 0
                Dim dwSize As UInteger = CType(Marshal.SizeOf(struTimeCfg), UInteger)
                Dim ptrTimeCfg As IntPtr = Marshal.AllocHGlobal(CType(dwSize, Int32))
                Marshal.StructureToPtr(struTimeCfg, ptrTimeCfg, False)
                'CHCNetSDK.NET_DVR_SetDVRConfig(userID, CHCNetSDK.NET_DVR_SET_TIMECFG, 1, ptrTimeCfg, dwSize)
                If Not CHCNetSDK.NET_DVR_SetDVRConfig(lUserID, CHCNetSDK.NET_DVR_SET_TIMECFG, 1, ptrTimeCfg, dwSize) Then
                    MessageBox.Show((lUserID & vbCrLf & "Get device time error code :" + CHCNetSDK.NET_DVR_GetLastError.ToString))
                    Marshal.FreeHGlobal(ptrTimeCfg)
                    ptrTimeCfg = IntPtr.Zero
                    XtraMessageBox.Show(ulf, "<size=10>Connection Failed</size>", "Failed")
                    Me.Cursor = Cursors.Default
                    Return
                Else
                    struTimeCfg = CType(Marshal.PtrToStructure(ptrTimeCfg, GetType(CHCNetSDK.NET_DVR_TIME)), CHCNetSDK.NET_DVR_TIME)
                    TextEdit1.Text = struTimeCfg.dwYear.ToString("0000") & "-" & struTimeCfg.dwMonth.ToString("00") & "-" & struTimeCfg.dwDay.ToString("00") & _
                         " " & struTimeCfg.dwHour.ToString("00") & ":" & struTimeCfg.dwMinute.ToString("00") & ":" & struTimeCfg.dwSecond.ToString("00")
                End If
                'logistatus = cn.HikvisionLogOut(lUserID)
            End If
        End If
        Me.Cursor = Cursors.Default
    End Sub
    Private Sub SimpleButtonDownload_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonDownload.Click
        If PopupContainerEdit1.EditValue.ToString.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Please select device</size>", "Failed")
            PopupContainerEdit1.Select()
            Exit Sub
        End If
        Dim datafile As String = System.Environment.CurrentDirectory & "\Data\" & Now.ToString("yyyyMMddHHmmss") & ".txt"

        Dim RowHandles() As Integer = GridView1.GetSelectedRows
        Dim IP, ID_NO, DeviceType, A_R, Purpose As Object
        'Dim commkey As Integer
        Dim IN_OUT As String
        Dim cn As Common = New Common
        Dim failIP As New List(Of String)()
        'DevExpress.XtraSplashScreen.SplashScreenManager.ShowForm(Me, GetType(WaitForm1), True, True, False)
        For Each rowHandle As Integer In RowHandles
            IP = GridView1.GetRowCellValue(rowHandle, GridView1.Columns("LOCATION"))
            IN_OUT = GridView1.GetRowCellValue(rowHandle, GridView1.Columns("IN_OUT"))
            ID_NO = GridView1.GetRowCellValue(rowHandle, GridView1.Columns("ID_NO"))
            XtraMasterTest.LabelControlStatus.Text = "Connecting " & IP & "..."
            Application.DoEvents()
            DeviceType = GridView1.GetRowCellValue(rowHandle, GridView1.Columns("DeviceType"))
            A_R = GridView1.GetRowCellValue(rowHandle, GridView1.Columns("A_R"))
            Purpose = GridView1.GetRowCellValue(rowHandle, GridView1.Columns("Purpose"))
            If DeviceType.ToString.Trim = "Bio-1/3/4/5/6/7/ATF-203/ATF-395/ACR-872" Then

            ElseIf DeviceType.ToString.Trim = "ZK(TFT)" Or DeviceType.ToString.Trim = "Bio-1Pro/ATF305Pro/ATF686Pro" Then

            ElseIf DeviceType = "HKSeries" Then

                Dim struDeviceInfoV40 As CHCNetSDK.NET_DVR_DEVICEINFO_V40 = New CHCNetSDK.NET_DVR_DEVICEINFO_V40
                Dim DeviceAdd As String = IP, userName As String = GridView1.GetRowCellValue(RowHandles(0), GridView1.Columns("HLogin")), pwd As String = GridView1.GetRowCellValue(RowHandles(0), GridView1.Columns("HPassword"))
                Dim lUserID As Integer = -1
                Dim failReason As String = ""
                Dim logistatus = cn.HikvisionLogin(DeviceAdd, userName, pwd, struDeviceInfoV40, lUserID, failReason)
                If logistatus = False Then
                    failIP.Add(IP)
                    Continue For
                    'XtraMessageBox.Show(ulf, "<size=10>Connection Failed</size>", "Failed")
                    'Me.Cursor = Cursors.Default
                    'Exit Sub
                Else
                    'serialNo = System.Text.Encoding.UTF8.GetString(struDeviceInfoV40.struDeviceV30.sSerialNumber).TrimEnd(Microsoft.VisualBasic.ChrW(92))
                    If failReason <> "" Then
                        XtraMessageBox.Show(ulf, "<size=10>" & failReason & "</size>", "Failed")
                        XtraMasterTest.LabelControlStatus.Text = ""
                        Application.DoEvents()
                        Me.Cursor = Cursors.Default
                        logistatus = cn.HikvisionLogOut(lUserID)
                        Continue For
                    End If

                    datafile = System.Environment.CurrentDirectory & "\Data\" & Now.ToString("yyyyMMddHHmmss") & ".txt"
                    'IP = GridView1.GetRowCellValue(rowHandle, GridView1.Columns("LOCATION"))
                    IN_OUT = GridView1.GetRowCellValue(rowHandle, GridView1.Columns("IN_OUT"))
                    ID_NO = GridView1.GetRowCellValue(rowHandle, GridView1.Columns("ID_NO"))

                    'listViewEvent.Items.Clear()
                    Dim m_lLogNum = 0
                    Dim struCond As CHCNetSDK.NET_DVR_ACS_EVENT_COND = New CHCNetSDK.NET_DVR_ACS_EVENT_COND()
                    struCond.Init()
                    struCond.dwSize = CUInt(Marshal.SizeOf(struCond))
                    Dim MajorType As String = "Event" 'comboBoxMainType.SelectedItem.ToString()
                    struCond.dwMajor = GetAcsEventType.ReturnMajorTypeValue(MajorType)
                    Dim MinorType As String = "All" '"FINGERPRINT_COMPARE_PASS" 'comboBoxSecondType.SelectedItem.ToString()
                    struCond.dwMinor = GetAcsEventType.ReturnMinorTypeValue(MinorType)

                    struCond.struStartTime.dwYear = DateEditFrmDate.DateTime.Year
                    struCond.struStartTime.dwMonth = DateEditFrmDate.DateTime.Month
                    struCond.struStartTime.dwDay = DateEditFrmDate.DateTime.Day
                    struCond.struStartTime.dwHour = DateEditFrmDate.DateTime.Hour
                    struCond.struStartTime.dwMinute = DateEditFrmDate.DateTime.Minute
                    struCond.struStartTime.dwSecond = DateEditFrmDate.DateTime.Second
                    struCond.struEndTime.dwYear = DateEditToDate.DateTime.Year
                    struCond.struEndTime.dwMonth = DateEditToDate.DateTime.Month
                    struCond.struEndTime.dwDay = DateEditToDate.DateTime.Day
                    struCond.struEndTime.dwHour = DateEditToDate.DateTime.Hour
                    struCond.struEndTime.dwMinute = DateEditToDate.DateTime.Minute
                    struCond.struEndTime.dwSecond = DateEditToDate.DateTime.Second
                    struCond.byPicEnable = 0
                    struCond.szMonitorID = ""
                    struCond.wInductiveEventType = 65535
                    Dim dwSize As UInteger = struCond.dwSize
                    Dim ptrCond As IntPtr = Marshal.AllocHGlobal(CInt(dwSize))
                    Marshal.StructureToPtr(struCond, ptrCond, False)
                    Dim m_lGetAcsEventHandle As Integer = CHCNetSDK.NET_DVR_StartRemoteConfig(lUserID, CHCNetSDK.NET_DVR_GET_ACS_EVENT, ptrCond, CInt(dwSize), Nothing, IntPtr.Zero)

                    If -1 = m_lGetAcsEventHandle Then
                        Marshal.FreeHGlobal(ptrCond)
                        failIP.Add(IP)
                        Continue For
                        'MessageBox.Show("NET_DVR_StartRemoteConfig FAIL, ERROR CODE" & CHCNetSDK.NET_DVR_GetLastError().ToString(), "Error", MessageBoxButtons.OK)
                        'Return
                    End If

                    Dim dwStatus As Integer = 0
                    Dim Flag As Boolean = True
                    Dim struCFG As CHCNetSDK.NET_DVR_ACS_EVENT_CFG = New CHCNetSDK.NET_DVR_ACS_EVENT_CFG()
                    struCFG.dwSize = CUInt(Marshal.SizeOf(struCFG))
                    Dim dwOutBuffSize As Integer = CInt(struCFG.dwSize)
                    struCFG.init()

                    Dim ptr As IntPtr = Marshal.AllocHGlobal(1024)
                    For n As Integer = 0 To 1024 - 1
                        Marshal.WriteByte(ptr, n, 0)
                    Next

                    While Flag
                        dwStatus = CHCNetSDK.NET_DVR_GetNextRemoteConfig(m_lGetAcsEventHandle, ptr, dwOutBuffSize)

                        Dim StructData As CHCNetSDK.NET_DVR_ACS_EVENT_CFG = New CHCNetSDK.NET_DVR_ACS_EVENT_CFG()
                        'Marshal.PtrToStructure(ptr, StructData)
                        '(CHCNetSDK.NET_DVR_ACS_EVENT_CFG)Marshal.PtrToStructure(ptr, typeof(CHCNetSDK.NET_DVR_ACS_EVENT_CFG))
                        StructData = Marshal.PtrToStructure(ptr, GetType(CHCNetSDK.NET_DVR_ACS_EVENT_CFG))
                        Select Case dwStatus
                            Case CHCNetSDK.NET_SDK_GET_NEXT_STATUS_SUCCESS
                                Try
                                    AddAcsEventToList(StructData, IN_OUT, ID_NO, datafile, DeviceAdd)
                                Catch ex As Exception
                                    Flag = False
                                End Try
                            Case CHCNetSDK.NET_SDK_GET_NEXT_STATUS_NEED_WAIT
                                Thread.Sleep(200)
                            Case CHCNetSDK.NET_SDK_GET_NEXT_STATUS_FAILED
                                CHCNetSDK.NET_DVR_StopRemoteConfig(m_lGetAcsEventHandle)
                                'MessageBox.Show("NET_SDK_GET_NEXT_STATUS_FAILED" & CHCNetSDK.NET_DVR_GetLastError().ToString(), "Error", MessageBoxButtons.OK)
                                Flag = False
                            Case CHCNetSDK.NET_SDK_GET_NEXT_STATUS_FINISH
                                CHCNetSDK.NET_DVR_StopRemoteConfig(m_lGetAcsEventHandle)
                                Flag = False
                            Case Else
                                'MessageBox.Show("NET_SDK_GET_NEXT_STATUS_UNKOWN" & CHCNetSDK.NET_DVR_GetLastError().ToString(), "Error", MessageBoxButtons.OK)
                                Flag = False
                                CHCNetSDK.NET_DVR_StopRemoteConfig(m_lGetAcsEventHandle)
                        End Select
                    End While

                    Marshal.FreeHGlobal(ptrCond)
                    logistatus = cn.HikvisionLogOut(lUserID)
                End If
            End If
        Next
        '"Connection fail" ' write logic to show connection fail devices
        XtraMasterTest.LabelControlStatus.Text = ""
        Application.DoEvents()
        SplashScreenManager.CloseForm(False)
        Dim failedIpArr() As String = failIP.ToArray
        Dim failIpStr As String = ""
        If failedIpArr.Length > 0 Then
            For i As Integer = 0 To failedIpArr.Length - 1
                failIpStr = failIpStr & " " & failedIpArr(i)
            Next
            XtraMessageBox.Show(ulf, "<size=10>" & failIpStr & " failed to download data</size>", "Failed")
        Else
            XtraMessageBox.Show(ulf, "<size=10>Download Success</size>", "Success")
        End If
        'Dim msg As New Message("Downloading", "Download Started...")
        'XtraMaster.AlertControl1.Show(XtraMaster, msg.Caption, msg.Text, "", msg.Image, msg)
        XtraMasterTest.LabelControlStatus.Text = ""
        Application.DoEvents()
    End Sub
    'Public Delegate Sub SetTextCallback(ByRef struEventCfg As CHCNetSDK.NET_DVR_ACS_EVENT_CFG)
    Private Sub AddAcsEventToList(ByRef struEventCfg As CHCNetSDK.NET_DVR_ACS_EVENT_CFG, MachineIN_OUT As String, ID_NO As String, datafile As String, DeviceAdd As String)
        'If InvokeRequired = True Then
        '    Dim d As SetTextCallback = New SetTextCallback(AddressOf Me.AddAcsEventToList)
        '    Me.Invoke(d, New Object() {struEventCfg})
        '    'Dim tmp = IN_OUT
        'Else
        'Threading.Thread.MemoryBarrier()
        'Thread.Sleep(20)
        'MsgBox(struEventCfg.struAcsEventInfo.dwSerialNo.ToString)
        Dim cn As Common = New Common
        Dim con1 As OleDbConnection
        Dim con As SqlConnection
        If Common.servername = "Access" Then
            con1 = New OleDbConnection(Common.ConnectionString)
        Else
            con = New SqlConnection(Common.ConnectionString)
        End If

        Dim CARDNO As String
        Dim EMPNO As String
        Dim tmpEmpno As String
        Dim OFFICEPUNCH As String
        Try
            CARDNO = System.Text.Encoding.UTF8.GetString(struEventCfg.struAcsEventInfo.byCardNo).Trim(vbNullChar)
            EMPNO = struEventCfg.struAcsEventInfo.dwEmployeeNo.ToString.Trim(vbNullChar) '
            tmpEmpno = System.Text.Encoding.UTF8.GetString(struEventCfg.struAcsEventInfo.byEmployeeNo).Trim(vbNullChar)
            If tmpEmpno.Trim <> "" And tmpEmpno <> "0" Then
                CARDNO = tmpEmpno.Trim
            End If



            If CARDNO = "" And (EMPNO.Trim = "" Or EMPNO.Trim = "0") Then
                Exit Sub
            ElseIf CARDNO = "" And EMPNO <> "" Then
                CARDNO = EMPNO
            End If
            Dim TempCard As String
            If IsNumeric(CARDNO) Then
                TempCard = CARDNO
                CARDNO = Convert.ToInt64(CARDNO).ToString("000000000000")
            End If

            OFFICEPUNCH = Convert.ToDateTime(cn.GetStrLogTime(struEventCfg.struTime)).ToString("yyyy-MM-dd HH:mm:00")
            XtraMasterTest.LabelControlStatus.Text = "Downloading " & CARDNO & " " & OFFICEPUNCH
            Application.DoEvents()

            'Dim IP As String = struEventCfg.struRemoteHostAddr.sIpV4

            Dim adap As SqlDataAdapter
            Dim adapA As OleDbDataAdapter
            Dim ds As DataSet = New DataSet
            Dim sSql As String


            'byMask 2=> nomask
            'ByIsAbnormalTempreature=>0
            'FcurrTempareyture 
            Dim MaskWear As String = ""
            Try
                If struEventCfg.struAcsEventInfo.byMask = 2 Then
                    MaskWear = "Without Mask"
                ElseIf struEventCfg.struAcsEventInfo.byMask = 3 Then
                    MaskWear = "With Mask"
                ElseIf struEventCfg.struAcsEventInfo.byMask = 1 OrElse struEventCfg.struAcsEventInfo.byMask = 0 Then
                    MaskWear = "Unknow Behavior"
                End If
            Catch ex As Exception

            End Try
            Dim TeperatureStatus As String = ""
            Try
                If struEventCfg.struAcsEventInfo.byIsAbnomalTemperature = 0 Then
                    TeperatureStatus = "No"
                ElseIf struEventCfg.struAcsEventInfo.byIsAbnomalTemperature = 1 Then
                    TeperatureStatus = "Yes"
                End If
            Catch ex As Exception

            End Try


            Dim fCurrTemperature As String = ""
            Try
                fCurrTemperature = struEventCfg.struAcsEventInfo.fCurrTemperature
            Catch ex As Exception

            End Try


            'insert in text file
            Dim fs As FileStream = New FileStream(datafile, FileMode.OpenOrCreate, FileAccess.Write)
            Dim sw As StreamWriter = New StreamWriter(fs)
            'find the end of the underlying filestream
            sw.BaseStream.Seek(0, SeekOrigin.End)
            sw.WriteLine(CARDNO & " " & OFFICEPUNCH & " " & ID_NO)
            sw.Flush()
            sw.Close()
            'end insert in text file


            'sSql = "Select Paycode  from tblEmployee where presentcardno='" & CARDNO & "'"
            'sSql = "select TblEmployee.PAYCODE, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK from TblEmployee, tblEmployeeShiftMaster where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.ACTIVE = 'Y' and TblEmployee.presentcardno = '" & CARDNO & "'"
            sSql = "select TblEmployee.EMPNAME,TblEmployee.PAYCODE, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK, EmployeeGroup.Id from TblEmployee, tblEmployeeShiftMaster, EmployeeGroup where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.ACTIVE = 'Y' and TblEmployee.PRESENTCARDNO = '" & CARDNO & "' and TblEmployee.EmployeeGroupId = EmployeeGroup.GroupId"

            ds = New DataSet
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, con1)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, con)
                adap.Fill(ds)
            End If
            If ds.Tables(0).Rows.Count > 0 Then
                Dim paycode As String = ds.Tables(0).Rows(0)("Paycode").ToString.Trim
                Dim ISROUNDTHECLOCKWORK As String = ds.Tables(0).Rows(0)("ISROUNDTHECLOCKWORK").ToString.Trim
                'MsgBox(serialNo)
                'Dim sSql2 As String = "select * from tblMachine where MAC_ADDRESS ='" & serialNo.Trim & "'"
                'Dim adap1 As SqlDataAdapter
                'Dim adapA1 As OleDbDataAdapter
                'Dim ds3 As DataSet = New DataSet()
                'If Common.servername = "Access" Then
                '    adapA1 = New OleDbDataAdapter(sSql2, con1)
                '    adapA1.Fill(ds3)
                'Else
                '    adap1 = New SqlDataAdapter(sSql2, con)
                '    adap1.Fill(ds3)
                'End If
                Dim IN_OUT As String
                If MachineIN_OUT = "B" Then
                    Dim ds2 As DataSet = New DataSet
                    Dim sSql1 = "select count (CARDNO) from MachineRawPunch where CARDNO = '" & CARDNO & "' and FORMAT(OFFICEPUNCH, 'yyyy-MM-dd') = '" & Convert.ToDateTime(OFFICEPUNCH).ToString("yyyy-MM-dd") & "'"

                    If Common.servername = "Access" Then
                        adapA = New OleDbDataAdapter(sSql1, con1)
                        adapA.Fill(ds2)
                    Else
                        sSql1 = "select count (CARDNO) from MachineRawPunch where CARDNO = '" & CARDNO & "' and convert(varchar, OFFICEPUNCH, 23) = '" & Convert.ToDateTime(OFFICEPUNCH).ToString("yyyy-MM-dd") & "'"
                        adap = New SqlDataAdapter(sSql1, con)
                        adap.Fill(ds2)
                    End If

                    If ds2.Tables(0).Rows(0).Item(0) = 0 Then
                        IN_OUT = "I"
                    Else
                        If ds2.Tables(0).Rows(0).Item(0) Mod 2 = 0 Then
                            IN_OUT = "I"
                        Else
                            IN_OUT = "O"
                        End If
                    End If
                Else
                    IN_OUT = MachineIN_OUT
                End If
                Try

                    Dim del As invokeDelegate = Function()
                                                    XtraMasterTest.LabelControlStatus.Text = "Downloading " & CARDNO & " " & OFFICEPUNCH
                                                End Function
                    Invoke(del)
                    Thread.Sleep(200)

                    Dim ds2 As DataSet = New DataSet
                    If Common.servername = "Access" Then
                        sSql = "select * from MachineRawPunch where CARDNO ='" & CARDNO & "' and  FORMAT(OFFICEPUNCH,'yyyy-MM-dd HH:mm:ss') = '" & OFFICEPUNCH & "' and PAYCODE=''"
                        adapA = New OleDbDataAdapter(sSql, con1)
                        adapA.Fill(ds2)
                    Else
                        sSql = "select * from MachineRawPunch where CARDNO ='" & CARDNO & "' and OFFICEPUNCH = '" & OFFICEPUNCH & "' and PAYCODE=''"
                        adap = New SqlDataAdapter(sSql, con)
                        adap.Fill(ds2)
                    End If
                    Dim ssql1 As String = ""
                    If ds2.Tables(0).Rows.Count > 0 Then
                        If Common.servername = "Access" Then
                            sSql = "update MachineRawPunch set PAYCODE='" & paycode & "' where  CARDNO ='" & CARDNO & "' and FORMAT(OFFICEPUNCH,'yyyy-MM-dd HH:mm:ss') = '" & OFFICEPUNCH & "' and PAYCODE=''"
                            ssql1 = "update MachineRawPunchAll set PAYCODE='" & paycode & "' where  CARDNO ='" & CARDNO & "' and FORMAT(OFFICEPUNCH,'yyyy-MM-dd HH:mm:ss') = '" & OFFICEPUNCH & "' and PAYCODE=''"
                        Else
                            sSql = "update MachineRawPunch set PAYCODE='" & paycode & "' where  CARDNO ='" & CARDNO & "' and OFFICEPUNCH = '" & OFFICEPUNCH & "' and PAYCODE=''"
                            ssql1 = "update MachineRawPunchAll set PAYCODE='" & paycode & "' where  CARDNO ='" & CARDNO & "' and OFFICEPUNCH = '" & OFFICEPUNCH & "' and PAYCODE=''"
                        End If
                    Else
                        sSql = "insert into MachineRawPunch ([CARDNO],[OFFICEPUNCH],[P_DAY],[ISMANUAL],[ReasonCode],[MC_NO],[INOUT],[PAYCODE],[Temperature],[MaskStatus],[IsAbnomal]) values('" & CARDNO & "','" & OFFICEPUNCH & "','N', 'N','','" & ID_NO & "','" & IN_OUT & "','" & paycode & "','" & fCurrTemperature & "','" & MaskWear & "','" & TeperatureStatus & "' ) "
                        ssql1 = "insert into MachineRawPunchAll ([CARDNO],[OFFICEPUNCH],[P_DAY],[ISMANUAL],[ReasonCode],[MC_NO],[INOUT],[PAYCODE],[Temperature],[MaskStatus],[IsAbnomal]) values('" & CARDNO & "','" & OFFICEPUNCH & "','N', 'N','','" & ID_NO & "','" & IN_OUT & "','" & paycode & "','" & fCurrTemperature & "','" & MaskWear & "','" & TeperatureStatus & "' ) "
                    End If
                    If Common.servername = "Access" Then
                        con1.Open()
                        Try
                            cmd1 = New OleDbCommand(sSql, con1)
                            cmd1.ExecuteNonQuery()
                        Catch ex As Exception
                            TraceServiceText(ex.ToString & ";" & sSql)
                        End Try
                        Try
                            cmd1 = New OleDbCommand(ssql1, con1)
                            cmd1.ExecuteNonQuery()
                        Catch ex As Exception
                            TraceServiceText(ex.ToString & ";" & ssql1)
                        End Try

                        con1.Close()
                    Else

                        con.Open()
                        Try
                            cmd = New SqlCommand(sSql, con)
                            cmd.ExecuteNonQuery()
                        Catch ex As Exception
                            TraceServiceText(ex.ToString & ";" & sSql)
                        End Try
                        Try
                            cmd = New SqlCommand(ssql1, con)
                            cmd.ExecuteNonQuery()
                        Catch ex As Exception
                            TraceServiceText(ex.ToString & ";" & ssql1)
                        End Try
                        con.Close()
                    End If
                    Dim pDate As DateTime = Convert.ToDateTime(OFFICEPUNCH)
                    cn.Remove_Duplicate_Punches(pDate, paycode, ds.Tables(0).Rows(0).Item("Id"))
                    If ISROUNDTHECLOCKWORK = "Y" Then
                        If Common.PrcessMode = "M" Then
                            cn.Process_AllnonRTCINOUT(pDate.AddDays(-1), Now.Date, paycode, paycode, ds.Tables(0).Rows(0).Item("Id"), ISROUNDTHECLOCKWORK)
                        Else
                            cn.Process_AllRTC(pDate.AddDays(-1), Now.Date, paycode, paycode, ds.Tables(0).Rows(0).Item("Id"))
                        End If
                        'cn.Process_AllRTC(pDate.AddDays(-1), Now.Date, paycode, paycode, ds.Tables(0).Rows(0).Item("Id"))
                    Else
                        If Common.PrcessMode = "M" Then
                            cn.Process_AllnonRTCINOUT(pDate, Now.Date, paycode, paycode, ds.Tables(0).Rows(0).Item("Id"), ISROUNDTHECLOCKWORK)
                        Else
                            cn.Process_AllnonRTC(pDate, Now.Date, paycode, paycode, ds.Tables(0).Rows(0).Item("Id"))
                        End If
                        'cn.Process_AllnonRTC(pDate, Now.Date, paycode, paycode, ds.Tables(0).Rows(0).Item("Id"))
                    End If

                    If Common.IsParallel = "Y" Then
                        Common.parallelInsert(TempCard, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, OFFICEPUNCH, IN_OUT, "N", ds.Tables(0).Rows(0).Item("EMPNAME").ToString.Trim, ID_NO, "0")
                        'Common.parallelInsertVisinEdge(TempCard, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, OFFICEPUNCH, IN_OUT, "N", ds.Tables(0).Rows(0).Item("EMPNAME").ToString.Trim, ID_NO, "0", MaskWear)
                    End If

                    'sms
                    If Common.g_SMSApplicable = "" Then
                        Common.Load_SMS_Policy()
                    End If
                    If Common.g_SMSApplicable = "Y" Then
                        If Common.g_isAllSMS = "Y" Then
                            Try
                                If ds.Tables(0).Rows(0).Item("TELEPHONE1").ToString.Trim <> "" Then
                                    Dim SmsContent As String = Common.g_AllContent1 & " " & OFFICEPUNCH & " " & Common.g_AllContent2
                                    cn.sendSMS(ds.Tables(0).Rows(0).Item("TELEPHONE1").ToString.Trim, SmsContent)
                                End If
                            Catch ex As Exception

                            End Try
                        End If
                    End If
                    'sms end
                Catch ex As Exception
                    'MsgBox(ex.Message & vbCrLf & CARDNO & vbCrLf & OFFICEPUNCH)
                    TraceServiceText(ex.ToString & ";" & sSql)
                End Try
            End If

        Catch ex As Exception
            'MsgBox(ex.Message & vbCrLf & CARDNO & vbCrLf & OFFICEPUNCH)
        End Try
        'XtraMasterTest.LabelControlStatus.Text = ""
        'Application.DoEvents()

        ''Me.listViewEvent.BeginUpdate()
        'Dim Item As ListViewItem = New ListViewItem
        ''Item.Text = (m_lLogNum + 1).ToString
        'Dim LogTime As String = cn.GetStrLogTime(struEventCfg.struTime)
        'Item.SubItems.Add(LogTime)
        'Dim Major As String = cn.ProcessMajorType(struEventCfg.dwMajor)
        'Item.SubItems.Add(Major)
        'CsTemp = cn.ProcessMinorType(struEventCfg)
        'Item.SubItems.Add(CsTemp)

        'CsTemp = cn.CardTypeMap(struEventCfg)
        'Item.SubItems.Add(CsTemp)
        'Item.SubItems.Add(struEventCfg.struAcsEventInfo.byWhiteListNo.ToString)
        ''WhiteList
        'CsTemp = cn.ProcessReportChannel(struEventCfg)
        'Item.SubItems.Add(CsTemp)
        'CsTemp = cn.ProcessCardReader(struEventCfg)
        'Item.SubItems.Add(CsTemp)
        'CsTemp = struEventCfg.struAcsEventInfo.dwCardReaderNo.ToString
        'Item.SubItems.Add(CsTemp)
        'Item.SubItems.Add(struEventCfg.struAcsEventInfo.dwDoorNo.ToString)
        'Item.SubItems.Add(struEventCfg.struAcsEventInfo.dwVerifyNo.ToString)
        'Item.SubItems.Add(struEventCfg.struAcsEventInfo.dwAlarmInNo.ToString)
        'Item.SubItems.Add(struEventCfg.struAcsEventInfo.dwAlarmOutNo.ToString)
        'Item.SubItems.Add(struEventCfg.struAcsEventInfo.dwCaseSensorNo.ToString)
        'Item.SubItems.Add(struEventCfg.struAcsEventInfo.dwRs485No.ToString)
        'Item.SubItems.Add(struEventCfg.struAcsEventInfo.dwMultiCardGroupNo.ToString)
        'Item.SubItems.Add(struEventCfg.struAcsEventInfo.wAccessChannel.ToString)
        'Item.SubItems.Add(struEventCfg.struAcsEventInfo.byDeviceNo.ToString)
        'Item.SubItems.Add(struEventCfg.struAcsEventInfo.dwEmployeeNo.ToString)
        'Item.SubItems.Add(struEventCfg.struAcsEventInfo.byDistractControlNo.ToString)
        'Item.SubItems.Add(struEventCfg.struAcsEventInfo.wLocalControllerID.ToString)
        'CsTemp = cn.ProcessInternatAccess(struEventCfg)
        'Item.SubItems.Add(CsTemp)
        'CsTemp = cn.ProcessByType(struEventCfg)
        'Item.SubItems.Add(CsTemp)
        'CsTemp = cn.ProcessMacAdd(struEventCfg)
        'Item.SubItems.Add(CsTemp)
        'CsTemp = cn.ProcessSwipeCard(struEventCfg)
        'Item.SubItems.Add(CsTemp)
        'Item.SubItems.Add(struEventCfg.struAcsEventInfo.dwSerialNo.ToString)
        'Item.SubItems.Add(struEventCfg.struAcsEventInfo.byChannelControllerID.ToString)
        'Item.SubItems.Add(struEventCfg.struAcsEventInfo.byChannelControllerLampID.ToString)
        'Item.SubItems.Add(struEventCfg.struAcsEventInfo.byChannelControllerIRAdaptorID.ToString)
        'Item.SubItems.Add(struEventCfg.struAcsEventInfo.byChannelControllerIREmitterID.ToString)
        'If (struEventCfg.wInductiveEventType < CType(GetAcsEventType.NumOfInductiveEvent, System.UInt16)) Then
        '    Item.SubItems.Add(GetAcsEventType.FindKeyOfInductive(struEventCfg.wInductiveEventType))
        'Else
        '    Item.SubItems.Add("Invalid")
        'End If

        'Item.SubItems.Add("0")
        ''RecordChannelNum
        'CsTemp = cn.ProcessbyUserType(struEventCfg)
        'Item.SubItems.Add(CsTemp)
        'CsTemp = cn.ProcessVerifyMode(struEventCfg)
        'Item.SubItems.Add(CsTemp)
        'CsTemp = System.Text.Encoding.UTF8.GetString(struEventCfg.struAcsEventInfo.byEmployeeNo)
        'Item.SubItems.Add(CsTemp)
        'CsTemp = Nothing
        'If Item.SubItems.Item(4).Text.Trim <> "" Then
        '    Me.listViewEvent.Items.Add(Item)
        'End If
        ''Me.listViewEvent.Items.Add(Item)
        'Me.listViewEvent.EndUpdate()
        'cn.ProcessPicData(struEventCfg)
        'End If
    End Sub
    Private Sub CheckEditTimePerioad_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckEditTimePerioad.CheckedChanged
        If Common.IsNepali = "Y" Then
            If CheckEditTimePerioad.Checked = True Then
                ComboNYearStr.Enabled = True
                ComboNMonthStr.Enabled = True
                ComboNDateStr.Enabled = True
                ComboNYearEnd.Enabled = True
                ComboNMonthEnd.Enabled = True
                ComboNDateEnd.Enabled = True
                LabelControl4.Enabled = True
                LabelControl5.Enabled = True
            Else
                ComboNYearStr.Enabled = False
                ComboNMonthStr.Enabled = False
                ComboNDateStr.Enabled = False
                ComboNYearEnd.Enabled = False
                ComboNMonthEnd.Enabled = False
                ComboNDateEnd.Enabled = False
                LabelControl4.Enabled = False
                LabelControl5.Enabled = False
            End If
        Else
            If CheckEditTimePerioad.Checked = True Then
                DateEditFrmDate.Enabled = True
                DateEditToDate.Enabled = True
                LabelControl4.Enabled = True
                LabelControl5.Enabled = True
            Else
                DateEditFrmDate.Enabled = False
                DateEditToDate.Enabled = False
                LabelControl4.Enabled = False
                LabelControl5.Enabled = False
            End If
        End If

    End Sub
    Public Sub TraceServiceText(ByVal errmessage As String)
        Try
            Dim fs As FileStream = New FileStream("String_" & Now.ToString("yyyyMMdd") & ".txt", FileMode.OpenOrCreate, FileAccess.Write)
            Dim sw As StreamWriter = New StreamWriter(fs)
            sw.BaseStream.Seek(0, SeekOrigin.[End])
            sw.WriteLine(vbLf & errmessage.Trim(vbNullChar) & " " & Now.ToString("yyyy-MM-dd HH:mm:ss"))
            sw.Flush()
            sw.Close()
        Catch
        End Try
    End Sub
End Class