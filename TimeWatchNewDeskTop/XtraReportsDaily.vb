﻿Imports System.Resources
Imports System.Globalization
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports System.ComponentModel
Imports DevExpress.XtraGrid.Columns
Imports System.Data.SqlClient
Imports System.Text
Imports System.Data.OleDb
Imports DevExpress.XtraSplashScreen
Imports System.Runtime.CompilerServices
Imports Microsoft.Office.Interop
Imports Microsoft.Office.Interop.Excel

Public Class XtraReportsDaily
    Dim ulf As UserLookAndFeel
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    Dim WhichReport As Integer
    Dim g_WhereClause As String
    Dim g_SkipAfterDept As Boolean
    Dim g_LinesPerPage As Integer
    Dim mStrProcName As String
    Dim mstrDepartmentCode As String
    Dim mintPageNo As Integer
    Dim mintLine As Integer
    Dim mFileNumber As Integer
    Dim mblnCheckReport As Boolean
    Dim mlngStartingNoOfDays As Long
    Dim mstrFile_Name As String
    Dim mShift As String
    Dim g_MachineRawPunch As String = "admin"
    Dim isPdf As Boolean
    Dim FromDateTodate As String
    Dim ForDate As String
    Public Shared InOutPdf As Boolean = False

    Public Sub New()
        InitializeComponent()
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True
    End Sub
    Private Sub XtraReportsDaily_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Me.Width = Common.NavWidth 'Me.Parent.Width
        Me.Height = Common.NavHeight 'Me.Parent.Height
        SplitContainerControl1.Width = SplitContainerControl1.Parent.Width
        SplitContainerControl1.SplitterPosition = (SplitContainerControl1.Parent.Width) * 85 / 100

        Common.splitforMasterMenuWidth = SplitContainerControl1.Width
        Common.SplitterPosition = SplitContainerControl1.SplitterPosition

        DateEdit1.DateTime = Now
        DateEdit2.EditValue = Now

        If Common.IsNepali = "Y" Then
            ComboNepaliYearFrm.Visible = True
            ComboNEpaliMonthFrm.Visible = True
            ComboNepaliDateFrm.Visible = True
            ComboNepaliYearTo.Visible = False 'True
            ComboNEpaliMonthTo.Visible = False 'True
            ComboNepaliDateTo.Visible = False 'True

            DateEdit1.Visible = False
            DateEdit2.Visible = False

            Dim DC As New DateConverter()

            Dim doj As String = DC.ToBS(New Date(DateEdit1.DateTime.Year, DateEdit1.DateTime.Month, DateEdit1.DateTime.Day))
            Dim dojTmp() As String = doj.Split("-")
            ComboNepaliYearFrm.EditValue = dojTmp(0)
            ComboNEpaliMonthFrm.SelectedIndex = dojTmp(1) - 1
            ComboNepaliDateFrm.EditValue = dojTmp(2)

            doj = DC.ToBS(New Date(DateEdit2.DateTime.Year, DateEdit2.DateTime.Month, DateEdit2.DateTime.Day))
            dojTmp = doj.Split("-")
            ComboNepaliYearTo.EditValue = dojTmp(0)
            ComboNEpaliMonthTo.SelectedIndex = dojTmp(1) - 1
            ComboNepaliDateTo.EditValue = dojTmp(2)

            ''original
            'Dim doj As DateTime = DC.ToBS(New Date(DateEdit1.DateTime.Year, DateEdit1.DateTime.Month, DateEdit1.DateTime.Day))
            'ComboNepaliYearFrm.EditValue = doj.Year
            'ComboNEpaliMonthFrm.SelectedIndex = doj.Month - 1
            'ComboNepaliDateFrm.EditValue = doj.Day

            'doj = DC.ToBS(New Date(DateEdit2.DateTime.Year, DateEdit2.DateTime.Month, DateEdit2.DateTime.Day))
            'ComboNepaliYearTo.EditValue = doj.Year
            'ComboNEpaliMonthTo.SelectedIndex = doj.Month - 1
            'ComboNepaliDateTo.EditValue = doj.Day
        Else
            ComboNepaliYearFrm.Visible = False
            ComboNEpaliMonthFrm.Visible = False
            ComboNepaliDateFrm.Visible = False
            ComboNepaliYearTo.Visible = False
            ComboNEpaliMonthTo.Visible = False
            ComboNepaliDateTo.Visible = False
            DateEdit1.Visible = True
            DateEdit2.Visible = False 'True
        End If
        If Common.servername = "Access" Then
            'Me.TblCompany1TableAdapter1.Fill(Me.SSSDBDataSet.tblCompany1)
            'GridControlComp.DataSource = SSSDBDataSet.tblCompany1

            'Me.TblEmployee1TableAdapter1.Fill(Me.SSSDBDataSet.TblEmployee1)
            'GridControlEmp.DataSource = SSSDBDataSet.TblEmployee1

            Me.TblDepartment1TableAdapter1.Fill(Me.SSSDBDataSet.tblDepartment1)
            GridControlDept.DataSource = SSSDBDataSet.tblDepartment1

            Me.TblCatagory1TableAdapter1.Fill(Me.SSSDBDataSet.tblCatagory1)
            GridControlCat.DataSource = SSSDBDataSet.tblCatagory1

            Me.TblShiftMaster1TableAdapter1.Fill(Me.SSSDBDataSet.tblShiftMaster1)
            GridControlShift.DataSource = SSSDBDataSet.tblShiftMaster1

            Me.TblGrade1TableAdapter1.Fill(Me.SSSDBDataSet.tblGrade1)
            GridControlGrade.DataSource = SSSDBDataSet.tblGrade1

            'Me.Tblbranch1TableAdapter1.Fill(Me.SSSDBDataSet.tblbranch1)
            'GridControlBranch.DataSource = SSSDBDataSet.tblbranch1

            'Me.TblMachine1TableAdapter1.Fill(Me.SSSDBDataSet.tblMachine1)
            'GridControlDevice.DataSource = SSSDBDataSet.tblMachine1
        Else
            'TblCompanyTableAdapter.Connection.ConnectionString = Common.ConnectionString
            'Me.TblCompanyTableAdapter.Fill(Me.SSSDBDataSet.tblCompany)
            'GridControlComp.DataSource = SSSDBDataSet.tblCompany

            'TblEmployeeTableAdapter.Connection.ConnectionString = Common.ConnectionString
            'Me.TblEmployeeTableAdapter.Fill(Me.SSSDBDataSet.TblEmployee)
            'GridControlEmp.DataSource = SSSDBDataSet.TblEmployee

            TblDepartmentTableAdapter.Connection.ConnectionString = Common.ConnectionString
            Me.TblDepartmentTableAdapter.Fill(Me.SSSDBDataSet.tblDepartment)
            GridControlDept.DataSource = SSSDBDataSet.tblDepartment

            TblCatagoryTableAdapter.Connection.ConnectionString = Common.ConnectionString
            Me.TblCatagoryTableAdapter.Fill(Me.SSSDBDataSet.tblCatagory)
            GridControlCat.DataSource = SSSDBDataSet.tblCatagory

            TblShiftMasterTableAdapter.Connection.ConnectionString = Common.ConnectionString
            Me.TblShiftMasterTableAdapter.Fill(Me.SSSDBDataSet.tblShiftMaster)
            GridControlShift.DataSource = SSSDBDataSet.tblShiftMaster

            TblGradeTableAdapter.Connection.ConnectionString = Common.ConnectionString
            Me.TblGradeTableAdapter.Fill(Me.SSSDBDataSet.tblGrade)
            GridControlGrade.DataSource = SSSDBDataSet.tblGrade

            'TblbranchTableAdapter.Connection.ConnectionString = Common.ConnectionString
            'Me.TblbranchTableAdapter.Fill(Me.SSSDBDataSet.tblbranch)
            'GridControlBranch.DataSource = SSSDBDataSet.tblbranch

            'TblMachineTableAdapter1.Connection.ConnectionString = Common.ConnectionString
            'Me.TblMachineTableAdapter1.Fill(Me.SSSDBDataSet.tblMachine)
            'GridControlDevice.DataSource = SSSDBDataSet.tblMachine
        End If
        GridControlEmp.DataSource = Common.EmpNonAdmin
        GridControlBranch.DataSource = Common.LocationNonAdmin
        GridControlDevice.DataSource = Common.MachineNonAdmin
        GridControlComp.DataSource = Common.CompanyNonAdmin

        TextFromTime.Visible = False
        TextToTime.Visible = False
        LabelControl12.Visible = False
        LabelControl13.Visible = False
        PopupContainerEditDevice.Visible = False

        Common.SetGridFont(GridViewComp, New System.Drawing.Font("Tahoma", 10))
        Common.SetGridFont(GridViewEmp, New System.Drawing.Font("Tahoma", 10))
        Common.SetGridFont(GridViewCat, New System.Drawing.Font("Tahoma", 10))
        Common.SetGridFont(GridViewDept, New System.Drawing.Font("Tahoma", 10))
        Common.SetGridFont(GridViewGrade, New System.Drawing.Font("Tahoma", 10))
        Common.SetGridFont(GridViewShift, New System.Drawing.Font("Tahoma", 10))
        Common.SetGridFont(GridViewBranch, New System.Drawing.Font("Tahoma", 10))
        Common.SetGridFont(GridViewDevice, New System.Drawing.Font("Tahoma", 10))

        If Common.IsCompliance Then
            For Each c As Control In PanelControl1.Controls
                c.Visible = False
            Next
            CheckDailyPerformance.Visible = True
            CheckAttendance.Visible = True
            CheckLateArr.Visible = True
            CheckPresentReport.Visible = True
            CheckOutWork.Visible = True
            CheckAbsenteeism.Visible = True
            CheckShiftWisePresence.Visible = True
            CheckEarlyArrival.Visible = True
            CheckEarlyDeparture.Visible = True
        End If
    End Sub
    Private Sub SimpleButton1_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton1.Click
        If Common.IsNepali = "Y" Then

            If ComboNEpaliMonthFrm.EditValue.ToString.Trim = "Magh" And ComboNepaliDateFrm.EditValue > 29 Then
                XtraMessageBox.Show(ulf, "<size=10>Invalid From Date</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                ComboNepaliDateFrm.Select()
                Exit Sub
            End If

            If ComboNEpaliMonthTo.EditValue.ToString.Trim = "Magh" And ComboNepaliDateTo.EditValue > 29 Then
                XtraMessageBox.Show(ulf, "<size=10>Invalid To Date</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                ComboNepaliDateTo.Select()
                Exit Sub
            End If

            Dim DC As New DateConverter()
            Dim tmpNow As String = DC.ToBS(New Date(Now.Year, Now.Month, Now.Day))
            Dim dojTmp() As String = tmpNow.Split("-")
            Common.runningDateTime = dojTmp(2) & "/" & Common.NepaliMonth(dojTmp(1) - 1) & "/" & dojTmp(0) & " " & Now.ToString("HH:mm")
            FromDateTodate = ComboNepaliDateFrm.EditValue & "/" & ComboNEpaliMonthFrm.EditValue.ToString.Trim & "/" & ComboNepaliYearFrm.EditValue & " To " & ComboNepaliDateTo.EditValue & "/" & ComboNEpaliMonthTo.EditValue.ToString.Trim & "/" & ComboNepaliYearTo.EditValue
            ForDate = ComboNepaliDateFrm.EditValue & "/" & ComboNEpaliMonthFrm.EditValue.ToString.Trim & "/" & ComboNepaliYearFrm.EditValue
            Try
                'DateEdit1.DateTime = DC.ToAD(New Date(ComboNepaliYearFrm.EditValue, ComboNEpaliMonthFrm.SelectedIndex + 1, ComboNepaliDateFrm.EditValue))
                DateEdit1.DateTime = DC.ToAD(ComboNepaliYearFrm.EditValue & "-" & ComboNEpaliMonthFrm.SelectedIndex + 1 & "-" & ComboNepaliDateFrm.EditValue)
            Catch ex As Exception
                XtraMessageBox.Show(ulf, "<size=10>Invalid From Date</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                ComboNepaliDateFrm.Select()
                Exit Sub
            End Try

            Try
                'DateEdit2.DateTime = DC.ToAD(New Date(ComboNepaliYearTo.EditValue, ComboNEpaliMonthTo.SelectedIndex + 1, ComboNepaliDateTo.EditValue))
                DateEdit2.DateTime = DC.ToAD(ComboNepaliYearTo.EditValue & "-" & ComboNEpaliMonthTo.SelectedIndex + 1 & "-" & ComboNepaliDateTo.EditValue)
            Catch ex As Exception
                XtraMessageBox.Show(ulf, "<size=10>Invalid To Date</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                ComboNepaliDateTo.Select()
                Exit Sub
            End Try
        Else
            Common.runningDateTime = Now.ToString("dd/MM/yyyy HH:mm")
            FromDateTodate = DateEdit1.DateTime.ToString("dd/MM/yyyy") & " To " & DateEdit2.DateTime.ToString("dd/MM/yyyy")
            ForDate = DateEdit1.DateTime.ToString("dd/MM/yyyy")
        End If

        If license.TrialPerioad And DateEdit1.DateTime.ToString("yyyy-MM-dd") < Convert.ToDateTime(Common.InstalledDate).ToString("yyyy-MM-dd") Then
            XtraMessageBox.Show(ulf, "<size=10>Cannot View Older than Installed Date Report in Trial Version</size>", "<size=9>iAS</size>")
            DateEdit1.Select()
            Exit Sub
        End If

        g_LinesPerPage = TextEdit1.Text
        If CheckDeptSkip.Checked = True Then
            g_SkipAfterDept = True
        Else
            g_SkipAfterDept = False
        End If
        If SidePanelSelection.Visible = False Then
            g_WhereClause = ""
        End If
        Set_Filter_Crystal()

        'If Trim(g_WhereClause) = "" Then
        '    g_WhereClause = gcompany & " and " & gdepartment
        'Else
        '    g_WhereClause = g_WhereClause & "and " & gcompany & " and " & gdepartment
        'End If
        If CheckLateArr.Checked = True Then
            If CheckText.Checked = True Then
                isPdf = False
                Spot_LateArrival("")
                mStrProcName = "Spot_LatzeArrival"
            ElseIf CheckExcel.Checked = True Then
                'Common.isPDF = False
                'SpotXl_LateArrival("")
                SpotXl_LateArrivalGrid("")
                mStrProcName = "SpotXl_LateArrival"
                mblnCheckReport = False
                'mReportPrintStatus = False
            ElseIf CheckPDF.Checked = True Then
                isPdf = True
                Spot_LateArrival("")
                mblnCheckReport = False
            End If
        ElseIf CheckAbsenteeism.Checked = True Then
            'if text
            If CheckText.Checked = True Then
                isPdf = False
                Spot_Absenteeism("")
                mStrProcName = "Spot_Absenteeism"
            ElseIf CheckExcel.Checked = True Then
                'SpotXl_Absenteeism("")
                SpotXl_AbsenteeismGrid("")
                mStrProcName = "SpotXl_Absenteeism"
                mblnCheckReport = False
                'mReportPrintStatus = False
            ElseIf CheckPDF.Checked = True Then
                isPdf = True
                Spot_Absenteeism("")
                mblnCheckReport = False
            End If
        ElseIf CheckAttendance.Checked = True Then
            If CheckText.Checked = True Then
                isPdf = False
                Spot_Attendance("")
                mStrProcName = "Spot_Attendance"
            ElseIf CheckExcel.Checked = True Then
                'SpotXl_Attendance("")
                SpotXl_AttendanceGrid("")
                mStrProcName = "SpotXl_Attendance"
                mblnCheckReport = False
                'mReportPrintStatus = False
            ElseIf CheckPDF.Checked = True Then
                isPdf = True
                Spot_Attendance("")
                mblnCheckReport = False
            End If
        ElseIf CheckDepartmentSummary.Checked = True Then
            If CheckText.Checked = True Then
                isPdf = False
                Spot_DepartmentSummary()
            ElseIf CheckExcel.Checked = True Then
                'SpotXl_DepartmentSummary()
                SpotXl_DepartmentSummaryGrid()
                mblnCheckReport = False
                'mReportPrintStatus = False
            ElseIf CheckPDF.Checked = True Then
                isPdf = True
                Spot_DepartmentSummary()
                mblnCheckReport = False
            End If
        ElseIf CheckEarlyArrival.Checked = True Then
            If CheckText.Checked = True Then
                isPdf = False
                Spot_EarlyArrival("")
                mStrProcName = "Spot_EarlyArrival"
            ElseIf CheckExcel.Checked = True Then
                'SpotXl_EarlyArrival("")
                SpotXl_EarlyArrivalGrid("")
                mStrProcName = "SpotXl_EarlyArrival"
                mblnCheckReport = False
                'mReportPrintStatus = False
            ElseIf CheckPDF.Checked = True Then
                isPdf = True
                Spot_EarlyArrival("")
                mblnCheckReport = False
            End If
        ElseIf CheckShiftWisePresence.Checked = True Then
            If CheckText.Checked = True Then
                isPdf = False
                Spot_ShiftWisePresence("")
                mStrProcName = "Spot_ShiftWisePresence"
            ElseIf CheckExcel.Checked = True Then
                'SpotXl_ShiftWisePresence("")
                SpotXl_ShiftWisePresenceGrid("")
                mStrProcName = "SpotXl_ShiftWisePresence"
                mblnCheckReport = False
            ElseIf CheckPDF.Checked = True Then
                isPdf = True
                Spot_ShiftWisePresence("")
                mblnCheckReport = False
            End If
        ElseIf CheckMachineRawPunch.Checked = True Then
            If CheckText.Checked = True Then
                isPdf = False
                Spot_MachineRawPunchData("")
                mStrProcName = "Spot_MachineRawPunchData"
            ElseIf CheckExcel.Checked = True Then
                'SpotXl_MachineRawPunchData("")
                'SpotXl_MachineRawPunchDataGrid("")
                SpotXl_MachineRawPunchDataGridNew("")
                mStrProcName = "SpotXl_MachineRawPunchData"
                mblnCheckReport = False
                'mReportPrintStatus = False
            ElseIf CheckPDF.Checked = True Then
                isPdf = True
                Spot_MachineRawPunchData("")
                mblnCheckReport = False
            End If
        ElseIf CheckMachineRawPunchAll.Checked = True Then
            If CheckText.Checked = True Then
                isPdf = False
                Spot_MachineRawPunchData("All")
                mStrProcName = "Spot_MachineRawPunchData"
            ElseIf CheckExcel.Checked = True Then
                'SpotXl_MachineRawPunchData("")
                'SpotXl_MachineRawPunchDataGrid("")
                SpotXl_MachineRawPunchDataGridNew("All")
                mStrProcName = "SpotXl_MachineRawPunchData"
                mblnCheckReport = False
                'mReportPrintStatus = False
            ElseIf CheckPDF.Checked = True Then
                isPdf = True
                Spot_MachineRawPunchData("All")
                mblnCheckReport = False
            End If
        ElseIf CheckManualPunchAudit.Checked = True Then
            isPdf = False
            Spot_ManualPunchAuditData("")
            mStrProcName = "Spot_ManualPunchAuditData"
        ElseIf CheckPresentReport.Checked = True Then
            If CheckText.Checked = True Then
                isPdf = False
                Spot_Present("")
                mStrProcName = "Spot_Present"
            ElseIf CheckExcel.Checked = True Then
                'SpotXl_Present("")
                SpotXl_PresentGrid("")
                mStrProcName = "SpotXl_Present"
                mblnCheckReport = False
                'mReportPrintStatus = False
            ElseIf CheckPDF.Checked = True Then
                isPdf = True
                Spot_Present("")
                mblnCheckReport = False
            End If
        ElseIf CheckDailyPerformance.Checked = True Then
            If CheckText.Checked = True Then
                isPdf = False
                Daily_Performance("")
                mStrProcName = "Daily_Performance"
            ElseIf CheckExcel.Checked = True Then
                'DailyXl_Performance("")
                DailyXl_PerformanceGrid("")
                mStrProcName = "DailyXl_Performance"
                mblnCheckReport = False
                'mReportPrintStatus = False
            ElseIf CheckPDF.Checked = True Then
                isPdf = True
                Daily_Performance("")
                mblnCheckReport = False
            End If
        ElseIf CheckEarlyDeparture.Checked = True Then
            If CheckText.Checked = True Then
                isPdf = False
                Daily_EarlyDeparture("")
                mStrProcName = "Daily_EarlyDeparture"
            ElseIf CheckExcel.Checked = True Then
                'DailyXl_EarlyDeparture("")
                DailyXl_EarlyDepartureGrid("")
                mStrProcName = "DailyXl_EarlyDeparture"
                mblnCheckReport = False
                'mReportPrintStatus = False
            ElseIf CheckPDF.Checked = True Then
                isPdf = True
                Daily_EarlyDeparture("")
                mblnCheckReport = False
            End If
        ElseIf CheckTimeLoss.Checked = True Then
            If CheckText.Checked = True Then
                isPdf = False
                Daily_TimeLoss("")
                mStrProcName = "Daily_TimeLoss"
            ElseIf CheckExcel.Checked = True Then
                'DailyXl_TimeLoss("")
                DailyXl_TimeLossGrid("")
                mStrProcName = "DailyXl_TimeLoss"
                mblnCheckReport = False
                'mReportPrintStatus = False
            ElseIf CheckPDF.Checked = True Then
                isPdf = True
                Daily_TimeLoss("")
                mblnCheckReport = False
            End If
        ElseIf CheckOverTime.Checked = True Then
            If CheckText.Checked = True Then
                isPdf = False
                Daily_OverTimeRegister("")
                mStrProcName = "Daily_OverTimeRegister"
            ElseIf CheckExcel.Checked = True Then
                'DailyXl_OverTimeRegister("")
                DailyXl_OverTimeRegisterGrid("")
                mStrProcName = "DailyXl_OverTimeRegister"
                mblnCheckReport = False
                'mReportPrintStatus = False
            ElseIf CheckPDF.Checked = True Then
                isPdf = True
                Daily_OverTimeRegister("")
                mblnCheckReport = False
            End If
        ElseIf CheckOverTimeSummary.Checked = True Then
            If CheckText.Checked = True Then
                isPdf = False
                Daily_OverTimeSummary()
            ElseIf CheckExcel.Checked = True Then
                'DailyXl_OverTimeSummary()
                DailyXl_OverTimeSummaryGrid()
                mblnCheckReport = False
                'mReportPrintStatus = False
            ElseIf CheckPDF.Checked = True Then
                isPdf = True
                Daily_OverTimeSummary()
                mblnCheckReport = False
            End If
        ElseIf CheckShiftChangeStatement.Checked = True Then
            If CheckText.Checked = True Then
                isPdf = False
                Daily_ShiftChangeStatement("")
                mStrProcName = "Daily_ShiftChangeStatement"
            ElseIf CheckExcel.Checked = True Then
                'DailyXl_ShiftChangeStatement("")
                DailyXl_ShiftChangeStatementGrid("")
                mStrProcName = "DailyXl_ShiftChangeStatement"
                mblnCheckReport = False
                'mReportPrintStatus = False
            ElseIf CheckPDF.Checked = True Then
                isPdf = True
                Daily_ShiftChangeStatement("")
                mblnCheckReport = False
            End If
        ElseIf CheckOutWork.Checked = True Then
            If CheckText.Checked = True Then
                isPdf = False
                Daily_OutWorkReport("")
                mStrProcName = "Daily_OutWorkReport"
            ElseIf CheckExcel.Checked = True Then
                'DailyXl_OutWorkReport("")
                DailyXl_OutWorkReportGrid("")
                mStrProcName = "DailyXl_OutWorkReport"
                mblnCheckReport = False
                'mReportPrintStatus = False
            ElseIf CheckPDF.Checked = True Then
                isPdf = True
                Daily_OutWorkReport("")
                mblnCheckReport = False
            End If
        ElseIf CheckMissingAndReverificaton.Checked = True Then
            If CheckText.Checked = True Then
                isPdf = False
                Daily_MissingPunchOrIncorrectShiftOrReverification("")
                mStrProcName = "Daily_MissingPunchOrIncorrectShiftOrReverification"
            ElseIf CheckExcel.Checked = True Then
                'DailyXl_MissingPunchOrIncorrectShiftOrReverification("")
                DailyXl_MissingPunchOrIncorrectShiftOrReverificationGrid("")
                mStrProcName = "DailyXl_MissingPunchOrIncorrectShiftOrReverification"
                mblnCheckReport = False
                'mReportPrintStatus = False
            ElseIf CheckPDF.Checked = True Then
                isPdf = True
                Daily_MissingPunchOrIncorrectShiftOrReverification("")
                mblnCheckReport = False
            End If
        ElseIf CheckInOut.Checked = True Then
            If CheckText.Checked = True Then
            ElseIf CheckExcel.Checked = True Then
                Daily_InOutGrid("")
                'Daily_InOutGrid_InnoliaEnergy("")
            ElseIf CheckPDF.Checked = True Then
                Daily_InOutGrid("")
                'Daily_InOutGrid_InnoliaEnergy("")
            End If
        ElseIf CheckContinuousLateArrival.Checked = True Then
            isPdf = False
            'Daily_ContinuousLateArrival("N", "")
            mStrProcName = "Daily_ContinuousLateArrival"
        ElseIf CheckDeviceWise.Checked = True Then
            SpotXl_DeviceWiseGrid("")
        ElseIf CheckPhotoReport.Checked = True Then
            DailyXl_Performance_Image("")
        ElseIf CheckPunchImage.Checked = True Then
            DailyXl_MachineRawPunchImage("")
        ElseIf CheckCustomized.Checked = True Then
            'DailyXL_CustomisedGrid()
            DailyXL_CustomisedGridNava()
        ElseIf CheckMultiShift.Checked = True Then
            DailyXL_MultiShiftGrid("")
        ElseIf CheckEditTemperature.Checked = True Then
            SpotXl_Temperature("")
        ElseIf CheckEditVReport.Checked Then
            'DailyXl_MachineRawPunchImageVisitor("")
            DailyXl_MachineRawPunchImageVisitorGrid("")
        End If
    End Sub
    Private Sub selectionChange()
        If CheckContinuousAbsenteeism.Checked = True Or CheckContinuousEarlyDeparture.Checked = True Or CheckContinuousLateArrival.Checked = True Then
            LabelControl3.Visible = True
            DateEdit2.Visible = True
            DateEdit2.DateTime = Now
            Exit Sub
        Else
            LabelControl3.Visible = False
            DateEdit2.Visible = False
            Exit Sub
        End If

        If CheckContinuousAbsenteeism.Checked Or CheckContinuousEarlyDeparture.Checked Or CheckContinuousLateArrival.Checked Then
            LabelControl3.Visible = True
            DateEdit2.Visible = True
            DateEdit2.DateTime = Now
            LabelControl11.Visible = True
            TextEdit2.Visible = True
            Exit Sub
        Else
            LabelControl3.Visible = False
            DateEdit2.Visible = False
            LabelControl11.Visible = False
            TextEdit2.Visible = False
            Exit Sub
        End If
    End Sub
    Private Sub CheckOverTimeSummary_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckOverTimeSummary.CheckedChanged
        If CheckOverTimeSummary.Checked = True Then
            SidePanelSelection.Visible = False
        Else
            SidePanelSelection.Visible = True
        End If
    End Sub
    Private Sub Set_Filter_Crystal() 'Optional ByVal Selection As String
        'On Error GoTo ErrorGen
        'Dim i As Integer
        Dim mCompanyString As String, mDepartmentString As String, mCatagoryString As String
        Dim mEmployeeString As String, r_CompantStr As String, mShiftString As String, mLocationString As String
        Dim mDivisionString As String, mGradeString As String, mActive As String

        mCompanyString = ""
        mActive = ""
        mDepartmentString = ""
        mCatagoryString = ""
        mEmployeeString = ""
        mShiftString = ""
        mDivisionString = ""
        mGradeString = ""
        mLocationString = ""
        Dim g_CompanyNames As String = ""
        'For CompanyCode
        If GridViewComp.GetSelectedRows.Count = 0 Then
            GridViewComp.SelectAll()
        End If       
        Dim selectedRowsComp As Integer() = GridViewComp.GetSelectedRows()
        Dim resultComp As Object() = New Object(selectedRowsComp.Length - 1) {}
        For i As Integer = 0 To selectedRowsComp.Length - 1
            Dim rowHandle As Integer = selectedRowsComp(i)
            If Not GridViewComp.IsGroupRow(rowHandle) Then
                resultComp(i) = GridViewComp.GetRowCellValue(rowHandle, "COMPANYCODE")
                If i <> 0 Then
                    mCompanyString = mCompanyString + " OR "
                End If
                mCompanyString = mCompanyString + "tblEmployee.CompanyCode = "
                mCompanyString = mCompanyString + "'" + resultComp(i) + "'"
                g_CompanyNames = g_CompanyNames & GridViewComp.GetRowCellValue(rowHandle, "COMPANYNAME") & ", "
            End If
        Next
        If GridViewComp.GetSelectedRows.Count = 0 Then
            GridViewComp.ClearSelection()
        End If
        CommonReport.g_CompanyNames = g_CompanyNames.Trim.TrimStart(",").TrimEnd(",")
        'For i = 0 To (LstCompanyTarget.ListCount - 1)
        '    LstCompanyTarget.ListIndex = i
        '    If i <> 0 Then
        '        mCompanyString = mCompanyString + " OR "
        '    End If
        '    mCompanyString = mCompanyString + "tblEmployee.CompanyCode = "
        '    mCompanyString = mCompanyString + "'" + Left(LstCompanyTarget.Text, 3) + "'"
        '    g_CompanyNames = g_CompanyNames & Trim(Mid(LstCompanyTarget.Text, 6)) & ", "
        'Next i

        'For DepartmentCode
        Dim selectedRowsDept As Integer() = GridViewDept.GetSelectedRows()
        Dim resultDept As Object() = New Object(selectedRowsDept.Length - 1) {}
        For i As Integer = 0 To selectedRowsDept.Length - 1
            Dim rowHandle As Integer = selectedRowsDept(i)
            If Not GridViewDept.IsGroupRow(rowHandle) Then
                resultDept(i) = GridViewDept.GetRowCellValue(rowHandle, "DEPARTMENTCODE")
                If i <> 0 Then
                    mDepartmentString = mDepartmentString + " OR "
                End If
                mDepartmentString = mDepartmentString + "tblEmployee.DepartmentCode = "
                mDepartmentString = mDepartmentString + "'" + resultDept(i) + "'"
            End If
        Next

        'For CatagoryCode
        Dim selectedRowsCat As Integer() = GridViewCat.GetSelectedRows()
        Dim resultCat As Object() = New Object(selectedRowsCat.Length - 1) {}
        For i As Integer = 0 To selectedRowsCat.Length - 1
            Dim rowHandle As Integer = selectedRowsCat(i)
            If Not GridViewCat.IsGroupRow(rowHandle) Then
                resultCat(i) = GridViewCat.GetRowCellValue(rowHandle, "CAT")
                If i <> 0 Then
                    mCatagoryString = mCatagoryString + " OR "
                End If
                mCatagoryString = mCatagoryString + "tblEmployee.CAT = "
                mCatagoryString = mCatagoryString + "'" + resultCat(i) + "'"
            End If
        Next

        ' For Division Code
        'For i = 0 To (lstDivisionTarget.ListCount - 1)
        '    lstDivisionTarget.ListIndex = i
        '    If i <> 0 Then
        '        mDivisionString = mDivisionString + " OR "
        '    End If
        '    mDivisionString = mDivisionString + "tblEmployee.DivisionCode = "
        '    mDivisionString = mDivisionString + "'" + Left(lstDivisionTarget.Text, 3) + "'"
        'Next i

        ' For Grade Code
        Dim selectedRowsGrade As Integer() = GridViewGrade.GetSelectedRows()
        Dim resultGrade As Object() = New Object(selectedRowsGrade.Length - 1) {}
        For i As Integer = 0 To selectedRowsGrade.Length - 1
            Dim rowHandle As Integer = selectedRowsGrade(i)
            If Not GridViewGrade.IsGroupRow(rowHandle) Then
                resultGrade(i) = GridViewGrade.GetRowCellValue(rowHandle, "GradeCode")
                If i <> 0 Then
                    mGradeString = mGradeString + " OR "
                End If
                mGradeString = mGradeString + "tblEmployee.GradeCode = "
                mGradeString = mGradeString + "'" + resultGrade(i) + "'"
            End If
        Next

        'For EmployeeCode  - Check for late arrival
        Dim selectedRowsEmp As Integer() = GridViewEmp.GetSelectedRows()
        Dim resultEmp As Object() = New Object(selectedRowsEmp.Length - 1) {}
        For i As Integer = 0 To selectedRowsEmp.Length - 1
            Dim rowHandle As Integer = selectedRowsEmp(i)
            If Not GridViewEmp.IsGroupRow(rowHandle) Then
                resultEmp(i) = GridViewEmp.GetRowCellValue(rowHandle, "PAYCODE")
                Dim emp As String = resultEmp(i)
                If i <> 0 Then
                    mEmployeeString = mEmployeeString + " OR "
                End If
                mEmployeeString = mEmployeeString & "tblEmployee.Paycode  = "
                mEmployeeString = mEmployeeString & "'" & emp & "'"
            End If
        Next

        'For Shift
        Dim selectedRowsShift As Integer() = GridViewShift.GetSelectedRows()
        Dim resultShift As Object() = New Object(selectedRowsShift.Length - 1) {}
        For i As Integer = 0 To selectedRowsShift.Length - 1
            Dim rowHandle As Integer = selectedRowsShift(i)
            If Not GridViewShift.IsGroupRow(rowHandle) Then
                resultShift(i) = GridViewShift.GetRowCellValue(rowHandle, "SHIFT")
                If i <> 0 Then
                    mShiftString = mShiftString + " OR "
                End If
                If WhichReport = 1 Then
                    mShiftString = mShiftString + "tblTimeRegister.ShiftAttended = "
                Else
                    mShiftString = mShiftString + "tblTimeRegister.ShiftAttended = "
                End If
                mShiftString = mShiftString + "'" + resultShift(i) + "'"
            End If
        Next

        'For Location
        Dim selectedRowsLocation As Integer() = GridViewBranch.GetSelectedRows()
        Dim resultLocation As Object() = New Object(selectedRowsLocation.Length - 1) {}
        For i As Integer = 0 To selectedRowsLocation.Length - 1
            Dim rowHandle As Integer = selectedRowsLocation(i)
            If Not GridViewBranch.IsGroupRow(rowHandle) Then
                resultLocation(i) = GridViewBranch.GetRowCellValue(rowHandle, "BRANCHCODE")
                If i <> 0 Then
                    mLocationString = mLocationString + " OR "
                End If
                If WhichReport = 1 Then
                    mLocationString = mLocationString + "tblEmployee.BRANCHCODE = "
                Else
                    mLocationString = mLocationString + "tblEmployee.BRANCHCODE = "
                End If
                mLocationString = mLocationString + "'" + resultLocation(i) + "'"
            End If
        Next
        'For i = 0 To (LstShiftTarget.ListCount - 1)
        '    LstShiftTarget.ListIndex = i
        '    If i <> 0 Then
        '        mShiftString = mShiftString + " OR "
        '    End If
        '    If WhichReport = 1 Then
        '        'If g_Report_view Then
        '        mShiftString = mShiftString + "tblTimeRegister.ShiftAttended = "
        '        'Else
        '        '    mShiftString = mShiftString + "tblTimeRegisterd.ShiftAttended = "
        '        'End If
        '    Else
        '        'If g_Report_view Then
        '        mShiftString = mShiftString + "tblTimeRegister.ShiftAttended = "
        '        'Else
        '        '    mShiftString = mShiftString + "tblTimeRegisterd.ShiftAttended = "
        '        'End If
        '    End If
        '    mShiftString = mShiftString + "'" + Left(LstShiftTarget.Text, 3) + "'"
        'Next i

        If CheckEdit1.Checked And CheckEdit2.Checked Then
            mActive = ""
        Else
            If CheckEdit1.Checked Then
                mActive = "tblEmployee.Active = 'Y'"
            End If
            If CheckEdit2.Checked Then
                mActive = "tblEmployee.Active = 'N'"
            End If
        End If
        Dim mCondition As String
        'Dim g_WhereClause As String
        mCondition = IIf(Len(Trim(mEmployeeString)) = 0, "(", "(" & Trim(mEmployeeString) & ") AND (") & _
                     IIf(Len(Trim(mCatagoryString)) = 0, "", " " & Trim(mCatagoryString) & ") AND (") & _
                     IIf(Len(Trim(mDepartmentString)) = 0, "", " " & Trim(mDepartmentString) & ") AND (") & _
                     IIf(Len(Trim(mCompanyString)) = 0, "", " " & Trim(mCompanyString) & ") AND (") & _
                     IIf(Len(Trim(mShiftString)) = 0, "", " " & Trim(mShiftString) & ") AND (") & _
                     IIf(Len(Trim(mGradeString)) = 0, "", " " & Trim(mGradeString) & ") AND (") & _
                     IIf(Len(Trim(mLocationString)) = 0, "", " " & Trim(mLocationString) & ") AND (") & _
                     IIf(Len(Trim(mActive)) = 0, "", " " & Trim(mActive) & ") AND (")

        If mCondition = "(" Then
            mCondition = ""
        End If
        If Not Len(Trim(mCondition)) = 0 Then
            mCondition = mCondition.Remove(mCondition.Trim.Length - 5)
            'mCondition = LTrim(mCondition, Len(Trim(mCondition)) - 5)
        End If
        g_WhereClause = mCondition

        If Common.USERTYPE <> "A" Then
            Dim emp() As String = Common.Auth_Branch.Split(",")
            Dim ls As New List(Of String)()
            For x As Integer = 0 To emp.Length - 1
                ls.Add(emp(x).Trim)
            Next
            If g_WhereClause = "" Then
                g_WhereClause = " TBLEmployee.BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "')"
            Else
                g_WhereClause = g_WhereClause & " and TBLEmployee.BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "')"
            End If

            Dim com() As String = Common.auth_comp.Split(",")
            Dim lsc As New List(Of String)()
            For x As Integer = 0 To com.Length - 1
                lsc.Add(com(x).Trim)
            Next
            If g_WhereClause = "" Then
                g_WhereClause = " TBLEmployee.COMPANYCODE IN ('" & String.Join("', '", lsc.ToArray()) & "')"
            Else
                g_WhereClause = g_WhereClause & " and TBLEmployee.COMPANYCODE IN ('" & String.Join("', '", lsc.ToArray()) & "')"
            End If
        End If
        Exit Sub
        'ErrorGen:
        '        MsgBox(Err.Description, vbInformation, "TimeWatch")
    End Sub
    Private Sub PopupContainerEditEmp_QueryResultValue(sender As System.Object, e As DevExpress.XtraEditors.Controls.QueryResultValueEventArgs) Handles PopupContainerEditEmp.QueryResultValue
        Dim selectedRows() As Integer = GridViewEmp.GetSelectedRows
        Dim sb As StringBuilder = New StringBuilder
        For Each selectionRow As Integer In selectedRows
            Dim a As System.Data.DataRowView = GridViewEmp.GetRow(selectionRow)
            If (sb.ToString.Length > 0) Then
                sb.Append(", ")
            End If
            sb.Append(a.Item("PAYCODE"))
        Next
        e.Value = sb.ToString
    End Sub
    Private Sub PopupContainerEditComp_QueryResultValue(sender As System.Object, e As DevExpress.XtraEditors.Controls.QueryResultValueEventArgs) Handles PopupContainerEditComp.QueryResultValue
        Dim selectedRows() As Integer = GridViewComp.GetSelectedRows
        Dim sb As StringBuilder = New StringBuilder
        For Each selectionRow As Integer In selectedRows
            Dim a As System.Data.DataRowView = GridViewComp.GetRow(selectionRow)
            If (sb.ToString.Length > 0) Then
                sb.Append(", ")
            End If
            sb.Append(a.Item("COMPANYCODE"))
        Next
        e.Value = sb.ToString
    End Sub
    Private Sub PopupContainerEditDept_QueryResultValue(sender As System.Object, e As DevExpress.XtraEditors.Controls.QueryResultValueEventArgs) Handles PopupContainerEditDept.QueryResultValue
        Dim selectedRows() As Integer = GridViewDept.GetSelectedRows
        Dim sb As StringBuilder = New StringBuilder
        For Each selectionRow As Integer In selectedRows
            Dim a As System.Data.DataRowView = GridViewDept.GetRow(selectionRow)
            If (sb.ToString.Length > 0) Then
                sb.Append(", ")
            End If
            sb.Append(a.Item("DEPARTMENTCODE"))
        Next
        e.Value = sb.ToString
    End Sub
    Private Sub PopupContainerEditShift_QueryResultValue(sender As System.Object, e As DevExpress.XtraEditors.Controls.QueryResultValueEventArgs) Handles PopupContainerEditShift.QueryResultValue
        Dim selectedRows() As Integer = GridViewShift.GetSelectedRows
        Dim sb As StringBuilder = New StringBuilder
        For Each selectionRow As Integer In selectedRows
            Dim a As System.Data.DataRowView = GridViewShift.GetRow(selectionRow)
            If (sb.ToString.Length > 0) Then
                sb.Append(", ")
            End If
            sb.Append(a.Item("SHIFT"))
        Next
        e.Value = sb.ToString
    End Sub
    Private Sub PopupContainerEditCat_QueryResultValue(sender As System.Object, e As DevExpress.XtraEditors.Controls.QueryResultValueEventArgs) Handles PopupContainerEditCat.QueryResultValue
        Dim selectedRows() As Integer = GridViewCat.GetSelectedRows
        Dim sb As StringBuilder = New StringBuilder
        For Each selectionRow As Integer In selectedRows
            Dim a As System.Data.DataRowView = GridViewCat.GetRow(selectionRow)
            If (sb.ToString.Length > 0) Then
                sb.Append(", ")
            End If
            sb.Append(a.Item("CAT"))
        Next
        e.Value = sb.ToString
    End Sub
    Private Sub PopupContainerEditGrade_QueryResultValue(sender As System.Object, e As DevExpress.XtraEditors.Controls.QueryResultValueEventArgs) Handles PopupContainerEditGrade.QueryResultValue
        Dim selectedRows() As Integer = GridViewGrade.GetSelectedRows
        Dim sb As StringBuilder = New StringBuilder
        For Each selectionRow As Integer In selectedRows
            Dim a As System.Data.DataRowView = GridViewGrade.GetRow(selectionRow)
            If (sb.ToString.Length > 0) Then
                sb.Append(", ")
            End If
            sb.Append(a.Item("GradeCode"))
        Next
        e.Value = sb.ToString
    End Sub
    Private Sub PopupContainerEditLocation_QueryResultValue(sender As System.Object, e As DevExpress.XtraEditors.Controls.QueryResultValueEventArgs) Handles PopupContainerEditLocation.QueryResultValue
        Dim selectedRows() As Integer = GridViewBranch.GetSelectedRows
        Dim sb As StringBuilder = New StringBuilder
        For Each selectionRow As Integer In selectedRows
            Dim a As System.Data.DataRowView = GridViewBranch.GetRow(selectionRow)
            If (sb.ToString.Length > 0) Then
                sb.Append(", ")
            End If
            sb.Append(a.Item("BRANCHCODE"))
        Next
        e.Value = sb.ToString
    End Sub
    Private Sub PopupContainerEditDevice_QueryResultValue(sender As System.Object, e As DevExpress.XtraEditors.Controls.QueryResultValueEventArgs) Handles PopupContainerEditDevice.QueryResultValue
        Dim selectedRows() As Integer = GridViewDevice.GetSelectedRows
        Dim sb As StringBuilder = New StringBuilder
        For Each selectionRow As Integer In selectedRows
            Dim a As System.Data.DataRowView = GridViewDevice.GetRow(selectionRow)
            If (sb.ToString.Length > 0) Then
                sb.Append(", ")
            End If
            sb.Append(a.Item("ID_NO"))
        Next
        e.Value = sb.ToString
    End Sub

    Sub Spot_LateArrival(strsortorder As String)
        'On Error GoTo LateArrival_Error
        Dim strsql As String, strDeptDivCode As String
        Dim v_Late As String, msrl As Integer
        Dim blnDeptAvailable As Boolean, mFirst As Boolean
        msrl = 1
        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If

        mstrDepartmentCode = " "
        mintPageNo = 1
        mintLine = 1
        mblnCheckReport = False
        blnDeptAvailable = False
        mFirst = True

        'If Left(strsortorder, 1) = "D" Or Left(strsortorder, 1) = "C" Then blnDeptAvailable = True
        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet
        If strsortorder = "" Then
            If Common.servername = "Access" Then
                'strsql = " Select tblCatagory.Catagoryname, tblDivision.DivisionName, tblTimeregister.PAYCODE, tblTimeregister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, " & _
                '    "tblDepartment.DepartmentCode, tblDepartment.DepartmentName,tblEmployee.PresentCARDNO, tblEmployee.EMPNAME, tblTimeregister.DATEOFFICE, " & _
                '    "tblTimeregister.SHIFTATTENDED, tblTimeregister.shiftStartTime, tblTimeregister.In1, tblTimeregister.Latearrival, " & _
                '    "IIF (tblTimeregister.Latearrival <= 10,'**','  ') as Late1, " & _
                '    "IIF (tblTimeregister.Latearrival > 10 And tblTimeregister.Latearrival <= 30, '**' ,'  ') AS Late10, " & _
                '    "IIF(tblTimeregister.Latearrival > 30 And tblTimeregister.Latearrival <= 60,'**','  ') AS Late30, " & _
                '    "IIF (tblTimeregister.Latearrival > 60 ,'**', '  ') AS Late60, tblEmployee.BUS " & _
                '    " From tblCatagory, tblDivision,tblTimeregister, tblEmployee, tblDepartment, tblCompany " & _
                '    "Where tblEmployee.CAT = tblCatagory.cat And tblEmployee.DivisionCode = tblDivision.DivisionCode And tblEmployee.PayCode = tblTimeregister.PayCode And " & _
                '    "tblEmployee.DepartmentCode = tblDepartment.DepartmentCode AND format(tbltimeregister.DateOffice,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' AND  " & _
                '    "tblEmployee.CompanyCode = tblCompany.CompanyCode And  " & _
                '    "tblTimeregister.Latearrival > 0" & g_HODDepartmentCode & _
                '    " And (tblemployee.LeavingDate >=#" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "# or tblemployee.LeavingDate is null)" & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause)

                strsql = " Select tblCatagory.Catagoryname, tblDivision.DivisionName, tblTimeregister.PAYCODE, tblTimeregister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, " & _
                    "tblDepartment.DepartmentCode, tblDepartment.DepartmentName,tblEmployee.PresentCARDNO, tblEmployee.EMPNAME, tblTimeregister.DATEOFFICE, " & _
                    "tblTimeregister.SHIFTATTENDED, tblTimeregister.shiftStartTime, tblTimeregister.In1, tblTimeregister.Latearrival, " & _
                    "IIF (tblTimeregister.Latearrival <= 10,'**','  ') as Late1, " & _
                    "IIF (tblTimeregister.Latearrival > 10 And tblTimeregister.Latearrival <= 30, '**' ,'  ') AS Late10, " & _
                    "IIF(tblTimeregister.Latearrival > 30 And tblTimeregister.Latearrival <= 60,'**','  ') AS Late30, " & _
                    "IIF (tblTimeregister.Latearrival > 60 ,'**', '  ') AS Late60, tblEmployee.BUS " & _
                    " From tblCatagory, tblDivision,tblTimeregister, tblEmployee, tblDepartment, tblCompany " & _
                    "Where tblEmployee.CAT = tblCatagory.cat And tblEmployee.PayCode = tblTimeregister.PayCode And " & _
                    "tblEmployee.DepartmentCode = tblDepartment.DepartmentCode AND format(tbltimeregister.DateOffice,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' AND  " & _
                    "tblEmployee.CompanyCode = tblCompany.CompanyCode And  " & _
                    "tblTimeregister.Latearrival > 0" & g_HODDepartmentCode & _
                    "" & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & " order by tblEmployee.PresentCARDNO, TblTimeRegister.DateOffice"
                If Common.IsCompliance Then
                    strsql = Microsoft.VisualBasic.Strings.Replace(strsql, "tbltimeregister", "tblTimeRegisterD", 1, -1, Microsoft.VisualBasic.Constants.vbTextCompare)
                End If

                adapA = New OleDbDataAdapter(strsql, Common.con1)
                adapA.Fill(Rs_Report)
            Else
                strsql = " Select tblCatagory.Catagoryname, tblDivision.DivisionName, tblTimeregister.PAYCODE, tblTimeregister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, " & _
                    "tblDepartment.DepartmentCode, tblDepartment.DepartmentName,tblEmployee.PresentCARDNO, tblEmployee.EMPNAME, tblTimeregister.DATEOFFICE, " & _
                    "tblTimeregister.SHIFTATTENDED, tblTimeregister.shiftStartTime, tblTimeregister.In1, tblTimeregister.Latearrival, " & _
                    "Late1 = Case When tblTimeregister.Latearrival <= 10 then '**' Else '  'End, " & _
                    "Late10 = Case When tblTimeregister.Latearrival > 10 And tblTimeregister.Latearrival <= 30 then '**' Else '  ' End, " & _
                    "Late30 = Case When tblTimeregister.Latearrival > 30 And tblTimeregister.Latearrival <= 60 then '**' Else '  ' End, " & _
                    "Late60 = Case When tblTimeregister.Latearrival > 60 then '**' Else '  ' End, tblEmployee.BUS " & _
                    " From tblCatagory, tblDivision,tblTimeregister, tblEmployee, tblDepartment, tblCompany " & _
                    "Where tblEmployee.CAT = tblCatagory.cat And tblEmployee.PayCode = tblTimeregister.PayCode And " & _
                    "tblEmployee.DepartmentCode = tblDepartment.DepartmentCode AND tbltimeregister.DateOffice = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' AND  " & _
                    "tblEmployee.CompanyCode = tblCompany.CompanyCode And  " & _
                    "tblTimeregister.Latearrival > 0" & g_HODDepartmentCode & _
                    " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & " order by tblEmployee.PresentCARDNO, TblTimeRegister.DateOffice"
                If Common.IsCompliance Then
                    strsql = Microsoft.VisualBasic.Strings.Replace(strsql, "tbltimeregister", "tblTimeRegisterD", 1, -1, Microsoft.VisualBasic.Constants.vbTextCompare)
                End If

                adap = New SqlDataAdapter(strsql, Common.con)
                adap.Fill(Rs_Report)
            End If

            'Rs_Report = Cn.Execute(strsql)
            'Rs_Report.Sort = "PayCode"
        Else
            'Rs_Report.Sort = strsortorder
        End If
        If Rs_Report.Tables(0).Rows.Count < 1 Then
            'G_Report_Error = False
            'Screen.MousePointer = vbDefault
            XtraMessageBox.Show(ulf, "<size=10>No Data Available For this Report</size>", "<size=9>iAS</size>")
            'mReportPrintStatus = False
            Exit Sub
        End If

        mFileNumber = FreeFile()
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".SRP"
        Dim objWriter As New System.IO.StreamWriter(mstrFile_Name, True)

        'With Rs_Report
        '    .MoveFirst()
        'Do While Not .EOF
        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            If mFirst Or mintLine > g_LinesPerPage Or (strsortorder.Trim = "Department" And strDeptDivCode <> Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim And g_SkipAfterDept) Then
                'If mFirst Or mintLine > g_LinesPerPage Or (Left(strsortorder, 10) = "Department" And strDeptDivCode <> !DepartmentCode And g_SkipAfterDept) Then
1:              If Not mFirst Then
                    objWriter.WriteLine(Space(2) & vbFormFeed)
                Else
                    mFirst = False
                End If
                objWriter.WriteLine(Space(2) & Space(40) & CommonReport.g_CompanyNames)
                objWriter.WriteLine(Space(2) + "")
                objWriter.WriteLine(Space(2) + "")
                'objWriter.WriteLine(Space(2) + "Page No." & mintPageNo & Space(39) & "Late Arival Report for " & DateEdit1.DateTime.ToString("dd/MM/yyyy") & Space(8) & "Run Date & Time :" & Now.ToString("dd/MM/yyyy HH:mm"))
                objWriter.WriteLine(Space(2) + "Page No." & mintPageNo & Space(39) & "Late Arival Report for " & ForDate & Space(8) & "Run Date & Time :" & Common.runningDateTime) 'Now.ToString("dd/MM/yyyy HH:mm"))
                objWriter.WriteLine(Space(2) + "---------------------------------------------------------------------------------------------------------------------------")
                objWriter.WriteLine(Space(2) + "Srl  EmpCode      Cardno.      Employee Name             Shift Start In    Shift  ------------- Late ------------  Bus Route")
                objWriter.WriteLine(Space(2) + "No.                                                                        Late   >(0.01) >(0.10) >(0.30) >(1.00)")
                objWriter.WriteLine(Space(2) + "---------------------------------------------------------------------------------------------------------------------------")
                mintLine = 8
                mintPageNo = mintPageNo + 1
            End If
            'If blnDeptAvailable And i <> Rs_Report.Tables(0).Rows.Count - 1 Then
            '    If Left(strsortorder, 10) = "Department" And strDeptDivCode <> Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim Then
            '        If mintLine + 3 > g_LinesPerPage Then GoTo 1
            '        Print #mFileNumber, Space(2) + ""
            '        strDeptDivCode = !DepartmentCode
            '        Print #mFileNumber, Space(2) + "** Department Code & Name : " & Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim & "  " & Rs_Report.Tables(0).Rows(i).Item("DepartmentName").ToString.Trim
            '        Print #mFileNumber, Space(2) + ""
            '        mintLine = mintLine + 3
            '    ElseIf Left(strsortorder, 8) = "Division" And strDeptDivCode <> Rs_Report.Tables(0).Rows(i).Item("DivisionCode").ToString.Trim Then
            '        If mintLine + 3 > g_LinesPerPage Then GoTo 1
            '        Print #mFileNumber, Space(2) + ""
            '        strDeptDivCode = !DivisionCode
            '        Print #mFileNumber, Space(2) + "** Section Code & Name : " & Rs_Report.Tables(0).Rows(i).Item("DivisionCode").ToString.Trim & "  " & Rs_Report.Tables(0).Rows(i).Item("DivisionName").ToString.Trim
            '        Print #mFileNumber, Space(2) + ""
            '        mintLine = mintLine + 3
            '    ElseIf Left(strsortorder, 3) = "Cat" And strDeptDivCode <> Rs_Report.Tables(0).Rows(i).Item("Cat").ToString.Trim Then
            '        If mintLine + 3 > g_LinesPerPage Then GoTo 1
            '        Print #mFileNumber, Space(2) + ""
            '        strDeptDivCode = !Cat
            '        Print #mFileNumber, Space(2) + "** Category Code & Name : " &  Rs_Report.Tables(0).Rows(i).Item("Cat").ToString.Trim & "  " &  Rs_Report.Tables(0).Rows(i).Item("CatagoryName").ToString.Trim
            '        Print #mFileNumber, Space(2) + ""
            '        mintLine = mintLine + 3
            '    End If
            'End If

            If Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim = "" Then
                v_Late = ""
            Else
                v_Late = Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim Mod 60).ToString("00")
            End If
            'v_Late = Length5(Min2Hr(Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim))
            objWriter.WriteLine(Space(2) & (i + 1).ToString.PadRight(4) & " " & Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim.PadRight(12) & " " & Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim.PadRight(12) & " " & _
                                Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim.PadRight(25) & " " & Rs_Report.Tables(0).Rows(i).Item("ShiftAttended").ToString.Trim.PadRight(3) & "   " & _
                                Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("shiftStartTime").ToString.Trim).ToString("HH:mm") & " " & Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("In1").ToString.Trim).ToString("hh:mm") & " " & _
                                Trim(v_Late) & "     " & Rs_Report.Tables(0).Rows(i).Item("Late1").ToString.Trim & "      " & Rs_Report.Tables(0).Rows(i).Item("Late10").ToString.Trim & "      " & Rs_Report.Tables(0).Rows(i).Item("Late30").ToString & "      " & Rs_Report.Tables(0).Rows(i).Item("Late60").ToString.Trim & "    " & Rs_Report.Tables(0).Rows(i).Item("bus").ToString.Trim & " ")

            msrl = msrl + 1
            mintLine = mintLine + 1
            mblnCheckReport = True
            '.MoveNext()
        Next
        objWriter.Close()

        If isPdf = True Then
            'pdf start 
            Common.pdfreport(mstrFile_Name, 8, ulf)
            'pdf end 
        Else
            Try : Try : Process.Start(mstrFile_Name) : Catch ex As Exception : Process.Start("notepad.exe", mstrFile_Name) : End Try : Catch ex As Exception : Process.Start("notepad.exe", mstrFile_Name) : End Try
        End If
    End Sub
    Sub Spot_Absenteeism(strsortorder As String)
        'On Error GoTo AbsenteeismError
        Dim strDeptDivCode As String
        Dim strsql As String
        Dim v_Absent As String '* 5
        Dim v_Leave As String ' * 5
        Dim v_OnDuty As String ' * 5
        Dim blnDeptAvailable As Boolean, mFirst As Boolean
        Dim msrl As Integer
        msrl = 1
        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If
        mstrDepartmentCode = " "
        'On Error GoTo AbsenteeismError
        mintPageNo = 1
        mintLine = 1
        mblnCheckReport = False
        blnDeptAvailable = False
        mFirst = True
        Dim g_HODDepartmentCode As String = "" 'nitin

        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet

        'If Left(strsortorder, 1) = "D" Or Left(strsortorder, 1) = "C" Then blnDeptAvailable = True
        If strsortorder = "" Then
            If Common.servername = "Access" Then
                ' strsql = " Select tblCatagory.Catagoryname, tblDivision.DivisionName, tblTimeregister.Paycode,tblTimeregister.reason,tblTimeregister.Shift,TblEmployee.EmpName, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, TblEmployee.PresentCardno, TblDepartment.DepartmentCode, tblDepartment.DepartmentName,tblTimeregister.Status, tblTimeregister.LeaveType From  tblCatagory, tblDivision, tblTimeregister, TblEmployee, tblDepartment,tblCompany " & _
                '" Where tblEmployee.CAT = tblCatagory.cat And tblEmployee.DivisionCode = tblDivision.DivisionCode And (tblTimeregister.AbsentValue > 0 Or tblTimeregister.LeaveType In ('A', 'P', 'L')) And " & _
                '" tblEmployee.companycode=tblCompany.companycode and tblEmployee.DepartmentCode = tblDepartment.DepartmentCode AND FORMAT(tbltimeregister.DateOffice,'MMM DD YYYY') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
                '" and tblEmployee.PayCode = tblTimeregister.Paycode" & g_HODDepartmentCode & _
                '" And (tblemployee.LeavingDate >=#" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "# or tblemployee.LeavingDate is null)" & IIf(Len(Trim(g_WhereClause)) = 0, "", " And " & g_WhereClause)

                strsql = " Select tblCatagory.Catagoryname, tblDivision.DivisionName, tblTimeregister.Paycode,tblTimeregister.reason,tblTimeregister.Shift,TblEmployee.EmpName, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, TblEmployee.PresentCardno, TblDepartment.DepartmentCode, tblDepartment.DepartmentName,tblTimeregister.Status, tblTimeregister.LeaveType From  tblCatagory, tblDivision, tblTimeregister, TblEmployee, tblDepartment,tblCompany " & _
             " Where tblEmployee.CAT = tblCatagory.cat And (tblTimeregister.AbsentValue > 0 Or tblTimeregister.LeaveType In ('A', 'P', 'L')) And " & _
             " tblEmployee.companycode=tblCompany.companycode and tblEmployee.DepartmentCode = tblDepartment.DepartmentCode AND FORMAT(tbltimeregister.DateOffice,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
             " and tblEmployee.PayCode = tblTimeregister.Paycode" & g_HODDepartmentCode & _
             " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " And " & g_WhereClause) & " order by tblEmployee.PresentCARDNO, TblTimeRegister.DateOffice"

                If Common.IsCompliance Then
                    strsql = Microsoft.VisualBasic.Strings.Replace(strsql, "tbltimeregister", "tblTimeRegisterD", 1, -1, Microsoft.VisualBasic.Constants.vbTextCompare)
                End If

                adapA = New OleDbDataAdapter(strsql, Common.con1)
                adapA.Fill(Rs_Report)
            Else
                strsql = " Select tblCatagory.Catagoryname, tblDivision.DivisionName, tblTimeregister.Paycode,tblTimeregister.reason,tblTimeregister.Shift,TblEmployee.EmpName, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, TblEmployee.PresentCardno, TblDepartment.DepartmentCode, tblDepartment.DepartmentName,tblTimeregister.Status, tblTimeregister.LeaveType From  tblCatagory, tblDivision, tblTimeregister, TblEmployee, tblDepartment,tblCompany " & _
               " Where tblEmployee.CAT = tblCatagory.cat And (tblTimeregister.AbsentValue > 0 Or tblTimeregister.LeaveType In ('A', 'P', 'L')) And " & _
               " tblEmployee.companycode=tblCompany.companycode and tblEmployee.DepartmentCode = tblDepartment.DepartmentCode AND tbltimeregister.DateOffice = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
               " and tblEmployee.PayCode = tblTimeregister.Paycode" & g_HODDepartmentCode & _
               " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " And " & g_WhereClause) & " order by tblEmployee.PresentCARDNO, TblTimeRegister.DateOffice"
                If Common.IsCompliance Then
                    strsql = Microsoft.VisualBasic.Strings.Replace(strsql, "tbltimeregister", "tblTimeRegisterD", 1, -1, Microsoft.VisualBasic.Constants.vbTextCompare)
                End If

                adap = New SqlDataAdapter(strsql, Common.con)
                adap.Fill(Rs_Report)
            End If
            'Rs_Report = Cn.Execute(strsql)
            'Rs_Report.Sort = "PayCode"
            'Else
            '    Rs_Report.Sort = strsortorder
        End If
        If Rs_Report.Tables(0).Rows.Count < 1 Then
            'G_Report_Error = False
            'Screen.MousePointer = vbDefault
            XtraMessageBox.Show(ulf, "<size=10>Nobody is absent on this Date.</size>", "<size=9>iAS</size>")
            'mReportPrintStatus = False
            Exit Sub
        End If

        mFileNumber = FreeFile()
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".SRP"
        'If CheckPDF.Checked = True Then
        '    mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".doc"
        'End If
        Dim objWriter As New System.IO.StreamWriter(mstrFile_Name, True)
        'With Rs_Report
        '    .MoveFirst()
        'Do While Not .EOF
        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            'If mFirst Or mintLine > g_LinesPerPage Or strsortorder.Trim = "Department" And strDeptDivCode <> Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim And g_SkipAfterDept) Then
            If mFirst Or mintLine > g_LinesPerPage Or (strsortorder.Trim = "Department" And strDeptDivCode <> Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim And g_SkipAfterDept) Then
1:              If Not mFirst Then
                    objWriter.WriteLine(Space(2) + vbFormFeed)
                Else
                    mFirst = False
                End If
                objWriter.WriteLine(Space(2) & Space(45) & CommonReport.g_CompanyNames)
                objWriter.WriteLine(Space(2) + "")
                objWriter.WriteLine(Space(2) + "")
                'objWriter.WriteLine(Space(2) + "Page No." & mintPageNo & Space(35) & "ABSENCE FROM DUTY ON " & DateEdit1.DateTime.ToString("dd/MM/yyyy") & Space(8) & "Run Date & Time :" & Format(Now(), "dd/MM/yyyy HH:mm"))
                objWriter.WriteLine(Space(2) + "Page No." & mintPageNo & Space(35) & "ABSENCE FROM DUTY ON " & ForDate & Space(8) & "Run Date & Time :" & Common.runningDateTime)
                objWriter.WriteLine(Space(2) + "----------------------------------------------------------------------------------------------------------------")
                objWriter.WriteLine(Space(2) + "Srl  EmpCode         Card No      Employee's Name         Absent  Leave   On Duty   Remark   ")
                objWriter.WriteLine(Space(2) + "No.                                                                               ")
                objWriter.WriteLine(Space(2) + "----------------------------------------------------------------------------------------------------------------")
                mintLine = 8
                mintPageNo = mintPageNo + 1
            End If



            v_Absent = IIf(Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim = "A" Or Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim = "HLF" Or Rs_Report.Tables(0).Rows(i).Item("LEAVETYPE").ToString.Trim = "A", Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim, "     ")
            v_OnDuty = IIf(Rs_Report.Tables(0).Rows(i).Item("LEAVETYPE").ToString.Trim = "P", Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim, "     ")
            v_Leave = IIf(Rs_Report.Tables(0).Rows(i).Item("LEAVETYPE").ToString.Trim = "L", Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim, "     ")
            objWriter.WriteLine(Space(2) & msrl.ToString.PadRight(4) & " " & Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim.PadRight(12) & "  " & Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim.PadRight(12) & "   " & Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim.PadRight(25) & "  " & v_Absent & "   " & _
                                 v_Leave.PadRight(3) & "    " & v_OnDuty.PadRight(2) & "    " & Trim(Rs_Report.Tables(0).Rows(i).Item("reason").ToString.Trim).PadRight(25))
            msrl = msrl + 1
            mintLine = mintLine + 1
            mblnCheckReport = True
            '.MoveNext()
        Next
        objWriter.Close()
        If isPdf = True Then
            'pdf start 
            ''Load Document
            'Dim document As New Spire.Doc.Document()
            'document.LoadFromFile("F:\developement\TimeWatchNewDeskTop\TimeWatchNewDeskTop\bin\Debug\Reports\TimeWatch_20180606143140.docx")
            ''Initialize a Header Instance
            'Dim header As Spire.Doc.HeaderFooter = document.Sections(0).HeadersFooters.Header
            ''Add Header Paragraph and Format
            'Dim paragraph As Spire.Doc.Documents.Paragraph = header.AddParagraph()
            'paragraph.Format.HorizontalAlignment = HorizontalAlignment.Right
            ''Append Picture for Header Paragraph and Format
            'Dim headerimage As DocPicture = paragraph.AppendPicture(Image.FromFile("F:\developement\TimeWatchNewDeskTop\TimeWatchNewDeskTop\images\Absent.png"))
            'headerimage.VerticalAlignment = Spire.Doc.ShapeVerticalAlignment.Bottom
            'document.SaveToFile(mstrFile_Name, Spire.Doc.FileFormat.Docx)
            'System.Diagnostics.Process.Start(mstrFile_Name)

            Common.pdfreport(mstrFile_Name, 8, ulf)
            'pdf end 
        Else
            Try : Process.Start(mstrFile_Name) : Catch ex As Exception : Process.Start("notepad.exe", mstrFile_Name) : End Try
        End If
    End Sub
    Sub Spot_Attendance(strsortorder As String)
        'On Error GoTo ErrorGen
        Dim strsql As String, strDeptDivCode As String
        Dim v_Late As String '* 6
        Dim v_InTime As String '* 6
        Dim strStatus As String
        Dim blnDeptAvailable As Boolean, mFirst As Boolean
        Dim msrl As Integer
        msrl = 1
        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If

        mstrDepartmentCode = " "
        blnDeptAvailable = False
        mFirst = True
        mintPageNo = 1
        mintLine = 1
        mblnCheckReport = False

        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet
        'If Left(strsortorder, 1) = "D" Or Left(strsortorder, 1) = "C" Then blnDeptAvailable = True

        If strsortorder = "" Then
            If Common.servername = "Access" Then
                'strsql = " Select tblTimeregister.PAYCODE, tblTimeregister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, tblDepartment.DepartmentCode, tblDepartment.DepartmentName,tblEmployee.PresentCARDNO, tblEmployee.EMPNAME, tblTimeregister.DATEOFFICE, " & _
                '     " tblTimeregister.SHIFTATTENDED, tblTimeregister.ShiftStartTime, tblTimeregister.In1, tblTimeregister.LateArrival, tblTimeregister.Status, tblCatagory.Catagoryname, tblDivision.DivisionName " & _
                '     " From  tblTimeregister, tblEmployee, tblDepartment, tblCatagory, tblDivision,tblCompany " & _
                '     " Where tblEmployee.PayCode = tblTimeregister.PayCode And " & _
                '     " tblEmployee.Companycode=tblCompany.Companycode and tblEmployee.CAT = tblCatagory.cat And " & _
                '     " tblEmployee.DivisionCode = tblDivision.DivisionCode AND FORMAT(tbltimeregister.DateOffice,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' And " & _
                '     " tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
                '     " And (tblemployee.LeavingDate >=#" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "# or tblemployee.LeavingDate is null)" & IIf(Len(Trim(g_WhereClause)) = 0, "", " and " & g_WhereClause)

                strsql = " Select tblTimeregister.PAYCODE, tblTimeregister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, tblDepartment.DepartmentCode, tblDepartment.DepartmentName,tblEmployee.PresentCARDNO, tblEmployee.EMPNAME, tblTimeregister.DATEOFFICE, " & _
                    " tblTimeregister.SHIFTATTENDED, tblTimeregister.ShiftStartTime, tblTimeregister.In1, tblTimeregister.LateArrival, tblTimeregister.Status, tblCatagory.Catagoryname, tblDivision.DivisionName " & _
                    " From  tblTimeregister, tblEmployee, tblDepartment, tblCatagory, tblDivision,tblCompany " & _
                    " Where tblEmployee.PayCode = tblTimeregister.PayCode And " & _
                    " tblEmployee.Companycode=tblCompany.Companycode and tblEmployee.CAT = tblCatagory.cat And " & _
                    " FORMAT(tbltimeregister.DateOffice,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' And " & _
                    " tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
                    " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " and " & g_WhereClause) & " order by tblEmployee.PresentCARDNO, TblTimeRegister.DateOffice"


                adapA = New OleDbDataAdapter(strsql, Common.con1)
                adapA.Fill(Rs_Report)
            Else
                strsql = " Select tblTimeregister.PAYCODE, tblTimeregister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, tblDepartment.DepartmentCode, tblDepartment.DepartmentName,tblEmployee.PresentCARDNO, tblEmployee.EMPNAME, tblTimeregister.DATEOFFICE, " & _
                         " tblTimeregister.SHIFTATTENDED, tblTimeregister.ShiftStartTime, tblTimeregister.In1, tblTimeregister.LateArrival, tblTimeregister.Status, tblCatagory.Catagoryname, tblDivision.DivisionName " & _
                         " From  tblTimeregister, tblEmployee, tblDepartment, tblCatagory, tblDivision,tblCompany " & _
                         " Where tblEmployee.PayCode = tblTimeregister.PayCode And " & _
                         " tblEmployee.Companycode=tblCompany.Companycode and tblEmployee.CAT = tblCatagory.cat And " & _
                         " tbltimeregister.DateOffice = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' And " & _
                         " tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
                         " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " and " & g_WhereClause) & " order by tblEmployee.PresentCARDNO, TblTimeRegister.DateOffice"
                adap = New SqlDataAdapter(strsql, Common.con)
                adap.Fill(Rs_Report)
            End If
            'Rs_Report = Cn.Execute(strsql)
            'Rs_Report.Sort = "PayCode"
        Else
            'Rs_Report.Sort = strsortorder
        End If
        If Rs_Report.Tables(0).Rows.Count < 1 Then
            'G_Report_Error = False
            'Screen.MousePointer = vbDefault
            XtraMessageBox.Show(ulf, "<size=10>No Data Available For this Report.</size>", "<size=9>iAS</size>")
            'mReportPrintStatus = False
            Exit Sub
        End If
        mFileNumber = FreeFile()
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".SRP"
        Dim objWriter As New System.IO.StreamWriter(mstrFile_Name, True)
        'With Rs_Report
        '.MoveFirst()
        'Do While Not .EOF
        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            If mFirst Or mintLine > g_LinesPerPage Or strsortorder.Trim = "Department" And strDeptDivCode <> Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim And g_SkipAfterDept Then
1:              If Not mFirst Then
                    objWriter.WriteLine(Space(2) + vbFormFeed)
                Else
                    mFirst = False
                End If
                objWriter.WriteLine(Space(2) & Space(20) & CommonReport.g_CompanyNames)
                objWriter.WriteLine(Space(2) + "")
                objWriter.WriteLine(Space(2) + "")
                'objWriter.WriteLine(Space(2) + "Page No." & mintPageNo & Space(14) & "Attendance Report for " & DateEdit1.DateTime.ToString("dd/MM/yyyy") & Space(6) & "Run Date & Time :" & Now().ToString("dd/MM/yyyy HH:mm"))
                objWriter.WriteLine(Space(2) + "Page No." & mintPageNo & Space(14) & "Attendance Report for " & ForDate & Space(6) & "Run Date & Time :" & Common.runningDateTime)
                objWriter.WriteLine(Space(2) + "-----------------------------------------------------------------------------------------------")
                objWriter.WriteLine(Space(2) + "Srl  EmpCode      CardNo       Employee Name             Shift Start   In     Shift  Status")
                objWriter.WriteLine(Space(2) + "No.                                                                           Late")
                objWriter.WriteLine(Space(2) + "----------------------------------------------------------------------------------------------")
                mintLine = 8
                mintPageNo = mintPageNo + 1
            End If
            'If blnDeptAvailable And Not .EOF Then
            '    If Left(strsortorder, 10) = "Department" And strDeptDivCode <> !DepartmentCode Then
            '        If mintLine + 3 > g_LinesPerPage Then GoTo 1
            '        Print #mFileNumber, Space(2) + ""
            '        strDeptDivCode = !DepartmentCode
            '        Print #mFileNumber, Space(2) + "** Department Code & Name : " & !DepartmentCode & "  " & !DepartmentName
            '        Print #mFileNumber, Space(2) + ""
            '        mintLine = mintLine + 3
            '    ElseIf Left(strsortorder, 8) = "Division" And strDeptDivCode <> !DivisionCode Then
            '        If mintLine + 3 > g_LinesPerPage Then GoTo 1
            '        Print #mFileNumber, Space(2) + ""
            '        strDeptDivCode = !DivisionCode
            '        Print #mFileNumber, Space(2) + "** Section Code & Name : " & !DivisionCode & "  " & !DivisionName
            '        Print #mFileNumber, Space(2) + ""
            '        mintLine = mintLine + 3
            '    ElseIf Left(strsortorder, 3) = "Cat" And strDeptDivCode <> !Cat Then
            '        If mintLine + 3 > g_LinesPerPage Then GoTo 1
            '        Print #mFileNumber, Space(2) + ""
            '        strDeptDivCode = !Cat
            '        Print #mFileNumber, Space(2) + "** Category Code & Name : " & !Cat & "  " & !CatagoryName
            '        Print #mFileNumber, Space(2) + ""
            '        mintLine = mintLine + 3
            '    End If
            'End If

            If Rs_Report.Tables(0).Rows(i).Item("In1").ToString.Trim = "" Then
                v_InTime = ""
            Else
                v_InTime = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("In1").ToString.Trim).ToString("HH:mm") & "  "
            End If
            If Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim = "" Or Val(Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim) = 0 Then
                v_Late = ""
            Else
                v_Late = Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim Mod 60).ToString("00")
            End If

            strStatus = ""
            If Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim <> "" Then
                If UCase(Trim(Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim)) = "MIS" Then
                    strStatus = "P"
                Else
                    strStatus = Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim
                End If
            End If



            'objWriter.WriteLine(Space(2) & msrl.ToString.PadRight(12) & " " & Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim.PadRight(12) & " " & Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim & " " & Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim.PadRight(25) & " " & Rs_Report.Tables(0).Rows(i).Item("ShiftAttended").ToString.Trim & "  " & _
            '                    IIf(Rs_Report.Tables(0).Rows(i).Item("shiftStartTime").ToString.Trim = "", Space(5), Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("shiftStartTime").ToString.Trim).ToString("HH:mm")) & "  " & IIf(Trim(v_InTime) = "", Space(6), v_InTime) & " " & IIf(Trim(v_Late) = "", Space(6), Trim(v_Late)) & "  " & strStatus)

            objWriter.WriteLine("")
            objWriter.Write(Space(2) & msrl.ToString.PadRight(4) & " " & Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim.PadRight(12) & " " & Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim.PadRight(12) & " " & Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim.PadRight(25) & " " & Rs_Report.Tables(0).Rows(i).Item("ShiftAttended").ToString.Trim.PadRight(3) & "  ")
            If Rs_Report.Tables(0).Rows(i).Item("shiftStartTime").ToString.Trim = "" Then
                objWriter.Write(Space(9))
            Else
                objWriter.Write(Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("shiftStartTime").ToString.Trim).ToString("HH:mm") & "  ")
            End If
            If Trim(v_InTime) = "" Then
                objWriter.Write(Space(9))
            Else
                objWriter.Write(v_InTime.PadRight(6) & "  ")
            End If
            If Trim(v_Late) = "" Then
                objWriter.Write(Space(10))
            Else
                objWriter.Write(Trim(v_Late.PadRight(6)) & "     ")
            End If
            objWriter.Write(strStatus.PadRight(3))

            msrl = msrl + 1
            mintLine = mintLine + 1
            mblnCheckReport = True
            '    .MoveNext()
            'Loop
        Next
        objWriter.Close()

        If isPdf = True Then
            'pdf start 
            Common.pdfreport(mstrFile_Name, 10, ulf)
            'pdf end 
        Else
            Try : Process.Start(mstrFile_Name) : Catch ex As Exception : Process.Start("notepad.exe", mstrFile_Name) : End Try
        End If
    End Sub
    Sub Spot_DepartmentSummary()
        Dim intFile As Integer
        Dim strsql As String

        Dim strDepartmentName As String
        Dim strDepartmentCode As String

        Dim dblLeaveAmount As Double
        Dim dblPresentValue As Double
        Dim dblAbsentValue As Double
        Dim dblLeaveValue As Double
        Dim dblWo_Value As Double

        Dim dblPresentValue1 As Double
        Dim dblAbsentValue1 As Double
        Dim dblLeaveValue1 As Double
        Dim dblWo_Value1 As Double

        Dim lngOnDutyCount As Long
        Dim strOnDutyCount7 As String '* 7
        Dim lngTotalEmpCount As Long

        Dim GtotalEmp As Double
        Dim gTotalAbs As Double
        Dim gTotablLeave As Double
        Dim gTotalPre As Double
        Dim gTotalWO As Double
        Dim gTotalOD As Double
        mblnCheckReport = False
        Dim msrl As Integer
        msrl = 1

        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet

        mintPageNo = 1
        mintLine = 1
        If Common.servername = "Access" Then
            'strsql = " Select tblCatagory.Catagoryname, tblDivision.DivisionName, tblTimeregister.Wo_Value,tblTimeregister.PresentValue, tblTimeregister.Shift,tblTimeregister.AbsentValue,tblTimeregister.LeaveValue,tblTimeregister.LeaveAmount,TblDepartment.DepartmentName,TblDepartment.DepartmentCode,tblEmployee.PresentCardNo " & _
            '     " from  tblCatagory, tblDivision,  tblTimeregister ,tblEmployee ,tblDepartment,TBLCOMPANY " & _
            '     " Where tblEmployee.Companycode = tblCompany.Companycode AND tblEmployee.CAT = tblCatagory.cat And tblEmployee.DivisionCode = tblDivision.DivisionCode And tblTimeregister.PayCode = TblEmployee.PayCode And TblEmployee.DepartmentCode = TblDepartment.DepartmentCode" & g_HODDepartmentCode & _
            '     " AND FORMAT(tbltimeregister.DateOffice,'MMM DD YYYY') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' And  (tblemployee.LeavingDate >=#" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "# or tblemployee.LeavingDate is null)" & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause)

            strsql = " Select tblCatagory.Catagoryname, tblDivision.DivisionName, tblTimeregister.Wo_Value,tblTimeregister.PresentValue, tblTimeregister.Shift,tblTimeregister.AbsentValue,tblTimeregister.LeaveValue,tblTimeregister.LeaveAmount,TblDepartment.DepartmentName,TblDepartment.DepartmentCode,tblEmployee.PresentCardNo " & _
                " from  tblCatagory, tblDivision,  tblTimeregister ,tblEmployee ,tblDepartment,TBLCOMPANY " & _
                " Where tblEmployee.Companycode = tblCompany.Companycode AND tblEmployee.CAT = tblCatagory.cat And tblTimeregister.PayCode = TblEmployee.PayCode And TblEmployee.DepartmentCode = TblDepartment.DepartmentCode" & g_HODDepartmentCode & _
                " AND FORMAT(tbltimeregister.DateOffice,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & " order by tblDepartment.DEPARTMENTCODE"

            adapA = New OleDbDataAdapter(strsql, Common.con1)
            adapA.Fill(Rs_Report)
        Else
            strsql = " Select tblCatagory.Catagoryname, tblDivision.DivisionName, tblTimeregister.Wo_Value,tblTimeregister.PresentValue, tblTimeregister.Shift,tblTimeregister.AbsentValue,tblTimeregister.LeaveValue,tblTimeregister.LeaveAmount,TblDepartment.DepartmentName,TblDepartment.DepartmentCode,tblEmployee.PresentCardNo " & _
                 " from  tblCatagory, tblDivision,  tblTimeregister ,tblEmployee ,tblDepartment,TBLCOMPANY " & _
                 " Where tblEmployee.Companycode = tblCompany.Companycode AND tblEmployee.CAT = tblCatagory.cat And tblTimeregister.PayCode = TblEmployee.PayCode And TblEmployee.DepartmentCode = TblDepartment.DepartmentCode" & g_HODDepartmentCode & _
                 " AND tbltimeregister.DateOffice = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & " order by tblDepartment.DEPARTMENTCODE"
            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(Rs_Report)
        End If
        'Rs_Report = Cn.Execute(strsql)

        'nitin test
        'Dim adap1 As SqlDataAdapter
        'Dim adapA1 As OleDbDataAdapter
        'Dim deptds As DataSet = New DataSet
        'Dim sSql As String
        'If PopupContainerEditDept.EditValue = "" Then
        '    sSql = "SELECT DISTINCT(DEPARTMENTCODE) ,DepartmentName from tblDepartment"
        'Else
        '    Dim dept() As String = PopupContainerEditDept.EditValue.ToString.Split(",")
        '    Dim ls As New List(Of String)()
        '    For x As Integer = 0 To dept.Length - 1
        '        ls.Add(dept(x).Trim)
        '    Next
        '    'select PAYCODE from TblEmployee where COMPANYCODE IN ('01')  and ACTIVE = 'Y'
        '    sSql = "select DEPARTMENTCODE, DepartmentName from tblDepartment where DEPARTMENTCODE IN ('" & String.Join("', '", ls.ToArray()) & "')"
        'End If
        'If Common.servername = "Access" Then
        '    adapA1 = New OleDbDataAdapter(sSql, Common.con1)
        '    adapA1.Fill(deptds)
        'Else
        '    adap1 = New SqlDataAdapter(sSql, Common.con)
        '    adap1.Fill(deptds)
        'End If
        'nitin test end

        If Rs_Report.Tables(0).Rows.Count < 1 Then
            'G_Report_Error = False
            'Screen.MousePointer = vbDefault
            XtraMessageBox.Show(ulf, "<size=10>No Data Available For this Report.</size>", "<size=9>iAS</size>")
            'mReportPrintStatus = False
            Exit Sub
        End If
        'Rs_Report.Sort = "DepartmentCode"
        intFile = FreeFile()

        mFileNumber = FreeFile()
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".SRP"
        Dim objWriter As New System.IO.StreamWriter(mstrFile_Name, True)


        objWriter.WriteLine(Space(2) & Space(20) & CommonReport.g_CompanyNames)
        objWriter.WriteLine(Space(2) + "")
        objWriter.WriteLine(Space(2) + "Page No." & mintPageNo)
        objWriter.WriteLine(Space(2) + "                  Run Date & Time : " & Common.runningDateTime) 'Format(Now(), "dd/MM/yyyy HH:mm"))
        objWriter.WriteLine(Space(2) + "")
        objWriter.WriteLine(Space(2) + "                                 Department Summary for " & ForDate) 'DateEdit1.DateTime.ToString("dd/MM/yyyy"))
        objWriter.WriteLine(Space(2) + "---------------------------------------------------------------------------------------------------------")
        objWriter.WriteLine(Space(2) + "Srl  Department                      Total    Present  On     Absent   Leave    Weekly")
        objWriter.WriteLine(Space(2) + "No.  Code Name                       Employee          Duty                     Off")
        objWriter.WriteLine(Space(2) + "---------------------------------------------------------------------------------------------------------")
        mintLine = 10
        'With Rs_Report
        Dim count As Integer = 0
        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            'Do While Not .EOF
            dblPresentValue = 0
            dblAbsentValue = 0
            dblLeaveValue = 0
            dblWo_Value = 0

            lngTotalEmpCount = 0
            lngOnDutyCount = 0
            strDepartmentName = Rs_Report.Tables(0).Rows(i).Item("DepartmentName").ToString.Trim
            strDepartmentCode = Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim

            Do While strDepartmentCode = Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim
                dblPresentValue1 = 0
                dblAbsentValue1 = 0
                dblLeaveValue1 = 0
                dblWo_Value1 = 0
                dblLeaveAmount = 0

                If Not (Rs_Report.Tables(0).Rows(i).Item("PRESENTVALUE").ToString.Trim = "") Then dblPresentValue1 = Rs_Report.Tables(0).Rows(i).Item("PRESENTVALUE").ToString.Trim
                If Not (Rs_Report.Tables(0).Rows(i).Item("ABSENTVALUE").ToString.Trim = "") Then dblAbsentValue1 = Rs_Report.Tables(0).Rows(i).Item("ABSENTVALUE").ToString.Trim
                If Not (Rs_Report.Tables(0).Rows(i).Item("LEAVEVALUE").ToString.Trim = "") Then dblLeaveValue1 = Rs_Report.Tables(0).Rows(i).Item("LEAVEVALUE").ToString.Trim
                If Not (Rs_Report.Tables(0).Rows(i).Item("WO_VALUE").ToString.Trim = "") Then dblWo_Value1 = Rs_Report.Tables(0).Rows(i).Item("WO_VALUE").ToString.Trim
                If Not (Rs_Report.Tables(0).Rows(i).Item("leaveamount").ToString.Trim = "") Then dblLeaveAmount = Rs_Report.Tables(0).Rows(i).Item("leaveamount").ToString.Trim

                If dblPresentValue1 = 1 And dblLeaveAmount > 0 Then lngOnDutyCount = lngOnDutyCount + 1
                If Not (dblPresentValue1 = 1 And dblLeaveAmount > 0) Then
                    dblPresentValue = dblPresentValue + dblPresentValue1
                End If
                dblAbsentValue = dblAbsentValue + dblAbsentValue1
                dblLeaveValue = dblLeaveValue + dblLeaveValue1
                dblWo_Value = dblWo_Value + dblWo_Value1

                lngTotalEmpCount = lngTotalEmpCount + 1
                'Continue For
                '.MoveNext()
                'If .EOF Then
                '    Exit Do
                'End If
                i = i + 1
                If i > Rs_Report.Tables(0).Rows.Count - 1 Then
                    i = i - 1
                    GoTo tmp
                    Exit Do
                End If
                'count = count + 1
            Loop
            i = i - 1
tmp:        GtotalEmp = GtotalEmp + lngTotalEmpCount
            gTotalAbs = gTotalAbs + dblAbsentValue
            gTotablLeave = gTotablLeave + dblLeaveValue
            gTotalPre = gTotalPre + dblPresentValue
            gTotalWO = gTotalWO + dblWo_Value
            gTotalOD = gTotalOD + lngOnDutyCount
            'objWriter.WriteLine(Space(2) & msrl.ToString.PadRight(4) & " " & strDepartmentCode.PadRight(3) & Space(2) & strDepartmentName.PadRight(25) & Space(1) & _
            '                Space(1) & Trim(Length7(CDbl(lngTotalEmpCount))) & Space(2) & Space(1) & Trim(Length5(dblPresentValue)) & Space(2) & _
            '                Space(1) & Trim(Length7(CDbl(lngOnDutyCount))) & Space(2) & Space(1) & Trim(Length5(dblAbsentValue)) & Space(4) & Space(1) & Trim(Length5(dblLeaveValue)) & Space(4) & _
            '                Space(1) & Trim(Length5(dblWo_Value)))

            objWriter.WriteLine(Space(2) & msrl.ToString.PadRight(4) & " " & strDepartmentCode.PadRight(3) & Space(2) & strDepartmentName.PadRight(25) & Space(1) & _
                           Space(1) & lngTotalEmpCount.ToString.PadRight(5) & Space(2) & Space(3) & dblPresentValue.ToString.PadRight(5) & Space(2) & _
                           Space(1) & lngOnDutyCount.ToString.PadRight(5) & Space(2) & Space(1) & dblAbsentValue.ToString.PadRight(5) & Space(4) & Space(1) & dblLeaveValue.ToString.PadRight(5) & Space(4) & _
                           Space(1) & dblWo_Value.ToString.PadRight(5))

            msrl = msrl + 1
            mintLine = mintLine + 1

            If mintLine >= g_LinesPerPage Then 'And Not .EOF Then
                mintPageNo = mintPageNo + 1
                objWriter.WriteLine(Space(2) + vbFormFeed) 'Don't count
                objWriter.WriteLine(Space(2) + Space(40), CommonReport.g_CompanyNames)
                'Print #intFile, Space(2) + Space(centerAllign(Len(CommonReport.g_CompanyNames), Len("----------------------------------------------------------------------------------------------------"))) & CommonReport.g_CompanyNames
                objWriter.WriteLine(Space(2) + "")
                objWriter.WriteLine(Space(2) + "Page No." & mintPageNo)
                objWriter.WriteLine(Space(2) + "                                                                  Run Date & Time : " & Format(Now(), "dd/MM/yyyy HH:mm"))
                objWriter.WriteLine(Space(2) + "")
                objWriter.WriteLine(Space(2) + "                                 Department Summary for " & DateEdit1.DateTime.ToString("dd/MM/yyyy"))
                objWriter.WriteLine(Space(2) + "----------------------------------------------------------------------------------------------------")
                objWriter.WriteLine(Space(2) + "Department                       Total    Present  On     Absent   Leave    Weekly")
                objWriter.WriteLine(Space(2) + "Code Name                        Employee          Duty                     Off")
                objWriter.WriteLine(Space(2) + "----------------------------------------------------------------------------------------------------")
                mintLine = 10
            End If
            'Loop
        Next
        objWriter.WriteLine("")
        objWriter.WriteLine("")
        objWriter.WriteLine(Space(28) & "Total: " & Space(3) & Space(1) & GtotalEmp.ToString.PadRight(5) & Space(2) & Space(3) & gTotalPre.ToString.PadRight(5) & Space(2) & _
                    Space(1) & gTotalOD.ToString.PadRight(5) & Space(2) & Space(1) & gTotalAbs.ToString.PadRight(5) & Space(4) & Space(1) & gTotablLeave.ToString.PadRight(5) & Space(4) & _
                    Space(1) & gTotalWO.ToString.PadRight(5))
        'End With

        'Close #intFile

        mblnCheckReport = True
        objWriter.Close()
        If isPdf = True Then
            'pdf start 
            Common.pdfreport(mstrFile_Name, 10, ulf)
            'pdf end 
        Else
            Try : Process.Start(mstrFile_Name) : Catch ex As Exception : Process.Start("notepad.exe", mstrFile_Name) : End Try
        End If
    End Sub
    Sub Spot_EarlyArrival(strsortorder As String)
        Dim strsql As String, strDeptDivCode As String
        Dim v_Late As String '* 6
        Dim blnDeptAvailable As Boolean, mFirst As Boolean
        Dim msrl As Integer
        msrl = 1

        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If

        mstrDepartmentCode = " "
        mintPageNo = 1
        mintLine = 1
        mblnCheckReport = False
        blnDeptAvailable = False
        mFirst = True

        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet

        'If Left(strsortorder, 1) = "D" Or Left(strsortorder, 1) = "C" Then blnDeptAvailable = True
        Dim g_HODDepartmentCode As String 'nitin
        If strsortorder = "" Then
            If Common.servername = "Access" Then
                'strsql = " Select tblCatagory.Catagoryname, tblDivision.DivisionName, tblTimeregister.PAYCODE,  tblTimeregister.Shift,tblDepartment.DepartmentCode, tblDepartment.DepartmentName,tblEmployee.PresentCARDNO, tblEmployee.EMPNAME, tblTimeregister.DATEOFFICE, " & _
                '     "tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, " & _
                '     "tblTimeregister.SHIFTATTENDED, tblTimeregister.shiftStartTime, tblTimeregister.In1, tblTimeregister.Earlyarrival, " & _
                '     "iif(tblTimeregister.Earlyarrival <= 10 ,'**', '  ') as Late1, " & _
                '     "iif(tblTimeregister.Earlyarrival > 10 And tblTimeregister.Earlyarrival <= 10, '**','  ') AS  Late10, " & _
                '     "iif(tblTimeregister.Earlyarrival > 30 And tblTimeregister.Earlyarrival <= 30 , '**' , '  ') as Late30, " & _
                '     "iif(tblTimeregister.Earlyarrival > 60 , '**' , '  ') as Late60 , tblEmployee.BUS " & _
                '     "From  tblCatagory, tblDivision,tblTimeregister, tblEmployee, tblDepartment, tblCompany " & _
                '     "Where tblEmployee.CompanyCode=tblCompany.CompanyCode and tblEmployee.CAT = tblCatagory.cat And tblEmployee.DivisionCode = tblDivision.DivisionCode And tblEmployee.PayCode = tblTimeregister.PayCode And " & _
                '     "tblEmployee.DepartmentCode = tblDepartment.DepartmentCode AND format(tbltimeregister.DateOffice,'MMM dd yyyy') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' AND  " & _
                '     "tblTimeregister.Earlyarrival > 0" & g_HODDepartmentCode & _
                '     " And (tblemployee.LeavingDate >=#" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "# or tblemployee.LeavingDate is null)" & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause)

                strsql = " Select tblCatagory.Catagoryname, tblDivision.DivisionName, tblTimeregister.PAYCODE,  tblTimeregister.Shift,tblDepartment.DepartmentCode, tblDepartment.DepartmentName,tblEmployee.PresentCARDNO, tblEmployee.EMPNAME, tblTimeregister.DATEOFFICE, " & _
                     "tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, " & _
                     "tblTimeregister.SHIFTATTENDED, tblTimeregister.shiftStartTime, tblTimeregister.In1, tblTimeregister.Earlyarrival, " & _
                     "iif(tblTimeregister.Earlyarrival <= 10 ,'**', '  ') as Late1, " & _
                     "iif(tblTimeregister.Earlyarrival > 10 And tblTimeregister.Earlyarrival <= 10, '**','  ') AS  Late10, " & _
                     "iif(tblTimeregister.Earlyarrival > 30 And tblTimeregister.Earlyarrival <= 30 , '**' , '  ') as Late30, " & _
                     "iif(tblTimeregister.Earlyarrival > 60 , '**' , '  ') as Late60 , tblEmployee.BUS " & _
                     "From  tblCatagory, tblDivision,tblTimeregister, tblEmployee, tblDepartment, tblCompany " & _
                     "Where tblEmployee.CompanyCode=tblCompany.CompanyCode and tblEmployee.CAT = tblCatagory.cat And tblEmployee.PayCode = tblTimeregister.PayCode And " & _
                     "tblEmployee.DepartmentCode = tblDepartment.DepartmentCode AND format(tbltimeregister.DateOffice,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' AND  " & _
                     "tblTimeregister.Earlyarrival > 0" & g_HODDepartmentCode & _
                     " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & " order by tblEmployee.PresentCARDNO, TblTimeRegister.DateOffice"
                If Common.IsCompliance Then
                    strsql = Microsoft.VisualBasic.Strings.Replace(strsql, "tbltimeregister", "tblTimeRegisterD", 1, -1, Microsoft.VisualBasic.Constants.vbTextCompare)
                End If
                adapA = New OleDbDataAdapter(strsql, Common.con1)
                adapA.Fill(Rs_Report)
            Else
                strsql = " Select tblCatagory.Catagoryname, tblDivision.DivisionName, tblTimeregister.PAYCODE,  tblTimeregister.Shift,tblDepartment.DepartmentCode, tblDepartment.DepartmentName,tblEmployee.PresentCARDNO, tblEmployee.EMPNAME, tblTimeregister.DATEOFFICE, " & _
                        "tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, " & _
                        "tblTimeregister.SHIFTATTENDED, tblTimeregister.shiftStartTime, tblTimeregister.In1, tblTimeregister.Earlyarrival, " & _
                        "Late1 = Case When tblTimeregister.Earlyarrival <= 10 then '**' Else '  'End, " & _
                        "Late10 = Case When tblTimeregister.Earlyarrival > 10 And tblTimeregister.Earlyarrival <= 30 then '**' Else '  ' End, " & _
                        "Late30 = Case When tblTimeregister.Earlyarrival > 30 And tblTimeregister.Earlyarrival <= 60 then '**' Else '  ' End, " & _
                        "Late60 = Case When tblTimeregister.Earlyarrival > 60 then '**' Else '  ' End, tblEmployee.BUS " & _
                        "From  tblCatagory, tblDivision,tblTimeregister, tblEmployee, tblDepartment, tblCompany " & _
                        "Where tblEmployee.CompanyCode=tblCompany.CompanyCode and tblEmployee.CAT = tblCatagory.cat And tblEmployee.PayCode = tblTimeregister.PayCode And " & _
                        "tblEmployee.DepartmentCode = tblDepartment.DepartmentCode AND tbltimeregister.DateOffice = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' AND  " & _
                        "tblTimeregister.Earlyarrival > 0" & g_HODDepartmentCode & _
                        " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & " order by tblEmployee.PresentCARDNO, TblTimeRegister.DateOffice"
                If Common.IsCompliance Then
                    strsql = Microsoft.VisualBasic.Strings.Replace(strsql, "tbltimeregister", "tblTimeRegisterD", 1, -1, Microsoft.VisualBasic.Constants.vbTextCompare)
                End If
                adap = New SqlDataAdapter(strsql, Common.con)
                adap.Fill(Rs_Report)
            End If
            'Rs_Report = Cn.Execute(strsql)
            'Rs_Report.Sort = "PayCode"
        Else
            'Rs_Report.Sort = strsortorder
        End If
        If Rs_Report.Tables(0).Rows.Count < 1 Then
            'G_Report_Error = False
            'Screen.MousePointer = vbDefault
            XtraMessageBox.Show(ulf, "<size=10>Nobody has come early on this date.</size>", "<size=9>iAS</size>")
            'mReportPrintStatus = False
            Exit Sub
        End If

        mFileNumber = FreeFile()
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".SRP"
        Dim objWriter As New System.IO.StreamWriter(mstrFile_Name, True)

        'With Rs_Report
        '.MoveFirst()
        'Do While Not .EOF
        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            'If mFirst Or mintLine > g_LinesPerPage Or (Left(strsortorder, 10) = "Department" And strDeptDivCode <> !DepartmentCode And g_SkipAfterDept) Then
            If mFirst Or mintLine > g_LinesPerPage Or (strsortorder.Trim = "Department" And strDeptDivCode <> Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim And g_SkipAfterDept) Then
1:              If Not mFirst Then
                    objWriter.WriteLine(Space(2) + vbFormFeed)
                Else
                    mFirst = False
                End If
                objWriter.WriteLine(Space(2) & Space(40) & CommonReport.g_CompanyNames)
                objWriter.WriteLine(Space(2) & "")
                objWriter.WriteLine(Space(2) & " ")
                'objWriter.WriteLine(Space(2) & "Page No." & mintPageNo & Space(35) & "Early Arrival Report for " & DateEdit1.DateTime.ToString("dd/MM/yyyy") & Space(8) & "Run Date & Time :" & Now.ToString("dd/MM/yyyy HH:mm"))
                objWriter.WriteLine(Space(2) & "Page No." & mintPageNo & Space(35) & "Early Arrival Report for " & ForDate & Space(8) & "Run Date & Time :" & Common.runningDateTime)
                objWriter.WriteLine(Space(2) & "-----------------------------------------------------------------------------------------------------------------------------")
                objWriter.WriteLine(Space(2) & "Srl  EmpCode      Cardno       Employee Name             Shift Start In    Shift  ------------- Early -----------  Bus Route")
                objWriter.WriteLine(Space(2) & "No.                                                                        Early  >(0.01) >(0.10) >(0.30) >(1.00)")
                objWriter.WriteLine(Space(2) & "-----------------------------------------------------------------------------------------------------------------------------")
                mintLine = 8
                mintPageNo = mintPageNo + 1
            End If
            'If blnDeptAvailable And Not .EOF Then
            '    If Left(strsortorder, 10) = "Department" And strDeptDivCode <> !DepartmentCode Then
            '        If mintLine + 3 > g_LinesPerPage Then GoTo 1
            '        Print #mFileNumber, Space(2) + ""
            '        strDeptDivCode = !DepartmentCode
            '        Print #mFileNumber, Space(2) + "** Department Code & Name : " & !DepartmentCode & "  " & !DepartmentName
            '        Print #mFileNumber, Space(2) + ""
            '        mintLine = mintLine + 3
            '    ElseIf Left(strsortorder, 8) = "Division" And strDeptDivCode <> !DivisionCode Then
            '        If mintLine + 3 > g_LinesPerPage Then GoTo 1
            '        Print #mFileNumber, Space(2) + ""
            '        strDeptDivCode = !DivisionCode
            '        Print #mFileNumber, Space(2) + "** Section Code & Name : " & !DivisionCode & "  " & !DivisionName
            '        Print #mFileNumber, Space(2) + ""
            '        mintLine = mintLine + 3
            '    ElseIf Left(strsortorder, 3) = "Cat" And strDeptDivCode <> !Cat Then
            '        If mintLine + 3 > g_LinesPerPage Then GoTo 1
            '        Print #mFileNumber, Space(2) + ""
            '        strDeptDivCode = !Cat
            '        Print #mFileNumber, Space(2) + "** Category Code & Name : " & !Cat & "  " & !CatagoryName
            '        Print #mFileNumber, Space(2) + ""
            '        mintLine = mintLine + 3
            '    End If
            'End If

            'v_Late = Length5(Min2Hr(!EARLYARRIVAL))
            If Rs_Report.Tables(0).Rows(i).Item("EARLYARRIVAL").ToString.Trim = "" Then
                v_Late = ""
            Else
                v_Late = Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("EARLYARRIVAL").ToString.Trim / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("EARLYARRIVAL").ToString.Trim Mod 60).ToString("00")
            End If

            objWriter.WriteLine(Space(2) & msrl.ToString.PadRight(4) & " " & Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim.PadRight(12) & " " & Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim.PadRight(12) & " " & _
                                Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim.PadRight(25) & " " & Rs_Report.Tables(0).Rows(i).Item("ShiftAttended").ToString.Trim.PadRight(3) & "   " & _
                                Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("shiftStartTime").ToString.Trim).ToString("HH:mm") & " " & Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("In1").ToString.Trim).ToString("HH:mm") & " " & _
                                Space(5 - Len(Trim(v_Late))) & Trim(v_Late) & "    " & Rs_Report.Tables(0).Rows(i).Item("Late1").ToString.Trim & "      " & Rs_Report.Tables(0).Rows(i).Item("Late10").ToString.Trim & "      " & Rs_Report.Tables(0).Rows(i).Item("Late30").ToString.Trim & "      " & Rs_Report.Tables(0).Rows(i).Item("Late60").ToString.Trim & "    " & Rs_Report.Tables(0).Rows(i).Item("bus").ToString.Trim & " ")

            msrl = msrl + 1
            mintLine = mintLine + 1
            mblnCheckReport = True
            '.MoveNext()
            'Loop
        Next
        objWriter.Close()
        If isPdf = True Then
            'pdf start 
            Common.pdfreport(mstrFile_Name, 8, ulf)
            'pdf end 
        Else
            Try : Process.Start(mstrFile_Name) : Catch ex As Exception : Process.Start("notepad.exe", mstrFile_Name) : End Try
        End If
    End Sub
    Sub Spot_ShiftWisePresence(strsortorder As String)
        'On Error GoTo ErrorGen
        Dim strsql As String, strDeptDivCode As String
        Dim v_Shift As String '* 5
        Dim v_Start As String '* 5
        Dim v_InTime As String '* 8
        Dim v_Late As String '* 5
        Dim v_Status As String '* 5
        Dim blnDeptAvailable As Boolean, mFirst As Boolean
        Dim msrl As Integer
        msrl = 1

        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If

        mstrDepartmentCode = " "
        mintPageNo = 1
        mintLine = 1
        mblnCheckReport = False
        blnDeptAvailable = False
        mFirst = True
        Dim g_HODDepartmentCode As String = ""   'nitin
        'If Left(strsortorder, 1) = "D" Or Left(strsortorder, 1) = "C" Then blnDeptAvailable = True
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet

        If strsortorder = "" Then
            If Common.servername = "Access" Then
                'strsql = " Select tblTimeregister.PAYCODE, tblTimeregister.Shiftattended, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, tblDepartment.DepartmentCode, tblDepartment.DepartmentName,tblEmployee.PresentCARDNO, tblEmployee.EMPNAME, tblTimeregister.DATEOFFICE, " & _
                '     " tblTimeregister.SHIFTATTENDED, tblTimeregister.ShiftStartTime, tblTimeregister.In1, tblTimeregister.LateArrival, tblTimeregister.Status, tblCatagory.Catagoryname, tblDivision.DivisionName " & _
                '     " From  tblTimeregister, tblEmployee, tblDepartment, tblCatagory, tblDivision,tblCompany " & _
                '     " Where tblEmployee.PayCode = tblTimeregister.PayCode And " & _
                '     " tblEmployee.Companycode=tblCompany.Companycode and tblEmployee.CAT = tblCatagory.cat And " & _
                '     " tblEmployee.DivisionCode = tblDivision.DivisionCode AND FORMAT(tbltimeregister.DateOffice,'MMM DD YYYY') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' And " & _
                '     " tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
                '     " And (tblemployee.LeavingDate >=#" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "# or tblemployee.LeavingDate is null)" & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & _
                '     " order by tblTimeregister.Shiftattended,tblEmployee.DepartmentCode,tblTimeregister.PayCode"

                strsql = " Select tblTimeregister.PAYCODE, tblTimeregister.Shiftattended, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, tblDepartment.DepartmentCode, tblDepartment.DepartmentName,tblEmployee.PresentCARDNO, tblEmployee.EMPNAME, tblTimeregister.DATEOFFICE, " & _
                    " tblTimeregister.SHIFTATTENDED, tblTimeregister.ShiftStartTime, tblTimeregister.In1, tblTimeregister.LateArrival, tblTimeregister.Status, tblCatagory.Catagoryname, tblDivision.DivisionName " & _
                    " From  tblTimeregister, tblEmployee, tblDepartment, tblCatagory, tblDivision,tblCompany " & _
                    " Where tblEmployee.PayCode = tblTimeregister.PayCode And " & _
                    " tblEmployee.Companycode=tblCompany.Companycode and tblEmployee.CAT = tblCatagory.cat And " & _
                    " FORMAT(tbltimeregister.DateOffice,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' And " & _
                    " tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
                    " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & _
                    " order by tblTimeregister.Shiftattended,tblEmployee.DepartmentCode,tblTimeregister.PresentCardNo"
                If Common.IsCompliance Then
                    strsql = Microsoft.VisualBasic.Strings.Replace(strsql, "tbltimeregister", "tblTimeRegisterD", 1, -1, Microsoft.VisualBasic.Constants.vbTextCompare)
                End If
                adapA = New OleDbDataAdapter(strsql, Common.con1)
                adapA.Fill(Rs_Report)
            Else
                strsql = " Select tblTimeregister.PAYCODE, tblTimeregister.Shiftattended, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, tblDepartment.DepartmentCode, tblDepartment.DepartmentName,tblEmployee.PresentCARDNO, tblEmployee.EMPNAME, tblTimeregister.DATEOFFICE, " & _
                         " tblTimeregister.SHIFTATTENDED, tblTimeregister.ShiftStartTime, tblTimeregister.In1, tblTimeregister.LateArrival, tblTimeregister.Status, tblCatagory.Catagoryname, tblDivision.DivisionName " & _
                         " From  tblTimeregister, tblEmployee, tblDepartment, tblCatagory, tblDivision,tblCompany " & _
                         " Where tblEmployee.PayCode = tblTimeregister.PayCode And " & _
                         " tblEmployee.Companycode=tblCompany.Companycode and tblEmployee.CAT = tblCatagory.cat And " & _
                         " tbltimeregister.DateOffice = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' And " & _
                         " tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
                         " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & _
                         " order by tblTimeregister.Shiftattended,tblEmployee.DepartmentCode,tblTimeregister.PresentCardNo"
                If Common.IsCompliance Then
                    strsql = Microsoft.VisualBasic.Strings.Replace(strsql, "tbltimeregister", "tblTimeRegisterD", 1, -1, Microsoft.VisualBasic.Constants.vbTextCompare)
                End If
                adap = New SqlDataAdapter(strsql, Common.con)
                adap.Fill(Rs_Report)
            End If
            'Rs_Report = Cn.Execute(strsql)
            If Rs_Report.Tables(0).Rows.Count < 1 Then
                'G_Report_Error = False
                'Screen.MousePointer = vbDefault
                XtraMessageBox.Show(ulf, "<size=10>No Data Available For this Report.</size>", "<size=9>iAS</size>")
                'mReportPrintStatus = False
                Exit Sub
            End If
            'Rs_Report.Sort = "ShiftAttended,PayCode"
        Else
            'Rs_Report.Sort = "ShiftAttended," & strsortorder
        End If


        mFileNumber = FreeFile()
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".SRP"
        Dim objWriter As New System.IO.StreamWriter(mstrFile_Name, True)
        'With Rs_Report
        '    .MoveFirst()
        '    Do While Not .EOF
        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            'If mFirst Or mintLine > g_LinesPerPage Or (Left(strsortorder, 10) = "Department" And mstrDepartmentCode <> !DepartmentCode And g_SkipAfterDept) Then
            If mFirst Or mintLine > g_LinesPerPage Or (strsortorder.Trim = "Department" And strDeptDivCode <> Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim And g_SkipAfterDept) Then
1:              If Not mFirst Then
                    objWriter.WriteLine(vbFormFeed)
                Else
                    mFirst = False
                End If
                objWriter.WriteLine(Space(25) & CommonReport.g_CompanyNames)
                objWriter.WriteLine("")
                objWriter.WriteLine("")
                'objWriter.WriteLine("Page No." & mintPageNo & Space(12) & "Shift Wise Report For date:-" & DateEdit1.DateTime.ToString("dd/MM/yyyy") & Space(8) & "Run Date & Time :" & Now().ToString("dd/MM/yyyy HH:mm"))
                objWriter.WriteLine("Page No." & mintPageNo & Space(12) & "Shift Wise Report For date:-" & ForDate & Space(8) & "Run Date & Time :" & Common.runningDateTime)
                objWriter.WriteLine("----------------------------------------------------------------------------------------------------")
                objWriter.WriteLine("Sr.No. Payroll      Card No      Employee's Name        Shift    Start     In      Late    Status")
                objWriter.WriteLine("----------------------------------------------------------------------------------------------------")
                mintLine = 9
                mintPageNo = mintPageNo + 1
            End If

            If mShift <> Rs_Report.Tables(0).Rows(i).Item("ShiftAttended").ToString.Trim Then
                mShift = Rs_Report.Tables(0).Rows(i).Item("ShiftAttended").ToString.Trim
                objWriter.WriteLine("*** Shift : " & mShift)
                objWriter.WriteLine("")
            End If
            If strsortorder.Trim = "Department" Then
                If mstrDepartmentCode <> Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim Then
                    objWriter.WriteLine("*** Department Code & Name " & Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim & " " & Rs_Report.Tables(0).Rows(i).Item("DepartmentName").ToString.Trim)
                    mstrDepartmentCode = Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim
                    objWriter.WriteLine("")
                End If
            End If

            'v_InTime = Format(!In1, "hh:mm") & "  "
            'v_Late = IIf(!latearrival = 0, Space(5), Length5(Min2Hr(!latearrival))) & "  "
            If Rs_Report.Tables(0).Rows(i).Item("In1").ToString.Trim = "" Then
                v_InTime = "      "
            Else
                v_InTime = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("In1").ToString.Trim).ToString("HH:mm") & "  "
            End If
            If Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim = "" Or Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim = "0" Then
                v_Late = "         "
            Else
                v_Late = Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim Mod 60).ToString("00") & "    "
            End If

            'v_Status = ""
            'If Not IsNull(!Status) Then
            '    If UCase(Trim(!Status)) = "MIS" Then
            '        v_Status = "P"
            '    Else
            '        v_Status = !Status
            '    End If
            'End If
            v_Status = ""
            If Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim <> "" Then
                If UCase(Trim(Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim)) = "MIS" Then
                    v_Status = "P"
                Else
                    v_Status = Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim
                End If
            End If

            v_Shift = Rs_Report.Tables(0).Rows(i).Item("ShiftAttended").ToString.Trim & " "
            If Rs_Report.Tables(0).Rows(i).Item("shiftStartTime").ToString.Trim <> "" Then
                v_Start = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("shiftStartTime").ToString.Trim).ToString("HH:mm") & " "
            Else
                v_Start = "      "
            End If

            objWriter.WriteLine(msrl.ToString.PadRight(4) & "   " & Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim.PadRight(12) & " " & Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim.PadRight(12) & " " & Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim.PadRight(25) & " " & _
                v_Shift.PadRight(3) & "  " & v_Start & "  " & v_InTime & "  " & Space(1) & v_Late & "  " & _
                v_Status)
            mblnCheckReport = True
            objWriter.WriteLine("")
            mShift = Rs_Report.Tables(0).Rows(i).Item("ShiftAttended").ToString.Trim
            msrl = msrl + 1
            mintLine = mintLine + 1
            '        .MoveNext()
            '            Loop
        Next
        objWriter.Close()
        If isPdf = True Then
            'pdf start 
            Common.pdfreport(mstrFile_Name, 10, ulf)
            'pdf end 
        Else
            Try : Process.Start(mstrFile_Name) : Catch ex As Exception : Process.Start("notepad.exe", mstrFile_Name) : End Try
        End If
    End Sub
    Sub Spot_MachineRawPunchData(strsortorder As String)
        Dim strsql As String, strDeptDivCode As String
        Dim mPunch1 As String ' * 9
        Dim mPunch2 As String '* 9
        Dim mPunch3 As String '* 9
        Dim mPunch4 As String '* 9
        Dim mDate As Date
        Dim blnDeptAvailable As Boolean, mFirst As Boolean
        Dim msrl As Integer
        Dim PrvDate
        Dim mCARDNO As String
        Dim mPaycode As String
        Dim mNewCard As Boolean
        Dim mStr1 As String
        Dim mStr2 As String
        Dim mCtr As Integer
        msrl = 1

        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If

        mstrDepartmentCode = " "
        mblnCheckReport = False
        blnDeptAvailable = False
        mFirst = True

        mintPageNo = 1
        mintLine = 1
        Dim g_HODDepartmentCode As String = ""

        'If Left(strsortorder, 1) = "D" Or Left(strsortorder, 1) = "C" Then blnDeptAvailable = True

        Dim conS As SqlConnection
        Dim conS1 As OleDb.OleDbConnection
        If Common.servername = "Access" Then
            conS1 = New OleDb.OleDbConnection(Common.ConnectionString)
        Else
            conS = New SqlConnection(Common.ConnectionString)
        End If

        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet
        Dim DC As DateConverter = New DateConverter
        If Common.servername = "Access" Then
            strsql = " Select tblCatagory.Catagoryname, MachineRawPunchAll.officepunch, MachineRawPunchAll.inout,tblEmployee.PresentCardNo, " &
                        " tblemployee.PAYCODE,tblemployee.empname, MachineRawPunchAll.CARDNO, MachineRawPunchAll.ismanual," &
                        " tblDepartment.DepartmentCode as DepartmentCode, tblDepartment.DepartmentName, " &
                        " tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, tblEmployee.DepartmentCode " &
                        "  From tblCatagory, MachineRawPunchAll, tblDepartment, tblEmployee,TBLCOMPANY " &
                        " Where OfficePunch between #" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & " 00:00# And " &
                        " #" & DateEdit2.DateTime.ToString("yyyy-MM-dd") & " 23:59# and " &
                        " TBLEMPLOYEE.COMPANYCODE=TBLCOMPANY.COMPANYCODE AND tblEmployee.CAT = tblCatagory.cat And tblemployee.DepartmentCode = tblDepartment.DepartmentCode AND  " &
                        " MachineRawPunchAll.Paycode = tblEmployee.Paycode " &
                        " " & g_HODDepartmentCode &
                        " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & "order by tblEmployee.PresentCARDNO,  MachineRawPunchAll.OFFICEPUNCH"

            If strsortorder.Trim = "" Then
                strsql = strsql.Replace("MachineRawPunchAll", "MachineRawPunch")
            End If

            adapA = New OleDbDataAdapter(strsql, conS1)
            adapA.Fill(Rs_Report)
        Else
            strsql = " Select tblCatagory.Catagoryname, MachineRawPunchAll.officepunch,MachineRawPunchAll.officepunch, MachineRawPunchAll.inout, tblEmployee.PresentCardNo, " &
                        " tblemployee.PAYCODE,tblemployee.empname, MachineRawPunchAll.CARDNO, MachineRawPunchAll.ismanual," &
                        " tblDepartment.DepartmentCode, tblDepartment.DepartmentName, " &
                        " tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, tblEmployee.DepartmentCode " &
                        "  From tblCatagory, MachineRawPunchAll, tblDepartment, tblEmployee,TBLCOMPANY " &
                        " Where OfficePunch Between '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & " 00:00' And " &
                        " '" & DateEdit2.DateTime.ToString("yyyy-MM-dd") & " 23:59' and " &
                        " TBLEMPLOYEE.COMPANYCODE=TBLCOMPANY.COMPANYCODE AND tblEmployee.CAT = tblCatagory.cat And tblemployee.DepartmentCode = tblDepartment.DepartmentCode AND  " &
                        " MachineRawPunchAll.Paycode = tblEmployee.Paycode " &
                        " " & g_HODDepartmentCode &
                        " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & "order by tblEmployee.PresentCARDNO,  MachineRawPunchAll.OFFICEPUNCH"

            If strsortorder.Trim = "" Then
                strsql = strsql.Replace("MachineRawPunchAll", "MachineRawPunch")
            End If
            adap = New SqlDataAdapter(strsql, conS)
            adap.Fill(Rs_Report)
        End If

        If Rs_Report.Tables(0).Rows.Count < 1 Then
            'G_Report_Error = False
            'Screen.MousePointer = vbDefault
            XtraMessageBox.Show(ulf, "<size=10>No Data Present For this Report.</size>", "<size=9>iAS</size>")
            'mReportPrintStatus = False
            Exit Sub
        End If


        mFileNumber = FreeFile()
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".SRP"
        Dim objWriter As New System.IO.StreamWriter(mstrFile_Name, True)

        'With Rs_Report
        '    .MoveFirst()
        '    Do While Not .EOF
        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            'If mFirst Or mintLine > g_LinesPerPage Or (Left(strsortorder, 10) = "Department" And strDeptDivCode <> !DepartmentCode And g_SkipAfterDept) Then
            If mFirst Or mintLine > g_LinesPerPage Or (strsortorder.Trim = "Department" And strDeptDivCode <> Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim And g_SkipAfterDept) Then
1:              If Not mFirst Then
                    objWriter.WriteLine(Space(2) & vbFormFeed)
                Else
                    mFirst = False
                End If
                objWriter.WriteLine(Space(2) & Space(30) & CommonReport.g_CompanyNames)
                objWriter.WriteLine(Space(2) + "")
                objWriter.WriteLine(Space(2) + "")
                objWriter.WriteLine(Space(2) + "Page No." & mintPageNo & Space(20) & "MACHINE RAW PUNCH DATA REPORT FOR " & ForDate) 'DateEdit1.DateTime.ToString("dd/MM/yyyy"))
                objWriter.WriteLine(Space(2) + Space(40) & "Run Date & Time :" & Common.runningDateTime) 'Now.ToString("dd/MM/yyyy HH:mm"))
                objWriter.WriteLine(Space(2) + "--------------------------------------------------------------------------------------------------------------")
                objWriter.WriteLine(Space(2) + "Srl  PayRoll No  Card No       Name                             ----------------- Punch ----------------------")
                objWriter.WriteLine(Space(2) + "No.                                                                 1           2           3           4     ")
                objWriter.WriteLine(Space(2) + "--------------------------------------------------------------------------------------------------------------")
                mintLine = 9
                mintPageNo = mintPageNo + 1
            End If
            'If blnDeptAvailable And Not .EOF Then
            '    If Left(strsortorder, 10) = "Department" And strDeptDivCode <> !DepartmentCode Then
            '        If mintLine + 3 > g_LinesPerPage Then GoTo 1
            '    Print #mFileNumber, Space(2) + ""
            '        strDeptDivCode = !DepartmentCode
            '    Print #mFileNumber, Space(2) + "** Department Code & Name : " & !DepartmentCode & "  " & !DepartmentName
            '    Print #mFileNumber, Space(2) + ""
            '        mintLine = mintLine + 3
            '    ElseIf Left(strsortorder, 8) = "Division" And strDeptDivCode <> !DivisionCode Then
            '        If mintLine + 3 > g_LinesPerPage Then GoTo 1
            '    Print #mFileNumber, Space(2) + ""
            '        strDeptDivCode = !DivisionCode
            '    Print #mFileNumber, Space(2) + "** Section Code & Name : " & !DivisionCode & "  " & !DivisionName
            '    Print #mFileNumber, Space(2) + ""
            '        mintLine = mintLine + 3
            '    ElseIf Left(strsortorder, 3) = "Cat" And strDeptDivCode <> !Cat Then
            '        If mintLine + 3 > g_LinesPerPage Then GoTo 1
            '    Print #mFileNumber, Space(2) + ""
            '        strDeptDivCode = !Cat
            '    Print #mFileNumber, Space(2) + "** Category Code & Name : " & !Cat & "  " & !CatagoryName
            '    Print #mFileNumber, Space(2) + ""
            '        mintLine = mintLine + 3
            '    End If
            'End If

            If PrvDate <> Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("officepunch").ToString.Trim).ToString("dd/MM/yyyy") Then
                If Common.IsNepali = "Y" Then
                    Dim tmp As DateTime = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("officepunch").ToString.Trim).ToString("dd/MM/yyyy")
                    Dim Vstart As String = DC.ToBS(New Date(tmp.Year, tmp.Month, tmp.Day))
                    Dim dojTmp() As String = Vstart.Split("-")
                    objWriter.WriteLine("** Date :- " & dojTmp(2) & "/" & Common.NepaliMonth(dojTmp(1) - 1) & "/" & dojTmp(0))
                Else
                    objWriter.WriteLine("** Date :- " & Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("officepunch").ToString.Trim).ToString("dd/MM/yyyy"))
                End If
                mintLine = mintLine + 2
                ' mNewCard = False
                PrvDate = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("officepunch").ToString.Trim).ToString("dd/MM/yyyy")
            End If

            mCARDNO = Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim
            mPaycode = Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim
            mNewCard = True
            msrl = msrl + 1
            mStr1 = msrl & " " & Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim & " " & Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim &
            Space(3) & Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim.PadRight(25, " "c)
            '" " & Format(Left(!EmpName, 25), "!" & String(25, "@")) & " "
            'objWriter.WriteLine(mStr1)
            mStr2 = ""
            mCtr = 0
            ' If Trim(!paycode) = "30007" Then Stop
            Do While mPaycode = Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim
                'If mNewCard Then
                '    objWriter.WriteLine("** Date :- " & Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("officepunch").ToString.Trim).ToString("dd/MM/yyyy"))
                '    objWriter.WriteLine(mStr1)
                '    'mLineNo = mLineNo + 2
                '    mNewCard = False
                '    PrvDate = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("officepunch").ToString.Trim).ToString("dd/MM/yyyy")
                'End If
                If PrvDate <> Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("officepunch").ToString.Trim).ToString("dd/MM/yyyy") Or mCtr = 4 Then
                    mNewCard = True
                    If mCtr = 4 And PrvDate = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("officepunch").ToString.Trim).ToString("dd/MM/yyyy") Then
                        mStr1 = msrl.ToString.PadRight(4) & " " & Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim.PadRight(12) & " " & Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim.PadRight(12) & Space(3) &
                        Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim.PadRight(25)
                        ' " " & Format((Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim & Space(25)))
                        objWriter.WriteLine(mStr1 & Space(5) & mStr2)
                        msrl = msrl + 1
                        mintLine = mintLine + 1
                        mCtr = 0
                        mStr2 = ""
                    Else
                        'mCount = 0
                        If Len(Trim(mStr2)) > 0 Then
                            mStr1 = msrl.ToString.PadRight(4) & " " & Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim.PadRight(12) & " " & Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim.PadRight(12) &
                              Space(3) & Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim.PadRight(25)
                            ' " " & Format(Left(!EmpName, 25), "!" & String(25, "@")) & " "

                            objWriter.WriteLine(mStr1 & Space(5) & mStr2)
                            msrl = msrl + 1
                            mStr2 = ""
                            mintLine = mintLine + 1
                        End If

                        If Common.IsNepali = "Y" Then
                            Dim tmp As DateTime = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("officepunch").ToString.Trim).ToString("dd/MM/yyyy")
                            Dim Vstart As String = DC.ToBS(New Date(tmp.Year, tmp.Month, tmp.Day))
                            Dim dojTmp() As String = Vstart.Split("-")
                            objWriter.WriteLine("** Date :- " & dojTmp(2) & "/" & Common.NepaliMonth(dojTmp(1) - 1) & "/" & dojTmp(0))
                        Else
                            objWriter.WriteLine("** Date :- " & Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("officepunch").ToString.Trim).ToString("dd/MM/yyyy"))
                        End If

                        'objWriter.WriteLine("** Date :- " & Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("officepunch").ToString.Trim).ToString("dd/MM/yyyy"))
                        mintLine = mintLine + 2
                        PrvDate = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("officepunch").ToString.Trim).ToString("dd/MM/yyyy")
                        mCtr = 0
                    End If
                End If
                'mStr2 = mStr2 & " " & Format(!officepunch, "HH:MM") & IIf(!IsManual = "Y", "M", " ") & " "
                mStr2 = mStr2 & "    " & Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("officepunch").ToString.Trim).ToString("HH:mm") & IIf(Rs_Report.Tables(0).Rows(i).Item("IsManual").ToString.Trim = "Y", "M", " ") & "-" & Rs_Report.Tables(0).Rows(i).Item("inout").ToString.Trim.PadRight(2)
                mCtr = mCtr + 1
                '.MoveNext()
                'If .EOF Then Exit Do
                i = i + 1
                If i > Rs_Report.Tables(0).Rows.Count - 1 Then
                    'i = i - 1
                    Exit Do
                End If
            Loop
            '.MovePrevious()
            i = i - 1
            mStr1 = msrl.ToString.PadRight(4) & " " & Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim.PadRight(12) & " " & Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim.PadRight(12) &
             Space(3) & Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim.PadRight(25)
            ' " " & Format(Left(!EmpName, 25), "!" & String(25, "@")) & " "
            objWriter.WriteLine(mStr1 & Space(5) & mStr2)
            PrvDate = ""
            mNewCard = True
            'mLineNo = mLineNo + 1
            msrl = msrl + 1
            mintLine = mintLine + 1
            mblnCheckReport = True
            '    .MoveNext()
            '        Loop
        Next
        objWriter.Close()
        If isPdf = True Then
            'pdf start 
            Common.pdfreport(mstrFile_Name, 9, ulf)
            'pdf end 
        Else
            Try : Process.Start(mstrFile_Name) : Catch ex As Exception : Process.Start("notepad.exe", mstrFile_Name) : End Try
        End If
    End Sub
    Sub Spot_ManualPunchAuditData(strsortorder As String)
        'On Error GoTo ErrorGen
        Dim strsql As String, strDeptDivCode As String
        Dim mPunch1 As String ' * 9
        Dim mPunch2 As String '* 9
        Dim mPunch3 As String '* 9
        Dim mPunch4 As String '* 9
        Dim mDate As Date
        Dim blnDeptAvailable As Boolean, mFirst As Boolean
        Dim msrl As Integer
        msrl = 1

        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If

        mstrDepartmentCode = " "
        mblnCheckReport = False
        blnDeptAvailable = False
        mFirst = True
        mintPageNo = 1
        mintLine = 1

        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet
        Dim g_HODDepartmentCode = ""
        'If Left(strsortorder, 1) = "D" Or Left(strsortorder, 1) = "C" Then blnDeptAvailable = True

        If strsortorder = "" Then
            CreateTableMachine()
            MachinePunch(DateEdit1.DateTime.ToString("dd/MM/yyyy"), DateEdit2.DateTime.ToString("dd/MM/yyyy")) ' Creating the data for the generation of the Spot reports.
            strsql = " Select tblCatagory.Catagoryname, tblDivision.DivisionName, mMachineRawPunch.PAYCODE, mMachineRawPunch.CARDNO, tblEmployee.PresentCardNo, " & _
                   " mMachineRawPunch.EMPNAME, mMachineRawPunch.PUNCH1, mMachineRawPunch.PUNCH2, mMachineRawPunch.PUNCH3, " & _
                   " mMachineRawPunch.PUNCH4, mMachineRawPunch.DATEOFFICE, mMachineRawPunch.PUNCH1M, " & _
                   " mMachineRawPunch.PUNCH2M, mMachineRawPunch.PUNCH3M, mMachineRawPunch.PUNCH4M,tblDepartment.DepartmentName,tblDepartment.DepartmentCode, " & _
                   " tblEmployee.Divisioncode,  tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat  " & _
                   "  From tblCatagory, tblDivision, " & g_MachineRawPunch & " mMachineRawPunch, tblDepartment, tblEmployee,TBLCOMPANY  " & _
                   " Where TBLEMPLOYEE.COMPANYCODE=TBLCOMPANY.COMPANYCODE AND (left(mMachineRawPunch.PUNCH1M,1)='M' or left(mMachineRawPunch.PUNCH2M,1)='M' or left(mMachineRawPunch.PUNCH3M,1)='M' or left(mMachineRawPunch.PUNCH4M,1)='M' ) and tblEmployee.CAT = tblCatagory.cat And  " & _
                   " mMachineRawPunch.DepartmentCode = tblDepartment.DepartmentCode AND mMachineRawPunch.Paycode = tblEmployee.Paycode AND mMachineRawPunch.CompanyCode = " & _
                   " tblEmployee.CompanyCode" & g_HODDepartmentCode & _
                   " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & _
                   " Order By mMachineRawPunch.DateOffice"

            'Rs_Report = Cn.Execute(strsql)
            'Rs_Report.Sort = "PayCode"
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(strsql, Common.con1)
                adapA.Fill(Rs_Report)
            Else
                adap = New SqlDataAdapter(strsql, Common.con)
                adap.Fill(Rs_Report)
            End If
            DropTableMachine()
        Else
            'Rs_Report.Sort = strsortorder
        End If
        If Rs_Report.Tables(0).Rows.Count < 1 Then
            'G_Report_Error = False
            'Screen.MousePointer = vbDefault
            XtraMessageBox.Show(ulf, "<size=10>No Data Present For this Report.</size>", "<size=10>Mistake</size>")
            'mReportPrintStatus = False
            Exit Sub
        End If

        mFileNumber = FreeFile()
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".SRP"
        Dim objWriter As New System.IO.StreamWriter(mstrFile_Name, True)

        'With Rs_Report
        '    .MoveFirst()
        '    Do While Not .EOF
        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            'If mFirst Or mintLine > g_LinesPerPage Or (Left(strsortorder, 10) = "Department" And strDeptDivCode <> !DepartmentCode And g_SkipAfterDept) Then
            If mFirst Or mintLine > g_LinesPerPage Or (strsortorder.Trim = "Department" And strDeptDivCode <> Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim And g_SkipAfterDept) Then
1:              If Not mFirst Then
                    objWriter.WriteLine(Space(2) & vbFormFeed)
                Else
                    mFirst = False
                End If
                objWriter.WriteLine(Space(2) + Space(30) & CommonReport.g_CompanyNames)
                objWriter.WriteLine(Space(2) + "")
                objWriter.WriteLine(Space(2) + "")
                objWriter.WriteLine(Space(2) + "Page No." & mintPageNo & Space(20) & "MANUAL PUNCH DATA REPORT FOR " & DateEdit1.DateTime.ToString("dd/MM/yyyy"))
                objWriter.WriteLine(Space(2) + Space(35) & "Run Date & Time :" & Now.ToString("dd/MM/yyyy HH:mm"))
                objWriter.WriteLine(Space(2) + "--------------------------------------------------------------------------------------------")
                objWriter.WriteLine(Space(2) + "Srl  PayRoll No  Card No     Name                    ----------------- Punch ---------------")
                objWriter.WriteLine(Space(2) + "No.                                                    1          2          3          4   ")
                objWriter.WriteLine(Space(2) + "--------------------------------------------------------------------------------------------")
                mintLine = 9
                mintPageNo = mintPageNo + 1
            End If
            'If blnDeptAvailable And Not .EOF Then
            '    If Left(strsortorder, 10) = "Department" And strDeptDivCode <> !DepartmentCode Then
            '        If mintLine + 3 > g_LinesPerPage Then GoTo 1
            '        Print #mFileNumber, Space(2) + ""
            '        strDeptDivCode = !DepartmentCode
            '        Print #mFileNumber, Space(2) + "** Department Code & Name : " & !DepartmentCode & "  " & !DepartmentName
            '        Print #mFileNumber, Space(2) + ""
            '        mintLine = mintLine + 3
            '    ElseIf Left(strsortorder, 8) = "Division" And strDeptDivCode <> !DivisionCode Then
            '        If mintLine + 3 > g_LinesPerPage Then GoTo 1
            '        Print #mFileNumber, Space(2) + ""
            '        strDeptDivCode = !DivisionCode
            '        Print #mFileNumber, Space(2) + "** Section Code & Name : " & !DivisionCode & "  " & !DivisionName
            '        Print #mFileNumber, Space(2) + ""
            '        mintLine = mintLine + 3
            '    ElseIf Left(strsortorder, 3) = "Cat" And strDeptDivCode <> !Cat Then
            '        If mintLine + 3 > g_LinesPerPage Then GoTo 1
            '        Print #mFileNumber, Space(2) + ""
            '        strDeptDivCode = !Cat
            '        Print #mFileNumber, Space(2) + "** Category Code & Name : " & !Cat & "  " & !CatagoryName
            '        Print #mFileNumber, Space(2) + ""
            '        mintLine = mintLine + 3
            '    End If
            'End If

            If mDate <> Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("DATEOFFICE").ToString.Trim).ToString("dd/MM/yyyy") Then
                mDate = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("DATEOFFICE").ToString.Trim).ToString("dd/MM/yyyy")
                objWriter.WriteLine(Space(2) + "")
                objWriter.WriteLine(Space(2) + "")
                objWriter.WriteLine(Space(2) + "*** Date " & mDate)
                objWriter.WriteLine(Space(2) + "")
                mintLine = mintLine + 4
            End If
            mPunch1 = Rs_Report.Tables(0).Rows(i).Item("PUNCH1").ToString.Trim & " " & Rs_Report.Tables(0).Rows(i).Item("punch1m").ToString.Trim
            mPunch2 = Rs_Report.Tables(0).Rows(i).Item("PUNCH2").ToString.Trim & " " & Rs_Report.Tables(0).Rows(i).Item("punch2m").ToString.Trim
            mPunch3 = Rs_Report.Tables(0).Rows(i).Item("PUNCH3").ToString.Trim & " " & Rs_Report.Tables(0).Rows(i).Item("punch3m").ToString.Trim
            mPunch4 = Rs_Report.Tables(0).Rows(i).Item("PUNCH4").ToString.Trim & " " & Rs_Report.Tables(0).Rows(i).Item("punch4m").ToString.Trim

            objWriter.WriteLine(Space(2) & msrl.ToString.PadRight(4) & " " & Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim.PadRight(12) & " " & Rs_Report.Tables(0).Rows(i).Item("CardNo").ToString.Trim.PadRight(12) & " " & Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim.PadRight(25) & "     " & _
                                 mPunch1 & "  " & mPunch2 & "  " & mPunch3 & "  " & mPunch4 & "  ")

            msrl = msrl + 1
            mintLine = mintLine + 1
            mblnCheckReport = True
            '        .MoveNext()
            '            Loop
        Next
        objWriter.Close()
        If isPdf = True Then
            'pdf start 
            Common.pdfreport(mstrFile_Name, 8, ulf)
            'pdf end 
        Else
            Try : Process.Start(mstrFile_Name) : Catch ex As Exception : Process.Start("notepad.exe", mstrFile_Name) : End Try
        End If
    End Sub
    Sub Spot_Present(strsortorder As String)
        'On Error GoTo ErrorGen
        Dim strsql As String, strDeptDivCode As String
        Dim v_Late As String '* 6
        Dim v_InTime As String '* 6
        Dim strStatus As String
        Dim blnDeptAvailable As Boolean, mFirst As Boolean
        Dim msrl As Integer
        msrl = 1
        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If

        mstrDepartmentCode = " "
        blnDeptAvailable = False
        mFirst = True
        mintPageNo = 1
        mintLine = 1
        mblnCheckReport = False
        Dim g_HODDepartmentCode As String = ""
        'If Left(strsortorder, 1) = "D" Or Left(strsortorder, 1) = "C" Then blnDeptAvailable = True

        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet

        If strsortorder = "" Then
            '    If Common.servername = "Access" Then
            '        '       strsql = " Select tblTimeregister.PAYCODE, tblTimeregister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, tblDepartment.DepartmentCode, tblDepartment.DepartmentName,tblEmployee.PresentCARDNO, tblEmployee.EMPNAME, tblTimeregister.DATEOFFICE, " & _
            '        '" tblTimeregister.SHIFTATTENDED, tblTimeregister.ShiftStartTime, tblTimeregister.In1, tblTimeregister.LateArrival, tblTimeregister.Status, tblCatagory.Catagoryname, tblDivision.DivisionName " & _
            '        '" From  tblTimeregister, tblEmployee, tblDepartment, tblCatagory, tblDivision,tblCompany " & _
            '        '" Where tbltimeregister.presentvalue>0 and tblEmployee.PayCode = tblTimeregister.PayCode And  tbltimeregister.in1 is not null  and " & _
            '        '" tblEmployee.Companycode=tblCompany.Companycode and tblEmployee.CAT = tblCatagory.cat And " & _
            '        '" tblEmployee.DivisionCode = tblDivision.DivisionCode AND FORMAT(tbltimeregister.DateOffice,'MMM DD YYYY') = '" & DateEdit1.DateTime.ToString("dd-MM-yyyy") & "' And " & _
            '        '" tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
            '        '" And (tblemployee.LeavingDate >=#" & DateEdit1.DateTime.ToString("dd-MM-yyyy") & "# or tblemployee.LeavingDate is null)" & IIf(Len(Trim(g_WhereClause)) = 0, "", " and " & g_WhereClause)

            '        strsql = " Select tblTimeregister.PAYCODE, tblTimeregister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, tblDepartment.DepartmentCode, tblDepartment.DepartmentName,tblEmployee.PresentCARDNO, tblEmployee.EMPNAME, tblTimeregister.DATEOFFICE, " & _
            '" tblTimeregister.SHIFTATTENDED, tblTimeregister.ShiftStartTime, tblTimeregister.In1, tblTimeregister.LateArrival, tblTimeregister.Status, tblCatagory.Catagoryname, tblDivision.DivisionName " & _
            '" From  tblTimeregister, tblEmployee, tblDepartment, tblCatagory, tblDivision,tblCompany " & _
            '" Where tbltimeregister.presentvalue>0 and tblEmployee.PayCode = tblTimeregister.PayCode And  tbltimeregister.in1 is not null  and " & _
            '" tblEmployee.Companycode=tblCompany.Companycode and tblEmployee.CAT = tblCatagory.cat And " & _
            '" FORMAT(tbltimeregister.DateOffice,'dd-MM-yyyy') = '" & DateEdit1.DateTime.ToString("dd-MM-yyyy") & "' And " & _
            '" tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
            '" And (tblemployee.LeavingDate >=#" & DateEdit1.DateTime.ToString("dd-MM-yyyy") & "# or tblemployee.LeavingDate is null)" & IIf(Len(Trim(g_WhereClause)) = 0, "", " and " & g_WhereClause)

            '        adapA = New OleDbDataAdapter(strsql, Common.con1)
            '        adapA.Fill(Rs_Report)
            '    Else
            '        strsql = " Select tblTimeregister.PAYCODE, tblTimeregister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, tblDepartment.DepartmentCode, tblDepartment.DepartmentName,tblEmployee.PresentCARDNO, tblEmployee.EMPNAME, tblTimeregister.DATEOFFICE, " & _
            '             " tblTimeregister.SHIFTATTENDED, tblTimeregister.ShiftStartTime, tblTimeregister.In1, tblTimeregister.LateArrival, tblTimeregister.Status, tblCatagory.Catagoryname, tblDivision.DivisionName " & _
            '             " From  tblTimeregister, tblEmployee, tblDepartment, tblCatagory, tblDivision,tblCompany " & _
            '             " Where tbltimeregister.presentvalue>0 and tblEmployee.PayCode = tblTimeregister.PayCode And  tbltimeregister.in1 is not null  and " & _
            '             " tblEmployee.Companycode=tblCompany.Companycode and tblEmployee.CAT = tblCatagory.cat And " & _
            '             " tblEmployee.DivisionCode = tblDivision.DivisionCode AND tbltimeregister.DateOffice = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' And " & _
            '             " tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
            '             " And (tblemployee.LeavingDate>='" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' or tblemployee.LeavingDate is null)" & IIf(Len(Trim(g_WhereClause)) = 0, "", " and " & g_WhereClause)
            '        adap = New SqlDataAdapter(strsql, Common.con)
            '        adap.Fill(Rs_Report)
            '    End If

            If Common.servername = "Access" Then
                'strsql = " Select tblTimeregister.PAYCODE, tblTimeregister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, tblDepartment.DepartmentCode, tblDepartment.DepartmentName,tblEmployee.PresentCARDNO, tblEmployee.EMPNAME, tblTimeregister.DATEOFFICE, " & _
                '     " tblTimeregister.SHIFTATTENDED, tblTimeregister.ShiftStartTime, tblTimeregister.In1, tblTimeregister.LateArrival, tblTimeregister.Status, tblCatagory.Catagoryname, tblDivision.DivisionName " & _
                '     " From  tblTimeregister, tblEmployee, tblDepartment, tblCatagory, tblDivision,tblCompany " & _
                '     " Where tblEmployee.PayCode = tblTimeregister.PayCode And tbltimeregister.in1 is not null  and " & _
                '     " tblEmployee.Companycode=tblCompany.Companycode and tblEmployee.CAT = tblCatagory.cat And " & _
                '     " tblEmployee.DivisionCode = tblDivision.DivisionCode AND FORMAT(tbltimeregister.DateOffice,'MMM dd yyyy') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' And " & _
                '     " tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
                '     " And (tblemployee.LeavingDate >=#" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "# or tblemployee.LeavingDate is null)" & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & strsortorder

                strsql = " Select tblTimeregister.PAYCODE, tblTimeregister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, tblDepartment.DepartmentCode, tblDepartment.DepartmentName,tblEmployee.PresentCARDNO, tblEmployee.EMPNAME, tblTimeregister.DATEOFFICE, " & _
                    " tblTimeregister.SHIFTATTENDED, tblTimeregister.ShiftStartTime, tblTimeregister.In1, tblTimeregister.LateArrival, tblTimeregister.Status, tblCatagory.Catagoryname, tblDivision.DivisionName " & _
                    " From  tblTimeregister, tblEmployee, tblDepartment, tblCatagory, tblDivision,tblCompany " & _
                    " Where tblEmployee.PayCode = tblTimeregister.PayCode And tbltimeregister.in1 is not null  and " & _
                    " tblEmployee.Companycode=tblCompany.Companycode and tblEmployee.CAT = tblCatagory.cat And " & _
                    " FORMAT(tbltimeregister.DateOffice,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' And " & _
                    " tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
                    " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & " order by tblEmployee.PresentCardNo, TblTimeRegister.DateOffice" '& strsortorder
                If Common.IsCompliance Then
                    strsql = Microsoft.VisualBasic.Strings.Replace(strsql, "tbltimeregister", "tblTimeRegisterD", 1, -1, Microsoft.VisualBasic.Constants.vbTextCompare)
                End If

                adapA = New OleDbDataAdapter(strsql, Common.con1)
                adapA.Fill(Rs_Report)
            Else
                strsql = " Select tblTimeregister.PAYCODE, tblTimeregister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, tblDepartment.DepartmentCode, tblDepartment.DepartmentName,tblEmployee.PresentCARDNO, tblEmployee.EMPNAME, tblTimeregister.DATEOFFICE, " & _
                     " tblTimeregister.SHIFTATTENDED, tblTimeregister.ShiftStartTime, tblTimeregister.In1, tblTimeregister.LateArrival, tblTimeregister.Status, tblCatagory.Catagoryname, tblDivision.DivisionName " & _
                     " From  tblTimeregister, tblEmployee, tblDepartment, tblCatagory, tblDivision,tblCompany " & _
                     " Where tblEmployee.PayCode = tblTimeregister.PayCode And tbltimeregister.in1 is not null  and " & _
                     " tblEmployee.Companycode=tblCompany.Companycode and tblEmployee.CAT = tblCatagory.cat And " & _
                     " tbltimeregister.DateOffice = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' And " & _
                     " tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
                     " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & " order by tblEmployee.PresentCardNo, TblTimeRegister.DateOffice" '& strsortorder
                If Common.IsCompliance Then
                    strsql = Microsoft.VisualBasic.Strings.Replace(strsql, "tbltimeregister", "tblTimeRegisterD", 1, -1, Microsoft.VisualBasic.Constants.vbTextCompare)
                End If
                adap = New SqlDataAdapter(strsql, Common.con)
                adap.Fill(Rs_Report)
            End If

            'Rs_Report = Cn.Execute(strsql)
            'Rs_Report.Sort = "PayCode"
        Else
            'Rs_Report.Sort = strsortorder
        End If
        If Rs_Report.Tables(0).Rows.Count < 1 Then
            'G_Report_Error = False
            'Screen.MousePointer = vbDefault
            XtraMessageBox.Show(ulf, "<size=10>No Data Available For this Report.</size>", "<size=9>iAS</size>")
            'mReportPrintStatus = False
            Exit Sub
        End If
        mFileNumber = FreeFile()
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".SRP"
        Dim objWriter As New System.IO.StreamWriter(mstrFile_Name, True)
        'With Rs_Report
        '    .MoveFirst()
        '    Do While Not .EOF
        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            ' If mFirst Or mintLine > g_LinesPerPage Or (Left(strsortorder, 10) = "Department" And strDeptDivCode <> !DepartmentCode And g_SkipAfterDept) Then
            If mFirst Or mintLine > g_LinesPerPage Or (strsortorder.Trim = "Department" And strDeptDivCode <> Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim And g_SkipAfterDept) Then
1:              If Not mFirst Then
                    objWriter.WriteLine(Space(2) & vbFormFeed)
                Else
                    mFirst = False
                End If
                objWriter.WriteLine(Space(2) + Space(20) & CommonReport.g_CompanyNames)
                objWriter.WriteLine(Space(2) + "")
                objWriter.WriteLine(Space(2) + "")
                'objWriter.WriteLine(Space(2) + "Page No." & mintPageNo & Space(10) & "Present Report for " & DateEdit1.DateTime.ToString("dd-MM-yyyy") & Space(4) & "Run Date & Time :" & Now().ToString("dd/MM/yyyy HH:mm"))
                objWriter.WriteLine(Space(2) + "Page No." & mintPageNo & Space(10) & "Present Report for " & ForDate & Space(4) & "Run Date & Time :" & Common.runningDateTime)
                objWriter.WriteLine(Space(2) + "-------------------------------------------------------------------------------------------")
                objWriter.WriteLine(Space(2) + "Srl  EmpCode      CardNo       Employee Name            Shift Start   In      Shift  Status")
                objWriter.WriteLine(Space(2) + "No.                                                                           Late")
                objWriter.WriteLine(Space(2) + "--------------------------------------------------------------------------------------------")
                mintLine = 8
                mintPageNo = mintPageNo + 1
            End If
            'If blnDeptAvailable And Not .EOF Then
            '    If Left(strsortorder, 10) = "Department" And strDeptDivCode <> !DepartmentCode Then
            '        If mintLine + 3 > g_LinesPerPage Then GoTo 1
            '        Print #mFileNumber, Space(2) + ""
            '        strDeptDivCode = !DepartmentCode
            '        Print #mFileNumber, Space(2) + "** Department Code & Name : " & !DepartmentCode & "  " & !DepartmentName
            '        Print #mFileNumber, Space(2) + ""
            '        mintLine = mintLine + 3
            '    ElseIf Left(strsortorder, 8) = "Division" And strDeptDivCode <> !DivisionCode Then
            '        If mintLine + 3 > g_LinesPerPage Then GoTo 1
            '        Print #mFileNumber, Space(2) + ""
            '        strDeptDivCode = !DivisionCode
            '        Print #mFileNumber, Space(2) + "** Section Code & Name : " & !DivisionCode & "  " & !DivisionName
            '        Print #mFileNumber, Space(2) + ""
            '        mintLine = mintLine + 3
            '    ElseIf Left(strsortorder, 3) = "Cat" And strDeptDivCode <> !Cat Then
            '        If mintLine + 3 > g_LinesPerPage Then GoTo 1
            '        Print #mFileNumber, Space(2) + ""
            '        strDeptDivCode = !Cat
            '        Print #mFileNumber, Space(2) + "** Category Code & Name : " & !Cat & "  " & !CatagoryName
            '        Print #mFileNumber, Space(2) + ""
            '        mintLine = mintLine + 3
            '    End If
            'End If

            'v_InTime = Format(!In1, "hh:mm") & "  "
            'v_Late = Length5(Min2Hr(!latearrival))

            If Rs_Report.Tables(0).Rows(i).Item("In1").ToString.Trim = "" Then
                v_InTime = ""
            Else
                v_InTime = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("In1").ToString.Trim).ToString("HH:mm") & "  "
            End If
            If Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim = "" Then
                v_Late = ""
            Else
                v_Late = Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim Mod 60).ToString("00")
            End If

            strStatus = ""
            If Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim <> "" Then
                If UCase(Trim(Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim)) = "MIS" Then
                    strStatus = "P"
                Else
                    strStatus = Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim
                End If
            End If

            If Val(v_Late) = 0 Then
                v_Late = ""
            End If

            Dim shiftStartTime As String
            If Rs_Report.Tables(0).Rows(i).Item("shiftStartTime").ToString.Trim = "" Then
                shiftStartTime = ""
            Else
                shiftStartTime = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("shiftStartTime").ToString.Trim).ToString("HH:mm")
            End If
            objWriter.WriteLine(Space(2) & msrl.ToString.PadRight(4) & " " & Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim.PadRight(12) & " " & Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim.PadRight(12) & " " & Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim.PadRight(25) & " " & Rs_Report.Tables(0).Rows(i).Item("ShiftAttended").ToString.Trim.PadRight(3) & "  " & _
                                IIf(Rs_Report.Tables(0).Rows(i).Item("shiftStartTime").ToString.Trim = "", Space(5), shiftStartTime) & "  " & IIf(Trim(v_InTime) = "", Space(6), v_InTime) & " " & IIf(Trim(v_Late) = "", Space(6), Space(6 - Len(Trim(v_Late))) & Trim(v_Late)) & "  " & strStatus)

            msrl = msrl + 1
            mintLine = mintLine + 1
            mblnCheckReport = True
            '        .MoveNext()
            '            Loop
        Next
        objWriter.Close()
        If isPdf = True Then
            'pdf start 
            Common.pdfreport(mstrFile_Name, 11, ulf)
            'pdf end 
        Else
            Try : Process.Start(mstrFile_Name) : Catch ex As Exception : Process.Start("notepad.exe", mstrFile_Name) : End Try
        End If
    End Sub
    Sub Daily_Performance(ByVal strsortorder As String)
        'On Error GoTo ErrorGen
        Dim intFile As Integer
        Dim strsql As String
        Dim strOsDuration6 As String '* 6
        Dim strOtAmount11 As String '* 11
        Dim intPrintLineCounter As Integer
        Dim strIn1Mannual_1 As String '* 1
        Dim strOut1Mannual_1 As String '* 1
        Dim strIn2Mannual_1 As String '* 1
        Dim strOut2Mannual_1 As String '* 1
        Dim blnDeptAvailable As Boolean
        Dim strDeptDivCode As String
        Dim strCurrentDeptDivCode As String, mFirst As Boolean
        Dim msrl As Integer
        msrl = 1

        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If

        Dim g_HODDepartmentCode As String = ""   'nitin
        'If Left(strsortorder, 1) = "D" Or Left(strsortorder, 1) = "C" Then blnDeptAvailable = True

        mFirst = True
        strDeptDivCode = ""
        mblnCheckReport = False
        mintPageNo = 1
        mintLine = 1

        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet

        If strsortorder = "" Then
            'If g_Report_view Then
            If Common.servername = "Access" Then
                strsql = "select tblCatagory.CatagoryName, tblTimeRegister.Shift, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tbltimeregister.ShiftAttended,tbltimeregister.ShiftStartTime,tbltimeregister.In1,tbltimeregister.Out1,tbltimeregister.In2,tbltimeregister.Out2,tbltimeregister.HoursWorked,tbltimeregister.Status,tbltimeregister.EarlyArrival," &
                " tbltimeregister.LateArrival,tbltimeregister.EarlyDeparture,tbltimeregister.ExcLunchHours,tbltimeregister.OtDuration,tbltimeregister.OtAmount,tbltimeregister.OsDuration,tbltimeregister.In1Mannual,tbltimeregister.In2Mannual,tbltimeregister.Out1Mannual,tbltimeregister.Out2Mannual,tblCalander.mDate,tblCalander.Process,tblCalander.NRTCProc" &
                " from tblCatagory,tbltimeregister,tblEmployee,tblCalander,tblCompany,tblDepartment" &
                " Where tblCatagory.Cat = tblEmployee.Cat And tbltimeregister.PayCode = tblEmployee.PayCode And FORMAT(tbltimeregister.DateOffice,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' And FORMAT(tblCalander.mDate,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" &
                " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode &
                " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & "order by tblEmployee.PresentCARDNO, TblTimeRegister.DateOffice"
                If Common.IsCompliance Then
                    strsql = Microsoft.VisualBasic.Strings.Replace(strsql, "tbltimeregister", "tblTimeRegisterD", 1, -1, Microsoft.VisualBasic.Constants.vbTextCompare)
                End If
                adapA = New OleDbDataAdapter(strsql, Common.con1)
                adapA.Fill(Rs_Report)
            Else
                strsql = "select tblCatagory.CatagoryName, tblTimeRegister.Shift, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tbltimeregister.ShiftAttended,tbltimeregister.ShiftStartTime,tbltimeregister.In1,tbltimeregister.Out1,tbltimeregister.In2,tbltimeregister.Out2,tbltimeregister.HoursWorked,tbltimeregister.Status,tbltimeregister.EarlyArrival," &
                 " tbltimeregister.LateArrival,tbltimeregister.EarlyDeparture,tbltimeregister.ExcLunchHours,tbltimeregister.OtDuration,tbltimeregister.OtAmount,tbltimeregister.OsDuration,tbltimeregister.In1Mannual,tbltimeregister.In2Mannual,tbltimeregister.Out1Mannual,tbltimeregister.Out2Mannual,tblCalander.mDate,tblCalander.Process,tblCalander.NRTCProc" &
                 " from tblCatagory,tbltimeregister,tblEmployee,tblCalander,tblCompany,tblDepartment" &
                 " Where tblCatagory.Cat = tblEmployee.Cat And tbltimeregister.PayCode = tblEmployee.PayCode And tbltimeregister.DateOffice = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' And tblCalander.mDate = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" &
                 " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode &
                 " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & "order by tblEmployee.PresentCARDNO, TblTimeRegister.DateOffice"
                If Common.IsCompliance Then
                    strsql = Microsoft.VisualBasic.Strings.Replace(strsql, "tbltimeregister", "tblTimeRegisterD", 1, -1, Microsoft.VisualBasic.Constants.vbTextCompare)
                End If
                adap = New SqlDataAdapter(strsql, Common.con)
                adap.Fill(Rs_Report)
            End If
            If Rs_Report.Tables(0).Rows.Count < 1 Then
                XtraMessageBox.Show(ulf, "<size=10>No Data Available For this Report.</size>", "<size=9>iAS</size>")
                Exit Sub
            End If
        Else
            'Rs_Report.Sort = strsortorder
        End If
        mFileNumber = FreeFile()
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".SRP"
        Dim objWriter As New System.IO.StreamWriter(mstrFile_Name, True)

        'With Rs_Report
        '    Do While Not .EOF
        Dim DepartmentCode = Rs_Report.Tables(0).Rows(0).Item("DepartmentCode").ToString.Trim
        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            'If mFirst Or mintLine > g_LinesPerPage Or (Left(strsortorder, 10) = "Department" And strDeptDivCode <> !DepartmentCode And g_SkipAfterDept) Then
            If mFirst Or mintLine > g_LinesPerPage Or (strsortorder.Trim = "Department" And strDeptDivCode <> Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim And g_SkipAfterDept) Then
1:              If Not mFirst Then
                    objWriter.WriteLine(Space(2) & vbFormFeed)
                Else
                    mFirst = False
                End If
                objWriter.WriteLine("")
                objWriter.WriteLine(Space(2) & Space(40) & CommonReport.g_CompanyNames)
                objWriter.WriteLine(Space(2) & "")
                objWriter.WriteLine(Space(2) & "Page No." & mintPageNo)
                objWriter.WriteLine(Space(2) & "                                                                                                                        Run Date & Time : " & Common.runningDateTime) 'Now.ToString("dd/MM/yyyy HH:mm"))
                objWriter.WriteLine(Space(2) & "                                                            Daily Performance for " & ForDate) 'DateEdit1.DateTime.ToString("dd/MM/yyyy"))
                objWriter.WriteLine(Space(2) & "------------------------------------------------------------------------------------------------------------------------------------------------------------------------")
                objWriter.WriteLine(Space(2) & "Srl  EmpCode      Cardno       Employee Name             Shift Start In    Lunch Lunch Out   Hours  Status Early   Shift Shift  Excess  Ot    Overtime    Over   Manual")
                objWriter.WriteLine(Space(2) & "No.                                                                        Out   In          Worked        Arrival Late  Early  Lunch         Amount      Stay")
                objWriter.WriteLine(Space(2) & "------------------------------------------------------------------------------------------------------------------------------------------------------------------------")
                mintPageNo = mintPageNo + 1
                mintLine = 9
            End If

            'If blnDeptAvailable And Not .EOF Then
            '    If Left(strsortorder, 10) = "Department" And strDeptDivCode <> !DepartmentCode Then
            '        If mintLine + 3 > g_LinesPerPage Then GoTo 1
            '        Print #intFile, Space(2) + ""
            '        strDeptDivCode = !DepartmentCode
            '        Print #intFile, Space(2) + "** Department Code & Name : " & !DepartmentCode & "  " & !DepartmentName
            '        Print #intFile, Space(2) + ""
            '        mintLine = mintLine + 3
            '    ElseIf Left(strsortorder, 8) = "Division" And strDeptDivCode <> !DivisionCode Then
            '        If mintLine + 3 > g_LinesPerPage Then GoTo 1
            '        Print #intFile, Space(2) + ""
            '        strDeptDivCode = !DivisionCode
            '        Print #intFile, Space(2) + "** Section Code & Name : " & !DivisionCode & "  " & !DivisionName
            '        Print #intFile, Space(2) + ""
            '        mintLine = mintLine + 3
            '    ElseIf Left(strsortorder, 3) = "Cat" And strDeptDivCode <> !Cat Then
            '        If mintLine + 3 > g_LinesPerPage Then GoTo 1
            '        Print #intFile, Space(2) + ""
            '        strDeptDivCode = !Cat
            '        Print #intFile, Space(2) + "** Category Code & Name : " & !Cat & "  " & !CatagoryName
            '        Print #intFile, Space(2) + ""
            '        mintLine = mintLine + 3
            '    End If
            'End If

            strOsDuration6 = ""
            strOtAmount11 = ""
            If Rs_Report.Tables(0).Rows(i).Item("OsDuration").ToString.Trim <> "" Then
                If Rs_Report.Tables(0).Rows(i).Item("OsDuration").ToString.Trim <> "0" Then
                    Dim tmp As String = Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("OsDuration").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("OsDuration").ToString.Trim Mod 60).ToString("00")
                    strOsDuration6 = Space(1) & tmp
                End If
            End If

            If Rs_Report.Tables(0).Rows(i).Item("OTAmount").ToString.Trim <> "" Then
                If Rs_Report.Tables(0).Rows(i).Item("OTAmount").ToString.Trim <> "0" Then
                    strOtAmount11 = Space(1) & Trim(Length7(CStr(Rs_Report.Tables(0).Rows(i).Item("OTAmount").ToString.Trim)))
                Else
                    strOtAmount11 = Space(7)
                End If
            End If
            objWriter.WriteLine("")     'for new line   ' objWriter.Write("") to continue on same line 
            objWriter.Write(Space(2) & msrl.ToString.PadRight(4) & " " & Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim.PadRight(12) & " " & Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim.PadRight(12) & " " & Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim.PadRight(25) & " ")

            If Rs_Report.Tables(0).Rows(i).Item("ShiftAttended").ToString.Trim = "" Then
                objWriter.Write(Space(3))
            Else
                objWriter.Write(Rs_Report.Tables(0).Rows(i).Item("ShiftAttended").ToString.Trim.PadRight(3))
            End If
            objWriter.Write(Space(3))

            If Rs_Report.Tables(0).Rows(i).Item("shiftStartTime").ToString.Trim = "" Then
                objWriter.Write(Space(5))
            ElseIf Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("shiftStartTime").ToString.Trim).ToString("HH:mm") = "  :  " Or Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("shiftStartTime").ToString.Trim).ToString("HH:mm") = "00:00" Then
                objWriter.Write(Space(5))
            Else
                objWriter.Write(Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("shiftStartTime").ToString.Trim).ToString("HH:mm"))
            End If

            objWriter.Write(Space(1))


            If Rs_Report.Tables(0).Rows(i).Item("In1").ToString.Trim = "" Then
                objWriter.Write(Space(5))
            ElseIf Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("In1").ToString.Trim).ToString("HH:mm") = "  :  " Then
                objWriter.Write(Space(5))
            Else
                objWriter.Write(Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("In1").ToString.Trim).ToString("HH:mm"))
            End If
            objWriter.Write(Space(1))

            If Rs_Report.Tables(0).Rows(i).Item("out1").ToString.Trim = "" Then
                objWriter.Write(Space(5))
            ElseIf Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("out1").ToString.Trim).ToString("HH:mm") = "  :  " Then
                objWriter.Write(Space(5))
            Else
                objWriter.Write(Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("out1").ToString.Trim).ToString("HH:mm"))
            End If
            objWriter.Write(Space(1))


            If Rs_Report.Tables(0).Rows(i).Item("in2").ToString.Trim = "" Then
                objWriter.Write(Space(5))
            ElseIf Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("in2").ToString.Trim).ToString("HH:mm") = "  :  " Then
                objWriter.Write(Space(5))
            Else
                objWriter.Write(Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("in2").ToString.Trim).ToString("HH:mm"))
            End If
            objWriter.Write(Space(1))

            If Rs_Report.Tables(0).Rows(i).Item("out2").ToString.Trim = "" Then
                objWriter.Write(Space(5))
            ElseIf Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("out2").ToString.Trim).ToString("HH:mm") = "  :  " Then
                objWriter.Write(Space(5))
            Else
                objWriter.Write(Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("out2").ToString.Trim).ToString("HH:mm"))
            End If
            objWriter.Write(Space(1))


            If Rs_Report.Tables(0).Rows(i).Item("HOURSWORKED").ToString.Trim = "" Then
                objWriter.Write(Space(6))
            ElseIf Rs_Report.Tables(0).Rows(i).Item("HOURSWORKED").ToString.Trim = "0" Then
                objWriter.Write(Space(6))
            Else
                Dim tmp As String = Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("HOURSWORKED").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("HOURSWORKED").ToString.Trim Mod 60).ToString("00")
                objWriter.Write(Space(1) & tmp)
            End If
            objWriter.Write(Space(3))


            If Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim = "" Then
                objWriter.Write(Space(6))
            ElseIf Len(Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim) = 0 Then
                objWriter.Write(Space(6))
            Else
                objWriter.Write(Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim.PadRight(5))
            End If
            objWriter.Write(Space(1))


            If Rs_Report.Tables(0).Rows(i).Item("EARLYARRIVAL").ToString.Trim = "" Then
                objWriter.Write(Space(5))
            ElseIf Rs_Report.Tables(0).Rows(i).Item("EARLYARRIVAL").ToString.Trim = "0" Then
                objWriter.Write(Space(5))
            Else
                Dim tmp As String = Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("EARLYARRIVAL").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("EARLYARRIVAL").ToString.Trim Mod 60).ToString("00")
                objWriter.Write(tmp)
            End If
            objWriter.Write(Space(2))


            If Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim = "" Then
                objWriter.Write(Space(5))
            ElseIf Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim = "0" Then
                objWriter.Write(Space(5))
            Else
                Dim tmp As String = Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim Mod 60).ToString("00")
                objWriter.Write(tmp)
            End If
            objWriter.Write(Space(2))

            'If IsNull(!earlydeparture) Then Print #intFile, Space(5); Else If !earlydeparture = 0 Then Print #intFile, Space(5); Else Print #intFile, String(5 - Len(Trim(Length5(Min2Hr(!earlydeparture)))), " ") & Trim(Length5(Min2Hr(!earlydeparture)));
            'Print #intFile, Space(1);

            If Rs_Report.Tables(0).Rows(i).Item("earlydeparture").ToString.Trim = "" Then
                objWriter.Write(Space(5))
            ElseIf Rs_Report.Tables(0).Rows(i).Item("earlydeparture").ToString.Trim = "0" Then
                objWriter.Write(Space(5))
            Else
                Dim tmp As String = Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("earlydeparture").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("earlydeparture").ToString.Trim Mod 60).ToString("00")
                objWriter.Write(tmp)
            End If
            objWriter.Write(Space(2))


            If Rs_Report.Tables(0).Rows(i).Item("EXCLUNCHHOURS").ToString.Trim = "" Then
                objWriter.Write(Space(5))
            ElseIf Rs_Report.Tables(0).Rows(i).Item("EXCLUNCHHOURS").ToString.Trim = "0" Then
                objWriter.Write(Space(5))
            Else
                Dim tmp As String = Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("EXCLUNCHHOURS").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("EXCLUNCHHOURS").ToString.Trim Mod 60).ToString("00")
                objWriter.Write(tmp)
            End If
            objWriter.Write(Space(2))


            If Rs_Report.Tables(0).Rows(i).Item("OtDuration").ToString.Trim = "" Then
                objWriter.Write(Space(5))
            ElseIf Rs_Report.Tables(0).Rows(i).Item("OtDuration").ToString.Trim = "0" Then
                objWriter.Write(Space(5))
            Else
                Dim tmp As String = Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("OtDuration").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("OtDuration").ToString.Trim Mod 60).ToString("00")
                objWriter.Write(tmp)
            End If
            objWriter.Write(Space(2))


            objWriter.Write(strOtAmount11 & Space(3) & strOsDuration6 & Space(4))

            If Rs_Report.Tables(0).Rows(i).Item("IN1MANNUAL").ToString.Trim = "" Then strIn1Mannual_1 = "" Else strIn1Mannual_1 = Rs_Report.Tables(0).Rows(i).Item("IN1MANNUAL").ToString.Trim
            If Rs_Report.Tables(0).Rows(i).Item("Out1Mannual").ToString.Trim = "" Then strOut1Mannual_1 = "" Else strOut1Mannual_1 = Rs_Report.Tables(0).Rows(i).Item("Out1Mannual").ToString.Trim
            If Rs_Report.Tables(0).Rows(i).Item("IN2MANNUAL").ToString.Trim = "" Then strIn2Mannual_1 = "" Else strIn2Mannual_1 = Rs_Report.Tables(0).Rows(i).Item("IN2MANNUAL").ToString.Trim
            If Rs_Report.Tables(0).Rows(i).Item("OUT2MANNUAL").ToString.Trim = "" Then strOut2Mannual_1 = "" Else strOut2Mannual_1 = Rs_Report.Tables(0).Rows(i).Item("OUT2MANNUAL").ToString.Trim
            objWriter.Write(Space(6))
            If UCase(strIn1Mannual_1) = "Y" Or UCase(strOut1Mannual_1) = "Y" Or UCase(strIn2Mannual_1) = "Y" Or UCase(strOut2Mannual_1) = "Y" Then
                objWriter.Write("Y")
            Else
                objWriter.Write("")
            End If
            msrl = msrl + 1
            mintLine = mintLine + 1
            mblnCheckReport = True
        Next
        objWriter.Close()
        If isPdf = True Then
            'pdf start 
            Common.pdfreport(mstrFile_Name, 6, ulf)
            'pdf end 
        Else
            Try : Process.Start(mstrFile_Name) : Catch ex As Exception : Process.Start("notepad.exe", mstrFile_Name) : End Try
        End If

    End Sub
    Sub Daily_EarlyDeparture(ByVal strsortorder As String)
        'On Error GoTo ErrorGen

        Dim strDeptDivCode As String
        Dim strCurrentDeptDivCode As String
        Dim intFile As Integer
        Dim strsql As String
        Dim strOsDuration6 As String '* 6
        Dim strDepartmentCode As String
        Dim intPrintLineCounter As Integer
        Dim blnDeptAvailable As Boolean, mFirst As Boolean
        Dim msrl As Integer
        msrl = 1

        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If

        mblnCheckReport = False
        mFirst = True
        mintPageNo = 1
        mintLine = 1

        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet

        Dim g_HODDepartmentCode As String = ""
        'If Left(strsortorder, 1) = "D" Or Left(strsortorder, 1) = "C" Then blnDeptAvailable = True
        If strsortorder = "" Then

            'If g_Report_view Then
            If Common.servername = "Access" Then
                'strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName,tblTimeRegister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tblEmployeeShiftMaster.IsRoundTheClockWork,tbltimeregister.ShiftAttended,tbltimeregister.ShiftEndTime,tbltimeregister.Out2," & _
                ' "tbltimeregister.EarlyDeparture,tbltimeregister.Status,tbltimeregister.OtDuration,tbltimeregister.OsDuration,tbltimeregister.In1Mannual,tbltimeregister.In2Mannual,tbltimeregister.Out1Mannual,tbltimeregister.Out2Mannual,tblcalander.mDate,tblcalander.Process,tblcalander.NRTCProc" & _
                ' " from tblCatagory,tblDivision,  tbltimeregister,tblEmployee,tblEmployeeShiftMaster ,tblCalander ,tblCompany,tblDepartment" & _
                ' " Where tblCatagory.Cat = tblEmployee.Cat And tblDivision.DivisionCode = tblEmployee.DivisionCode And tbltimeRegister.EarlyDeparture > 0 And tbltimeregister.PayCode = tblEmployee.PayCode And tbltimeregister.PayCode = tblEmployeeShiftMaster.PayCode And FORMAT(tbltimeregister.DateOffice,'MMM DD YYYY') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & " And FORMAT(tblcalander.mDate,'MMM DD YYYY') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
                ' " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
                ' " And (tblemployee.LeavingDate >=#" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "# or tblemployee.LeavingDate is null)" & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause)

                strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName,tblTimeRegister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tblEmployeeShiftMaster.IsRoundTheClockWork,tbltimeregister.ShiftAttended,tbltimeregister.ShiftEndTime,tbltimeregister.Out2," & _
                 "tbltimeregister.EarlyDeparture,tbltimeregister.Status,tbltimeregister.OtDuration,tbltimeregister.OsDuration,tbltimeregister.In1Mannual,tbltimeregister.In2Mannual,tbltimeregister.Out1Mannual,tbltimeregister.Out2Mannual,tblcalander.mDate,tblcalander.Process,tblcalander.NRTCProc" & _
                 " from tblCatagory,tblDivision,  tbltimeregister,tblEmployee,tblEmployeeShiftMaster ,tblCalander ,tblCompany,tblDepartment" & _
                 " Where tblCatagory.Cat = tblEmployee.Cat And tbltimeRegister.EarlyDeparture > 0 And tbltimeregister.PayCode = tblEmployee.PayCode And tbltimeregister.PayCode = tblEmployeeShiftMaster.PayCode And FORMAT(tbltimeregister.DateOffice,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & " And FORMAT(tblcalander.mDate,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
                 " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
                 " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & "order by tblEmployee.PresentCARDNO, TblTimeRegister.DateOffice"
                If Common.IsCompliance Then
                    strsql = Microsoft.VisualBasic.Strings.Replace(strsql, "tbltimeregister", "tblTimeRegisterD", 1, -1, Microsoft.VisualBasic.Constants.vbTextCompare)
                End If
                adapA = New OleDbDataAdapter(strsql, Common.con1)
                adapA.Fill(Rs_Report)
            Else
                strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName,tblTimeRegister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tblEmployeeShiftMaster.IsRoundTheClockWork,tbltimeregister.ShiftAttended,tbltimeregister.ShiftEndTime,tbltimeregister.Out2," & _
                 "tbltimeregister.EarlyDeparture,tbltimeregister.Status,tbltimeregister.OtDuration,tbltimeregister.OsDuration,tbltimeregister.In1Mannual,tbltimeregister.In2Mannual,tbltimeregister.Out1Mannual,tbltimeregister.Out2Mannual,tblcalander.mDate,tblcalander.Process,tblcalander.NRTCProc" & _
                 " from tblCatagory,tblDivision,  tbltimeregister,tblEmployee,tblEmployeeShiftMaster ,tblCalander ,tblCompany,tblDepartment" & _
                 " Where tblCatagory.Cat = tblEmployee.Cat And tbltimeRegister.EarlyDeparture > 0 And tbltimeregister.PayCode = tblEmployee.PayCode And tbltimeregister.PayCode = tblEmployeeShiftMaster.PayCode And tbltimeregister.DateOffice = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & " And tblcalander.mDate = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
                 " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
                 " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & "order by tblEmployee.PresentCARDNO, TblTimeRegister.DateOffice"
                If Common.IsCompliance Then
                    strsql = Microsoft.VisualBasic.Strings.Replace(strsql, "tbltimeregister", "tblTimeRegisterD", 1, -1, Microsoft.VisualBasic.Constants.vbTextCompare)
                End If
                adap = New SqlDataAdapter(strsql, Common.con)
                adap.Fill(Rs_Report)
            End If
            'Else
            '    strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName,tblTimeRegisterD.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tblTimeRegisterD.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tblEmployeeShiftMaster.IsRoundTheClockWork,tblTimeRegisterD.ShiftAttended,tblTimeRegisterD.ShiftEndTime,tblTimeRegisterD.Out2," & _
            '         "tblTimeRegisterD.EarlyDeparture,tblTimeRegisterD.Status,tblTimeRegisterD.OtDuration,tblTimeRegisterD.OsDuration,tblTimeRegisterD.In1Mannual,tblTimeRegisterD.In2Mannual,tblTimeRegisterD.Out1Mannual,tblTimeRegisterD.Out2Mannual,tblcalander.mDate,tblcalander.Process,tblcalander.NRTCProc" & _
            '         " from tblCatagory,tblDivision,  tblTimeRegisterD,tblEmployee,tblEmployeeShiftMaster ,tblCalander ,tblCompany,tblDepartment" & _
            '         " Where tblCatagory.Cat = tblEmployee.Cat And tblDivision.DivisionCode = tblEmployee.DivisionCode And tblTimeRegisterD.EarlyDeparture > 0 And tblTimeRegisterD.PayCode = tblEmployee.PayCode And tblTimeRegisterD.PayCode = tblEmployeeShiftMaster.PayCode And tblTimeRegisterD.DateOffice = '" & Format(frmDailyAttReport.txtFromDate.Value, "MMM dd yyyy") & "'" & " And tblcalander.mDate = '" & Format(frmDailyAttReport.txtFromDate.Value, "MMM dd yyyy") & "'" & _
            '         " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
            '         " And (tblemployee.LeavingDate>='" & Format(frmDailyAttReport.txtFromDate, "MMM dd yyyy") & "' or tblemployee.LeavingDate is null)" & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause)

            'End If
            'Rs_Report = Cn.Execute(strsql)
            If Rs_Report.Tables(0).Rows.Count < 1 Then
                'G_Report_Error = False
                'Screen.MousePointer = vbDefault
                XtraMessageBox.Show(ulf, "<size=10>Nobody has departed before the Shift End Time on this Date.</size>", "<size=9>iAS</size>")
                'mReportPrintStatus = False
                Exit Sub
            End If
            'Rs_Report.Sort = "PayCode"
        Else
            'Rs_Report.Sort = strsortorder
        End If

        If Rs_Report.Tables(0).Rows(0).Item("Process").ToString.Trim = "" And Rs_Report.Tables(0).Rows(0).Item("NRTCProc").ToString.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Records are not Processed</size>", "<size=9>iAS</size>")
            'mReportPrintStatus = False
            Exit Sub
        ElseIf InStr("YX", Rs_Report.Tables(0).Rows(0).Item("Process").ToString.Trim) > 0 Or InStr("Y", Rs_Report.Tables(0).Rows(0).Item("nRTCProc").ToString.Trim) > 0 Then

            mFileNumber = FreeFile()
            mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".SRP"
            Dim objWriter As New System.IO.StreamWriter(mstrFile_Name, True)

            'With Rs_Report
            '    Do While Not .EOF
            For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
                ' If mFirst Or mintLine > g_LinesPerPage Or (Left(strsortorder, 10) = "Department" And strDeptDivCode <> !DepartmentCode And g_SkipAfterDept) Then
                If mFirst Or mintLine > g_LinesPerPage Or (strsortorder.Trim = "Department" And strDeptDivCode <> Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim And g_SkipAfterDept) Then
1:                  If Not mFirst Then
                        objWriter.WriteLine(Space(2) & vbFormFeed)
                    Else
                        mFirst = False
                    End If

                    objWriter.WriteLine(Space(2) & Space(40) & CommonReport.g_CompanyNames)
                    objWriter.WriteLine(Space(2) + "")
                    objWriter.WriteLine(Space(2) + "Page No." & mintPageNo)
                    objWriter.WriteLine(Space(2) + "                                 Early Departure for " & ForDate) 'DateEdit1.DateTime.ToString("dd/MM/yyyy"))
                    objWriter.WriteLine(Space(2) + "                                 Run Date & Time : " & Common.runningDateTime) 'Now().ToString("dd/MM/yyyy HH:mm"))
                    objWriter.WriteLine(Space(2) + "--------------------------------------------------------------------------------------------------------------")
                    objWriter.WriteLine(Space(2) + "Srl  EmpCode      Cardno       Employee Name             RTC Shift End   Out   Shift Status Ot    Over   Manual")
                    objWriter.WriteLine(Space(2) + "No.                                                                            Early              Stay")
                    objWriter.WriteLine(Space(2) + "---------------------------------------------------------------------------------------------------------------")
                    mintLine = 9
                    mintPageNo = mintPageNo + 1
                End If
                'If blnDeptAvailable And Not .EOF Then
                '    If Left(strsortorder, 10) = "Department" And strDeptDivCode <> !DepartmentCode Then
                '        If mintLine + 3 > g_LinesPerPage Then GoTo 1
                '    Print #intFile, Space(2) + ""
                '        strDeptDivCode = !DepartmentCode
                '    Print #intFile, Space(2) + "** Department Code & Name : " & !DepartmentCode & "  " & !DepartmentName
                '    Print #intFile, Space(2) + ""
                '        mintLine = mintLine + 3
                '    ElseIf Left(strsortorder, 8) = "Division" And strDeptDivCode <> !DivisionCode Then
                '        If mintLine + 3 > g_LinesPerPage Then GoTo 1
                '    Print #intFile, Space(2) + ""
                '        strDeptDivCode = !DivisionCode
                '    Print #intFile, Space(2) + "** Section Code & Name : " & !DivisionCode & "  " & !DivisionName
                '    Print #intFile, Space(2) + ""
                '        mintLine = mintLine + 3
                '    ElseIf Left(strsortorder, 3) = "Cat" And strDeptDivCode <> !Cat Then
                '        If mintLine + 3 > g_LinesPerPage Then GoTo 1
                '    Print #intFile, Space(2) + ""
                '        strDeptDivCode = !Cat
                '    Print #intFile, Space(2) + "** Category Code & Name : " & !Cat & "  " & !CatagoryName
                '    Print #intFile, Space(2) + ""
                '        mintLine = mintLine + 3
                '    End If
                'End If


                If Rs_Report.Tables(0).Rows(i).Item("OsDuration").ToString.Trim <> "" Then
                    If Rs_Report.Tables(0).Rows(i).Item("OsDuration").ToString.Trim <> "0" Then
                        Dim tmp As String = Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("OsDuration").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("OsDuration").ToString.Trim Mod 60).ToString("00")
                        strOsDuration6 = Space(1) & tmp
                    End If
                End If

                'Print #intFile, Space(2) + Format(msrl, "@@@@") & " " & !paycode & " " & !presentcardno & " " & !EmpName & " " & !isroundtheclockwork & Space(3) & !ShiftAttended & Space(3) & _
                '        IIf(Format(!shiftEndTime, "HH:HH") = "  :  " Or Format(!shiftEndTime, "HH:MM") = "00:00" Or IsNull(!shiftEndTime), Space(5), Format(!shiftEndTime, "HH:MM")) & Space(1) & _
                '        IIf(IsNull(!out2) Or Format(!out2, "HH:MM") = "  :  ", Space(5), Format(!out2, "HH:MM")) & Space(1) & _
                '        IIf(IsNull(!earlydeparture) Or !earlydeparture = 0, Space(5), String(5 - Len(Trim(Length5(Min2Hr(!earlydeparture)))), " ") & Trim(Length5(Min2Hr(!earlydeparture)))) & Space(1) & _
                '        IIf(IsNull(!Status) Or Len(Trim(!Status)) = 0, Space(5), !Status) & Space(2) & _
                '        IIf(IsNull(!OtDuration) Or !OtDuration = 0, Space(5), String(5 - Len(Trim(Length5(Min2Hr(!OtDuration)))), " ") & Trim(Length5(Min2Hr(!OtDuration)))) & Space(1) & _
                '        strOsDuration6 & Space(1) & _
                '        IIf(UCase(!IN1MANNUAL) = "Y", "M", IIf(UCase(!Out1Mannual) = "Y", "M", IIf(UCase(!IN2MANNUAL) = "Y", "M", IIf(UCase(!OUT2MANNUAL) = "Y", "M", ""))))

                objWriter.WriteLine("")
                objWriter.Write(Space(2) & msrl.ToString.PadRight(4) & " " & Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim.PadRight(12) & " " & Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim.PadRight(12) & " " & Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim.PadRight(25) & " " & Rs_Report.Tables(0).Rows(i).Item("isroundtheclockwork").ToString.Trim & Space(3) & Rs_Report.Tables(0).Rows(i).Item("ShiftAttended").ToString.Trim & Space(3))
                objWriter.Write(IIf(Rs_Report.Tables(0).Rows(i).Item("shiftEndTime").ToString.Trim = "" Or Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("shiftEndTime").ToString.Trim).ToString("HH:mm") = "00:00", Space(5), Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("shiftEndTime").ToString.Trim).ToString("HH:mm") & Space(1)))
                objWriter.Write(IIf(Rs_Report.Tables(0).Rows(i).Item("out2").ToString.Trim = "" Or Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("out2").ToString.Trim).ToString("HH:mm") = "00:00", Space(5), Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("out2").ToString.Trim).ToString("HH:mm") & Space(1)))
                'objWriter.Write(IIf(Rs_Report.Tables(0).Rows(i).Item("earlydeparture").ToString.Trim = "" Or Rs_Report.Tables(0).Rows(i).Item("earlydeparture").ToString.Trim = "0", Space(5), Space(5 - Len(Trim(Length5(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("earlydeparture").ToString.Trim / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("earlydeparture").ToString.Trim Mod 60).ToString("00"))))) & Trim(Length5(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("earlydeparture").ToString.Trim / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("earlydeparture").ToString.Trim Mod 60).ToString("00"))) & Space(1)))
                objWriter.Write(IIf(Rs_Report.Tables(0).Rows(i).Item("earlydeparture").ToString.Trim = "" Or Rs_Report.Tables(0).Rows(i).Item("earlydeparture").ToString.Trim = "0", Space(5), Space(1) & Math.Truncate(Convert.ToInt32((Rs_Report.Tables(0).Rows(i).Item("earlydeparture").ToString.Trim)) / 60).ToString("00") & ":" & Convert.ToInt32((Rs_Report.Tables(0).Rows(i).Item("earlydeparture").ToString.Trim) Mod 60).ToString("00")) & Space(1))
                objWriter.Write(IIf(Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim = "", Space(5), Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim & Space(2)))
                objWriter.Write(IIf(Rs_Report.Tables(0).Rows(i).Item("OtDuration").ToString.Trim = "" Or Rs_Report.Tables(0).Rows(i).Item("OtDuration").ToString.Trim = "0", Space(5), Space(1) & Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("OtDuration").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("OtDuration").ToString.Trim Mod 60).ToString("00")) & Space(1))
                objWriter.Write(strOsDuration6 & Space(1))
                objWriter.Write(IIf(UCase(Rs_Report.Tables(0).Rows(i).Item("IN1MANNUAL").ToString.Trim) = "Y", "M", IIf(UCase(Rs_Report.Tables(0).Rows(i).Item("Out1Mannual").ToString.Trim) = "Y", "M", IIf(UCase(Rs_Report.Tables(0).Rows(i).Item("IN2MANNUAL").ToString.Trim) = "Y", "M", IIf(UCase(Rs_Report.Tables(0).Rows(i).Item("OUT2MANNUAL").ToString.Trim) = "Y", "M", "")))))

                msrl = msrl + 1
                mintLine = mintLine + 1
                mblnCheckReport = True
            Next
            objWriter.Close()
            If isPdf = True Then
                'pdf start 
                Common.pdfreport(mstrFile_Name, 9, ulf)
                'pdf end 
            Else
                Try : Process.Start(mstrFile_Name) : Catch ex As Exception : Process.Start("notepad.exe", mstrFile_Name) : End Try
            End If
        Else
            XtraMessageBox.Show(ulf, "<size=10>Records are not Processed</size>", "<size=9>iAS</size>")
            'mReportPrintStatus = False
            Exit Sub
        End If
    End Sub
    Sub Daily_TimeLoss(ByVal strsortorder As String)
        'On Error GoTo ErrorGen
        Dim strDeptDivCode As String
        Dim strCurrentDeptDivCode As String
        Dim intFile As Integer
        Dim strsql As String
        Dim strOsDuration6 As String ' * 6
        Dim strDepartmentCode As String
        Dim intPrintLineCounter As Integer
        Dim blnDeptAvailable As Boolean, mFirst As Boolean
        Dim msrl As Integer
        msrl = 1

        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If

        mblnCheckReport = False
        mFirst = True
        mintPageNo = 1
        mintLine = 1
        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet

        'If Left(strsortorder, 1) = "D" Or Left(strsortorder, 1) = "C" Then blnDeptAvailable = True
        If strsortorder = "" Then
            'If g_Report_view Then
            If Common.servername = "Access" Then
                'strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tblTimeRegister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tbltimeregister.ShiftAttended,tbltimeregister.ShiftStartTime,tbltimeregister.ShiftEndTime,tbltimeregister.EarlyArrival," & _
                ' "tbltimeregister.LateArrival,tbltimeregister.EarlyDeparture,tbltimeregister.Status,tbltimeregister.LunchLateArrival,tbltimeregister.LunchEarlyDeparture,tbltimeregister.ExcLunchHours,tbltimeregister.OtDuration," & _
                ' "tbltimeregister.TotalLossHrs,tbltimeregister.OsDuration,tbltimeregister.In1Mannual,tbltimeregister.In2Mannual,tbltimeregister.Out1Mannual,tbltimeregister.Out2Mannual,tblCalander.mDate,tblCalander.Process,tblCalander.NRTCProc" & _
                ' " from tblCatagory,tblDivision,  tbltimeregister,tblEmployee,tblCalander,tblCompany,tblDepartment" & _
                ' " Where tblCatagory.Cat = tblEmployee.Cat And tblDivision.DivisionCode = tblEmployee.DivisionCode And tblTimeRegister.totallosshrs > 0 and tbltimeregister.PayCode = tblEmployee.PayCode And FORMAT(tbltimeregister.DateOffice,'MMM DD YYYY') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' And FORMAT(tblCalander.mDate,'MMM DD YYYY') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
                ' " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
                ' " And (tblemployee.LeavingDate >=#" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "# or tblemployee.LeavingDate is null)" & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause)


                strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tblTimeRegister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tbltimeregister.ShiftAttended,tbltimeregister.ShiftStartTime,tbltimeregister.ShiftEndTime,tbltimeregister.EarlyArrival," & _
                "tbltimeregister.LateArrival,tbltimeregister.EarlyDeparture,tbltimeregister.Status,tbltimeregister.LunchLateArrival,tbltimeregister.LunchEarlyDeparture,tbltimeregister.ExcLunchHours,tbltimeregister.OtDuration," & _
                "tbltimeregister.TotalLossHrs,tbltimeregister.OsDuration,tbltimeregister.In1Mannual,tbltimeregister.In2Mannual,tbltimeregister.Out1Mannual,tbltimeregister.Out2Mannual,tblCalander.mDate,tblCalander.Process,tblCalander.NRTCProc" & _
                " from tblCatagory,tblDivision,  tbltimeregister,tblEmployee,tblCalander,tblCompany,tblDepartment" & _
                " Where tblCatagory.Cat = tblEmployee.Cat And tblTimeRegister.totallosshrs > 0 and tbltimeregister.PayCode = tblEmployee.PayCode And FORMAT(tbltimeregister.DateOffice,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' And FORMAT(tblCalander.mDate,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
                " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
                " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & "order by tblEmployee.PresentCARDNO, TblTimeRegister.DateOffice"


                adapA = New OleDbDataAdapter(strsql, Common.con1)
                adapA.Fill(Rs_Report)
            Else
                strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tblTimeRegister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tbltimeregister.ShiftAttended,tbltimeregister.ShiftStartTime,tbltimeregister.ShiftEndTime,tbltimeregister.EarlyArrival," & _
                 "tbltimeregister.LateArrival,tbltimeregister.EarlyDeparture,tbltimeregister.Status,tbltimeregister.LunchLateArrival,tbltimeregister.LunchEarlyDeparture,tbltimeregister.ExcLunchHours,tbltimeregister.OtDuration," & _
                 "tbltimeregister.TotalLossHrs,tbltimeregister.OsDuration,tbltimeregister.In1Mannual,tbltimeregister.In2Mannual,tbltimeregister.Out1Mannual,tbltimeregister.Out2Mannual,tblCalander.mDate,tblCalander.Process,tblCalander.NRTCProc" & _
                 " from tblCatagory,tblDivision,  tbltimeregister,tblEmployee,tblCalander,tblCompany,tblDepartment" & _
                 " Where tblCatagory.Cat = tblEmployee.Cat And tblTimeRegister.totallosshrs > 0 and tbltimeregister.PayCode = tblEmployee.PayCode And tbltimeregister.DateOffice = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' And tblCalander.mDate = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
                 " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
                 " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & "order by tblEmployee.PresentCARDNO, TblTimeRegister.DateOffice"
                adap = New SqlDataAdapter(strsql, Common.con)
                adap.Fill(Rs_Report)
            End If
            'Else
            '    strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tbltimeregisterD.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregisterD.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tbltimeregisterD.ShiftAttended,tbltimeregisterD.ShiftStartTime,tbltimeregisterD.ShiftEndTime,tbltimeregisterD.EarlyArrival," & _
            '         "tbltimeregisterD.LateArrival,tbltimeregisterD.EarlyDeparture,tbltimeregisterD.Status,tbltimeregisterD.LunchLateArrival,tbltimeregisterD.LunchEarlyDeparture,tbltimeregisterD.ExcLunchHours,tbltimeregisterD.OtDuration," & _
            '         "tbltimeregisterD.TotalLossHrs,tbltimeregisterD.OsDuration,tbltimeregisterD.In1Mannual,tbltimeregisterD.In2Mannual,tbltimeregisterD.Out1Mannual,tbltimeregisterD.Out2Mannual,tblCalander.mDate,tblCalander.Process,tblCalander.NRTCProc" & _
            '         " from tblCatagory,tblDivision,  tbltimeregisterD,tblEmployee,tblCalander,tblCompany,tblDepartment" & _
            '         " Where tblCatagory.Cat = tblEmployee.Cat And tblDivision.DivisionCode = tblEmployee.DivisionCode And tbltimeregisterD.totallosshrs > 0 and tbltimeregisterD.PayCode = tblEmployee.PayCode And tbltimeregisterD.DateOffice = '" & Format(frmDailyAttReport.txtFromDate.Value, "MMM dd yyyy") & "' And tblCalander.mDate = '" & Format(frmDailyAttReport.txtFromDate.Value, "MMM dd yyyy") & "'" & _
            '         " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
            '         " And (tblemployee.LeavingDate>='" & Format(frmDailyAttReport.txtFromDate, "MMM dd yyyy") & "' or tblemployee.LeavingDate is null)" & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause)
            'End If
            'Rs_Report = Cn.Execute(strsql)
            If Rs_Report.Tables(0).Rows.Count < 1 Then
                'G_Report_Error = False
                'Screen.MousePointer = vbDefault
                XtraMessageBox.Show(ulf, "<size=10>There is no Time Loss for any Employee on this Date.</size>", "<size=9>iAS</size>")
                'mReportPrintStatus = False
                Exit Sub
            End If
            'Rs_Report.Sort = "PayCode"
        Else
            'Rs_Report.Sort = strsortorder
        End If

        'If IsNull(Rs_Report!Process) And IsNull(Rs_Report!NRTCProc) Then
        '    MsgBox "Records are not Processed", vbInformation + vbExclamation, "TimeWatch  Information"
        '    mReportPrintStatus = False
        '    Exit Sub
        'ElseIf InStr("YX", Rs_Report("Process")) > 0 Or InStr("Y", Rs_Report("nRTCProc")) > 0 Then
        mFileNumber = FreeFile()
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".SRP"
        Dim objWriter As New System.IO.StreamWriter(mstrFile_Name, True)

        'With Rs_Report
        'Do While Not .EOF
        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            If mFirst Or mintLine > g_LinesPerPage Or (strsortorder.Trim = "Department" And strDeptDivCode <> Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim And g_SkipAfterDept) Then
                'If mFirst Or mintLine > g_LinesPerPage Or (Left(strsortorder, 10) = "Department" And strDeptDivCode <> !DepartmentCode And g_SkipAfterDept) Then
1:              If Not mFirst Then
                    objWriter.WriteLine(Space(2) & vbFormFeed)
                Else
                    mFirst = False
                End If
                objWriter.WriteLine(Space(2) + Space(40) & CommonReport.g_CompanyNames)
                objWriter.WriteLine(Space(2) + "")
                objWriter.WriteLine(Space(2) + "Page No." & mintPageNo)
                objWriter.WriteLine(Space(2) + "                                                                                                     Run Date & Time : " & Common.runningDateTime) 'Now.ToString("dd/MM/yyyy HH:mm"))
                objWriter.WriteLine(Space(2) + "                                                       Time Loss for " & ForDate) 'DateEdit1.DateTime.ToString("dd/MM/yyyy"))
                objWriter.WriteLine(Space(2) + "------------------------------------------------------------------------------------------------------------------------------------------")
                objWriter.WriteLine(Space(2) + "Srl  EmpCode      Cardno       Employee Name             Shift Start End   Status Early    Shift Shift Lunch Lunch Excess Ot    Loss  Over")
                objWriter.WriteLine(Space(2) + "No.                                                                               Arrival  Late  Early Late  Early Lunch        hours Stay")
                objWriter.WriteLine(Space(2) + "------------------------------------------------------------------------------------------------------------------------------------------")
                mintLine = 9
                mintPageNo = mintPageNo + 1
            End If

            'If blnDeptAvailable And Not .EOF Then
            '    If Left(strsortorder, 10) = "Department" And strDeptDivCode <> !DepartmentCode Then
            '        If mintLine + 3 > g_LinesPerPage Then GoTo 1
            '        Print #intFile, Space(2) + ""
            '        strDeptDivCode = !DepartmentCode
            '        Print #intFile, Space(2) + "** Department Code & Name : " & !DepartmentCode & "  " & !DepartmentName
            '        Print #intFile, Space(2) + ""
            '        mintLine = mintLine + 3
            '    ElseIf Left(strsortorder, 8) = "Division" And strDeptDivCode <> !DivisionCode Then
            '        If mintLine + 3 > g_LinesPerPage Then GoTo 1
            '        Print #intFile, Space(2) + ""
            '        strDeptDivCode = !DivisionCode
            '        Print #intFile, Space(2) + "** Section Code & Name : " & !DivisionCode & "  " & !DivisionName
            '        Print #intFile, Space(2) + ""
            '        mintLine = mintLine + 3
            '    ElseIf Left(strsortorder, 3) = "Cat" And strDeptDivCode <> !Cat Then
            '        If mintLine + 3 > g_LinesPerPage Then GoTo 1
            '        Print #intFile, Space(2) + ""
            '        strDeptDivCode = !Cat
            '        Print #intFile, Space(2) + "** Category Code & Name : " & !Cat & "  " & !CatagoryName
            '        Print #intFile, Space(2) + ""
            '        mintLine = mintLine + 3
            '    End If
            'End If
            strOsDuration6 = ""
            If Rs_Report.Tables(0).Rows(i).Item("OsDuration").ToString.Trim = "" Then If Rs_Report.Tables(0).Rows(i).Item("OsDuration").ToString.Trim <> "0" Then strOsDuration6 = Space(1) & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("OsDuration").ToString.Trim / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("OsDuration").ToString.Trim Mod 60).ToString("00")

            objWriter.WriteLine("")
            objWriter.Write(Space(2) & msrl.ToString.PadRight(4) & " " & Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim.PadRight(12) & " " & Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim.PadRight(12) & " " & Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim.PadRight(25) & " " & Rs_Report.Tables(0).Rows(i).Item("ShiftAttended").ToString & Space(3))
            objWriter.Write(IIf(Rs_Report.Tables(0).Rows(i).Item("shiftStartTime").ToString.Trim = "" Or Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("shiftStartTime").ToString.Trim).ToString("HH:mm") = "00:00", Space(5), Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("shiftStartTime").ToString.Trim).ToString("HH:mm") & Space(1)))
            objWriter.Write(IIf(Rs_Report.Tables(0).Rows(i).Item("shiftEndTime").ToString.Trim = "" Or Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("shiftEndTime").ToString.Trim).ToString("HH:mm") = "00:00", Space(5), Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("shiftEndTime").ToString.Trim).ToString("HH:mm") & Space(1)))
            objWriter.Write(IIf(Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim = "", Space(5), Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim) & Space(2))
            objWriter.Write(IIf(Rs_Report.Tables(0).Rows(i).Item("EARLYARRIVAL").ToString.Trim = "" Or Rs_Report.Tables(0).Rows(i).Item("EARLYARRIVAL").ToString.Trim = "0", Space(5), Space(1) & Trim(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("EARLYARRIVAL").ToString.Trim / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("EARLYARRIVAL").ToString.Trim Mod 60).ToString("00"))) & Space(3))
            objWriter.Write(IIf(Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim = "" Or Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim = "0", Space(5), Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim Mod 60).ToString("00")) & Space(1))
            objWriter.Write(IIf(Rs_Report.Tables(0).Rows(i).Item("earlydeparture").ToString.Trim = "" Or Rs_Report.Tables(0).Rows(i).Item("earlydeparture").ToString.Trim = "0", Space(5), Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("earlydeparture").ToString.Trim / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("earlydeparture").ToString.Trim Mod 60).ToString("00")) & Space(1))
            objWriter.Write(IIf(Rs_Report.Tables(0).Rows(i).Item("LunchLateArrival").ToString.Trim = "" Or Rs_Report.Tables(0).Rows(i).Item("LunchLateArrival").ToString.Trim = "0", Space(5), Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("LunchLateArrival").ToString.Trim / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("LunchLateArrival").ToString.Trim Mod 60).ToString("00")) & Space(1))
            objWriter.Write(IIf(Rs_Report.Tables(0).Rows(i).Item("LUNCHEARLYDEPARTURE").ToString.Trim = "" Or Rs_Report.Tables(0).Rows(i).Item("LUNCHEARLYDEPARTURE").ToString.Trim = "0", Space(5), Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("LUNCHEARLYDEPARTURE").ToString.Trim / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("LUNCHEARLYDEPARTURE").ToString.Trim Mod 60).ToString("00")) & Space(1))
            objWriter.Write(IIf(Rs_Report.Tables(0).Rows(i).Item("EXCLUNCHHOURS").ToString.Trim = "" Or Rs_Report.Tables(0).Rows(i).Item("EXCLUNCHHOURS").ToString.Trim = "0", Space(5), Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("EXCLUNCHHOURS").ToString.Trim / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("EXCLUNCHHOURS").ToString.Trim Mod 60).ToString("00")) & Space(1))
            objWriter.Write(IIf(Rs_Report.Tables(0).Rows(i).Item("OtDuration").ToString.Trim = "" Or Rs_Report.Tables(0).Rows(i).Item("OtDuration").ToString.Trim = "0", Space(5), Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("OtDuration").ToString.Trim / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("OtDuration").ToString.Trim Mod 60).ToString("00")) & Space(1))
            objWriter.Write(IIf(Rs_Report.Tables(0).Rows(i).Item("TotalLossHrs").ToString.Trim = "" Or Rs_Report.Tables(0).Rows(i).Item("TotalLossHrs").ToString.Trim = "0", Space(5), Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("TotalLossHrs").ToString.Trim / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("TotalLossHrs").ToString.Trim Mod 60).ToString("00")) & Space(1))
            objWriter.Write(strOsDuration6 & Space(1))
            objWriter.Write(IIf(UCase(Rs_Report.Tables(0).Rows(i).Item("IN1MANNUAL").ToString.Trim) = "Y", "M", IIf(UCase(Rs_Report.Tables(0).Rows(i).Item("Out1Mannual").ToString.Trim) = "Y", "M", IIf(UCase(Rs_Report.Tables(0).Rows(i).Item("IN2MANNUAL").ToString.Trim) = "Y", "M", IIf(UCase(Rs_Report.Tables(0).Rows(i).Item("OUT2MANNUAL").ToString.Trim) = "Y", "M", "")))))
            msrl = msrl + 1
            mintLine = mintLine + 1
            mblnCheckReport = True
            '                .MoveNext()
            '            Loop
        Next
        objWriter.Close()
        If isPdf = True Then
            'pdf start 
            Common.pdfreport(mstrFile_Name, 7, ulf)
            'pdf end 
        Else
            Try : Process.Start(mstrFile_Name) : Catch ex As Exception : Process.Start("notepad.exe", mstrFile_Name) : End Try
        End If
    End Sub
    Sub Daily_OverTimeRegister(ByVal strsortorder As String)
        'On Error GoTo ErrorGen
        Dim strDeptDivCode As String
        Dim strCurrentDeptDivCode As String
        Dim intFile As Integer
        Dim strsql As String
        Dim strOtAmount11 As String ' * 11
        Dim strDepartmentCode As String
        Dim intPrintLineCounter As Integer
        Dim blnDeptAvailable As Boolean, mFirst As Boolean
        Dim msrl As Integer
        msrl = 1

        mblnCheckReport = False

        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If

        mFirst = True
        mintPageNo = 1
        mintLine = 1

        Dim g_HODDepartmentCode As String = ""  'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet
        'If Left(strsortorder, 1) = "D" Or Left(strsortorder, 1) = "C" Then blnDeptAvailable = True
        If strsortorder = "" Then
            'If g_Report_view Then
            If Common.servername = "Access" Then
                'strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tblTimeRegister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tbltimeregister.OtDuration,tbltimeregister.OtAmount,tblCalander.mDate,tblCalander.Process,tblCalander.NRTCProc" & _
                ' " from tblCatagory,tblDivision,  tbltimeregister,tblEmployee,tblCalander,tblCompany,tblDepartment" & _
                ' " Where tblCatagory.Cat = tblEmployee.Cat And tblDivision.DivisionCode = tblEmployee.DivisionCode And tbltimeregister.OtDuration > 0 and tbltimeregister.PayCode = tblEmployee.PayCode And FORMAT(tbltimeregister.DateOffice,'MMM DD YYYY') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' And FORMAT(tblCalander.mDate,'MMM DD YYYY') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
                ' " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
                ' " And (tblemployee.LeavingDate >=#" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "# or tblemployee.LeavingDate is null)" & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause)

                strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tblTimeRegister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tbltimeregister.OtDuration,tbltimeregister.OtAmount,tblCalander.mDate,tblCalander.Process,tblCalander.NRTCProc" & _
                " from tblCatagory,tblDivision,  tbltimeregister,tblEmployee,tblCalander,tblCompany,tblDepartment" & _
                " Where tblCatagory.Cat = tblEmployee.Cat And tbltimeregister.OtDuration > 0 and tbltimeregister.PayCode = tblEmployee.PayCode And FORMAT(tbltimeregister.DateOffice,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' And FORMAT(tblCalander.mDate,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
                " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
                " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & "order by tblEmployee.PresentCARDNO, TblTimeRegister.DateOffice"
                adapA = New OleDbDataAdapter(strsql, Common.con1)
                adapA.Fill(Rs_Report)
            Else
                strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tblTimeRegister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tbltimeregister.OtDuration,tbltimeregister.OtAmount,tblCalander.mDate,tblCalander.Process,tblCalander.NRTCProc" & _
                 " from tblCatagory,tblDivision,  tbltimeregister,tblEmployee,tblCalander,tblCompany,tblDepartment" & _
                 " Where tblCatagory.Cat = tblEmployee.Cat And tbltimeregister.OtDuration > 0 and tbltimeregister.PayCode = tblEmployee.PayCode And tbltimeregister.DateOffice = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' And tblCalander.mDate = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
                 " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
                 " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & "order by tblEmployee.PresentCARDNO, TblTimeRegister.DateOffice"
                adap = New SqlDataAdapter(strsql, Common.con)
                adap.Fill(Rs_Report)
            End If
            'Else
            '    strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tbltimeregisterD.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregisterD.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tbltimeregisterD.OtDuration,tbltimeregisterD.OtAmount,tblCalander.mDate,tblCalander.Process,tblCalander.NRTCProc" & _
            '         " from tblCatagory,tblDivision,  tbltimeregisterD,tblEmployee,tblCalander,tblCompany,tblDepartment" & _
            '         " Where tblCatagory.Cat = tblEmployee.Cat And tblDivision.DivisionCode = tblEmployee.DivisionCode And tbltimeregisterD.OtDuration > 0 and tbltimeregisterD.PayCode = tblEmployee.PayCode And tbltimeregisterD.DateOffice = '" & Format(frmDailyAttReport.txtFromDate.Value, "MMM dd yyyy") & "' And tblCalander.mDate = '" & Format(frmDailyAttReport.txtFromDate.Value, "MMM dd yyyy") & "'" & _
            '         " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
            '         " And (tblemployee.LeavingDate>='" & Format(frmDailyAttReport.txtFromDate, "MMM dd yyyy") & "' or tblemployee.LeavingDate is null)" & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause)
            'End If
            'Rs_Report = Cn.Execute(strsql)
            If Rs_Report.Tables(0).Rows.Count < 1 Then
                'G_Report_Error = False
                'Screen.MousePointer = vbDefault
                XtraMessageBox.Show(ulf, "<size=10>There is no Over Time for any Employee on this Date.</size>", "<size=9>iAS</size>")
                'mReportPrintStatus = False
                Exit Sub
            End If
            'Rs_Report.Sort = "PayCode"
        Else
            'Rs_Report.Sort = strsortorder
        End If

        'If IsNull(Rs_Report!Process) And IsNull(Rs_Report!NRTCProc) Then
        '   MsgBox "Records are not Processed", vbInformation + vbExclamation, "TimeWatch  Information"
        '   mReportPrintStatus = False
        '   Exit Sub
        'ElseIf InStr("YX", Rs_Report("Process")) > 0 Or InStr("Y", Rs_Report("nRTCProc")) > 0 Then
        mFileNumber = FreeFile()
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".SRP"
        Dim objWriter As New System.IO.StreamWriter(mstrFile_Name, True)

        'With Rs_Report
        '    Do While Not .EOF
        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            If mFirst Or mintLine > g_LinesPerPage Or (strsortorder.Trim = "Department" And strDeptDivCode <> Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim And g_SkipAfterDept) Then
                'If mFirst Or mintLine > g_LinesPerPage Or (Left(strsortorder, 10) = "Department" And strDeptDivCode <> !DepartmentCode And g_SkipAfterDept) Then
1:              If Not mFirst Then
                    objWriter.WriteLine(Space(2) & vbFormFeed)
                Else
                    mFirst = False
                End If
                objWriter.WriteLine(Space(2) & Space(40) & CommonReport.g_CompanyNames)
                objWriter.WriteLine(Space(2) + "")
                objWriter.WriteLine(Space(2) + "Page No." & mintPageNo)
                objWriter.WriteLine(Space(2) + "                            Run Date & Time : " & Common.runningDateTime) 'Now.ToString("dd/MM/yyyy HH:mm"))
                objWriter.WriteLine(Space(2) + "                Over Time for " & ForDate) 'DateEdit1.DateTime.ToString("dd/MM/yyyy"))
                objWriter.WriteLine(Space(2) + "-----------------------------------------------------------------------")
                objWriter.WriteLine(Space(2) + "Srl  EmpCode      Cardno       Employee Name             Ot    Overtime")
                objWriter.WriteLine(Space(2) + "No.                                                             Amount")
                objWriter.WriteLine(Space(2) + "------------------------------------------------------------------------")
                mintPageNo = mintPageNo + 1
                mintLine = 9
            End If
            'If blnDeptAvailable And Not .EOF Then
            '    If Left(strsortorder, 10) = "Department" And strDeptDivCode <> !DepartmentCode Then
            '        If mintLine + 3 > g_LinesPerPage Then GoTo 1
            '        Print #intFile, Space(2) + ""
            '        strDeptDivCode = !DepartmentCode
            '        Print #intFile, Space(2) + "** Department Code & Name : " & !DepartmentCode & "  " & !DepartmentName
            '        Print #intFile, Space(2) + ""
            '        mintLine = mintLine + 3
            '    ElseIf Left(strsortorder, 8) = "Division" And strDeptDivCode <> !DivisionCode Then
            '        If mintLine + 3 > g_LinesPerPage Then GoTo 1
            '        Print #intFile, Space(2) + ""
            '        strDeptDivCode = !DivisionCode
            '        Print #intFile, Space(2) + "** Section Code & Name : " & !DivisionCode & "  " & !DivisionName
            '        Print #intFile, Space(2) + ""
            '        mintLine = mintLine + 3
            '    ElseIf Left(strsortorder, 3) = "Cat" And strDeptDivCode <> !Cat Then
            '        If mintLine + 3 > g_LinesPerPage Then GoTo 1
            '        Print #intFile, Space(2) + ""
            '        strDeptDivCode = !Cat
            '        Print #intFile, Space(2) + "** Category Code & Name : " & !Cat & "  " & !CatagoryName
            '        Print #intFile, Space(2) + ""
            '        mintLine = mintLine + 3
            '    End If
            'End If
            strOtAmount11 = ""
            If Rs_Report.Tables(0).Rows(i).Item("OTAmount").ToString.Trim <> "" Then
                If Rs_Report.Tables(0).Rows(i).Item("OTAmount").ToString.Trim <> "0" Then
                    strOtAmount11 = Space(7 - 5) & Trim(Length7(CStr(Rs_Report.Tables(0).Rows(i).Item("OTAmount").ToString.Trim)))
                End If
            End If
            objWriter.WriteLine(Space(2) & msrl.ToString.PadRight(4) & " " & Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim.PadRight(12) & " " & Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim.PadRight(12) & " " & Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim.PadRight(25) & " " & _
                                IIf(Rs_Report.Tables(0).Rows(i).Item("OtDuration").ToString.Trim = "" Or Rs_Report.Tables(0).Rows(i).Item("OtDuration").ToString.Trim = "0", Space(5), Space(1) & Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("OtDuration").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("OtDuration").ToString.Trim Mod 60).ToString("00") & Space(1) & _
                                strOtAmount11))
            msrl = msrl + 1
            mintLine = mintLine + 1
            mblnCheckReport = True
            '                .MoveNext()
            '            Loop
        Next
        objWriter.Close()
        If isPdf = True Then
            'pdf start 
            Common.pdfreport(mstrFile_Name, 8, ulf)
            'pdf end 
        Else
            Try : Process.Start(mstrFile_Name) : Catch ex As Exception : Process.Start("notepad.exe", mstrFile_Name) : End Try
        End If
    End Sub
    Sub Daily_OverTimeSummary()
        'On Error GoTo ErrorGen
        Dim intFile As Integer
        Dim strsql As String

        Dim strDepartmentName As String
        Dim strDepartmentName45 As String ' * 45
        Dim strOtDuration10 As String ' * 10
        Dim strOtAmount11 As String ' * 11
        Dim dblOtDuration As Double
        Dim dblOtAmount As Double
        Dim msrl As Integer
        msrl = 1

        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If

        mblnCheckReport = False
        mintPageNo = 1
        mintLine = 1
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet

        'If g_Report_view Then
        If Common.servername = "Access" Then
            'strsql = "select tblCatagory.CatagoryName, tblTimeRegister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregister.PayCode,tbltimeregister.OtDuration,tbltimeregister.OtAmount,tblDepartment.DepartmentName,tblCalander.mDate,tblCalander.Process,tblCalander.NRTCProc" & _
            ' " from tblCatagory,tbltimeregister,tblDepartment,tblCalander,tblEmployee " & _
            ' " Where tblCatagory.Cat = tblEmployee.Cat And tblTimeRegister.OtDuration > 0 And tbltimeregister.PayCode = tblEmployee.PayCode And TblEmployee.DepartmentCode = TblDepartment.DepartmentCode And FORMAT(tbltimeregister.DateOffice,'DD-MMM-YYYY') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' And FORMAT(tblCalander.mDate,'DD-MMM-YYYY') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
            ' " And (tblemployee.LeavingDate >=#" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "# or tblemployee.LeavingDate is null)"

            strsql = "select tblCatagory.CatagoryName, tblTimeRegister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregister.PayCode,tbltimeregister.OtDuration,tbltimeregister.OtAmount,tblDepartment.DepartmentName,tblCalander.mDate,tblCalander.Process,tblCalander.NRTCProc" & _
             " from tblCatagory,tbltimeregister,tblDepartment,tblCalander,tblEmployee " & _
             " Where tblCatagory.Cat = tblEmployee.Cat And tblTimeRegister.OtDuration > 0 And tbltimeregister.PayCode = tblEmployee.PayCode And TblEmployee.DepartmentCode = TblDepartment.DepartmentCode And FORMAT(tbltimeregister.DateOffice,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' And FORMAT(tblCalander.mDate,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
             "" & "order by tblEmployee.PresentCARDNO, TblTimeRegister.DateOffice"
            adapA = New OleDbDataAdapter(strsql, Common.con1)
            adapA.Fill(Rs_Report)
        Else
            strsql = "select tblCatagory.CatagoryName, tblTimeRegister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregister.PayCode,tbltimeregister.OtDuration,tbltimeregister.OtAmount,tblDepartment.DepartmentName,tblCalander.mDate,tblCalander.Process,tblCalander.NRTCProc" & _
             " from tblCatagory,tbltimeregister,tblDepartment,tblCalander,tblEmployee " & _
             " Where tblCatagory.Cat = tblEmployee.Cat And tblTimeRegister.OtDuration > 0 And tbltimeregister.PayCode = tblEmployee.PayCode And TblEmployee.DepartmentCode = TblDepartment.DepartmentCode And tbltimeregister.DateOffice = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' And tblCalander.mDate = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
             " " & "order by tblEmployee.PresentCARDNO, TblTimeRegister.DateOffice"
            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(Rs_Report)
        End If
        'Else
        '    strsql = "select tblCatagory.CatagoryName, tbltimeregisterD.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregisterD.PayCode,tbltimeregisterD.OtDuration,tbltimeregisterD.OtAmount,tblDepartment.DepartmentName,tblCalander.mDate,tblCalander.Process,tblCalander.NRTCProc" & _
        '         " from tblCatagory,tbltimeregisterD,tblDepartment,tblCalander,tblEmployee " & _
        '         " Where tblCatagory.Cat = tblEmployee.Cat And tbltimeregisterD.OtDuration > 0 And tbltimeregisterD.PayCode = tblEmployee.PayCode And TblEmployee.DepartmentCode = TblDepartment.DepartmentCode And tbltimeregisterD.DateOffice = '" & Format(frmDailyAttReport.txtFromDate.Value, "dd-MMM-yyyy") & "' And tblCalander.mDate = '" & Format(frmDailyAttReport.txtFromDate.Value, "dd-MMM-yyyy") & "'" & _
        '         " And (tblemployee.LeavingDate>='" & Format(frmDailyAttReport.txtFromDate, "MMM dd yyyy") & "' or tblemployee.LeavingDate is null)"
        'End If
        'Rs_Report = Cn.Execute(strsql)

        If Rs_Report.Tables(0).Rows.Count < 1 Then
            'G_Report_Error = False
            'Screen.MousePointer = vbDefault
            XtraMessageBox.Show(ulf, "<size=10>There is no Over Time for any Employee on this Date.</size>", "<size=9>iAS</size>")
            'mReportPrintStatus = False
            Exit Sub
        End If

        'Rs_Report.Sort = "DepartmentName"
        'If IsNull(Rs_Report!Process) And IsNull(Rs_Report!NRTCProc) Then
        '   MsgBox "Records are not Processed", vbInformation + vbExclamation, "TimeWatch  Information"
        '   mReportPrintStatus = False
        '   Exit Sub
        'ElseIf InStr("YX", Rs_Report("Process")) > 0 Or InStr("Y", Rs_Report("nRTCProc")) > 0 Then
        mFileNumber = FreeFile()
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".SRP"
        Dim objWriter As New System.IO.StreamWriter(mstrFile_Name, True)

        objWriter.WriteLine(Space(2) & Space(20) & CommonReport.g_CompanyNames)
        objWriter.WriteLine(Space(2) + "")
        objWriter.WriteLine(Space(2) + "Page No." & mintPageNo)
        objWriter.WriteLine(Space(2) + "                    Run Date & Time : " & Common.runningDateTime) 'Now.ToString("dd/MM/yyyy HH:mm"))
        objWriter.WriteLine(Space(2) + "         Over Time Summary for " & ForDate) 'DateEdit1.DateTime.ToString("dd/MM/yyyy"))
        objWriter.WriteLine(Space(2) + "-------------------------------------------------------------------------")
        objWriter.WriteLine(Space(2) + "Srl  Department                     Ot         Overtime")
        objWriter.WriteLine(Space(2) + "No.                                            Amount")
        objWriter.WriteLine(Space(2) + "-------------------------------------------------------------------------")
        mintLine = 9
        'With Rs_Report
        '    Do While Not .EOF
        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            strDepartmentName = Rs_Report.Tables(0).Rows(i).Item("DepartmentName").ToString.Trim
            dblOtDuration = 0
            dblOtAmount = 0
            strOtDuration10 = 0
            strOtAmount11 = 0
            Do While strDepartmentName = Rs_Report.Tables(0).Rows(i).Item("DepartmentName").ToString.Trim
                If (Rs_Report.Tables(0).Rows(i).Item("OtDuration").ToString.Trim <> "") Or (Rs_Report.Tables(0).Rows(i).Item("OtDuration").ToString.Trim <> "0") Then dblOtDuration = CDbl(Rs_Report.Tables(0).Rows(i).Item("OtDuration").ToString.Trim) + dblOtDuration
                If (Rs_Report.Tables(0).Rows(i).Item("OTAmount").ToString.Trim <> "") Or (Rs_Report.Tables(0).Rows(i).Item("OTAmount").ToString.Trim <> "0") Then dblOtAmount = CDbl(Rs_Report.Tables(0).Rows(i).Item("OTAmount").ToString.Trim) + dblOtAmount
                '.MoveNext()
                i = i + 1
                If i >= Rs_Report.Tables(0).Rows.Count - 1 Then
                    i = i - 1
                    Exit Do
                End If
            Loop
            strOtDuration10 = ""
            strOtAmount11 = ""

            If (dblOtDuration <> 0) Then If dblOtDuration <> 0 Then strOtDuration10 = Space(7 - 5) & (Math.Truncate((dblOtDuration / 60)).ToString("00") & ":" & (dblOtDuration Mod 60).ToString("00"))
            If (dblOtAmount <> 0) Then If dblOtAmount <> 0 Then strOtAmount11 = Space(7 - 5) & Trim(Length7(CStr(dblOtAmount)))
            strDepartmentName45 = strDepartmentName
            objWriter.WriteLine(Space(2) & msrl.ToString.PadRight(4) & " " & strDepartmentName45.PadRight(25) & Space(1) & strOtDuration10 & Space(1) & strOtAmount11)
            msrl = msrl + 1
            mintLine = mintLine + 1
            mblnCheckReport = True
            If mintLine >= g_LinesPerPage Then   ' And Not .EOF
                mintPageNo = mintPageNo + 1
                objWriter.WriteLine(Space(2) & vbFormFeed)
                objWriter.WriteLine(Space(2) + Space(40) & CommonReport.g_CompanyNames)
                objWriter.WriteLine(Space(2) + "")
                objWriter.WriteLine(Space(2) + "Page No." & mintPageNo)
                objWriter.WriteLine(Space(2) + "                                  Run Date & Time : " & Now.ToString("dd/MM/yyyy HH:mm"))
                objWriter.WriteLine(Space(2) + "                 Over Time Summary for " & DateEdit1.DateTime.ToString("dd/MM/yyyy"))
                objWriter.WriteLine(Space(2) + "--------------------------------------------------------------------")
                objWriter.WriteLine(Space(2) + "Department                                    Ot         Overtime")
                objWriter.WriteLine(Space(2) + "                                                         Amount")
                objWriter.WriteLine(Space(2) + "--------------------------------------------------------------------")
                mintLine = 9
            End If
        Next
        objWriter.Close()
        If isPdf = True Then
            'pdf start 
            Common.pdfreport(mstrFile_Name, 11, ulf)
            'pdf end 
        Else
            Try : Process.Start(mstrFile_Name) : Catch ex As Exception : Process.Start("notepad.exe", mstrFile_Name) : End Try
        End If
    End Sub
    Sub Daily_ShiftChangeStatement(ByVal strsortorder As String)
        'On Error GoTo ErrorGen
        Dim strDeptDivCode As String
        Dim strCurrentDeptDivCode As String
        Dim intFile As Integer
        Dim strsql As String
        Dim strDepartmentCode As String
        Dim intPrintLineCounter As Integer
        Dim blnDeptAvailable As Boolean, mFirst As Boolean
        Dim msrl As Integer
        msrl = 1

        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If

        mblnCheckReport = False
        mFirst = True
        mintPageNo = 1
        mintLine = 1

        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet

        'If Left(strsortorder, 1) = "D" Or Left(strsortorder, 1) = "C" Then blnDeptAvailable = True
        If strsortorder = "" Then
            'If g_Report_view Then
            If Common.servername = "Access" Then
                'strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tbltimeregister.Shift,tbltimeregister.ShiftAttended,tbltimeregister.ShiftStartTime,tbltimeregister.In1,tbltimeregister.Out2,tbltimeregister.HoursWorked,tbltimeregister.Status,tbltimeregister.EarlyArrival,tbltimeregister.LateArrival,tbltimeregister.In1Mannual,tbltimeregister.In2Mannual,tbltimeregister.Out1Mannual,tbltimeregister.Out2Mannual,tblCalander.Process,tblCalander.NRTCProc" & _
                ' " from tblCatagory,tblDivision,  tbltimeregister,tblEmployee,tblCalander,tblCompany,tblDepartment" & _
                ' " Where tblCatagory.Cat = tblEmployee.Cat And tblDivision.DivisionCode = tblEmployee.DivisionCode And tbltimeregister.PayCode = tblEmployee.PayCode And tbltimeregister.Shift <> tbltimeregister.ShiftAttended And FORMAT(tbltimeregister.DateOffice,'MMM DD YYYY') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & " And FORMAT(tblCalander.mDate,'MMM DD YYYY') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
                ' " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
                ' " And (tblemployee.LeavingDate >=#" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "# or tblemployee.LeavingDate is null)" & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause)


                strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tbltimeregister.Shift,tbltimeregister.ShiftAttended,tbltimeregister.ShiftStartTime,tbltimeregister.In1,tbltimeregister.Out2,tbltimeregister.HoursWorked,tbltimeregister.Status,tbltimeregister.EarlyArrival,tbltimeregister.LateArrival,tbltimeregister.In1Mannual,tbltimeregister.In2Mannual,tbltimeregister.Out1Mannual,tbltimeregister.Out2Mannual,tblCalander.Process,tblCalander.NRTCProc" & _
                 " from tblCatagory,tblDivision,  tbltimeregister,tblEmployee,tblCalander,tblCompany,tblDepartment" & _
                 " Where tblCatagory.Cat = tblEmployee.Cat And tbltimeregister.PayCode = tblEmployee.PayCode And tbltimeregister.Shift <> tbltimeregister.ShiftAttended And FORMAT(tbltimeregister.DateOffice,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & " And FORMAT(tblCalander.mDate,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
                 " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
                 " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & "order by tblEmployee.PresentCARDNO, TblTimeRegister.DateOffice"
                adapA = New OleDbDataAdapter(strsql, Common.con1)
                adapA.Fill(Rs_Report)
            Else
                strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tbltimeregister.Shift,tbltimeregister.ShiftAttended,tbltimeregister.ShiftStartTime,tbltimeregister.In1,tbltimeregister.Out2,tbltimeregister.HoursWorked,tbltimeregister.Status,tbltimeregister.EarlyArrival,tbltimeregister.LateArrival,tbltimeregister.In1Mannual,tbltimeregister.In2Mannual,tbltimeregister.Out1Mannual,tbltimeregister.Out2Mannual,tblCalander.Process,tblCalander.NRTCProc" & _
                 " from tblCatagory,tblDivision,  tbltimeregister,tblEmployee,tblCalander,tblCompany,tblDepartment" & _
                 " Where tblCatagory.Cat = tblEmployee.Cat And tbltimeregister.PayCode = tblEmployee.PayCode And tbltimeregister.Shift <> tbltimeregister.ShiftAttended And tbltimeregister.DateOffice = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & " And tblCalander.mDate = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
                 " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
                 " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & "order by tblEmployee.PresentCARDNO, TblTimeRegister.DateOffice"
                adap = New SqlDataAdapter(strsql, Common.con)
                adap.Fill(Rs_Report)
            End If
            'Else
            '    strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregisterD.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tbltimeregisterD.Shift,tbltimeregisterD.ShiftAttended,tbltimeregisterD.ShiftStartTime,tbltimeregisterD.In1,tbltimeregisterD.Out2,tbltimeregisterD.HoursWorked,tbltimeregisterD.Status,tbltimeregisterD.EarlyArrival,tbltimeregisterD.LateArrival,tbltimeregisterD.In1Mannual,tbltimeregisterD.In2Mannual,tbltimeregisterD.Out1Mannual,tbltimeregisterD.Out2Mannual,tblCalander.Process,tblCalander.NRTCProc" & _
            '         " from tblCatagory,tblDivision,  tbltimeregisterD,tblEmployee,tblCalander,tblCompany,tblDepartment" & _
            '         " Where tblCatagory.Cat = tblEmployee.Cat And tblDivision.DivisionCode = tblEmployee.DivisionCode And tbltimeregisterD.PayCode = tblEmployee.PayCode And tbltimeregisterD.Shift <> tbltimeregisterD.ShiftAttended And tbltimeregisterD.DateOffice = '" & Format(frmDailyAttReport.txtFromDate.Value, "MMM dd yyyy") & "'" & " And tblCalander.mDate = '" & Format(frmDailyAttReport.txtFromDate.Value, "MMM dd yyyy") & "'" & _
            '         " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
            '         " And (tblemployee.LeavingDate>='" & Format(frmDailyAttReport.txtFromDate, "MMM dd yyyy") & "' or tblemployee.LeavingDate is null)" & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause)
            'End If
            'Rs_Report = Cn.Execute(strsql)
            If Rs_Report.Tables(0).Rows.Count < 1 Then
                'G_Report_Error = False
                'Screen.MousePointer = vbDefault
                XtraMessageBox.Show(ulf, "<size=10>There is no Auto Shift Change for any Employee on this Date.</size>", "<size=9>iAS</size>")
                'mReportPrintStatus = False
                Exit Sub
            End If
            'Rs_Report.Sort = "PayCode"
        Else
            'Rs_Report.Sort = strsortorder
        End If

        'If IsNull(Rs_Report!Process) And IsNull(Rs_Report!NRTCProc) Then
        '   MsgBox "Records are not Processed", vbInformation + vbExclamation, "TimeWatch  Information"
        '   mReportPrintStatus = False
        '   Exit Sub
        'ElseIf InStr("YX", Rs_Report("Process")) > 0 Or InStr("Y", Rs_Report("nRTCProc")) > 0 Then
        mFileNumber = FreeFile()
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".SRP"
        Dim objWriter As New System.IO.StreamWriter(mstrFile_Name, True)
        'With Rs_Report
        'Do While Not .EOF
        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            If mFirst Or mintLine > g_LinesPerPage Or (strsortorder.Trim = "Department" And strDeptDivCode <> Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim And g_SkipAfterDept) Then
                'If mFirst Or mintLine > g_LinesPerPage Or (Left(strsortorder, 10) = "Department" And strDeptDivCode <> !DepartmentCode And g_SkipAfterDept) Then
1:              If Not mFirst Then
                    objWriter.WriteLine(Space(2) & vbFormFeed)
                Else
                    mFirst = False
                End If
                objWriter.WriteLine(Space(2) & Space(40) & CommonReport.g_CompanyNames)
                objWriter.WriteLine(Space(2) & "")
                objWriter.WriteLine(Space(2) + "Page No." & mintPageNo)
                objWriter.WriteLine(Space(2) + "                                                                             Run Date & Time : " & Common.runningDateTime) 'Now.ToString("dd/MM/yyyy HH:mm"))
                objWriter.WriteLine(Space(2) + "                                    Shift change statement for " & ForDate) 'DateEdit1.DateTime.ToString("dd/MM/yyyy"))
                objWriter.WriteLine(Space(2) + "--------------------------------------------------------------------------------------------------------------------------")
                objWriter.WriteLine(Space(2) + "Srl  EmpCode      Card         Employee Name             Actual Shift Start In    Out   Hours  Status Early   Shift Manual")
                objWriter.WriteLine(Space(2) + "No.               No.                                    Shift                          Worked        Arrival Late")
                objWriter.WriteLine(Space(2) + "---------------------------------------------------------------------------------------------------------------------------")
                mintLine = 9
                mintPageNo = mintPageNo + 1
            End If

            'If blnDeptAvailable And Not .EOF Then
            '    If Left(strsortorder, 10) = "Department" And strDeptDivCode <> !DepartmentCode Then
            '        If mintLine + 3 > g_LinesPerPage Then GoTo 1
            '    Print #intFile, Space(2) + ""
            '        strDeptDivCode = !DepartmentCode
            '    Print #intFile, Space(2) + "** Department Code & Name : " & !DepartmentCode & "  " & !DepartmentName
            '    Print #intFile, Space(2) + ""
            '        mintLine = mintLine + 3
            '    ElseIf Left(strsortorder, 8) = "Division" And strDeptDivCode <> !DivisionCode Then
            '        If mintLine + 3 > g_LinesPerPage Then GoTo 1
            '    Print #intFile, Space(2) + ""
            '        strDeptDivCode = !DivisionCode
            '    Print #intFile, Space(2) + "** Section Code & Name : " & !DivisionCode & "  " & !DivisionName
            '    Print #intFile, Space(2) + ""
            '        mintLine = mintLine + 3
            '    ElseIf Left(strsortorder, 3) = "Cat" And strDeptDivCode <> !Cat Then
            '        If mintLine + 3 > g_LinesPerPage Then GoTo 1
            '    Print #intFile, Space(2) + ""
            '        strDeptDivCode = !Cat
            '    Print #intFile, Space(2) + "** Category Code & Name : " & !Cat & "  " & !CatagoryName
            '    Print #intFile, Space(2) + ""
            '        mintLine = mintLine + 3
            '    End If
            'End If
            objWriter.WriteLine("")
            objWriter.Write(Space(2) & msrl.ToString.PadRight(4) & " " & Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim.PadRight(12) & " " & Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim.PadRight(12) & " " & Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim.PadRight(25) & " " & Rs_Report.Tables(0).Rows(i).Item("Shift").ToString.Trim & Space(4) & Rs_Report.Tables(0).Rows(i).Item("ShiftAttended").ToString.Trim & Space(3))
            objWriter.Write(IIf(Rs_Report.Tables(0).Rows(i).Item("shiftStartTime").ToString.Trim = "" Or Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("shiftStartTime").ToString.Trim).ToString("HH:mm") = "00:00", Space(5), Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("shiftStartTime").ToString.Trim).ToString("HH:mm") & Space(1)))
            objWriter.Write(IIf(Rs_Report.Tables(0).Rows(i).Item("In1").ToString.Trim = "" Or Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("In1").ToString.Trim).ToString("HH:mm") = "00:00", Space(5), Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("In1").ToString.Trim).ToString("HH:mm") & Space(1)))
            'objWriter.Write(IIf(Rs_Report.Tables(0).Rows(i).Item("out2").ToString.Trim = "" Or Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("out2").ToString.Trim).ToString("HH:mm") = "00:00", Space(5), Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("out2").ToString.Trim).ToString("HH:mm") & Space(1)))

            If Rs_Report.Tables(0).Rows(i).Item("out2").ToString.Trim <> "" Then
                If Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("out2").ToString.Trim).ToString("HH:mm") = "00:00" Then
                    objWriter.Write(Space(5))
                End If
                objWriter.Write(Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("out2").ToString.Trim).ToString("HH:mm") & Space(1))
            Else
                objWriter.Write(Space(5))
            End If

            objWriter.Write(IIf(Rs_Report.Tables(0).Rows(i).Item("HOURSWORKED").ToString.Trim = "" Or Rs_Report.Tables(0).Rows(i).Item("HOURSWORKED").ToString.Trim = "0", Space(8), Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("HOURSWORKED").ToString.Trim / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("HOURSWORKED").ToString.Trim Mod 60).ToString("00") & Space(2)))
            objWriter.Write(IIf(Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim = "", Space(5), Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim & Space(2)))
            objWriter.Write(IIf(Rs_Report.Tables(0).Rows(i).Item("EARLYARRIVAL").ToString.Trim = "" Or Rs_Report.Tables(0).Rows(i).Item("EARLYARRIVAL").ToString.Trim = "0", Space(8), Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("EARLYARRIVAL").ToString.Trim / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("EARLYARRIVAL").ToString.Trim Mod 60).ToString("00") & Space(3)))
            objWriter.Write(IIf(Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim = "" Or Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim = "0", Space(8), Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim Mod 60).ToString("00") & Space(1)))
            'objWriter.Write(IIf(UCase(Rs_Report.Tables(0).Rows(i).Item("IN1MANNUAL").ToString.Trim) = "Y", "M", IIf(UCase(Rs_Report.Tables(0).Rows(i).Item("Out1Mannual").ToString.Trim) = "Y", "M", IIf(UCase(Rs_Report.Tables(0).Rows(i).Item("IN2MANNUAL").ToString.Trim) = "Y", "M", IIf(UCase(Rs_Report.Tables(0).Rows(i).Item("OUT2MANNUAL").ToString.Trim) = "Y", "M", "")))))
            If UCase(Rs_Report.Tables(0).Rows(i).Item("IN1MANNUAL").ToString.Trim) = "Y" Or UCase(Rs_Report.Tables(0).Rows(i).Item("Out1Mannual").ToString.Trim) = "Y" Or UCase(Rs_Report.Tables(0).Rows(i).Item("IN2MANNUAL").ToString.Trim) = "Y" Or UCase(Rs_Report.Tables(0).Rows(i).Item("OUT2MANNUAL").ToString.Trim) = "Y" Then
                objWriter.Write("M")
            Else
                objWriter.Write("")
            End If
            msrl = msrl + 1
            mintLine = mintLine + 1
            mblnCheckReport = True
            '.MoveNext()
            'Loop
        Next
        objWriter.Close()
        If isPdf = True Then
            'pdf start 
            Common.pdfreport(mstrFile_Name, 6, ulf)
            'pdf end 
        Else
            Try : Process.Start(mstrFile_Name) : Catch ex As Exception : Process.Start("notepad.exe", mstrFile_Name) : End Try
        End If
    End Sub
    Sub Daily_OutWorkReport(ByVal strsortorder As String)
        'On Local Error Resume Next
        Dim intFile As Integer
        Dim strsql As String
        Dim strIn As String
        Dim strOut As String
        Dim iOutWorkCtr As Integer

        Dim strDeptDivCode As String
        Dim strCurrentDeptDivCode As String

        Dim intPrintLineCounter As Integer
        Dim blnDeptAvailable As Boolean, mFirst As Boolean
        Dim mField As Object
        Dim msrl As Integer
        msrl = 1

        mblnCheckReport = False
        mFirst = True
        mintPageNo = 1
        mintLine = 1

        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If
        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet

        'If Left(strsortorder, 1) = "D" Or Left(strsortorder, 1) = "C" Then blnDeptAvailable = True
        If strsortorder = "" Then
            If Common.servername = "Access" Then
                '    strsql = "select tblDivision.DivisionName,tblCatagory.CatagoryName,tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tblOutWorkRegister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tblOutWorkRegister.OutWorkDuration,tblOutWorkRegister.in1,tblOutWorkRegister.in2,tblOutWorkRegister.in3,tblOutWorkRegister.in4,tblOutWorkRegister.in5,tblOutWorkRegister.in6,tblOutWorkRegister.in7,tblOutWorkRegister.in8,tblOutWorkRegister.in9,tblOutWorkRegister.in10,tblOutWorkRegister.out1,tblOutWorkRegister.out2,tblOutWorkRegister.out3,tblOutWorkRegister.out4,tblOutWorkRegister.out5,tblOutWorkRegister.out6,tblOutWorkRegister.out7,tblOutWorkRegister.out8,tblOutWorkRegister.out9,tblOutWorkRegister.out10,tblCalander.Process,tblCalander.NRTCProc, " & _
                '         " tblOutWorkRegister.Rin1,tblOutWorkRegister.Rin2,tblOutWorkRegister.Rin3,tblOutWorkRegister.Rin4,tblOutWorkRegister.Rin5,tblOutWorkRegister.Rin6,tblOutWorkRegister.Rin7,tblOutWorkRegister.Rin8,tblOutWorkRegister.Rin9,tblOutWorkRegister.Rin10,tblOutWorkRegister.Rout1,tblOutWorkRegister.Rout2,tblOutWorkRegister.Rout3,tblOutWorkRegister.Rout4,tblOutWorkRegister.Rout5,tblOutWorkRegister.Rout6,tblOutWorkRegister.Rout7,tblOutWorkRegister.Rout8,tblOutWorkRegister.Rout9,tblOutWorkRegister.Rout10 " & _
                '         " from tblDivision,tblCatagory,tblOutWorkRegister ,tblEmployee,tblCalander,tblDepartment,tblCompany " & _
                '         " Where tblDivision.DivisionCode = tblEmployee.DivisionCode And tblCatagory.Cat = tblEmployee.Cat And tblOutWorkRegister.Paycode = tblEmployee.Paycode And tblOutWorkRegister.In1 Is Not Null And FORMAT(tblOutWorkRegister.DateOffice,'MMM DD YYYY') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' And FORMAT(tblCalander.mDate,'MMM DD YYYY') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
                '         " And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
                '         " And (tblemployee.LeavingDate >=#" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "# or tblemployee.LeavingDate is null)" & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & _
                '         " And tblOutworkRegister.reason_OutWork = 'N' And tblCompany.CompanyCode = tblEmployee.CompanyCode "

                strsql = "select tblCatagory.CatagoryName, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tblOutWorkRegister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tblOutWorkRegister.OutWorkDuration,tblOutWorkRegister.in1,tblOutWorkRegister.in2,tblOutWorkRegister.in3,tblOutWorkRegister.in4,tblOutWorkRegister.in5,tblOutWorkRegister.in6,tblOutWorkRegister.in7,tblOutWorkRegister.in8,tblOutWorkRegister.in9,tblOutWorkRegister.in10,tblOutWorkRegister.out1,tblOutWorkRegister.out2,tblOutWorkRegister.out3,tblOutWorkRegister.out4,tblOutWorkRegister.out5,tblOutWorkRegister.out6,tblOutWorkRegister.out7,tblOutWorkRegister.out8,tblOutWorkRegister.out9,tblOutWorkRegister.out10,tblCalander.Process,tblCalander.NRTCProc, " & _
                     " tblOutWorkRegister.Rin1,tblOutWorkRegister.Rin2,tblOutWorkRegister.Rin3,tblOutWorkRegister.Rin4,tblOutWorkRegister.Rin5,tblOutWorkRegister.Rin6,tblOutWorkRegister.Rin7,tblOutWorkRegister.Rin8,tblOutWorkRegister.Rin9,tblOutWorkRegister.Rin10,tblOutWorkRegister.Rout1,tblOutWorkRegister.Rout2,tblOutWorkRegister.Rout3,tblOutWorkRegister.Rout4,tblOutWorkRegister.Rout5,tblOutWorkRegister.Rout6,tblOutWorkRegister.Rout7,tblOutWorkRegister.Rout8,tblOutWorkRegister.Rout9,tblOutWorkRegister.Rout10 " & _
                     " from tblCatagory,tblOutWorkRegister ,tblEmployee,tblCalander,tblDepartment,tblCompany, tblTimeRegister " & _
                     " Where tblTimeRegister.paycode=tblEmployee.paycode and tblCatagory.Cat = tblEmployee.Cat And tblOutWorkRegister.Paycode = tblTimeRegister.Paycode And tblOutWorkRegister.Paycode = tblEmployee.Paycode And tblOutWorkRegister.In1 Is Not Null And FORMAT(tblOutWorkRegister.DateOffice,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' And FORMAT(tblCalander.mDate,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
                     " and tblCompany.COMPANYCODE	=TblEmployee.COMPANYCODE and tblTimeRegister.DateOFFICE = tblOutWorkRegister.DateOFFICE " & _
                      " And  tblOutWorkRegister.DateOffice =tblTimeRegister.DateOFFICE" & _
                     " And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
                     " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & _
                     " And tblOutworkRegister.reason_OutWork = 'N' And tblCompany.CompanyCode = tblEmployee.CompanyCode " & "order by tblEmployee.PresentCARDNO"
                If Common.IsCompliance Then
                    strsql = Microsoft.VisualBasic.Strings.Replace(strsql, "tbltimeregister", "tblTimeRegisterD", 1, -1, Microsoft.VisualBasic.Constants.vbTextCompare)
                End If
                adapA = New OleDbDataAdapter(strsql, Common.con1)
                adapA.Fill(Rs_Report)
            Else
                strsql = "select tblCatagory.CatagoryName,tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tblOutWorkRegister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tblOutWorkRegister.OutWorkDuration,tblOutWorkRegister.in1,tblOutWorkRegister.in2,tblOutWorkRegister.in3,tblOutWorkRegister.in4,tblOutWorkRegister.in5,tblOutWorkRegister.in6,tblOutWorkRegister.in7,tblOutWorkRegister.in8,tblOutWorkRegister.in9,tblOutWorkRegister.in10,tblOutWorkRegister.out1,tblOutWorkRegister.out2,tblOutWorkRegister.out3,tblOutWorkRegister.out4,tblOutWorkRegister.out5,tblOutWorkRegister.out6,tblOutWorkRegister.out7,tblOutWorkRegister.out8,tblOutWorkRegister.out9,tblOutWorkRegister.out10,tblCalander.Process,tblCalander.NRTCProc, " & _
                         " tblOutWorkRegister.Rin1,tblOutWorkRegister.Rin2,tblOutWorkRegister.Rin3,tblOutWorkRegister.Rin4,tblOutWorkRegister.Rin5,tblOutWorkRegister.Rin6,tblOutWorkRegister.Rin7,tblOutWorkRegister.Rin8,tblOutWorkRegister.Rin9,tblOutWorkRegister.Rin10,tblOutWorkRegister.Rout1,tblOutWorkRegister.Rout2,tblOutWorkRegister.Rout3,tblOutWorkRegister.Rout4,tblOutWorkRegister.Rout5,tblOutWorkRegister.Rout6,tblOutWorkRegister.Rout7,tblOutWorkRegister.Rout8,tblOutWorkRegister.Rout9,tblOutWorkRegister.Rout10 " & _
                         " from tblCatagory,tblOutWorkRegister ,tblEmployee,tblCalander,tblDepartment,tblCompany, tblTimeRegister " & _
                         " Where  tblTimeRegister.paycode=tblEmployee.paycode  and tblCatagory.Cat = tblEmployee.Cat And tblOutWorkRegister.Paycode = tblTimeRegister.Paycode And tblOutWorkRegister.Paycode = tblEmployee.Paycode And tblOutWorkRegister.In1 Is Not Null And tblOutWorkRegister.DateOffice = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' And tblCalander.mDate = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
                        " And  tblOutWorkRegister.DateOffice =tblTimeRegister.DateOFFICE" & _
                        " And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
                         " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & _
                         " And tblOutworkRegister.reason_OutWork = 'N' And tblCompany.CompanyCode = tblEmployee.CompanyCode " & "order by tblEmployee.PresentCARDNO"
                If Common.IsCompliance Then
                    strsql = Microsoft.VisualBasic.Strings.Replace(strsql, "tbltimeregister", "tblTimeRegisterD", 1, -1, Microsoft.VisualBasic.Constants.vbTextCompare)
                End If
                adap = New SqlDataAdapter(strsql, Common.con)
                adap.Fill(Rs_Report)
            End If
            'Rs_Report = Cn.Execute(strsql)
            If Rs_Report.Tables(0).Rows.Count < 1 Then
                XtraMessageBox.Show(ulf, "<size=10>There is no Out Work for any Employee on this Date.</size>", "<size=9>iAS</size>")
                Exit Sub
            End If
            'Rs_Report.Sort = "PayCode"
        Else
            'Rs_Report.Sort = strsortorder
        End If

        'If IsNull(Rs_Report!Process) And IsNull(Rs_Report!NRTCProc) Then
        '    MsgBox "Records are not Processed", vbInformation + vbExclamation, "TimeWatch  Information"
        '   Exit Sub
        'ElseIf InStr("YX", Rs_Report("Process")) > 0 Or InStr("Y", Rs_Report("nRTCProc")) > 0 Then
        mFileNumber = FreeFile()
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".SRP"
        Dim objWriter As New System.IO.StreamWriter(mstrFile_Name, True)
        'With Rs_Report
        '    Do While Not .EOF
        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            If mFirst Or mintLine > g_LinesPerPage Or (strsortorder.Trim = "Department" And strDeptDivCode <> Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim And g_SkipAfterDept) Then
                'If mFirst Or mintLine > g_LinesPerPage Or (Left(strsortorder, 10) = "Department" And strDeptDivCode <> !DepartmentCode And g_SkipAfterDept) Then
1:              If Not mFirst Then
                    objWriter.WriteLine(Space(2) & vbFormFeed)
                Else
                    mFirst = False
                End If
                objWriter.WriteLine(Space(2) + Space(40) & CommonReport.g_CompanyNames)
                objWriter.WriteLine(Space(2) & "")
                objWriter.WriteLine(Space(2) + "Page No." & mintPageNo)
                objWriter.WriteLine(Space(2) + "                                                                           Run Date & Time : " & Common.runningDateTime) 'Now.ToString("dd/MM/yyyy HH:mm"))
                objWriter.WriteLine(Space(2) + Space(40) & "Out Work Report for  " & ForDate) 'DateEdit1.DateTime.ToString("dd/MM/yyyy"))
                objWriter.WriteLine(Space(2) + "----------------------------------------------------------------------------------------------------------------------------------")
                objWriter.WriteLine(Space(2) + "Srl  PayRoll      Card         Employee Name             Total ")
                objWriter.WriteLine(Space(2) + "No.   No.         No.                                     OW     I      II     III    IV     V      VI     VII    VIII   IX     X")
                objWriter.WriteLine(Space(2) + "----------------------------------------------------------------------------------------------------------------------------------")
                mintLine = 9
                mintPageNo = mintPageNo + 1
            End If
            'If blnDeptAvailable And Not .EOF Then
            '    If Left(strsortorder, 10) = "Department" And strDeptDivCode <> !DepartmentCode Then
            '        If mintLine + 3 > g_LinesPerPage Then GoTo 1
            '            Print #intFile, Space(2) + ""
            '        strDeptDivCode = !DepartmentCode
            '            Print #intFile, Space(2) + "** Department Code & Name : " & !DepartmentCode & "  " & !DepartmentName
            '            Print #intFile, Space(2) + ""
            '        mintLine = mintLine + 3
            '    ElseIf Left(strsortorder, 8) = "Division" And strDeptDivCode <> !DivisionCode Then
            '        If mintLine + 3 > g_LinesPerPage Then GoTo 1
            '            Print #intFile, Space(2) + ""
            '        strDeptDivCode = !DivisionCode
            '            Print #intFile, Space(2) + "** Section Code & Name : " & !DivisionCode & "  " & !DivisionName
            '            Print #intFile, Space(2) + ""
            '        mintLine = mintLine + 3
            '    ElseIf Left(strsortorder, 3) = "Cat" And strDeptDivCode <> !Cat Then
            '        If mintLine + 3 > g_LinesPerPage Then GoTo 1
            '            Print #intFile, Space(2) + ""
            '        strDeptDivCode = !Cat
            '            Print #intFile, Space(2) + "** Category Code & Name : " & !Cat & "  " & !CatagoryName
            '            Print #intFile, Space(2) + ""
            '        mintLine = mintLine + 3
            '    End If
            'End If

            strIn = ""
            strOut = ""
            For iOutWorkCtr = 1 To 10
                If (Rs_Report.Tables(0).Rows(i).Item("In" & Trim(Str(iOutWorkCtr)))).ToString.Trim <> "" Then
                    strIn = strIn & Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("In" & Trim(Str(iOutWorkCtr))).ToString.Trim).ToString("HH:mm") & " "
                Else
                    strIn = strIn & Space(8)
                End If
                If (Rs_Report.Tables(0).Rows(i).Item("OUT" & Trim(Str(iOutWorkCtr)))).ToString.Trim <> "" Then
                    strOut = strOut & Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("OUT" & Trim(Str(iOutWorkCtr))).ToString.Trim).ToString("HH:mm") & " "
                Else
                    strOut = strOut & Space(8)
                End If
            Next iOutWorkCtr

            objWriter.WriteLine(Space(2) & msrl.ToString.PadRight(4) & " " & Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim.PadRight(12) & " " & Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim.PadRight(12) & " " & Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim.PadRight(25) & " " & Space(5 - 4) & Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("OUTWORKDURATION").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("OUTWORKDURATION").ToString.Trim Mod 60).ToString("00") & " " & strIn)
            objWriter.WriteLine(Space(2) & "                                                                " & strOut)
            msrl = msrl + 1
            mintLine = mintLine + 2
            mblnCheckReport = True
            '                .MoveNext()
            '            Loop
        Next
        objWriter.Close()
        If isPdf = True Then
            'pdf start 
            Common.pdfreport(mstrFile_Name, 6, ulf)
            'pdf end 
        Else
            Try : Process.Start(mstrFile_Name) : Catch ex As Exception : Process.Start("notepad.exe", mstrFile_Name) : End Try
        End If
    End Sub
    Sub Daily_MissingPunchOrIncorrectShiftOrReverification(ByVal strsortorder As String)
        Dim adapS As SqlDataAdapter
        Dim adapAc As OleDbDataAdapter
        Dim Rs As DataSet = New DataSet
        Dim sSql As String = " Select * from tblSetUp where setupid =(Select Convert(varchar(10),Max(Convert(int,Setupid))) from tb" & _
        "lsetup )"
        If Common.servername = "Access" Then
            sSql = " Select * from tblSetUp where setupid =(Select CVar(Max(CInt(Setupid))) from tblsetup )"
            adapAc = New OleDbDataAdapter(sSql, Common.con1)
            adapAc.Fill(Rs)
        Else
            adapS = New SqlDataAdapter(sSql, Common.con)
            adapS.Fill(Rs)
        End If
        Dim g_TIME1 As String = Rs.Tables(0).Rows(0).Item("Time1").ToString.Trim
        Dim G_CHECKLATE As String = Rs.Tables(0).Rows(0).Item("CHECKLATE").ToString.Trim
        Dim g_CHECKEARLY As String = Rs.Tables(0).Rows(0).Item("CHECKEARLY").ToString.Trim
        'On Error GoTo ErrorGen
        Dim strDeptDivCode As String
        Dim strCurrentDeptDivCode As String
        Dim intFile As Integer
        Dim strsql As String
        Dim strStatus As String
        Dim lngHoursWorked As Long
        Dim lngLateArrival As Long
        Dim lngEarlyDeparture As Long
        Dim g_DummyHoursWorked As Long
        Dim g_DummyLateArrival As Long
        Dim g_DummyEarlyDeparture As Long
        'g_DummyHoursWorked = g_TIME1
        'g_DummyLateArrival = G_CHECKLATE
        'g_DummyEarlyDeparture = g_CHECKEARLY
        If g_TIME1 = "" Then : g_DummyHoursWorked = 0 : Else : g_DummyHoursWorked = g_TIME1 : End If
        If G_CHECKLATE = "" Then : g_DummyLateArrival = 0 : Else : g_DummyLateArrival = G_CHECKLATE : End If
        If g_CHECKEARLY = "" Then : g_DummyEarlyDeparture = 0 : Else : g_DummyEarlyDeparture = g_CHECKEARLY : End If
        Dim strDepartmentCode As String
        Dim intPrintLineCounter As Integer
        Dim blnDeptAvailable As Boolean, mFirst As Boolean
        Dim msrl As Integer
        msrl = 1

        mblnCheckReport = False
        mFirst = True
        mintPageNo = 1
        mintLine = 1

        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If
        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet

        'If Left(strsortorder, 1) = "D" Or Left(strsortorder, 1) = "C" Then blnDeptAvailable = True
        If strsortorder = "" Then
            'If g_Report_view Then
            If Common.servername = "Access" Then
                'strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tblTimeRegister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tbltimeregister.ShiftAttended,tbltimeregister.In1,tbltimeregister.Out1,tbltimeregister.In2,tbltimeregister.Out2,tbltimeregister.HoursWorked,tbltimeregister.LateArrival,tbltimeregister.EarlyDeparture,tbltimeregister.Status,tbltimeregister.OtDuration,tblCalander.mDate,tblCalander.Process,tblCalander.NRTCProc" & _
                ' " from tblCatagory,tblDivision,  tbltimeregister,tblEmployee,tblCalander,tblCompany,tblDepartment" & _
                ' " Where tblCatagory.Cat = tblEmployee.Cat And tblDivision.DivisionCode = tblEmployee.DivisionCode And tbltimeregister.PayCode = tblEmployee.PayCode And FORMAT(tbltimeregister.DateOffice,'MMM DD YYYY') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & " And FORMAT(tblCalander.mDate,'MMM DD YYYY') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
                ' " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
                ' " And (tbltimeregister.Status = 'MIS' Or tbltimeregister.HoursWorked >" & g_DummyHoursWorked & " Or tbltimeregister.LateArrival >" & g_DummyLateArrival & " Or tbltimeregister.EarlyDeparture > " & g_DummyEarlyDeparture & ")" & _
                ' " And (tblemployee.LeavingDate >=#" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "# or tblemployee.LeavingDate is null)" & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause)

                strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tblTimeRegister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tbltimeregister.ShiftAttended,tbltimeregister.In1,tbltimeregister.Out1,tbltimeregister.In2,tbltimeregister.Out2,tbltimeregister.HoursWorked,tbltimeregister.LateArrival,tbltimeregister.EarlyDeparture,tbltimeregister.Status,tbltimeregister.OtDuration,tblCalander.mDate,tblCalander.Process,tblCalander.NRTCProc" & _
                 " from tblCatagory,tblDivision,  tbltimeregister,tblEmployee,tblCalander,tblCompany,tblDepartment" & _
                 " Where tblCatagory.Cat = tblEmployee.Cat And tbltimeregister.PayCode = tblEmployee.PayCode And FORMAT(tbltimeregister.DateOffice,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & " And FORMAT(tblCalander.mDate,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
                 " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
                 " And (tbltimeregister.Status = 'MIS' Or tbltimeregister.HoursWorked >" & g_DummyHoursWorked & " Or tbltimeregister.LateArrival >" & g_DummyLateArrival & " Or tbltimeregister.EarlyDeparture > " & g_DummyEarlyDeparture & ")" & _
                 " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & "order by tblEmployee.PresentCARDNO, TblTimeRegister.DateOffice"
                adapA = New OleDbDataAdapter(strsql, Common.con1)
                adapA.Fill(Rs_Report)
            Else
                strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tblTimeRegister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tbltimeregister.ShiftAttended,tbltimeregister.In1,tbltimeregister.Out1,tbltimeregister.In2,tbltimeregister.Out2,tbltimeregister.HoursWorked,tbltimeregister.LateArrival,tbltimeregister.EarlyDeparture,tbltimeregister.Status,tbltimeregister.OtDuration,tblCalander.mDate,tblCalander.Process,tblCalander.NRTCProc" & _
                 " from tblCatagory,tblDivision,  tbltimeregister,tblEmployee,tblCalander,tblCompany,tblDepartment" & _
                 " Where tblCatagory.Cat = tblEmployee.Cat And tbltimeregister.PayCode = tblEmployee.PayCode And tbltimeregister.DateOffice = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & " And tblCalander.mDate = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
                 " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
                 " And (tbltimeregister.Status = 'MIS' Or tbltimeregister.HoursWorked >" & g_DummyHoursWorked & " Or tbltimeregister.LateArrival >" & g_DummyLateArrival & " Or tbltimeregister.EarlyDeparture > " & g_DummyEarlyDeparture & ")" & _
                 " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & "order by tblEmployee.PresentCARDNO, TblTimeRegister.DateOffice"
                adap = New SqlDataAdapter(strsql, Common.con)
                adap.Fill(Rs_Report)
            End If
            'Else
            '    strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tbltimeregisterD.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregisterD.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tbltimeregisterD.ShiftAttended,tbltimeregisterD.In1,tbltimeregisterD.Out1,tbltimeregisterD.In2,tbltimeregisterD.Out2,tbltimeregisterD.HoursWorked,tbltimeregisterD.LateArrival,tbltimeregisterD.EarlyDeparture,tbltimeregisterD.Status,tbltimeregisterD.OtDuration,tblCalander.mDate,tblCalander.Process,tblCalander.NRTCProc" & _
            '         " from tblCatagory,tblDivision,  tbltimeregisterD,tblEmployee,tblCalander,tblCompany,tblDepartment" & _
            '         " Where tblCatagory.Cat = tblEmployee.Cat And tblDivision.DivisionCode = tblEmployee.DivisionCode And tbltimeregisterD.PayCode = tblEmployee.PayCode And tbltimeregisterD.DateOffice = '" & Format(frmDailyAttReport.txtFromDate.Value, "MMM dd yyyy") & "'" & " And tblCalander.mDate = '" & Format(frmDailyAttReport.txtFromDate.Value, "MMM dd yyyy") & "'" & _
            '         " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
            '         " And (tbltimeregisterD.Status = 'MIS' Or tbltimeregisterD.HoursWorked >" & g_DummyHoursWorked & " Or tbltimeregisterD.LateArrival >" & g_DummyLateArrival & " Or tbltimeregisterD.EarlyDeparture > " & g_DummyEarlyDeparture & ")" & _
            '         " And (tblemployee.LeavingDate>='" & Format(frmDailyAttReport.txtFromDate, "MMM dd yyyy") & "' or tblemployee.LeavingDate is null)" & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause)
            'End If
            'Rs_Report = Cn.Execute(strsql)
            If Rs_Report.Tables(0).Rows.Count < 1 Then
                'G_Report_Error = False
                'Screen.MousePointer = vbDefault
                XtraMessageBox.Show(ulf, "<size=10>There is no missing punches or under check parameter punch for any Employee on this Date.</size>", "<size=9>iAS</size>")
                'mReportPrintStatus = False
                Exit Sub
            End If
            'Rs_Report.Sort = "PayCode"
        Else
            'Rs_Report.Sort = strsortorder
        End If


        'If IsNull(Rs_Report!Process) And IsNull(Rs_Report!NRTCProc) Then
        '   MsgBox "Records are not Processed", vbInformation + vbExclamation, "TimeWatch  Information"
        '   mReportPrintStatus = False
        '   Exit Sub
        'ElseIf InStr("YX", Rs_Report("Process")) > 0 Or InStr("Y", Rs_Report("nRTCProc")) > 0 Then

        mFileNumber = FreeFile()
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".SRP"
        Dim objWriter As New System.IO.StreamWriter(mstrFile_Name, True)

        'With Rs_Report
        '    Do While Not .EOF
        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            If mFirst Or mintLine > g_LinesPerPage Or (strsortorder.Trim = "Department" And strDeptDivCode <> Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim And g_SkipAfterDept) Then
                'If mFirst Or mintLine > g_LinesPerPage Or (Left(strsortorder, 10) = "Department" And strDeptDivCode <> !DepartmentCode And g_SkipAfterDept) Then
1:              If Not mFirst Then
                    objWriter.WriteLine(Space(2) & vbFormFeed)
                Else
                    mFirst = False
                End If
                objWriter.WriteLine(Space(2) & Space(40) & CommonReport.g_CompanyNames)
                objWriter.WriteLine(Space(2) & "")
                objWriter.WriteLine(Space(2) & "Page No." & mintPageNo)
                objWriter.WriteLine(Space(2) & "                                                                         Run Date & Time : " & Common.runningDateTime) 'Now.ToString("dd/MM/yyyy HH:mm"))
                objWriter.WriteLine(Space(2) & Space(40) & "Missing punch, incorrect shift or reverification report of " & ForDate) 'DateEdit1.DateTime.ToString("dd/MM/yyyy"))
                objWriter.WriteLine(Space(2) & "--------------------------------------------------------------------------------------------------------------------")
                objWriter.WriteLine(Space(2) & "Srl  EmpCode      Cardno       Employee Name             Shift In    Lunch Lunch Out   Hours  Late  Early Status Ot")
                objWriter.WriteLine(Space(2) & "No.                                                                   Out   In        Worked")
                objWriter.WriteLine(Space(2) & "--------------------------------------------------------------------------------------------------------------------")
                mintLine = 9
                mintPageNo = mintPageNo + 1
            End If
            'If blnDeptAvailable And Not .EOF Then
            '    If Left(strsortorder, 10) = "Department" And strDeptDivCode <> !DepartmentCode Then
            '        If mintLine + 3 > g_LinesPerPage Then GoTo 1
            '        Print #intFile, Space(2) + ""
            '        strDeptDivCode = !DepartmentCode
            '        Print #intFile, Space(2) + "** Department Code & Name : " & !DepartmentCode & "  " & !DepartmentName
            '        Print #intFile, Space(2) + ""
            '        mintLine = mintLine + 3
            '    ElseIf Left(strsortorder, 8) = "Division" And strDeptDivCode <> !DivisionCode Then
            '        If mintLine + 3 > g_LinesPerPage Then GoTo 1
            '        Print #intFile, Space(2) + ""
            '        strDeptDivCode = !DivisionCode
            '        Print #intFile, Space(2) + "** Section Code & Name : " & !DivisionCode & "  " & !DivisionName
            '        Print #intFile, Space(2) + ""
            '        mintLine = mintLine + 3
            '    ElseIf Left(strsortorder, 3) = "Cat" And strDeptDivCode <> !Cat Then
            '        If mintLine + 3 > g_LinesPerPage Then GoTo 1
            '        Print #intFile, Space(2) + ""
            '        strDeptDivCode = !Cat
            '        Print #intFile, Space(2) + "** Category Code & Name : " & !Cat & "  " & !CatagoryName
            '        Print #intFile, Space(2) + ""
            '        mintLine = mintLine + 3
            '    End If
            'End If
            objWriter.WriteLine("")
            strStatus = IIf(Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim = "", Space(5), Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim & Space(2))
            lngHoursWorked = IIf(Rs_Report.Tables(0).Rows(i).Item("HOURSWORKED").ToString.Trim = "", 0, Rs_Report.Tables(0).Rows(i).Item("HOURSWORKED").ToString.Trim)
            lngLateArrival = IIf(Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim = "", 0, Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim)
            lngEarlyDeparture = IIf(Rs_Report.Tables(0).Rows(i).Item("earlydeparture").ToString.Trim = "", 0, Rs_Report.Tables(0).Rows(i).Item("earlydeparture").ToString.Trim)
            objWriter.Write(Space(2) & msrl.ToString.PadRight(4) & " " & Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim.PadRight(12) & " " & Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim.PadRight(12) & " " & Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim.PadRight(25) & " " & Rs_Report.Tables(0).Rows(i).Item("ShiftAttended").ToString.Trim & Space(3))
            'objWriter.Write(IIf(Rs_Report.Tables(0).Rows(i).Item("In1").ToString.Trim = "", Space(5), Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("In1").ToString.Trim).ToString("HH:mm") & Space(1)))
            If Rs_Report.Tables(0).Rows(i).Item("In1").ToString.Trim = "" Then
                objWriter.Write(Space(5))
            Else
                objWriter.Write(Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("In1").ToString.Trim).ToString("HH:mm") & Space(1))
            End If
            'objWriter.Write(IIf(Rs_Report.Tables(0).Rows(i).Item("out1").ToString.Trim = "", Space(5), Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("out1").ToString.Trim).ToString("HH:mm") & Space(1)))
            If Rs_Report.Tables(0).Rows(i).Item("out1").ToString.Trim = "" Then
                objWriter.Write(Space(5))
            Else
                objWriter.Write(Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("out1").ToString.Trim).ToString("HH:mm") & Space(1))
            End If
            'objWriter.Write(IIf(Rs_Report.Tables(0).Rows(i).Item("in2").ToString.Trim = "", Space(5), Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("in2").ToString.Trim).ToString("HH:mm") & Space(1)))
            If Rs_Report.Tables(0).Rows(i).Item("in2").ToString.Trim = "" Then
                objWriter.Write(Space(5))
            Else
                objWriter.Write(Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("in2").ToString.Trim).ToString("HH:mm") & Space(1))
            End If
            'objWriter.Write(IIf(Rs_Report.Tables(0).Rows(i).Item("out2").ToString.Trim = "", Space(5), Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("out2").ToString.Trim).ToString("HH:mm") & Space(1)))
            If Rs_Report.Tables(0).Rows(i).Item("out2").ToString.Trim = "" Then
                objWriter.Write(Space(5))
            Else
                objWriter.Write(Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("out2").ToString.Trim).ToString("HH:mm") & Space(1))
            End If
            objWriter.Write(IIf(Rs_Report.Tables(0).Rows(i).Item("HOURSWORKED").ToString.Trim = "" Or Rs_Report.Tables(0).Rows(i).Item("HOURSWORKED").ToString.Trim = "0", Space(8), Space(5 - 4) & Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("HOURSWORKED").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("HOURSWORKED").ToString.Trim Mod 60).ToString("00") & Space(2)))
            objWriter.Write(IIf(Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim = "" Or Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim = "0", Space(8), Space(5 - 4) & Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim Mod 60).ToString("00") & Space(2)))
            objWriter.Write(IIf(Rs_Report.Tables(0).Rows(i).Item("earlydeparture").ToString.Trim = "" Or Rs_Report.Tables(0).Rows(i).Item("earlydeparture").ToString.Trim = "0", Space(8), Space(5 - 4) & Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("earlydeparture").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("earlydeparture").ToString.Trim Mod 60).ToString("00") & Space(2)))

            objWriter.Write(IIf(Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim = "", Space(5), Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim & Space(5)))
            objWriter.Write(IIf(Rs_Report.Tables(0).Rows(i).Item("OtDuration").ToString.Trim = "" Or Rs_Report.Tables(0).Rows(i).Item("OtDuration").ToString.Trim = "0", Space(5), Space(5 - 4) & Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("OtDuration").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("OtDuration").ToString.Trim Mod 60).ToString("00") & Space(2)))

            msrl = msrl + 1
            mintLine = mintLine + 1
            mblnCheckReport = True
            '                .MoveNext()
            '            Loop
        Next
        objWriter.Close()
        If isPdf = True Then
            'pdf start 
            Common.pdfreport(mstrFile_Name, 6, ulf)
            'pdf end 
        Else
            Try : Process.Start(mstrFile_Name) : Catch ex As Exception : Process.Start("notepad.exe", mstrFile_Name) : End Try
        End If
    End Sub
    '    Sub Daily_ContinuousLateArrival(AllContinue As String, ByVal strsortorder As String)
    '        'On Error GoTo ErrorGen
    '        Dim strDeptDivCode As String
    '        Dim strCurrentDeptDivCode As String
    '        Dim intFile As Integer
    '        Dim strsql As String
    '        Dim lngStartingNoOfDays As Integer
    '        Dim strPayCode As String
    '        Dim strPayCode1 As String
    '        Dim strPayCode2 As String
    '        Dim strPresentCardNo As String
    '        Dim strEmpName As String
    '        Dim strBus As String
    '        Dim dtmDateOffice As Date
    '        Dim blnCheck As Boolean
    '        Dim dtmLateSince As Date
    '        Dim dtmLateSinceTemp As Date
    '        Dim lngStartingNoOfDaysTemp As Integer
    '        Dim lngLateArrival() As Long
    '        Dim dtmDateLateSince() As Date
    '        Dim dtmDateForPrint As Date
    '        Dim lngNoOfDaysForPrint As Long
    '        Dim strLateStarsForPrint As String
    '        Dim lngCounter As Long
    '        Dim lngCounter1 As Long
    '        Dim lngCounter2 As Long
    '        Dim lngDateCounter As Long
    '        Dim lngTotalLateArrival As Long
    '        Dim strDepartmentCode As String
    '        Dim intPrintLineCounter As Integer
    '        Dim blnDeptAvailable As Boolean, mFirst As Boolean
    '        Dim msrl As Integer
    '        msrl = 1

    '        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
    '            CommonReport.GetCompanies()
    '        End If

    '        lngStartingNoOfDays = mlngStartingNoOfDays
    '        mblnCheckReport = False
    '        mFirst = True
    '        mintPageNo = 1
    '        mintLine = 1

    '        Dim g_HODDepartmentCode As String = "" 'nitin
    '        Dim adap As SqlDataAdapter
    '        Dim adapA As OleDbDataAdapter
    '        Dim Rs_Report As DataSet = New DataSet

    '        'If Left(strsortorder, 1) = "D" Or Left(strsortorder, 1) = "C" Then blnDeptAvailable = True
    '        If strsortorder = "" Then
    '            'If g_Report_view Then
    '            If Common.servername = "Access" Then
    '                strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tbltimeregister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tbltimeregister.DateOffice,tbltimeregister.LateArrival,tblEmployee.Bus,tblCalander.Process,tblCalander.NRTCProc" & _
    '                 " from tblCatagory,tblDivision,  tbltimeregister,tblEmployee,tblCalander,tblCompany,tblDepartment" & _
    '                 " Where tblCatagory.Cat = tblEmployee.Cat And tblDivision.DivisionCode = tblEmployee.DivisionCode And tbltimeregister.PayCode = tblEmployee.PayCode And tbltimeregister.LateArrival > 0 And FORMAT(tbltimeregister.DateOffice,'MMM DD YYYY') between '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' And '" & DateEdit2.DateTime.ToString("yyyy-MM-dd") & "' And FORMAT(tblCalander.mDate,'MMM DD YYYY') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
    '                 " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
    '                 " And (tblemployee.LeavingDate >=#" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "# or tblemployee.LeavingDate is null)" & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause)
    '                adapA = New OleDbDataAdapter(strsql, Common.con1)
    '                adapA.Fill(Rs_Report)
    '            Else
    '                strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tbltimeregister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tbltimeregister.DateOffice,tbltimeregister.LateArrival,tblEmployee.Bus,tblCalander.Process,tblCalander.NRTCProc" & _
    '                 " from tblCatagory,tblDivision,  tbltimeregister,tblEmployee,tblCalander,tblCompany,tblDepartment" & _
    '                 " Where tblCatagory.Cat = tblEmployee.Cat And tblDivision.DivisionCode = tblEmployee.DivisionCode And tbltimeregister.PayCode = tblEmployee.PayCode And tbltimeregister.LateArrival > 0 And tbltimeregister.DateOffice between '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' And '" & DateEdit2.DateTime.ToString("yyyy-MM-dd") & "' And tblCalander.mDate = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
    '                 " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
    '                 " And (tblemployee.LeavingDate>='" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' or tblemployee.LeavingDate is null)" & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause)
    '                adap = New SqlDataAdapter(strsql, Common.con)
    '                adap.Fill(Rs_Report)
    '            End If
    '            'Else
    '            '    strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tbltimeregisterD.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregisterD.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tbltimeregisterD.DateOffice,tbltimeregisterD.LateArrival,tblEmployee.Bus,tblCalander.Process,tblCalander.NRTCProc" & _
    '            '         " from tblCatagory,tblDivision,  tbltimeregisterD,tblEmployee,tblCalander,tblCompany,tblDepartment" & _
    '            '         " Where tblCatagory.Cat = tblEmployee.Cat And tblDivision.DivisionCode = tblEmployee.DivisionCode And tbltimeregisterD.PayCode = tblEmployee.PayCode And tbltimeregisterD.LateArrival > 0 And tbltimeregisterD.DateOffice between '" & Format(frmDailyAttReport.txtFromDate.Value, "MMM dd yyyy") & "' And '" & Format(frmDailyAttReport.TxtToDate.Value, "MMM dd yyyy") & "' And tblCalander.mDate = '" & Format(frmDailyAttReport.txtFromDate.Value, "MMM dd yyyy") & "'" & _
    '            '         " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
    '            '         " And (tblemployee.LeavingDate>='" & Format(frmDailyAttReport.txtFromDate, "MMM dd yyyy") & "' or tblemployee.LeavingDate is null)" & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause)
    '            'End If
    '            'Rs_Report = Cn.Execute(strsql)
    '            If Rs_Report.Tables(0).Rows.Count < 1 Then
    '                'G_Report_Error = False
    '                'Screen.MousePointer = vbDefault
    '                XtraMessageBox.Show(ulf, "<size=10>Nobody Continuous Late within this date range</size>", "<size=9>iAS</size>")
    '                'mReportPrintStatus = False
    '                Exit Sub
    '            End If
    '            'Rs_Report.Sort = "PayCode,DateOffice"
    '        Else
    '            'strsortorder = strsortorder & ",DateOffice"
    '            'Rs_Report.Sort = strsortorder
    '        End If

    '        'If IsNull(Rs_Report!Process) And IsNull(Rs_Report!NRTCProc) Then
    '        '   MsgBox "Records are not Processed", vbInformation + vbExclamation, "TimeWatch  Information"
    '        '   mReportPrintStatus = False
    '        '   Exit Sub
    '        'ElseIf InStr("YX", Rs_Report("Process")) > 0 Or InStr("Y", Rs_Report("nRTCProc")) > 0 Then

    '        mFileNumber = FreeFile()
    '        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".SRP"
    '        Dim objWriter As New System.IO.StreamWriter(mstrFile_Name, True)

    '        'With Rs_Report
    '        Do While Not .EOF
    '            If mFirst Or mintLine > g_LinesPerPage Or (Left(strsortorder, 10) = "Department" And strDeptDivCode <> !DepartmentCode And g_SkipAfterDept) Then
    '1:              If Not mFirst Then
    '                        Print #intFile, Space(2) + vbFormFeed
    '                Else
    '                    mFirst = False
    '                End If

    '                    Print #intFile, Space(2) + Space(centerAllign(Len(g_CompanyNames), Len("---------------------------------------------------------------------------------------------------------"))) & g_CompanyNames
    '                    Print #intFile, Space(2) + ""
    '                    Print #intFile, Space(2) + "Page No." & mintPageNo
    '                    Print #intFile, Space(2) + "                                                                       Run Date & Time : " & Format(Now(), "dd/mm/yyyy HH:MM")
    '                    Print #intFile, Space(2) + "                             Continuous late arrival report from " & Format(frmDailyAttReport.txtFromDate.Value, "dd/mm/yyyy") & " to " & Format(frmDailyAttReport.TxtToDate.Value, "dd/mm/yyyy")
    '                    Print #intFile, Space(2) + "--------------------------------------------------------------------------------------------------------------"
    '                    Print #intFile, Space(2) + "Srl  EmpCode    Card     Employee Name             Late        No of  Late  ----------Late----------     Bus"
    '                    Print #intFile, Space(2) + "No.             No.                                Since       Days         >(0.10)  >(0.30)  >(1.00)    Route"
    '                    Print #intFile, Space(2) + "--------------------------------------------------------------------------------------------------------------"
    '                mintLine = 9
    '                mintPageNo = mintPageNo + 1
    '            End If
    '            If Left(strsortorder, 10) = "Department" And strDeptDivCode <> !DepartmentCode Then
    '                If mintLine + 3 > g_LinesPerPage Then GoTo 1
    '                    Print #intFile, Space(2) + ""
    '                strDeptDivCode = !DepartmentCode
    '                    Print #intFile, Space(2) + "** Department Code & Name : " & !DepartmentCode & "  " & !DepartmentName
    '                    Print #intFile, Space(2) + ""
    '                mintLine = mintLine + 3
    '            ElseIf Left(strsortorder, 8) = "Division" And strDeptDivCode <> !DivisionCode Then
    '                If mintLine + 3 > g_LinesPerPage Then GoTo 1
    '                    Print #intFile, Space(2) + ""
    '                strDeptDivCode = !DivisionCode
    '                    Print #intFile, Space(2) + "** Section Code & Name : " & !DivisionCode & "  " & !DivisionName
    '                    Print #intFile, Space(2) + ""
    '                mintLine = mintLine + 3
    '            ElseIf Left(strsortorder, 3) = "Cat" And strDeptDivCode <> !Cat Then
    '                If mintLine + 3 > g_LinesPerPage Then GoTo 1
    '                    Print #intFile, Space(2) + ""
    '                strDeptDivCode = !Cat
    '                    Print #intFile, Space(2) + "** Category Code & Name : " & !Cat & "  " & !CatagoryName
    '                    Print #intFile, Space(2) + ""
    '                mintLine = mintLine + 3
    '            End If

    '            ReDim lngLateArrival((DateDiff("d", frmDailyAttReport.txtFromDate.Value, frmDailyAttReport.TxtToDate.Value) + 1))
    '            ReDim dtmDateLateSince((DateDiff("d", frmDailyAttReport.txtFromDate.Value, frmDailyAttReport.TxtToDate.Value) + 1))
    '            strPayCode = !paycode
    '            lngStartingNoOfDaysTemp = lngStartingNoOfDays
    '            dtmLateSince = !DATEOFFICE
    '            dtmLateSinceTemp = !DATEOFFICE
    '            dtmLateSinceTemp = dtmLateSinceTemp - 1
    '            lngCounter = 0
    '            lngCounter1 = lngStartingNoOfDays
    '            lngDateCounter = 0
    '            lngTotalLateArrival = 0
    '            Do While strPayCode = !paycode
    '                lngCounter = lngCounter + 1
    '                lngLateArrival(lngCounter) = !latearrival
    '                dtmDateLateSince(lngCounter) = !DATEOFFICE
    '                lngStartingNoOfDaysTemp = lngStartingNoOfDaysTemp - 1
    '                dtmLateSinceTemp = dtmLateSinceTemp + 1
    '                strPayCode1 = !paycode
    '                strPresentCardNo = !presentcardno
    '                strEmpName = !EmpName
    '                If Not IsNull(!bus) Then
    '                    strBus = !bus
    '                End If
    '                dtmDateOffice = !DATEOFFICE

    '                .MoveNext()
    '                If Not .EOF Then
    '                    strPayCode2 = !paycode
    '                Else
    '                    blnCheck = True
    '                End If
    '                If dtmLateSinceTemp = dtmDateOffice Then
    '                    lngDateCounter = lngDateCounter + 1
    '                Else
    '                    If lngDateCounter < lngStartingNoOfDays Then
    '                        If strPayCode1 = strPayCode2 Then
    '                            ReDim lngLateArrival((DateDiff("d", frmDailyAttReport.txtFromDate.Value, frmDailyAttReport.TxtToDate.Value) + 1))
    '                            ReDim dtmDateLateSince((DateDiff("d", frmDailyAttReport.txtFromDate.Value, frmDailyAttReport.TxtToDate.Value) + 1))
    '                            lngStartingNoOfDaysTemp = lngStartingNoOfDays
    '                            lngDateCounter = 0
    '                            lngTotalLateArrival = 0
    '                            lngCounter = 0
    '                            lngCounter1 = lngStartingNoOfDays
    '                            lngStartingNoOfDaysTemp = lngStartingNoOfDays
    '                            If Not .EOF Then
    '                                .MovePrevious()
    '                                dtmLateSinceTemp = !DATEOFFICE
    '                                dtmLateSinceTemp = dtmLateSinceTemp - 1
    '                            End If
    '                        End If
    '                    End If
    '                End If
    '                If lngDateCounter >= lngStartingNoOfDays Then
    '                    If lngStartingNoOfDaysTemp <= 0 Then
    '                        If dtmLateSinceTemp <> dtmDateOffice Or strPayCode1 <> strPayCode2 Or blnCheck Then
    '                            lngCounter2 = lngCounter - lngCounter1
    '                            If strPayCode1 <> strPayCode2 Or blnCheck Then
    '                                If lngCounter2 = 0 Then
    '                                    lngCounter2 = lngCounter2 + 1
    '                                End If
    '                            End If
    '                            dtmDateForPrint = dtmDateLateSince(lngCounter2)
    '                            If dtmLateSinceTemp <> dtmDateOffice Then
    '                                Do While lngCounter2 < lngCounter
    '                                    lngTotalLateArrival = lngTotalLateArrival + lngLateArrival(lngCounter2)
    '                                    lngCounter2 = lngCounter2 + 1
    '                                Loop
    '                            Else
    '                                Do While lngCounter2 <= lngCounter
    '                                    lngTotalLateArrival = lngTotalLateArrival + lngLateArrival(lngCounter2)
    '                                    lngCounter2 = lngCounter2 + 1
    '                                Loop
    '                            End If
    '                            If strPayCode1 <> strPayCode2 Or blnCheck Then
    '                                lngNoOfDaysForPrint = lngCounter1 + 1
    '                                If lngNoOfDaysForPrint > lngDateCounter Then
    '                                    lngNoOfDaysForPrint = lngDateCounter
    '                                End If
    '                            Else
    '                                lngNoOfDaysForPrint = lngCounter1
    '                            End If
    '                            strLateStarsForPrint = "                             "
    '                            If lngTotalLateArrival > 10 And lngTotalLateArrival <= 30 Then
    '                                strLateStarsForPrint = "   **                        "
    '                            ElseIf lngTotalLateArrival > 30 And lngTotalLateArrival <= 60 Then
    '                                strLateStarsForPrint = "            **               "
    '                            ElseIf lngTotalLateArrival > 60 Then
    '                                strLateStarsForPrint = "                     **      "
    '                            End If
    '                            If UCase(AllContinue) = "Y" Then
    '                                If strPayCode1 <> strPayCode2 Or blnCheck Then
    '                                        Print #intFile, Space(2) + Format(msrl, "@@@@") & " " & strPayCode & " " & strPresentCardNo & " " & strEmpName & " " & Format(dtmDateForPrint, "dd/mm/yyyy") & "  " & String(5 - Len(Trim(Length5(CDbl(lngNoOfDaysForPrint)))), " ") & Trim(Length5(CDbl(lngNoOfDaysForPrint))) & String(5 - Len(Trim(Length5(Min2Hr(CStr(lngTotalLateArrival))))), " ") & " " & Trim(Length5(Min2Hr(CStr(lngTotalLateArrival)))) & " " & strLateStarsForPrint & strBus
    '                                    msrl = msrl + 1
    '                                    mintLine = mintLine + 1
    '                                End If
    '                            Else
    '                                    Print #intFile, Space(2) + Format(msrl, "@@@@") & " " & strPayCode & " " & strPresentCardNo & " " & strEmpName & " " & Format(dtmDateForPrint, "dd/mm/yyyy") & "  " & String(5 - Len(Trim(Length5(CDbl(lngNoOfDaysForPrint)))), " ") & Trim(Length5(CDbl(lngNoOfDaysForPrint))) & String(5 - Len(Trim(Length5(Min2Hr(CStr(lngTotalLateArrival))))), " ") & " " & Trim(Length5(Min2Hr(CStr(lngTotalLateArrival)))) & " " & strLateStarsForPrint & strBus
    '                                msrl = msrl + 1
    '                                mintLine = mintLine + 1
    '                            End If
    '                            mblnCheckReport = True
    '                            If strPayCode1 = strPayCode2 And Not .EOF Then
    '                                ReDim lngLateArrival((DateDiff("d", frmDailyAttReport.txtFromDate.Value, frmDailyAttReport.TxtToDate.Value) + 1))
    '                                ReDim dtmDateLateSince((DateDiff("d", frmDailyAttReport.txtFromDate.Value, frmDailyAttReport.TxtToDate.Value) + 1))
    '                                lngDateCounter = 0
    '                                lngTotalLateArrival = 0
    '                                lngCounter = 0
    '                                lngCounter1 = lngStartingNoOfDays
    '                                lngStartingNoOfDaysTemp = lngStartingNoOfDays
    '                                If Not .EOF Then
    '                                    .MovePrevious()
    '                                    dtmLateSinceTemp = !DATEOFFICE
    '                                    dtmLateSinceTemp = dtmLateSinceTemp - 1
    '                                End If
    '                            End If
    '                        End If
    '                        If lngStartingNoOfDaysTemp < 0 Then
    '                            lngCounter1 = lngCounter1 + 1
    '                        End If
    '                    End If
    '                End If
    '                If .EOF Then Exit Do
    '            Loop
    '        Loop
    '        'End With
    '        '    Close #intFile

    '        '        'Else
    '        '        '  MsgBox "Records are not Processed", vbInformation + vbExclamation, "TimeWatch  Information"
    '        '        ''  mReportPrintStatus = False
    '        '        ' Exit Sub
    '        '        'End If
    '        '        Exit Sub
    '        'ErrorGen:
    '        '        MsgBox(Err.Description, vbInformation + vbOKOnly, "Report Generation Information")
    '        '        Screen.MousePointer = vbDefault

    '    End Sub
    Sub SpotXl_LateArrival(strsortorder As String)
        On Error Resume Next

        Dim xlapp As Excel.Application
        Dim xlwb As Excel.Workbook
        Dim xlst As Excel.Sheets
        Dim strsql As String, mCount As Integer
        Dim v_Late As String
        Dim rowcnt As Integer
        xlapp = CreateObject("Excel.Application")
        xlwb = xlapp.Workbooks.Add
        'If UCase(Dir(App.Path & "\TimeWatch.xls")) = UCase("TimeWatch.xls") Then
        '    Kill(App.Path & "\TimeWatch.xls")
        'End If
        'MsgBox(XtraShortOrder.g_SortOrder)
        If XtraShortOrder.g_SortOrder <> "" Then
            strsortorder = XtraShortOrder.g_SortOrder
        End If
        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If
        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".xls"

        mstrDepartmentCode = " "
        mintPageNo = 1
        mintLine = 1
        mblnCheckReport = False
        'strsql = " Select tblCatagory.Catagoryname, tblDivision.DivisionName, tblTimeregister.PAYCODE, tblTimeregister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, " & _
        '          "tblDepartment.DepartmentCode, tblDepartment.DepartmentName,tblEmployee.PresentCARDNO, tblEmployee.EMPNAME, tblTimeregister.DATEOFFICE, " & _
        '          "tblTimeregister.SHIFTATTENDED, tblTimeregister.shiftStartTime, tblTimeregister.In1, tblTimeregister.Latearrival, " & _
        '          "Late1 = Case When tblTimeregister.Latearrival <= 10 then '**' Else '  'End, " & _
        '          "Late10 = Case When tblTimeregister.Latearrival > 10 And tblTimeregister.Latearrival <= 30 then '**' Else '  ' End, " & _
        '          "Late30 = Case When tblTimeregister.Latearrival > 30 And tblTimeregister.Latearrival <= 60 then '**' Else '  ' End, " & _
        '          "Late60 = Case When tblTimeregister.Latearrival > 60 then '**' Else '  ' End, tblEmployee.BUS " & _
        '          " From tblCatagory, tblDivision,tblTimeregister, tblEmployee, tblDepartment, tblCompany " & _
        '          "Where tblEmployee.CAT = tblCatagory.cat And tblEmployee.PayCode = tblTimeregister.PayCode And " & _
        '          "tblEmployee.DepartmentCode = tblDepartment.DepartmentCode AND tbltimeregister.DateOffice = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' AND  " & _
        '          "tblEmployee.CompanyCode = tblCompany.CompanyCode And  " & _
        '          "tblTimeregister.Latearrival > 0" & g_HODDepartmentCode & _
        '          " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & strsortorder

        'If Common.servername = "Access" Then
        '    adapA = New OleDbDataAdapter(strsql, Common.con1)
        '    adapA.Fill(Rs_Report)
        'Else
        '    adap = New SqlDataAdapter(strsql, Common.con)
        '    adap.Fill(Rs_Report)
        'End If
        If Common.servername = "Access" Then
            'strsql = " Select tblCatagory.Catagoryname, tblDivision.DivisionName, tblTimeregister.PAYCODE, tblTimeregister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, " & _
            '    "tblDepartment.DepartmentCode, tblDepartment.DepartmentName,tblEmployee.PresentCARDNO, tblEmployee.EMPNAME, tblTimeregister.DATEOFFICE, " & _
            '    "tblTimeregister.SHIFTATTENDED, tblTimeregister.shiftStartTime, tblTimeregister.In1, tblTimeregister.Latearrival, " & _
            '    "IIF (tblTimeregister.Latearrival <= 10,'**','  ') as Late1, " & _
            '    "IIF (tblTimeregister.Latearrival > 10 And tblTimeregister.Latearrival <= 30, '**' ,'  ') AS Late10, " & _
            '    "IIF(tblTimeregister.Latearrival > 30 And tblTimeregister.Latearrival <= 60,'**','  ') AS Late30, " & _
            '    "IIF (tblTimeregister.Latearrival > 60 ,'**', '  ') AS Late60, tblEmployee.BUS " & _
            '    " From tblCatagory, tblDivision,tblTimeregister, tblEmployee, tblDepartment, tblCompany " & _
            '    "Where tblEmployee.CAT = tblCatagory.cat And tblEmployee.DivisionCode = tblDivision.DivisionCode And tblEmployee.PayCode = tblTimeregister.PayCode And " & _
            '    "tblEmployee.DepartmentCode = tblDepartment.DepartmentCode AND format(tbltimeregister.DateOffice,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' AND  " & _
            '    "tblEmployee.CompanyCode = tblCompany.CompanyCode And  " & _
            '    "tblTimeregister.Latearrival > 0" & g_HODDepartmentCode & _
            '    " And (tblemployee.LeavingDate >=#" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "# or tblemployee.LeavingDate is null)" & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause)

            strsql = " Select tblCatagory.Catagoryname, tblDivision.DivisionName, tblTimeregister.PAYCODE, tblTimeregister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, " & _
                "tblDepartment.DepartmentCode, tblDepartment.DepartmentName,tblEmployee.PresentCARDNO, tblEmployee.EMPNAME, tblTimeregister.DATEOFFICE, " & _
                "tblTimeregister.SHIFTATTENDED, tblTimeregister.shiftStartTime, tblTimeregister.In1, tblTimeregister.Latearrival, " & _
                "IIF (tblTimeregister.Latearrival <= 10,'**','  ') as Late1, " & _
                "IIF (tblTimeregister.Latearrival > 10 And tblTimeregister.Latearrival <= 30, '**' ,'  ') AS Late10, " & _
                "IIF(tblTimeregister.Latearrival > 30 And tblTimeregister.Latearrival <= 60,'**','  ') AS Late30, " & _
                "IIF (tblTimeregister.Latearrival > 60 ,'**', '  ') AS Late60, tblEmployee.BUS " & _
                " From tblCatagory, tblDivision,tblTimeregister, tblEmployee, tblDepartment, tblCompany " & _
                "Where tblEmployee.CAT = tblCatagory.cat And tblEmployee.PayCode = tblTimeregister.PayCode And " & _
                "tblEmployee.DepartmentCode = tblDepartment.DepartmentCode AND format(tbltimeregister.DateOffice,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' AND  " & _
                "tblEmployee.CompanyCode = tblCompany.CompanyCode And  " & _
                "tblTimeregister.Latearrival > 0" & g_HODDepartmentCode & _
                "" & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause)

            adapA = New OleDbDataAdapter(strsql, Common.con1)
            adapA.Fill(Rs_Report)
        Else
            strsql = " Select tblCatagory.Catagoryname, tblDivision.DivisionName, tblTimeregister.PAYCODE, tblTimeregister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, " & _
                "tblDepartment.DepartmentCode, tblDepartment.DepartmentName,tblEmployee.PresentCARDNO, tblEmployee.EMPNAME, tblTimeregister.DATEOFFICE, " & _
                "tblTimeregister.SHIFTATTENDED, tblTimeregister.shiftStartTime, tblTimeregister.In1, tblTimeregister.Latearrival, " & _
                "Late1 = Case When tblTimeregister.Latearrival <= 10 then '**' Else '  'End, " & _
                "Late10 = Case When tblTimeregister.Latearrival > 10 And tblTimeregister.Latearrival <= 30 then '**' Else '  ' End, " & _
                "Late30 = Case When tblTimeregister.Latearrival > 30 And tblTimeregister.Latearrival <= 60 then '**' Else '  ' End, " & _
                "Late60 = Case When tblTimeregister.Latearrival > 60 then '**' Else '  ' End, tblEmployee.BUS " & _
                " From tblCatagory, tblDivision,tblTimeregister, tblEmployee, tblDepartment, tblCompany " & _
                "Where tblEmployee.CAT = tblCatagory.cat And tblEmployee.PayCode = tblTimeregister.PayCode And " & _
                "tblEmployee.DepartmentCode = tblDepartment.DepartmentCode AND tbltimeregister.DateOffice = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' AND  " & _
                "tblEmployee.CompanyCode = tblCompany.CompanyCode And  " & _
                "tblTimeregister.Latearrival > 0" & g_HODDepartmentCode & _
                " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause)

            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(Rs_Report)
        End If
        'Rs_Report = Cn.Execute(strsql)
        If strsortorder = "" Then
            'Rs_Report.Sort = "PayCode"
        Else
            'Rs_Report.Sort = strsortorder
        End If
        If Rs_Report.Tables(0).Rows.Count < 1 Then
            'G_Report_Error = False
            'Screen.MousePointer = vbDefault
            XtraMessageBox.Show(ulf, "<size=10>No Data Available For this Report</size>", "<size=9>iAS</size>")
            'mReportPrintStatus = False
            Exit Sub
        End If
        mCount = 0
        rowcnt = 1
        xlapp.Visible = True

        xlapp.Cells(rowcnt, 5).Font.Bold = True
        xlapp.Cells(rowcnt, 5) = "Company Name:" & CommonReport.g_CompanyNames
        rowcnt = rowcnt + 1
        xlapp.Cells(rowcnt, 5).Font.Bold = True
        xlapp.Cells(rowcnt, 5) = "Run Date & Time :" & Now.ToString("dd/MM/yyyy HH:mm")
        rowcnt = rowcnt + 2
        xlapp.Cells.Font.Size = 8
        xlapp.Columns.ColumnWidth = 10
        xlapp.Columns.Font.Name = "TAHOMA"
        xlapp.Cells(rowcnt, 5).Font.Bold = True

        xlapp.Cells(rowcnt, 5) = "LATE ARRIVAL REPORT FOR DATE : " & DateEdit1.DateTime.ToString("dd/MM/yyyy")
        rowcnt = rowcnt + 1

        xlapp.Range("A5:M5").Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Range("A5:M5").Font.Bold = True
        xlapp.Range("A5:M5").Font.ColorIndex = 2 'vbWhite
        xlapp.Range("A5:M5").HorizontalAlignment = "Left" ' xlLeft
        xlapp.Range("A5:M5").Interior.ColorIndex = 51


        xlapp.Cells(rowcnt, 1) = "Sr.No"
        xlapp.Cells(rowcnt, 2) = "PayCode"
        xlapp.Cells(rowcnt, 3) = "Card No"
        xlapp.Cells(rowcnt, 4) = "Employee Name"
        xlapp.Cells(rowcnt, 6) = "Shift  "
        xlapp.Cells(rowcnt, 7) = "Shift Start "
        xlapp.Cells(rowcnt, 8) = "In"
        xlapp.Cells(rowcnt, 9) = "Shift Late "
        xlapp.Cells(rowcnt, 10) = "          ---------------Shift Late -----------"
        rowcnt = rowcnt + 1
        xlapp.Cells(rowcnt, 10).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 11).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 12).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 10) = "         >(0.10)     >(0.30)   >(1.00)    "
        rowcnt = rowcnt - 1
        xlapp.Cells(rowcnt, 13).Borders.LineStyle = XlLineStyle.xlContinuous
        'xlapp.Cells(rowcnt, 14).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 13) = "Bus Route "
        rowcnt = rowcnt + 1
        'With Rs_Report
        '    .MoveFirst()
        '    Do While Not .EOF
        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            'If mFirst Or mintLine > g_LinesPerPage Or (strsortorder.Trim = "Department" And strDeptDivCode <> Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim And g_SkipAfterDept) Then
            If strsortorder = "Department" Or XtraShortOrder.CheckDeptName.Checked Then
                If mstrDepartmentCode <> Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim Then
                    rowcnt = rowcnt + 2
                    xlapp.Cells(rowcnt, 1) = "*** Department Code & Name " & Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim & " " & Rs_Report.Tables(0).Rows(i).Item("DepartmentName").ToString.Trim
                    mstrDepartmentCode = Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString
                    rowcnt = rowcnt + 1
                End If
            End If
            If strsortorder.Contains("Division") Or XtraShortOrder.CheckSectionPay.Checked Then
                If mstrDepartmentCode <> Rs_Report.Tables(0).Rows(i).Item("DivisionCode").ToString Then
                    rowcnt = rowcnt + 2
                    xlapp.Cells(rowcnt, 1) = "*** Section Code & Name " & Rs_Report.Tables(0).Rows(i).Item("DivisionCode").ToString.Trim & " " & Rs_Report.Tables(0).Rows(i).Item("DivisionName").ToString.Trim
                    mstrDepartmentCode = Rs_Report.Tables(0).Rows(i).Item("DivisionCode").ToString.Trim
                    rowcnt = rowcnt + 1
                End If
            End If
            If strsortorder.Contains("Cat") Or XtraShortOrder.CheckCatagoryPaycode.Checked Then
                If mstrDepartmentCode <> Rs_Report.Tables(0).Rows(i).Item("Cat").ToString.Trim Then
                    rowcnt = rowcnt + 2

                    xlapp.Cells(rowcnt, 1) = "*** Category Code & Name " & Rs_Report.Tables(0).Rows(i).Item("Cat").ToString.Trim & " " & Rs_Report.Tables(0).Rows(i).Item("CatagoryName").ToString.Trim
                    mstrDepartmentCode = Rs_Report.Tables(0).Rows(i).Item("Cat").ToString.Trim
                    rowcnt = rowcnt + 1
                End If
            End If
            mCount = mCount + 1

            If Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim = "0" Then
                v_Late = ""
            Else
                v_Late = Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim Mod 60).ToString("00")
            End If

            'v_Late = Length5(Min2Hr(!latearrival))
            rowcnt = rowcnt + 1
            xlapp.Cells(rowcnt, 1).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 1) = mCount
            xlapp.Cells(rowcnt, 2).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 2) = "'" & Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim
            xlapp.Cells(rowcnt, 3).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 3) = "'" & Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim
            xlapp.Cells(rowcnt, 4).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 4) = Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim
            xlapp.Cells(rowcnt, 5).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 6).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 6) = Rs_Report.Tables(0).Rows(i).Item("ShiftAttended").ToString.Trim
            xlapp.Cells(rowcnt, 7).Borders.LineStyle = XlLineStyle.xlContinuous
            If Rs_Report.Tables(0).Rows(i).Item("shiftStartTime").ToString.Trim = "" Then
                xlapp.Cells(rowcnt, 7) = ""
            Else
                xlapp.Cells(rowcnt, 7) = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("shiftStartTime").ToString.Trim).ToString("HH:mm")
            End If
            'xlapp.Cells(rowcnt, 7) = Format(!shiftStartTime, "hh:mm")
            xlapp.Cells(rowcnt, 8).Borders.LineStyle = XlLineStyle.xlContinuous
            If Rs_Report.Tables(0).Rows(i).Item("In1").ToString.Trim = "" Then
                xlapp.Cells(rowcnt, 8) = ""
            Else
                xlapp.Cells(rowcnt, 8) = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("In1").ToString.Trim).ToString("HH:mm")
            End If
            'xlapp.Cells(rowcnt, 8) = Format(!In1, "hh:mm")
            xlapp.Cells(rowcnt, 9).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 9) = "'" & (v_Late)
            xlapp.Cells(rowcnt, 10).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 11).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 12).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 13).Borders.LineStyle = XlLineStyle.xlContinuous
            'xlapp.Cells(rowcnt, 14).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 13) = Rs_Report.Tables(0).Rows(i).Item("bus").ToString.Trim
            mblnCheckReport = True
            '    .MoveNext()
            'Loop
        Next
        'End With

        xlwb.SaveAs(mstrFile_Name)
        'Process.Start(mstrFile_Name)
        'g_PageNo = mintPageNo
        'Screen.MousePointer = vbDefault

        '        Exit Sub

        'LateArrival_Error:
        '        MsgBox(Err.Description, vbInformation + vbOKOnly, "Late Arrival")
        '        Screen.MousePointer = vbDefault
    End Sub
    Sub SpotXl_Absenteeism(strsortorder As String)
        Dim xlapp As Excel.Application
        Dim xlwb As Excel.Workbook
        Dim xlst As Excel.Sheets
        Dim strsql As String, mCount As Integer
        Dim v_Absent As String '* 5
        Dim v_Leave As String '* 5
        Dim v_OnDuty As String '* 5
        Dim rowcnt As Integer
        xlapp = CreateObject("Excel.Application")
        xlwb = xlapp.Workbooks.Add
        'If UCase(Dir(App.Path & "\TimeWatch.xls")) = UCase("TimeWatch.xls") Then
        '    Kill(App.Path & "\TimeWatch.xls")
        'End If
        mstrDepartmentCode = " "
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".xls"

        'On Error GoTo AbsenteeismError
        mintPageNo = 1
        mintLine = 1
        mblnCheckReport = False
        If XtraShortOrder.g_SortOrder <> "" Then
            strsortorder = XtraShortOrder.g_SortOrder
        End If
        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If
        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet

        If Common.servername = "Access" Then
            'strsql = " Select tblCatagory.Catagoryname, tblDivision.DivisionName, tblTimeregister.Paycode,tblTimeregister.reason,tblTimeregister.Shift,TblEmployee.EmpName, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, TblEmployee.PresentCardno, TblDepartment.DepartmentCode, tblDepartment.DepartmentName,tblTimeregister.Status, tblTimeregister.LeaveType From  tblCatagory, tblDivision, tblTimeregister, TblEmployee, tblDepartment,tblCompany " & _
            '   " Where tblEmployee.CAT = tblCatagory.cat And tblEmployee.DivisionCode = tblDivision.DivisionCode And (tblTimeregister.AbsentValue > 0 Or tblTimeregister.LeaveType In ('A', 'P', 'L')) And " & _
            '   " tblEmployee.companycode=tblCompany.companycode and tblEmployee.DepartmentCode = tblDepartment.DepartmentCode AND FORMAT(tbltimeregister.DateOffice,'MMM DD YYYY') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
            '   " and tblEmployee.PayCode = tblTimeregister.Paycode" & g_HODDepartmentCode & _
            '   " And (tblemployee.LeavingDate>=#" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "# or tblemployee.LeavingDate is null)" & IIf(Len(Trim(g_WhereClause)) = 0, "", " And " & g_WhereClause) & strsortorder

            strsql = " Select tblCatagory.Catagoryname, tblDivision.DivisionName, tblTimeregister.Paycode,tblTimeregister.reason,tblTimeregister.Shift,TblEmployee.EmpName, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, TblEmployee.PresentCardno, TblDepartment.DepartmentCode, tblDepartment.DepartmentName,tblTimeregister.Status, tblTimeregister.LeaveType From  tblCatagory, tblDivision, tblTimeregister, TblEmployee, tblDepartment,tblCompany " & _
               " Where tblEmployee.CAT = tblCatagory.cat And (tblTimeregister.AbsentValue > 0 Or tblTimeregister.LeaveType In ('A', 'P', 'L')) And " & _
               " tblEmployee.companycode=tblCompany.companycode and tblEmployee.DepartmentCode = tblDepartment.DepartmentCode AND FORMAT(tbltimeregister.DateOffice,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
               " and tblEmployee.PayCode = tblTimeregister.Paycode" & g_HODDepartmentCode & _
               " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " And " & g_WhereClause) & strsortorder
            adapA = New OleDbDataAdapter(strsql, Common.con1)
            adapA.Fill(Rs_Report)
        Else
            strsql = " Select tblCatagory.Catagoryname, tblDivision.DivisionName, tblTimeregister.Paycode,tblTimeregister.reason,tblTimeregister.Shift,TblEmployee.EmpName, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, TblEmployee.PresentCardno, TblDepartment.DepartmentCode, tblDepartment.DepartmentName,tblTimeregister.Status, tblTimeregister.LeaveType From  tblCatagory, tblDivision, tblTimeregister, TblEmployee, tblDepartment,tblCompany " & _
               " Where tblEmployee.CAT = tblCatagory.cat And  (tblTimeregister.AbsentValue > 0 Or tblTimeregister.LeaveType In ('A', 'P', 'L')) And " & _
               " tblEmployee.companycode=tblCompany.companycode and tblEmployee.DepartmentCode = tblDepartment.DepartmentCode AND tbltimeregister.DateOffice = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
               " and tblEmployee.PayCode = tblTimeregister.Paycode" & g_HODDepartmentCode & _
               " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " And " & g_WhereClause) & strsortorder
            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(Rs_Report)
        End If
        'Rs_Report = Cn.Execute(strsql)
        If strsortorder = "" Then
            'Rs_Report.Sort = "PayCode"
        Else
            'Rs_Report.Sort = strsortorder
        End If
        If Rs_Report.Tables(0).Rows.Count < 1 Then
            'G_Report_Error = False
            'Screen.MousePointer = vbDefault
            XtraMessageBox.Show(ulf, "<size=10>Nobody is absent on this Date.</size>", "<size=9>iAS</size>")
            'mReportPrintStatus = False
            Exit Sub
        End If
        rowcnt = 1
        xlapp.Visible = True

        xlapp.Columns.ColumnWidth = 9
        xlapp.Columns.Font.Name = "TAHOMA"
        xlapp.Columns.Font.Size = 8
        xlapp.Columns.Font.ColorIndex = 1 'vbBlack
        xlapp.Cells.Font.Bold = False

        xlapp.Cells(rowcnt, 5).Font.Bold = True
        xlapp.Cells(rowcnt, 5) = "Company Name:" & CommonReport.g_CompanyNames
        rowcnt = rowcnt + 1
        xlapp.Cells(rowcnt, 5).Font.Bold = True
        xlapp.Cells(rowcnt, 5) = "Run Date & Time :" & Now.ToString("dd/MM/yyyy HH:mm")
        rowcnt = rowcnt + 2
        xlapp.Cells(rowcnt, 4).Font.Bold = True
        xlapp.Cells(rowcnt, 4) = " ABSENCE FROM DUTY ON DATE : " & DateEdit1.DateTime.ToString("dd/MM/yyyy")
        rowcnt = rowcnt + 1

        xlapp.Range("A5:I5").Font.Bold = True
        xlapp.Range("A5:I5").Font.ColorIndex = 2 'vbWhite
        'xlapp.Range("A5:I5").HorizontalAlignment = xlLeft
        xlapp.Range("A5:I5").Interior.ColorIndex = 51
        xlapp.Range("A5:I5").Borders.LineStyle = XlLineStyle.xlContinuous

        xlapp.Cells(rowcnt, 1) = "Sr.No."
        xlapp.Cells(rowcnt, 2) = "PayCode"
        xlapp.Cells(rowcnt, 3) = "Card No"
        xlapp.Cells(rowcnt, 4) = "Employee Name"
        xlapp.Cells(rowcnt, 6) = "Absent  "
        xlapp.Cells(rowcnt, 7) = "Leave "
        xlapp.Cells(rowcnt, 8) = "On Duty"
        xlapp.Cells(rowcnt, 9) = "Remarks "
        'rowcnt = rowcnt + 1
        mCount = 0
        'With Rs_Report
        '    .MoveFirst()
        '    Do While Not .EOF
        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            If strsortorder = "Department" Or XtraShortOrder.CheckDeptName.Checked Then
                If mstrDepartmentCode <> Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim Then
                    rowcnt = rowcnt + 2
                    xlapp.Cells(rowcnt, 1) = "*** Department Code & Name " & Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim & " " & Rs_Report.Tables(0).Rows(i).Item("DepartmentName").ToString.Trim
                    mstrDepartmentCode = Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString
                    rowcnt = rowcnt + 1
                End If
            End If
            If strsortorder.Contains("Division") Or XtraShortOrder.CheckSectionPay.Checked Then
                If mstrDepartmentCode <> Rs_Report.Tables(0).Rows(i).Item("DivisionCode").ToString Then
                    rowcnt = rowcnt + 2
                    xlapp.Cells(rowcnt, 1) = "*** Section Code & Name " & Rs_Report.Tables(0).Rows(i).Item("DivisionCode").ToString.Trim & " " & Rs_Report.Tables(0).Rows(i).Item("DivisionName").ToString.Trim
                    mstrDepartmentCode = Rs_Report.Tables(0).Rows(i).Item("DivisionCode").ToString.Trim
                    rowcnt = rowcnt + 1
                End If
            End If
            If strsortorder.Contains("Cat") Or XtraShortOrder.CheckCatagoryPaycode.Checked Then
                If mstrDepartmentCode <> Rs_Report.Tables(0).Rows(i).Item("Cat").ToString.Trim Then
                    rowcnt = rowcnt + 2

                    xlapp.Cells(rowcnt, 1) = "*** Category Code & Name " & Rs_Report.Tables(0).Rows(i).Item("Cat").ToString.Trim & " " & Rs_Report.Tables(0).Rows(i).Item("CatagoryName").ToString.Trim
                    mstrDepartmentCode = Rs_Report.Tables(0).Rows(i).Item("Cat").ToString.Trim
                    rowcnt = rowcnt + 1
                End If
            End If

            mCount = mCount + 1
            v_Absent = IIf(Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim = "A" Or Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim = "HLF" Or Rs_Report.Tables(0).Rows(i).Item("LEAVETYPE").ToString.Trim = "A", Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim, " ")
            v_OnDuty = IIf(Rs_Report.Tables(0).Rows(i).Item("LEAVETYPE").ToString.Trim = "P", Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim, "   ")
            v_Leave = IIf(Rs_Report.Tables(0).Rows(i).Item("LEAVETYPE").ToString.Trim = "L", Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim, "   ")
            rowcnt = rowcnt + 1
            xlapp.Cells(rowcnt, 1).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 1) = mCount
            xlapp.Cells(rowcnt, 2).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 2) = "'" & Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim
            xlapp.Cells(rowcnt, 3).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 3) = "'" & Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim
            xlapp.Cells(rowcnt, 4).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 4) = Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim
            xlapp.Cells(rowcnt, 5).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 6).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 6) = v_Absent
            xlapp.Cells(rowcnt, 7).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 7) = v_Leave
            xlapp.Cells(rowcnt, 8).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 8) = v_OnDuty
            xlapp.Cells(rowcnt, 9).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 9) = Rs_Report.Tables(0).Rows(i).Item("reason").ToString.Trim
            mintLine = mintLine + 1
            mblnCheckReport = True
            '.MoveNext()
            '    Loop
        Next
        'End With
        xlwb.SaveAs(mstrFile_Name)
        '        xlwb.SaveAs(App.Path & "/TimeWatch.xls")

        '        g_PageNo = mintPageNo
        '        Exit Sub
        'AbsenteeismError:
        '        ErrMessage("AbsenteeismInformation")
    End Sub
    Sub SpotXl_Attendance(strsortorder As String)
        Dim strsql As String
        Dim v_Late As String '* 6
        Dim v_InTime As String '* 6
        Dim strStatus As String, mCount As Integer
        Dim xlapp As Excel.Application
        Dim xlwb As Excel.Workbook
        Dim xlst As Excel.Sheets
        Dim rowcnt As Integer
        xlapp = CreateObject("Excel.Application")
        xlwb = xlapp.Workbooks.Add
        'If UCase(Dir(App.Path & "\TimeWatch.xls")) = UCase("TimeWatch.xls") Then
        '    Kill(App.Path & "\TimeWatch.xls")
        'End If
        mstrDepartmentCode = " "
        mintPageNo = 1
        mintLine = 1
        If XtraShortOrder.g_SortOrder <> "" Then
            strsortorder = XtraShortOrder.g_SortOrder
        End If
        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If
        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".xls"

        mblnCheckReport = False
        If Common.servername = "Access" Then
            'strsql = " Select tblTimeregister.PAYCODE, tblTimeregister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, tblDepartment.DepartmentCode, tblDepartment.DepartmentName,tblEmployee.PresentCARDNO, tblEmployee.EMPNAME, tblTimeregister.DATEOFFICE, " & _
            '     " tblTimeregister.SHIFTATTENDED, tblTimeregister.ShiftStartTime, tblTimeregister.In1, tblTimeregister.LateArrival, tblTimeregister.Status, tblCatagory.Catagoryname, tblDivision.DivisionName " & _
            '     " From  tblTimeregister, tblEmployee, tblDepartment, tblCatagory, tblDivision,tblCompany " & _
            '     " Where tblEmployee.PayCode = tblTimeregister.PayCode And " & _
            '     " tblEmployee.Companycode=tblCompany.Companycode and tblEmployee.CAT = tblCatagory.cat And " & _
            '     " tblEmployee.DivisionCode = tblDivision.DivisionCode AND FORMAT(tbltimeregister.DateOffice,'MMM DD YYYY') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' And " & _
            '     " tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
            '     " And (tblemployee.LeavingDate >=#" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "# or tblemployee.LeavingDate is null)" & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & strsortorder


            strsql = " Select tblTimeregister.PAYCODE, tblTimeregister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, tblDepartment.DepartmentCode, tblDepartment.DepartmentName,tblEmployee.PresentCARDNO, tblEmployee.EMPNAME, tblTimeregister.DATEOFFICE, " & _
                " tblTimeregister.SHIFTATTENDED, tblTimeregister.ShiftStartTime, tblTimeregister.In1, tblTimeregister.LateArrival, tblTimeregister.Status, tblCatagory.Catagoryname, tblDivision.DivisionName " & _
                " From  tblTimeregister, tblEmployee, tblDepartment, tblCatagory, tblDivision,tblCompany " & _
                " Where tblEmployee.PayCode = tblTimeregister.PayCode And " & _
                " tblEmployee.Companycode=tblCompany.Companycode and tblEmployee.CAT = tblCatagory.cat And " & _
                " FORMAT(tbltimeregister.DateOffice,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' And " & _
                " tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
                " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & strsortorder
            adapA = New OleDbDataAdapter(strsql, Common.con1)
            adapA.Fill(Rs_Report)
        Else
            strsql = " Select tblTimeregister.PAYCODE, tblTimeregister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, tblDepartment.DepartmentCode, tblDepartment.DepartmentName,tblEmployee.PresentCARDNO, tblEmployee.EMPNAME, tblTimeregister.DATEOFFICE, " & _
                     " tblTimeregister.SHIFTATTENDED, tblTimeregister.ShiftStartTime, tblTimeregister.In1, tblTimeregister.LateArrival, tblTimeregister.Status, tblCatagory.Catagoryname, tblDivision.DivisionName " & _
                     " From  tblTimeregister, tblEmployee, tblDepartment, tblCatagory, tblDivision,tblCompany " & _
                     " Where tblEmployee.PayCode = tblTimeregister.PayCode And " & _
                     " tblEmployee.Companycode=tblCompany.Companycode and tblEmployee.CAT = tblCatagory.cat And " & _
                     "  tbltimeregister.DateOffice = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' And " & _
                     " tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
                     " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & strsortorder
            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(Rs_Report)
        End If
        'Rs_Report = Cn.Execute(strsql)
        If strsortorder = "" Then
            'Rs_Report.Sort = "PayCode"
        Else
            'Rs_Report.Sort = strsortorder
        End If
        If Rs_Report.Tables(0).Rows.Count < 1 Then
            'G_Report_Error = False
            'Screen.MousePointer = vbDefault
            XtraMessageBox.Show(ulf, "<size=10>No Data Available For this Report.</size>", "<size=9>iAS</size>")
            'mReportPrintStatus = False
            Exit Sub
        End If
        rowcnt = 1
        xlapp.Visible = True
        xlapp.Columns.Font.Name = "TAHOMA"
        xlapp.Columns.Font.Size = 8
        xlapp.Columns.Font.ColorIndex = 1 ' vbBlack
        xlapp.Cells(rowcnt, 5).Font.Bold = True
        xlapp.Cells(rowcnt, 5) = "Company Name:" & CommonReport.g_CompanyNames
        rowcnt = rowcnt + 1
        xlapp.Cells(rowcnt, 5).Font.Bold = True
        xlapp.Cells(rowcnt, 5) = "Run Date & Time :" & Now.ToString("dd/MM/yyyy HH:mm")
        rowcnt = rowcnt + 2
        xlapp.Cells(rowcnt, 5).Font.Bold = True
        xlapp.Cells(rowcnt, 5) = "ATTENDANCE REPORT FOR DATE : " & DateEdit1.DateTime.ToString("dd/MM/yyyy")
        rowcnt = rowcnt + 1
        rowcnt = rowcnt + 1


        xlapp.Range("A6:J6").Font.Bold = True
        xlapp.Range("A6:J6").Font.ColorIndex = 2 'vbWhite
        'xlapp.Range("A6:J6").HorizontalAlignment = xlLeft
        xlapp.Range("A6:J6").Interior.ColorIndex = 51
        xlapp.Range("A6:J6").Borders.LineStyle = XlLineStyle.xlContinuous



        xlapp.Cells(rowcnt, 1) = "Sr.No."
        xlapp.Cells(rowcnt, 2) = "PayCode"
        xlapp.Cells(rowcnt, 3) = "Card No"
        xlapp.Cells(rowcnt, 4) = "Employee Name"
        xlapp.Cells(rowcnt, 6) = "Shift  "
        xlapp.Cells(rowcnt, 7) = "Start "
        xlapp.Cells(rowcnt, 8) = "   In "
        xlapp.Cells(rowcnt, 9) = "Shift Late  "
        xlapp.Cells(rowcnt, 10) = "Status "
        'rowcnt = rowcnt + 1
        mCount = 0
        'With Rs_Report
        '    .MoveFirst()
        '    Do While Not .EOF
        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            If strsortorder = "Department" Or XtraShortOrder.CheckDeptName.Checked Then
                If mstrDepartmentCode <> Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim Then
                    rowcnt = rowcnt + 2
                    xlapp.Cells(rowcnt, 1) = "*** Department Code & Name " & Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim & " " & Rs_Report.Tables(0).Rows(i).Item("DepartmentName").ToString.Trim
                    mstrDepartmentCode = Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString
                    rowcnt = rowcnt + 1
                End If
            End If
            If strsortorder.Contains("Division") Or XtraShortOrder.CheckSectionPay.Checked Then
                If mstrDepartmentCode <> Rs_Report.Tables(0).Rows(i).Item("DivisionCode").ToString Then
                    rowcnt = rowcnt + 2
                    xlapp.Cells(rowcnt, 1) = "*** Section Code & Name " & Rs_Report.Tables(0).Rows(i).Item("DivisionCode").ToString.Trim & " " & Rs_Report.Tables(0).Rows(i).Item("DivisionName").ToString.Trim
                    mstrDepartmentCode = Rs_Report.Tables(0).Rows(i).Item("DivisionCode").ToString.Trim
                    rowcnt = rowcnt + 1
                End If
            End If
            If strsortorder.Contains("Cat") Or XtraShortOrder.CheckCatagoryPaycode.Checked Then
                If mstrDepartmentCode <> Rs_Report.Tables(0).Rows(i).Item("Cat").ToString.Trim Then
                    rowcnt = rowcnt + 2

                    xlapp.Cells(rowcnt, 1) = "*** Category Code & Name " & Rs_Report.Tables(0).Rows(i).Item("Cat").ToString.Trim & " " & Rs_Report.Tables(0).Rows(i).Item("CatagoryName").ToString.Trim
                    mstrDepartmentCode = Rs_Report.Tables(0).Rows(i).Item("Cat").ToString.Trim
                    rowcnt = rowcnt + 1
                End If
            End If

            If Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim = "0" Or Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim = "" Then
                v_Late = ""
            Else
                v_Late = Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim Mod 60).ToString("00")
            End If
            If Rs_Report.Tables(0).Rows(i).Item("In1").ToString.Trim = "" Then
                v_InTime = " "
            Else
                v_InTime = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("In1").ToString.Trim).ToString("HH:mm")
            End If
            'v_InTime = Format(!In1, "hh:mm") & "  "
            'v_Late = Length5(Min2Hr(!latearrival))
            strStatus = ""
            If Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim <> "" Then
                If UCase(Trim(Rs_Report.Tables(0).Rows(i).Item("Status").ToString)) = "MIS" Then
                    strStatus = "P"
                Else
                    strStatus = Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim
                End If
            End If
            rowcnt = rowcnt + 1
            mCount = mCount + 1
            xlapp.Cells(rowcnt, 1).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 1) = mCount
            xlapp.Cells(rowcnt, 2).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 2) = "'" & Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim
            xlapp.Cells(rowcnt, 3).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 3) = "'" & Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim
            xlapp.Cells(rowcnt, 4).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 4) = Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim
            xlapp.Cells(rowcnt, 5).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 6).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 6) = Rs_Report.Tables(0).Rows(i).Item("ShiftAttended").ToString.Trim
            xlapp.Cells(rowcnt, 7).Borders.LineStyle = XlLineStyle.xlContinuous
            'xlapp.Cells(rowcnt, 7) = Format(!shiftStartTime, "hh:mm")
            If Rs_Report.Tables(0).Rows(i).Item("shiftStartTime").ToString.Trim = "" Then
                xlapp.Cells(rowcnt, 7) = ""
            Else
                xlapp.Cells(rowcnt, 7) = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("shiftStartTime").ToString.Trim).ToString("HH:mm")
            End If

            xlapp.Cells(rowcnt, 8).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 8) = v_InTime
            xlapp.Cells(rowcnt, 9).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 9) = "'" & v_Late
            xlapp.Cells(rowcnt, 10).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 10) = strStatus
            mintLine = 1
            '.MoveNext()
            '    Loop
        Next
        xlwb.SaveAs(mstrFile_Name)
        'End With
        'xlwb.SaveAs(App.Path & "/TimeWatch.xls")
        'Screen.MousePointer = vbDefault
        'g_PageNo = mintPageNo
    End Sub
    Sub SpotXl_DepartmentSummary()
        On Error Resume Next
        Dim xlapp As Excel.Application
        Dim xlwb As Excel.Workbook
        Dim xlst As Excel.Sheets
        Dim intFile As Integer
        Dim strsql As String, mCount As Integer

        Dim strDepartmentName As String
        Dim strDepartmentCode As String

        Dim dblLeaveAmount As Double
        Dim dblPresentValue As Double
        Dim dblAbsentValue As Double
        Dim dblLeaveValue As Double
        Dim dblWo_Value As Double

        Dim dblPresentValue1 As Double
        Dim dblAbsentValue1 As Double
        Dim dblLeaveValue1 As Double
        Dim dblWo_Value1 As Double

        Dim lngOnDutyCount As Long
        Dim strOnDutyCount7 As String '* 7
        Dim lngTotalEmpCount As Long

        Dim rowcnt As Integer
        Dim GtotalEmp As Double
        Dim gTotalAbs As Double
        Dim gTotablLeave As Double
        Dim gTotalPre As Double
        Dim gTotalWO As Double
        Dim gTotalOD As Double
        xlapp = CreateObject("Excel.Application")
        xlwb = xlapp.Workbooks.Add
        'If UCase(Dir(App.Path & "\TimeWatch.xls")) = UCase("TimeWatch.xls") Then
        '    Kill(App.Path & "\TimeWatch.xls")
        'End If

        mintPageNo = 1
        mintLine = 1
        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If
        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".xls"

        strsql = " Select tblCatagory.Catagoryname, tblDivision.DivisionName, tblTimeregister.Wo_Value,tblTimeregister.PresentValue, tblTimeregister.Shift,tblTimeregister.AbsentValue,tblTimeregister.LeaveValue,tblTimeregister.LeaveAmount,TblDepartment.DepartmentName,TblDepartment.DepartmentCode,tblEmployee.PresentCardNo " & _
                " from  tblCatagory, tblDivision,  tblTimeregister ,tblEmployee ,tblDepartment,TBLCOMPANY " & _
                " Where tblEmployee.Companycode = tblCompany.Companycode AND tblEmployee.CAT = tblCatagory.cat And tblEmployee.DivisionCode = tblDivision.DivisionCode And tblTimeregister.PayCode = TblEmployee.PayCode And TblEmployee.DepartmentCode = TblDepartment.DepartmentCode" & g_HODDepartmentCode & _
                " AND tbltimeregister.DateOffice = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' And  (tblemployee.LeavingDate>='" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' or tblemployee.LeavingDate is null)" & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & " ORDER by tblDepartment.DEPARTMENTCODE ASC"

        'Rs_Report = Cn.Execute(strsql)

        If Common.servername = "Access" Then

            strsql = " Select tblCatagory.Catagoryname, tblDivision.DivisionName, tblTimeregister.Wo_Value,tblTimeregister.PresentValue, tblTimeregister.Shift,tblTimeregister.AbsentValue,tblTimeregister.LeaveValue,tblTimeregister.LeaveAmount,TblDepartment.DepartmentName,TblDepartment.DepartmentCode,tblEmployee.PresentCardNo " & _
                    " from  tblCatagory, tblDivision,  tblTimeregister ,tblEmployee ,tblDepartment,TBLCOMPANY " & _
                    " Where tblEmployee.Companycode = tblCompany.Companycode AND tblEmployee.CAT = tblCatagory.cat And tblTimeregister.PayCode = TblEmployee.PayCode And TblEmployee.DepartmentCode = TblDepartment.DepartmentCode" & g_HODDepartmentCode & _
                    " AND FORMAT(tbltimeregister.DateOffice,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & " ORDER by tblDepartment.DEPARTMENTCODE ASC"


            adapA = New OleDbDataAdapter(strsql, Common.con1)
            adapA.Fill(Rs_Report)
        Else
            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(Rs_Report)
        End If

        If Rs_Report.Tables(0).Rows.Count < 1 Then
            'G_Report_Error = False
            'Screen.MousePointer = vbDefault
            XtraMessageBox.Show(ulf, "<size=10>No Data Available For this Report.</size>", "<size=9>iAS</size>")
            'mReportPrintStatus = False
            Exit Sub
        End If
        'Rs_Report.Sort = "DepartmentCode"
        rowcnt = 1
        xlapp.Visible = True
        'xlapp.Cells.HorizontalAlignment = xlLeft
        xlapp.Columns.ColumnWidth = 9
        xlapp.Columns.Font.Name = "TAHOMA"
        xlapp.Columns.Font.Size = 8
        xlapp.Columns.Font.ColorIndex = 1 'vbBlack

        xlapp.Cells(rowcnt, 5).Font.Bold = True
        xlapp.Cells(rowcnt, 5) = "Company Name:" & CommonReport.g_CompanyNames
        rowcnt = rowcnt + 1
        xlapp.Cells(rowcnt, 5).Font.Bold = True
        xlapp.Cells(rowcnt, 5) = "Run Date & Time :" & Now.ToString("dd/MM/yyyy HH:mm")
        rowcnt = rowcnt + 2
        xlapp.Cells.Font.Size = 8
        xlapp.Cells(rowcnt, 5).Font.Bold = True
        xlapp.Cells(rowcnt, 5) = " DEPARTMENT SUMMARY ON DATE : " & DateEdit1.DateTime.ToString("dd/MM/yyyy HH:mm")
        rowcnt = rowcnt + 1

        xlapp.Range("A5:J5").Font.Bold = True
        xlapp.Range("A5:J5").Font.ColorIndex = 2 ' vbWhite
        'xlapp.Range("A5:J5").HorizontalAlignment = xlLeft
        xlapp.Range("A5:J5").Interior.ColorIndex = 51
        xlapp.Range("A5:J5").Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 1) = "Sr.No."
        xlapp.Cells(rowcnt, 2) = "Dept Code"
        xlapp.Cells(rowcnt, 3) = "Dept Name"
        xlapp.Cells(rowcnt, 4) = " Total Employee"
        xlapp.Cells(rowcnt, 6) = "Present  "
        xlapp.Cells(rowcnt, 7) = "On Duty "
        xlapp.Cells(rowcnt, 8) = "Absent"
        xlapp.Cells(rowcnt, 9) = "Leave "
        xlapp.Cells(rowcnt, 10) = "Weekly Off "
        'rowcnt = rowcnt + 1
        mintLine = 10
        mCount = 0
        'With Rs_Report
        '    Do While Not .EOF
        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            dblPresentValue = 0
            dblAbsentValue = 0
            dblLeaveValue = 0
            dblWo_Value = 0
            lngTotalEmpCount = 0
            lngOnDutyCount = 0

            strDepartmentName = Rs_Report.Tables(0).Rows(i).Item("DepartmentName").ToString.Trim
            strDepartmentCode = Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim
            'For j As Integer = 0 To strDepartmentCode = Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim
            Do While strDepartmentCode = Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim
                dblPresentValue1 = 0
                dblAbsentValue1 = 0
                dblLeaveValue1 = 0
                dblWo_Value1 = 0
                dblLeaveAmount = 0

                If Rs_Report.Tables(0).Rows(i).Item("PRESENTVALUE").ToString.Trim <> "" Then dblPresentValue1 = Rs_Report.Tables(0).Rows(i).Item("PRESENTVALUE").ToString.Trim
                If Rs_Report.Tables(0).Rows(i).Item("ABSENTVALUE").ToString.Trim <> "" Then dblAbsentValue1 = Rs_Report.Tables(0).Rows(i).Item("ABSENTVALUE").ToString.Trim
                If Rs_Report.Tables(0).Rows(i).Item("LEAVEVALUE").ToString.Trim <> "" Then dblLeaveValue1 = Rs_Report.Tables(0).Rows(i).Item("LEAVEVALUE").ToString.Trim
                If Rs_Report.Tables(0).Rows(i).Item("WO_VALUE").ToString.Trim <> "" Then dblWo_Value1 = Rs_Report.Tables(0).Rows(i).Item("WO_VALUE").ToString.Trim
                If Rs_Report.Tables(0).Rows(i).Item("leaveamount").ToString.Trim Then dblLeaveAmount = Rs_Report.Tables(0).Rows(i).Item("leaveamount").ToString.Trim

                If dblPresentValue1 = 1 And dblLeaveAmount > 0 Then lngOnDutyCount = lngOnDutyCount + 1
                If Not (dblPresentValue1 = 1 And dblLeaveAmount > 0) Then
                    dblPresentValue = dblPresentValue + dblPresentValue1
                End If
                dblAbsentValue = dblAbsentValue + dblAbsentValue1
                dblLeaveValue = dblLeaveValue + dblLeaveValue1
                dblWo_Value = dblWo_Value + dblWo_Value1

                lngTotalEmpCount = lngTotalEmpCount + 1
                '.MoveNext()
                'If .EOF Then
                '    Exit Do
                'End If

                i = i + 1
                If i > Rs_Report.Tables(0).Rows.Count - 1 Then
                    i = i - 1
                    GoTo tmp
                    Exit Do
                End If
            Loop
            'Next
            i = i - 1
tmp:        strDepartmentName = Rs_Report.Tables(0).Rows(i).Item("DepartmentName").ToString.Trim
            strDepartmentCode = Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim

            GtotalEmp = GtotalEmp + lngTotalEmpCount
            gTotalAbs = gTotalAbs + dblAbsentValue
            gTotablLeave = gTotablLeave + dblLeaveValue
            gTotalPre = gTotalPre + dblPresentValue
            gTotalWO = gTotalWO + dblWo_Value
            gTotalOD = gTotalOD + lngOnDutyCount
            rowcnt = rowcnt + 1
            mCount = mCount + 1
            xlapp.Cells(rowcnt, 1).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 1) = mCount
            xlapp.Cells(rowcnt, 2).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 2) = strDepartmentCode
            xlapp.Cells(rowcnt, 3).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 3) = strDepartmentName
            xlapp.Cells(rowcnt, 4).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 4) = "'" & lngTotalEmpCount
            xlapp.Cells(rowcnt, 5).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 6).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 6) = "'" & dblPresentValue
            xlapp.Cells(rowcnt, 7).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 7) = "'" & lngOnDutyCount
            xlapp.Cells(rowcnt, 8).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 8) = "'" & dblAbsentValue
            xlapp.Cells(rowcnt, 9).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 9) = "'" & dblLeaveValue
            xlapp.Cells(rowcnt, 10).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 10) = "'" & dblWo_Value
            mintLine = mintLine + 1
            'Loop
        Next
        rowcnt = rowcnt + 1
        'xlapp.Cells(rowcnt, 1) = mCount
        'xlapp.Cells(rowcnt, 2) = strDepartmentCode
        xlapp.Cells(rowcnt, 3).Font.Bold = True
        xlapp.Cells(rowcnt, 3).Font.ColorIndex = 2 'vbWhite
        xlapp.Cells(rowcnt, 3).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 3).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 3) = "Total :"
        xlapp.Cells(rowcnt, 4).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 4) = "'" & GtotalEmp
        xlapp.Cells(rowcnt, 5).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 6).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 6) = "'" & (gTotalPre)
        xlapp.Cells(rowcnt, 7).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 7) = "'" & (gTotalOD)
        xlapp.Cells(rowcnt, 8).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 8) = "'" & (gTotalAbs)
        xlapp.Cells(rowcnt, 9).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 9) = "'" & (gTotablLeave)
        xlapp.Cells(rowcnt, 10).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 10) = "'" & (gTotalWO)

        'End With
        xlwb.SaveAs(mstrFile_Name)
        'xlwb.SaveAs(App.Path & "/TimeWatch.xls")
        'mblnCheckReport = True
    End Sub
    Sub SpotXl_EarlyArrival(strsortorder As String)
        Dim xlapp As Excel.Application
        Dim xlwb As Excel.Workbook
        Dim xlst As Excel.Sheets
        Dim strsql As String, mCount As Integer
        Dim v_Late As String '* 6
        Dim rowcnt As Integer
        xlapp = CreateObject("Excel.Application")
        xlwb = xlapp.Workbooks.Add
        'If UCase(Dir(App.Path & "\TimeWatch.xls")) = UCase("TimeWatch.xls") Then
        '    Kill(App.Path & "\TimeWatch.xls")
        'End If
        mstrDepartmentCode = " "
        mintPageNo = 1
        mintLine = 1
        If XtraShortOrder.g_SortOrder <> "" Then
            strsortorder = XtraShortOrder.g_SortOrder
        End If
        mblnCheckReport = False
        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If
        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".xls"


        If Common.servername = "Access" Then
            'strsql = " Select tblCatagory.Catagoryname, tblDivision.DivisionName, tblTimeregister.PAYCODE,  tblTimeregister.Shift,tblDepartment.DepartmentCode, tblDepartment.DepartmentName,tblEmployee.PresentCARDNO, tblEmployee.EMPNAME, tblTimeregister.DATEOFFICE, " & _
            '    "tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, " & _
            '    "tblTimeregister.SHIFTATTENDED, tblTimeregister.shiftStartTime, tblTimeregister.In1, tblTimeregister.Earlyarrival, " & _
            '    "IIF(tblTimeregister.Earlyarrival <= 10,'**','  ') AS Late1, " & _
            '    "IIF(tblTimeregister.Earlyarrival > 10 And tblTimeregister.Earlyarrival <= 30, '**' , '  ') AS Late10, " & _
            '    "IIF(tblTimeregister.Earlyarrival > 30 And tblTimeregister.Earlyarrival <= 60,'**' ,'  ') AS Late30, " & _
            '    "IIF(TblTimeregister.Earlyarrival > 60 , '**', '  ') AS Late60, tblEmployee.BUS " & _
            '    "From  tblCatagory, tblDivision,tblTimeregister, tblEmployee, tblDepartment, tblCompany " & _
            '    "Where tblEmployee.CompanyCode=tblCompany.CompanyCode and tblEmployee.CAT = tblCatagory.cat And tblEmployee.DivisionCode = tblDivision.DivisionCode And tblEmployee.PayCode = tblTimeregister.PayCode And " & _
            '    "tblEmployee.DepartmentCode = tblDepartment.DepartmentCode AND FORMAT(tbltimeregister.DateOffice,'MMM DD YYYY') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' AND  " & _
            '    "tblTimeregister.Earlyarrival > 0" & g_HODDepartmentCode & _
            '    " And (tblemployee.LeavingDate >=#" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "# or tblemployee.LeavingDate is null)" & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & strsortorder


            strsql = " Select tblCatagory.Catagoryname, tblDivision.DivisionName, tblTimeregister.PAYCODE,  tblTimeregister.Shift,tblDepartment.DepartmentCode, tblDepartment.DepartmentName,tblEmployee.PresentCARDNO, tblEmployee.EMPNAME, tblTimeregister.DATEOFFICE, " & _
              "tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, " & _
              "tblTimeregister.SHIFTATTENDED, tblTimeregister.shiftStartTime, tblTimeregister.In1, tblTimeregister.Earlyarrival, " & _
              "IIF(tblTimeregister.Earlyarrival <= 10,'**','  ') AS Late1, " & _
              "IIF(tblTimeregister.Earlyarrival > 10 And tblTimeregister.Earlyarrival <= 30, '**' , '  ') AS Late10, " & _
              "IIF(tblTimeregister.Earlyarrival > 30 And tblTimeregister.Earlyarrival <= 60,'**' ,'  ') AS Late30, " & _
              "IIF(TblTimeregister.Earlyarrival > 60 , '**', '  ') AS Late60, tblEmployee.BUS " & _
              "From  tblCatagory, tblDivision,tblTimeregister, tblEmployee, tblDepartment, tblCompany " & _
              "Where tblEmployee.CompanyCode=tblCompany.CompanyCode and tblEmployee.CAT = tblCatagory.cat And tblEmployee.PayCode = tblTimeregister.PayCode And " & _
              "tblEmployee.DepartmentCode = tblDepartment.DepartmentCode AND FORMAT(tbltimeregister.DateOffice,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' AND  " & _
              "tblTimeregister.Earlyarrival > 0" & g_HODDepartmentCode & _
              " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & strsortorder
            adapA = New OleDbDataAdapter(strsql, Common.con1)
            adapA.Fill(Rs_Report)
        Else
            strsql = " Select tblCatagory.Catagoryname, tblDivision.DivisionName, tblTimeregister.PAYCODE,  tblTimeregister.Shift,tblDepartment.DepartmentCode, tblDepartment.DepartmentName,tblEmployee.PresentCARDNO, tblEmployee.EMPNAME, tblTimeregister.DATEOFFICE, " & _
                    "tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, " & _
                    "tblTimeregister.SHIFTATTENDED, tblTimeregister.shiftStartTime, tblTimeregister.In1, tblTimeregister.Earlyarrival, " & _
                    "Late1 = Case When tblTimeregister.Earlyarrival <= 10 then '**' Else '  'End, " & _
                    "Late10 = Case When tblTimeregister.Earlyarrival > 10 And tblTimeregister.Earlyarrival <= 30 then '**' Else '  ' End, " & _
                    "Late30 = Case When tblTimeregister.Earlyarrival > 30 And tblTimeregister.Earlyarrival <= 60 then '**' Else '  ' End, " & _
                    "Late60 = Case When tblTimeregister.Earlyarrival > 60 then '**' Else '  ' End, tblEmployee.BUS " & _
                    "From  tblCatagory, tblDivision,tblTimeregister, tblEmployee, tblDepartment, tblCompany " & _
                    "Where tblEmployee.CompanyCode=tblCompany.CompanyCode and tblEmployee.CAT = tblCatagory.cat And tblEmployee.PayCode = tblTimeregister.PayCode And " & _
                    "tblEmployee.DepartmentCode = tblDepartment.DepartmentCode AND tbltimeregister.DateOffice = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' AND  " & _
                    "tblTimeregister.Earlyarrival > 0" & g_HODDepartmentCode & _
                    " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & strsortorder
            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(Rs_Report)
        End If
        'Rs_Report = Cn.Execute(strsql)
        If strsortorder = "" Then
            'Rs_Report.Sort = "PayCode"
        Else
            'Rs_Report.Sort = strsortorder
        End If
        If Rs_Report.Tables(0).Rows.Count < 1 Then
            'G_Report_Error = False
            'Screen.MousePointer = vbDefault
            XtraMessageBox.Show(ulf, "<size=10>Nobody has come early on this date.</size>", "<size=9>iAS</size>")
            'mReportPrintStatus = False
            Exit Sub
        End If

        rowcnt = 1
        xlapp.Visible = True
        xlapp.Columns.ColumnWidth = 9
        xlapp.Columns.Font.Name = "TAHOMA"
        xlapp.Columns.Font.Size = 8
        xlapp.Columns.Font.ColorIndex = 1 'vbBlack
        xlapp.Cells(rowcnt, 5).Font.Bold = True
        xlapp.Cells(rowcnt, 5) = "Company Name:" & CommonReport.g_CompanyNames
        rowcnt = rowcnt + 1
        xlapp.Cells(rowcnt, 5).Font.Bold = True
        xlapp.Cells(rowcnt, 5) = "Run Date & Time :" & Format(Now(), "dd/MM/yyyy HH:MM")
        rowcnt = rowcnt + 2
        xlapp.Cells(rowcnt, 4).Font.Bold = True
        xlapp.Cells(rowcnt, 4) = "EARLY ARRIVAL REPORT FOR DATE : " & DateEdit1.DateTime.ToString("dd/MM/yyyy HH:mm")
        rowcnt = rowcnt + 1

        xlapp.Range("A5:N5").Font.Bold = True
        xlapp.Range("A5:N5").Font.ColorIndex = 2 'vbWhite
        'xlapp.Range("A5:N5").HorizontalAlignment = xlLeft
        xlapp.Range("A5:N5").Interior.ColorIndex = 51
        xlapp.Range("A5:N5").Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 1) = "Sr.No."
        xlapp.Cells(rowcnt, 2) = "PayCode"
        xlapp.Cells(rowcnt, 3) = "Card No"
        xlapp.Cells(rowcnt, 4) = "Employee Name"
        xlapp.Cells(rowcnt, 6) = "Shift  "
        xlapp.Cells(rowcnt, 7) = "Shift Start "
        xlapp.Cells(rowcnt, 8) = "In"
        xlapp.Cells(rowcnt, 9) = "Shift Early "
        xlapp.Cells(rowcnt, 10) = "      ------------------------Shift Early ----------------------"
        rowcnt = rowcnt + 1
        xlapp.Cells(rowcnt, 10) = "   >(0.1)       >(0.10)     >(0.30)   >(1.00)    "
        rowcnt = rowcnt - 1
        xlapp.Cells(rowcnt, 14) = "Bus Route "
        'rowcnt = rowcnt + 1
        mCount = 0

        'With Rs_Report
        '    .MoveFirst()

        '    Do While Not .EOF
        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            If strsortorder = "Department" Or XtraShortOrder.CheckDeptName.Checked Then
                If mstrDepartmentCode <> Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim Then
                    rowcnt = rowcnt + 2
                    xlapp.Cells(rowcnt, 1) = "*** Department Code & Name " & Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim & " " & Rs_Report.Tables(0).Rows(i).Item("DepartmentName").ToString.Trim
                    mstrDepartmentCode = Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString
                    rowcnt = rowcnt + 1
                End If
            End If
            If strsortorder.Contains("Division") Or XtraShortOrder.CheckSectionPay.Checked Then
                If mstrDepartmentCode <> Rs_Report.Tables(0).Rows(i).Item("DivisionCode").ToString Then
                    rowcnt = rowcnt + 2
                    xlapp.Cells(rowcnt, 1) = "*** Section Code & Name " & Rs_Report.Tables(0).Rows(i).Item("DivisionCode").ToString.Trim & " " & Rs_Report.Tables(0).Rows(i).Item("DivisionName").ToString.Trim
                    mstrDepartmentCode = Rs_Report.Tables(0).Rows(i).Item("DivisionCode").ToString.Trim
                    rowcnt = rowcnt + 1
                End If
            End If
            If strsortorder.Contains("Cat") Or XtraShortOrder.CheckCatagoryPaycode.Checked Then
                If mstrDepartmentCode <> Rs_Report.Tables(0).Rows(i).Item("Cat").ToString.Trim Then
                    rowcnt = rowcnt + 2

                    xlapp.Cells(rowcnt, 1) = "*** Category Code & Name " & Rs_Report.Tables(0).Rows(i).Item("Cat").ToString.Trim & " " & Rs_Report.Tables(0).Rows(i).Item("CatagoryName").ToString.Trim
                    mstrDepartmentCode = Rs_Report.Tables(0).Rows(i).Item("Cat").ToString.Trim
                    rowcnt = rowcnt + 1
                End If
            End If

            If Rs_Report.Tables(0).Rows(i).Item("EARLYARRIVAL").ToString.Trim = "0" Then
                v_Late = ""
            Else
                v_Late = Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("EARLYARRIVAL").ToString.Trim / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("EARLYARRIVAL").ToString.Trim Mod 60).ToString("00")
            End If
            'v_Late = Length5(Min2Hr(!EARLYARRIVAL))

            mCount = mCount + 1
            rowcnt = rowcnt + 1
            xlapp.Cells(rowcnt, 1).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 1) = mCount
            xlapp.Cells(rowcnt, 2).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 2) = "'" & Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim
            xlapp.Cells(rowcnt, 3).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 3) = "'" & Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim
            xlapp.Cells(rowcnt, 4).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 4) = Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim
            xlapp.Cells(rowcnt, 5).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 6).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 6) = Rs_Report.Tables(0).Rows(i).Item("ShiftAttended").ToString.Trim
            xlapp.Cells(rowcnt, 7).Borders.LineStyle = XlLineStyle.xlContinuous
            If Rs_Report.Tables(0).Rows(i).Item("shiftStartTime").ToString.Trim = "" Then
                xlapp.Cells(rowcnt, 7) = ""
            Else
                xlapp.Cells(rowcnt, 7) = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("shiftStartTime").ToString.Trim).ToString("HH:mm")
            End If
            'xlapp.Cells(rowcnt, 7) = Format(!shiftStartTime, "hh:mm")
            xlapp.Cells(rowcnt, 8).Borders.LineStyle = XlLineStyle.xlContinuous
            If Rs_Report.Tables(0).Rows(i).Item("In1").ToString.Trim = "" Then
                xlapp.Cells(rowcnt, 8) = ""
            Else
                xlapp.Cells(rowcnt, 8) = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("In1").ToString.Trim).ToString("HH:mm")
            End If
            'xlapp.Cells(rowcnt, 8) = Format(!In1, "hh:mm")
            xlapp.Cells(rowcnt, 9).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 9) = "'" & (v_Late)
            xlapp.Cells(rowcnt, 10).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 11).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 12).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 13).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 14).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 14) = Rs_Report.Tables(0).Rows(i).Item("bus").ToString.Trim

            mintLine = mintLine + 1
            mblnCheckReport = True
            '        .MoveNext()
            '    Loop
        Next
        'End With

        '        xlwb.SaveAs(App.Path & "/TimeWatch.xls")

        '        g_PageNo = mintPageNo
        '        Exit Sub
        'ErrorGen:
        '        MsgBox(Err.Description, vbInformation + vbOKOnly, "Report Generation Information")
        '        Screen.MousePointer = vbDefault
        xlwb.SaveAs(mstrFile_Name)
    End Sub
    Sub SpotXl_ShiftWisePresence(strsortorder As String)
        Dim xlapp As Excel.Application
        Dim xlwb As Excel.Workbook
        Dim xlst As Excel.Sheets
        Dim strsql As String
        Dim v_Shift As String '* 5
        Dim v_Start As String '* 5
        Dim v_InTime As String '* 8
        Dim v_Late As String '* 5
        Dim v_Status As String '* 5
        Dim rowcnt As Integer
        Dim mCount As Integer
        xlapp = CreateObject("Excel.Application")
        xlwb = xlapp.Workbooks.Add
        'If UCase(Dir(App.Path & "\TimeWatch.xls")) = UCase("TimeWatch.xls") Then
        '    Kill(App.Path & "\TimeWatch.xls")
        'End If
        If XtraShortOrder.g_SortOrder <> "" Then
            strsortorder = XtraShortOrder.g_SortOrder
        End If
        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If
        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".xls"

        mstrDepartmentCode = " "
        mintPageNo = 1
        mintLine = 1
        mblnCheckReport = False
        If Common.servername = "Access" Then
            'strsql = " Select tblTimeregister.PAYCODE, tblTimeregister.Shiftattended, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, tblDepartment.DepartmentCode, tblDepartment.DepartmentName,tblEmployee.PresentCARDNO, tblEmployee.EMPNAME, tblTimeregister.DATEOFFICE, " & _
            '     " tblTimeregister.SHIFTATTENDED, tblTimeregister.ShiftStartTime, tblTimeregister.In1, tblTimeregister.LateArrival, tblTimeregister.Status, tblCatagory.Catagoryname, tblDivision.DivisionName " & _
            '     " From  tblTimeregister, tblEmployee, tblDepartment, tblCatagory, tblDivision,tblCompany " & _
            '     " Where tblEmployee.PayCode = tblTimeregister.PayCode And " & _
            '     " tblEmployee.Companycode=tblCompany.Companycode and tblEmployee.CAT = tblCatagory.cat And " & _
            '     " tblEmployee.DivisionCode = tblDivision.DivisionCode AND FORMAT(tbltimeregister.DateOffice,'MMM DD YYYY') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' And " & _
            '     " tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
            '     " And (tblemployee.LeavingDate>='" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' or tblemployee.LeavingDate is null)" & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & _
            '     " order by tblTimeregister.Shiftattended,tblEmployee.DepartmentCode,tblTimeregister.PayCode"

            strsql = " Select tblTimeregister.PAYCODE, tblTimeregister.Shiftattended, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, tblDepartment.DepartmentCode, tblDepartment.DepartmentName,tblEmployee.PresentCARDNO, tblEmployee.EMPNAME, tblTimeregister.DATEOFFICE, " & _
                " tblTimeregister.SHIFTATTENDED, tblTimeregister.ShiftStartTime, tblTimeregister.In1, tblTimeregister.LateArrival, tblTimeregister.Status, tblCatagory.Catagoryname, tblDivision.DivisionName " & _
                " From  tblTimeregister, tblEmployee, tblDepartment, tblCatagory, tblDivision,tblCompany " & _
                " Where tblEmployee.PayCode = tblTimeregister.PayCode And " & _
                " tblEmployee.Companycode=tblCompany.Companycode and tblEmployee.CAT = tblCatagory.cat And " & _
                "  FORMAT(tbltimeregister.DateOffice,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' And " & _
                " tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
                " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & _
                " order by tblTimeregister.Shiftattended,tblEmployee.DepartmentCode,tblTimeregister.PayCode"

            adapA = New OleDbDataAdapter(strsql, Common.con1)
            adapA.Fill(Rs_Report)
        Else

            strsql = " Select tblTimeregister.PAYCODE, tblTimeregister.Shiftattended, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, tblDepartment.DepartmentCode, tblDepartment.DepartmentName,tblEmployee.PresentCARDNO, tblEmployee.EMPNAME, tblTimeregister.DATEOFFICE, " & _
                     " tblTimeregister.SHIFTATTENDED, tblTimeregister.ShiftStartTime, tblTimeregister.In1, tblTimeregister.LateArrival, tblTimeregister.Status, tblCatagory.Catagoryname, tblDivision.DivisionName " & _
                     " From  tblTimeregister, tblEmployee, tblDepartment, tblCatagory, tblDivision,tblCompany " & _
                     " Where tblEmployee.PayCode = tblTimeregister.PayCode And " & _
                     " tblEmployee.Companycode=tblCompany.Companycode and tblEmployee.CAT = tblCatagory.cat And " & _
                     " tbltimeregister.DateOffice = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' And " & _
                     " tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
                     " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & _
                     " order by tblTimeregister.Shiftattended,tblEmployee.DepartmentCode,tblTimeregister.PayCode"
            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(Rs_Report)
        End If
        'Rs_Report = Cn.Execute(strsql)

        'If strsortorder = "" Then
        '    Rs_Report.Sort = "Shiftattended,DepartmentCode,PayCode"
        'Else
        '    If g_SortOrder = "Paycode" Or g_SortOrder = "DepartmentCode,Paycode" Then
        '        Rs_Report.Sort = "Shiftattended,DepartmentCode,PayCode"
        '    ElseIf g_SortOrder = "PresentCardNo" Or g_SortOrder = "DepartmentCode,PresentCardNo" Then
        '        Rs_Report.Sort = "Shiftattended,DepartmentCode,Cardno"
        '    ElseIf g_SortOrder = "EmpName" Or g_SortOrder = "DepartmentCode,EmpName" Then
        '        Rs_Report.Sort = "Shiftattended,DepartmentCode,EmployeesName,Paycode"
        '    ElseIf g_SortOrder = "DivisionCode,Paycode" Then
        '        Rs_Report.Sort = "Shiftattended,DepartmentCode,Divisioncode,PayCode"
        '    ElseIf g_SortOrder = "DivisionCode,PresentCardNo" Then
        '        Rs_Report.Sort = "Shiftattended,DepartmentCode,Divisioncode,Cardno"
        '    ElseIf g_SortOrder = "DivisionCode,EmpName" Then
        '        Rs_Report.Sort = "Shiftattended,DepartmentCode,Divisioncode,EmployeesName,PayCode"
        '    ElseIf g_SortOrder = "Cat,Paycode" Then
        '        Rs_Report.Sort = "Shiftattended,DepartmentCode,Cat,PayCode"
        '    ElseIf g_SortOrder = "Cat,PresentCardNo" Then
        '        Rs_Report.Sort = "Shiftattended,DepartmentCode,Cat,Cardno"
        '    ElseIf g_SortOrder = "Cat,EmpName" Then
        '        Rs_Report.Sort = "Shiftattended,DepartmentCode,Cat,EmployeesName,PayCode"
        '    End If
        'End If
        If Rs_Report.Tables(0).Rows.Count < 1 Then
            'G_Report_Error = False
            'Screen.MousePointer = vbDefault
            XtraMessageBox.Show(ulf, "<size=10>No Data Available For this Report.</size>", "<size=9>iAS</size>")
            'mReportPrintStatus = False
            Exit Sub
        End If
        rowcnt = 1
        xlapp.Visible = True
        xlapp.Columns.Font.Name = "TAHOMA"
        xlapp.Columns.Font.Size = 8
        xlapp.Columns.Font.ColorIndex = 1 'vbBlack

        xlapp.Cells(rowcnt, 5).Font.Bold = True
        xlapp.Cells(rowcnt, 5) = "Company Name:" & CommonReport.g_CompanyNames
        rowcnt = rowcnt + 1
        xlapp.Cells(rowcnt, 5).Font.Bold = True
        xlapp.Cells(rowcnt, 5) = "Run Date & Time :" & Now.ToString("dd/MM/yyyy HH:mm")
        rowcnt = rowcnt + 2
        xlapp.Cells(rowcnt, 4).Font.Bold = True
        xlapp.Cells(rowcnt, 4) = " SHIFT WISE PRESENCE REPORT FOR DATE : " & DateEdit1.DateTime.ToString("dd/MM/yyyy")
        rowcnt = rowcnt + 1

        xlapp.Range("A5:K5").Font.Bold = True
        xlapp.Range("A5:K5").Font.ColorIndex = 2 'vbWhite
        'xlapp.Range("A5:K5").HorizontalAlignment = xlLeft
        xlapp.Range("A5:K5").Interior.ColorIndex = 51
        xlapp.Range("A5:K5").Borders.LineStyle = XlLineStyle.xlContinuous

        xlapp.Cells(rowcnt, 2) = "Srl.No."
        xlapp.Cells(rowcnt, 3) = "PayCode"
        xlapp.Cells(rowcnt, 4) = "Card No"
        xlapp.Cells(rowcnt, 5) = "Employee Name"
        xlapp.Cells(rowcnt, 7) = "Shift  "
        xlapp.Cells(rowcnt, 8) = "Shift Start "
        xlapp.Cells(rowcnt, 9) = "    In"
        xlapp.Cells(rowcnt, 10) = "Late "
        xlapp.Cells(rowcnt, 11) = "Status"
        rowcnt = rowcnt + 1
        'With Rs_Report
        '    .MoveFirst()
        mCount = 0
        mShift = Rs_Report.Tables(0).Rows(0).Item("ShiftAttended").ToString.Trim
        mstrDepartmentCode = Rs_Report.Tables(0).Rows(0).Item("DepartmentCode").ToString.Trim
        xlapp.Cells(rowcnt, 1).Font.Bold = True
        xlapp.Cells(rowcnt, 1).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 1) = " Shift : " & mShift
        rowcnt = rowcnt + 1
        xlapp.Cells(rowcnt, 1).Font.Bold = True
        xlapp.Cells(rowcnt, 1).Borders.LineStyle = XlLineStyle.xlContinuous

        xlapp.Cells(rowcnt, 1) = "Department Code & Name " & Rs_Report.Tables(0).Rows(0).Item("DepartmentCode").ToString.Trim & " " & Rs_Report.Tables(0).Rows(0).Item("DepartmentName").ToString.Trim
        mstrDepartmentCode = Rs_Report.Tables(0).Rows(0).Item("DepartmentCode").ToString.Trim
        mShift = Rs_Report.Tables(0).Rows(0).Item("ShiftAttended").ToString.Trim
        rowcnt = rowcnt + 1
        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            'Do While Not .EOF
            If StrComp(strsortorder, "DepartmentCode", 1) <> 0 Then
                If StrComp(mShift, Rs_Report.Tables(0).Rows(i).Item("ShiftAttended").ToString.Trim, 1) = 0 Then
                    If mstrDepartmentCode = Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim Then

                        If Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim = "0" Or Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim = "" Then
                            v_Late = ""
                        Else
                            v_Late = Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim Mod 60).ToString("00")
                        End If
                        If Rs_Report.Tables(0).Rows(i).Item("In1").ToString.Trim = "" Then
                            v_InTime = ""
                        Else
                            v_InTime = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("In1").ToString.Trim).ToString("HH:mm")
                        End If
                        'v_InTime = Format(!In1, "hh:mm") & "  "
                        'v_Late = Length5(Min2Hr(!latearrival))
                        v_Status = ""
                        If Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim <> "" Then
                            If UCase(Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim) = "MIS" Then
                                v_Status = "P"
                            Else
                                v_Status = Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim
                            End If
                        End If
                        v_Shift = Rs_Report.Tables(0).Rows(i).Item("ShiftAttended").ToString.Trim & " "
                        If Rs_Report.Tables(0).Rows(i).Item("shiftStartTime").ToString.Trim = "" Then
                            v_Start = ""
                        Else
                            v_Start = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("shiftStartTime").ToString.Trim).ToString("HH:mm")
                        End If
                        'v_Start = Format(!shiftStartTime, "HH:mm") & " "
                        rowcnt = rowcnt + 1
                        mCount = mCount + 1
                        xlapp.Cells(rowcnt, 2).Borders.LineStyle = XlLineStyle.xlContinuous
                        xlapp.Cells(rowcnt, 2) = "'" & mCount
                        xlapp.Cells(rowcnt, 3).Borders.LineStyle = XlLineStyle.xlContinuous
                        xlapp.Cells(rowcnt, 3) = "'" & Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim
                        xlapp.Cells(rowcnt, 4).Borders.LineStyle = XlLineStyle.xlContinuous
                        xlapp.Cells(rowcnt, 4) = Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim
                        xlapp.Cells(rowcnt, 5).Borders.LineStyle = XlLineStyle.xlContinuous
                        xlapp.Cells(rowcnt, 6).Borders.LineStyle = XlLineStyle.xlContinuous
                        xlapp.Cells(rowcnt, 5) = Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim
                        xlapp.Cells(rowcnt, 7).Borders.LineStyle = XlLineStyle.xlContinuous
                        xlapp.Cells(rowcnt, 7) = v_Shift
                        xlapp.Cells(rowcnt, 8).Borders.LineStyle = XlLineStyle.xlContinuous
                        xlapp.Cells(rowcnt, 8) = v_Start
                        xlapp.Cells(rowcnt, 9).Borders.LineStyle = XlLineStyle.xlContinuous
                        xlapp.Cells(rowcnt, 9) = v_InTime
                        xlapp.Cells(rowcnt, 10).Borders.LineStyle = XlLineStyle.xlContinuous
                        xlapp.Cells(rowcnt, 10) = "'" & (v_Late)
                        xlapp.Cells(rowcnt, 11).Borders.LineStyle = XlLineStyle.xlContinuous
                        xlapp.Cells(rowcnt, 11) = v_Status
                        mblnCheckReport = True
                        mShift = Rs_Report.Tables(0).Rows(i).Item("ShiftAttended").ToString.Trim
                    Else
                        rowcnt = rowcnt + 1
                        xlapp.Cells(rowcnt, 1).Font.Bold = True
                        xlapp.Cells(rowcnt, 1).Borders.LineStyle = XlLineStyle.xlContinuous
                        xlapp.Cells(rowcnt, 1) = "Department Code & Name " & Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim & " " & Rs_Report.Tables(0).Rows(i).Item("DepartmentName").ToString.Trim
                        mstrDepartmentCode = Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim

                        If Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim = "0" Or Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim = "" Then
                            v_Late = ""
                        Else
                            v_Late = Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim Mod 60).ToString("00")
                        End If
                        If Rs_Report.Tables(0).Rows(i).Item("In1").ToString.Trim = "" Then
                            v_InTime = ""
                        Else
                            v_InTime = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("In1").ToString.Trim).ToString("HH:mm")
                        End If

                        'v_InTime = Format(!In1, "hh:mm") & "  "
                        'v_Late = Length5(Min2Hr(!latearrival))
                        v_Status = ""
                        If Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim <> "" Then
                            If UCase(Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim) = "MIS" Then
                                v_Status = "P"
                            Else
                                v_Status = Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim
                            End If
                        End If
                        v_Shift = Rs_Report.Tables(0).Rows(i).Item("ShiftAttended").ToString.Trim & " "
                        If Rs_Report.Tables(0).Rows(i).Item("shiftStartTime").ToString.Trim = "" Then
                            v_Start = ""
                        Else
                            v_Start = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("shiftStartTime").ToString.Trim).ToString("HH:mm")
                        End If
                        'v_Start = Format(!shiftStartTime, "hh:mm") & " "
                        rowcnt = rowcnt + 1
                        mCount = mCount + 1
                        xlapp.Cells(rowcnt, 2).Borders.LineStyle = XlLineStyle.xlContinuous
                        xlapp.Cells(rowcnt, 2) = "'" & mCount
                        xlapp.Cells(rowcnt, 3).Borders.LineStyle = XlLineStyle.xlContinuous
                        xlapp.Cells(rowcnt, 3) = "'" & Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim
                        xlapp.Cells(rowcnt, 4).Borders.LineStyle = XlLineStyle.xlContinuous
                        xlapp.Cells(rowcnt, 4) = Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim
                        xlapp.Cells(rowcnt, 5).Borders.LineStyle = XlLineStyle.xlContinuous
                        xlapp.Cells(rowcnt, 6).Borders.LineStyle = XlLineStyle.xlContinuous
                        xlapp.Cells(rowcnt, 5) = Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim
                        xlapp.Cells(rowcnt, 7).Borders.LineStyle = XlLineStyle.xlContinuous
                        xlapp.Cells(rowcnt, 7) = v_Shift
                        xlapp.Cells(rowcnt, 8).Borders.LineStyle = XlLineStyle.xlContinuous
                        xlapp.Cells(rowcnt, 8) = v_Start
                        xlapp.Cells(rowcnt, 9).Borders.LineStyle = XlLineStyle.xlContinuous
                        xlapp.Cells(rowcnt, 9) = v_InTime
                        xlapp.Cells(rowcnt, 10).Borders.LineStyle = XlLineStyle.xlContinuous
                        xlapp.Cells(rowcnt, 10) = "'" & (v_Late)
                        xlapp.Cells(rowcnt, 11).Borders.LineStyle = XlLineStyle.xlContinuous
                        xlapp.Cells(rowcnt, 11) = v_Status
                        mblnCheckReport = True
                        mShift = Rs_Report.Tables(0).Rows(i).Item("ShiftAttended").ToString.Trim
                    End If
                Else
                    rowcnt = rowcnt + 1
                    xlapp.Cells(rowcnt, 1).Font.Bold = True
                    xlapp.Cells(rowcnt, 1).Borders.LineStyle = XlLineStyle.xlContinuous
                    xlapp.Cells(rowcnt, 1) = "Shift : " & mShift
                    rowcnt = rowcnt + 1
                    xlapp.Cells(rowcnt, 1).Font.Bold = True
                    xlapp.Cells(rowcnt, 1).Borders.LineStyle = XlLineStyle.xlContinuous
                    xlapp.Cells(rowcnt, 1) = "Department Code & Name " & Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim & " " & Rs_Report.Tables(0).Rows(i).Item("DepartmentName").ToString.Trim
                    mstrDepartmentCode = Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim
                    mShift = Rs_Report.Tables(0).Rows(i).Item("ShiftAttended").ToString.Trim
                    With Rs_Report

                        If Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim = "0" Or Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim = "" Then
                            v_Late = ""
                        Else
                            v_Late = Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim Mod 60).ToString("00")
                        End If
                        If Rs_Report.Tables(0).Rows(i).Item("In1").ToString.Trim = "" Then
                            v_InTime = ""
                        Else
                            v_InTime = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("In1").ToString.Trim).ToString("HH:mm")
                        End If

                        'v_InTime = !INTIME & "  "
                        'v_Late = Length5(Min2Hr(!LATE))
                        v_Status = ""
                        If Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim <> "" Then
                            If UCase(Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim) = "MIS" Then
                                v_Status = "P"
                            Else
                                v_Status = Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim
                            End If
                        End If
                        v_Shift = Rs_Report.Tables(0).Rows(i).Item("ShiftAttended").ToString.Trim & " "
                        If Rs_Report.Tables(0).Rows(i).Item("shiftStartTime").ToString.Trim = "" Then
                            v_Start = ""
                        Else
                            v_Start = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("shiftStartTime").ToString.Trim).ToString("HH:mm")
                        End If

                        rowcnt = rowcnt + 1
                        mCount = mCount + 1
                        xlapp.Cells(rowcnt, 2).Borders.LineStyle = XlLineStyle.xlContinuous
                        xlapp.Cells(rowcnt, 2) = "'" & mCount
                        xlapp.Cells(rowcnt, 3).Borders.LineStyle = XlLineStyle.xlContinuous
                        xlapp.Cells(rowcnt, 3) = "'" & Rs_Report.Tables(0).Rows(i).Item("Paycode").ToString.Trim
                        xlapp.Cells(rowcnt, 4).Borders.LineStyle = XlLineStyle.xlContinuous
                        xlapp.Cells(rowcnt, 4) = Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim
                        xlapp.Cells(rowcnt, 5).Borders.LineStyle = XlLineStyle.xlContinuous
                        xlapp.Cells(rowcnt, 5) = Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim
                        xlapp.Cells(rowcnt, 6).Borders.LineStyle = XlLineStyle.xlContinuous
                        xlapp.Cells(rowcnt, 7).Borders.LineStyle = XlLineStyle.xlContinuous
                        xlapp.Cells(rowcnt, 7) = v_Shift
                        xlapp.Cells(rowcnt, 8).Borders.LineStyle = XlLineStyle.xlContinuous
                        xlapp.Cells(rowcnt, 8) = v_Start
                        xlapp.Cells(rowcnt, 9).Borders.LineStyle = XlLineStyle.xlContinuous
                        xlapp.Cells(rowcnt, 9) = v_InTime
                        xlapp.Cells(rowcnt, 10).Borders.LineStyle = XlLineStyle.xlContinuous
                        xlapp.Cells(rowcnt, 10) = "'" & (v_Late)
                        xlapp.Cells(rowcnt, 11).Borders.LineStyle = XlLineStyle.xlContinuous
                        'xlapp.Cells(rowcnt, 10) = "'" & String((5 - Len(Trim(v_Late))) * 1.8, " ") & Trim(v_Late)
                        xlapp.Cells(rowcnt, 11).Borders.LineStyle = XlLineStyle.xlContinuous
                        xlapp.Cells(rowcnt, 11) = v_Status
                        mblnCheckReport = True
                        mShift = Rs_Report.Tables(0).Rows(i).Item("ShiftAttended").ToString.Trim
                    End With
                End If
            End If
            mintLine = mintLine + 1
            '.MoveNext()
            'Loop
        Next
        'End With
        '        xlwb.SaveAs(App.Path & "/TimeWatch.xls")
        '        g_PageNo = mintPageNo
        '        Exit Sub
        'ErrorGen:
        '        MsgBox(Err.Description, vbInformation + vbOKOnly, "Report Generation Information")
        '        Screen.MousePointer = vbDefault
        xlwb.SaveAs(mstrFile_Name)
    End Sub
    Sub SpotXl_MachineRawPunchData(strsortorder As String)
        Dim strsql As String, mCount As Integer
        Dim mPunch1 As String ' * 9
        Dim mPunch2 As String '* 9
        Dim mPunch3 As String '* 9
        Dim mPunch4 As String '* 9
        Dim mCtr As Integer
        Dim mDate As Date
        Dim xlapp As Excel.Application
        Dim xlwb As Excel.Workbook
        Dim xlst As Excel.Sheets
        Dim rowcnt As Integer
        xlapp = CreateObject("Excel.Application")
        xlwb = xlapp.Workbooks.Add
        'If UCase(Dir(App.Path & "\TimeWatch.xls")) = UCase("TimeWatch.xls") Then
        '    Kill(App.Path & "\TimeWatch.xls")
        'End If

        Dim colcnt As Integer
        Dim mPaycode As String
        Dim mEmpName As String
        Dim mCARDNO As String
        Dim mColCnt As Integer
        mstrDepartmentCode = " "
        mblnCheckReport = False
        mintPageNo = 1
        mintLine = 1
        If XtraShortOrder.g_SortOrder <> "" Then
            strsortorder = XtraShortOrder.g_SortOrder
        End If
        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If
        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".xls"

        'Call CreateTableMachine
        'MachinePunch frmDailyAttReport.txtFromDate, frmDailyAttReport.TxtToDate ' Creating the data for the generation of the Spot reports.

        'strsql = " Select tblCatagory.Catagoryname, tblDivision.DivisionName, tblEmployee.PresentCardNo, " & _
        '" mMachineRawPunch.PAYCODE, mMachineRawPunch.CARDNO, " & _
        '" mMachineRawPunch.EMPNAME, mMachineRawPunch.PUNCH1, mMachineRawPunch.PUNCH2, " & _
        '" mMachineRawPunch.PUNCH3, mMachineRawPunch.PUNCH4, mMachineRawPunch.DATEOFFICE, " & _
        '" mMachineRawPunch.PUNCH1M, mMachineRawPunch.PUNCH2M, mMachineRawPunch.PUNCH3M, " & _
        '" mMachineRawPunch.PUNCH4M, tblDepartment.DepartmentCode, tblDepartment.DepartmentName, tblEmployee.Divisioncode, " & _
        '" tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, tblEmployee.DepartmentCode " & _
        '"  From tblCatagory, tblDivision, " & g_MachineRawPunch & " mMachineRawPunch, tblDepartment, tblEmployee " & _
        '" Where tblEmployee.CAT = tblCatagory.cat And tblEmployee.DivisionCode = tblDivision.DivisionCode And mMachineRawPunch.DepartmentCode = tblDepartment.DepartmentCode AND  " & _
        '" mMachineRawPunch.Paycode = tblEmployee.Paycode AND " & _
        '" mMachineRawPunch.CompanyCode = tblEmployee.CompanyCode" & g_HODDepartmentCode & _
        '" And (tblemployee.LeavingDate>='" & Format(frmDailyAttReport.txtFromDate, "MMM dd yyyy") & "' or tblemployee.LeavingDate is null)" & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause)

        If Common.servername = "Access" Then
            'strsql = " Select tblCatagory.Catagoryname, machinerawpunch.officepunch,Machinerawpunch.officepunch as mdate, machinerawpunch.inout, tblDivision.DivisionName, tblEmployee.PresentCardNo, " & _
            '           " tblemployee.PAYCODE,tblemployee.empname, Machinerawpunch.CARDNO, machinerawpunch.ismanual," & _
            '           " tblDepartment.DepartmentCode, tblDepartment.DepartmentName, tblEmployee.Divisioncode, " & _
            '           " tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, tblEmployee.DepartmentCode " & _
            '           "  From tblCatagory, tblDivision, MachineRawPunch, tblDepartment, tblEmployee,TBLCOMPANY " & _
            '           " Where OfficePunch between #" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & " 00:00# And " & _
            '           " #" & DateEdit2.DateTime.ToString("yyyy-MM-dd") & " 23:59# and " & _
            '           " TBLEMPLOYEE.COMPANYCODE=TBLCOMPANY.COMPANYCODE AND tblEmployee.CAT = tblCatagory.cat And tblEmployee.DivisionCode = tblDivision.DivisionCode And tblemployee.DepartmentCode = tblDepartment.DepartmentCode AND  " & _
            '           " MachineRawPunch.Paycode = tblEmployee.Paycode AND " & _
            '           " " & g_HODDepartmentCode & _
            '           " (tblemployee.LeavingDate >=#" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "# or tblemployee.LeavingDate is null)" & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & "order by tblEmployee.PresentCARDNO,  MachineRawPunch.OFFICEPUNCH"

            strsql = " Select tblCatagory.Catagoryname, machinerawpunch.officepunch,Machinerawpunch.officepunch as mdate, machinerawpunch.inout, tblDivision.DivisionName, tblEmployee.PresentCardNo, " & _
                       " tblemployee.PAYCODE,tblemployee.empname, Machinerawpunch.CARDNO, machinerawpunch.ismanual," & _
                       " tblDepartment.DepartmentCode, tblDepartment.DepartmentName, tblEmployee.Divisioncode, " & _
                       " tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, tblEmployee.DepartmentCode " & _
                       "  From tblCatagory, tblDivision, MachineRawPunch, tblDepartment, tblEmployee,TBLCOMPANY " & _
                       " Where OfficePunch between #" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & " 00:00# And " & _
                       " #" & DateEdit2.DateTime.ToString("yyyy-MM-dd") & " 23:59# and " & _
                       " TBLEMPLOYEE.COMPANYCODE=TBLCOMPANY.COMPANYCODE AND tblEmployee.CAT = tblCatagory.cat And tblemployee.DepartmentCode = tblDepartment.DepartmentCode AND  " & _
                       " MachineRawPunch.Paycode = tblEmployee.Paycode " & _
                       " " & g_HODDepartmentCode & _
                       " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & "order by tblEmployee.PresentCARDNO,  MachineRawPunch.OFFICEPUNCH"


            adapA = New OleDbDataAdapter(strsql, Common.con1)
            adapA.Fill(Rs_Report)
        Else
            'strsql = " Select tblCatagory.Catagoryname, machinerawpunch.officepunch,Machinerawpunch.officepunch as mdate, machinerawpunch.inout, tblDivision.DivisionName, tblEmployee.PresentCardNo, " & _
            '           " tblemployee.PAYCODE,tblemployee.empname, Machinerawpunch.CARDNO, machinerawpunch.ismanual," & _
            '           " tblDepartment.DepartmentCode, tblDepartment.DepartmentName, tblEmployee.Divisioncode, " & _
            '           " tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, tblEmployee.DepartmentCode " & _
            '           "  From tblCatagory, tblDivision, MachineRawPunch, tblDepartment, tblEmployee,TBLCOMPANY " & _
            '           " Where OfficePunch Between '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & " 00:00' And " & _
            '           " '" & DateEdit2.DateTime.ToString("yyyy-MM-dd") & " 23:59' and " & _
            '           " TBLEMPLOYEE.COMPANYCODE=TBLCOMPANY.COMPANYCODE AND tblEmployee.CAT = tblCatagory.cat And tblemployee.DepartmentCode = tblDepartment.DepartmentCode AND  " & _
            '           " MachineRawPunch.Paycode = tblEmployee.Paycode AND " & _
            '           " " & g_HODDepartmentCode & _
            '           " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & "order by tblEmployee.PresentCARDNO, MachineRawPunch.OFFICEPUNCH"

            strsql = " Select tblCatagory.Catagoryname, machinerawpunch.officepunch,Machinerawpunch.officepunch as mdate, machinerawpunch.inout, tblDivision.DivisionName, tblEmployee.PresentCardNo, " & _
                          " tblemployee.PAYCODE,tblemployee.empname, Machinerawpunch.CARDNO, machinerawpunch.ismanual," & _
                          " tblDepartment.DepartmentCode, tblDepartment.DepartmentName, tblEmployee.Divisioncode, " & _
                          " tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, tblEmployee.DepartmentCode " & _
                          "  From tblCatagory, tblDivision, MachineRawPunch, tblDepartment, tblEmployee,TBLCOMPANY " & _
                          " Where OfficePunch Between '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & " 00:00' And " & _
                          " '" & DateEdit2.DateTime.ToString("yyyy-MM-dd") & " 23:59' and " & _
                          " TBLEMPLOYEE.COMPANYCODE=TBLCOMPANY.COMPANYCODE AND tblEmployee.CAT = tblCatagory.cat And tblemployee.DepartmentCode = tblDepartment.DepartmentCode AND  " & _
                          " MachineRawPunch.Paycode = tblEmployee.Paycode " & _
                          " " & g_HODDepartmentCode & _
                          " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & "order by tblEmployee.PresentCARDNO,  MachineRawPunch.OFFICEPUNCH"

            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(Rs_Report)
        End If
        'Rs_Report = Cn.Execute(strsql)
        ' Call DropTableMachine

        'If strsortorder = "" Then
        '    Rs_Report.Sort = "PayCode,OFFICEPUNCH"
        'Else
        '    Rs_Report.Sort = strsortorder & ",officepunch"
        'End If

        If Rs_Report.Tables(0).Rows.Count < 1 Then
            'G_Report_Error = False
            'Screen.MousePointer = vbDefault
            XtraMessageBox.Show(ulf, "<size=10>No Data Present For this Report.</size>", "<size=9>iAS</size>")
            'mReportPrintStatus = False
            Exit Sub
        End If
        rowcnt = 1
        xlapp.Visible = True
        xlapp.Columns.Font.Name = "TAHOMA"
        xlapp.Columns.Font.Size = 8
        xlapp.Columns.Font.ColorIndex = 1 'vbBlack
        xlapp.Columns.ColumnWidth = 9
        xlapp.Cells(rowcnt, 4).Font.Bold = True
        xlapp.Cells(rowcnt, 4) = "Company Name:" & CommonReport.g_CompanyNames
        rowcnt = rowcnt + 1
        xlapp.Cells(rowcnt, 4).Font.Bold = True
        xlapp.Cells(rowcnt, 4) = "Run Date & Time :" & Now.ToString("dd/MM/yyyy HH:dd")
        rowcnt = rowcnt + 2
        xlapp.Cells(rowcnt, 4).Font.Bold = True
        xlapp.Cells(rowcnt, 4) = "MACHINE  RAW  PUNCH  FOR  DATE : " & DateEdit1.DateTime.ToString("dd/MM/yyyy")
        rowcnt = rowcnt + 1
        rowcnt = rowcnt + 1


        xlapp.Range("A6:M6").Font.Bold = True
        xlapp.Range("A6:M6").Font.ColorIndex = 2 'vbWhite
        'xlapp.Range("A6:M6").HorizontalAlignment = xlLeft
        xlapp.Range("A6:M6").Interior.ColorIndex = 51
        xlapp.Range("A6:M6").Borders.LineStyle = XlLineStyle.xlContinuous

        xlapp.Cells(rowcnt, 1) = "Sr.No."
        xlapp.Cells(rowcnt, 2) = "PayCode"
        xlapp.Cells(rowcnt, 3) = "Card No"
        xlapp.Cells(rowcnt, 4) = "Employee Name"
        xlapp.Cells(rowcnt, 5) = "Date"
        xlapp.Cells(rowcnt, 6) = "Punch1"
        xlapp.Cells(rowcnt, 7) = "Is Manual"
        xlapp.Cells(rowcnt, 8) = "Punch2"
        xlapp.Cells(rowcnt, 9) = "Is Manual"
        xlapp.Cells(rowcnt, 10) = "Punch3"
        xlapp.Cells(rowcnt, 11) = "Is Manual"
        xlapp.Cells(rowcnt, 12) = "Punch4"
        xlapp.Cells(rowcnt, 13) = "Is Manual"

        '-----------------------Punch----------------------->  "
        rowcnt = rowcnt + 1
        'xlapp.Cells(rowcnt, 6) = " 1                  2                3                4 "
        mCount = 0
        'With Rs_Report
        '    .MoveFirst()
        '    Do While Not .EOF
        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            If strsortorder = "Department" Or XtraShortOrder.CheckDeptName.Checked Then
                If mstrDepartmentCode <> Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim Then
                    rowcnt = rowcnt + 2
                    xlapp.Cells(rowcnt, 1) = "*** Department Code & Name " & Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim & " " & Rs_Report.Tables(0).Rows(i).Item("DepartmentName").ToString.Trim
                    mstrDepartmentCode = Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString
                    rowcnt = rowcnt + 1
                End If
            End If
            If strsortorder.Contains("Division") Or XtraShortOrder.CheckSectionPay.Checked Then
                If mstrDepartmentCode <> Rs_Report.Tables(0).Rows(i).Item("DivisionCode").ToString Then
                    rowcnt = rowcnt + 2
                    xlapp.Cells(rowcnt, 1) = "*** Section Code & Name " & Rs_Report.Tables(0).Rows(i).Item("DivisionCode").ToString.Trim & " " & Rs_Report.Tables(0).Rows(i).Item("DivisionName").ToString.Trim
                    mstrDepartmentCode = Rs_Report.Tables(0).Rows(i).Item("DivisionCode").ToString.Trim
                    rowcnt = rowcnt + 1
                End If
            End If
            If strsortorder.Contains("Cat") Or XtraShortOrder.CheckCatagoryPaycode.Checked Then
                If mstrDepartmentCode <> Rs_Report.Tables(0).Rows(i).Item("Cat").ToString.Trim Then
                    rowcnt = rowcnt + 2

                    xlapp.Cells(rowcnt, 1) = "*** Category Code & Name " & Rs_Report.Tables(0).Rows(i).Item("Cat").ToString.Trim & " " & Rs_Report.Tables(0).Rows(i).Item("CatagoryName").ToString.Trim
                    mstrDepartmentCode = Rs_Report.Tables(0).Rows(i).Item("Cat").ToString.Trim
                    rowcnt = rowcnt + 1
                End If
            End If

            mPaycode = Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim
            mCARDNO = Rs_Report.Tables(0).Rows(i).Item("CardNo").ToString.Trim
            mEmpName = Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim
            mDate = "00:00:00"
            mCtr = 0
            mCount = mCount + 1
            Do While mPaycode = Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim
                If mDate.ToString("yyyy/MM/dd") <> Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("officepunch").ToString.Trim).ToString("yyyy/MM/dd") Or mCtr > 3 Then
                    rowcnt = rowcnt + 1
                    If mDate = "00:00:00" Then
                        xlapp.Cells(rowcnt, 1).Borders.LineStyle = XlLineStyle.xlContinuous
                        xlapp.Cells(rowcnt, 1) = mCount
                        xlapp.Cells(rowcnt, 2).Borders.LineStyle = XlLineStyle.xlContinuous
                        xlapp.Cells(rowcnt, 2) = "'" & mPaycode
                        xlapp.Cells(rowcnt, 3).Borders.LineStyle = XlLineStyle.xlContinuous
                        xlapp.Cells(rowcnt, 3) = "'" & mCARDNO
                        xlapp.Cells(rowcnt, 4).Borders.LineStyle = XlLineStyle.xlContinuous
                        xlapp.Cells(rowcnt, 4) = "'" & mEmpName
                    End If
                    mDate = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("officepunch").ToString.Trim).ToString("dd/MM/yyyy")
                    If mCtr <= 4 Then
                        xlapp.Cells(rowcnt, 5).Borders.LineStyle = XlLineStyle.xlContinuous
                        xlapp.Cells(rowcnt, 5) = Format(mDate, "dd-MMM-yyyy")
                    End If
                    mCtr = 0
                    mColCnt = 5
                End If
                mColCnt = mColCnt + 1
                xlapp.Cells(rowcnt, mColCnt).Borders.LineStyle = XlLineStyle.xlContinuous
                xlapp.Cells(rowcnt, mColCnt) = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("officepunch").ToString.Trim).ToString("HH:mm")  'mEmpname
                If Rs_Report.Tables(0).Rows(i).Item("IsManual").ToString.Trim = "Y" Then
                    mColCnt = mColCnt + 1
                    xlapp.Cells(rowcnt, mColCnt).Borders.LineStyle = XlLineStyle.xlContinuous
                    xlapp.Cells(rowcnt, mColCnt) = "Y" ' Format(!officepunch, "hh:mm") 'mEmpname
                Else
                    mColCnt = mColCnt + 1
                    xlapp.Cells(rowcnt, mColCnt).Borders.LineStyle = XlLineStyle.xlContinuous
                    xlapp.Cells(rowcnt, mColCnt) = "N" ' Format(!officepunch, "hh:mm") 'mEmpname
                End If
                mCtr = mCtr + 1

                i = i + 1
                If i > Rs_Report.Tables(0).Rows.Count - 1 Then
                    'i = i - 1
                    'GoTo tmp
                    Exit Do
                End If
                '.MoveNext()
                'If .EOF Then Exit Do
            Loop
            i = i - 1

            rowcnt = rowcnt + 1
            mintLine = mintLine + 1
            mblnCheckReport = True
            'If .EOF Then Exit Do
            '.MoveNext
            'Loop
        Next
        'End With
        '        xlwb.SaveAs(App.Path & "/TimeWatch.xls")
        '        g_PageNo = mintPageNo
        '        Exit Sub
        'ErrorGen:
        '        MsgBox(Err.Description, vbInformation + vbOKOnly, "Report Generation Error")
        '        Screen.MousePointer = vbDefault
        xlwb.SaveAs(mstrFile_Name)
    End Sub
    Sub SpotXl_Present(strsortorder As String)
        Dim strsql As String
        Dim v_Late As String '* 6
        Dim v_InTime As String '* 6
        Dim strStatus As String, mCount As Integer
        Dim xlapp As Excel.Application
        Dim xlwb As Excel.Workbook
        Dim xlst As Excel.Sheets
        Dim rowcnt As Integer
        xlapp = CreateObject("Excel.Application")
        xlwb = xlapp.Workbooks.Add
        'If UCase(Dir(App.Path & "\TimeWatch.xls")) = UCase("TimeWatch.xls") Then
        '    Kill(App.Path & "\TimeWatch.xls")
        'End If
        mstrDepartmentCode = " "
        mintPageNo = 1
        mintLine = 1
        If XtraShortOrder.g_SortOrder <> "" Then
            strsortorder = XtraShortOrder.g_SortOrder
        End If
        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If
        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".xls"

        mblnCheckReport = False
        If Common.servername = "Access" Then
            'strsql = " Select tblTimeregister.PAYCODE, tblTimeregister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, tblDepartment.DepartmentCode, tblDepartment.DepartmentName,tblEmployee.PresentCARDNO, tblEmployee.EMPNAME, tblTimeregister.DATEOFFICE, " & _
            '     " tblTimeregister.SHIFTATTENDED, tblTimeregister.ShiftStartTime, tblTimeregister.In1, tblTimeregister.LateArrival, tblTimeregister.Status, tblCatagory.Catagoryname, tblDivision.DivisionName " & _
            '     " From  tblTimeregister, tblEmployee, tblDepartment, tblCatagory, tblDivision,tblCompany " & _
            '     " Where tblEmployee.PayCode = tblTimeregister.PayCode And tbltimeregister.in1 is not null  and " & _
            '     " tblEmployee.Companycode=tblCompany.Companycode and tblEmployee.CAT = tblCatagory.cat And " & _
            '     " tblEmployee.DivisionCode = tblDivision.DivisionCode AND FORMAT(tbltimeregister.DateOffice,'MMM dd yyyy') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' And " & _
            '     " tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
            '     " And (tblemployee.LeavingDate >=#" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "# or tblemployee.LeavingDate is null)" & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & strsortorder

            strsql = " Select tblTimeregister.PAYCODE, tblTimeregister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, tblDepartment.DepartmentCode, tblDepartment.DepartmentName,tblEmployee.PresentCARDNO, tblEmployee.EMPNAME, tblTimeregister.DATEOFFICE, " & _
                " tblTimeregister.SHIFTATTENDED, tblTimeregister.ShiftStartTime, tblTimeregister.In1, tblTimeregister.LateArrival, tblTimeregister.Status, tblCatagory.Catagoryname, tblDivision.DivisionName " & _
                " From  tblTimeregister, tblEmployee, tblDepartment, tblCatagory, tblDivision,tblCompany " & _
                " Where tblEmployee.PayCode = tblTimeregister.PayCode And tbltimeregister.in1 is not null  and " & _
                " tblEmployee.Companycode=tblCompany.Companycode and tblEmployee.CAT = tblCatagory.cat And " & _
                " FORMAT(tbltimeregister.DateOffice,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' And " & _
                " tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
                " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & strsortorder

            adapA = New OleDbDataAdapter(strsql, Common.con1)
            adapA.Fill(Rs_Report)
        Else
            strsql = " Select tblTimeregister.PAYCODE, tblTimeregister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, tblDepartment.DepartmentCode, tblDepartment.DepartmentName,tblEmployee.PresentCARDNO, tblEmployee.EMPNAME, tblTimeregister.DATEOFFICE, " & _
                 " tblTimeregister.SHIFTATTENDED, tblTimeregister.ShiftStartTime, tblTimeregister.In1, tblTimeregister.LateArrival, tblTimeregister.Status, tblCatagory.Catagoryname, tblDivision.DivisionName " & _
                 " From  tblTimeregister, tblEmployee, tblDepartment, tblCatagory, tblDivision,tblCompany " & _
                 " Where tblEmployee.PayCode = tblTimeregister.PayCode And tbltimeregister.in1 is not null  and " & _
                 " tblEmployee.Companycode=tblCompany.Companycode and tblEmployee.CAT = tblCatagory.cat And " & _
                 " tbltimeregister.DateOffice = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' And " & _
                 " tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
                 " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & strsortorder
            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(Rs_Report)
        End If
        'Rs_Report = Cn.Execute(strsql)
        If strsortorder = "" Then
            'Rs_Report.Sort = "PayCode"
        Else
            'Rs_Report.Sort = strsortorder
        End If
        If Rs_Report.Tables(0).Rows.Count < 1 Then
            MsgBox(ulf, "<size=10>No Data Available For this Report.</size>", "<size=9>iAS</size>")
            Exit Sub
        End If
        rowcnt = 1
        xlapp.Visible = True
        xlapp.Columns.Font.Name = "TAHOMA"
        xlapp.Columns.Font.Size = 8
        xlapp.Columns.Font.ColorIndex = 1 ' vbBlack
        xlapp.Cells(rowcnt, 5).Font.Bold = True
        xlapp.Cells(rowcnt, 5) = "Company Name:" & CommonReport.g_CompanyNames
        rowcnt = rowcnt + 1
        xlapp.Cells(rowcnt, 5).Font.Bold = True
        xlapp.Cells(rowcnt, 5) = "Run Date & Time :" & Now.ToString("dd/MM/yyyy HH:MM")
        rowcnt = rowcnt + 2
        xlapp.Cells(rowcnt, 4).Font.Bold = True
        xlapp.Cells(rowcnt, 4) = "Present Report For DATE : " & DateEdit1.DateTime.ToString("dd/MM/yyyy HH:mm")
        rowcnt = rowcnt + 1
        rowcnt = rowcnt + 1


        xlapp.Range("A6:J6").Font.Bold = True
        xlapp.Range("A6:J6").Font.ColorIndex = 2 'vbWhite
        'xlapp.Range("A6:J6").HorizontalAlignment = xlLeft
        xlapp.Range("A6:J6").Interior.ColorIndex = 51
        xlapp.Range("A6:J6").Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 1) = "Sr.No."
        xlapp.Cells(rowcnt, 2) = "PayCode"
        xlapp.Cells(rowcnt, 3) = "Card No"
        xlapp.Cells(rowcnt, 4) = "Employee Name"
        xlapp.Cells(rowcnt, 6) = "Shift  "
        xlapp.Cells(rowcnt, 7) = "Start "
        xlapp.Cells(rowcnt, 8) = "   In "
        xlapp.Cells(rowcnt, 9) = "Shift Late  "
        xlapp.Cells(rowcnt, 10) = "Status "
        'rowcnt = rowcnt + 1
        mCount = 0
        'With Rs_Report
        '    .MoveFirst()
        '    Do While Not .EOF
        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            If strsortorder = "Department" Or XtraShortOrder.CheckDeptName.Checked Then
                If mstrDepartmentCode <> Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim Then
                    rowcnt = rowcnt + 2
                    xlapp.Cells(rowcnt, 1) = "*** Department Code & Name " & Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim & " " & Rs_Report.Tables(0).Rows(i).Item("DepartmentName").ToString.Trim
                    mstrDepartmentCode = Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString
                    rowcnt = rowcnt + 1
                End If
            End If
            If strsortorder.Contains("Division") Or XtraShortOrder.CheckSectionPay.Checked Then
                If mstrDepartmentCode <> Rs_Report.Tables(0).Rows(i).Item("DivisionCode").ToString Then
                    rowcnt = rowcnt + 2
                    xlapp.Cells(rowcnt, 1) = "*** Section Code & Name " & Rs_Report.Tables(0).Rows(i).Item("DivisionCode").ToString.Trim & " " & Rs_Report.Tables(0).Rows(i).Item("DivisionName").ToString.Trim
                    mstrDepartmentCode = Rs_Report.Tables(0).Rows(i).Item("DivisionCode").ToString.Trim
                    rowcnt = rowcnt + 1
                End If
            End If
            If strsortorder.Contains("Cat") Or XtraShortOrder.CheckCatagoryPaycode.Checked Then
                If mstrDepartmentCode <> Rs_Report.Tables(0).Rows(i).Item("Cat").ToString.Trim Then
                    rowcnt = rowcnt + 2

                    xlapp.Cells(rowcnt, 1) = "*** Category Code & Name " & Rs_Report.Tables(0).Rows(i).Item("Cat").ToString.Trim & " " & Rs_Report.Tables(0).Rows(i).Item("CatagoryName").ToString.Trim
                    mstrDepartmentCode = Rs_Report.Tables(0).Rows(i).Item("Cat").ToString.Trim
                    rowcnt = rowcnt + 1
                End If
            End If


            If Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim = "0" Then
                v_Late = ""
            Else
                v_Late = Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim Mod 60).ToString("00")
            End If
            If Rs_Report.Tables(0).Rows(i).Item("In1").ToString.Trim = "" Then
                v_InTime = " "
            Else
                v_InTime = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("In1").ToString.Trim).ToString("HH:mm")
            End If

            'v_InTime = Format(!In1, "hh:mm") & "  "
            'v_Late = Length5(Min2Hr(!latearrival))
            strStatus = ""
            If Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim <> "" Then
                If UCase(Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim) = "MIS" Then
                    strStatus = "P"
                Else
                    strStatus = Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim
                End If
            End If
            rowcnt = rowcnt + 1
            mCount = mCount + 1
            xlapp.Cells(rowcnt, 1).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 1) = mCount
            xlapp.Cells(rowcnt, 2).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 2) = "'" & Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim
            xlapp.Cells(rowcnt, 3).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 3) = "'" & Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim
            xlapp.Cells(rowcnt, 4).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 4) = Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim
            xlapp.Cells(rowcnt, 5).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 6).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 6) = Rs_Report.Tables(0).Rows(i).Item("ShiftAttended").ToString.Trim
            xlapp.Cells(rowcnt, 7).Borders.LineStyle = XlLineStyle.xlContinuous
            If Rs_Report.Tables(0).Rows(i).Item("shiftStartTime").ToString.Trim = "" Then
                xlapp.Cells(rowcnt, 7) = ""
            Else
                xlapp.Cells(rowcnt, 7) = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("shiftStartTime").ToString.Trim).ToString("HH:mm")
            End If
            'xlapp.Cells(rowcnt, 7) = Format(!shiftStartTime, "hh:mm")
            xlapp.Cells(rowcnt, 8).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 8) = v_InTime
            xlapp.Cells(rowcnt, 9).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 9) = "'" & (v_Late)
            xlapp.Cells(rowcnt, 10).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 10) = strStatus
            mintLine = 1
            '.MoveNext()
            'Loop
        Next
        'End With
        'xlwb.SaveAs(App.Path & "/TimeWatch.xls")

        'Screen.MousePointer = vbDefault
        'g_PageNo = mintPageNo
        xlwb.SaveAs(mstrFile_Name)
    End Sub
    Sub DailyXl_Performance(ByVal strsortorder As String)
        'On Error GoTo ErrorGen
        Dim intFile As Integer
        Dim strsql As String, mCount As Integer
        Dim strOsDuration6 As String ' * 6
        Dim strOtAmount11 As String '* 11
        Dim intPrintLineCounter As Integer
        Dim strIn1Mannual_1 As String '* 1
        Dim strOut1Mannual_1 As String '* 1
        Dim strIn2Mannual_1 As String '* 1
        Dim strOut2Mannual_1 As String '* 1
        Dim blnDeptAvailable As Boolean
        Dim strDeptDivCode As String
        Dim strCurrentDeptDivCode As String
        Dim xlapp As Excel.Application
        Dim xlwb As Excel.Workbook
        Dim xlst As Excel.Sheets

        Dim rowcnt As Integer
        xlapp = CreateObject("Excel.Application")
        xlwb = xlapp.Workbooks.Add
        'xlwb.ActiveSheet("Sheet1").PageSetup.PaperSize = System.Drawing.Printing.PaperKind.A4

        'If UCase(Dir(App.Path & "\TimeWatch.xls")) = UCase("TimeWatch.xls") Then
        '    Kill(App.Path & "\TimeWatch.xls")
        'End If
        If XtraShortOrder.g_SortOrder <> "" Then
            strsortorder = XtraShortOrder.g_SortOrder
        End If
        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If
        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".xls"

        'If Left(strsortorder, 1) = "D" Or Left(strsortorder, 1) = "C" Then blnDeptAvailable = True
        mblnCheckReport = False
        mintPageNo = 1
        mintLine = 1
        'If g_Report_view Then
        If Common.servername = "Access" Then
            'strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName,tblTimeRegister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tbltimeregister.ShiftAttended,tbltimeregister.ShiftStartTime,tbltimeregister.In1,tbltimeregister.Out1,tbltimeregister.In2,tbltimeregister.Out2,tbltimeregister.HoursWorked,tbltimeregister.Status,tbltimeregister.EarlyArrival," & _
            ' " tbltimeregister.LateArrival,tbltimeregister.EarlyDeparture,tbltimeregister.ExcLunchHours,tbltimeregister.OtDuration,tbltimeregister.OtAmount,tbltimeregister.OsDuration,tbltimeregister.In1Mannual,tbltimeregister.In2Mannual,tbltimeregister.Out1Mannual,tbltimeregister.Out2Mannual,tblCalander.mDate,tblCalander.Process,tblCalander.NRTCProc" & _
            ' " from tblCatagory,tblDivision,tbltimeregister,tblEmployee,tblCalander,tblCompany,tblDepartment" & _
            ' " Where tblCatagory.Cat = tblEmployee.Cat And tblDivision.DivisionCode = tblEmployee.DivisionCode And tbltimeregister.PayCode = tblEmployee.PayCode And FORMAT(tbltimeregister.DateOffice,'MMM DD YYYY') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' And FORMAT(tblCalander.mDate,'MMM DD YYYY') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
            ' " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
            ' " And (tblemployee.LeavingDate >=#" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "# or tblemployee.LeavingDate is null)" & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & strsortorder

            strsql = "select tblCatagory.CatagoryName,TblEmployee.EMPPHOTO, tblDivision.DivisionName,tblTimeRegister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tbltimeregister.ShiftAttended,tbltimeregister.ShiftStartTime,tbltimeregister.In1,tbltimeregister.Out1,tbltimeregister.In2,tbltimeregister.Out2,tbltimeregister.HoursWorked,tbltimeregister.Status,tbltimeregister.EarlyArrival," & _
            " tbltimeregister.LateArrival,tbltimeregister.EarlyDeparture,tbltimeregister.ExcLunchHours,tbltimeregister.OtDuration,tbltimeregister.OtAmount,tbltimeregister.OsDuration,tbltimeregister.In1Mannual,tbltimeregister.In2Mannual,tbltimeregister.Out1Mannual,tbltimeregister.Out2Mannual,tblCalander.mDate,tblCalander.Process,tblCalander.NRTCProc" & _
            " from tblCatagory,tblDivision,tbltimeregister,tblEmployee,tblCalander,tblCompany,tblDepartment" & _
            " Where tblCatagory.Cat = tblEmployee.Cat And tbltimeregister.PayCode = tblEmployee.PayCode And FORMAT(tbltimeregister.DateOffice,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' And FORMAT(tblCalander.mDate,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
            " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
            " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & strsortorder
            adapA = New OleDbDataAdapter(strsql, Common.con1)
            adapA.Fill(Rs_Report)
        Else
            strsql = "select tblCatagory.CatagoryName, TblEmployee.EMPPHOTO, tblDivision.DivisionName,tblTimeRegister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tbltimeregister.ShiftAttended,tbltimeregister.ShiftStartTime,tbltimeregister.In1,tbltimeregister.Out1,tbltimeregister.In2,tbltimeregister.Out2,tbltimeregister.HoursWorked,tbltimeregister.Status,tbltimeregister.EarlyArrival," & _
             " tbltimeregister.LateArrival,tbltimeregister.EarlyDeparture,tbltimeregister.ExcLunchHours,tbltimeregister.OtDuration,tbltimeregister.OtAmount,tbltimeregister.OsDuration,tbltimeregister.In1Mannual,tbltimeregister.In2Mannual,tbltimeregister.Out1Mannual,tbltimeregister.Out2Mannual,tblCalander.mDate,tblCalander.Process,tblCalander.NRTCProc" & _
             " from tblCatagory,tblDivision,tbltimeregister,tblEmployee,tblCalander,tblCompany,tblDepartment" & _
             " Where tblCatagory.Cat = tblEmployee.Cat And tbltimeregister.PayCode = tblEmployee.PayCode And tbltimeregister.DateOffice = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' And tblCalander.mDate = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
             " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
             " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & strsortorder
            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(Rs_Report)
        End If
        'Else
        '    strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName,tblTimeRegisterD.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tblTimeRegisterD.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tblTimeRegisterD.ShiftAttended,tblTimeRegisterD.ShiftStartTime,tblTimeRegisterD.In1,tblTimeRegisterD.Out1,tblTimeRegisterD.In2,tblTimeRegisterD.Out2,tblTimeRegisterD.HoursWorked,tblTimeRegisterD.Status,tblTimeRegisterD.EarlyArrival," & _
        '         " tblTimeRegisterD.LateArrival,tblTimeRegisterD.EarlyDeparture,tblTimeRegisterD.ExcLunchHours,tblTimeRegisterD.OtDuration,tblTimeRegisterD.OtAmount,tblTimeRegisterD.OsDuration,tblTimeRegisterD.In1Mannual,tblTimeRegisterD.In2Mannual,tblTimeRegisterD.Out1Mannual,tblTimeRegisterD.Out2Mannual,tblCalander.mDate,tblCalander.Process,tblCalander.NRTCProc" & _
        '         " from tblCatagory,tblDivision,tblTimeRegisterD,tblEmployee,tblCalander,tblCompany,tblDepartment" & _
        '         " Where tblCatagory.Cat = tblEmployee.Cat And tblDivision.DivisionCode = tblEmployee.DivisionCode And tblTimeRegisterD.PayCode = tblEmployee.PayCode And tbltimeregisterD.DateOffice = '" & Format(frmDailyAttReport.txtFromDate.Value, "MMM dd yyyy") & "' And tblCalander.mDate = '" & Format(frmDailyAttReport.txtFromDate.Value, "MMM dd yyyy") & "'" & _
        '         " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
        '         " And (tblemployee.LeavingDate>='" & Format(frmDailyAttReport.txtFromDate, "MMM dd yyyy") & "' or tblemployee.LeavingDate is null)" & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause)
        'End If
        'Rs_Report = Cn.Execute(strsql)
        If Rs_Report.Tables(0).Rows.Count < 1 Then
            XtraMessageBox.Show(ulf, "<size=10>No Data Available For this Report.</size>", "<size=9>iAS</size>")
            'mReportPrintStatus = False
            Exit Sub
        End If
        If strsortorder = "" Then
            'Rs_Report.Sort = "PayCode"
        Else
            'Rs_Report.Sort = strsortorder
        End If
        'If IsNull(Rs_Report!Process) And IsNull(Rs_Report!NRTCProc) Then
        '    MsgBox "Records are not Processed", vbInformation + vbExclamation, "TimeWatch Information"
        '    mReportPrintStatus = False
        '    Exit Sub
        'ElseIf InStr("YX", Rs_Report("Process")) > 0 Or InStr("Y", Rs_Report("nRTCProc")) > 0 Then
        rowcnt = 1
        xlapp.Visible = True
        xlapp.Columns.Font.Name = "TAHOMA"
        xlapp.Cells(rowcnt, 4).Font.Bold = True

        xlapp.Cells(rowcnt, 4) = "Company Name:" & CommonReport.g_CompanyNames
        rowcnt = rowcnt + 1

        xlapp.Cells(rowcnt, 4).Font.Bold = True
        xlapp.Cells(rowcnt, 4) = "Run Date & Time :" & Now.ToString("dd/MM/yyyy HH:mm")

        rowcnt = rowcnt + 2
        xlapp.Cells.Font.Size = 8
        xlapp.Cells(rowcnt, 4).Font.Bold = True
        xlapp.Cells(rowcnt, 4) = "DAILY PERFORMANCE REPORT FOR DATE : " & DateEdit1.DateTime.ToString("dd/MM/yyyy")
        rowcnt = rowcnt + 1
        rowcnt = rowcnt + 1
        xlapp.Columns.ColumnWidth = 9
        xlapp.Columns.Font.Name = "TAHOMA"
        xlapp.Columns.Font.Size = 8
        xlapp.Columns.Font.ColorIndex = 1 'vbBlack
        ' xlapp.Cells(rowcnt, 1) = "User Number"
        ' xlapp.Cells.Font.Bold = False
        xlapp.Range("A6:V6").Font.Bold = True
        xlapp.Range("A6:V6").Font.ColorIndex = 2 'vbWhite
        'xlapp.Range("A6:U6").HorizontalAlignment = xlLeft
        xlapp.Range("A6:V6").Interior.ColorIndex = 51
        xlapp.Range("A6:V6").Borders.LineStyle = XlLineStyle.xlContinuous


        xlapp.Cells(rowcnt, 1) = "Sr.No."
        xlapp.Cells(rowcnt, 2) = "PayCode"
        xlapp.Cells(rowcnt, 3) = "Card No"
        xlapp.Cells(rowcnt, 4) = "Employee Name"
        xlapp.Cells(rowcnt, 6) = "Shift  "
        xlapp.Cells(rowcnt, 7) = "Start "
        xlapp.Cells(rowcnt, 8) = "   In "
        xlapp.Cells(rowcnt, 9) = "Lunch Out"
        xlapp.Cells(rowcnt, 10) = "Lunch In"
        xlapp.Cells(rowcnt, 11) = "   Out "
        xlapp.Cells(rowcnt, 12) = "Hrs Works "
        xlapp.Cells(rowcnt, 13) = "Status "
        xlapp.Cells(rowcnt, 14) = "Early Arriv."
        xlapp.Cells(rowcnt, 15) = "Late Arriv."
        xlapp.Cells(rowcnt, 16) = "Shift Early"
        xlapp.Cells(rowcnt, 17) = "Excess Lunch "
        xlapp.Cells(rowcnt, 18) = "OT "
        xlapp.Cells(rowcnt, 19) = "OT Amount "
        xlapp.Cells(rowcnt, 20) = "OS "
        xlapp.Cells(rowcnt, 21) = "Manual "
        xlapp.Cells(rowcnt, 22) = "Picture "
        'rowcnt = rowcnt + 1
        mintLine = 9
        mCount = 0
        'With Rs_Report
        'If blnDeptAvailable And Not .EOF Then
        '    rowcnt = rowcnt + 1
        '    If strsortorder.Contains("Department") = True Or XtraShortOrder.CheckDeptName.Checked Then
        '        xlapp.Cells(rowcnt, 1) = "** Department Code & Name : " & Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.trim & "  " & Rs_Report.Tables(0).Rows(i).Item("DepartmentName").ToString.trim
        '    ElseIf strsortorder.Contains("Division") Or strsortorder.Contains("Section)" Or XtraShortOrder.CheckSectionPay.Checked Then
        '        xlapp.Cells(rowcnt, 1) = "** Section Code & Name : " & !DivisionCode & "  " & !DivisionName
        '    Else
        '        xlapp.Cells(rowcnt, 1) = "** Category Code & Name : " & !Cat & "  " & !CatagoryName
        '    End If
        '    rowcnt = rowcnt + 1
        '    mintLine = mintLine + 3
        'End If
        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            'Do While Not .EOF
            strOsDuration6 = 0
            strOtAmount11 = 0
            If Rs_Report.Tables(0).Rows(i).Item("OsDuration").ToString.Trim = "" Or Rs_Report.Tables(0).Rows(i).Item("OsDuration").ToString.Trim = "0" Then
            Else
                strOsDuration6 = Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("OsDuration").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("OsDuration").ToString.Trim Mod 60).ToString("00")
            End If
            If Rs_Report.Tables(0).Rows(i).Item("OTAmount").ToString.Trim = "" Or Rs_Report.Tables(0).Rows(i).Item("OTAmount").ToString.Trim = "0" Then
            Else
                strOtAmount11 = Rs_Report.Tables(0).Rows(i).Item("OTAmount").ToString.Trim
            End If

            If Rs_Report.Tables(0).Rows(i).Item("IN1MANNUAL").ToString.Trim = "" Then strIn1Mannual_1 = "" Else strIn1Mannual_1 = Rs_Report.Tables(0).Rows(i).Item("IN1MANNUAL").ToString.Trim
            If Rs_Report.Tables(0).Rows(i).Item("Out1Mannual").ToString.Trim = "" Then strOut1Mannual_1 = "" Else strOut1Mannual_1 = Rs_Report.Tables(0).Rows(i).Item("Out1Mannual").ToString.Trim
            If Rs_Report.Tables(0).Rows(i).Item("IN2MANNUAL").ToString.Trim = "" Then strIn2Mannual_1 = "" Else strIn2Mannual_1 = Rs_Report.Tables(0).Rows(i).Item("IN2MANNUAL").ToString.Trim
            If Rs_Report.Tables(0).Rows(i).Item("OUT2MANNUAL").ToString.Trim = "" Then strOut2Mannual_1 = "" Else strOut2Mannual_1 = Rs_Report.Tables(0).Rows(i).Item("OUT2MANNUAL").ToString.Trim
            Dim X As String
            If UCase(strIn1Mannual_1) = "Y" Or UCase(strOut1Mannual_1) = "Y" Or UCase(strIn2Mannual_1) = "Y" Or UCase(strOut2Mannual_1) = "Y" Then
                X = "Y"
            Else
                X = " "
            End If
            mCount = mCount + 1
            rowcnt = rowcnt + 1
            xlapp.Rows(rowcnt).RowHeight = 80
            xlapp.Cells(rowcnt, 1).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 1) = mCount
            xlapp.Cells(rowcnt, 2).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 2) = "'" & Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim
            xlapp.Cells(rowcnt, 3).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 3) = "'" & Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim
            xlapp.Cells(rowcnt, 4).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 4) = Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim
            xlapp.Cells(rowcnt, 5).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 6).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 6) = Rs_Report.Tables(0).Rows(i).Item("ShiftAttended").ToString.Trim
            xlapp.Cells(rowcnt, 7).Borders.LineStyle = XlLineStyle.xlContinuous
            If Rs_Report.Tables(0).Rows(i).Item("shiftStartTime").ToString.Trim = "" Then
                xlapp.Cells(rowcnt, 7) = ""
            Else
                xlapp.Cells(rowcnt, 7) = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("shiftStartTime").ToString.Trim).ToString("HH:mm")
            End If
            xlapp.Cells(rowcnt, 8).Borders.LineStyle = XlLineStyle.xlContinuous
            If Rs_Report.Tables(0).Rows(i).Item("In1").ToString.Trim = "" Then
                xlapp.Cells(rowcnt, 8) = ""
            Else
                xlapp.Cells(rowcnt, 8) = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("In1").ToString.Trim).ToString("HH:mm")
            End If
            'xlapp.Cells(rowcnt, 8) = Format(!In1, "HH:MM")
            xlapp.Cells(rowcnt, 9).Borders.LineStyle = XlLineStyle.xlContinuous
            If Rs_Report.Tables(0).Rows(i).Item("out1").ToString.Trim = "" Then
                xlapp.Cells(rowcnt, 9) = ""
            Else
                xlapp.Cells(rowcnt, 9) = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("out1").ToString.Trim).ToString("HH:mm")
            End If
            'xlapp.Cells(rowcnt, 9) = Format(!out1, "HH:MM")
            xlapp.Cells(rowcnt, 10).Borders.LineStyle = XlLineStyle.xlContinuous
            If Rs_Report.Tables(0).Rows(i).Item("In2").ToString.Trim = "" Then
                xlapp.Cells(rowcnt, 10) = ""
            Else
                xlapp.Cells(rowcnt, 10) = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("In2").ToString.Trim).ToString("HH:mm")
            End If
            'xlapp.Cells(rowcnt, 10) = Format(!in2, "HH:MM")
            xlapp.Cells(rowcnt, 11).Borders.LineStyle = XlLineStyle.xlContinuous
            If Rs_Report.Tables(0).Rows(i).Item("out2").ToString.Trim = "" Then
                xlapp.Cells(rowcnt, 11) = ""
            Else
                xlapp.Cells(rowcnt, 11) = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("out2").ToString.Trim).ToString("HH:mm")
            End If
            'xlapp.Cells(rowcnt, 11) = Format(!out2, "HH:MM")
            xlapp.Cells(rowcnt, 12).Borders.LineStyle = XlLineStyle.xlContinuous
            If Rs_Report.Tables(0).Rows(i).Item("HOURSWORKED").ToString.Trim = "0" Or Rs_Report.Tables(0).Rows(i).Item("HOURSWORKED").ToString.Trim = "" Then
                xlapp.Cells(rowcnt, 12) = ""
            Else
                xlapp.Cells(rowcnt, 12) = "'" & Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("HOURSWORKED").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("HOURSWORKED").ToString.Trim Mod 60).ToString("00")
            End If
            'xlapp.Cells(rowcnt, 12) = "'" & Allign(Min2Hr(!HOURSWORKED))
            xlapp.Cells(rowcnt, 13).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 13) = Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim
            xlapp.Cells(rowcnt, 14).Borders.LineStyle = XlLineStyle.xlContinuous
            If Rs_Report.Tables(0).Rows(i).Item("EARLYARRIVAL").ToString.Trim = "0" Or Rs_Report.Tables(0).Rows(i).Item("EARLYARRIVAL").ToString.Trim = "" Then
                xlapp.Cells(rowcnt, 14) = ""
            Else
                xlapp.Cells(rowcnt, 14) = "'" & Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("EARLYARRIVAL").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("EARLYARRIVAL").ToString.Trim Mod 60).ToString("00")
            End If
            'xlapp.Cells(rowcnt, 14) = "'" & Allign(Min2Hr(!EARLYARRIVAL))
            xlapp.Cells(rowcnt, 15).Borders.LineStyle = XlLineStyle.xlContinuous
            If Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim = "0" Or Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim = "" Then
                xlapp.Cells(rowcnt, 15) = ""
            Else
                xlapp.Cells(rowcnt, 15) = "'" & Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim Mod 60).ToString("00")
            End If
            'xlapp.Cells(rowcnt, 15) = "'" & Allign(Min2Hr(!latearrival))
            xlapp.Cells(rowcnt, 16).Borders.LineStyle = XlLineStyle.xlContinuous
            If Rs_Report.Tables(0).Rows(i).Item("earlydeparture").ToString.Trim = "0" Or Rs_Report.Tables(0).Rows(i).Item("earlydeparture").ToString.Trim = "" Then
                xlapp.Cells(rowcnt, 16) = ""
            Else
                xlapp.Cells(rowcnt, 16) = "'" & Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("earlydeparture").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("earlydeparture").ToString.Trim Mod 60).ToString("00")
            End If
            'xlapp.Cells(rowcnt, 16) = "'" & Allign(Min2Hr(!earlydeparture))
            xlapp.Cells(rowcnt, 17).Borders.LineStyle = XlLineStyle.xlContinuous
            If Rs_Report.Tables(0).Rows(i).Item("EXCLUNCHHOURS").ToString.Trim = "0" Or Rs_Report.Tables(0).Rows(i).Item("EXCLUNCHHOURS").ToString.Trim = "" Then
                xlapp.Cells(rowcnt, 17) = ""
            Else
                xlapp.Cells(rowcnt, 17) = "'" & Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("EXCLUNCHHOURS").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("EXCLUNCHHOURS").ToString.Trim Mod 60).ToString("00")
            End If
            'xlapp.Cells(rowcnt, 17) = "'" & Allign(Min2Hr(!EXCLUNCHHOURS))
            xlapp.Cells(rowcnt, 18).Borders.LineStyle = XlLineStyle.xlContinuous
            If Rs_Report.Tables(0).Rows(i).Item("OtDuration").ToString.Trim = "0" Or Rs_Report.Tables(0).Rows(i).Item("OtDuration").ToString.Trim = "" Then
                xlapp.Cells(rowcnt, 18) = ""
            Else
                xlapp.Cells(rowcnt, 18) = "'" & Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("OtDuration").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("OtDuration").ToString.Trim Mod 60).ToString("00")
            End If
            'xlapp.Cells(rowcnt, 18) = "'" & Allign(Min2Hr(!OtDuration))
            xlapp.Cells(rowcnt, 19).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 19) = "'" & (strOtAmount11)
            xlapp.Cells(rowcnt, 20).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 20) = "'" & (strOsDuration6)
            xlapp.Cells(rowcnt, 21).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 21) = X

            ' xlapp.Cells(rowcnt, 22).Borders.LineStyle = XlLineStyle.xlContinuous
            'xlapp.Cells(rowcnt, 22) = X

            Dim PicPath As String = My.Application.Info.DirectoryPath & (Rs_Report.Tables(0).Rows(i).Item("EMPPHOTO").ToString.Trim).TrimStart(".").Replace("/", "\")
            With xlapp.ActiveSheet.Pictures.Insert(PicPath)
                With .ShapeRange
                    '.LockAspectRatio = msoTrue
                    .Width = 110
                    .Height = 80
                End With
                .Left = xlapp.ActiveSheet.Cells(rowcnt, 22).Left
                .Top = xlapp.ActiveSheet.Cells(rowcnt, 22).Top
                .Placement = 1
                .PrintObject = True
            End With

            'Dim img As Image = Image.FromFile(My.Application.Info.DirectoryPath & (Rs_Report.Tables(0).Rows(i).Item("EMPPHOTO").ToString.Trim).TrimStart(".").Replace("/", "\"))

            ''Dim sheet As XLSheet = wb.Sheets("Forecasting Report")
            'xlapp.Cells(rowcnt, 22) = img
            'MsgBox(xlapp.Cells(rowcnt, 22).GetType)
            'Dim ws As Worksheet
            'Dim r As Range
            'Dim fileName As String
            'Dim pictureWidth As Integer
            'Dim pictureHeight As Integer
            'Dim shape As Shape
            ''ws = Worksheets(1)
            'r = xlapp.Cells(rowcnt, 22) 'ws.Cells(rowcnt, 22)
            'fileName = Rs_Report.Tables(0).Rows(i).Item("EMPPHOTO").ToString.Trim
            'pictureWidth = 195
            'pictureHeight = 102
            'shape = ws.Shapes.AddPicture(fileName, True, True, r.Left, r.Top, pictureWidth * 3 / 4, pictureHeight * 3 / 4)
            ''shape = ws.Shapes.AddPicture(fileName, r.Left, r.Top, pictureWidth * 3 / 4, pictureHeight * 3 / 4)
            'shape.LockAspectRatio = True
            ''shape.ScaleHeight(2, True, ScaleFromTopLeft)



            mblnCheckReport = True
            mintLine = mintLine + 1

            If strsortorder.Contains("Department") Then
                strDeptDivCode = Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim
            ElseIf strsortorder.Contains("Division") Then
                strDeptDivCode = Rs_Report.Tables(0).Rows(i).Item("DivisionCode").ToString.Trim
            Else
                strDeptDivCode = Rs_Report.Tables(0).Rows(i).Item("Cat").ToString.Trim
            End If
            '.MoveNext()
            'i = i + 1
            'If blnDeptAvailable And i <= Rs_Report.Tables(0).Rows.Count - 1 Then
            '    If strsortorder.Contains("Department") Then
            '        strDeptDivCode = Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim
            '    ElseIf strsortorder.Contains("Division") Then
            '        strDeptDivCode = Rs_Report.Tables(0).Rows(i).Item("DivisionCode").ToString.Trim
            '    Else
            '        strDeptDivCode = Rs_Report.Tables(0).Rows(i).Item("Cat").ToString.Trim
            '    End If
            '    If strDeptDivCode <> strCurrentDeptDivCode Then
            '        rowcnt = rowcnt + 1
            '        If Left(strsortorder, 10) = "Department" Or frmSorting.optDeptEmpName.Value Then
            '            xlapp.Cells(rowcnt, 1) = "** Department Code & Name : " & !DepartmentCode & "  " & !DepartmentName
            '        ElseIf Left(strsortorder, 1) = "Division" Or frmSorting.optSectionPaycode.Value Then
            '            xlapp.Cells(rowcnt, 1) = "** Section Code & Name : " & !DivisionCode & "  " & !DivisionName
            '        Else
            '            xlapp.Cells(rowcnt, 1) = "** Category Code & Name : " & !Cat & "  " & !CatagoryName
            '        End If
            '        rowcnt = rowcnt + 1
            '        mintLine = mintLine + 3
            '    End If
            'End If
        Next
        'Loop
        'End With

        '        xlwb.SaveAs(App.Path & "\TimeWatch.xls")
        '        Exit Sub
        'ErrorGen:
        '        If Err.Description = "Application-defined or object-defined error" Or Err.Description = "Method 'SaveAs' of object '_Workbook' failed" Then
        '            Screen.MousePointer = vbDefault
        '        Else
        '            MsgBox(Err.Description, vbInformation + vbOKOnly, "Report Generation Information")
        '            Screen.MousePointer = vbDefault
        '        End If
        xlwb.SaveAs(mstrFile_Name)
    End Sub
    Sub DailyXl_EarlyDeparture(ByVal strsortorder As String)
        'On Error GoTo ErrorGen
        Dim strDeptDivCode As String
        Dim strCurrentDeptDivCode As String
        Dim intFile As Integer
        Dim strsql As String, mCount As Integer
        Dim strOsDuration6 As String ' * 6
        Dim strDepartmentCode As String
        Dim intPrintLineCounter As Integer
        Dim blnDeptAvailable As Boolean
        Dim xlapp As Excel.Application
        Dim xlwb As Excel.Workbook
        Dim xlst As Excel.Sheets
        Dim rowcnt As Integer
        xlapp = CreateObject("Excel.Application")
        xlwb = xlapp.Workbooks.Add
        'If UCase(Dir(App.Path & "\TimeWatch.xls")) = UCase("TimeWatch.xls") Then
        '    Kill(App.Path & "\TimeWatch.xls")
        'End If
        If XtraShortOrder.g_SortOrder <> "" Then
            strsortorder = XtraShortOrder.g_SortOrder
        End If
        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If
        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".xls"

        mblnCheckReport = False
        mintPageNo = 1
        mintLine = 1
        If strsortorder.Substring(0, 1) = "D" Or strsortorder.Substring(0, 1) = "C" Then blnDeptAvailable = True
        'If g_Report_view Then
        If Common.servername = "Access" Then
            'strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName,tblTimeRegister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tblEmployeeShiftMaster.IsRoundTheClockWork,tbltimeregister.ShiftAttended,tbltimeregister.ShiftEndTime,tbltimeregister.Out2," & _
            ' "tbltimeregister.EarlyDeparture,tbltimeregister.Status,tbltimeregister.OtDuration,tbltimeregister.OsDuration,tbltimeregister.In1Mannual,tbltimeregister.In2Mannual,tbltimeregister.Out1Mannual,tbltimeregister.Out2Mannual,tblcalander.mDate,tblcalander.Process,tblcalander.NRTCProc" & _
            ' " from tblCatagory,tblDivision,  tbltimeregister,tblEmployee,tblEmployeeShiftMaster ,tblCalander ,tblCompany,tblDepartment" & _
            ' " Where tblCatagory.Cat = tblEmployee.Cat And tblDivision.DivisionCode = tblEmployee.DivisionCode And tbltimeRegister.EarlyDeparture > 0 And tbltimeregister.PayCode = tblEmployee.PayCode And tbltimeregister.PayCode = tblEmployeeShiftMaster.PayCode And FORMAT(tbltimeregister.DateOffice,'MMM DD YYYY') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & " And FORMAT(tblcalander.mDate,'MMM DD YYYY') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
            ' " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
            ' " And (tblemployee.LeavingDate >=#" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "# or tblemployee.LeavingDate is null)" & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & strsortorder

            strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName,tblTimeRegister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tblEmployeeShiftMaster.IsRoundTheClockWork,tbltimeregister.ShiftAttended,tbltimeregister.ShiftEndTime,tbltimeregister.Out2," & _
            "tbltimeregister.EarlyDeparture,tbltimeregister.Status,tbltimeregister.OtDuration,tbltimeregister.OsDuration,tbltimeregister.In1Mannual,tbltimeregister.In2Mannual,tbltimeregister.Out1Mannual,tbltimeregister.Out2Mannual,tblcalander.mDate,tblcalander.Process,tblcalander.NRTCProc" & _
            " from tblCatagory,tblDivision,  tbltimeregister,tblEmployee,tblEmployeeShiftMaster ,tblCalander ,tblCompany,tblDepartment" & _
            " Where tblCatagory.Cat = tblEmployee.Cat And tbltimeRegister.EarlyDeparture > 0 And tbltimeregister.PayCode = tblEmployee.PayCode And tbltimeregister.PayCode = tblEmployeeShiftMaster.PayCode And FORMAT(tbltimeregister.DateOffice,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & " And FORMAT(tblcalander.mDate,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
            " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
            " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & strsortorder
            adapA = New OleDbDataAdapter(strsql, Common.con1)
            adapA.Fill(Rs_Report)
        Else
            strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName,tblTimeRegister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tblEmployeeShiftMaster.IsRoundTheClockWork,tbltimeregister.ShiftAttended,tbltimeregister.ShiftEndTime,tbltimeregister.Out2," & _
             "tbltimeregister.EarlyDeparture,tbltimeregister.Status,tbltimeregister.OtDuration,tbltimeregister.OsDuration,tbltimeregister.In1Mannual,tbltimeregister.In2Mannual,tbltimeregister.Out1Mannual,tbltimeregister.Out2Mannual,tblcalander.mDate,tblcalander.Process,tblcalander.NRTCProc" & _
             " from tblCatagory,tblDivision,  tbltimeregister,tblEmployee,tblEmployeeShiftMaster ,tblCalander ,tblCompany,tblDepartment" & _
             " Where tblCatagory.Cat = tblEmployee.Cat And tbltimeRegister.EarlyDeparture > 0 And tbltimeregister.PayCode = tblEmployee.PayCode And tbltimeregister.PayCode = tblEmployeeShiftMaster.PayCode And tbltimeregister.DateOffice = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & " And tblcalander.mDate = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
             " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
             " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & strsortorder
            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(Rs_Report)
        End If
        'Else
        'strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName,tblTimeRegisterD.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tblTimeRegisterD.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tblEmployeeShiftMaster.IsRoundTheClockWork,tblTimeRegisterD.ShiftAttended,tblTimeRegisterD.ShiftEndTime,tblTimeRegisterD.Out2," & _
        '     "tblTimeRegisterD.EarlyDeparture,tblTimeRegisterD.Status,tblTimeRegisterD.OtDuration,tblTimeRegisterD.OsDuration,tblTimeRegisterD.In1Mannual,tblTimeRegisterD.In2Mannual,tblTimeRegisterD.Out1Mannual,tblTimeRegisterD.Out2Mannual,tblcalander.mDate,tblcalander.Process,tblcalander.NRTCProc" & _
        '     " from tblCatagory,tblDivision,  tblTimeRegisterD,tblEmployee,tblEmployeeShiftMaster ,tblCalander ,tblCompany,tblDepartment" & _
        '     " Where tblCatagory.Cat = tblEmployee.Cat And tblDivision.DivisionCode = tblEmployee.DivisionCode And tblTimeRegisterD.EarlyDeparture > 0 And tblTimeRegisterD.PayCode = tblEmployee.PayCode And tblTimeRegisterD.PayCode = tblEmployeeShiftMaster.PayCode And tblTimeRegisterD.DateOffice = '" & Format(frmDailyAttReport.txtFromDate.Value, "MMM dd yyyy") & "'" & " And tblcalander.mDate = '" & Format(frmDailyAttReport.txtFromDate.Value, "MMM dd yyyy") & "'" & _
        '     " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
        '     " And (tblemployee.LeavingDate>='" & Format(frmDailyAttReport.txtFromDate, "MMM dd yyyy") & "' or tblemployee.LeavingDate is null)" & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause)
        'End If
        'Rs_Report = Cn.Execute(strsql)
        If Rs_Report.Tables(0).Rows.Count < 1 Then
            'G_Report_Error = False
            'Screen.MousePointer = vbDefault
            XtraMessageBox.Show(ulf, "<size=10>Nobody has departed before the Shift End Time on this Date.</size>", "<size=9>iAS</size>")
            'mReportPrintStatus = False
            Exit Sub
        End If
        'If strsortorder = "" Then
        '    Rs_Report.Sort = "PayCode"
        'Else
        '    Rs_Report.Sort = strsortorder
        'End If


        If Rs_Report.Tables(0).Rows(0).Item("Process").ToString.Trim = "" And Rs_Report.Tables(0).Rows(0).Item("NRTCProc").ToString.Trim = "" Then
            MsgBox("Records are not Processed", vbInformation + vbExclamation, "Information")
            'mReportPrintStatus = False
            Exit Sub
        ElseIf InStr("YX", Rs_Report.Tables(0).Rows(0).Item("Process").ToString.Trim) > 0 Or InStr("Y", Rs_Report.Tables(0).Rows(0).Item("nRTCProc").ToString.Trim) > 0 Then
            rowcnt = 1
            xlapp.Visible = True
            xlapp.Columns.ColumnWidth = 9
            xlapp.Columns.Font.Name = "TAHOMA"
            xlapp.Columns.Font.Size = 8
            xlapp.Columns.Font.ColorIndex = 1 'vbBlack
            xlapp.Cells(rowcnt, 4).Font.Bold = True
            xlapp.Cells(rowcnt, 4) = "Company Name:" & CommonReport.g_CompanyNames
            rowcnt = rowcnt + 1
            xlapp.Cells(rowcnt, 4).Font.Bold = True
            xlapp.Cells(rowcnt, 4) = "Run Date & Time :" & Now.ToString("dd/MM/yyyy HH:mm")
            rowcnt = rowcnt + 2
            xlapp.Cells(rowcnt, 4).Font.Bold = True
            xlapp.Cells(rowcnt, 4) = "EARLY DEPARTURE FOR DATE : " & DateEdit1.DateTime.ToString("dd/MM/yyyy")
            rowcnt = rowcnt + 1


            xlapp.Range("A5:N5").Font.Bold = True
            xlapp.Range("A5:N5").Font.ColorIndex = 2 'vbWhite
            'xlapp.Range("A5:N5").HorizontalAlignment = xlLeft
            xlapp.Range("A5:N5").Interior.ColorIndex = 51
            xlapp.Range("A5:N5").Borders.LineStyle = XlLineStyle.xlContinuous

            xlapp.Cells(rowcnt, 1) = "Sr.No."
            xlapp.Cells(rowcnt, 2) = "PayCode"
            xlapp.Cells(rowcnt, 3) = "Card No"
            xlapp.Cells(rowcnt, 4) = "Employee Name"
            xlapp.Cells(rowcnt, 6) = "RTC  "
            xlapp.Cells(rowcnt, 7) = "Shift "
            xlapp.Cells(rowcnt, 8) = "End"
            xlapp.Cells(rowcnt, 9) = "Out "
            xlapp.Cells(rowcnt, 10) = "Shift Early "
            xlapp.Cells(rowcnt, 11) = "Status  "
            xlapp.Cells(rowcnt, 12) = "Ot "
            xlapp.Cells(rowcnt, 13) = "Over Stay "
            xlapp.Cells(rowcnt, 14) = "Manual "
            'rowcnt = rowcnt + 1
            mintLine = 9
            mCount = 0
            'With Rs_Report
            'If blnDeptAvailable And Not .EOF Then
            '    rowcnt = rowcnt + 1
            '    If Left(strsortorder, 10) = "Department" Or frmSorting.optDeptEmpName.Value Then
            '        xlapp.Cells(rowcnt, 1) = "** Department Code & Name : " & !DepartmentCode & "  " & !DepartmentName
            '    ElseIf Left(strsortorder, 8) = "Division" Or Left(strsortorder, 7) = "Section" Or frmSorting.optSectionPaycode.Value Then
            '        xlapp.Cells(rowcnt, 1) = "** Section Code & Name : " & !DivisionCode & "  " & !DivisionName
            '    Else
            '        xlapp.Cells(rowcnt, 1) = "** Category Code & Name : " & !Cat & "  " & !CatagoryName
            '    End If
            '    rowcnt = rowcnt + 1
            '    mintLine = mintLine + 3
            'End If
            For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
                'Do While Not .EOF
                'strOsDuration6 = 0
                'If Not IsNull(OsDuration) Then If !OsDuration <> 0 Then strOsDuration6 = Trim(Length5(Min2Hr(!OsDuration)))
                If Rs_Report.Tables(0).Rows(i).Item("OsDuration").ToString.Trim <> "" Or Rs_Report.Tables(0).Rows(i).Item("OsDuration").ToString.Trim <> "0" Then
                    strOsDuration6 = Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("OsDuration").ToString.Trim / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("OsDuration").ToString.Trim Mod 60).ToString("00")
                Else
                    strOsDuration6 = ""
                End If
                rowcnt = rowcnt + 1
                mCount = mCount + 1
                xlapp.Cells(rowcnt, 1).Borders.LineStyle = XlLineStyle.xlContinuous
                xlapp.Cells(rowcnt, 1) = mCount
                xlapp.Cells(rowcnt, 2).Borders.LineStyle = XlLineStyle.xlContinuous
                xlapp.Cells(rowcnt, 2) = "'" & Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim
                xlapp.Cells(rowcnt, 3).Borders.LineStyle = XlLineStyle.xlContinuous
                xlapp.Cells(rowcnt, 3) = "'" & Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim
                xlapp.Cells(rowcnt, 4).Borders.LineStyle = XlLineStyle.xlContinuous
                xlapp.Cells(rowcnt, 4) = Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim
                xlapp.Cells(rowcnt, 5).Borders.LineStyle = XlLineStyle.xlContinuous
                xlapp.Cells(rowcnt, 6).Borders.LineStyle = XlLineStyle.xlContinuous
                xlapp.Cells(rowcnt, 6) = Rs_Report.Tables(0).Rows(i).Item("isroundtheclockwork").ToString.Trim
                xlapp.Cells(rowcnt, 7).Borders.LineStyle = XlLineStyle.xlContinuous
                xlapp.Cells(rowcnt, 7) = Rs_Report.Tables(0).Rows(i).Item("ShiftAttended").ToString.Trim
                xlapp.Cells(rowcnt, 8).Borders.LineStyle = XlLineStyle.xlContinuous
                If Rs_Report.Tables(0).Rows(i).Item("shiftEndTime").ToString.Trim = "" Then
                    xlapp.Cells(rowcnt, 8) = ""
                Else
                    xlapp.Cells(rowcnt, 8) = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("shiftEndTime").ToString.Trim).ToString("HH:mm")
                End If
                'xlapp.Cells(rowcnt, 8) = Format(!shiftEndTime, "HH:MM")
                xlapp.Cells(rowcnt, 9).Borders.LineStyle = XlLineStyle.xlContinuous
                If Rs_Report.Tables(0).Rows(i).Item("out2").ToString.Trim = "" Then
                    xlapp.Cells(rowcnt, 9) = ""
                Else
                    xlapp.Cells(rowcnt, 9) = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("out2").ToString.Trim).ToString("HH:mm")
                End If
                'xlapp.Cells(rowcnt, 9) = Format(!out2, "HH:MM")
                xlapp.Cells(rowcnt, 10).Borders.LineStyle = XlLineStyle.xlContinuous
                If Rs_Report.Tables(0).Rows(i).Item("earlydeparture").ToString.Trim = "0" Then
                    xlapp.Cells(rowcnt, 10) = ""
                Else
                    xlapp.Cells(rowcnt, 10) = "'" & Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("earlydeparture").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("earlydeparture").ToString.Trim Mod 60).ToString("00")
                End If
                'xlapp.Cells(rowcnt, 10) = "'" & Allign(Min2Hr(!earlydeparture))
                xlapp.Cells(rowcnt, 11).Borders.LineStyle = XlLineStyle.xlContinuous
                xlapp.Cells(rowcnt, 11) = Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim
                xlapp.Cells(rowcnt, 12).Borders.LineStyle = XlLineStyle.xlContinuous
                If Rs_Report.Tables(0).Rows(i).Item("OtDuration").ToString.Trim = "0" Then
                    xlapp.Cells(rowcnt, 12) = ""
                Else
                    xlapp.Cells(rowcnt, 12) = "'" & Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("OtDuration").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("OtDuration").ToString.Trim Mod 60).ToString("00")
                End If
                'xlapp.Cells(rowcnt, 12) = "'" & Allign(Min2Hr(!OtDuration))
                xlapp.Cells(rowcnt, 13).Borders.LineStyle = XlLineStyle.xlContinuous
                xlapp.Cells(rowcnt, 13) = "'" & (strOsDuration6)
                xlapp.Cells(rowcnt, 14).Borders.LineStyle = XlLineStyle.xlContinuous
                xlapp.Cells(rowcnt, 14) = IIf(UCase(Rs_Report.Tables(0).Rows(i).Item("IN1MANNUAL").ToString.Trim) = "Y", "M", IIf(UCase(Rs_Report.Tables(0).Rows(i).Item("Out1Mannual").ToString.Trim) = "Y", "M", IIf(UCase(Rs_Report.Tables(0).Rows(i).Item("IN2MANNUAL").ToString.Trim) = "Y", "M", IIf(UCase(Rs_Report.Tables(0).Rows(i).Item("OUT2MANNUAL").ToString.Trim) = "Y", "M", ""))))

                rowcnt = rowcnt + 1

                mintLine = mintLine + 1
                mblnCheckReport = True
                'If strsortorder.Substring(0, 10) = "Department" Then
                '    strDeptDivCode = Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim
                'ElseIf strsortorder.Substring(0, 8) = "Division" Then
                '    strDeptDivCode = Rs_Report.Tables(0).Rows(i).Item("DivisionCode").ToString.Trim
                'Else
                '    strDeptDivCode = Rs_Report.Tables(0).Rows(i).Item("Cat").
                'End If
                '.MoveNext()
                'If blnDeptAvailable And Not .EOF Then
                '    If Left(strsortorder, 10) = "Department" Then
                '        strCurrentDeptDivCode = !DepartmentCode
                '    ElseIf Left(strsortorder, 8) = "Division" Then
                '        strCurrentDeptDivCode = !DivisionCode
                '    Else
                '        strCurrentDeptDivCode = !Cat
                '    End If
                '    If strDeptDivCode <> strCurrentDeptDivCode Then
                '        rowcnt = rowcnt + 1
                '        If Left(strsortorder, 10) = "Department" Or frmSorting.optDeptEmpName.Value Then
                '            xlapp.Cells(rowcnt, 1) = "** Department Code & Name : " & !DepartmentCode & "  " & !DepartmentName
                '        ElseIf Left(strsortorder, 1) = "Division" Or frmSorting.optSectionPaycode.Value Then
                '            xlapp.Cells(rowcnt, 1) = "** Section Code & Name : " & !DivisionCode & "  " & !DivisionName
                '        Else
                '            xlapp.Cells(rowcnt, 1) = "** Category Code & Name : " & !Cat & "  " & !CatagoryName
                '        End If
                '        rowcnt = rowcnt + 1
                '        mintLine = mintLine + 3
                '    End If
                'End If
                '    Loop
            Next
            'End With
            xlwb.SaveAs(mstrFile_Name)
            'xlwb.SaveAs(App.Path & "\TimeWatch.xls")
        Else
            XtraMessageBox.Show(ulf, "<size=10>Records are not Processed</size>", "<size=9>iAS</size>")
            'mReportPrintStatus = False
            Exit Sub
        End If
        '        Exit Sub
        'ErrorGen:
        '        MsgBox(Err.Description, vbInformation + vbOKOnly, "Report Generation Information")
        '        Screen.MousePointer = vbDefault
    End Sub
    Sub DailyXl_TimeLoss(ByVal strsortorder As String)
        'On Error GoTo ErrorGen
        Dim xlapp As Excel.Application
        Dim xlwb As Excel.Workbook
        Dim xlst As Excel.Sheets
        Dim strDeptDivCode As String
        Dim strCurrentDeptDivCode As String
        Dim intFile As Integer
        Dim strsql As String, mCount As Integer
        Dim strOsDuration6 As String '* 6
        Dim strDepartmentCode As String
        Dim intPrintLineCounter As Integer
        Dim blnDeptAvailable As Boolean
        Dim rowcnt As Integer
        xlapp = CreateObject("Excel.Application")
        xlwb = xlapp.Workbooks.Add
        'If UCase(Dir(App.Path & "\TimeWatch.xls")) = UCase("TimeWatch.xls") Then
        '    Kill(App.Path & "\TimeWatch.xls")
        'End If
        mblnCheckReport = False
        mintPageNo = 1
        mintLine = 1
        If XtraShortOrder.g_SortOrder <> "" Then
            strsortorder = XtraShortOrder.g_SortOrder
        End If
        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If
        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".xls"

        'If Left(strsortorder, 1) = "D" Or Left(strsortorder, 1) = "C" Then blnDeptAvailable = True
        'If g_Report_view Then
        If Common.servername = "Access" Then
            'strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tblTimeRegister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tbltimeregister.ShiftAttended,tbltimeregister.ShiftStartTime,tbltimeregister.ShiftEndTime,tbltimeregister.EarlyArrival," & _
            ' "tbltimeregister.LateArrival,tbltimeregister.EarlyDeparture,tbltimeregister.Status,tbltimeregister.LunchLateArrival,tbltimeregister.LunchEarlyDeparture,tbltimeregister.ExcLunchHours,tbltimeregister.OtDuration," & _
            ' "tbltimeregister.TotalLossHrs,tbltimeregister.OsDuration,tbltimeregister.In1Mannual,tbltimeregister.In2Mannual,tbltimeregister.Out1Mannual,tbltimeregister.Out2Mannual,tblCalander.mDate,tblCalander.Process,tblCalander.NRTCProc" & _
            ' " from tblCatagory,tblDivision,  tbltimeregister,tblEmployee,tblCalander,tblCompany,tblDepartment" & _
            ' " Where tblCatagory.Cat = tblEmployee.Cat And tblDivision.DivisionCode = tblEmployee.DivisionCode And tblTimeRegister.totallosshrs > 0 and tbltimeregister.PayCode = tblEmployee.PayCode And FORMAT(tbltimeregister.DateOffice,'MMM DD YYYY') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' And FORMAT(tblCalander.mDate,'MMM DD YYYY') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
            ' " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
            ' " And (tblemployee.LeavingDate >=#" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "# or tblemployee.LeavingDate is null)" & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & strsortorder

            strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tblTimeRegister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tbltimeregister.ShiftAttended,tbltimeregister.ShiftStartTime,tbltimeregister.ShiftEndTime,tbltimeregister.EarlyArrival," & _
            "tbltimeregister.LateArrival,tbltimeregister.EarlyDeparture,tbltimeregister.Status,tbltimeregister.LunchLateArrival,tbltimeregister.LunchEarlyDeparture,tbltimeregister.ExcLunchHours,tbltimeregister.OtDuration," & _
            "tbltimeregister.TotalLossHrs,tbltimeregister.OsDuration,tbltimeregister.In1Mannual,tbltimeregister.In2Mannual,tbltimeregister.Out1Mannual,tbltimeregister.Out2Mannual,tblCalander.mDate,tblCalander.Process,tblCalander.NRTCProc" & _
            " from tblCatagory,tblDivision,  tbltimeregister,tblEmployee,tblCalander,tblCompany,tblDepartment" & _
            " Where tblCatagory.Cat = tblEmployee.Cat And tblTimeRegister.totallosshrs > 0 and tbltimeregister.PayCode = tblEmployee.PayCode And FORMAT(tbltimeregister.DateOffice,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' And FORMAT(tblCalander.mDate,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
            " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
            " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & strsortorder

            adapA = New OleDbDataAdapter(strsql, Common.con1)
            adapA.Fill(Rs_Report)
        Else
            strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tblTimeRegister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tbltimeregister.ShiftAttended,tbltimeregister.ShiftStartTime,tbltimeregister.ShiftEndTime,tbltimeregister.EarlyArrival," & _
             "tbltimeregister.LateArrival,tbltimeregister.EarlyDeparture,tbltimeregister.Status,tbltimeregister.LunchLateArrival,tbltimeregister.LunchEarlyDeparture,tbltimeregister.ExcLunchHours,tbltimeregister.OtDuration," & _
             "tbltimeregister.TotalLossHrs,tbltimeregister.OsDuration,tbltimeregister.In1Mannual,tbltimeregister.In2Mannual,tbltimeregister.Out1Mannual,tbltimeregister.Out2Mannual,tblCalander.mDate,tblCalander.Process,tblCalander.NRTCProc" & _
             " from tblCatagory,tblDivision,  tbltimeregister,tblEmployee,tblCalander,tblCompany,tblDepartment" & _
             " Where tblCatagory.Cat = tblEmployee.Cat And tblTimeRegister.totallosshrs > 0 and tbltimeregister.PayCode = tblEmployee.PayCode And tbltimeregister.DateOffice = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' And tblCalander.mDate = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
             " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
             " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & strsortorder
            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(Rs_Report)
        End If
        'Else
        '    strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tbltimeregisterD.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregisterD.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tbltimeregisterD.ShiftAttended,tbltimeregisterD.ShiftStartTime,tbltimeregisterD.ShiftEndTime,tbltimeregisterD.EarlyArrival," & _
        '         "tbltimeregisterD.LateArrival,tbltimeregisterD.EarlyDeparture,tbltimeregisterD.Status,tbltimeregisterD.LunchLateArrival,tbltimeregisterD.LunchEarlyDeparture,tbltimeregisterD.ExcLunchHours,tbltimeregisterD.OtDuration," & _
        '         "tbltimeregisterD.TotalLossHrs,tbltimeregisterD.OsDuration,tbltimeregisterD.In1Mannual,tbltimeregisterD.In2Mannual,tbltimeregisterD.Out1Mannual,tbltimeregisterD.Out2Mannual,tblCalander.mDate,tblCalander.Process,tblCalander.NRTCProc" & _
        '         " from tblCatagory,tblDivision,  tbltimeregisterD,tblEmployee,tblCalander,tblCompany,tblDepartment" & _
        '         " Where tblCatagory.Cat = tblEmployee.Cat And tblDivision.DivisionCode = tblEmployee.DivisionCode And tbltimeregisterD.totallosshrs > 0 and tbltimeregisterD.PayCode = tblEmployee.PayCode And tbltimeregisterD.DateOffice = '" & Format(frmDailyAttReport.txtFromDate.Value, "MMM dd yyyy") & "' And tblCalander.mDate = '" & Format(frmDailyAttReport.txtFromDate.Value, "MMM dd yyyy") & "'" & _
        '         " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
        '         " And (tblemployee.LeavingDate>='" & Format(frmDailyAttReport.txtFromDate, "MMM dd yyyy") & "' or tblemployee.LeavingDate is null)" & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause)
        'End If
        'Rs_Report = Cn.Execute(strsql)
        If Rs_Report.Tables(0).Rows.Count < 1 Then
            'G_Report_Error = False
            'Screen.MousePointer = vbDefault
            XtraMessageBox.Show(ulf, "<size=10>There is no Time Loss for any Employee on this Date.</size>", "<size=9>iAS</size>")
            'mReportPrintStatus = False
            Exit Sub
        End If
        If strsortorder = "" Then
            'Rs_Report.Sort = "PayCode"
        Else
            'Rs_Report.Sort = strsortorder
        End If

        'If IsNull(Rs_Report!Process) And IsNull(Rs_Report!NRTCProc) Then
        ' MsgBox "Records are not Processed", vbInformation + vbExclamation, "TimeWatch  Information"
        ' mReportPrintStatus = False
        ' Exit Sub
        'ElseIf InStr("YX", Rs_Report("Process")) > 0 Or InStr("Y", Rs_Report("nRTCProc")) > 0 Then

        rowcnt = 1
        xlapp.Visible = True
        xlapp.Columns.ColumnWidth = 9
        xlapp.Columns.Font.Name = "TAHOMA"
        xlapp.Columns.Font.Size = 8
        xlapp.Columns.Font.ColorIndex = 1 'vbBlack
        xlapp.Cells(rowcnt, 5).Font.Bold = True
        xlapp.Cells(rowcnt, 5) = "Company Name:" & CommonReport.g_CompanyNames
        rowcnt = rowcnt + 1
        xlapp.Cells(rowcnt, 5).Font.Bold = True
        xlapp.Cells(rowcnt, 5) = "Run Date & Time :" & Now.ToString("dd/MM/yyyy HH:mm")
        rowcnt = rowcnt + 2
        xlapp.Cells(rowcnt, 5).Font.Bold = True
        xlapp.Cells(rowcnt, 5) = "TIME LOSS FOR DATE : " & DateEdit1.DateTime.ToString("dd/MM/yyyy HH:mm")
        rowcnt = rowcnt + 1

        xlapp.Range("A5:S5").Font.Bold = True
        xlapp.Range("A5:S5").Font.ColorIndex = 2 'vbWhite
        'xlapp.Range("A5:S5").HorizontalAlignment = xlLeft
        xlapp.Range("A5:S5").Interior.ColorIndex = 51
        xlapp.Range("A5:S5").Borders.LineStyle = XlLineStyle.xlContinuous

        xlapp.Cells(rowcnt, 1) = "Sr.No."
        xlapp.Cells(rowcnt, 2) = "PayCode"
        xlapp.Cells(rowcnt, 3) = "Card No"
        xlapp.Cells(rowcnt, 4) = "Employee Name"
        xlapp.Cells(rowcnt, 6) = "Shift "
        xlapp.Cells(rowcnt, 7) = "Start "
        xlapp.Cells(rowcnt, 8) = "End"
        xlapp.Cells(rowcnt, 9) = "Status  "
        xlapp.Cells(rowcnt, 10) = "Early Arriv."
        xlapp.Cells(rowcnt, 11) = "Shift Late "
        xlapp.Cells(rowcnt, 12) = "Early Departure"
        xlapp.Cells(rowcnt, 13) = "Lunch Late "
        xlapp.Cells(rowcnt, 14) = "Lunch Early"
        xlapp.Cells(rowcnt, 15) = "Excess Lunch "
        xlapp.Cells(rowcnt, 16) = "OT "
        xlapp.Cells(rowcnt, 17) = "Loss Hours "
        xlapp.Cells(rowcnt, 18) = "Over Stay"
        xlapp.Cells(rowcnt, 19) = "Manual "
        rowcnt = rowcnt + 1
        mintLine = 9
        mCount = 0
        'With Rs_Report
        'If blnDeptAvailable And Not .EOF Then
        '    rowcnt = rowcnt + 1
        '    If Left(strsortorder, 10) = "Department" Or frmSorting.optDeptEmpName.Value Then
        '        xlapp.Cells(rowcnt, 1) = "** Department Code & Name : " & !DepartmentCode & "  " & !DepartmentName
        '    ElseIf Left(strsortorder, 8) = "Division" Or frmSorting.optSectionPaycode.Value Then
        '        xlapp.Cells(rowcnt, 1) = "** Section Code & Name : " & !DivisionCode & "  " & !DivisionName
        '    Else
        '        xlapp.Cells(rowcnt, 1) = "** Category Code & Name : " & !Cat & "  " & !CatagoryName
        '    End If
        '    rowcnt = rowcnt + 1
        '    mintLine = mintLine + 3
        'End If

        'Do While Not .EOF
        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            strOsDuration6 = 0

            If Rs_Report.Tables(0).Rows(i).Item("OsDuration").ToString.Trim <> "" Or Rs_Report.Tables(0).Rows(i).Item("OsDuration").ToString.Trim <> "0" Then
                strOsDuration6 = Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("OsDuration").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("OsDuration").ToString.Trim Mod 60).ToString("00")
            Else
                strOsDuration6 = ""
            End If

            rowcnt = rowcnt + 1
            mCount = mCount + 1
            xlapp.Cells(rowcnt, 1).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 1) = mCount
            xlapp.Cells(rowcnt, 2).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 2) = "'" & Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim
            xlapp.Cells(rowcnt, 3).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 3) = "'" & Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim
            xlapp.Cells(rowcnt, 4).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 4) = Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim
            xlapp.Cells(rowcnt, 5).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 6).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 6) = Rs_Report.Tables(0).Rows(i).Item("ShiftAttended").ToString.Trim
            xlapp.Cells(rowcnt, 7).Borders.LineStyle = XlLineStyle.xlContinuous
            If Rs_Report.Tables(0).Rows(i).Item("shiftStartTime").ToString.Trim = "" Then
                xlapp.Cells(rowcnt, 7) = ""
            Else
                xlapp.Cells(rowcnt, 7) = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("shiftStartTime").ToString.Trim).ToString("HH:mm")
            End If
            'xlapp.Cells(rowcnt, 7) = Format(!shiftStartTime, "hh:mm")
            xlapp.Cells(rowcnt, 8).Borders.LineStyle = XlLineStyle.xlContinuous
            If Rs_Report.Tables(0).Rows(i).Item("shiftEndTime").ToString.Trim = "" Then
                xlapp.Cells(rowcnt, 8) = ""
            Else
                xlapp.Cells(rowcnt, 8) = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("shiftEndTime").ToString.Trim).ToString("HH:mm")
            End If
            'xlapp.Cells(rowcnt, 8) = Format(!shiftEndTime, "hh:mm")
            xlapp.Cells(rowcnt, 9).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 9) = Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim

            xlapp.Cells(rowcnt, 10).Borders.LineStyle = XlLineStyle.xlContinuous
            If Rs_Report.Tables(0).Rows(i).Item("EARLYARRIVAL").ToString.Trim = "0" Then
                xlapp.Cells(rowcnt, 10) = ""
            Else
                xlapp.Cells(rowcnt, 10) = "'" & Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("EARLYARRIVAL").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("EARLYARRIVAL").ToString.Trim Mod 60).ToString("00")
            End If
            'xlapp.Cells(rowcnt, 10) = "'" & Allign(Min2Hr(!EARLYARRIVAL))

            xlapp.Cells(rowcnt, 11).Borders.LineStyle = XlLineStyle.xlContinuous
            If Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim = "0" Then
                xlapp.Cells(rowcnt, 11) = ""
            Else
                xlapp.Cells(rowcnt, 11) = "'" & Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim Mod 60).ToString("00")
            End If
            'xlapp.Cells(rowcnt, 11) = "'" & Allign(Min2Hr(!latearrival))

            xlapp.Cells(rowcnt, 12).Borders.LineStyle = XlLineStyle.xlContinuous
            If Rs_Report.Tables(0).Rows(i).Item("earlydeparture").ToString.Trim = "0" Then
                xlapp.Cells(rowcnt, 12) = ""
            Else
                xlapp.Cells(rowcnt, 12) = "'" & Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("earlydeparture").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("earlydeparture").ToString.Trim Mod 60).ToString("00")
            End If
            'xlapp.Cells(rowcnt, 12) = "'" & Allign(Min2Hr(!earlydeparture))

            xlapp.Cells(rowcnt, 13).Borders.LineStyle = XlLineStyle.xlContinuous
            If Rs_Report.Tables(0).Rows(i).Item("LunchLateArrival").ToString.Trim = "0" Then
                xlapp.Cells(rowcnt, 13) = ""
            Else
                xlapp.Cells(rowcnt, 13) = "'" & Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("LunchLateArrival").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("LunchLateArrival").ToString.Trim Mod 60).ToString("00")
            End If
            'xlapp.Cells(rowcnt, 13) = "'" & Allign(Min2Hr(!LunchLateArrival))

            xlapp.Cells(rowcnt, 14).Borders.LineStyle = XlLineStyle.xlContinuous
            If Rs_Report.Tables(0).Rows(i).Item("LUNCHEARLYDEPARTURE").ToString.Trim = "0" Then
                xlapp.Cells(rowcnt, 14) = ""
            Else
                xlapp.Cells(rowcnt, 14) = "'" & Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("LUNCHEARLYDEPARTURE").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("LUNCHEARLYDEPARTURE").ToString.Trim Mod 60).ToString("00")
            End If
            'xlapp.Cells(rowcnt, 14) = "'" & Allign(Min2Hr(!LUNCHEARLYDEPARTURE))

            xlapp.Cells(rowcnt, 15).Borders.LineStyle = XlLineStyle.xlContinuous
            If Rs_Report.Tables(0).Rows(i).Item("EXCLUNCHHOURS").ToString.Trim = "0" Then
                xlapp.Cells(rowcnt, 15) = ""
            Else
                xlapp.Cells(rowcnt, 15) = "'" & Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("EXCLUNCHHOURS").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("EXCLUNCHHOURS").ToString.Trim Mod 60).ToString("00")
            End If
            'xlapp.Cells(rowcnt, 15) = "'" & Allign(Min2Hr(!EXCLUNCHHOURS))

            xlapp.Cells(rowcnt, 16).Borders.LineStyle = XlLineStyle.xlContinuous
            If Rs_Report.Tables(0).Rows(i).Item("OtDuration").ToString.Trim = "0" Then
                xlapp.Cells(rowcnt, 16) = ""
            Else
                xlapp.Cells(rowcnt, 16) = "'" & Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("OtDuration").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("OtDuration").ToString.Trim Mod 60).ToString("00")
            End If
            'xlapp.Cells(rowcnt, 16) = "'" & Allign(Min2Hr(!OtDuration))

            xlapp.Cells(rowcnt, 17).Borders.LineStyle = XlLineStyle.xlContinuous
            If Rs_Report.Tables(0).Rows(i).Item("TotalLossHrs").ToString.Trim = "0" Then
                xlapp.Cells(rowcnt, 17) = ""
            Else
                xlapp.Cells(rowcnt, 17) = "'" & Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("TotalLossHrs").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("TotalLossHrs").ToString.Trim Mod 60).ToString("00")
            End If
            'xlapp.Cells(rowcnt, 17) = "'" & Allign(Min2Hr(!TotalLossHrs))
            xlapp.Cells(rowcnt, 18).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 18) = "'" & (strOsDuration6)
            xlapp.Cells(rowcnt, 19).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 19) = IIf(UCase(Rs_Report.Tables(0).Rows(i).Item("IN1MANNUAL").ToString.Trim) = "Y", "M", IIf(UCase(Rs_Report.Tables(0).Rows(i).Item("Out1Mannual").ToString.Trim) = "Y", "M", IIf(UCase(Rs_Report.Tables(0).Rows(i).Item("IN2MANNUAL").ToString.Trim) = "Y", "M", IIf(UCase(Rs_Report.Tables(0).Rows(i).Item("OUT2MANNUAL").ToString.Trim) = "Y", "M", ""))))
            rowcnt = rowcnt + 1

            mintLine = mintLine + 1
            mblnCheckReport = True
            'If Left(strsortorder, 10) = "Department" Then
            '    strDeptDivCode = !DepartmentCode
            'ElseIf Left(strsortorder, 8) = "Division" Then
            '    strDeptDivCode = !DivisionCode
            'Else
            '    strDeptDivCode = !Cat
            'End If
            '.MoveNext()
            'If blnDeptAvailable And Not .EOF Then
            '    If Left(strsortorder, 10) = "Department" Then
            '        strCurrentDeptDivCode = !DepartmentCode
            '    ElseIf Left(strsortorder, 8) = "Division" Then
            '        strCurrentDeptDivCode = !DivisionCode
            '    Else
            '        strCurrentDeptDivCode = !Cat
            '    End If
            '    If strDeptDivCode <> strCurrentDeptDivCode Then
            '        rowcnt = rowcnt + 1
            '        If Left(strsortorder, 10) = "Department" Or frmSorting.optDeptEmpName.Value Then
            '            xlapp.Cells(rowcnt, 1) = "** Department Code & Name : " & !DepartmentCode & "  " & !DepartmentName
            '        ElseIf Left(strsortorder, 1) = "Division" Or frmSorting.optSectionPaycode.Value Then
            '            xlapp.Cells(rowcnt, 1) = "** Section Code & Name : " & !DivisionCode & "  " & !DivisionName
            '        Else
            '            xlapp.Cells(rowcnt, 1) = "** Category Code & Name : " & !Cat & "  " & !CatagoryName
            '        End If
            '        rowcnt = rowcnt + 1
            '        mintLine = mintLine + 3
            '    End If
            'End If
            'Loop
        Next
        'End With

        '        xlwb.SaveAs(App.Path & "\TimeWatch.xls")
        '        Exit Sub
        'ErrorGen:
        '        MsgBox(Err.Description, vbInformation + vbOKOnly, "Report Generation Information")
        '        Screen.MousePointer = vbDefault
        xlwb.SaveAs(mstrFile_Name)
    End Sub
    Sub DailyXl_OverTimeRegister(ByVal strsortorder As String)
        ' On Error GoTo ErrorGen
        Dim strDeptDivCode As String
        Dim strCurrentDeptDivCode As String
        Dim intFile As Integer
        Dim strsql As String, mCount As Integer
        Dim strOtAmount11 As String '* 11
        Dim strDepartmentCode As String
        Dim intPrintLineCounter As Integer
        Dim blnDeptAvailable As Boolean
        Dim xlapp As Excel.Application
        Dim xlwb As Excel.Workbook
        Dim xlst As Excel.Sheets
        Dim rowcnt As Integer
        xlapp = CreateObject("Excel.Application")
        xlwb = xlapp.Workbooks.Add
        'If UCase(Dir(App.Path & "\TimeWatch.xls")) = UCase("TimeWatch.xls") Then
        '    Kill(App.Path & "\TimeWatch.xls")
        'End If

        mblnCheckReport = False
        mintPageNo = 1
        mintLine = 1
        If XtraShortOrder.g_SortOrder <> "" Then
            strsortorder = XtraShortOrder.g_SortOrder
        End If
        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If
        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".xls"

        'If Left(strsortorder, 1) = "D" Or Left(strsortorder, 1) = "C" Then blnDeptAvailable = True
        'If g_Report_view Then
        If Common.servername = "Access" Then
            'strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tblTimeRegister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tbltimeregister.OtDuration,tbltimeregister.OtAmount,tblCalander.mDate,tblCalander.Process,tblCalander.NRTCProc" & _
            ' " from tblCatagory,tblDivision,  tbltimeregister,tblEmployee,tblCalander,tblCompany,tblDepartment" & _
            ' " Where tblCatagory.Cat = tblEmployee.Cat And tblDivision.DivisionCode = tblEmployee.DivisionCode And tbltimeregister.OtDuration > 0 and tbltimeregister.PayCode = tblEmployee.PayCode And FORMAT(tbltimeregister.DateOffice,'MMM DD YYYY') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' And FORMAT(tblCalander.mDate,'MMM DD YYYY') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
            ' " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
            ' " And (tblemployee.LeavingDate >=#" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "# or tblemployee.LeavingDate is null)" & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & strsortorder

            strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tblTimeRegister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tbltimeregister.OtDuration,tbltimeregister.OtAmount,tblCalander.mDate,tblCalander.Process,tblCalander.NRTCProc" & _
           " from tblCatagory,tblDivision,  tbltimeregister,tblEmployee,tblCalander,tblCompany,tblDepartment" & _
           " Where tblCatagory.Cat = tblEmployee.Cat And tbltimeregister.OtDuration > 0 and tbltimeregister.PayCode = tblEmployee.PayCode And FORMAT(tbltimeregister.DateOffice,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' And FORMAT(tblCalander.mDate,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
           " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
           " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & strsortorder

            adapA = New OleDbDataAdapter(strsql, Common.con1)
            adapA.Fill(Rs_Report)
        Else
            strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tblTimeRegister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tbltimeregister.OtDuration,tbltimeregister.OtAmount,tblCalander.mDate,tblCalander.Process,tblCalander.NRTCProc" & _
             " from tblCatagory,tblDivision,  tbltimeregister,tblEmployee,tblCalander,tblCompany,tblDepartment" & _
             " Where tblCatagory.Cat = tblEmployee.Cat And tbltimeregister.OtDuration > 0 and tbltimeregister.PayCode = tblEmployee.PayCode And tbltimeregister.DateOffice = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' And tblCalander.mDate = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
             " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
             " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & strsortorder
            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(Rs_Report)
        End If
        'Else
        '    strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tbltimeregisterD.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregisterD.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tbltimeregisterD.OtDuration,tbltimeregisterD.OtAmount,tblCalander.mDate,tblCalander.Process,tblCalander.NRTCProc" & _
        '         " from tblCatagory,tblDivision,  tbltimeregisterD,tblEmployee,tblCalander,tblCompany,tblDepartment" & _
        '         " Where tblCatagory.Cat = tblEmployee.Cat And tblDivision.DivisionCode = tblEmployee.DivisionCode And tbltimeregisterD.OtDuration > 0 and tbltimeregisterD.PayCode = tblEmployee.PayCode And tbltimeregisterD.DateOffice = '" & Format(frmDailyAttReport.txtFromDate.Value, "MMM dd yyyy") & "' And tblCalander.mDate = '" & Format(frmDailyAttReport.txtFromDate.Value, "MMM dd yyyy") & "'" & _
        '         " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
        '         " And (tblemployee.LeavingDate>='" & Format(frmDailyAttReport.txtFromDate, "MMM dd yyyy") & "' or tblemployee.LeavingDate is null)" & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause)
        'End If
        'Rs_Report = Cn.Execute(strsql)
        If Rs_Report.Tables(0).Rows.Count < 1 Then
            'G_Report_Error = False
            'Screen.MousePointer = vbDefault
            XtraMessageBox.Show(ulf, "<size=10>There is no Over Time for any Employee on this Date.</size>", "<size=9>iAS</size>")
            'mReportPrintStatus = False
            Exit Sub
        End If
        If strsortorder = "" Then
            'Rs_Report.Sort = "PayCode"
        Else
            'Rs_Report.Sort = strsortorder
        End If


        'If IsNull(Rs_Report!Process) And IsNull(Rs_Report!NRTCProc) Then
        '   MsgBox "Records are not Processed", vbInformation + vbExclamation, "TimeWatch  Information"
        '   mReportPrintStatus = False
        '   Exit Sub
        'ElseIf InStr("YX", Rs_Report("Process")) > 0 Or InStr("Y", Rs_Report("nRTCProc")) > 0 Then

        rowcnt = 1
        xlapp.Visible = True
        xlapp.Columns.ColumnWidth = 9
        xlapp.Columns.Font.Name = "TAHOMA"
        xlapp.Columns.Font.Size = 8
        xlapp.Columns.Font.ColorIndex = 1 'vbBlack
        xlapp.Cells(rowcnt, 4).Font.Bold = True
        xlapp.Cells(rowcnt, 4) = "Company Name:" & CommonReport.g_CompanyNames
        rowcnt = rowcnt + 1
        xlapp.Cells(rowcnt, 4).Font.Bold = True
        xlapp.Cells(rowcnt, 4) = "Run Date & Time :" & Now.ToString("dd/MM/yyyy HH:mm")
        rowcnt = rowcnt + 2
        xlapp.Cells(rowcnt, 5).Font.Bold = True
        xlapp.Cells(rowcnt, 5) = "OVER TIME FOR DATE : " & DateEdit1.DateTime.ToString("dd/MM/yyyy")
        rowcnt = rowcnt + 1
        rowcnt = rowcnt + 1

        xlapp.Range("A6:G6").Font.Bold = True
        xlapp.Range("A6:G6").Font.ColorIndex = 2 ' vbWhite
        'xlapp.Range("A6:G6").HorizontalAlignment = xlLeft
        xlapp.Range("A6:G6").Interior.ColorIndex = 51
        xlapp.Range("A6:G6").Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 1) = "   Sr.No."
        xlapp.Cells(rowcnt, 2) = "PayCode"
        xlapp.Cells(rowcnt, 3) = "Card No"
        xlapp.Cells(rowcnt, 4) = "Employee Name"
        xlapp.Cells(rowcnt, 6) = "     Ot"
        xlapp.Cells(rowcnt, 7) = "OT Amount "
        '    rowcnt = rowcnt + 1

        mintLine = 9
        mCount = 0
        'With Rs_Report
        'If blnDeptAvailable And Not .EOF Then
        '    rowcnt = rowcnt + 1
        '    If Left(strsortorder, 10) = "Department" Or frmSorting.optDeptEmpName.Value Then
        '        xlapp.Cells(rowcnt, 1) = "** Department Code & Name : " & !DepartmentCode & "  " & !DepartmentName
        '    ElseIf Left(strsortorder, 8) = "Division" Or frmSorting.optSectionPaycode.Value Then
        '        xlapp.Cells(rowcnt, 1) = "** Section Code & Name : " & !DivisionCode & "  " & !DivisionName
        '    Else
        '        xlapp.Cells(rowcnt, 1) = "** Category Code & Name : " & !Cat & "  " & !CatagoryName
        '    End If
        '    rowcnt = rowcnt + 1
        '    mintLine = mintLine + 3
        'End If
        'Do While Not .EOF
        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            strOtAmount11 = 0
            If Rs_Report.Tables(0).Rows(i).Item("OTAmount").ToString.Trim <> "" Then If Rs_Report.Tables(0).Rows(i).Item("OTAmount").ToString.Trim <> "0" Then strOtAmount11 = Trim(Length7(CStr(Rs_Report.Tables(0).Rows(i).Item("OTAmount").ToString.Trim)))
            rowcnt = rowcnt + 1
            mCount = mCount + 1
            xlapp.Cells(rowcnt, 1).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 1) = mCount
            xlapp.Cells(rowcnt, 2).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 2) = "'" & Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim
            xlapp.Cells(rowcnt, 3).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 3) = "'" & Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim
            xlapp.Cells(rowcnt, 4).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 4) = Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim
            xlapp.Cells(rowcnt, 5).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 6).Borders.LineStyle = XlLineStyle.xlContinuous
            If Rs_Report.Tables(0).Rows(i).Item("OtDuration").ToString.Trim = "0" Then
                xlapp.Cells(rowcnt, 6) = ""
            Else
                xlapp.Cells(rowcnt, 6) = "'" & Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("OtDuration").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("OtDuration").ToString.Trim Mod 60).ToString("00")
            End If
            'xlapp.Cells(rowcnt, 6) = "'" & Allign(Min2Hr(!OtDuration))
            xlapp.Cells(rowcnt, 7).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 7) = "'" & (strOtAmount11)

            mintLine = mintLine + 1
            mblnCheckReport = True
            'If Left(strsortorder, 10) = "Department" Then
            '    strDeptDivCode = !DepartmentCode
            'ElseIf Left(strsortorder, 8) = "Division" Then
            '    strDeptDivCode = !DivisionCode
            'Else
            '    strDeptDivCode = !Cat
            'End If
            '.MoveNext()
            'If blnDeptAvailable And Not .EOF Then
            '    If Left(strsortorder, 10) = "Department" Then
            '        strCurrentDeptDivCode = !DepartmentCode
            '    ElseIf Left(strsortorder, 8) = "Division" Then
            '        strCurrentDeptDivCode = !DivisionCode
            '    Else
            '        strCurrentDeptDivCode = !Cat
            '    End If
            '    If strDeptDivCode <> strCurrentDeptDivCode Then
            '        rowcnt = rowcnt + 1
            '        If Left(strsortorder, 10) = "Department" Or frmSorting.optDeptEmpName.Value Then
            '            xlapp.Cells(rowcnt, 1) = "** Department Code & Name : " & !DepartmentCode & "  " & !DepartmentName
            '        ElseIf Left(strsortorder, 1) = "Division" Or frmSorting.optSectionPaycode.Value Then
            '            xlapp.Cells(rowcnt, 1) = "** Section Code & Name : " & !DivisionCode & "  " & !DivisionName
            '        Else
            '            xlapp.Cells(rowcnt, 1) = "** Category Code & Name : " & !Cat & "  " & !CatagoryName
            '        End If
            '        rowcnt = rowcnt + 1
            '        mintLine = mintLine + 3
            '    Else
            '        rowcnt = rowcnt + 1
            '        If Left(strsortorder, 10) = "Department" Or frmSorting.optDeptEmpName.Value Then
            '            xlapp.Cells(rowcnt, 1) = "** Department Code & Name : " & !DepartmentCode & "  " & !DepartmentName
            '        ElseIf Left(strsortorder, 1) = "Division" Or frmSorting.optSectionPaycode.Value Then
            '            xlapp.Cells(rowcnt, 1) = "** Section Code & Name : " & !DivisionCode & "  " & !DivisionName
            '        Else
            '            xlapp.Cells(rowcnt, 1) = "** Category Code & Name : " & !Cat & "  " & !CatagoryName
            '        End If
            '        rowcnt = rowcnt + 1
            '        mintLine = mintLine + 3
            '    End If
            'End If
            'Loop
        Next
        'End With

        '        xlwb.SaveAs(App.Path & "\TimeWatch.xls")
        '        Exit Sub
        'ErrorGen:
        '        MsgBox(Err.Description, vbInformation + vbOKOnly, "Report Generation Information")
        '        Screen.MousePointer = vbDefault
        xlwb.SaveAs(mstrFile_Name)
    End Sub
    Sub DailyXl_OverTimeSummary()
        'On Error GoTo ErrorGen
        Dim xlapp As Excel.Application
        Dim xlwb As Excel.Workbook
        Dim xlst As Excel.Sheets
        Dim intFile As Integer
        Dim strsql As String, mCount As Integer
        Dim strDepartmentName As String
        Dim strDepartmentName45 As String '* 45
        Dim strOtDuration10 As String '* 10
        Dim strOtAmount11 As String '* 11
        Dim dblOtDuration As Double
        Dim dblOtAmount As Double

        Dim rowcnt As Integer
        xlapp = CreateObject("Excel.Application")
        xlwb = xlapp.Workbooks.Add
        'If UCase(Dir(App.Path & "\TimeWatch.xls")) = UCase("TimeWatch.xls") Then
        '    Kill(App.Path & "\TimeWatch.xls")
        'End If

        'If XtraShortOrder.g_SortOrder <> "" Then
        '    strsortorder = XtraShortOrder.g_SortOrder
        'End If
        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If
        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".xls"

        mblnCheckReport = False
        mintPageNo = 1
        mintLine = 1
        'If g_Report_view Then
        If Common.servername = "Access" Then
            'strsql = "select tblCatagory.CatagoryName, tblTimeRegister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregister.PayCode,tbltimeregister.OtDuration,tbltimeregister.OtAmount,tblDepartment.DepartmentName,tblCalander.mDate,tblCalander.Process,tblCalander.NRTCProc" & _
            ' " from tblCatagory,tbltimeregister,tblDepartment,tblCalander,tblEmployee " & _
            ' " Where tblCatagory.Cat = tblEmployee.Cat And tblTimeRegister.OtDuration > 0 And tbltimeregister.PayCode = tblEmployee.PayCode And TblEmployee.DepartmentCode = TblDepartment.DepartmentCode And FORMAT(tbltimeregister.DateOffice,'DD-MMM-YYYY') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' And FORMAT(tblCalander.mDate,'DD-MMM-YYYY') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
            ' " And (tblemployee.LeavingDate >=#" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "# or tblemployee.LeavingDate is null)"

            strsql = "select tblCatagory.CatagoryName, tblTimeRegister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregister.PayCode,tbltimeregister.OtDuration,tbltimeregister.OtAmount,tblDepartment.DepartmentName,tblCalander.mDate,tblCalander.Process,tblCalander.NRTCProc" & _
             " from tblCatagory,tbltimeregister,tblDepartment,tblCalander,tblEmployee " & _
             " Where tblCatagory.Cat = tblEmployee.Cat And tblTimeRegister.OtDuration > 0 And tbltimeregister.PayCode = tblEmployee.PayCode And TblEmployee.DepartmentCode = TblDepartment.DepartmentCode And FORMAT(tbltimeregister.DateOffice,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' And FORMAT(tblCalander.mDate,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
             " "

            adapA = New OleDbDataAdapter(strsql, Common.con1)
            adapA.Fill(Rs_Report)
        Else
            strsql = "select tblCatagory.CatagoryName, tblTimeRegister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregister.PayCode,tbltimeregister.OtDuration,tbltimeregister.OtAmount,tblDepartment.DepartmentName,tblCalander.mDate,tblCalander.Process,tblCalander.NRTCProc" & _
             " from tblCatagory,tbltimeregister,tblDepartment,tblCalander,tblEmployee " & _
             " Where tblCatagory.Cat = tblEmployee.Cat And tblTimeRegister.OtDuration > 0 And tbltimeregister.PayCode = tblEmployee.PayCode And TblEmployee.DepartmentCode = TblDepartment.DepartmentCode And tbltimeregister.DateOffice = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' And tblCalander.mDate = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
             " "
            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(Rs_Report)
        End If
        'Else
        'strsql = "select tblCatagory.CatagoryName, tbltimeregisterD.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregisterD.PayCode,tbltimeregisterD.OtDuration,tbltimeregisterD.OtAmount,tblDepartment.DepartmentName,tblCalander.mDate,tblCalander.Process,tblCalander.NRTCProc" & _
        '     " from tblCatagory,tbltimeregisterD,tblDepartment,tblCalander,tblEmployee " & _
        '     " Where tblCatagory.Cat = tblEmployee.Cat And tbltimeregisterD.OtDuration > 0 And tbltimeregisterD.PayCode = tblEmployee.PayCode And TblEmployee.DepartmentCode = TblDepartment.DepartmentCode And tbltimeregisterD.DateOffice = '" & Format(frmDailyAttReport.txtFromDate.Value, "dd-MMM-yyyy") & "' And tblCalander.mDate = '" & Format(frmDailyAttReport.txtFromDate.Value, "dd-MMM-yyyy") & "'" & _
        '     " And (tblemployee.LeavingDate>='" & Format(frmDailyAttReport.txtFromDate, "MMM dd yyyy") & "' or tblemployee.LeavingDate is null)"
        'End If
        'Rs_Report = Cn.Execute(strsql)

        If Rs_Report.Tables(0).Rows.Count < 1 Then
            'G_Report_Error = False
            'Screen.MousePointer = vbDefault
            XtraMessageBox.Show(ulf, "<size=10>There is no Over Time for any Employee on this Date.</size>", "<size=9>iAS</size>")
            'mReportPrintStatus = False
            Exit Sub
        End If

        'Rs_Report.Sort = "DepartmentName"

        rowcnt = 1
        xlapp.Visible = True
        xlapp.Columns.Font.Name = "TAHOMA"
        xlapp.Columns.Font.Size = 8
        xlapp.Columns.Font.ColorIndex = 1 'vbBlack

        xlapp.Columns.ColumnWidth = 9
        xlapp.Cells(rowcnt, 3).Font.Bold = True
        xlapp.Cells(rowcnt, 3) = "Company Name:" & CommonReport.g_CompanyNames
        rowcnt = rowcnt + 1
        xlapp.Cells(rowcnt, 3).Font.Bold = True
        xlapp.Cells(rowcnt, 3) = "Run Date & Time :" & Now.ToString("dd/MM/yyyy HH:mm")
        rowcnt = rowcnt + 2
        xlapp.Cells(rowcnt, 3).Font.Bold = True
        xlapp.Cells(rowcnt, 3) = "OVER TIME SUMMARY FOR DATE : " & DateEdit1.DateTime.ToString("yyyy-MM-dd")
        rowcnt = rowcnt + 1
        rowcnt = rowcnt + 1


        xlapp.Range("A6:F6").Font.Bold = True
        xlapp.Range("A6:F6").Font.ColorIndex = 2 'vbWhite
        'xlapp.Range("A6:F6").HorizontalAlignment = xlLeft
        xlapp.Range("A6:F6").Interior.ColorIndex = 51
        xlapp.Range("A6:F6").Borders.LineStyle = XlLineStyle.xlContinuous

        xlapp.Cells(rowcnt, 1) = "   Sr.No."
        xlapp.Cells(rowcnt, 2) = "Department"
        xlapp.Cells(rowcnt, 5) = "      Ot "
        xlapp.Cells(rowcnt, 6) = "OT Amount "
        'rowcnt = rowcnt + 1

        mintLine = 9
        mCount = 0
        'With Rs_Report
        'Do While Not .EOF
        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            strDepartmentName = Rs_Report.Tables(0).Rows(i).Item("DepartmentName").ToString.Trim
            dblOtDuration = 0
            dblOtAmount = 0
            strOtDuration10 = 0
            strOtAmount11 = 0
            Do While strDepartmentName = Rs_Report.Tables(0).Rows(i).Item("DepartmentName").ToString.Trim
                If Rs_Report.Tables(0).Rows(i).Item("OtDuration").ToString.Trim <> "" Or Rs_Report.Tables(0).Rows(i).Item("OtDuration").ToString.Trim <> "0" Then dblOtDuration = CDbl(Rs_Report.Tables(0).Rows(i).Item("OtDuration").ToString.Trim) + dblOtDuration
                If Rs_Report.Tables(0).Rows(i).Item("OTAmount").ToString.Trim <> "" Or Rs_Report.Tables(0).Rows(i).Item("OTAmount").ToString.Trim <> "0" Then dblOtAmount = CDbl(Rs_Report.Tables(0).Rows(i).Item("OTAmount").ToString.Trim) + dblOtAmount
                'If (Not IsNull(!OTAmount)) Or (!OTAmount <> 0) Then dblOtAmount = CDbl(!OTAmount) + dblOtAmount
                '.MoveNext()
                'If .EOF Then
                '    Exit Do
                'End If
                i = i + 1
                If i >= Rs_Report.Tables(0).Rows.Count - 1 Then
                    i = i - 1
                    Exit Do
                End If
            Loop
            strOtDuration10 = 0
            strOtAmount11 = 0
            If dblOtDuration <> 0 Then strOtDuration10 = Math.Truncate(dblOtDuration / 60).ToString("00") & ":" & (dblOtDuration Mod 60).ToString("00")

            If dblOtAmount.ToString.Trim <> "" Then If dblOtAmount <> 0 Then strOtAmount11 = Trim(Length7(CStr(dblOtAmount)))
            strDepartmentName45 = strDepartmentName
            rowcnt = rowcnt + 1
            mCount = mCount + 1
            xlapp.Cells(rowcnt, 1).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 1) = mCount
            xlapp.Cells(rowcnt, 2).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 2) = strDepartmentName
            xlapp.Cells(rowcnt, 3).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 4).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 5).Borders.LineStyle = XlLineStyle.xlContinuous

            xlapp.Cells(rowcnt, 5) = "'" & (strOtDuration10)
            xlapp.Cells(rowcnt, 6).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 6) = "'" & (strOtAmount11)
            rowcnt = rowcnt + 1
            mintLine = mintLine + 1
            mblnCheckReport = True
            If mintLine >= g_LinesPerPage Then  'And Not .EOF
                mintPageNo = mintPageNo + 1
                rowcnt = 1
                xlapp.Visible = True

                xlapp.Columns.ColumnWidth = 9
                xlapp.Cells(rowcnt, 5) = "Company Name:" & CommonReport.g_CompanyNames
                rowcnt = rowcnt + 1
                xlapp.Cells(rowcnt, 5) = "Run Date & Time :" & Now.ToString("dd/MM/yyyy HH:mm")
                rowcnt = rowcnt + 2
                xlapp.Cells.Font.Size = 10
                xlapp.Cells(rowcnt, 5) = "OVER TIME SUMMARY FOR DATE : " & DateEdit1.DateTime.ToString("yyyy-MM-dd")
                rowcnt = rowcnt + 1
                xlapp.Cells(rowcnt, 2) = "Department"
                xlapp.Cells(rowcnt, 5) = "              Ot "
                xlapp.Cells(rowcnt, 6) = "OT Amount "
                mintLine = 9
            End If
            'Loop
        Next
        'End With

        '        xlwb.SaveAs(App.Path & "\TimeWatch.xls")
        '        Exit Sub
        'ErrorGen:
        '        MsgBox(Err.Description, vbInformation + vbOKOnly, "Report Generation Information")
        '        Screen.MousePointer = vbDefault
        xlwb.SaveAs(mstrFile_Name)
    End Sub
    Sub DailyXl_ShiftChangeStatement(ByVal strsortorder As String)
        'On Error GoTo ErrorGen
        Dim strDeptDivCode As String
        Dim strCurrentDeptDivCode As String
        Dim intFile As Integer
        Dim strsql As String
        Dim strDepartmentCode As String
        Dim intPrintLineCounter As Integer
        Dim blnDeptAvailable As Boolean
        Dim xlapp As Excel.Application
        Dim xlwb As Excel.Workbook
        Dim xlst As Excel.Sheets
        Dim rowcnt As Integer
        xlapp = CreateObject("Excel.Application")
        xlwb = xlapp.Workbooks.Add
        'If UCase(Dir(App.Path & "\TimeWatch.xls")) = UCase("TimeWatch.xls") Then
        '    Kill(App.Path & "\TimeWatch.xls")
        'End If

        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If
        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".xls"

        mblnCheckReport = False
        mintPageNo = 1
        mintLine = 1
        'If Left(strsortorder, 1) = "D" Or Left(strsortorder, 1) = "C" Then blnDeptAvailable = True
        If strsortorder = "" Then
            'If g_Report_view Then
            If Common.servername = "Access" Then
                'strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tbltimeregister.Shift,tbltimeregister.ShiftAttended,tbltimeregister.ShiftStartTime,tbltimeregister.In1,tbltimeregister.Out2,tbltimeregister.HoursWorked,tbltimeregister.Status,tbltimeregister.EarlyArrival,tbltimeregister.LateArrival,tbltimeregister.In1Mannual,tbltimeregister.In2Mannual,tbltimeregister.Out1Mannual,tbltimeregister.Out2Mannual,tblCalander.Process,tblCalander.NRTCProc" & _
                ' " from tblCatagory,tblDivision,  tbltimeregister,tblEmployee,tblCalander,tblCompany,tblDepartment" & _
                ' " Where tblCatagory.Cat = tblEmployee.Cat And tblDivision.DivisionCode = tblEmployee.DivisionCode And tbltimeregister.PayCode = tblEmployee.PayCode And tbltimeregister.Shift <> tbltimeregister.ShiftAttended And FORMAT(tbltimeregister.DateOffice,'MMM DD YYYY') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & " And FORMAT(tblCalander.mDate,'MMM DD YYYY') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
                ' " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
                ' " And (tblemployee.LeavingDate >=#" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "# or tblemployee.LeavingDate is null)" & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & strsortorder

                strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tbltimeregister.Shift,tbltimeregister.ShiftAttended,tbltimeregister.ShiftStartTime,tbltimeregister.In1,tbltimeregister.Out2,tbltimeregister.HoursWorked,tbltimeregister.Status,tbltimeregister.EarlyArrival,tbltimeregister.LateArrival,tbltimeregister.In1Mannual,tbltimeregister.In2Mannual,tbltimeregister.Out1Mannual,tbltimeregister.Out2Mannual,tblCalander.Process,tblCalander.NRTCProc" & _
                " from tblCatagory,tblDivision,  tbltimeregister,tblEmployee,tblCalander,tblCompany,tblDepartment" & _
                " Where tblCatagory.Cat = tblEmployee.Cat And tbltimeregister.PayCode = tblEmployee.PayCode And tbltimeregister.Shift <> tbltimeregister.ShiftAttended And FORMAT(tbltimeregister.DateOffice,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & " And FORMAT(tblCalander.mDate,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
                " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
                " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & strsortorder

                adapA = New OleDbDataAdapter(strsql, Common.con1)
                adapA.Fill(Rs_Report)
            Else
                strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tbltimeregister.Shift,tbltimeregister.ShiftAttended,tbltimeregister.ShiftStartTime,tbltimeregister.In1,tbltimeregister.Out2,tbltimeregister.HoursWorked,tbltimeregister.Status,tbltimeregister.EarlyArrival,tbltimeregister.LateArrival,tbltimeregister.In1Mannual,tbltimeregister.In2Mannual,tbltimeregister.Out1Mannual,tbltimeregister.Out2Mannual,tblCalander.Process,tblCalander.NRTCProc" & _
                 " from tblCatagory,tblDivision,  tbltimeregister,tblEmployee,tblCalander,tblCompany,tblDepartment" & _
                 " Where tblCatagory.Cat = tblEmployee.Cat And tbltimeregister.PayCode = tblEmployee.PayCode And tbltimeregister.Shift <> tbltimeregister.ShiftAttended And tbltimeregister.DateOffice = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & " And tblCalander.mDate = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
                 " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
                 " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & strsortorder
                adap = New SqlDataAdapter(strsql, Common.con)
                adap.Fill(Rs_Report)
            End If
            'Else
            '    strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregisterD.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tbltimeregisterD.Shift,tbltimeregisterD.ShiftAttended,tbltimeregisterD.ShiftStartTime,tbltimeregisterD.In1,tbltimeregisterD.Out2,tbltimeregisterD.HoursWorked,tbltimeregisterD.Status,tbltimeregisterD.EarlyArrival,tbltimeregisterD.LateArrival,tbltimeregisterD.In1Mannual,tbltimeregisterD.In2Mannual,tbltimeregisterD.Out1Mannual,tbltimeregisterD.Out2Mannual,tblCalander.Process,tblCalander.NRTCProc" & _
            '         " from tblCatagory,tblDivision,  tbltimeregisterD,tblEmployee,tblCalander,tblCompany,tblDepartment" & _
            '         " Where tblCatagory.Cat = tblEmployee.Cat And tblDivision.DivisionCode = tblEmployee.DivisionCode And tbltimeregisterD.PayCode = tblEmployee.PayCode And tbltimeregisterD.Shift <> tbltimeregisterD.ShiftAttended And tbltimeregisterD.DateOffice = '" & Format(frmDailyAttReport.txtFromDate.Value, "MMM dd yyyy") & "'" & " And tblCalander.mDate = '" & Format(frmDailyAttReport.txtFromDate.Value, "MMM dd yyyy") & "'" & _
            '         " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
            '         " And (tblemployee.LeavingDate>='" & Format(frmDailyAttReport.txtFromDate, "MMM dd yyyy") & "' or tblemployee.LeavingDate is null)" & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause)
            'End If
            'Rs_Report = Cn.Execute(strsql)
            If Rs_Report.Tables(0).Rows.Count < 1 Then
                'G_Report_Error = False
                'Screen.MousePointer = vbDefault
                XtraMessageBox.Show(ulf, "<size=10>There is no Auto Shift Change for any Employee on this Date.</size>", "<size=9>iAS</size>")
                'mReportPrintStatus = False
                Exit Sub
            End If
            'Rs_Report.Sort = "PayCode"
        Else
            'Rs_Report.Sort = strsortorder
        End If


        'If IsNull(Rs_Report!Process) And IsNull(Rs_Report!NRTCProc) Then
        '   MsgBox "Records are not Processed", vbInformation + vbExclamation, "TimeWatch  Information"
        '   mReportPrintStatus = False
        '   Exit Sub
        'ElseIf InStr("YX", Rs_Report("Process")) > 0 Or InStr("Y", Rs_Report("nRTCProc")) > 0 Then
        rowcnt = 1
        xlapp.Visible = True
        xlapp.Columns.Font.Name = "TAHOMA"
        xlapp.Columns.Font.Size = 8
        xlapp.Columns.Font.ColorIndex = 1 'vbBlack
        xlapp.Cells(rowcnt, 5).Font.Bold = True
        xlapp.Cells(rowcnt, 5) = "Company Name:" & CommonReport.g_CompanyNames
        rowcnt = rowcnt + 1
        xlapp.Cells(rowcnt, 5).Font.Bold = True
        xlapp.Cells(rowcnt, 5) = "Run Date & Time :" & Now.ToString("dd/MM/yyyy HH:mm")
        rowcnt = rowcnt + 2
        xlapp.Cells(rowcnt, 4).Font.Bold = True
        xlapp.Cells(rowcnt, 4) = "SHIFT CHANGE STATEMENT FOR DATE : " & DateEdit1.DateTime.ToString("dd/MM/yyyy")
        rowcnt = rowcnt + 1
        rowcnt = rowcnt + 1

        xlapp.Range("B6:N6").Font.Bold = True
        xlapp.Range("B6:N6").Font.ColorIndex = 2 'vbWhite
        'xlapp.Range("B6:N6").HorizontalAlignment = xlLeft
        xlapp.Range("B6:N6").Interior.ColorIndex = 51
        xlapp.Range("B6:N6").Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 2) = "PayCode"
        xlapp.Cells(rowcnt, 3) = "Card No"
        xlapp.Cells(rowcnt, 4) = "Employee Name"
        xlapp.Cells(rowcnt, 6) = "Actual Shift  "
        xlapp.Cells(rowcnt, 7) = "Shift "
        xlapp.Cells(rowcnt, 8) = " Start "
        xlapp.Cells(rowcnt, 9) = "     In "
        xlapp.Cells(rowcnt, 10) = "   Out "
        xlapp.Cells(rowcnt, 11) = "Hours Worked "
        xlapp.Cells(rowcnt, 12) = "Status "
        xlapp.Cells(rowcnt, 13) = "Shift Late "
        xlapp.Cells(rowcnt, 14) = "Manual "
        rowcnt = rowcnt + 1
        mintLine = 9
        'With Rs_Report
        'If blnDeptAvailable And Not .EOF Then
        '    rowcnt = rowcnt + 1
        '    If Left(strsortorder, 10) = "Department" Or frmSorting.optDeptEmpName.Value Then
        '        xlapp.Cells(rowcnt, 1) = "** Department Code & Name : " & !DepartmentCode & "  " & !DepartmentName
        '    ElseIf Left(strsortorder, 1) = "Division" Or frmSorting.optSectionPaycode.Value Then
        '        xlapp.Cells(rowcnt, 1) = "** Section Code & Name : " & !DivisionCode & "  " & !DivisionName
        '    Else
        '        xlapp.Cells(rowcnt, 1) = "** Category Code & Name : " & !Cat & "  " & !CatagoryName
        '    End If
        '    rowcnt = rowcnt + 1
        '    mintLine = mintLine + 3
        'End If
        'Do While Not .EOF
        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            rowcnt = rowcnt + 1
            xlapp.Cells(rowcnt, 2).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 2) = "'" & Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim
            xlapp.Cells(rowcnt, 3).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 3) = "'" & Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim
            xlapp.Cells(rowcnt, 4).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 4) = Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim
            xlapp.Cells(rowcnt, 5).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 6).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 6) = Rs_Report.Tables(0).Rows(i).Item("Shift").ToString.Trim
            xlapp.Cells(rowcnt, 7).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 7) = Rs_Report.Tables(0).Rows(i).Item("ShiftAttended").ToString.Trim
            xlapp.Cells(rowcnt, 8).Borders.LineStyle = XlLineStyle.xlContinuous
            If Rs_Report.Tables(0).Rows(i).Item("shiftStartTime").ToString.Trim = "" Then
                xlapp.Cells(rowcnt, 8) = ""
            Else
                xlapp.Cells(rowcnt, 8) = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("shiftStartTime").ToString.Trim).ToString("HH:mm")
            End If
            'xlapp.Cells(rowcnt, 8) = Format(!shiftStartTime, "HH:MM")
            xlapp.Cells(rowcnt, 9).Borders.LineStyle = XlLineStyle.xlContinuous
            If Rs_Report.Tables(0).Rows(i).Item("In1").ToString.Trim = "" Then
                xlapp.Cells(rowcnt, 9) = ""
            Else
                xlapp.Cells(rowcnt, 9) = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("In1").ToString.Trim).ToString("HH:mm")
            End If
            'xlapp.Cells(rowcnt, 9) = Format(!In1, "HH:MM")
            xlapp.Cells(rowcnt, 10).Borders.LineStyle = XlLineStyle.xlContinuous
            If Rs_Report.Tables(0).Rows(i).Item("out2").ToString.Trim = "" Then
                xlapp.Cells(rowcnt, 10) = ""
            Else
                xlapp.Cells(rowcnt, 10) = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("out2").ToString.Trim).ToString("HH:mm")
            End If
            'xlapp.Cells(rowcnt, 10) = Format(!out2, "HH:MM")
            xlapp.Cells(rowcnt, 11).Borders.LineStyle = XlLineStyle.xlContinuous
            If Rs_Report.Tables(0).Rows(i).Item("HOURSWORKED").ToString.Trim = "0" Then
                xlapp.Cells(rowcnt, 11) = "0"
            Else
                xlapp.Cells(rowcnt, 11) = "'" & Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("HOURSWORKED").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("HOURSWORKED").ToString.Trim Mod 60).ToString("00")
            End If
            'xlapp.Cells(rowcnt, 11) = Length5(Min2Hr(!HOURSWORKED))
            xlapp.Cells(rowcnt, 12).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 12) = Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim
            xlapp.Cells(rowcnt, 13).Borders.LineStyle = XlLineStyle.xlContinuous
            If Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim = "0" Then
                xlapp.Cells(rowcnt, 13) = ""
            Else
                xlapp.Cells(rowcnt, 13) = "'" & Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim Mod 60).ToString("00")
            End If
            'xlapp.Cells(rowcnt, 13) = Length5(Min2Hr(!latearrival))
            xlapp.Cells(rowcnt, 14).Borders.LineStyle = XlLineStyle.xlContinuous
            'xlapp.Cells(rowcnt, 14) = IIf(UCase(Rs_Report.Tables(0).Rows(i).Item("IN1MANNUAL").ToString.Trim) = "Y", "M", IIf(UCase(Rs_Report.Tables(0).Rows(i).Item("Out1Mannual").ToString.Trim) = "Y", "M", IIf(UCase(Rs_Report.Tables(0).Rows(i).Item("IN2MANNUAL").ToString.Trim) = "Y", "M", IIf(UCase(Rs_Report.Tables(0).Rows(i).Item("OUT2MANNUAL").ToString.Trim) = "Y", "M", ""))))

            If UCase(Rs_Report.Tables(0).Rows(i).Item("IN1MANNUAL").ToString.Trim) = "Y" Or UCase(Rs_Report.Tables(0).Rows(i).Item("Out1Mannual").ToString.Trim) = "Y" Or UCase(Rs_Report.Tables(0).Rows(i).Item("IN2MANNUAL").ToString.Trim) = "Y" Or UCase(Rs_Report.Tables(0).Rows(i).Item("OUT2MANNUAL").ToString.Trim) = "Y" Then
                xlapp.Cells(rowcnt, 14) = "M"
            Else
                xlapp.Cells(rowcnt, 14) = ""
            End If

            rowcnt = rowcnt + 1
            mintLine = mintLine + 1
            mblnCheckReport = True
            'If Left(strsortorder, 10) = "Department" Then
            '    strDeptDivCode = !DepartmentCode
            'ElseIf Left(strsortorder, 8) = "Division" Then
            '    strDeptDivCode = !DivisionCode
            'Else
            '    strDeptDivCode = !Cat
            'End If
            '.MoveNext()
            'If blnDeptAvailable And Not .EOF Then
            '    If Left(strsortorder, 10) = "Department" Then
            '        strCurrentDeptDivCode = !DepartmentCode
            '    ElseIf Left(strsortorder, 8) = "Division" Then
            '        strCurrentDeptDivCode = !DivisionCode
            '    Else
            '        strCurrentDeptDivCode = !Cat
            '    End If
            '    If strDeptDivCode <> strCurrentDeptDivCode Then
            '        rowcnt = rowcnt + 1
            '        If Left(strsortorder, 10) = "Department" Or frmSorting.optDeptEmpName.Value Then
            '            xlapp.Cells(rowcnt, 1) = "** Department Code & Name : " & !DepartmentCode & "  " & !DepartmentName
            '        ElseIf Left(strsortorder, 1) = "Division" Or frmSorting.optSectionPaycode.Value Then
            '            xlapp.Cells(rowcnt, 1) = "** Section Code & Name : " & !DivisionCode & "  " & !DivisionName
            '        Else
            '            xlapp.Cells(rowcnt, 1) = "** Category Code & Name : " & !Cat & "  " & !CatagoryName
            '        End If
            '        rowcnt = rowcnt + 1
            '        mintLine = mintLine + 3
            '    Else
            '        rowcnt = rowcnt + 1
            '        If Left(strsortorder, 10) = "Department" Or frmSorting.optDeptEmpName.Value Then
            '            xlapp.Cells(rowcnt, 1) = "** Department Code & Name : " & !DepartmentCode & "  " & !DepartmentName
            '        ElseIf Left(strsortorder, 1) = "Division" Or frmSorting.optSectionPaycode.Value Then
            '            xlapp.Cells(rowcnt, 1) = "** Section Code & Name : " & !DivisionCode & "  " & !DivisionName
            '        Else
            '            xlapp.Cells(rowcnt, 1) = "** Category Code & Name : " & !Cat & "  " & !CatagoryName
            '        End If
            '        rowcnt = rowcnt + 1
            '        mintLine = mintLine + 3
            '    End If
            'End If
        Next
        'Loop
        'End With
        '        xlwb.SaveAs(App.Path & "\TimeWatch.xls")
        '        Exit Sub
        'ErrorGen:
        '        MsgBox(Err.Description, vbInformation + vbOKOnly, "Report Generation Information")
        '        Screen.MousePointer = vbDefault
        xlwb.SaveAs(mstrFile_Name)
    End Sub
    Sub DailyXl_OutWorkReport(ByVal strsortorder As String)
        'On Error GoTo ErrorGen
        Dim intFile As Integer
        Dim strsql As String
        Dim strIn As String
        Dim strOut As String
        Dim iOutWorkCtr As Integer
        Dim strDeptDivCode As String
        Dim strCurrentDeptDivCode As String
        Dim intPrintLineCounter As Integer, mCount As Integer
        Dim blnDeptAvailable As Boolean
        Dim mField As Object
        Dim xlapp As Excel.Application
        Dim xlwb As Excel.Workbook
        Dim xlst As Excel.Sheets
        Dim rowcnt As Integer
        xlapp = CreateObject("Excel.Application")
        xlwb = xlapp.Workbooks.Add
        'If UCase(Dir(App.Path & "\TimeWatch.xls")) = UCase("TimeWatch.xls") Then
        '    Kill(App.Path & "\TimeWatch.xls")
        'End If
        mCount = 0
        mblnCheckReport = False
        mintPageNo = 1
        mintLine = 1
        If XtraShortOrder.g_SortOrder <> "" Then
            strsortorder = XtraShortOrder.g_SortOrder
        End If
        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If
        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".xls"

        'If Left(strsortorder, 1) = "D" Or Left(strsortorder, 1) = "C" Then blnDeptAvailable = True

        If Common.servername = "Access" Then
            'strsql = "select tblDivision.DivisionName,tblCatagory.CatagoryName,tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tblOutWorkRegister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tblOutWorkRegister.OutWorkDuration,tblOutWorkRegister.in1,tblOutWorkRegister.in2,tblOutWorkRegister.in3,tblOutWorkRegister.in4,tblOutWorkRegister.in5,tblOutWorkRegister.in6,tblOutWorkRegister.in7,tblOutWorkRegister.in8,tblOutWorkRegister.in9,tblOutWorkRegister.in10,tblOutWorkRegister.out1,tblOutWorkRegister.out2,tblOutWorkRegister.out3,tblOutWorkRegister.out4,tblOutWorkRegister.out5,tblOutWorkRegister.out6,tblOutWorkRegister.out7,tblOutWorkRegister.out8,tblOutWorkRegister.out9,tblOutWorkRegister.out10,tblCalander.Process,tblCalander.NRTCProc, " & _
            '     " tblOutWorkRegister.Rin1,tblOutWorkRegister.Rin2,tblOutWorkRegister.Rin3,tblOutWorkRegister.Rin4,tblOutWorkRegister.Rin5,tblOutWorkRegister.Rin6,tblOutWorkRegister.Rin7,tblOutWorkRegister.Rin8,tblOutWorkRegister.Rin9,tblOutWorkRegister.Rin10,tblOutWorkRegister.Rout1,tblOutWorkRegister.Rout2,tblOutWorkRegister.Rout3,tblOutWorkRegister.Rout4,tblOutWorkRegister.Rout5,tblOutWorkRegister.Rout6,tblOutWorkRegister.Rout7,tblOutWorkRegister.Rout8,tblOutWorkRegister.Rout9,tblOutWorkRegister.Rout10 " & _
            '     " from tblDivision,tblCatagory,tblOutWorkRegister ,tblEmployee,tblCalander,tblDepartment,tblCompany " & _
            '     " Where tblDivision.DivisionCode = tblEmployee.DivisionCode And tblCatagory.Cat = tblEmployee.Cat And tblOutWorkRegister.Paycode = tblEmployee.Paycode And tblOutWorkRegister.In1 Is Not Null And FORMAT(tblOutWorkRegister.DateOffice,'MMM DD YYYY') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' And FORMAT(tblCalander.mDate,'MMM DD YYYY') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
            '     " And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
            '     " And (tblemployee.LeavingDate >=#" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "# or tblemployee.LeavingDate is null)" & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & _
            '     " And tblOutworkRegister.reason_OutWork = 'N' And tblCompany.CompanyCode = tblEmployee.CompanyCode " & strsortorder


            strsql = "select tblDivision.DivisionName,tblCatagory.CatagoryName,tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tblOutWorkRegister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tblOutWorkRegister.OutWorkDuration,tblOutWorkRegister.in1,tblOutWorkRegister.in2,tblOutWorkRegister.in3,tblOutWorkRegister.in4,tblOutWorkRegister.in5,tblOutWorkRegister.in6,tblOutWorkRegister.in7,tblOutWorkRegister.in8,tblOutWorkRegister.in9,tblOutWorkRegister.in10,tblOutWorkRegister.out1,tblOutWorkRegister.out2,tblOutWorkRegister.out3,tblOutWorkRegister.out4,tblOutWorkRegister.out5,tblOutWorkRegister.out6,tblOutWorkRegister.out7,tblOutWorkRegister.out8,tblOutWorkRegister.out9,tblOutWorkRegister.out10,tblCalander.Process,tblCalander.NRTCProc, " & _
                " tblOutWorkRegister.Rin1,tblOutWorkRegister.Rin2,tblOutWorkRegister.Rin3,tblOutWorkRegister.Rin4,tblOutWorkRegister.Rin5,tblOutWorkRegister.Rin6,tblOutWorkRegister.Rin7,tblOutWorkRegister.Rin8,tblOutWorkRegister.Rin9,tblOutWorkRegister.Rin10,tblOutWorkRegister.Rout1,tblOutWorkRegister.Rout2,tblOutWorkRegister.Rout3,tblOutWorkRegister.Rout4,tblOutWorkRegister.Rout5,tblOutWorkRegister.Rout6,tblOutWorkRegister.Rout7,tblOutWorkRegister.Rout8,tblOutWorkRegister.Rout9,tblOutWorkRegister.Rout10 " & _
                " from tblDivision,tblCatagory,tblOutWorkRegister ,tblEmployee,tblCalander,tblDepartment,tblCompany,tblTimeRegister " & _
                " Where tblTimeRegister.paycode=tblEmployee.paycode and tblCatagory.Cat = tblEmployee.Cat And tblOutWorkRegister.Paycode = tblEmployee.Paycode And tblOutWorkRegister.In1 Is Not Null And FORMAT(tblOutWorkRegister.DateOffice,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' And FORMAT(tblCalander.mDate,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
                " And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
                " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & _
                " And tblOutworkRegister.reason_OutWork = 'N' And tblCompany.CompanyCode = tblEmployee.CompanyCode " & strsortorder

            adapA = New OleDbDataAdapter(strsql, Common.con1)
            adapA.Fill(Rs_Report)
        Else
            strsql = "select tblDivision.DivisionName,tblCatagory.CatagoryName,tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tblOutWorkRegister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tblOutWorkRegister.OutWorkDuration,tblOutWorkRegister.in1,tblOutWorkRegister.in2,tblOutWorkRegister.in3,tblOutWorkRegister.in4,tblOutWorkRegister.in5,tblOutWorkRegister.in6,tblOutWorkRegister.in7,tblOutWorkRegister.in8,tblOutWorkRegister.in9,tblOutWorkRegister.in10,tblOutWorkRegister.out1,tblOutWorkRegister.out2,tblOutWorkRegister.out3,tblOutWorkRegister.out4,tblOutWorkRegister.out5,tblOutWorkRegister.out6,tblOutWorkRegister.out7,tblOutWorkRegister.out8,tblOutWorkRegister.out9,tblOutWorkRegister.out10,tblCalander.Process,tblCalander.NRTCProc, " & _
                     " tblOutWorkRegister.Rin1,tblOutWorkRegister.Rin2,tblOutWorkRegister.Rin3,tblOutWorkRegister.Rin4,tblOutWorkRegister.Rin5,tblOutWorkRegister.Rin6,tblOutWorkRegister.Rin7,tblOutWorkRegister.Rin8,tblOutWorkRegister.Rin9,tblOutWorkRegister.Rin10,tblOutWorkRegister.Rout1,tblOutWorkRegister.Rout2,tblOutWorkRegister.Rout3,tblOutWorkRegister.Rout4,tblOutWorkRegister.Rout5,tblOutWorkRegister.Rout6,tblOutWorkRegister.Rout7,tblOutWorkRegister.Rout8,tblOutWorkRegister.Rout9,tblOutWorkRegister.Rout10 " & _
                     " from tblDivision,tblCatagory,tblOutWorkRegister ,tblEmployee,tblCalander,tblDepartment,tblCompany, tblTimeRegister " & _
                     " Where tblTimeRegister.paycode=tblEmployee.paycode and tblCatagory.Cat = tblEmployee.Cat And tblOutWorkRegister.Paycode = tblEmployee.Paycode And tblOutWorkRegister.In1 Is Not Null And tblOutWorkRegister.DateOffice = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' And tblCalander.mDate = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
                     " And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
                     " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & _
                     " And tblOutworkRegister.reason_OutWork = 'N' And tblCompany.CompanyCode = tblEmployee.CompanyCode " & strsortorder
            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(Rs_Report)
        End If

        'Rs_Report = Cn.Execute(strsql)
        If Rs_Report.Tables(0).Rows.Count < 1 Then
            'G_Report_Error = False
            'Screen.MousePointer = vbDefault
            XtraMessageBox.Show(ulf, "<size=10>There is no Out Work for any Employee on this Date.<size>", "<size=9>iAS</size>")
            'mReportPrintStatus = False
            Exit Sub
        End If
        If strsortorder = "" Then
            'Rs_Report.Sort = "PayCode"
        Else
            'Rs_Report.Sort = strsortorder
        End If

        'If IsNull(Rs_Report!Process) And IsNull(Rs_Report!NRTCProc) Then
        '    MsgBox "Records are not Processed", vbInformation + vbExclamation, "TimeWatch  Information"
        '   Exit Sub
        ' ElseIf InStr("YX", Rs_Report("Process")) > 0 Or InStr("Y", Rs_Report("nRTCProc")) > 0 Then

        rowcnt = 1
        xlapp.Visible = True
        xlapp.Columns.ColumnWidth = 9
        xlapp.Columns.Font.Name = "TAHOMA"
        xlapp.Columns.Font.Size = 8
        xlapp.Columns.Font.ColorIndex = 1 'vbBlack
        xlapp.Cells(rowcnt, 5).Font.Bold = True
        xlapp.Cells(rowcnt, 5) = "Company Name:" & CommonReport.g_CompanyNames
        rowcnt = rowcnt + 1
        xlapp.Cells(rowcnt, 5).Font.Bold = True
        xlapp.Cells(rowcnt, 5) = "Run Date & Time :" & Now.ToString("dd/MM/yyyy HH:mm")
        rowcnt = rowcnt + 2
        xlapp.Cells(rowcnt, 5).Font.Bold = True
        xlapp.Cells(rowcnt, 5) = "OUT WORK REPORT FOR DATE : " & DateEdit1.DateTime.ToString("dd/MM/yyyy")
        rowcnt = rowcnt + 1
        rowcnt = rowcnt + 1


        xlapp.Range("A6:H6").Font.Bold = True
        xlapp.Range("A6:H6").Font.ColorIndex = 2 'vbWhite
        'xlapp.Range("A6:H6").HorizontalAlignment = xlLeft
        xlapp.Range("A6:H6").Interior.ColorIndex = 51
        xlapp.Range("A6:H6").Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 2) = "Srl.No"
        xlapp.Cells(rowcnt, 3) = "PayRoll No"
        xlapp.Cells(rowcnt, 4) = " Card No "
        xlapp.Cells(rowcnt, 5) = "Employee Name "
        xlapp.Cells(rowcnt, 7) = "Total OW"
        xlapp.Cells(rowcnt, 8) = " I         II        III         IV         V         VI         VII         VIII         IX         X"
        rowcnt = rowcnt + 1


        mintLine = 9
        'With Rs_Report
        'If blnDeptAvailable And Not .EOF Then
        '    rowcnt = rowcnt + 1
        '    If Left(strsortorder, 10) = "Department" Or frmSorting.optDeptEmpName.Value Then
        '        xlapp.Cells(rowcnt, 1) = "** Department Code & Name : " & !DepartmentCode & "  " & !DepartmentName
        '    ElseIf Left(strsortorder, 8) = "Division" Or frmSorting.optSectionPaycode.Value Then
        '        xlapp.Cells(rowcnt, 1) = "** Section Code & Name : " & !DivisionCode & "  " & !DivisionName
        '    Else
        '        xlapp.Cells(rowcnt, 1) = "** Category Code & Name : " & !Cat & "  " & !CatagoryName
        '    End If
        '    rowcnt = rowcnt + 1
        '    mintLine = mintLine + 3
        'End If
        'Do While Not .EOF
        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            strIn = "'"
            strOut = "'"
            For iOutWorkCtr = 1 To 10
                If (Rs_Report.Tables(0).Rows(i).Item("In" & Trim(Str(iOutWorkCtr)))).ToString.Trim <> "" Then
                    strIn = strIn & Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("In" & Trim(Str(iOutWorkCtr))).ToString.Trim).ToString("HH:mm") & " "
                End If
                If (Rs_Report.Tables(0).Rows(i).Item("OUT" & Trim(Str(iOutWorkCtr)))).ToString.Trim <> "" Then
                    strOut = strOut & Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("OUT" & Trim(Str(iOutWorkCtr))).ToString.Trim).ToString("HH:mm") & " "
                End If
            Next iOutWorkCtr
            mCount = mCount + 1
            xlapp.Cells(rowcnt, 2).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 2) = "'" & mCount
            xlapp.Cells(rowcnt, 3).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 3) = "'" & Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim
            xlapp.Cells(rowcnt, 4).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 4) = "'" & Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim
            xlapp.Cells(rowcnt, 5).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 5) = Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim
            xlapp.Cells(rowcnt, 6).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 7).Borders.LineStyle = XlLineStyle.xlContinuous
            If Rs_Report.Tables(0).Rows(i).Item("OUTWORKDURATION").ToString.Trim = "0" Then
                xlapp.Cells(rowcnt, 7) = "0"
            Else
                xlapp.Cells(rowcnt, 7) = "'" & (Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("OUTWORKDURATION").ToString.Trim) / 60)).ToString("00") & ":" & (Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("OUTWORKDURATION").ToString.Trim) Mod 60).ToString("00")
            End If
            'xlapp.Cells(rowcnt, 7) = "'" & Allign(Min2Hr(CStr(!OUTWORKDURATION)))
            xlapp.Cells(rowcnt, 8).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 8) = strIn

            rowcnt = rowcnt + 1
            xlapp.Cells(rowcnt, 8).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 8) = strOut
            rowcnt = rowcnt + 1
            mintLine = mintLine + 2
            mblnCheckReport = True
            'If Left(strsortorder, 10) = "Department" Then
            '    strDeptDivCode = !DepartmentCode
            'ElseIf Left(strsortorder, 8) = "Division" Then
            '    strDeptDivCode = !DivisionCode
            'Else
            '    strDeptDivCode = !Cat
            'End If
            '.MoveNext()
            'If blnDeptAvailable And Not .EOF Then
            '    If Left(strsortorder, 10) = "Department" Then
            '        strCurrentDeptDivCode = !DepartmentCode
            '    ElseIf Left(strsortorder, 8) = "Division" Then
            '        strCurrentDeptDivCode = !DivisionCode
            '    Else
            '        strCurrentDeptDivCode = !Cat
            '    End If
            '    If strDeptDivCode <> strCurrentDeptDivCode Then
            '        rowcnt = rowcnt + 1
            '        If Left(strsortorder, 10) = "Department" Or frmSorting.optDeptEmpName.Value Then
            '            xlapp.Cells(rowcnt, 1) = "** Department Code & Name : " & !DepartmentCode & "  " & !DepartmentName
            '        ElseIf Left(strsortorder, 1) = "Division" Or frmSorting.optSectionPaycode.Value Then
            '            xlapp.Cells(rowcnt, 1) = "** Section Code & Name : " & !DivisionCode & "  " & !DivisionName
            '        Else
            '            xlapp.Cells(rowcnt, 1) = "** Category Code & Name : " & !Cat & "  " & !CatagoryName
            '        End If
            '        rowcnt = rowcnt + 1
            '        mintLine = mintLine + 3
            '    End If
            'End If
        Next
        'Loop
        'End With

        '        xlwb.SaveAs(App.Path & "\TimeWatch.xls")
        '        Exit Sub
        'ErrorGen:
        '        MsgBox(Err.Description, vbInformation + vbOKOnly, "Report Generation Information")
        '        Screen.MousePointer = vbDefault
        xlwb.SaveAs(mstrFile_Name)
    End Sub
    Sub DailyXl_MissingPunchOrIncorrectShiftOrReverification(ByVal strsortorder As String)
        'On Error GoTo ErrorGen
        Dim adapS As SqlDataAdapter
        Dim adapAc As OleDbDataAdapter
        Dim Rs As DataSet = New DataSet
        Dim sSql As String = " Select * from tblSetUp where setupid =(Select Convert(varchar(10),Max(Convert(int,Setupid))) from tb" & _
        "lsetup )"
        If Common.servername = "Access" Then
            sSql = " Select * from tblSetUp where setupid =(Select CVar(Max(CInt(Setupid))) from tblsetup )"
            adapAc = New OleDbDataAdapter(sSql, Common.con1)
            adapAc.Fill(Rs)
        Else
            adapS = New SqlDataAdapter(sSql, Common.con)
            adapS.Fill(Rs)
        End If
        Dim g_TIME1 As String
        If Rs.Tables(0).Rows(0)("Time1").ToString.Trim = "" Then
            g_TIME1 = 0
        Else
            g_TIME1 = Rs.Tables(0).Rows(0)("Time1").ToString.Trim
        End If
        Dim G_CHECKLATE As String
        If Rs.Tables(0).Rows(0)("CHECKLATE").ToString.Trim = "" Then
            G_CHECKLATE = 0
        Else
            G_CHECKLATE = Rs.Tables(0).Rows(0)("CHECKLATE").ToString.Trim
        End If

        Dim g_CHECKEARLY As String
        If Rs.Tables(0).Rows(0)("CHECKEARLY").ToString.Trim = "" Then
            g_CHECKEARLY = 0
        Else
            g_CHECKEARLY = Rs.Tables(0).Rows(0)("CHECKEARLY").ToString.Trim
        End If
        Dim strDeptDivCode As String
        Dim strCurrentDeptDivCode As String
        Dim intFile As Integer
        Dim strsql As String
        Dim strStatus As String
        Dim lngHoursWorked As Long
        Dim lngLateArrival As Long
        Dim lngEarlyDeparture As Long
        Dim g_DummyHoursWorked As Long
        Dim g_DummyLateArrival As Long
        Dim g_DummyEarlyDeparture As Long
        g_DummyHoursWorked = g_TIME1
        g_DummyLateArrival = G_CHECKLATE
        g_DummyEarlyDeparture = g_CHECKEARLY
        Dim strDepartmentCode As String
        Dim intPrintLineCounter As Integer
        Dim blnDeptAvailable As Boolean
        Dim xlapp As Excel.Application
        Dim xlwb As Excel.Workbook
        Dim xlst As Excel.Sheets
        Dim rowcnt As Integer, mCount As Integer
        xlapp = CreateObject("Excel.Application")
        xlwb = xlapp.Workbooks.Add
        'If UCase(Dir(App.Path & "\TimeWatch.xls")) = UCase("TimeWatch.xls") Then
        '    Kill(App.Path & "\TimeWatch.xls")
        'End If
        mblnCheckReport = False
        mintPageNo = 1
        mintLine = 1
        mCount = 0
        If XtraShortOrder.g_SortOrder <> "" Then
            strsortorder = XtraShortOrder.g_SortOrder
        End If
        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If
        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".xls"

        'If Left(strsortorder, 1) = "D" Or Left(strsortorder, 1) = "C" Then blnDeptAvailable = True
        'If g_Report_view Then
        If Common.servername = "Access" Then
            'strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tblTimeRegister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tbltimeregister.ShiftAttended,tbltimeregister.In1,tbltimeregister.Out1,tbltimeregister.In2,tbltimeregister.Out2,tbltimeregister.HoursWorked,tbltimeregister.LateArrival,tbltimeregister.EarlyDeparture,tbltimeregister.Status,tbltimeregister.OtDuration,tblCalander.mDate,tblCalander.Process,tblCalander.NRTCProc" & _
            ' " from tblCatagory,tblDivision,  tbltimeregister,tblEmployee,tblCalander,tblCompany,tblDepartment" & _
            ' " Where tblCatagory.Cat = tblEmployee.Cat And tblDivision.DivisionCode = tblEmployee.DivisionCode And tbltimeregister.PayCode = tblEmployee.PayCode And FORMAT(tbltimeregister.DateOffice,'MMM DD YYYY') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & " And FORMAT(tblCalander.mDate,'MMM DD YYYY') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
            ' " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
            ' " And (tbltimeregister.Status = 'MIS' Or tbltimeregister.HoursWorked >" & g_DummyHoursWorked & " Or tbltimeregister.LateArrival >" & g_DummyLateArrival & " Or tbltimeregister.EarlyDeparture > " & g_DummyEarlyDeparture & ")" & _
            ' " And (tblemployee.LeavingDate >=#" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "# or tblemployee.LeavingDate is null)" & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & strsortorder

            strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tblTimeRegister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tbltimeregister.ShiftAttended,tbltimeregister.In1,tbltimeregister.Out1,tbltimeregister.In2,tbltimeregister.Out2,tbltimeregister.HoursWorked,tbltimeregister.LateArrival,tbltimeregister.EarlyDeparture,tbltimeregister.Status,tbltimeregister.OtDuration,tblCalander.mDate,tblCalander.Process,tblCalander.NRTCProc" & _
           " from tblCatagory,tblDivision,  tbltimeregister,tblEmployee,tblCalander,tblCompany,tblDepartment" & _
           " Where tblCatagory.Cat = tblEmployee.Cat And tbltimeregister.PayCode = tblEmployee.PayCode And FORMAT(tbltimeregister.DateOffice,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & " And FORMAT(tblCalander.mDate,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
           " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
           " And (tbltimeregister.Status = 'MIS' Or tbltimeregister.HoursWorked >" & g_DummyHoursWorked & " Or tbltimeregister.LateArrival >" & g_DummyLateArrival & " Or tbltimeregister.EarlyDeparture > " & g_DummyEarlyDeparture & ")" & _
           " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & strsortorder

            adapA = New OleDbDataAdapter(strsql, Common.con1)
            adapA.Fill(Rs_Report)
        Else
            strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tblTimeRegister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tbltimeregister.ShiftAttended,tbltimeregister.In1,tbltimeregister.Out1,tbltimeregister.In2,tbltimeregister.Out2,tbltimeregister.HoursWorked,tbltimeregister.LateArrival,tbltimeregister.EarlyDeparture,tbltimeregister.Status,tbltimeregister.OtDuration,tblCalander.mDate,tblCalander.Process,tblCalander.NRTCProc" & _
             " from tblCatagory,tblDivision,  tbltimeregister,tblEmployee,tblCalander,tblCompany,tblDepartment" & _
             " Where tblCatagory.Cat = tblEmployee.Cat And tbltimeregister.PayCode = tblEmployee.PayCode And tbltimeregister.DateOffice = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & " And tblCalander.mDate = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
             " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
             " And (tbltimeregister.Status = 'MIS' Or tbltimeregister.HoursWorked >" & g_DummyHoursWorked & " Or tbltimeregister.LateArrival >" & g_DummyLateArrival & " Or tbltimeregister.EarlyDeparture > " & g_DummyEarlyDeparture & ")" & _
             " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & strsortorder
            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(Rs_Report)
        End If
        'Else
        'strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tbltimeregisterD.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregisterD.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tbltimeregisterD.ShiftAttended,tbltimeregisterD.In1,tbltimeregisterD.Out1,tbltimeregisterD.In2,tbltimeregisterD.Out2,tbltimeregisterD.HoursWorked,tbltimeregisterD.LateArrival,tbltimeregisterD.EarlyDeparture,tbltimeregisterD.Status,tbltimeregisterD.OtDuration,tblCalander.mDate,tblCalander.Process,tblCalander.NRTCProc" & _
        '    " from tblCatagory,tblDivision,  tbltimeregisterD,tblEmployee,tblCalander,tblCompany,tblDepartment" & _
        '    " Where tblCatagory.Cat = tblEmployee.Cat And tblDivision.DivisionCode = tblEmployee.DivisionCode And tbltimeregisterD.PayCode = tblEmployee.PayCode And tbltimeregisterD.DateOffice = '" & Format(frmDailyAttReport.txtFromDate.Value, "MMM dd yyyy") & "'" & " And tblCalander.mDate = '" & Format(frmDailyAttReport.txtFromDate.Value, "MMM dd yyyy") & "'" & _
        '    " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
        '    " And (tbltimeregisterD.Status = 'MIS' Or tbltimeregisterD.HoursWorked >" & g_DummyHoursWorked & " Or tbltimeregisterD.LateArrival >" & g_DummyLateArrival & " Or tbltimeregisterD.EarlyDeparture > " & g_DummyEarlyDeparture & ")" & _
        '    " And (tblemployee.LeavingDate>='" & Format(frmDailyAttReport.txtFromDate, "MMM dd yyyy") & "' or tblemployee.LeavingDate is null)" & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause)
        'End If
        'Rs_Report = Cn.Execute(strsql)
        If Rs_Report.Tables(0).Rows.Count < 1 Then
            'G_Report_Error = False
            'Screen.MousePointer = vbDefault
            XtraMessageBox.Show(ulf, "<size=10>There is no missing punches or under check parameter punch for any Employee on this Date.</size>", "<size=9>iAS</size>")
            'mReportPrintStatus = False
            Exit Sub
        End If
        If strsortorder = "" Then
            'Rs_Report.Sort = "PayCode"
        Else
            'Rs_Report.Sort = strsortorder
        End If
        rowcnt = 1
        xlapp.Visible = True
        xlapp.Columns.Font.Name = "TAHOMA"
        xlapp.Columns.Font.Size = 8
        xlapp.Columns.Font.ColorIndex = 1 'vbBlack
        xlapp.Cells(rowcnt, 5).Font.Bold = True
        xlapp.Cells(rowcnt, 5) = "Company Name:" & CommonReport.g_CompanyNames
        rowcnt = rowcnt + 1
        xlapp.Cells(rowcnt, 5).Font.Bold = True
        xlapp.Cells(rowcnt, 5) = "Run Date & Time :" & Format(Now(), "dd/MM/yyyy HH:mm")
        rowcnt = rowcnt + 2
        xlapp.Cells(rowcnt, 4).Font.Bold = True
        xlapp.Cells(rowcnt, 4) = "MISSING PUNCH OR REVERIFICATION REPORT FOR DATE : " & DateEdit1.DateTime.ToString("dd/MM/yyyy")
        rowcnt = rowcnt + 1
        rowcnt = rowcnt + 1


        xlapp.Range("A6:P6").Font.Bold = True
        xlapp.Range("A6:P6").Font.ColorIndex = 2 'vbWhite
        'xlapp.Range("A6:P6").HorizontalAlignment = xlLeft
        xlapp.Range("A6:P6").Interior.ColorIndex = 51
        xlapp.Range("A6:P6").Borders.LineStyle = XlLineStyle.xlContinuous

        xlapp.Cells(rowcnt, 2) = "Srl No."
        xlapp.Cells(rowcnt, 3) = "PayCode"
        xlapp.Cells(rowcnt, 4) = "Card No"
        xlapp.Cells(rowcnt, 5) = "Employee Name"
        xlapp.Cells(rowcnt, 7) = "Shift  "
        xlapp.Cells(rowcnt, 8) = "   In "
        xlapp.Cells(rowcnt, 9) = " Lunch Out"
        xlapp.Cells(rowcnt, 10) = " Lunch In "
        xlapp.Cells(rowcnt, 11) = "   Out "
        xlapp.Cells(rowcnt, 12) = " Works "
        xlapp.Cells(rowcnt, 13) = "  Late "
        xlapp.Cells(rowcnt, 14) = " Early "
        xlapp.Cells(rowcnt, 15) = "Status "
        xlapp.Cells(rowcnt, 16) = "OT "
        rowcnt = rowcnt + 1
        mintLine = 9
        'With Rs_Report
        'If blnDeptAvailable And Not .EOF Then
        '    rowcnt = rowcnt + 1
        '    If Left(strsortorder, 10) = "Department" Or frmSorting.optDeptEmpName.Value Then
        '        xlapp.Cells(rowcnt, 1) = "** Department Code & Name : " & !DepartmentCode & "  " & !DepartmentName
        '    ElseIf Left(strsortorder, 1) = "Division" Or frmSorting.optSectionPaycode.Value Then
        '        xlapp.Cells(rowcnt, 1) = "** Section Code & Name : " & !DivisionCode & "  " & !DivisionName
        '    Else
        '        xlapp.Cells(rowcnt, 1) = "** Category Code & Name : " & !Cat & "  " & !CatagoryName
        '    End If
        '    rowcnt = rowcnt + 1
        '    mintLine = mintLine + 3
        'End If
        'Do While Not .EOF
        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            'strStatus = IIf(IsNull(!Status) Or Len(Trim(!Status)) = 0, Space(5), !Status) & Space(2)
            'lngHoursWorked = IIf(IsNull(!HOURSWORKED) Or !HOURSWORKED = 0, 0, !HOURSWORKED)
            'lngLateArrival = IIf(IsNull(!latearrival) Or !latearrival = 0, 0, !latearrival)
            'lngEarlyDeparture = IIf(IsNull(!earlydeparture) Or !earlydeparture = 0, 0, !earlydeparture)

            strStatus = IIf(Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim = "", Space(5), Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim & Space(2))
            lngHoursWorked = IIf(Rs_Report.Tables(0).Rows(i).Item("HOURSWORKED").ToString.Trim = "", 0, Rs_Report.Tables(0).Rows(i).Item("HOURSWORKED").ToString.Trim)
            lngLateArrival = IIf(Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim = "", 0, Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim)
            lngEarlyDeparture = IIf(Rs_Report.Tables(0).Rows(i).Item("earlydeparture").ToString.Trim = "", 0, Rs_Report.Tables(0).Rows(i).Item("earlydeparture").ToString.Trim)

            mCount = mCount + 1
            xlapp.Cells(rowcnt, 2).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 2) = "'" & mCount
            xlapp.Cells(rowcnt, 3).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 3) = "'" & Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim
            xlapp.Cells(rowcnt, 4).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 4) = "'" & Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim
            xlapp.Cells(rowcnt, 5).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 5) = Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim
            xlapp.Cells(rowcnt, 6).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 7).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 7) = Rs_Report.Tables(0).Rows(i).Item("ShiftAttended").ToString.Trim
            xlapp.Cells(rowcnt, 8).Borders.LineStyle = XlLineStyle.xlContinuous
            If Rs_Report.Tables(0).Rows(i).Item("In1").ToString.Trim = "" Then
                xlapp.Cells(rowcnt, 8) = (Space(5))
            Else
                xlapp.Cells(rowcnt, 8) = (Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("In1").ToString.Trim).ToString("HH:mm") & Space(1))
            End If

            xlapp.Cells(rowcnt, 9).Borders.LineStyle = XlLineStyle.xlContinuous
            If Rs_Report.Tables(0).Rows(i).Item("out1").ToString.Trim = "" Then
                xlapp.Cells(rowcnt, 9) = (Space(5))
            Else
                xlapp.Cells(rowcnt, 9) = (Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("out1").ToString.Trim).ToString("HH:mm") & Space(1))
            End If

            xlapp.Cells(rowcnt, 10).Borders.LineStyle = XlLineStyle.xlContinuous
            If Rs_Report.Tables(0).Rows(i).Item("in2").ToString.Trim = "" Then
                xlapp.Cells(rowcnt, 10) = (Space(5))
            Else
                xlapp.Cells(rowcnt, 10) = (Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("in2").ToString.Trim).ToString("HH:mm") & Space(1))
            End If

            xlapp.Cells(rowcnt, 11).Borders.LineStyle = XlLineStyle.xlContinuous
            If Rs_Report.Tables(0).Rows(i).Item("out2").ToString.Trim = "" Then
                xlapp.Cells(rowcnt, 11) = (Space(5))
            Else
                xlapp.Cells(rowcnt, 11) = (Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("out2").ToString.Trim).ToString("HH:mm") & Space(1))
            End If

            xlapp.Cells(rowcnt, 12).Borders.LineStyle = XlLineStyle.xlContinuous
            If Rs_Report.Tables(0).Rows(i).Item("HOURSWORKED").ToString.Trim = "0" Then
                xlapp.Cells(rowcnt, 12) = "0"
            Else
                xlapp.Cells(rowcnt, 12) = Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("HOURSWORKED").ToString.Trim) / 60).ToString("00") & ":" & (Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("HOURSWORKED").ToString.Trim) Mod 60).ToString("00")
            End If
            'xlapp.Cells(rowcnt, 12) = "'" & Allign(Min2Hr(!HOURSWORKED))
            xlapp.Cells(rowcnt, 13).Borders.LineStyle = XlLineStyle.xlContinuous
            If Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim = "0" Then
                xlapp.Cells(rowcnt, 13) = "0"
            Else
                xlapp.Cells(rowcnt, 13) = Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim) / 60).ToString("00") & ":" & (Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim) Mod 60).ToString("00")
            End If
            'xlapp.Cells(rowcnt, 13) = "'" & Allign(Min2Hr(!latearrival))
            xlapp.Cells(rowcnt, 14).Borders.LineStyle = XlLineStyle.xlContinuous
            If Rs_Report.Tables(0).Rows(i).Item("earlydeparture").ToString.Trim = "0" Then
                xlapp.Cells(rowcnt, 14) = "0"
            Else
                xlapp.Cells(rowcnt, 14) = Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("earlydeparture").ToString.Trim) / 60).ToString("00") & ":" & (Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("earlydeparture").ToString.Trim) Mod 60).ToString("00")
            End If
            'xlapp.Cells(rowcnt, 14) = "'" & Allign(Min2Hr(!earlydeparture))
            xlapp.Cells(rowcnt, 15).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 15) = Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim
            xlapp.Cells(rowcnt, 16).Borders.LineStyle = XlLineStyle.xlContinuous
            If Rs_Report.Tables(0).Rows(i).Item("OtDuration").ToString.Trim = "0" Then
                xlapp.Cells(rowcnt, 16) = "0"
            Else
                xlapp.Cells(rowcnt, 16) = Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("OtDuration").ToString.Trim) / 60).ToString("00") & ":" & (Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("OtDuration").ToString.Trim) Mod 60).ToString("00")
            End If
            'xlapp.Cells(rowcnt, 16) = "'" & Allign(Min2Hr(!OtDuration))

            rowcnt = rowcnt + 1
            mintLine = mintLine + 1
            mblnCheckReport = True
            'If Left(strsortorder, 10) = "Department" Then
            '    strDeptDivCode = !DepartmentCode
            'ElseIf Left(strsortorder, 8) = "Division" Then
            '    strDeptDivCode = !DivisionCode
            'Else
            '    strDeptDivCode = !Cat
            'End If
            '.MoveNext()
            'If blnDeptAvailable And Not .EOF Then
            '    If Left(strsortorder, 10) = "Department" Then
            '        strCurrentDeptDivCode = !DepartmentCode
            '    ElseIf Left(strsortorder, 8) = "Division" Then
            '        strCurrentDeptDivCode = !DivisionCode
            '    Else
            '        strCurrentDeptDivCode = !Cat
            '    End If
            '    If strDeptDivCode <> strCurrentDeptDivCode Then
            '        rowcnt = rowcnt + 1
            '        If Left(strsortorder, 10) = "Department" Or frmSorting.optDeptEmpName.Value Then
            '            xlapp.Cells(rowcnt, 1) = "** Department Code & Name : " & !DepartmentCode & "  " & !DepartmentName
            '        ElseIf Left(strsortorder, 1) = "Division" Or frmSorting.optSectionPaycode.Value Then
            '            xlapp.Cells(rowcnt, 1) = "** Section Code & Name : " & !DivisionCode & "  " & !DivisionName
            '        Else
            '            xlapp.Cells(rowcnt, 1) = "** Category Code & Name : " & !Cat & "  " & !CatagoryName
            '        End If
            '        rowcnt = rowcnt + 1
            '        mintLine = mintLine + 3
            '    End If
            'End If
            'Loop
            'End With
        Next
        '        xlwb.SaveAs(App.Path & "\TimeWatch.xls")
        '        Exit Sub
        'ErrorGen:
        '        MsgBox(Err.Description, vbInformation + vbOKOnly, "Report Generation Information")
        '        Screen.MousePointer = vbDefault
        xlwb.SaveAs(mstrFile_Name)
    End Sub
    Sub SpotXl_DeviceWiseGrid(ByVal strsortorder As String)
        Dim strsql As String, mCount As Integer
        Me.Cursor = Cursors.WaitCursor
        Dim rowcnt As Integer
        If XtraShortOrder.g_SortOrder <> "" Then
            strsortorder = XtraShortOrder.g_SortOrder
        End If
        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If
        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".xls"

        mstrDepartmentCode = " "
        mblnCheckReport = False
        mintPageNo = 1
        mintLine = 1

        'Call CreateTableMachine
        'MachinePunch frmDailyAttReport.txtFromDate, frmDailyAttReport.TxtToDate ' Creating the data for the generation of the Spot reports.
        Dim fromdate As DateTime = Convert.ToDateTime(DateEdit1.DateTime.ToString("yyyy-MM-dd") & " " & TextFromTime.Text.Trim & ":00")
        Dim Todate As DateTime = Convert.ToDateTime(DateEdit1.DateTime.ToString("yyyy-MM-dd") & " " & TextToTime.Text.Trim & ":59")

        If Common.servername = "Access" Then
            If GridViewDevice.SelectedRowsCount = 0 Then
                strsql = "Select tblEmployee.empname,tblCatagory.Catagoryname, tblDepartment.DepartmentCode, tblDepartment.DepartmentName, " &
                     " tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, tblEmployee.DepartmentCode , machinerawpunch.*,tblmachine.branch from tblCatagory, tblDepartment, tblEmployee ,machinerawpunch,tblmachine where machinerawpunch.mc_no=tblmachine.id_no and FORMAT(machinerawpunch.OfficePunch, 'yyyy-MM-dd HH:mm:ss') between '" & fromdate.ToString("yyyy-MM-dd HH:mm:ss") & "' and '" & Todate.ToString("yyyy-MM-dd HH:mm:ss") & "' and tblEmployee.departmentcode = tbldepartment.departmentcode And tblEmployee.CAT = tblCatagory.cat And   " &
                     " MachineRawPunch.Paycode = tblEmployee.Paycode " & g_HODDepartmentCode &
                     " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause)
            Else

                Dim com() As String = PopupContainerEditDevice.EditValue.Split(",")
                Dim ls As New List(Of String)()
                For x As Integer = 0 To com.Length - 1
                    ls.Add(com(x).Trim)
                Next

                strsql = "Select tblEmployee.empname,tblCatagory.Catagoryname, tblDepartment.DepartmentCode, tblDepartment.DepartmentName,  " &
                     " tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, tblEmployee.DepartmentCode , machinerawpunch.*,tblmachine.branch from tblCatagory,  tblDepartment, tblEmployee ,machinerawpunch,tblmachine where  machinerawpunch.mc_no IN ('" & String.Join("', '", ls.ToArray()) & "') and machinerawpunch.mc_no=tblmachine.id_no and FORMAT(machinerawpunch.OfficePunch, 'yyyy-MM-dd HH:mm:ss') between '" & fromdate.ToString("yyyy-MM-dd HH:mm:ss") & "' and '" & Todate.ToString("yyyy-MM-dd HH:mm:ss") & "' and tblEmployee.departmentcode = tbldepartment.departmentcode And tblEmployee.CAT = tblCatagory.cat And " &
                     " MachineRawPunch.Paycode = tblEmployee.Paycode " & g_HODDepartmentCode &
                     " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause)
            End If
            adapA = New OleDbDataAdapter(strsql, Common.con1)
            adapA.Fill(Rs_Report)
        Else
            If GridViewDevice.SelectedRowsCount = 0 Then
                strsql = "Select tblEmployee.empname,tblCatagory.Catagoryname, tblDepartment.DepartmentCode, tblDepartment.DepartmentName,  " &
                     " tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, tblEmployee.DepartmentCode , machinerawpunch.*,tblmachine.branch from tblCatagory, tblDepartment, tblEmployee ,machinerawpunch,tblmachine where machinerawpunch.mc_no=tblmachine.id_no and machinerawpunch.officepunch between '" & fromdate.ToString("yyyy-MM-dd HH:mm:ss") & "' and '" & Todate.ToString("yyyy-MM-dd HH:mm:ss") & "' and tblEmployee.departmentcode = tbldepartment.departmentcode And tblEmployee.CAT = tblCatagory.cat And " &
                     " MachineRawPunch.Paycode = tblEmployee.Paycode " & g_HODDepartmentCode &
                     " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause)
            Else
                Dim com() As String = PopupContainerEditDevice.EditValue.Split(",")
                Dim ls As New List(Of String)()
                For x As Integer = 0 To com.Length - 1
                    ls.Add(com(x).Trim)
                Next
                strsql = "Select tblEmployee.empname,tblCatagory.Catagoryname, tblDepartment.DepartmentCode, tblDepartment.DepartmentName, " &
                     " tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, tblEmployee.DepartmentCode , machinerawpunch.*,tblmachine.branch from tblCatagory, tblDepartment, tblEmployee ,machinerawpunch,tblmachine where  machinerawpunch.mc_no IN ('" & String.Join("', '", ls.ToArray()) & "') and machinerawpunch.mc_no=tblmachine.id_no and machinerawpunch.officepunch between '" & fromdate.ToString("yyyy-MM-dd HH:mm:ss") & "' and '" & Todate.ToString("yyyy-MM-dd HH:mm:ss") & "' and tblEmployee.departmentcode = tbldepartment.departmentcode And tblEmployee.CAT = tblCatagory.cat And " &
                     " MachineRawPunch.Paycode = tblEmployee.Paycode " & g_HODDepartmentCode &
                     " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause)
                'strsql = "Select machinerawpunch.*,tblmachine.branch from machinerawpunch,tblmachine where machinerawpunch.mc_no=tblmachine.id_no and machinerawpunch.officepunch between '" & Format(frmDailyAttReport.txtFromDate, "yyyy mmm dd") & " 00:00:00' and '" & Format(frmDailyAttReport.TxtToDate, "yyyy mmm dd") & " 23:59:59' and machinerawpunch.mc_no='" & Mid(Trim(frmDailyAttReport.cbomachine.Text), 1, 2) & "' "
            End If
            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(Rs_Report)
        End If

        If Rs_Report.Tables(0).Rows.Count = 0 Then
            Me.Cursor = Cursors.Default
            XtraMessageBox.Show(ulf, "<size=10>No Recode Found.</size>", "<size=9>iAS</size>")
            Exit Sub
        End If
        rowcnt = 1

        Common.frodatetodatetoReportGrid = "DEVICE WISE REPORT  FOR " & fromdate.ToString("dd/MM/yyyy")


        Common.tbl = New Data.DataTable()
        Common.tbl.Columns.Add("User No", GetType(String))
        Common.tbl.Columns.Add("Emp. ID", GetType(String))
        Common.tbl.Columns.Add("Employee Name", GetType(String))
        Common.tbl.Columns.Add("Date", GetType(String))
        Common.tbl.Columns.Add("Time", GetType(String))
        Common.tbl.Columns.Add("Location", GetType(String))
        Common.tbl.Columns.Add("Machine No", GetType(String))
        Common.tbl.Columns.Add("Punch Type", GetType(String))

        mCount = 0
        Dim strPayCode As String = ""
        Dim paycodelist As New List(Of String)()
        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1  'Do While Not .EOF
            paycodelist.Add(Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim)
            Common.tbl.Rows.Add(Rs_Report.Tables(0).Rows(i).Item("CardNo").ToString.Trim, Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim,
                    Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim, Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("officepunch").ToString.Trim).ToString("dd/MM/yyyy"),
                    Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("officepunch").ToString.Trim).ToString("HH:mm:ss"), Rs_Report.Tables(0).Rows(i).Item("Branch").ToString.Trim,
                    Rs_Report.Tables(0).Rows(i).Item("MC_No").ToString.Trim, Rs_Report.Tables(0).Rows(i).Item("INOUT").ToString.Trim)
        Next ' Loop

        'Dim totalInCount As Double = 0
        'Dim totalOutCount As Double = 0
        'Dim paycodeArray = paycodelist.Distinct.ToArray
        'For i As Integer = 0 To paycodeArray.Length - 1
        '    Dim count As Double = 0
        '    For j As Integer = 0 To Rs_Report.Tables.Count - 1
        '        If paycodeArray(i) = Rs_Report.Tables(0).Rows(j).Item("paycode").ToString.Trim Then
        '            count = count + 1
        '        End If
        '    Next
        '    If count Mod 2 = 0 Then
        '        totalInCount = totalInCount + 1
        '    Else
        '        totalOutCount = totalOutCount + 1
        '    End If
        'Next
        'Common.tbl.Rows.Add("")
        'Common.tbl.Rows.Add("Total In Campus", totalInCount)
        'Common.tbl.Rows.Add("Total Out Campus", totalOutCount)
        Me.Cursor = Cursors.Default
        XtraReportGrid.ShowDialog()
    End Sub
    Sub SpotXl_LateArrivalGrid(strsortorder As String)
        Dim whereClause As String
        If Common.ReportMail = True Or Common.dashBoardClick = "Late" Then
            DateEdit1.DateTime = Now
            'whereClause = Common.whereClauseEmail
            If Common.whereClauseEmail.Trim = "" Then
                whereClause = " TBLEmployee.ACTIVE='Y'"
            Else
                whereClause = Common.whereClauseEmail & " and TBLEmployee.ACTIVE='Y'"
            End If
            If Common.IsNepali = "Y" Then
                Dim DC As New DateConverter()
                Dim tmpNow As String = DC.ToBS(New Date(Now.Year, Now.Month, Now.Day))
                Dim dojTmp() As String = tmpNow.Split("-")
                Common.runningDateTime = dojTmp(2) & "/" & Common.NepaliMonth(dojTmp(1) - 1) & "/" & dojTmp(0) & " " & Now.ToString("HH:mm")
                Dim Vstart As String = DC.ToBS(New Date(DateEdit1.DateTime.Year, DateEdit1.DateTime.Month, DateEdit1.DateTime.Day))
                dojTmp = Vstart.Split("-")
                ForDate = dojTmp(2) & "/" & Common.NepaliMonth(dojTmp(1) - 1) & "/" & dojTmp(0)
            Else
                ForDate = DateEdit1.DateTime.ToString("dd/MM/yyyy")
                Common.runningDateTime = Now.ToString("dd/MM/yyyy HH:mm")
            End If
        Else
            whereClause = g_WhereClause
        End If

        ' If Common.USERTYPE <> "A" Then
        '    Dim emp() As String = Common.Auth_Branch.Split(",")
        '    Dim ls As New List(Of String)()
        '    For x As Integer = 0 To emp.Length - 1
        '        ls.Add(emp(x).Trim)
        '    Next
        '    If whereClause = "" Then
        '        whereClause = " TBLEmployee.BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "')"
        '    Else
        '        whereClause = whereClause & " and TBLEmployee.BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "')"
        '    End If
        'End If

        If Common.USERTYPE <> "A" Then
            Dim emp() As String = Common.Auth_Branch.Split(",")
            Dim ls As New List(Of String)()
            For x As Integer = 0 To emp.Length - 1
                ls.Add(emp(x).Trim)
            Next

            Dim com() As String = Common.auth_comp.Split(",")
            Dim lsC As New List(Of String)()
            For x As Integer = 0 To com.Length - 1
                lsC.Add(com(x).Trim)
            Next

            If whereClause = "" Then
                whereClause = " TBLEmployee.BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "')  and TBLEmployee.COMPANYCODE IN ('" & String.Join("', '", lsC.ToArray()) & "')"
            Else
                whereClause = whereClause & " and TBLEmployee.BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "')  and TBLEmployee.COMPANYCODE IN ('" & String.Join("', '", lsC.ToArray()) & "')"
            End If
        End If

        'Dim xlapp As Excel.Application
        'Dim xlwb As Excel.Workbook
        'Dim xlst As Excel.Sheets
        Dim strsql As String, mCount As Integer
        Dim v_Late As String
        Dim rowcnt As Integer
        'xlapp = CreateObject("Excel.Application")
        'xlwb = xlapp.Workbooks.Add

        If XtraShortOrder.g_SortOrder <> "" Then
            strsortorder = XtraShortOrder.g_SortOrder
        End If
        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If
        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".xls"

        mstrDepartmentCode = " "
        mintPageNo = 1
        mintLine = 1
        mblnCheckReport = False

        If Common.servername = "Access" Then
            strsql = " Select tblCatagory.Catagoryname, tblDivision.DivisionName, tblTimeregister.PAYCODE, tblTimeregister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, " &
                "tblDepartment.DepartmentCode, tblDepartment.DepartmentName,tblEmployee.PresentCARDNO, tblEmployee.EMPNAME, tblTimeregister.DATEOFFICE, " &
                "tblTimeregister.SHIFTATTENDED, tblTimeregister.shiftStartTime, tblTimeregister.In1, tblTimeregister.Latearrival, " &
                "IIF (tblTimeregister.Latearrival <= 10,'**','  ') as Late1, " &
                "IIF (tblTimeregister.Latearrival > 10 And tblTimeregister.Latearrival <= 30, '**' ,'  ') AS Late10, " &
                "IIF(tblTimeregister.Latearrival > 30 And tblTimeregister.Latearrival <= 60,'**','  ') AS Late30, " &
                "IIF (tblTimeregister.Latearrival > 60 ,'**', '  ') AS Late60, tblEmployee.BUS " &
                " From tblCatagory, tblDivision,tblTimeregister, tblEmployee, tblDepartment, tblCompany " &
                "Where tblEmployee.CAT = tblCatagory.cat And tblEmployee.PayCode = tblTimeregister.PayCode And " &
                "tblEmployee.DepartmentCode = tblDepartment.DepartmentCode AND format(tbltimeregister.DateOffice,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' AND  " &
                "tblEmployee.CompanyCode = tblCompany.CompanyCode And  " &
                "tblTimeregister.Latearrival > 0" & g_HODDepartmentCode &
                "" & IIf(Len(Trim(whereClause)) = 0, "", " AND " & whereClause)
            If Common.IsCompliance Then
                strsql = Microsoft.VisualBasic.Strings.Replace(strsql, "tbltimeregister", "tblTimeRegisterD", 1, -1, Microsoft.VisualBasic.Constants.vbTextCompare)
            End If

            adapA = New OleDbDataAdapter(strsql, Common.con1)
            adapA.Fill(Rs_Report)
        Else
            strsql = " Select tblCatagory.Catagoryname, tblDivision.DivisionName, tblTimeregister.PAYCODE, tblTimeregister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, " &
                "tblDepartment.DepartmentCode, tblDepartment.DepartmentName,tblEmployee.PresentCARDNO, tblEmployee.EMPNAME, tblTimeregister.DATEOFFICE, " &
                "tblTimeregister.SHIFTATTENDED, tblTimeregister.shiftStartTime, tblTimeregister.In1, tblTimeregister.Latearrival, " &
                "Late1 = Case When tblTimeregister.Latearrival <= 10 then '**' Else '  'End, " &
                "Late10 = Case When tblTimeregister.Latearrival > 10 And tblTimeregister.Latearrival <= 30 then '**' Else '  ' End, " &
                "Late30 = Case When tblTimeregister.Latearrival > 30 And tblTimeregister.Latearrival <= 60 then '**' Else '  ' End, " &
                "Late60 = Case When tblTimeregister.Latearrival > 60 then '**' Else '  ' End, tblEmployee.BUS " &
                " From tblCatagory, tblDivision,tblTimeregister, tblEmployee, tblDepartment, tblCompany " &
                "Where tblEmployee.CAT = tblCatagory.cat And tblEmployee.PayCode = tblTimeregister.PayCode And " &
                "tblEmployee.DepartmentCode = tblDepartment.DepartmentCode AND tbltimeregister.DateOffice = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' AND  " &
                "tblEmployee.CompanyCode = tblCompany.CompanyCode And  " &
                "tblTimeregister.Latearrival > 0" & g_HODDepartmentCode &
                " " & IIf(Len(Trim(whereClause)) = 0, "", " AND " & whereClause)
            If Common.IsCompliance Then
                strsql = Microsoft.VisualBasic.Strings.Replace(strsql, "tbltimeregister", "tblTimeRegisterD", 1, -1, Microsoft.VisualBasic.Constants.vbTextCompare)
            End If

            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(Rs_Report)
        End If

        If strsortorder = "" Then
            'Rs_Report.Sort = "PayCode"
        Else
            'Rs_Report.Sort = strsortorder
        End If

        If Rs_Report.Tables(0).Rows.Count < 1 Then
            If Common.ReportMail = True Then
            Else
                XtraMessageBox.Show(ulf, "<size=10>No Data Available For this Report</size>", "<size=9>iAS</size>")
            End If
            Exit Sub
        End If
        mCount = 0
        rowcnt = 1

        Common.frodatetodatetoReportGrid = "LATE ARRIVAL REPORT FOR DATE : " & ForDate 'DateEdit1.DateTime.ToString("dd/MM/yyyy")
        rowcnt = rowcnt + 1

        Me.Cursor = Cursors.WaitCursor
        Common.tbl = New Data.DataTable()
        Common.tbl.Columns.Add("Srl", GetType(String))
        Common.tbl.Columns.Add("PayCode", GetType(String))
        Common.tbl.Columns.Add("Card No.", GetType(String))
        Common.tbl.Columns.Add("Employee Name", GetType(String))
        Common.tbl.Columns.Add(" ", GetType(String))
        Common.tbl.Columns.Add("Shift", GetType(String))
        Common.tbl.Columns.Add("Shift Start", GetType(String))
        Common.tbl.Columns.Add("In", GetType(String))
        Common.tbl.Columns.Add("Shift Late", GetType(String))
        Common.tbl.Columns.Add("---------", GetType(String))
        Common.tbl.Columns.Add("Shift Late ", GetType(String))
        Common.tbl.Columns.Add("----------", GetType(String))
        Common.tbl.Columns.Add("Bus Route", GetType(String))


        Common.tbl.Rows.Add("", "", "", "", "", "", "", "", "", ">(0.10)", ">(0.30)", ">(1.00)")

        rowcnt = rowcnt + 1

        Dim shiftStartTimeStr As String
        Dim In1Str As String
        Dim DepartmentCode = Rs_Report.Tables(0).Rows(0).Item("DepartmentCode").ToString.Trim
        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            If strsortorder.Contains("Department") Then
                If i = 0 Then
                    'objWriter.WriteLine("Department : " & Rs_Report.Tables(0).Rows(i).Item("DepartmentName").ToString.Trim)
                    Common.tbl.Rows.Add("Department : ", Rs_Report.Tables(0).Rows(i).Item("DepartmentName").ToString.Trim)
                End If
                If DepartmentCode <> Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim Then
                    DepartmentCode = Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim
                    Common.tbl.Rows.Add()
                    Common.tbl.Rows.Add("Department : ", Rs_Report.Tables(0).Rows(i).Item("DepartmentName").ToString.Trim)
                End If
            End If
            'If mFirst Or mintLine > g_LinesPerPage Or (strsortorder.Trim = "Department" And strDeptDivCode <> Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim And g_SkipAfterDept) Then
            If strsortorder = "Department" Or XtraShortOrder.CheckDeptName.Checked Then
                If mstrDepartmentCode <> Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim Then
                    rowcnt = rowcnt + 2
                    'xlapp.Cells(rowcnt, 1) = "*** Department Code & Name " & Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim & " " & Rs_Report.Tables(0).Rows(i).Item("DepartmentName").ToString.Trim
                    mstrDepartmentCode = Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString
                    rowcnt = rowcnt + 1
                End If
            End If
            If strsortorder.Contains("Division") Or XtraShortOrder.CheckSectionPay.Checked Then
                If mstrDepartmentCode <> Rs_Report.Tables(0).Rows(i).Item("DivisionCode").ToString Then
                    rowcnt = rowcnt + 2
                    'xlapp.Cells(rowcnt, 1) = "*** Section Code & Name " & Rs_Report.Tables(0).Rows(i).Item("DivisionCode").ToString.Trim & " " & Rs_Report.Tables(0).Rows(i).Item("DivisionName").ToString.Trim
                    mstrDepartmentCode = Rs_Report.Tables(0).Rows(i).Item("DivisionCode").ToString.Trim
                    rowcnt = rowcnt + 1
                End If
            End If
            If strsortorder.Contains("Cat") Or XtraShortOrder.CheckCatagoryPaycode.Checked Then
                If mstrDepartmentCode <> Rs_Report.Tables(0).Rows(i).Item("Cat").ToString.Trim Then
                    rowcnt = rowcnt + 2

                    'xlapp.Cells(rowcnt, 1) = "*** Category Code & Name " & Rs_Report.Tables(0).Rows(i).Item("Cat").ToString.Trim & " " & Rs_Report.Tables(0).Rows(i).Item("CatagoryName").ToString.Trim
                    mstrDepartmentCode = Rs_Report.Tables(0).Rows(i).Item("Cat").ToString.Trim
                    rowcnt = rowcnt + 1
                End If
            End If
            mCount = mCount + 1

            If Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim = "0" Then
                v_Late = ""
            Else
                'v_Late = Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim Mod 60).ToString("00")
                v_Late = Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim Mod 60).ToString("00")
            End If
            'v_Late = Length5(Min2Hr(!latearrival))
            rowcnt = rowcnt + 1

            If Rs_Report.Tables(0).Rows(i).Item("shiftStartTime").ToString.Trim = "" Then
                shiftStartTimeStr = ""
            Else
                shiftStartTimeStr = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("shiftStartTime").ToString.Trim).ToString("HH:mm")
            End If

            If Rs_Report.Tables(0).Rows(i).Item("In1").ToString.Trim = "" Then
                In1Str = ""
            Else
                In1Str = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("In1").ToString.Trim).ToString("HH:mm")
            End If
            mblnCheckReport = True
            Common.tbl.Rows.Add(mCount, Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim, Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim, Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim, "",
                         Rs_Report.Tables(0).Rows(i).Item("ShiftAttended").ToString.Trim, shiftStartTimeStr, In1Str, v_Late, Rs_Report.Tables(0).Rows(i).Item("bus").ToString.Trim)
        Next
        Me.Cursor = Cursors.Default
        XtraReportGrid.ShowDialog()
    End Sub
    Sub SpotXl_AbsenteeismGrid(strsortorder As String)
        Dim whereClause As String = ""
        If Common.ReportMail = True Or Common.dashBoardClick = "Absent" Then
            DateEdit1.DateTime = Now
            'whereClause = Common.whereClauseEmail
            If Common.whereClauseEmail.Trim = "" Then
                whereClause = " TBLEmployee.ACTIVE='Y'"
            Else
                whereClause = Common.whereClauseEmail & " and TBLEmployee.ACTIVE='Y'"
            End If
            If Common.IsNepali = "Y" Then
                Dim DC As New DateConverter()
                'Dim tmpNow As DateTime = DC.ToBS(New Date(Now.Year, Now.Month, Now.Day))
                'Common.runningDateTime = tmpNow.Day & "/" & Common.NepaliMonth(tmpNow.Month - 1) & "/" & tmpNow.Year & " " & Now.ToString("HH:mm")
                'Dim Vstart As DateTime = DC.ToBS(New Date(DateEdit1.DateTime.Year, DateEdit1.DateTime.Month, DateEdit1.DateTime.Day))
                'ForDate = Vstart.Day & "/" & Common.NepaliMonth(Vstart.Month - 1) & "/" & Vstart.Year

                Dim tmpNow As String = DC.ToBS(New Date(Now.Year, Now.Month, Now.Day))
                Dim dojTmp() As String = tmpNow.Split("-")
                Common.runningDateTime = dojTmp(2) & "/" & Common.NepaliMonth(dojTmp(1) - 1) & "/" & dojTmp(0) & " " & Now.ToString("HH:mm")
                Dim Vstart As String = DC.ToBS(New Date(DateEdit1.DateTime.Year, DateEdit1.DateTime.Month, DateEdit1.DateTime.Day))
                dojTmp = Vstart.Split("-")
                ForDate = dojTmp(2) & "/" & Common.NepaliMonth(dojTmp(1) - 1) & "/" & dojTmp(0)

            Else
                ForDate = DateEdit1.DateTime.ToString("dd/MM/yyyy")
                Common.runningDateTime = Now.ToString("dd/MM/yyyy HH:mm")
            End If
        Else
            whereClause = g_WhereClause
        End If

         If Common.USERTYPE <> "A" Then
            Dim emp() As String = Common.Auth_Branch.Split(",")
            Dim ls As New List(Of String)()
            For x As Integer = 0 To emp.Length - 1
                ls.Add(emp(x).Trim)
            Next

            Dim com() As String = Common.auth_comp.Split(",")
            Dim lsC As New List(Of String)()
            For x As Integer = 0 To com.Length - 1
                lsC.Add(com(x).Trim)
            Next

            If whereClause = "" Then
                whereClause = " TBLEmployee.BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "') and TBLEmployee.COMPANYCODE IN ('" & String.Join("', '", lsC.ToArray()) & "')"
            Else
                whereClause = whereClause & " and TBLEmployee.BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "') and TBLEmployee.COMPANYCODE IN ('" & String.Join("', '", lsC.ToArray()) & "')"
            End If
        End If

        'Dim xlapp As Excel.Application
        'Dim xlwb As Excel.Workbook
        'Dim xlst As Excel.Sheets
        Dim strsql As String, mCount As Integer
        Dim v_Absent As String '* 5
        Dim v_Leave As String '* 5
        Dim v_OnDuty As String '* 5
        Dim rowcnt As Integer
        'xlapp = CreateObject("Excel.Application")
        'xlwb = xlapp.Workbooks.Add

        mstrDepartmentCode = " "
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".xls"

        'On Error GoTo AbsenteeismError
        mintPageNo = 1
        mintLine = 1
        mblnCheckReport = False
        If XtraShortOrder.g_SortOrder <> "" Then
            strsortorder = XtraShortOrder.g_SortOrder
        End If
        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If
        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet

        If Common.servername = "Access" Then
            strsql = " Select tblCatagory.Catagoryname, tblDivision.DivisionName, tblTimeregister.Paycode,tblTimeregister.reason,tblTimeregister.Shift,TblEmployee.EmpName, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, TblEmployee.PresentCardno, TblDepartment.DepartmentCode, tblDepartment.DepartmentName,tblTimeregister.Status, tblTimeregister.LeaveType From  tblCatagory, tblDivision, tblTimeregister, TblEmployee, tblDepartment,tblCompany " & _
               " Where tblEmployee.CAT = tblCatagory.cat And (tblTimeregister.AbsentValue > 0 Or tblTimeregister.LeaveType In ('A', 'P', 'L')) And " & _
               " tblEmployee.companycode=tblCompany.companycode and tblEmployee.DepartmentCode = tblDepartment.DepartmentCode AND FORMAT(tbltimeregister.DateOffice,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
               " and tblEmployee.PayCode = tblTimeregister.Paycode" & g_HODDepartmentCode & _
               " " & IIf(Len(Trim(whereClause)) = 0, "", " And " & whereClause) & strsortorder
            If Common.IsCompliance Then
                strsql = Microsoft.VisualBasic.Strings.Replace(strsql, "tbltimeregister", "tblTimeRegisterD", 1, -1, Microsoft.VisualBasic.Constants.vbTextCompare)
            End If
            adapA = New OleDbDataAdapter(strsql, Common.con1)
            adapA.Fill(Rs_Report)
        Else
            strsql = " Select tblCatagory.Catagoryname, tblDivision.DivisionName, tblTimeregister.Paycode,tblTimeregister.reason,tblTimeregister.Shift,TblEmployee.EmpName, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, TblEmployee.PresentCardno, TblDepartment.DepartmentCode, tblDepartment.DepartmentName,tblTimeregister.Status, tblTimeregister.LeaveType From  tblCatagory, tblDivision, tblTimeregister, TblEmployee, tblDepartment,tblCompany " & _
               " Where tblEmployee.CAT = tblCatagory.cat And  (tblTimeregister.AbsentValue > 0 Or tblTimeregister.LeaveType In ('A', 'P', 'L')) And " & _
               " tblEmployee.companycode=tblCompany.companycode and tblEmployee.DepartmentCode = tblDepartment.DepartmentCode AND tbltimeregister.DateOffice = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
               " and tblEmployee.PayCode = tblTimeregister.Paycode" & g_HODDepartmentCode & _
               " " & IIf(Len(Trim(whereClause)) = 0, "", " And " & whereClause) & strsortorder
            If Common.IsCompliance Then
                strsql = Microsoft.VisualBasic.Strings.Replace(strsql, "tbltimeregister", "tblTimeRegisterD", 1, -1, Microsoft.VisualBasic.Constants.vbTextCompare)
            End If
            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(Rs_Report)
        End If
        If Rs_Report.Tables(0).Rows.Count < 1 Then
            If Common.ReportMail = True Then
            Else
                XtraMessageBox.Show(ulf, "<size=10>No Data Available For this Report</size>", "<size=9>iAS</size>")
            End If
            Exit Sub
        End If
        rowcnt = 1

        Common.frodatetodatetoReportGrid = " ABSENCE FROM DUTY ON DATE : " & ForDate 'DateEdit1.DateTime.ToString("dd/MM/yyyy")
        rowcnt = rowcnt + 1
        Me.Cursor = Cursors.WaitCursor
        Common.tbl = New Data.DataTable()
        Common.tbl.Columns.Add("Srl", GetType(String))
        Common.tbl.Columns.Add("PayCode", GetType(String))
        Common.tbl.Columns.Add("Card No.", GetType(String))
        Common.tbl.Columns.Add("Employee Name", GetType(String))
        Common.tbl.Columns.Add(" ", GetType(String))
        Common.tbl.Columns.Add("Absent", GetType(String))
        Common.tbl.Columns.Add("Leave", GetType(String))
        Common.tbl.Columns.Add("On Duty", GetType(String))
        Common.tbl.Columns.Add("Remarks", GetType(String))

        mCount = 0
        Dim DepartmentCode = Rs_Report.Tables(0).Rows(0).Item("DepartmentCode").ToString.Trim
        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            If strsortorder.Contains("Department") Then
                If i = 0 Then
                    'objWriter.WriteLine("Department : " & Rs_Report.Tables(0).Rows(i).Item("DepartmentName").ToString.Trim)
                    Common.tbl.Rows.Add("Department : ", Rs_Report.Tables(0).Rows(i).Item("DepartmentName").ToString.Trim)
                End If
                If DepartmentCode <> Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim Then
                    DepartmentCode = Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim
                    Common.tbl.Rows.Add()
                    Common.tbl.Rows.Add("Department : ", Rs_Report.Tables(0).Rows(i).Item("DepartmentName").ToString.Trim)
                End If
            End If
            If strsortorder = "Department" Or XtraShortOrder.CheckDeptName.Checked Then
                If mstrDepartmentCode <> Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim Then
                    rowcnt = rowcnt + 2
                    'xlapp.Cells(rowcnt, 1) = "*** Department Code & Name " & Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim & " " & Rs_Report.Tables(0).Rows(i).Item("DepartmentName").ToString.Trim
                    mstrDepartmentCode = Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString
                    rowcnt = rowcnt + 1
                End If
            End If
            If strsortorder.Contains("Division") Or XtraShortOrder.CheckSectionPay.Checked Then
                If mstrDepartmentCode <> Rs_Report.Tables(0).Rows(i).Item("DivisionCode").ToString Then
                    rowcnt = rowcnt + 2
                    'xlapp.Cells(rowcnt, 1) = "*** Section Code & Name " & Rs_Report.Tables(0).Rows(i).Item("DivisionCode").ToString.Trim & " " & Rs_Report.Tables(0).Rows(i).Item("DivisionName").ToString.Trim
                    mstrDepartmentCode = Rs_Report.Tables(0).Rows(i).Item("DivisionCode").ToString.Trim
                    rowcnt = rowcnt + 1
                End If
            End If
            If strsortorder.Contains("Cat") Or XtraShortOrder.CheckCatagoryPaycode.Checked Then
                If mstrDepartmentCode <> Rs_Report.Tables(0).Rows(i).Item("Cat").ToString.Trim Then
                    rowcnt = rowcnt + 2
                    'xlapp.Cells(rowcnt, 1) = "*** Category Code & Name " & Rs_Report.Tables(0).Rows(i).Item("Cat").ToString.Trim & " " & Rs_Report.Tables(0).Rows(i).Item("CatagoryName").ToString.Trim
                    mstrDepartmentCode = Rs_Report.Tables(0).Rows(i).Item("Cat").ToString.Trim
                    rowcnt = rowcnt + 1
                End If
            End If

            mCount = mCount + 1
            v_Absent = IIf(Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim = "A" Or Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim = "HLF" Or Rs_Report.Tables(0).Rows(i).Item("LEAVETYPE").ToString.Trim = "A", Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim, " ")
            v_OnDuty = IIf(Rs_Report.Tables(0).Rows(i).Item("LEAVETYPE").ToString.Trim = "P", Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim, "   ")
            v_Leave = IIf(Rs_Report.Tables(0).Rows(i).Item("LEAVETYPE").ToString.Trim = "L", Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim, "   ")
            Common.tbl.Rows.Add(mCount, Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim, Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim, Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim, "", v_Absent, v_Leave, v_OnDuty, Rs_Report.Tables(0).Rows(i).Item("reason").ToString.Trim)
            mblnCheckReport = True
        Next
        Me.Cursor = Cursors.Default
        XtraReportGrid.ShowDialog()
    End Sub
    Sub SpotXl_AttendanceGrid(strsortorder As String)
        Dim whereClause As String
        If Common.ReportMail = True Then
            DateEdit1.DateTime = Now
            whereClause = Common.whereClauseEmail
        Else
            whereClause = g_WhereClause
        End If

         If Common.USERTYPE <> "A" Then
            Dim emp() As String = Common.Auth_Branch.Split(",")
            Dim ls As New List(Of String)()
            For x As Integer = 0 To emp.Length - 1
                ls.Add(emp(x).Trim)
            Next
            If whereClause = "" Then
                whereClause = " TBLEmployee.BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "')"
            Else
                whereClause = whereClause & " and TBLEmployee.BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "')"
            End If
        End If

        Dim strsql As String
        Dim v_Late As String '* 6
        Dim v_InTime As String '* 6
        Dim strStatus As String, mCount As Integer
        'Dim xlapp As Excel.Application
        'Dim xlwb As Excel.Workbook
        'Dim xlst As Excel.Sheets
        Dim rowcnt As Integer
        'xlapp = CreateObject("Excel.Application")
        'xlwb = xlapp.Workbooks.Add
        mstrDepartmentCode = " "
        mintPageNo = 1
        mintLine = 1
        If XtraShortOrder.g_SortOrder <> "" Then
            strsortorder = XtraShortOrder.g_SortOrder
        End If
        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If
        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".xls"

        mblnCheckReport = False
        If Common.servername = "Access" Then
            strsql = " Select tblTimeregister.PAYCODE, tblTimeregister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, tblDepartment.DepartmentCode, tblDepartment.DepartmentName,tblEmployee.PresentCARDNO, tblEmployee.EMPNAME, tblTimeregister.DATEOFFICE, " & _
               " tblTimeregister.SHIFTATTENDED, tblTimeregister.ShiftStartTime, tblTimeregister.In1, tblTimeregister.LateArrival, tblTimeregister.Status, tblCatagory.Catagoryname, tblDivision.DivisionName " & _
               " From  tblTimeregister, tblEmployee, tblDepartment, tblCatagory, tblDivision,tblCompany " & _
               " Where tblEmployee.PayCode = tblTimeregister.PayCode And " & _
               " tblEmployee.Companycode=tblCompany.Companycode and tblEmployee.CAT = tblCatagory.cat And " & _
               " FORMAT(tbltimeregister.DateOffice,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' And " & _
               " tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
               " " & IIf(Len(Trim(whereClause)) = 0, "", " AND " & whereClause) & strsortorder
            adapA = New OleDbDataAdapter(strsql, Common.con1)
            adapA.Fill(Rs_Report)
        Else
            strsql = " Select tblTimeregister.PAYCODE, tblTimeregister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, tblDepartment.DepartmentCode, tblDepartment.DepartmentName,tblEmployee.PresentCARDNO, tblEmployee.EMPNAME, tblTimeregister.DATEOFFICE, " & _
                     " tblTimeregister.SHIFTATTENDED, tblTimeregister.ShiftStartTime, tblTimeregister.In1, tblTimeregister.LateArrival, tblTimeregister.Status, tblCatagory.Catagoryname, tblDivision.DivisionName " & _
                     " From  tblTimeregister, tblEmployee, tblDepartment, tblCatagory, tblDivision,tblCompany " & _
                     " Where tblEmployee.PayCode = tblTimeregister.PayCode And " & _
                     " tblEmployee.Companycode=tblCompany.Companycode and tblEmployee.CAT = tblCatagory.cat And " & _
                     "  tbltimeregister.DateOffice = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' And " & _
                     " tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
                     " " & IIf(Len(Trim(whereClause)) = 0, "", " AND " & whereClause) & strsortorder
            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(Rs_Report)
        End If
        If Rs_Report.Tables(0).Rows.Count < 1 Then
            If Common.ReportMail = True Then
            Else
                XtraMessageBox.Show(ulf, "<size=10>No Data Available For this Report</size>", "<size=9>iAS</size>")
            End If
            Exit Sub
        End If
        Common.frodatetodatetoReportGrid = "ATTENDANCE REPORT FOR DATE : " & ForDate 'DateEdit1.DateTime.ToString("dd/MM/yyyy")
        Me.Cursor = Cursors.WaitCursor
        Common.tbl = New Data.DataTable()
        Common.tbl.Columns.Add("Srl", GetType(String))
        Common.tbl.Columns.Add("PayCode", GetType(String))
        Common.tbl.Columns.Add("Card No.", GetType(String))
        Common.tbl.Columns.Add("Employee Name", GetType(String))
        Common.tbl.Columns.Add(" ", GetType(String))
        Common.tbl.Columns.Add("Shift", GetType(String))
        Common.tbl.Columns.Add("Start", GetType(String))
        Common.tbl.Columns.Add("In", GetType(String))
        Common.tbl.Columns.Add("Shift Late", GetType(String))
        Common.tbl.Columns.Add("Status", GetType(String))

        Dim shiftStartTimeStr As String
        mCount = 0
        Dim DepartmentCode = Rs_Report.Tables(0).Rows(0).Item("DepartmentCode").ToString.Trim
        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            If strsortorder.Contains("Department") Then
                If i = 0 Then
                    'objWriter.WriteLine("Department : " & Rs_Report.Tables(0).Rows(i).Item("DepartmentName").ToString.Trim)
                    Common.tbl.Rows.Add("Department : ", Rs_Report.Tables(0).Rows(i).Item("DepartmentName").ToString.Trim)
                End If
                If DepartmentCode <> Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim Then
                    DepartmentCode = Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim
                    Common.tbl.Rows.Add()
                    Common.tbl.Rows.Add("Department : ", Rs_Report.Tables(0).Rows(i).Item("DepartmentName").ToString.Trim)
                End If
            End If
            'If strsortorder = "Department" Or XtraShortOrder.CheckDeptName.Checked Then
            '    If mstrDepartmentCode <> Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim Then
            '        rowcnt = rowcnt + 2
            '        'xlapp.Cells(rowcnt, 1) = "*** Department Code & Name " & Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim & " " & Rs_Report.Tables(0).Rows(i).Item("DepartmentName").ToString.Trim
            '        mstrDepartmentCode = Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString
            '        rowcnt = rowcnt + 1
            '    End If
            'End If
            'If strsortorder.Contains("Division") Or XtraShortOrder.CheckSectionPay.Checked Then
            '    If mstrDepartmentCode <> Rs_Report.Tables(0).Rows(i).Item("DivisionCode").ToString Then
            '        rowcnt = rowcnt + 2
            '        'xlapp.Cells(rowcnt, 1) = "*** Section Code & Name " & Rs_Report.Tables(0).Rows(i).Item("DivisionCode").ToString.Trim & " " & Rs_Report.Tables(0).Rows(i).Item("DivisionName").ToString.Trim
            '        mstrDepartmentCode = Rs_Report.Tables(0).Rows(i).Item("DivisionCode").ToString.Trim
            '        rowcnt = rowcnt + 1
            '    End If
            'End If
            'If strsortorder.Contains("Cat") Or XtraShortOrder.CheckCatagoryPaycode.Checked Then
            '    If mstrDepartmentCode <> Rs_Report.Tables(0).Rows(i).Item("Cat").ToString.Trim Then
            '        rowcnt = rowcnt + 2
            '        'xlapp.Cells(rowcnt, 1) = "*** Category Code & Name " & Rs_Report.Tables(0).Rows(i).Item("Cat").ToString.Trim & " " & Rs_Report.Tables(0).Rows(i).Item("CatagoryName").ToString.Trim
            '        mstrDepartmentCode = Rs_Report.Tables(0).Rows(i).Item("Cat").ToString.Trim
            '        rowcnt = rowcnt + 1
            '    End If
            'End If

            If Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim = "0" Or Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim = "" Then
                v_Late = ""
            Else
                v_Late = Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim Mod 60).ToString("00")
            End If
            If Rs_Report.Tables(0).Rows(i).Item("In1").ToString.Trim = "" Then
                v_InTime = " "
            Else
                v_InTime = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("In1").ToString.Trim).ToString("HH:mm")
            End If
            strStatus = ""
            If Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim <> "" Then
                If UCase(Trim(Rs_Report.Tables(0).Rows(i).Item("Status").ToString)) = "MIS" Then
                    strStatus = "P"
                Else
                    strStatus = Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim
                End If
            End If
            rowcnt = rowcnt + 1
            mCount = mCount + 1
            If Rs_Report.Tables(0).Rows(i).Item("shiftStartTime").ToString.Trim = "" Then
                shiftStartTimeStr = ""
            Else
                shiftStartTimeStr = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("shiftStartTime").ToString.Trim).ToString("HH:mm")
            End If
            mintLine = 1
            Common.tbl.Rows.Add(mCount, Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim, Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim,
                               Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim, "", Rs_Report.Tables(0).Rows(i).Item("ShiftAttended").ToString.Trim,
                              shiftStartTimeStr, v_InTime, v_Late, strStatus)
        Next
        Me.Cursor = Cursors.Default
        XtraReportGrid.ShowDialog()
    End Sub
    Sub SpotXl_DepartmentSummaryGrid()
        On Error Resume Next
        'Dim xlapp As Excel.Application
        'Dim xlwb As Excel.Workbook
        'Dim xlst As Excel.Sheets
        Dim intFile As Integer
        Dim strsql As String, mCount As Integer

        Dim strDepartmentName As String
        Dim strDepartmentCode As String

        Dim dblLeaveAmount As Double
        Dim dblPresentValue As Double
        Dim dblAbsentValue As Double
        Dim dblLeaveValue As Double
        Dim dblWo_Value As Double

        Dim dblPresentValue1 As Double
        Dim dblAbsentValue1 As Double
        Dim dblLeaveValue1 As Double
        Dim dblWo_Value1 As Double

        Dim lngOnDutyCount As Long
        Dim strOnDutyCount7 As String '* 7
        Dim lngTotalEmpCount As Long

        Dim rowcnt As Integer
        Dim GtotalEmp As Double
        Dim gTotalAbs As Double
        Dim gTotablLeave As Double
        Dim gTotalPre As Double
        Dim gTotalWO As Double
        Dim gTotalOD As Double
        'xlapp = CreateObject("Excel.Application")
        'xlwb = xlapp.Workbooks.Add
        mintPageNo = 1
        mintLine = 1
        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If

        Dim whereClause As String = g_WhereClause ' ""

         If Common.USERTYPE <> "A" Then
            Dim emp() As String = Common.Auth_Branch.Split(",")
            Dim ls As New List(Of String)()
            For x As Integer = 0 To emp.Length - 1
                ls.Add(emp(x).Trim)
            Next
            If whereClause = "" Then
                whereClause = " TBLEmployee.BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "')"
            Else
                whereClause = g_WhereClause & " and TBLEmployee.BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "')"
            End If
        End If

        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".xls"

        'strsql = " Select tblCatagory.Catagoryname, tblDivision.DivisionName, tblTimeregister.Wo_Value,tblTimeregister.PresentValue, tblTimeregister.Shift,tblTimeregister.AbsentValue,tblTimeregister.LeaveValue,tblTimeregister.LeaveAmount,TblDepartment.DepartmentName,TblDepartment.DepartmentCode,tblEmployee.PresentCardNo " & _
        '        " from  tblCatagory, tblDivision,  tblTimeregister ,tblEmployee ,tblDepartment,TBLCOMPANY " & _
        '        " Where tblEmployee.Companycode = tblCompany.Companycode AND tblEmployee.CAT = tblCatagory.cat And tblEmployee.DivisionCode = tblDivision.DivisionCode And tblTimeregister.PayCode = TblEmployee.PayCode And TblEmployee.DepartmentCode = TblDepartment.DepartmentCode" & g_HODDepartmentCode & _
        '        " AND tbltimeregister.DateOffice = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' And  (tblemployee.LeavingDate>='" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' or tblemployee.LeavingDate is null)" & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & " ORDER by tblDepartment.DEPARTMENTCODE ASC"
        'If Common.servername = "Access" Then

        '    strsql = " Select tblCatagory.Catagoryname, tblDivision.DivisionName, tblTimeregister.Wo_Value,tblTimeregister.PresentValue, tblTimeregister.Shift,tblTimeregister.AbsentValue,tblTimeregister.LeaveValue,tblTimeregister.LeaveAmount,TblDepartment.DepartmentName,TblDepartment.DepartmentCode,tblEmployee.PresentCardNo " & _
        '            " from  tblCatagory, tblDivision,  tblTimeregister ,tblEmployee ,tblDepartment,TBLCOMPANY " & _
        '            " Where tblEmployee.Companycode = tblCompany.Companycode AND tblEmployee.CAT = tblCatagory.cat And tblTimeregister.PayCode = TblEmployee.PayCode And TblEmployee.DepartmentCode = TblDepartment.DepartmentCode" & g_HODDepartmentCode & _
        '            " AND FORMAT(tbltimeregister.DateOffice,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & " ORDER by tblDepartment.DEPARTMENTCODE ASC"
        '    adapA = New OleDbDataAdapter(strsql, Common.con1)
        '    adapA.Fill(Rs_Report)
        'Else
        '    adap = New SqlDataAdapter(strsql, Common.con)
        '    adap.Fill(Rs_Report)
        'End If

        If Common.servername = "Access" Then
            strsql = " Select tblCatagory.Catagoryname, tblDivision.DivisionName, tblTimeregister.Wo_Value,tblTimeregister.PresentValue, tblTimeregister.Shift,tblTimeregister.AbsentValue,tblTimeregister.LeaveValue,tblTimeregister.LeaveAmount,TblDepartment.DepartmentName,TblDepartment.DepartmentCode,tblEmployee.PresentCardNo " & _
                " from  tblCatagory, tblDivision,  tblTimeregister ,tblEmployee ,tblDepartment,TBLCOMPANY " & _
                " Where tblEmployee.Companycode = tblCompany.Companycode AND tblEmployee.CAT = tblCatagory.cat And tblTimeregister.PayCode = TblEmployee.PayCode And TblEmployee.DepartmentCode = TblDepartment.DepartmentCode" & g_HODDepartmentCode & _
                " AND FORMAT(tbltimeregister.DateOffice,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' " & IIf(Len(Trim(whereClause)) = 0, "", " AND " & whereClause) & " order by tblDepartment.DEPARTMENTCODE"
            adapA = New OleDbDataAdapter(strsql, Common.con1)
            adapA.Fill(Rs_Report)
        Else
            strsql = " Select tblCatagory.Catagoryname, tblDivision.DivisionName, tblTimeregister.Wo_Value,tblTimeregister.PresentValue, tblTimeregister.Shift,tblTimeregister.AbsentValue,tblTimeregister.LeaveValue,tblTimeregister.LeaveAmount,TblDepartment.DepartmentName,TblDepartment.DepartmentCode,tblEmployee.PresentCardNo " & _
                 " from  tblCatagory, tblDivision,  tblTimeregister ,tblEmployee ,tblDepartment,TBLCOMPANY " & _
                 " Where tblEmployee.Companycode = tblCompany.Companycode AND tblEmployee.CAT = tblCatagory.cat And tblTimeregister.PayCode = TblEmployee.PayCode And TblEmployee.DepartmentCode = TblDepartment.DepartmentCode" & g_HODDepartmentCode & _
                 " AND tbltimeregister.DateOffice = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' " & IIf(Len(Trim(whereClause)) = 0, "", " AND " & whereClause) & " order by tblDepartment.DEPARTMENTCODE"
            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(Rs_Report)
        End If
        If Rs_Report.Tables(0).Rows.Count < 1 Then
            XtraMessageBox.Show(ulf, "<size=10>No Data Available For this Report.</size>", "<size=9>iAS</size>")
            Exit Sub
        End If
        'Rs_Report.Sort = "DepartmentCode"
        rowcnt = 1

        Common.frodatetodatetoReportGrid = " DEPARTMENT SUMMARY ON DATE : " & ForDate 'DateEdit1.DateTime.ToString("dd/MM/yyyy HH:mm")
        rowcnt = rowcnt + 1
        Me.Cursor = Cursors.WaitCursor
        Common.tbl = New Data.DataTable()
        Common.tbl.Columns.Add("Srl", GetType(String))
        Common.tbl.Columns.Add("Dept Code", GetType(String))
        Common.tbl.Columns.Add("Dept Name", GetType(String))
        Common.tbl.Columns.Add("Total Employee", GetType(String))
        Common.tbl.Columns.Add(" ", GetType(String))
        Common.tbl.Columns.Add("Present", GetType(String))
        Common.tbl.Columns.Add("On Duty", GetType(String))
        Common.tbl.Columns.Add("Absent", GetType(String))
        Common.tbl.Columns.Add("Leave", GetType(String))
        Common.tbl.Columns.Add("Weekly Off", GetType(String))

        'rowcnt = rowcnt + 1
        mintLine = 10
        mCount = 0
        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            dblPresentValue = 0
            dblAbsentValue = 0
            dblLeaveValue = 0
            dblWo_Value = 0
            lngTotalEmpCount = 0
            lngOnDutyCount = 0

            strDepartmentName = Rs_Report.Tables(0).Rows(i).Item("DepartmentName").ToString.Trim
            strDepartmentCode = Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim
            'For j As Integer = 0 To strDepartmentCode = Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim
            Do While strDepartmentCode = Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim
                dblPresentValue1 = 0
                dblAbsentValue1 = 0
                dblLeaveValue1 = 0
                dblWo_Value1 = 0
                dblLeaveAmount = 0

                If Rs_Report.Tables(0).Rows(i).Item("PRESENTVALUE").ToString.Trim <> "" Then dblPresentValue1 = Rs_Report.Tables(0).Rows(i).Item("PRESENTVALUE").ToString.Trim
                If Rs_Report.Tables(0).Rows(i).Item("ABSENTVALUE").ToString.Trim <> "" Then dblAbsentValue1 = Rs_Report.Tables(0).Rows(i).Item("ABSENTVALUE").ToString.Trim
                If Rs_Report.Tables(0).Rows(i).Item("LEAVEVALUE").ToString.Trim <> "" Then dblLeaveValue1 = Rs_Report.Tables(0).Rows(i).Item("LEAVEVALUE").ToString.Trim
                If Rs_Report.Tables(0).Rows(i).Item("WO_VALUE").ToString.Trim <> "" Then dblWo_Value1 = Rs_Report.Tables(0).Rows(i).Item("WO_VALUE").ToString.Trim
                If Rs_Report.Tables(0).Rows(i).Item("leaveamount").ToString.Trim Then dblLeaveAmount = Rs_Report.Tables(0).Rows(i).Item("leaveamount").ToString.Trim

                If dblPresentValue1 = 1 And dblLeaveAmount > 0 Then lngOnDutyCount = lngOnDutyCount + 1
                If Not (dblPresentValue1 = 1 And dblLeaveAmount > 0) Then
                    dblPresentValue = dblPresentValue + dblPresentValue1
                End If
                dblAbsentValue = dblAbsentValue + dblAbsentValue1
                dblLeaveValue = dblLeaveValue + dblLeaveValue1
                dblWo_Value = dblWo_Value + dblWo_Value1

                lngTotalEmpCount = lngTotalEmpCount + 1
                i = i + 1
                If i > Rs_Report.Tables(0).Rows.Count - 1 Then
                    i = i - 1
                    GoTo tmp
                    Exit Do
                End If
            Loop
            'Next
            i = i - 1
tmp:        strDepartmentName = Rs_Report.Tables(0).Rows(i).Item("DepartmentName").ToString.Trim
            strDepartmentCode = Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim

            GtotalEmp = GtotalEmp + lngTotalEmpCount
            gTotalAbs = gTotalAbs + dblAbsentValue
            gTotablLeave = gTotablLeave + dblLeaveValue
            gTotalPre = gTotalPre + dblPresentValue
            gTotalWO = gTotalWO + dblWo_Value
            gTotalOD = gTotalOD + lngOnDutyCount
            rowcnt = rowcnt + 1
            mCount = mCount + 1
            Common.tbl.Rows.Add(mCount, strDepartmentCode, strDepartmentName, lngTotalEmpCount, "", dblPresentValue, lngOnDutyCount, dblAbsentValue, dblLeaveValue, dblWo_Value)
            mintLine = mintLine + 1
        Next
        rowcnt = rowcnt + 1
        Common.tbl.Rows.Add("", "", "Total :", GtotalEmp, "", gTotalPre, gTotalOD, gTotalAbs, gTotablLeave, gTotalWO)
        Me.Cursor = Cursors.Default
        XtraReportGrid.ShowDialog()
    End Sub
    Sub SpotXl_EarlyArrivalGrid(strsortorder As String)
        'Dim xlapp As Excel.Application
        'Dim xlwb As Excel.Workbook
        'Dim xlst As Excel.Sheets
        Dim strsql As String, mCount As Integer
        Dim v_Late As String '* 6
        Dim rowcnt As Integer
        'xlapp = CreateObject("Excel.Application")
        'xlwb = xlapp.Workbooks.Add

        mstrDepartmentCode = " "
        mintPageNo = 1
        mintLine = 1
        If XtraShortOrder.g_SortOrder <> "" Then
            strsortorder = XtraShortOrder.g_SortOrder
        End If
        mblnCheckReport = False
        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If
        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".xls"


        If Common.servername = "Access" Then
            strsql = " Select tblCatagory.Catagoryname, tblDivision.DivisionName, tblTimeregister.PAYCODE,  tblTimeregister.Shift,tblDepartment.DepartmentCode, tblDepartment.DepartmentName,tblEmployee.PresentCARDNO, tblEmployee.EMPNAME, tblTimeregister.DATEOFFICE, " &
           "tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, " &
           "tblTimeregister.SHIFTATTENDED, tblTimeregister.shiftStartTime, tblTimeregister.In1, tblTimeregister.Earlyarrival, " &
           "IIF(tblTimeregister.Earlyarrival <= 10,'**','  ') AS Late1, " &
           "IIF(tblTimeregister.Earlyarrival > 10 And tblTimeregister.Earlyarrival <= 30, '**' , '  ') AS Late10, " &
           "IIF(tblTimeregister.Earlyarrival > 30 And tblTimeregister.Earlyarrival <= 60,'**' ,'  ') AS Late30, " &
           "IIF(TblTimeregister.Earlyarrival > 60 , '**', '  ') AS Late60, tblEmployee.BUS " &
           "From  tblCatagory, tblDivision,tblTimeregister, tblEmployee, tblDepartment, tblCompany " &
           "Where tblEmployee.CompanyCode=tblCompany.CompanyCode and tblEmployee.CAT = tblCatagory.cat And tblEmployee.PayCode = tblTimeregister.PayCode And " &
           "tblEmployee.DepartmentCode = tblDepartment.DepartmentCode AND FORMAT(tbltimeregister.DateOffice,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' AND  " &
           "tblTimeregister.Earlyarrival > 0" & g_HODDepartmentCode &
           " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & strsortorder
            If Common.IsCompliance Then
                strsql = Microsoft.VisualBasic.Strings.Replace(strsql, "tbltimeregister", "tblTimeRegisterD", 1, -1, Microsoft.VisualBasic.Constants.vbTextCompare)
            End If
            adapA = New OleDbDataAdapter(strsql, Common.con1)
            adapA.Fill(Rs_Report)
        Else
            strsql = " Select tblCatagory.Catagoryname, tblDivision.DivisionName, tblTimeregister.PAYCODE,  tblTimeregister.Shift,tblDepartment.DepartmentCode, tblDepartment.DepartmentName,tblEmployee.PresentCARDNO, tblEmployee.EMPNAME, tblTimeregister.DATEOFFICE, " &
                    "tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, " &
                    "tblTimeregister.SHIFTATTENDED, tblTimeregister.shiftStartTime, tblTimeregister.In1, tblTimeregister.Earlyarrival, " &
                    "Late1 = Case When tblTimeregister.Earlyarrival <= 10 then '**' Else '  'End, " &
                    "Late10 = Case When tblTimeregister.Earlyarrival > 10 And tblTimeregister.Earlyarrival <= 30 then '**' Else '  ' End, " &
                    "Late30 = Case When tblTimeregister.Earlyarrival > 30 And tblTimeregister.Earlyarrival <= 60 then '**' Else '  ' End, " &
                    "Late60 = Case When tblTimeregister.Earlyarrival > 60 then '**' Else '  ' End, tblEmployee.BUS " &
                    "From  tblCatagory, tblDivision,tblTimeregister, tblEmployee, tblDepartment, tblCompany " &
                    "Where tblEmployee.CompanyCode=tblCompany.CompanyCode and tblEmployee.CAT = tblCatagory.cat And tblEmployee.PayCode = tblTimeregister.PayCode And " &
                    "tblEmployee.DepartmentCode = tblDepartment.DepartmentCode AND tbltimeregister.DateOffice = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' AND  " &
                    "tblTimeregister.Earlyarrival > 0" & g_HODDepartmentCode &
                    " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & strsortorder
            If Common.IsCompliance Then
                strsql = Microsoft.VisualBasic.Strings.Replace(strsql, "tbltimeregister", "tblTimeRegisterD", 1, -1, Microsoft.VisualBasic.Constants.vbTextCompare)
            End If
            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(Rs_Report)
        End If

        If Rs_Report.Tables(0).Rows.Count < 1 Then
            XtraMessageBox.Show(ulf, "<size=10>Nobody has come early on this date.</size>", "<size=9>iAS</size>")
            Exit Sub
        End If

        rowcnt = 1

        Common.frodatetodatetoReportGrid = "EARLY ARRIVAL REPORT FOR DATE : " & ForDate 'DateEdit1.DateTime.ToString("dd/MM/yyyy HH:mm")
        rowcnt = rowcnt + 1

        Me.Cursor = Cursors.WaitCursor
        Common.tbl = New Data.DataTable()
        Common.tbl.Columns.Add("Srl", GetType(String))
        Common.tbl.Columns.Add("PayCode", GetType(String))
        Common.tbl.Columns.Add("Card No.", GetType(String))
        Common.tbl.Columns.Add("Employee Name", GetType(String))
        Common.tbl.Columns.Add(" ", GetType(String))
        Common.tbl.Columns.Add("Shift", GetType(String))
        Common.tbl.Columns.Add("Shift Start", GetType(String))
        Common.tbl.Columns.Add("In", GetType(String))
        Common.tbl.Columns.Add("Shift Early", GetType(String))
        Common.tbl.Columns.Add("---------", GetType(String))
        Common.tbl.Columns.Add("Shift Early ", GetType(String))
        Common.tbl.Columns.Add("----------", GetType(String))
        Common.tbl.Columns.Add("Bus Route", GetType(String))


        Common.tbl.Rows.Add("", "", "", "", "", "", "", "", "", ">(0.10)", ">(0.30)", ">(1.00)")
        mCount = 0

        Dim shiftStartTimeStr As String
        Dim In1Str As String
        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            If strsortorder = "Department" Or XtraShortOrder.CheckDeptName.Checked Then
                If mstrDepartmentCode <> Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim Then
                    rowcnt = rowcnt + 2
                    'xlapp.Cells(rowcnt, 1) = "*** Department Code & Name " & Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim & " " & Rs_Report.Tables(0).Rows(i).Item("DepartmentName").ToString.Trim
                    mstrDepartmentCode = Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString
                    rowcnt = rowcnt + 1
                End If
            End If
            If strsortorder.Contains("Division") Or XtraShortOrder.CheckSectionPay.Checked Then
                If mstrDepartmentCode <> Rs_Report.Tables(0).Rows(i).Item("DivisionCode").ToString Then
                    rowcnt = rowcnt + 2
                    'xlapp.Cells(rowcnt, 1) = "*** Section Code & Name " & Rs_Report.Tables(0).Rows(i).Item("DivisionCode").ToString.Trim & " " & Rs_Report.Tables(0).Rows(i).Item("DivisionName").ToString.Trim
                    mstrDepartmentCode = Rs_Report.Tables(0).Rows(i).Item("DivisionCode").ToString.Trim
                    rowcnt = rowcnt + 1
                End If
            End If
            If strsortorder.Contains("Cat") Or XtraShortOrder.CheckCatagoryPaycode.Checked Then
                If mstrDepartmentCode <> Rs_Report.Tables(0).Rows(i).Item("Cat").ToString.Trim Then
                    rowcnt = rowcnt + 2
                    'xlapp.Cells(rowcnt, 1) = "*** Category Code & Name " & Rs_Report.Tables(0).Rows(i).Item("Cat").ToString.Trim & " " & Rs_Report.Tables(0).Rows(i).Item("CatagoryName").ToString.Trim
                    mstrDepartmentCode = Rs_Report.Tables(0).Rows(i).Item("Cat").ToString.Trim
                    rowcnt = rowcnt + 1
                End If
            End If

            If Rs_Report.Tables(0).Rows(i).Item("EARLYARRIVAL").ToString.Trim = "0" Then
                v_Late = ""
            Else
                v_Late = Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("EARLYARRIVAL").ToString.Trim / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("EARLYARRIVAL").ToString.Trim Mod 60).ToString("00")
            End If

            mCount = mCount + 1
            rowcnt = rowcnt + 1

            'xlapp.Cells(rowcnt, 1) = mCount
            'xlapp.Cells(rowcnt, 2) = "'" & Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim
            'xlapp.Cells(rowcnt, 3) = "'" & Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim
            'xlapp.Cells(rowcnt, 4) = Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim
            ' xlapp.Cells(rowcnt, 6) = Rs_Report.Tables(0).Rows(i).Item("ShiftAttended").ToString.Trim
            If Rs_Report.Tables(0).Rows(i).Item("shiftStartTime").ToString.Trim = "" Then
                shiftStartTimeStr = ""
            Else
                shiftStartTimeStr = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("shiftStartTime").ToString.Trim).ToString("HH:mm")
            End If
            If Rs_Report.Tables(0).Rows(i).Item("In1").ToString.Trim = "" Then
                In1Str = ""
            Else
                In1Str = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("In1").ToString.Trim).ToString("HH:mm")
            End If
            'xlapp.Cells(rowcnt, 9) = "'" & (v_Late)
            'xlapp.Cells(rowcnt, 14) = Rs_Report.Tables(0).Rows(i).Item("bus").ToString.Trim

            Common.tbl.Rows.Add(mCount, Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim, Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim, _
                                Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim, "", Rs_Report.Tables(0).Rows(i).Item("ShiftAttended").ToString.Trim, shiftStartTimeStr, _
                                In1Str, v_Late, "", "", "", Rs_Report.Tables(0).Rows(i).Item("bus").ToString.Trim)
            mintLine = mintLine + 1
            mblnCheckReport = True
        Next
        Me.Cursor = Cursors.Default
        XtraReportGrid.ShowDialog()
    End Sub
    Sub SpotXl_ShiftWisePresenceGrid(strsortorder As String)
        'Dim xlapp As Excel.Application
        'Dim xlwb As Excel.Workbook
        'Dim xlst As Excel.Sheets
        Dim strsql As String
        Dim v_Shift As String '* 5
        Dim v_Start As String '* 5
        Dim v_InTime As String '* 8
        Dim v_Late As String '* 5
        Dim v_Status As String '* 5
        Dim rowcnt As Integer
        Dim mCount As Integer
        'xlapp = CreateObject("Excel.Application")
        'xlwb = xlapp.Workbooks.Add
        If XtraShortOrder.g_SortOrder <> "" Then
            strsortorder = XtraShortOrder.g_SortOrder
        End If
        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If
        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".xls"

        mstrDepartmentCode = " "
        mintPageNo = 1
        mintLine = 1
        mblnCheckReport = False
        If Common.servername = "Access" Then
            strsql = " Select tblTimeregister.PAYCODE, tblTimeregister.Shiftattended, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, tblDepartment.DepartmentCode, tblDepartment.DepartmentName,tblEmployee.PresentCARDNO, tblEmployee.EMPNAME, tblTimeregister.DATEOFFICE, " &
           " tblTimeregister.SHIFTATTENDED, tblTimeregister.ShiftStartTime, tblTimeregister.In1, tblTimeregister.LateArrival, tblTimeregister.Status, tblCatagory.Catagoryname, tblDivision.DivisionName " &
           " From  tblTimeregister, tblEmployee, tblDepartment, tblCatagory, tblDivision,tblCompany " &
           " Where tblEmployee.PayCode = tblTimeregister.PayCode And " &
           " tblEmployee.Companycode=tblCompany.Companycode and tblEmployee.CAT = tblCatagory.cat And " &
           "  FORMAT(tbltimeregister.DateOffice,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' And " &
           " tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode &
           " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) &
           " order by tblTimeregister.Shiftattended,tblEmployee.DepartmentCode,tblTimeregister.PayCode"
            If Common.IsCompliance Then
                strsql = Microsoft.VisualBasic.Strings.Replace(strsql, "tbltimeregister", "tblTimeRegisterD", 1, -1, Microsoft.VisualBasic.Constants.vbTextCompare)
            End If
            adapA = New OleDbDataAdapter(strsql, Common.con1)
            adapA.Fill(Rs_Report)
        Else
            strsql = " Select tblTimeregister.PAYCODE, tblTimeregister.Shiftattended, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, tblDepartment.DepartmentCode, tblDepartment.DepartmentName,tblEmployee.PresentCARDNO, tblEmployee.EMPNAME, tblTimeregister.DATEOFFICE, " &
                     " tblTimeregister.SHIFTATTENDED, tblTimeregister.ShiftStartTime, tblTimeregister.In1, tblTimeregister.LateArrival, tblTimeregister.Status, tblCatagory.Catagoryname, tblDivision.DivisionName " &
                     " From  tblTimeregister, tblEmployee, tblDepartment, tblCatagory, tblDivision,tblCompany " &
                     " Where tblEmployee.PayCode = tblTimeregister.PayCode And " &
                     " tblEmployee.Companycode=tblCompany.Companycode and tblEmployee.CAT = tblCatagory.cat And " &
                     " tbltimeregister.DateOffice = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' And " &
                     " tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode &
                     " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) &
                     " order by tblTimeregister.Shiftattended,tblEmployee.DepartmentCode,tblTimeregister.PayCode"
            If Common.IsCompliance Then
                strsql = Microsoft.VisualBasic.Strings.Replace(strsql, "tbltimeregister", "tblTimeRegisterD", 1, -1, Microsoft.VisualBasic.Constants.vbTextCompare)
            End If
            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(Rs_Report)
        End If
        'Rs_Report = Cn.Execute(strsql)


        If Rs_Report.Tables(0).Rows.Count < 1 Then
            XtraMessageBox.Show(ulf, "<size=10>No Data Available For this Report.</size>", "<size=9>iAS</size>")
            Exit Sub
        End If
        rowcnt = 1

        Common.frodatetodatetoReportGrid = " SHIFT WISE PRESENCE REPORT FOR DATE : " & ForDate 'DateEdit1.DateTime.ToString("dd/MM/yyyy")
        Me.Cursor = Cursors.WaitCursor
        Common.tbl = New Data.DataTable()
        Common.tbl.Columns.Add("Srl", GetType(String))
        Common.tbl.Columns.Add("PayCode", GetType(String))
        Common.tbl.Columns.Add("Card No.", GetType(String))
        Common.tbl.Columns.Add("Employee Name", GetType(String))
        Common.tbl.Columns.Add(" ", GetType(String))
        Common.tbl.Columns.Add("Shift", GetType(String))
        Common.tbl.Columns.Add("Shift Start", GetType(String))
        Common.tbl.Columns.Add("In", GetType(String))
        Common.tbl.Columns.Add("Late", GetType(String))
        Common.tbl.Columns.Add("Status", GetType(String))

        mCount = 0
        mShift = Rs_Report.Tables(0).Rows(0).Item("ShiftAttended").ToString.Trim
        mstrDepartmentCode = Rs_Report.Tables(0).Rows(0).Item("DepartmentCode").ToString.Trim
        Common.tbl.Rows.Add(" Shift : " & mShift)
        Common.tbl.Rows.Add("Department Code & Name ", Rs_Report.Tables(0).Rows(0).Item("DepartmentCode").ToString.Trim & " " & Rs_Report.Tables(0).Rows(0).Item("DepartmentName").ToString.Trim)
        mstrDepartmentCode = Rs_Report.Tables(0).Rows(0).Item("DepartmentCode").ToString.Trim
        mShift = Rs_Report.Tables(0).Rows(0).Item("ShiftAttended").ToString.Trim
        rowcnt = rowcnt + 1
        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            If StrComp(strsortorder, "DepartmentCode", 1) <> 0 Then
                If StrComp(mShift, Rs_Report.Tables(0).Rows(i).Item("ShiftAttended").ToString.Trim, 1) = 0 Then
                    If mstrDepartmentCode = Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim Then
                        If Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim = "0" Or Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim = "" Then
                            v_Late = ""
                        Else
                            v_Late = Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim Mod 60).ToString("00")
                        End If
                        If Rs_Report.Tables(0).Rows(i).Item("In1").ToString.Trim = "" Then
                            v_InTime = ""
                        Else
                            v_InTime = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("In1").ToString.Trim).ToString("HH:mm")
                        End If
                        v_Status = ""
                        If Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim <> "" Then
                            If UCase(Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim) = "MIS" Then
                                v_Status = "P"
                            Else
                                v_Status = Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim
                            End If
                        End If
                        v_Shift = Rs_Report.Tables(0).Rows(i).Item("ShiftAttended").ToString.Trim & " "
                        If Rs_Report.Tables(0).Rows(i).Item("shiftStartTime").ToString.Trim = "" Then
                            v_Start = ""
                        Else
                            v_Start = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("shiftStartTime").ToString.Trim).ToString("HH:mm")
                        End If
                        rowcnt = rowcnt + 1
                        mCount = mCount + 1
                        ' xlapp.Cells(rowcnt, 2) = "'" & mCount
                        '  xlapp.Cells(rowcnt, 3) = "'" & Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim
                        '  xlapp.Cells(rowcnt, 4) = Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim
                        ' xlapp.Cells(rowcnt, 5) = Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim
                        ' xlapp.Cells(rowcnt, 7) = v_Shift
                        ' xlapp.Cells(rowcnt, 8) = v_Start
                        ' xlapp.Cells(rowcnt, 9) = v_InTime
                        ' xlapp.Cells(rowcnt, 10) = "'" & (v_Late)
                        'xlapp.Cells(rowcnt, 11) = v_Status
                        mblnCheckReport = True
                        mShift = Rs_Report.Tables(0).Rows(i).Item("ShiftAttended").ToString.Trim
                        Common.tbl.Rows.Add(mCount, Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim, Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim, _
                                           Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim, "", v_Shift, v_Start, v_InTime, v_Late, v_Status)
                    Else
                        rowcnt = rowcnt + 1
                        'xlapp.Cells(rowcnt, 1) = "Department Code & Name " & Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim & " " & Rs_Report.Tables(0).Rows(i).Item("DepartmentName").ToString.Trim
                        Common.tbl.Rows.Add("Department Code & Name ", Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim & " " & Rs_Report.Tables(0).Rows(i).Item("DepartmentName").ToString.Trim)
                        mstrDepartmentCode = Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim
                        If Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim = "0" Or Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim = "" Then
                            v_Late = ""
                        Else
                            v_Late = Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim Mod 60).ToString("00")
                        End If
                        If Rs_Report.Tables(0).Rows(i).Item("In1").ToString.Trim = "" Then
                            v_InTime = ""
                        Else
                            v_InTime = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("In1").ToString.Trim).ToString("HH:mm")
                        End If
                        v_Status = ""
                        If Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim <> "" Then
                            If UCase(Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim) = "MIS" Then
                                v_Status = "P"
                            Else
                                v_Status = Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim
                            End If
                        End If
                        v_Shift = Rs_Report.Tables(0).Rows(i).Item("ShiftAttended").ToString.Trim & " "
                        If Rs_Report.Tables(0).Rows(i).Item("shiftStartTime").ToString.Trim = "" Then
                            v_Start = ""
                        Else
                            v_Start = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("shiftStartTime").ToString.Trim).ToString("HH:mm")
                        End If
                        rowcnt = rowcnt + 1
                        mCount = mCount + 1
                        'xlapp.Cells(rowcnt, 2) = "'" & mCount
                        'xlapp.Cells(rowcnt, 3) = "'" & Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim
                        'xlapp.Cells(rowcnt, 4) = Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim
                        'xlapp.Cells(rowcnt, 5) = Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim
                        'xlapp.Cells(rowcnt, 7) = v_Shift
                        'xlapp.Cells(rowcnt, 8) = v_Start
                        'xlapp.Cells(rowcnt, 9) = v_InTime
                        'xlapp.Cells(rowcnt, 10) = "'" & (v_Late)
                        'xlapp.Cells(rowcnt, 11) = v_Status
                        mblnCheckReport = True
                        mShift = Rs_Report.Tables(0).Rows(i).Item("ShiftAttended").ToString.Trim
                        Common.tbl.Rows.Add(mCount, Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim, Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim, _
                                          Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim, "", v_Shift, v_Start, v_InTime, v_Late, v_Status)
                    End If
                Else
                    rowcnt = rowcnt + 1
                    'xlapp.Cells(rowcnt, 1) = "Shift : " & mShift
                    Common.tbl.Rows.Add("Shift : ", mShift)
                    'xlapp.Cells(rowcnt, 1) = "Department Code & Name " & Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim & " " & Rs_Report.Tables(0).Rows(i).Item("DepartmentName").ToString.Trim
                    Common.tbl.Rows.Add("Department Code & Name ", Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim & " " & Rs_Report.Tables(0).Rows(i).Item("DepartmentName").ToString.Trim)

                    mstrDepartmentCode = Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim
                    mShift = Rs_Report.Tables(0).Rows(i).Item("ShiftAttended").ToString.Trim
                    With Rs_Report
                        If Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim = "0" Or Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim = "" Then
                            v_Late = ""
                        Else
                            v_Late = Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim Mod 60).ToString("00")
                        End If
                        If Rs_Report.Tables(0).Rows(i).Item("In1").ToString.Trim = "" Then
                            v_InTime = ""
                        Else
                            v_InTime = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("In1").ToString.Trim).ToString("HH:mm")
                        End If
                        v_Status = ""
                        If Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim <> "" Then
                            If UCase(Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim) = "MIS" Then
                                v_Status = "P"
                            Else
                                v_Status = Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim
                            End If
                        End If
                        v_Shift = Rs_Report.Tables(0).Rows(i).Item("ShiftAttended").ToString.Trim & " "
                        If Rs_Report.Tables(0).Rows(i).Item("shiftStartTime").ToString.Trim = "" Then
                            v_Start = ""
                        Else
                            v_Start = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("shiftStartTime").ToString.Trim).ToString("HH:mm")
                        End If

                        rowcnt = rowcnt + 1
                        mCount = mCount + 1
                        'xlapp.Cells(rowcnt, 2) = "'" & mCount
                        'xlapp.Cells(rowcnt, 3) = "'" & Rs_Report.Tables(0).Rows(i).Item("Paycode").ToString.Trim
                        'xlapp.Cells(rowcnt, 4) = Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim
                        'xlapp.Cells(rowcnt, 5) = Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim
                        'xlapp.Cells(rowcnt, 7) = v_Shift
                        'xlapp.Cells(rowcnt, 8) = v_Start
                        'xlapp.Cells(rowcnt, 9) = v_InTime
                        'xlapp.Cells(rowcnt, 10) = "'" & (v_Late)
                        'xlapp.Cells(rowcnt, 11) = v_Status
                        mblnCheckReport = True
                        mShift = Rs_Report.Tables(0).Rows(i).Item("ShiftAttended").ToString.Trim
                        Common.tbl.Rows.Add(mCount, Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim, Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim, _
                                          Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim, "", v_Shift, v_Start, v_InTime, v_Late, v_Status)

                    End With
                End If
            End If
            mintLine = mintLine + 1
        Next
        Me.Cursor = Cursors.Default
        XtraReportGrid.ShowDialog()
    End Sub
    Sub SpotXl_MachineRawPunchDataGrid(strsortorder As String)
        Dim strsql As String, mCount As Integer
        Dim mPunch1 As String ' * 9
        Dim mPunch2 As String '* 9
        Dim mPunch3 As String '* 9
        Dim mPunch4 As String '* 9
        Dim mCtr As Integer
        Dim mDate As Date
        'Dim xlapp As Excel.Application
        'Dim xlwb As Excel.Workbook
        'Dim xlst As Excel.Sheets
        Dim rowcnt As Integer
        'xlapp = CreateObject("Excel.Application")
        'xlwb = xlapp.Workbooks.Add
        Dim colcnt As Integer
        Dim mPaycode As String
        Dim mEmpName As String
        Dim mCARDNO As String
        Dim mColCnt As Integer
        mstrDepartmentCode = " "
        mblnCheckReport = False
        mintPageNo = 1
        mintLine = 1
        If XtraShortOrder.g_SortOrder <> "" Then
            strsortorder = XtraShortOrder.g_SortOrder
        End If
        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If
        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".xls"
        If Common.servername = "Access" Then
            strsql = " Select tblCatagory.Catagoryname, machinerawpunchAll.officepunch,MachinerawpunchAll.officepunch as mdate, machinerawpunchAll.inout, tblDivision.DivisionName, tblEmployee.PresentCardNo, " & _
                      " tblemployee.PAYCODE,tblemployee.empname, MachinerawpunchAll.CARDNO, machinerawpunchAll.ismanual," & _
                      " tblDepartment.DepartmentCode, tblDepartment.DepartmentName, tblEmployee.Divisioncode, " & _
                      " tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, tblEmployee.DepartmentCode " & _
                      "  From tblCatagory, tblDivision, MachineRawPunchAll, tblDepartment, tblEmployee,TBLCOMPANY " & _
                      " Where OfficePunch between #" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & " 00:00# And " & _
                      " #" & DateEdit2.DateTime.ToString("yyyy-MM-dd") & " 23:59# and " & _
                      " TBLEMPLOYEE.COMPANYCODE=TBLCOMPANY.COMPANYCODE AND tblEmployee.CAT = tblCatagory.cat And tblemployee.DepartmentCode = tblDepartment.DepartmentCode AND  " & _
                      " MachineRawPunchAll.Paycode = tblEmployee.Paycode " & _
                      " " & g_HODDepartmentCode & _
                      " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & "order by tblEmployee.PresentCARDNO,  MachineRawPunchAll.OFFICEPUNCH"
            adapA = New OleDbDataAdapter(strsql, Common.con1)
            adapA.Fill(Rs_Report)
        Else
            strsql = " Select tblCatagory.Catagoryname, machinerawpunchAll.officepunch,MachinerawpunchAll.officepunch as mdate, machinerawpunchAll.inout, tblDivision.DivisionName, tblEmployee.PresentCardNo, " & _
                          " tblemployee.PAYCODE,tblemployee.empname, MachinerawpunchAll.CARDNO, machinerawpunchAll.ismanual," & _
                          " tblDepartment.DepartmentCode, tblDepartment.DepartmentName, tblEmployee.Divisioncode, " & _
                          " tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, tblEmployee.DepartmentCode " & _
                          "  From tblCatagory, tblDivision, MachineRawPunchAll, tblDepartment, tblEmployee,TBLCOMPANY " & _
                          " Where OfficePunch Between '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & " 00:00' And " & _
                          " '" & DateEdit2.DateTime.ToString("yyyy-MM-dd") & " 23:59' and " & _
                          " TBLEMPLOYEE.COMPANYCODE=TBLCOMPANY.COMPANYCODE AND tblEmployee.CAT = tblCatagory.cat And tblemployee.DepartmentCode = tblDepartment.DepartmentCode AND  " & _
                          " MachineRawPunchAll.Paycode = tblEmployee.Paycode " & _
                          " " & g_HODDepartmentCode & _
                          " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & "order by tblEmployee.PresentCARDNO,  MachineRawPunchAll.OFFICEPUNCH"
            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(Rs_Report)
        End If
        If Rs_Report.Tables(0).Rows.Count < 1 Then
            XtraMessageBox.Show(ulf, "<size=10>No Data Present For this Report.</size>", "<size=9>iAS</size>")
            Exit Sub
        End If
        rowcnt = 1

        Common.frodatetodatetoReportGrid = "MACHINE  RAW  PUNCH  FOR  DATE : " & ForDate 'DateEdit1.DateTime.ToString("dd/MM/yyyy")

        Me.Cursor = Cursors.WaitCursor
        Common.tbl = New Data.DataTable()
        Common.tbl.Columns.Add("Srl", GetType(String))
        Common.tbl.Columns.Add("PayCode", GetType(String))
        Common.tbl.Columns.Add("Card No.", GetType(String))
        Common.tbl.Columns.Add("Employee Name", GetType(String))
        Common.tbl.Columns.Add("Date", GetType(String))
        Common.tbl.Columns.Add("Punch1", GetType(String))
        Common.tbl.Columns.Add("Is Manual", GetType(String))
        Common.tbl.Columns.Add("Punch2", GetType(String))
        Common.tbl.Columns.Add("Is Manual ", GetType(String))
        Common.tbl.Columns.Add("Punch3", GetType(String))
        Common.tbl.Columns.Add("Is Manual  ", GetType(String))
        Common.tbl.Columns.Add("Punch4", GetType(String))
        Common.tbl.Columns.Add(" Is Manual ", GetType(String))

        Dim colName As New List(Of String)()
        colName.Add("Srl")
        colName.Add("PayCode")
        colName.Add("Card No.")
        colName.Add("Employee Name")
        colName.Add("Date")
        colName.Add("Punch1")
        colName.Add("Is Manual")
        colName.Add("Punch2")
        colName.Add("Is Manual ")
        colName.Add("Punch3")
        colName.Add("Is Manual  ")
        colName.Add("Punch4")
        colName.Add(" Is Manual ")
        Dim colNameArr() As String = colName.ToArray

        colName.Add("Srl")
        colName.Add("Srl")

        rowcnt = rowcnt + 1
        mCount = 0
        Dim IsManual As String
        Dim FulRawPContaint As New List(Of String)()
        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            Dim tmpCount As Integer = 0
            mPaycode = Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim
            mCARDNO = Rs_Report.Tables(0).Rows(i).Item("CardNo").ToString.Trim
            mEmpName = Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim
            mDate = "00:00:00"
            mCtr = 0
            mCount = mCount + 1
            Do While mPaycode = Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim
                FulRawPContaint.Clear()


                If mDate.ToString("yyyy/MM/dd") <> Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("officepunch").ToString.Trim).ToString("yyyy/MM/dd") Or mCtr > 3 Then
                    'rowcnt = rowcnt + 1
                    If mDate = "00:00:00" Then
                        If tmpCount = 0 Then
                            FulRawPContaint.Add(mCount)
                            FulRawPContaint.Add(mPaycode)
                            FulRawPContaint.Add(mCARDNO)
                            FulRawPContaint.Add(mEmpName)
                        End If
                    End If
                    If tmpCount = 0 Then
                        FulRawPContaint.Add(Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("officepunch").ToString.Trim).ToString("dd/MM/yyyy"))
                    End If
                    If mCtr <= 4 Then
                        'FulRawPContaint.Add(Format(mDate, "dd-MMM-yyyy"))
                    End If
                    'mCtr = 0
                    mColCnt = 5
                Else
                    FulRawPContaint.Add("")
                    FulRawPContaint.Add("")
                    FulRawPContaint.Add("")
                    FulRawPContaint.Add("")
                End If
                mColCnt = mColCnt + 1
                While mCtr <= 4
                    FulRawPContaint.Add(Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("officepunch").ToString.Trim).ToString("HH:mm")) 'mEmpname
                    If Rs_Report.Tables(0).Rows(i).Item("IsManual").ToString.Trim = "Y" Then
                        mColCnt = mColCnt + 1
                        FulRawPContaint.Add("Y") ' Format(!officepunch, "hh:mm") 'mEmpname
                    Else
                        mColCnt = mColCnt + 1
                        FulRawPContaint.Add("N") ' Format(!officepunch, "hh:mm") 'mEmpname
                    End If
                    mCtr = mCtr + 1

                    i = i + 1
                    If i > Rs_Report.Tables(0).Rows.Count - 1 Then
                        Exit Do
                    End If
                    tmpCount = tmpCount + 1
                End While
                Dim FulRawPContaintArr() As String = FulRawPContaint.ToArray
                Dim drRaw As DataRow = Common.tbl.NewRow
                Dim x As Integer = 0
                Dim tmpdate As DateTime = DateEdit1.DateTime
                'drRaw(" ") = FulRawPContaintArr(x)
                'x = 1
                Do While x <= FulRawPContaintArr.Length - 1
                    drRaw(colNameArr(x)) = FulRawPContaintArr(x)
                    tmpdate = tmpdate.AddDays(1)
                    x = x + 1
                    If x >= FulRawPContaintArr.Length Then
                        Exit Do
                    End If
                Loop
                Common.tbl.Rows.Add(drRaw)
            Loop

            i = i - 1
            rowcnt = rowcnt + 1
            mintLine = mintLine + 1
            mblnCheckReport = True
        Next
        Me.Cursor = Cursors.Default
        XtraReportGrid.ShowDialog()
    End Sub
    Sub SpotXl_MachineRawPunchDataGridNew(strsortorder As String)
        Dim whereClause As String
        whereClause = g_WhereClause
        If XtraShortOrder.g_SortOrder <> "" Then
            strsortorder = XtraShortOrder.g_SortOrder
        End If
        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If
        Dim strsql As String
        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet
        Dim tmpmstrFile_Name As String = "iAS_" & Now.ToString("yyyyMMddHHmmss") & ".xlsx"
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\" & tmpmstrFile_Name 'Now.ToString("yyyyMMddHHmmss") & ".xlsx"

        mstrDepartmentCode = " "
        mintPageNo = 1
        mintLine = 1
        mblnCheckReport = False

        strsql = " select * from TblEmployee, tblDepartment, tblCompany, tblCatagory, tblbranch, tblGrade " & _
         " WHERE(TblEmployee.DEPARTMENTCODE = tblDepartment.DEPARTMENTCODE And TblEmployee.COMPANYCODE = tblCompany.COMPANYCODE) " & _
          "and TblEmployee.CAT= tblCatagory.CAT and TblEmployee.BRANCHCODE = tblbranch.BRANCHCODE and  " & _
          "TblEmployee.GradeCode =tblGrade.GradeCode" & IIf(Len(Trim(whereClause)) = 0, "", " AND " & whereClause) & strsortorder
        If Common.servername = "Access" Then          
            adapA = New OleDbDataAdapter(strsql, Common.con1)
            adapA.Fill(Rs_Report)
        Else
            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(Rs_Report)
        End If

        If Rs_Report.Tables(0).Rows.Count < 1 Then
            XtraMessageBox.Show(ulf, "<size=10>Nobody Record Found.</size>", "<size=9>iAS</size>")
            Exit Sub
        End If

        Dim DC As DateConverter = New DateConverter
        Me.Cursor = Cursors.WaitCursor
        Common.tbl = New Data.DataTable()
        Common.tbl.Columns.Add("SrNo", GetType(String))
        Common.tbl.Columns.Add("Date", GetType(String))
        Common.tbl.Columns.Add("PayCode", GetType(String))
        Common.tbl.Columns.Add("Card No", GetType(String))
        Common.tbl.Columns.Add("Employee Name", GetType(String))       
        Common.tbl.Columns.Add("Punche1", GetType(String))
        Common.tbl.Columns.Add("Punche2", GetType(String))
        Common.tbl.Columns.Add("Punche3", GetType(String))
        Common.tbl.Columns.Add("Punche4", GetType(String))
        Dim rowCount As Integer = 0
        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1   'employee            
            Dim startDate As DateTime = DateEdit1.DateTime
            Dim enddate As DateTime = DateEdit2.DateTime
            Dim InOutPunches As String = ""
            'While startDate.ToString("dd/MM/yyyy") <= enddate.ToString("dd/MM/yyyy")
Whilego:    While startDate <= enddate
                Dim Rs_Punches As DataSet = New DataSet
                If Common.servername = "Access" Then
                    strsql = "select * from MachineRawPunchAll where PAYCODE = '" & Rs_Report.Tables(0).Rows(i).Item("PayCode").ToString.Trim & "' " & _
                    "and Format(OFFICEPUNCH,'yyyy-MM-dd HH:mm:ss') >= '" & startDate.ToString("yyyy-MM-dd 00:00:00") & "' and Format(OFFICEPUNCH,'yyyy-MM-dd HH:mm:ss') <= '" & startDate.ToString("yyyy-MM-dd 23:59:59") & "' ORDER by OFFICEPUNCH ASC"

                    adapA = New OleDbDataAdapter(strsql, Common.con1)
                    adapA.Fill(Rs_Punches)
                Else
                    strsql = "select * from MachineRawPunchAll where PAYCODE = '" & Rs_Report.Tables(0).Rows(i).Item("PayCode").ToString.Trim & "' " & _
                    "and OFFICEPUNCH >= '" & startDate.ToString("yyyy-MM-dd 00:00:00") & "' and OFFICEPUNCH <= '" & startDate.ToString("yyyy-MM-dd 23:59:59") & "' ORDER by OFFICEPUNCH ASC"
                    adap = New SqlDataAdapter(strsql, Common.con)
                    adap.Fill(Rs_Punches)
                End If
                InOutPunches = ""
                If Rs_Punches.Tables(0).Rows.Count > 0 Then
                    Common.tbl.Rows.Add()
                    Common.tbl.DefaultView(rowCount)("SrNo") = rowCount + 1 ' i + 1
                    If Common.IsNepali = "Y" Then
                        Dim Vstart As String = DC.ToBS(New Date(startDate.Year, startDate.Month, startDate.Day))
                        Dim dojTmp() As String = Vstart.Split("-")
                        Common.tbl.DefaultView(rowCount)("Date") = dojTmp(2) & "/" & Common.NepaliMonth(dojTmp(1) - 1) & "/" & dojTmp(0)
                    Else
                        Common.tbl.DefaultView(rowCount)("Date") = startDate.ToString("dd/MM/yyyy")
                    End If

                    Common.tbl.DefaultView(rowCount)("PayCode") = Rs_Report.Tables(0).Rows(i).Item("PayCode").ToString.Trim
                    Common.tbl.DefaultView(rowCount)("Card No") = Rs_Report.Tables(0).Rows(i).Item("PRESENTCARDNO").ToString.Trim
                    Common.tbl.DefaultView(rowCount)("Employee Name") = Rs_Report.Tables(0).Rows(i).Item("EMPNAME").ToString.Trim

                    Dim x As Int16 = 0
                    For j As Integer = 0 To Rs_Punches.Tables(0).Rows.Count - 1
                        XtraMasterTest.LabelControlStatus.Text = "Adding Data to Report " & Rs_Report.Tables(0).Rows(i).Item("PayCode").ToString.Trim & " " & _
                            Convert.ToDateTime(Rs_Punches.Tables(0).Rows(j).Item("OFFICEPUNCH").ToString.Trim).ToString("dd/MM/yyyy HH:mm") & "..."
                        System.Windows.Forms.Application.DoEvents()

                        Dim ISMANUAL As String
                        If Rs_Punches.Tables(0).Rows(j).Item("ISMANUAL").ToString.Trim = "Y" Then
                            ISMANUAL = "(M)"
                        Else
                            ISMANUAL = " "
                        End If

                        If x / 4 < 1 Then
tmp:                        Common.tbl.DefaultView(rowCount)("Punche" & x + 1) = Convert.ToDateTime(Rs_Punches.Tables(0).Rows(j).Item("OFFICEPUNCH").ToString.Trim).ToString("HH:mm") & ISMANUAL & " - " & Rs_Punches.Tables(0).Rows(j).Item("INOUT").ToString.Trim
                            x = x + 1
                        Else
                            x = 0
                            rowCount = rowCount + 1
                            Common.tbl.Rows.Add()
                            GoTo tmp
                        End If
                    Next
                    rowCount = rowCount + 1
                Else
                    startDate = startDate.AddDays(1)
                    Continue While
                    'i = i + 1
                    'GoTo Whilego
                End If
                startDate = startDate.AddDays(1)
            End While
            Common.tbl.Rows.Add("")
            rowCount = rowCount + 1
        Next
        XtraMasterTest.LabelControlStatus.Text = ""
        System.Windows.Forms.Application.DoEvents()
        XtraReportGrid.ShowDialog()
        Me.Cursor = Cursors.Default
    End Sub
    Sub SpotXl_PresentGrid(strsortorder As String)
        Dim whereClause As String = ""
        If Common.ReportMail = True Or Common.dashBoardClick = "Present" Then
            DateEdit1.DateTime = Now
            'whereClause = Common.whereClauseEmail & " and TBLEmployee.ACTIVE='Y'"
            If Common.whereClauseEmail.Trim = "" Then
                whereClause = " TBLEmployee.ACTIVE='Y'"
            Else
                whereClause = Common.whereClauseEmail & " and TBLEmployee.ACTIVE='Y'"
            End If
            If Common.IsNepali = "Y" Then
                Dim DC As New DateConverter()
                'Dim tmpNow As DateTime = DC.ToBS(New Date(Now.Year, Now.Month, Now.Day))
                'Common.runningDateTime = tmpNow.Day & "/" & Common.NepaliMonth(tmpNow.Month - 1) & "/" & tmpNow.Year & " " & Now.ToString("HH:mm")
                'Dim Vstart As DateTime = DC.ToBS(New Date(DateEdit1.DateTime.Year, DateEdit1.DateTime.Month, DateEdit1.DateTime.Day))
                'ForDate = Vstart.Day & "/" & Common.NepaliMonth(Vstart.Month - 1) & "/" & Vstart.Year
                Dim tmpNow As String = DC.ToBS(New Date(Now.Year, Now.Month, Now.Day))
                Dim dojTmp() As String = tmpNow.Split("-")
                Common.runningDateTime = dojTmp(2) & "/" & Common.NepaliMonth(dojTmp(1) - 1) & "/" & dojTmp(0) & " " & Now.ToString("HH:mm")
                Dim Vstart As String = DC.ToBS(New Date(DateEdit1.DateTime.Year, DateEdit1.DateTime.Month, DateEdit1.DateTime.Day))
                dojTmp = Vstart.Split("-")
                ForDate = dojTmp(2) & "/" & Common.NepaliMonth(dojTmp(1) - 1) & "/" & dojTmp(0)

            Else
                ForDate = DateEdit1.DateTime.ToString("dd/MM/yyyy")
                Common.runningDateTime = Now.ToString("dd/MM/yyyy HH:mm")
            End If
        Else
            whereClause = g_WhereClause
        End If

        If Common.USERTYPE <> "A" Then
            Dim emp() As String = Common.Auth_Branch.Split(",")
            Dim ls As New List(Of String)()
            For x As Integer = 0 To emp.Length - 1
                ls.Add(emp(x).Trim)
            Next

            Dim com() As String = Common.auth_comp.Split(",")
            Dim lsC As New List(Of String)()
            For x As Integer = 0 To com.Length - 1
                lsC.Add(com(x).Trim)
            Next

            If whereClause = "" Then
                whereClause = " TBLEmployee.BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "')  and TBLEmployee.COMPANYCODE IN ('" & String.Join("', '", lsC.ToArray()) & "')"
            Else
                whereClause = whereClause & " and TBLEmployee.BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "')  and TBLEmployee.COMPANYCODE IN ('" & String.Join("', '", lsC.ToArray()) & "')"
            End If
        End If

        Dim strsql As String
        Dim v_Late As String '* 6
        Dim v_InTime As String '* 6
        Dim strStatus As String, mCount As Integer
        'Dim xlapp As Excel.Application
        'Dim xlwb As Excel.Workbook
        'Dim xlst As Excel.Sheets
        Dim rowcnt As Integer
        'xlapp = CreateObject("Excel.Application")
        'xlwb = xlapp.Workbooks.Add
        mstrDepartmentCode = " "
        mintPageNo = 1
        mintLine = 1
        If XtraShortOrder.g_SortOrder <> "" Then
            strsortorder = XtraShortOrder.g_SortOrder
        End If
        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If
        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".xls"

        mblnCheckReport = False
        If Common.servername = "Access" Then
            strsql = " Select tblTimeregister.PAYCODE, tblTimeregister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, tblDepartment.DepartmentCode, tblDepartment.DepartmentName,tblEmployee.PresentCARDNO, tblEmployee.EMPNAME, tblTimeregister.DATEOFFICE, " & _
              " tblTimeregister.SHIFTATTENDED, tblTimeregister.ShiftStartTime, tblTimeregister.In1, tblTimeregister.LateArrival, tblTimeregister.Status, tblCatagory.Catagoryname, tblDivision.DivisionName " & _
              " From  tblTimeregister, tblEmployee, tblDepartment, tblCatagory, tblDivision,tblCompany " & _
              " Where tblEmployee.PayCode = tblTimeregister.PayCode And tbltimeregister.in1 is not null  and " & _
              " tblEmployee.Companycode=tblCompany.Companycode and tblEmployee.CAT = tblCatagory.cat And " & _
              " FORMAT(tbltimeregister.DateOffice,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' And " & _
              " tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
              " " & IIf(Len(Trim(whereClause)) = 0, "", " AND " & whereClause) & strsortorder
            If Common.IsCompliance Then
                strsql = Microsoft.VisualBasic.Strings.Replace(strsql, "tbltimeregister", "tblTimeRegisterD", 1, -1, Microsoft.VisualBasic.Constants.vbTextCompare)
            End If
            adapA = New OleDbDataAdapter(strsql, Common.con1)
            adapA.Fill(Rs_Report)
        Else
            strsql = " Select tblTimeregister.PAYCODE, tblTimeregister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, tblDepartment.DepartmentCode, tblDepartment.DepartmentName,tblEmployee.PresentCARDNO, tblEmployee.EMPNAME, tblTimeregister.DATEOFFICE, " & _
                 " tblTimeregister.SHIFTATTENDED, tblTimeregister.ShiftStartTime, tblTimeregister.In1, tblTimeregister.LateArrival, tblTimeregister.Status, tblCatagory.Catagoryname, tblDivision.DivisionName " & _
                 " From  tblTimeregister, tblEmployee, tblDepartment, tblCatagory, tblDivision,tblCompany " & _
                 " Where tblEmployee.PayCode = tblTimeregister.PayCode And tbltimeregister.in1 is not null  and " & _
                 " tblEmployee.Companycode=tblCompany.Companycode and tblEmployee.CAT = tblCatagory.cat And " & _
                 " tbltimeregister.DateOffice = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' And " & _
                 " tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
                 " " & IIf(Len(Trim(whereClause)) = 0, "", " AND " & whereClause) & strsortorder
            If Common.IsCompliance Then
                strsql = Microsoft.VisualBasic.Strings.Replace(strsql, "tbltimeregister", "tblTimeRegisterD", 1, -1, Microsoft.VisualBasic.Constants.vbTextCompare)
            End If
            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(Rs_Report)
        End If
        If Rs_Report.Tables(0).Rows.Count < 1 Then
            If Common.ReportMail = True Then
            Else
                XtraMessageBox.Show(ulf, "<size=10>No Data Available For this Report</size>", "<size=9>iAS</size>")
            End If
            Exit Sub
        End If
        rowcnt = 1

        Common.frodatetodatetoReportGrid = "Present Report For DATE : " & ForDate 'DateEdit1.DateTime.ToString("dd/MM/yyyy HH:mm")

        Me.Cursor = Cursors.WaitCursor
        Common.tbl = New Data.DataTable()
        Common.tbl.Columns.Add("Srl", GetType(String))
        Common.tbl.Columns.Add("PayCode", GetType(String))
        Common.tbl.Columns.Add("Card No.", GetType(String))
        Common.tbl.Columns.Add("Employee Name", GetType(String))
        Common.tbl.Columns.Add(" ", GetType(String))
        Common.tbl.Columns.Add("Shift", GetType(String))
        Common.tbl.Columns.Add("Start", GetType(String))
        Common.tbl.Columns.Add("In", GetType(String))
        Common.tbl.Columns.Add("Late", GetType(String))
        Common.tbl.Columns.Add("Status", GetType(String))

        mCount = 0
        Dim shiftStartTimeStr As String
        Dim DepartmentCode = Rs_Report.Tables(0).Rows(0).Item("DepartmentCode").ToString.Trim
        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            If strsortorder.Contains("Department") Then
                If i = 0 Then
                    'objWriter.WriteLine("Department : " & Rs_Report.Tables(0).Rows(i).Item("DepartmentName").ToString.Trim)
                    Common.tbl.Rows.Add("Department : ", Rs_Report.Tables(0).Rows(i).Item("DepartmentName").ToString.Trim)
                End If
                If DepartmentCode <> Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim Then
                    DepartmentCode = Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim
                    Common.tbl.Rows.Add()
                    Common.tbl.Rows.Add("Department : ", Rs_Report.Tables(0).Rows(i).Item("DepartmentName").ToString.Trim)
                End If
            End If

            If Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim = "0" Then
                v_Late = ""
            Else
                v_Late = Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim Mod 60).ToString("00")
            End If
            If Rs_Report.Tables(0).Rows(i).Item("In1").ToString.Trim = "" Then
                v_InTime = " "
            Else
                v_InTime = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("In1").ToString.Trim).ToString("HH:mm")
            End If

            strStatus = ""
            If Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim <> "" Then
                If UCase(Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim) = "MIS" Then
                    strStatus = "P"
                Else
                    strStatus = Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim
                End If
            End If
            rowcnt = rowcnt + 1
            mCount = mCount + 1
            If Rs_Report.Tables(0).Rows(i).Item("shiftStartTime").ToString.Trim = "" Then
                shiftStartTimeStr = ""
            Else
                shiftStartTimeStr = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("shiftStartTime").ToString.Trim).ToString("HH:mm")
            End If
            mintLine = 1
            Common.tbl.Rows.Add(mCount, Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim, Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim,
                               Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim, "", Rs_Report.Tables(0).Rows(i).Item("ShiftAttended").ToString.Trim,
                              shiftStartTimeStr, v_InTime, v_Late, strStatus)
        Next
        Me.Cursor = Cursors.Default
        XtraReportGrid.ShowDialog()
    End Sub
    Sub DailyXl_PerformanceGrid(ByVal strsortorder As String)
        Dim whereClause As String
        If Common.ReportMail = True Then
            DateEdit1.DateTime = Now.AddDays(-1)
            whereClause = Common.whereClauseEmail
        Else
            whereClause = g_WhereClause
        End If
        Dim intFile As Integer
        Dim strsql As String, mCount As Integer
        Dim strOsDuration6 As String ' * 6
        Dim strOtAmount11 As String '* 11
        Dim intPrintLineCounter As Integer
        Dim strIn1Mannual_1 As String '* 1
        Dim strOut1Mannual_1 As String '* 1
        Dim strIn2Mannual_1 As String '* 1
        Dim strOut2Mannual_1 As String '* 1
        Dim blnDeptAvailable As Boolean
        Dim strDeptDivCode As String
        Dim strCurrentDeptDivCode As String
        'Dim xlapp As Excel.Application
        'Dim xlwb As Excel.Workbook
        'Dim xlst As Excel.Sheets
        Dim rowcnt As Integer
        'xlapp = CreateObject("Excel.Application")
        'xlwb = xlapp.Workbooks.Add
        If XtraShortOrder.g_SortOrder <> "" Then
            strsortorder = XtraShortOrder.g_SortOrder
        End If
        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If
        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".xls"

        'If Left(strsortorder, 1) = "D" Or Left(strsortorder, 1) = "C" Then blnDeptAvailable = True
        mblnCheckReport = False
        mintPageNo = 1
        mintLine = 1
        If Common.servername = "Access" Then
            strsql = "select tblCatagory.CatagoryName,TblEmployee.EMPPHOTO, tblDivision.DivisionName,tblTimeRegister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tbltimeregister.ShiftAttended,tbltimeregister.ShiftStartTime,tbltimeregister.In1,tbltimeregister.Out1,tbltimeregister.In2,tbltimeregister.Out2,tbltimeregister.HoursWorked,tbltimeregister.Status,tbltimeregister.EarlyArrival," &
            " tbltimeregister.LateArrival,tbltimeregister.EarlyDeparture,tbltimeregister.ExcLunchHours,tbltimeregister.OtDuration,tbltimeregister.OtAmount,tbltimeregister.OsDuration,tbltimeregister.In1Mannual,tbltimeregister.In2Mannual,tbltimeregister.Out1Mannual,tbltimeregister.Out2Mannual,tblCalander.mDate,tblCalander.Process,tblCalander.NRTCProc, tbltimeregister.ManOTDuration " &
            " from tblCatagory,tblDivision,tbltimeregister,tblEmployee,tblCalander,tblCompany,tblDepartment" &
            " Where tblCatagory.Cat = tblEmployee.Cat And tbltimeregister.PayCode = tblEmployee.PayCode And FORMAT(tbltimeregister.DateOffice,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' And FORMAT(tblCalander.mDate,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" &
            " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode &
            " " & IIf(Len(Trim(whereClause)) = 0, "", " AND " & whereClause) & strsortorder
            If Common.IsCompliance Then
                strsql = Microsoft.VisualBasic.Strings.Replace(strsql, "tbltimeregister", "tblTimeRegisterD", 1, -1, Microsoft.VisualBasic.Constants.vbTextCompare)
            End If

            adapA = New OleDbDataAdapter(strsql, Common.con1)
            adapA.Fill(Rs_Report)
        Else
            strsql = "select tblCatagory.CatagoryName,TblEmployee.EMPPHOTO, tblDivision.DivisionName,tblTimeRegister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tbltimeregister.ShiftAttended,tbltimeregister.ShiftStartTime,tbltimeregister.In1,tbltimeregister.Out1,tbltimeregister.In2,tbltimeregister.Out2,tbltimeregister.HoursWorked,tbltimeregister.Status,tbltimeregister.EarlyArrival," &
             " tbltimeregister.LateArrival,tbltimeregister.EarlyDeparture,tbltimeregister.ExcLunchHours,tbltimeregister.OtDuration,tbltimeregister.OtAmount,tbltimeregister.OsDuration,tbltimeregister.In1Mannual,tbltimeregister.In2Mannual,tbltimeregister.Out1Mannual,tbltimeregister.Out2Mannual,tblCalander.mDate,tblCalander.Process,tblCalander.NRTCProc, tbltimeregister.ManOTDuration " &
             " from tblCatagory,tblDivision,tbltimeregister,tblEmployee,tblCalander,tblCompany,tblDepartment" &
             " Where tblCatagory.Cat = tblEmployee.Cat And tbltimeregister.PayCode = tblEmployee.PayCode And tbltimeregister.DateOffice = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' And tblCalander.mDate = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" &
             " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode &
             " " & IIf(Len(Trim(whereClause)) = 0, "", " AND " & whereClause) & strsortorder
            If Common.IsCompliance Then
                strsql = Microsoft.VisualBasic.Strings.Replace(strsql, "tbltimeregister", "tblTimeRegisterD", 1, -1, Microsoft.VisualBasic.Constants.vbTextCompare)
            End If

            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(Rs_Report)
        End If
        If Rs_Report.Tables(0).Rows.Count < 1 Then
            If Common.ReportMail = True Then
            Else
                XtraMessageBox.Show(ulf, "<size=10>No Data Available For this Report</size>", "<size=9>iAS</size>")
            End If
            Exit Sub
        End If
        rowcnt = 1

        Common.frodatetodatetoReportGrid = "DAILY PERFORMANCE REPORT FOR DATE : " & ForDate 'DateEdit1.DateTime.ToString("dd/MM/yyyy")

        Me.Cursor = Cursors.WaitCursor
        Common.tbl = New Data.DataTable()
        Common.tbl.Columns.Add("Srl", GetType(String))
        Common.tbl.Columns.Add("PayCode", GetType(String))
        Common.tbl.Columns.Add("Card No.", GetType(String))
        Common.tbl.Columns.Add("Employee Name", GetType(String))
        Common.tbl.Columns.Add(" ", GetType(String))
        Common.tbl.Columns.Add("Shift", GetType(String))
        Common.tbl.Columns.Add("Start", GetType(String))
        Common.tbl.Columns.Add("In", GetType(String))
        Common.tbl.Columns.Add("Lunch Out", GetType(String))
        Common.tbl.Columns.Add("Lunch In", GetType(String))
        Common.tbl.Columns.Add("Out", GetType(String))
        Common.tbl.Columns.Add("Hrs Works", GetType(String))
        Common.tbl.Columns.Add("Status", GetType(String))
        Common.tbl.Columns.Add("Early Arriv.", GetType(String))
        Common.tbl.Columns.Add("Late Arriv.", GetType(String))
        Common.tbl.Columns.Add("Shift Early", GetType(String))
        Common.tbl.Columns.Add("Excess Lunch", GetType(String))
        Common.tbl.Columns.Add("OT", GetType(String))
        Common.tbl.Columns.Add("OT Amount", GetType(String))
        Common.tbl.Columns.Add("OS", GetType(String))
        Common.tbl.Columns.Add("Manual", GetType(String))
        Common.tbl.Columns.Add("OTManual", GetType(String))
        'Common.tbl.Columns.Add("EMPPHOTO", GetType(Image))

        mintLine = 9
        mCount = 0
        Dim shiftStartTimeStr As String
        Dim In1Str As String, In2Str As String, out1str As String, out2Str As String, HOURSWORKEDStr As String, EARLYARRIVALStr As String, latearrivalStr As String
        Dim earlydepartureStr As String, EXCLUNCHHOURSStr As String, OtDurationStr As String
        Dim DepartmentCode = Rs_Report.Tables(0).Rows(0).Item("DepartmentCode").ToString.Trim
        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            If strsortorder.Contains("Department") Then
                If i = 0 Then
                    'objWriter.WriteLine("Department : " & Rs_Report.Tables(0).Rows(i).Item("DepartmentName").ToString.Trim)
                   Common.tbl.Rows.Add("Department : ", Rs_Report.Tables(0).Rows(i).Item("DepartmentName").ToString.Trim)
                End If
                If DepartmentCode <> Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim Then
                    DepartmentCode = Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim
                    Common.tbl.Rows.Add()
                    Common.tbl.Rows.Add("Department : ", Rs_Report.Tables(0).Rows(i).Item("DepartmentName").ToString.Trim)
                End If
            End If

            mCount = mCount + 1
            'Do While Not .EOF
            strOsDuration6 = 0
            strOtAmount11 = 0
            If Rs_Report.Tables(0).Rows(i).Item("OsDuration").ToString.Trim = "" Or Rs_Report.Tables(0).Rows(i).Item("OsDuration").ToString.Trim = "0" Then
            Else
                strOsDuration6 = Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("OsDuration").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("OsDuration").ToString.Trim Mod 60).ToString("00")
            End If
            If Rs_Report.Tables(0).Rows(i).Item("OTAmount").ToString.Trim = "" Or Rs_Report.Tables(0).Rows(i).Item("OTAmount").ToString.Trim = "0" Then
            Else
                strOtAmount11 = Rs_Report.Tables(0).Rows(i).Item("OTAmount").ToString.Trim
            End If

            If Rs_Report.Tables(0).Rows(i).Item("IN1MANNUAL").ToString.Trim = "" Then strIn1Mannual_1 = "" Else strIn1Mannual_1 = Rs_Report.Tables(0).Rows(i).Item("IN1MANNUAL").ToString.Trim
            If Rs_Report.Tables(0).Rows(i).Item("Out1Mannual").ToString.Trim = "" Then strOut1Mannual_1 = "" Else strOut1Mannual_1 = Rs_Report.Tables(0).Rows(i).Item("Out1Mannual").ToString.Trim
            If Rs_Report.Tables(0).Rows(i).Item("IN2MANNUAL").ToString.Trim = "" Then strIn2Mannual_1 = "" Else strIn2Mannual_1 = Rs_Report.Tables(0).Rows(i).Item("IN2MANNUAL").ToString.Trim
            If Rs_Report.Tables(0).Rows(i).Item("OUT2MANNUAL").ToString.Trim = "" Then strOut2Mannual_1 = "" Else strOut2Mannual_1 = Rs_Report.Tables(0).Rows(i).Item("OUT2MANNUAL").ToString.Trim
            Dim X As String
            If UCase(strIn1Mannual_1) = "Y" Or UCase(strOut1Mannual_1) = "Y" Or UCase(strIn2Mannual_1) = "Y" Or UCase(strOut2Mannual_1) = "Y" Then
                X = "Y"
            Else
                X = " "
            End If
            If Rs_Report.Tables(0).Rows(i).Item("shiftStartTime").ToString.Trim = "" Then
                shiftStartTimeStr = ""
            Else
                shiftStartTimeStr = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("shiftStartTime").ToString.Trim).ToString("HH:mm")
            End If
            If Rs_Report.Tables(0).Rows(i).Item("In1").ToString.Trim = "" Then
                In1Str = ""
            Else
                In1Str = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("In1").ToString.Trim).ToString("HH:mm")
            End If
            If Rs_Report.Tables(0).Rows(i).Item("out1").ToString.Trim = "" Then
                out1str = ""
            Else
                out1str = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("out1").ToString.Trim).ToString("HH:mm")
            End If
            If Rs_Report.Tables(0).Rows(i).Item("In2").ToString.Trim = "" Then
                In2Str = ""
            Else
                In2Str = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("In2").ToString.Trim).ToString("HH:mm")
            End If
            If Rs_Report.Tables(0).Rows(i).Item("out2").ToString.Trim = "" Then
                out2Str = ""
            Else
                out2Str = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("out2").ToString.Trim).ToString("HH:mm")
            End If
            If Rs_Report.Tables(0).Rows(i).Item("HOURSWORKED").ToString.Trim = "0" Or Rs_Report.Tables(0).Rows(i).Item("HOURSWORKED").ToString.Trim = "" Then
                HOURSWORKEDStr = ""
            Else
                HOURSWORKEDStr = Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("HOURSWORKED").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("HOURSWORKED").ToString.Trim Mod 60).ToString("00")
            End If
            If Rs_Report.Tables(0).Rows(i).Item("EARLYARRIVAL").ToString.Trim = "0" Or Rs_Report.Tables(0).Rows(i).Item("EARLYARRIVAL").ToString.Trim = "" Then
                EARLYARRIVALStr = ""
            Else
                EARLYARRIVALStr = Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("EARLYARRIVAL").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("EARLYARRIVAL").ToString.Trim Mod 60).ToString("00")
            End If
            If Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim = "0" Or Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim = "" Then
                latearrivalStr = ""
            Else
                latearrivalStr = Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim Mod 60).ToString("00")
            End If
            If Rs_Report.Tables(0).Rows(i).Item("earlydeparture").ToString.Trim = "0" Or Rs_Report.Tables(0).Rows(i).Item("earlydeparture").ToString.Trim = "" Then
                earlydepartureStr = ""
            Else
                earlydepartureStr = Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("earlydeparture").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("earlydeparture").ToString.Trim Mod 60).ToString("00")
            End If
            If Rs_Report.Tables(0).Rows(i).Item("EXCLUNCHHOURS").ToString.Trim = "0" Or Rs_Report.Tables(0).Rows(i).Item("EXCLUNCHHOURS").ToString.Trim = "" Then
                EXCLUNCHHOURSStr = ""
            Else
                EXCLUNCHHOURSStr = Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("EXCLUNCHHOURS").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("EXCLUNCHHOURS").ToString.Trim Mod 60).ToString("00")
            End If
            If Rs_Report.Tables(0).Rows(i).Item("OtDuration").ToString.Trim = "0" Or Rs_Report.Tables(0).Rows(i).Item("OtDuration").ToString.Trim = "" Then
                OtDurationStr = ""
            Else
                OtDurationStr = Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("OtDuration").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("OtDuration").ToString.Trim Mod 60).ToString("00")
            End If


            Dim REASON As String = ""
            If Rs_Report.Tables(0).Rows(i).Item("ManOTDuration").ToString.Trim = "0" Or Rs_Report.Tables(0).Rows(i).Item("ManOTDuration").ToString.Trim = "" Then
                REASON = ""
            Else
                REASON = Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("ManOTDuration").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("ManOTDuration").ToString.Trim Mod 60).ToString("00")
            End If


            mblnCheckReport = True
            mintLine = mintLine + 1
            Common.tbl.Rows.Add(mCount, Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim, Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim,
 Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim, "", Rs_Report.Tables(0).Rows(i).Item("ShiftAttended").ToString.Trim, shiftStartTimeStr, In1Str,
 out1str, In2Str, out2Str, HOURSWORKEDStr, Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim, EARLYARRIVALStr, latearrivalStr, earlydepartureStr, EXCLUNCHHOURSStr,
 OtDurationStr, strOtAmount11, strOsDuration6, X, REASON)
        Next
        Me.Cursor = Cursors.Default
        XtraReportGrid.ShowDialog()
    End Sub
    Sub DailyXl_EarlyDepartureGrid(ByVal strsortorder As String)
        'On Error GoTo ErrorGen
        Dim strDeptDivCode As String
        Dim strCurrentDeptDivCode As String
        Dim intFile As Integer
        Dim strsql As String, mCount As Integer
        Dim strOsDuration6 As String ' * 6
        Dim strDepartmentCode As String
        Dim intPrintLineCounter As Integer
        Dim blnDeptAvailable As Boolean
        'Dim xlapp As Excel.Application
        'Dim xlwb As Excel.Workbook
        'Dim xlst As Excel.Sheets
        Dim rowcnt As Integer
        'xlapp = CreateObject("Excel.Application")
        'xlwb = xlapp.Workbooks.Add
        If XtraShortOrder.g_SortOrder <> "" Then
            strsortorder = XtraShortOrder.g_SortOrder
        End If
        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If
        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".xls"

        mblnCheckReport = False
        mintPageNo = 1
        mintLine = 1
        If strsortorder.Substring(0, 1) = "D" Or strsortorder.Substring(0, 1) = "C" Then blnDeptAvailable = True
        If Common.servername = "Access" Then
            strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName,tblTimeRegister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tblEmployeeShiftMaster.IsRoundTheClockWork,tbltimeregister.ShiftAttended,tbltimeregister.ShiftEndTime,tbltimeregister.Out2," & _
           "tbltimeregister.EarlyDeparture,tbltimeregister.Status,tbltimeregister.OtDuration,tbltimeregister.OsDuration,tbltimeregister.In1Mannual,tbltimeregister.In2Mannual,tbltimeregister.Out1Mannual,tbltimeregister.Out2Mannual,tblcalander.mDate,tblcalander.Process,tblcalander.NRTCProc" & _
           " from tblCatagory,tblDivision,  tbltimeregister,tblEmployee,tblEmployeeShiftMaster ,tblCalander ,tblCompany,tblDepartment" & _
           " Where tblCatagory.Cat = tblEmployee.Cat And tbltimeRegister.EarlyDeparture > 0 And tbltimeregister.PayCode = tblEmployee.PayCode And tbltimeregister.PayCode = tblEmployeeShiftMaster.PayCode And FORMAT(tbltimeregister.DateOffice,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & " And FORMAT(tblcalander.mDate,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
           " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
           " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & strsortorder
            If Common.IsCompliance Then
                strsql = Microsoft.VisualBasic.Strings.Replace(strsql, "tbltimeregister", "tblTimeRegisterD", 1, -1, Microsoft.VisualBasic.Constants.vbTextCompare)
            End If
            adapA = New OleDbDataAdapter(strsql, Common.con1)
            adapA.Fill(Rs_Report)
        Else
            strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName,tblTimeRegister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tblEmployeeShiftMaster.IsRoundTheClockWork,tbltimeregister.ShiftAttended,tbltimeregister.ShiftEndTime,tbltimeregister.Out2," & _
             "tbltimeregister.EarlyDeparture,tbltimeregister.Status,tbltimeregister.OtDuration,tbltimeregister.OsDuration,tbltimeregister.In1Mannual,tbltimeregister.In2Mannual,tbltimeregister.Out1Mannual,tbltimeregister.Out2Mannual,tblcalander.mDate,tblcalander.Process,tblcalander.NRTCProc" & _
             " from tblCatagory,tblDivision,  tbltimeregister,tblEmployee,tblEmployeeShiftMaster ,tblCalander ,tblCompany,tblDepartment" & _
             " Where tblCatagory.Cat = tblEmployee.Cat And tbltimeRegister.EarlyDeparture > 0 And tbltimeregister.PayCode = tblEmployee.PayCode And tbltimeregister.PayCode = tblEmployeeShiftMaster.PayCode And tbltimeregister.DateOffice = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & " And tblcalander.mDate = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
             " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
             " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & strsortorder
            If Common.IsCompliance Then
                strsql = Microsoft.VisualBasic.Strings.Replace(strsql, "tbltimeregister", "tblTimeRegisterD", 1, -1, Microsoft.VisualBasic.Constants.vbTextCompare)
            End If
            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(Rs_Report)
        End If
        If Rs_Report.Tables(0).Rows.Count < 1 Then
            XtraMessageBox.Show(ulf, "<size=10>Nobody has departed before the Shift End Time on this Date.</size>", "<size=9>iAS</size>")
            Exit Sub
        End If
        If Rs_Report.Tables(0).Rows(0).Item("Process").ToString.Trim = "" And Rs_Report.Tables(0).Rows(0).Item("NRTCProc").ToString.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Records are not Processed</size>", "<size=9>Information</size>")
            Exit Sub
        ElseIf InStr("YX", Rs_Report.Tables(0).Rows(0).Item("Process").ToString.Trim) > 0 Or InStr("Y", Rs_Report.Tables(0).Rows(0).Item("nRTCProc").ToString.Trim) > 0 Then
            rowcnt = 1
            Common.frodatetodatetoReportGrid = "EARLY DEPARTURE FOR DATE : " & ForDate 'DateEdit1.DateTime.ToString("dd/MM/yyyy")

            Me.Cursor = Cursors.WaitCursor
            Common.tbl = New Data.DataTable()
            Common.tbl.Columns.Add("Srl", GetType(String))
            Common.tbl.Columns.Add("PayCode", GetType(String))
            Common.tbl.Columns.Add("Card No.", GetType(String))
            Common.tbl.Columns.Add("Employee Name", GetType(String))
            Common.tbl.Columns.Add(" ", GetType(String))
            Common.tbl.Columns.Add("RTC", GetType(String))
            Common.tbl.Columns.Add("Shift", GetType(String))
            Common.tbl.Columns.Add("End", GetType(String))
            Common.tbl.Columns.Add("Out", GetType(String))
            Common.tbl.Columns.Add("Shift Early", GetType(String))
            Common.tbl.Columns.Add("Status", GetType(String))
            Common.tbl.Columns.Add("Ot", GetType(String))
            Common.tbl.Columns.Add("Over Stay", GetType(String))
            Common.tbl.Columns.Add("Manual", GetType(String))

            mintLine = 9
            mCount = 0
            Dim shiftEndTimeStr As String
            Dim In1Str As String, In2Str As String, out1str As String, out2Str As String, HOURSWORKEDStr As String, EARLYARRIVALStr As String, latearrivalStr As String
            Dim earlydepartureStr As String, EXCLUNCHHOURSStr As String, OtDurationStr As String, IN1MANNUALStr As String

            For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
                If Rs_Report.Tables(0).Rows(i).Item("OsDuration").ToString.Trim <> "" Or Rs_Report.Tables(0).Rows(i).Item("OsDuration").ToString.Trim <> "0" Then
                    strOsDuration6 = Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("OsDuration").ToString.Trim / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("OsDuration").ToString.Trim Mod 60).ToString("00")
                Else
                    strOsDuration6 = ""
                End If
                rowcnt = rowcnt + 1
                mCount = mCount + 1
                If Rs_Report.Tables(0).Rows(i).Item("shiftEndTime").ToString.Trim = "" Then
                    shiftEndTimeStr = ""
                Else
                    shiftEndTimeStr = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("shiftEndTime").ToString.Trim).ToString("HH:mm")
                End If
                If Rs_Report.Tables(0).Rows(i).Item("out2").ToString.Trim = "" Then
                    out2Str = ""
                Else
                    out2Str = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("out2").ToString.Trim).ToString("HH:mm")
                End If
                If Rs_Report.Tables(0).Rows(i).Item("earlydeparture").ToString.Trim = "0" Then
                    earlydepartureStr = ""
                Else
                    earlydepartureStr = Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("earlydeparture").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("earlydeparture").ToString.Trim Mod 60).ToString("00")
                End If
                If Rs_Report.Tables(0).Rows(i).Item("OtDuration").ToString.Trim = "0" Then
                    OtDurationStr = ""
                Else
                    OtDurationStr = Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("OtDuration").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("OtDuration").ToString.Trim Mod 60).ToString("00")
                End If
                IN1MANNUALStr = IIf(UCase(Rs_Report.Tables(0).Rows(i).Item("IN1MANNUAL").ToString.Trim) = "Y", "M", IIf(UCase(Rs_Report.Tables(0).Rows(i).Item("Out1Mannual").ToString.Trim) = "Y", "M", IIf(UCase(Rs_Report.Tables(0).Rows(i).Item("IN2MANNUAL").ToString.Trim) = "Y", "M", IIf(UCase(Rs_Report.Tables(0).Rows(i).Item("OUT2MANNUAL").ToString.Trim) = "Y", "M", ""))))
                rowcnt = rowcnt + 1
                mintLine = mintLine + 1
                mblnCheckReport = True
                Common.tbl.Rows.Add(mCount, Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim, Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim, _
                                    Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim.Trim, "", Rs_Report.Tables(0).Rows(i).Item("isroundtheclockwork").ToString.Trim, _
                                    Rs_Report.Tables(0).Rows(i).Item("ShiftAttended").ToString.Trim, shiftEndTimeStr, out2Str, earlydepartureStr, _
                                    Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim, OtDurationStr, strOsDuration6, IN1MANNUALStr)
            Next
        Else
            XtraMessageBox.Show(ulf, "<size=10>Records are not Processed</size>", "<size=9>iAS</size>")
            Exit Sub
        End If
        Me.Cursor = Cursors.Default
        XtraReportGrid.ShowDialog()
    End Sub
    Sub DailyXl_TimeLossGrid(ByVal strsortorder As String)
        'On Error GoTo ErrorGen
        'Dim xlapp As Excel.Application
        'Dim xlwb As Excel.Workbook
        'Dim xlst As Excel.Sheets
        Dim strDeptDivCode As String
        Dim strCurrentDeptDivCode As String
        Dim intFile As Integer
        Dim strsql As String, mCount As Integer
        Dim strOsDuration6 As String '* 6
        Dim strDepartmentCode As String
        Dim intPrintLineCounter As Integer
        Dim blnDeptAvailable As Boolean
        Dim rowcnt As Integer
        'xlapp = CreateObject("Excel.Application")
        'xlwb = xlapp.Workbooks.Add
        mblnCheckReport = False
        mintPageNo = 1
        mintLine = 1
        If XtraShortOrder.g_SortOrder <> "" Then
            strsortorder = XtraShortOrder.g_SortOrder
        End If
        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If
        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".xls"
        If Common.servername = "Access" Then
            strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tblTimeRegister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tbltimeregister.ShiftAttended,tbltimeregister.ShiftStartTime,tbltimeregister.ShiftEndTime,tbltimeregister.EarlyArrival," & _
           "tbltimeregister.LateArrival,tbltimeregister.EarlyDeparture,tbltimeregister.Status,tbltimeregister.LunchLateArrival,tbltimeregister.LunchEarlyDeparture,tbltimeregister.ExcLunchHours,tbltimeregister.OtDuration," & _
           "tbltimeregister.TotalLossHrs,tbltimeregister.OsDuration,tbltimeregister.In1Mannual,tbltimeregister.In2Mannual,tbltimeregister.Out1Mannual,tbltimeregister.Out2Mannual,tblCalander.mDate,tblCalander.Process,tblCalander.NRTCProc" & _
           " from tblCatagory,tblDivision,  tbltimeregister,tblEmployee,tblCalander,tblCompany,tblDepartment" & _
           " Where tblCatagory.Cat = tblEmployee.Cat And tblTimeRegister.totallosshrs > 0 and tbltimeregister.PayCode = tblEmployee.PayCode And FORMAT(tbltimeregister.DateOffice,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' And FORMAT(tblCalander.mDate,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
           " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
           " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & strsortorder
            adapA = New OleDbDataAdapter(strsql, Common.con1)
            adapA.Fill(Rs_Report)
        Else
            strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tblTimeRegister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tbltimeregister.ShiftAttended,tbltimeregister.ShiftStartTime,tbltimeregister.ShiftEndTime,tbltimeregister.EarlyArrival," & _
             "tbltimeregister.LateArrival,tbltimeregister.EarlyDeparture,tbltimeregister.Status,tbltimeregister.LunchLateArrival,tbltimeregister.LunchEarlyDeparture,tbltimeregister.ExcLunchHours,tbltimeregister.OtDuration," & _
             "tbltimeregister.TotalLossHrs,tbltimeregister.OsDuration,tbltimeregister.In1Mannual,tbltimeregister.In2Mannual,tbltimeregister.Out1Mannual,tbltimeregister.Out2Mannual,tblCalander.mDate,tblCalander.Process,tblCalander.NRTCProc" & _
             " from tblCatagory,tblDivision,  tbltimeregister,tblEmployee,tblCalander,tblCompany,tblDepartment" & _
             " Where tblCatagory.Cat = tblEmployee.Cat And tblTimeRegister.totallosshrs > 0 and tbltimeregister.PayCode = tblEmployee.PayCode And tbltimeregister.DateOffice = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' And tblCalander.mDate = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
             " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
             " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & strsortorder
            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(Rs_Report)
        End If

        If Rs_Report.Tables(0).Rows.Count < 1 Then
            XtraMessageBox.Show(ulf, "<size=10>There is no Time Loss for any Employee on this Date.</size>", "<size=9>iAS</size>")
            Exit Sub
        End If
        Common.frodatetodatetoReportGrid = "TIME LOSS FOR DATE : " & ForDate 'DateEdit1.DateTime.ToString("dd/MM/yyyy")
        rowcnt = rowcnt + 1

        Me.Cursor = Cursors.WaitCursor
        Common.tbl = New Data.DataTable()
        Common.tbl.Columns.Add("Srl", GetType(String))
        Common.tbl.Columns.Add("PayCode", GetType(String))
        Common.tbl.Columns.Add("Card No.", GetType(String))
        Common.tbl.Columns.Add("Employee Name", GetType(String))
        Common.tbl.Columns.Add(" ", GetType(String))
        Common.tbl.Columns.Add("Shift", GetType(String))
        Common.tbl.Columns.Add("Start", GetType(String))
        Common.tbl.Columns.Add("End", GetType(String))
        Common.tbl.Columns.Add("Status", GetType(String))
        Common.tbl.Columns.Add("Early Arriv.", GetType(String))
        Common.tbl.Columns.Add("Shift Late", GetType(String))
        Common.tbl.Columns.Add("Early Departure", GetType(String))
        Common.tbl.Columns.Add("Lunch Late", GetType(String))
        Common.tbl.Columns.Add("Lunch Early", GetType(String))
        Common.tbl.Columns.Add("Excess Lunch", GetType(String))
        Common.tbl.Columns.Add("OT", GetType(String))
        Common.tbl.Columns.Add("Loss Hours", GetType(String))
        Common.tbl.Columns.Add("Over Stay", GetType(String))
        Common.tbl.Columns.Add("Manual", GetType(String))

        rowcnt = rowcnt + 1
        mintLine = 9
        mCount = 0
        Dim shiftStartTimeStr As String, shiftEndTimeStr As String, LunchLateArrivalStr As String, LUNCHEARLYDEPARTUREStr As String, TotalLossHrsStr As String, IN1MANNUALStr As String
        Dim In1Str As String, In2Str As String, out1str As String, out2Str As String, HOURSWORKEDStr As String, EARLYARRIVALStr As String, latearrivalStr As String
        Dim earlydepartureStr As String, EXCLUNCHHOURSStr As String, OtDurationStr As String
        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            strOsDuration6 = 0
            If Rs_Report.Tables(0).Rows(i).Item("OsDuration").ToString.Trim <> "" Or Rs_Report.Tables(0).Rows(i).Item("OsDuration").ToString.Trim <> "0" Then
                strOsDuration6 = Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("OsDuration").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("OsDuration").ToString.Trim Mod 60).ToString("00")
            Else
                strOsDuration6 = ""
            End If
            rowcnt = rowcnt + 1
            mCount = mCount + 1

            If Rs_Report.Tables(0).Rows(i).Item("shiftStartTime").ToString.Trim = "" Then
                shiftStartTimeStr = ""
            Else
                shiftStartTimeStr = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("shiftStartTime").ToString.Trim).ToString("HH:mm")
            End If
            If Rs_Report.Tables(0).Rows(i).Item("shiftEndTime").ToString.Trim = "" Then
                shiftEndTimeStr = ""
            Else
                shiftEndTimeStr = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("shiftEndTime").ToString.Trim).ToString("HH:mm")
            End If
            If Rs_Report.Tables(0).Rows(i).Item("EARLYARRIVAL").ToString.Trim = "0" Then
                EARLYARRIVALStr = ""
            Else
                EARLYARRIVALStr = Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("EARLYARRIVAL").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("EARLYARRIVAL").ToString.Trim Mod 60).ToString("00")
            End If

            If Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim = "0" Then
                latearrivalStr = ""
            Else
                latearrivalStr = Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim Mod 60).ToString("00")
            End If

            If Rs_Report.Tables(0).Rows(i).Item("earlydeparture").ToString.Trim = "0" Then
                earlydepartureStr = ""
            Else
                earlydepartureStr = Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("earlydeparture").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("earlydeparture").ToString.Trim Mod 60).ToString("00")
            End If

            If Rs_Report.Tables(0).Rows(i).Item("LunchLateArrival").ToString.Trim = "0" Then
                LunchLateArrivalStr = ""
            Else
                LunchLateArrivalStr = Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("LunchLateArrival").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("LunchLateArrival").ToString.Trim Mod 60).ToString("00")
            End If

            If Rs_Report.Tables(0).Rows(i).Item("LUNCHEARLYDEPARTURE").ToString.Trim = "0" Then
                LUNCHEARLYDEPARTUREStr = ""
            Else
                LUNCHEARLYDEPARTUREStr = Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("LUNCHEARLYDEPARTURE").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("LUNCHEARLYDEPARTURE").ToString.Trim Mod 60).ToString("00")
            End If

            If Rs_Report.Tables(0).Rows(i).Item("EXCLUNCHHOURS").ToString.Trim = "0" Then
                EXCLUNCHHOURSStr = ""
            Else
                EXCLUNCHHOURSStr = Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("EXCLUNCHHOURS").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("EXCLUNCHHOURS").ToString.Trim Mod 60).ToString("00")
            End If

            If Rs_Report.Tables(0).Rows(i).Item("OtDuration").ToString.Trim = "0" Then
                OtDurationStr = ""
            Else
                OtDurationStr = Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("OtDuration").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("OtDuration").ToString.Trim Mod 60).ToString("00")
            End If

            If Rs_Report.Tables(0).Rows(i).Item("TotalLossHrs").ToString.Trim = "0" Then
                TotalLossHrsStr = ""
            Else
                TotalLossHrsStr = Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("TotalLossHrs").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("TotalLossHrs").ToString.Trim Mod 60).ToString("00")
            End If
            IN1MANNUALStr = IIf(UCase(Rs_Report.Tables(0).Rows(i).Item("IN1MANNUAL").ToString.Trim) = "Y", "M", IIf(UCase(Rs_Report.Tables(0).Rows(i).Item("Out1Mannual").ToString.Trim) = "Y", "M", IIf(UCase(Rs_Report.Tables(0).Rows(i).Item("IN2MANNUAL").ToString.Trim) = "Y", "M", IIf(UCase(Rs_Report.Tables(0).Rows(i).Item("OUT2MANNUAL").ToString.Trim) = "Y", "M", ""))))
            rowcnt = rowcnt + 1

            mintLine = mintLine + 1
            mblnCheckReport = True
            Common.tbl.Rows.Add(mCount, Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim, Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim, _
                               Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim, "", Rs_Report.Tables(0).Rows(i).Item("ShiftAttended").ToString.Trim.Trim, _
                               shiftStartTimeStr, shiftEndTimeStr, Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim, EARLYARRIVALStr, latearrivalStr, _
                               earlydepartureStr, LunchLateArrivalStr, LUNCHEARLYDEPARTUREStr, EXCLUNCHHOURSStr, OtDurationStr, TotalLossHrsStr, strOsDuration6, IN1MANNUALStr)
        Next
        Me.Cursor = Cursors.Default
        XtraReportGrid.ShowDialog()
    End Sub
    Sub DailyXl_OverTimeRegisterGrid(ByVal strsortorder As String)
        Dim strDeptDivCode As String
        Dim strCurrentDeptDivCode As String
        Dim intFile As Integer
        Dim strsql As String, mCount As Integer
        Dim strOtAmount11 As String '* 11
        Dim strDepartmentCode As String
        Dim intPrintLineCounter As Integer
        Dim blnDeptAvailable As Boolean
        'Dim xlapp As Excel.Application
        'Dim xlwb As Excel.Workbook
        'Dim xlst As Excel.Sheets
        Dim rowcnt As Integer
        'xlapp = CreateObject("Excel.Application")
        'xlwb = xlapp.Workbooks.Add

        mblnCheckReport = False
        mintPageNo = 1
        mintLine = 1
        If XtraShortOrder.g_SortOrder <> "" Then
            strsortorder = XtraShortOrder.g_SortOrder
        End If
        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If
        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".xls"

        If Common.servername = "Access" Then
            strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tblTimeRegister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tbltimeregister.OtDuration,tbltimeregister.OtAmount,tblCalander.mDate,tblCalander.Process,tblCalander.NRTCProc" & _
           " from tblCatagory,tblDivision,  tbltimeregister,tblEmployee,tblCalander,tblCompany,tblDepartment" & _
           " Where tblCatagory.Cat = tblEmployee.Cat And tbltimeregister.OtDuration > 0 and tbltimeregister.PayCode = tblEmployee.PayCode And FORMAT(tbltimeregister.DateOffice,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' And FORMAT(tblCalander.mDate,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
           " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
           " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & strsortorder
            adapA = New OleDbDataAdapter(strsql, Common.con1)
            adapA.Fill(Rs_Report)
        Else
            strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tblTimeRegister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tbltimeregister.OtDuration,tbltimeregister.OtAmount,tblCalander.mDate,tblCalander.Process,tblCalander.NRTCProc" & _
             " from tblCatagory,tblDivision,  tbltimeregister,tblEmployee,tblCalander,tblCompany,tblDepartment" & _
             " Where tblCatagory.Cat = tblEmployee.Cat And tbltimeregister.OtDuration > 0 and tbltimeregister.PayCode = tblEmployee.PayCode And tbltimeregister.DateOffice = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' And tblCalander.mDate = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
             " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
             " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & strsortorder
            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(Rs_Report)
        End If
        If Rs_Report.Tables(0).Rows.Count < 1 Then
            XtraMessageBox.Show(ulf, "<size=10>There is no Over Time for any Employee on this Date.</size>", "<size=9>iAS</size>")
            Exit Sub
        End If
        Common.frodatetodatetoReportGrid = "OVER TIME FOR DATE : " & ForDate 'DateEdit1.DateTime.ToString("dd/MM/yyyy")
        Me.Cursor = Cursors.WaitCursor
        Common.tbl = New Data.DataTable()
        Common.tbl.Columns.Add("Srl", GetType(String))
        Common.tbl.Columns.Add("PayCode", GetType(String))
        Common.tbl.Columns.Add("Card No.", GetType(String))
        Common.tbl.Columns.Add("Employee Name", GetType(String))
        Common.tbl.Columns.Add(" ", GetType(String))
        Common.tbl.Columns.Add("Ot", GetType(String))
        Common.tbl.Columns.Add("OT Amount", GetType(String))

        mintLine = 9
        mCount = 0
        Dim OtDurationStr As String
        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            strOtAmount11 = 0
            If Rs_Report.Tables(0).Rows(i).Item("OTAmount").ToString.Trim <> "" Then If Rs_Report.Tables(0).Rows(i).Item("OTAmount").ToString.Trim <> "0" Then strOtAmount11 = Trim(Length7(CStr(Rs_Report.Tables(0).Rows(i).Item("OTAmount").ToString.Trim)))
            rowcnt = rowcnt + 1
            mCount = mCount + 1
            If Rs_Report.Tables(0).Rows(i).Item("OtDuration").ToString.Trim = "0" Then
                OtDurationStr = ""
            Else
                OtDurationStr = Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("OtDuration").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("OtDuration").ToString.Trim Mod 60).ToString("00")
            End If
            Common.tbl.Rows.Add(mCount, Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim, Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim, _
                                Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim, "", OtDurationStr, strOtAmount11)
            mintLine = mintLine + 1
            mblnCheckReport = True
        Next
        Me.Cursor = Cursors.Default
        XtraReportGrid.ShowDialog()
    End Sub
    Sub DailyXl_OverTimeSummaryGrid()
        'Dim xlapp As Excel.Application
        ' Dim xlwb As Excel.Workbook
        ' Dim xlst As Excel.Sheets
        Dim intFile As Integer
        Dim strsql As String, mCount As Integer
        Dim strDepartmentName As String
        Dim strDepartmentName45 As String '* 45
        Dim strOtDuration10 As String '* 10
        Dim strOtAmount11 As String '* 11
        Dim dblOtDuration As Double
        Dim dblOtAmount As Double

        Dim rowcnt As Integer
        'xlapp = CreateObject("Excel.Application")
        'xlwb = xlapp.Workbooks.Add
        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If
        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".xls"

        mblnCheckReport = False
        mintPageNo = 1
        mintLine = 1
        If Common.servername = "Access" Then
            strsql = "select tblCatagory.CatagoryName, tblTimeRegister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregister.PayCode,tbltimeregister.OtDuration,tbltimeregister.OtAmount,tblDepartment.DepartmentName,tblCalander.mDate,tblCalander.Process,tblCalander.NRTCProc" & _
            " from tblCatagory,tbltimeregister,tblDepartment,tblCalander,tblEmployee " & _
            " Where tblCatagory.Cat = tblEmployee.Cat And tblTimeRegister.OtDuration > 0 And tbltimeregister.PayCode = tblEmployee.PayCode And TblEmployee.DepartmentCode = TblDepartment.DepartmentCode And FORMAT(tbltimeregister.DateOffice,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' And FORMAT(tblCalander.mDate,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
            " "
            adapA = New OleDbDataAdapter(strsql, Common.con1)
            adapA.Fill(Rs_Report)
        Else
            strsql = "select tblCatagory.CatagoryName, tblTimeRegister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregister.PayCode,tbltimeregister.OtDuration,tbltimeregister.OtAmount,tblDepartment.DepartmentName,tblCalander.mDate,tblCalander.Process,tblCalander.NRTCProc" & _
             " from tblCatagory,tbltimeregister,tblDepartment,tblCalander,tblEmployee " & _
             " Where tblCatagory.Cat = tblEmployee.Cat And tblTimeRegister.OtDuration > 0 And tbltimeregister.PayCode = tblEmployee.PayCode And TblEmployee.DepartmentCode = TblDepartment.DepartmentCode And tbltimeregister.DateOffice = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' And tblCalander.mDate = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
             " "
            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(Rs_Report)
        End If
        If Rs_Report.Tables(0).Rows.Count < 1 Then
            XtraMessageBox.Show(ulf, "<size=10>There is no Over Time for any Employee on this Date.</size>", "<size=9>iAS</size>")
            Exit Sub
        End If

        Common.frodatetodatetoReportGrid = "OVER TIME SUMMARY FOR DATE : " & ForDate 'DateEdit1.DateTime.ToString("yyyy-MM-dd")
        rowcnt = rowcnt + 1
        rowcnt = rowcnt + 1

        Me.Cursor = Cursors.WaitCursor
        Common.tbl = New Data.DataTable()
        Common.tbl.Columns.Add("Srl", GetType(String))
        Common.tbl.Columns.Add("Department", GetType(String))
        Common.tbl.Columns.Add(" ", GetType(String))
        Common.tbl.Columns.Add("  ", GetType(String))
        Common.tbl.Columns.Add("Ot", GetType(String))
        Common.tbl.Columns.Add("OT Amount", GetType(String))

        mintLine = 9
        mCount = 0
        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            strDepartmentName = Rs_Report.Tables(0).Rows(i).Item("DepartmentName").ToString.Trim
            dblOtDuration = 0
            dblOtAmount = 0
            strOtDuration10 = 0
            strOtAmount11 = 0
            Do While strDepartmentName = Rs_Report.Tables(0).Rows(i).Item("DepartmentName").ToString.Trim
                If Rs_Report.Tables(0).Rows(i).Item("OtDuration").ToString.Trim <> "" Or Rs_Report.Tables(0).Rows(i).Item("OtDuration").ToString.Trim <> "0" Then dblOtDuration = CDbl(Rs_Report.Tables(0).Rows(i).Item("OtDuration").ToString.Trim) + dblOtDuration
                If Rs_Report.Tables(0).Rows(i).Item("OTAmount").ToString.Trim <> "" Or Rs_Report.Tables(0).Rows(i).Item("OTAmount").ToString.Trim <> "0" Then dblOtAmount = CDbl(Rs_Report.Tables(0).Rows(i).Item("OTAmount").ToString.Trim) + dblOtAmount
                i = i + 1
                If i >= Rs_Report.Tables(0).Rows.Count - 1 Then
                    i = i - 1
                    Exit Do
                End If
            Loop
            strOtDuration10 = 0
            strOtAmount11 = 0
            If dblOtDuration <> 0 Then strOtDuration10 = Math.Truncate(dblOtDuration / 60).ToString("00") & ":" & (dblOtDuration Mod 60).ToString("00")

            If dblOtAmount.ToString.Trim <> "" Then If dblOtAmount <> 0 Then strOtAmount11 = Trim(Length7(CStr(dblOtAmount)))
            strDepartmentName45 = strDepartmentName
            rowcnt = rowcnt + 1
            mCount = mCount + 1
            'xlapp.Cells(rowcnt, 1) = mCount
            'xlapp.Cells(rowcnt, 2) = strDepartmentName
            'xlapp.Cells(rowcnt, 5) = "'" & (strOtDuration10)
            'xlapp.Cells(rowcnt, 6) = "'" & (strOtAmount11)
            rowcnt = rowcnt + 1
            Common.tbl.Rows.Add(mCount, strDepartmentName, "", "", strOtDuration10, strOtAmount11)
            mintLine = mintLine + 1
            mblnCheckReport = True
            'If mintLine >= g_LinesPerPage Then  'And Not .EOF
            '    mintPageNo = mintPageNo + 1
            '    rowcnt = 1
            '    xlapp.Visible = True

            '    xlapp.Cells(rowcnt, 5) = "Company Name:" & CommonReport.g_CompanyNames
            '    rowcnt = rowcnt + 1
            '    xlapp.Cells(rowcnt, 5) = "Run Date & Time :" & Now.ToString("dd/MM/yyyy HH:mm")
            '    rowcnt = rowcnt + 2
            '    xlapp.Cells.Font.Size = 10
            '    xlapp.Cells(rowcnt, 5) = "OVER TIME SUMMARY FOR DATE : " & DateEdit1.DateTime.ToString("yyyy-MM-dd")
            '    rowcnt = rowcnt + 1
            '    xlapp.Cells(rowcnt, 2) = "Department"
            '    xlapp.Cells(rowcnt, 5) = "              Ot "
            '    xlapp.Cells(rowcnt, 6) = "OT Amount "
            'mintLine = 9
            'End If
        Next
        Me.Cursor = Cursors.Default
        XtraReportGrid.ShowDialog()
    End Sub
    Sub DailyXl_ShiftChangeStatementGrid(ByVal strsortorder As String)
        'On Error GoTo ErrorGen
        Dim strDeptDivCode As String
        Dim strCurrentDeptDivCode As String
        Dim intFile As Integer
        Dim strsql As String
        Dim strDepartmentCode As String
        Dim intPrintLineCounter As Integer
        Dim blnDeptAvailable As Boolean
        'Dim xlapp As Excel.Application
        'Dim xlwb As Excel.Workbook
        'Dim xlst As Excel.Sheets
        Dim rowcnt As Integer
        'xlapp = CreateObject("Excel.Application")
        'xlwb = xlapp.Workbooks.Add

        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If
        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".xls"

        mblnCheckReport = False
        mintPageNo = 1
        mintLine = 1
        If strsortorder = "" Then
            If Common.servername = "Access" Then
                strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tbltimeregister.Shift,tbltimeregister.ShiftAttended,tbltimeregister.ShiftStartTime,tbltimeregister.In1,tbltimeregister.Out2,tbltimeregister.HoursWorked,tbltimeregister.Status,tbltimeregister.EarlyArrival,tbltimeregister.LateArrival,tbltimeregister.In1Mannual,tbltimeregister.In2Mannual,tbltimeregister.Out1Mannual,tbltimeregister.Out2Mannual,tblCalander.Process,tblCalander.NRTCProc" & _
               " from tblCatagory,tblDivision,  tbltimeregister,tblEmployee,tblCalander,tblCompany,tblDepartment" & _
               " Where tblCatagory.Cat = tblEmployee.Cat And tbltimeregister.PayCode = tblEmployee.PayCode And tbltimeregister.Shift <> tbltimeregister.ShiftAttended And FORMAT(tbltimeregister.DateOffice,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & " And FORMAT(tblCalander.mDate,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
               " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
               " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & strsortorder
                adapA = New OleDbDataAdapter(strsql, Common.con1)
                adapA.Fill(Rs_Report)
            Else
                strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tbltimeregister.Shift,tbltimeregister.ShiftAttended,tbltimeregister.ShiftStartTime,tbltimeregister.In1,tbltimeregister.Out2,tbltimeregister.HoursWorked,tbltimeregister.Status,tbltimeregister.EarlyArrival,tbltimeregister.LateArrival,tbltimeregister.In1Mannual,tbltimeregister.In2Mannual,tbltimeregister.Out1Mannual,tbltimeregister.Out2Mannual,tblCalander.Process,tblCalander.NRTCProc" & _
                 " from tblCatagory,tblDivision,  tbltimeregister,tblEmployee,tblCalander,tblCompany,tblDepartment" & _
                 " Where tblCatagory.Cat = tblEmployee.Cat And tbltimeregister.PayCode = tblEmployee.PayCode And tbltimeregister.Shift <> tbltimeregister.ShiftAttended And tbltimeregister.DateOffice = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & " And tblCalander.mDate = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
                 " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
                 " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & strsortorder
                adap = New SqlDataAdapter(strsql, Common.con)
                adap.Fill(Rs_Report)
            End If
            If Rs_Report.Tables(0).Rows.Count < 1 Then
                XtraMessageBox.Show(ulf, "<size=10>There is no Auto Shift Change for any Employee on this Date.</size>", "<size=9>iAS</size>")
                Exit Sub
            End If
        End If
        Common.frodatetodatetoReportGrid = "SHIFT CHANGE STATEMENT FOR DATE : " & ForDate 'DateEdit1.DateTime.ToString("dd/MM/yyyy")
        rowcnt = rowcnt + 1
        rowcnt = rowcnt + 1

        Me.Cursor = Cursors.WaitCursor
        Common.tbl = New Data.DataTable()
        Common.tbl.Columns.Add("PayCode", GetType(String))
        Common.tbl.Columns.Add("Card No.", GetType(String))
        Common.tbl.Columns.Add("Employee Name", GetType(String))
        Common.tbl.Columns.Add(" ", GetType(String))
        Common.tbl.Columns.Add("Actual Shift", GetType(String))
        Common.tbl.Columns.Add("Shift", GetType(String))
        Common.tbl.Columns.Add("Start", GetType(String))
        Common.tbl.Columns.Add("In", GetType(String))
        Common.tbl.Columns.Add("Out", GetType(String))
        Common.tbl.Columns.Add("Hours Worked", GetType(String))
        Common.tbl.Columns.Add("Status", GetType(String))
        Common.tbl.Columns.Add("Shift Late", GetType(String))
        Common.tbl.Columns.Add("Manual", GetType(String))
        rowcnt = rowcnt + 1
        mintLine = 9

        Dim shiftStartTimeStr As String, shiftEndTimeStr As String, LunchLateArrivalStr As String, LUNCHEARLYDEPARTUREStr As String, TotalLossHrsStr As String, IN1MANNUALStr As String
        Dim In1Str As String, In2Str As String, out1str As String, out2Str As String, HOURSWORKEDStr As String, EARLYARRIVALStr As String, latearrivalStr As String
        Dim earlydepartureStr As String, EXCLUNCHHOURSStr As String, OtDurationStr As String

        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            rowcnt = rowcnt + 1
            'xlapp.Cells(rowcnt, 2) = "'" & Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim
            'xlapp.Cells(rowcnt, 3) = "'" & Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim
            'xlapp.Cells(rowcnt, 4) = Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim
            ' xlapp.Cells(rowcnt, 6) = Rs_Report.Tables(0).Rows(i).Item("Shift").ToString.Trim
            'xlapp.Cells(rowcnt, 7) = Rs_Report.Tables(0).Rows(i).Item("ShiftAttended").ToString.Trim
            If Rs_Report.Tables(0).Rows(i).Item("shiftStartTime").ToString.Trim = "" Then
                shiftStartTimeStr = ""
            Else
                shiftStartTimeStr = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("shiftStartTime").ToString.Trim).ToString("HH:mm")
            End If
            If Rs_Report.Tables(0).Rows(i).Item("In1").ToString.Trim = "" Then
                In1Str = ""
            Else
                In1Str = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("In1").ToString.Trim).ToString("HH:mm")
            End If
            If Rs_Report.Tables(0).Rows(i).Item("out2").ToString.Trim = "" Then
                out2Str = ""
            Else
                out2Str = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("out2").ToString.Trim).ToString("HH:mm")
            End If
            If Rs_Report.Tables(0).Rows(i).Item("HOURSWORKED").ToString.Trim = "0" Then
                HOURSWORKEDStr = "0"
            Else
                HOURSWORKEDStr = Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("HOURSWORKED").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("HOURSWORKED").ToString.Trim Mod 60).ToString("00")
            End If
            'xlapp.Cells(rowcnt, 12) = Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim
            If Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim = "0" Then
                latearrivalStr = ""
            Else
                latearrivalStr = Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim Mod 60).ToString("00")
            End If
            If UCase(Rs_Report.Tables(0).Rows(i).Item("IN1MANNUAL").ToString.Trim) = "Y" Or UCase(Rs_Report.Tables(0).Rows(i).Item("Out1Mannual").ToString.Trim) = "Y" Or UCase(Rs_Report.Tables(0).Rows(i).Item("IN2MANNUAL").ToString.Trim) = "Y" Or UCase(Rs_Report.Tables(0).Rows(i).Item("OUT2MANNUAL").ToString.Trim) = "Y" Then
                IN1MANNUALStr = "M"
            Else
                IN1MANNUALStr = ""
            End If
            rowcnt = rowcnt + 1
            mintLine = mintLine + 1
            mblnCheckReport = True
            Common.tbl.Rows.Add(Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim, Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim, _
                               Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim, "", Rs_Report.Tables(0).Rows(i).Item("Shift").ToString.Trim,
                               Rs_Report.Tables(0).Rows(i).Item("ShiftAttended").ToString.Trim, shiftStartTimeStr, In1Str, out2Str, HOURSWORKEDStr, _
                               Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim, latearrivalStr, IN1MANNUALStr)

        Next
        Me.Cursor = Cursors.Default
        XtraReportGrid.ShowDialog()
    End Sub
    Sub DailyXl_OutWorkReportGrid(ByVal strsortorder As String)
        Dim intFile As Integer
        Dim strsql As String
        Dim strIn As String
        Dim strOut As String
        Dim iOutWorkCtr As Integer
        Dim strDeptDivCode As String
        Dim strCurrentDeptDivCode As String
        Dim intPrintLineCounter As Integer, mCount As Integer
        Dim blnDeptAvailable As Boolean
        Dim mField As Object
        'Dim xlapp As Excel.Application
        'Dim xlwb As Excel.Workbook
        'Dim xlst As Excel.Sheets
        Dim rowcnt As Integer
        'xlapp = CreateObject("Excel.Application")
        'xlwb = xlapp.Workbooks.Add

        mCount = 0
        mblnCheckReport = False
        mintPageNo = 1
        mintLine = 1
        If XtraShortOrder.g_SortOrder <> "" Then
            strsortorder = XtraShortOrder.g_SortOrder
        End If
        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If
        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".xls"
        If Common.servername = "Access" Then
            strsql = "select tblCatagory.CatagoryName, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tblOutWorkRegister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tblOutWorkRegister.OutWorkDuration,tblOutWorkRegister.in1,tblOutWorkRegister.in2,tblOutWorkRegister.in3,tblOutWorkRegister.in4,tblOutWorkRegister.in5,tblOutWorkRegister.in6,tblOutWorkRegister.in7,tblOutWorkRegister.in8,tblOutWorkRegister.in9,tblOutWorkRegister.in10,tblOutWorkRegister.out1,tblOutWorkRegister.out2,tblOutWorkRegister.out3,tblOutWorkRegister.out4,tblOutWorkRegister.out5,tblOutWorkRegister.out6,tblOutWorkRegister.out7,tblOutWorkRegister.out8,tblOutWorkRegister.out9,tblOutWorkRegister.out10,tblCalander.Process,tblCalander.NRTCProc, " & _
                     " tblOutWorkRegister.Rin1,tblOutWorkRegister.Rin2,tblOutWorkRegister.Rin3,tblOutWorkRegister.Rin4,tblOutWorkRegister.Rin5,tblOutWorkRegister.Rin6,tblOutWorkRegister.Rin7,tblOutWorkRegister.Rin8,tblOutWorkRegister.Rin9,tblOutWorkRegister.Rin10,tblOutWorkRegister.Rout1,tblOutWorkRegister.Rout2,tblOutWorkRegister.Rout3,tblOutWorkRegister.Rout4,tblOutWorkRegister.Rout5,tblOutWorkRegister.Rout6,tblOutWorkRegister.Rout7,tblOutWorkRegister.Rout8,tblOutWorkRegister.Rout9,tblOutWorkRegister.Rout10 " & _
                     " from tblCatagory,tblOutWorkRegister ,tblEmployee,tblCalander,tblDepartment,tblCompany, tblTimeRegister " & _
                     " Where tblTimeRegister.paycode=tblEmployee.paycode and tblCatagory.Cat = tblEmployee.Cat And tblOutWorkRegister.Paycode = tblTimeRegister.Paycode And tblOutWorkRegister.Paycode = tblEmployee.Paycode And tblOutWorkRegister.In1 Is Not Null And FORMAT(tblOutWorkRegister.DateOffice,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' And FORMAT(tblCalander.mDate,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
                     " and tblCompany.COMPANYCODE=TblEmployee.COMPANYCODE and tblTimeRegister.DateOFFICE = tblOutWorkRegister.DateOFFICE " & _
                     " And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
                     " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & _
                     " And tblOutworkRegister.reason_OutWork = 'N' And tblCompany.CompanyCode = tblEmployee.CompanyCode " & "order by tblEmployee.PresentCARDNO"

            If Common.IsCompliance Then
                strsql = Microsoft.VisualBasic.Strings.Replace(strsql, "tbltimeregister", "tblTimeRegisterD", 1, -1, Microsoft.VisualBasic.Constants.vbTextCompare)
            End If
            adapA = New OleDbDataAdapter(strsql, Common.con1)
            adapA.Fill(Rs_Report)
        Else
            strsql = "select tblCatagory.CatagoryName,tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tblOutWorkRegister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tblOutWorkRegister.OutWorkDuration,tblOutWorkRegister.in1,tblOutWorkRegister.in2,tblOutWorkRegister.in3,tblOutWorkRegister.in4,tblOutWorkRegister.in5,tblOutWorkRegister.in6,tblOutWorkRegister.in7,tblOutWorkRegister.in8,tblOutWorkRegister.in9,tblOutWorkRegister.in10,tblOutWorkRegister.out1,tblOutWorkRegister.out2,tblOutWorkRegister.out3,tblOutWorkRegister.out4,tblOutWorkRegister.out5,tblOutWorkRegister.out6,tblOutWorkRegister.out7,tblOutWorkRegister.out8,tblOutWorkRegister.out9,tblOutWorkRegister.out10,tblCalander.Process,tblCalander.NRTCProc, " & _
                     " tblOutWorkRegister.Rin1,tblOutWorkRegister.Rin2,tblOutWorkRegister.Rin3,tblOutWorkRegister.Rin4,tblOutWorkRegister.Rin5,tblOutWorkRegister.Rin6,tblOutWorkRegister.Rin7,tblOutWorkRegister.Rin8,tblOutWorkRegister.Rin9,tblOutWorkRegister.Rin10,tblOutWorkRegister.Rout1,tblOutWorkRegister.Rout2,tblOutWorkRegister.Rout3,tblOutWorkRegister.Rout4,tblOutWorkRegister.Rout5,tblOutWorkRegister.Rout6,tblOutWorkRegister.Rout7,tblOutWorkRegister.Rout8,tblOutWorkRegister.Rout9,tblOutWorkRegister.Rout10 " & _
                     " from tblCatagory,tblOutWorkRegister ,tblEmployee,tblCalander,tblDepartment,tblCompany, tblTimeRegister " & _
                     " Where tblTimeRegister.paycode=tblEmployee.paycode and tblCatagory.Cat = tblEmployee.Cat And tblOutWorkRegister.Paycode = tblTimeRegister.Paycode And tblOutWorkRegister.Paycode = tblEmployee.Paycode And tblOutWorkRegister.In1 Is Not Null And tblOutWorkRegister.DateOffice = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' And tblCalander.mDate = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
                      " and tblCompany.COMPANYCODE=TblEmployee.COMPANYCODE and tblTimeRegister.DateOFFICE = tblOutWorkRegister.DateOFFICE " & _
                     " And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
                     " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & _
                     " And tblOutworkRegister.reason_OutWork = 'N' And tblCompany.CompanyCode = tblEmployee.CompanyCode " & strsortorder
            If Common.IsCompliance Then
                strsql = Microsoft.VisualBasic.Strings.Replace(strsql, "tbltimeregister", "tblTimeRegisterD", 1, -1, Microsoft.VisualBasic.Constants.vbTextCompare)
            End If
            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(Rs_Report)
        End If
        If Rs_Report.Tables(0).Rows.Count < 1 Then
            XtraMessageBox.Show(ulf, "<size=10>There is no Out Work for any Employee on this Date.<size>", "<size=9>iAS</size>")
            Exit Sub
        End If
        rowcnt = 1
        Common.frodatetodatetoReportGrid = "OUT WORK REPORT FOR DATE : " & ForDate 'DateEdit1.DateTime.ToString("dd/MM/yyyy")
        rowcnt = rowcnt + 1
        rowcnt = rowcnt + 1

        Me.Cursor = Cursors.WaitCursor
        Common.tbl = New Data.DataTable()
        Common.tbl.Columns.Add("Srl", GetType(String))
        Common.tbl.Columns.Add("PayCode", GetType(String))
        Common.tbl.Columns.Add("Card No.", GetType(String))
        Common.tbl.Columns.Add("Employee Name", GetType(String))
        Common.tbl.Columns.Add(" ", GetType(String))
        Common.tbl.Columns.Add("Total OW", GetType(String))
        Common.tbl.Columns.Add(" I         II        III         IV         V         VI         VII         VIII         IX         X", GetType(String))

        rowcnt = rowcnt + 1
        mintLine = 9
        Dim OUTWORKDURATIONStr As String
        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            strIn = ""
            strOut = ""
            For iOutWorkCtr = 1 To 10
                If (Rs_Report.Tables(0).Rows(i).Item("In" & Trim(Str(iOutWorkCtr)))).ToString.Trim <> "" Then
                    strIn = strIn & Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("In" & Trim(Str(iOutWorkCtr))).ToString.Trim).ToString("HH:mm") & " "
                End If
                If (Rs_Report.Tables(0).Rows(i).Item("OUT" & Trim(Str(iOutWorkCtr)))).ToString.Trim <> "" Then
                    strOut = strOut & Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("OUT" & Trim(Str(iOutWorkCtr))).ToString.Trim).ToString("HH:mm") & " "
                End If
            Next iOutWorkCtr
            mCount = mCount + 1
            If Rs_Report.Tables(0).Rows(i).Item("OUTWORKDURATION").ToString.Trim = "0" Then
                OUTWORKDURATIONStr = "0"
            Else
                OUTWORKDURATIONStr = (Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("OUTWORKDURATION").ToString.Trim) / 60)).ToString("00") & ":" & (Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("OUTWORKDURATION").ToString.Trim) Mod 60).ToString("00")
            End If
             Common.tbl.Rows.Add(mCount, Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim, Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim, _
                                Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim, "", OUTWORKDURATIONStr, strIn)
            rowcnt = rowcnt + 1
            rowcnt = rowcnt + 1
            mintLine = mintLine + 2
            mblnCheckReport = True
            Common.tbl.Rows.Add("", "", "", _
                               "", "", "", strOut)
        Next
        Me.Cursor = Cursors.Default
        XtraReportGrid.ShowDialog()
    End Sub
    Sub DailyXl_MissingPunchOrIncorrectShiftOrReverificationGrid(ByVal strsortorder As String)
        Dim adapS As SqlDataAdapter
        Dim adapAc As OleDbDataAdapter
        Dim Rs As DataSet = New DataSet
        Dim sSql As String = " Select * from tblSetUp where setupid =(Select Convert(varchar(10),Max(Convert(int,Setupid))) from tb" & _
        "lsetup )"
        If Common.servername = "Access" Then
            sSql = " Select * from tblSetUp where setupid =(Select CVar(Max(CInt(Setupid))) from tblsetup )"
            adapAc = New OleDbDataAdapter(sSql, Common.con1)
            adapAc.Fill(Rs)
        Else
            adapS = New SqlDataAdapter(sSql, Common.con)
            adapS.Fill(Rs)
        End If
        Dim g_TIME1 As String
        If Rs.Tables(0).Rows(0)("Time1").ToString.Trim = "" Then
            g_TIME1 = 0
        Else
            g_TIME1 = Rs.Tables(0).Rows(0)("Time1").ToString.Trim
        End If
        Dim G_CHECKLATE As String
        If Rs.Tables(0).Rows(0)("CHECKLATE").ToString.Trim = "" Then
            G_CHECKLATE = 0
        Else
            G_CHECKLATE = Rs.Tables(0).Rows(0)("CHECKLATE").ToString.Trim
        End If

        Dim g_CHECKEARLY As String
        If Rs.Tables(0).Rows(0)("CHECKEARLY").ToString.Trim = "" Then
            g_CHECKEARLY = 0
        Else
            g_CHECKEARLY = Rs.Tables(0).Rows(0)("CHECKEARLY").ToString.Trim
        End If
        Dim strDeptDivCode As String
        Dim strCurrentDeptDivCode As String
        Dim intFile As Integer
        Dim strsql As String
        Dim strStatus As String
        Dim lngHoursWorked As Long
        Dim lngLateArrival As Long
        Dim lngEarlyDeparture As Long
        Dim g_DummyHoursWorked As Long
        Dim g_DummyLateArrival As Long
        Dim g_DummyEarlyDeparture As Long
        g_DummyHoursWorked = g_TIME1
        g_DummyLateArrival = G_CHECKLATE
        g_DummyEarlyDeparture = g_CHECKEARLY
        Dim strDepartmentCode As String
        Dim intPrintLineCounter As Integer
        Dim blnDeptAvailable As Boolean
        'Dim xlapp As Excel.Application
        'Dim xlwb As Excel.Workbook
        'Dim xlst As Excel.Sheets
        Dim rowcnt As Integer, mCount As Integer
        'xlapp = CreateObject("Excel.Application")
        'xlwb = xlapp.Workbooks.Add
        mblnCheckReport = False
        mintPageNo = 1
        mintLine = 1
        mCount = 0
        If XtraShortOrder.g_SortOrder <> "" Then
            strsortorder = XtraShortOrder.g_SortOrder
        End If
        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If
        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".xls"

        If Common.servername = "Access" Then
            strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tblTimeRegister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tbltimeregister.ShiftAttended,tbltimeregister.In1,tbltimeregister.Out1,tbltimeregister.In2,tbltimeregister.Out2,tbltimeregister.HoursWorked,tbltimeregister.LateArrival,tbltimeregister.EarlyDeparture,tbltimeregister.Status,tbltimeregister.OtDuration,tblCalander.mDate,tblCalander.Process,tblCalander.NRTCProc" & _
            " from tblCatagory,tblDivision,  tbltimeregister,tblEmployee,tblCalander,tblCompany,tblDepartment" & _
            " Where tblCatagory.Cat = tblEmployee.Cat And tbltimeregister.PayCode = tblEmployee.PayCode And FORMAT(tbltimeregister.DateOffice,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & " And FORMAT(tblCalander.mDate,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
            " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
            " And (tbltimeregister.Status = 'MIS' Or tbltimeregister.HoursWorked >" & g_DummyHoursWorked & " Or tbltimeregister.LateArrival >" & g_DummyLateArrival & " Or tbltimeregister.EarlyDeparture > " & g_DummyEarlyDeparture & ")" & _
            " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & strsortorder
            adapA = New OleDbDataAdapter(strsql, Common.con1)
            adapA.Fill(Rs_Report)
        Else
            strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tblTimeRegister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tbltimeregister.ShiftAttended,tbltimeregister.In1,tbltimeregister.Out1,tbltimeregister.In2,tbltimeregister.Out2,tbltimeregister.HoursWorked,tbltimeregister.LateArrival,tbltimeregister.EarlyDeparture,tbltimeregister.Status,tbltimeregister.OtDuration,tblCalander.mDate,tblCalander.Process,tblCalander.NRTCProc" & _
             " from tblCatagory,tblDivision,  tbltimeregister,tblEmployee,tblCalander,tblCompany,tblDepartment" & _
             " Where tblCatagory.Cat = tblEmployee.Cat And tbltimeregister.PayCode = tblEmployee.PayCode And tbltimeregister.DateOffice = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & " And tblCalander.mDate = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
             " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
             " And (tbltimeregister.Status = 'MIS' Or tbltimeregister.HoursWorked >" & g_DummyHoursWorked & " Or tbltimeregister.LateArrival >" & g_DummyLateArrival & " Or tbltimeregister.EarlyDeparture > " & g_DummyEarlyDeparture & ")" & _
             " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & strsortorder
            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(Rs_Report)
        End If
        If Rs_Report.Tables(0).Rows.Count < 1 Then
            XtraMessageBox.Show(ulf, "<size=10>There is no missing punches or under check parameter punch for any Employee on this Date.</size>", "<size=9>iAS</size>")
            Exit Sub
        End If
        rowcnt = 1
        Common.frodatetodatetoReportGrid = "MISSING PUNCH OR REVERIFICATION REPORT FOR DATE : " & ForDate 'DateEdit1.DateTime.ToString("dd/MM/yyyy")

        Me.Cursor = Cursors.WaitCursor
        Common.tbl = New Data.DataTable()
        Common.tbl.Columns.Add("Srl", GetType(String))
        Common.tbl.Columns.Add("PayCode", GetType(String))
        Common.tbl.Columns.Add("Card No.", GetType(String))
        Common.tbl.Columns.Add("Employee Name", GetType(String))
        Common.tbl.Columns.Add(" ", GetType(String))
        Common.tbl.Columns.Add("Shift", GetType(String))
        Common.tbl.Columns.Add("In", GetType(String))
        Common.tbl.Columns.Add("Lunch Out", GetType(String))
        Common.tbl.Columns.Add("Lunch In", GetType(String))
        Common.tbl.Columns.Add("Out", GetType(String))
        Common.tbl.Columns.Add("Works", GetType(String))
        Common.tbl.Columns.Add("Late", GetType(String))
        Common.tbl.Columns.Add("Early", GetType(String))
        Common.tbl.Columns.Add("Status", GetType(String))
        Common.tbl.Columns.Add("OT", GetType(String))

        rowcnt = rowcnt + 1
        mintLine = 9

        Dim shiftStartTimeStr As String, shiftEndTimeStr As String, LunchLateArrivalStr As String, LUNCHEARLYDEPARTUREStr As String, TotalLossHrsStr As String, IN1MANNUALStr As String
        Dim In1Str As String, In2Str As String, out1str As String, out2Str As String, HOURSWORKEDStr As String, EARLYARRIVALStr As String, latearrivalStr As String
        Dim earlydepartureStr As String, EXCLUNCHHOURSStr As String, OtDurationStr As String

        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            strStatus = IIf(Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim = "", Space(5), Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim & Space(2))
            lngHoursWorked = IIf(Rs_Report.Tables(0).Rows(i).Item("HOURSWORKED").ToString.Trim = "", 0, Rs_Report.Tables(0).Rows(i).Item("HOURSWORKED").ToString.Trim)
            lngLateArrival = IIf(Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim = "", 0, Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim)
            lngEarlyDeparture = IIf(Rs_Report.Tables(0).Rows(i).Item("earlydeparture").ToString.Trim = "", 0, Rs_Report.Tables(0).Rows(i).Item("earlydeparture").ToString.Trim)

            mCount = mCount + 1
            'xlapp.Cells(rowcnt, 2) = "'" & mCount
            'xlapp.Cells(rowcnt, 3) = "'" & Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim
            'xlapp.Cells(rowcnt, 4) = "'" & Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim
            'xlapp.Cells(rowcnt, 5) = Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim
            'xlapp.Cells(rowcnt, 7) = Rs_Report.Tables(0).Rows(i).Item("ShiftAttended").ToString.Trim
            If Rs_Report.Tables(0).Rows(i).Item("In1").ToString.Trim = "" Then
                In1Str = ""
            Else
                In1Str = (Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("In1").ToString.Trim).ToString("HH:mm") & Space(1))
            End If
            If Rs_Report.Tables(0).Rows(i).Item("out1").ToString.Trim = "" Then
                out1str = ""
            Else
                out1str = (Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("out1").ToString.Trim).ToString("HH:mm") & Space(1))
            End If
            If Rs_Report.Tables(0).Rows(i).Item("in2").ToString.Trim = "" Then
                In2Str = ""
            Else
                In2Str = (Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("in2").ToString.Trim).ToString("HH:mm") & Space(1))
            End If
            If Rs_Report.Tables(0).Rows(i).Item("out2").ToString.Trim = "" Then
                out2Str = ""
            Else
                out2Str = (Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("out2").ToString.Trim).ToString("HH:mm") & Space(1))
            End If

            If Rs_Report.Tables(0).Rows(i).Item("HOURSWORKED").ToString.Trim = "0" Then
                HOURSWORKEDStr = "0"
            Else
                HOURSWORKEDStr = Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("HOURSWORKED").ToString.Trim) / 60).ToString("00") & ":" & (Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("HOURSWORKED").ToString.Trim) Mod 60).ToString("00")
            End If
            If Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim = "0" Then
                latearrivalStr = "0"
            Else
                latearrivalStr = Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim) / 60).ToString("00") & ":" & (Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim) Mod 60).ToString("00")
            End If
            If Rs_Report.Tables(0).Rows(i).Item("earlydeparture").ToString.Trim = "0" Then
                earlydepartureStr = "0"
            Else
                earlydepartureStr = Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("earlydeparture").ToString.Trim) / 60).ToString("00") & ":" & (Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("earlydeparture").ToString.Trim) Mod 60).ToString("00")
            End If
            'xlapp.Cells(rowcnt, 15) = Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim
            If Rs_Report.Tables(0).Rows(i).Item("OtDuration").ToString.Trim = "0" Then
                OtDurationStr = "0"
            Else
                OtDurationStr = Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("OtDuration").ToString.Trim) / 60).ToString("00") & ":" & (Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("OtDuration").ToString.Trim) Mod 60).ToString("00")
            End If
            rowcnt = rowcnt + 1
            mintLine = mintLine + 1
            mblnCheckReport = True
            Common.tbl.Rows.Add(mCount, Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim, Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim, _
                                Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim, "", Rs_Report.Tables(0).Rows(i).Item("ShiftAttended").ToString.Trim, _
                                In1Str, out1str, In2Str, out2Str, HOURSWORKEDStr, latearrivalStr, earlydepartureStr, Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim, _
                                OtDurationStr)
        Next
        Me.Cursor = Cursors.Default
        XtraReportGrid.ShowDialog()
    End Sub
    Sub Daily_InOutGrid(ByVal strsortorder As String)
        Dim whereClause As String
        whereClause = g_WhereClause
        If XtraShortOrder.g_SortOrder <> "" Then
            strsortorder = XtraShortOrder.g_SortOrder
        End If

        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If
        Dim strsql As String
        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet
        Dim tmpmstrFile_Name As String = "iAS_" & Now.ToString("yyyyMMddHHmmss") & ".xlsx"
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\" & tmpmstrFile_Name 'Now.ToString("yyyyMMddHHmmss") & ".xlsx"

        mstrDepartmentCode = " "
        mintPageNo = 1
        mintLine = 1
        mblnCheckReport = False

        If Common.servername = "Access" Then
            strsql = " select E.PAYCODE, E.EMPNAME,T.DateOFFICE, T.SHIFT,T.SHIFTATTENDED, T.IN1, T.OUT2, T.STATUS from TblEmployee E, tblTimeRegister T where E.PAYCODE=T.PAYCODE  and Format(T.DateOFFICE, 'yyyy-MM-dd')='" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
                "" & IIf(Len(Trim(whereClause)) = 0, "", " AND " & whereClause.Replace("TBLEmployee", "E").Replace("tblEmployee", "E"))
            adapA = New OleDbDataAdapter(strsql, Common.con1)
            adapA.Fill(Rs_Report)
        Else
            strsql = " select E.PAYCODE, E.EMPNAME,T.DateOFFICE, T.SHIFT,T.SHIFTATTENDED, T.IN1, T.OUT2, T.STATUS from TblEmployee E, tblTimeRegister T where E.PAYCODE=T.PAYCODE  and T.DateOFFICE='" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
                " " & IIf(Len(Trim(whereClause)) = 0, "", " AND " & whereClause.Replace("tblEmployee", "E"))
            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(Rs_Report)
        End If

        If Rs_Report.Tables(0).Rows.Count < 1 Then
            XtraMessageBox.Show(ulf, "<size=10>Nobody Record Found.</size>", "<size=9>iAS</size>")
            Exit Sub
        End If

        Me.Cursor = Cursors.WaitCursor
        Common.tbl = New Data.DataTable()
        Common.tbl.Columns.Add("PayCode", GetType(String))
        Common.tbl.Columns.Add("Employee Name", GetType(String))
        Common.tbl.Columns.Add("Shift", GetType(String))
        Common.tbl.Columns.Add("Total In Time", GetType(String))
        Common.tbl.Columns.Add("Total Out Time", GetType(String))
        Common.tbl.Columns.Add("Status", GetType(String))
        Common.tbl.Columns.Add("In/Out Punches", GetType(String))
        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            Dim Rs_Punches As DataSet = New DataSet
            Dim TmpIN1 As DateTime
            Dim TmpOUT2 As DateTime
            Dim IN1 As String = ""
            Dim OUT2 As String = ""
            Dim totalInTime As Double = 0
            Dim totalOutTime As Double = 0
            If Rs_Report.Tables(0).Rows(i).Item("IN1").ToString.Trim <> "" Then
                IN1 = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("IN1").ToString.Trim).ToString("HH:mm")
                TmpIN1 = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("IN1").ToString.Trim).ToString("yyyy-MM-dd HH:mm:ss")
            End If
            If Rs_Report.Tables(0).Rows(i).Item("OUT2").ToString.Trim <> "" Then
                OUT2 = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("OUT2").ToString.Trim).ToString("HH:mm")
                TmpOUT2 = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("OUT2").ToString.Trim).ToString("yyyy-MM-dd HH:mm:ss")
            Else
                TmpOUT2 = TmpIN1.ToString("yyyy-MM-dd") & " 23:59:59"
            End If
            Dim InOutPunches As String = ""
            If IN1.Trim <> "" Or OUT2.Trim <> "" Then
                If Common.servername = "Access" Then                    
                    strsql = "select * from MachineRawPunch where PAYCODE = '" & Rs_Report.Tables(0).Rows(i).Item("PayCode").ToString.Trim & "' " & _
                    "and Format(OFFICEPUNCH,'yyyy-MM-dd HH:mm:ss') >= '" & TmpIN1.ToString("yyyy-MM-dd HH:mm:00") & "' and Format(OFFICEPUNCH,'yyyy-MM-dd HH:mm:59') <= '" & TmpOUT2.ToString("yyyy-MM-dd HH:mm:59") & "' ORDER by OFFICEPUNCH ASC"
                    adapA = New OleDbDataAdapter(strsql, Common.con1)
                    adapA.Fill(Rs_Punches)
                Else
                    strsql = "select * from MachineRawPunch where PAYCODE = '" & Rs_Report.Tables(0).Rows(i).Item("PayCode").ToString.Trim & "' " & _
                    "and OFFICEPUNCH >= '" & TmpIN1.ToString("yyyy-MM-dd HH:mm:00") & "' and OFFICEPUNCH <= '" & TmpOUT2.ToString("yyyy-MM-dd HH:mm:59") & "' ORDER by OFFICEPUNCH ASC"
                    adap = New SqlDataAdapter(strsql, Common.con)
                    adap.Fill(Rs_Punches)
                End If
               
                If Rs_Punches.Tables(0).Rows.Count > 0 Then
                    For j As Integer = 0 To Rs_Punches.Tables(0).Rows.Count - 1
                        If Rs_Punches.Tables(0).Rows(j).Item("INOUT").ToString.Trim = "I" Then
                            If j < Rs_Punches.Tables(0).Rows.Count - 1 Then
                                totalInTime = totalInTime + Convert.ToDateTime(Rs_Punches.Tables(0).Rows(j + 1).Item("OFFICEPUNCH").ToString.Trim).Subtract(Convert.ToDateTime(Rs_Punches.Tables(0).Rows(j).Item("OFFICEPUNCH").ToString.Trim)).TotalMinutes
                            End If
                            InOutPunches = InOutPunches & "," & Convert.ToDateTime(Rs_Punches.Tables(0).Rows(j).Item("OFFICEPUNCH").ToString.Trim).ToString("HH:mm") & "(I)"
                        ElseIf Rs_Punches.Tables(0).Rows(j).Item("INOUT").ToString.Trim = "O" Then
                            If j < Rs_Punches.Tables(0).Rows.Count - 1 Then
                                totalOutTime = totalOutTime + Convert.ToDateTime(Rs_Punches.Tables(0).Rows(j + 1).Item("OFFICEPUNCH").ToString.Trim).Subtract(Convert.ToDateTime(Rs_Punches.Tables(0).Rows(j).Item("OFFICEPUNCH").ToString.Trim)).TotalMinutes
                            End If
                            InOutPunches = InOutPunches & "," & Convert.ToDateTime(Rs_Punches.Tables(0).Rows(j).Item("OFFICEPUNCH").ToString.Trim).ToString("HH:mm") & "(O)"
                        Else
                            If j Mod 2 = 0 Then
                                If j < Rs_Punches.Tables(0).Rows.Count - 1 Then
                                    totalInTime = totalInTime + Convert.ToDateTime(Rs_Punches.Tables(0).Rows(j + 1).Item("OFFICEPUNCH").ToString.Trim).Subtract(Convert.ToDateTime(Rs_Punches.Tables(0).Rows(j).Item("OFFICEPUNCH").ToString.Trim)).TotalMinutes
                                End If
                                InOutPunches = InOutPunches & "," & Convert.ToDateTime(Rs_Punches.Tables(0).Rows(j).Item("OFFICEPUNCH").ToString.Trim).ToString("HH:mm") & "(I)"
                                'InOutPunches = InOutPunches & "," & Convert.ToDateTime(Rs_Punches.Tables(0).Rows(j).Item("OFFICEPUNCH").ToString.Trim).ToString("HH:mm") & "(O)"
                            Else
                                If j < Rs_Punches.Tables(0).Rows.Count - 1 Then
                                    totalOutTime = totalOutTime + Convert.ToDateTime(Rs_Punches.Tables(0).Rows(j + 1).Item("OFFICEPUNCH").ToString.Trim).Subtract(Convert.ToDateTime(Rs_Punches.Tables(0).Rows(j).Item("OFFICEPUNCH").ToString.Trim)).TotalMinutes
                                End If
                                InOutPunches = InOutPunches & "," & Convert.ToDateTime(Rs_Punches.Tables(0).Rows(j).Item("OFFICEPUNCH").ToString.Trim).ToString("HH:mm") & "(O)"
                                'InOutPunches = InOutPunches & "," & Convert.ToDateTime(Rs_Punches.Tables(0).Rows(j).Item("OFFICEPUNCH").ToString.Trim).ToString("HH:mm") & "(I)"
                            End If
                        End If
                    Next

                    InOutPunches = InOutPunches.TrimStart(",")
                End If
            End If
            Dim totalInTimeText As String = Math.Truncate(totalInTime / 60).ToString("00") & ":" & (totalInTime Mod 60).ToString("00")
            Dim totalOutTimeText As String = Math.Truncate(totalOutTime / 60).ToString("00") & ":" & (totalOutTime Mod 60).ToString("00")
            Dim InOutPunchesArr() As String = InOutPunches.Split(",")
            If InOutPunchesArr.Length <= 4 Then
                Common.tbl.Rows.Add(Rs_Report.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, Rs_Report.Tables(0).Rows(i).Item("EMPNAME").ToString.Trim, Rs_Report.Tables(0).Rows(i).Item("SHIFTATTENDED").ToString.Trim, _
                                totalInTimeText, totalOutTimeText, Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim, InOutPunches)
            Else
                InOutPunches = ""
                Dim m As Integer = 0
                For x As Integer = 0 To InOutPunchesArr.Length - 1
tmp:                If m / 4 < 1 Then
                        InOutPunches = InOutPunches & "," & InOutPunchesArr(x)
                        m = m + 1
                    Else
                        InOutPunches = InOutPunches.TrimStart(",")
                        If x = 4 Then
                            Common.tbl.Rows.Add(Rs_Report.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, Rs_Report.Tables(0).Rows(i).Item("EMPNAME").ToString.Trim, Rs_Report.Tables(0).Rows(i).Item("SHIFTATTENDED").ToString.Trim, _
                               totalInTimeText, totalOutTimeText, Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim, InOutPunches)
                        Else
                            Common.tbl.Rows.Add("", "", "", "", "", "", InOutPunches)
                        End If
                       
                        InOutPunches = ""
                        If x < InOutPunchesArr.Length - 1 Then
                            m = 0
                            'x = x + 1
                            GoTo tmp
                        End If

                    End If                   
                Next
                If InOutPunches <> "" Then
                    InOutPunches = InOutPunches.TrimStart(",")
                    Common.tbl.Rows.Add("", "", "", "", "", "", InOutPunches)
                End If
            End If
        Next

        If CheckPDF.Checked = True Then
            InOutPdf = True
            Common.ReportMail = True
            Common.reportName = tmpmstrFile_Name
            XtraReportGrid.ShowDialog()
            XL.ExcelPDF(mstrFile_Name)
            XtraMasterTest.LabelControlStatus.Text = ""
            Process.Start(mstrFile_Name & ".pdf")
            Common.ReportMail = False
        Else
            XtraReportGrid.ShowDialog()
        End If
        InOutPdf = False
        Me.Cursor = Cursors.Default
    End Sub
    Sub Daily_InOutGrid_InnoliaEnergy(ByVal strsortorder As String)
        Dim whereClause As String
        whereClause = g_WhereClause
        If XtraShortOrder.g_SortOrder <> "" Then
            strsortorder = XtraShortOrder.g_SortOrder
        End If

        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If
        Dim strsql As String
        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet
        Dim tmpmstrFile_Name As String = "iAS_" & Now.ToString("yyyyMMddHHmmss") & ".xlsx"
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\" & tmpmstrFile_Name 'Now.ToString("yyyyMMddHHmmss") & ".xlsx"

        mstrDepartmentCode = " "
        mintPageNo = 1
        mintLine = 1
        mblnCheckReport = False

        If Common.servername = "Access" Then
            strsql = " select E.PAYCODE, E.EMPNAME,T.DateOFFICE, T.SHIFT,T.SHIFTATTENDED, T.IN1, T.OUT2, T.STATUS from TblEmployee E, tblTimeRegister T where E.PAYCODE=T.PAYCODE  and Format(T.DateOFFICE, 'yyyy-MM-dd')='" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
                "" & IIf(Len(Trim(whereClause)) = 0, "", " AND " & whereClause.Replace("TBLEmployee", "E").Replace("tblEmployee", "E"))
            adapA = New OleDbDataAdapter(strsql, Common.con1)
            adapA.Fill(Rs_Report)
        Else
            strsql = " select E.PAYCODE, E.EMPNAME,T.DateOFFICE, T.SHIFT,T.SHIFTATTENDED, T.IN1, T.OUT2, T.STATUS from TblEmployee E, tblTimeRegister T where E.PAYCODE=T.PAYCODE  and T.DateOFFICE='" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
                " " & IIf(Len(Trim(whereClause)) = 0, "", " AND " & whereClause.Replace("tblEmployee", "E"))
            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(Rs_Report)
        End If

        If Rs_Report.Tables(0).Rows.Count < 1 Then
            XtraMessageBox.Show(ulf, "<size=10>Nobody Record Found.</size>", "<size=9>iAS</size>")
            Exit Sub
        End If

        Me.Cursor = Cursors.WaitCursor
        Common.tbl = New Data.DataTable()
        Common.tbl.Columns.Add("PayCode", GetType(String))
        Common.tbl.Columns.Add("Employee Name", GetType(String))
        Common.tbl.Columns.Add("Shift", GetType(String))

        Common.tbl.Columns.Add("PunchIn1", GetType(String))
        Common.tbl.Columns.Add("PunchOut1", GetType(String))
        Common.tbl.Columns.Add("PunchIn2", GetType(String))
        Common.tbl.Columns.Add("PunchOut2", GetType(String))
        Common.tbl.Columns.Add("PunchIn3", GetType(String))
        Common.tbl.Columns.Add("PunchOut3", GetType(String))
        Common.tbl.Columns.Add("PunchIn4", GetType(String))
        Common.tbl.Columns.Add("PunchOut4", GetType(String))
        Common.tbl.Columns.Add("PunchIn5", GetType(String))
        Common.tbl.Columns.Add("PunchOut5", GetType(String))

        Common.tbl.Columns.Add("PunchIn6", GetType(String))
        Common.tbl.Columns.Add("PunchOut6", GetType(String))
        Common.tbl.Columns.Add("PunchIn7", GetType(String))
        Common.tbl.Columns.Add("PunchOut7", GetType(String))
        Common.tbl.Columns.Add("PunchIn8", GetType(String))
        Common.tbl.Columns.Add("PunchOut8", GetType(String))
        Common.tbl.Columns.Add("PunchIn9", GetType(String))
        Common.tbl.Columns.Add("PunchOut9", GetType(String))
        Common.tbl.Columns.Add("PunchIn10", GetType(String))
        Common.tbl.Columns.Add("PunchOut10", GetType(String))


        Common.tbl.Columns.Add("Total In Time", GetType(String))
        Common.tbl.Columns.Add("Total Out Time", GetType(String))
        Common.tbl.Columns.Add("Status", GetType(String))
        'Common.tbl.Columns.Add("In/Out Punches", GetType(String))
        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            Dim Rs_Punches As DataSet = New DataSet
            Dim TmpIN1 As DateTime
            Dim TmpOUT2 As DateTime
            Dim IN1 As String = ""
            Dim OUT2 As String = ""
            Dim totalInTime As Double = 0
            Dim totalOutTime As Double = 0
            If Rs_Report.Tables(0).Rows(i).Item("IN1").ToString.Trim <> "" Then
                IN1 = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("IN1").ToString.Trim).ToString("HH:mm")
                TmpIN1 = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("IN1").ToString.Trim).ToString("yyyy-MM-dd HH:mm:ss")
            End If
            If Rs_Report.Tables(0).Rows(i).Item("OUT2").ToString.Trim <> "" Then
                OUT2 = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("OUT2").ToString.Trim).ToString("HH:mm")
                TmpOUT2 = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("OUT2").ToString.Trim).ToString("yyyy-MM-dd HH:mm:ss")
            Else
                TmpOUT2 = TmpIN1.ToString("yyyy-MM-dd") & " 23:59:59"
            End If
            Dim InOutPunches As String = ""

            Dim PunchIn1 As String = ""
            Dim PunchOut1 As String = ""
            Dim PunchIn2 As String = ""
            Dim PunchOut2 As String = ""
            Dim PunchIn3 As String = ""
            Dim PunchOut3 As String = ""
            Dim PunchIn4 As String = ""
            Dim PunchOut4 As String = ""
            Dim PunchIn5 As String = ""
            Dim PunchOut5 As String = ""

            Dim PunchIn6 As String = ""
            Dim PunchOut6 As String = ""
            Dim PunchIn7 As String = ""
            Dim PunchOut7 As String = ""
            Dim PunchIn8 As String = ""
            Dim PunchOut8 As String = ""
            Dim PunchIn9 As String = ""
            Dim PunchOut9 As String = ""
            Dim PunchIn10 As String = ""
            Dim PunchOut10 As String = ""

            If IN1.Trim <> "" Or OUT2.Trim <> "" Then
                If Common.servername = "Access" Then
                    strsql = "select * from MachineRawPunch where PAYCODE = '" & Rs_Report.Tables(0).Rows(i).Item("PayCode").ToString.Trim & "' " & _
                    "and Format(OFFICEPUNCH,'yyyy-MM-dd HH:mm:ss') >= '" & TmpIN1.ToString("yyyy-MM-dd HH:mm:00") & "' and Format(OFFICEPUNCH,'yyyy-MM-dd HH:mm:59') <= '" & TmpOUT2.ToString("yyyy-MM-dd HH:mm:59") & "' ORDER by OFFICEPUNCH ASC"
                    adapA = New OleDbDataAdapter(strsql, Common.con1)
                    adapA.Fill(Rs_Punches)
                Else
                    strsql = "select * from MachineRawPunch where PAYCODE = '" & Rs_Report.Tables(0).Rows(i).Item("PayCode").ToString.Trim & "' " & _
                    "and OFFICEPUNCH >= '" & TmpIN1.ToString("yyyy-MM-dd HH:mm:00") & "' and OFFICEPUNCH <= '" & TmpOUT2.ToString("yyyy-MM-dd HH:mm:59") & "' ORDER by OFFICEPUNCH ASC"
                    adap = New SqlDataAdapter(strsql, Common.con)
                    adap.Fill(Rs_Punches)
                End If

                If Rs_Punches.Tables(0).Rows.Count > 0 Then
                    For j As Integer = 0 To Rs_Punches.Tables(0).Rows.Count - 1
                        If Rs_Punches.Tables(0).Rows(j).Item("INOUT").ToString.Trim = "I" Then
                            If j < Rs_Punches.Tables(0).Rows.Count - 1 Then
                                totalInTime = totalInTime + Convert.ToDateTime(Rs_Punches.Tables(0).Rows(j + 1).Item("OFFICEPUNCH").ToString.Trim).Subtract(Convert.ToDateTime(Rs_Punches.Tables(0).Rows(j).Item("OFFICEPUNCH").ToString.Trim)).TotalMinutes
                            End If
                            InOutPunches = InOutPunches & "," & Convert.ToDateTime(Rs_Punches.Tables(0).Rows(j).Item("OFFICEPUNCH").ToString.Trim).ToString("HH:mm") & "(I)"
                        ElseIf Rs_Punches.Tables(0).Rows(j).Item("INOUT").ToString.Trim = "O" Then
                            If j < Rs_Punches.Tables(0).Rows.Count - 1 Then
                                totalOutTime = totalOutTime + Convert.ToDateTime(Rs_Punches.Tables(0).Rows(j + 1).Item("OFFICEPUNCH").ToString.Trim).Subtract(Convert.ToDateTime(Rs_Punches.Tables(0).Rows(j).Item("OFFICEPUNCH").ToString.Trim)).TotalMinutes
                            End If
                            InOutPunches = InOutPunches & "," & Convert.ToDateTime(Rs_Punches.Tables(0).Rows(j).Item("OFFICEPUNCH").ToString.Trim).ToString("HH:mm") & "(O)"
                        Else
                            If j Mod 2 = 0 Then
                                If j < Rs_Punches.Tables(0).Rows.Count - 1 Then
                                    totalInTime = totalInTime + Convert.ToDateTime(Rs_Punches.Tables(0).Rows(j + 1).Item("OFFICEPUNCH").ToString.Trim).Subtract(Convert.ToDateTime(Rs_Punches.Tables(0).Rows(j).Item("OFFICEPUNCH").ToString.Trim)).TotalMinutes
                                End If
                                InOutPunches = InOutPunches & "," & Convert.ToDateTime(Rs_Punches.Tables(0).Rows(j).Item("OFFICEPUNCH").ToString.Trim).ToString("HH:mm") & "(I)"
                                'InOutPunches = InOutPunches & "," & Convert.ToDateTime(Rs_Punches.Tables(0).Rows(j).Item("OFFICEPUNCH").ToString.Trim).ToString("HH:mm") & "(O)"
                            Else
                                If j < Rs_Punches.Tables(0).Rows.Count - 1 Then
                                    totalOutTime = totalOutTime + Convert.ToDateTime(Rs_Punches.Tables(0).Rows(j + 1).Item("OFFICEPUNCH").ToString.Trim).Subtract(Convert.ToDateTime(Rs_Punches.Tables(0).Rows(j).Item("OFFICEPUNCH").ToString.Trim)).TotalMinutes
                                End If
                                InOutPunches = InOutPunches & "," & Convert.ToDateTime(Rs_Punches.Tables(0).Rows(j).Item("OFFICEPUNCH").ToString.Trim).ToString("HH:mm") & "(O)"
                                'InOutPunches = InOutPunches & "," & Convert.ToDateTime(Rs_Punches.Tables(0).Rows(j).Item("OFFICEPUNCH").ToString.Trim).ToString("HH:mm") & "(I)"
                            End If
                        End If
                    Next

                    InOutPunches = InOutPunches.TrimStart(",")

                    Try
                        PunchIn1 = Convert.ToDateTime(Rs_Punches.Tables(0).Rows(0).Item("OFFICEPUNCH").ToString.Trim).ToString("HH:mm")                       
                    Catch ex As Exception
                        PunchIn1 = ""
                    End Try

                    Try
                        PunchOut1 = Convert.ToDateTime(Rs_Punches.Tables(0).Rows(1).Item("OFFICEPUNCH").ToString.Trim).ToString("HH:mm")
                    Catch ex As Exception
                        PunchOut1 = ""
                    End Try

                    Try
                        PunchIn2 = Convert.ToDateTime(Rs_Punches.Tables(0).Rows(2).Item("OFFICEPUNCH").ToString.Trim).ToString("HH:mm")
                    Catch ex As Exception
                        PunchIn2 = ""
                    End Try

                    Try
                        PunchOut2 = Convert.ToDateTime(Rs_Punches.Tables(0).Rows(3).Item("OFFICEPUNCH").ToString.Trim).ToString("HH:mm")
                    Catch ex As Exception
                        PunchOut2 = ""
                    End Try

                    Try
                        PunchIn3 = Convert.ToDateTime(Rs_Punches.Tables(0).Rows(4).Item("OFFICEPUNCH").ToString.Trim).ToString("HH:mm")
                    Catch ex As Exception
                        PunchIn3 = ""
                    End Try

                    Try
                        PunchOut3 = Convert.ToDateTime(Rs_Punches.Tables(0).Rows(5).Item("OFFICEPUNCH").ToString.Trim).ToString("HH:mm")
                    Catch ex As Exception
                        PunchOut3 = ""
                    End Try

                    Try
                        PunchIn4 = Convert.ToDateTime(Rs_Punches.Tables(0).Rows(6).Item("OFFICEPUNCH").ToString.Trim).ToString("HH:mm")
                    Catch ex As Exception
                        PunchIn4 = ""
                    End Try

                    Try
                        PunchOut4 = Convert.ToDateTime(Rs_Punches.Tables(0).Rows(7).Item("OFFICEPUNCH").ToString.Trim).ToString("HH:mm")
                    Catch ex As Exception
                        PunchOut4 = ""
                    End Try

                    Try
                        PunchIn5 = Convert.ToDateTime(Rs_Punches.Tables(0).Rows(8).Item("OFFICEPUNCH").ToString.Trim).ToString("HH:mm")
                    Catch ex As Exception
                        PunchIn5 = ""
                    End Try

                    Try
                        PunchOut5 = Convert.ToDateTime(Rs_Punches.Tables(0).Rows(9).Item("OFFICEPUNCH").ToString.Trim).ToString("HH:mm")
                    Catch ex As Exception
                        PunchOut5 = ""
                    End Try

                    Try
                        PunchIn6 = Convert.ToDateTime(Rs_Punches.Tables(0).Rows(10).Item("OFFICEPUNCH").ToString.Trim).ToString("HH:mm")
                    Catch ex As Exception
                        PunchIn6 = ""
                    End Try

                    Try
                        PunchOut6 = Convert.ToDateTime(Rs_Punches.Tables(0).Rows(11).Item("OFFICEPUNCH").ToString.Trim).ToString("HH:mm")
                    Catch ex As Exception
                        PunchOut6 = ""
                    End Try

                    Try
                        PunchIn7 = Convert.ToDateTime(Rs_Punches.Tables(0).Rows(12).Item("OFFICEPUNCH").ToString.Trim).ToString("HH:mm")
                    Catch ex As Exception
                        PunchIn7 = ""
                    End Try

                    Try
                        PunchOut7 = Convert.ToDateTime(Rs_Punches.Tables(0).Rows(13).Item("OFFICEPUNCH").ToString.Trim).ToString("HH:mm")
                    Catch ex As Exception
                        PunchOut7 = ""
                    End Try

                    Try
                        PunchIn8 = Convert.ToDateTime(Rs_Punches.Tables(0).Rows(14).Item("OFFICEPUNCH").ToString.Trim).ToString("HH:mm")
                    Catch ex As Exception
                        PunchIn8 = ""
                    End Try

                    Try
                        PunchOut8 = Convert.ToDateTime(Rs_Punches.Tables(0).Rows(15).Item("OFFICEPUNCH").ToString.Trim).ToString("HH:mm")
                    Catch ex As Exception
                        PunchOut8 = ""
                    End Try

                    Try
                        PunchIn9 = Convert.ToDateTime(Rs_Punches.Tables(0).Rows(16).Item("OFFICEPUNCH").ToString.Trim).ToString("HH:mm")
                    Catch ex As Exception
                        PunchIn9 = ""
                    End Try

                    Try
                        PunchOut9 = Convert.ToDateTime(Rs_Punches.Tables(0).Rows(17).Item("OFFICEPUNCH").ToString.Trim).ToString("HH:mm")
                    Catch ex As Exception
                        PunchOut9 = ""
                    End Try

                    Try
                        PunchIn10 = Convert.ToDateTime(Rs_Punches.Tables(0).Rows(18).Item("OFFICEPUNCH").ToString.Trim).ToString("HH:mm")
                    Catch ex As Exception
                        PunchIn10 = ""
                    End Try

                    Try
                        PunchOut10 = Convert.ToDateTime(Rs_Punches.Tables(0).Rows(19).Item("OFFICEPUNCH").ToString.Trim).ToString("HH:mm")
                    Catch ex As Exception
                        PunchOut10 = ""
                    End Try
                End If
            End If
            Dim totalInTimeText As String = Math.Truncate(totalInTime / 60).ToString("00") & ":" & (totalInTime Mod 60).ToString("00")
            Dim totalOutTimeText As String = Math.Truncate(totalOutTime / 60).ToString("00") & ":" & (totalOutTime Mod 60).ToString("00")
            Dim InOutPunchesArr() As String = InOutPunches.Split(",")
            'If InOutPunchesArr.Length <= 4 Then
            '    Common.tbl.Rows.Add(Rs_Report.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, Rs_Report.Tables(0).Rows(i).Item("EMPNAME").ToString.Trim, Rs_Report.Tables(0).Rows(i).Item("SHIFTATTENDED").ToString.Trim, _
            '                    totalInTimeText, totalOutTimeText, Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim, InOutPunches)

            Common.tbl.Rows.Add(Rs_Report.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, Rs_Report.Tables(0).Rows(i).Item("EMPNAME").ToString.Trim, Rs_Report.Tables(0).Rows(i).Item("SHIFTATTENDED").ToString.Trim,
            PunchIn1, PunchOut1, PunchIn2, PunchOut2, PunchIn3, PunchOut3, PunchIn4, PunchOut4, PunchIn5, PunchOut5,
            PunchIn6, PunchOut6, PunchIn7, PunchOut7, PunchIn8, PunchOut8, PunchIn9, PunchOut9, PunchIn10, PunchOut10,
            totalInTimeText, totalOutTimeText, Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim)

            '            Else
            '            InOutPunches = ""
            '            Dim m As Integer = 0
            '            For x As Integer = 0 To InOutPunchesArr.Length - 1
            'tmp:            If m / 4 < 1 Then
            '                    InOutPunches = InOutPunches & "," & InOutPunchesArr(x)
            '                    m = m + 1
            '                Else
            '                    InOutPunches = InOutPunches.TrimStart(",")
            '                    If x = 4 Then
            '                        Common.tbl.Rows.Add(Rs_Report.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, Rs_Report.Tables(0).Rows(i).Item("EMPNAME").ToString.Trim, Rs_Report.Tables(0).Rows(i).Item("SHIFTATTENDED").ToString.Trim, _
            '                           totalInTimeText, totalOutTimeText, Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim, InOutPunches)
            '                    Else
            '                        Common.tbl.Rows.Add("", "", "", "", "", "", InOutPunches)
            '                    End If

            '                    InOutPunches = ""
            '                    If x < InOutPunchesArr.Length - 1 Then
            '                        m = 0
            '                        'x = x + 1
            '                        GoTo tmp
            '                    End If

            '                End If
            '            Next
            '            If InOutPunches <> "" Then
            '                InOutPunches = InOutPunches.TrimStart(",")
            '                Common.tbl.Rows.Add("", "", "", "", "", "", InOutPunches)
            '            End If
            '            End If
        Next

        If CheckPDF.Checked = True Then
            InOutPdf = True
            Common.ReportMail = True
            Common.reportName = tmpmstrFile_Name
            XtraReportGrid.ShowDialog()
            XL.ExcelPDF(mstrFile_Name)
            XtraMasterTest.LabelControlStatus.Text = ""
            Process.Start(mstrFile_Name & ".pdf")
            Common.ReportMail = False
        Else
            XtraReportGrid.ShowDialog()
        End If
        InOutPdf = False
        Me.Cursor = Cursors.Default
    End Sub
    Sub DailyXl_Performance_Image(ByVal strsortorder As String)
        'On Error GoTo ErrorGen
        Me.Cursor = Cursors.WaitCursor
        XtraMasterTest.LabelControlStatus.Text = "Geting Attendance and Picture Data"
        Dim intFile As Integer
        Dim strsql As String, mCount As Integer
        Dim strOsDuration6 As String ' * 6
        Dim strOtAmount11 As String '* 11
        Dim intPrintLineCounter As Integer
        Dim strIn1Mannual_1 As String '* 1
        Dim strOut1Mannual_1 As String '* 1
        Dim strIn2Mannual_1 As String '* 1
        Dim strOut2Mannual_1 As String '* 1
        Dim blnDeptAvailable As Boolean
        Dim strDeptDivCode As String
        Dim strCurrentDeptDivCode As String
        Dim xlapp As Excel.Application
        Dim xlwb As Excel.Workbook
        Dim xlst As Excel.Sheets
        Dim rowcnt As Integer
        xlapp = CreateObject("Excel.Application")
        xlwb = xlapp.Workbooks.Add
        'Me.Cursor = Cursors.WaitCursor
        If XtraShortOrder.g_SortOrder <> "" Then
            strsortorder = XtraShortOrder.g_SortOrder
        End If
        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If
        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".xls"

        'If Left(strsortorder, 1) = "D" Or Left(strsortorder, 1) = "C" Then blnDeptAvailable = True
        mblnCheckReport = False
        mintPageNo = 1
        mintLine = 1
        'If g_Report_view Then
        If Common.servername = "Access" Then
            strsql = "select tblCatagory.CatagoryName,tblbranch.BRANCHNAME,TblEmployee.EMPPHOTO, tblTimeRegister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tbltimeregister.ShiftAttended,tbltimeregister.ShiftStartTime,tbltimeregister.In1,tbltimeregister.Out1,tbltimeregister.In2,tbltimeregister.Out2,tbltimeregister.HoursWorked,tbltimeregister.Status,tbltimeregister.EarlyArrival," & _
           " tbltimeregister.LateArrival,tbltimeregister.EarlyDeparture,tbltimeregister.ExcLunchHours,tbltimeregister.OtDuration,tbltimeregister.OtAmount,tbltimeregister.OsDuration,tbltimeregister.In1Mannual,tbltimeregister.In2Mannual,tbltimeregister.Out1Mannual,tbltimeregister.Out2Mannual,tblCalander.mDate,tblCalander.Process,tblCalander.NRTCProc" & _
           " from tblCatagory,tbltimeregister,tblEmployee,tblCalander,tblCompany,tblDepartment, tblbranch " & _
           " Where tblbranch.BRANCHCODE=TblEmployee.BRANCHCODE and tblCatagory.Cat = tblEmployee.Cat And tbltimeregister.PayCode = tblEmployee.PayCode And FORMAT(tbltimeregister.DateOffice,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' And FORMAT(tblCalander.mDate,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
           " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode " & g_HODDepartmentCode & _
           " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & strsortorder
            adapA = New OleDbDataAdapter(strsql, Common.con1)
            adapA.Fill(Rs_Report)
        Else
            strsql = "select tblCatagory.CatagoryName,tblbranch.BRANCHNAME, TblEmployee.EMPPHOTO, tblTimeRegister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregister.PayCode,tblEmployee.PresentCardNo,tblEmployee.EmpName,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tbltimeregister.ShiftAttended,tbltimeregister.ShiftStartTime,tbltimeregister.In1,tbltimeregister.Out1,tbltimeregister.In2,tbltimeregister.Out2,tbltimeregister.HoursWorked,tbltimeregister.Status,tbltimeregister.EarlyArrival," & _
             " tbltimeregister.LateArrival,tbltimeregister.EarlyDeparture,tbltimeregister.ExcLunchHours,tbltimeregister.OtDuration,tbltimeregister.OtAmount,tbltimeregister.OsDuration,tbltimeregister.In1Mannual,tbltimeregister.In2Mannual,tbltimeregister.Out1Mannual,tbltimeregister.Out2Mannual,tblCalander.mDate,tblCalander.Process,tblCalander.NRTCProc" & _
             " from tblCatagory,tbltimeregister,tblEmployee,tblCalander,tblCompany,tblDepartment, tblbranch " & _
             " Where tblbranch.BRANCHCODE=TblEmployee.BRANCHCODE and tblCatagory.Cat = tblEmployee.Cat And tbltimeregister.PayCode = tblEmployee.PayCode And tbltimeregister.DateOffice = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' And tblCalander.mDate = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "'" & _
             " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode " & g_HODDepartmentCode & _
             " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & strsortorder
            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(Rs_Report)
        End If
        If Rs_Report.Tables(0).Rows.Count < 1 Then
            Me.Cursor = Cursors.Default
            XtraMasterTest.LabelControlStatus.Text = ""
            XtraMessageBox.Show(ulf, "<size=10>No Data Available For this Report.</size>", "<size=9>iAS</size>")
            Exit Sub
        End If
        If strsortorder = "" Then
            'Rs_Report.Sort = "PayCode"
        Else
            'Rs_Report.Sort = strsortorder
        End If

        rowcnt = 1
        'xlapp.Visible = True
        xlapp.Columns.Font.Name = "TAHOMA"
        xlapp.Cells(rowcnt, 4).Font.Bold = True

        xlapp.Range("D1:H1").Merge()
        xlapp.Cells(rowcnt, 4) = "Company Name:" & CommonReport.g_CompanyNames
        rowcnt = rowcnt + 1

        xlapp.Range("D2:H2").Merge()
        xlapp.Cells(rowcnt, 4).Font.Bold = True
        xlapp.Cells(rowcnt, 4) = "Run Date & Time :" & Now.ToString("dd/MM/yyyy HH:mm")

        rowcnt = rowcnt + 2
        xlapp.Cells.Font.Size = 8
        xlapp.Cells(rowcnt, 4).Font.Bold = True

        xlapp.Range("D4:H4").Merge()
        xlapp.Cells(rowcnt, 4) = "DAILY  REPORT FOR DATE : " & DateEdit1.DateTime.ToString("dd/MM/yyyy")
        rowcnt = rowcnt + 1
        rowcnt = rowcnt + 1
        xlapp.Columns.ColumnWidth = 9
        xlapp.Columns.Font.Name = "TAHOMA"
        xlapp.Columns.Font.Size = 8
        xlapp.Columns.Font.ColorIndex = 1 'vbBlack
        ' xlapp.Cells(rowcnt, 1) = "User Number"
        ' xlapp.Cells.Font.Bold = False
        xlapp.Range("A6:J6").Font.Bold = True
        xlapp.Range("A6:J6").Font.ColorIndex = 2 'vbWhite
        'xlapp.Range("A6:U6").HorizontalAlignment = xlLeft
        xlapp.Range("A6:I6").Interior.ColorIndex = 51
        xlapp.Range("A6:I6").Borders.LineStyle = XlLineStyle.xlContinuous


        xlapp.Cells(rowcnt, 1) = "Sr.No."
        xlapp.Cells(rowcnt, 2) = "PayCode"
        'xlapp.Cells(rowcnt, 3) = "Card No"
        xlapp.Cells(rowcnt, 3) = "Employee Name"
        xlapp.Cells(rowcnt, 4) = "Branch Name"
        xlapp.Cells(rowcnt, 5) = "   In "
        xlapp.Cells(rowcnt, 6) = "   Out "
        xlapp.Cells(rowcnt, 7) = "Hrs Works "
        'xlapp.Cells(rowcnt, 8) = "Status "
        xlapp.Cells(rowcnt, 8) = "Picture "
        'rowcnt = rowcnt + 1
        mintLine = 9
        mCount = 0
        xlapp.Range("H6:I6").Merge()

        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            'Do While Not .EOF
            strOsDuration6 = 0
            strOtAmount11 = 0
            If Rs_Report.Tables(0).Rows(i).Item("OsDuration").ToString.Trim = "" Or Rs_Report.Tables(0).Rows(i).Item("OsDuration").ToString.Trim = "0" Then
            Else
                strOsDuration6 = Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("OsDuration").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("OsDuration").ToString.Trim Mod 60).ToString("00")
            End If
            If Rs_Report.Tables(0).Rows(i).Item("OTAmount").ToString.Trim = "" Or Rs_Report.Tables(0).Rows(i).Item("OTAmount").ToString.Trim = "0" Then
            Else
                strOtAmount11 = Rs_Report.Tables(0).Rows(i).Item("OTAmount").ToString.Trim
            End If

            If Rs_Report.Tables(0).Rows(i).Item("IN1MANNUAL").ToString.Trim = "" Then strIn1Mannual_1 = "" Else strIn1Mannual_1 = Rs_Report.Tables(0).Rows(i).Item("IN1MANNUAL").ToString.Trim
            If Rs_Report.Tables(0).Rows(i).Item("Out1Mannual").ToString.Trim = "" Then strOut1Mannual_1 = "" Else strOut1Mannual_1 = Rs_Report.Tables(0).Rows(i).Item("Out1Mannual").ToString.Trim
            If Rs_Report.Tables(0).Rows(i).Item("IN2MANNUAL").ToString.Trim = "" Then strIn2Mannual_1 = "" Else strIn2Mannual_1 = Rs_Report.Tables(0).Rows(i).Item("IN2MANNUAL").ToString.Trim
            If Rs_Report.Tables(0).Rows(i).Item("OUT2MANNUAL").ToString.Trim = "" Then strOut2Mannual_1 = "" Else strOut2Mannual_1 = Rs_Report.Tables(0).Rows(i).Item("OUT2MANNUAL").ToString.Trim
            Dim X As String
            If UCase(strIn1Mannual_1) = "Y" Or UCase(strOut1Mannual_1) = "Y" Or UCase(strIn2Mannual_1) = "Y" Or UCase(strOut2Mannual_1) = "Y" Then
                X = "Y"
            Else
                X = " "
            End If
            mCount = mCount + 1
            rowcnt = rowcnt + 1
            xlapp.Rows(rowcnt).RowHeight = 80
            xlapp.Cells(rowcnt, 1).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 1) = mCount
            xlapp.Cells(rowcnt, 1).ColumnWidth = 5
            xlapp.Cells(rowcnt, 2).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 2) = "'" & Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim
            'xlapp.Cells(rowcnt, 3).Borders.LineStyle = XlLineStyle.xlContinuous
            'xlapp.Cells(rowcnt, 3) = "'" & Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim
            xlapp.Cells(rowcnt, 3).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 3) = Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim
            xlapp.Cells(rowcnt, 3).Style.WrapText = True
            xlapp.Cells(rowcnt, 3).CurrentRegion.HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter
            xlapp.Cells(rowcnt, 3).CurrentRegion.VerticalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter

            'xlapp.Cells(rowcnt, 5).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 4).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 4) = Rs_Report.Tables(0).Rows(i).Item("BRANCHNAME").ToString.Trim ' Rs_Report.Tables(0).Rows(i).Item("ShiftAttended").ToString.Trim
            xlapp.Cells(rowcnt, 5).Borders.LineStyle = XlLineStyle.xlContinuous
            If Rs_Report.Tables(0).Rows(i).Item("In1").ToString.Trim = "" Then
                xlapp.Cells(rowcnt, 5) = ""
            Else
                xlapp.Cells(rowcnt, 5) = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("In1").ToString.Trim).ToString("HH:mm")
            End If

            xlapp.Cells(rowcnt, 6).Borders.LineStyle = XlLineStyle.xlContinuous
            If Rs_Report.Tables(0).Rows(i).Item("out2").ToString.Trim = "" Then
                xlapp.Cells(rowcnt, 6) = ""
            Else
                xlapp.Cells(rowcnt, 6) = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("out2").ToString.Trim).ToString("HH:mm")
            End If
            'xlapp.Cells(rowcnt, 11) = Format(!out2, "HH:MM")
            xlapp.Cells(rowcnt, 7).Borders.LineStyle = XlLineStyle.xlContinuous
            If Rs_Report.Tables(0).Rows(i).Item("HOURSWORKED").ToString.Trim = "0" Or Rs_Report.Tables(0).Rows(i).Item("HOURSWORKED").ToString.Trim = "" Then
                xlapp.Cells(rowcnt, 7) = ""
            Else
                xlapp.Cells(rowcnt, 7) = "'" & Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("HOURSWORKED").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("HOURSWORKED").ToString.Trim Mod 60).ToString("00")
            End If
            'xlapp.Cells(rowcnt, 8).Borders.LineStyle = XlLineStyle.xlContinuous
            'xlapp.Cells(rowcnt, 8) = Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim

            xlapp.Cells(rowcnt, 8).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Range("H" & rowcnt & ":" & "I" & rowcnt).Merge()
            xlapp.Range("H" & rowcnt & ":" & "I" & rowcnt).Borders.LineStyle = XlLineStyle.xlContinuous
            ''xlapp.Cells(rowcnt, 22) = X
            'xlapp.Cells(rowcnt, 8).ColumnWidth = 3
            'xlapp.Cells(rowcnt, 9).ColumnWidth = 3
            If Rs_Report.Tables(0).Rows(i).Item("EMPPHOTO").ToString.Trim = "" Then
                xlapp.Cells(rowcnt, 8) = ""
            Else
                Dim PicPath As String = My.Application.Info.DirectoryPath & (Rs_Report.Tables(0).Rows(i).Item("EMPPHOTO").ToString.Trim).TrimStart(".").Replace("/", "\")
                With xlapp.ActiveSheet.Pictures.Insert(PicPath)
                    With .ShapeRange
                        '.LockAspectRatio = msoTrue
                        .Width = 100
                        .Height = 78
                    End With
                    .Left = xlapp.ActiveSheet.Cells(rowcnt, 8).Left
                    .Top = xlapp.ActiveSheet.Cells(rowcnt, 8).Top
                    .Placement = 1
                    .PrintObject = True
                End With
            End If
            mblnCheckReport = True
            mintLine = mintLine + 1

            If strsortorder.Contains("Department") Then
                strDeptDivCode = Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim
            ElseIf strsortorder.Contains("Division") Then
                strDeptDivCode = Rs_Report.Tables(0).Rows(i).Item("DivisionCode").ToString.Trim
            Else
                strDeptDivCode = Rs_Report.Tables(0).Rows(i).Item("Cat").ToString.Trim
            End If
        Next
        Me.Cursor = Cursors.Default
        xlwb.SaveAs(mstrFile_Name)
        xlwb.Close()
        XL.ExcelPDF(mstrFile_Name)
        XtraMasterTest.LabelControlStatus.Text = ""
        Process.Start(mstrFile_Name & ".pdf")
        Me.Cursor = Cursors.Default
    End Sub
    Sub DailyXl_MachineRawPunchImage(ByVal strsortorder As String)
        Me.Cursor = Cursors.WaitCursor
        XtraMasterTest.LabelControlStatus.Text = "Geting Punches and Picture Data"

        Dim intFile As Integer
        Dim strsql As String, mCount As Integer
        Dim strOsDuration6 As String ' * 6
        Dim strOtAmount11 As String '* 11
        Dim intPrintLineCounter As Integer
        Dim strIn1Mannual_1 As String '* 1
        Dim strOut1Mannual_1 As String '* 1
        Dim strIn2Mannual_1 As String '* 1
        Dim strOut2Mannual_1 As String '* 1
        Dim blnDeptAvailable As Boolean
        Dim strDeptDivCode As String
        Dim strCurrentDeptDivCode As String
        Dim xlapp As Excel.Application
        Dim xlwb As Excel.Workbook
        Dim xlst As Excel.Sheets
        Dim rowcnt As Integer
        xlapp = CreateObject("Excel.Application")
        xlwb = xlapp.Workbooks.Add
        'Me.Cursor = Cursors.WaitCursor
        If XtraShortOrder.g_SortOrder <> "" Then
            strsortorder = XtraShortOrder.g_SortOrder
        End If
        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If
        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".xls"

        'If Left(strsortorder, 1) = "D" Or Left(strsortorder, 1) = "C" Then blnDeptAvailable = True
        mblnCheckReport = False
        mintPageNo = 1
        mintLine = 1
        'If g_Report_view Then
        If Common.servername = "Access" Then
            strsql = " Select Machinerawpunch.officepunch as mdate,  " & _
                    " tblemployee.PAYCODE,tblemployee.empname, Machinerawpunch.CARDNO " & _
                    "  From MachineRawPunch,tblEmployee " & _
                    " Where OfficePunch between #" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & " 00:00# And " & _
                    " #" & DateEdit2.DateTime.ToString("yyyy-MM-dd") & " 23:59# and " & _
                    " MachineRawPunch.Paycode = tblEmployee.Paycode " & _
                    " " & g_HODDepartmentCode & _
                    " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & "order by tblEmployee.PresentCARDNO,  MachineRawPunch.OFFICEPUNCH"
            adapA = New OleDbDataAdapter(strsql, Common.con1)
            adapA.Fill(Rs_Report)
        Else
            strsql = " Select Machinerawpunch.officepunch as mdate, " & _
                    " tblemployee.PAYCODE,tblemployee.empname, Machinerawpunch.CARDNO " & _
                    "  From MachineRawPunch, tblEmployee " & _
                    " Where OfficePunch Between '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & " 00:00' And " & _
                    " '" & DateEdit2.DateTime.ToString("yyyy-MM-dd") & " 23:59' and " & _
                    " MachineRawPunch.Paycode = tblEmployee.Paycode " & _
                    " " & g_HODDepartmentCode & _
                    " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & "order by tblEmployee.PresentCARDNO,  MachineRawPunch.OFFICEPUNCH"
            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(Rs_Report)
        End If
        If Rs_Report.Tables(0).Rows.Count < 1 Then
            Me.Cursor = Cursors.Default
            XtraMasterTest.LabelControlStatus.Text = ""
            XtraMessageBox.Show(ulf, "<size=10>No Data Available For this Report.</size>", "<size=9>iAS</size>")
            Exit Sub
        End If

        rowcnt = 1
        'xlapp.Visible = True
        xlapp.Columns.Font.Name = "TAHOMA"
        xlapp.Cells(rowcnt, 4).Font.Bold = True

        xlapp.Range("B1:F1").Merge()
        xlapp.Cells(rowcnt, 2) = "Company Name:" & CommonReport.g_CompanyNames
        rowcnt = rowcnt + 1

        xlapp.Range("B2:F2").Merge()
        xlapp.Cells(rowcnt, 2).Font.Bold = True
        xlapp.Cells(rowcnt, 2) = "Run Date & Time :" & Now.ToString("dd/MM/yyyy HH:mm")

        rowcnt = rowcnt + 2
        xlapp.Cells.Font.Size = 8
        xlapp.Cells(rowcnt, 2).Font.Bold = True

        xlapp.Range("B4:F4").Merge()
        xlapp.Cells(rowcnt, 2) = "DAILY PUNCH AND IMAGE REPORT FOR DATE : " & DateEdit1.DateTime.ToString("dd/MM/yyyy")
        rowcnt = rowcnt + 1
        rowcnt = rowcnt + 1
        xlapp.Columns.ColumnWidth = 9
        xlapp.Columns.Font.Name = "TAHOMA"
        xlapp.Columns.Font.Size = 8
        xlapp.Columns.Font.ColorIndex = 1 'vbBlack
        ' xlapp.Cells(rowcnt, 1) = "User Number"
        ' xlapp.Cells.Font.Bold = False
        xlapp.Range("A6:J6").Font.Bold = True
        xlapp.Range("A6:J6").Font.ColorIndex = 2 'vbWhite
        'xlapp.Range("A6:U6").HorizontalAlignment = xlLeft
        xlapp.Range("A6:F6").Interior.ColorIndex = 51
        xlapp.Range("A6:F6").Borders.LineStyle = XlLineStyle.xlContinuous


        xlapp.Cells(rowcnt, 1) = "Sr.No."
        xlapp.Cells(rowcnt, 2) = "PayCode"
        xlapp.Cells(rowcnt, 3) = "Employee Name"
        xlapp.Cells(rowcnt, 4) = "Card No"
        xlapp.Cells(rowcnt, 5) = "Punch Time"
        xlapp.Cells(rowcnt, 6) = "Picture "
        'rowcnt = rowcnt + 1
        mintLine = 9
        mCount = 0
        'xlapp.Range("F6:G6").Merge()

        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            mCount = mCount + 1
            rowcnt = rowcnt + 1
            xlapp.Rows(rowcnt).RowHeight = 80
            xlapp.Cells(rowcnt, 1).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 1) = mCount
            xlapp.Cells(rowcnt, 1).ColumnWidth = 5
            xlapp.Cells(rowcnt, 2).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 2) = "'" & Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim
            xlapp.Cells(rowcnt, 3).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 3) = Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim
            xlapp.Cells(rowcnt, 3).Style.WrapText = True
            xlapp.Cells(rowcnt, 3).CurrentRegion.HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter
            xlapp.Cells(rowcnt, 3).CurrentRegion.VerticalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter

            xlapp.Cells(rowcnt, 4).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 4) = Rs_Report.Tables(0).Rows(i).Item("CARDNO").ToString.Trim

            xlapp.Cells(rowcnt, 5).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 5) = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("mdate").ToString.Trim).ToString("dd-MM-yyyy HH:mm:ss")

            'xlapp.Cells(rowcnt, 6).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 6).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 6).ColumnWidth = 10
            'xlapp.Range("F" & rowcnt & ":" & "G" & rowcnt).Merge()
            'xlapp.Range("F" & rowcnt & ":" & "G" & rowcnt).Borders.LineStyle = XlLineStyle.xlContinuous
            Try
                Dim cardNo As Double = Convert.ToDouble(Rs_Report.Tables(0).Rows(i).Item("CARDNO").ToString.Trim)
                Dim PicPath As String = My.Application.Info.DirectoryPath & "\RealTimeImages\" & Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("mdate").ToString.Trim).ToString("yyyyMMddHHmmss") & "-" & cardNo & ".jpg"
                With xlapp.ActiveSheet.Pictures.Insert(PicPath)
                    With .ShapeRange
                        '.LockAspectRatio = msoTrue
                        .Width = 110
                        .Height = 75
                    End With
                    .Left = xlapp.ActiveSheet.Cells(rowcnt, 6).Left
                    .Top = xlapp.ActiveSheet.Cells(rowcnt, 6).Top
                    .Placement = 1
                    .PrintObject = True
                End With
            Catch ex As Exception
                xlapp.Cells(rowcnt, 6) = ""
            End Try
            mblnCheckReport = True
            mintLine = mintLine + 1
        Next

        Me.Cursor = Cursors.Default
        xlwb.SaveAs(mstrFile_Name)
        xlwb.Close()
        XL.ExcelPDF(mstrFile_Name)
        XtraMasterTest.LabelControlStatus.Text = ""
        Process.Start(mstrFile_Name & ".pdf")
        Me.Cursor = Cursors.Default
    End Sub
    Sub DailyXL_CustomisedGrid()
        Dim sSql As String
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim ds As DataSet = New DataSet
        Dim strsortorder As String = ""
        If XtraShortOrder.g_SortOrder <> "" Then
            strsortorder = XtraShortOrder.g_SortOrder
        End If
        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If
        sSql = "select * from CustomisedReport"
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(ds)
        End If
        If ds.Tables(0).Rows.Count = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>Please select Customised Columns.<size>", "<size=9>iAS</size>")
            Exit Sub
        Else
            sSql = "select * from TblEmployee, tblTimeRegister, tblDepartment, tblCatagory, tblbranch, tblGrade, EmployeeGroup, tblCompany " &
            "where TblEmployee.PAYCODE = tblTimeRegister.PAYCODE and TblEmployee.DEPARTMENTCODE=tblDepartment.DEPARTMENTCODE and  " &
            "TblEmployee.CAT = tblCatagory.CAT and TblEmployee.BRANCHCODE= tblbranch.BRANCHCODE and TblEmployee.GradeCode=tblGrade.GradeCode AND  " &
            "TblEmployee.EmployeeGroupId=EmployeeGroup.GroupId and TblEmployee.COMPANYCODE=tblCompany.COMPANYCODE and  " &
            "tblTimeRegister.DateOFFICE >= '" & DateEdit1.DateTime.ToString("yyyy-MM-dd 00:00:00") & "' and tblTimeRegister.DateOFFICE <='" & DateEdit1.DateTime.ToString("yyyy-MM-dd 23:59:59") & "' " &
            " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & strsortorder

            Dim Rs_Report As DataSet = New DataSet
            If Common.servername = "Access" Then
                sSql = sSql.Replace("tblTimeRegister.DateOFFICE", "FORMAT(tblTimeRegister.DateOFFICE,'yyyy-MM-dd HH:mm:ss')")
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(Rs_Report)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(Rs_Report)
            End If
            If Rs_Report.Tables(0).Rows.Count < 1 Then
                XtraMessageBox.Show(ulf, "<size=10>No Data Available For this Report.</size>", "<size=9>iAS</size>")
                Exit Sub
            End If
            Me.Cursor = Cursors.WaitCursor
            Common.frodatetodatetoReportGrid = " CUSTOMISED REPORT FOR DATE : " & ForDate 'DateEdit1.DateTime.ToString("dd/MM/yyyy")
            Common.tbl = New Data.DataTable()
            If ds.Tables(0).Rows(0).Item("Paycode").ToString.Trim = "Y" Then
                Common.tbl.Columns.Add("Paycode")
            End If
            If ds.Tables(0).Rows(0).Item("Name").ToString.Trim = "Y" Then
                Common.tbl.Columns.Add("Name")
            End If
            If ds.Tables(0).Rows(0).Item("CardNumber").ToString.Trim = "Y" Then
                Common.tbl.Columns.Add("User Number")
            End If
            If ds.Tables(0).Rows(0).Item("Company").ToString.Trim = "Y" Then
                Common.tbl.Columns.Add("Company")
            End If
            If ds.Tables(0).Rows(0).Item("Location").ToString.Trim = "Y" Then
                Common.tbl.Columns.Add("Location")
            End If
            If ds.Tables(0).Rows(0).Item("Department").ToString.Trim = "Y" Then
                Common.tbl.Columns.Add("Department")
            End If
            If ds.Tables(0).Rows(0).Item("Grade").ToString.Trim = "Y" Then
                Common.tbl.Columns.Add("Grade")
            End If
            If ds.Tables(0).Rows(0).Item("Catagory").ToString.Trim = "Y" Then
                Common.tbl.Columns.Add("Catagory")
            End If
            If ds.Tables(0).Rows(0).Item("EmpGrp").ToString.Trim = "Y" Then
                Common.tbl.Columns.Add("Employee Group")
            End If

            If ds.Tables(0).Rows(0).Item("SHIFTATTENDED").ToString.Trim = "Y" Then
                Common.tbl.Columns.Add("Shift Attended")
            End If
            If ds.Tables(0).Rows(0).Item("SHIFTSTARTTIME").ToString.Trim = "Y" Then
                Common.tbl.Columns.Add("Shift StartTime")
            End If
            If ds.Tables(0).Rows(0).Item("SHIFTENDTIME").ToString.Trim = "Y" Then
                Common.tbl.Columns.Add("Shift EndTime")
            End If
            If ds.Tables(0).Rows(0).Item("IN1").ToString.Trim = "Y" Then
                Common.tbl.Columns.Add("In Time")
            End If
            If ds.Tables(0).Rows(0).Item("OUT1").ToString.Trim = "Y" Then
                Common.tbl.Columns.Add("Lunch Out")
            End If
            If ds.Tables(0).Rows(0).Item("IN2").ToString.Trim = "Y" Then
                Common.tbl.Columns.Add("Lunch In")
            End If
            If ds.Tables(0).Rows(0).Item("OUT2").ToString.Trim = "Y" Then
                Common.tbl.Columns.Add("Out Time")
            End If
            If ds.Tables(0).Rows(0).Item("STATUS").ToString.Trim = "Y" Then
                Common.tbl.Columns.Add("Status")
            End If
            If ds.Tables(0).Rows(0).Item("HOURSWORKED").ToString.Trim = "Y" Then
                Common.tbl.Columns.Add("Hours Worked")
            End If
            If ds.Tables(0).Rows(0).Item("LATEARRIVAL").ToString.Trim = "Y" Then
                Common.tbl.Columns.Add("Late Arrival")
            End If
            If ds.Tables(0).Rows(0).Item("EARLYDEPARTURE").ToString.Trim = "Y" Then
                Common.tbl.Columns.Add("Early Departure")
            End If
            If ds.Tables(0).Rows(0).Item("OTDURATION").ToString.Trim = "Y" Then
                Common.tbl.Columns.Add("OT Duration")
            End If
            If ds.Tables(0).Rows(0).Item("OSDURATION").ToString.Trim = "Y" Then
                Common.tbl.Columns.Add("OS Duration")
            End If

            For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
                Common.tbl.Rows.Add()
                'Common.tbl.DefaultView(i)("Paycode") = "123"
                'Common.tbl.DefaultView(i)(1) = "Name"

                If ds.Tables(0).Rows(0).Item("Paycode").ToString.Trim = "Y" Then
                    'Common.tbl.Columns.Add("Paycode")
                    If Common.servername = "Access" Then
                        Common.tbl.DefaultView(i)("Paycode") = Rs_Report.Tables(0).Rows(i).Item("TblEmployee.PAYCODE").ToString.Trim
                    Else
                        Common.tbl.DefaultView(i)("Paycode") = Rs_Report.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim
                    End If

                End If
                If ds.Tables(0).Rows(0).Item("Name").ToString.Trim = "Y" Then
                    'Common.tbl.Columns.Add("Name")
                    Common.tbl.DefaultView(i)("Name") = Rs_Report.Tables(0).Rows(i).Item("EMPNAME").ToString.Trim
                End If
                If ds.Tables(0).Rows(0).Item("CardNumber").ToString.Trim = "Y" Then
                    'Common.tbl.Columns.Add("User Number")
                    Common.tbl.DefaultView(i)("User Number") = Rs_Report.Tables(0).Rows(i).Item("PRESENTCARDNO").ToString.Trim
                End If
                If ds.Tables(0).Rows(0).Item("Company").ToString.Trim = "Y" Then
                    'Common.tbl.Columns.Add("Company")
                    Common.tbl.DefaultView(i)("Company") = Rs_Report.Tables(0).Rows(i).Item("COMPANYNAME").ToString.Trim
                End If
                If ds.Tables(0).Rows(0).Item("Location").ToString.Trim = "Y" Then
                    'Common.tbl.Columns.Add("Location")
                    Common.tbl.DefaultView(i)("Location") = Rs_Report.Tables(0).Rows(i).Item("BRANCHNAME").ToString.Trim
                End If
                If ds.Tables(0).Rows(0).Item("Department").ToString.Trim = "Y" Then
                    'Common.tbl.Columns.Add("Department")
                    Common.tbl.DefaultView(i)("Department") = Rs_Report.Tables(0).Rows(i).Item("DEPARTMENTNAME").ToString.Trim
                End If
                If ds.Tables(0).Rows(0).Item("Grade").ToString.Trim = "Y" Then
                    'Common.tbl.Columns.Add("Grade")
                    Common.tbl.DefaultView(i)("Grade") = Rs_Report.Tables(0).Rows(i).Item("GradeName").ToString.Trim
                End If
                If ds.Tables(0).Rows(0).Item("Catagory").ToString.Trim = "Y" Then
                    'Common.tbl.Columns.Add("Catagory")
                    Common.tbl.DefaultView(i)("Catagory") = Rs_Report.Tables(0).Rows(i).Item("CATAGORYNAME").ToString.Trim
                End If
                If ds.Tables(0).Rows(0).Item("EmpGrp").ToString.Trim = "Y" Then
                    'Common.tbl.Columns.Add("Employee Group")
                    Common.tbl.DefaultView(i)("Employee Group") = Rs_Report.Tables(0).Rows(i).Item("GroupName").ToString.Trim
                End If

                If ds.Tables(0).Rows(0).Item("SHIFTATTENDED").ToString.Trim = "Y" Then
                    'Common.tbl.Columns.Add("Shift Attended")
                    Common.tbl.DefaultView(i)("Shift Attended") = Rs_Report.Tables(0).Rows(i).Item("SHIFTATTENDED").ToString.Trim
                End If

                Dim TTIMETmp As DateTime
                If ds.Tables(0).Rows(0).Item("SHIFTSTARTTIME").ToString.Trim = "Y" Then
                    'Common.tbl.Columns.Add("Shift StartTime")
                    'Common.tbl.DefaultView(i)("Shift StartTime") = Rs_Report.Tables(0).Rows(i).Item("SHIFTSTARTTIME").ToString.Trim
                    Try
                        TTIMETmp = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("SHIFTSTARTTIME").ToString.Trim)
                        Common.tbl.DefaultView(i)("Shift StartTime") = TTIMETmp.ToString("HH:mm")
                    Catch ex As Exception
                        Common.tbl.DefaultView(i)("Shift StartTime") = ""
                    End Try
                End If
                If ds.Tables(0).Rows(0).Item("SHIFTENDTIME").ToString.Trim = "Y" Then
                    'Common.tbl.Columns.Add("Shift EndTime")
                    'Common.tbl.DefaultView(i)("Shift EndTime") = Rs_Report.Tables(0).Rows(i).Item("SHIFTENDTIME").ToString.Trim
                    Try
                        TTIMETmp = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("SHIFTENDTIME").ToString.Trim)
                        Common.tbl.DefaultView(i)("Shift EndTime") = TTIMETmp.ToString("HH:mm")
                    Catch ex As Exception
                        Common.tbl.DefaultView(i)("Shift EndTime") = ""
                    End Try
                End If
                If ds.Tables(0).Rows(0).Item("IN1").ToString.Trim = "Y" Then
                    'Common.tbl.Columns.Add("In Time")
                    'Common.tbl.DefaultView(i)("In Time") = Rs_Report.Tables(0).Rows(i).Item("IN1").ToString.Trim
                    Try
                        TTIMETmp = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("IN1").ToString.Trim)
                        Common.tbl.DefaultView(i)("In Time") = TTIMETmp.ToString("HH:mm")
                    Catch ex As Exception
                        Common.tbl.DefaultView(i)("In Time") = ""
                    End Try
                End If
                If ds.Tables(0).Rows(0).Item("OUT1").ToString.Trim = "Y" Then
                    'Common.tbl.Columns.Add("Lunch Out")
                    'Common.tbl.DefaultView(i)("Lunch Out") = Rs_Report.Tables(0).Rows(i).Item("OUT1").ToString.Trim
                    Try
                        TTIMETmp = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("OUT1").ToString.Trim)
                        Common.tbl.DefaultView(i)("Lunch Out") = TTIMETmp.ToString("HH:mm")
                    Catch ex As Exception
                        Common.tbl.DefaultView(i)("Lunch Out") = ""
                    End Try
                End If
                If ds.Tables(0).Rows(0).Item("IN2").ToString.Trim = "Y" Then
                    'Common.tbl.Columns.Add("Lunch In")
                    'Common.tbl.DefaultView(i)("Lunch In") = Rs_Report.Tables(0).Rows(i).Item("IN2").ToString.Trim
                    Try
                        TTIMETmp = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("IN2").ToString.Trim)
                        Common.tbl.DefaultView(i)("Lunch In") = TTIMETmp.ToString("HH:mm")
                    Catch ex As Exception
                        Common.tbl.DefaultView(i)("Lunch In") = ""
                    End Try
                End If
                If ds.Tables(0).Rows(0).Item("OUT2").ToString.Trim = "Y" Then
                    'Common.tbl.Columns.Add("Out Time")
                    'Common.tbl.DefaultView(i)("Out Time") = Rs_Report.Tables(0).Rows(i).Item("OUT2").ToString.Trim
                    Try
                        TTIMETmp = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("OUT2").ToString.Trim)
                        Common.tbl.DefaultView(i)("Out Time") = TTIMETmp.ToString("HH:mm")
                    Catch ex As Exception
                        Common.tbl.DefaultView(i)("Out Time") = ""
                    End Try
                End If
                If ds.Tables(0).Rows(0).Item("STATUS").ToString.Trim = "Y" Then
                    'Common.tbl.Columns.Add("Status")
                    Common.tbl.DefaultView(i)("Status") = Rs_Report.Tables(0).Rows(i).Item("STATUS").ToString.Trim
                End If
                If ds.Tables(0).Rows(0).Item("HOURSWORKED").ToString.Trim = "Y" Then
                    'Common.tbl.Columns.Add("Hours Worked")
                    If Rs_Report.Tables(0).Rows(i).Item("HOURSWORKED").ToString.Trim = "" Or Rs_Report.Tables(0).Rows(i).Item("HOURSWORKED").ToString.Trim = "0" Then
                        Common.tbl.DefaultView(i)("Hours Worked") = ""
                    Else
                        Common.tbl.DefaultView(i)("Hours Worked") = (Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("HOURSWORKED").ToString.Trim) / 60)).ToString("00") & ":" & (Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("HOURSWORKED").ToString.Trim) Mod 60).ToString("00") ' Rs_Report.Tables(0).Rows(i).Item("HOURSWORKED").ToString.Trim
                    End If

                End If
                If ds.Tables(0).Rows(0).Item("LATEARRIVAL").ToString.Trim = "Y" Then
                    'Common.tbl.Columns.Add("Late Arrival")
                    'Common.tbl.DefaultView(i)("Late Arrival") = Rs_Report.Tables(0).Rows(i).Item("LATEARRIVAL").ToString.Trim
                    If Rs_Report.Tables(0).Rows(i).Item("LATEARRIVAL").ToString.Trim = "" Or Rs_Report.Tables(0).Rows(i).Item("LATEARRIVAL").ToString.Trim = "0" Then
                        Common.tbl.DefaultView(i)("Late Arrival") = ""
                    Else
                        Common.tbl.DefaultView(i)("Late Arrival") = (Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("LATEARRIVAL").ToString.Trim) / 60)).ToString("00") & ":" & (Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("LATEARRIVAL").ToString.Trim) Mod 60).ToString("00")
                    End If
                End If
                If ds.Tables(0).Rows(0).Item("EARLYDEPARTURE").ToString.Trim = "Y" Then
                    'Common.tbl.Columns.Add("Early Departure")
                    'Common.tbl.DefaultView(i)("Early Departure") = Rs_Report.Tables(0).Rows(i).Item("EARLYDEPARTURE").ToString.Trim
                    If Rs_Report.Tables(0).Rows(i).Item("EARLYDEPARTURE").ToString.Trim = "" Or Rs_Report.Tables(0).Rows(i).Item("EARLYDEPARTURE").ToString.Trim = "0" Then
                        Common.tbl.DefaultView(i)("Early Departure") = ""
                    Else
                        Common.tbl.DefaultView(i)("Early Departure") = (Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("EARLYDEPARTURE").ToString.Trim) / 60)).ToString("00") & ":" & (Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("EARLYDEPARTURE").ToString.Trim) Mod 60).ToString("00")
                    End If
                End If
                If ds.Tables(0).Rows(0).Item("OTDURATION").ToString.Trim = "Y" Then
                    'Common.tbl.Columns.Add("OT Duration")
                    'Common.tbl.DefaultView(i)("OT Duration") = Rs_Report.Tables(0).Rows(i).Item("OTDURATION").ToString.Trim
                    If Rs_Report.Tables(0).Rows(i).Item("OTDURATION").ToString.Trim = "" Or Rs_Report.Tables(0).Rows(i).Item("OTDURATION").ToString.Trim = "0" Then
                        Common.tbl.DefaultView(i)("OT Duration") = ""
                    Else
                        Common.tbl.DefaultView(i)("OT Duration") = (Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("OTDURATION").ToString.Trim) / 60)).ToString("00") & ":" & (Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("OTDURATION").ToString.Trim) Mod 60).ToString("00")
                    End If
                End If
                If ds.Tables(0).Rows(0).Item("OSDURATION").ToString.Trim = "Y" Then
                    'Common.tbl.Columns.Add("OS Duration")
                    'Common.tbl.DefaultView(i)("OS Duration") = Rs_Report.Tables(0).Rows(i).Item("OSDURATION").ToString.Trim
                    If Rs_Report.Tables(0).Rows(i).Item("OSDURATION").ToString.Trim = "" Or Rs_Report.Tables(0).Rows(i).Item("OSDURATION").ToString.Trim = "0" Then
                        Common.tbl.DefaultView(i)("OS Duration") = ""
                    Else
                        Common.tbl.DefaultView(i)("OS Duration") = (Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("OSDURATION").ToString.Trim) / 60)).ToString("00") & ":" & (Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("OSDURATION").ToString.Trim) Mod 60).ToString("00")
                    End If
                End If
            Next
            Me.Cursor = Cursors.Default
            XtraReportGrid.ShowDialog()
        End If
    End Sub
    Public Structure CusrReport
        Dim column As String
        Dim caption As String
        Dim index As Integer
    End Structure
    Sub DailyXL_CustomisedGridNava()
        Dim CusrReportArr(26) As CusrReport
        'Dim EmpGrpCusrReportList As New List(Of CusrReport)

        Dim sSql As String
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim ds As DataSet = New DataSet
        Dim strsortorder As String = ""
        If XtraShortOrder.g_SortOrder <> "" Then
            strsortorder = XtraShortOrder.g_SortOrder
        End If
        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If

        sSql = "select * from CustomisedReport"
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(ds)
        End If
        If ds.Tables(0).Rows.Count = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>Please select Customised Columns.<size>", "<size=9>iAS</size>")
            Exit Sub
        Else
            Dim isTemperature As String = ds.Tables(0).Rows(0).Item("Temperature").ToString.Trim
            Dim PreIsPrifix As String = ds.Tables(0).Rows(0).Item("PreIsPrifix").ToString.Trim
            Dim PreLength As String = ds.Tables(0).Rows(0).Item("PreLength").ToString.Trim
            Dim PreText As String = ds.Tables(0).Rows(0).Item("PreText").ToString.Trim
            If PreIsPrifix = "N" Then
                PreLength = ""
                PreText = ""
            End If
            Dim PayIsPrefix As String = ds.Tables(0).Rows(0).Item("PayIsPrefix").ToString.Trim
            Dim PayLength As String = ds.Tables(0).Rows(0).Item("PayLength").ToString.Trim
            Dim PayText As String = ds.Tables(0).Rows(0).Item("PayText").ToString.Trim
            If PayIsPrefix = "N" Then
                PayLength = ""
                PreText = ""
            End If
            Dim DateFormat As String = "dd/MM/yyyy"
            If ds.Tables(0).Rows(0).Item("DateFormat").ToString.Trim() <> "" Then
                DateFormat = ds.Tables(0).Rows(0).Item("DateFormat").ToString.Trim()
            End If
            Dim indextmp As Integer = 0
            If ds.Tables(0).Rows(0).Item("Paycode").ToString.Trim = "Y" Then
                If ds.Tables(0).Rows(0).Item("PaycodeCap").ToString.Trim = "" Then
                    CusrReportArr(ds.Tables(0).Rows(0).Item("PaycodePos") - 1).caption = "Paycode"
                Else
                    CusrReportArr(ds.Tables(0).Rows(0).Item("PaycodePos") - 1).caption = ds.Tables(0).Rows(0).Item("PaycodeCap").ToString.Trim
                End If
                CusrReportArr(ds.Tables(0).Rows(0).Item("PaycodePos") - 1).column = "TblEmployee.Paycode"
                CusrReportArr(ds.Tables(0).Rows(0).Item("PaycodePos") - 1).index = ds.Tables(0).Rows(0).Item("PaycodePos") - 1
                indextmp = indextmp + 1
            End If
            If ds.Tables(0).Rows(0).Item("Name").ToString.Trim = "Y" Then
                If ds.Tables(0).Rows(0).Item("NameCap").ToString.Trim = "" Then
                    CusrReportArr(ds.Tables(0).Rows(0).Item("NamePos") - 1).caption = "Name"
                Else
                    CusrReportArr(ds.Tables(0).Rows(0).Item("NamePos") - 1).caption = ds.Tables(0).Rows(0).Item("NameCap").ToString.Trim
                End If
                CusrReportArr(ds.Tables(0).Rows(0).Item("NamePos") - 1).column = "TblEmployee.EMPNAME"
                CusrReportArr(ds.Tables(0).Rows(0).Item("NamePos") - 1).index = ds.Tables(0).Rows(0).Item("NamePos") - 1
                indextmp = indextmp + 1
            End If
            If ds.Tables(0).Rows(0).Item("CardNumber").ToString.Trim = "Y" Then
                If ds.Tables(0).Rows(0).Item("CardNumberCap").ToString.Trim = "" Then
                    CusrReportArr(ds.Tables(0).Rows(0).Item("CardNumberPos") - 1).caption = "User Number"
                Else
                    CusrReportArr(ds.Tables(0).Rows(0).Item("CardNumberPos") - 1).caption = ds.Tables(0).Rows(0).Item("CardNumberCap").ToString.Trim
                End If
                CusrReportArr(ds.Tables(0).Rows(0).Item("CardNumberPos") - 1).column = "TblEmployee.PRESENTCARDNO"
                CusrReportArr(ds.Tables(0).Rows(0).Item("CardNumberPos") - 1).index = ds.Tables(0).Rows(0).Item("CardNumberPos") - 1
                indextmp = indextmp + 1
            End If
            If ds.Tables(0).Rows(0).Item("Company").ToString.Trim = "Y" Then
                If ds.Tables(0).Rows(0).Item("CompanyCap").ToString.Trim = "" Then
                    CusrReportArr(ds.Tables(0).Rows(0).Item("CompanyPos") - 1).caption = "Company"
                Else
                    CusrReportArr(ds.Tables(0).Rows(0).Item("CompanyPos") - 1).caption = ds.Tables(0).Rows(0).Item("CompanyCap").ToString.Trim
                End If
                CusrReportArr(ds.Tables(0).Rows(0).Item("CompanyPos") - 1).column = "tblCompany.COMPANYNAME"
                CusrReportArr(ds.Tables(0).Rows(0).Item("CompanyPos") - 1).index = ds.Tables(0).Rows(0).Item("CompanyPos") - 1
                indextmp = indextmp + 1
            End If
            If ds.Tables(0).Rows(0).Item("Location").ToString.Trim = "Y" Then
                If ds.Tables(0).Rows(0).Item("LocationCap").ToString.Trim = "" Then
                    CusrReportArr(ds.Tables(0).Rows(0).Item("LocationPos") - 1).caption = "Location"
                Else
                    CusrReportArr(ds.Tables(0).Rows(0).Item("LocationPos") - 1).caption = ds.Tables(0).Rows(0).Item("LocationCap").ToString.Trim
                End If
                CusrReportArr(ds.Tables(0).Rows(0).Item("LocationPos") - 1).column = "tblbranch.BRANCHNAME"
                CusrReportArr(ds.Tables(0).Rows(0).Item("LocationPos") - 1).index = ds.Tables(0).Rows(0).Item("LocationPos") - 1
                indextmp = indextmp + 1
            End If
            If ds.Tables(0).Rows(0).Item("Department").ToString.Trim = "Y" Then
                If ds.Tables(0).Rows(0).Item("DepartmentCap").ToString.Trim = "" Then
                    CusrReportArr(ds.Tables(0).Rows(0).Item("DepartmentPos") - 1).caption = "Department"
                Else
                    CusrReportArr(ds.Tables(0).Rows(0).Item("DepartmentPos") - 1).caption = ds.Tables(0).Rows(0).Item("DepartmentCap").ToString.Trim
                End If
                CusrReportArr(ds.Tables(0).Rows(0).Item("DepartmentPos") - 1).column = "tblDepartment.DEPARTMENTNAME"
                CusrReportArr(ds.Tables(0).Rows(0).Item("DepartmentPos") - 1).index = ds.Tables(0).Rows(0).Item("DepartmentPos") - 1
                indextmp = indextmp + 1
            End If
            If ds.Tables(0).Rows(0).Item("Grade").ToString.Trim = "Y" Then
                If ds.Tables(0).Rows(0).Item("GradeCap").ToString.Trim = "" Then
                    CusrReportArr(ds.Tables(0).Rows(0).Item("GradePos") - 1).caption = "Grade"
                Else
                    CusrReportArr(ds.Tables(0).Rows(0).Item("GradePos") - 1).caption = ds.Tables(0).Rows(0).Item("GradeCap").ToString.Trim
                End If
                CusrReportArr(ds.Tables(0).Rows(0).Item("GradePos") - 1).column = "tblGrade.GradeName"
                CusrReportArr(ds.Tables(0).Rows(0).Item("GradePos") - 1).index = ds.Tables(0).Rows(0).Item("GradePos") - 1
                indextmp = indextmp + 1
            End If
            If ds.Tables(0).Rows(0).Item("Catagory").ToString.Trim = "Y" Then
                If ds.Tables(0).Rows(0).Item("CatagoryCap").ToString.Trim = "" Then
                    CusrReportArr(ds.Tables(0).Rows(0).Item("CatagoryPos") - 1).caption = "Catagory"
                Else
                    CusrReportArr(ds.Tables(0).Rows(0).Item("CatagoryPos") - 1).caption = ds.Tables(0).Rows(0).Item("CatagoryCap").ToString.Trim
                End If
                CusrReportArr(ds.Tables(0).Rows(0).Item("CatagoryPos") - 1).column = "tblCatagory.CATAGORYNAME"
                CusrReportArr(ds.Tables(0).Rows(0).Item("CatagoryPos") - 1).index = ds.Tables(0).Rows(0).Item("CatagoryPos") - 1
                indextmp = indextmp + 1
            End If
            If ds.Tables(0).Rows(0).Item("EmpGrp").ToString.Trim = "Y" Then
                If ds.Tables(0).Rows(0).Item("EmpGrpCap").ToString.Trim = "" Then
                    CusrReportArr(ds.Tables(0).Rows(0).Item("EmpGrpPos") - 1).caption = "Employee Group"
                Else
                    CusrReportArr(ds.Tables(0).Rows(0).Item("EmpGrpPos") - 1).caption = ds.Tables(0).Rows(0).Item("EmpGrpCap").ToString.Trim
                End If
                CusrReportArr(ds.Tables(0).Rows(0).Item("EmpGrpPos") - 1).column = "EmployeeGroup.GroupName"
                CusrReportArr(ds.Tables(0).Rows(0).Item("EmpGrpPos") - 1).index = ds.Tables(0).Rows(0).Item("EmpGrpPos") - 1
                indextmp = indextmp + 1
            End If
            If ds.Tables(0).Rows(0).Item("SHIFTATTENDED").ToString.Trim = "Y" Then
                If ds.Tables(0).Rows(0).Item("SHIFTATTENDEDCap").ToString.Trim = "" Then
                    CusrReportArr(ds.Tables(0).Rows(0).Item("SHIFTATTENDEDPos") - 1).caption = "Shift Attended"
                Else
                    CusrReportArr(ds.Tables(0).Rows(0).Item("SHIFTATTENDEDPos") - 1).caption = ds.Tables(0).Rows(0).Item("SHIFTATTENDEDCap").ToString.Trim
                End If
                CusrReportArr(ds.Tables(0).Rows(0).Item("SHIFTATTENDEDPos") - 1).column = "tblTimeRegister.SHIFTATTENDED"
                CusrReportArr(ds.Tables(0).Rows(0).Item("SHIFTATTENDEDPos") - 1).index = ds.Tables(0).Rows(0).Item("SHIFTATTENDEDPos") - 1
                indextmp = indextmp + 1
            End If
            If ds.Tables(0).Rows(0).Item("SHIFTSTARTTIME").ToString.Trim = "Y" Then
                If ds.Tables(0).Rows(0).Item("SHIFTSTARTTIMECap").ToString.Trim = "" Then
                    CusrReportArr(ds.Tables(0).Rows(0).Item("SHIFTSTARTTIMEPos") - 1).caption = "Shift StartTime"
                Else
                    CusrReportArr(ds.Tables(0).Rows(0).Item("SHIFTSTARTTIMEPos") - 1).caption = ds.Tables(0).Rows(0).Item("SHIFTSTARTTIMECap").ToString.Trim
                End If
                CusrReportArr(ds.Tables(0).Rows(0).Item("SHIFTSTARTTIMEPos") - 1).column = "tblTimeRegister.SHIFTSTARTTIME"
                CusrReportArr(ds.Tables(0).Rows(0).Item("SHIFTSTARTTIMEPos") - 1).index = ds.Tables(0).Rows(0).Item("SHIFTSTARTTIMEPos") - 1
                indextmp = indextmp + 1
            End If
            If ds.Tables(0).Rows(0).Item("SHIFTENDTIME").ToString.Trim = "Y" Then
                If ds.Tables(0).Rows(0).Item("SHIFTENDTIMECap").ToString.Trim = "" Then
                    CusrReportArr(ds.Tables(0).Rows(0).Item("SHIFTENDTIMEPos") - 1).caption = "Shift EndTime"
                Else
                    CusrReportArr(ds.Tables(0).Rows(0).Item("SHIFTENDTIMEPos") - 1).caption = ds.Tables(0).Rows(0).Item("SHIFTENDTIMECap").ToString.Trim
                End If
                CusrReportArr(ds.Tables(0).Rows(0).Item("SHIFTENDTIMEPos") - 1).column = "tblTimeRegister.SHIFTENDTIME"
                CusrReportArr(ds.Tables(0).Rows(0).Item("SHIFTENDTIMEPos") - 1).index = ds.Tables(0).Rows(0).Item("SHIFTENDTIMEPos") - 1
                indextmp = indextmp + 1
            End If
            If ds.Tables(0).Rows(0).Item("IN1").ToString.Trim = "Y" Then
                If ds.Tables(0).Rows(0).Item("IN1Cap").ToString.Trim = "" Then
                    CusrReportArr(ds.Tables(0).Rows(0).Item("IN1Pos") - 1).caption = "In Time"
                Else
                    CusrReportArr(ds.Tables(0).Rows(0).Item("IN1Pos") - 1).caption = ds.Tables(0).Rows(0).Item("IN1Cap").ToString.Trim
                End If
                CusrReportArr(ds.Tables(0).Rows(0).Item("IN1Pos") - 1).column = "tblTimeRegister.IN1"
                CusrReportArr(ds.Tables(0).Rows(0).Item("IN1Pos") - 1).index = ds.Tables(0).Rows(0).Item("IN1Pos") - 1
                indextmp = indextmp + 1
            End If
            If ds.Tables(0).Rows(0).Item("OUT1").ToString.Trim = "Y" Then
                If ds.Tables(0).Rows(0).Item("OUT1Cap").ToString.Trim = "" Then
                    CusrReportArr(ds.Tables(0).Rows(0).Item("OUT1Pos") - 1).caption = "Lunch Out"
                Else
                    CusrReportArr(ds.Tables(0).Rows(0).Item("OUT1Pos") - 1).caption = ds.Tables(0).Rows(0).Item("OUT1Cap").ToString.Trim
                End If
                CusrReportArr(ds.Tables(0).Rows(0).Item("OUT1Pos") - 1).column = "tblTimeRegister.OUT1"
                CusrReportArr(ds.Tables(0).Rows(0).Item("OUT1Pos") - 1).index = ds.Tables(0).Rows(0).Item("OUT1Pos") - 1
                indextmp = indextmp + 1
            End If
            If ds.Tables(0).Rows(0).Item("IN2").ToString.Trim = "Y" Then
                If ds.Tables(0).Rows(0).Item("IN2Cap").ToString.Trim = "" Then
                    CusrReportArr(ds.Tables(0).Rows(0).Item("IN2Pos") - 1).caption = "Lunch In"
                Else
                    CusrReportArr(ds.Tables(0).Rows(0).Item("IN2Pos") - 1).caption = ds.Tables(0).Rows(0).Item("IN2Cap").ToString.Trim
                End If
                CusrReportArr(ds.Tables(0).Rows(0).Item("IN2Pos") - 1).column = "tblTimeRegister.IN2"
                CusrReportArr(ds.Tables(0).Rows(0).Item("IN2Pos") - 1).index = ds.Tables(0).Rows(0).Item("IN2Pos") - 1
                indextmp = indextmp + 1
            End If
            If ds.Tables(0).Rows(0).Item("OUT2").ToString.Trim = "Y" Then
                If ds.Tables(0).Rows(0).Item("OUT2Cap").ToString.Trim = "" Then
                    CusrReportArr(ds.Tables(0).Rows(0).Item("OUT2Pos") - 1).caption = "Out Time"
                Else
                    CusrReportArr(ds.Tables(0).Rows(0).Item("OUT2Pos") - 1).caption = ds.Tables(0).Rows(0).Item("OUT2Cap").ToString.Trim
                End If
                CusrReportArr(ds.Tables(0).Rows(0).Item("OUT2Pos") - 1).column = "tblTimeRegister.OUT2"
                CusrReportArr(ds.Tables(0).Rows(0).Item("OUT2Pos") - 1).index = ds.Tables(0).Rows(0).Item("OUT2Pos") - 1
                indextmp = indextmp + 1
            End If
            If ds.Tables(0).Rows(0).Item("STATUS").ToString.Trim = "Y" Then
                If ds.Tables(0).Rows(0).Item("STATUSCap").ToString.Trim = "" Then
                    CusrReportArr(ds.Tables(0).Rows(0).Item("STATUSPos") - 1).caption = "Status"
                Else
                    CusrReportArr(ds.Tables(0).Rows(0).Item("STATUSPos") - 1).caption = ds.Tables(0).Rows(0).Item("STATUSCap").ToString.Trim
                End If
                CusrReportArr(ds.Tables(0).Rows(0).Item("STATUSPos") - 1).column = "tblTimeRegister.STATUS"
                CusrReportArr(ds.Tables(0).Rows(0).Item("STATUSPos") - 1).index = ds.Tables(0).Rows(0).Item("STATUSPos") - 1
                indextmp = indextmp + 1
            End If
            If ds.Tables(0).Rows(0).Item("HOURSWORKED").ToString.Trim = "Y" Then
                If ds.Tables(0).Rows(0).Item("HOURSWORKEDCap").ToString.Trim = "" Then
                    CusrReportArr(ds.Tables(0).Rows(0).Item("HOURSWORKEDPos") - 1).caption = "Hours Worked"
                Else
                    CusrReportArr(ds.Tables(0).Rows(0).Item("HOURSWORKEDPos") - 1).caption = ds.Tables(0).Rows(0).Item("HOURSWORKEDCap").ToString.Trim
                End If
                CusrReportArr(ds.Tables(0).Rows(0).Item("HOURSWORKEDPos") - 1).column = "tblTimeRegister.HOURSWORKED"
                CusrReportArr(ds.Tables(0).Rows(0).Item("HOURSWORKEDPos") - 1).index = ds.Tables(0).Rows(0).Item("HOURSWORKEDPos") - 1
                indextmp = indextmp + 1
            End If
            If ds.Tables(0).Rows(0).Item("LATEARRIVAL").ToString.Trim = "Y" Then
                If ds.Tables(0).Rows(0).Item("LATEARRIVALCap").ToString.Trim = "" Then
                    CusrReportArr(ds.Tables(0).Rows(0).Item("LATEARRIVALPos") - 1).caption = "Late Arrival"
                Else
                    CusrReportArr(ds.Tables(0).Rows(0).Item("LATEARRIVALPos") - 1).caption = ds.Tables(0).Rows(0).Item("LATEARRIVALCap").ToString.Trim
                End If
                CusrReportArr(ds.Tables(0).Rows(0).Item("LATEARRIVALPos") - 1).column = "tblTimeRegister.LATEARRIVAL"
                CusrReportArr(ds.Tables(0).Rows(0).Item("LATEARRIVALPos") - 1).index = ds.Tables(0).Rows(0).Item("LATEARRIVALPos") - 1
                indextmp = indextmp + 1
            End If
            If ds.Tables(0).Rows(0).Item("EARLYDEPARTURE").ToString.Trim = "Y" Then
                If ds.Tables(0).Rows(0).Item("EARLYDEPARTURECap").ToString.Trim = "" Then
                    CusrReportArr(ds.Tables(0).Rows(0).Item("EARLYDEPARTUREPos") - 1).caption = "Early Departure"
                Else
                    CusrReportArr(ds.Tables(0).Rows(0).Item("EARLYDEPARTUREPos") - 1).caption = ds.Tables(0).Rows(0).Item("EARLYDEPARTURECap").ToString.Trim
                End If
                CusrReportArr(ds.Tables(0).Rows(0).Item("EARLYDEPARTUREPos") - 1).column = "tblTimeRegister.EARLYDEPARTURE"
                CusrReportArr(ds.Tables(0).Rows(0).Item("EARLYDEPARTUREPos") - 1).index = ds.Tables(0).Rows(0).Item("EARLYDEPARTUREPos") - 1
                indextmp = indextmp + 1
            End If
            If ds.Tables(0).Rows(0).Item("OTDURATION").ToString.Trim = "Y" Then
                If ds.Tables(0).Rows(0).Item("OTDURATIONCap").ToString.Trim = "" Then
                    CusrReportArr(ds.Tables(0).Rows(0).Item("OTDURATIONPos") - 1).caption = "OT Duration"
                Else
                    CusrReportArr(ds.Tables(0).Rows(0).Item("OTDURATIONPos") - 1).caption = ds.Tables(0).Rows(0).Item("OTDURATIONCap").ToString.Trim
                End If
                CusrReportArr(ds.Tables(0).Rows(0).Item("OTDURATIONPos") - 1).column = "tblTimeRegister.OTDURATION"
                CusrReportArr(ds.Tables(0).Rows(0).Item("OTDURATIONPos") - 1).index = ds.Tables(0).Rows(0).Item("OTDURATIONPos") - 1
                indextmp = indextmp + 1
            End If
            If ds.Tables(0).Rows(0).Item("OSDURATION").ToString.Trim = "Y" Then
                If ds.Tables(0).Rows(0).Item("OSDURATIONCap").ToString.Trim = "" Then
                    CusrReportArr(ds.Tables(0).Rows(0).Item("OSDURATIONPos") - 1).caption = "OS Duration"
                Else
                    CusrReportArr(ds.Tables(0).Rows(0).Item("OSDURATIONPos") - 1).caption = ds.Tables(0).Rows(0).Item("OSDURATIONCap").ToString.Trim
                End If
                CusrReportArr(ds.Tables(0).Rows(0).Item("OSDURATIONPos") - 1).column = "tblTimeRegister.OSDURATION"
                CusrReportArr(ds.Tables(0).Rows(0).Item("OSDURATIONPos") - 1).index = ds.Tables(0).Rows(0).Item("OSDURATIONPos") - 1
                indextmp = indextmp + 1
            End If
            If ds.Tables(0).Rows(0).Item("GuardianName").ToString.Trim = "Y" Then
                If ds.Tables(0).Rows(0).Item("GuardianNameCap").ToString.Trim = "" Then
                    CusrReportArr(ds.Tables(0).Rows(0).Item("GuardianNamePos") - 1).caption = "Guardian Name"
                Else
                    CusrReportArr(ds.Tables(0).Rows(0).Item("GuardianNamePos") - 1).caption = ds.Tables(0).Rows(0).Item("GuardianNameCap").ToString.Trim
                End If
                CusrReportArr(ds.Tables(0).Rows(0).Item("GuardianNamePos") - 1).column = "TblEmployee.GUARDIANNAME"
                CusrReportArr(ds.Tables(0).Rows(0).Item("GuardianNamePos") - 1).index = ds.Tables(0).Rows(0).Item("GuardianNamePos") - 1
                indextmp = indextmp + 1
            End If
            If ds.Tables(0).Rows(0).Item("Designation").ToString.Trim = "Y" Then
                If ds.Tables(0).Rows(0).Item("DesignationCap").ToString.Trim = "" Then
                    CusrReportArr(ds.Tables(0).Rows(0).Item("DesignationPos") - 1).caption = "Designation"
                Else
                    CusrReportArr(ds.Tables(0).Rows(0).Item("DesignationPos") - 1).caption = ds.Tables(0).Rows(0).Item("DesignationCap").ToString.Trim
                End If
                CusrReportArr(ds.Tables(0).Rows(0).Item("DesignationPos") - 1).column = "TblEmployee.DESIGNATION"
                CusrReportArr(ds.Tables(0).Rows(0).Item("DesignationPos") - 1).index = ds.Tables(0).Rows(0).Item("DesignationPos") - 1
                indextmp = indextmp + 1
            End If
            If ds.Tables(0).Rows(0).Item("DateOfJoining").ToString.Trim = "Y" Then
                If ds.Tables(0).Rows(0).Item("DateOfJoiningCap").ToString.Trim = "" Then
                    CusrReportArr(ds.Tables(0).Rows(0).Item("DateOfJoiningPos") - 1).caption = "Date Of Joining"
                Else
                    CusrReportArr(ds.Tables(0).Rows(0).Item("DateOfJoiningPos") - 1).caption = ds.Tables(0).Rows(0).Item("DateOfJoiningCap").ToString.Trim
                End If
                CusrReportArr(ds.Tables(0).Rows(0).Item("DateOfJoiningPos") - 1).column = "TblEmployee.DateOFJOIN"
                CusrReportArr(ds.Tables(0).Rows(0).Item("DateOfJoiningPos") - 1).index = ds.Tables(0).Rows(0).Item("DateOfJoiningPos") - 1
                indextmp = indextmp + 1
            End If
            If ds.Tables(0).Rows(0).Item("Gender").ToString.Trim = "Y" Then
                If ds.Tables(0).Rows(0).Item("GenderCap").ToString.Trim = "" Then
                    CusrReportArr(ds.Tables(0).Rows(0).Item("GenderPos") - 1).caption = "Gender"
                Else
                    CusrReportArr(ds.Tables(0).Rows(0).Item("GenderPos") - 1).caption = ds.Tables(0).Rows(0).Item("GenderCap").ToString.Trim
                End If
                CusrReportArr(ds.Tables(0).Rows(0).Item("GenderPos") - 1).column = "TblEmployee.SEX"
                CusrReportArr(ds.Tables(0).Rows(0).Item("GenderPos") - 1).index = ds.Tables(0).Rows(0).Item("GenderPos") - 1
                indextmp = indextmp + 1
            End If
            If ds.Tables(0).Rows(0).Item("DateCol").ToString.Trim = "Y" Then
                If ds.Tables(0).Rows(0).Item("DateColCap").ToString.Trim = "" Then
                    CusrReportArr(ds.Tables(0).Rows(0).Item("DateColPos") - 1).caption = "Date"
                Else
                    CusrReportArr(ds.Tables(0).Rows(0).Item("DateColPos") - 1).caption = ds.Tables(0).Rows(0).Item("DateColCap").ToString.Trim
                End If
                CusrReportArr(ds.Tables(0).Rows(0).Item("DateColPos") - 1).column = "tblTimeRegister.DateOFFICE"
                CusrReportArr(ds.Tables(0).Rows(0).Item("DateColPos") - 1).index = ds.Tables(0).Rows(0).Item("DateColPos") - 1
                indextmp = indextmp + 1
            End If
            If ds.Tables(0).Rows(0).Item("EXCLUNCHHOURS").ToString.Trim = "Y" Then
                If ds.Tables(0).Rows(0).Item("EXCLUNCHHOURSCap").ToString.Trim = "" Then
                    CusrReportArr(ds.Tables(0).Rows(0).Item("EXCLUNCHHOURSPos") - 1).caption = "Excess Lunch"
                Else
                    CusrReportArr(ds.Tables(0).Rows(0).Item("EXCLUNCHHOURSPos") - 1).caption = ds.Tables(0).Rows(0).Item("EXCLUNCHHOURSCap").ToString.Trim
                End If
                CusrReportArr(ds.Tables(0).Rows(0).Item("EXCLUNCHHOURSPos") - 1).column = "tblTimeRegister.EXCLUNCHHOURS"
                CusrReportArr(ds.Tables(0).Rows(0).Item("EXCLUNCHHOURSPos") - 1).index = ds.Tables(0).Rows(0).Item("EXCLUNCHHOURSPos") - 1
                indextmp = indextmp + 1
            End If
            If ds.Tables(0).Rows(0).Item("TELEPHONE2").ToString.Trim = "Y" Then 'Adhar
                If ds.Tables(0).Rows(0).Item("TELEPHONE2Cap").ToString.Trim = "" Then
                    CusrReportArr(ds.Tables(0).Rows(0).Item("TELEPHONE2Pos") - 1).caption = "UID"
                Else
                    CusrReportArr(ds.Tables(0).Rows(0).Item("TELEPHONE2Pos") - 1).caption = ds.Tables(0).Rows(0).Item("TELEPHONE2Cap").ToString.Trim
                End If
                CusrReportArr(ds.Tables(0).Rows(0).Item("TELEPHONE2Pos") - 1).column = "tblEmployee.TELEPHONE2"
                CusrReportArr(ds.Tables(0).Rows(0).Item("TELEPHONE2Pos") - 1).index = ds.Tables(0).Rows(0).Item("TELEPHONE2Pos") - 1
                indextmp = indextmp + 1
            End If
            'MsgBox(indextmp)
            Dim CusrReportArrNew(indextmp - 1) As CusrReport
            Dim sSqlSelect As String = ""
            For i As Integer = 0 To indextmp - 1
                CusrReportArrNew(i) = CusrReportArr(i)
                sSqlSelect = sSqlSelect & "," & CusrReportArrNew(i).column
            Next
            sSqlSelect = sSqlSelect.TrimStart(",")
            'MsgBox(CusrReportArrNew.Length)
            'Exit Sub
            sSql = "select " & sSqlSelect & " from TblEmployee, tblTimeRegister, tblDepartment, tblCatagory, tblbranch, tblGrade, EmployeeGroup, tblCompany " & _
            "where TblEmployee.PAYCODE = tblTimeRegister.PAYCODE and TblEmployee.DEPARTMENTCODE=tblDepartment.DEPARTMENTCODE and  " & _
            "TblEmployee.CAT = tblCatagory.CAT and TblEmployee.BRANCHCODE= tblbranch.BRANCHCODE and TblEmployee.GradeCode=tblGrade.GradeCode AND  " & _
            "TblEmployee.EmployeeGroupId=EmployeeGroup.GroupId and TblEmployee.COMPANYCODE=tblCompany.COMPANYCODE and  " & _
            "tblTimeRegister.DateOFFICE >= '" & DateEdit1.DateTime.ToString("yyyy-MM-dd 00:00:00") & "' and tblTimeRegister.DateOFFICE <='" & DateEdit1.DateTime.ToString("yyyy-MM-dd 23:59:59") & "' " & _
            " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & strsortorder

            Dim Rs_Report As DataSet = New DataSet
            If Common.servername = "Access" Then
                sSql = sSql.Replace("tblTimeRegister.DateOFFICE", "FORMAT(tblTimeRegister.DateOFFICE,'yyyy-MM-dd HH:mm:ss')")
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(Rs_Report)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(Rs_Report)
            End If
            If Rs_Report.Tables(0).Rows.Count < 1 Then
                XtraMessageBox.Show(ulf, "<size=10>No Data Available For this Report.</size>", "<size=9>iAS</size>")
                Exit Sub
            End If
            Me.Cursor = Cursors.WaitCursor
            Common.frodatetodatetoReportGrid = " CUSTOMISED REPORT FOR DATE : " & ForDate 'DateEdit1.DateTime.ToString("dd/MM/yyyy")
            Common.tbl = New Data.DataTable()
            For i As Integer = 0 To indextmp - 1
                Common.tbl.Columns.Add(CusrReportArrNew(i).caption)
            Next

            For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
                XtraMasterTest.LabelControlStatus.Text = "Adding Data to Report " & i + 1 & " of " & Rs_Report.Tables(0).Rows.Count & "..."
                System.Windows.Forms.Application.DoEvents()
                Common.tbl.Rows.Add()
                For j As Integer = 0 To indextmp - 1
                    If CusrReportArrNew(j).column.Contains("Paycode") And PayIsPrefix = "Y" Then
                        Common.tbl.DefaultView(i)(j) = Rs_Report.Tables(0).Rows(i).Item(j).ToString.Trim.PadLeft(PayLength, PayText)
                    ElseIf CusrReportArrNew(j).column.Contains("PRESENTCARDNO") And PreIsPrifix = "Y" Then
                        Common.tbl.DefaultView(i)(j) = Rs_Report.Tables(0).Rows(i).Item(j).ToString.Trim.PadLeft(PreLength, PreText)
                    ElseIf CusrReportArrNew(j).column.Contains("Date") Then
                        If Common.IsNepali = "Y" Then
                            Dim tmp As DateTime = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item(j).ToString.Trim)
                            Dim DC As New DateConverter()
                            'Dim doj As DateTime = DC.ToBS(New Date(tmp.Year, tmp.Month, tmp.Day))
                            'Common.tbl.DefaultView(i)(j) = (doj.Day & "/" & Common.NepaliMonth(doj.Month - 1) & "/" & doj.Year)

                            'Dim tmp As DateTime = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("DATEOFFICE").ToString.Trim)
                            Dim Vstart As String = DC.ToBS(New Date(tmp.Year, tmp.Month, tmp.Day))
                            Dim dojTmp() As String = Vstart.Split("-")
                            Common.tbl.DefaultView(i)(j) = dojTmp(2) & "/" & Common.NepaliMonth(dojTmp(1) - 1) & "/" & dojTmp(0)

                        Else
                            Common.tbl.DefaultView(i)(j) = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item(j).ToString.Trim).ToString(DateFormat)
                        End If
                    ElseIf CusrReportArrNew(j).column.Contains("SHIFTSTARTTIME") Or CusrReportArrNew(j).column.Contains("SHIFTENDTIME") Then
                        Try
                            Common.tbl.DefaultView(i)(j) = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item(j).ToString.Trim).ToString("HH:mm")
                        Catch ex As Exception
                            Common.tbl.DefaultView(i)(j) = ""
                        End Try
                    ElseIf CusrReportArrNew(j).column.Contains("IN1") Or CusrReportArrNew(j).column.Contains("IN2") Or
                        CusrReportArrNew(j).column.Contains("OUT1") Or CusrReportArrNew(j).column.Contains("OUT2") Then
                        Try
                            Common.tbl.DefaultView(i)(j) = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item(j).ToString.Trim).ToString("HH:mm")
                            If isTemperature = "Y" Then
                                sSql = "select temperature from machinerawpunch where Paycode='" & Rs_Report.Tables(0).Rows(i).Item("Paycode").ToString.Trim & "' and officepunch ='" & Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item(j).ToString.Trim).ToString("yyyy-MM-dd HH:mm:ss") & "'"
                                Dim TempR As DataSet = New DataSet
                                If Common.servername = "Access" Then
                                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                                    adapA.Fill(TempR)
                                Else
                                    adap = New SqlDataAdapter(sSql, Common.con)
                                    adap.Fill(TempR)
                                End If
                                If TempR.Tables(0).Rows.Count > 0 Then
                                    Common.tbl.DefaultView(i)(j) = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item(j).ToString.Trim).ToString("HH:mm") & " (" & TempR.Tables(0).Rows(0)(0).ToString.Trim & ")"
                                End If
                            End If

                        Catch ex As Exception
                            Common.tbl.DefaultView(i)(j) = ""
                        End Try
                    ElseIf CusrReportArrNew(j).column.Contains("LATEARRIVAL") Or CusrReportArrNew(j).column.Contains("HOURSWORKED") Or
                        CusrReportArrNew(j).column.Contains("EARLYDEPARTURE") Or CusrReportArrNew(j).column.Contains("OTDURATION") Or
                        CusrReportArrNew(j).column.Contains("OSDURATION") Or CusrReportArrNew(j).column.Contains("EXCLUNCHHOURS") Then
                        Try
                            Common.tbl.DefaultView(i)(j) = (Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item(j).ToString.Trim) / 60)).ToString("00") & ":" & (Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item(j).ToString.Trim) Mod 60).ToString("00")
                            If Common.tbl.DefaultView(i)(j) = "00:00" Then
                                Common.tbl.DefaultView(i)(j) = ""
                            End If
                        Catch ex As Exception
                            Common.tbl.DefaultView(i)(j) = ""
                        End Try
                    Else
                        Common.tbl.DefaultView(i)(j) = Rs_Report.Tables(0).Rows(i).Item(j).ToString.Trim
                    End If
                Next

                'If ds.Tables(0).Rows(0).Item("Paycode").ToString.Trim = "Y" Then
                '    'Common.tbl.Columns.Add("Paycode")
                '    If Common.servername = "Access" Then
                '        Common.tbl.DefaultView(i)("Paycode") = Rs_Report.Tables(0).Rows(i).Item("TblEmployee.PAYCODE").ToString.Trim
                '    Else
                '        Common.tbl.DefaultView(i)("Paycode") = Rs_Report.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim
                '    End If

                'End If
                'If ds.Tables(0).Rows(0).Item("Name").ToString.Trim = "Y" Then
                '    'Common.tbl.Columns.Add("Name")
                '    Common.tbl.DefaultView(i)("Name") = Rs_Report.Tables(0).Rows(i).Item("EMPNAME").ToString.Trim
                'End If
                'If ds.Tables(0).Rows(0).Item("CardNumber").ToString.Trim = "Y" Then
                '    'Common.tbl.Columns.Add("User Number")
                '    Common.tbl.DefaultView(i)("User Number") = Rs_Report.Tables(0).Rows(i).Item("PRESENTCARDNO").ToString.Trim
                'End If
                'If ds.Tables(0).Rows(0).Item("Company").ToString.Trim = "Y" Then
                '    'Common.tbl.Columns.Add("Company")
                '    Common.tbl.DefaultView(i)("Company") = Rs_Report.Tables(0).Rows(i).Item("COMPANYNAME").ToString.Trim
                'End If
                'If ds.Tables(0).Rows(0).Item("Location").ToString.Trim = "Y" Then
                '    'Common.tbl.Columns.Add("Location")
                '    Common.tbl.DefaultView(i)("Location") = Rs_Report.Tables(0).Rows(i).Item("BRANCHNAME").ToString.Trim
                'End If
                'If ds.Tables(0).Rows(0).Item("Department").ToString.Trim = "Y" Then
                '    'Common.tbl.Columns.Add("Department")
                '    Common.tbl.DefaultView(i)("Department") = Rs_Report.Tables(0).Rows(i).Item("DEPARTMENTNAME").ToString.Trim
                'End If
                'If ds.Tables(0).Rows(0).Item("Grade").ToString.Trim = "Y" Then
                '    'Common.tbl.Columns.Add("Grade")
                '    Common.tbl.DefaultView(i)("Grade") = Rs_Report.Tables(0).Rows(i).Item("GradeName").ToString.Trim
                'End If
                'If ds.Tables(0).Rows(0).Item("Catagory").ToString.Trim = "Y" Then
                '    'Common.tbl.Columns.Add("Catagory")
                '    Common.tbl.DefaultView(i)("Catagory") = Rs_Report.Tables(0).Rows(i).Item("CATAGORYNAME").ToString.Trim
                'End If
                'If ds.Tables(0).Rows(0).Item("EmpGrp").ToString.Trim = "Y" Then
                '    'Common.tbl.Columns.Add("Employee Group")
                '    Common.tbl.DefaultView(i)("Employee Group") = Rs_Report.Tables(0).Rows(i).Item("GroupName").ToString.Trim
                'End If

                'If ds.Tables(0).Rows(0).Item("SHIFTATTENDED").ToString.Trim = "Y" Then
                '    'Common.tbl.Columns.Add("Shift Attended")
                '    Common.tbl.DefaultView(i)("Shift Attended") = Rs_Report.Tables(0).Rows(i).Item("SHIFTATTENDED").ToString.Trim
                'End If

                'Dim TTIMETmp As DateTime
                'If ds.Tables(0).Rows(0).Item("SHIFTSTARTTIME").ToString.Trim = "Y" Then
                '    'Common.tbl.Columns.Add("Shift StartTime")
                '    'Common.tbl.DefaultView(i)("Shift StartTime") = Rs_Report.Tables(0).Rows(i).Item("SHIFTSTARTTIME").ToString.Trim
                '    Try
                '        TTIMETmp = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("SHIFTSTARTTIME").ToString.Trim)
                '        Common.tbl.DefaultView(i)("Shift StartTime") = TTIMETmp.ToString("HH:mm")
                '    Catch ex As Exception
                '        Common.tbl.DefaultView(i)("Shift StartTime") = ""
                '    End Try
                'End If
                'If ds.Tables(0).Rows(0).Item("SHIFTENDTIME").ToString.Trim = "Y" Then
                '    'Common.tbl.Columns.Add("Shift EndTime")
                '    'Common.tbl.DefaultView(i)("Shift EndTime") = Rs_Report.Tables(0).Rows(i).Item("SHIFTENDTIME").ToString.Trim
                '    Try
                '        TTIMETmp = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("SHIFTENDTIME").ToString.Trim)
                '        Common.tbl.DefaultView(i)("Shift EndTime") = TTIMETmp.ToString("HH:mm")
                '    Catch ex As Exception
                '        Common.tbl.DefaultView(i)("Shift EndTime") = ""
                '    End Try
                'End If
                'If ds.Tables(0).Rows(0).Item("IN1").ToString.Trim = "Y" Then
                '    'Common.tbl.Columns.Add("In Time")
                '    'Common.tbl.DefaultView(i)("In Time") = Rs_Report.Tables(0).Rows(i).Item("IN1").ToString.Trim
                '    Try
                '        TTIMETmp = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("IN1").ToString.Trim)
                '        Common.tbl.DefaultView(i)("In Time") = TTIMETmp.ToString("HH:mm")
                '    Catch ex As Exception
                '        Common.tbl.DefaultView(i)("In Time") = ""
                '    End Try
                'End If
                'If ds.Tables(0).Rows(0).Item("OUT1").ToString.Trim = "Y" Then
                '    'Common.tbl.Columns.Add("Lunch Out")
                '    'Common.tbl.DefaultView(i)("Lunch Out") = Rs_Report.Tables(0).Rows(i).Item("OUT1").ToString.Trim
                '    Try
                '        TTIMETmp = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("OUT1").ToString.Trim)
                '        Common.tbl.DefaultView(i)("Lunch Out") = TTIMETmp.ToString("HH:mm")
                '    Catch ex As Exception
                '        Common.tbl.DefaultView(i)("Lunch Out") = ""
                '    End Try
                'End If
                'If ds.Tables(0).Rows(0).Item("IN2").ToString.Trim = "Y" Then
                '    'Common.tbl.Columns.Add("Lunch In")
                '    'Common.tbl.DefaultView(i)("Lunch In") = Rs_Report.Tables(0).Rows(i).Item("IN2").ToString.Trim
                '    Try
                '        TTIMETmp = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("IN2").ToString.Trim)
                '        Common.tbl.DefaultView(i)("Lunch In") = TTIMETmp.ToString("HH:mm")
                '    Catch ex As Exception
                '        Common.tbl.DefaultView(i)("Lunch In") = ""
                '    End Try
                'End If
                'If ds.Tables(0).Rows(0).Item("OUT2").ToString.Trim = "Y" Then
                '    'Common.tbl.Columns.Add("Out Time")
                '    'Common.tbl.DefaultView(i)("Out Time") = Rs_Report.Tables(0).Rows(i).Item("OUT2").ToString.Trim
                '    Try
                '        TTIMETmp = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("OUT2").ToString.Trim)
                '        Common.tbl.DefaultView(i)("Out Time") = TTIMETmp.ToString("HH:mm")
                '    Catch ex As Exception
                '        Common.tbl.DefaultView(i)("Out Time") = ""
                '    End Try
                'End If
                'If ds.Tables(0).Rows(0).Item("STATUS").ToString.Trim = "Y" Then
                '    'Common.tbl.Columns.Add("Status")
                '    Common.tbl.DefaultView(i)("Status") = Rs_Report.Tables(0).Rows(i).Item("STATUS").ToString.Trim
                'End If
                'If ds.Tables(0).Rows(0).Item("HOURSWORKED").ToString.Trim = "Y" Then
                '    'Common.tbl.Columns.Add("Hours Worked")
                '    If Rs_Report.Tables(0).Rows(i).Item("HOURSWORKED").ToString.Trim = "" Or Rs_Report.Tables(0).Rows(i).Item("HOURSWORKED").ToString.Trim = "0" Then
                '        Common.tbl.DefaultView(i)("Hours Worked") = ""
                '    Else
                '        Common.tbl.DefaultView(i)("Hours Worked") = (Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("HOURSWORKED").ToString.Trim) / 60)).ToString("00") & ":" & (Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("HOURSWORKED").ToString.Trim) Mod 60).ToString("00") ' Rs_Report.Tables(0).Rows(i).Item("HOURSWORKED").ToString.Trim
                '    End If

                'End If
                'If ds.Tables(0).Rows(0).Item("LATEARRIVAL").ToString.Trim = "Y" Then
                '    'Common.tbl.Columns.Add("Late Arrival")
                '    'Common.tbl.DefaultView(i)("Late Arrival") = Rs_Report.Tables(0).Rows(i).Item("LATEARRIVAL").ToString.Trim
                '    If Rs_Report.Tables(0).Rows(i).Item("LATEARRIVAL").ToString.Trim = "" Or Rs_Report.Tables(0).Rows(i).Item("LATEARRIVAL").ToString.Trim = "0" Then
                '        Common.tbl.DefaultView(i)("Late Arrival") = ""
                '    Else
                '        Common.tbl.DefaultView(i)("Late Arrival") = (Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("LATEARRIVAL").ToString.Trim) / 60)).ToString("00") & ":" & (Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("LATEARRIVAL").ToString.Trim) Mod 60).ToString("00")
                '    End If
                'End If
                'If ds.Tables(0).Rows(0).Item("EARLYDEPARTURE").ToString.Trim = "Y" Then
                '    'Common.tbl.Columns.Add("Early Departure")
                '    'Common.tbl.DefaultView(i)("Early Departure") = Rs_Report.Tables(0).Rows(i).Item("EARLYDEPARTURE").ToString.Trim
                '    If Rs_Report.Tables(0).Rows(i).Item("EARLYDEPARTURE").ToString.Trim = "" Or Rs_Report.Tables(0).Rows(i).Item("EARLYDEPARTURE").ToString.Trim = "0" Then
                '        Common.tbl.DefaultView(i)("Early Departure") = ""
                '    Else
                '        Common.tbl.DefaultView(i)("Early Departure") = (Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("EARLYDEPARTURE").ToString.Trim) / 60)).ToString("00") & ":" & (Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("EARLYDEPARTURE").ToString.Trim) Mod 60).ToString("00")
                '    End If
                'End If
                'If ds.Tables(0).Rows(0).Item("OTDURATION").ToString.Trim = "Y" Then
                '    'Common.tbl.Columns.Add("OT Duration")
                '    'Common.tbl.DefaultView(i)("OT Duration") = Rs_Report.Tables(0).Rows(i).Item("OTDURATION").ToString.Trim
                '    If Rs_Report.Tables(0).Rows(i).Item("OTDURATION").ToString.Trim = "" Or Rs_Report.Tables(0).Rows(i).Item("OTDURATION").ToString.Trim = "0" Then
                '        Common.tbl.DefaultView(i)("OT Duration") = ""
                '    Else
                '        Common.tbl.DefaultView(i)("OT Duration") = (Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("OTDURATION").ToString.Trim) / 60)).ToString("00") & ":" & (Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("OTDURATION").ToString.Trim) Mod 60).ToString("00")
                '    End If
                'End If
                'If ds.Tables(0).Rows(0).Item("OSDURATION").ToString.Trim = "Y" Then
                '    'Common.tbl.Columns.Add("OS Duration")
                '    'Common.tbl.DefaultView(i)("OS Duration") = Rs_Report.Tables(0).Rows(i).Item("OSDURATION").ToString.Trim
                '    If Rs_Report.Tables(0).Rows(i).Item("OSDURATION").ToString.Trim = "" Or Rs_Report.Tables(0).Rows(i).Item("OSDURATION").ToString.Trim = "0" Then
                '        Common.tbl.DefaultView(i)("OS Duration") = ""
                '    Else
                '        Common.tbl.DefaultView(i)("OS Duration") = (Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("OSDURATION").ToString.Trim) / 60)).ToString("00") & ":" & (Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("OSDURATION").ToString.Trim) Mod 60).ToString("00")
                '    End If
                'End If
            Next
            XtraMasterTest.LabelControlStatus.Text = ""
            System.Windows.Forms.Application.DoEvents()
            Me.Cursor = Cursors.Default
            XtraReportGrid.ShowDialog()
        End If
    End Sub
    Sub DailyXL_MultiShiftGrid(ByVal strsortorder As String)
        Dim whereClause As String
        If Common.ReportMail = True Then
            DateEdit1.DateTime = Now.AddDays(-1)
            whereClause = Common.whereClauseEmail
        Else
            whereClause = g_WhereClause
        End If
        Dim intFile As Integer
        Dim strsql As String, mCount As Integer
        Dim strOsDuration6 As String ' * 6
        Dim strOtAmount11 As String '* 11
        Dim intPrintLineCounter As Integer
        Dim strIn1Mannual_1 As String '* 1
        Dim strOut1Mannual_1 As String '* 1
        Dim strIn2Mannual_1 As String '* 1
        Dim strOut2Mannual_1 As String '* 1
        Dim blnDeptAvailable As Boolean
        Dim strDeptDivCode As String
        Dim strCurrentDeptDivCode As String
        Dim rowcnt As Integer
        If XtraShortOrder.g_SortOrder <> "" Then
            strsortorder = XtraShortOrder.g_SortOrder
        End If
        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If
        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".xls"

        mblnCheckReport = False
        mintPageNo = 1
        mintLine = 1
        If Common.servername = "Access" Then
            strsql = "select tblCatagory.CatagoryName,tblTimeRegister.Shift, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,  " &
            "tbltimeregister.PayCode, tblEmployee.PresentCardNo, tblEmployee.EmpName, tblEmployee.DepartmentCode," &
            "tblDepartment.DepartmentName, tbltimeregister.ShiftAttended, tbltimeregister.ShiftStartTime, tbltimeregister.ShiftEndTime, " &
            "tbltimeregister.In1, tbltimeregister.Out1, tbltimeregister.In2, tbltimeregister.Out2, tbltimeregister.HoursWorked, " &
            "tbltimeregister.Status, tbltimeregister.EarlyArrival, " &
            "tbltimeregister.LateArrival, tbltimeregister.EarlyDeparture, tbltimeregister.ExcLunchHours, tbltimeregister.OtDuration, " &
            "tbltimeregister.OtAmount, tbltimeregister.OsDuration, tbltimeregister.In1Mannual, tbltimeregister.In2Mannual, " &
            "tbltimeregister.Out1Mannual, tbltimeregister.Out2Mannual, " &
            "tblTimeRegister.MShift, tbltimeregister.MShiftAttended, tbltimeregister.MShiftStartTime, tbltimeregister.MShiftEndTime, " &
            "tbltimeregister.MIn1, tbltimeregister.MOut1, tbltimeregister.MIn2, tbltimeregister.MOut2, tbltimeregister.MHoursWorked, " &
            "tbltimeregister.MStatus " &
             "from tblCatagory,tbltimeregister,tblEmployee,tblCompany,tblDepartment, EmployeeGroup" &
             " Where tblCatagory.Cat = tblEmployee.Cat And tbltimeregister.PayCode = tblEmployee.PayCode And FORMAT(tbltimeregister.DateOffice,'yyyy-MM-dd') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' " &
            " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode and EmployeeGroup.GroupId=tblEmployee.EmployeeGroupId and EmployeeGroup.SHIFTTYPE = 'M' " & g_HODDepartmentCode &
            " " & IIf(Len(Trim(whereClause)) = 0, "", " AND " & whereClause) & strsortorder
            adapA = New OleDbDataAdapter(strsql, Common.con1)
            adapA.Fill(Rs_Report)
        Else
            strsql = "select tblCatagory.CatagoryName,tblTimeRegister.Shift, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,  " &
            "tbltimeregister.PayCode, tblEmployee.PresentCardNo, tblEmployee.EmpName, tblEmployee.DepartmentCode," &
            "tblDepartment.DepartmentName, tbltimeregister.ShiftAttended, tbltimeregister.ShiftStartTime, tbltimeregister.ShiftEndTime, " &
            "tbltimeregister.In1, tbltimeregister.Out1, tbltimeregister.In2, tbltimeregister.Out2, tbltimeregister.HoursWorked, " &
            "tbltimeregister.Status, tbltimeregister.EarlyArrival, " &
            "tbltimeregister.LateArrival, tbltimeregister.EarlyDeparture, tbltimeregister.ExcLunchHours, tbltimeregister.OtDuration, " &
            "tbltimeregister.OtAmount, tbltimeregister.OsDuration, tbltimeregister.In1Mannual, tbltimeregister.In2Mannual, " &
            "tbltimeregister.Out1Mannual, tbltimeregister.Out2Mannual, " &
            "tblTimeRegister.MShift, tbltimeregister.MShiftAttended, tbltimeregister.MShiftStartTime, tbltimeregister.MShiftEndTime, " &
            "tbltimeregister.MIn1, tbltimeregister.MOut1, tbltimeregister.MIn2, tbltimeregister.MOut2, tbltimeregister.MHoursWorked, " &
            "tbltimeregister.MStatus " &
             "from tblCatagory,tbltimeregister,tblEmployee,tblCompany,tblDepartment, EmployeeGroup" &
             " Where tblCatagory.Cat = tblEmployee.Cat And tbltimeregister.PayCode = tblEmployee.PayCode And tbltimeregister.DateOffice = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' " &
             " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode and EmployeeGroup.GroupId=tblEmployee.EmployeeGroupId and EmployeeGroup.SHIFTTYPE = 'M' " & g_HODDepartmentCode &
             " " & IIf(Len(Trim(whereClause)) = 0, "", " AND " & whereClause) & strsortorder
            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(Rs_Report)
        End If
        If Rs_Report.Tables(0).Rows.Count < 1 Then
            If Common.ReportMail = True Then
            Else
                XtraMessageBox.Show(ulf, "<size=10>No Data Available For this Report</size>", "<size=9>iAS</size>")
            End If
            Exit Sub
        End If
        rowcnt = 1

        Common.frodatetodatetoReportGrid = "MULTI SHIFT REPORT FOR DATE : " & ForDate 'DateEdit1.DateTime.ToString("dd/MM/yyyy")

        Me.Cursor = Cursors.WaitCursor
        Common.tbl = New Data.DataTable()
        Common.tbl.Columns.Add("Srl", GetType(String))
        Common.tbl.Columns.Add("PayCode", GetType(String))
        'Common.tbl.Columns.Add("Card No.", GetType(String))
        Common.tbl.Columns.Add("Employee Name", GetType(String))
        Common.tbl.Columns.Add(" ", GetType(String))
        Common.tbl.Columns.Add("Shift", GetType(String))
        Common.tbl.Columns.Add("Shift Start", GetType(String))
        Common.tbl.Columns.Add("Shift End", GetType(String))
        Common.tbl.Columns.Add("In", GetType(String))
        'Common.tbl.Columns.Add("Lunch Out", GetType(String))
        'Common.tbl.Columns.Add("Lunch In", GetType(String))
        Common.tbl.Columns.Add("Out", GetType(String))
        'Common.tbl.Columns.Add("Hrs Works", GetType(String))
        Common.tbl.Columns.Add("Status", GetType(String))
        'Common.tbl.Columns.Add("Early Arriv.", GetType(String))
        'Common.tbl.Columns.Add("Late Arriv.", GetType(String))
        'Common.tbl.Columns.Add("Shift Early", GetType(String))
        'Common.tbl.Columns.Add("Excess Lunch", GetType(String))
        'Common.tbl.Columns.Add("OT", GetType(String))
        'Common.tbl.Columns.Add("OT Amount", GetType(String))
        'Common.tbl.Columns.Add("OS", GetType(String))
        'Common.tbl.Columns.Add("Manual", GetType(String))
        'Common.tbl.Columns.Add("EMPPHOTO", GetType(Image))

        mintLine = 9
        mCount = 0
        Dim shiftStartTimeStr As String, MshiftStartTimeStr As String
        Dim shiftEndTimeStr As String, MshiftEndTimeStr As String
        Dim In1Str As String, out2Str As String, MIn1Str As String, Mout2Str As String  ', In2Str As String, out1str As String
        'Dim HOURSWORKEDStr As String, EARLYARRIVALStr As String, latearrivalStr As String
        'Dim earlydepartureStr As String, EXCLUNCHHOURSStr As String, OtDurationStr As String
        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            mCount = mCount + 1
            'Do While Not .EOF
            strOsDuration6 = 0
            strOtAmount11 = 0
            If Rs_Report.Tables(0).Rows(i).Item("OsDuration").ToString.Trim = "" Or Rs_Report.Tables(0).Rows(i).Item("OsDuration").ToString.Trim = "0" Then
            Else
                strOsDuration6 = Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("OsDuration").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("OsDuration").ToString.Trim Mod 60).ToString("00")
            End If
            If Rs_Report.Tables(0).Rows(i).Item("OTAmount").ToString.Trim = "" Or Rs_Report.Tables(0).Rows(i).Item("OTAmount").ToString.Trim = "0" Then
            Else
                strOtAmount11 = Rs_Report.Tables(0).Rows(i).Item("OTAmount").ToString.Trim
            End If

            If Rs_Report.Tables(0).Rows(i).Item("IN1MANNUAL").ToString.Trim = "" Then strIn1Mannual_1 = "" Else strIn1Mannual_1 = Rs_Report.Tables(0).Rows(i).Item("IN1MANNUAL").ToString.Trim
            If Rs_Report.Tables(0).Rows(i).Item("Out1Mannual").ToString.Trim = "" Then strOut1Mannual_1 = "" Else strOut1Mannual_1 = Rs_Report.Tables(0).Rows(i).Item("Out1Mannual").ToString.Trim
            If Rs_Report.Tables(0).Rows(i).Item("IN2MANNUAL").ToString.Trim = "" Then strIn2Mannual_1 = "" Else strIn2Mannual_1 = Rs_Report.Tables(0).Rows(i).Item("IN2MANNUAL").ToString.Trim
            If Rs_Report.Tables(0).Rows(i).Item("OUT2MANNUAL").ToString.Trim = "" Then strOut2Mannual_1 = "" Else strOut2Mannual_1 = Rs_Report.Tables(0).Rows(i).Item("OUT2MANNUAL").ToString.Trim
            Dim X As String
            If UCase(strIn1Mannual_1) = "Y" Or UCase(strOut1Mannual_1) = "Y" Or UCase(strIn2Mannual_1) = "Y" Or UCase(strOut2Mannual_1) = "Y" Then
                X = "Y"
            Else
                X = " "
            End If
            If Rs_Report.Tables(0).Rows(i).Item("shiftStartTime").ToString.Trim = "" Then
                shiftStartTimeStr = ""
            Else
                shiftStartTimeStr = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("shiftStartTime").ToString.Trim).ToString("HH:mm")
            End If

            If Rs_Report.Tables(0).Rows(i).Item("MshiftStartTime").ToString.Trim = "" Then
                MshiftStartTimeStr = ""
            Else
                MshiftStartTimeStr = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("MshiftStartTime").ToString.Trim).ToString("HH:mm")
            End If


            If Rs_Report.Tables(0).Rows(i).Item("shiftEndTime").ToString.Trim = "" Then
                shiftEndTimeStr = ""
            Else
                shiftEndTimeStr = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("shiftEndTime").ToString.Trim).ToString("HH:mm")
            End If

            If Rs_Report.Tables(0).Rows(i).Item("MshiftEndTime").ToString.Trim = "" Then
                MshiftEndTimeStr = ""
            Else
                MshiftEndTimeStr = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("MshiftEndTime").ToString.Trim).ToString("HH:mm")
            End If



            If Rs_Report.Tables(0).Rows(i).Item("In1").ToString.Trim = "" Then
                In1Str = ""
            Else
                In1Str = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("In1").ToString.Trim).ToString("HH:mm")
            End If
            If Rs_Report.Tables(0).Rows(i).Item("MIn1").ToString.Trim = "" Then
                MIn1Str = ""
            Else
                MIn1Str = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("MIn1").ToString.Trim).ToString("HH:mm")
            End If
            'If Rs_Report.Tables(0).Rows(i).Item("out1").ToString.Trim = "" Then
            '    out1str = ""
            'Else
            '    out1str = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("out1").ToString.Trim).ToString("HH:mm")
            'End If
            'If Rs_Report.Tables(0).Rows(i).Item("In2").ToString.Trim = "" Then
            '    In2Str = ""
            'Else
            '    In2Str = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("In2").ToString.Trim).ToString("HH:mm")
            'End If
            If Rs_Report.Tables(0).Rows(i).Item("out2").ToString.Trim = "" Then
                out2Str = ""
            Else
                out2Str = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("out2").ToString.Trim).ToString("HH:mm")
            End If
            If Rs_Report.Tables(0).Rows(i).Item("Mout2").ToString.Trim = "" Then
                Mout2Str = ""
            Else
                Mout2Str = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("Mout2").ToString.Trim).ToString("HH:mm")
            End If
            'If Rs_Report.Tables(0).Rows(i).Item("HOURSWORKED").ToString.Trim = "0" Or Rs_Report.Tables(0).Rows(i).Item("HOURSWORKED").ToString.Trim = "" Then
            '    HOURSWORKEDStr = ""
            'Else
            '    HOURSWORKEDStr = Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("HOURSWORKED").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("HOURSWORKED").ToString.Trim Mod 60).ToString("00")
            'End If
            'If Rs_Report.Tables(0).Rows(i).Item("EARLYARRIVAL").ToString.Trim = "0" Or Rs_Report.Tables(0).Rows(i).Item("EARLYARRIVAL").ToString.Trim = "" Then
            '    EARLYARRIVALStr = ""
            'Else
            '    EARLYARRIVALStr = Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("EARLYARRIVAL").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("EARLYARRIVAL").ToString.Trim Mod 60).ToString("00")
            'End If
            'If Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim = "0" Or Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim = "" Then
            '    latearrivalStr = ""
            'Else
            '    latearrivalStr = Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("latearrival").ToString.Trim Mod 60).ToString("00")
            'End If
            'If Rs_Report.Tables(0).Rows(i).Item("earlydeparture").ToString.Trim = "0" Or Rs_Report.Tables(0).Rows(i).Item("earlydeparture").ToString.Trim = "" Then
            '    earlydepartureStr = ""
            'Else
            '    earlydepartureStr = Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("earlydeparture").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("earlydeparture").ToString.Trim Mod 60).ToString("00")
            'End If
            'If Rs_Report.Tables(0).Rows(i).Item("EXCLUNCHHOURS").ToString.Trim = "0" Or Rs_Report.Tables(0).Rows(i).Item("EXCLUNCHHOURS").ToString.Trim = "" Then
            '    EXCLUNCHHOURSStr = ""
            'Else
            '    EXCLUNCHHOURSStr = Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("EXCLUNCHHOURS").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("EXCLUNCHHOURS").ToString.Trim Mod 60).ToString("00")
            'End If
            'If Rs_Report.Tables(0).Rows(i).Item("OtDuration").ToString.Trim = "0" Or Rs_Report.Tables(0).Rows(i).Item("OtDuration").ToString.Trim = "" Then
            '    OtDurationStr = ""
            'Else
            '    OtDurationStr = Math.Truncate(Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("OtDuration").ToString.Trim) / 60).ToString("00") & ":" & Convert.ToInt32(Rs_Report.Tables(0).Rows(i).Item("OtDuration").ToString.Trim Mod 60).ToString("00")
            'End If

            mblnCheckReport = True
            mintLine = mintLine + 1
            Common.tbl.Rows.Add(mCount, Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim, Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim, "",
            Rs_Report.Tables(0).Rows(i).Item("ShiftAttended").ToString.Trim, shiftStartTimeStr, shiftEndTimeStr, In1Str, out2Str, Rs_Report.Tables(0).Rows(i).Item("Status").ToString.Trim)
            Common.tbl.Rows.Add("", "", "", "",
           Rs_Report.Tables(0).Rows(i).Item("MShiftAttended").ToString.Trim, MshiftStartTimeStr, MshiftEndTimeStr, MIn1Str, Mout2Str, Rs_Report.Tables(0).Rows(i).Item("MStatus").ToString.Trim)

        Next
        Me.Cursor = Cursors.Default
        XtraReportGrid.ShowDialog()
    End Sub
    Sub SpotXl_Temperature(ByVal strsortorder As String)
        Dim strsql As String, mCount As Integer
        Me.Cursor = Cursors.WaitCursor
        Dim rowcnt As Integer
        If XtraShortOrder.g_SortOrder <> "" Then
            strsortorder = XtraShortOrder.g_SortOrder
        End If
        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If
        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".xls"

        mstrDepartmentCode = " "
        mblnCheckReport = False
        mintPageNo = 1
        mintLine = 1

        'Call CreateTableMachine
        'MachinePunch frmDailyAttReport.txtFromDate, frmDailyAttReport.TxtToDate ' Creating the data for the generation of the Spot reports.
        Dim fromdate As DateTime = Convert.ToDateTime(DateEdit1.DateTime.ToString("yyyy-MM-dd") & " " & TextFromTime.Text.Trim & ":00")
        Dim Todate As DateTime = Convert.ToDateTime(DateEdit1.DateTime.ToString("yyyy-MM-dd") & " " & TextToTime.Text.Trim & ":59")

        If Common.servername = "Access" Then

            strsql = "Select tblEmployee.empname,tblCatagory.Catagoryname, tblDepartment.DepartmentCode, tblDepartment.DepartmentName, " &
                     " tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, tblEmployee.DepartmentCode , machinerawpunch.*,tblmachine.branch from tblCatagory, tblDepartment, tblEmployee ,machinerawpunch,tblmachine where FORMAT(machinerawpunch.OfficePunch, 'yyyy-MM-dd HH:mm:ss') between '" & fromdate.ToString("yyyy-MM-dd HH:mm:ss") & "' and '" & Todate.ToString("yyyy-MM-dd HH:mm:ss") & "' and tblEmployee.departmentcode = tbldepartment.departmentcode And tblEmployee.CAT = tblCatagory.cat And " &
                     " MachineRawPunch.Paycode = tblEmployee.Paycode " & g_HODDepartmentCode &
                     " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause)

            adapA = New OleDbDataAdapter(strsql, Common.con1)
            adapA.Fill(Rs_Report)
        Else
            strsql = "Select tblEmployee.empname,tblCatagory.Catagoryname, tblDepartment.DepartmentCode, tblDepartment.DepartmentName,  " &
                     " tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, tblEmployee.DepartmentCode , machinerawpunch.*,tblmachine.branch from tblCatagory, tblDepartment, tblEmployee ,machinerawpunch,tblmachine where machinerawpunch.officepunch between '" & fromdate.ToString("yyyy-MM-dd HH:mm:ss") & "' and '" & Todate.ToString("yyyy-MM-dd HH:mm:ss") & "' and tblEmployee.departmentcode = tbldepartment.departmentcode And tblEmployee.CAT = tblCatagory.cat And " &
                     " MachineRawPunch.Paycode = tblEmployee.Paycode " & g_HODDepartmentCode &
                     " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause)

            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(Rs_Report)
        End If

        If Rs_Report.Tables(0).Rows.Count = 0 Then
            Me.Cursor = Cursors.Default
            XtraMessageBox.Show(ulf, "<size=10>No Recode Found.</size>", "<size=9>iAS</size>")
            Exit Sub
        End If
        rowcnt = 1

        Common.frodatetodatetoReportGrid = "TEMPERATURE REPORT  FOR " & fromdate.ToString("dd/MM/yyyy")


        Common.tbl = New Data.DataTable()
        Common.tbl.Columns.Add("User No", GetType(String))
        Common.tbl.Columns.Add("Emp. ID", GetType(String))
        Common.tbl.Columns.Add("Employee Name", GetType(String))
        Common.tbl.Columns.Add("Date", GetType(String))
        Common.tbl.Columns.Add("Time", GetType(String))
        Common.tbl.Columns.Add("Location", GetType(String))
        Common.tbl.Columns.Add("Machine No", GetType(String))
        Common.tbl.Columns.Add("Temperature in C", GetType(String))
        Common.tbl.Columns.Add("Temperature in F", GetType(String))

        mCount = 0
        Dim strPayCode As String = ""
        Dim paycodelist As New List(Of String)()
        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1  'Do While Not .EOF
            paycodelist.Add(Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim)
            Dim Ctemp As String = 0
            Dim Ftemp As String = 0

            If Rs_Report.Tables(0).Rows(i).Item("Temperature").ToString.Trim <> "" Then
                Ctemp = Rs_Report.Tables(0).Rows(i).Item("Temperature").ToString.Trim
                Ftemp = (Ctemp * 9 / 5) + 32
            End If

            Common.tbl.Rows.Add(Rs_Report.Tables(0).Rows(i).Item("CardNo").ToString.Trim, Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim,
                    Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim, Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("officepunch").ToString.Trim).ToString("dd/MM/yyyy"),
                    Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("officepunch").ToString.Trim).ToString("HH:mm:ss"), Rs_Report.Tables(0).Rows(i).Item("Branch").ToString.Trim,
                    Rs_Report.Tables(0).Rows(i).Item("MC_No").ToString.Trim, Ctemp, Ftemp)
        Next ' Loop

        'Dim totalInCount As Double = 0
        'Dim totalOutCount As Double = 0
        'Dim paycodeArray = paycodelist.Distinct.ToArray
        'For i As Integer = 0 To paycodeArray.Length - 1
        '    Dim count As Double = 0
        '    For j As Integer = 0 To Rs_Report.Tables.Count - 1
        '        If paycodeArray(i) = Rs_Report.Tables(0).Rows(j).Item("paycode").ToString.Trim Then
        '            count = count + 1
        '        End If
        '    Next
        '    If count Mod 2 = 0 Then
        '        totalInCount = totalInCount + 1
        '    Else
        '        totalOutCount = totalOutCount + 1
        '    End If
        'Next
        'Common.tbl.Rows.Add("")
        'Common.tbl.Rows.Add("Total In Campus", totalInCount)
        'Common.tbl.Rows.Add("Total Out Campus", totalOutCount)
        Me.Cursor = Cursors.Default
        XtraReportGrid.ShowDialog()
    End Sub
 Sub DailyXl_MachineRawPunchImageVisitor(ByVal strsortorder As String)
        Me.Cursor = Cursors.WaitCursor
        XtraMasterTest.LabelControlStatus.Text = "Geting Punches and Picture Data..."

        Dim intFile As Integer
        Dim strsql As String, mCount As Integer
        Dim xlapp As Excel.Application
        Dim xlwb As Excel.Workbook
        Dim rowcnt As Integer
        xlapp = CreateObject("Excel.Application")
        xlwb = xlapp.Workbooks.Add
        'Me.Cursor = Cursors.WaitCursor
        If XtraShortOrder.g_SortOrder <> "" Then
            strsortorder = XtraShortOrder.g_SortOrder
        End If
        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If
        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".xls"

        'If Left(strsortorder, 1) = "D" Or Left(strsortorder, 1) = "C" Then blnDeptAvailable = True
        mblnCheckReport = False
        mintPageNo = 1
        mintLine = 1
        'If g_Report_view Then
        If Common.servername = "Access" Then
            strsql = " Select * From MachineRawPunch " &
                    " Where OfficePunch between #" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & " 00:00# And " &
                    " #" & DateEdit2.DateTime.ToString("yyyy-MM-dd") & " 23:59# and CARDNO='Visitor' order by OFFICEPUNCH"
            adapA = New OleDbDataAdapter(strsql, Common.con1)
            adapA.Fill(Rs_Report)
        Else
            strsql = " Select * From MachineRawPunch " &
                    " Where OfficePunch Between '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & " 00:00' And " &
                    " '" & DateEdit2.DateTime.ToString("yyyy-MM-dd") & " 23:59' and CARDNO='Visitor' order by OFFICEPUNCH"
            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(Rs_Report)
        End If
        If Rs_Report.Tables(0).Rows.Count < 1 Then
            Me.Cursor = Cursors.Default
            XtraMasterTest.LabelControlStatus.Text = ""
            XtraMessageBox.Show(ulf, "<size=10>No Data Available For this Report.</size>", "<size=9>ULtra</size>")
            Exit Sub
        End If

        rowcnt = 1
        'xlapp.Visible = True
        xlapp.Columns.Font.Name = "TAHOMA"
        xlapp.Cells(rowcnt, 4).Font.Bold = True

        xlapp.Range("B1:F1").Merge()
        xlapp.Cells(rowcnt, 2) = "Company Name:" & CommonReport.g_CompanyNames
        rowcnt = rowcnt + 1

        xlapp.Range("B2:F2").Merge()
        xlapp.Cells(rowcnt, 2).Font.Bold = True
        xlapp.Cells(rowcnt, 2) = "Run Date & Time :" & Now.ToString("dd/MM/yyyy HH:mm")

        rowcnt = rowcnt + 2
        xlapp.Cells.Font.Size = 8
        xlapp.Cells(rowcnt, 2).Font.Bold = True

        xlapp.Range("B4:F4").Merge()
        xlapp.Cells(rowcnt, 2) = "DAILY VISITOR TEMPERATURE REPORT FOR DATE : " & DateEdit1.DateTime.ToString("dd/MM/yyyy")
        rowcnt = rowcnt + 1
        rowcnt = rowcnt + 1
        xlapp.Columns.ColumnWidth = 9
        xlapp.Columns.Font.Name = "TAHOMA"
        xlapp.Columns.Font.Size = 8
        xlapp.Columns.Font.ColorIndex = 1 'vbBlack
        ' xlapp.Cells(rowcnt, 1) = "User Number"
        ' xlapp.Cells.Font.Bold = False
        xlapp.Range("A6:J6").Font.Bold = True
        xlapp.Range("A6:J6").Font.ColorIndex = 2 'vbWhite
        'xlapp.Range("A6:U6").HorizontalAlignment = xlLeft
        xlapp.Range("A6:G6").Interior.ColorIndex = 51
        xlapp.Range("A6:G6").Borders.LineStyle = XlLineStyle.xlContinuous


        xlapp.Cells(rowcnt, 1) = "Sr.No."
        'xlapp.Cells(rowcnt, 2) = "PayCode"
        'xlapp.Cells(rowcnt, 3) = "Employee Name"
        'xlapp.Cells(rowcnt, 4) = "Card No"
        xlapp.Cells(rowcnt, 2) = "Punch Time"
        xlapp.Cells(rowcnt, 3) = "Temperature"
        xlapp.Cells(rowcnt, 4) = "MaskStatus"
        xlapp.Cells(rowcnt, 5) = "IsAbnomal"
        xlapp.Cells(rowcnt, 6) = "Normal Image"
        xlapp.Cells(rowcnt, 7) = "Thermal Image "
        'rowcnt = rowcnt + 1
        mintLine = 9
        mCount = 0
        'xlapp.Range("F6:G6").Merge()

        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            mCount = mCount + 1
            rowcnt = rowcnt + 1
            xlapp.Rows(rowcnt).RowHeight = 80
            xlapp.Cells(rowcnt, 1).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 1) = mCount
            xlapp.Cells(rowcnt, 1).ColumnWidth = 5
            xlapp.Cells(rowcnt, 1).CurrentRegion.HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter
            xlapp.Cells(rowcnt, 1).CurrentRegion.VerticalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter

            'xlapp.Cells(rowcnt, 2).Borders.LineStyle = XlLineStyle.xlContinuous
            'xlapp.Cells(rowcnt, 2) = "'" & Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim
            'xlapp.Cells(rowcnt, 3).Borders.LineStyle = XlLineStyle.xlContinuous
            'xlapp.Cells(rowcnt, 3) = Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim
            'xlapp.Cells(rowcnt, 3).Style.WrapText = True
            'xlapp.Cells(rowcnt, 3).CurrentRegion.HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter
            'xlapp.Cells(rowcnt, 3).CurrentRegion.VerticalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter

            'xlapp.Cells(rowcnt, 4).Borders.LineStyle = XlLineStyle.xlContinuous
            'xlapp.Cells(rowcnt, 4) = Rs_Report.Tables(0).Rows(i).Item("CARDNO").ToString.Trim

            xlapp.Cells(rowcnt, 2).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 2) = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("OFFICEPUNCH").ToString.Trim).ToString("HH:mm:ss")
            xlapp.Cells(rowcnt, 2).CurrentRegion.HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter
            xlapp.Cells(rowcnt, 2).CurrentRegion.VerticalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter

            xlapp.Cells(rowcnt, 3).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 3) = Rs_Report.Tables(0).Rows(i).Item("Temperature").ToString.Trim
            xlapp.Cells(rowcnt, 3).CurrentRegion.HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter
            xlapp.Cells(rowcnt, 3).CurrentRegion.VerticalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter

            xlapp.Cells(rowcnt, 4).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 4) = Rs_Report.Tables(0).Rows(i).Item("MaskStatus").ToString.Trim
            xlapp.Cells(rowcnt, 4).CurrentRegion.HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter
            xlapp.Cells(rowcnt, 4).CurrentRegion.VerticalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter

            xlapp.Cells(rowcnt, 5).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 5) = Rs_Report.Tables(0).Rows(i).Item("IsAbnomal").ToString.Trim
            xlapp.Cells(rowcnt, 5).CurrentRegion.HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter
            xlapp.Cells(rowcnt, 5).CurrentRegion.VerticalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter

            'xlapp.Cells(rowcnt, 6).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 6).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 6).ColumnWidth = 20
            'xlapp.Range("F" & rowcnt & ":" & "G" & rowcnt).Merge()
            'xlapp.Range("F" & rowcnt & ":" & "G" & rowcnt).Borders.LineStyle = XlLineStyle.xlContinuous
            Dim cardNo As String = Rs_Report.Tables(0).Rows(i).Item("CARDNO").ToString.Trim
            If IsNumeric(cardNo) Then
                cardNo = Convert.ToDouble(Rs_Report.Tables(0).Rows(i).Item("CARDNO").ToString.Trim)
            End If
            Try
                'Dim PicPath As String = My.Application.Info.DirectoryPath & "\RealTimeImages\Light" & cardNo & "_" & Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("mdate").ToString.Trim).ToString("yyyyMMddHHmmss") & ".bmp"
                Dim PicPath As String = My.Application.Info.DirectoryPath & "\RealTimeImages\Light_" & Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("OFFICEPUNCH").ToString.Trim).ToString("yyyyMMddHHmmss") & ".bmp"

                With xlapp.ActiveSheet.Pictures.Insert(PicPath)
                    With .ShapeRange
                        '.LockAspectRatio = msoTrue
                        .Width = 110
                        .Height = 75
                    End With
                    .Left = xlapp.ActiveSheet.Cells(rowcnt, 6).Left
                    .Top = xlapp.ActiveSheet.Cells(rowcnt, 6).Top
                    .Placement = 1
                    .PrintObject = True
                End With
            Catch ex As Exception
                xlapp.Cells(rowcnt, 6) = ""
            End Try
            xlapp.Cells(rowcnt, 7).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 7).ColumnWidth = 10
            Try
                Dim PicPath As String = My.Application.Info.DirectoryPath & "\RealTimeImages\Thermal_" & Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("OFFICEPUNCH").ToString.Trim).ToString("yyyyMMddHHmmss") & ".bmp"
                With xlapp.ActiveSheet.Pictures.Insert(PicPath)
                    With .ShapeRange
                        '.LockAspectRatio = msoTrue
                        .Width = 110
                        .Height = 75
                    End With
                    .Left = xlapp.ActiveSheet.Cells(rowcnt, 7).Left
                    .Top = xlapp.ActiveSheet.Cells(rowcnt, 7).Top
                    .Placement = 1
                    .PrintObject = True
                End With
            Catch ex As Exception
                xlapp.Cells(rowcnt, 7) = ""
            End Try
            mblnCheckReport = True
            mintLine = mintLine + 1
        Next

        Me.Cursor = Cursors.Default
        xlwb.SaveAs(mstrFile_Name)
        xlwb.Close()
        XL.ExcelPDF(mstrFile_Name)
        XtraMasterTest.LabelControlStatus.Text = ""
        Process.Start(mstrFile_Name & ".pdf")
        Me.Cursor = Cursors.Default
    End Sub

    Sub DailyXl_MachineRawPunchImageVisitorGrid(ByVal strsortorder As String)
        Me.Cursor = Cursors.WaitCursor
        XtraMasterTest.LabelControlStatus.Text = "Geting Punches and Picture Data..."
        System.Windows.Forms.Application.DoEvents()
        Dim strsql As String, mCount As Integer
        Dim rowcnt As Integer
        If XtraShortOrder.g_SortOrder <> "" Then
            strsortorder = XtraShortOrder.g_SortOrder
        End If
        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If

        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".xls"

        'If Left(strsortorder, 1) = "D" Or Left(strsortorder, 1) = "C" Then blnDeptAvailable = True
        mblnCheckReport = False
        mintPageNo = 1
        mintLine = 1
        'If g_Report_view Then
        If Common.servername = "Access" Then
            strsql = " Select * From MachineRawPunch " &
                    " Where OfficePunch between #" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & " 00:00# And " &
                    " #" & DateEdit2.DateTime.ToString("yyyy-MM-dd") & " 23:59# and CARDNO='Visitor' order by OFFICEPUNCH"
            adapA = New OleDbDataAdapter(strsql, Common.con1)
            adapA.Fill(Rs_Report)
        Else
            strsql = " Select * From MachineRawPunch " &
                    " Where OfficePunch Between '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & " 00:00' And " &
                    " '" & DateEdit2.DateTime.ToString("yyyy-MM-dd") & " 23:59' and CARDNO='Visitor' order by OFFICEPUNCH"
            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(Rs_Report)
        End If
        If Rs_Report.Tables(0).Rows.Count < 1 Then
            Me.Cursor = Cursors.Default
            XtraMasterTest.LabelControlStatus.Text = ""
            XtraMessageBox.Show(ulf, "<size=10>No Data Available For this Report.</size>", "<size=9>ULtra</size>")
            Exit Sub
        End If

        rowcnt = 1

        Common.frodatetodatetoReportGrid = "DAILY VISITOR TEMPERATURE REPORT FROM DATE: " & DateEdit1.DateTime.ToString("dd/MM/yyyy") & " TO DATE: " & DateEdit2.DateTime.ToString("dd/MM/yyyy")
        Me.Cursor = Cursors.WaitCursor
        Common.tbl = New Data.DataTable()
        Common.tbl.Columns.Add("Sr.No.", GetType(String))
        Common.tbl.Columns.Add("Punch Time", GetType(String))
        Common.tbl.Columns.Add("Temp C", GetType(String))
        Common.tbl.Columns.Add("Temp F", GetType(String))
        Common.tbl.Columns.Add("MaskStatus", GetType(String))
        Common.tbl.Columns.Add("IsAbnomal", GetType(String))
        Common.tbl.Columns.Add("Normal Image", GetType(Image))
        Common.tbl.Columns.Add("Thermal Image", GetType(Image))

        mCount = 0

        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            mCount = mCount + 1
            rowcnt = rowcnt + 1

            Dim PunchTime As String = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("OFFICEPUNCH").ToString.Trim).ToString("HH:mm:ss")
            Dim Temperature As String = Rs_Report.Tables(0).Rows(i).Item("Temperature").ToString.Trim
            Dim MaskStatus As String = Rs_Report.Tables(0).Rows(i).Item("MaskStatus").ToString.Trim
            Dim IsAbnomal As String = Rs_Report.Tables(0).Rows(i).Item("IsAbnomal").ToString.Trim
            Dim NormalImage As Image
            Dim ThermalImage As Image

            Dim Ctemp As String = 0
            Dim Ftemp As String = 0

            If Rs_Report.Tables(0).Rows(i).Item("Temperature").ToString.Trim <> "" Then
                Ctemp = Rs_Report.Tables(0).Rows(i).Item("Temperature").ToString.Trim
                Ftemp = (Ctemp * 9 / 5) + 32

                If Rs_Report.Tables(0).Rows(i).Item("Temperature").ToString.Trim > 80 Then
                    Ftemp = Rs_Report.Tables(0).Rows(i).Item("Temperature").ToString.Trim
                    Ctemp = Math.Round((Rs_Report.Tables(0).Rows(i).Item("Temperature").ToString.Trim - 32) * 5 / 9, 2)
                ElseIf Rs_Report.Tables(0).Rows(i).Item("Temperature").ToString.Trim <= 80 Then
                    Ctemp = Rs_Report.Tables(0).Rows(i).Item("Temperature").ToString.Trim
                    Ftemp = Math.Round((Rs_Report.Tables(0).Rows(i).Item("Temperature").ToString.Trim * 9 / 5) + 32, 2)
                ElseIf Rs_Report.Tables(0).Rows(i).Item("Temperature").ToString.Trim.Trim = "" Then
                    Ftemp = ""
                    Ctemp = ""
                End If

            End If

            Dim cardNo As String = Rs_Report.Tables(0).Rows(i).Item("CARDNO").ToString.Trim
            If IsNumeric(cardNo) Then
                cardNo = Convert.ToDouble(Rs_Report.Tables(0).Rows(i).Item("CARDNO").ToString.Trim)
            End If
            Try
                'Dim PicPath As String = My.Application.Info.DirectoryPath & "\RealTimeImages\Light" & cardNo & "_" & Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("mdate").ToString.Trim).ToString("yyyyMMddHHmmss") & ".bmp"
                NormalImage = Image.FromFile(My.Application.Info.DirectoryPath & "\RealTimeImages\Light_" & Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("OFFICEPUNCH").ToString.Trim).ToString("yyyyMMddHHmmss") & ".bmp")
            Catch ex As Exception
                NormalImage = Nothing
            End Try

            Try
                ThermalImage = Image.FromFile(My.Application.Info.DirectoryPath & "\RealTimeImages\Thermal_" & Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("OFFICEPUNCH").ToString.Trim).ToString("yyyyMMddHHmmss") & ".bmp")
            Catch ex As Exception
                ThermalImage = Nothing
            End Try
            Common.tbl.Rows.Add(mCount, PunchTime, Ctemp, Ftemp, MaskStatus, IsAbnomal, NormalImage, ThermalImage)

            mblnCheckReport = True
            mintLine = mintLine + 1
        Next
        XtraMasterTest.LabelControlStatus.Text = ""
        System.Windows.Forms.Application.DoEvents()
        Me.Cursor = Cursors.Default
        XtraReportGrid.ShowDialog()
        'If CheckPDF.Checked Then
        '    XL.ExcelPDF(mstrFile_Name)
        '    Process.Start(mstrFile_Name & ".pdf")
        'Else
        '    Process.Start(mstrFile_Name)
        'End If

        XtraMasterTest.LabelControlStatus.Text = ""
        System.Windows.Forms.Application.DoEvents()
        Me.Cursor = Cursors.Default
    End Sub
    Private Sub CheckContinuousLateArrival_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckContinuousLateArrival.CheckedChanged
        selectionChange()
    End Sub
    Private Sub CheckContinuousEarlyDeparture_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckContinuousEarlyDeparture.CheckedChanged
        selectionChange()
    End Sub
    Private Sub CheckContinuousAbsenteeism_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckContinuousAbsenteeism.CheckedChanged
        selectionChange()
    End Sub
    Private Sub CheckMachineRawPunch_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckMachineRawPunch.CheckedChanged
        If CheckMachineRawPunch.Checked = True Then
            CheckExcel.Visible = True
            LabelControl3.Visible = True
            If Common.IsNepali = "Y" Then
                DateEdit2.Visible = False
                ComboNepaliDateTo.Visible = True
                ComboNEpaliMonthTo.Visible = True
                ComboNepaliYearTo.Visible = True
            Else
                DateEdit2.Visible = True
                ComboNepaliDateTo.Visible = False
                ComboNEpaliMonthTo.Visible = False
                ComboNepaliYearTo.Visible = False
            End If
            'DateEdit2.Visible = True
            'DateEdit2.DateTime = Now
            Exit Sub
        Else
            CheckExcel.Visible = True
            LabelControl3.Visible = False
            DateEdit2.Visible = False
            ComboNepaliDateTo.Visible = False
            ComboNEpaliMonthTo.Visible = False
            ComboNepaliYearTo.Visible = False
            Exit Sub
        End If
    End Sub
    Private Sub CheckManualPunchAudit_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckManualPunchAudit.CheckedChanged
        If CheckManualPunchAudit.Checked = True Then
            LabelControl3.Visible = True
            DateEdit2.Visible = True
            DateEdit2.DateTime = Now
            Exit Sub
        Else
            LabelControl3.Visible = False
            DateEdit2.Visible = False
            Exit Sub
        End If
    End Sub
    Function Length7(ByVal dblA As Double) As String
        Dim strB As String '* 7
        strB = Format(dblA, "0.00")
        Length7 = strB  '.Substring(0, 6)
        'MsgBox("Length7 " & Length7)
    End Function
    Function Length5(ByVal dblA As Double) As String
        Dim strB As String '* 5
        strB = Format(dblA, "0.00")
        Length5 = strB  '.Substring(0, 5)
        'MsgBox("Length5 " & Length5)
    End Function

    Sub CreateTableMachine()
        'On Error GoTo ErrorGen
        'On Local Error Resume Next
        Dim strsql As String
        Dim rsSysObjects As DataSet = New DataSet 'ADODB.Recordset
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter

        'Removing the table if it is already in database    START
        If Common.servername = "Access" Then
            strsql = "Drop table " & g_MachineRawPunch
            cmd1 = New OleDbCommand(strsql, Common.con1)
            cmd1.ExecuteNonQuery()
            'Cn.Execute(strsql)
        Else
            strsql = "Select name from sysObjects where name = '" & g_MachineRawPunch & "'"
            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(rsSysObjects)
            'rsSysObjects = Cn.Execute(strsql)
            If rsSysObjects.Tables(0).Rows.Count > 0 Then
                strsql = "Drop table " & g_MachineRawPunch
                cmd = New SqlCommand(strsql, Common.con)
                cmd.ExecuteNonQuery()
                'Cn.Execute(strsql)
            End If
            'rsSysObjects.Close()
        End If
        'Removing the table if it is already in database    End

        strsql = "Create Table " & g_MachineRawPunch & _
            "(PAYCODE Char(10) Null, CARDNO Char(8) Null, " & _
            " EMPNAME Char(25) Null, PUNCH1 Char(5) Null, " & _
            " PUNCH2 Char(5) Null, PUNCH3 Char(5) Null, " & _
            " PUNCH4 Char(5) Null, DATEOFFICE DateTime Null, " & _
            " PUNCH1M Char(3) Null, PUNCH2M Char(3) Null, " & _
            " PUNCH3M Char(3) Null, PUNCH4M Char(3) Null, " & _
            " COMPANYCODE Char(3) Null, DEPARTMENTCODE Char(3) Null, " & _
            " CAT Char(3) Null,INOUT Char(1) Null)"

        If Common.servername = "Access" Then
            Common.con1.Open()
            cmd1 = New OleDbCommand(strsql, Common.con1)
            cmd1.ExecuteNonQuery()
            Common.con1.Close()
        Else
            Common.con.Open()
            cmd = New SqlCommand(strsql, Common.con)
            cmd.ExecuteNonQuery()
            Common.con.Close()
        End If
        'Cn.Execute(strsql)
        '        Exit Sub
        'ErrorGen:
        '        MsgBox(Err.Description, vbInformation, "TimeWatch")
        '        Screen.MousePointer = vbDefault
    End Sub
    Sub DropTableMachine()
        'On Error GoTo ErrorGen
        Dim strsql As String
        strsql = "Drop Table " & g_MachineRawPunch
        If Common.servername = "Access" Then
            Common.con1.Open()
            cmd1 = New OleDbCommand(strsql, Common.con1)
            cmd1.ExecuteNonQuery()
            Common.con1.Close()
        Else
            Common.con.Open()
            cmd = New SqlCommand(strsql, Common.con)
            cmd.ExecuteNonQuery()
            Common.con.Close()
        End If
        'Cn.Execute(strsql)
        '        Exit Sub
        'ErrorGen:
        '        MsgBox(Err.Description, vbInformation, "TimeWatch")
        'Screen.MousePointer = vbDefault
    End Sub
    Sub MachinePunch(mFirstDate As Date, mSecondDate As Date)
        'On Error GoTo ErrorGen
        Dim rsEmp As DataSet = New DataSet 'ADODB.Recordset, 
        Dim rsMrp As DataSet = New DataSet 'ADODB.Recordset
        Dim RS As DataSet = New DataSet
        Dim mF_Date As Date
        Dim mS_Date As Date

        Dim sSql As String = "Select * From " & g_MachineRawPunch
        rsMrp = New DataSet 'ADODB.Recordset

        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter

        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(rsMrp)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(rsMrp)
        End If
        'rsMrp.Open(sSql, Cn, adOpenKeyset, adLockBatchOptimistic)

        sSql = "Select PayCode, PresentCardNo, EmpName, CompanyCode, DepartmentCode, Cat From tblEmployee Order By PayCode"
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(rsEmp)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(rsEmp)
        End If
        'rsEmp = Cn.Execute(sSql)

        'With rsEmp
        '    .MoveFirst()
        '    Do While Not .EOF
        For i As Integer = 0 To rsEmp.Tables(0).Rows.Count - 1
            mF_Date = mFirstDate
            mS_Date = mSecondDate
            Do While mF_Date <= mS_Date
                If Common.servername = "Access" Then
                    sSql = "Select * from MachineRawPunch Where " & _
                        " OfficePunch between #" & mF_Date.ToString("dd-MM-yyyy 00:00:00") & "# and #" & mF_Date.ToString("dd-MM-yyyy 23:59:59") & _
                        "# And paycode = '" & rsEmp.Tables(0).Rows(i).Item("paycode").ToString.Trim & "' Order By  OfficePunch"
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(RS)
                Else
                    sSql = "Select * from MachineRawPunch Where " & _
                            " OfficePunch between '" & mF_Date.ToString("dd-MM-yyyy 00:00:00") & "' and '" & mF_Date.ToString("dd-MM-yyyy 23:59:59") & _
                            "' And paycode = '" & rsEmp.Tables(0).Rows(i).Item("paycode").ToString.Trim & "' Order By  OfficePunch"
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(RS)
                End If
                'Rs = Cn.Execute(sSql)

                Dim paycode As String
                Dim CardNo As String
                Dim EmpName As String
                Dim companycode As String
                Dim DepartmentCode As String
                Dim Cat As String
                Dim DATEOFFICE As String
                Dim PUNCH1 As String
                Dim punch1m As String
                Dim PUNCH2 As String = ""
                Dim punch2m As String = ""
                Dim PUNCH3 As String = ""
                Dim punch3m As String = ""
                Dim PUNCH4 As String = ""
                Dim punch4m As String = ""
                For j As Integer = 0 To RS.Tables(0).Rows.Count - 1
                    'Do While Not RS.EOF
                    paycode = rsEmp.Tables(0).Rows(i).Item("paycode").ToString.Trim
                    CardNo = rsEmp.Tables(0).Rows(i).Item("presentcardno").ToString.Trim
                    EmpName = rsEmp.Tables(0).Rows(i).Item("EmpName").ToString.Trim
                    companycode = rsEmp.Tables(0).Rows(i).Item("companycode").ToString.Trim
                    DepartmentCode = rsEmp.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim
                    Cat = rsEmp.Tables(0).Rows(i).Item("Cat").ToString.Trim
                    DATEOFFICE = Convert.ToDateTime(RS.Tables(0).Rows(i).Item("officepunch").ToString.Trim).ToString("DD/MM/YYYY")
                    PUNCH1 = Convert.ToDateTime(RS.Tables(0).Rows(i).Item("officepunch").ToString.Trim).ToString("HH:mm")
                    punch1m = IIf(RS.Tables(0).Rows(i).Item("IsManual").ToString.Trim = "Y", "M", " ") & "-" & IIf(RS.Tables(0).Rows(i).Item("inout").ToString.Trim = "", RS.Tables(0).Rows(i).Item("inout").ToString.Trim, " ")

                    'RS.MoveNext()
                    Continue For
                    If j > RS.Tables(0).Rows.Count - 1 Then
                        PUNCH2 = Convert.ToDateTime(RS.Tables(0).Rows(i).Item("officepunch").ToString.Trim).ToString("HH:mm")
                        punch2m = IIf(RS.Tables(0).Rows(i).Item("IsManual").ToString.Trim = "Y", "M", " ") & "-" & IIf(RS.Tables(0).Rows(i).Item("inout").ToString.Trim = "", RS.Tables(0).Rows(i).Item("inout").ToString.Trim, " ")
                    Else
                        'rsMrp.Update()
                        Exit For
                    End If
                    'RS.MoveNext()
                    Continue For
                    If j > RS.Tables(0).Rows.Count - 1 Then
                        PUNCH3 = Convert.ToDateTime(RS.Tables(0).Rows(i).Item("officepunch").ToString.Trim).ToString("HH:mm")
                        punch3m = IIf(RS.Tables(0).Rows(i).Item("IsManual").ToString.Trim = "Y", "M", " ") & "-" & IIf(RS.Tables(0).Rows(i).Item("inout").ToString.Trim = "", RS.Tables(0).Rows(i).Item("inout").ToString.Trim, " ")
                    Else
                        'rsMrp.Update()
                        Exit For
                    End If
                    'RS.MoveNext()
                    Continue For
                    If j > RS.Tables(0).Rows.Count - 1 Then
                        PUNCH4 = Convert.ToDateTime(RS.Tables(0).Rows(i).Item("officepunch").ToString.Trim).ToString("HH:mm")
                        punch4m = IIf(RS.Tables(0).Rows(i).Item("IsManual").ToString.Trim = "Y", "M", " ") & "-" & IIf(RS.Tables(0).Rows(i).Item("inout").ToString.Trim = "", RS.Tables(0).Rows(i).Item("inout").ToString.Trim, " ")
                    Else
                        'rsMrp.Update()
                        Exit For
                    End If
                    'RS.MoveNext()
                    'rsMrp.Update()
                    'Loop
                Next

                Dim sSqltmp As String = "insert into " & g_MachineRawPunch & " (paycode,CardNo,EmpName,companycode,DepartmentCode,Cat,DATEOFFICE,PUNCH1,punch1m,PUNCH2,punch2m,PUNCH3,punch3m,PUNCH4,punch4m)" & _
                    "values('" & paycode & "','" & CardNo & "','" & EmpName & "','" & companycode & "','" & DepartmentCode & "','" & Cat & "','" & DATEOFFICE & "','" & PUNCH1 & "','" & punch1m & "','" & PUNCH2 & "','" & punch2m & "','" & PUNCH3 & "','" & punch3m & "','" & PUNCH4 & "','" & punch4m & "')"
                If Common.servername = "Access" Then
                    Common.con1.Open()
                    cmd1 = New OleDbCommand(sSqltmp, Common.con1)
                    cmd1.ExecuteNonQuery()
                    Common.con1.Close()
                Else
                    Common.con.Open()
                    cmd = New SqlCommand(sSqltmp, Common.con)
                    cmd.ExecuteNonQuery()
                    Common.con.Close()
                End If
                mF_Date = mF_Date.AddDays(1)
            Loop
            '.MoveNext()
            'Loop
        Next
        'End With
        'rsMrp.UpdateBatch()
        'rsMrp.Close()
        'rsEmp.Close()

        '        Exit Sub
        'ErrorGen:
        '        MsgBox(Err.Description, vbInformation, "TimeWatch")
        '        Screen.MousePointer = vbDefault
    End Sub
    'Sub SpotXl_Temperature(ByVal strsortorder As String)
    '    Dim strsql As String, mCount As Integer
    '    Me.Cursor = Cursors.WaitCursor
    '    Dim rowcnt As Integer
    '    If XtraShortOrder.g_SortOrder <> "" Then
    '        strsortorder = XtraShortOrder.g_SortOrder
    '    End If
    '    If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
    '        CommonReport.GetCompanies()
    '    End If
    '    Dim g_HODDepartmentCode As String = "" 'nitin
    '    Dim adap As SqlDataAdapter
    '    Dim adapA As OleDbDataAdapter
    '    Dim Rs_Report As DataSet = New DataSet
    '    mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".xls"

    '    mstrDepartmentCode = " "
    '    mblnCheckReport = False
    '    mintPageNo = 1
    '    mintLine = 1

    '    'Call CreateTableMachine
    '    'MachinePunch frmDailyAttReport.txtFromDate, frmDailyAttReport.TxtToDate ' Creating the data for the generation of the Spot reports.
    '    Dim fromdate As DateTime = Convert.ToDateTime(DateEdit1.DateTime.ToString("yyyy-MM-dd") & " " & TextFromTime.Text.Trim & ":00")
    '    Dim Todate As DateTime = Convert.ToDateTime(DateEdit2.DateTime.ToString("yyyy-MM-dd") & " " & TextToTime.Text.Trim & ":59")

    '    Dim conS As SqlConnection
    '    Dim conS1 As System.Data.OleDb.OleDbConnection
    '    If Common.servername = "Access" Then
    '        conS1 = New System.Data.OleDb.OleDbConnection(Common.ConnectionString)
    '    Else
    '        conS = New SqlConnection(Common.ConnectionString)
    '    End If

    '    If Common.servername = "Access" Then

    '        strsql = "Select tblEmployee.empname,tblCatagory.Catagoryname, tblDepartment.DepartmentCode, tblDepartment.DepartmentName, " &
    '                 " tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, tblEmployee.DepartmentCode , MachineRawPunchAll.*,tblmachine.branch from tblCatagory, tblDepartment, tblEmployee ,MachineRawPunchAll,tblmachine where FORMAT(MachineRawPunchAll.OfficePunch, 'yyyy-MM-dd HH:mm:ss') between '" & fromdate.ToString("yyyy-MM-dd 00:00:00") & "' and '" & Todate.ToString("yyyy-MM-dd 23:59:59") & "' and tblEmployee.departmentcode = tbldepartment.departmentcode And tblEmployee.CAT = tblCatagory.cat And   " &
    '                 " MachineRawPunchAll.Paycode = tblEmployee.Paycode " & g_HODDepartmentCode &
    '                 " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause)

    '        adapA = New OleDbDataAdapter(strsql, conS1)
    '        adapA.Fill(Rs_Report)
    '    Else
    '        strsql = "Select tblEmployee.empname,tblCatagory.Catagoryname, tblDepartment.DepartmentCode, tblDepartment.DepartmentName,  " &
    '                 " tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat, tblEmployee.DepartmentCode , MachineRawPunchAll.*,tblmachine.branch from tblCatagory, tblDepartment, tblEmployee ,MachineRawPunchAll,tblmachine where MachineRawPunchAll.officepunch between '" & fromdate.ToString("yyyy-MM-dd 00:00:00") & "' and '" & Todate.ToString("yyyy-MM-dd 23:59:59") & "' and tblEmployee.departmentcode = tbldepartment.departmentcode And tblEmployee.CAT = tblCatagory.cat And " &
    '                 " MachineRawPunchAll.Paycode = tblEmployee.Paycode " & g_HODDepartmentCode &
    '                 " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause)

    '        adap = New SqlDataAdapter(strsql, conS)
    '        adap.Fill(Rs_Report)
    '    End If

    '    If Rs_Report.Tables(0).Rows.Count = 0 Then
    '        Me.Cursor = Cursors.Default
    '        XtraMessageBox.Show(ulf, "<size=10>No Recode Found.</size>", "<size=9>ULtra</size>")
    '        Exit Sub
    '    End If
    '    rowcnt = 1

    '    Common.frodatetodatetoReportGrid = "DAILY TEMPERATURE REPORT FROM DATE: " & DateEdit1.DateTime.ToString("dd/MM/yyyy") & " TO DATE: " & DateEdit2.DateTime.ToString("dd/MM/yyyy")


    '    Common.tbl = New Data.DataTable()
    '    Common.tbl.Columns.Add("User No", GetType(String))
    '    Common.tbl.Columns.Add("Emp. ID", GetType(String))
    '    Common.tbl.Columns.Add("Employee Name", GetType(String))
    '    Common.tbl.Columns.Add("Date", GetType(String))
    '    Common.tbl.Columns.Add("Time", GetType(String))
    '    Common.tbl.Columns.Add("Location", GetType(String))
    '    Common.tbl.Columns.Add("Machine No", GetType(String))
    '    Common.tbl.Columns.Add("Temp C", GetType(String))
    '    Common.tbl.Columns.Add("Temp F", GetType(String))
    '    Common.tbl.Columns.Add("MaskStatus", GetType(String))
    '    Common.tbl.Columns.Add("IsAbnomal", GetType(String))
    '    Common.tbl.Columns.Add("Normal Image", GetType(Image))
    '    Common.tbl.Columns.Add("Thermal Image", GetType(Image))

    '    mCount = 0
    '    Dim strPayCode As String = ""
    '    Dim paycodelist As New List(Of String)()
    '    For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1  'Do While Not .EOF
    '        paycodelist.Add(Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim)
    '        Dim Ctemp As String = 0
    '        Dim Ftemp As String = 0

    '        If Rs_Report.Tables(0).Rows(i).Item("Temperature").ToString.Trim <> "" Then
    '            Ctemp = Rs_Report.Tables(0).Rows(i).Item("Temperature").ToString.Trim
    '            Ftemp = (Ctemp * 9 / 5) + 32

    '            If Rs_Report.Tables(0).Rows(i).Item("Temperature").ToString.Trim > 80 Then
    '                Ftemp = Rs_Report.Tables(0).Rows(i).Item("Temperature").ToString.Trim
    '                Ctemp = Math.Round((Rs_Report.Tables(0).Rows(i).Item("Temperature").ToString.Trim - 32) * 5 / 9, 2)
    '            ElseIf Rs_Report.Tables(0).Rows(i).Item("Temperature").ToString.Trim <= 80 Then
    '                Ctemp = Rs_Report.Tables(0).Rows(i).Item("Temperature").ToString.Trim
    '                Ftemp = Math.Round((Rs_Report.Tables(0).Rows(i).Item("Temperature").ToString.Trim * 9 / 5) + 32, 2)
    '            ElseIf Rs_Report.Tables(0).Rows(i).Item("Temperature").ToString.Trim.Trim = "" Then
    '                Ftemp = ""
    '                Ctemp = ""
    '            End If

    '        End If

    '        Dim NormalImage As Image
    '        Dim ThermalImage As Image
    '        Dim cardNo As String = Rs_Report.Tables(0).Rows(i).Item("CardNo").ToString.Trim
    '        If IsNumeric(cardNo) Then
    '            cardNo = Convert.ToDouble(cardNo).ToString
    '        End If
    '        Try
    '            'NormalImage = Image.FromFile(My.Application.Info.DirectoryPath & "\RealTimeImages\Light" & cardNo & "_" & Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("OFFICEPUNCH").ToString.Trim).ToString("yyyyMMddHHmmss") & ".bmp")
    '            Dim FileLocation As DirectoryInfo = New DirectoryInfo(My.Application.Info.DirectoryPath & "\RealTimeImages\")
    '            Dim fi As FileInfo() = FileLocation.GetFiles("Light" & cardNo & "_" & Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("OFFICEPUNCH").ToString.Trim).ToString("yyyyMMddHHmm") & "*.bmp")
    '            NormalImage = Image.FromFile(fi(0).FullName)
    '        Catch ex As Exception
    '            NormalImage = Nothing
    '        End Try

    '        Try
    '            'ThermalImage = Image.FromFile(My.Application.Info.DirectoryPath & "\RealTimeImages\Thermal" & cardNo & "_" & Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("OFFICEPUNCH").ToString.Trim).ToString("yyyyMMddHHmmss") & ".bmp")
    '            Dim FileLocation As DirectoryInfo = New DirectoryInfo(My.Application.Info.DirectoryPath & "\RealTimeImages\")
    '            Dim fi As FileInfo() = FileLocation.GetFiles("Thermal" & cardNo & "_" & Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("OFFICEPUNCH").ToString.Trim).ToString("yyyyMMddHHmm") & "*.bmp")
    '            ThermalImage = Image.FromFile(fi(0).FullName)
    '        Catch ex As Exception
    '            ThermalImage = Nothing
    '        End Try
    '        Common.tbl.Rows.Add(Rs_Report.Tables(0).Rows(i).Item("CardNo").ToString.Trim, Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim,
    '                Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim, Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("officepunch").ToString.Trim).ToString("dd/MM/yyyy"),
    '                Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("officepunch").ToString.Trim).ToString("HH:mm:ss"), Rs_Report.Tables(0).Rows(i).Item("Branch").ToString.Trim,
    '                Rs_Report.Tables(0).Rows(i).Item("MC_No").ToString.Trim, Ctemp, Ftemp, Rs_Report.Tables(0).Rows(i).Item("MaskStatus"), Rs_Report.Tables(0).Rows(i).Item("IsAbnomal"),
    '                NormalImage, ThermalImage)
    '    Next ' Loop
    '    Me.Cursor = Cursors.Default
    '    XtraReportGrid.ShowDialog()
    'End Sub
    Private Sub CheckExcel_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckExcel.CheckedChanged
        If CheckExcel.Checked = True Then
            XtraShortOrder.ShowDialog()
        End If
    End Sub
    Private Sub CheckExcel_VisibleChanged(sender As System.Object, e As System.EventArgs) Handles CheckExcel.VisibleChanged
        If CheckExcel.Visible = False Then
            CheckText.Checked = True
        End If
    End Sub
    Private Sub CheckDeviceWise_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckDeviceWise.CheckedChanged
        If CheckDeviceWise.Checked = True Then
            CheckExcel.Checked = True
            CheckText.Visible = False
            CheckPDF.Visible = False

            PopupContainerEditDevice.Visible = True
            PopupContainerEditDevice.EditValue = ""
            TextFromTime.Visible = True
            TextToTime.Visible = True
            LabelControl12.Visible = True
            LabelControl13.Visible = True
            GridViewDevice.ClearSelection()
        Else
            CheckText.Visible = True
            CheckPDF.Visible = True

            PopupContainerEditDevice.Visible = False
            TextFromTime.Visible = False
            TextToTime.Visible = False
            LabelControl12.Visible = False
            LabelControl13.Visible = False
        End If
    End Sub
    Private Sub CheckInOut_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckInOut.CheckedChanged
        If CheckInOut.Checked = True Then
            CheckExcel.Checked = True
            CheckText.Visible = False
            CheckPDF.Visible = True
        Else
            CheckText.Visible = True
            CheckPDF.Visible = True
        End If
    End Sub
    Private Sub CheckPhotoReport_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckPhotoReport.CheckedChanged
        If CheckPhotoReport.Checked = True Then
            CheckExcel.Visible = False
            CheckText.Visible = False
            CheckPDF.Checked = True
        Else
            CheckText.Visible = True
            CheckExcel.Visible = True
        End If
    End Sub
    Private Sub CheckEdit3_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckPunchImage.CheckedChanged
        If CheckPunchImage.Checked = True Then
            CheckExcel.Visible = False
            CheckText.Visible = False
            CheckPDF.Checked = True
        Else
            CheckText.Visible = True
            CheckExcel.Visible = True
        End If
    End Sub
    Private Sub CheckCustomized_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckCustomized.CheckedChanged
        If CheckCustomized.Checked = True Then
            CheckExcel.Visible = True
            CheckExcel.Checked = True
            CheckText.Visible = False
            CheckPDF.Visible = False
        Else
            CheckText.Visible = True
            CheckPDF.Visible = True
        End If
    End Sub
    Private Sub CheckMultiShift_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckMultiShift.CheckedChanged
        If CheckMultiShift.Checked = True Then
            CheckExcel.Visible = True
            CheckExcel.Checked = True
            CheckText.Visible = False
            CheckPDF.Visible = False
        Else
            CheckText.Visible = True
            CheckPDF.Visible = True
        End If
    End Sub
    Private Sub CheckEditTemperature_CheckedChanged(sender As Object, e As EventArgs) Handles CheckEditTemperature.CheckedChanged
        If CheckEditTemperature.Checked = True Then
            CheckExcel.Visible = True
            CheckExcel.Checked = True
            CheckText.Visible = False
            CheckPDF.Visible = False
        Else
            CheckText.Visible = True
            CheckPDF.Visible = True
        End If
        If CheckEditTemperature.Checked = True Then
            LabelControl3.Visible = True
            If Common.IsNepali = "Y" Then
                DateEdit2.Visible = False
                ComboNepaliDateTo.Visible = True
                ComboNEpaliMonthTo.Visible = True
                ComboNepaliYearTo.Visible = True
            Else
                DateEdit2.Visible = True
                ComboNepaliDateTo.Visible = False
                ComboNEpaliMonthTo.Visible = False
                ComboNepaliYearTo.Visible = False
            End If
            'DateEdit2.Visible = True
            'DateEdit2.DateTime = Now
            Exit Sub
        Else
            LabelControl3.Visible = False
            DateEdit2.Visible = False
            ComboNepaliDateTo.Visible = False
            ComboNEpaliMonthTo.Visible = False
            ComboNepaliYearTo.Visible = False
            Exit Sub
        End If
    End Sub
    Private Sub CheckEditVReport_CheckedChanged(sender As Object, e As EventArgs) Handles CheckEditVReport.CheckedChanged
        If CheckEditVReport.Checked = True Then
            CheckExcel.Visible = True
            CheckExcel.Checked = True
            CheckText.Visible = False
            CheckPDF.Visible = False
            SidePanelSelection.Visible = False
        Else
            CheckText.Visible = True
            CheckExcel.Visible = True
            CheckPDF.Visible = True
            SidePanelSelection.Visible = True
        End If

        If CheckEditVReport.Checked = True Then
            LabelControl3.Visible = True
            If Common.IsNepali = "Y" Then
                DateEdit2.Visible = False
                ComboNepaliDateTo.Visible = True
                ComboNEpaliMonthTo.Visible = True
                ComboNepaliYearTo.Visible = True
            Else
                DateEdit2.Visible = True
                ComboNepaliDateTo.Visible = False
                ComboNEpaliMonthTo.Visible = False
                ComboNepaliYearTo.Visible = False
            End If
            'DateEdit2.Visible = True
            'DateEdit2.DateTime = Now
            Exit Sub
        Else
            LabelControl3.Visible = False
            DateEdit2.Visible = False
            ComboNepaliDateTo.Visible = False
            ComboNEpaliMonthTo.Visible = False
            ComboNepaliYearTo.Visible = False
            Exit Sub
        End If

    End Sub
    Private Sub DateEdit1_Leave(sender As System.Object, e As System.EventArgs) Handles DateEdit1.Leave
        DateEdit2.DateTime = DateEdit1.DateTime.AddMonths(1).AddDays(-1)
        If DateEdit1.DateTime > DateEdit2.DateTime Then
            DateEdit2.DateTime = DateEdit1.DateTime
        End If
    End Sub
    Private Sub DateEdit2_Leave(sender As System.Object, e As System.EventArgs) Handles DateEdit2.Leave
        If DateEdit2.DateTime < DateEdit1.DateTime Then
            DateEdit2.DateTime = DateEdit1.DateTime.AddMonths(1).AddDays(-1)
        End If
    End Sub
End Class
Module XL
    Sub ExcelPDF(ByVal mstrFile_Name As String)
        Dim xl As Object
        xl = CreateObject("Excel.Application")
        Dim xwb As Object = xl.Workbooks.Open(mstrFile_Name)
        xwb.ActiveSheet.ExportAsFixedFormat(0, mstrFile_Name & ".pdf")
        'xwb.ActiveSheet.ExportAsFixedFormat(0, mstrFile_Name)
        xl.Quit()

    End Sub
End Module