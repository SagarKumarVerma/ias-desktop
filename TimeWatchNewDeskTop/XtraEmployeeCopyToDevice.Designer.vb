﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraEmployeeCopyToDevice
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim GridLevelNode1 As DevExpress.XtraGrid.GridLevelNode = New DevExpress.XtraGrid.GridLevelNode()
        Dim GridLevelNode2 As DevExpress.XtraGrid.GridLevelNode = New DevExpress.XtraGrid.GridLevelNode()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(XtraEmployeeCopyToDevice))
        Me.SSSDBDataSet = New iAS.SSSDBDataSet()
        Me.TblMachineBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TblMachineTableAdapter = New iAS.SSSDBDataSetTableAdapters.tblMachineTableAdapter()
        Me.TblMachine1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.tblMachine1TableAdapter()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.GridControl2 = New DevExpress.XtraGrid.GridControl()
        Me.TblEmployeeBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colACTIVE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPAYCODE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colEMPNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colGUARDIANNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDateOFBIRTH = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDateOFJOIN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPRESENTCARDNO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCOMPANYCODE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDEPARTMENTCODE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCAT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSEX = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colISMARRIED = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBUS = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colQUALIFICATION = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colEXPERIENCE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDESIGNATION = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colADDRESS1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPINCODE1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colTELEPHONE1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colE_MAIL1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colADDRESS2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPINCODE2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colTELEPHONE2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBLOODGROUP = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colEMPPHOTO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colEMPSIGNATURE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDivisionCode = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colGradeCode = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLeavingdate = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLeavingReason = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVehicleNo = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPFNO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPF_NO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colESINO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colAUTHORISEDMACHINE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colEMPTYPE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBankAcc = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colbankCODE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colReporting1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colReporting2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBRANCHCODE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDESPANSARYCODE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colValidityStartDate = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colValidityEndDate = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colEmployeeGroupId = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLastModifiedBy = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLastModifiedDate = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colMachineCard = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colID_NO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLOCATION = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colbranch = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDeviceType = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colA_R = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colcommkey = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colMAC_ADDRESS = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.TblEmployeeTableAdapter = New iAS.SSSDBDataSetTableAdapters.TblEmployeeTableAdapter()
        Me.TblEmployee1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.TblEmployee1TableAdapter()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.btnUploadFinger = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl20 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl19 = New DevExpress.XtraEditors.LabelControl()
        Me.DateEditEnd = New DevExpress.XtraEditors.DateEdit()
        Me.DateEditStart = New DevExpress.XtraEditors.DateEdit()
        Me.GroupControl3 = New DevExpress.XtraEditors.GroupControl()
        Me.CheckFrmEmp = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckFrmDt = New DevExpress.XtraEditors.CheckEdit()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.ComboNepaliVEndYear = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboNepaliVStartMonth = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboNepaliVStartDate = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboNepaliVEndMonth = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboNepaliVStartYear = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboNepaliVEndDate = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.CheckEdit1 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditCopyCard = New DevExpress.XtraEditors.CheckEdit()
        Me.AxFP_CLOCK1 = New AxFP_CLOCKLib.AxFP_CLOCK()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblMachineBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblEmployeeBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEditEnd.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEditEnd.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEditStart.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEditStart.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.CheckFrmEmp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckFrmDt.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.ComboNepaliVEndYear.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboNepaliVStartMonth.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboNepaliVStartDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboNepaliVEndMonth.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboNepaliVStartYear.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboNepaliVEndDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditCopyCard.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AxFP_CLOCK1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SSSDBDataSet
        '
        Me.SSSDBDataSet.DataSetName = "SSSDBDataSet"
        Me.SSSDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'TblMachineBindingSource
        '
        Me.TblMachineBindingSource.DataMember = "tblMachine"
        Me.TblMachineBindingSource.DataSource = Me.SSSDBDataSet
        '
        'TblMachineTableAdapter
        '
        Me.TblMachineTableAdapter.ClearBeforeFill = True
        '
        'TblMachine1TableAdapter1
        '
        Me.TblMachine1TableAdapter1.ClearBeforeFill = True
        '
        'GroupControl2
        '
        Me.GroupControl2.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl2.AppearanceCaption.Options.UseFont = True
        Me.GroupControl2.Controls.Add(Me.GridControl2)
        Me.GroupControl2.Location = New System.Drawing.Point(12, 266)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(535, 250)
        Me.GroupControl2.TabIndex = 19
        Me.GroupControl2.Text = "Employee Details"
        '
        'GridControl2
        '
        Me.GridControl2.DataSource = Me.TblEmployeeBindingSource
        Me.GridControl2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControl2.EmbeddedNavigator.Buttons.Append.Visible = False
        Me.GridControl2.EmbeddedNavigator.Buttons.CancelEdit.Visible = False
        Me.GridControl2.EmbeddedNavigator.Buttons.Edit.Visible = False
        Me.GridControl2.EmbeddedNavigator.Buttons.EndEdit.Visible = False
        Me.GridControl2.EmbeddedNavigator.Buttons.Remove.Visible = False
        GridLevelNode1.RelationName = "Level1"
        Me.GridControl2.LevelTree.Nodes.AddRange(New DevExpress.XtraGrid.GridLevelNode() {GridLevelNode1})
        Me.GridControl2.Location = New System.Drawing.Point(2, 23)
        Me.GridControl2.MainView = Me.GridView2
        Me.GridControl2.Name = "GridControl2"
        Me.GridControl2.Size = New System.Drawing.Size(531, 225)
        Me.GridControl2.TabIndex = 1
        Me.GridControl2.UseEmbeddedNavigator = True
        Me.GridControl2.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView2})
        '
        'TblEmployeeBindingSource
        '
        Me.TblEmployeeBindingSource.DataMember = "TblEmployee"
        Me.TblEmployeeBindingSource.DataSource = Me.SSSDBDataSet
        '
        'GridView2
        '
        Me.GridView2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colACTIVE, Me.colPAYCODE, Me.colEMPNAME, Me.colGUARDIANNAME, Me.colDateOFBIRTH, Me.colDateOFJOIN, Me.colPRESENTCARDNO, Me.colCOMPANYCODE, Me.colDEPARTMENTCODE, Me.colCAT, Me.colSEX, Me.colISMARRIED, Me.colBUS, Me.colQUALIFICATION, Me.colEXPERIENCE, Me.colDESIGNATION, Me.colADDRESS1, Me.colPINCODE1, Me.colTELEPHONE1, Me.colE_MAIL1, Me.colADDRESS2, Me.colPINCODE2, Me.colTELEPHONE2, Me.colBLOODGROUP, Me.colEMPPHOTO, Me.colEMPSIGNATURE, Me.colDivisionCode, Me.colGradeCode, Me.colLeavingdate, Me.colLeavingReason, Me.colVehicleNo, Me.colPFNO, Me.colPF_NO, Me.colESINO, Me.colAUTHORISEDMACHINE, Me.colEMPTYPE, Me.colBankAcc, Me.colbankCODE, Me.colReporting1, Me.colReporting2, Me.colBRANCHCODE, Me.colDESPANSARYCODE, Me.colValidityStartDate, Me.colValidityEndDate, Me.colEmployeeGroupId, Me.colLastModifiedBy, Me.colLastModifiedDate, Me.colMachineCard})
        Me.GridView2.GridControl = Me.GridControl2
        Me.GridView2.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always
        Me.GridView2.Name = "GridView2"
        Me.GridView2.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView2.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView2.OptionsBehavior.Editable = False
        Me.GridView2.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridView2.OptionsSelection.MultiSelect = True
        Me.GridView2.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        Me.GridView2.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colPRESENTCARDNO, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colACTIVE
        '
        Me.colACTIVE.FieldName = "ACTIVE"
        Me.colACTIVE.Name = "colACTIVE"
        '
        'colPAYCODE
        '
        Me.colPAYCODE.FieldName = "PAYCODE"
        Me.colPAYCODE.Name = "colPAYCODE"
        Me.colPAYCODE.Visible = True
        Me.colPAYCODE.VisibleIndex = 1
        '
        'colEMPNAME
        '
        Me.colEMPNAME.FieldName = "EMPNAME"
        Me.colEMPNAME.Name = "colEMPNAME"
        Me.colEMPNAME.Visible = True
        Me.colEMPNAME.VisibleIndex = 2
        '
        'colGUARDIANNAME
        '
        Me.colGUARDIANNAME.FieldName = "GUARDIANNAME"
        Me.colGUARDIANNAME.Name = "colGUARDIANNAME"
        '
        'colDateOFBIRTH
        '
        Me.colDateOFBIRTH.FieldName = "DateOFBIRTH"
        Me.colDateOFBIRTH.Name = "colDateOFBIRTH"
        '
        'colDateOFJOIN
        '
        Me.colDateOFJOIN.FieldName = "DateOFJOIN"
        Me.colDateOFJOIN.Name = "colDateOFJOIN"
        '
        'colPRESENTCARDNO
        '
        Me.colPRESENTCARDNO.FieldName = "PRESENTCARDNO"
        Me.colPRESENTCARDNO.Name = "colPRESENTCARDNO"
        Me.colPRESENTCARDNO.Visible = True
        Me.colPRESENTCARDNO.VisibleIndex = 3
        '
        'colCOMPANYCODE
        '
        Me.colCOMPANYCODE.FieldName = "COMPANYCODE"
        Me.colCOMPANYCODE.Name = "colCOMPANYCODE"
        '
        'colDEPARTMENTCODE
        '
        Me.colDEPARTMENTCODE.FieldName = "DEPARTMENTCODE"
        Me.colDEPARTMENTCODE.Name = "colDEPARTMENTCODE"
        '
        'colCAT
        '
        Me.colCAT.FieldName = "CAT"
        Me.colCAT.Name = "colCAT"
        '
        'colSEX
        '
        Me.colSEX.FieldName = "SEX"
        Me.colSEX.Name = "colSEX"
        '
        'colISMARRIED
        '
        Me.colISMARRIED.FieldName = "ISMARRIED"
        Me.colISMARRIED.Name = "colISMARRIED"
        '
        'colBUS
        '
        Me.colBUS.FieldName = "BUS"
        Me.colBUS.Name = "colBUS"
        '
        'colQUALIFICATION
        '
        Me.colQUALIFICATION.FieldName = "QUALIFICATION"
        Me.colQUALIFICATION.Name = "colQUALIFICATION"
        '
        'colEXPERIENCE
        '
        Me.colEXPERIENCE.FieldName = "EXPERIENCE"
        Me.colEXPERIENCE.Name = "colEXPERIENCE"
        '
        'colDESIGNATION
        '
        Me.colDESIGNATION.FieldName = "DESIGNATION"
        Me.colDESIGNATION.Name = "colDESIGNATION"
        '
        'colADDRESS1
        '
        Me.colADDRESS1.FieldName = "ADDRESS1"
        Me.colADDRESS1.Name = "colADDRESS1"
        '
        'colPINCODE1
        '
        Me.colPINCODE1.FieldName = "PINCODE1"
        Me.colPINCODE1.Name = "colPINCODE1"
        '
        'colTELEPHONE1
        '
        Me.colTELEPHONE1.FieldName = "TELEPHONE1"
        Me.colTELEPHONE1.Name = "colTELEPHONE1"
        '
        'colE_MAIL1
        '
        Me.colE_MAIL1.FieldName = "E_MAIL1"
        Me.colE_MAIL1.Name = "colE_MAIL1"
        '
        'colADDRESS2
        '
        Me.colADDRESS2.FieldName = "ADDRESS2"
        Me.colADDRESS2.Name = "colADDRESS2"
        '
        'colPINCODE2
        '
        Me.colPINCODE2.FieldName = "PINCODE2"
        Me.colPINCODE2.Name = "colPINCODE2"
        '
        'colTELEPHONE2
        '
        Me.colTELEPHONE2.FieldName = "TELEPHONE2"
        Me.colTELEPHONE2.Name = "colTELEPHONE2"
        '
        'colBLOODGROUP
        '
        Me.colBLOODGROUP.FieldName = "BLOODGROUP"
        Me.colBLOODGROUP.Name = "colBLOODGROUP"
        '
        'colEMPPHOTO
        '
        Me.colEMPPHOTO.FieldName = "EMPPHOTO"
        Me.colEMPPHOTO.Name = "colEMPPHOTO"
        '
        'colEMPSIGNATURE
        '
        Me.colEMPSIGNATURE.FieldName = "EMPSIGNATURE"
        Me.colEMPSIGNATURE.Name = "colEMPSIGNATURE"
        '
        'colDivisionCode
        '
        Me.colDivisionCode.FieldName = "DivisionCode"
        Me.colDivisionCode.Name = "colDivisionCode"
        '
        'colGradeCode
        '
        Me.colGradeCode.FieldName = "GradeCode"
        Me.colGradeCode.Name = "colGradeCode"
        '
        'colLeavingdate
        '
        Me.colLeavingdate.FieldName = "Leavingdate"
        Me.colLeavingdate.Name = "colLeavingdate"
        '
        'colLeavingReason
        '
        Me.colLeavingReason.FieldName = "LeavingReason"
        Me.colLeavingReason.Name = "colLeavingReason"
        '
        'colVehicleNo
        '
        Me.colVehicleNo.FieldName = "VehicleNo"
        Me.colVehicleNo.Name = "colVehicleNo"
        '
        'colPFNO
        '
        Me.colPFNO.FieldName = "PFNO"
        Me.colPFNO.Name = "colPFNO"
        '
        'colPF_NO
        '
        Me.colPF_NO.FieldName = "PF_NO"
        Me.colPF_NO.Name = "colPF_NO"
        '
        'colESINO
        '
        Me.colESINO.FieldName = "ESINO"
        Me.colESINO.Name = "colESINO"
        '
        'colAUTHORISEDMACHINE
        '
        Me.colAUTHORISEDMACHINE.FieldName = "AUTHORISEDMACHINE"
        Me.colAUTHORISEDMACHINE.Name = "colAUTHORISEDMACHINE"
        '
        'colEMPTYPE
        '
        Me.colEMPTYPE.FieldName = "EMPTYPE"
        Me.colEMPTYPE.Name = "colEMPTYPE"
        '
        'colBankAcc
        '
        Me.colBankAcc.FieldName = "BankAcc"
        Me.colBankAcc.Name = "colBankAcc"
        '
        'colbankCODE
        '
        Me.colbankCODE.FieldName = "bankCODE"
        Me.colbankCODE.Name = "colbankCODE"
        '
        'colReporting1
        '
        Me.colReporting1.FieldName = "Reporting1"
        Me.colReporting1.Name = "colReporting1"
        '
        'colReporting2
        '
        Me.colReporting2.FieldName = "Reporting2"
        Me.colReporting2.Name = "colReporting2"
        '
        'colBRANCHCODE
        '
        Me.colBRANCHCODE.FieldName = "BRANCHCODE"
        Me.colBRANCHCODE.Name = "colBRANCHCODE"
        '
        'colDESPANSARYCODE
        '
        Me.colDESPANSARYCODE.FieldName = "DESPANSARYCODE"
        Me.colDESPANSARYCODE.Name = "colDESPANSARYCODE"
        '
        'colValidityStartDate
        '
        Me.colValidityStartDate.FieldName = "ValidityStartDate"
        Me.colValidityStartDate.Name = "colValidityStartDate"
        '
        'colValidityEndDate
        '
        Me.colValidityEndDate.FieldName = "ValidityEndDate"
        Me.colValidityEndDate.Name = "colValidityEndDate"
        '
        'colEmployeeGroupId
        '
        Me.colEmployeeGroupId.FieldName = "EmployeeGroupId"
        Me.colEmployeeGroupId.Name = "colEmployeeGroupId"
        '
        'colLastModifiedBy
        '
        Me.colLastModifiedBy.FieldName = "LastModifiedBy"
        Me.colLastModifiedBy.Name = "colLastModifiedBy"
        '
        'colLastModifiedDate
        '
        Me.colLastModifiedDate.FieldName = "LastModifiedDate"
        Me.colLastModifiedDate.Name = "colLastModifiedDate"
        '
        'colMachineCard
        '
        Me.colMachineCard.Caption = "MachineCard"
        Me.colMachineCard.FieldName = "MachineCard"
        Me.colMachineCard.Name = "colMachineCard"
        '
        'GroupControl1
        '
        Me.GroupControl1.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl1.AppearanceCaption.Options.UseFont = True
        Me.GroupControl1.Controls.Add(Me.GridControl1)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 10)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(535, 250)
        Me.GroupControl1.TabIndex = 18
        Me.GroupControl1.Text = "Device List"
        '
        'GridControl1
        '
        Me.GridControl1.DataSource = Me.TblMachineBindingSource
        Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControl1.EmbeddedNavigator.Buttons.Append.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.Edit.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.Remove.Visible = False
        GridLevelNode2.RelationName = "Level1"
        Me.GridControl1.LevelTree.Nodes.AddRange(New DevExpress.XtraGrid.GridLevelNode() {GridLevelNode2})
        Me.GridControl1.Location = New System.Drawing.Point(2, 23)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(531, 225)
        Me.GridControl1.TabIndex = 1
        Me.GridControl1.UseEmbeddedNavigator = True
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colID_NO, Me.colLOCATION, Me.colbranch, Me.colDeviceType, Me.colA_R, Me.colcommkey, Me.colMAC_ADDRESS})
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.Editable = False
        Me.GridView1.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridView1.OptionsSelection.MultiSelect = True
        Me.GridView1.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        Me.GridView1.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colID_NO, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colID_NO
        '
        Me.colID_NO.Caption = "Controller Id"
        Me.colID_NO.FieldName = "ID_NO"
        Me.colID_NO.Name = "colID_NO"
        Me.colID_NO.Visible = True
        Me.colID_NO.VisibleIndex = 1
        '
        'colLOCATION
        '
        Me.colLOCATION.Caption = "Device IP"
        Me.colLOCATION.FieldName = "LOCATION"
        Me.colLOCATION.Name = "colLOCATION"
        Me.colLOCATION.Visible = True
        Me.colLOCATION.VisibleIndex = 2
        '
        'colbranch
        '
        Me.colbranch.Caption = "Location"
        Me.colbranch.FieldName = "branch"
        Me.colbranch.Name = "colbranch"
        Me.colbranch.Visible = True
        Me.colbranch.VisibleIndex = 3
        '
        'colDeviceType
        '
        Me.colDeviceType.Caption = "Device Type"
        Me.colDeviceType.FieldName = "DeviceType"
        Me.colDeviceType.Name = "colDeviceType"
        Me.colDeviceType.Visible = True
        Me.colDeviceType.VisibleIndex = 4
        '
        'colA_R
        '
        Me.colA_R.FieldName = "A_R"
        Me.colA_R.Name = "colA_R"
        '
        'colcommkey
        '
        Me.colcommkey.Caption = "GridColumn1"
        Me.colcommkey.FieldName = "commkey"
        Me.colcommkey.Name = "colcommkey"
        '
        'colMAC_ADDRESS
        '
        Me.colMAC_ADDRESS.Caption = "MAC_ADDRESS"
        Me.colMAC_ADDRESS.FieldName = "MAC_ADDRESS"
        Me.colMAC_ADDRESS.Name = "colMAC_ADDRESS"
        '
        'TblEmployeeTableAdapter
        '
        Me.TblEmployeeTableAdapter.ClearBeforeFill = True
        '
        'TblEmployee1TableAdapter1
        '
        Me.TblEmployee1TableAdapter1.ClearBeforeFill = True
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.SimpleButton1.Appearance.Options.UseFont = True
        Me.SimpleButton1.Location = New System.Drawing.Point(785, 491)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(68, 23)
        Me.SimpleButton1.TabIndex = 33
        Me.SimpleButton1.Text = "Close"
        '
        'btnUploadFinger
        '
        Me.btnUploadFinger.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.btnUploadFinger.Appearance.Options.UseFont = True
        Me.btnUploadFinger.Location = New System.Drawing.Point(631, 491)
        Me.btnUploadFinger.Name = "btnUploadFinger"
        Me.btnUploadFinger.Size = New System.Drawing.Size(148, 23)
        Me.btnUploadFinger.TabIndex = 34
        Me.btnUploadFinger.Text = "Upload User"
        '
        'LabelControl20
        '
        Me.LabelControl20.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl20.Appearance.Options.UseFont = True
        Me.LabelControl20.Location = New System.Drawing.Point(11, 45)
        Me.LabelControl20.Name = "LabelControl20"
        Me.LabelControl20.Size = New System.Drawing.Size(21, 14)
        Me.LabelControl20.TabIndex = 53
        Me.LabelControl20.Text = "End"
        '
        'LabelControl19
        '
        Me.LabelControl19.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl19.Appearance.Options.UseFont = True
        Me.LabelControl19.Location = New System.Drawing.Point(9, 19)
        Me.LabelControl19.Name = "LabelControl19"
        Me.LabelControl19.Size = New System.Drawing.Size(27, 14)
        Me.LabelControl19.TabIndex = 52
        Me.LabelControl19.Text = "Start"
        '
        'DateEditEnd
        '
        Me.DateEditEnd.EditValue = Nothing
        Me.DateEditEnd.Enabled = False
        Me.DateEditEnd.Location = New System.Drawing.Point(55, 42)
        Me.DateEditEnd.Name = "DateEditEnd"
        Me.DateEditEnd.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.DateEditEnd.Properties.Appearance.Options.UseFont = True
        Me.DateEditEnd.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEditEnd.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEditEnd.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm"
        Me.DateEditEnd.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.DateEditEnd.Size = New System.Drawing.Size(158, 20)
        Me.DateEditEnd.TabIndex = 51
        '
        'DateEditStart
        '
        Me.DateEditStart.EditValue = Nothing
        Me.DateEditStart.Enabled = False
        Me.DateEditStart.Location = New System.Drawing.Point(55, 16)
        Me.DateEditStart.Name = "DateEditStart"
        Me.DateEditStart.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.DateEditStart.Properties.Appearance.Options.UseFont = True
        Me.DateEditStart.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEditStart.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEditStart.Properties.Mask.EditMask = "dd/MM/yyyy HH:mm"
        Me.DateEditStart.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.DateEditStart.Size = New System.Drawing.Size(158, 20)
        Me.DateEditStart.TabIndex = 50
        '
        'GroupControl3
        '
        Me.GroupControl3.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl3.AppearanceCaption.Options.UseFont = True
        Me.GroupControl3.Controls.Add(Me.CheckFrmEmp)
        Me.GroupControl3.Controls.Add(Me.CheckFrmDt)
        Me.GroupControl3.Controls.Add(Me.PanelControl1)
        Me.GroupControl3.Controls.Add(Me.CheckEdit1)
        Me.GroupControl3.Location = New System.Drawing.Point(553, 10)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(300, 191)
        Me.GroupControl3.TabIndex = 54
        Me.GroupControl3.Text = "User Validity"
        '
        'CheckFrmEmp
        '
        Me.CheckFrmEmp.Location = New System.Drawing.Point(123, 66)
        Me.CheckFrmEmp.Name = "CheckFrmEmp"
        Me.CheckFrmEmp.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckFrmEmp.Properties.Appearance.Options.UseFont = True
        Me.CheckFrmEmp.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Transparent
        Me.CheckFrmEmp.Properties.AppearanceFocused.Options.UseBorderColor = True
        Me.CheckFrmEmp.Properties.Caption = "From Employee Master"
        Me.CheckFrmEmp.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckFrmEmp.Properties.RadioGroupIndex = 0
        Me.CheckFrmEmp.Size = New System.Drawing.Size(155, 19)
        Me.CheckFrmEmp.TabIndex = 57
        Me.CheckFrmEmp.TabStop = False
        '
        'CheckFrmDt
        '
        Me.CheckFrmDt.EditValue = True
        Me.CheckFrmDt.Location = New System.Drawing.Point(22, 66)
        Me.CheckFrmDt.Name = "CheckFrmDt"
        Me.CheckFrmDt.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckFrmDt.Properties.Appearance.Options.UseFont = True
        Me.CheckFrmDt.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Transparent
        Me.CheckFrmDt.Properties.AppearanceFocused.Options.UseBorderColor = True
        Me.CheckFrmDt.Properties.Caption = "Select Date"
        Me.CheckFrmDt.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckFrmDt.Properties.RadioGroupIndex = 0
        Me.CheckFrmDt.Size = New System.Drawing.Size(95, 19)
        Me.CheckFrmDt.TabIndex = 56
        '
        'PanelControl1
        '
        Me.PanelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.PanelControl1.Controls.Add(Me.DateEditStart)
        Me.PanelControl1.Controls.Add(Me.ComboNepaliVEndYear)
        Me.PanelControl1.Controls.Add(Me.ComboNepaliVStartMonth)
        Me.PanelControl1.Controls.Add(Me.ComboNepaliVStartDate)
        Me.PanelControl1.Controls.Add(Me.LabelControl19)
        Me.PanelControl1.Controls.Add(Me.DateEditEnd)
        Me.PanelControl1.Controls.Add(Me.ComboNepaliVEndMonth)
        Me.PanelControl1.Controls.Add(Me.ComboNepaliVStartYear)
        Me.PanelControl1.Controls.Add(Me.LabelControl20)
        Me.PanelControl1.Controls.Add(Me.ComboNepaliVEndDate)
        Me.PanelControl1.Location = New System.Drawing.Point(14, 93)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(267, 77)
        Me.PanelControl1.TabIndex = 55
        '
        'ComboNepaliVEndYear
        '
        Me.ComboNepaliVEndYear.Location = New System.Drawing.Point(175, 42)
        Me.ComboNepaliVEndYear.Name = "ComboNepaliVEndYear"
        Me.ComboNepaliVEndYear.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliVEndYear.Properties.Appearance.Options.UseFont = True
        Me.ComboNepaliVEndYear.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliVEndYear.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNepaliVEndYear.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNepaliVEndYear.Properties.Items.AddRange(New Object() {"2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037", "2038", "2039", "2040", "2041", "2042", "2043", "2044", "2045", "2046", "2047", "2048", "2049", "2050", "2051", "2052", "2053", "2054", "2055", "2056", "2057", "2058", "2059", "2060", "2061", "2062", "2063", "2064", "2065", "2066", "2067", "2068", "2069", "2070", "2071", "2072", "2073", "2074", "2075", "2076", "2077", "2078", "2079", "2080", "2081", "2082", "2083", "2084", "2085", "2086", "2087", "2088", "2089"})
        Me.ComboNepaliVEndYear.Size = New System.Drawing.Size(61, 20)
        Me.ComboNepaliVEndYear.TabIndex = 60
        '
        'ComboNepaliVStartMonth
        '
        Me.ComboNepaliVStartMonth.Location = New System.Drawing.Point(103, 16)
        Me.ComboNepaliVStartMonth.Name = "ComboNepaliVStartMonth"
        Me.ComboNepaliVStartMonth.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliVStartMonth.Properties.Appearance.Options.UseFont = True
        Me.ComboNepaliVStartMonth.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliVStartMonth.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNepaliVStartMonth.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNepaliVStartMonth.Properties.Items.AddRange(New Object() {"Baishakh", "Jestha", "Asar", "Shrawan", "Bhadau", "Aswin", "Kartik", "Mansir", "Poush", "Magh", "Falgun", "Chaitra"})
        Me.ComboNepaliVStartMonth.Size = New System.Drawing.Size(66, 20)
        Me.ComboNepaliVStartMonth.TabIndex = 56
        '
        'ComboNepaliVStartDate
        '
        Me.ComboNepaliVStartDate.Location = New System.Drawing.Point(55, 16)
        Me.ComboNepaliVStartDate.Name = "ComboNepaliVStartDate"
        Me.ComboNepaliVStartDate.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliVStartDate.Properties.Appearance.Options.UseFont = True
        Me.ComboNepaliVStartDate.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliVStartDate.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNepaliVStartDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNepaliVStartDate.Properties.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32"})
        Me.ComboNepaliVStartDate.Size = New System.Drawing.Size(42, 20)
        Me.ComboNepaliVStartDate.TabIndex = 55
        '
        'ComboNepaliVEndMonth
        '
        Me.ComboNepaliVEndMonth.Location = New System.Drawing.Point(103, 42)
        Me.ComboNepaliVEndMonth.Name = "ComboNepaliVEndMonth"
        Me.ComboNepaliVEndMonth.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliVEndMonth.Properties.Appearance.Options.UseFont = True
        Me.ComboNepaliVEndMonth.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliVEndMonth.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNepaliVEndMonth.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNepaliVEndMonth.Properties.Items.AddRange(New Object() {"Baishakh", "Jestha", "Asar", "Shrawan", "Bhadau", "Aswin", "Kartik", "Mansir", "Poush", "Magh", "Falgun", "Chaitra"})
        Me.ComboNepaliVEndMonth.Size = New System.Drawing.Size(66, 20)
        Me.ComboNepaliVEndMonth.TabIndex = 59
        '
        'ComboNepaliVStartYear
        '
        Me.ComboNepaliVStartYear.Location = New System.Drawing.Point(175, 16)
        Me.ComboNepaliVStartYear.Name = "ComboNepaliVStartYear"
        Me.ComboNepaliVStartYear.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliVStartYear.Properties.Appearance.Options.UseFont = True
        Me.ComboNepaliVStartYear.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliVStartYear.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNepaliVStartYear.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNepaliVStartYear.Properties.Items.AddRange(New Object() {"2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037", "2038", "2039", "2040", "2041", "2042", "2043", "2044", "2045", "2046", "2047", "2048", "2049", "2050", "2051", "2052", "2053", "2054", "2055", "2056", "2057", "2058", "2059", "2060", "2061", "2062", "2063", "2064", "2065", "2066", "2067", "2068", "2069", "2070", "2071", "2072", "2073", "2074", "2075", "2076", "2077", "2078", "2079", "2080", "2081", "2082", "2083", "2084", "2085", "2086", "2087", "2088", "2089"})
        Me.ComboNepaliVStartYear.Size = New System.Drawing.Size(61, 20)
        Me.ComboNepaliVStartYear.TabIndex = 57
        '
        'ComboNepaliVEndDate
        '
        Me.ComboNepaliVEndDate.Location = New System.Drawing.Point(55, 42)
        Me.ComboNepaliVEndDate.Name = "ComboNepaliVEndDate"
        Me.ComboNepaliVEndDate.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliVEndDate.Properties.Appearance.Options.UseFont = True
        Me.ComboNepaliVEndDate.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliVEndDate.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNepaliVEndDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNepaliVEndDate.Properties.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32"})
        Me.ComboNepaliVEndDate.Size = New System.Drawing.Size(42, 20)
        Me.ComboNepaliVEndDate.TabIndex = 58
        '
        'CheckEdit1
        '
        Me.CheckEdit1.Location = New System.Drawing.Point(22, 39)
        Me.CheckEdit1.Name = "CheckEdit1"
        Me.CheckEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEdit1.Properties.Appearance.Options.UseFont = True
        Me.CheckEdit1.Properties.Caption = "Assign Validity"
        Me.CheckEdit1.Size = New System.Drawing.Size(121, 19)
        Me.CheckEdit1.TabIndex = 54
        '
        'CheckEditCopyCard
        '
        Me.CheckEditCopyCard.Location = New System.Drawing.Point(553, 216)
        Me.CheckEditCopyCard.Name = "CheckEditCopyCard"
        Me.CheckEditCopyCard.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditCopyCard.Properties.Appearance.Options.UseFont = True
        Me.CheckEditCopyCard.Properties.Caption = "Copy Card from Employee Master"
        Me.CheckEditCopyCard.Size = New System.Drawing.Size(300, 19)
        Me.CheckEditCopyCard.TabIndex = 55
        '
        'AxFP_CLOCK1
        '
        Me.AxFP_CLOCK1.Enabled = True
        Me.AxFP_CLOCK1.Location = New System.Drawing.Point(576, 322)
        Me.AxFP_CLOCK1.Name = "AxFP_CLOCK1"
        Me.AxFP_CLOCK1.OcxState = CType(resources.GetObject("AxFP_CLOCK1.OcxState"), System.Windows.Forms.AxHost.State)
        Me.AxFP_CLOCK1.Size = New System.Drawing.Size(60, 44)
        Me.AxFP_CLOCK1.TabIndex = 68
        Me.AxFP_CLOCK1.Visible = False
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Location = New System.Drawing.Point(572, 241)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(176, 14)
        Me.LabelControl1.TabIndex = 69
        Me.LabelControl1.Text = "(ZK/Bio/Ultra800/Atf686n/Ultra)"
        '
        'XtraEmployeeCopyToDevice
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(865, 536)
        Me.ControlBox = False
        Me.Controls.Add(Me.LabelControl1)
        Me.Controls.Add(Me.AxFP_CLOCK1)
        Me.Controls.Add(Me.CheckEditCopyCard)
        Me.Controls.Add(Me.GroupControl3)
        Me.Controls.Add(Me.btnUploadFinger)
        Me.Controls.Add(Me.SimpleButton1)
        Me.Controls.Add(Me.GroupControl2)
        Me.Controls.Add(Me.GroupControl1)
        Me.LookAndFeel.SkinName = "iMaginary"
        Me.LookAndFeel.UseDefaultLookAndFeel = False
        Me.Name = "XtraEmployeeCopyToDevice"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Export To Device"
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblMachineBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblEmployeeBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEditEnd.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEditEnd.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEditStart.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEditStart.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        CType(Me.CheckFrmEmp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckFrmDt.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.ComboNepaliVEndYear.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboNepaliVStartMonth.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboNepaliVStartDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboNepaliVEndMonth.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboNepaliVStartYear.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboNepaliVEndDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditCopyCard.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AxFP_CLOCK1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents SSSDBDataSet As iAS.SSSDBDataSet
    Friend WithEvents TblMachineBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblMachineTableAdapter As iAS.SSSDBDataSetTableAdapters.tblMachineTableAdapter
    Friend WithEvents TblMachine1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.tblMachine1TableAdapter
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GridControl2 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colID_NO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLOCATION As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colbranch As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDeviceType As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colA_R As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents TblEmployeeBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblEmployeeTableAdapter As iAS.SSSDBDataSetTableAdapters.TblEmployeeTableAdapter
    Friend WithEvents colACTIVE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPAYCODE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colEMPNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colGUARDIANNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDateOFBIRTH As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDateOFJOIN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPRESENTCARDNO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCOMPANYCODE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDEPARTMENTCODE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCAT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSEX As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colISMARRIED As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBUS As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colQUALIFICATION As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colEXPERIENCE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDESIGNATION As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colADDRESS1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPINCODE1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colTELEPHONE1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colE_MAIL1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colADDRESS2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPINCODE2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colTELEPHONE2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBLOODGROUP As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colEMPPHOTO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colEMPSIGNATURE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDivisionCode As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colGradeCode As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLeavingdate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLeavingReason As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVehicleNo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPFNO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPF_NO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colESINO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colAUTHORISEDMACHINE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colEMPTYPE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBankAcc As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colbankCODE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colReporting1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colReporting2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBRANCHCODE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDESPANSARYCODE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colValidityStartDate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colValidityEndDate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colEmployeeGroupId As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLastModifiedBy As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLastModifiedDate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents TblEmployee1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.TblEmployee1TableAdapter
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnUploadFinger As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl20 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl19 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents DateEditEnd As DevExpress.XtraEditors.DateEdit
    Friend WithEvents DateEditStart As DevExpress.XtraEditors.DateEdit
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CheckEdit1 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents ComboNepaliVEndYear As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboNepaliVEndMonth As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboNepaliVEndDate As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboNepaliVStartYear As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboNepaliVStartMonth As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboNepaliVStartDate As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents colcommkey As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents CheckFrmEmp As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckFrmDt As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditCopyCard As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents colMachineCard As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents AxFP_CLOCK1 As AxFP_CLOCKLib.AxFP_CLOCK
    Friend WithEvents colMAC_ADDRESS As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
End Class
