﻿Imports System.Data.OleDb
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.Data.SqlClient
Imports System.IO
Imports System.Resources
Imports System.Globalization
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid.Views.Base

Public Class XtraManualEntry
    'Dim connection As SqlConnection
    'Dim ConnectionString As String
    Dim Con1 As OleDbConnection
    Dim ulf As UserLookAndFeel
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    Dim leaveFlage As Boolean = True
    Public Sub New()
        InitializeComponent()
        If Common.servername = "Access" Then
            'Me.LeaveApplication1TableAdapter1.Fill(Me.SSSDBDataSet.LeaveApplication1)
            'GridControl1.DataSource = SSSDBDataSet.EmployeeGroup1
        Else
            'LeaveApplicationTableAdapter.Connection.ConnectionString = Common.ConnectionString '"Data Source=DESKTOP-PG6NDV8;Initial Catalog=SSSDB;Integrated Security=True"
            'Me.LeaveApplicationTableAdapter.Fill(Me.SSSDBDataSet.LeaveApplication)
            'GridControl1.DataSource = SSSDBDataSet.EmployeeGroup

            'TblLeaveMasterTableAdapter.Connection.ConnectionString = Common.ConnectionString
            TblEmployeeTableAdapter.Connection.ConnectionString = Common.ConnectionString
        End If
        Common.SetGridFont(GridView1, New Font("Tahoma", 11))
        Common.SetGridFont(GridView2, New Font("Tahoma", 11))
    End Sub
    Private Sub XtraManualEntry_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        'for xtramessage box
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True

        'TextEdit1.Select()
        'Me.Width = My.Computer.Screen.WorkingArea.Width
        Me.Width = Common.NavWidth 'Me.Parent.Width
        Me.Height = Common.NavHeight 'Me.Parent.Height
        SplitContainerControl1.Width = SplitContainerControl1.Parent.Width
        SplitContainerControl1.SplitterPosition = (SplitContainerControl1.Parent.Width) * 85 / 100

        SidePanel2.Width = SidePanel2.Parent.Width * 65 / 100
        'not to do in all pages
        Common.splitforMasterMenuWidth = SplitContainerControl1.Width
        Common.SplitterPosition = SplitContainerControl1.SplitterPosition
        'Common.res_man = New ResourceManager("TimeWatchNewDeskTop.Res", GetType(XtraCompany).Assembly)
        'Common.cul = CultureInfo.CreateSpecificCulture("en")

        'If Common.servername = "Access" Then
        '    Me.TblEmployee1TableAdapter1.Fill(Me.SSSDBDataSet.TblEmployee1)
        '    LookUpEdit1.Properties.DataSource = SSSDBDataSet.TblEmployee1
        'Else
        '    Me.TblEmployeeTableAdapter.Fill(Me.SSSDBDataSet.TblEmployee)
        '    LookUpEdit1.Properties.DataSource = SSSDBDataSet.TblEmployee
        'End If
        LookUpEdit1.Properties.DataSource = Common.EmpNonAdmin

        checkColumn()
        SidePanel3.Width = SplitContainerControl1.Panel1.Width - SidePanel2.Width
        setDefault()
    End Sub
    Private Sub checkColumn()
        'MARKMISSASHALFDAY
        Dim adapAc As OleDbDataAdapter
        Dim adapS As SqlDataAdapter
        Dim sSql As String = "select top 1 Purpose,Remark from MachineRawPunch"
        Dim Rs1 As DataSet = New DataSet
        Try
            If Common.servername = "Access" Then
                adapAc = New OleDbDataAdapter(sSql, Common.con1)
                adapAc.Fill(Rs1)
            Else
                adapS = New SqlDataAdapter(sSql, Common.con)
                adapS.Fill(Rs1)
            End If
        Catch ex As Exception
            If Common.servername = "Access" Then
                sSql = "ALTER TABLE MachineRawPunch ADD Purpose char(1), Remark longtext"
                If Common.con1.State <> ConnectionState.Open Then
                    Common.con1.Open()
                End If
                Dim cmd1 As OleDbCommand
                cmd1 = New OleDbCommand(sSql, Common.con1)
                cmd1.ExecuteNonQuery()
                If Common.con1.State <> ConnectionState.Closed Then
                    Common.con1.Close()
                End If
            Else
                sSql = "ALTER TABLE MachineRawPunch ADD Purpose char(1), Remark varchar(500)"
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                Dim cmd As SqlCommand
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
        End Try
    End Sub
    Private Sub setDefault()
        TextEditRemark.Text = ""
        LookUpEdit1.EditValue = ""
        TextEdit2.Text = ""
        TextEdit3.Text = "00:00"
        LabelControl6.Text = ""
        DateEdit1.DateTime = Now
        DateEdit2.DateTime = Now
        ComboPurpose.SelectedIndex = 0
        TextEditRemark.Text = ""

        GridControl1.DataSource = Nothing
        GridControl2.DataSource = Nothing
        SimpleButton1.Enabled = False

        If Common.IsNepali = "Y" Then
            ComboPNDate.Visible = True
            ComboPNMonth.Visible = True
            ComboPNYear.Visible = True
            ComboFNDate.Visible = True
            ComboFNMonth.Visible = True
            ComboFNYear.Visible = True
            DateEdit1.Visible = False
            DateEdit2.Visible = False

            Dim DC As New DateConverter()
            Dim Vstart As String = DC.ToBS(New Date(DateEdit1.DateTime.Year, DateEdit1.DateTime.Month, DateEdit1.DateTime.Day))
            Dim dojTmp() As String = Vstart.Split("-")
            ComboFNYear.EditValue = dojTmp(0)
            ComboFNMonth.SelectedIndex = dojTmp(1) - 1
            ComboFNDate.EditValue = dojTmp(2)


            Dim VEnd As String = DC.ToBS(New Date(DateEdit2.DateTime.Year, DateEdit2.DateTime.Month, DateEdit2.DateTime.Day))
            dojTmp = VEnd.Split("-")
            ComboPNYear.EditValue = dojTmp(0)
            ComboPNMonth.SelectedIndex = dojTmp(1) - 1
            ComboPNDate.EditValue = dojTmp(2)

        Else
            ComboPNDate.Visible = False
            ComboPNMonth.Visible = False
            ComboPNYear.Visible = False
            ComboFNDate.Visible = False
            ComboFNMonth.Visible = False
            ComboFNYear.Visible = False
            DateEdit1.Visible = True
            DateEdit2.Visible = True
        End If
    End Sub
    Private Sub LookUpEdit1_Leave(sender As System.Object, e As System.EventArgs) Handles LookUpEdit1.Leave
        If LookUpEdit1.EditValue.ToString.Trim = "" Then
            Exit Sub
        End If
        If Not leaveFlage Then Return
        leaveFlage = False

        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim ds As DataSet = New DataSet
        Dim sSql As String = "select PRESENTCARDNO, EMPNAME from TblEmployee where PAYCODE = '" & LookUpEdit1.EditValue.ToString.Trim & "'"
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(ds)
        End If
        If ds.Tables(0).Rows.Count = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>Invalid paycode</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            LookUpEdit1.Select()
            leaveFlage = True
            setDefault()
            Exit Sub
        Else
            TextEdit2.Text = ds.Tables(0).Rows(0).Item("PRESENTCARDNO").ToString
            LabelControl6.Text = ds.Tables(0).Rows(0).Item("EMPNAME").ToString
        End If
        leaveFlage = True
    End Sub
    Private Sub DateEdit1_Leave(sender As System.Object, e As System.EventArgs) Handles DateEdit1.Leave
        dateLeave()
    End Sub
    Private Sub dateLeave()
        If LookUpEdit1.EditValue.ToString.Trim <> "" Then
            If DateEdit1.DateTime.ToString("yyyy-MM-dd") = "0001-01-01" Then
                XtraMessageBox.Show(ulf, "<size=10>Invalid date</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                DateEdit1.DateTime = Now
                DateEdit2.DateTime = Now
                DateEdit1.Select()
                Exit Sub
            Else
                If DateEdit1.DateTime > Now Then
                    XtraMessageBox.Show(ulf, "<size=10>Date Cannot be Greater than Current date</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    DateEdit1.DateTime = Now
                    DateEdit2.DateTime = Now
                    Exit Sub
                End If
                If Common.IsNepali = "Y" Then
                    Dim DC As New DateConverter()
                    Try
                        'DateEdit1.DateTime = DC.ToAD(New Date(ComboFNYear.EditValue, ComboFNMonth.SelectedIndex + 1, ComboFNDate.EditValue))
                        DateEdit1.DateTime = DC.ToAD(ComboFNYear.EditValue & "-" & ComboFNMonth.SelectedIndex + 1 & "-" & ComboFNDate.EditValue)
                        If DateEdit1.DateTime > Now Then
                            XtraMessageBox.Show(ulf, "<size=10>Date Cannot be Greater than Current date</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            DateEdit1.DateTime = Now
                            DateEdit2.DateTime = Now
                            Dim Vstart As String = DC.ToBS(New Date(DateEdit1.DateTime.Year, DateEdit1.DateTime.Month, DateEdit1.DateTime.Day))
                            Dim dojTmp() As String = Vstart.Split("-")
                            ComboFNYear.EditValue = dojTmp(0)
                            ComboFNMonth.SelectedIndex = dojTmp(1) - 1
                            ComboFNDate.EditValue = dojTmp(2)


                            Dim VEnd As String = DC.ToBS(New Date(DateEdit2.DateTime.Year, DateEdit2.DateTime.Month, DateEdit2.DateTime.Day))
                            dojTmp = VEnd.Split("-")
                            ComboPNYear.EditValue = dojTmp(0)
                            ComboPNMonth.SelectedIndex = dojTmp(1) - 1
                            ComboPNDate.EditValue = dojTmp(2)
                            Exit Sub
                        End If
                    Catch ex As Exception
                        XtraMessageBox.Show(ulf, "<size=10>Invalid Process From Date</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        ComboFNDate.Select()
                        Exit Sub
                    End Try
                End If
                DateEdit2.EditValue = DateEdit1.EditValue
                If Common.IsNepali = "Y" Then
                    Dim DC As New DateConverter()
                    Dim VEnd As String = DC.ToBS(New Date(DateEdit2.DateTime.Year, DateEdit2.DateTime.Month, DateEdit2.DateTime.Day))
                    Dim dojTmp() As String = VEnd.Split("-")
                    ComboPNYear.EditValue = dojTmp(0)
                    ComboPNMonth.SelectedIndex = dojTmp(1) - 1
                    ComboPNDate.EditValue = dojTmp(2)
                End If
            End If
            LoadPunchHistoryGrid()
        End If
    End Sub
    Private Sub LoadPunchHistoryGrid()
        If Common.IsNepali = "Y" Then
            Dim DC As New DateConverter()
            Try
                'DateEdit1.DateTime = DC.ToAD(New Date(ComboFNYear.EditValue, ComboFNMonth.SelectedIndex + 1, ComboFNDate.EditValue))
                DateEdit1.DateTime = DC.ToAD(ComboFNYear.EditValue & "-" & ComboFNMonth.SelectedIndex + 1 & "-" & ComboFNDate.EditValue)
            Catch ex As Exception
                XtraMessageBox.Show(ulf, "<size=10>Invalid Process From Date</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                ComboFNDate.Select()
                Exit Sub
            End Try
            Try
                'DateEdit2.DateTime = DC.ToAD(New Date(ComboPNYear.EditValue, ComboPNMonth.SelectedIndex + 1, ComboPNDate.EditValue))
                DateEdit2.DateTime = DC.ToAD(ComboPNYear.EditValue & "-" & ComboPNMonth.SelectedIndex + 1 & "-" & ComboPNDate.EditValue)
            Catch ex As Exception
                XtraMessageBox.Show(ulf, "<size=10>Invalid Punch Date</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                ComboPNDate.Select()
                Exit Sub
            End Try
        End If
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim ds As DataSet = New DataSet
        Dim getRTC As String = "select ISROUNDTHECLOCKWORK from tblEmployeeShiftMaster where PAYCODE='" & LookUpEdit1.EditValue.ToString.Trim & "'"
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(getRTC, Common.con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(getRTC, Common.con)
            adap.Fill(ds)
        End If
        Dim gridtblregisterselet As String
        Dim machineRawPunch As String
        If ds.Tables(0).Rows(0).Item("ISROUNDTHECLOCKWORK").ToString = "Y" Then
            DateEdit2.Enabled = True
            ComboPNDate.Enabled = True
            ComboPNMonth.Enabled = True
            ComboPNYear.Enabled = True
            ToggleSwitch1.IsOn = True

            If DateEdit1.DateTime > Now Then  'select for current date only for rtc
                If Common.servername = "Access" Then
                    gridtblregisterselet = "select * from tblTimeRegister where PayCode = '" & LookUpEdit1.EditValue.ToString.Trim & "' and FORMAT(DateOFFICE, 'yyyy-MM-dd 00:00:00') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd 00:00:00") & "'"
                    machineRawPunch = "select * from MachineRawPunch where CARDNO = '" & TextEdit2.Text.Trim & "' and FORMAT(OFFICEPUNCH,'yyyy-MM-dd 00:00:00') >= '" & DateEdit1.DateTime.ToString("yyyy-MM-dd 00:00:00") & "' and FORMAT(OFFICEPUNCH,'yyyy-MM-dd 23:59:59') <='" & DateEdit1.DateTime.AddDays(1).ToString("yyyy-MM-dd 23:59:59") & "' ORDER BY OFFICEPUNCH ASC"
                Else
                    gridtblregisterselet = "select * from tblTimeRegister where PayCode = '" & LookUpEdit1.EditValue.ToString.Trim & "' and DateOFFICE >= '" & DateEdit1.DateTime.ToString("yyyy-MM-dd 00:00:00") & "' and DateOFFICE <= '" & Now.ToString("yyyy-MM-dd 00:00:00") & "'"
                    machineRawPunch = "select * from MachineRawPunch where CARDNO = '" & TextEdit2.Text.Trim & "' and OFFICEPUNCH >= '" & DateEdit1.DateTime.ToString("yyyy-MM-dd 00:00:00") & "' and OFFICEPUNCH <='" & Now.ToString("yyyy-MM-dd 23:59:59") & "'  ORDER BY OFFICEPUNCH ASC"
                End If
            Else
                If Common.servername = "Access" Then
                    gridtblregisterselet = "select * from tblTimeRegister where PayCode = '" & LookUpEdit1.EditValue.ToString.Trim & "' and FORMAT(DateOFFICE, 'yyyy-MM-dd 00:00:00') >= '" & DateEdit1.DateTime.ToString("yyyy-MM-dd 00:00:00") & "' and FORMAT(DateOFFICE, 'yyyy-MM-dd 00:00:00') <= '" & Now.ToString("yyyy-MM-dd 00:00:00") & "'"
                    machineRawPunch = "select * from MachineRawPunch where CARDNO = '" & TextEdit2.Text.Trim & "' and FORMAT(OFFICEPUNCH,'yyyy-MM-dd 00:00:00') >= '" & DateEdit1.DateTime.ToString("yyyy-MM-dd 00:00:00") & "' and FORMAT(OFFICEPUNCH,'yyyy-MM-dd 23:59:59') <='" & Now.ToString("yyyy-MM-dd 23:59:59") & "' ORDER BY OFFICEPUNCH ASC"
                Else
                    gridtblregisterselet = "select * from tblTimeRegister where PayCode = '" & LookUpEdit1.EditValue.ToString.Trim & "' and DateOFFICE >= '" & DateEdit1.DateTime.ToString("yyyy-MM-dd 00:00:00") & "' and DateOFFICE <= '" & Now.ToString("yyyy-MM-dd 00:00:00") & "'"
                    machineRawPunch = "select * from MachineRawPunch where CARDNO = '" & TextEdit2.Text.Trim & "' and OFFICEPUNCH >= '" & DateEdit1.DateTime.ToString("yyyy-MM-dd 00:00:00") & "' and OFFICEPUNCH <='" & Now.ToString("yyyy-MM-dd 23:59:59") & "'  ORDER BY OFFICEPUNCH ASC"
                End If
            End If
        Else
            DateEdit2.Enabled = False
            ComboPNDate.Enabled = False
            ComboPNMonth.Enabled = False
            ComboPNYear.Enabled = False

            ToggleSwitch1.IsOn = False
            If Common.servername = "Access" Then
                gridtblregisterselet = "select * from tblTimeRegister where PayCode = '" & LookUpEdit1.EditValue.ToString.Trim & "' and FORMAT(DateOFFICE, 'yyyy-MM-dd 00:00:00') = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd 00:00:00") & "'"
                machineRawPunch = "select * from MachineRawPunch where PAYCODE = '" & LookUpEdit1.EditValue.ToString.Trim & "' and FORMAT(OFFICEPUNCH,'yyyy-MM-dd 00:00:00') >= '" & DateEdit1.DateTime.ToString("yyyy-MM-dd 00:00:00") & "' and FORMAT(OFFICEPUNCH,'yyyy-MM-dd 23:59:59') <='" & DateEdit1.DateTime.ToString("yyyy-MM-dd 23:59:59") & "'"
            Else
                gridtblregisterselet = "select * from tblTimeRegister where PayCode = '" & LookUpEdit1.EditValue.ToString.Trim & "' and DateOFFICE = '" & DateEdit1.DateTime.ToString("yyyy-MM-dd 00:00:00") & "'"
                machineRawPunch = "select * from MachineRawPunch where PAYCODE = '" & LookUpEdit1.EditValue.ToString.Trim & "' and OFFICEPUNCH >= '" & DateEdit1.DateTime.ToString("yyyy-MM-dd 00:00:00") & "' and OFFICEPUNCH <='" & DateEdit1.DateTime.ToString("yyyy-MM-dd 23:59:59") & "'"
            End If
        End If
        Dim WTDataTable As DataTable
        If Common.servername = "Access" Then
            Dim dataAdapter As New OleDbDataAdapter(gridtblregisterselet, Common.con1)
            WTDataTable = New DataTable("tblTimeRegister")
            dataAdapter.Fill(WTDataTable)
        Else
            Dim dataAdapter As New SqlClient.SqlDataAdapter(gridtblregisterselet, Common.con)
            WTDataTable = New DataTable("tblTimeRegister")
            dataAdapter.Fill(WTDataTable)
        End If
        GridControl1.DataSource = WTDataTable
        If WTDataTable.Rows.Count = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>No Roster present for employee." & vbCrLf & "Create Roster or Punch Date is less than date of joining of employee.</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            setDefault()
            LookUpEdit1.Select()
            Exit Sub
        End If

        Dim WTDataTable1 As DataTable
        If Common.servername = "Access" Then
            Dim dataAdapter As New OleDbDataAdapter(machineRawPunch, Common.con1)
            WTDataTable1 = New DataTable("MachineRawPunch")
            dataAdapter.Fill(WTDataTable1)
        Else
            Dim dataAdapter As New SqlClient.SqlDataAdapter(machineRawPunch, Common.con)
            WTDataTable1 = New DataTable("MachineRawPunch")
            dataAdapter.Fill(WTDataTable1)
        End If
        'MsgBox(WTDataTable1.Rows.Count)
        GridControl2.DataSource = WTDataTable1
        SimpleButton1.Enabled = True


        'Dim sSql As String = "select OTDURATION from tbltimeregister where paycode ='" & LookUpEdit1.EditValue.ToString.Trim & "' and dateOffice='" & DateEdit1.DateTime.ToString("yyyy-MM-dd 00:00:00") & "'"
        'Dim dsT As DataSet = New DataSet
        'Dim adapT As SqlDataAdapter
        'Dim adapTA As OleDbDataAdapter
        'Try
        '    If Common.servername = "Access" Then
        '        sSql = sSql.Replace("DateOFFICE", "FORMAT(DateOFFICE, 'yyyy-MM-dd 00:00:00')")
        '        adapTA = New OleDbDataAdapter(sSql, Common.con1)
        '        adapTA.Fill(dsT)
        '    Else
        '        adapT = New SqlDataAdapter(sSql, Common.con)
        '        adapT.Fill(dsT)
        '    End If
        '    If dsT.Tables(0).Rows.Count > 0 Then
        '        If dsT.Tables(0).Rows(0).Item("OTDURATION").ToString.Trim <> "" Then
        '            TextOT.Text = (Math.Truncate(Convert.ToInt32(dsT.Tables(0).Rows(0).Item("OTDURATION").ToString.Trim) / 60)).ToString("00") & ":" & (Convert.ToInt32(dsT.Tables(0).Rows(0).Item("OTDURATION").ToString.Trim) Mod 60).ToString("00")
        '        Else
        '            TextOT.Text = "00:00"
        '        End If
        '    Else
        '        TextOT.Text = "00:00"
        '    End If
        'Catch ex As Exception
        '    TextOT.Text = "00:00"
        'End Try
    End Sub
    Private Sub TextEdit3_Leave(sender As System.Object, e As System.EventArgs) Handles TextEdit3.Leave
        If LookUpEdit1.EditValue.ToString.Trim <> "" And DateEdit1.DateTime.ToString("yyyy-MM-dd") <> "0001-01-01" Then
            SimpleButton1.Enabled = True
        End If
    End Sub
    Private Sub SimpleButton1_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton1.Click
        If Common.IsNepali = "Y" Then
            Dim DC As New DateConverter()
            Try
                'DateEdit1.DateTime = DC.ToAD(New Date(ComboFNYear.EditValue, ComboFNMonth.SelectedIndex + 1, ComboFNDate.EditValue))
                DateEdit1.DateTime = DC.ToAD(ComboFNYear.EditValue & "-" & ComboFNMonth.SelectedIndex + 1 & "-" & ComboFNDate.EditValue)
            Catch ex As Exception
                XtraMessageBox.Show(ulf, "<size=10>Invalid Process From Date</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                ComboFNDate.Select()
                Exit Sub
            End Try
            Try
                'DateEdit2.DateTime = DC.ToAD(New Date(ComboPNYear.EditValue, ComboPNMonth.SelectedIndex + 1, ComboPNDate.EditValue))
                DateEdit2.DateTime = DC.ToAD(ComboPNYear.EditValue & "-" & ComboPNMonth.SelectedIndex + 1 & "-" & ComboPNDate.EditValue)
            Catch ex As Exception
                XtraMessageBox.Show(ulf, "<size=10>Invalid Punch Date</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                ComboPNDate.Select()
                Exit Sub
            End Try
        End If
        Me.Cursor = Cursors.WaitCursor
        Dim OFFICEPUNCH As DateTime = Convert.ToDateTime(DateEdit2.DateTime.ToString("yyyy-MM-dd") & " " & TextEdit3.Text.Trim & ":00")
        Dim Purpose As String = ""
        Dim Remark As String = TextEditRemark.Text.Trim
        If ComboPurpose.SelectedIndex = 0 Then
            Purpose = "O"
        ElseIf ComboPurpose.SelectedIndex = 1 Then
            If TextEditRemark.Text.Trim = "" Then
                XtraMessageBox.Show(ulf, "<size=10>Remark Cannot be Empty</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                TextEditRemark.Select()
                Exit Sub
            End If
            Purpose = "P"
        ElseIf ComboPurpose.SelectedIndex = 2 Then
            Purpose = "D"
        End If
        
        'MsgBox(OFFICEPUNCH.ToString)
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim ds As DataSet = New DataSet
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter("select CARDNO from MachineRawPunch where CARDNO='" & TextEdit2.Text.Trim & "' and FORMAT(OFFICEPUNCH,'yyyy-MM-dd HH:mm:ss')='" & OFFICEPUNCH.ToString("yyyy-MM-dd HH:mm:ss") & "'", Common.con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter("select CARDNO from MachineRawPunch where CARDNO='" & TextEdit2.Text.Trim & "' and OFFICEPUNCH='" & OFFICEPUNCH.ToString("yyyy-MM-dd HH:mm:ss") & "'", Common.con)
            adap.Fill(ds)
        End If
        If ds.Tables(0).Rows.Count > 0 Then
            XtraMessageBox.Show(ulf, "<size=10>Punch already present for date and time.</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Me.Cursor = Cursors.Default
            Exit Sub
        End If
        Dim sSql As String = "insert INTO MachineRawPunch (CARDNO,OFFICEPUNCH,P_DAY,ISMANUAL,PAYCODE, Purpose, Remark) VALUES('" & TextEdit2.Text.Trim & "','" & OFFICEPUNCH.ToString("yyyy-MM-dd HH:mm:ss") & "','N','Y','" & LookUpEdit1.EditValue.ToString.Trim & "','" & Purpose & "','" & Remark & "')"
        Dim sSql1 As String = "insert INTO MachineRawPunchAll (CARDNO,OFFICEPUNCH,P_DAY,ISMANUAL,PAYCODE, Purpose, Remark) VALUES('" & TextEdit2.Text.Trim & "','" & OFFICEPUNCH.ToString("yyyy-MM-dd HH:mm:ss") & "','N','Y','" & LookUpEdit1.EditValue.ToString.Trim & "','" & Purpose & "','" & Remark & "')"
        If Common.servername = "Access" Then
            'sSql = "insert INTO MachineRawPunch (CARDNO,FORMAT(OFFICEPUNCH,'yyyy-MM-dd HH:mm:ss'),P_DAY,ISMANUAL,PAYCODE) VALUES('" & TextEdit2.Text.Trim & "','" & OFFICEPUNCH.ToString("yyyy-MM-dd HH:mm:ss") & "','N','Y','" & LookUpEdit1.EditValue.ToString.Trim & "')"
            If Common.con1.State <> ConnectionState.Open Then
                Common.con1.Open()
            End If
            cmd1 = New OleDbCommand(sSql, Common.con1)
            cmd1.ExecuteNonQuery()
            Try
                cmd1 = New OleDbCommand(sSql1, Common.con1)
                cmd1.ExecuteNonQuery()
            Catch ex As Exception
            End Try
            Common.con1.Close()
        Else
            If Common.con.State <> ConnectionState.Open Then
                Common.con.Open()
            End If
            cmd = New SqlCommand(sSql, Common.con)
            cmd.ExecuteNonQuery()
            Try
                cmd = New SqlCommand(sSql1, Common.con)
                cmd.ExecuteNonQuery()
            Catch ex As Exception

            End Try
            Common.con.Close()
        End If
        Common.LogPost("Manual Punch Entry; Paycode=" & LookUpEdit1.EditValue.ToString.Trim & ", Punch Time=" & OFFICEPUNCH.ToString("yyyy-MM-dd HH:mm:ss"))
        If Common.IsParallel = "Y" Then
            Dim cardNo As String
            If IsNumeric(TextEdit2.Text.Trim) Then
                cardNo = Convert.ToDouble(TextEdit2.Text.Trim)
            Else
                cardNo = TextEdit2.Text.Trim
            End If
            Common.parallelInsert(cardNo, LookUpEdit1.EditValue.ToString.Trim, OFFICEPUNCH, "N", "Y", LabelControl6.Text.Trim, "0", "0")
        End If
        Dim comclass As Common = New Common

        Dim adap1 As SqlDataAdapter
        Dim adapA1 As OleDbDataAdapter
        Dim ds1 As DataSet = New DataSet
        Dim getRTC As String = "select tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK,EmployeeGroup.ID from tblEmployeeShiftMaster, TblEmployee, EmployeeGroup where tblEmployeeShiftMaster.PAYCODE='" & LookUpEdit1.EditValue.ToString.Trim & "' and tblEmployeeShiftMaster.PAYCODE= TblEmployee.PAYCODE and TblEmployee.EmployeeGroupId = EmployeeGroup.GroupId"
        ' "select ISROUNDTHECLOCKWORK from tblEmployeeShiftMaster where PAYCODE='" & LookUpEdit1.EditValue.ToString.Trim & "'"
        If Common.servername = "Access" Then
            adapA1 = New OleDbDataAdapter(getRTC, Common.con1)
            adapA1.Fill(ds1)
        Else
            adap1 = New SqlDataAdapter(getRTC, Common.con)
            adap1.Fill(ds1)
        End If
        comclass.Remove_Duplicate_Punches(DateEdit1.DateTime, LookUpEdit1.EditValue.ToString.Trim, ds1.Tables(0).Rows(0).Item("Id"))
        If ds1.Tables(0).Rows(0).Item("ISROUNDTHECLOCKWORK").ToString = "Y" Then
            'comclass.Process_AllRTC(DateEdit1.DateTime.AddDays(-1), Now, TextEdit1.Text.Trim, TextEdit1.Text.Trim)
            If DateEdit1.DateTime > Now Then
                If Common.PrcessMode = "M" Then
                    comclass.Process_AllnonRTCINOUT(DateEdit2.DateTime.AddDays(-1), DateEdit2.DateTime.AddDays(1), LookUpEdit1.EditValue.ToString.Trim, LookUpEdit1.EditValue.ToString.Trim, ds1.Tables(0).Rows(0).Item("Id"), ds1.Tables(0).Rows(0).Item("ISROUNDTHECLOCKWORK").ToString)
                Else
                    comclass.Process_AllRTC(DateEdit2.DateTime.AddDays(-1), DateEdit2.DateTime.AddDays(1), LookUpEdit1.EditValue.ToString.Trim, LookUpEdit1.EditValue.ToString.Trim, ds1.Tables(0).Rows(0).Item("Id"))
                End If
                'comclass.Process_AllRTC(DateEdit2.DateTime.AddDays(-1), DateEdit2.DateTime.AddDays(1), LookUpEdit1.EditValue.ToString.Trim, LookUpEdit1.EditValue.ToString.Trim, ds1.Tables(0).Rows(0).Item("Id"))
            Else
                If Common.PrcessMode = "M" Then
                    comclass.Process_AllnonRTCINOUT(DateEdit2.DateTime.AddDays(-1), Now, LookUpEdit1.EditValue.ToString.Trim, LookUpEdit1.EditValue.ToString.Trim, ds1.Tables(0).Rows(0).Item("Id"), ds1.Tables(0).Rows(0).Item("ISROUNDTHECLOCKWORK").ToString)
                Else
                    comclass.Process_AllRTC(DateEdit2.DateTime.AddDays(-1), Now, LookUpEdit1.EditValue.ToString.Trim, LookUpEdit1.EditValue.ToString.Trim, ds1.Tables(0).Rows(0).Item("Id"))
                End If
                'comclass.Process_AllRTC(DateEdit2.DateTime.AddDays(-1), Now, LookUpEdit1.EditValue.ToString.Trim, LookUpEdit1.EditValue.ToString.Trim, ds1.Tables(0).Rows(0).Item("Id"))
            End If
            'comclass.Process_AllRTC(DateEdit1.DateTime, DateEdit1.DateTime, TextEdit1.Text.Trim, TextEdit1.Text.Trim)
        Else
            If Common.PrcessMode = "M" Then
                comclass.Process_AllnonRTCINOUT(DateEdit1.DateTime, DateEdit1.DateTime, LookUpEdit1.EditValue.ToString.Trim, LookUpEdit1.EditValue.ToString.Trim, ds1.Tables(0).Rows(0).Item("Id"), ds1.Tables(0).Rows(0).Item("ISROUNDTHECLOCKWORK").ToString)
            Else
                comclass.Process_AllnonRTC(DateEdit1.DateTime, DateEdit1.DateTime, LookUpEdit1.EditValue.ToString.Trim, LookUpEdit1.EditValue.ToString.Trim, ds1.Tables(0).Rows(0).Item("Id"))
            End If
            'comclass.Process_AllnonRTC(DateEdit1.DateTime, DateEdit1.DateTime, LookUpEdit1.EditValue.ToString.Trim, LookUpEdit1.EditValue.ToString.Trim, ds1.Tables(0).Rows(0).Item("Id"))
            If Common.EmpGrpArr(ds1.Tables(0).Rows(0).Item("Id")).SHIFTTYPE = "M" Then
                comclass.Process_AllnonRTCMulti(DateEdit1.DateTime, DateEdit1.DateTime, LookUpEdit1.EditValue.ToString.Trim, LookUpEdit1.EditValue.ToString.Trim, ds1.Tables(0).Rows(0).Item("Id"))
            End If
        End If
        XtraMasterTest.LabelControlStatus.Text = ""
        Application.DoEvents()
        LoadPunchHistoryGrid()
        Me.Cursor = Cursors.Default
    End Sub
    Private Sub GridControl2_EmbeddedNavigator_ButtonClick(sender As System.Object, e As DevExpress.XtraEditors.NavigatorButtonClickEventArgs) Handles GridControl2.EmbeddedNavigator.ButtonClick
        If e.Button.ButtonType = DevExpress.XtraEditors.NavigatorButtonType.Remove Then
            If XtraMessageBox.Show(ulf, "<size=10>" & Common.res_man.GetString("askdelete", Common.cul) & "</size>", Common.res_man.GetString("confirmdeleteion", Common.cul), _
                              MessageBoxButtons.YesNo, MessageBoxIcon.Question) <> DialogResult.Yes Then
                Me.Validate()
                e.Handled = True
                'MsgBox("Your records have been saved and updated successfully!")
            Else
                Me.Validate()
                e.Handled = True
                Dim row As System.Data.DataRow = GridView2.GetDataRow(GridView2.FocusedRowHandle)
                Dim OFFICEPUNCH As DateTime = Convert.ToDateTime(row("OFFICEPUNCH").ToString.Trim)

                Dim sSql As String = "delete from MachineRawPunch where CARDNO ='" & TextEdit2.Text.Trim & "' and OFFICEPUNCH = '" & OFFICEPUNCH.ToString("yyyy-MM-dd HH:mm:ss") & "'"
                If Common.servername = "Access" Then
                    sSql = "delete from MachineRawPunch where CARDNO ='" & TextEdit2.Text.Trim & "' and FORMAT(OFFICEPUNCH,'yyyy-MM-dd HH:mm:ss') = '" & OFFICEPUNCH.ToString("yyyy-MM-dd HH:mm:ss") & "'"
                    If Common.con1.State <> ConnectionState.Open Then
                        Common.con1.Open()
                    End If
                    cmd1 = New OleDbCommand(sSql, Common.con1)
                    cmd1.ExecuteNonQuery()

                    sSql = sSql.Replace("MachineRawPunch", "MachineRawPunchAll")
                    cmd1 = New OleDbCommand(sSql, Common.con1)
                    cmd1.ExecuteNonQuery()

                    Common.con1.Close()
                Else
                    If Common.con.State <> ConnectionState.Open Then
                        Common.con.Open()
                    End If
                    cmd = New SqlCommand(sSql, Common.con)
                    cmd.ExecuteNonQuery()

                    sSql = sSql.Replace("MachineRawPunch", "MachineRawPunchAll")
                    cmd = New SqlCommand(sSql, Common.con)
                    cmd.ExecuteNonQuery()
                    Common.con.Close()
                End If
                Common.LogPost("Manual Punch Delete;Paycode=" & LookUpEdit1.EditValue.ToString.Trim & ", Punch Time=" & OFFICEPUNCH.ToString("yyyy-MM-dd HH:mm:ss"))
                Dim comclass As Common = New Common

                Dim adap As SqlDataAdapter
                Dim adapA As OleDbDataAdapter
                Dim ds As DataSet = New DataSet
                sSql = "select tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK,EmployeeGroup.ID from tblEmployeeShiftMaster, TblEmployee, EmployeeGroup where tblEmployeeShiftMaster.PAYCODE='" & LookUpEdit1.EditValue.ToString.Trim & "' and tblEmployeeShiftMaster.PAYCODE= TblEmployee.PAYCODE and TblEmployee.EmployeeGroupId = EmployeeGroup.GroupId"
                '"select ISROUNDTHECLOCKWORK from tblEmployeeShiftMaster where PAYCODE = '" & LookUpEdit1.EditValue.ToString.Trim & "'"
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(ds)
                Else
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(ds)
                End If
                If ds.Tables(0).Rows(0).Item("ISROUNDTHECLOCKWORK").ToString = "Y" Then
                    If Common.PrcessMode = "M" Then
                        comclass.Process_AllnonRTCINOUT(DateEdit1.DateTime.AddDays(-1), Now, LookUpEdit1.EditValue.ToString.Trim, LookUpEdit1.EditValue.ToString.Trim, ds.Tables(0).Rows(0).Item("Id"), ds.Tables(0).Rows(0).Item("ISROUNDTHECLOCKWORK").ToString)
                    Else
                        comclass.Process_AllRTC(DateEdit1.DateTime.AddDays(-1), Now, LookUpEdit1.EditValue.ToString.Trim, LookUpEdit1.EditValue.ToString.Trim, ds.Tables(0).Rows(0).Item("Id"))
                    End If
                    'comclass.Process_AllRTC(DateEdit1.DateTime.AddDays(-1), Now, LookUpEdit1.EditValue.ToString.Trim, LookUpEdit1.EditValue.ToString.Trim, ds.Tables(0).Rows(0).Item("Id"))
                Else
                    If Common.PrcessMode = "M" Then
                        comclass.Process_AllnonRTCINOUT(DateEdit1.DateTime, DateEdit1.DateTime, LookUpEdit1.EditValue.ToString.Trim, LookUpEdit1.EditValue.ToString.Trim, ds.Tables(0).Rows(0).Item("Id"), ds.Tables(0).Rows(0).Item("ISROUNDTHECLOCKWORK").ToString)
                    Else
                        comclass.Process_AllnonRTC(DateEdit1.DateTime, DateEdit1.DateTime, LookUpEdit1.EditValue.ToString.Trim, LookUpEdit1.EditValue.ToString.Trim, ds.Tables(0).Rows(0).Item("Id"))
                    End If
                    'comclass.Process_AllnonRTC(DateEdit1.DateTime, DateEdit1.DateTime, LookUpEdit1.EditValue.ToString.Trim, LookUpEdit1.EditValue.ToString.Trim, ds.Tables(0).Rows(0).Item("Id"))
                    If Common.EmpGrpArr(ds.Tables(0).Rows(0).Item("Id")).SHIFTTYPE = "M" Then
                        comclass.Process_AllnonRTCMulti(DateEdit1.DateTime, DateEdit1.DateTime, LookUpEdit1.EditValue.ToString.Trim, LookUpEdit1.EditValue.ToString.Trim, ds.Tables(0).Rows(0).Item("Id"))
                    End If
                End If
                XtraMasterTest.LabelControlStatus.Text = ""
                Application.DoEvents()
                LoadPunchHistoryGrid()
            End If
        End If
    End Sub
    Private Sub GridView2_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles GridView2.KeyDown
        If (e.KeyCode = Keys.Delete And e.Modifiers = Keys.Control) Then
            If (MessageBox.Show("Delete row?", "Confirmation", _
              MessageBoxButtons.YesNo) <> DialogResult.Yes) Then Return
            Dim view As GridView = CType(sender, GridView)
            view.DeleteRow(view.FocusedRowHandle)
        End If
    End Sub
    Private Sub GridView1_CustomColumnDisplayText(sender As System.Object, e As DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs) Handles GridView1.CustomColumnDisplayText
        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
        Dim view As ColumnView = TryCast(sender, ColumnView)
        If e.Column.FieldName = "HOURSWORKED" Then
            If row("HOURSWORKED").ToString.Trim = "0" Or row("HOURSWORKED").ToString.Trim = "" Then
                e.DisplayText = ""
            Else
                e.DisplayText = (view.GetListSourceRowCellValue(e.ListSourceRowIndex, "HOURSWORKED").ToString().Trim \ 60).ToString("00") & ":" & (view.GetListSourceRowCellValue(e.ListSourceRowIndex, "HOURSWORKED").ToString().Trim Mod 60).ToString("00")
            End If
        End If

        If e.Column.FieldName = "OTDURATION" Then
            If row("OTDURATION").ToString.Trim = "0" Or row("OTDURATION").ToString.Trim = "" Then
                e.DisplayText = ""
            Else
                e.DisplayText = (view.GetListSourceRowCellValue(e.ListSourceRowIndex, "OTDURATION").ToString().Trim \ 60).ToString("00") & ":" & (view.GetListSourceRowCellValue(e.ListSourceRowIndex, "OTDURATION").ToString().Trim Mod 60).ToString("00")
            End If
        End If



        If Common.IsNepali = "Y" Then
            If e.Column.FieldName = "DateOFFICE" Then
                If row("DateOFFICE").ToString.Trim <> "" Then
                    Dim DC As New DateConverter()
                    Dim dt As DateTime = Convert.ToDateTime(row("DateOFFICE").ToString.Trim)
                    Dim Vstart As String = DC.ToBS(New Date(dt.Year, dt.Month, dt.Day))
                    Dim dojTmp() As String = Vstart.Split("-")
                    e.DisplayText = dojTmp(2) & "-" & ComboFNMonth.Properties.Items(dojTmp(1) - 1).ToString & "-" & dojTmp(0)
                End If
            End If

            If e.Column.FieldName = "IN1" Then
                If row("IN1").ToString.Trim <> "" Then
                    Dim DC As New DateConverter()
                    Dim dt As DateTime = Convert.ToDateTime(row("IN1").ToString.Trim)
                    Dim Vstart As String = DC.ToBS(New Date(dt.Year, dt.Month, dt.Day))
                    'e.DisplayText = Vstart.Day & "-" & ComboFNMonth.Properties.Items(Vstart.Month - 1).ToString & "-" & Vstart.Year
                    Dim dojTmp() As String = Vstart.Split("-")
                    e.DisplayText = dojTmp(2) & "-" & ComboFNMonth.Properties.Items(dojTmp(1) - 1).ToString & "-" & dojTmp(0)

                End If
            End If

            If e.Column.FieldName = "OUT2" Then
                If row("OUT2").ToString.Trim <> "" Then
                    Dim DC As New DateConverter()
                    Dim dt As DateTime = Convert.ToDateTime(row("OUT2").ToString.Trim)
                    Dim Vstart As String = DC.ToBS(New Date(dt.Year, dt.Month, dt.Day))
                    'e.DisplayText = Vstart.Day & "-" & ComboFNMonth.Properties.Items(Vstart.Month - 1).ToString & "-" & Vstart.Year
                    Dim dojTmp() As String = Vstart.Split("-")
                    e.DisplayText = dojTmp(2) & "-" & ComboFNMonth.Properties.Items(dojTmp(1) - 1).ToString & "-" & dojTmp(0)

                End If
            End If

            If e.Column.FieldName = "OUT1" Then
                If row("OUT1").ToString.Trim <> "" Then
                    Dim DC As New DateConverter()
                    Dim dt As DateTime = Convert.ToDateTime(row("OUT1").ToString.Trim)
                    Dim Vstart As String = DC.ToBS(New Date(dt.Year, dt.Month, dt.Day))
                    'e.DisplayText = Vstart.Day & "-" & ComboFNMonth.Properties.Items(Vstart.Month - 1).ToString & "-" & Vstart.Year
                    Dim dojTmp() As String = Vstart.Split("-")
                    e.DisplayText = dojTmp(2) & "-" & ComboFNMonth.Properties.Items(dojTmp(1) - 1).ToString & "-" & dojTmp(0)

                End If
            End If

            If e.Column.FieldName = "IN2" Then
                If row("IN2").ToString.Trim <> "" Then
                    Dim DC As New DateConverter()
                    Dim dt As DateTime = Convert.ToDateTime(row("IN2").ToString.Trim)
                    Dim Vstart As String = DC.ToBS(New Date(dt.Year, dt.Month, dt.Day))
                    'e.DisplayText = Vstart.Day & "-" & ComboFNMonth.Properties.Items(Vstart.Month - 1).ToString & "-" & Vstart.Year
                    Dim dojTmp() As String = Vstart.Split("-")
                    e.DisplayText = dojTmp(2) & "-" & ComboFNMonth.Properties.Items(dojTmp(1) - 1).ToString & "-" & dojTmp(0)
                End If
            End If
        End If
    End Sub
    Private Sub ComboFNYear_Leave(sender As System.Object, e As System.EventArgs) Handles ComboFNYear.Leave
        dateLeave()
    End Sub
    Private Sub GridView2_CustomColumnDisplayText(sender As System.Object, e As DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs) Handles GridView2.CustomColumnDisplayText
        Dim row As System.Data.DataRow = GridView2.GetDataRow(GridView2.FocusedRowHandle)
        Dim view As ColumnView = TryCast(sender, ColumnView)
        If Common.IsNepali = "Y" Then
            If e.Column.Name = "colOFFICEPUNCH" Then
                If row("OFFICEPUNCH").ToString.Trim <> "" Then
                    Dim DC As New DateConverter()
                    Dim dt As DateTime = Convert.ToDateTime(row("OFFICEPUNCH").ToString.Trim)
                    Dim Vstart As String = DC.ToBS(New Date(dt.Year, dt.Month, dt.Day))
                    Dim dojTmp() As String = Vstart.Split("-")
                    e.DisplayText = dojTmp(2) & "-" & ComboFNMonth.Properties.Items(dojTmp(1) - 1).ToString & "-" & dojTmp(0)
                End If
            End If
        End If
    End Sub
    Private Sub SimpleButtonExcel_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonExcel.Click
        XtraManualPunchUpload.ShowDialog()
    End Sub

End Class
