﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraCustomisedReportEdit
    Inherits DevExpress.XtraEditors.XtraUserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(XtraCustomisedReportEdit))
        Me.SplitContainerControl1 = New DevExpress.XtraEditors.SplitContainerControl()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.GroupControl3 = New DevExpress.XtraEditors.GroupControl()
        Me.TextTempRPos = New DevExpress.XtraEditors.TextEdit()
        Me.TextTempRCap = New DevExpress.XtraEditors.TextEdit()
        Me.CheckTempR = New DevExpress.XtraEditors.CheckEdit()
        Me.TextExcessLunchPos = New DevExpress.XtraEditors.TextEdit()
        Me.TextExcessLunchCap = New DevExpress.XtraEditors.TextEdit()
        Me.CheckExcessLunch = New DevExpress.XtraEditors.CheckEdit()
        Me.TextDatePos = New DevExpress.XtraEditors.TextEdit()
        Me.TextDateCap = New DevExpress.XtraEditors.TextEdit()
        Me.CheckDate = New DevExpress.XtraEditors.CheckEdit()
        Me.TextOSPos = New DevExpress.XtraEditors.TextEdit()
        Me.TextOSCap = New DevExpress.XtraEditors.TextEdit()
        Me.TextOTPos = New DevExpress.XtraEditors.TextEdit()
        Me.TextOTCap = New DevExpress.XtraEditors.TextEdit()
        Me.TextEarlyDPos = New DevExpress.XtraEditors.TextEdit()
        Me.TextEarlyDCap = New DevExpress.XtraEditors.TextEdit()
        Me.TextLatePos = New DevExpress.XtraEditors.TextEdit()
        Me.TextLateCap = New DevExpress.XtraEditors.TextEdit()
        Me.TextHourWorkedPos = New DevExpress.XtraEditors.TextEdit()
        Me.TextHourWorkedCap = New DevExpress.XtraEditors.TextEdit()
        Me.TextStatusPos = New DevExpress.XtraEditors.TextEdit()
        Me.TextStatusCap = New DevExpress.XtraEditors.TextEdit()
        Me.TextOut2Pos = New DevExpress.XtraEditors.TextEdit()
        Me.TextOut2Cap = New DevExpress.XtraEditors.TextEdit()
        Me.TextIn2Pos = New DevExpress.XtraEditors.TextEdit()
        Me.TextIn2Cap = New DevExpress.XtraEditors.TextEdit()
        Me.TextOut1Pos = New DevExpress.XtraEditors.TextEdit()
        Me.TextOut1Cap = New DevExpress.XtraEditors.TextEdit()
        Me.TextIn1Pos = New DevExpress.XtraEditors.TextEdit()
        Me.TextIn1Cap = New DevExpress.XtraEditors.TextEdit()
        Me.TextShiftEndPos = New DevExpress.XtraEditors.TextEdit()
        Me.TextShiftEndCap = New DevExpress.XtraEditors.TextEdit()
        Me.TextShiftStartPos = New DevExpress.XtraEditors.TextEdit()
        Me.TextShiftStartCap = New DevExpress.XtraEditors.TextEdit()
        Me.TextSAttPos = New DevExpress.XtraEditors.TextEdit()
        Me.TextSAttCap = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.CheckOS = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckOT = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckShiftEnd = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckShiftStart = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckShift = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEarlyDeparture = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckIN1 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckLate = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckOut1 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckHourWorked = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckIN2 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckStatus = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckOut2 = New DevExpress.XtraEditors.CheckEdit()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.ComboBKType = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.TextDateFormat = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl18 = New DevExpress.XtraEditors.LabelControl()
        Me.TextPriTextPay = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.TextPriFixLenghtPay = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.CheckPrefixPrePaycode = New DevExpress.XtraEditors.CheckEdit()
        Me.TextPriTextPre = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.TextPriFixLenghtPre = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.CheckPrefixPreCard = New DevExpress.XtraEditors.CheckEdit()
        Me.TextGenPos = New DevExpress.XtraEditors.TextEdit()
        Me.TextGenCap = New DevExpress.XtraEditors.TextEdit()
        Me.TextDojPos = New DevExpress.XtraEditors.TextEdit()
        Me.TextDojCap = New DevExpress.XtraEditors.TextEdit()
        Me.TextDesPos = New DevExpress.XtraEditors.TextEdit()
        Me.TextDesCap = New DevExpress.XtraEditors.TextEdit()
        Me.TextGNamePos = New DevExpress.XtraEditors.TextEdit()
        Me.TextGNameCap = New DevExpress.XtraEditors.TextEdit()
        Me.TextEmpGrpPos = New DevExpress.XtraEditors.TextEdit()
        Me.TextEmpGrpCap = New DevExpress.XtraEditors.TextEdit()
        Me.TextCatPos = New DevExpress.XtraEditors.TextEdit()
        Me.TextCatCap = New DevExpress.XtraEditors.TextEdit()
        Me.TextGradPos = New DevExpress.XtraEditors.TextEdit()
        Me.TextGradeCap = New DevExpress.XtraEditors.TextEdit()
        Me.TextDeptPos = New DevExpress.XtraEditors.TextEdit()
        Me.TextDeptCap = New DevExpress.XtraEditors.TextEdit()
        Me.TextLocPos = New DevExpress.XtraEditors.TextEdit()
        Me.TextLocCap = New DevExpress.XtraEditors.TextEdit()
        Me.TextComPos = New DevExpress.XtraEditors.TextEdit()
        Me.TextComCap = New DevExpress.XtraEditors.TextEdit()
        Me.TextUserNoPos = New DevExpress.XtraEditors.TextEdit()
        Me.TextUserNoCap = New DevExpress.XtraEditors.TextEdit()
        Me.TextNamePos = New DevExpress.XtraEditors.TextEdit()
        Me.TextNameCap = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.TextPayPos = New DevExpress.XtraEditors.TextEdit()
        Me.TextPayCap = New DevExpress.XtraEditors.TextEdit()
        Me.CheckGender = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckDoj = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckDes = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckGName = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEmpPay = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEmpGrp = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEmpName = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckCat = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckUserNo = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckGrade = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckCompany = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckDept = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckLocation = New DevExpress.XtraEditors.CheckEdit()
        Me.SidePanel1 = New DevExpress.XtraEditors.SidePanel()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.MemoEdit1 = New DevExpress.XtraEditors.MemoEdit()
        Me.TextUidPos = New DevExpress.XtraEditors.TextEdit()
        Me.TextUidCap = New DevExpress.XtraEditors.TextEdit()
        Me.CheckEditUID = New DevExpress.XtraEditors.CheckEdit()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerControl1.SuspendLayout()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.TextTempRPos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextTempRCap.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckTempR.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextExcessLunchPos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextExcessLunchCap.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckExcessLunch.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextDatePos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextDateCap.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextOSPos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextOSCap.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextOTPos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextOTCap.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEarlyDPos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEarlyDCap.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextLatePos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextLateCap.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextHourWorkedPos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextHourWorkedCap.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextStatusPos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextStatusCap.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextOut2Pos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextOut2Cap.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextIn2Pos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextIn2Cap.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextOut1Pos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextOut1Cap.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextIn1Pos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextIn1Cap.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextShiftEndPos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextShiftEndCap.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextShiftStartPos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextShiftStartCap.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextSAttPos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextSAttCap.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckOS.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckOT.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckShiftEnd.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckShiftStart.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckShift.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEarlyDeparture.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckIN1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckLate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckOut1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckHourWorked.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckIN2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckStatus.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckOut2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.ComboBKType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextDateFormat.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextPriTextPay.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextPriFixLenghtPay.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckPrefixPrePaycode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextPriTextPre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextPriFixLenghtPre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckPrefixPreCard.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextGenPos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextGenCap.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextDojPos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextDojCap.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextDesPos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextDesCap.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextGNamePos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextGNameCap.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEmpGrpPos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEmpGrpCap.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextCatPos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextCatCap.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextGradPos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextGradeCap.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextDeptPos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextDeptCap.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextLocPos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextLocCap.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextComPos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextComCap.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextUserNoPos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextUserNoCap.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextNamePos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextNameCap.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextPayPos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextPayCap.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckGender.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckDoj.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckDes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckGName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEmpPay.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEmpGrp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEmpName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckCat.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckUserNo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckGrade.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckCompany.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckDept.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckLocation.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SidePanel1.SuspendLayout()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextUidPos.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextUidCap.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditUID.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SplitContainerControl1
        '
        Me.SplitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainerControl1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainerControl1.Name = "SplitContainerControl1"
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.GroupControl1)
        Me.SplitContainerControl1.Panel1.Text = "Panel1"
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.MemoEdit1)
        Me.SplitContainerControl1.Panel2.Text = "Panel2"
        Me.SplitContainerControl1.Size = New System.Drawing.Size(1145, 568)
        Me.SplitContainerControl1.SplitterPosition = 1036
        Me.SplitContainerControl1.TabIndex = 2
        Me.SplitContainerControl1.Text = "SplitContainerControl1"
        '
        'GroupControl1
        '
        Me.GroupControl1.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.GroupControl1.AppearanceCaption.ForeColor = System.Drawing.Color.Blue
        Me.GroupControl1.AppearanceCaption.Options.UseFont = True
        Me.GroupControl1.AppearanceCaption.Options.UseForeColor = True
        Me.GroupControl1.Controls.Add(Me.GroupControl3)
        Me.GroupControl1.Controls.Add(Me.GroupControl2)
        Me.GroupControl1.Controls.Add(Me.SidePanel1)
        Me.GroupControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupControl1.Location = New System.Drawing.Point(0, 0)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(1036, 568)
        Me.GroupControl1.TabIndex = 4
        Me.GroupControl1.Text = "Select Columns "
        '
        'GroupControl3
        '
        Me.GroupControl3.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.GroupControl3.AppearanceCaption.ForeColor = System.Drawing.Color.Blue
        Me.GroupControl3.AppearanceCaption.Options.UseFont = True
        Me.GroupControl3.AppearanceCaption.Options.UseForeColor = True
        Me.GroupControl3.Controls.Add(Me.TextTempRPos)
        Me.GroupControl3.Controls.Add(Me.TextTempRCap)
        Me.GroupControl3.Controls.Add(Me.CheckTempR)
        Me.GroupControl3.Controls.Add(Me.TextExcessLunchPos)
        Me.GroupControl3.Controls.Add(Me.TextExcessLunchCap)
        Me.GroupControl3.Controls.Add(Me.CheckExcessLunch)
        Me.GroupControl3.Controls.Add(Me.TextDatePos)
        Me.GroupControl3.Controls.Add(Me.TextDateCap)
        Me.GroupControl3.Controls.Add(Me.CheckDate)
        Me.GroupControl3.Controls.Add(Me.TextOSPos)
        Me.GroupControl3.Controls.Add(Me.TextOSCap)
        Me.GroupControl3.Controls.Add(Me.TextOTPos)
        Me.GroupControl3.Controls.Add(Me.TextOTCap)
        Me.GroupControl3.Controls.Add(Me.TextEarlyDPos)
        Me.GroupControl3.Controls.Add(Me.TextEarlyDCap)
        Me.GroupControl3.Controls.Add(Me.TextLatePos)
        Me.GroupControl3.Controls.Add(Me.TextLateCap)
        Me.GroupControl3.Controls.Add(Me.TextHourWorkedPos)
        Me.GroupControl3.Controls.Add(Me.TextHourWorkedCap)
        Me.GroupControl3.Controls.Add(Me.TextStatusPos)
        Me.GroupControl3.Controls.Add(Me.TextStatusCap)
        Me.GroupControl3.Controls.Add(Me.TextOut2Pos)
        Me.GroupControl3.Controls.Add(Me.TextOut2Cap)
        Me.GroupControl3.Controls.Add(Me.TextIn2Pos)
        Me.GroupControl3.Controls.Add(Me.TextIn2Cap)
        Me.GroupControl3.Controls.Add(Me.TextOut1Pos)
        Me.GroupControl3.Controls.Add(Me.TextOut1Cap)
        Me.GroupControl3.Controls.Add(Me.TextIn1Pos)
        Me.GroupControl3.Controls.Add(Me.TextIn1Cap)
        Me.GroupControl3.Controls.Add(Me.TextShiftEndPos)
        Me.GroupControl3.Controls.Add(Me.TextShiftEndCap)
        Me.GroupControl3.Controls.Add(Me.TextShiftStartPos)
        Me.GroupControl3.Controls.Add(Me.TextShiftStartCap)
        Me.GroupControl3.Controls.Add(Me.TextSAttPos)
        Me.GroupControl3.Controls.Add(Me.TextSAttCap)
        Me.GroupControl3.Controls.Add(Me.LabelControl4)
        Me.GroupControl3.Controls.Add(Me.LabelControl5)
        Me.GroupControl3.Controls.Add(Me.LabelControl6)
        Me.GroupControl3.Controls.Add(Me.CheckOS)
        Me.GroupControl3.Controls.Add(Me.CheckOT)
        Me.GroupControl3.Controls.Add(Me.CheckShiftEnd)
        Me.GroupControl3.Controls.Add(Me.CheckShiftStart)
        Me.GroupControl3.Controls.Add(Me.CheckShift)
        Me.GroupControl3.Controls.Add(Me.CheckEarlyDeparture)
        Me.GroupControl3.Controls.Add(Me.CheckIN1)
        Me.GroupControl3.Controls.Add(Me.CheckLate)
        Me.GroupControl3.Controls.Add(Me.CheckOut1)
        Me.GroupControl3.Controls.Add(Me.CheckHourWorked)
        Me.GroupControl3.Controls.Add(Me.CheckIN2)
        Me.GroupControl3.Controls.Add(Me.CheckStatus)
        Me.GroupControl3.Controls.Add(Me.CheckOut2)
        Me.GroupControl3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupControl3.Location = New System.Drawing.Point(499, 25)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(535, 506)
        Me.GroupControl3.TabIndex = 14
        Me.GroupControl3.Text = "Attendance "
        '
        'TextTempRPos
        '
        Me.TextTempRPos.Enabled = False
        Me.TextTempRPos.Location = New System.Drawing.Point(336, 443)
        Me.TextTempRPos.Name = "TextTempRPos"
        Me.TextTempRPos.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextTempRPos.Properties.Appearance.Options.UseFont = True
        Me.TextTempRPos.Properties.Mask.EditMask = "[0-9]*"
        Me.TextTempRPos.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextTempRPos.Properties.MaxLength = 2
        Me.TextTempRPos.Size = New System.Drawing.Size(53, 20)
        Me.TextTempRPos.TabIndex = 77
        Me.TextTempRPos.Visible = False
        '
        'TextTempRCap
        '
        Me.TextTempRCap.Location = New System.Drawing.Point(177, 443)
        Me.TextTempRCap.Name = "TextTempRCap"
        Me.TextTempRCap.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextTempRCap.Properties.Appearance.Options.UseFont = True
        Me.TextTempRCap.Properties.Mask.EditMask = "[a-zA-Z0-9 ]*"
        Me.TextTempRCap.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextTempRCap.Properties.MaxLength = 25
        Me.TextTempRCap.Size = New System.Drawing.Size(135, 20)
        Me.TextTempRCap.TabIndex = 76
        Me.TextTempRCap.Visible = False
        '
        'CheckTempR
        '
        Me.CheckTempR.Location = New System.Drawing.Point(18, 444)
        Me.CheckTempR.Name = "CheckTempR"
        Me.CheckTempR.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckTempR.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.CheckTempR.Properties.Appearance.Options.UseFont = True
        Me.CheckTempR.Properties.Appearance.Options.UseForeColor = True
        Me.CheckTempR.Properties.Caption = "Temperature"
        Me.CheckTempR.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckTempR.Properties.ValueChecked = "Y"
        Me.CheckTempR.Properties.ValueGrayed = "N"
        Me.CheckTempR.Properties.ValueUnchecked = "N"
        Me.CheckTempR.Size = New System.Drawing.Size(136, 19)
        Me.CheckTempR.TabIndex = 75
        '
        'TextExcessLunchPos
        '
        Me.TextExcessLunchPos.Enabled = False
        Me.TextExcessLunchPos.Location = New System.Drawing.Point(336, 417)
        Me.TextExcessLunchPos.Name = "TextExcessLunchPos"
        Me.TextExcessLunchPos.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextExcessLunchPos.Properties.Appearance.Options.UseFont = True
        Me.TextExcessLunchPos.Properties.Mask.EditMask = "[0-9]*"
        Me.TextExcessLunchPos.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextExcessLunchPos.Properties.MaxLength = 2
        Me.TextExcessLunchPos.Size = New System.Drawing.Size(53, 20)
        Me.TextExcessLunchPos.TabIndex = 74
        '
        'TextExcessLunchCap
        '
        Me.TextExcessLunchCap.Location = New System.Drawing.Point(177, 417)
        Me.TextExcessLunchCap.Name = "TextExcessLunchCap"
        Me.TextExcessLunchCap.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextExcessLunchCap.Properties.Appearance.Options.UseFont = True
        Me.TextExcessLunchCap.Properties.Mask.EditMask = "[a-zA-Z0-9 ]*"
        Me.TextExcessLunchCap.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextExcessLunchCap.Properties.MaxLength = 25
        Me.TextExcessLunchCap.Size = New System.Drawing.Size(135, 20)
        Me.TextExcessLunchCap.TabIndex = 73
        '
        'CheckExcessLunch
        '
        Me.CheckExcessLunch.Location = New System.Drawing.Point(18, 418)
        Me.CheckExcessLunch.Name = "CheckExcessLunch"
        Me.CheckExcessLunch.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckExcessLunch.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.CheckExcessLunch.Properties.Appearance.Options.UseFont = True
        Me.CheckExcessLunch.Properties.Appearance.Options.UseForeColor = True
        Me.CheckExcessLunch.Properties.Caption = "Excess Lunch"
        Me.CheckExcessLunch.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckExcessLunch.Properties.ValueChecked = "Y"
        Me.CheckExcessLunch.Properties.ValueGrayed = "N"
        Me.CheckExcessLunch.Properties.ValueUnchecked = "N"
        Me.CheckExcessLunch.Size = New System.Drawing.Size(136, 19)
        Me.CheckExcessLunch.TabIndex = 72
        '
        'TextDatePos
        '
        Me.TextDatePos.Enabled = False
        Me.TextDatePos.Location = New System.Drawing.Point(336, 391)
        Me.TextDatePos.Name = "TextDatePos"
        Me.TextDatePos.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextDatePos.Properties.Appearance.Options.UseFont = True
        Me.TextDatePos.Properties.Mask.EditMask = "[0-9]*"
        Me.TextDatePos.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextDatePos.Properties.MaxLength = 2
        Me.TextDatePos.Size = New System.Drawing.Size(53, 20)
        Me.TextDatePos.TabIndex = 71
        '
        'TextDateCap
        '
        Me.TextDateCap.Location = New System.Drawing.Point(177, 391)
        Me.TextDateCap.Name = "TextDateCap"
        Me.TextDateCap.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextDateCap.Properties.Appearance.Options.UseFont = True
        Me.TextDateCap.Properties.Mask.EditMask = "[a-zA-Z0-9 ]*"
        Me.TextDateCap.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextDateCap.Properties.MaxLength = 25
        Me.TextDateCap.Size = New System.Drawing.Size(135, 20)
        Me.TextDateCap.TabIndex = 70
        '
        'CheckDate
        '
        Me.CheckDate.Location = New System.Drawing.Point(18, 392)
        Me.CheckDate.Name = "CheckDate"
        Me.CheckDate.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckDate.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.CheckDate.Properties.Appearance.Options.UseFont = True
        Me.CheckDate.Properties.Appearance.Options.UseForeColor = True
        Me.CheckDate.Properties.Caption = "Date"
        Me.CheckDate.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckDate.Properties.ValueChecked = "Y"
        Me.CheckDate.Properties.ValueGrayed = "N"
        Me.CheckDate.Properties.ValueUnchecked = "N"
        Me.CheckDate.Size = New System.Drawing.Size(136, 19)
        Me.CheckDate.TabIndex = 69
        '
        'TextOSPos
        '
        Me.TextOSPos.Enabled = False
        Me.TextOSPos.Location = New System.Drawing.Point(336, 365)
        Me.TextOSPos.Name = "TextOSPos"
        Me.TextOSPos.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextOSPos.Properties.Appearance.Options.UseFont = True
        Me.TextOSPos.Properties.Mask.EditMask = "[0-9]*"
        Me.TextOSPos.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextOSPos.Properties.MaxLength = 2
        Me.TextOSPos.Size = New System.Drawing.Size(53, 20)
        Me.TextOSPos.TabIndex = 68
        '
        'TextOSCap
        '
        Me.TextOSCap.Location = New System.Drawing.Point(177, 365)
        Me.TextOSCap.Name = "TextOSCap"
        Me.TextOSCap.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextOSCap.Properties.Appearance.Options.UseFont = True
        Me.TextOSCap.Properties.Mask.EditMask = "[a-zA-Z0-9 ]*"
        Me.TextOSCap.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextOSCap.Properties.MaxLength = 25
        Me.TextOSCap.Size = New System.Drawing.Size(135, 20)
        Me.TextOSCap.TabIndex = 67
        '
        'TextOTPos
        '
        Me.TextOTPos.Enabled = False
        Me.TextOTPos.Location = New System.Drawing.Point(336, 340)
        Me.TextOTPos.Name = "TextOTPos"
        Me.TextOTPos.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextOTPos.Properties.Appearance.Options.UseFont = True
        Me.TextOTPos.Properties.Mask.EditMask = "[0-9]*"
        Me.TextOTPos.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextOTPos.Properties.MaxLength = 2
        Me.TextOTPos.Size = New System.Drawing.Size(53, 20)
        Me.TextOTPos.TabIndex = 66
        '
        'TextOTCap
        '
        Me.TextOTCap.Location = New System.Drawing.Point(177, 340)
        Me.TextOTCap.Name = "TextOTCap"
        Me.TextOTCap.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextOTCap.Properties.Appearance.Options.UseFont = True
        Me.TextOTCap.Properties.Mask.EditMask = "[a-zA-Z0-9 ]*"
        Me.TextOTCap.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextOTCap.Properties.MaxLength = 25
        Me.TextOTCap.Size = New System.Drawing.Size(135, 20)
        Me.TextOTCap.TabIndex = 65
        '
        'TextEarlyDPos
        '
        Me.TextEarlyDPos.Enabled = False
        Me.TextEarlyDPos.Location = New System.Drawing.Point(336, 315)
        Me.TextEarlyDPos.Name = "TextEarlyDPos"
        Me.TextEarlyDPos.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEarlyDPos.Properties.Appearance.Options.UseFont = True
        Me.TextEarlyDPos.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEarlyDPos.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEarlyDPos.Properties.MaxLength = 2
        Me.TextEarlyDPos.Size = New System.Drawing.Size(53, 20)
        Me.TextEarlyDPos.TabIndex = 64
        '
        'TextEarlyDCap
        '
        Me.TextEarlyDCap.Location = New System.Drawing.Point(177, 315)
        Me.TextEarlyDCap.Name = "TextEarlyDCap"
        Me.TextEarlyDCap.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEarlyDCap.Properties.Appearance.Options.UseFont = True
        Me.TextEarlyDCap.Properties.Mask.EditMask = "[a-zA-Z0-9 ]*"
        Me.TextEarlyDCap.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEarlyDCap.Properties.MaxLength = 25
        Me.TextEarlyDCap.Size = New System.Drawing.Size(135, 20)
        Me.TextEarlyDCap.TabIndex = 63
        '
        'TextLatePos
        '
        Me.TextLatePos.Enabled = False
        Me.TextLatePos.Location = New System.Drawing.Point(336, 290)
        Me.TextLatePos.Name = "TextLatePos"
        Me.TextLatePos.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextLatePos.Properties.Appearance.Options.UseFont = True
        Me.TextLatePos.Properties.Mask.EditMask = "[0-9]*"
        Me.TextLatePos.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextLatePos.Properties.MaxLength = 2
        Me.TextLatePos.Size = New System.Drawing.Size(53, 20)
        Me.TextLatePos.TabIndex = 62
        '
        'TextLateCap
        '
        Me.TextLateCap.Location = New System.Drawing.Point(177, 290)
        Me.TextLateCap.Name = "TextLateCap"
        Me.TextLateCap.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextLateCap.Properties.Appearance.Options.UseFont = True
        Me.TextLateCap.Properties.Mask.EditMask = "[a-zA-Z0-9 ]*"
        Me.TextLateCap.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextLateCap.Properties.MaxLength = 25
        Me.TextLateCap.Size = New System.Drawing.Size(135, 20)
        Me.TextLateCap.TabIndex = 61
        '
        'TextHourWorkedPos
        '
        Me.TextHourWorkedPos.Enabled = False
        Me.TextHourWorkedPos.Location = New System.Drawing.Point(336, 265)
        Me.TextHourWorkedPos.Name = "TextHourWorkedPos"
        Me.TextHourWorkedPos.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextHourWorkedPos.Properties.Appearance.Options.UseFont = True
        Me.TextHourWorkedPos.Properties.Mask.EditMask = "[0-9]*"
        Me.TextHourWorkedPos.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextHourWorkedPos.Properties.MaxLength = 2
        Me.TextHourWorkedPos.Size = New System.Drawing.Size(53, 20)
        Me.TextHourWorkedPos.TabIndex = 60
        '
        'TextHourWorkedCap
        '
        Me.TextHourWorkedCap.Location = New System.Drawing.Point(177, 265)
        Me.TextHourWorkedCap.Name = "TextHourWorkedCap"
        Me.TextHourWorkedCap.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextHourWorkedCap.Properties.Appearance.Options.UseFont = True
        Me.TextHourWorkedCap.Properties.Mask.EditMask = "[a-zA-Z0-9 ]*"
        Me.TextHourWorkedCap.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextHourWorkedCap.Properties.MaxLength = 25
        Me.TextHourWorkedCap.Size = New System.Drawing.Size(135, 20)
        Me.TextHourWorkedCap.TabIndex = 59
        '
        'TextStatusPos
        '
        Me.TextStatusPos.Enabled = False
        Me.TextStatusPos.Location = New System.Drawing.Point(336, 240)
        Me.TextStatusPos.Name = "TextStatusPos"
        Me.TextStatusPos.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextStatusPos.Properties.Appearance.Options.UseFont = True
        Me.TextStatusPos.Properties.Mask.EditMask = "[0-9]*"
        Me.TextStatusPos.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextStatusPos.Properties.MaxLength = 2
        Me.TextStatusPos.Size = New System.Drawing.Size(53, 20)
        Me.TextStatusPos.TabIndex = 58
        '
        'TextStatusCap
        '
        Me.TextStatusCap.Location = New System.Drawing.Point(177, 240)
        Me.TextStatusCap.Name = "TextStatusCap"
        Me.TextStatusCap.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextStatusCap.Properties.Appearance.Options.UseFont = True
        Me.TextStatusCap.Properties.Mask.EditMask = "[a-zA-Z0-9 ]*"
        Me.TextStatusCap.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextStatusCap.Properties.MaxLength = 25
        Me.TextStatusCap.Size = New System.Drawing.Size(135, 20)
        Me.TextStatusCap.TabIndex = 57
        '
        'TextOut2Pos
        '
        Me.TextOut2Pos.Enabled = False
        Me.TextOut2Pos.Location = New System.Drawing.Point(336, 215)
        Me.TextOut2Pos.Name = "TextOut2Pos"
        Me.TextOut2Pos.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextOut2Pos.Properties.Appearance.Options.UseFont = True
        Me.TextOut2Pos.Properties.Mask.EditMask = "[0-9]*"
        Me.TextOut2Pos.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextOut2Pos.Properties.MaxLength = 2
        Me.TextOut2Pos.Size = New System.Drawing.Size(53, 20)
        Me.TextOut2Pos.TabIndex = 56
        '
        'TextOut2Cap
        '
        Me.TextOut2Cap.Location = New System.Drawing.Point(177, 215)
        Me.TextOut2Cap.Name = "TextOut2Cap"
        Me.TextOut2Cap.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextOut2Cap.Properties.Appearance.Options.UseFont = True
        Me.TextOut2Cap.Properties.Mask.EditMask = "[a-zA-Z0-9 ]*"
        Me.TextOut2Cap.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextOut2Cap.Properties.MaxLength = 25
        Me.TextOut2Cap.Size = New System.Drawing.Size(135, 20)
        Me.TextOut2Cap.TabIndex = 55
        '
        'TextIn2Pos
        '
        Me.TextIn2Pos.Enabled = False
        Me.TextIn2Pos.Location = New System.Drawing.Point(336, 190)
        Me.TextIn2Pos.Name = "TextIn2Pos"
        Me.TextIn2Pos.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextIn2Pos.Properties.Appearance.Options.UseFont = True
        Me.TextIn2Pos.Properties.Mask.EditMask = "[0-9]*"
        Me.TextIn2Pos.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextIn2Pos.Properties.MaxLength = 2
        Me.TextIn2Pos.Size = New System.Drawing.Size(53, 20)
        Me.TextIn2Pos.TabIndex = 54
        '
        'TextIn2Cap
        '
        Me.TextIn2Cap.Location = New System.Drawing.Point(177, 190)
        Me.TextIn2Cap.Name = "TextIn2Cap"
        Me.TextIn2Cap.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextIn2Cap.Properties.Appearance.Options.UseFont = True
        Me.TextIn2Cap.Properties.Mask.EditMask = "[a-zA-Z0-9 ]*"
        Me.TextIn2Cap.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextIn2Cap.Properties.MaxLength = 25
        Me.TextIn2Cap.Size = New System.Drawing.Size(135, 20)
        Me.TextIn2Cap.TabIndex = 53
        '
        'TextOut1Pos
        '
        Me.TextOut1Pos.Enabled = False
        Me.TextOut1Pos.Location = New System.Drawing.Point(336, 165)
        Me.TextOut1Pos.Name = "TextOut1Pos"
        Me.TextOut1Pos.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextOut1Pos.Properties.Appearance.Options.UseFont = True
        Me.TextOut1Pos.Properties.Mask.EditMask = "[0-9]*"
        Me.TextOut1Pos.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextOut1Pos.Properties.MaxLength = 2
        Me.TextOut1Pos.Size = New System.Drawing.Size(53, 20)
        Me.TextOut1Pos.TabIndex = 52
        '
        'TextOut1Cap
        '
        Me.TextOut1Cap.Location = New System.Drawing.Point(177, 165)
        Me.TextOut1Cap.Name = "TextOut1Cap"
        Me.TextOut1Cap.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextOut1Cap.Properties.Appearance.Options.UseFont = True
        Me.TextOut1Cap.Properties.Mask.EditMask = "[a-zA-Z0-9 ]*"
        Me.TextOut1Cap.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextOut1Cap.Properties.MaxLength = 25
        Me.TextOut1Cap.Size = New System.Drawing.Size(135, 20)
        Me.TextOut1Cap.TabIndex = 51
        '
        'TextIn1Pos
        '
        Me.TextIn1Pos.Enabled = False
        Me.TextIn1Pos.Location = New System.Drawing.Point(336, 140)
        Me.TextIn1Pos.Name = "TextIn1Pos"
        Me.TextIn1Pos.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextIn1Pos.Properties.Appearance.Options.UseFont = True
        Me.TextIn1Pos.Properties.Mask.EditMask = "[0-9]*"
        Me.TextIn1Pos.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextIn1Pos.Properties.MaxLength = 2
        Me.TextIn1Pos.Size = New System.Drawing.Size(53, 20)
        Me.TextIn1Pos.TabIndex = 50
        '
        'TextIn1Cap
        '
        Me.TextIn1Cap.Location = New System.Drawing.Point(177, 140)
        Me.TextIn1Cap.Name = "TextIn1Cap"
        Me.TextIn1Cap.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextIn1Cap.Properties.Appearance.Options.UseFont = True
        Me.TextIn1Cap.Properties.Mask.EditMask = "[a-zA-Z0-9 ]*"
        Me.TextIn1Cap.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextIn1Cap.Properties.MaxLength = 25
        Me.TextIn1Cap.Size = New System.Drawing.Size(135, 20)
        Me.TextIn1Cap.TabIndex = 49
        '
        'TextShiftEndPos
        '
        Me.TextShiftEndPos.Enabled = False
        Me.TextShiftEndPos.Location = New System.Drawing.Point(336, 115)
        Me.TextShiftEndPos.Name = "TextShiftEndPos"
        Me.TextShiftEndPos.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextShiftEndPos.Properties.Appearance.Options.UseFont = True
        Me.TextShiftEndPos.Properties.Mask.EditMask = "[0-9]*"
        Me.TextShiftEndPos.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextShiftEndPos.Properties.MaxLength = 2
        Me.TextShiftEndPos.Size = New System.Drawing.Size(53, 20)
        Me.TextShiftEndPos.TabIndex = 48
        '
        'TextShiftEndCap
        '
        Me.TextShiftEndCap.Location = New System.Drawing.Point(177, 115)
        Me.TextShiftEndCap.Name = "TextShiftEndCap"
        Me.TextShiftEndCap.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextShiftEndCap.Properties.Appearance.Options.UseFont = True
        Me.TextShiftEndCap.Properties.Mask.EditMask = "[a-zA-Z0-9 ]*"
        Me.TextShiftEndCap.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextShiftEndCap.Properties.MaxLength = 25
        Me.TextShiftEndCap.Size = New System.Drawing.Size(135, 20)
        Me.TextShiftEndCap.TabIndex = 47
        '
        'TextShiftStartPos
        '
        Me.TextShiftStartPos.Enabled = False
        Me.TextShiftStartPos.Location = New System.Drawing.Point(336, 90)
        Me.TextShiftStartPos.Name = "TextShiftStartPos"
        Me.TextShiftStartPos.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextShiftStartPos.Properties.Appearance.Options.UseFont = True
        Me.TextShiftStartPos.Properties.Mask.EditMask = "[0-9]*"
        Me.TextShiftStartPos.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextShiftStartPos.Properties.MaxLength = 2
        Me.TextShiftStartPos.Size = New System.Drawing.Size(53, 20)
        Me.TextShiftStartPos.TabIndex = 46
        '
        'TextShiftStartCap
        '
        Me.TextShiftStartCap.Location = New System.Drawing.Point(177, 90)
        Me.TextShiftStartCap.Name = "TextShiftStartCap"
        Me.TextShiftStartCap.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextShiftStartCap.Properties.Appearance.Options.UseFont = True
        Me.TextShiftStartCap.Properties.Mask.EditMask = "[a-zA-Z0-9 ]*"
        Me.TextShiftStartCap.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextShiftStartCap.Properties.MaxLength = 25
        Me.TextShiftStartCap.Size = New System.Drawing.Size(135, 20)
        Me.TextShiftStartCap.TabIndex = 45
        '
        'TextSAttPos
        '
        Me.TextSAttPos.Enabled = False
        Me.TextSAttPos.Location = New System.Drawing.Point(336, 65)
        Me.TextSAttPos.Name = "TextSAttPos"
        Me.TextSAttPos.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextSAttPos.Properties.Appearance.Options.UseFont = True
        Me.TextSAttPos.Properties.Mask.EditMask = "[0-9]*"
        Me.TextSAttPos.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextSAttPos.Properties.MaxLength = 2
        Me.TextSAttPos.Size = New System.Drawing.Size(53, 20)
        Me.TextSAttPos.TabIndex = 44
        '
        'TextSAttCap
        '
        Me.TextSAttCap.Location = New System.Drawing.Point(177, 65)
        Me.TextSAttCap.Name = "TextSAttCap"
        Me.TextSAttCap.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextSAttCap.Properties.Appearance.Options.UseFont = True
        Me.TextSAttCap.Properties.Mask.EditMask = "[a-zA-Z0-9 ]*"
        Me.TextSAttCap.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextSAttCap.Properties.MaxLength = 25
        Me.TextSAttCap.Size = New System.Drawing.Size(135, 20)
        Me.TextSAttCap.TabIndex = 43
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.LabelControl4.Appearance.Options.UseFont = True
        Me.LabelControl4.Location = New System.Drawing.Point(321, 39)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(91, 16)
        Me.LabelControl4.TabIndex = 21
        Me.LabelControl4.Text = "Column Position"
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.LabelControl5.Appearance.Options.UseFont = True
        Me.LabelControl5.Location = New System.Drawing.Point(193, 39)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(90, 16)
        Me.LabelControl5.TabIndex = 20
        Me.LabelControl5.Text = "Column Caption"
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.LabelControl6.Appearance.Options.UseFont = True
        Me.LabelControl6.Location = New System.Drawing.Point(27, 39)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(88, 16)
        Me.LabelControl6.TabIndex = 19
        Me.LabelControl6.Text = "Select Columns"
        '
        'CheckOS
        '
        Me.CheckOS.Location = New System.Drawing.Point(18, 366)
        Me.CheckOS.Name = "CheckOS"
        Me.CheckOS.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckOS.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.CheckOS.Properties.Appearance.Options.UseFont = True
        Me.CheckOS.Properties.Appearance.Options.UseForeColor = True
        Me.CheckOS.Properties.Caption = "OS Duration"
        Me.CheckOS.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckOS.Properties.ValueChecked = "Y"
        Me.CheckOS.Properties.ValueGrayed = "N"
        Me.CheckOS.Properties.ValueUnchecked = "N"
        Me.CheckOS.Size = New System.Drawing.Size(136, 19)
        Me.CheckOS.TabIndex = 13
        '
        'CheckOT
        '
        Me.CheckOT.Location = New System.Drawing.Point(18, 341)
        Me.CheckOT.Name = "CheckOT"
        Me.CheckOT.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckOT.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.CheckOT.Properties.Appearance.Options.UseFont = True
        Me.CheckOT.Properties.Appearance.Options.UseForeColor = True
        Me.CheckOT.Properties.Caption = "OT Duration"
        Me.CheckOT.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckOT.Properties.ValueChecked = "Y"
        Me.CheckOT.Properties.ValueGrayed = "N"
        Me.CheckOT.Properties.ValueUnchecked = "N"
        Me.CheckOT.Size = New System.Drawing.Size(136, 19)
        Me.CheckOT.TabIndex = 12
        '
        'CheckShiftEnd
        '
        Me.CheckShiftEnd.Location = New System.Drawing.Point(18, 116)
        Me.CheckShiftEnd.Name = "CheckShiftEnd"
        Me.CheckShiftEnd.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckShiftEnd.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.CheckShiftEnd.Properties.Appearance.Options.UseFont = True
        Me.CheckShiftEnd.Properties.Appearance.Options.UseForeColor = True
        Me.CheckShiftEnd.Properties.Caption = "Shift End Time"
        Me.CheckShiftEnd.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckShiftEnd.Properties.ValueChecked = "Y"
        Me.CheckShiftEnd.Properties.ValueGrayed = "N"
        Me.CheckShiftEnd.Properties.ValueUnchecked = "N"
        Me.CheckShiftEnd.Size = New System.Drawing.Size(136, 19)
        Me.CheckShiftEnd.TabIndex = 3
        '
        'CheckShiftStart
        '
        Me.CheckShiftStart.Location = New System.Drawing.Point(18, 91)
        Me.CheckShiftStart.Name = "CheckShiftStart"
        Me.CheckShiftStart.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckShiftStart.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.CheckShiftStart.Properties.Appearance.Options.UseFont = True
        Me.CheckShiftStart.Properties.Appearance.Options.UseForeColor = True
        Me.CheckShiftStart.Properties.Caption = "Shift Start Time"
        Me.CheckShiftStart.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckShiftStart.Properties.ValueChecked = "Y"
        Me.CheckShiftStart.Properties.ValueGrayed = "N"
        Me.CheckShiftStart.Properties.ValueUnchecked = "N"
        Me.CheckShiftStart.Size = New System.Drawing.Size(136, 19)
        Me.CheckShiftStart.TabIndex = 2
        '
        'CheckShift
        '
        Me.CheckShift.Location = New System.Drawing.Point(18, 66)
        Me.CheckShift.Name = "CheckShift"
        Me.CheckShift.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckShift.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.CheckShift.Properties.Appearance.Options.UseFont = True
        Me.CheckShift.Properties.Appearance.Options.UseForeColor = True
        Me.CheckShift.Properties.Caption = "Shift Attended"
        Me.CheckShift.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckShift.Properties.ValueChecked = "Y"
        Me.CheckShift.Properties.ValueGrayed = "N"
        Me.CheckShift.Properties.ValueUnchecked = "N"
        Me.CheckShift.Size = New System.Drawing.Size(136, 19)
        Me.CheckShift.TabIndex = 1
        '
        'CheckEarlyDeparture
        '
        Me.CheckEarlyDeparture.Location = New System.Drawing.Point(18, 316)
        Me.CheckEarlyDeparture.Name = "CheckEarlyDeparture"
        Me.CheckEarlyDeparture.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEarlyDeparture.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.CheckEarlyDeparture.Properties.Appearance.Options.UseFont = True
        Me.CheckEarlyDeparture.Properties.Appearance.Options.UseForeColor = True
        Me.CheckEarlyDeparture.Properties.Caption = "Early Departure"
        Me.CheckEarlyDeparture.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEarlyDeparture.Properties.ValueChecked = "Y"
        Me.CheckEarlyDeparture.Properties.ValueGrayed = "N"
        Me.CheckEarlyDeparture.Properties.ValueUnchecked = "N"
        Me.CheckEarlyDeparture.Size = New System.Drawing.Size(136, 19)
        Me.CheckEarlyDeparture.TabIndex = 11
        '
        'CheckIN1
        '
        Me.CheckIN1.Location = New System.Drawing.Point(18, 141)
        Me.CheckIN1.Name = "CheckIN1"
        Me.CheckIN1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckIN1.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.CheckIN1.Properties.Appearance.Options.UseFont = True
        Me.CheckIN1.Properties.Appearance.Options.UseForeColor = True
        Me.CheckIN1.Properties.Caption = "IN Time"
        Me.CheckIN1.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckIN1.Properties.ValueChecked = "Y"
        Me.CheckIN1.Properties.ValueGrayed = "N"
        Me.CheckIN1.Properties.ValueUnchecked = "N"
        Me.CheckIN1.Size = New System.Drawing.Size(136, 19)
        Me.CheckIN1.TabIndex = 4
        '
        'CheckLate
        '
        Me.CheckLate.Location = New System.Drawing.Point(18, 291)
        Me.CheckLate.Name = "CheckLate"
        Me.CheckLate.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckLate.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.CheckLate.Properties.Appearance.Options.UseFont = True
        Me.CheckLate.Properties.Appearance.Options.UseForeColor = True
        Me.CheckLate.Properties.Caption = "Late Arrival"
        Me.CheckLate.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckLate.Properties.ValueChecked = "Y"
        Me.CheckLate.Properties.ValueGrayed = "N"
        Me.CheckLate.Properties.ValueUnchecked = "N"
        Me.CheckLate.Size = New System.Drawing.Size(136, 19)
        Me.CheckLate.TabIndex = 10
        '
        'CheckOut1
        '
        Me.CheckOut1.Location = New System.Drawing.Point(18, 166)
        Me.CheckOut1.Name = "CheckOut1"
        Me.CheckOut1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckOut1.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.CheckOut1.Properties.Appearance.Options.UseFont = True
        Me.CheckOut1.Properties.Appearance.Options.UseForeColor = True
        Me.CheckOut1.Properties.Caption = "Lunch Out"
        Me.CheckOut1.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckOut1.Properties.ValueChecked = "Y"
        Me.CheckOut1.Properties.ValueGrayed = "N"
        Me.CheckOut1.Properties.ValueUnchecked = "N"
        Me.CheckOut1.Size = New System.Drawing.Size(136, 19)
        Me.CheckOut1.TabIndex = 5
        '
        'CheckHourWorked
        '
        Me.CheckHourWorked.Location = New System.Drawing.Point(18, 266)
        Me.CheckHourWorked.Name = "CheckHourWorked"
        Me.CheckHourWorked.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckHourWorked.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.CheckHourWorked.Properties.Appearance.Options.UseFont = True
        Me.CheckHourWorked.Properties.Appearance.Options.UseForeColor = True
        Me.CheckHourWorked.Properties.Caption = "Hours Worked"
        Me.CheckHourWorked.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckHourWorked.Properties.ValueChecked = "Y"
        Me.CheckHourWorked.Properties.ValueGrayed = "N"
        Me.CheckHourWorked.Properties.ValueUnchecked = "N"
        Me.CheckHourWorked.Size = New System.Drawing.Size(136, 19)
        Me.CheckHourWorked.TabIndex = 9
        '
        'CheckIN2
        '
        Me.CheckIN2.Location = New System.Drawing.Point(18, 191)
        Me.CheckIN2.Name = "CheckIN2"
        Me.CheckIN2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckIN2.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.CheckIN2.Properties.Appearance.Options.UseFont = True
        Me.CheckIN2.Properties.Appearance.Options.UseForeColor = True
        Me.CheckIN2.Properties.Caption = "Lunch In"
        Me.CheckIN2.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckIN2.Properties.ValueChecked = "Y"
        Me.CheckIN2.Properties.ValueGrayed = "N"
        Me.CheckIN2.Properties.ValueUnchecked = "N"
        Me.CheckIN2.Size = New System.Drawing.Size(136, 19)
        Me.CheckIN2.TabIndex = 6
        '
        'CheckStatus
        '
        Me.CheckStatus.Location = New System.Drawing.Point(18, 241)
        Me.CheckStatus.Name = "CheckStatus"
        Me.CheckStatus.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckStatus.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.CheckStatus.Properties.Appearance.Options.UseFont = True
        Me.CheckStatus.Properties.Appearance.Options.UseForeColor = True
        Me.CheckStatus.Properties.Caption = "Status"
        Me.CheckStatus.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckStatus.Properties.ValueChecked = "Y"
        Me.CheckStatus.Properties.ValueGrayed = "N"
        Me.CheckStatus.Properties.ValueUnchecked = "N"
        Me.CheckStatus.Size = New System.Drawing.Size(136, 19)
        Me.CheckStatus.TabIndex = 8
        '
        'CheckOut2
        '
        Me.CheckOut2.Location = New System.Drawing.Point(18, 216)
        Me.CheckOut2.Name = "CheckOut2"
        Me.CheckOut2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckOut2.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.CheckOut2.Properties.Appearance.Options.UseFont = True
        Me.CheckOut2.Properties.Appearance.Options.UseForeColor = True
        Me.CheckOut2.Properties.Caption = "Out Time"
        Me.CheckOut2.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckOut2.Properties.ValueChecked = "Y"
        Me.CheckOut2.Properties.ValueGrayed = "N"
        Me.CheckOut2.Properties.ValueUnchecked = "N"
        Me.CheckOut2.Size = New System.Drawing.Size(136, 19)
        Me.CheckOut2.TabIndex = 7
        '
        'GroupControl2
        '
        Me.GroupControl2.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.GroupControl2.AppearanceCaption.ForeColor = System.Drawing.Color.Blue
        Me.GroupControl2.AppearanceCaption.Options.UseFont = True
        Me.GroupControl2.AppearanceCaption.Options.UseForeColor = True
        Me.GroupControl2.Controls.Add(Me.TextUidPos)
        Me.GroupControl2.Controls.Add(Me.TextUidCap)
        Me.GroupControl2.Controls.Add(Me.CheckEditUID)
        Me.GroupControl2.Controls.Add(Me.ComboBKType)
        Me.GroupControl2.Controls.Add(Me.LabelControl7)
        Me.GroupControl2.Controls.Add(Me.TextDateFormat)
        Me.GroupControl2.Controls.Add(Me.LabelControl18)
        Me.GroupControl2.Controls.Add(Me.TextPriTextPay)
        Me.GroupControl2.Controls.Add(Me.LabelControl12)
        Me.GroupControl2.Controls.Add(Me.TextPriFixLenghtPay)
        Me.GroupControl2.Controls.Add(Me.LabelControl13)
        Me.GroupControl2.Controls.Add(Me.CheckPrefixPrePaycode)
        Me.GroupControl2.Controls.Add(Me.TextPriTextPre)
        Me.GroupControl2.Controls.Add(Me.LabelControl11)
        Me.GroupControl2.Controls.Add(Me.TextPriFixLenghtPre)
        Me.GroupControl2.Controls.Add(Me.LabelControl10)
        Me.GroupControl2.Controls.Add(Me.CheckPrefixPreCard)
        Me.GroupControl2.Controls.Add(Me.TextGenPos)
        Me.GroupControl2.Controls.Add(Me.TextGenCap)
        Me.GroupControl2.Controls.Add(Me.TextDojPos)
        Me.GroupControl2.Controls.Add(Me.TextDojCap)
        Me.GroupControl2.Controls.Add(Me.TextDesPos)
        Me.GroupControl2.Controls.Add(Me.TextDesCap)
        Me.GroupControl2.Controls.Add(Me.TextGNamePos)
        Me.GroupControl2.Controls.Add(Me.TextGNameCap)
        Me.GroupControl2.Controls.Add(Me.TextEmpGrpPos)
        Me.GroupControl2.Controls.Add(Me.TextEmpGrpCap)
        Me.GroupControl2.Controls.Add(Me.TextCatPos)
        Me.GroupControl2.Controls.Add(Me.TextCatCap)
        Me.GroupControl2.Controls.Add(Me.TextGradPos)
        Me.GroupControl2.Controls.Add(Me.TextGradeCap)
        Me.GroupControl2.Controls.Add(Me.TextDeptPos)
        Me.GroupControl2.Controls.Add(Me.TextDeptCap)
        Me.GroupControl2.Controls.Add(Me.TextLocPos)
        Me.GroupControl2.Controls.Add(Me.TextLocCap)
        Me.GroupControl2.Controls.Add(Me.TextComPos)
        Me.GroupControl2.Controls.Add(Me.TextComCap)
        Me.GroupControl2.Controls.Add(Me.TextUserNoPos)
        Me.GroupControl2.Controls.Add(Me.TextUserNoCap)
        Me.GroupControl2.Controls.Add(Me.TextNamePos)
        Me.GroupControl2.Controls.Add(Me.TextNameCap)
        Me.GroupControl2.Controls.Add(Me.LabelControl3)
        Me.GroupControl2.Controls.Add(Me.LabelControl2)
        Me.GroupControl2.Controls.Add(Me.LabelControl1)
        Me.GroupControl2.Controls.Add(Me.TextPayPos)
        Me.GroupControl2.Controls.Add(Me.TextPayCap)
        Me.GroupControl2.Controls.Add(Me.CheckGender)
        Me.GroupControl2.Controls.Add(Me.CheckDoj)
        Me.GroupControl2.Controls.Add(Me.CheckDes)
        Me.GroupControl2.Controls.Add(Me.CheckGName)
        Me.GroupControl2.Controls.Add(Me.CheckEmpPay)
        Me.GroupControl2.Controls.Add(Me.CheckEmpGrp)
        Me.GroupControl2.Controls.Add(Me.CheckEmpName)
        Me.GroupControl2.Controls.Add(Me.CheckCat)
        Me.GroupControl2.Controls.Add(Me.CheckUserNo)
        Me.GroupControl2.Controls.Add(Me.CheckGrade)
        Me.GroupControl2.Controls.Add(Me.CheckCompany)
        Me.GroupControl2.Controls.Add(Me.CheckDept)
        Me.GroupControl2.Controls.Add(Me.CheckLocation)
        Me.GroupControl2.Dock = System.Windows.Forms.DockStyle.Left
        Me.GroupControl2.Location = New System.Drawing.Point(2, 25)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(497, 506)
        Me.GroupControl2.TabIndex = 13
        Me.GroupControl2.Text = "Master"
        '
        'ComboBKType
        '
        Me.ComboBKType.EditValue = "TEXT"
        Me.ComboBKType.Location = New System.Drawing.Point(441, 481)
        Me.ComboBKType.Name = "ComboBKType"
        Me.ComboBKType.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBKType.Properties.Appearance.Options.UseFont = True
        Me.ComboBKType.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBKType.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboBKType.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBKType.Properties.AppearanceFocused.Options.UseFont = True
        Me.ComboBKType.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBKType.Properties.Items.AddRange(New Object() {"TEXT", "DAT", "EXCEL"})
        Me.ComboBKType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.ComboBKType.Size = New System.Drawing.Size(100, 20)
        Me.ComboBKType.TabIndex = 77
        Me.ComboBKType.Visible = False
        '
        'LabelControl7
        '
        Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl7.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.LabelControl7.Appearance.Options.UseFont = True
        Me.LabelControl7.Appearance.Options.UseForeColor = True
        Me.LabelControl7.Location = New System.Drawing.Point(306, 484)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(90, 14)
        Me.LabelControl7.TabIndex = 78
        Me.LabelControl7.Text = "Report File Type"
        Me.LabelControl7.Visible = False
        '
        'TextDateFormat
        '
        Me.TextDateFormat.Location = New System.Drawing.Point(152, 470)
        Me.TextDateFormat.Name = "TextDateFormat"
        Me.TextDateFormat.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextDateFormat.Properties.Appearance.Options.UseFont = True
        Me.TextDateFormat.Properties.MaxLength = 50
        Me.TextDateFormat.Size = New System.Drawing.Size(150, 20)
        Me.TextDateFormat.TabIndex = 75
        '
        'LabelControl18
        '
        Me.LabelControl18.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl18.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.LabelControl18.Appearance.Options.UseFont = True
        Me.LabelControl18.Appearance.Options.UseForeColor = True
        Me.LabelControl18.Location = New System.Drawing.Point(17, 472)
        Me.LabelControl18.Name = "LabelControl18"
        Me.LabelControl18.Size = New System.Drawing.Size(68, 14)
        Me.LabelControl18.TabIndex = 76
        Me.LabelControl18.Text = "Date Format"
        '
        'TextPriTextPay
        '
        Me.TextPriTextPay.Location = New System.Drawing.Point(369, 418)
        Me.TextPriTextPay.Name = "TextPriTextPay"
        Me.TextPriTextPay.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextPriTextPay.Properties.Appearance.Options.UseFont = True
        Me.TextPriTextPay.Properties.MaxLength = 1
        Me.TextPriTextPay.Size = New System.Drawing.Size(40, 20)
        Me.TextPriTextPay.TabIndex = 70
        '
        'LabelControl12
        '
        Me.LabelControl12.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl12.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.LabelControl12.Appearance.Options.UseFont = True
        Me.LabelControl12.Appearance.Options.UseForeColor = True
        Me.LabelControl12.Location = New System.Drawing.Point(295, 420)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(60, 14)
        Me.LabelControl12.TabIndex = 74
        Me.LabelControl12.Text = "Prefix Text"
        '
        'TextPriFixLenghtPay
        '
        Me.TextPriFixLenghtPay.Location = New System.Drawing.Point(226, 418)
        Me.TextPriFixLenghtPay.Name = "TextPriFixLenghtPay"
        Me.TextPriFixLenghtPay.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextPriFixLenghtPay.Properties.Appearance.Options.UseFont = True
        Me.TextPriFixLenghtPay.Properties.Mask.EditMask = "[0-9]*"
        Me.TextPriFixLenghtPay.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextPriFixLenghtPay.Properties.MaxLength = 2
        Me.TextPriFixLenghtPay.Size = New System.Drawing.Size(40, 20)
        Me.TextPriFixLenghtPay.TabIndex = 69
        '
        'LabelControl13
        '
        Me.LabelControl13.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl13.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.LabelControl13.Appearance.Options.UseFont = True
        Me.LabelControl13.Appearance.Options.UseForeColor = True
        Me.LabelControl13.Location = New System.Drawing.Point(185, 420)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(34, 14)
        Me.LabelControl13.TabIndex = 73
        Me.LabelControl13.Text = "Lengh"
        '
        'CheckPrefixPrePaycode
        '
        Me.CheckPrefixPrePaycode.Location = New System.Drawing.Point(17, 418)
        Me.CheckPrefixPrePaycode.Name = "CheckPrefixPrePaycode"
        Me.CheckPrefixPrePaycode.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckPrefixPrePaycode.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.CheckPrefixPrePaycode.Properties.Appearance.Options.UseFont = True
        Me.CheckPrefixPrePaycode.Properties.Appearance.Options.UseForeColor = True
        Me.CheckPrefixPrePaycode.Properties.Caption = "Prefix for Paycode"
        Me.CheckPrefixPrePaycode.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckPrefixPrePaycode.Properties.ValueChecked = "Y"
        Me.CheckPrefixPrePaycode.Properties.ValueGrayed = "N"
        Me.CheckPrefixPrePaycode.Properties.ValueUnchecked = "N"
        Me.CheckPrefixPrePaycode.Size = New System.Drawing.Size(137, 19)
        Me.CheckPrefixPrePaycode.TabIndex = 68
        '
        'TextPriTextPre
        '
        Me.TextPriTextPre.Location = New System.Drawing.Point(369, 444)
        Me.TextPriTextPre.Name = "TextPriTextPre"
        Me.TextPriTextPre.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextPriTextPre.Properties.Appearance.Options.UseFont = True
        Me.TextPriTextPre.Properties.MaxLength = 1
        Me.TextPriTextPre.Size = New System.Drawing.Size(40, 20)
        Me.TextPriTextPre.TabIndex = 67
        '
        'LabelControl11
        '
        Me.LabelControl11.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl11.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.LabelControl11.Appearance.Options.UseFont = True
        Me.LabelControl11.Appearance.Options.UseForeColor = True
        Me.LabelControl11.Location = New System.Drawing.Point(295, 446)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(60, 14)
        Me.LabelControl11.TabIndex = 72
        Me.LabelControl11.Text = "Prefix Text"
        '
        'TextPriFixLenghtPre
        '
        Me.TextPriFixLenghtPre.Location = New System.Drawing.Point(226, 444)
        Me.TextPriFixLenghtPre.Name = "TextPriFixLenghtPre"
        Me.TextPriFixLenghtPre.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextPriFixLenghtPre.Properties.Appearance.Options.UseFont = True
        Me.TextPriFixLenghtPre.Properties.Mask.EditMask = "[0-9]*"
        Me.TextPriFixLenghtPre.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextPriFixLenghtPre.Properties.MaxLength = 2
        Me.TextPriFixLenghtPre.Size = New System.Drawing.Size(40, 20)
        Me.TextPriFixLenghtPre.TabIndex = 66
        '
        'LabelControl10
        '
        Me.LabelControl10.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl10.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.LabelControl10.Appearance.Options.UseFont = True
        Me.LabelControl10.Appearance.Options.UseForeColor = True
        Me.LabelControl10.Location = New System.Drawing.Point(185, 446)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(34, 14)
        Me.LabelControl10.TabIndex = 71
        Me.LabelControl10.Text = "Lengh"
        '
        'CheckPrefixPreCard
        '
        Me.CheckPrefixPreCard.Location = New System.Drawing.Point(17, 444)
        Me.CheckPrefixPreCard.Name = "CheckPrefixPreCard"
        Me.CheckPrefixPreCard.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckPrefixPreCard.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.CheckPrefixPreCard.Properties.Appearance.Options.UseFont = True
        Me.CheckPrefixPreCard.Properties.Appearance.Options.UseForeColor = True
        Me.CheckPrefixPreCard.Properties.Caption = "Prefix for User Number"
        Me.CheckPrefixPreCard.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckPrefixPreCard.Properties.ValueChecked = "Y"
        Me.CheckPrefixPreCard.Properties.ValueGrayed = "N"
        Me.CheckPrefixPreCard.Properties.ValueUnchecked = "N"
        Me.CheckPrefixPreCard.Size = New System.Drawing.Size(162, 19)
        Me.CheckPrefixPreCard.TabIndex = 65
        '
        'TextGenPos
        '
        Me.TextGenPos.Enabled = False
        Me.TextGenPos.Location = New System.Drawing.Point(345, 365)
        Me.TextGenPos.Name = "TextGenPos"
        Me.TextGenPos.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextGenPos.Properties.Appearance.Options.UseFont = True
        Me.TextGenPos.Properties.Mask.EditMask = "[0-9]*"
        Me.TextGenPos.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextGenPos.Properties.MaxLength = 2
        Me.TextGenPos.Size = New System.Drawing.Size(53, 20)
        Me.TextGenPos.TabIndex = 42
        '
        'TextGenCap
        '
        Me.TextGenCap.Location = New System.Drawing.Point(186, 365)
        Me.TextGenCap.Name = "TextGenCap"
        Me.TextGenCap.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextGenCap.Properties.Appearance.Options.UseFont = True
        Me.TextGenCap.Properties.Mask.EditMask = "[a-zA-Z0-9 ]*"
        Me.TextGenCap.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextGenCap.Properties.MaxLength = 25
        Me.TextGenCap.Size = New System.Drawing.Size(135, 20)
        Me.TextGenCap.TabIndex = 41
        '
        'TextDojPos
        '
        Me.TextDojPos.Enabled = False
        Me.TextDojPos.Location = New System.Drawing.Point(345, 340)
        Me.TextDojPos.Name = "TextDojPos"
        Me.TextDojPos.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextDojPos.Properties.Appearance.Options.UseFont = True
        Me.TextDojPos.Properties.Mask.EditMask = "[0-9]*"
        Me.TextDojPos.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextDojPos.Properties.MaxLength = 2
        Me.TextDojPos.Size = New System.Drawing.Size(53, 20)
        Me.TextDojPos.TabIndex = 40
        '
        'TextDojCap
        '
        Me.TextDojCap.Location = New System.Drawing.Point(186, 340)
        Me.TextDojCap.Name = "TextDojCap"
        Me.TextDojCap.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextDojCap.Properties.Appearance.Options.UseFont = True
        Me.TextDojCap.Properties.Mask.EditMask = "[a-zA-Z0-9 ]*"
        Me.TextDojCap.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextDojCap.Properties.MaxLength = 25
        Me.TextDojCap.Size = New System.Drawing.Size(135, 20)
        Me.TextDojCap.TabIndex = 39
        '
        'TextDesPos
        '
        Me.TextDesPos.Enabled = False
        Me.TextDesPos.Location = New System.Drawing.Point(345, 315)
        Me.TextDesPos.Name = "TextDesPos"
        Me.TextDesPos.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextDesPos.Properties.Appearance.Options.UseFont = True
        Me.TextDesPos.Properties.Mask.EditMask = "[0-9]*"
        Me.TextDesPos.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextDesPos.Properties.MaxLength = 2
        Me.TextDesPos.Size = New System.Drawing.Size(53, 20)
        Me.TextDesPos.TabIndex = 38
        '
        'TextDesCap
        '
        Me.TextDesCap.Location = New System.Drawing.Point(186, 315)
        Me.TextDesCap.Name = "TextDesCap"
        Me.TextDesCap.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextDesCap.Properties.Appearance.Options.UseFont = True
        Me.TextDesCap.Properties.Mask.EditMask = "[a-zA-Z0-9 ]*"
        Me.TextDesCap.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextDesCap.Properties.MaxLength = 25
        Me.TextDesCap.Size = New System.Drawing.Size(135, 20)
        Me.TextDesCap.TabIndex = 37
        '
        'TextGNamePos
        '
        Me.TextGNamePos.Enabled = False
        Me.TextGNamePos.Location = New System.Drawing.Point(345, 290)
        Me.TextGNamePos.Name = "TextGNamePos"
        Me.TextGNamePos.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextGNamePos.Properties.Appearance.Options.UseFont = True
        Me.TextGNamePos.Properties.Mask.EditMask = "[0-9]*"
        Me.TextGNamePos.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextGNamePos.Properties.MaxLength = 2
        Me.TextGNamePos.Size = New System.Drawing.Size(53, 20)
        Me.TextGNamePos.TabIndex = 36
        '
        'TextGNameCap
        '
        Me.TextGNameCap.Location = New System.Drawing.Point(186, 290)
        Me.TextGNameCap.Name = "TextGNameCap"
        Me.TextGNameCap.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextGNameCap.Properties.Appearance.Options.UseFont = True
        Me.TextGNameCap.Properties.Mask.EditMask = "[a-zA-Z0-9 ]*"
        Me.TextGNameCap.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextGNameCap.Properties.MaxLength = 25
        Me.TextGNameCap.Size = New System.Drawing.Size(135, 20)
        Me.TextGNameCap.TabIndex = 35
        '
        'TextEmpGrpPos
        '
        Me.TextEmpGrpPos.Enabled = False
        Me.TextEmpGrpPos.Location = New System.Drawing.Point(345, 265)
        Me.TextEmpGrpPos.Name = "TextEmpGrpPos"
        Me.TextEmpGrpPos.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEmpGrpPos.Properties.Appearance.Options.UseFont = True
        Me.TextEmpGrpPos.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEmpGrpPos.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEmpGrpPos.Properties.MaxLength = 2
        Me.TextEmpGrpPos.Size = New System.Drawing.Size(53, 20)
        Me.TextEmpGrpPos.TabIndex = 34
        '
        'TextEmpGrpCap
        '
        Me.TextEmpGrpCap.Location = New System.Drawing.Point(186, 265)
        Me.TextEmpGrpCap.Name = "TextEmpGrpCap"
        Me.TextEmpGrpCap.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEmpGrpCap.Properties.Appearance.Options.UseFont = True
        Me.TextEmpGrpCap.Properties.Mask.EditMask = "[a-zA-Z0-9 ]*"
        Me.TextEmpGrpCap.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEmpGrpCap.Properties.MaxLength = 25
        Me.TextEmpGrpCap.Size = New System.Drawing.Size(135, 20)
        Me.TextEmpGrpCap.TabIndex = 33
        '
        'TextCatPos
        '
        Me.TextCatPos.Enabled = False
        Me.TextCatPos.Location = New System.Drawing.Point(345, 240)
        Me.TextCatPos.Name = "TextCatPos"
        Me.TextCatPos.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextCatPos.Properties.Appearance.Options.UseFont = True
        Me.TextCatPos.Properties.Mask.EditMask = "[0-9]*"
        Me.TextCatPos.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextCatPos.Properties.MaxLength = 2
        Me.TextCatPos.Size = New System.Drawing.Size(53, 20)
        Me.TextCatPos.TabIndex = 32
        '
        'TextCatCap
        '
        Me.TextCatCap.Location = New System.Drawing.Point(186, 240)
        Me.TextCatCap.Name = "TextCatCap"
        Me.TextCatCap.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextCatCap.Properties.Appearance.Options.UseFont = True
        Me.TextCatCap.Properties.Mask.EditMask = "[a-zA-Z0-9 ]*"
        Me.TextCatCap.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextCatCap.Properties.MaxLength = 25
        Me.TextCatCap.Size = New System.Drawing.Size(135, 20)
        Me.TextCatCap.TabIndex = 31
        '
        'TextGradPos
        '
        Me.TextGradPos.Enabled = False
        Me.TextGradPos.Location = New System.Drawing.Point(345, 215)
        Me.TextGradPos.Name = "TextGradPos"
        Me.TextGradPos.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextGradPos.Properties.Appearance.Options.UseFont = True
        Me.TextGradPos.Properties.Mask.EditMask = "[0-9]*"
        Me.TextGradPos.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextGradPos.Properties.MaxLength = 2
        Me.TextGradPos.Size = New System.Drawing.Size(53, 20)
        Me.TextGradPos.TabIndex = 30
        '
        'TextGradeCap
        '
        Me.TextGradeCap.Location = New System.Drawing.Point(186, 215)
        Me.TextGradeCap.Name = "TextGradeCap"
        Me.TextGradeCap.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextGradeCap.Properties.Appearance.Options.UseFont = True
        Me.TextGradeCap.Properties.Mask.EditMask = "[a-zA-Z0-9 ]*"
        Me.TextGradeCap.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextGradeCap.Properties.MaxLength = 25
        Me.TextGradeCap.Size = New System.Drawing.Size(135, 20)
        Me.TextGradeCap.TabIndex = 29
        '
        'TextDeptPos
        '
        Me.TextDeptPos.Enabled = False
        Me.TextDeptPos.Location = New System.Drawing.Point(345, 190)
        Me.TextDeptPos.Name = "TextDeptPos"
        Me.TextDeptPos.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextDeptPos.Properties.Appearance.Options.UseFont = True
        Me.TextDeptPos.Properties.Mask.EditMask = "[0-9]*"
        Me.TextDeptPos.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextDeptPos.Properties.MaxLength = 2
        Me.TextDeptPos.Size = New System.Drawing.Size(53, 20)
        Me.TextDeptPos.TabIndex = 28
        '
        'TextDeptCap
        '
        Me.TextDeptCap.Location = New System.Drawing.Point(186, 190)
        Me.TextDeptCap.Name = "TextDeptCap"
        Me.TextDeptCap.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextDeptCap.Properties.Appearance.Options.UseFont = True
        Me.TextDeptCap.Properties.Mask.EditMask = "[a-zA-Z0-9 ]*"
        Me.TextDeptCap.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextDeptCap.Properties.MaxLength = 25
        Me.TextDeptCap.Size = New System.Drawing.Size(135, 20)
        Me.TextDeptCap.TabIndex = 27
        '
        'TextLocPos
        '
        Me.TextLocPos.Enabled = False
        Me.TextLocPos.Location = New System.Drawing.Point(345, 165)
        Me.TextLocPos.Name = "TextLocPos"
        Me.TextLocPos.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextLocPos.Properties.Appearance.Options.UseFont = True
        Me.TextLocPos.Properties.Mask.EditMask = "[0-9]*"
        Me.TextLocPos.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextLocPos.Properties.MaxLength = 2
        Me.TextLocPos.Size = New System.Drawing.Size(53, 20)
        Me.TextLocPos.TabIndex = 26
        '
        'TextLocCap
        '
        Me.TextLocCap.Location = New System.Drawing.Point(186, 165)
        Me.TextLocCap.Name = "TextLocCap"
        Me.TextLocCap.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextLocCap.Properties.Appearance.Options.UseFont = True
        Me.TextLocCap.Properties.Mask.EditMask = "[a-zA-Z0-9 ]*"
        Me.TextLocCap.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextLocCap.Properties.MaxLength = 25
        Me.TextLocCap.Size = New System.Drawing.Size(135, 20)
        Me.TextLocCap.TabIndex = 25
        '
        'TextComPos
        '
        Me.TextComPos.Enabled = False
        Me.TextComPos.Location = New System.Drawing.Point(345, 140)
        Me.TextComPos.Name = "TextComPos"
        Me.TextComPos.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextComPos.Properties.Appearance.Options.UseFont = True
        Me.TextComPos.Properties.Mask.EditMask = "[0-9]*"
        Me.TextComPos.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextComPos.Properties.MaxLength = 2
        Me.TextComPos.Size = New System.Drawing.Size(53, 20)
        Me.TextComPos.TabIndex = 24
        '
        'TextComCap
        '
        Me.TextComCap.Location = New System.Drawing.Point(186, 140)
        Me.TextComCap.Name = "TextComCap"
        Me.TextComCap.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextComCap.Properties.Appearance.Options.UseFont = True
        Me.TextComCap.Properties.Mask.EditMask = "[a-zA-Z0-9 ]*"
        Me.TextComCap.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextComCap.Properties.MaxLength = 25
        Me.TextComCap.Size = New System.Drawing.Size(135, 20)
        Me.TextComCap.TabIndex = 23
        '
        'TextUserNoPos
        '
        Me.TextUserNoPos.Enabled = False
        Me.TextUserNoPos.Location = New System.Drawing.Point(345, 115)
        Me.TextUserNoPos.Name = "TextUserNoPos"
        Me.TextUserNoPos.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextUserNoPos.Properties.Appearance.Options.UseFont = True
        Me.TextUserNoPos.Properties.Mask.EditMask = "[0-9]*"
        Me.TextUserNoPos.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextUserNoPos.Properties.MaxLength = 2
        Me.TextUserNoPos.Size = New System.Drawing.Size(53, 20)
        Me.TextUserNoPos.TabIndex = 22
        '
        'TextUserNoCap
        '
        Me.TextUserNoCap.Location = New System.Drawing.Point(186, 115)
        Me.TextUserNoCap.Name = "TextUserNoCap"
        Me.TextUserNoCap.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextUserNoCap.Properties.Appearance.Options.UseFont = True
        Me.TextUserNoCap.Properties.Mask.EditMask = "[a-zA-Z0-9 ]*"
        Me.TextUserNoCap.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextUserNoCap.Properties.MaxLength = 25
        Me.TextUserNoCap.Size = New System.Drawing.Size(135, 20)
        Me.TextUserNoCap.TabIndex = 21
        '
        'TextNamePos
        '
        Me.TextNamePos.Enabled = False
        Me.TextNamePos.Location = New System.Drawing.Point(345, 90)
        Me.TextNamePos.Name = "TextNamePos"
        Me.TextNamePos.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextNamePos.Properties.Appearance.Options.UseFont = True
        Me.TextNamePos.Properties.Mask.EditMask = "[0-9]*"
        Me.TextNamePos.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextNamePos.Properties.MaxLength = 2
        Me.TextNamePos.Size = New System.Drawing.Size(53, 20)
        Me.TextNamePos.TabIndex = 20
        '
        'TextNameCap
        '
        Me.TextNameCap.Location = New System.Drawing.Point(186, 90)
        Me.TextNameCap.Name = "TextNameCap"
        Me.TextNameCap.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextNameCap.Properties.Appearance.Options.UseFont = True
        Me.TextNameCap.Properties.Mask.EditMask = "[a-zA-Z0-9 ]*"
        Me.TextNameCap.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextNameCap.Properties.MaxLength = 25
        Me.TextNameCap.Size = New System.Drawing.Size(135, 20)
        Me.TextNameCap.TabIndex = 19
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.LabelControl3.Appearance.Options.UseFont = True
        Me.LabelControl3.Location = New System.Drawing.Point(328, 39)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(91, 16)
        Me.LabelControl3.TabIndex = 18
        Me.LabelControl3.Text = "Column Position"
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.LabelControl2.Appearance.Options.UseFont = True
        Me.LabelControl2.Location = New System.Drawing.Point(200, 39)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(90, 16)
        Me.LabelControl2.TabIndex = 17
        Me.LabelControl2.Text = "Column Caption"
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Location = New System.Drawing.Point(34, 39)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(88, 16)
        Me.LabelControl1.TabIndex = 16
        Me.LabelControl1.Text = "Select Columns"
        '
        'TextPayPos
        '
        Me.TextPayPos.Enabled = False
        Me.TextPayPos.Location = New System.Drawing.Point(345, 65)
        Me.TextPayPos.Name = "TextPayPos"
        Me.TextPayPos.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextPayPos.Properties.Appearance.Options.UseFont = True
        Me.TextPayPos.Properties.Mask.EditMask = "[0-9]*"
        Me.TextPayPos.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextPayPos.Properties.MaxLength = 2
        Me.TextPayPos.Size = New System.Drawing.Size(53, 20)
        Me.TextPayPos.TabIndex = 15
        '
        'TextPayCap
        '
        Me.TextPayCap.Location = New System.Drawing.Point(186, 65)
        Me.TextPayCap.Name = "TextPayCap"
        Me.TextPayCap.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextPayCap.Properties.Appearance.Options.UseFont = True
        Me.TextPayCap.Properties.Mask.EditMask = "[a-zA-Z0-9 ]*"
        Me.TextPayCap.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextPayCap.Properties.MaxLength = 25
        Me.TextPayCap.Size = New System.Drawing.Size(135, 20)
        Me.TextPayCap.TabIndex = 14
        '
        'CheckGender
        '
        Me.CheckGender.Location = New System.Drawing.Point(18, 366)
        Me.CheckGender.Name = "CheckGender"
        Me.CheckGender.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckGender.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.CheckGender.Properties.Appearance.Options.UseFont = True
        Me.CheckGender.Properties.Appearance.Options.UseForeColor = True
        Me.CheckGender.Properties.Caption = "Gender"
        Me.CheckGender.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckGender.Properties.ValueChecked = "Y"
        Me.CheckGender.Properties.ValueGrayed = "N"
        Me.CheckGender.Properties.ValueUnchecked = "N"
        Me.CheckGender.Size = New System.Drawing.Size(136, 19)
        Me.CheckGender.TabIndex = 13
        '
        'CheckDoj
        '
        Me.CheckDoj.Location = New System.Drawing.Point(18, 341)
        Me.CheckDoj.Name = "CheckDoj"
        Me.CheckDoj.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckDoj.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.CheckDoj.Properties.Appearance.Options.UseFont = True
        Me.CheckDoj.Properties.Appearance.Options.UseForeColor = True
        Me.CheckDoj.Properties.Caption = "Date Of Joining"
        Me.CheckDoj.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckDoj.Properties.ValueChecked = "Y"
        Me.CheckDoj.Properties.ValueGrayed = "N"
        Me.CheckDoj.Properties.ValueUnchecked = "N"
        Me.CheckDoj.Size = New System.Drawing.Size(136, 19)
        Me.CheckDoj.TabIndex = 12
        '
        'CheckDes
        '
        Me.CheckDes.Location = New System.Drawing.Point(18, 316)
        Me.CheckDes.Name = "CheckDes"
        Me.CheckDes.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckDes.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.CheckDes.Properties.Appearance.Options.UseFont = True
        Me.CheckDes.Properties.Appearance.Options.UseForeColor = True
        Me.CheckDes.Properties.Caption = "Designation"
        Me.CheckDes.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckDes.Properties.NullText = "N"
        Me.CheckDes.Properties.ValueChecked = "Y"
        Me.CheckDes.Properties.ValueGrayed = "N"
        Me.CheckDes.Properties.ValueUnchecked = "N"
        Me.CheckDes.Size = New System.Drawing.Size(136, 19)
        Me.CheckDes.TabIndex = 11
        '
        'CheckGName
        '
        Me.CheckGName.Location = New System.Drawing.Point(18, 291)
        Me.CheckGName.Name = "CheckGName"
        Me.CheckGName.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckGName.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.CheckGName.Properties.Appearance.Options.UseFont = True
        Me.CheckGName.Properties.Appearance.Options.UseForeColor = True
        Me.CheckGName.Properties.Caption = "Guardian Name"
        Me.CheckGName.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckGName.Properties.NullText = "N"
        Me.CheckGName.Properties.ValueChecked = "Y"
        Me.CheckGName.Properties.ValueGrayed = "N"
        Me.CheckGName.Properties.ValueUnchecked = "N"
        Me.CheckGName.Size = New System.Drawing.Size(136, 19)
        Me.CheckGName.TabIndex = 10
        '
        'CheckEmpPay
        '
        Me.CheckEmpPay.Location = New System.Drawing.Point(18, 66)
        Me.CheckEmpPay.Name = "CheckEmpPay"
        Me.CheckEmpPay.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEmpPay.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.CheckEmpPay.Properties.Appearance.Options.UseFont = True
        Me.CheckEmpPay.Properties.Appearance.Options.UseForeColor = True
        Me.CheckEmpPay.Properties.Caption = "Employee Paycode"
        Me.CheckEmpPay.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEmpPay.Properties.ValueChecked = "Y"
        Me.CheckEmpPay.Properties.ValueGrayed = "N"
        Me.CheckEmpPay.Properties.ValueUnchecked = "N"
        Me.CheckEmpPay.Size = New System.Drawing.Size(136, 19)
        Me.CheckEmpPay.TabIndex = 1
        '
        'CheckEmpGrp
        '
        Me.CheckEmpGrp.Location = New System.Drawing.Point(18, 266)
        Me.CheckEmpGrp.Name = "CheckEmpGrp"
        Me.CheckEmpGrp.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEmpGrp.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.CheckEmpGrp.Properties.Appearance.Options.UseFont = True
        Me.CheckEmpGrp.Properties.Appearance.Options.UseForeColor = True
        Me.CheckEmpGrp.Properties.Caption = "Employee Group"
        Me.CheckEmpGrp.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEmpGrp.Properties.ValueChecked = "Y"
        Me.CheckEmpGrp.Properties.ValueGrayed = "N"
        Me.CheckEmpGrp.Properties.ValueUnchecked = "N"
        Me.CheckEmpGrp.Size = New System.Drawing.Size(136, 19)
        Me.CheckEmpGrp.TabIndex = 9
        '
        'CheckEmpName
        '
        Me.CheckEmpName.Location = New System.Drawing.Point(18, 91)
        Me.CheckEmpName.Name = "CheckEmpName"
        Me.CheckEmpName.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEmpName.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.CheckEmpName.Properties.Appearance.Options.UseFont = True
        Me.CheckEmpName.Properties.Appearance.Options.UseForeColor = True
        Me.CheckEmpName.Properties.Caption = "Employee Name"
        Me.CheckEmpName.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEmpName.Properties.ValueChecked = "Y"
        Me.CheckEmpName.Properties.ValueGrayed = "N"
        Me.CheckEmpName.Properties.ValueUnchecked = "N"
        Me.CheckEmpName.Size = New System.Drawing.Size(136, 19)
        Me.CheckEmpName.TabIndex = 2
        '
        'CheckCat
        '
        Me.CheckCat.Location = New System.Drawing.Point(18, 241)
        Me.CheckCat.Name = "CheckCat"
        Me.CheckCat.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckCat.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.CheckCat.Properties.Appearance.Options.UseFont = True
        Me.CheckCat.Properties.Appearance.Options.UseForeColor = True
        Me.CheckCat.Properties.Caption = "Catagory"
        Me.CheckCat.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckCat.Properties.ValueChecked = "Y"
        Me.CheckCat.Properties.ValueGrayed = "N"
        Me.CheckCat.Properties.ValueUnchecked = "N"
        Me.CheckCat.Size = New System.Drawing.Size(136, 19)
        Me.CheckCat.TabIndex = 8
        '
        'CheckUserNo
        '
        Me.CheckUserNo.Location = New System.Drawing.Point(18, 116)
        Me.CheckUserNo.Name = "CheckUserNo"
        Me.CheckUserNo.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckUserNo.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.CheckUserNo.Properties.Appearance.Options.UseFont = True
        Me.CheckUserNo.Properties.Appearance.Options.UseForeColor = True
        Me.CheckUserNo.Properties.Caption = "User Number"
        Me.CheckUserNo.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckUserNo.Properties.ValueChecked = "Y"
        Me.CheckUserNo.Properties.ValueGrayed = "N"
        Me.CheckUserNo.Properties.ValueUnchecked = "N"
        Me.CheckUserNo.Size = New System.Drawing.Size(136, 19)
        Me.CheckUserNo.TabIndex = 3
        '
        'CheckGrade
        '
        Me.CheckGrade.Location = New System.Drawing.Point(18, 216)
        Me.CheckGrade.Name = "CheckGrade"
        Me.CheckGrade.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckGrade.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.CheckGrade.Properties.Appearance.Options.UseFont = True
        Me.CheckGrade.Properties.Appearance.Options.UseForeColor = True
        Me.CheckGrade.Properties.Caption = "Grade"
        Me.CheckGrade.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckGrade.Properties.ValueChecked = "Y"
        Me.CheckGrade.Properties.ValueGrayed = "N"
        Me.CheckGrade.Properties.ValueUnchecked = "N"
        Me.CheckGrade.Size = New System.Drawing.Size(136, 19)
        Me.CheckGrade.TabIndex = 7
        '
        'CheckCompany
        '
        Me.CheckCompany.Location = New System.Drawing.Point(18, 141)
        Me.CheckCompany.Name = "CheckCompany"
        Me.CheckCompany.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckCompany.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.CheckCompany.Properties.Appearance.Options.UseFont = True
        Me.CheckCompany.Properties.Appearance.Options.UseForeColor = True
        Me.CheckCompany.Properties.Caption = "Company"
        Me.CheckCompany.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckCompany.Properties.ValueChecked = "Y"
        Me.CheckCompany.Properties.ValueGrayed = "N"
        Me.CheckCompany.Properties.ValueUnchecked = "N"
        Me.CheckCompany.Size = New System.Drawing.Size(136, 19)
        Me.CheckCompany.TabIndex = 4
        '
        'CheckDept
        '
        Me.CheckDept.Location = New System.Drawing.Point(18, 191)
        Me.CheckDept.Name = "CheckDept"
        Me.CheckDept.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckDept.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.CheckDept.Properties.Appearance.Options.UseFont = True
        Me.CheckDept.Properties.Appearance.Options.UseForeColor = True
        Me.CheckDept.Properties.Caption = "Department"
        Me.CheckDept.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckDept.Properties.ValueChecked = "Y"
        Me.CheckDept.Properties.ValueGrayed = "N"
        Me.CheckDept.Properties.ValueUnchecked = "N"
        Me.CheckDept.Size = New System.Drawing.Size(136, 19)
        Me.CheckDept.TabIndex = 6
        '
        'CheckLocation
        '
        Me.CheckLocation.Location = New System.Drawing.Point(18, 166)
        Me.CheckLocation.Name = "CheckLocation"
        Me.CheckLocation.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckLocation.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.CheckLocation.Properties.Appearance.Options.UseFont = True
        Me.CheckLocation.Properties.Appearance.Options.UseForeColor = True
        Me.CheckLocation.Properties.Caption = "Location"
        Me.CheckLocation.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckLocation.Properties.ValueChecked = "Y"
        Me.CheckLocation.Properties.ValueGrayed = "N"
        Me.CheckLocation.Properties.ValueUnchecked = "N"
        Me.CheckLocation.Size = New System.Drawing.Size(136, 19)
        Me.CheckLocation.TabIndex = 5
        '
        'SidePanel1
        '
        Me.SidePanel1.Controls.Add(Me.SimpleButton1)
        Me.SidePanel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.SidePanel1.Location = New System.Drawing.Point(2, 531)
        Me.SidePanel1.Name = "SidePanel1"
        Me.SidePanel1.Size = New System.Drawing.Size(1032, 35)
        Me.SidePanel1.TabIndex = 12
        Me.SidePanel1.Text = "SidePanel1"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButton1.Appearance.Options.UseFont = True
        Me.SimpleButton1.Location = New System.Drawing.Point(13, 5)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton1.TabIndex = 5
        Me.SimpleButton1.Text = "Save"
        '
        'MemoEdit1
        '
        Me.MemoEdit1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.MemoEdit1.EditValue = resources.GetString("MemoEdit1.EditValue")
        Me.MemoEdit1.Location = New System.Drawing.Point(0, 0)
        Me.MemoEdit1.Name = "MemoEdit1"
        Me.MemoEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.MemoEdit1.Properties.Appearance.Options.UseFont = True
        Me.MemoEdit1.Properties.MaxLength = 100000
        Me.MemoEdit1.Properties.ReadOnly = True
        Me.MemoEdit1.Size = New System.Drawing.Size(104, 568)
        Me.MemoEdit1.TabIndex = 2
        '
        'TextUidPos
        '
        Me.TextUidPos.Enabled = False
        Me.TextUidPos.Location = New System.Drawing.Point(344, 391)
        Me.TextUidPos.Name = "TextUidPos"
        Me.TextUidPos.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextUidPos.Properties.Appearance.Options.UseFont = True
        Me.TextUidPos.Properties.Mask.EditMask = "[0-9]*"
        Me.TextUidPos.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextUidPos.Properties.MaxLength = 2
        Me.TextUidPos.Size = New System.Drawing.Size(53, 20)
        Me.TextUidPos.TabIndex = 44
        '
        'TextUidCap
        '
        Me.TextUidCap.Location = New System.Drawing.Point(185, 391)
        Me.TextUidCap.Name = "TextUidCap"
        Me.TextUidCap.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextUidCap.Properties.Appearance.Options.UseFont = True
        Me.TextUidCap.Properties.Mask.EditMask = "[a-zA-Z0-9 ]*"
        Me.TextUidCap.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextUidCap.Properties.MaxLength = 25
        Me.TextUidCap.Size = New System.Drawing.Size(135, 20)
        Me.TextUidCap.TabIndex = 43
        '
        'CheckEditUID
        '
        Me.CheckEditUID.Location = New System.Drawing.Point(17, 392)
        Me.CheckEditUID.Name = "CheckEditUID"
        Me.CheckEditUID.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditUID.Properties.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.CheckEditUID.Properties.Appearance.Options.UseFont = True
        Me.CheckEditUID.Properties.Appearance.Options.UseForeColor = True
        Me.CheckEditUID.Properties.Caption = "UID"
        Me.CheckEditUID.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckEditUID.Properties.ValueChecked = "Y"
        Me.CheckEditUID.Properties.ValueGrayed = "N"
        Me.CheckEditUID.Properties.ValueUnchecked = "N"
        Me.CheckEditUID.Size = New System.Drawing.Size(136, 19)
        Me.CheckEditUID.TabIndex = 79
        '
        'XtraCustomisedReportEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.SplitContainerControl1)
        Me.Name = "XtraCustomisedReportEdit"
        Me.Size = New System.Drawing.Size(1145, 568)
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerControl1.ResumeLayout(False)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        Me.GroupControl3.PerformLayout()
        CType(Me.TextTempRPos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextTempRCap.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckTempR.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextExcessLunchPos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextExcessLunchCap.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckExcessLunch.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextDatePos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextDateCap.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextOSPos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextOSCap.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextOTPos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextOTCap.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEarlyDPos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEarlyDCap.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextLatePos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextLateCap.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextHourWorkedPos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextHourWorkedCap.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextStatusPos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextStatusCap.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextOut2Pos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextOut2Cap.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextIn2Pos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextIn2Cap.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextOut1Pos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextOut1Cap.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextIn1Pos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextIn1Cap.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextShiftEndPos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextShiftEndCap.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextShiftStartPos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextShiftStartCap.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextSAttPos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextSAttCap.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckOS.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckOT.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckShiftEnd.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckShiftStart.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckShift.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEarlyDeparture.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckIN1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckLate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckOut1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckHourWorked.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckIN2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckStatus.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckOut2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.GroupControl2.PerformLayout()
        CType(Me.ComboBKType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextDateFormat.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextPriTextPay.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextPriFixLenghtPay.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckPrefixPrePaycode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextPriTextPre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextPriFixLenghtPre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckPrefixPreCard.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextGenPos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextGenCap.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextDojPos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextDojCap.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextDesPos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextDesCap.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextGNamePos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextGNameCap.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEmpGrpPos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEmpGrpCap.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextCatPos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextCatCap.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextGradPos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextGradeCap.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextDeptPos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextDeptCap.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextLocPos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextLocCap.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextComPos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextComCap.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextUserNoPos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextUserNoCap.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextNamePos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextNameCap.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextPayPos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextPayCap.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckGender.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckDoj.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckDes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckGName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEmpPay.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEmpGrp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEmpName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckCat.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckUserNo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckGrade.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckCompany.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckDept.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckLocation.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SidePanel1.ResumeLayout(False)
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextUidPos.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextUidCap.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditUID.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SplitContainerControl1 As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents MemoEdit1 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents SidePanel1 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CheckOS As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckOT As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckShiftEnd As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckShiftStart As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckShift As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEarlyDeparture As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckIN1 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckLate As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckOut1 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckHourWorked As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckIN2 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckStatus As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckOut2 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CheckEmpPay As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEmpGrp As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEmpName As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckCat As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckUserNo As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckGrade As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckCompany As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckDept As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckLocation As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckGender As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckDoj As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckDes As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckGName As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents TextPayCap As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextPayPos As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextGenPos As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextGenCap As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextDojPos As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextDojCap As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextDesPos As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextDesCap As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextGNamePos As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextGNameCap As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEmpGrpPos As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEmpGrpCap As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextCatPos As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextCatCap As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextGradPos As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextGradeCap As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextDeptPos As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextDeptCap As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextLocPos As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextLocCap As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextComPos As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextComCap As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextUserNoPos As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextUserNoCap As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextNamePos As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextNameCap As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextOSPos As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextOSCap As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextOTPos As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextOTCap As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEarlyDPos As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEarlyDCap As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextLatePos As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextLateCap As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextHourWorkedPos As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextHourWorkedCap As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextStatusPos As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextStatusCap As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextOut2Pos As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextOut2Cap As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextIn2Pos As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextIn2Cap As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextOut1Pos As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextOut1Cap As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextIn1Pos As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextIn1Cap As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextShiftEndPos As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextShiftEndCap As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextShiftStartPos As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextShiftStartCap As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextSAttPos As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextSAttCap As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextPriTextPay As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextPriFixLenghtPay As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents CheckPrefixPrePaycode As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents TextPriTextPre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextPriFixLenghtPre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents CheckPrefixPreCard As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents TextDateFormat As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl18 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ComboBKType As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextDatePos As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextDateCap As DevExpress.XtraEditors.TextEdit
    Friend WithEvents CheckDate As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents TextExcessLunchPos As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextExcessLunchCap As DevExpress.XtraEditors.TextEdit
    Friend WithEvents CheckExcessLunch As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents TextUidPos As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextUidCap As DevExpress.XtraEditors.TextEdit
    Friend WithEvents CheckEditUID As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents TextTempRPos As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextTempRCap As DevExpress.XtraEditors.TextEdit
    Friend WithEvents CheckTempR As DevExpress.XtraEditors.CheckEdit
End Class
