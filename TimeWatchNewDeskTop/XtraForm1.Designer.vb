﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraForm1
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.TblCompanyBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SSSDBDataSet = New iAS.SSSDBDataSet()
        Me.TblCompanyTableAdapter = New iAS.SSSDBDataSetTableAdapters.tblCompanyTableAdapter()
        Me.TblLeaveLedgerBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TblLeaveLedgerTableAdapter = New iAS.SSSDBDataSetTableAdapters.tblLeaveLedgerTableAdapter()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.TblCompanyBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblLeaveLedgerBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TblCompanyBindingSource
        '
        Me.TblCompanyBindingSource.DataMember = "tblCompany"
        Me.TblCompanyBindingSource.DataSource = Me.SSSDBDataSet
        '
        'SSSDBDataSet
        '
        Me.SSSDBDataSet.DataSetName = "SSSDBDataSet"
        Me.SSSDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'TblCompanyTableAdapter
        '
        Me.TblCompanyTableAdapter.ClearBeforeFill = True
        '
        'TblLeaveLedgerBindingSource
        '
        Me.TblLeaveLedgerBindingSource.DataMember = "tblLeaveLedger"
        Me.TblLeaveLedgerBindingSource.DataSource = Me.SSSDBDataSet
        '
        'TblLeaveLedgerTableAdapter
        '
        Me.TblLeaveLedgerTableAdapter.ClearBeforeFill = True
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Location = New System.Drawing.Point(12, 12)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton1.TabIndex = 6
        Me.SimpleButton1.Text = "Create"
        '
        'XtraForm1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(268, 68)
        Me.Controls.Add(Me.SimpleButton1)
        Me.Name = "XtraForm1"
        Me.Text = "XtraForm1"
        CType(Me.TblCompanyBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblLeaveLedgerBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SSSDBDataSet As iAS.SSSDBDataSet
    Friend WithEvents TblCompanyBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblCompanyTableAdapter As iAS.SSSDBDataSetTableAdapters.tblCompanyTableAdapter
    Friend WithEvents TblLeaveLedgerBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblLeaveLedgerTableAdapter As iAS.SSSDBDataSetTableAdapters.tblLeaveLedgerTableAdapter
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
End Class
