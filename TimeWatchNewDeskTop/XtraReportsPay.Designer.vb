﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraReportsPay
    Inherits DevExpress.XtraEditors.XtraUserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(XtraReportsPay))
        Me.SplitContainerControl1 = New DevExpress.XtraEditors.SplitContainerControl()
        Me.SidePanel3 = New DevExpress.XtraEditors.SidePanel()
        Me.PopupContainerControlEmp = New DevExpress.XtraEditors.PopupContainerControl()
        Me.GridControlEmp = New DevExpress.XtraGrid.GridControl()
        Me.TblEmployeeBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SSSDBDataSet = New iAS.SSSDBDataSet()
        Me.GridViewEmp = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colPAYCODE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colEMPNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTimeEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit()
        Me.PopupContainerControlDept = New DevExpress.XtraEditors.PopupContainerControl()
        Me.GridControlDept = New DevExpress.XtraGrid.GridControl()
        Me.TblDepartmentBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GridViewDept = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colDEPARTMENTCODE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDEPARTMENTNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTimeEdit3 = New DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit()
        Me.PopupContainerControlComp = New DevExpress.XtraEditors.PopupContainerControl()
        Me.GridControlComp = New DevExpress.XtraGrid.GridControl()
        Me.TblCompanyBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GridViewComp = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colCOMPANYCODE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCOMPANYNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTimeEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit()
        Me.SidePanel2 = New DevExpress.XtraEditors.SidePanel()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.CheckEditLEaveEncash = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditPendingReEm = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditCashAdj = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdidFullNFinal = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditSummaryStat = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditReesPyReg = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditReemSt = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditPieceM = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditPieceEmp = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditArrearSlip = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditArrearReg = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditESISt = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditESICh = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditPfSt = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditPfCh = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditLoanEntry = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditLoanEntryDetails = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckSalaryNonBank = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditAdvEntry = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckSalaryBank = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditAdvEntryDetails = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckSalaryCash = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckSalaryCheque = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckSalaryRegister = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckSalarySlip = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditEmpWiseCtc = New DevExpress.XtraEditors.CheckEdit()
        Me.PopupContainerControlBranch = New DevExpress.XtraEditors.PopupContainerControl()
        Me.GridControlBranch = New DevExpress.XtraGrid.GridControl()
        Me.GridViewBranch = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colBRANCHCODE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBRANCHNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTimeEdit7 = New DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit()
        Me.RepositoryItemDateEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit()
        Me.PopupContainerControlGrade = New DevExpress.XtraEditors.PopupContainerControl()
        Me.GridControlGrade = New DevExpress.XtraGrid.GridControl()
        Me.TblGradeBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GridViewGrade = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTimeEdit6 = New DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit()
        Me.PopupContainerControlCat = New DevExpress.XtraEditors.PopupContainerControl()
        Me.GridControlCat = New DevExpress.XtraGrid.GridControl()
        Me.TblCatagoryBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GridViewCat = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colCAT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCATAGORYNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTimeEdit4 = New DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit()
        Me.CheckEdit3 = New DevExpress.XtraEditors.CheckEdit()
        Me.SidePanel1 = New DevExpress.XtraEditors.SidePanel()
        Me.ComboNepaliYearTo = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboNEpaliMonthTo = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboNepaliYearFrm = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboNEpaliMonthFrm = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.CheckPDF = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckExcel = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckText = New DevExpress.XtraEditors.CheckEdit()
        Me.SidePanelSelection = New DevExpress.XtraEditors.SidePanel()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.PopupContainerEditEmp = New DevExpress.XtraEditors.PopupContainerEdit()
        Me.PopupContainerEditGrade = New DevExpress.XtraEditors.PopupContainerEdit()
        Me.CheckEdit1 = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.CheckEdit2 = New DevExpress.XtraEditors.CheckEdit()
        Me.PopupContainerEditLocation = New DevExpress.XtraEditors.PopupContainerEdit()
        Me.PopupContainerEditComp = New DevExpress.XtraEditors.PopupContainerEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.PopupContainerEditDept = New DevExpress.XtraEditors.PopupContainerEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.PopupContainerEditCat = New DevExpress.XtraEditors.PopupContainerEdit()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.DateEditTo = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.DateEditFrom = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.MemoEdit1 = New DevExpress.XtraEditors.MemoEdit()
        Me.TblDepartmentTableAdapter = New iAS.SSSDBDataSetTableAdapters.tblDepartmentTableAdapter()
        Me.TblEmployee1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.TblEmployee1TableAdapter()
        Me.TblEmployeeTableAdapter = New iAS.SSSDBDataSetTableAdapters.TblEmployeeTableAdapter()
        Me.TblCompany1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.tblCompany1TableAdapter()
        Me.TblDepartment1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.tblDepartment1TableAdapter()
        Me.TblCatagoryTableAdapter = New iAS.SSSDBDataSetTableAdapters.tblCatagoryTableAdapter()
        Me.TblGradeTableAdapter = New iAS.SSSDBDataSetTableAdapters.tblGradeTableAdapter()
        Me.TblCatagory1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.tblCatagory1TableAdapter()
        Me.TblCompanyTableAdapter = New iAS.SSSDBDataSetTableAdapters.tblCompanyTableAdapter()
        Me.TblGrade1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.tblGrade1TableAdapter()
        Me.Tblbranch1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.tblbranch1TableAdapter()
        Me.TblbranchTableAdapter = New iAS.SSSDBDataSetTableAdapters.tblbranchTableAdapter()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerControl1.SuspendLayout()
        Me.SidePanel3.SuspendLayout()
        CType(Me.PopupContainerControlEmp, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PopupContainerControlEmp.SuspendLayout()
        CType(Me.GridControlEmp, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblEmployeeBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewEmp, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTimeEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerControlDept, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PopupContainerControlDept.SuspendLayout()
        CType(Me.GridControlDept, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblDepartmentBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewDept, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTimeEdit3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerControlComp, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PopupContainerControlComp.SuspendLayout()
        CType(Me.GridControlComp, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblCompanyBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewComp, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTimeEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SidePanel2.SuspendLayout()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.CheckEditLEaveEncash.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditPendingReEm.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditCashAdj.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEdidFullNFinal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditSummaryStat.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditReesPyReg.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditReemSt.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditPieceM.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditPieceEmp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditArrearSlip.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditArrearReg.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditESISt.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditESICh.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditPfSt.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditPfCh.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditLoanEntry.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditLoanEntryDetails.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckSalaryNonBank.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditAdvEntry.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckSalaryBank.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditAdvEntryDetails.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckSalaryCash.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckSalaryCheque.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckSalaryRegister.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckSalarySlip.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditEmpWiseCtc.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerControlBranch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PopupContainerControlBranch.SuspendLayout()
        CType(Me.GridControlBranch, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewBranch, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTimeEdit7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit2.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerControlGrade, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PopupContainerControlGrade.SuspendLayout()
        CType(Me.GridControlGrade, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblGradeBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewGrade, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTimeEdit6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerControlCat, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PopupContainerControlCat.SuspendLayout()
        CType(Me.GridControlCat, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblCatagoryBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewCat, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTimeEdit4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEdit3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SidePanel1.SuspendLayout()
        CType(Me.ComboNepaliYearTo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboNEpaliMonthTo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboNepaliYearFrm.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboNEpaliMonthFrm.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckPDF.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckExcel.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckText.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SidePanelSelection.SuspendLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerEditEmp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerEditGrade.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerEditLocation.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerEditComp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerEditDept.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerEditCat.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEditTo.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEditTo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEditFrom.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEditFrom.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SplitContainerControl1
        '
        Me.SplitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainerControl1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainerControl1.Name = "SplitContainerControl1"
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.SidePanel3)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.SidePanel2)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.SidePanel1)
        Me.SplitContainerControl1.Panel1.Text = "Panel1"
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.MemoEdit1)
        Me.SplitContainerControl1.Panel2.Text = "Panel2"
        Me.SplitContainerControl1.Size = New System.Drawing.Size(1145, 568)
        Me.SplitContainerControl1.SplitterPosition = 1036
        Me.SplitContainerControl1.TabIndex = 2
        Me.SplitContainerControl1.Text = "SplitContainerControl1"
        '
        'SidePanel3
        '
        Me.SidePanel3.Controls.Add(Me.PopupContainerControlEmp)
        Me.SidePanel3.Controls.Add(Me.PopupContainerControlDept)
        Me.SidePanel3.Controls.Add(Me.PopupContainerControlComp)
        Me.SidePanel3.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.SidePanel3.Location = New System.Drawing.Point(0, 514)
        Me.SidePanel3.Name = "SidePanel3"
        Me.SidePanel3.Size = New System.Drawing.Size(1036, 54)
        Me.SidePanel3.TabIndex = 4
        Me.SidePanel3.Text = "SidePanel3"
        Me.SidePanel3.Visible = False
        '
        'PopupContainerControlEmp
        '
        Me.PopupContainerControlEmp.Controls.Add(Me.GridControlEmp)
        Me.PopupContainerControlEmp.Location = New System.Drawing.Point(793, 10)
        Me.PopupContainerControlEmp.Name = "PopupContainerControlEmp"
        Me.PopupContainerControlEmp.Size = New System.Drawing.Size(300, 300)
        Me.PopupContainerControlEmp.TabIndex = 10
        '
        'GridControlEmp
        '
        Me.GridControlEmp.DataSource = Me.TblEmployeeBindingSource
        Me.GridControlEmp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControlEmp.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridControlEmp.Location = New System.Drawing.Point(0, 0)
        Me.GridControlEmp.MainView = Me.GridViewEmp
        Me.GridControlEmp.Name = "GridControlEmp"
        Me.GridControlEmp.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTimeEdit1})
        Me.GridControlEmp.Size = New System.Drawing.Size(300, 300)
        Me.GridControlEmp.TabIndex = 6
        Me.GridControlEmp.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewEmp})
        '
        'TblEmployeeBindingSource
        '
        Me.TblEmployeeBindingSource.DataMember = "TblEmployee"
        Me.TblEmployeeBindingSource.DataSource = Me.SSSDBDataSet
        '
        'SSSDBDataSet
        '
        Me.SSSDBDataSet.DataSetName = "SSSDBDataSet"
        Me.SSSDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GridViewEmp
        '
        Me.GridViewEmp.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colPAYCODE, Me.colEMPNAME})
        Me.GridViewEmp.GridControl = Me.GridControlEmp
        Me.GridViewEmp.Name = "GridViewEmp"
        Me.GridViewEmp.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewEmp.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewEmp.OptionsBehavior.Editable = False
        Me.GridViewEmp.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridViewEmp.OptionsSelection.MultiSelect = True
        Me.GridViewEmp.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        Me.GridViewEmp.OptionsView.ColumnAutoWidth = False
        '
        'colPAYCODE
        '
        Me.colPAYCODE.Caption = "Paycode"
        Me.colPAYCODE.FieldName = "PAYCODE"
        Me.colPAYCODE.Name = "colPAYCODE"
        Me.colPAYCODE.Visible = True
        Me.colPAYCODE.VisibleIndex = 1
        '
        'colEMPNAME
        '
        Me.colEMPNAME.Caption = "Name"
        Me.colEMPNAME.FieldName = "EMPNAME"
        Me.colEMPNAME.Name = "colEMPNAME"
        Me.colEMPNAME.Visible = True
        Me.colEMPNAME.VisibleIndex = 2
        Me.colEMPNAME.Width = 125
        '
        'RepositoryItemTimeEdit1
        '
        Me.RepositoryItemTimeEdit1.AutoHeight = False
        Me.RepositoryItemTimeEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemTimeEdit1.Mask.EditMask = "HH:mm"
        Me.RepositoryItemTimeEdit1.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemTimeEdit1.Name = "RepositoryItemTimeEdit1"
        '
        'PopupContainerControlDept
        '
        Me.PopupContainerControlDept.Controls.Add(Me.GridControlDept)
        Me.PopupContainerControlDept.Location = New System.Drawing.Point(487, 9)
        Me.PopupContainerControlDept.Name = "PopupContainerControlDept"
        Me.PopupContainerControlDept.Size = New System.Drawing.Size(300, 300)
        Me.PopupContainerControlDept.TabIndex = 14
        '
        'GridControlDept
        '
        Me.GridControlDept.DataSource = Me.TblDepartmentBindingSource
        Me.GridControlDept.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControlDept.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridControlDept.Location = New System.Drawing.Point(0, 0)
        Me.GridControlDept.MainView = Me.GridViewDept
        Me.GridControlDept.Name = "GridControlDept"
        Me.GridControlDept.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTimeEdit3})
        Me.GridControlDept.Size = New System.Drawing.Size(300, 300)
        Me.GridControlDept.TabIndex = 6
        Me.GridControlDept.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewDept})
        '
        'TblDepartmentBindingSource
        '
        Me.TblDepartmentBindingSource.DataMember = "tblDepartment"
        Me.TblDepartmentBindingSource.DataSource = Me.SSSDBDataSet
        '
        'GridViewDept
        '
        Me.GridViewDept.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colDEPARTMENTCODE, Me.colDEPARTMENTNAME})
        Me.GridViewDept.GridControl = Me.GridControlDept
        Me.GridViewDept.Name = "GridViewDept"
        Me.GridViewDept.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewDept.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewDept.OptionsBehavior.Editable = False
        Me.GridViewDept.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridViewDept.OptionsSelection.MultiSelect = True
        Me.GridViewDept.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        Me.GridViewDept.OptionsView.ColumnAutoWidth = False
        '
        'colDEPARTMENTCODE
        '
        Me.colDEPARTMENTCODE.Caption = "Depatment Code"
        Me.colDEPARTMENTCODE.FieldName = "DEPARTMENTCODE"
        Me.colDEPARTMENTCODE.Name = "colDEPARTMENTCODE"
        Me.colDEPARTMENTCODE.Visible = True
        Me.colDEPARTMENTCODE.VisibleIndex = 1
        Me.colDEPARTMENTCODE.Width = 100
        '
        'colDEPARTMENTNAME
        '
        Me.colDEPARTMENTNAME.Caption = "Name"
        Me.colDEPARTMENTNAME.FieldName = "DEPARTMENTNAME"
        Me.colDEPARTMENTNAME.Name = "colDEPARTMENTNAME"
        Me.colDEPARTMENTNAME.Visible = True
        Me.colDEPARTMENTNAME.VisibleIndex = 2
        Me.colDEPARTMENTNAME.Width = 120
        '
        'RepositoryItemTimeEdit3
        '
        Me.RepositoryItemTimeEdit3.AutoHeight = False
        Me.RepositoryItemTimeEdit3.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemTimeEdit3.Mask.EditMask = "HH:mm"
        Me.RepositoryItemTimeEdit3.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemTimeEdit3.Name = "RepositoryItemTimeEdit3"
        '
        'PopupContainerControlComp
        '
        Me.PopupContainerControlComp.Controls.Add(Me.GridControlComp)
        Me.PopupContainerControlComp.Location = New System.Drawing.Point(327, 10)
        Me.PopupContainerControlComp.Name = "PopupContainerControlComp"
        Me.PopupContainerControlComp.Size = New System.Drawing.Size(300, 300)
        Me.PopupContainerControlComp.TabIndex = 13
        '
        'GridControlComp
        '
        Me.GridControlComp.DataSource = Me.TblCompanyBindingSource
        Me.GridControlComp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControlComp.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridControlComp.Location = New System.Drawing.Point(0, 0)
        Me.GridControlComp.MainView = Me.GridViewComp
        Me.GridControlComp.Name = "GridControlComp"
        Me.GridControlComp.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTimeEdit2})
        Me.GridControlComp.Size = New System.Drawing.Size(300, 300)
        Me.GridControlComp.TabIndex = 6
        Me.GridControlComp.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewComp})
        '
        'TblCompanyBindingSource
        '
        Me.TblCompanyBindingSource.DataMember = "tblCompany"
        Me.TblCompanyBindingSource.DataSource = Me.SSSDBDataSet
        '
        'GridViewComp
        '
        Me.GridViewComp.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colCOMPANYCODE, Me.colCOMPANYNAME})
        Me.GridViewComp.GridControl = Me.GridControlComp
        Me.GridViewComp.Name = "GridViewComp"
        Me.GridViewComp.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewComp.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewComp.OptionsBehavior.Editable = False
        Me.GridViewComp.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridViewComp.OptionsSelection.MultiSelect = True
        Me.GridViewComp.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        Me.GridViewComp.OptionsView.ColumnAutoWidth = False
        '
        'colCOMPANYCODE
        '
        Me.colCOMPANYCODE.Caption = "Company Code"
        Me.colCOMPANYCODE.FieldName = "COMPANYCODE"
        Me.colCOMPANYCODE.Name = "colCOMPANYCODE"
        Me.colCOMPANYCODE.Visible = True
        Me.colCOMPANYCODE.VisibleIndex = 1
        Me.colCOMPANYCODE.Width = 100
        '
        'colCOMPANYNAME
        '
        Me.colCOMPANYNAME.Caption = "Name"
        Me.colCOMPANYNAME.FieldName = "COMPANYNAME"
        Me.colCOMPANYNAME.Name = "colCOMPANYNAME"
        Me.colCOMPANYNAME.Visible = True
        Me.colCOMPANYNAME.VisibleIndex = 2
        Me.colCOMPANYNAME.Width = 120
        '
        'RepositoryItemTimeEdit2
        '
        Me.RepositoryItemTimeEdit2.AutoHeight = False
        Me.RepositoryItemTimeEdit2.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemTimeEdit2.Mask.EditMask = "HH:mm"
        Me.RepositoryItemTimeEdit2.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemTimeEdit2.Name = "RepositoryItemTimeEdit2"
        '
        'SidePanel2
        '
        Me.SidePanel2.Controls.Add(Me.GroupControl1)
        Me.SidePanel2.Controls.Add(Me.PopupContainerControlBranch)
        Me.SidePanel2.Controls.Add(Me.PopupContainerControlGrade)
        Me.SidePanel2.Controls.Add(Me.PopupContainerControlCat)
        Me.SidePanel2.Controls.Add(Me.CheckEdit3)
        Me.SidePanel2.Dock = System.Windows.Forms.DockStyle.Top
        Me.SidePanel2.Location = New System.Drawing.Point(0, 202)
        Me.SidePanel2.Name = "SidePanel2"
        Me.SidePanel2.Size = New System.Drawing.Size(1036, 477)
        Me.SidePanel2.TabIndex = 3
        Me.SidePanel2.Text = "SidePanel2"
        '
        'GroupControl1
        '
        Me.GroupControl1.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.GroupControl1.AppearanceCaption.ForeColor = System.Drawing.Color.Blue
        Me.GroupControl1.AppearanceCaption.Options.UseFont = True
        Me.GroupControl1.AppearanceCaption.Options.UseForeColor = True
        Me.GroupControl1.Controls.Add(Me.CheckEditLEaveEncash)
        Me.GroupControl1.Controls.Add(Me.CheckEditPendingReEm)
        Me.GroupControl1.Controls.Add(Me.CheckEditCashAdj)
        Me.GroupControl1.Controls.Add(Me.CheckEdidFullNFinal)
        Me.GroupControl1.Controls.Add(Me.CheckEditSummaryStat)
        Me.GroupControl1.Controls.Add(Me.CheckEditReesPyReg)
        Me.GroupControl1.Controls.Add(Me.CheckEditReemSt)
        Me.GroupControl1.Controls.Add(Me.CheckEditPieceM)
        Me.GroupControl1.Controls.Add(Me.CheckEditPieceEmp)
        Me.GroupControl1.Controls.Add(Me.CheckEditArrearSlip)
        Me.GroupControl1.Controls.Add(Me.CheckEditArrearReg)
        Me.GroupControl1.Controls.Add(Me.CheckEditESISt)
        Me.GroupControl1.Controls.Add(Me.CheckEditESICh)
        Me.GroupControl1.Controls.Add(Me.CheckEditPfSt)
        Me.GroupControl1.Controls.Add(Me.CheckEditPfCh)
        Me.GroupControl1.Controls.Add(Me.CheckEditLoanEntry)
        Me.GroupControl1.Controls.Add(Me.CheckEditLoanEntryDetails)
        Me.GroupControl1.Controls.Add(Me.CheckSalaryNonBank)
        Me.GroupControl1.Controls.Add(Me.CheckEditAdvEntry)
        Me.GroupControl1.Controls.Add(Me.CheckSalaryBank)
        Me.GroupControl1.Controls.Add(Me.CheckEditAdvEntryDetails)
        Me.GroupControl1.Controls.Add(Me.CheckSalaryCash)
        Me.GroupControl1.Controls.Add(Me.CheckSalaryCheque)
        Me.GroupControl1.Controls.Add(Me.CheckSalaryRegister)
        Me.GroupControl1.Controls.Add(Me.CheckSalarySlip)
        Me.GroupControl1.Controls.Add(Me.CheckEditEmpWiseCtc)
        Me.GroupControl1.Location = New System.Drawing.Point(19, 18)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(1014, 263)
        Me.GroupControl1.TabIndex = 26
        Me.GroupControl1.Text = "Report"
        '
        'CheckEditLEaveEncash
        '
        Me.CheckEditLEaveEncash.Location = New System.Drawing.Point(781, 167)
        Me.CheckEditLEaveEncash.Name = "CheckEditLEaveEncash"
        Me.CheckEditLEaveEncash.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditLEaveEncash.Properties.Appearance.Options.UseFont = True
        Me.CheckEditLEaveEncash.Properties.Caption = "Leave Encashment"
        Me.CheckEditLEaveEncash.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEditLEaveEncash.Properties.RadioGroupIndex = 1
        Me.CheckEditLEaveEncash.Size = New System.Drawing.Size(218, 19)
        Me.CheckEditLEaveEncash.TabIndex = 33
        Me.CheckEditLEaveEncash.TabStop = False
        '
        'CheckEditPendingReEm
        '
        Me.CheckEditPendingReEm.Location = New System.Drawing.Point(781, 142)
        Me.CheckEditPendingReEm.Name = "CheckEditPendingReEm"
        Me.CheckEditPendingReEm.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditPendingReEm.Properties.Appearance.Options.UseFont = True
        Me.CheckEditPendingReEm.Properties.Caption = "Pending Reimburshment List"
        Me.CheckEditPendingReEm.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEditPendingReEm.Properties.RadioGroupIndex = 1
        Me.CheckEditPendingReEm.Size = New System.Drawing.Size(218, 19)
        Me.CheckEditPendingReEm.TabIndex = 32
        Me.CheckEditPendingReEm.TabStop = False
        '
        'CheckEditCashAdj
        '
        Me.CheckEditCashAdj.Location = New System.Drawing.Point(206, 167)
        Me.CheckEditCashAdj.Name = "CheckEditCashAdj"
        Me.CheckEditCashAdj.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditCashAdj.Properties.Appearance.Options.UseFont = True
        Me.CheckEditCashAdj.Properties.Caption = "Cash Adjustment"
        Me.CheckEditCashAdj.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEditCashAdj.Properties.RadioGroupIndex = 1
        Me.CheckEditCashAdj.Size = New System.Drawing.Size(158, 19)
        Me.CheckEditCashAdj.TabIndex = 31
        Me.CheckEditCashAdj.TabStop = False
        '
        'CheckEdidFullNFinal
        '
        Me.CheckEdidFullNFinal.Location = New System.Drawing.Point(9, 217)
        Me.CheckEdidFullNFinal.Name = "CheckEdidFullNFinal"
        Me.CheckEdidFullNFinal.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEdidFullNFinal.Properties.Appearance.Options.UseFont = True
        Me.CheckEdidFullNFinal.Properties.Caption = "Full And Final"
        Me.CheckEdidFullNFinal.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEdidFullNFinal.Properties.RadioGroupIndex = 1
        Me.CheckEdidFullNFinal.Size = New System.Drawing.Size(158, 19)
        Me.CheckEdidFullNFinal.TabIndex = 30
        Me.CheckEdidFullNFinal.TabStop = False
        '
        'CheckEditSummaryStat
        '
        Me.CheckEditSummaryStat.Location = New System.Drawing.Point(206, 142)
        Me.CheckEditSummaryStat.Name = "CheckEditSummaryStat"
        Me.CheckEditSummaryStat.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditSummaryStat.Properties.Appearance.Options.UseFont = True
        Me.CheckEditSummaryStat.Properties.Caption = "Summary Statement"
        Me.CheckEditSummaryStat.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEditSummaryStat.Properties.RadioGroupIndex = 1
        Me.CheckEditSummaryStat.Size = New System.Drawing.Size(158, 19)
        Me.CheckEditSummaryStat.TabIndex = 29
        Me.CheckEditSummaryStat.TabStop = False
        '
        'CheckEditReesPyReg
        '
        Me.CheckEditReesPyReg.Location = New System.Drawing.Point(781, 117)
        Me.CheckEditReesPyReg.Name = "CheckEditReesPyReg"
        Me.CheckEditReesPyReg.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditReesPyReg.Properties.Appearance.Options.UseFont = True
        Me.CheckEditReesPyReg.Properties.Caption = "Reimburshment Payment Register"
        Me.CheckEditReesPyReg.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEditReesPyReg.Properties.RadioGroupIndex = 1
        Me.CheckEditReesPyReg.Size = New System.Drawing.Size(218, 19)
        Me.CheckEditReesPyReg.TabIndex = 28
        Me.CheckEditReesPyReg.TabStop = False
        '
        'CheckEditReemSt
        '
        Me.CheckEditReemSt.Location = New System.Drawing.Point(781, 92)
        Me.CheckEditReemSt.Name = "CheckEditReemSt"
        Me.CheckEditReemSt.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditReemSt.Properties.Appearance.Options.UseFont = True
        Me.CheckEditReemSt.Properties.Caption = "Reimburshment Statement"
        Me.CheckEditReemSt.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEditReemSt.Properties.RadioGroupIndex = 1
        Me.CheckEditReemSt.Size = New System.Drawing.Size(177, 19)
        Me.CheckEditReemSt.TabIndex = 27
        Me.CheckEditReemSt.TabStop = False
        '
        'CheckEditPieceM
        '
        Me.CheckEditPieceM.Location = New System.Drawing.Point(781, 42)
        Me.CheckEditPieceM.Name = "CheckEditPieceM"
        Me.CheckEditPieceM.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditPieceM.Properties.Appearance.Options.UseFont = True
        Me.CheckEditPieceM.Properties.Caption = "Piece Master"
        Me.CheckEditPieceM.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEditPieceM.Properties.RadioGroupIndex = 1
        Me.CheckEditPieceM.Size = New System.Drawing.Size(119, 19)
        Me.CheckEditPieceM.TabIndex = 25
        Me.CheckEditPieceM.TabStop = False
        '
        'CheckEditPieceEmp
        '
        Me.CheckEditPieceEmp.Location = New System.Drawing.Point(781, 67)
        Me.CheckEditPieceEmp.Name = "CheckEditPieceEmp"
        Me.CheckEditPieceEmp.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditPieceEmp.Properties.Appearance.Options.UseFont = True
        Me.CheckEditPieceEmp.Properties.Caption = "Employee Wise Piece "
        Me.CheckEditPieceEmp.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEditPieceEmp.Properties.RadioGroupIndex = 1
        Me.CheckEditPieceEmp.Size = New System.Drawing.Size(137, 19)
        Me.CheckEditPieceEmp.TabIndex = 26
        Me.CheckEditPieceEmp.TabStop = False
        '
        'CheckEditArrearSlip
        '
        Me.CheckEditArrearSlip.Location = New System.Drawing.Point(657, 42)
        Me.CheckEditArrearSlip.Name = "CheckEditArrearSlip"
        Me.CheckEditArrearSlip.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditArrearSlip.Properties.Appearance.Options.UseFont = True
        Me.CheckEditArrearSlip.Properties.Caption = "Arrear Slip"
        Me.CheckEditArrearSlip.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEditArrearSlip.Properties.RadioGroupIndex = 1
        Me.CheckEditArrearSlip.Size = New System.Drawing.Size(119, 19)
        Me.CheckEditArrearSlip.TabIndex = 23
        Me.CheckEditArrearSlip.TabStop = False
        '
        'CheckEditArrearReg
        '
        Me.CheckEditArrearReg.Location = New System.Drawing.Point(657, 67)
        Me.CheckEditArrearReg.Name = "CheckEditArrearReg"
        Me.CheckEditArrearReg.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditArrearReg.Properties.Appearance.Options.UseFont = True
        Me.CheckEditArrearReg.Properties.Caption = "Arrear Register"
        Me.CheckEditArrearReg.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEditArrearReg.Properties.RadioGroupIndex = 1
        Me.CheckEditArrearReg.Size = New System.Drawing.Size(119, 19)
        Me.CheckEditArrearReg.TabIndex = 24
        Me.CheckEditArrearReg.TabStop = False
        '
        'CheckEditESISt
        '
        Me.CheckEditESISt.Location = New System.Drawing.Point(521, 42)
        Me.CheckEditESISt.Name = "CheckEditESISt"
        Me.CheckEditESISt.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditESISt.Properties.Appearance.Options.UseFont = True
        Me.CheckEditESISt.Properties.Caption = "ESI Statement"
        Me.CheckEditESISt.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEditESISt.Properties.RadioGroupIndex = 1
        Me.CheckEditESISt.Size = New System.Drawing.Size(119, 19)
        Me.CheckEditESISt.TabIndex = 21
        Me.CheckEditESISt.TabStop = False
        '
        'CheckEditESICh
        '
        Me.CheckEditESICh.Location = New System.Drawing.Point(521, 67)
        Me.CheckEditESICh.Name = "CheckEditESICh"
        Me.CheckEditESICh.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditESICh.Properties.Appearance.Options.UseFont = True
        Me.CheckEditESICh.Properties.Caption = "ESI Chalan"
        Me.CheckEditESICh.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEditESICh.Properties.RadioGroupIndex = 1
        Me.CheckEditESICh.Size = New System.Drawing.Size(119, 19)
        Me.CheckEditESICh.TabIndex = 22
        Me.CheckEditESICh.TabStop = False
        '
        'CheckEditPfSt
        '
        Me.CheckEditPfSt.Location = New System.Drawing.Point(396, 42)
        Me.CheckEditPfSt.Name = "CheckEditPfSt"
        Me.CheckEditPfSt.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditPfSt.Properties.Appearance.Options.UseFont = True
        Me.CheckEditPfSt.Properties.Caption = "PF Statement"
        Me.CheckEditPfSt.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEditPfSt.Properties.RadioGroupIndex = 1
        Me.CheckEditPfSt.Size = New System.Drawing.Size(119, 19)
        Me.CheckEditPfSt.TabIndex = 13
        Me.CheckEditPfSt.TabStop = False
        '
        'CheckEditPfCh
        '
        Me.CheckEditPfCh.Location = New System.Drawing.Point(396, 67)
        Me.CheckEditPfCh.Name = "CheckEditPfCh"
        Me.CheckEditPfCh.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditPfCh.Properties.Appearance.Options.UseFont = True
        Me.CheckEditPfCh.Properties.Caption = "PF Chalan"
        Me.CheckEditPfCh.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEditPfCh.Properties.RadioGroupIndex = 1
        Me.CheckEditPfCh.Size = New System.Drawing.Size(119, 19)
        Me.CheckEditPfCh.TabIndex = 14
        Me.CheckEditPfCh.TabStop = False
        '
        'CheckEditLoanEntry
        '
        Me.CheckEditLoanEntry.Location = New System.Drawing.Point(206, 42)
        Me.CheckEditLoanEntry.Name = "CheckEditLoanEntry"
        Me.CheckEditLoanEntry.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditLoanEntry.Properties.Appearance.Options.UseFont = True
        Me.CheckEditLoanEntry.Properties.Caption = "Loan Entry Statement"
        Me.CheckEditLoanEntry.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEditLoanEntry.Properties.RadioGroupIndex = 1
        Me.CheckEditLoanEntry.Size = New System.Drawing.Size(158, 19)
        Me.CheckEditLoanEntry.TabIndex = 13
        Me.CheckEditLoanEntry.TabStop = False
        '
        'CheckEditLoanEntryDetails
        '
        Me.CheckEditLoanEntryDetails.Location = New System.Drawing.Point(206, 67)
        Me.CheckEditLoanEntryDetails.Name = "CheckEditLoanEntryDetails"
        Me.CheckEditLoanEntryDetails.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditLoanEntryDetails.Properties.Appearance.Options.UseFont = True
        Me.CheckEditLoanEntryDetails.Properties.Caption = "Loan Entry Datails"
        Me.CheckEditLoanEntryDetails.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEditLoanEntryDetails.Properties.RadioGroupIndex = 1
        Me.CheckEditLoanEntryDetails.Size = New System.Drawing.Size(158, 19)
        Me.CheckEditLoanEntryDetails.TabIndex = 14
        Me.CheckEditLoanEntryDetails.TabStop = False
        '
        'CheckSalaryNonBank
        '
        Me.CheckSalaryNonBank.Location = New System.Drawing.Point(9, 192)
        Me.CheckSalaryNonBank.Name = "CheckSalaryNonBank"
        Me.CheckSalaryNonBank.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckSalaryNonBank.Properties.Appearance.Options.UseFont = True
        Me.CheckSalaryNonBank.Properties.Caption = "Non Bank Salary Statement"
        Me.CheckSalaryNonBank.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckSalaryNonBank.Properties.RadioGroupIndex = 1
        Me.CheckSalaryNonBank.Size = New System.Drawing.Size(177, 19)
        Me.CheckSalaryNonBank.TabIndex = 20
        Me.CheckSalaryNonBank.TabStop = False
        '
        'CheckEditAdvEntry
        '
        Me.CheckEditAdvEntry.Location = New System.Drawing.Point(206, 92)
        Me.CheckEditAdvEntry.Name = "CheckEditAdvEntry"
        Me.CheckEditAdvEntry.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditAdvEntry.Properties.Appearance.Options.UseFont = True
        Me.CheckEditAdvEntry.Properties.Caption = "Advance Entry Statement"
        Me.CheckEditAdvEntry.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEditAdvEntry.Properties.RadioGroupIndex = 1
        Me.CheckEditAdvEntry.Size = New System.Drawing.Size(167, 19)
        Me.CheckEditAdvEntry.TabIndex = 15
        Me.CheckEditAdvEntry.TabStop = False
        '
        'CheckSalaryBank
        '
        Me.CheckSalaryBank.Location = New System.Drawing.Point(9, 167)
        Me.CheckSalaryBank.Name = "CheckSalaryBank"
        Me.CheckSalaryBank.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckSalaryBank.Properties.Appearance.Options.UseFont = True
        Me.CheckSalaryBank.Properties.Caption = "Bank Salary Statement"
        Me.CheckSalaryBank.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckSalaryBank.Properties.RadioGroupIndex = 1
        Me.CheckSalaryBank.Size = New System.Drawing.Size(158, 19)
        Me.CheckSalaryBank.TabIndex = 19
        Me.CheckSalaryBank.TabStop = False
        '
        'CheckEditAdvEntryDetails
        '
        Me.CheckEditAdvEntryDetails.Location = New System.Drawing.Point(206, 117)
        Me.CheckEditAdvEntryDetails.Name = "CheckEditAdvEntryDetails"
        Me.CheckEditAdvEntryDetails.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditAdvEntryDetails.Properties.Appearance.Options.UseFont = True
        Me.CheckEditAdvEntryDetails.Properties.Caption = "Advance Entry Datails"
        Me.CheckEditAdvEntryDetails.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEditAdvEntryDetails.Properties.RadioGroupIndex = 1
        Me.CheckEditAdvEntryDetails.Size = New System.Drawing.Size(158, 19)
        Me.CheckEditAdvEntryDetails.TabIndex = 16
        Me.CheckEditAdvEntryDetails.TabStop = False
        '
        'CheckSalaryCash
        '
        Me.CheckSalaryCash.Location = New System.Drawing.Point(9, 142)
        Me.CheckSalaryCash.Name = "CheckSalaryCash"
        Me.CheckSalaryCash.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckSalaryCash.Properties.Appearance.Options.UseFont = True
        Me.CheckSalaryCash.Properties.Caption = "Salary By Cash"
        Me.CheckSalaryCash.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckSalaryCash.Properties.RadioGroupIndex = 1
        Me.CheckSalaryCash.Size = New System.Drawing.Size(158, 19)
        Me.CheckSalaryCash.TabIndex = 18
        Me.CheckSalaryCash.TabStop = False
        '
        'CheckSalaryCheque
        '
        Me.CheckSalaryCheque.Location = New System.Drawing.Point(9, 117)
        Me.CheckSalaryCheque.Name = "CheckSalaryCheque"
        Me.CheckSalaryCheque.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckSalaryCheque.Properties.Appearance.Options.UseFont = True
        Me.CheckSalaryCheque.Properties.Caption = "Salary By Cheque"
        Me.CheckSalaryCheque.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckSalaryCheque.Properties.RadioGroupIndex = 1
        Me.CheckSalaryCheque.Size = New System.Drawing.Size(158, 19)
        Me.CheckSalaryCheque.TabIndex = 17
        Me.CheckSalaryCheque.TabStop = False
        '
        'CheckSalaryRegister
        '
        Me.CheckSalaryRegister.EditValue = True
        Me.CheckSalaryRegister.Location = New System.Drawing.Point(9, 42)
        Me.CheckSalaryRegister.Name = "CheckSalaryRegister"
        Me.CheckSalaryRegister.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckSalaryRegister.Properties.Appearance.Options.UseFont = True
        Me.CheckSalaryRegister.Properties.Caption = "Salary Register"
        Me.CheckSalaryRegister.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckSalaryRegister.Properties.RadioGroupIndex = 1
        Me.CheckSalaryRegister.Size = New System.Drawing.Size(158, 19)
        Me.CheckSalaryRegister.TabIndex = 11
        '
        'CheckSalarySlip
        '
        Me.CheckSalarySlip.Location = New System.Drawing.Point(9, 67)
        Me.CheckSalarySlip.Name = "CheckSalarySlip"
        Me.CheckSalarySlip.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckSalarySlip.Properties.Appearance.Options.UseFont = True
        Me.CheckSalarySlip.Properties.Caption = "Salary Slip"
        Me.CheckSalarySlip.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckSalarySlip.Properties.RadioGroupIndex = 1
        Me.CheckSalarySlip.Size = New System.Drawing.Size(158, 19)
        Me.CheckSalarySlip.TabIndex = 12
        Me.CheckSalarySlip.TabStop = False
        '
        'CheckEditEmpWiseCtc
        '
        Me.CheckEditEmpWiseCtc.Location = New System.Drawing.Point(9, 92)
        Me.CheckEditEmpWiseCtc.Name = "CheckEditEmpWiseCtc"
        Me.CheckEditEmpWiseCtc.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditEmpWiseCtc.Properties.Appearance.Options.UseFont = True
        Me.CheckEditEmpWiseCtc.Properties.Caption = "Employee Wise CTC"
        Me.CheckEditEmpWiseCtc.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEditEmpWiseCtc.Properties.RadioGroupIndex = 1
        Me.CheckEditEmpWiseCtc.Size = New System.Drawing.Size(158, 19)
        Me.CheckEditEmpWiseCtc.TabIndex = 11
        Me.CheckEditEmpWiseCtc.TabStop = False
        '
        'PopupContainerControlBranch
        '
        Me.PopupContainerControlBranch.Controls.Add(Me.GridControlBranch)
        Me.PopupContainerControlBranch.Location = New System.Drawing.Point(855, 221)
        Me.PopupContainerControlBranch.Name = "PopupContainerControlBranch"
        Me.PopupContainerControlBranch.Size = New System.Drawing.Size(300, 300)
        Me.PopupContainerControlBranch.TabIndex = 25
        '
        'GridControlBranch
        '
        Me.GridControlBranch.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControlBranch.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridControlBranch.Location = New System.Drawing.Point(0, 0)
        Me.GridControlBranch.MainView = Me.GridViewBranch
        Me.GridControlBranch.Name = "GridControlBranch"
        Me.GridControlBranch.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTimeEdit7, Me.RepositoryItemDateEdit2})
        Me.GridControlBranch.Size = New System.Drawing.Size(300, 300)
        Me.GridControlBranch.TabIndex = 6
        Me.GridControlBranch.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewBranch})
        '
        'GridViewBranch
        '
        Me.GridViewBranch.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colBRANCHCODE, Me.colBRANCHNAME})
        Me.GridViewBranch.GridControl = Me.GridControlBranch
        Me.GridViewBranch.Name = "GridViewBranch"
        Me.GridViewBranch.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewBranch.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewBranch.OptionsBehavior.Editable = False
        Me.GridViewBranch.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridViewBranch.OptionsSelection.MultiSelect = True
        Me.GridViewBranch.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        Me.GridViewBranch.OptionsView.ColumnAutoWidth = False
        '
        'colBRANCHCODE
        '
        Me.colBRANCHCODE.Caption = "Location Code"
        Me.colBRANCHCODE.FieldName = "BRANCHCODE"
        Me.colBRANCHCODE.Name = "colBRANCHCODE"
        Me.colBRANCHCODE.Visible = True
        Me.colBRANCHCODE.VisibleIndex = 1
        Me.colBRANCHCODE.Width = 100
        '
        'colBRANCHNAME
        '
        Me.colBRANCHNAME.Caption = "Name"
        Me.colBRANCHNAME.FieldName = "BRANCHNAME"
        Me.colBRANCHNAME.Name = "colBRANCHNAME"
        Me.colBRANCHNAME.Visible = True
        Me.colBRANCHNAME.VisibleIndex = 2
        Me.colBRANCHNAME.Width = 120
        '
        'RepositoryItemTimeEdit7
        '
        Me.RepositoryItemTimeEdit7.AutoHeight = False
        Me.RepositoryItemTimeEdit7.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemTimeEdit7.Mask.EditMask = "HH:mm"
        Me.RepositoryItemTimeEdit7.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemTimeEdit7.Name = "RepositoryItemTimeEdit7"
        '
        'RepositoryItemDateEdit2
        '
        Me.RepositoryItemDateEdit2.AutoHeight = False
        Me.RepositoryItemDateEdit2.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit2.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit2.Mask.EditMask = "HH:mm"
        Me.RepositoryItemDateEdit2.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemDateEdit2.Name = "RepositoryItemDateEdit2"
        '
        'PopupContainerControlGrade
        '
        Me.PopupContainerControlGrade.Controls.Add(Me.GridControlGrade)
        Me.PopupContainerControlGrade.Location = New System.Drawing.Point(225, 300)
        Me.PopupContainerControlGrade.Name = "PopupContainerControlGrade"
        Me.PopupContainerControlGrade.Size = New System.Drawing.Size(300, 300)
        Me.PopupContainerControlGrade.TabIndex = 23
        '
        'GridControlGrade
        '
        Me.GridControlGrade.DataSource = Me.TblGradeBindingSource
        Me.GridControlGrade.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControlGrade.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridControlGrade.Location = New System.Drawing.Point(0, 0)
        Me.GridControlGrade.MainView = Me.GridViewGrade
        Me.GridControlGrade.Name = "GridControlGrade"
        Me.GridControlGrade.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTimeEdit6})
        Me.GridControlGrade.Size = New System.Drawing.Size(300, 300)
        Me.GridControlGrade.TabIndex = 6
        Me.GridControlGrade.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewGrade})
        '
        'TblGradeBindingSource
        '
        Me.TblGradeBindingSource.DataMember = "tblGrade"
        Me.TblGradeBindingSource.DataSource = Me.SSSDBDataSet
        '
        'GridViewGrade
        '
        Me.GridViewGrade.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn2})
        Me.GridViewGrade.GridControl = Me.GridControlGrade
        Me.GridViewGrade.Name = "GridViewGrade"
        Me.GridViewGrade.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewGrade.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewGrade.OptionsBehavior.Editable = False
        Me.GridViewGrade.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridViewGrade.OptionsSelection.MultiSelect = True
        Me.GridViewGrade.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        Me.GridViewGrade.OptionsView.ColumnAutoWidth = False
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Grade Code"
        Me.GridColumn1.FieldName = "GradeCode"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 1
        Me.GridColumn1.Width = 100
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Name"
        Me.GridColumn2.FieldName = "GradeName"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 2
        Me.GridColumn2.Width = 120
        '
        'RepositoryItemTimeEdit6
        '
        Me.RepositoryItemTimeEdit6.AutoHeight = False
        Me.RepositoryItemTimeEdit6.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemTimeEdit6.Mask.EditMask = "HH:mm"
        Me.RepositoryItemTimeEdit6.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemTimeEdit6.Name = "RepositoryItemTimeEdit6"
        '
        'PopupContainerControlCat
        '
        Me.PopupContainerControlCat.Controls.Add(Me.GridControlCat)
        Me.PopupContainerControlCat.Location = New System.Drawing.Point(531, 300)
        Me.PopupContainerControlCat.Name = "PopupContainerControlCat"
        Me.PopupContainerControlCat.Size = New System.Drawing.Size(300, 300)
        Me.PopupContainerControlCat.TabIndex = 15
        '
        'GridControlCat
        '
        Me.GridControlCat.DataSource = Me.TblCatagoryBindingSource
        Me.GridControlCat.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControlCat.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridControlCat.Location = New System.Drawing.Point(0, 0)
        Me.GridControlCat.MainView = Me.GridViewCat
        Me.GridControlCat.Name = "GridControlCat"
        Me.GridControlCat.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTimeEdit4})
        Me.GridControlCat.Size = New System.Drawing.Size(300, 300)
        Me.GridControlCat.TabIndex = 6
        Me.GridControlCat.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewCat})
        '
        'TblCatagoryBindingSource
        '
        Me.TblCatagoryBindingSource.DataMember = "tblCatagory"
        Me.TblCatagoryBindingSource.DataSource = Me.SSSDBDataSet
        '
        'GridViewCat
        '
        Me.GridViewCat.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colCAT, Me.colCATAGORYNAME})
        Me.GridViewCat.GridControl = Me.GridControlCat
        Me.GridViewCat.Name = "GridViewCat"
        Me.GridViewCat.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewCat.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewCat.OptionsBehavior.Editable = False
        Me.GridViewCat.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridViewCat.OptionsSelection.MultiSelect = True
        Me.GridViewCat.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        Me.GridViewCat.OptionsView.ColumnAutoWidth = False
        '
        'colCAT
        '
        Me.colCAT.Caption = "Catagory Code"
        Me.colCAT.FieldName = "CAT"
        Me.colCAT.Name = "colCAT"
        Me.colCAT.Visible = True
        Me.colCAT.VisibleIndex = 1
        Me.colCAT.Width = 100
        '
        'colCATAGORYNAME
        '
        Me.colCATAGORYNAME.Caption = "Name"
        Me.colCATAGORYNAME.FieldName = "CATAGORYNAME"
        Me.colCATAGORYNAME.Name = "colCATAGORYNAME"
        Me.colCATAGORYNAME.Visible = True
        Me.colCATAGORYNAME.VisibleIndex = 2
        Me.colCATAGORYNAME.Width = 120
        '
        'RepositoryItemTimeEdit4
        '
        Me.RepositoryItemTimeEdit4.AutoHeight = False
        Me.RepositoryItemTimeEdit4.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemTimeEdit4.Mask.EditMask = "HH:mm"
        Me.RepositoryItemTimeEdit4.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemTimeEdit4.Name = "RepositoryItemTimeEdit4"
        '
        'CheckEdit3
        '
        Me.CheckEdit3.Location = New System.Drawing.Point(19, 287)
        Me.CheckEdit3.Name = "CheckEdit3"
        Me.CheckEdit3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEdit3.Properties.Appearance.Options.UseFont = True
        Me.CheckEdit3.Properties.Caption = "Salary Slip"
        Me.CheckEdit3.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEdit3.Properties.RadioGroupIndex = 1
        Me.CheckEdit3.Size = New System.Drawing.Size(158, 19)
        Me.CheckEdit3.TabIndex = 12
        Me.CheckEdit3.TabStop = False
        Me.CheckEdit3.Visible = False
        '
        'SidePanel1
        '
        Me.SidePanel1.Controls.Add(Me.ComboNepaliYearTo)
        Me.SidePanel1.Controls.Add(Me.ComboNEpaliMonthTo)
        Me.SidePanel1.Controls.Add(Me.ComboNepaliYearFrm)
        Me.SidePanel1.Controls.Add(Me.ComboNEpaliMonthFrm)
        Me.SidePanel1.Controls.Add(Me.CheckPDF)
        Me.SidePanel1.Controls.Add(Me.CheckExcel)
        Me.SidePanel1.Controls.Add(Me.CheckText)
        Me.SidePanel1.Controls.Add(Me.SidePanelSelection)
        Me.SidePanel1.Controls.Add(Me.SimpleButton1)
        Me.SidePanel1.Controls.Add(Me.DateEditTo)
        Me.SidePanel1.Controls.Add(Me.LabelControl3)
        Me.SidePanel1.Controls.Add(Me.DateEditFrom)
        Me.SidePanel1.Controls.Add(Me.LabelControl2)
        Me.SidePanel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.SidePanel1.Location = New System.Drawing.Point(0, 0)
        Me.SidePanel1.Name = "SidePanel1"
        Me.SidePanel1.Size = New System.Drawing.Size(1036, 202)
        Me.SidePanel1.TabIndex = 2
        Me.SidePanel1.Text = "SidePanel1"
        '
        'ComboNepaliYearTo
        '
        Me.ComboNepaliYearTo.Location = New System.Drawing.Point(449, 16)
        Me.ComboNepaliYearTo.Name = "ComboNepaliYearTo"
        Me.ComboNepaliYearTo.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliYearTo.Properties.Appearance.Options.UseFont = True
        Me.ComboNepaliYearTo.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliYearTo.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNepaliYearTo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNepaliYearTo.Properties.Items.AddRange(New Object() {"2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037", "2038", "2039", "2040", "2041", "2042", "2043", "2044", "2045", "2046", "2047", "2048", "2049", "2050", "2051", "2052", "2053", "2054", "2055", "2056", "2057", "2058", "2059", "2060", "2061", "2062", "2063", "2064", "2065", "2066", "2067", "2068", "2069", "2070", "2071", "2072", "2073", "2074", "2075", "2076", "2077", "2078", "2079", "2080", "2081", "2082", "2083", "2084", "2085", "2086", "2087", "2088", "2089"})
        Me.ComboNepaliYearTo.Size = New System.Drawing.Size(61, 20)
        Me.ComboNepaliYearTo.TabIndex = 31
        '
        'ComboNEpaliMonthTo
        '
        Me.ComboNEpaliMonthTo.Location = New System.Drawing.Point(363, 16)
        Me.ComboNEpaliMonthTo.Name = "ComboNEpaliMonthTo"
        Me.ComboNEpaliMonthTo.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNEpaliMonthTo.Properties.Appearance.Options.UseFont = True
        Me.ComboNEpaliMonthTo.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNEpaliMonthTo.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNEpaliMonthTo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNEpaliMonthTo.Properties.Items.AddRange(New Object() {"Baishakh", "Jestha", "Asar", "Shrawan", "Bhadau", "Aswin", "Kartik", "Mansir", "Poush", "Magh", "Falgun", "Chaitra"})
        Me.ComboNEpaliMonthTo.Size = New System.Drawing.Size(80, 20)
        Me.ComboNEpaliMonthTo.TabIndex = 30
        '
        'ComboNepaliYearFrm
        '
        Me.ComboNepaliYearFrm.Location = New System.Drawing.Point(180, 16)
        Me.ComboNepaliYearFrm.Name = "ComboNepaliYearFrm"
        Me.ComboNepaliYearFrm.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliYearFrm.Properties.Appearance.Options.UseFont = True
        Me.ComboNepaliYearFrm.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliYearFrm.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNepaliYearFrm.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNepaliYearFrm.Properties.Items.AddRange(New Object() {"2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037", "2038", "2039", "2040", "2041", "2042", "2043", "2044", "2045", "2046", "2047", "2048", "2049", "2050", "2051", "2052", "2053", "2054", "2055", "2056", "2057", "2058", "2059", "2060", "2061", "2062", "2063", "2064", "2065", "2066", "2067", "2068", "2069", "2070", "2071", "2072", "2073", "2074", "2075", "2076", "2077", "2078", "2079", "2080", "2081", "2082", "2083", "2084", "2085", "2086", "2087", "2088", "2089"})
        Me.ComboNepaliYearFrm.Size = New System.Drawing.Size(61, 20)
        Me.ComboNepaliYearFrm.TabIndex = 29
        '
        'ComboNEpaliMonthFrm
        '
        Me.ComboNEpaliMonthFrm.Location = New System.Drawing.Point(94, 16)
        Me.ComboNEpaliMonthFrm.Name = "ComboNEpaliMonthFrm"
        Me.ComboNEpaliMonthFrm.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNEpaliMonthFrm.Properties.Appearance.Options.UseFont = True
        Me.ComboNEpaliMonthFrm.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNEpaliMonthFrm.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNEpaliMonthFrm.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNEpaliMonthFrm.Properties.Items.AddRange(New Object() {"Baishakh", "Jestha", "Asar", "Shrawan", "Bhadau", "Aswin", "Kartik", "Mansir", "Poush", "Magh", "Falgun", "Chaitra"})
        Me.ComboNEpaliMonthFrm.Size = New System.Drawing.Size(80, 20)
        Me.ComboNEpaliMonthFrm.TabIndex = 28
        '
        'CheckPDF
        '
        Me.CheckPDF.Location = New System.Drawing.Point(855, 13)
        Me.CheckPDF.Name = "CheckPDF"
        Me.CheckPDF.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckPDF.Properties.Appearance.Options.UseFont = True
        Me.CheckPDF.Properties.Caption = "PDF"
        Me.CheckPDF.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckPDF.Properties.RadioGroupIndex = 0
        Me.CheckPDF.Size = New System.Drawing.Size(63, 19)
        Me.CheckPDF.TabIndex = 25
        Me.CheckPDF.TabStop = False
        Me.CheckPDF.Visible = False
        '
        'CheckExcel
        '
        Me.CheckExcel.Location = New System.Drawing.Point(768, 13)
        Me.CheckExcel.Name = "CheckExcel"
        Me.CheckExcel.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckExcel.Properties.Appearance.Options.UseFont = True
        Me.CheckExcel.Properties.Caption = "Excel"
        Me.CheckExcel.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckExcel.Properties.RadioGroupIndex = 0
        Me.CheckExcel.Size = New System.Drawing.Size(63, 19)
        Me.CheckExcel.TabIndex = 23
        Me.CheckExcel.TabStop = False
        Me.CheckExcel.Visible = False
        '
        'CheckText
        '
        Me.CheckText.EditValue = True
        Me.CheckText.Location = New System.Drawing.Point(663, 14)
        Me.CheckText.Name = "CheckText"
        Me.CheckText.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckText.Properties.Appearance.Options.UseFont = True
        Me.CheckText.Properties.Caption = "Text"
        Me.CheckText.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckText.Properties.RadioGroupIndex = 0
        Me.CheckText.Size = New System.Drawing.Size(63, 19)
        Me.CheckText.TabIndex = 22
        Me.CheckText.Visible = False
        '
        'SidePanelSelection
        '
        Me.SidePanelSelection.Controls.Add(Me.GridControl1)
        Me.SidePanelSelection.Controls.Add(Me.LabelControl1)
        Me.SidePanelSelection.Controls.Add(Me.LabelControl9)
        Me.SidePanelSelection.Controls.Add(Me.PopupContainerEditEmp)
        Me.SidePanelSelection.Controls.Add(Me.PopupContainerEditGrade)
        Me.SidePanelSelection.Controls.Add(Me.CheckEdit1)
        Me.SidePanelSelection.Controls.Add(Me.LabelControl8)
        Me.SidePanelSelection.Controls.Add(Me.CheckEdit2)
        Me.SidePanelSelection.Controls.Add(Me.PopupContainerEditLocation)
        Me.SidePanelSelection.Controls.Add(Me.PopupContainerEditComp)
        Me.SidePanelSelection.Controls.Add(Me.LabelControl4)
        Me.SidePanelSelection.Controls.Add(Me.PopupContainerEditDept)
        Me.SidePanelSelection.Controls.Add(Me.LabelControl6)
        Me.SidePanelSelection.Controls.Add(Me.LabelControl5)
        Me.SidePanelSelection.Controls.Add(Me.PopupContainerEditCat)
        Me.SidePanelSelection.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.SidePanelSelection.Location = New System.Drawing.Point(0, 44)
        Me.SidePanelSelection.Name = "SidePanelSelection"
        Me.SidePanelSelection.Size = New System.Drawing.Size(1036, 157)
        Me.SidePanelSelection.TabIndex = 21
        Me.SidePanelSelection.Text = "SidePanel4"
        '
        'GridControl1
        '
        Me.GridControl1.Location = New System.Drawing.Point(737, 4)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(283, 148)
        Me.GridControl1.TabIndex = 21
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        Me.GridControl1.Visible = False
        '
        'GridView1
        '
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Location = New System.Drawing.Point(16, 19)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(91, 14)
        Me.LabelControl1.TabIndex = 5
        Me.LabelControl1.Text = "Select Employee"
        '
        'LabelControl9
        '
        Me.LabelControl9.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl9.Appearance.Options.UseFont = True
        Me.LabelControl9.Location = New System.Drawing.Point(366, 70)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(70, 14)
        Me.LabelControl9.TabIndex = 20
        Me.LabelControl9.Text = "Select Grade"
        '
        'PopupContainerEditEmp
        '
        Me.PopupContainerEditEmp.Location = New System.Drawing.Point(113, 16)
        Me.PopupContainerEditEmp.Name = "PopupContainerEditEmp"
        Me.PopupContainerEditEmp.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditEmp.Properties.Appearance.Options.UseFont = True
        Me.PopupContainerEditEmp.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditEmp.Properties.AppearanceDropDown.Options.UseFont = True
        Me.PopupContainerEditEmp.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditEmp.Properties.AppearanceFocused.Options.UseFont = True
        Me.PopupContainerEditEmp.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.PopupContainerEditEmp.Properties.PopupControl = Me.PopupContainerControlEmp
        Me.PopupContainerEditEmp.Size = New System.Drawing.Size(225, 20)
        Me.PopupContainerEditEmp.TabIndex = 6
        Me.PopupContainerEditEmp.ToolTip = "Leave blank if want for all Employees"
        '
        'PopupContainerEditGrade
        '
        Me.PopupContainerEditGrade.Location = New System.Drawing.Point(501, 67)
        Me.PopupContainerEditGrade.Name = "PopupContainerEditGrade"
        Me.PopupContainerEditGrade.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditGrade.Properties.Appearance.Options.UseFont = True
        Me.PopupContainerEditGrade.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditGrade.Properties.AppearanceDropDown.Options.UseFont = True
        Me.PopupContainerEditGrade.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditGrade.Properties.AppearanceFocused.Options.UseFont = True
        Me.PopupContainerEditGrade.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.PopupContainerEditGrade.Properties.PopupControl = Me.PopupContainerControlGrade
        Me.PopupContainerEditGrade.Size = New System.Drawing.Size(225, 20)
        Me.PopupContainerEditGrade.TabIndex = 19
        Me.PopupContainerEditGrade.ToolTip = "Leave blank if want for all Employees"
        '
        'CheckEdit1
        '
        Me.CheckEdit1.EditValue = True
        Me.CheckEdit1.Location = New System.Drawing.Point(19, 115)
        Me.CheckEdit1.Name = "CheckEdit1"
        Me.CheckEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEdit1.Properties.Appearance.Options.UseFont = True
        Me.CheckEdit1.Properties.Caption = "Active Employee"
        Me.CheckEdit1.Size = New System.Drawing.Size(155, 19)
        Me.CheckEdit1.TabIndex = 7
        '
        'LabelControl8
        '
        Me.LabelControl8.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl8.Appearance.Options.UseFont = True
        Me.LabelControl8.Location = New System.Drawing.Point(366, 44)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(84, 14)
        Me.LabelControl8.TabIndex = 18
        Me.LabelControl8.Text = "Select Location"
        '
        'CheckEdit2
        '
        Me.CheckEdit2.EditValue = True
        Me.CheckEdit2.Location = New System.Drawing.Point(247, 115)
        Me.CheckEdit2.Name = "CheckEdit2"
        Me.CheckEdit2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEdit2.Properties.Appearance.Options.UseFont = True
        Me.CheckEdit2.Properties.Caption = "Inactive Employee"
        Me.CheckEdit2.Size = New System.Drawing.Size(155, 19)
        Me.CheckEdit2.TabIndex = 8
        '
        'PopupContainerEditLocation
        '
        Me.PopupContainerEditLocation.Location = New System.Drawing.Point(501, 41)
        Me.PopupContainerEditLocation.Name = "PopupContainerEditLocation"
        Me.PopupContainerEditLocation.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditLocation.Properties.Appearance.Options.UseFont = True
        Me.PopupContainerEditLocation.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditLocation.Properties.AppearanceDropDown.Options.UseFont = True
        Me.PopupContainerEditLocation.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditLocation.Properties.AppearanceFocused.Options.UseFont = True
        Me.PopupContainerEditLocation.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.PopupContainerEditLocation.Properties.PopupControl = Me.PopupContainerControlBranch
        Me.PopupContainerEditLocation.Size = New System.Drawing.Size(225, 20)
        Me.PopupContainerEditLocation.TabIndex = 17
        Me.PopupContainerEditLocation.ToolTip = "Leave blank if want for all Employees"
        '
        'PopupContainerEditComp
        '
        Me.PopupContainerEditComp.Location = New System.Drawing.Point(113, 42)
        Me.PopupContainerEditComp.Name = "PopupContainerEditComp"
        Me.PopupContainerEditComp.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditComp.Properties.Appearance.Options.UseFont = True
        Me.PopupContainerEditComp.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditComp.Properties.AppearanceDropDown.Options.UseFont = True
        Me.PopupContainerEditComp.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditComp.Properties.AppearanceFocused.Options.UseFont = True
        Me.PopupContainerEditComp.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.PopupContainerEditComp.Properties.PopupControl = Me.PopupContainerControlComp
        Me.PopupContainerEditComp.Size = New System.Drawing.Size(225, 20)
        Me.PopupContainerEditComp.TabIndex = 9
        Me.PopupContainerEditComp.ToolTip = "Leave blank if want for all Employees"
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl4.Appearance.Options.UseFont = True
        Me.LabelControl4.Location = New System.Drawing.Point(16, 44)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(88, 14)
        Me.LabelControl4.TabIndex = 10
        Me.LabelControl4.Text = "Select Company"
        '
        'PopupContainerEditDept
        '
        Me.PopupContainerEditDept.Location = New System.Drawing.Point(501, 15)
        Me.PopupContainerEditDept.Name = "PopupContainerEditDept"
        Me.PopupContainerEditDept.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditDept.Properties.Appearance.Options.UseFont = True
        Me.PopupContainerEditDept.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditDept.Properties.AppearanceDropDown.Options.UseFont = True
        Me.PopupContainerEditDept.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditDept.Properties.AppearanceFocused.Options.UseFont = True
        Me.PopupContainerEditDept.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.PopupContainerEditDept.Properties.PopupControl = Me.PopupContainerControlDept
        Me.PopupContainerEditDept.Size = New System.Drawing.Size(225, 20)
        Me.PopupContainerEditDept.TabIndex = 11
        Me.PopupContainerEditDept.ToolTip = "Leave blank if want for all Employees"
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl6.Appearance.Options.UseFont = True
        Me.LabelControl6.Location = New System.Drawing.Point(16, 68)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(86, 14)
        Me.LabelControl6.TabIndex = 14
        Me.LabelControl6.Text = "Select Catagory"
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl5.Appearance.Options.UseFont = True
        Me.LabelControl5.Location = New System.Drawing.Point(366, 18)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(104, 14)
        Me.LabelControl5.TabIndex = 12
        Me.LabelControl5.Text = "Select Department"
        '
        'PopupContainerEditCat
        '
        Me.PopupContainerEditCat.Location = New System.Drawing.Point(113, 66)
        Me.PopupContainerEditCat.Name = "PopupContainerEditCat"
        Me.PopupContainerEditCat.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditCat.Properties.Appearance.Options.UseFont = True
        Me.PopupContainerEditCat.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditCat.Properties.AppearanceDropDown.Options.UseFont = True
        Me.PopupContainerEditCat.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditCat.Properties.AppearanceFocused.Options.UseFont = True
        Me.PopupContainerEditCat.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.PopupContainerEditCat.Properties.PopupControl = Me.PopupContainerControlCat
        Me.PopupContainerEditCat.Size = New System.Drawing.Size(225, 20)
        Me.PopupContainerEditCat.TabIndex = 13
        Me.PopupContainerEditCat.ToolTip = "Leave blank if want for all Employees"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButton1.Appearance.Options.UseFont = True
        Me.SimpleButton1.Location = New System.Drawing.Point(520, 15)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton1.TabIndex = 4
        Me.SimpleButton1.Text = "Generate"
        '
        'DateEditTo
        '
        Me.DateEditTo.EditValue = Nothing
        Me.DateEditTo.Location = New System.Drawing.Point(366, 16)
        Me.DateEditTo.Name = "DateEditTo"
        Me.DateEditTo.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.DateEditTo.Properties.Appearance.Options.UseFont = True
        Me.DateEditTo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEditTo.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEditTo.Properties.Mask.EditMask = "MM/yyyy"
        Me.DateEditTo.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.DateEditTo.Size = New System.Drawing.Size(100, 20)
        Me.DateEditTo.TabIndex = 3
        Me.DateEditTo.Visible = False
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl3.Appearance.Options.UseFont = True
        Me.LabelControl3.Location = New System.Drawing.Point(282, 19)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(45, 14)
        Me.LabelControl3.TabIndex = 2
        Me.LabelControl3.Text = "To Date"
        Me.LabelControl3.Visible = False
        '
        'DateEditFrom
        '
        Me.DateEditFrom.EditValue = Nothing
        Me.DateEditFrom.Location = New System.Drawing.Point(94, 16)
        Me.DateEditFrom.Name = "DateEditFrom"
        Me.DateEditFrom.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.DateEditFrom.Properties.Appearance.Options.UseFont = True
        Me.DateEditFrom.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEditFrom.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEditFrom.Properties.Mask.EditMask = "MM/yyyy"
        Me.DateEditFrom.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.DateEditFrom.Size = New System.Drawing.Size(100, 20)
        Me.DateEditFrom.TabIndex = 1
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl2.Appearance.Options.UseFont = True
        Me.LabelControl2.Location = New System.Drawing.Point(9, 19)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(57, 14)
        Me.LabelControl2.TabIndex = 0
        Me.LabelControl2.Text = "From Date"
        '
        'MemoEdit1
        '
        Me.MemoEdit1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.MemoEdit1.EditValue = resources.GetString("MemoEdit1.EditValue")
        Me.MemoEdit1.Location = New System.Drawing.Point(0, 0)
        Me.MemoEdit1.Name = "MemoEdit1"
        Me.MemoEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.MemoEdit1.Properties.Appearance.Options.UseFont = True
        Me.MemoEdit1.Properties.MaxLength = 100000
        Me.MemoEdit1.Properties.ReadOnly = True
        Me.MemoEdit1.Size = New System.Drawing.Size(101, 568)
        Me.MemoEdit1.TabIndex = 3
        '
        'TblDepartmentTableAdapter
        '
        Me.TblDepartmentTableAdapter.ClearBeforeFill = True
        '
        'TblEmployee1TableAdapter1
        '
        Me.TblEmployee1TableAdapter1.ClearBeforeFill = True
        '
        'TblEmployeeTableAdapter
        '
        Me.TblEmployeeTableAdapter.ClearBeforeFill = True
        '
        'TblCompany1TableAdapter1
        '
        Me.TblCompany1TableAdapter1.ClearBeforeFill = True
        '
        'TblDepartment1TableAdapter1
        '
        Me.TblDepartment1TableAdapter1.ClearBeforeFill = True
        '
        'TblCatagoryTableAdapter
        '
        Me.TblCatagoryTableAdapter.ClearBeforeFill = True
        '
        'TblGradeTableAdapter
        '
        Me.TblGradeTableAdapter.ClearBeforeFill = True
        '
        'TblCatagory1TableAdapter1
        '
        Me.TblCatagory1TableAdapter1.ClearBeforeFill = True
        '
        'TblCompanyTableAdapter
        '
        Me.TblCompanyTableAdapter.ClearBeforeFill = True
        '
        'TblGrade1TableAdapter1
        '
        Me.TblGrade1TableAdapter1.ClearBeforeFill = True
        '
        'Tblbranch1TableAdapter1
        '
        Me.Tblbranch1TableAdapter1.ClearBeforeFill = True
        '
        'TblbranchTableAdapter
        '
        Me.TblbranchTableAdapter.ClearBeforeFill = True
        '
        'XtraReportsPay
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.SplitContainerControl1)
        Me.LookAndFeel.SkinName = "iMaginary"
        Me.LookAndFeel.UseDefaultLookAndFeel = False
        Me.Name = "XtraReportsPay"
        Me.Size = New System.Drawing.Size(1145, 568)
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerControl1.ResumeLayout(False)
        Me.SidePanel3.ResumeLayout(False)
        CType(Me.PopupContainerControlEmp, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PopupContainerControlEmp.ResumeLayout(False)
        CType(Me.GridControlEmp, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblEmployeeBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewEmp, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTimeEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerControlDept, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PopupContainerControlDept.ResumeLayout(False)
        CType(Me.GridControlDept, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblDepartmentBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewDept, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTimeEdit3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerControlComp, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PopupContainerControlComp.ResumeLayout(False)
        CType(Me.GridControlComp, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblCompanyBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewComp, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTimeEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SidePanel2.ResumeLayout(False)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        CType(Me.CheckEditLEaveEncash.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditPendingReEm.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditCashAdj.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEdidFullNFinal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditSummaryStat.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditReesPyReg.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditReemSt.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditPieceM.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditPieceEmp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditArrearSlip.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditArrearReg.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditESISt.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditESICh.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditPfSt.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditPfCh.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditLoanEntry.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditLoanEntryDetails.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckSalaryNonBank.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditAdvEntry.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckSalaryBank.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditAdvEntryDetails.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckSalaryCash.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckSalaryCheque.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckSalaryRegister.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckSalarySlip.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditEmpWiseCtc.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerControlBranch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PopupContainerControlBranch.ResumeLayout(False)
        CType(Me.GridControlBranch, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewBranch, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTimeEdit7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit2.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerControlGrade, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PopupContainerControlGrade.ResumeLayout(False)
        CType(Me.GridControlGrade, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblGradeBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewGrade, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTimeEdit6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerControlCat, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PopupContainerControlCat.ResumeLayout(False)
        CType(Me.GridControlCat, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblCatagoryBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewCat, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTimeEdit4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEdit3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SidePanel1.ResumeLayout(False)
        Me.SidePanel1.PerformLayout()
        CType(Me.ComboNepaliYearTo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboNEpaliMonthTo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboNepaliYearFrm.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboNEpaliMonthFrm.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckPDF.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckExcel.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckText.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SidePanelSelection.ResumeLayout(False)
        Me.SidePanelSelection.PerformLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerEditEmp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerEditGrade.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerEditLocation.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerEditComp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerEditDept.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerEditCat.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEditTo.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEditTo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEditFrom.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEditFrom.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SplitContainerControl1 As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents SidePanel3 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents PopupContainerControlEmp As DevExpress.XtraEditors.PopupContainerControl
    Friend WithEvents GridControlEmp As DevExpress.XtraGrid.GridControl
    Friend WithEvents TblEmployeeBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SSSDBDataSet As iAS.SSSDBDataSet
    Friend WithEvents GridViewEmp As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colPAYCODE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colEMPNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemTimeEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit
    Friend WithEvents PopupContainerControlDept As DevExpress.XtraEditors.PopupContainerControl
    Friend WithEvents GridControlDept As DevExpress.XtraGrid.GridControl
    Friend WithEvents TblDepartmentBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents GridViewDept As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colDEPARTMENTCODE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDEPARTMENTNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemTimeEdit3 As DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit
    Friend WithEvents PopupContainerControlComp As DevExpress.XtraEditors.PopupContainerControl
    Friend WithEvents GridControlComp As DevExpress.XtraGrid.GridControl
    Friend WithEvents TblCompanyBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents GridViewComp As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colCOMPANYCODE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCOMPANYNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemTimeEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit
    Friend WithEvents SidePanel2 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents PopupContainerControlGrade As DevExpress.XtraEditors.PopupContainerControl
    Friend WithEvents GridControlGrade As DevExpress.XtraGrid.GridControl
    Friend WithEvents TblGradeBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents GridViewGrade As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemTimeEdit6 As DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit
    Friend WithEvents PopupContainerControlCat As DevExpress.XtraEditors.PopupContainerControl
    Friend WithEvents GridControlCat As DevExpress.XtraGrid.GridControl
    Friend WithEvents TblCatagoryBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents GridViewCat As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colCAT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCATAGORYNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemTimeEdit4 As DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit
    Friend WithEvents SidePanel1 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PopupContainerEditGrade As DevExpress.XtraEditors.PopupContainerEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PopupContainerEditLocation As DevExpress.XtraEditors.PopupContainerEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PopupContainerEditCat As DevExpress.XtraEditors.PopupContainerEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PopupContainerEditDept As DevExpress.XtraEditors.PopupContainerEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PopupContainerEditComp As DevExpress.XtraEditors.PopupContainerEdit
    Friend WithEvents CheckEdit2 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit1 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents PopupContainerEditEmp As DevExpress.XtraEditors.PopupContainerEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents DateEditTo As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents DateEditFrom As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents MemoEdit1 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents TblDepartmentTableAdapter As iAS.SSSDBDataSetTableAdapters.tblDepartmentTableAdapter
    Friend WithEvents TblEmployee1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.TblEmployee1TableAdapter
    Friend WithEvents TblEmployeeTableAdapter As iAS.SSSDBDataSetTableAdapters.TblEmployeeTableAdapter
    Friend WithEvents TblCompany1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.tblCompany1TableAdapter
    Friend WithEvents TblDepartment1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.tblDepartment1TableAdapter
    Friend WithEvents TblCatagoryTableAdapter As iAS.SSSDBDataSetTableAdapters.tblCatagoryTableAdapter
    Friend WithEvents TblGradeTableAdapter As iAS.SSSDBDataSetTableAdapters.tblGradeTableAdapter
    Friend WithEvents TblCatagory1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.tblCatagory1TableAdapter
    Friend WithEvents TblCompanyTableAdapter As iAS.SSSDBDataSetTableAdapters.tblCompanyTableAdapter
    Friend WithEvents TblGrade1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.tblGrade1TableAdapter
    Friend WithEvents SidePanelSelection As DevExpress.XtraEditors.SidePanel
    Friend WithEvents CheckExcel As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckText As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckSalaryRegister As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents PopupContainerControlBranch As DevExpress.XtraEditors.PopupContainerControl
    Friend WithEvents GridControlBranch As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridViewBranch As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colBRANCHCODE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBRANCHNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemTimeEdit7 As DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit
    Friend WithEvents RepositoryItemDateEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents Tblbranch1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.tblbranch1TableAdapter
    Friend WithEvents TblbranchTableAdapter As iAS.SSSDBDataSetTableAdapters.tblbranchTableAdapter
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents CheckPDF As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CheckSalarySlip As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit3 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditEmpWiseCtc As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditLoanEntry As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditLoanEntryDetails As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditAdvEntryDetails As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditAdvEntry As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents ComboNepaliYearTo As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboNEpaliMonthTo As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboNepaliYearFrm As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboNEpaliMonthFrm As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents CheckSalaryNonBank As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckSalaryBank As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckSalaryCash As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckSalaryCheque As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditPfSt As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditPfCh As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditESISt As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditESICh As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditArrearSlip As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditArrearReg As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditPieceM As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditPieceEmp As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditReesPyReg As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditReemSt As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditSummaryStat As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdidFullNFinal As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditCashAdj As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditPendingReEm As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditLEaveEncash As DevExpress.XtraEditors.CheckEdit

End Class
