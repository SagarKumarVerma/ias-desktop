﻿Imports System.Resources
Imports System.Globalization
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports System.ComponentModel
Imports DevExpress.XtraGrid.Columns
Imports System.Data.SqlClient
Imports System.Text
Imports System.Data.OleDb

Public Class XtraPayArrear
    Dim ulf As UserLookAndFeel
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    Dim leaveFlage As Boolean = True
    Dim bProcess As Boolean, bCapture As Boolean
    Public Sub New()
        InitializeComponent()
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True

        If Common.servername = "Access" Then
            Me.TblEmployee1TableAdapter1.Fill(Me.SSSDBDataSet.TblEmployee1)
        Else
            TblEmployeeTableAdapter.Connection.ConnectionString = Common.ConnectionString
            'Me.TblEmployeeTableAdapter.Fill(Me.SSSDBDataSet.TblEmployee)
        End If
    End Sub
    Private Sub XtraPayMaintenance_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Me.Width = Common.NavWidth 'Me.Parent.Width
        Me.Height = Common.NavHeight 'Me.Parent.Height
        'SplitContainerControl1.Width = Common.splitforMasterMenuWidth 'SplitContainerControl1.Parent.Width
        SplitContainerControl1.SplitterPosition = Common.SplitterPosition '(SplitContainerControl1.Parent.Width) * 85 / 100

        'If Common.servername = "Access" Then
        '    Me.TblEmployee1TableAdapter1.Fill(Me.SSSDBDataSet.TblEmployee1)
        '    LookUpEdit1.Properties.DataSource = SSSDBDataSet.TblEmployee1
        'Else
        '    TblEmployeeTableAdapter.Connection.ConnectionString = Common.ConnectionString
        '    Me.TblEmployeeTableAdapter.Fill(Me.SSSDBDataSet.TblEmployee)
        '    LookUpEdit1.Properties.DataSource = SSSDBDataSet.TblEmployee
        'End If
        LookUpEdit1.Properties.DataSource = Common.EmpNonAdmin
        setDefault()
    End Sub
    Private Sub LookUpEdit1_Leave(sender As System.Object, e As System.EventArgs) Handles LookUpEdit1.Leave
        If Not leaveFlage Then Return
        leaveFlage = False
        Dim adap, adap1 As SqlDataAdapter
        Dim adapA, adapA1 As OleDbDataAdapter
        Dim ds As DataSet = New DataSet
        Dim ds1 As DataSet
        Dim sSql As String = "select PAYCODE from TblEmployee where PAYCODE = '" & LookUpEdit1.EditValue.ToString.Trim & "'"
        If LookUpEdit1.EditValue.ToString.Trim <> "" Then
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
            If ds.Tables(0).Rows.Count = 0 Then
                If LookUpEdit1.EditValue.ToString.Trim <> "" Then
                    XtraMessageBox.Show(ulf, "<size=10>No such Employee</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    LookUpEdit1.Select()
                    setDefault()
                    leaveFlage = True
                    Exit Sub
                End If
            Else
                Dim sSql1 As String = "select EMPNAME, PRESENTCARDNO, (select GroupName from EmployeeGroup where EmployeeGroup.GroupId = TblEmployee.EmployeeGroupId) as EmpGrp, (select DEPARTMENTNAME from tblDepartment where tblDepartment.DEPARTMENTCODE = TblEmployee.DEPARTMENTCODE) as DEPName, (select CATAGORYNAME from tblCatagory WHERE tblCatagory.CAT = TblEmployee.CAT) as CATName, DESIGNATION  from TblEmployee where PAYCODE = '" & LookUpEdit1.EditValue.ToString.Trim & "'"
                sSql1 = "select EMPNAME, PRESENTCARDNO, " & _
                       "(SELECT COMPANYNAME from tblCompany WHERE tblCompany.COMPANYCODE = TblEmployee.COMPANYCODE) as CompName, " & _
                       "(select GroupName from EmployeeGroup where EmployeeGroup.GroupId = TblEmployee.EmployeeGroupId) as EmpGrp," & _
                       "(select DEPARTMENTNAME from tblDepartment where tblDepartment.DEPARTMENTCODE = TblEmployee.DEPARTMENTCODE) as DEPName, " & _
                       "(select CATAGORYNAME from tblCatagory WHERE tblCatagory.CAT = TblEmployee.CAT) as CATName, " & _
                       "(select GradeName from tblGrade WHERE tblGrade.GradeCode = TblEmployee.GradeCode) as GrdName, DESIGNATION  from TblEmployee where PAYCODE = '" & LookUpEdit1.EditValue.ToString.Trim & "'"
                If Common.servername = "Access" Then
                    sSql1 = "select T.EMPNAME, T.PRESENTCARDNO, C.COMPANYNAME  as CompName,  T.DESIGNATION, E.GroupName as EmpGrp, D.DEPARTMENTNAME as DEPName, cat.CATAGORYNAME as CATName, grd.GradeName as GrdName  from TblEmployee T,  tblCompany C, EmployeeGroup E, tblDepartment D, tblCatagory cat, tblGrade grd where C.COMPANYCODE = T.COMPANYCODE  and E.GroupId = T.EmployeeGroupId and D.DEPARTMENTCODE = T.DEPARTMENTCODE and cat.CAT = T.CAT and grd.GradeCode = T.GradeCode and T.Paycode='" & LookUpEdit1.EditValue.ToString.Trim & "'"
                    adapA1 = New OleDbDataAdapter(sSql1, Common.con1)
                    ds1 = New DataSet
                    adapA1.Fill(ds1)
                Else
                    adap1 = New SqlDataAdapter(sSql1, Common.con)
                    ds1 = New DataSet
                    adap1.Fill(ds1)
                End If
                lblName.Text = ds1.Tables(0).Rows(0).Item("EMPNAME").ToString
                lblCardNum.Text = ds1.Tables(0).Rows(0).Item("PRESENTCARDNO").ToString
                lblDept.Text = ds1.Tables(0).Rows(0).Item("DEPName").ToString
            End If
        End If
        leaveFlage = True

        'setValues()
    End Sub
    Private Sub setDefault()
        LookUpEdit1.EditValue = ""
        lblName.Text = ""
        lblCardNum.Text = ""
        lblDept.Text = ""

        Dim rsPaysetup As DataSet = New DataSet
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        txtPayableMonth.EditValue = Now().AddDays(-Now.Day + 1)
        txtapplicablefrom.EditValue = txtPayableMonth.DateTime.AddMonths(-1) ' "01/" & Format(DateAdd("M", -1, Now()), "MM/yyyy")
        txtapplicableto.EditValue = txtapplicablefrom.DateTime.AddMonths(1).AddDays(-1) 'DateAdd("d", -1, DateAdd("m", 1, txtapplicablefrom.Value))

        optPayable1.Checked= True
        mskPaidDays.Visible = False
        Label9.Visible = False
        'cmdOk.Enabled = False
        'cmdAccept.Enabled = True
        Dim sSql As String = "SELECT * FROM PAY_SETUP"
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(rsPaysetup)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(rsPaysetup)
        End If
        'rsPaysetup = Cn.Execute(sSql)
        lbl1.Text = IIf((rsPaysetup.Tables(0).Rows(0)("VIES_1").ToString.Trim) = "", "", rsPaysetup.Tables(0).Rows(0)("VIES_1").ToString.Trim)
        lbl2.Text = IIf((rsPaysetup.Tables(0).Rows(0)("VIES_2").ToString.Trim) = "", "", rsPaysetup.Tables(0).Rows(0)("VIES_2").ToString.Trim)
        lbl3.Text = IIf((rsPaysetup.Tables(0).Rows(0)("VIES_3").ToString.Trim) = "", "", rsPaysetup.Tables(0).Rows(0)("VIES_3").ToString.Trim)
        lbl4.Text = IIf((rsPaysetup.Tables(0).Rows(0)("VIES_4").ToString.Trim) = "", "", rsPaysetup.Tables(0).Rows(0)("VIES_4").ToString.Trim)
        lbl5.Text = IIf((rsPaysetup.Tables(0).Rows(0)("VIES_5").ToString.Trim) = "", "", rsPaysetup.Tables(0).Rows(0)("VIES_5").ToString.Trim)
        lbl6.Text = IIf((rsPaysetup.Tables(0).Rows(0)("VIES_6").ToString.Trim) = "", "", rsPaysetup.Tables(0).Rows(0)("VIES_6").ToString.Trim)
        lbl7.Text = IIf((rsPaysetup.Tables(0).Rows(0)("VIES_7").ToString.Trim) = "", "", rsPaysetup.Tables(0).Rows(0)("VIES_7").ToString.Trim)
        lbl8.Text = IIf((rsPaysetup.Tables(0).Rows(0)("VIES_8").ToString.Trim) = "", "", rsPaysetup.Tables(0).Rows(0)("VIES_8").ToString.Trim)
        lbl9.Text = IIf((rsPaysetup.Tables(0).Rows(0)("VIES_9").ToString.Trim) = "", "", rsPaysetup.Tables(0).Rows(0)("VIES_9").ToString.Trim)
        lbl10.Text = IIf((rsPaysetup.Tables(0).Rows(0)("VIES_10").ToString.Trim) = "", "", rsPaysetup.Tables(0).Rows(0)("VIES_10").ToString.Trim)
        If Trim(lbl1.Text) = "" Then
            txtEarn1.Visible = False
            txtEarningEarn1.Visible = False
        End If
        If Trim(lbl2.Text) = "" Then
            txtEarn2.Visible = False
            txtEarningEarn2.Visible = False
        End If
        If Trim(lbl3.Text) = "" Then
            txtEarn3.Visible = False
            txtEarningEarn3.Visible = False
        End If
        If Trim(lbl4.Text) = "" Then
            txtEarn4.Visible = False
            txtEarningEarn4.Visible = False
        End If
        If Trim(lbl5.Text) = "" Then
            txtEarn5.Visible = False
            txtEarningEarn5.Visible = False
        End If
        If Trim(lbl6.Text) = "" Then
            txtEarn6.Visible = False
            txtEarningEarn6.Visible = False
        End If
        If Trim(lbl7.Text) = "" Then
            txtEarn7.Visible = False
            txtEarningEarn7.Visible = False
        End If
        If Trim(lbl8.Text) = "" Then
            txtEarn8.Visible = False
            txtEarningEarn8.Visible = False
        End If
        If Trim(lbl9.Text) = "" Then
            txtEarn9.Visible = False
            txtEarningEarn9.Visible = False
        End If
        If Trim(lbl10.Text) = "" Then
            txtEarn10.Visible = False
            txtEarningEarn10.Visible = False
        End If

        ClearTextBox(PanelControl1)
        mskPaidDays.Text = "0"
        'txtPayableMonth.DateTime = Now
        If Common.IsNepali = "Y" Then
            ComboNepaliYear.Visible = True
            ComboNEpaliMonth.Visible = True
            txtPayableMonth.Visible = False
            Dim DC As New DateConverter()
            Dim doj As String = DC.ToBS(New Date(txtPayableMonth.DateTime.Year, txtPayableMonth.DateTime.Month, 1))
            Dim dojTmp() As String = doj.Split("-")
            ComboNepaliYear.EditValue = dojTmp(0)
            ComboNEpaliMonth.SelectedIndex = dojTmp(1) - 1

        Else
            ComboNepaliYear.Visible = False
            ComboNEpaliMonth.Visible = False
            txtPayableMonth.Visible = True
        End If
    End Sub
    Public Sub ClearTextBox(ByVal root As Control)
        For Each ctrl As Control In root.Controls
            ClearTextBox(ctrl)
            If TypeOf ctrl Is TextEdit Then
                CType(ctrl, TextEdit).Text = "0"
            End If
        Next ctrl
    End Sub
    'Private Sub setValues()
    '    Dim sSql As String
    '    Dim rsTR As DataSet = New DataSet 'ADODB.Recordset
    '    Dim iLate As String
    '    Dim iEarly As String
    '    Dim iOT As String

    '    If Common.IsNepali = "Y" Then
    '        Dim DC As New DateConverter()
    '        Try
    '            'DateEditFrom.DateTime = DC.ToAD(New Date(ComboNepaliYear.EditValue, ComboNEpaliMonth.SelectedIndex + 1, 1))
    '            DateEditPay.DateTime = DC.ToAD(ComboNepaliYear.EditValue & "-" & ComboNEpaliMonth.SelectedIndex + 1 & "-" & 1)
    '        Catch ex As Exception
    '            XtraMessageBox.Show(ulf, "<size=10>Invalid Date Of Joining</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
    '            ComboNEpaliMonth.Select()
    '            Exit Sub
    '        End Try
    '    End If

    '    If Common.servername = "Access" Then
    '        sSql = "Select * From Pay_result" & " Where Paycode='" & LookUpEdit1.EditValue.ToString.Trim & "' And FORMAT(Mon_Year,'MM')=" & DateEditPay.DateTime.ToString("MM") & " And FORMAT(Mon_Year,'YYYY')=" & DateEditPay.DateTime.ToString("yyyy")
    '    Else
    '        sSql = "Select * From Pay_result" & " Where Paycode='" & LookUpEdit1.EditValue.ToString.Trim & "' And DatePart(mm,Mon_Year)=" & DateEditPay.DateTime.ToString("MM") & " And DatePart(yy,Mon_Year)=" & DateEditPay.DateTime.ToString("yyyy")
    '    End If
    '    Dim adap As SqlDataAdapter
    '    Dim adapA As OleDbDataAdapter
    '    If Common.servername = "Access" Then
    '        adapA = New OleDbDataAdapter(sSql, Common.con1)
    '        adapA.Fill(rsTR)
    '    Else
    '        adap = New SqlDataAdapter(sSql, Common.con)
    '        adap.Fill(rsTR)
    '    End If
    '    'rsTR = Cn.Execute(sSql)
    '    If rsTR.Tables(0).Rows.Count = 0 Then
    '        XtraMessageBox.Show(ulf, "<size=10>No attendance data is available for this employee for the specified period.</size>", "<size=9>Error</size>")
    '        setDefault()
    '        LookUpEdit1.Select()
    '        Exit Sub
    '    Else
    '        If rsTR.Tables(0).Rows(0).Item("VPRE").ToString.Trim = "" Then : txtDaysWorked.Text = "00.00" : Else : txtDaysWorked.Text = rsTR.Tables(0).Rows(0).Item("VPRE").ToString.Trim : End If
    '        'txtAbsent.Text = IIf(IsNull(rsTR("VABS")), "00.00", Format(rsTR("VABS"), "00.00"))
    '        If rsTR.Tables(0).Rows(0).Item("VABS").ToString.Trim = "" Then : txtAbsent.Text = "00.00" : Else : txtAbsent.Text = rsTR.Tables(0).Rows(0).Item("VABS").ToString.Trim : End If
    '        'iOT = Min2Hr(rsTR("VOT"))
    '        iOT = rsTR.Tables(0).Rows(0).Item("VOT").ToString.Trim
    '        'txtOT.Text = IIf(IsNull(iOT), "000.00", Format(iOT, "000.00"))
    '        If iOT = "" Then : txtOT.Text = "000.00" : Else : txtOT.Text = iOT : End If

    '        iLate = (rsTR.Tables(0).Rows(0).Item("VT_LATE") / 60) 'Min2Hr(rsTR("VT_LATE"))
    '        'txtLate.Text = IIf(IsNull(iLate), "000.00", Format(iLate, "000.00"))
    '        If iLate = "" Then : txtLate.Text = "000.00" : Else : txtLate.Text = iLate : End If

    '        iEarly = (rsTR.Tables(0).Rows(0).Item("VT_EARLY") / 60) ' Min2Hr(rsTR("VT_EARLY"))
    '        'txtEarly.Text = IIf(IsNull(iEarly), "000.00", Format(iEarly, "000.00"))
    '        If iEarly = "" Then : txtEarly.Text = "000.00" : Else : txtEarly.Text = iEarly : End If

    '        'txtCL.Text = IIf(IsNull(rsTR("VCL")), "00.00", Format(rsTR("VCL"), "00.00"))
    '        If rsTR.Tables(0).Rows(0).Item("VCL").ToString.Trim = "" Then : txtCL.Text = "00.00" : Else : txtCL.Text = rsTR.Tables(0).Rows(0).Item("VCL").ToString.Trim : End If

    '        'txtSL.Text = IIf(IsNull(rsTR("VSL")), "00.00", Format(rsTR("VSL"), "00.00"))
    '        If rsTR.Tables(0).Rows(0).Item("VSL").ToString.Trim = "" Then : txtSL.Text = "00.00" : Else : txtSL.Text = rsTR.Tables(0).Rows(0).Item("VSL").ToString.Trim : End If

    '        'txtPLEL.Text = IIf(IsNull(rsTR("VPL_EL")), "00.00", Format(rsTR("VPL_EL"), "00.00"))
    '        If rsTR.Tables(0).Rows(0).Item("VPL_EL").ToString.Trim = "" Then : txtPLEL.Text = "00.00" : Else : txtPLEL.Text = rsTR.Tables(0).Rows(0).Item("VPL_EL").ToString.Trim : End If

    '        'txtWO.Text = IIf(IsNull(rsTR("VWO")), "00.00", Format(rsTR("VWO"), "00.00"))
    '        If rsTR.Tables(0).Rows(0).Item("VWO").ToString.Trim = "" Then : txtWO.Text = "00.00" : Else : txtWO.Text = rsTR.Tables(0).Rows(0).Item("VWO").ToString.Trim : End If

    '        'txtLateDays.Text = IIf(IsNull(rsTR("VLATE")), "00.00", Format(rsTR("VLATE"), "00.00"))
    '        If rsTR.Tables(0).Rows(0).Item("VLATE").ToString.Trim = "" Then : txtLateDays.Text = "00.00" : Else : txtLateDays.Text = rsTR.Tables(0).Rows(0).Item("VLATE").ToString.Trim : End If

    '        'txtEarlyDays.Text = IIf(IsNull(rsTR("VEARLY")), "00.00", Format(rsTR("VEARLY"), "00.00"))
    '        If rsTR.Tables(0).Rows(0).Item("VEARLY").ToString.Trim = "" Then : txtEarlyDays.Text = "00.00" : Else : txtEarlyDays.Text = rsTR.Tables(0).Rows(0).Item("VEARLY").ToString.Trim : End If

    '        'txtOtherLeave.Text = IIf(IsNull(rsTR("VOTHER_LV")), "00.00", Format(rsTR("VOTHER_LV"), "00.00"))
    '        If rsTR.Tables(0).Rows(0).Item("VOTHER_LV").ToString.Trim = "" Then : txtOtherLeave.Text = "00.00" : Else : txtOtherLeave.Text = rsTR.Tables(0).Rows(0).Item("VOTHER_LV").ToString.Trim : End If

    '        'txtHolidays.Text = IIf(IsNull(rsTR("VHLD")), "00.00", Format(rsTR("VHLD"), "00.00"))
    '        If rsTR.Tables(0).Rows(0).Item("VHLD").ToString.Trim = "" Then : txtHolidays.Text = "00.00" : Else : txtHolidays.Text = rsTR.Tables(0).Rows(0).Item("VHLD").ToString.Trim : End If

    '    End If
    'End Sub

    'Private Sub SimpleButtonSave_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonSave.Click
    '    Dim sSql As String
    '    'Dim i As Integer
    '    Try ' On Error GoTo AKHIL
    '        If Common.IsNepali = "Y" Then
    '            Dim DC As New DateConverter()
    '            Try
    '                'DateEditFrom.DateTime = DC.ToAD(New Date(ComboNepaliYear.EditValue, ComboNEpaliMonth.SelectedIndex + 1, 1))
    '                DateEditPay.DateTime = DC.ToAD(ComboNepaliYear.EditValue & "-" & ComboNEpaliMonth.SelectedIndex + 1 & "-" & 1)
    '            Catch ex As Exception
    '                XtraMessageBox.Show(ulf, "<size=10>Invalid Date Of Joining</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
    '                ComboNEpaliMonth.Select()
    '                Exit Sub
    '            End Try
    '        End If

    '        If Val(txtDaysWorked.Text) + Val(txtAbsent.Text) + Val(txtCL.Text) + Val(txtSL.Text) + Val(txtPLEL.Text) + Val(txtOtherLeave.Text) + Val(txtHolidays.Text) + Val(txtWO.Text) > Date.DaysInMonth(DateEditPay.DateTime.Year, DateEditPay.DateTime.Month) Then
    '            XtraMessageBox.Show(ulf, "<size=10>Total days cannot be greater than the Month Days. Please check</size>", "<size=9>Total days error</size>")
    '            txtDaysWorked.Select()
    '            Exit Sub
    '        End If
    '        sSql = "select * from PAY_RESULT where PAYCODE = '" & LookUpEdit1.EditValue.ToString.Trim & "' And MONTH(Mon_Year)=" & DateEditPay.DateTime.ToString("MM") & " And YEAR(Mon_Year)=" & DateEditPay.DateTime.ToString("yyyy")
    '        Dim adap As SqlDataAdapter
    '        Dim adapA As OleDbDataAdapter
    '        'rsChkdata = Cn.Execute(sSql)
    '        Dim ds As DataSet = New DataSet
    '        If Common.servername = "Access" Then
    '            adapA = New OleDbDataAdapter(sSql, Common.con1)
    '            adapA.Fill(ds)
    '        Else
    '            adap = New SqlDataAdapter(sSql, Common.con)
    '            adap.Fill(ds)
    '        End If
    '        If ds.Tables(0).Rows.Count = 0 Then
    '            If Common.IsNepali = "Y" Then
    '                XtraMessageBox.Show(ulf, "<size=10>Data not update becouse data not capture on " & ComboNEpaliMonth.EditValue.ToString.Trim & " " & ComboNepaliYear.EditValue.ToString.Trim & "</size>", "<size=9>Error</size>")
    '            Else
    '                XtraMessageBox.Show(ulf, "<size=10>Data not update becouse data not capture on " & DateEditPay.DateTime.ToString("MMM yyyy") & "</size>", "<size=9>Error</size>")
    '            End If

    '            LookUpEdit1.Select()
    '            setDefault()
    '            Exit Sub
    '        End If
    '        'If MsgBox("Do you want to save changes", vbInformation + vbYesNo) = vbYes Then
    '        If XtraMessageBox.Show(ulf, "<size=10>Do you want to save changes?</size>", Common.res_man.GetString("confirmdeleteion", Common.cul), _
    '                      MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
    '            If Common.servername = "Access" Then
    '                sSql = "Update pay_result Set VPRE=" & Trim(txtDaysWorked.Text) & "," & _
    '                "VABS=" & Trim(txtAbsent.Text) & ",VOT=" & Trim(txtOT.Text) & "," & _
    '                "VT_LATE=" & Trim(txtLate.Text) & ",VT_EARLY=" & Trim(txtEarly.Text) & "," & _
    '                "VCL=" & Trim(txtCL.Text) & ",VSL=" & Trim(txtSL.Text) & ",VWO=" & Trim(txtWO.Text) & "," & _
    '                "VPL_EL=" & Trim(txtPLEL.Text) & ",VLATE=" & Trim(txtLateDays.Text) & "," & _
    '                "VEARLY=" & Trim(txtEarlyDays.Text) & ",VOTHER_LV=" & Trim(txtOtherLeave.Text) & "," & _
    '                "VHLD=" & Trim(txtHolidays.Text) & " Where Paycode='" & Trim(LookUpEdit1.EditValue.ToString.Trim) & "' And MONTH(Mon_Year)=" & DateEditPay.DateTime.ToString("MM") & " And YEAR(Mon_Year)=" & DateEditPay.DateTime.ToString("yyyy")
    '            Else
    '                sSql = "Update pay_result Set VPRE=" & Trim(txtDaysWorked.Text) & "," & _
    '                "VABS=" & Trim(txtAbsent.Text) & ",VOT=" & Trim(txtOT.Text) & "," & _
    '                "VT_LATE=" & Trim(txtLate.Text) & ",VT_EARLY=" & Trim(txtEarly.Text) & "," & _
    '                "VCL=" & Trim(txtCL.Text) & ",VSL=" & Trim(txtSL.Text) & ",VWO=" & Trim(txtWO.Text) & "," & _
    '                "VPL_EL=" & Trim(txtPLEL.Text) & ",VLATE=" & Trim(txtLateDays.Text) & "," & _
    '                "VEARLY=" & Trim(txtEarlyDays.Text) & ",VOTHER_LV=" & Trim(txtOtherLeave.Text) & "," & _
    '                "VHLD=" & Trim(txtHolidays.Text) & " Where Paycode='" & Trim(LookUpEdit1.EditValue.ToString.Trim) & "' And DatePart(mm,Mon_Year)=" & DateEditPay.DateTime.ToString("MM") & " And DatePart(yyyy,Mon_Year)=" & DateEditPay.DateTime.ToString("yyyy")


    '            End If
    '            'Cn.Execute (sSql), i
    '            If Common.servername = "Access" Then
    '                If Common.con1.State <> ConnectionState.Open Then
    '                    Common.con1.Open()
    '                End If
    '                cmd1 = New OleDbCommand(sSql, Common.con1)
    '                cmd1.ExecuteNonQuery()
    '                If Common.con1.State <> ConnectionState.Closed Then
    '                    Common.con1.Close()
    '                End If
    '            Else
    '                If Common.con.State <> ConnectionState.Open Then
    '                    Common.con.Open()
    '                End If
    '                cmd = New SqlCommand(sSql, Common.con)
    '                cmd.ExecuteNonQuery()
    '                If Common.con.State <> ConnectionState.Closed Then
    '                    Common.con.Close()
    '                End If
    '            End If
    '            XtraMessageBox.Show(ulf, "<size=10>Saved Successfully</size>", "<size=9>Success</size>")
    '            LookUpEdit1.Select()
    '            setDefault()
    '        End If
    '    Catch
    '        XtraMessageBox.Show(ulf, "<size=10> " & Err.Number & Chr(13) & Err.Description & "</size>", "<size=9>Error</size>")
    '    End Try

    'End Sub

    'Private Sub DateEditFrom_Leave(sender As System.Object, e As System.EventArgs) Handles DateEditPay.Leave
    '    setValues()
    'End Sub

    'Private Sub ComboNepaliYear_Leave(sender As System.Object, e As System.EventArgs) Handles ComboNepaliYear.Leave
    '    setValues()
    'End Sub
    Private Sub optPayable2_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles optPayable2.CheckedChanged
        If optPayable2.Checked = True Then
            Label9.Visible = True
            mskPaidDays.Visible = True
        Else
            Label9.Visible = False
            mskPaidDays.Visible = False
        End If
        
    End Sub

    Dim arrFDate As DateTime
    Dim arrTDate As DateTime
    Private Sub btnApply_Click(sender As System.Object, e As System.EventArgs) Handles btnApply.Click
        Dim esiamt As Double, mothers As Double

        'cmdOk.Enabled = True
        Dim rspaymaster As DataSet = New DataSet
        Dim rsrecordchk As DataSet = New DataSet
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim cmd As New SqlCommand
        Dim cmd1 As New OleDbCommand
        arrFDate = "01/" & Format(txtapplicablefrom.DateTime, "MM/yyyy")
        arrTDate = "01/" & Format(txtapplicableto.DateTime, "MM/yyyy")

        Dim sSql As String = "select paycode from pay_master where paycode='" & LookUpEdit1.EditValue & "'"
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(rspaymaster)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(rspaymaster)
        End If
        'rspaymaster = Cn.Execute(sSql)
        If rspaymaster.Tables(0).Rows.Count > 0 Then
            sSql = "select * from arrear where paycode='" & LookUpEdit1.EditValue & "' And DatePart(mm,for_month)=" & Format(txtapplicablefrom.DateTime, "MM") & " And DatePart(yyyy,for_month)=" & Format(txtapplicablefrom.DateTime, "yyyy")
            'rsrecordchk = Cn.Execute(sSql)
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(rsrecordchk)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(rsrecordchk)
            End If
            If rsrecordchk.Tables(0).Rows.Count > 0 Then
                sSql = "delete from arrear where paycode='" & LookUpEdit1.EditValue & "' And DatePart(mm,for_month)=" & Format(txtapplicablefrom.DateTime, "MM") & " And DatePart(yyyy,for_month)=" & Format(txtapplicablefrom.DateTime, "yyyy")
                'Cn.Execute(sSql)
                If Common.servername = "Access" Then
                    If Common.con1.State <> ConnectionState.Open Then
                        Common.con1.Open()
                    End If
                    cmd1 = New OleDbCommand(sSql, Common.con1)
                    cmd1.ExecuteNonQuery()
                    If Common.con1.State <> ConnectionState.Closed Then
                        Common.con1.Close()
                    End If
                Else
                    If Common.con.State <> ConnectionState.Open Then
                        Common.con.Open()
                    End If
                    cmd = New SqlCommand(sSql, Common.con)
                    cmd.ExecuteNonQuery()
                    If Common.con.State <> ConnectionState.Closed Then
                        Common.con.Close()
                    End If
                End If
            End If
            txtEarningBasic.Text = "0"
            txtEarningDA.Text = "0"
            txtEarningHRA.Text = "0"
            txtEarningConv.Text = "0"
            txtEarningMed.Text = "0"
            txtEarningEarn1.Text = "0"
            txtEarningEarn2.Text = "0"
            txtEarningEarn3.Text = "0"
            txtEarningEarn4.Text = "0"
            txtEarningEarn5.Text = "0"
            txtEarningEarn6.Text = "0"
            txtEarningEarn7.Text = "0"
            txtEarningEarn8.Text = "0"
            txtEarningEarn9.Text = "0"
            txtEarningEarn10.Text = "0"
            txtCalculatedPF.Text = "0"
            txtCalculatedVPF.Text = "0"
            txtESIAmount.Text = "0"
            txtcalculatedEpf.Text = "0"
            txtcalculatedFpf.Text = "0"
            txtAmtonPf.Text = "0"
            txtAmtonVPf.Text = "0"
            txtAmtonESI.Text = "0"
            TXTPAYdAYS.Text = "0"

            Do While CDate(arrFDate) <= CDate(arrTDate)
                sSql = "delete from arreartemp"
                If Common.servername = "Access" Then
                    If Common.con1.State <> ConnectionState.Open Then
                        Common.con1.Open()
                    End If
                    cmd1 = New OleDbCommand(sSql, Common.con1)
                    cmd1.ExecuteNonQuery()
                    If Common.con1.State <> ConnectionState.Closed Then
                        Common.con1.Close()
                    End If
                Else
                    If Common.con.State <> ConnectionState.Open Then
                        Common.con.Open()
                    End If
                    cmd = New SqlCommand(sSql, Common.con)
                    cmd.ExecuteNonQuery()
                    If Common.con.State <> ConnectionState.Closed Then
                        Common.con.Close()
                    End If
                End If
                'Cn.Execute(sSql)
                bCapture = DATACAPTURE()
                sSql = "select A.*,B.VEmployeeType,B.pfulimit from pay_result A,PAY_MASTER B where A.PAYCODE=B.PAYCODE AND A.PAYCODE ='" & LookUpEdit1.EditValue & "' And DatePart(mm,A.Mon_Year)=" & Format(txtapplicablefrom.DateTime, "MM") & " And DatePart(yyyy,A.Mon_Year)=" & Format(txtapplicablefrom.DateTime, "yyyy")
                'rsResult = Cn.Execute(sSql)  
                Dim rsResult As DataSet = New DataSet
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(rsResult)
                Else
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(rsResult)
                End If
                
                sSql = "select A.*,B.DATEOFBIRTH from arreartemp A,TBLEMPLOYEE B where A.PAYCODE=B.PAYCODE AND A.paycode='" & LookUpEdit1.EditValue & "' And DatePart(mm,A.for_month)=" & Format(txtapplicablefrom.DateTime, "MM") & " And DatePart(yyyy,A.for_month)=" & Format(txtapplicablefrom.DateTime, "yyyy")
                bProcess = DoProcess(sSql, rsResult)
                sSql = "select * from arreartemp where paycode='" & LookUpEdit1.EditValue & "' And DatePart(mm,for_month)=" & Format(txtapplicablefrom.DateTime, "MM") & " And DatePart(yyyy,for_month)=" & Format(txtapplicablefrom.DateTime, "yyyy")
                'rsMonthTable = Cn.Execute(sSql)
                Dim rsMonthTable As DataSet = New DataSet
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(rsMonthTable)
                Else
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(rsMonthTable)
                End If
                arrFDate = arrFDate.AddMonths(1) ' DateAdd("m", 1, arrFDate)
                If rsMonthTable.Tables(0).Rows.Count > 0 Then
                    txtEarningBasic.Text = Val(txtEarningBasic.Text) + rsMonthTable.Tables(0).Rows(0)("vsalary")
                    txtEarningDA.Text = Val(txtEarningDA.Text) + rsMonthTable.Tables(0).Rows(0)("vda_amt")
                    txtEarningHRA.Text = Val(txtEarningHRA.Text) + rsMonthTable.Tables(0).Rows(0)("vhra_amt")
                    txtEarningConv.Text = Val(txtEarningConv.Text) + rsMonthTable.Tables(0).Rows(0)("vconv_amt")
                    txtEarningMed.Text = Val(txtEarningMed.Text) + rsMonthTable.Tables(0).Rows(0)("vmed_amt")
                    txtEarningEarn1.Text = Val(txtEarningEarn1.Text) + rsMonthTable.Tables(0).Rows(0)("VI_1_AMT")
                    txtEarningEarn2.Text = Val(txtEarningEarn2.Text) + rsMonthTable.Tables(0).Rows(0)("VI_2_AMT")
                    txtEarningEarn3.Text = Val(txtEarningEarn3.Text) + rsMonthTable.Tables(0).Rows(0)("VI_3_AMT")
                    txtEarningEarn4.Text = Val(txtEarningEarn4.Text) + rsMonthTable.Tables(0).Rows(0)("VI_4_AMT")
                    txtEarningEarn5.Text = Val(txtEarningEarn5.Text) + rsMonthTable.Tables(0).Rows(0)("VI_5_AMT")
                    txtEarningEarn6.Text = Val(txtEarningEarn6.Text) + rsMonthTable.Tables(0).Rows(0)("VI_6_AMT")
                    txtEarningEarn7.Text = Val(txtEarningEarn7.Text) + rsMonthTable.Tables(0).Rows(0)("VI_7_AMT")
                    txtEarningEarn8.Text = Val(txtEarningEarn8.Text) + rsMonthTable.Tables(0).Rows(0)("VI_8_AMT")
                    txtEarningEarn9.Text = Val(txtEarningEarn9.Text) + rsMonthTable.Tables(0).Rows(0)("VI_9_AMT")
                    txtEarningEarn10.Text = Val(txtEarningEarn10.Text) + rsMonthTable.Tables(0).Rows(0)("VI_10_AMT")
                    txtCalculatedPF.Text = Val(txtCalculatedPF.Text) + rsMonthTable.Tables(0).Rows(0)("VPF_AMT")
                    txtCalculatedVPF.Text = Val(txtCalculatedVPF.Text) + rsMonthTable.Tables(0).Rows(0)("VvPF_AMT")
                    txtESIAmount.Text = Val(txtESIAmount.Text) + rsMonthTable.Tables(0).Rows(0)("Vesi_AMT")

                    txtcalculatedEpf.Text = Val(txtcalculatedEpf.Text) + rsMonthTable.Tables(0).Rows(0)("Vepf_AMT")
                    txtcalculatedFpf.Text = Val(txtcalculatedFpf.Text) + rsMonthTable.Tables(0).Rows(0)("Vfpf_AMT")
                    txtAmtonPf.Text = Val(txtAmtonPf.Text) + rsMonthTable.Tables(0).Rows(0)("amtonpf")
                    txtAmtonVPf.Text = Val(txtAmtonVPf.Text) + rsMonthTable.Tables(0).Rows(0)("amtonvpf")
                    txtAmtonESI.Text = Val(txtAmtonESI.Text) + rsMonthTable.Tables(0).Rows(0)("amtonesi")
                    TXTPAYdAYS.Text = Val(TXTPAYdAYS.Text) + rsMonthTable.Tables(0).Rows(0)("VTDAYS")
                End If
            Loop
        End If
    End Sub
    Private Function DoProcess(sSql As String, rsResult As DataSet) As Boolean
        Dim sAdvFn As String
        Dim rsProcess As DataSet = New DataSet
        Dim rsProf As DataSet = New DataSet
        Dim sWhichFormula As String '* 355
        Dim sPaycode As String '* 10, 
        Dim decpos As Integer
        Dim iPFAMT As Double, iFPFAMT As Double, iOtAmt As Double, iHRAAMT As Double, iESIAMT As Double
        Dim iDedAmt1 As Double, iDedAmt2 As Double, iDedAmt3 As Double, iDedAmt4 As Double, iDedAmt5 As Double, iDedAmt6 As Double, iDedAmt7 As Double, iDedAmt8 As Double, iDedAmt9 As Double, iDedAmt10 As Double
        Dim iErnAmt1 As Double, iErnAmt2 As Double, iErnAmt3 As Double, iErnAmt4 As Double, iErnAmt5 As Double, iErnAmt6 As Double, iErnAmt7 As Double, iErnAmt8 As Double, iErnAmt9 As Double, iErnAmt10 As Double
        Dim iSalary As Double, iNetSal As Double, iGrossSal As Double
        Dim BASIC As Double, DA As Double, CONV As Double, MED As Double, PF As Double, FPF As Double, OTRate As Double
        Dim HRA As Double, ESI As Double, Fine As Double, Advance As Double
        Dim PRE As Double, ABS1 As Double, HLD As Double, LATE As Double
        Dim EARLY As Double, OT As Double, CL As Double, SL As Double, PL_EL As Double
        Dim OTHER_LV As Double, LEAVE As Double, TDAYS As Double, T_LATE As Double, T_EARLY As Double
        Dim D_1TOD_10 As Double, E_1TOE_10 As Double, OT_RATE As Double, MON_DAY As Double
        Dim ided1 As Double, ided2 As Double, ided3 As Double, ided4 As Double, ided5 As Double, ided6 As Double, ided7 As Double, ided8 As Double, ided9 As Double, ided10 As Double
        Dim iern1 As Double, iern2 As Double, iern3 As Double, iern4 As Double, iern5 As Double, iern6 As Double, iern7 As Double, iern8 As Double, iern9 As Double, iern10 As Double
        Dim iRESULT As Double
        Dim iWOValue As Integer
        Dim test As Double, mArrearSalary As Double
        Dim rsarrear As DataSet = New DataSet
        Dim rsarrearpayday As DataSet = New DataSet
        Dim marrear As Double, marresiamt As Double, marrpfamt As Double, marrearesi As Double, marrearpf As Double, marrearfpf As Double, marrearpfonamt As Double, marrearvpf As Double
        Dim uptodate As Date, arrearpayday As Double, arrearmonday As Double
        Dim amt1_amt As Double, amt2_amt As Double, amt3_amt As Double, amt4_amt As Double, amt5_amt As Double, n As Integer
        Dim conv_amt As Double, medical_amt As Double, tds_amt As Double, prof_tax_amt As Double, amt_on_pf As Double, amt_on_esi As Double, iVPFAMT As Double, iEPFAMT As Double, TDS As Double, PROF_TAX As Double
        Dim rsPaysetup As DataSet = New DataSet
        Dim arrFDate As Date, arrTDate As Date
        'On Error GoTo ErrHand
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim cmd As New SqlCommand
        Dim cmd1 As New OleDbCommand
        Dim PP As XtraPayrollProcess = New XtraPayrollProcess
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(rsProcess)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(rsProcess)
        End If
        'rsProcess = Cn.Execute(sSql)

        If rsProcess.Tables(0).Rows.Count > 0 Then
            sSql = "select * from pay_setup"
            'rsPaysetup = Cn.Execute(sSql)
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(rsPaysetup)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(rsPaysetup)
            End If
            iPFAMT = 0 : iFPFAMT = 0 : iOtAmt = 0 : iHRAAMT = 0 : Advance = 0 : Fine = 0 : iDedAmt1 = 0 : iDedAmt2 = 0 : iDedAmt3 = 0 : iDedAmt4 = 0 : iDedAmt5 = 0 : iDedAmt6 = 0 : iDedAmt7 = 0 : iDedAmt8 = 0 : iDedAmt9 = 0 : iDedAmt10 = 0
            iErnAmt1 = 0 : iErnAmt2 = 0 : iErnAmt3 = 0 : iErnAmt4 = 0 : iErnAmt5 = 0 : iErnAmt6 = 0 : iErnAmt7 = 0 : iErnAmt8 = 0 : iErnAmt9 = 0 : iErnAmt10 = 0 : iESIAMT = 0
            conv_amt = 0 : medical_amt = 0 : tds_amt = 0 : prof_tax_amt = 0 : amt_on_pf = 0 : amt_on_esi = 0 : iVPFAMT = 0 : iEPFAMT = 0
            sPaycode = rsProcess.Tables(0).Rows(0)("PAYCODE") : BASIC = rsProcess.Tables(0).Rows(0)("VBASIC")
            DA = IIf(rsProcess.Tables(0).Rows(0)("VDA_RATE").ToString.Trim = "", 0, rsProcess.Tables(0).Rows(0)("VDA_RATE")) : HRA = IIf(rsProcess.Tables(0).Rows(0)("VHRA_RATE").ToString.Trim = "", 0, rsProcess.Tables(0).Rows(0)("VHRA_RATE"))
            MED = IIf(rsProcess.Tables(0).Rows(0)("Vmed_rate").ToString.Trim = "", 0, rsProcess.Tables(0).Rows(0)("Vmed_rate")) : CONV = IIf(rsProcess.Tables(0).Rows(0)("Vconv_rate").ToString.Trim = "", 0, rsProcess.Tables(0).Rows(0)("Vconv_rate"))
            TDAYS = rsProcess.Tables(0).Rows(0)("Vtdays")
            iern1 = IIf(rsProcess.Tables(0).Rows(0)("VI_1").ToString.Trim = "", 0, rsProcess.Tables(0).Rows(0)("VI_1"))
            iern2 = IIf(rsProcess.Tables(0).Rows(0)("VI_2").ToString.Trim = "", 0, rsProcess.Tables(0).Rows(0)("VI_2"))
            iern3 = IIf(rsProcess.Tables(0).Rows(0)("VI_3").ToString.Trim = "", 0, rsProcess.Tables(0).Rows(0)("VI_3"))
            iern4 = IIf(rsProcess.Tables(0).Rows(0)("VI_4").ToString.Trim = "", 0, rsProcess.Tables(0).Rows(0)("VI_4"))
            iern5 = IIf(rsProcess.Tables(0).Rows(0)("VI_5").ToString.Trim = "", 0, rsProcess.Tables(0).Rows(0)("VI_5"))
            iern6 = IIf(rsProcess.Tables(0).Rows(0)("VI_6").ToString.Trim = "", 0, rsProcess.Tables(0).Rows(0)("VI_6"))
            iern7 = IIf(rsProcess.Tables(0).Rows(0)("VI_7").ToString.Trim = "", 0, rsProcess.Tables(0).Rows(0)("VI_7"))
            iern8 = IIf(rsProcess.Tables(0).Rows(0)("VI_8").ToString.Trim = "", 0, rsProcess.Tables(0).Rows(0)("VI_8"))
            iern9 = IIf(rsProcess.Tables(0).Rows(0)("VI_9").ToString.Trim = "", 0, rsProcess.Tables(0).Rows(0)("VI_9"))
            iern10 = IIf(rsProcess.Tables(0).Rows(0)("VI_10").ToString.Trim = "", 0, rsProcess.Tables(0).Rows(0)("VI_10"))
            'rsResult
            'MON_DAY = NoOfDay(txtapplicablefrom.Value)
            MON_DAY = NoOfDay(rsResult.Tables(0).Rows(0)("mon_year"))
            '******-FOR BASIC SALARY-*************

            If rsPaysetup.Tables(0).Rows(0)("BASIC_RND") = "Y" Then
                iSalary = Math.Round((BASIC / MON_DAY) * TDAYS, 0)
            Else
                iSalary = Math.Round((BASIC / MON_DAY) * TDAYS, 2)
            End If
            '******-FOR DA*************
            If rsProcess.Tables(0).Rows(0)("Vda_f") = "F" Then
                DA = rsProcess.Tables(0).Rows(0)("vda_RATE")
            Else
                DA = (rsProcess.Tables(0).Rows(0)("vda_RATE") / MON_DAY) * TDAYS
            End If
            If rsPaysetup.Tables(0).Rows(0)("DA_RND") = "Y" Then
                DA = Math.Round(DA, 0)
            Else
                DA = Math.Round(DA, 2)
            End If
            '******-FOR HRA -*************
            If rsProcess.Tables(0).Rows(0)("VHRA_F") = "F" Then
                iHRAAMT = rsProcess.Tables(0).Rows(0)("VHRA_RATE")
            Else
                iHRAAMT = (rsProcess.Tables(0).Rows(0)("vHRA_RATE") / MON_DAY) * TDAYS
            End If
            If rsPaysetup.Tables(0).Rows(0)("HRA_RND") = "Y" Then
                iHRAAMT = Math.Round(iHRAAMT, 0)
            Else
                iHRAAMT = Math.Round(iHRAAMT, 2)
            End If
            '******-FOR CONVEYANCE*************
            If rsProcess.Tables(0).Rows(0)("Vconv_f") = "F" Then
                conv_amt = rsProcess.Tables(0).Rows(0)("Vconv_rate")
            Else
                conv_amt = (rsProcess.Tables(0).Rows(0)("Vconv_rate") / MON_DAY) * TDAYS
            End If
            If rsPaysetup.Tables(0).Rows(0)("CONV_RND") = "Y" Then
                conv_amt = Math.Round(conv_amt, 0)
            Else
                conv_amt = Math.Round(conv_amt, 2)
            End If
            '******-FOR MEDICAL*************
            If rsProcess.Tables(0).Rows(0)("Vmed_f") = "F" Then
                medical_amt = rsProcess.Tables(0).Rows(0)("Vmed_rate")
            Else
                medical_amt = (rsProcess.Tables(0).Rows(0)("Vmed_rate") / MON_DAY) * TDAYS
            End If
            If rsPaysetup.Tables(0).Rows(0)("MEDICAL_RND") = "Y" Then
                medical_amt = Math.Round(medical_amt, 0)
            Else
                medical_amt = Math.Round(medical_amt, 2)
            End If
            '******-FOR Earning   Formula -**********

            If rsProcess.Tables(0).Rows(0)("VIT_1").ToString.Trim <> "" And rsProcess.Tables(0).Rows(0)("VIT_1").ToString.Trim <> "0" Then
                sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(0)("VIT_1"))
                If sWhichFormula <> "" And sWhichFormula <> " " Then
                    iErnAmt1 = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                End If
            Else
                iErnAmt1 = iern1
            End If
            If rsPaysetup.Tables(0).Rows(0)("EARN1_RND") = "Y" Then
                iErnAmt1 = Math.Round(iErnAmt1, 0)
            Else
                iErnAmt1 = Math.Round(iErnAmt1, 2)
            End If
            If (rsProcess.Tables(0).Rows(0)("VIT_2").ToString.Trim <> "" And rsProcess.Tables(0).Rows(0)("VIT_2").ToString.Trim <> "0") Then
                sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(0)("VIT_2"))
                If sWhichFormula <> "" And sWhichFormula <> " " Then
                    iErnAmt2 = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                End If
            Else
                iErnAmt2 = iern2
            End If
            If rsPaysetup.Tables(0).Rows(0)("EARN2_RND") = "Y" Then
                iErnAmt2 = Math.Round(iErnAmt2, 0)
            Else
                iErnAmt2 = Math.Round(iErnAmt2, 2)
            End If
            If (rsProcess.Tables(0).Rows(0)("VIT_3").ToString.Trim <> "" And rsProcess.Tables(0).Rows(0)("VIT_3").ToString.Trim <> "0") Then
                sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(0)("VIT_3"))
                If sWhichFormula <> "" And sWhichFormula <> " " Then
                    iErnAmt3 = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                End If
            Else
                iErnAmt3 = iern3
            End If
            If rsPaysetup.Tables(0).Rows(0)("EARN3_RND") = "Y" Then
                iErnAmt3 = Math.Round(iErnAmt3, 0)
            Else
                iErnAmt3 = Math.Round(iErnAmt3, 2)
            End If

            If (rsProcess.Tables(0).Rows(0)("VIT_4").ToString.Trim <> "" And rsProcess.Tables(0).Rows(0)("VIT_4").ToString.Trim <> "0") Then
                sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(0)("VIT_4"))
                If sWhichFormula <> "" And sWhichFormula <> " " Then
                    iErnAmt4 = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                End If
            Else
                iErnAmt4 = iern4
            End If
            If rsPaysetup.Tables(0).Rows(0)("EARN4_RND") = "Y" Then
                iErnAmt4 = Math.Round(iErnAmt4, 0)
            Else
                iErnAmt4 = Math.Round(iErnAmt4, 2)
            End If

            If (rsProcess.Tables(0).Rows(0)("VIT_5").ToString.Trim <> "" And rsProcess.Tables(0).Rows(0)("VIT_5").ToString.Trim <> "0") Then
                sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(0)("VIT_5"))
                If sWhichFormula <> "" And sWhichFormula <> " " Then
                    iErnAmt5 = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                End If
            Else
                iErnAmt5 = iern5
            End If
            If rsPaysetup.Tables(0).Rows(0)("EARN5_RND") = "Y" Then
                iErnAmt5 = Math.Round(iErnAmt5, 0)
            Else
                iErnAmt5 = Math.Round(iErnAmt5, 2)
            End If
            If (rsProcess.Tables(0).Rows(0)("VIT_6").ToString.Trim <> "" And rsProcess.Tables(0).Rows(0)("VIT_6").ToString.Trim <> "0") Then
                sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(0)("VIT_6"))
                If sWhichFormula <> "" And sWhichFormula <> " " Then
                    iErnAmt6 = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                End If
            Else
                iErnAmt6 = iern6
            End If
            If rsPaysetup.Tables(0).Rows(0)("EARN6_RND") = "Y" Then
                iErnAmt6 = Math.Round(iErnAmt6, 0)
            Else
                iErnAmt6 = Math.Round(iErnAmt6, 2)
            End If
            If (rsProcess.Tables(0).Rows(0)("VIT_7").ToString.Trim <> "" And rsProcess.Tables(0).Rows(0)("VIT_7").ToString.Trim <> "0") Then
                sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(0)("VIT_7"))
                If sWhichFormula <> "" And sWhichFormula <> " " Then
                    iErnAmt7 = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                End If
            Else
                iErnAmt7 = iern7
            End If
            If rsPaysetup.Tables(0).Rows(0)("EARN7_RND") = "Y" Then
                iErnAmt7 = Math.Round(iErnAmt7, 0)
            Else
                iErnAmt7 = Math.Round(iErnAmt7, 2)
            End If
            If (rsProcess.Tables(0).Rows(0)("VIT_8").ToString.Trim <> "" And rsProcess.Tables(0).Rows(0)("VIT_8").ToString.Trim <> "0") Then
                sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(0)("VIT_8"))
                If sWhichFormula <> "" And sWhichFormula <> " " Then
                    iErnAmt8 = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                End If
            Else
                iErnAmt8 = iern8
            End If
            If rsPaysetup.Tables(0).Rows(0)("EARN8_RND") = "Y" Then
                iErnAmt8 = Math.Round(iErnAmt8, 0)
            Else
                iErnAmt8 = Math.Round(iErnAmt8, 2)
            End If
            If (rsProcess.Tables(0).Rows(0)("VIT_9").ToString.Trim <> "" And rsProcess.Tables(0).Rows(0)("VIT_9").ToString.Trim <> "0") Then
                sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(0)("VIT_9"))
                If sWhichFormula <> "" And sWhichFormula <> " " Then
                    iErnAmt9 = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                End If
            Else
                iErnAmt9 = iern9
            End If
            If rsPaysetup.Tables(0).Rows(0)("EARN9_RND") = "Y" Then
                iErnAmt9 = Math.Round(iErnAmt9, 0)
            Else
                iErnAmt9 = Math.Round(iErnAmt9, 2)
            End If
            If (rsProcess.Tables(0).Rows(0)("VIT_10").ToString.Trim <> "" And rsProcess.Tables(0).Rows(0)("VIT_10").ToString.Trim <> "0") Then
                sWhichFormula = PP.ProcFormula(rsProcess.Tables(0).Rows(0)("VIT_10"))
                If sWhichFormula <> "" And sWhichFormula <> " " Then
                    iErnAmt10 = PP.FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                End If
            Else
                iErnAmt10 = iern10
            End If
            If rsPaysetup.Tables(0).Rows(0)("EARN10_RND") = "Y" Then
                iErnAmt10 = Math.Round(iErnAmt10, 0)
            Else
                iErnAmt10 = Math.Round(iErnAmt10, 2)
            End If
            '******-FOR PF AMT FORMULA-**************
            If rsProcess.Tables(0).Rows(0)("PF_ALLOWED") = "Y" Then
                For n = 1 To Len(Trim(rsPaysetup.Tables(0).Rows(0)("PFREL")))
                    If n = 1 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("PFREL")), n, 1) = "1" Then
                        amt_on_pf = amt_on_pf + iSalary
                    End If
                    If n = 2 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("PFREL")), n, 1) = "1" Then
                        amt_on_pf = amt_on_pf + iHRAAMT
                    End If
                    If n = 3 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("PFREL")), n, 1) = "1" Then
                        amt_on_pf = amt_on_pf + DA
                    End If
                    If n = 4 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("PFREL")), n, 1) = "1" Then
                        amt_on_pf = amt_on_pf + conv_amt
                    End If
                    If n = 5 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("PFREL")), n, 1) = "1" Then
                        amt_on_pf = amt_on_pf + medical_amt
                    End If
                    If n = 6 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("PFREL")), n, 1) = "1" Then
                        amt_on_pf = amt_on_pf + iErnAmt1
                    End If
                    If n = 7 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("PFREL")), n, 1) = "1" Then
                        amt_on_pf = amt_on_pf + iErnAmt2
                    End If
                    If n = 8 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("PFREL")), n, 1) = "1" Then
                        amt_on_pf = amt_on_pf + iErnAmt3
                    End If
                    If n = 9 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("PFREL")), n, 1) = "1" Then
                        amt_on_pf = amt_on_pf + iErnAmt4
                    End If
                    If n = 10 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("PFREL")), n, 1) = "1" Then
                        amt_on_pf = amt_on_pf + iErnAmt5
                    End If
                    If n = 11 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("PFREL")), n, 1) = "1" Then
                        amt_on_pf = amt_on_pf + iErnAmt6
                    End If
                    If n = 12 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("PFREL")), n, 1) = "1" Then
                        amt_on_pf = amt_on_pf + iErnAmt7
                    End If
                    If n = 13 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("PFREL")), n, 1) = "1" Then
                        amt_on_pf = amt_on_pf + iErnAmt8
                    End If
                    If n = 14 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("PFREL")), n, 1) = "1" Then
                        amt_on_pf = amt_on_pf + iErnAmt9
                    End If
                    If n = 15 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("PFREL")), n, 1) = "1" Then
                        amt_on_pf = amt_on_pf + iErnAmt10
                    End If
                Next n
                If rsProcess.Tables(0).Rows(0)("VPF_ALLOWED") = "Y" Then
                    iVPFAMT = Math.Round((amt_on_pf * Convert.ToDouble(rsPaysetup.Tables(0).Rows(0)("VPF"))) / 100) ', Convert.ToDouble(rsPaysetup.Tables(0).Rows(0)("PFRND")))
                    iFPFAMT = iFPFAMT + iVPFAMT
                End If
                If rsResult.Tables(0).Rows.Count > 0 Then
                    If rsResult.Tables(0).Rows(0)("pfulimit") = "Y" Then
                        If rsResult.Tables(0).Rows(0)("AMTONPF") >= rsPaysetup.Tables(0).Rows(0)("PFLIMIT") Then
                            amt_on_pf = 0
                        Else
                            amt_on_pf = IIf((amt_on_pf + rsResult.Tables(0).Rows(0)("AMTONPF")) > rsPaysetup.Tables(0).Rows(0)("PFLIMIT"), rsPaysetup.Tables(0).Rows(0)("PFLIMIT") - rsResult.Tables(0).Rows(0)("AMTONPF"), amt_on_pf)
                        End If
                    End If
                End If
                iPFAMT = Math.Round((amt_on_pf * Convert.ToDouble(rsPaysetup.Tables(0).Rows(0)("PF"))) / 100) ', rsPaysetup.Tables(0).Rows(0)("PFRND"))
                iEPFAMT = Math.Round((IIf(amt_on_pf > Convert.ToDouble(rsPaysetup.Tables(0).Rows(0)("PFLIMIT")), Convert.ToDouble(rsPaysetup.Tables(0).Rows(0)("PFLIMIT")), amt_on_pf) * Convert.ToDouble(rsPaysetup.Tables(0).Rows(0)("EPF"))) / 100) ', rsPaysetup.Tables(0).Rows(0)("PFRND"))
                iFPFAMT = iPFAMT - iEPFAMT
                If rsProcess.Tables(0).Rows(0)("DATEOFBIRTH").ToString.Trim <> "" Then
                    If DateDiff("YYYY", rsProcess.Tables(0).Rows(0)("DATEOFBIRTH"), rsProcess.Tables(0).Rows(0)("MON_YEAR")) >= 58 Then
                        iFPFAMT = iFPFAMT + iEPFAMT
                        iEPFAMT = 0
                    End If
                End If
            End If
            '******-FOR ESI AMT FORMULA-*************
            If rsProcess.Tables(0).Rows(0)("ESI_ALLOWED") = "Y" Then
                For n = 1 To Len(Trim(rsPaysetup.Tables(0).Rows(0)("ESIREL")))
                    If n = 1 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("ESIREL")), n, 1) = "1" Then
                        amt_on_esi = amt_on_esi + iSalary
                    End If
                    If n = 2 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("ESIREL")), n, 1) = "1" Then
                        amt_on_esi = amt_on_esi + iHRAAMT
                    End If
                    If n = 3 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("ESIREL")), n, 1) = "1" Then
                        amt_on_esi = amt_on_esi + DA
                    End If
                    If n = 4 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("ESIREL")), n, 1) = "1" Then
                        amt_on_esi = amt_on_esi + conv_amt
                    End If
                    If n = 5 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("ESIREL")), n, 1) = "1" Then
                        amt_on_esi = amt_on_esi + medical_amt
                    End If
                    If n = 6 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("ESIREL")), n, 1) = "1" Then
                        amt_on_esi = amt_on_esi + iErnAmt1
                    End If
                    If n = 7 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("ESIREL")), n, 1) = "1" Then
                        amt_on_esi = amt_on_esi + iErnAmt2
                    End If
                    If n = 8 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("ESIREL")), n, 1) = "1" Then
                        amt_on_esi = amt_on_esi + iErnAmt3
                    End If
                    If n = 9 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("ESIREL")), n, 1) = "1" Then
                        amt_on_esi = amt_on_esi + iErnAmt4
                    End If
                    If n = 10 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("ESIREL")), n, 1) = "1" Then
                        amt_on_esi = amt_on_esi + iErnAmt5
                    End If
                    If n = 11 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("ESIREL")), n, 1) = "1" Then
                        amt_on_esi = amt_on_esi + iErnAmt6
                    End If
                    If n = 12 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("ESIREL")), n, 1) = "1" Then
                        amt_on_esi = amt_on_esi + iErnAmt7
                    End If
                    If n = 13 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("ESIREL")), n, 1) = "1" Then
                        amt_on_esi = amt_on_esi + iErnAmt8
                    End If
                    If n = 14 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("ESIREL")), n, 1) = "1" Then
                        amt_on_esi = amt_on_esi + iErnAmt9
                    End If
                    If n = 15 And Mid(Trim(rsPaysetup.Tables(0).Rows(0)("ESIREL")), n, 1) = "1" Then
                        amt_on_esi = amt_on_esi + iErnAmt10
                    End If
                Next n
                If rsResult.Tables(0).Rows.Count > 0 And rsResult.Tables(0).Rows(0)("AMTONESI") > 0 Then
                    iESIAMT = IIf(amt_on_esi > rsPaysetup.Tables(0).Rows(0)("ESILIMIT"), 0, (amt_on_esi * rsPaysetup.Tables(0).Rows(0)("ESIE")) / 100)
                    iESIAMT = Math.Round(iESIAMT, 0) + IIf(iESIAMT - Math.Round(iESIAMT, 0) > 0, 1, 0)
                    'iESIAMT = IIf(amt_on_esi > rsPaysetup("ESILIMIT"), 0,  Math.Round((amt_on_esi * rsPaysetup("ESIE")) / 100, rsPaysetup("ESIRND")))
                Else
                    iESIAMT = 0
                    amt_on_esi = 0
                End If
            End If

            '******-FOR PROFESSIONAL TAX*************
            If rsProcess.Tables(0).Rows(0)("PROF_TAX_ALLOWED") = "Y" Then
                sSql = "select * from professionaltax where lowerlimit<=" & iGrossSal & " and upperlimit>=" & iGrossSal
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(rsProf)
                Else
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(rsProf)
                End If
                'rsProf = Cn.Execute(sSql)
                If rsProf.Tables(0).Rows.Count > 0 Then
                    prof_tax_amt = rsProf.Tables(0).Rows(0)("taxamount")
                End If
                If rsPaysetup.Tables(0).Rows(0)("PROF_TAX_RND") = "Y" Then
                    prof_tax_amt = Math.Round(prof_tax_amt, 0)
                Else
                    prof_tax_amt = Math.Round(prof_tax_amt, 2)
                End If
            End If
            sSql = "Update arreartemp Set AMTONPF=" & amt_on_pf & ",VPF_AMT=" & iPFAMT & ",VEPF_AMT=" & iEPFAMT & ",VFPF_AMT=" & iFPFAMT & ",VVPF_AMT=" & iVPFAMT & _
                     ",PROF_TAX_AMT=" & prof_tax_amt & ",VHRA_AMT=" & iHRAAMT & ",VCONV_AMT=" & conv_amt & ",VMED_AMT=" & medical_amt & ",AMTONESI=" & amt_on_esi & ",VESI_AMT=" & iESIAMT & "," & _
                     "VDA_AMT=" & DA & _
                     ",VI_1_AMT=" & iErnAmt1 & ",VI_2_AMT=" & iErnAmt2 & _
                     ",VI_3_AMT=" & iErnAmt3 & ",VI_4_AMT=" & iErnAmt4 & _
                     ",VI_5_AMT=" & iErnAmt5 & ",VI_6_AMT=" & iErnAmt6 & _
                     ",VI_7_AMT=" & iErnAmt7 & ",VI_8_AMT=" & iErnAmt8 & _
                     ",VI_9_AMT=" & iErnAmt9 & ",VI_10_AMT=" & iErnAmt10 & _
                     ",VSALARY=" & iSalary & ",VTDAYS=" & TDAYS
            If Common.servername = "Access" Then
                If Common.con1.State <> ConnectionState.Open Then
                    Common.con1.Open()
                End If
                cmd1 = New OleDbCommand(sSql, Common.con1)
                cmd1.ExecuteNonQuery()
                If Common.con1.State <> ConnectionState.Closed Then
                    Common.con1.Close()
                End If
            Else
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
        End If
        Exit Function
        'ErrHand:
        '        'Screen.MousePointer = vbNormal
        '        'MsgBox(Err.Number & " --> AKDoProcess " & Chr(13) & Err.Description)
        '        Resume Next
        '        rsProcess = Nothing
        '        'rsAdvance = Nothing
        '        'rsFine = Nothing
        '        DoProcess = False
    End Function
    Function DATACAPTURE() As Boolean
        Dim sSql As String
        Dim rspaymaster As DataSet = New DataSet
        Dim sPaycode As String
        Dim iVTotHrsWorked As Integer, iVOTDuration As Integer
        Dim iVEarlyDeparture As Integer, iVLateArrival As Integer
        Dim iEarlyDays As Integer, iLateDays As Integer
        Dim iVLossHours As Integer, iPresentValue As Double
        Dim iAbsentValue As Double, iHolidayValue As Double
        Dim iWOValue As Double, sLeaveCode As String ' * 3
        Dim iVCL As Double, iVSL As Double
        Dim iVPL_EL As Double, iVOther_LV As Double
        Dim iTotalLeave As Double, iLeaveAmount As Double
        Dim sMonth As String '* 6,
        Dim iTotalCount As Long
        Dim i As Integer, mRsEmployee As DataSet = New DataSet
        Dim iTotalDays As Double
        Dim rsMonthTable As DataSet = New DataSet
        iTotalDays = 0
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim cmd As New SqlCommand
        Dim cmd1 As New OleDbCommand
        sSql = "Select * From Pay_master" & " Where Paycode='" & Trim(LookUpEdit1.EditValue) & "'"
        'rspaymaster = Cn.Execute(sSql)
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(rspaymaster)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(rspaymaster)
        End If

        sSql = "select * from pay_result where paycode='" & Trim(LookUpEdit1.EditValue) & "' and DatePart(mm,Mon_Year)=" & arrFDate.ToString("MM") & " And DatePart(yy,Mon_Year)=" & arrFDate.ToString("yyyy")
        'rsMonthTable = Cn.Execute(sSql)
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(rsMonthTable)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(rsMonthTable)
        End If

        If rsMonthTable.Tables(0).Rows.Count > 0 Then
            iTotalDays = rsMonthTable.Tables(0).Rows(0)("vtdays")
        End If
        'sSql = "Insert Into arreartemp(PAYCODE,mon_year,for_month) values('" & Trim(LookUpEdit1.EditValue) & "','" & Format("01/" & Format(txtPayableMonth.DateTime, "MM/yyyy"), "MMM dd yyyy") & "','" & Format("01/" & Format(txtapplicablefrom.DateTime, "MM/yyyy"), "MMM dd yyyy") & "')"
        sSql = "Insert Into arreartemp(PAYCODE,mon_year,for_month) values('" & Trim(LookUpEdit1.EditValue) & "','" & txtPayableMonth.DateTime.ToString("yyyy-MM-dd") & "','" & txtapplicablefrom.DateTime.ToString("yyyy-MM-dd") & "')"
        'Cn.Execute(sSql)
        If Common.servername = "Access" Then
            If Common.con1.State <> ConnectionState.Open Then
                Common.con1.Open()
            End If
            cmd1 = New OleDbCommand(sSql, Common.con1)
            cmd1.ExecuteNonQuery()
            If Common.con1.State <> ConnectionState.Closed Then
                Common.con1.Close()
            End If
        Else
            If Common.con.State <> ConnectionState.Open Then
                Common.con.Open()
            End If
            cmd = New SqlCommand(sSql, Common.con)
            cmd.ExecuteNonQuery()
            If Common.con.State <> ConnectionState.Closed Then
                Common.con.Close()
            End If
        End If
        sSql = "Update arreartemp Set VBASIC=" & txtBasic.Text & ",VDA_RATE=" & txtDA.Text & ",VDA_F='" & rspaymaster.Tables(0).Rows(0)("VDA_F") & "'" & _
            ",VCONV_RATE=" & txtConv.Text & ",VCONV_F='" & rspaymaster.Tables(0).Rows(0)("VCONV_F") & "'" & _
            ",VMED_RATE=" & txtMed.Text & ",VMED_F='" & rspaymaster.Tables(0).Rows(0)("VMED_F") & "'" & _
            ",PF_ALLOWED='" & rspaymaster.Tables(0).Rows(0)("PF_ALLOWED") & "',VPF_ALLOWED='" & rspaymaster.Tables(0).Rows(0)("VPF_ALLOWED") & "'," & _
            "VHRA_RATE=" & txtHRA.Text & ",VHRA_F='" & rspaymaster.Tables(0).Rows(0)("VHRA_F") & "',ESI_ALLOWED='" & rspaymaster.Tables(0).Rows(0)("ESI_ALLOWED") & "'," & _
            "VIT_1='" & rspaymaster.Tables(0).Rows(0)("VIT_1") & "',VI_1=" & txtEarn1.Text & ",VIT_2='" & rspaymaster.Tables(0).Rows(0)("VIT_2") & "',VI_2=" & txtEarn2.Text & _
            ",VIT_3='" & rspaymaster.Tables(0).Rows(0)("VIT_3") & "',VI_3=" & txtEarn3.Text & ",VIT_4='" & rspaymaster.Tables(0).Rows(0)("VIT_4") & "',VI_4=" & txtEarn4.Text & _
            ",VIT_5='" & rspaymaster.Tables(0).Rows(0)("VIT_5") & "',VI_5=" & txtEarn5.Text & ",VIT_6='" & rspaymaster.Tables(0).Rows(0)("VIT_6") & "',VI_6=" & txtEarn6.Text & _
            ",VIT_7='" & rspaymaster.Tables(0).Rows(0)("VIT_7") & "',VI_7=" & txtEarn7.Text & ",VIT_8='" & rspaymaster.Tables(0).Rows(0)("VIT_8") & "',VI_8=" & txtEarn8.Text & _
            ",VIT_9='" & rspaymaster.Tables(0).Rows(0)("VIT_9") & "',VI_9=" & txtEarn9.Text & ",VIT_10='" & rspaymaster.Tables(0).Rows(0)("VIT_10") & "',VI_10=" & txtEarn10.Text & _
            ",PROF_TAX_Allowed='" & rspaymaster.Tables(0).Rows(0)("PROF_TAX_Allowed") & "'"
        'Cn.Execute(sSql)
        If Common.servername = "Access" Then
            If Common.con1.State <> ConnectionState.Open Then
                Common.con1.Open()
            End If
            cmd1 = New OleDbCommand(sSql, Common.con1)
            cmd1.ExecuteNonQuery()
            If Common.con1.State <> ConnectionState.Closed Then
                Common.con1.Close()
            End If
        Else
            If Common.con.State <> ConnectionState.Open Then
                Common.con.Open()
            End If
            cmd = New SqlCommand(sSql, Common.con)
            cmd.ExecuteNonQuery()
            If Common.con.State <> ConnectionState.Closed Then
                Common.con.Close()
            End If
        End If
        If optPayable1.Checked = True Then
            sSql = "Update arreartemp Set VTDAYS=" & iTotalDays
        Else
            sSql = "Update arreartemp Set VTDAYS=" & mskPaidDays.Text
        End If
        If Common.servername = "Access" Then
            If Common.con1.State <> ConnectionState.Open Then
                Common.con1.Open()
            End If
            cmd1 = New OleDbCommand(sSql, Common.con1)
            cmd1.ExecuteNonQuery()
            If Common.con1.State <> ConnectionState.Closed Then
                Common.con1.Close()
            End If
        Else
            If Common.con.State <> ConnectionState.Open Then
                Common.con.Open()
            End If
            cmd = New SqlCommand(sSql, Common.con)
            cmd.ExecuteNonQuery()
            If Common.con.State <> ConnectionState.Closed Then
                Common.con.Close()
            End If
        End If
        'Cn.Execute(sSql)
        'arrFDate = arrFDate.AddMonths(1)
    End Function
    Private Function NoOfDay(ProcDate As Date) As Integer
        'Dim dtm1 As Date
        'Dim dtm2 As Date
        'dtm1 = DateSerial(year(ProcDate), Month(ProcDate), 1)
        'dtm2 = DateAdd("m", 1, dtm1)
        'dtm2 = DateAdd("d", -1, dtm2)
        'NoOfDay = Day(dtm2)
        Return Date.DaysInMonth(ProcDate.Year, ProcDate.Month)
    End Function
    Private Sub txtapplicablefrom_Leave(sender As System.Object, e As System.EventArgs) Handles txtapplicablefrom.Leave
        Dim rstest As DataSet = New DataSet
        btnApply.Enabled = True
        SimpleButtonSave.Enabled = True
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim sSql As String = "select * from arrear where paycode='" & LookUpEdit1.EditValue & "' And DatePart(MM,MON_YEAR)=" & Format(txtPayableMonth.DateTime, "MM") & " And DatePart(yy,MON_YEAR)=" & Format(txtPayableMonth.DateTime, "yyyy") & " And DatePart(MM,FOR_MONTH)=" & Format(txtapplicablefrom.DateTime, "MM") & " And DatePart(yy,FOR_MONTH)=" & Format(txtapplicablefrom.DateTime, "yyyy")
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(rstest)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(rstest)
        End If
        If rstest.Tables(0).Rows.Count > 0 Then                     
            'txtapplicablefrom = rstest!applicablefrom
            txtBasic.Text = IIf(rstest.Tables(0).Rows(0)("arrearbasic").ToString.Trim = "", "0", rstest.Tables(0).Rows(0)("arrearbasic"))
            txtDA.Text = IIf(rstest.Tables(0).Rows(0)("arrearda").ToString.Trim = "", "0", rstest.Tables(0).Rows(0)("arrearda"))
            txtHRA.Text = IIf(rstest.Tables(0).Rows(0)("arrearhra").ToString.Trim = "", "0", rstest.Tables(0).Rows(0)("arrearhra"))
            txtConv.Text = IIf(rstest.Tables(0).Rows(0)("arrearconv").ToString.Trim = "", "0", rstest.Tables(0).Rows(0)("arrearconv"))
            txtMed.Text = IIf(rstest.Tables(0).Rows(0)("arrearMed").ToString.Trim = "", "0", rstest.Tables(0).Rows(0)("arrearMed"))
            txtEarn1.Text = IIf(rstest.Tables(0).Rows(0)("arrearamount1").ToString.Trim = "", "0", rstest.Tables(0).Rows(0)("arrearamount1"))
            txtEarn2.Text = IIf(rstest.Tables(0).Rows(0)("arrearamount2").ToString.Trim = "", "0", rstest.Tables(0).Rows(0)("arrearamount2"))
            txtEarn3.Text = IIf(rstest.Tables(0).Rows(0)("arrearamount3").ToString.Trim = "", "0", rstest.Tables(0).Rows(0)("arrearamount3"))
            txtEarn4.Text = IIf(rstest.Tables(0).Rows(0)("arrearamount4").ToString.Trim = "", "0", rstest.Tables(0).Rows(0)("arrearamount4"))
            txtEarn5.Text = IIf(rstest.Tables(0).Rows(0)("arrearamount5").ToString.Trim = "", "0", rstest.Tables(0).Rows(0)("arrearamount5"))
            txtEarn6.Text = IIf(rstest.Tables(0).Rows(0)("arrearamount6").ToString.Trim = "", "0", rstest.Tables(0).Rows(0)("arrearamount6"))
            txtEarn7.Text = IIf(rstest.Tables(0).Rows(0)("arrearamount7").ToString.Trim = "", "0", rstest.Tables(0).Rows(0)("arrearamount7"))
            txtEarn8.Text = IIf(rstest.Tables(0).Rows(0)("arrearamount8").ToString.Trim = "", "0", rstest.Tables(0).Rows(0)("arrearamount8"))
            txtEarn9.Text = IIf(rstest.Tables(0).Rows(0)("arrearamount9").ToString.Trim = "", "0", rstest.Tables(0).Rows(0)("arrearamount9"))
            txtEarn10.Text = IIf(rstest.Tables(0).Rows(0)("arrearamount10").ToString.Trim = "", "0", rstest.Tables(0).Rows(0)("arrearamount10"))
            txtEarningBasic.Text = IIf(rstest.Tables(0).Rows(0)("arrearbasic_amt").ToString.Trim = "", "0", rstest.Tables(0).Rows(0)("arrearbasic_amt"))
            txtEarningDA.Text = IIf(rstest.Tables(0).Rows(0)("arrearda_amt").ToString.Trim = "", "0", rstest.Tables(0).Rows(0)("arrearda_amt"))
            txtEarningHRA.Text = IIf(rstest.Tables(0).Rows(0)("arrearhra_amt").ToString.Trim = "", "0", rstest.Tables(0).Rows(0)("arrearhra_amt"))
            txtEarningConv.Text = IIf(rstest.Tables(0).Rows(0)("arrearConv_amt").ToString.Trim = "", "0", rstest.Tables(0).Rows(0)("arrearConv_amt"))
            txtEarningMed.Text = IIf(rstest.Tables(0).Rows(0)("arrearMed_amt").ToString.Trim = "", "0", rstest.Tables(0).Rows(0)("arrearMed_amt"))
            txtEarningEarn1.Text = IIf(rstest.Tables(0).Rows(0)("arrearamount1_amt").ToString.Trim = "", "0", rstest.Tables(0).Rows(0)("arrearamount1_amt"))
            txtEarningEarn2.Text = IIf(rstest.Tables(0).Rows(0)("arrearamount2_amt").ToString.Trim = "", "0", rstest.Tables(0).Rows(0)("arrearamount2_amt"))
            txtEarningEarn3.Text = IIf(rstest.Tables(0).Rows(0)("arrearamount3_amt").ToString.Trim = "", "0", rstest.Tables(0).Rows(0)("arrearamount3_amt"))
            txtEarningEarn4.Text = IIf(rstest.Tables(0).Rows(0)("arrearamount4_amt").ToString.Trim = "", "0", rstest.Tables(0).Rows(0)("arrearamount4_amt"))
            txtEarningEarn5.Text = IIf(rstest.Tables(0).Rows(0)("arrearamount5_amt").ToString.Trim = "", "0", rstest.Tables(0).Rows(0)("arrearamount5_amt"))
            txtEarningEarn6.Text = IIf(rstest.Tables(0).Rows(0)("arrearamount6_amt").ToString.Trim = "", "0", rstest.Tables(0).Rows(0)("arrearamount6_amt"))
            txtEarningEarn7.Text = IIf(rstest.Tables(0).Rows(0)("arrearamount7_amt").ToString.Trim = "", "0", rstest.Tables(0).Rows(0)("arrearamount7_amt"))
            txtEarningEarn8.Text = IIf(rstest.Tables(0).Rows(0)("arrearamount8_amt").ToString.Trim = "", "0", rstest.Tables(0).Rows(0)("arrearamount8_amt"))
            txtEarningEarn9.Text = IIf(rstest.Tables(0).Rows(0)("arrearamount9_amt").ToString.Trim = "", "0", rstest.Tables(0).Rows(0)("arrearamount9_amt"))
            txtEarningEarn10.Text = IIf(rstest.Tables(0).Rows(0)("arrearamount10_amt").ToString.Trim = "", "0", rstest.Tables(0).Rows(0)("arrearamount10_amt"))
            txtCalculatedPF.Text = rstest.Tables(0).Rows(0)("ARREARPF")
            txtCalculatedVPF.Text = rstest.Tables(0).Rows(0)("arrearvpf")
            txtESIAmount.Text = rstest.Tables(0).Rows(0)("ARREARESI")
            'txtapplicablefrom.EditValue = Format(rstest.Tables(0).Rows(0)("FOR_MONTH"), "dd/MM/yyyy")
            'txtapplicableto.EditValue = Format(rstest.Tables(0).Rows(0)("TO_MONTH"), "dd/MM/yyyy")
        End If
    End Sub
    Private Sub SimpleButtonSave_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonSave.Click
        Dim rspaymaster As DataSet = New DataSet
        Dim rsrecordchk As DataSet = New DataSet
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim cmd As New SqlCommand
        Dim cmd1 As New OleDbCommand
        Dim sSql As String = "select * from arreartemp where paycode='" & LookUpEdit1.EditValue.ToString.Trim & "' And DatePart(MM,for_month)=" & Format(txtapplicablefrom.DateTime, "MM") & " And DatePart(yy,for_month)=" & Format(txtapplicablefrom.DateTime, "yyyy")
        'rsrecordchk = Cn.Execute(sSql)
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(rsrecordchk)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(rsrecordchk)
        End If
        If rsrecordchk.Tables(0).Rows.Count > 0 Then
            sSql = "insert into arrear (paycode,mon_year,for_month,to_month,paiddays,arrearbasic,arrearbasic_amt, " & _
                    "arrearda,arrearda_amt,arrearhra,arrearhra_amt,arrearconv,arrearconv_amt,arrearmed,arrearmed_amt, " & _
                    "arrearamount1,arrearamount1_amt,arrearamount2,arrearamount2_amt, " & _
                    "arrearamount3,arrearamount3_amt,arrearamount4,arrearamount4_amt, " & _
                    "arrearamount5,arrearamount5_amt,arrearamount6,arrearamount6_amt, " & _
                    "arrearamount7,arrearamount7_amt,arrearamount8,arrearamount8_amt, " & _
                    "arrearamount9,arrearamount9_amt,arrearamount10,arrearamount10_amt, " & _
                    "arrearpf,arrearvpf,arrearepf_amt,arrearfpf_amt,arrearesi,arrearpfonamt,arrearVpfonamt,ARREARESIONAMT) values(" & _
                    "'" & rsrecordchk.Tables(0).Rows(0)("PAYCODE").ToString.Trim & "','" & txtPayableMonth.DateTime.ToString("yyyy-MM-dd") & "','" & txtapplicablefrom.DateTime.ToString("yyyy-MM-dd") & "','" & txtapplicableto.DateTime.ToString("yyyy-MM-dd") & "'," & TXTPAYdAYS.Text & "," & _
                    rsrecordchk.Tables(0).Rows(0)("VBASIC") & "," & txtEarningBasic.Text.Trim & "," & _
                    rsrecordchk.Tables(0).Rows(0)("VDA_RATE") & "," & txtEarningDA.Text.Trim & "," & _
                    rsrecordchk.Tables(0).Rows(0)("VHRA_RATE") & "," & txtEarningHRA.Text.Trim & "," & _
                    rsrecordchk.Tables(0).Rows(0)("VCONV_RATE") & "," & txtEarningConv.Text.Trim & "," & _
                    rsrecordchk.Tables(0).Rows(0)("VMED_RATE") & "," & txtEarningMed.Text.Trim & ","

            sSql = sSql & rsrecordchk.Tables(0).Rows(0)("VI_1") & "," & txtEarningEarn1.Text.Trim & "," & _
                rsrecordchk.Tables(0).Rows(0)("VI_2") & "," & txtEarningEarn2.Text.Trim & "," & _
                rsrecordchk.Tables(0).Rows(0)("VI_3") & "," & txtEarningEarn3.Text.Trim & "," & _
                rsrecordchk.Tables(0).Rows(0)("VI_4") & "," & txtEarningEarn4.Text.Trim & "," & _
                rsrecordchk.Tables(0).Rows(0)("VI_5") & "," & txtEarningEarn5.Text.Trim & "," & _
                rsrecordchk.Tables(0).Rows(0)("VI_6") & "," & txtEarningEarn6.Text.Trim & "," & _
                rsrecordchk.Tables(0).Rows(0)("VI_7") & "," & txtEarningEarn7.Text.Trim & "," & _
                rsrecordchk.Tables(0).Rows(0)("VI_8") & "," & txtEarningEarn8.Text.Trim & "," & _
                rsrecordchk.Tables(0).Rows(0)("VI_9") & "," & txtEarningEarn9.Text.Trim & "," & _
                rsrecordchk.Tables(0).Rows(0)("VI_10") & "," & txtEarningEarn10.Text.Trim & "," & _
                txtCalculatedPF.Text.Trim & "," & txtCalculatedVPF.Text.Trim & "," & _
                txtcalculatedEpf.Text & "," & txtcalculatedFpf.Text & "," & _
                txtESIAmount.Text.Trim & "," & txtAmtonPf.Text & "," & _
                txtAmtonVPf.Text & "," & txtAmtonESI.Text & ")"

            If Common.servername = "Access" Then
                If Common.con1.State <> ConnectionState.Open Then
                    Common.con1.Open()
                End If
                cmd1 = New OleDbCommand(sSql, Common.con1)
                cmd1.ExecuteNonQuery()               
                If Common.con1.State <> ConnectionState.Closed Then
                    Common.con1.Close()
                End If
            Else
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()              
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If

            sSql = "insert into pay_mastert  select * from pay_master WHERE PAYCODE='" & Trim(LookUpEdit1.EditValue) & "'"
            If Common.servername = "Access" Then
                If Common.con1.State <> ConnectionState.Open Then
                    Common.con1.Open()
                End If
                cmd1 = New OleDbCommand(sSql, Common.con1)
                cmd1.ExecuteNonQuery()
                If Common.con1.State <> ConnectionState.Closed Then
                    Common.con1.Close()
                End If
            Else
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If

            sSql = "Update Pay_Master SET VBASIC=VBASIC+" & Val(txtBasic.Text) & ",VDA_RATE=VDA_RATE+" & Val(txtDA.Text) & ",VCONV_RATE=VCONV_RATE+" & Val(txtConv.Text) & ",VMED_RATE=VMED_RATE+" & Val(txtMed.Text) & ",VHRA_RATE=VHRA_RATE+" & Val(txtHRA.Text) & _
                   ",VI_1=VI_1+" & Val(txtEarn1.Text) & ",VI_2=VI_2+" & Val(txtEarn2.Text) & _
                    ",VI_3=VI_3+" & Val(txtEarn3.Text) & ",VI_4=VI_4+" & Val(txtEarn4.Text) & _
                    ",VI_5=VI_5+" & Val(txtEarn5.Text) & ",VI_6=VI_6+" & Val(txtEarn6.Text) & _
                    ",VI_7=VI_7+" & Val(txtEarn7.Text) & ",VI_8=VI_8+" & Val(txtEarn8.Text) & _
                    ",VI_9=VI_9+" & Val(txtEarn9.Text) & ",VI_10=VI_10+" & Val(txtEarn10.Text) & _
                    ",EFFECTFROM='" & txtapplicablefrom.DateTime.ToString("yyyy-MM-dd") & _
                    "' WHERE PAYCODE='" & Trim(LookUpEdit1.EditValue) & "'"

            If Common.servername = "Access" Then
                If Common.con1.State <> ConnectionState.Open Then
                    Common.con1.Open()
                End If
                cmd1 = New OleDbCommand(sSql, Common.con1)
                cmd1.ExecuteNonQuery()
                If Common.con1.State <> ConnectionState.Closed Then
                    Common.con1.Close()
                End If
            Else
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
            sSql = "Update Pay_Master SET VGross=vbasic+vda_rate+vmed_rate+VCONV_RATE+VHRA_RATE+VI_1+VI_2+VI_3+VI_4+VI_5+VI_6+VI_7+VI_8+VI_9+VI_10" & ",VIncrement=" & Val(txtBasic.Text) & "+" & Val(txtDA.Text) & "+" & Val(txtConv.Text) & "+" & Val(txtMed.Text) & "+" & Val(txtHRA.Text) & "+" & Val(txtEarn1.Text) & "+" & Val(txtEarn2.Text) & "+" & Val(txtEarn3.Text) & "+" & Val(txtEarn4.Text) & "+" & Val(txtEarn5.Text) & "+" & Val(txtEarn6.Text) & "+" & Val(txtEarn7.Text) & "+" & Val(txtEarn8.Text) & "+" & Val(txtEarn9.Text) & "+" & Val(txtEarn10.Text) & _
                    " WHERE PAYCODE='" & Trim(LookUpEdit1.EditValue) & "'"

            If Common.servername = "Access" Then
                If Common.con1.State <> ConnectionState.Open Then
                    Common.con1.Open()
                End If
                cmd1 = New OleDbCommand(sSql, Common.con1)
                cmd1.ExecuteNonQuery()
                If Common.con1.State <> ConnectionState.Closed Then
                    Common.con1.Close()
                End If
            Else
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
        End If
        setDefault()
    End Sub
End Class
