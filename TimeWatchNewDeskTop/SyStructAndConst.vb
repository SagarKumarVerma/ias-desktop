﻿Imports System
Imports System.Runtime.InteropServices

Namespace HSeriesSampleCSharp

    Enum VerifyKind

        vrfkDefault = 0

        vrfkOnlyFingerPrint = 1

        vrfkOnlyPassword = 2

        vrfkOnlyCard = 3

        vrfkFingerPrint_Or_Password = 4

        vrfkFingerPrint_Or_Card = 5

        vrfkPassword_Or_Card = 6

        vrfkFingerPrint_Or_Card_Or_Password = 7

        vrfkFingerPrint_And_Password = 8

        vrfkFingerPrint_And_Card = 9

        vrfkPassword_And_Card = 10

        vrfkFingerPrint_And_Card_And_Password = 0

        B
    End Enum

    Enum UserType

        utNormalUser = 0

        utAdmin = 1

        utRegisterUser = 2

        utSuperAdmin = 3

        utStoreAdmin = 4
    End Enum

    Enum LogKind

        lkFingerPrint = 1

        lkPassword = 2

        lkCard = 3

        lkFingerPrint_Password = 4

        lkFingerPrint_Card = 5

        lkPassword_Card = 6

        lkFingerPrint_Card_Password = 7
    End Enum

    Enum SyLastError As Integer

        sleSuss = 0

        sleSendError = 1

        sleRecvDataError = 2

        sleRecvTimeOut = 3

        sleCallOSFuncError = 4

        sleInvalidParam = 5

        sleNoNewLog = 6

        sleDevNotReady = 15

        slePasswordError = 19

        sleInvalidUserID = 48

        sleFullForRegister = 49

        sleFullForUser = 50

        sleTimeoutForReg = 51

        sleRegistering = 55

        sleNeverRegister = 56

        sleNoRegForBackupID = 65

        sleNoFunctionInDll = 66

        sleUnknownError = 254
    End Enum

    Public Structure WorkingShift

        Public secA As WorkingSection

        Public secB As WorkingSection

        Public secC As WorkingSection
    End Structure

    <StructLayout(LayoutKind.Sequential, Pack:=1)> _
    Public Structure WorkingSection

        Public startHour As Byte

        Public startMin As Byte

        Public EndHour As Byte

        Public EndMin As Byte

        Public attribute As Byte
    End Structure

    <StructLayout(LayoutKind.Sequential, Pack:=1)> _
    Public Structure SySystemOption

        Public cLanguage As Byte

        Public cTimeFmt As Byte

        Public cLockPersonNum As Byte

        'daylight saving time
        Public cDLST_Flag As Byte

        Public cDLST_mode2_Flag As Byte

        Public cDLST_En As Byte

        Public cDLST_DateFmt As Byte

        Public cDLST_StartMon As Byte

        Public cDLST_StartDate As Byte

        Public cDLST_StartWS As Byte

        Public cDLST_StartWK As Byte

        Public cDLST_StartHour As Byte

        Public cDLST_StartMin As Byte

        Public cDLST_EndMon As Byte

        Public cDLST_EndDate As Byte

        Public cDLST_EndWS As Byte

        Public cDLST_EndWK As Byte

        Public cDLST_EndHour As Byte

        Public cDLST_EndMin As Byte

        'advanced settings
        Public cFpShowScore As Byte

        Public cFpSecurLev1 As Byte

        Public cFpSecurLevN As Byte

        Public cUserIdMust As Byte

        Public cTwoSensorsEn As Byte

        Public cVoiceHintEn As Byte

        'A variety of card authentication settings
        Public cNoCardOnly As Byte

        Public cRegCardOnly As Byte

        Public cFpCardOnly As Byte

        'Additional feature set 
        Public cRemoteVerify As Byte

        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public cAuthServerIP() As Byte

        Public cWorkCode As Byte

        Public cButtonBeepEn As Byte

        Public cAdjustVoice As Byte

        Public cImdtPrintEn As Byte

        Public cDefendTailEn As Byte

        Public cUserPswEdit As Byte
    End Structure

    <StructLayout(LayoutKind.Sequential, Pack:=1)> _
    Public Structure SyDailyEntryOption

        Public wAlarmAdminLogFreeNum As UInt16

        Public wAlarmLogFreeNum As UInt16

        Public cRecheckMin As Byte

        Public cReadyBlock As Byte
    End Structure

    <StructLayout(LayoutKind.Sequential, Pack:=1)> _
    Public Structure SyTimeFormat

        Public wYear As UInt16

        Public cMonth As Byte

        Public [cDate] As Byte

        Public cDay As Byte

        Public cHour As Byte

        Public cMin As Byte

        Public cSec As Byte
    End Structure

    <StructLayout(LayoutKind.Sequential, Pack:=1)> _
    Public Structure SyCommOption

        Public cBaudRate As Byte

        Public cDevNum As UInt32

        Public cDHCP_EN As Byte

        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public cIP_Addr() As Byte

        Public cNetSpeed As Byte

        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public cNetMask() As Byte

        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public cGateway() As Byte

        Public cEthernetEn As Byte

        Public cRS232_En As Byte

        Public cRS485_En As Byte

        Public cUSB_En As Byte

        Public cUP_Now_Event As Byte

        Public dwLinkPws As UInt32

        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=6)> _
        Public cMac() As Byte

        Public wLporta As UInt16

        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public sSeverIp() As Byte

        Public wRport As UInt16

        Public cWiegand As Byte
    End Structure

    <StructLayout(LayoutKind.Sequential, Pack:=1)> _
    Public Structure SyPowerTime

        Public cHour As Byte

        Public cMin As Byte
    End Structure

    <StructLayout(LayoutKind.Sequential, Pack:=1)> _
    Public Structure SyPowerManage

        Public sShutdown As SyPowerTime

        Public sPowerOn As SyPowerTime

        Public sSleep As SyPowerTime

        Public cIdleMode As Byte

        Public wIdleMin As UInt16

        'The timing when the bell rings and long
        Public cBellSec As Byte

        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=8)> _
        Public sBell() As SyPowerTime

        ' lock off key
        Public cLockPowButton As Byte
    End Structure

    <StructLayout(LayoutKind.Sequential, Pack:=1)> _
    Public Structure SySystemInfo

        Public wUserCnt As UInt16

        Public wAdminCnt As UInt16

        Public wTempCnt As UInt16

        Public wFpCnt As UInt16

        Public wPwsCnt As UInt16

        Public wCardCnt As UInt16

        Public wAttLogCnt As UInt32

        Public wAdminLogCnt As UInt32

        Public wLogCnt As UInt32

        'The remaining capacity
        Public wFpFreeNum As UInt16

        Public wPwsFreeNum As UInt16

        Public wCardFreeNum As UInt16

        Public wLogFreeNum As UInt32

        'Device Information
        Public wFpNum As UInt16

        Public wLogNum As UInt16

        Public sManuTime As SyTimeFormat

        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=13)> _
        Public cSerialNum() As Byte

        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=16)> _
        Public cManuName() As Byte

        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=8)> _
        Public cDevName() As Byte

        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=16)> _
        Public cAlgVer() As Byte

        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=24)> _
        Public cFirmwareVer() As Byte

        Public wCodingPSW As UInt32

        Public wCommonPSW As UInt16

        Public wEquipmentNum As UInt16

        Public wPowerOffKey As Byte

        Public wNewRcdOffSet As UInt16
    End Structure

    <StructLayout(LayoutKind.Sequential, Pack:=1)> _
    Public Structure SyFingerTemplate

        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=512)> _
        Public data() As Byte
    End Structure

    <StructLayout(LayoutKind.Sequential, Pack:=1)> _
    Public Structure SyFingerTemplateEx

        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=(512 + 256))> _
        Public data() As Byte
    End Structure

    <StructLayout(LayoutKind.Sequential, Pack:=1)> _
    Public Structure SyTimeSeg

        Public startHour As Byte

        Public startMin As Byte

        Public EndHour As Byte

        Public EndMin As Byte
    End Structure

    <StructLayout(LayoutKind.Sequential, Pack:=1)> _
    Public Structure SyWeekTime

        Public sSun As SyTimeSeg

        Public sMon As SyTimeSeg

        Public sTues As SyTimeSeg

        Public sWed As SyTimeSeg

        Public sThurs As SyTimeSeg

        Public sFrid As SyTimeSeg

        Public sSat As SyTimeSeg
    End Structure

    <StructLayout(LayoutKind.Sequential, Pack:=1)> _
    Public Structure SyAccessOption

        Public cLockTime As Byte

        Public cDSenDelay As Byte

        Public cDSenMode As Byte

        'Alarm Settings
        Public cAlarmHelpKey As Byte

        Public cAlarmFpTrig1 As Byte

        Public cAlarmFpTrigN As Byte

        Public cAlarmPwsTrig As Byte

        Public cAlarmDelay As Byte

        Public cAlarmFailCnt As Byte

        Public WorkingFlag As Byte

        Public WorkingTime As SyTimeSeg

        Public cAlarmTamper As Byte

        Public cCheckDoorState As Byte

        Public cAlarmIllegalDoor As Byte
    End Structure

    <StructLayout(LayoutKind.Sequential, Pack:=1)> _
    Public Structure SyGroupRestrict

        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=3)> _
        Public cValidTime() As Byte

        Public cVerifyType As Byte
    End Structure

    <StructLayout(LayoutKind.Sequential, CharSet:=CharSet.Ansi, Pack:=1)> _
    Public Structure SyUserInfo

        Public wUserID As UInt16

        Public sUserType As Byte

        Public sEnrollStatus As Byte

        Public dwHoldTime As UInt32

        Public cGroup As Byte

        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=3)> _
        Public cValidTime() As Byte

        Public cVerifyType As Byte

        Public cAccessComb As Byte

        <MarshalAs(UnmanagedType.ByValTStr, SizeConst:=16)> _
        Public name As String

        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=5)> _
        Public MbIndex() As UInt16
    End Structure

    <StructLayoutAttribute(LayoutKind.Sequential, Pack:=1)> _
    Public Structure SyUserInfoExt

        Public sUserInfo As SyUserInfo

        Public dwPws As Integer

        Public dwCardID As Integer
    End Structure

    <StructLayoutAttribute(LayoutKind.Sequential, Pack:=1)> _
    Public Structure SyLogRecord

        Public wUserID As UInt16

        Public cLogState As Byte

        Public cLogType As Byte

        Public sRecordTime As UInt32
    End Structure

    <StructLayoutAttribute(LayoutKind.Sequential, Pack:=1)> _
    Public Structure SyLogRecordEX

        Public wUserID As UInt16

        Public cLogState As Byte

        Public cLogType As Byte

        Public sRecordTime As UInt32

        Public wPos As UInt16
    End Structure

    Public Structure SyDate

        Public year As Integer

        Public month As Integer

        Public day As Integer

        Public Sub New(ByVal year As Integer, ByVal month As Integer, ByVal day As Integer)
            'MyBase.New()
            Me.year = Me.year
            Me.month = Me.month
            Me.day = Me.day
        End Sub
    End Structure

    <StructLayout(LayoutKind.Sequential, Pack:=1)> _
    Public Structure SyComCfg

        Public mPortNo As Byte

        Public mBaudRate As UInt32
    End Structure

    <StructLayout(LayoutKind.Sequential, Pack:=1)> _
    Public Structure SyNetCfg

        Public mPortNo As UInt16

        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=4)> _
        Public mIPAddr() As Byte

        Public mIsTCP As Byte
    End Structure

    <StructLayout(LayoutKind.Sequential, Pack:=1)> _
    Public Structure BELL_CELL

        Public Hour As Byte

        Public Min As Byte

        Public Sec As Byte

        Public enable As Byte
    End Structure

    '1562 BYTE
    <StructLayout(LayoutKind.Sequential, Pack:=1)> _
    Public Structure SYS_BELL

        Public BellTime As Byte

        'Alarm number - Time s
        <MarshalAs(UnmanagedType.ByValArray, SizeConst:=30)> _
        Public XinQi() As BELL_CELL
    End Structure
End Namespace