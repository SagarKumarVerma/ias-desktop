﻿Imports System.IO
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Resources
Imports System.Globalization
Imports DevExpress.XtraEditors
Imports DevExpress.LookAndFeel
Imports System.Net.Sockets
Imports System.Threading
Imports System.Net
Imports System.Text
Imports Newtonsoft.Json.Linq
Imports ULtra.AscDemo
Imports System.Runtime.InteropServices
Imports Newtonsoft.Json
Imports ULtra.EventByDeploy

'Imports System.Runtime.InteropServices

Public Class XtraRealTimePunches
    Dim TmpLED As String = ""
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    Dim ulf As UserLookAndFeel
    Dim adap As SqlDataAdapter
    Dim adapA As OleDbDataAdapter
    Dim VerifyMode As String
    Dim PunchDate As String
    Dim PunchTime As String
    Dim Cn As Common '= New Common

    'Dim deviceIp As String
    'Dim DevicePort As String
    'Dim DeviceUserName As String
    'Dim DevicePassword As String

    'Dim m_UserID As Integer = -1 'HKLOgin value
    Private lFortifyHandle As Integer = -1
    Private m_falarmData As CHCNetSDK.MSGCallBack = Nothing
    'Private m_IsapialarmData As CHCNetSDK.ISAPIMsgCallBack = Nothing
    Private Shared m_lLogNum As Integer = 0
    Private ShowData As String = Nothing

    Dim m_UserIDLS As New List(Of Integer)()
    Dim totalCount As Double = 0
    Public Sub New()
        InitializeComponent()
        Common.SetGridFont(GridView1, New Font("Tahoma", 10))
    End Sub
    Private Sub XtraRealTimePunches_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        m_UserIDLS.Clear()
        SidePanel3.Width = Me.Width * 80 / 100
        SidePanel1.Width = Me.Width * 20 / 100
        PictureEdit.Width = SidePanel1.Width * 1 / 4
        PictureEdit1.Width = SidePanel1.Width * 1 / 4
        PictureEdit.Height = SidePanel1.Height ' * 3 / 4
        PictureEdit1.Height = SidePanel1.Height ' * 3 / 4
        Application.DoEvents()
        Cn = New Common
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True
        Dim adapS As SqlDataAdapter
        Dim adapAc As OleDbDataAdapter
        Dim Rs As DataSet = New DataSet
        Dim sSql As String = "select * from tblMachine where DeviceType='HKSeries'"
        If Common.servername = "Access" Then
            adapAc = New OleDbDataAdapter(sSql, Common.con1)
            adapAc.Fill(Rs)
        Else
            adapS = New SqlDataAdapter(sSql, Common.con)
            adapS.Fill(Rs)
        End If

        If Rs.Tables(0).Rows.Count = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>No device present in Device Master</size>", "<size=9>ULtra</size>")
            Me.Close()
        End If
        For i As Integer = 0 To Rs.Tables(0).Rows.Count - 1
            Dim deviceIp As String
            Dim DevicePort As String
            Dim DeviceUserName As String
            Dim DevicePassword As String
            deviceIp = Rs.Tables(0).Rows(i)("LOCATION").ToString.Trim
            DevicePort = 8000 ' Rs.Tables(0).Rows(0)("").ToString.Trim
            DeviceUserName = Rs.Tables(0).Rows(i)("HLogin").ToString.Trim
            DevicePassword = Rs.Tables(0).Rows(i)("HPassword").ToString.Trim
            Dim m_UserID As Integer = 0
            deviceConnect(deviceIp, DeviceUserName, DevicePassword, m_UserID)
            m_UserIDLS.Add(m_UserID)
        Next
        totalCount = 0
        lblTotal.Text = "Total : 0"
        SetupLogListview()

    End Sub
    Private Sub XtraRealTimePunches_Resize(sender As System.Object, e As System.EventArgs) Handles MyBase.Resize
        SidePanel3.Width = Me.Width * 80 / 100
        'PictureEdit.Width = SidePanel1.Width
        'PictureEdit.Height = SidePanel1.Height * 3 / 4

        PictureEdit.Width = SidePanel1.Width * 1 / 4
        PictureEdit1.Width = SidePanel1.Width * 1 / 4
        PictureEdit.Height = SidePanel1.Height ' * 3 / 4
        PictureEdit1.Height = SidePanel1.Height ' * 3 / 4

    End Sub
    Private Sub XtraRealTimePunches_FormClosing(sender As System.Object, e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        If XtraMessageBox.Show(ulf, "<size=10>Are you sure to stop Real Time Monitor?</size>", "<size=9>Confirmation</size>",
                            MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
            Dim m_UserIDArr As Integer() = m_UserIDLS.ToArray
            For i As Integer = 0 To m_UserIDArr.Length - 1
                Cn.HikvisionLogOut(m_UserIDArr(i))
            Next
        Else
            e.Cancel = True
        End If

    End Sub
    Private Sub SetupLogListview()
        GridControl1.DataSource = Nothing
        Dim dt As DataTable = New DataTable
        dt.Columns.Add("No")
        dt.Columns.Add("EnrollNo")
        dt.Columns.Add("Paycode")
        dt.Columns.Add("Name")
        'dt.Columns.Add("VerifyMode")
        'dt.Columns.Add("InOut")
        dt.Columns.Add("Punch Date")
        dt.Columns.Add("Punch Time")
        dt.Columns.Add("IP/Serial No")
        'dt.Columns.Add("Port")
        dt.Columns.Add("DevID")
        dt.Columns.Add("Mask Status")
        dt.Columns.Add("IsAbnomal")
        dt.Columns.Add("Temp C")
        dt.Columns.Add("Temp F")

        Dim datase As DataSet = New DataSet()
        datase.Tables.Add(dt)
        GridControl1.DataSource = dt
        GridView1.Columns.Item(0).Width = 50
        GridView1.Columns.Item(1).Width = 100
        GridView1.Columns.Item(2).Width = 100
        GridView1.Columns.Item(6).Width = 100
        GridView1.Columns.Item(7).Width = 50
        GridView1.Columns.Item(8).Width = 100
    End Sub
    Public Sub saveattlog(ByVal EnrollNumber As String, punchtime As DateTime, LightPath As String, ThermalPath As String, MaskWear As String, TeperatureStatus As String, fCurrTemperature As String, DeviceIpfromData As String)
        Dim conS As SqlConnection
        Dim conS1 As OleDbConnection

        If Common.servername = "Access" Then
            conS1 = New OleDbConnection(Common.ConnectionString)
        Else
            conS = New SqlConnection(Common.ConnectionString)
        End If

        'Dim EnrollNumber As String = attlog.Substring(0, attlog.IndexOf("" & vbTab, 1))
        'If EnrollNumber = "" Or EnrollNumber = "0" Then
        '    Return
        'End If
        'MsgBox("EnrollNumber " & EnrollNumber & vbCrLf & "punchtime " & punchtime.ToString("yyyy-MM-dd HH:mm:ss") & vbCrLf &
        '    "LightPath:" & LightPath & vbCrLf &
        '    "ThermalPath " & ThermalPath & vbCrLf &
        '    "MaskWear " & MaskWear & vbCrLf &
        '    "TeperatureStatus " & TeperatureStatus & vbCrLf &
        '       "fCurrTemperature " & fCurrTemperature & " DeviceIpfromData " & DeviceIpfromData)

        Try
            If TeperatureStatus.Trim = "Yes" Then
                My.Computer.Audio.Play("./tempExSound.wav")
            End If
        Catch ex As Exception

        End Try

        Dim sSql As String
        Dim EnrNum As String
        If IsNumeric(EnrollNumber) Then
            EnrNum = Convert.ToInt64(EnrollNumber).ToString("000000000000")
        Else
            EnrNum = EnrollNumber
        End If
        Dim deviceId As String = ""
        Dim IN_OUT As String = ""
        Dim LocName As String = "" 'IOCL
        Dim LocCode As String = "" 'IOCL
        Dim TELEPHONE1 As String = ""
        Dim Name As String = ""
        Dim Paycode As String = ""
        Dim EId As Integer
        Dim ISROUNDTHECLOCKWORK As String = ""
        Dim VehicleNo As String = ""
        sSql = "SELECT ID_NO, IN_OUT, LEDIP from tblMachine where LOCATION='" & DeviceIpfromData & "'"
        'for IOCL
        sSql = "SELECT ID_NO, IN_OUT, branch, BRANCHCODE,LEDIP from tblMachine, tblbranch where LOCATION='" & DeviceIpfromData & "' and tblMachine.branch=tblbranch.BRANCHNAME"
        Dim ds As DataSet = New DataSet
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, conS1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, conS)
            adap.Fill(ds)
        End If
        If ds.Tables(0).Rows.Count > 0 Then
            deviceId = ds.Tables(0).Rows(0).Item("ID_NO").ToString.Trim
            IN_OUT = ds.Tables(0).Rows(0).Item("IN_OUT").ToString.Trim
            LocCode = ds.Tables(0).Rows(0).Item("BRANCHCODE").ToString.Trim
            LocName = ds.Tables(0).Rows(0).Item("branch").ToString.Trim
        End If
        If EnrNum <> "Visitor" Then
            If IN_OUT = "B" Then
                Dim ds2 As DataSet = New DataSet
                Dim sSql1 As String '= "select count (CARDNO) from MachineRawPunch where CARDNO = '" & EnrNum & "' and FORMAT(OFFICEPUNCH, 'yyyy-MM-dd') = '" & Convert.ToDateTime(atttime.ToString).ToString("yyyy-MM-dd") & "'"
                Dim adapAT As OleDbDataAdapter
                Dim adapT As SqlDataAdapter
                If Common.servername = "Access" Then
                    sSql1 = "select count (CARDNO) from MachineRawPunch where CARDNO = '" & EnrNum & "' and FORMAT(OFFICEPUNCH, 'yyyy-MM-dd') = '" & punchtime.ToString("yyyy-MM-dd") & "'"
                    adapAT = New OleDbDataAdapter(sSql1, conS1)
                    adapAT.Fill(ds2)
                Else
                    sSql1 = "select count (CARDNO) from MachineRawPunch where CARDNO = '" & EnrNum & "' and convert(varchar, OFFICEPUNCH, 23) = '" & punchtime.ToString("yyyy-MM-dd") & "'"
                    adapT = New SqlDataAdapter(sSql1, conS)
                    adapT.Fill(ds2)
                End If

                If ds2.Tables(0).Rows(0).Item(0) = 0 Then
                    IN_OUT = "I"
                Else
                    If ds2.Tables(0).Rows(0).Item(0) Mod 2 = 0 Then
                        IN_OUT = "I"
                    Else
                        IN_OUT = "O"
                    End If
                End If
            End If
            'sSql = "select * from tblemployee, EmployeeGroup where PRESENTCARDNO='" & EnrNum & "' and EmployeeGroup.GroupId=TblEmployee.EmployeeGroupId and tblemployee.ACTIVE='Y'"
            'ds = New DataSet
            'If Common.servername = "Access" Then
            '    adapA = New OleDbDataAdapter(sSql, conS1)
            '    adapA.Fill(ds)
            'Else
            '    adap = New SqlDataAdapter(sSql, conS)
            '    adap.Fill(ds)
            'End If
            'If ds.Tables(0).Rows.Count > 0 Then
            '    Name = ds.Tables(0).Rows(0)("EMPNAME").ToString.Trim
            '    Paycode = ds.Tables(0).Rows(0)("Paycode").ToString.Trim
            '    EId = ds.Tables(0).Rows(0)("Id")
            '    LocCode = ds.Tables(0).Rows(0)("COMPANYCODE").ToString.Trim
            '    TELEPHONE1 = ds.Tables(0).Rows(0)("TELEPHONE1").ToString.Trim
            '    ISROUNDTHECLOCKWORK = ds.Tables(0).Rows(0)("ISROUNDTHECLOCKWORK").ToString.Trim
            'End If
            Dim EmRow As DataRow = Common.EmpNonAdminGrp.Rows.Find(EnrNum)
            If EmRow IsNot Nothing Then
                'Name = ds.Tables(0).Rows(0).Item("EMPNAME").ToString.Trim
                Name = EmRow("EMPNAME").ToString.Trim
                Paycode = EmRow("Paycode").ToString.Trim
                EId = EmRow("Id")
                LocCode = EmRow("COMPANYCODE").ToString.Trim
                TELEPHONE1 = EmRow("TELEPHONE1").ToString.Trim
                ISROUNDTHECLOCKWORK = EmRow("ISROUNDTHECLOCKWORK").ToString.Trim
            End If
            If EnrollNumber <> "" And EnrollNumber <> "0" And EnrollNumber <> "NotAuthorized" Then
#Region "MachineRaw"
                sSql = "insert into MachineRawPunch ([CARDNO],[OFFICEPUNCH],[P_DAY],[ISMANUAL],[ReasonCode],[MC_NO],[INOUT],[PAYCODE],[Temperature],[MaskStatus],[IsAbnomal]) values('" & EnrNum & "','" & punchtime.ToString("yyyy-MM-dd HH:mm:ss") & "','N', 'N','','" & deviceId & "','" & IN_OUT & "','" & Paycode & "','" & fCurrTemperature & "','" & MaskWear & "','" & TeperatureStatus & "' ) "
                Dim sSql1 As String = "insert into MachineRawPunchAll ([CARDNO],[OFFICEPUNCH],[P_DAY],[ISMANUAL],[ReasonCode],[MC_NO],[INOUT],[PAYCODE],[Temperature],[MaskStatus],[IsAbnomal]) values('" & EnrNum & "','" & punchtime.ToString("yyyy-MM-dd HH:mm:ss") & "','N', 'N','','" & deviceId & "','" & IN_OUT & "','" & Paycode & "','" & fCurrTemperature & "','" & MaskWear & "','" & TeperatureStatus & "' ) "
                Try
                    If Common.servername = "Access" Then
                        If Common.con1.State <> ConnectionState.Open Then
                            Common.con1.Open()
                        End If
                        cmd1 = New OleDbCommand(sSql, Common.con1)
                        cmd1.ExecuteNonQuery()
                        Try
                            cmd1 = New OleDbCommand(sSql1, Common.con1)
                            cmd1.ExecuteNonQuery()
                        Catch ex As Exception

                        End Try
                        If Common.con1.State <> ConnectionState.Closed Then
                            Common.con1.Close()
                        End If
                    Else
                        If conS.State <> ConnectionState.Open Then
                            conS.Open()
                        End If
                        cmd = New SqlCommand(sSql, conS)
                        cmd.ExecuteNonQuery()
                        Try
                            cmd = New SqlCommand(sSql1, conS)
                            cmd.ExecuteNonQuery()
                        Catch ex As Exception

                        End Try
                        If conS.State <> ConnectionState.Closed Then
                            conS.Close()
                        End If
                    End If


                    'If Common.IsParallel = "Y" Then
                    '    Dim parallelThread As Thread = New Thread(Sub() Common.parallelInsert(EnrollNumber, Paycode, Convert.ToDateTime(atttime.ToString), IN_OUT, "N", Name))
                    '    parallelThread.Start()
                    '    parallelThread.IsBackground = True
                    'End If

                    'sms
                    If Common.g_SMSApplicable = "" Then
                        Common.Load_SMS_Policy()
                    End If
                    If Common.g_SMSApplicable = "Y" Then
                        If Common.g_isAllSMS = "Y" Then
                            Try
                                If ds.Tables(0).Rows(0).Item("TELEPHONE1").ToString.Trim <> "" Then

                                    Dim SmsContent As String = ""
                                    If Common.g_DeviceWiseInOut = "Y" Then
                                        If IN_OUT = "I" Then
                                            SmsContent = Common.g_InContent1 & " " & punchtime.ToString("yyyy-MM-dd HH:mm") & " " & Common.g_InContent2
                                        ElseIf IN_OUT = "O" Then
                                            SmsContent = Common.g_OutContent1 & " " & punchtime.ToString("yyyy-MM-dd HH:mm") & " " & Common.g_OutContent2
                                        End If
                                    Else
                                        SmsContent = Common.g_AllContent1 & " " & punchtime.ToString("yyyy-MM-dd HH:mm") & " " & Common.g_AllContent2
                                    End If
                                    SmsContent = Name & " " & SmsContent
                                    Cn.sendSMS(ds.Tables(0).Rows(0).Item("TELEPHONE1").ToString.Trim, SmsContent)
                                End If
                            Catch ex As Exception

                            End Try
                        End If
                    End If
                    'sms end
                Catch ex As Exception

                End Try
#End Region
                'Dim SaveTomachineRaw As Thread = New Thread(Sub() Me.SaveTomachineRawPunch(EnrNum, punchtime, deviceId, IN_OUT, Paycode, fCurrTemperature, MaskWear, TeperatureStatus, TELEPHONE1, EId, ISROUNDTHECLOCKWORK, VehicleNo, Name))
                'SaveTomachineRaw.Start()
                'SaveTomachineRaw.IsBackground = True
                SaveTomachineRawPunch(EnrNum, punchtime, deviceId, IN_OUT, Paycode, fCurrTemperature, MaskWear, TeperatureStatus, TELEPHONE1, EId, ISROUNDTHECLOCKWORK, VehicleNo, Name)
            End If
            SetText(EnrNum, Paycode, Name, VerifyMode, punchtime, DeviceIpfromData, deviceId, EId, LightPath, ThermalPath, MaskWear, TeperatureStatus, fCurrTemperature)
        Else
            'non registered punches
            'Dim SaveTomachineRaw As Thread = New Thread(Sub() SaveTomachineRawPunch(EnrNum, punchtime, deviceId, IN_OUT, "", fCurrTemperature, MaskWear, TeperatureStatus, TELEPHONE1, 0, ISROUNDTHECLOCKWORK, VehicleNo, ""))
            'SaveTomachineRaw.Start()
            'SaveTomachineRaw.IsBackground = True

            SaveTomachineRawPunch(EnrNum, punchtime, deviceId, IN_OUT, "", fCurrTemperature, MaskWear, TeperatureStatus, TELEPHONE1, 0, ISROUNDTHECLOCKWORK, VehicleNo, "")
#Region "MachineRaw"
            sSql = "insert into MachineRawPunch ([CARDNO],[OFFICEPUNCH],[MC_NO],[Temperature],[MaskStatus],[IsAbnomal]) values('Visitor','" & punchtime.ToString("yyyy-MM-dd HH:mm:ss") & "','" & deviceId & "','" & fCurrTemperature & "','" & MaskWear & "','" & TeperatureStatus & "' ) "
            Dim sSql1 As String = "insert into MachineRawPunchAll ([CARDNO],[OFFICEPUNCH],[MC_NO],[Temperature],[MaskStatus],[IsAbnomal]) values('Visitor','" & punchtime.ToString("yyyy-MM-dd HH:mm:ss") & "','" & deviceId & "','" & fCurrTemperature & "','" & MaskWear & "','" & TeperatureStatus & "' )"
            'MsgBox(sSql)
            Try
                If Common.servername = "Access" Then
                    If Common.con1.State <> ConnectionState.Open Then
                        Common.con1.Open()
                    End If
                    cmd1 = New OleDbCommand(sSql, Common.con1)
                    cmd1.ExecuteNonQuery()
                    Try
                        cmd1 = New OleDbCommand(sSql1, Common.con1)
                        cmd1.ExecuteNonQuery()
                    Catch ex As Exception

                    End Try
                    If Common.con1.State <> ConnectionState.Closed Then
                        Common.con1.Close()
                    End If
                Else
                    If conS.State <> ConnectionState.Open Then
                        conS.Open()
                    End If
                    cmd = New SqlCommand(sSql, conS)
                    cmd.ExecuteNonQuery()
                    Try
                        cmd = New SqlCommand(sSql1, conS)
                        cmd.ExecuteNonQuery()
                    Catch ex As Exception

                    End Try
                    If conS.State <> ConnectionState.Closed Then
                        conS.Close()
                    End If
                End If

            Catch ex As Exception
                'MsgBox(ex.Message)
            End Try
#End Region
            SetText(EnrNum, "", "Not registered", VerifyMode, punchtime, DeviceIpfromData, deviceId, EId, LightPath, ThermalPath, MaskWear, TeperatureStatus, fCurrTemperature)
        End If

        If Common.servername = "Access" Then
            conS1.Dispose()
        Else
            conS.Dispose()
        End If
        Return
    End Sub
    'to access grid from tread
    Public Delegate Sub SetTextCallback(EnrNum As String, Paycode As String, name As String, verifymode As String, PunchDate As DateTime, machine As String, deviceId As String, EId As Integer, LightPath As String, ThermalPath As String, MaskWear As String, TeperatureStatus As String, fCurrTemperature As String)
    Private Sub SetText(EnrNum As String, Paycode As String, name As String, verifymode As String, PunchDate As DateTime, machine As String, deviceId As String, EId As Integer, LightPath As String, ThermalPath As String, MaskWear As String, TeperatureStatus As String, fCurrTemperature As String)
        ' InvokeRequired required compares the thread ID of the
        ' calling thread to the thread ID of the creating thread.
        ' If these threads are different, it returns true.
        Try
            If Me.GridControl1.InvokeRequired Then
                Dim d As SetTextCallback = New SetTextCallback(AddressOf Me.SetText)
                Me.Invoke(d, New Object() {EnrNum, Paycode, name, verifymode, PunchDate, machine, deviceId, EId, LightPath, ThermalPath, MaskWear, TeperatureStatus, fCurrTemperature})
            Else
                Dim Ftemp As String = ""
                Dim Ctemp As String = ""
                'MsgBox(EnrNum)
                'MsgBox(fCurrTemperature)
                If fCurrTemperature.Trim <> "" Then
                    If fCurrTemperature > 80 Then
                        Ftemp = fCurrTemperature
                        Ctemp = Math.Round((fCurrTemperature - 32) * 5 / 9, 2)
                    ElseIf fCurrTemperature <= 80 Then
                        Ctemp = fCurrTemperature
                        Ftemp = Math.Round((fCurrTemperature * 9 / 5) + 32, 2)
                    ElseIf fCurrTemperature.Trim = "" Then
                        Ftemp = ""
                        Ctemp = ""
                    End If
                End If
                'Me.textBox1.Text = Text

                If GridView1.RowCount > 50 Then
                    Me.SetupLogListview()
                End If

                GridView1.AddNewRow()
                Dim rowHandle As Integer = GridView1.GetRowHandle(GridView1.DataRowCount)
                If GridView1.IsNewItemRow(rowHandle) Then

                    'Me.GridControl1.BeginInvoke(New Action(Sub()
                    '                                           GridView1.SetRowCellValue(rowHandle, GridView1.Columns(0), GridView1.RowCount)
                    '                                           GridView1.SetRowCellValue(rowHandle, GridView1.Columns(1), EnrNum)
                    '                                           GridView1.SetRowCellValue(rowHandle, GridView1.Columns(2), Paycode)
                    '                                           GridView1.SetRowCellValue(rowHandle, GridView1.Columns(3), name)
                    '                                           'GridView1.SetRowCellValue(rowHandle, GridView1.Columns(4), verifymode)
                    '                                           GridView1.SetRowCellValue(rowHandle, GridView1.Columns(4), PunchDate.ToString("dd MMM yyyy"))
                    '                                           GridView1.SetRowCellValue(rowHandle, GridView1.Columns(5), PunchDate.ToString("HH:mm"))
                    '                                           GridView1.SetRowCellValue(rowHandle, GridView1.Columns(6), machine)
                    '                                           GridView1.SetRowCellValue(rowHandle, GridView1.Columns(7), deviceId)
                    '                                           GridView1.SetRowCellValue(rowHandle, GridView1.Columns(8), MaskWear)
                    '                                           GridView1.SetRowCellValue(rowHandle, GridView1.Columns(9), TeperatureStatus)
                    '                                           GridView1.SetRowCellValue(rowHandle, GridView1.Columns(10), Ctemp)
                    '                                           GridView1.SetRowCellValue(rowHandle, GridView1.Columns(11), Ftemp)
                    '                                       End Sub))

                    GridView1.SetRowCellValue(rowHandle, GridView1.Columns(0), GridView1.RowCount)
                    GridView1.SetRowCellValue(rowHandle, GridView1.Columns(1), EnrNum)
                    GridView1.SetRowCellValue(rowHandle, GridView1.Columns(2), Paycode)
                    GridView1.SetRowCellValue(rowHandle, GridView1.Columns(3), name)
                    'GridView1.SetRowCellValue(rowHandle, GridView1.Columns(4), verifymode)
                    GridView1.SetRowCellValue(rowHandle, GridView1.Columns(4), PunchDate.ToString("dd MMM yyyy"))
                    GridView1.SetRowCellValue(rowHandle, GridView1.Columns(5), PunchDate.ToString("HH:mm"))
                    GridView1.SetRowCellValue(rowHandle, GridView1.Columns(6), machine)
                    GridView1.SetRowCellValue(rowHandle, GridView1.Columns(7), deviceId)
                    GridView1.SetRowCellValue(rowHandle, GridView1.Columns(8), MaskWear)
                    GridView1.SetRowCellValue(rowHandle, GridView1.Columns(9), TeperatureStatus)
                    GridView1.SetRowCellValue(rowHandle, GridView1.Columns(10), Ctemp)
                    GridView1.SetRowCellValue(rowHandle, GridView1.Columns(11), Ftemp)

                End If
                If System.IO.File.Exists(LightPath) Then
                    'VisitorPictureEdit.Image = Image.FromFile("./EmpImages/" & TextEdit2.Text & ".jpg")
                    PictureEdit.LoadAsync(LightPath)
                    'Me.PictureEdit.BeginInvoke(New Action(Sub()
                    '                                          PictureEdit.LoadAsync(LightPath)
                    '                                      End Sub))
                Else
                    PictureEdit.Image = Nothing
                End If
                If System.IO.File.Exists(ThermalPath) Then
                    'VisitorPictureEdit.Image = Image.FromFile("./EmpImages/" & TextEdit2.Text & ".jpg")
                    PictureEdit1.LoadAsync(ThermalPath)
                    'Me.PictureEdit1.BeginInvoke(New Action(Sub()
                    '                                           PictureEdit1.LoadAsync(ThermalPath)
                    '                                       End Sub))
                Else
                    PictureEdit1.Image = Nothing
                End If

                'lblTotal.Text = ("Total : " & GridView1.RowCount)
                totalCount = totalCount + 1
                lblTotal.Text = ("Total : " & totalCount)


                'If EnrNum.Trim <> "" Or EnrNum.Trim <> "0" Then
                '    Cn.Remove_Duplicate_Punches(PunchDate, Paycode, EId)
                '    Dim sSqltmp As String = "select TblEmployee.PAYCODE,tblEmployee.VehicleNo, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK from TblEmployee, tblEmployeeShiftMaster where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.ACTIVE = 'Y' and TblEmployee.PAYCODE = '" & Paycode & "'"
                '    Dim ds As DataSet = New DataSet
                '    If Common.servername = "Access" Then
                '        adapA = New OleDbDataAdapter(sSqltmp, Common.con1)
                '        adapA.Fill(ds)
                '    Else
                '        adap = New SqlDataAdapter(sSqltmp, Common.con)
                '        adap.Fill(ds)
                '    End If
                '    Dim vehicleNo As String
                '    If ds.Tables(0).Rows.Count > 0 Then
                '        vehicleNo = ds.Tables(0).Rows(0).Item("VehicleNo").ToString
                '        If ds.Tables(0).Rows(0).Item("ISROUNDTHECLOCKWORK").ToString = "Y" Then
                '            Cn.Process_AllRTC(PunchDate.AddDays(-1), Now.Date, Paycode, Paycode, EId)
                '        Else
                '            Cn.Process_AllnonRTC(PunchDate, Now.Date, Paycode, Paycode, EId)
                '            If Common.EmpGrpArr(EId).SHIFTTYPE = "M" Then
                '                Cn.Process_AllnonRTCMulti(PunchDate, Now.Date, Paycode, Paycode, EId)
                '            End If
                '        End If
                '        XtraMasterTest.LabelControlStatus.Text = ""
                '        Application.DoEvents()
                '    End If
                'End If

                'XtraMasterTest.LabelControlStatus.Text = ""
                'Application.DoEvents()
            End If
        Catch ex As Exception

        End Try
    End Sub
    ' Get Host IP
    Protected Function GetIP() As String
        Dim ipHost As IPHostEntry = Dns.Resolve(Dns.GetHostName)
        Dim ipAddr As IPAddress = ipHost.AddressList(0)
        Return ipAddr.ToString
    End Function
    Private Sub SimpleButtonClear_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonClear.Click
        lblTotal.Text = "Total : 0"
        Me.SetupLogListview()
        PictureEdit.Image = Nothing
        PictureEdit1.Image = Nothing
        TextEdit1.Text = ""
    End Sub
    Public Class RegionCoordinates
        Public Property positionX() As Integer
        Public Property positionY() As Integer
    End Class
    Public Class FaceTemperatureMeasurementEvent
        Public Property deviceName() As String
        Public Property serialNo() As Integer
        Public Property thermometryUnit() As String
        Public Property currTemperature() As Single
        Public Property isAbnomalTemperature() As Boolean
        Public Property RegionCoordinates() As RegionCoordinates
        Public Property remoteCheck() As Boolean
        Public Property mask() As String
        Public Property visibleLightContentID() As String
        Public Property thermalContentID() As String
    End Class
    Public Class FaceAlarmInfo
        Public Property ipAddress() As String
        Public Property ipv6Address() As String
        Public Property portNo() As Integer
        Public Property protocol() As String
        Public Property macAddress() As String
        Public Property channelID() As Integer
        Public Property dateTime() As String
        Public Property activePostCount() As Integer
        Public Property eventType() As String
        Public Property eventState() As String
        Public Property eventDescription() As String
        Public Property FaceTemperatureMeasurementEvent() As FaceTemperatureMeasurementEvent
    End Class
    Public Sub deviceConnect(DeviceAdd As String, userName As String, pwd As String, m_UserID As Integer)
        Dim struDeviceInfoV40 As CHCNetSDK.NET_DVR_DEVICEINFO_V40 = New CHCNetSDK.NET_DVR_DEVICEINFO_V40
        'Dim DeviceAdd As String = deviceIp, userName As String = DeviceUserName, pwd As String = DevicePassword
        Dim failReason As String = ""
        Dim logistatus = Cn.HikvisionLogin(DeviceAdd, userName, pwd, struDeviceInfoV40, m_UserID, failReason)
        If logistatus = False Then
            If failReason <> "" Then
                XtraMessageBox.Show(ulf, "<size=10>" & failReason & "</size>", "Failed")
                XtraMasterTest.LabelControlStatus.Text = ""
                Application.DoEvents()
                Me.Cursor = Cursors.Default
                Exit Sub
            End If
        Else
            If failReason <> "" Then
                XtraMessageBox.Show(ulf, "<size=10>" & failReason & "</size>", "Failed")
                XtraMasterTest.LabelControlStatus.Text = ""
                Application.DoEvents()
                Me.Cursor = Cursors.Default
                logistatus = Cn.HikvisionLogOut(m_UserID)
                Exit Sub
            End If
            Dim serialNo As String = System.Text.Encoding.UTF8.GetString(struDeviceInfoV40.struDeviceV30.sSerialNumber).TrimEnd(Microsoft.VisualBasic.ChrW(92))

            Dim struSetupAlarmParam As New CHCNetSDK.NET_DVR_SETUPALARM_PARAM()
            struSetupAlarmParam.dwSize = CUInt(Marshal.SizeOf(struSetupAlarmParam))
            struSetupAlarmParam.byLevel = 1
            struSetupAlarmParam.byAlarmInfoType = 1
            struSetupAlarmParam.byDeployType = CByte(0)

            If CHCNetSDK.NET_DVR_SetupAlarmChan_V41(m_UserID, struSetupAlarmParam) < 0 Then
                XtraMessageBox.Show(ulf, "<size=10>NET_DVR_SetupAlarmChan_V41 fail error: " & CHCNetSDK.NET_DVR_GetLastError() & " " & DeviceAdd & "</size>", "Setup alarm chan failed")
                Return
            Else
                'MessageBox.Show("Setup alarm chan succeed")
            End If
            m_falarmData = New CHCNetSDK.MSGCallBack(AddressOf MsgCallback)
            If CHCNetSDK.NET_DVR_SetDVRMessageCallBack_V50(0, m_falarmData, IntPtr.Zero) Then
                'MessageBox.Show("NET_DVR_SetDVRMessageCallBack_V50 succ", "succ", MessageBoxButtons.OK)
            Else
                XtraMessageBox.Show(ulf, "<size=10>NET_DVR_SetDVRMessageCallBack_V50 fail" & " " & DeviceAdd & "</size>", "operation fail")
            End If
        End If
    End Sub
    Private Sub MsgCallback(ByVal lCommand As Integer, ByRef pAlarmer As CHCNetSDK.NET_DVR_ALARMER, ByVal pAlarmInfo As IntPtr, ByVal dwBufLen As UInteger, ByVal pUser As IntPtr)
        Select Case lCommand
            Case CHCNetSDK.COMM_ALARM_ACS
                ProcessCommAlarmACS(pAlarmer, pAlarmInfo, dwBufLen, pUser)
            Case CHCNetSDK.COMM_ISAPI_ALARM
                ProcessFaceAlarmACS(pAlarmer, pAlarmInfo, dwBufLen, pUser)
            Case Else
        End Select
    End Sub
    Private Sub ProcessFaceAlarmACS(ByRef pAlarmer As CHCNetSDK.NET_DVR_ALARMER, ByVal pAlarmInfo As IntPtr, ByVal dwBufLen As UInteger, ByVal pUser As IntPtr)
        'Parse Json Alarm Info
        Dim Len As Integer = CInt(dwBufLen)
        Dim strOutJson As String = ""
        Dim szInfoBuf As String = Nothing
        Dim MaskWear As String = String.Empty
        Dim TeperatureStatus As String = String.Empty
        Dim fCurrTemperature As String = String.Empty
        Dim DeviceIpfromData As String = ""
        Dim punchTime As DateTime
        Dim struIsapiInfo As CHCNetSDK.NET_DVR_ALARM_ISAPI_INFO = DirectCast(Marshal.PtrToStructure(pAlarmInfo, GetType(CHCNetSDK.NET_DVR_ALARM_ISAPI_INFO)), CHCNetSDK.NET_DVR_ALARM_ISAPI_INFO)
        'Parse Temperature Info
        strOutJson = Marshal.PtrToStringAnsi(struIsapiInfo.pAlarmData, CInt(struIsapiInfo.dwAlarmDataLen))
        If strOutJson <> "" Then
            Dim Facert As FaceAlarmInfo = JsonConvert.DeserializeObject(Of FaceAlarmInfo)(strOutJson)
            Dim FaceEvent As FaceTemperatureMeasurementEvent = Facert.FaceTemperatureMeasurementEvent
            MaskWear = Facert.FaceTemperatureMeasurementEvent.mask
            If MaskWear = "no" Then
                MaskWear = "Without Mask"
            ElseIf MaskWear = "yes" Then
                MaskWear = "With Mask"
            End If
            TeperatureStatus = Facert.FaceTemperatureMeasurementEvent.isAbnomalTemperature
            If TeperatureStatus = "True" Then
                TeperatureStatus = "Yes"
            ElseIf TeperatureStatus = "False" Then
                TeperatureStatus = "No"
            End If
            fCurrTemperature = Facert.FaceTemperatureMeasurementEvent.currTemperature.ToString()
            DeviceIpfromData = Facert.ipAddress
            'Dim easternZone As TimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time")
            punchTime = Convert.ToDateTime(Facert.dateTime).AddMinutes(150) 'Facert.dateTime
            szInfoBuf = "[Temperature Alarm Only] " & " [IP]: " & Facert.ipAddress & " Mask: " & Facert.FaceTemperatureMeasurementEvent.mask & "  isAbnormal: " & Facert.FaceTemperatureMeasurementEvent.isAbnomalTemperature & "  CurrTemperature: " & Facert.FaceTemperatureMeasurementEvent.currTemperature.ToString()
        End If
        'Parse Picture
        If struIsapiInfo.byPicturesNumber <> 0 Then
            'parse PicData with struct
            'CHCNetSDK.NET_DVR_ALARM_ISAPI_PICDATA struPicData = new CHCNetSDK.NET_DVR_ALARM_ISAPI_PICDATA();
            Dim struPicData As CHCNetSDK.NET_DVR_ALARM_ISAPI_PICDATA = DirectCast(Marshal.PtrToStructure(struIsapiInfo.pPicPackData, GetType(CHCNetSDK.NET_DVR_ALARM_ISAPI_PICDATA)), CHCNetSDK.NET_DVR_ALARM_ISAPI_PICDATA)
            Dim path As String = Nothing

            For i As Integer = 0 To struIsapiInfo.byPicturesNumber - 1
                Dim ArrLen As Integer = 0
                Dim j As Integer = 0
                'INSTANT VB WARNING: An assignment within expression was extracted from the following statement:
                'ORIGINAL LINE: while (struPicData.szFilename[j++] != ControlChars.NullChar)
                Do While struPicData.szFilename(j) <> ControlChars.NullChar
                    j += 1
                    ArrLen += 1
                Loop
                j += 1
                Dim NewPicName(ArrLen - 1) As Char
                Array.Copy(struPicData.szFilename, NewPicName, ArrLen)
                Dim PicName As New String(NewPicName)
                'Save Pic into Local Disk

                'INSTANT VB TODO TASK: There is no VB equivalent to 'unchecked' in this context:
                'ORIGINAL LINE: Random rand = new Random(unchecked((int)DateTime.Now.Ticks));
                Dim rand As String = "_" & punchTime.ToString("yyyyMMddHHmmss")
                path = String.Format("./RealTimeImages/Light{0}.bmp", rand)
                Try
                    Using fs As New FileStream(path, FileMode.OpenOrCreate)
                        Dim iLen As Integer = CInt(struPicData.dwPicLen)
                        Dim by(iLen - 1) As Byte
                        Marshal.Copy(struPicData.pPicData, by, 0, iLen)
                        fs.Write(by, 0, iLen)
                        fs.Close()
                        fs.Dispose()
                    End Using
                Catch ex As Exception

                End Try
                szInfoBuf = szInfoBuf & "FirstPath:" & path
            Next i
            If (path <> "") Then
                saveattlog("Visitor", punchTime, path, "", MaskWear, TeperatureStatus, fCurrTemperature, DeviceIpfromData)
            End If

        End If

        'Me.listViewAlarmInfo.BeginInvoke(New Action(Sub()
        '                                                Dim Item As New ListViewItem()
        '                                                m_lLogNum += 1
        '                                                Item.Text = (m_lLogNum).ToString()
        '                                                Item.SubItems.Add(DateTime.Now.ToString())
        '                                                Item.SubItems.Add(szInfoBuf)
        '                                                Me.listViewAlarmInfo.Items.Add(Item)
        '                                            End Sub))
    End Sub
    Private Sub ProcessCommAlarmACS(ByRef pAlarmer As CHCNetSDK.NET_DVR_ALARMER, ByVal pAlarmInfo As IntPtr, ByVal dwBufLen As UInteger, ByVal pUser As IntPtr)
        Try
            Dim LightPath As String = Nothing
            Dim ThermalPath As String = Nothing
            Dim MaskWear As String = String.Empty
            Dim TeperatureStatus As String = String.Empty
            Dim fCurrTemperature As String = String.Empty
            'Dim struAcsAlarmInfo As CHCNetSDK.NET_DVR_ACS_ALARM_INFO = DirectCast(Marshal.PtrToStructure(pAlarmInfo, GetType(CHCNetSDK.NET_DVR_ACS_ALARM_INFO)), CHCNetSDK.NET_DVR_ACS_ALARM_INFO)
            Dim struAcsAlarmInfo As CHCNetSDK.NET_DVR_ACS_ALARM_INFO = CType(Marshal.PtrToStructure(pAlarmInfo, GetType(CHCNetSDK.NET_DVR_ACS_ALARM_INFO)), CHCNetSDK.NET_DVR_ACS_ALARM_INFO)
            Dim struFileInfo As New CHCNetSDK.NET_DVR_LOG_V30()
            struFileInfo.dwMajorType = struAcsAlarmInfo.dwMajor
            struFileInfo.dwMinorType = struAcsAlarmInfo.dwMinor
            Dim csTmp(255) As Char
            If CHCNetSDK.MAJOR_ALARM = struFileInfo.dwMajorType Then
                TypeMap.AlarmMinorTypeMap(struFileInfo, csTmp)
            ElseIf CHCNetSDK.MAJOR_OPERATION = struFileInfo.dwMajorType Then
                TypeMap.OperationMinorTypeMap(struFileInfo, csTmp)
            ElseIf CHCNetSDK.MAJOR_EXCEPTION = struFileInfo.dwMajorType Then
                TypeMap.ExceptionMinorTypeMap(struFileInfo, csTmp)
            ElseIf CHCNetSDK.MAJOR_EVENT = struFileInfo.dwMajorType Then
                TypeMap.EventMinorTypeMap(struFileInfo, csTmp)
            End If
            Dim szInfo As String = (New String(csTmp)).TrimEnd(ControlChars.NullChar)
            'Dim szInfoBuf As String = Nothing
            '''***********************************************
            'INSTANT VB NOTE: The variable name was renamed since Visual Basic does not handle local variables named the same as class members well:
            Dim name_Conflict As String = System.Text.Encoding.UTF8.GetString(struAcsAlarmInfo.sNetUser).TrimEnd(ControlChars.NullChar)
            For i As Integer = 0 To struAcsAlarmInfo.sNetUser.Length - 1
                If struAcsAlarmInfo.sNetUser(i) = 0 Then
                    name_Conflict = name_Conflict.Substring(0, i)
                    Exit For
                End If
            Next i

            Dim punchTime As DateTime = Convert.ToDateTime(struAcsAlarmInfo.struTime.dwYear.ToString("0000") & "-" & struAcsAlarmInfo.struTime.dwMonth.ToString("00") & "-" & struAcsAlarmInfo.struTime.dwDay.ToString("00") & " " & struAcsAlarmInfo.struTime.dwHour.ToString("00") & ":" & struAcsAlarmInfo.struTime.dwMinute.ToString("00") & ":" & struAcsAlarmInfo.struTime.dwSecond.ToString("00"))
            Dim EmpNo As String = ""
            Dim DeviceIpfromData As String = pAlarmer.sDeviceIP
            'MsgBox(DeviceIpfromData)
            'szInfoBuf = String.Format("{0} time:{1,4}-{2:D2}-{3} {4:D2}:{5:D2}:{6:D2}, [{7}]({8})", szInfo, struAcsAlarmInfo.struTime.dwYear, struAcsAlarmInfo.struTime.dwMonth, struAcsAlarmInfo.struTime.dwDay, struAcsAlarmInfo.struTime.dwHour, struAcsAlarmInfo.struTime.dwMinute, struAcsAlarmInfo.struTime.dwSecond, struAcsAlarmInfo.struRemoteHostAddr.sIpV4, name_Conflict)
            If struAcsAlarmInfo.struAcsEventInfo.byCardNo(0) <> 0 Then
                'szInfoBuf = szInfoBuf & "+Card Number:" & System.Text.Encoding.UTF8.GetString(struAcsAlarmInfo.struAcsEventInfo.byCardNo).TrimEnd(ControlChars.NullChar)
            End If
            'Dim szCardType() As String = {"normal card", "disabled card", "blacklist card", "night watch card", "stress card", "super card", "guest card"}
            Dim byCardType As Byte = struAcsAlarmInfo.struAcsEventInfo.byCardType
            'If struAcsAlarmInfo.struAcsEventInfo.dwEmployeeNo <> 0 Then
            If struAcsAlarmInfo.byAcsEventInfoExtend <> 0 AndAlso struAcsAlarmInfo.pAcsEventInfoExtend <> Nothing Then
                'szInfoBuf = szInfoBuf & " EmployeeNo:" & struAcsAlarmInfo.struAcsEventInfo.dwEmployeeNo
                Dim struExtendEventInfo As CHCNetSDK.NET_DVR_ACS_EVENT_INFO_EXTEND = CType(Marshal.PtrToStructure(struAcsAlarmInfo.pAcsEventInfoExtend, GetType(CHCNetSDK.NET_DVR_ACS_EVENT_INFO_EXTEND)), CHCNetSDK.NET_DVR_ACS_EVENT_INFO_EXTEND)
                If struExtendEventInfo.byEmployeeNo.Length <> 0 Then
                    EmpNo = System.Text.Encoding.[Default].GetString(struExtendEventInfo.byEmployeeNo).Trim(vbNullChar)
                    'szInfoBuf = szInfoBuf & " EmployeeNo:" + EmpNo
                Else
                    'szInfoBuf = szInfoBuf & " EmployeeNo: none"
                End If
            ElseIf struAcsAlarmInfo.struAcsEventInfo.dwEmployeeNo.ToString.Trim <> "" And struAcsAlarmInfo.struAcsEventInfo.dwEmployeeNo.ToString.Trim <> "0" Then
                EmpNo = struAcsAlarmInfo.struAcsEventInfo.dwEmployeeNo.ToString.Trim
            Else
                EmpNo = "Visitor"
                'Exit Sub
            End If
            If struAcsAlarmInfo.struAcsEventInfo.byDeviceNo <> 0 Then
                'szInfoBuf = szInfoBuf & " byDeviceNo:" & struAcsAlarmInfo.struAcsEventInfo.byDeviceNo.ToString()
            End If
            'Newly Add Temperature Test
            'INSTANT VB WARNING: VB does not allow comparing non-nullable value types with 'null' - they are never equal to 'null':
            'ORIGINAL LINE: if (struAcsAlarmInfo.byAcsEventInfoExtendV20 == 1 && struAcsAlarmInfo.pAcsEventInfoExtendV20 != null)
            If struAcsAlarmInfo.byAcsEventInfoExtendV20 = 1 AndAlso True Then
                'Parse Temperature Information
                Dim AcsEventInfoV20 As CHCNetSDK.NET_DVR_ACS_EVENT_INFO_EXTEND_V20 = DirectCast(Marshal.PtrToStructure(struAcsAlarmInfo.pAcsEventInfoExtendV20, GetType(CHCNetSDK.NET_DVR_ACS_EVENT_INFO_EXTEND_V20)), CHCNetSDK.NET_DVR_ACS_EVENT_INFO_EXTEND_V20)

                If struAcsAlarmInfo.struAcsEventInfo.byMask = 2 Then
                    MaskWear = "Without Mask"
                ElseIf struAcsAlarmInfo.struAcsEventInfo.byMask = 3 Then
                    MaskWear = "With Mask"
                ElseIf struAcsAlarmInfo.struAcsEventInfo.byMask = 1 OrElse struAcsAlarmInfo.struAcsEventInfo.byMask = 0 Then
                    MaskWear = "Unknow Behavior"
                End If
                If AcsEventInfoV20.byIsAbnomalTemperature = 0 Then
                    TeperatureStatus = "No"
                ElseIf AcsEventInfoV20.byIsAbnomalTemperature = 1 Then
                    TeperatureStatus = "Yes"
                End If
                fCurrTemperature = AcsEventInfoV20.fCurrTemperature.ToString()
                'szInfoBuf = szInfoBuf & " IsAbnomalTemperature: " & TeperatureStatus & " CurrTemperature: " & AcsEventInfoV20.fCurrTemperature.ToString() & " Mask: " & MaskWear

                If AcsEventInfoV20.dwVisibleLightDataLen > 0 AndAlso AcsEventInfoV20.pVisibleLightData <> IntPtr.Zero Then
                    LightPath = Nothing
                    szInfo = "VisibleLightPic"
                    'INSTANT VB TODO TASK: There is no VB equivalent to 'unchecked' in this context:
                    'ORIGINAL LINE: Random rand = new Random(unchecked((int)DateTime.Now.Ticks));
                    Dim rand As String = EmpNo & "_" & punchTime.ToString("yyyyMMddHHmmss")
                    LightPath = String.Format("./RealTimeImages/Light{0}.bmp", rand)
                    Try
                        Using fs As New FileStream(LightPath, FileMode.Create)
                            Dim iLen As Integer = CInt(AcsEventInfoV20.dwVisibleLightDataLen)
                            Dim by(iLen - 1) As Byte
                            Marshal.Copy(AcsEventInfoV20.pVisibleLightData, by, 0, iLen)
                            fs.Write(by, 0, iLen)
                            fs.Close()
                            fs.Dispose()
                        End Using
                    Catch ex As Exception

                    End Try
                    'szInfoBuf = szInfoBuf & "SavePath:" & LightPath
                End If
                If AcsEventInfoV20.dwThermalDataLen > 0 AndAlso AcsEventInfoV20.pThermalData <> IntPtr.Zero Then
                    ThermalPath = Nothing
                    szInfo = "ThermalPic"
                    'INSTANT VB TODO TASK: There is no VB equivalent to 'unchecked' in this context:
                    'ORIGINAL LINE: Random rand = new Random(unchecked((int)DateTime.Now.Ticks));
                    Dim rand As String = EmpNo & "_" & punchTime.ToString("yyyyMMddHHmmss")
                    ThermalPath = String.Format("./RealTimeImages/Thermal{0}.bmp", rand)
                    Try
                        Using fs As New FileStream(ThermalPath, FileMode.Create)
                            Dim iLen As Integer = CInt(AcsEventInfoV20.dwThermalDataLen)
                            Dim by(iLen - 1) As Byte
                            Marshal.Copy(AcsEventInfoV20.pThermalData, by, 0, iLen)
                            fs.Write(by, 0, iLen)
                            fs.Close()
                            fs.Dispose()
                        End Using
                    Catch ex As Exception

                    End Try

                    'szInfoBuf = szInfoBuf & "FirstPath:" & ThermalPath
                End If


            End If

            If struAcsAlarmInfo.dwPicDataLen > 0 Then
                LightPath = Nothing
                szInfo = "VisibleLightPic"
                'INSTANT VB TODO TASK: There is no VB equivalent to 'unchecked' in this context:
                'ORIGINAL LINE: Random rand = new Random(unchecked((int)DateTime.Now.Ticks));
                'Dim rand As New Random(CLng(DateTime.Now.Ticks))
                Dim rand As String = EmpNo & "_" & punchTime.ToString("yyyyMMddHHmmss")
                LightPath = String.Format("./RealTimeImages/Light{0}.bmp", rand)
                Try
                    Using fs As New FileStream(LightPath, FileMode.Create)
                        Dim iLen As Integer = CInt(struAcsAlarmInfo.dwPicDataLen)
                        Dim by(iLen - 1) As Byte
                        Marshal.Copy(struAcsAlarmInfo.pPicData, by, 0, iLen)
                        fs.Write(by, 0, iLen)
                        fs.Close()
                        fs.Dispose()

                    End Using
                Catch ex As Exception

                End Try

                'szInfoBuf = szInfoBuf & vbLf & "SecondPath:" & LightPath
            End If

            Thread.Sleep(50)
            If MaskWear = "Unknow Behavior" Then
                Return
            End If
            If EmpNo = "" Then
                EmpNo = "Visitor"
            End If
            If EmpNo = "Visitor" Then
                If (LightPath <> "" Or ThermalPath <> "") Then
                    saveattlog(EmpNo, punchTime, LightPath, ThermalPath, MaskWear, TeperatureStatus, fCurrTemperature, DeviceIpfromData)
                End If
            ElseIf EmpNo <> "Visitor" Then
                saveattlog(EmpNo, punchTime, LightPath, ThermalPath, MaskWear, TeperatureStatus, fCurrTemperature, DeviceIpfromData)
            End If
            'Me.listViewAlarmInfo.BeginInvoke(New Action(Sub()
            '                                                Dim Item As New ListViewItem()
            '                                                m_lLogNum += 1
            '                                                Item.Text = (m_lLogNum).ToString()
            '                                                Item.SubItems.Add(DateTime.Now.ToString())
            '                                                Item.SubItems.Add(szInfoBuf)
            '                                                Me.listViewAlarmInfo.Items.Add(Item)
            '                                            End Sub))
        Catch ex As Exception

        End Try
    End Sub
    Private Sub GridView1_RowStyle(sender As Object, e As DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs) Handles GridView1.RowStyle
        e.Appearance.ForeColor = Color.Green
        Try
            Dim quantity As String = GridView1.GetRowCellValue(e.RowHandle, "Mask Status")
            If quantity.Trim.ToUpper = "WITHOUT MASK" Then
                e.Appearance.ForeColor = Color.OrangeRed
                'Else
                '    e.Appearance.BackColor = Color.LightGreen
            End If
            e.HighPriority = True   'override any other formatting
        Catch ex As Exception

        End Try

        Try
            Dim quantity As String = GridView1.GetRowCellValue(e.RowHandle, "IsAbnomal")
            If quantity.Trim.ToUpper = "YES" Then
                e.Appearance.ForeColor = Color.Red
                'Else
                '    e.Appearance.BackColor = Color.LightGreen
            End If
            e.HighPriority = True   'override any other formatting
        Catch ex As Exception

        End Try

    End Sub
    Private Sub SaveTomachineRawPunch(EnrNum As String, PunchTime As DateTime, deviceId As String, IN_OUT As String, Paycode As String, fCurrTemperature As String, MaskWear As String, TeperatureStatus As String, TELEPHONE1 As String, EId As Integer, ISROUNDTHECLOCKWORK As String, VehicleNo As String, Name As String)
        Thread.Sleep(50)
        Try
            Dim conS As SqlConnection
            Dim conS1 As OleDbConnection
            If Common.servername = "Access" Then
                conS1 = New OleDbConnection(Common.ConnectionString)
            Else
                conS = New SqlConnection(Common.ConnectionString)
            End If

            Dim sSql As String
            Dim sSql1 As String
            'If EnrNum = "Visitor" Then
            '    sSql = "insert into MachineRawPunch ([CARDNO],[OFFICEPUNCH],[MC_NO],[Temperature],[MaskStatus],[IsAbnomal]) values('Visitor','" & PunchTime.ToString("yyyy-MM-dd HH:mm:ss") & "','" & deviceId & "','" & fCurrTemperature & "','" & MaskWear & "','" & TeperatureStatus & "' ) "
            '    sSql1 = "insert into MachineRawPunchAll ([CARDNO],[OFFICEPUNCH],[MC_NO],[Temperature],[MaskStatus],[IsAbnomal]) values('Visitor','" & PunchTime.ToString("yyyy-MM-dd HH:mm:ss") & "','" & deviceId & "','" & fCurrTemperature & "','" & MaskWear & "','" & TeperatureStatus & "' )"
            'Else
            '    sSql = "insert into MachineRawPunch ([CARDNO],[OFFICEPUNCH],[P_DAY],[ISMANUAL],[ReasonCode],[MC_NO],[INOUT],[PAYCODE],[Temperature],[MaskStatus],[IsAbnomal]) values('" & EnrNum & "','" & PunchTime.ToString("yyyy-MM-dd HH:mm:ss") & "','N', 'N','','" & deviceId & "','" & IN_OUT & "','" & Paycode & "','" & fCurrTemperature & "','" & MaskWear & "','" & TeperatureStatus & "' ) "
            '    sSql1 = "insert into MachineRawPunchAll ([CARDNO],[OFFICEPUNCH],[P_DAY],[ISMANUAL],[ReasonCode],[MC_NO],[INOUT],[PAYCODE],[Temperature],[MaskStatus],[IsAbnomal]) values('" & EnrNum & "','" & PunchTime.ToString("yyyy-MM-dd HH:mm:ss") & "','N', 'N','','" & deviceId & "','" & IN_OUT & "','" & Paycode & "','" & fCurrTemperature & "','" & MaskWear & "','" & TeperatureStatus & "' ) "
            'End If
            Try
                'If Common.servername = "Access" Then
                '    If conS1.State <> ConnectionState.Open Then
                '        conS1.Open()
                '    End If
                '    cmd1 = New OleDbCommand(sSql, conS1)
                '    cmd1.ExecuteNonQuery()
                '    Try
                '        cmd1 = New OleDbCommand(sSql1, conS1)
                '        cmd1.ExecuteNonQuery()
                '    Catch ex As Exception

                '    End Try
                '    If conS1.State <> ConnectionState.Closed Then
                '        conS1.Close()
                '        'conS1.Dispose()
                '    End If
                'Else
                '    If conS.State <> ConnectionState.Open Then
                '        conS.Open()
                '    End If
                '    cmd = New SqlCommand(sSql, conS)
                '    cmd.ExecuteNonQuery()
                '    Try
                '        cmd = New SqlCommand(sSql1, conS)
                '        cmd.ExecuteNonQuery()
                '    Catch ex As Exception

                '    End Try
                '    If conS.State <> ConnectionState.Closed Then
                '        conS.Close()
                '        'conS.Dispose()
                '    End If
                'End If


                If Common.IsParallel = "Y" Then
                    Common.parallelInsert(EnrNum, Paycode, PunchTime, IN_OUT, "N", Name, deviceId, fCurrTemperature)
                End If


                If EnrNum.Trim <> "" And EnrNum.Trim <> "0" And EnrNum.Trim <> "Visitor" Then
                    Cn.Remove_Duplicate_Punches(PunchTime, Paycode, EId)
                    'Dim sSqltmp As String = "select TblEmployee.PAYCODE,tblEmployee.VehicleNo, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK from TblEmployee, tblEmployeeShiftMaster where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.ACTIVE = 'Y' and TblEmployee.PAYCODE = '" & Paycode & "'"
                    'Dim ds As DataSet = New DataSet
                    'If Common.servername = "Access" Then
                    '    adapA = New OleDbDataAdapter(sSqltmp, Common.con1)
                    '    adapA.Fill(ds)
                    'Else
                    '    adap = New SqlDataAdapter(sSqltmp, Common.con)
                    '    adap.Fill(ds)
                    'End If

                    'If ds.Tables(0).Rows.Count > 0 Then
                    '    vehicleNo = ds.Tables(0).Rows(0).Item("VehicleNo").ToString
                    If ISROUNDTHECLOCKWORK = "Y" Then
                        Cn.Process_AllRTC(PunchTime.AddDays(-1), Now.Date, Paycode, Paycode, EId)
                    Else
                        Cn.Process_AllnonRTC(PunchTime, Now.Date, Paycode, Paycode, EId)
                        If Common.EmpGrpArr(EId).SHIFTTYPE = "M" Then
                            Cn.Process_AllnonRTCMulti(PunchTime, Now.Date, Paycode, Paycode, EId)
                        End If
                    End If
                    XtraMasterTest.LabelControlStatus.Text = ""
                    Application.DoEvents()
                    'End If
                End If


                'sms
                If Common.g_SMSApplicable = "" Then
                    Common.Load_SMS_Policy()
                End If
                If Common.g_SMSApplicable = "Y" Then
                    If Common.g_isAllSMS = "Y" Then
                        Try
                            If TELEPHONE1 <> "" Then
                                Dim SmsContent As String = ""
                                If Common.g_DeviceWiseInOut = "Y" Then
                                    If IN_OUT = "I" Then
                                        SmsContent = Common.g_InContent1 & " " & PunchTime.ToString("yyyy-MM-dd HH:mm") & " " & Common.g_InContent2
                                    ElseIf IN_OUT = "O" Then
                                        SmsContent = Common.g_OutContent1 & " " & PunchTime.ToString("yyyy-MM-dd HH:mm") & " " & Common.g_OutContent2
                                    End If
                                Else
                                    SmsContent = Common.g_AllContent1 & " " & PunchTime.ToString("yyyy-MM-dd HH:mm") & " " & Common.g_AllContent2
                                End If
                                SmsContent = Name & " " & SmsContent
                                Cn.sendSMS(TELEPHONE1, SmsContent)
                            End If
                        Catch ex As Exception

                        End Try
                    End If
                End If

                If Common.servername = "Access" Then
                    conS1.Dispose()
                Else
                    conS.Dispose()
                End If
                'sms end
            Catch ex As Exception
                'MsgBox(ex.Message & " 1018 ")
                If Common.servername = "Access" Then
                    conS1.Dispose()
                Else
                    conS.Dispose()
                End If
            End Try
        Catch ex As Exception

        End Try
    End Sub
End Class
