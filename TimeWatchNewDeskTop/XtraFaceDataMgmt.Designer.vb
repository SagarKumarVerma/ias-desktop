﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraFaceDataMgmt
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim GridLevelNode1 As DevExpress.XtraGrid.GridLevelNode = New DevExpress.XtraGrid.GridLevelNode()
        Dim GridLevelNode2 As DevExpress.XtraGrid.GridLevelNode = New DevExpress.XtraGrid.GridLevelNode()
        Me.SSSDBDataSet = New iAS.SSSDBDataSet()
        Me.TblMachineBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TblMachineTableAdapter = New iAS.SSSDBDataSetTableAdapters.tblMachineTableAdapter()
        Me.TblMachine1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.tblMachine1TableAdapter()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colID_NO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLOCATION = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colbranch = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDeviceType = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colA_R = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colcommkey = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.GridControl2 = New DevExpress.XtraGrid.GridControl()
        Me.FptableBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colEMachineNumber = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colEnrollNumber = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colUserName = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colFingerNumber = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPrivilege = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPassword = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colTemplate = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCardnumber = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colcalid = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colauthority = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colcheck_type = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colopendoor_type = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colface_Data1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colface_data2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colface_data3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colface_data4 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colface_data5 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colface_data6 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colface_data7 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colface_data8 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colface_data9 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colface_data10 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colface_data11 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colface_data12 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colface_data13 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colface_data14 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colface_data15 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colface_data16 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colface_data17 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colface_data18 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colTemplate_Face = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colTemplate_Tw = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVerifyMode = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colHTemplatePath = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.FptableTableAdapter = New iAS.SSSDBDataSetTableAdapters.fptableTableAdapter()
        Me.Fptable1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.fptable1TableAdapter()
        Me.btnDownloadFinger = New DevExpress.XtraEditors.SimpleButton()
        Me.ToggleDownloadAll = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.TextSelectUser = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.btnUploadFinger = New DevExpress.XtraEditors.SimpleButton()
        Me.btnDeleteFrmDB = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.ToggleMakeAdmin = New DevExpress.XtraEditors.ToggleSwitch()
        Me.btnDeleteFrmDevice = New DevExpress.XtraEditors.SimpleButton()
        Me.btnClearAdmin = New DevExpress.XtraEditors.SimpleButton()
        Me.GroupControl3 = New DevExpress.XtraEditors.GroupControl()
        Me.CheckFP = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckFace = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.ToggleCreateEmp = New DevExpress.XtraEditors.ToggleSwitch()
        Me.GroupControl4 = New DevExpress.XtraEditors.GroupControl()
        Me.GroupControl5 = New DevExpress.XtraEditors.GroupControl()
        Me.btnLockClose = New DevExpress.XtraEditors.SimpleButton()
        Me.btnLockOpen = New DevExpress.XtraEditors.SimpleButton()
        Me.TextPassword = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.btnClearDeviceData = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblMachineBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FptableBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleDownloadAll.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextSelectUser.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleMakeAdmin.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.CheckFP.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckFace.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleCreateEmp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl4.SuspendLayout()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl5.SuspendLayout()
        CType(Me.TextPassword.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SSSDBDataSet
        '
        Me.SSSDBDataSet.DataSetName = "SSSDBDataSet"
        Me.SSSDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'TblMachineBindingSource
        '
        Me.TblMachineBindingSource.DataMember = "tblMachine"
        Me.TblMachineBindingSource.DataSource = Me.SSSDBDataSet
        '
        'TblMachineTableAdapter
        '
        Me.TblMachineTableAdapter.ClearBeforeFill = True
        '
        'TblMachine1TableAdapter1
        '
        Me.TblMachine1TableAdapter1.ClearBeforeFill = True
        '
        'GroupControl1
        '
        Me.GroupControl1.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl1.AppearanceCaption.Options.UseFont = True
        Me.GroupControl1.Controls.Add(Me.GridControl1)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 12)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(584, 250)
        Me.GroupControl1.TabIndex = 16
        Me.GroupControl1.Text = "Device List"
        '
        'GridControl1
        '
        Me.GridControl1.DataSource = Me.TblMachineBindingSource
        Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControl1.EmbeddedNavigator.Buttons.Append.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.Edit.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.Remove.Visible = False
        GridLevelNode1.RelationName = "Level1"
        Me.GridControl1.LevelTree.Nodes.AddRange(New DevExpress.XtraGrid.GridLevelNode() {GridLevelNode1})
        Me.GridControl1.Location = New System.Drawing.Point(2, 23)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(580, 225)
        Me.GridControl1.TabIndex = 1
        Me.GridControl1.UseEmbeddedNavigator = True
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colID_NO, Me.colLOCATION, Me.colbranch, Me.colDeviceType, Me.colA_R, Me.colcommkey, Me.GridColumn2})
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.Editable = False
        Me.GridView1.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridView1.OptionsSelection.MultiSelect = True
        Me.GridView1.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        Me.GridView1.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colID_NO, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colID_NO
        '
        Me.colID_NO.Caption = "Controller Id"
        Me.colID_NO.FieldName = "ID_NO"
        Me.colID_NO.Name = "colID_NO"
        Me.colID_NO.Visible = True
        Me.colID_NO.VisibleIndex = 1
        '
        'colLOCATION
        '
        Me.colLOCATION.Caption = "Device IP"
        Me.colLOCATION.FieldName = "LOCATION"
        Me.colLOCATION.Name = "colLOCATION"
        Me.colLOCATION.Visible = True
        Me.colLOCATION.VisibleIndex = 2
        '
        'colbranch
        '
        Me.colbranch.Caption = "Location"
        Me.colbranch.FieldName = "branch"
        Me.colbranch.Name = "colbranch"
        Me.colbranch.Visible = True
        Me.colbranch.VisibleIndex = 3
        '
        'colDeviceType
        '
        Me.colDeviceType.Caption = "Device Type"
        Me.colDeviceType.FieldName = "DeviceType"
        Me.colDeviceType.Name = "colDeviceType"
        '
        'colA_R
        '
        Me.colA_R.FieldName = "A_R"
        Me.colA_R.Name = "colA_R"
        '
        'colcommkey
        '
        Me.colcommkey.Caption = "GridColumn2"
        Me.colcommkey.FieldName = "commkey"
        Me.colcommkey.Name = "colcommkey"
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "MAC_ADDRESS"
        Me.GridColumn2.FieldName = "MAC_ADDRESS"
        Me.GridColumn2.Name = "GridColumn2"
        '
        'GroupControl2
        '
        Me.GroupControl2.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl2.AppearanceCaption.Options.UseFont = True
        Me.GroupControl2.Controls.Add(Me.GridControl2)
        Me.GroupControl2.Location = New System.Drawing.Point(12, 268)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(584, 250)
        Me.GroupControl2.TabIndex = 17
        Me.GroupControl2.Text = "Template Details"
        '
        'GridControl2
        '
        Me.GridControl2.DataSource = Me.FptableBindingSource
        Me.GridControl2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControl2.EmbeddedNavigator.Buttons.Append.Visible = False
        Me.GridControl2.EmbeddedNavigator.Buttons.CancelEdit.Visible = False
        Me.GridControl2.EmbeddedNavigator.Buttons.Edit.Visible = False
        Me.GridControl2.EmbeddedNavigator.Buttons.EndEdit.Visible = False
        Me.GridControl2.EmbeddedNavigator.Buttons.Remove.Visible = False
        GridLevelNode2.RelationName = "Level1"
        Me.GridControl2.LevelTree.Nodes.AddRange(New DevExpress.XtraGrid.GridLevelNode() {GridLevelNode2})
        Me.GridControl2.Location = New System.Drawing.Point(2, 23)
        Me.GridControl2.MainView = Me.GridView2
        Me.GridControl2.Name = "GridControl2"
        Me.GridControl2.Size = New System.Drawing.Size(580, 225)
        Me.GridControl2.TabIndex = 1
        Me.GridControl2.UseEmbeddedNavigator = True
        Me.GridControl2.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView2})
        '
        'FptableBindingSource
        '
        Me.FptableBindingSource.DataMember = "fptable"
        Me.FptableBindingSource.DataSource = Me.SSSDBDataSet
        '
        'GridView2
        '
        Me.GridView2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colEMachineNumber, Me.colEnrollNumber, Me.colUserName, Me.colFingerNumber, Me.GridColumn1, Me.colPrivilege, Me.colPassword, Me.colTemplate, Me.colCardnumber, Me.colcalid, Me.colauthority, Me.colcheck_type, Me.colopendoor_type, Me.colface_Data1, Me.colface_data2, Me.colface_data3, Me.colface_data4, Me.colface_data5, Me.colface_data6, Me.colface_data7, Me.colface_data8, Me.colface_data9, Me.colface_data10, Me.colface_data11, Me.colface_data12, Me.colface_data13, Me.colface_data14, Me.colface_data15, Me.colface_data16, Me.colface_data17, Me.colface_data18, Me.colTemplate_Face, Me.colTemplate_Tw, Me.colVerifyMode, Me.colHTemplatePath, Me.GridColumn3})
        Me.GridView2.GridControl = Me.GridControl2
        Me.GridView2.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always
        Me.GridView2.Name = "GridView2"
        Me.GridView2.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView2.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView2.OptionsBehavior.Editable = False
        Me.GridView2.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridView2.OptionsSelection.MultiSelect = True
        Me.GridView2.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        Me.GridView2.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colEnrollNumber, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colEMachineNumber
        '
        Me.colEMachineNumber.Caption = "Controler Id"
        Me.colEMachineNumber.FieldName = "EMachineNumber"
        Me.colEMachineNumber.Name = "colEMachineNumber"
        Me.colEMachineNumber.Visible = True
        Me.colEMachineNumber.VisibleIndex = 1
        '
        'colEnrollNumber
        '
        Me.colEnrollNumber.FieldName = "EnrollNumber"
        Me.colEnrollNumber.Name = "colEnrollNumber"
        Me.colEnrollNumber.Visible = True
        Me.colEnrollNumber.VisibleIndex = 2
        Me.colEnrollNumber.Width = 120
        '
        'colUserName
        '
        Me.colUserName.FieldName = "UserName"
        Me.colUserName.Name = "colUserName"
        Me.colUserName.Visible = True
        Me.colUserName.VisibleIndex = 3
        Me.colUserName.Width = 120
        '
        'colFingerNumber
        '
        Me.colFingerNumber.Caption = "Template Type"
        Me.colFingerNumber.FieldName = "FingerNumber"
        Me.colFingerNumber.Name = "colFingerNumber"
        Me.colFingerNumber.Visible = True
        Me.colFingerNumber.VisibleIndex = 4
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Device Type"
        Me.GridColumn1.FieldName = "GridColumn1"
        Me.GridColumn1.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
        Me.GridColumn1.OptionsFilter.FilterBySortField = DevExpress.Utils.DefaultBoolean.[True]
        Me.GridColumn1.SortMode = DevExpress.XtraGrid.ColumnSortMode.DisplayText
        Me.GridColumn1.UnboundType = DevExpress.Data.UnboundColumnType.[String]
        Me.GridColumn1.Width = 100
        '
        'colPrivilege
        '
        Me.colPrivilege.FieldName = "Privilege"
        Me.colPrivilege.Name = "colPrivilege"
        '
        'colPassword
        '
        Me.colPassword.FieldName = "Password"
        Me.colPassword.Name = "colPassword"
        '
        'colTemplate
        '
        Me.colTemplate.FieldName = "Template"
        Me.colTemplate.Name = "colTemplate"
        '
        'colCardnumber
        '
        Me.colCardnumber.FieldName = "Cardnumber"
        Me.colCardnumber.Name = "colCardnumber"
        '
        'colcalid
        '
        Me.colcalid.FieldName = "calid"
        Me.colcalid.Name = "colcalid"
        '
        'colauthority
        '
        Me.colauthority.FieldName = "authority"
        Me.colauthority.Name = "colauthority"
        '
        'colcheck_type
        '
        Me.colcheck_type.FieldName = "check_type"
        Me.colcheck_type.Name = "colcheck_type"
        '
        'colopendoor_type
        '
        Me.colopendoor_type.FieldName = "opendoor_type"
        Me.colopendoor_type.Name = "colopendoor_type"
        '
        'colface_Data1
        '
        Me.colface_Data1.FieldName = "face_Data1"
        Me.colface_Data1.Name = "colface_Data1"
        '
        'colface_data2
        '
        Me.colface_data2.FieldName = "face_data2"
        Me.colface_data2.Name = "colface_data2"
        '
        'colface_data3
        '
        Me.colface_data3.FieldName = "face_data3"
        Me.colface_data3.Name = "colface_data3"
        '
        'colface_data4
        '
        Me.colface_data4.FieldName = "face_data4"
        Me.colface_data4.Name = "colface_data4"
        '
        'colface_data5
        '
        Me.colface_data5.FieldName = "face_data5"
        Me.colface_data5.Name = "colface_data5"
        '
        'colface_data6
        '
        Me.colface_data6.FieldName = "face_data6"
        Me.colface_data6.Name = "colface_data6"
        '
        'colface_data7
        '
        Me.colface_data7.FieldName = "face_data7"
        Me.colface_data7.Name = "colface_data7"
        '
        'colface_data8
        '
        Me.colface_data8.FieldName = "face_data8"
        Me.colface_data8.Name = "colface_data8"
        '
        'colface_data9
        '
        Me.colface_data9.FieldName = "face_data9"
        Me.colface_data9.Name = "colface_data9"
        '
        'colface_data10
        '
        Me.colface_data10.FieldName = "face_data10"
        Me.colface_data10.Name = "colface_data10"
        '
        'colface_data11
        '
        Me.colface_data11.FieldName = "face_data11"
        Me.colface_data11.Name = "colface_data11"
        '
        'colface_data12
        '
        Me.colface_data12.FieldName = "face_data12"
        Me.colface_data12.Name = "colface_data12"
        '
        'colface_data13
        '
        Me.colface_data13.FieldName = "face_data13"
        Me.colface_data13.Name = "colface_data13"
        '
        'colface_data14
        '
        Me.colface_data14.FieldName = "face_data14"
        Me.colface_data14.Name = "colface_data14"
        '
        'colface_data15
        '
        Me.colface_data15.FieldName = "face_data15"
        Me.colface_data15.Name = "colface_data15"
        '
        'colface_data16
        '
        Me.colface_data16.FieldName = "face_data16"
        Me.colface_data16.Name = "colface_data16"
        '
        'colface_data17
        '
        Me.colface_data17.FieldName = "face_data17"
        Me.colface_data17.Name = "colface_data17"
        '
        'colface_data18
        '
        Me.colface_data18.FieldName = "face_data18"
        Me.colface_data18.Name = "colface_data18"
        '
        'colTemplate_Face
        '
        Me.colTemplate_Face.FieldName = "Template_Face"
        Me.colTemplate_Face.Name = "colTemplate_Face"
        '
        'colTemplate_Tw
        '
        Me.colTemplate_Tw.FieldName = "Template_Tw"
        Me.colTemplate_Tw.Name = "colTemplate_Tw"
        '
        'colVerifyMode
        '
        Me.colVerifyMode.FieldName = "VerifyMode"
        Me.colVerifyMode.Name = "colVerifyMode"
        '
        'colHTemplatePath
        '
        Me.colHTemplatePath.Caption = "HTemplatePath"
        Me.colHTemplatePath.FieldName = "HTemplatePath"
        Me.colHTemplatePath.Name = "colHTemplatePath"
        '
        'GridColumn3
        '
        Me.GridColumn3.Caption = "Controler Id"
        Me.GridColumn3.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText
        Me.GridColumn3.Name = "GridColumn3"
        '
        'FptableTableAdapter
        '
        Me.FptableTableAdapter.ClearBeforeFill = True
        '
        'Fptable1TableAdapter1
        '
        Me.Fptable1TableAdapter1.ClearBeforeFill = True
        '
        'btnDownloadFinger
        '
        Me.btnDownloadFinger.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.btnDownloadFinger.Appearance.Options.UseFont = True
        Me.btnDownloadFinger.Location = New System.Drawing.Point(21, 115)
        Me.btnDownloadFinger.Name = "btnDownloadFinger"
        Me.btnDownloadFinger.Size = New System.Drawing.Size(148, 23)
        Me.btnDownloadFinger.TabIndex = 18
        Me.btnDownloadFinger.Text = "Download Templates"
        '
        'ToggleDownloadAll
        '
        Me.ToggleDownloadAll.Location = New System.Drawing.Point(189, 29)
        Me.ToggleDownloadAll.Name = "ToggleDownloadAll"
        Me.ToggleDownloadAll.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleDownloadAll.Properties.Appearance.Options.UseFont = True
        Me.ToggleDownloadAll.Properties.OffText = "No"
        Me.ToggleDownloadAll.Properties.OnText = "Yes"
        Me.ToggleDownloadAll.Size = New System.Drawing.Size(95, 25)
        Me.ToggleDownloadAll.TabIndex = 19
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Location = New System.Drawing.Point(5, 63)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(65, 16)
        Me.LabelControl1.TabIndex = 20
        Me.LabelControl1.Text = "Select User"
        '
        'TextSelectUser
        '
        Me.TextSelectUser.Location = New System.Drawing.Point(147, 60)
        Me.TextSelectUser.Name = "TextSelectUser"
        Me.TextSelectUser.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextSelectUser.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextSelectUser.Properties.MaxLength = 15
        Me.TextSelectUser.Size = New System.Drawing.Size(113, 20)
        Me.TextSelectUser.TabIndex = 21
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.LabelControl2.Appearance.Options.UseFont = True
        Me.LabelControl2.Location = New System.Drawing.Point(5, 34)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(74, 16)
        Me.LabelControl2.TabIndex = 22
        Me.LabelControl2.Text = "Download All"
        '
        'btnUploadFinger
        '
        Me.btnUploadFinger.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.btnUploadFinger.Appearance.Options.UseFont = True
        Me.btnUploadFinger.Location = New System.Drawing.Point(21, 73)
        Me.btnUploadFinger.Name = "btnUploadFinger"
        Me.btnUploadFinger.Size = New System.Drawing.Size(148, 23)
        Me.btnUploadFinger.TabIndex = 23
        Me.btnUploadFinger.Text = "Upload Templates"
        '
        'btnDeleteFrmDB
        '
        Me.btnDeleteFrmDB.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.btnDeleteFrmDB.Appearance.Options.UseFont = True
        Me.btnDeleteFrmDB.Location = New System.Drawing.Point(8, 66)
        Me.btnDeleteFrmDB.Name = "btnDeleteFrmDB"
        Me.btnDeleteFrmDB.Size = New System.Drawing.Size(148, 23)
        Me.btnDeleteFrmDB.TabIndex = 24
        Me.btnDeleteFrmDB.Text = "Delete User From DB"
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.LabelControl3.Appearance.Options.UseFont = True
        Me.LabelControl3.Location = New System.Drawing.Point(9, 38)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(70, 16)
        Me.LabelControl3.TabIndex = 26
        Me.LabelControl3.Text = "Make Admin"
        Me.LabelControl3.Visible = False
        '
        'ToggleMakeAdmin
        '
        Me.ToggleMakeAdmin.Location = New System.Drawing.Point(95, 33)
        Me.ToggleMakeAdmin.Name = "ToggleMakeAdmin"
        Me.ToggleMakeAdmin.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleMakeAdmin.Properties.Appearance.Options.UseFont = True
        Me.ToggleMakeAdmin.Properties.OffText = "No"
        Me.ToggleMakeAdmin.Properties.OnText = "Yes"
        Me.ToggleMakeAdmin.Size = New System.Drawing.Size(95, 25)
        Me.ToggleMakeAdmin.TabIndex = 25
        Me.ToggleMakeAdmin.Visible = False
        '
        'btnDeleteFrmDevice
        '
        Me.btnDeleteFrmDevice.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.btnDeleteFrmDevice.Appearance.Options.UseFont = True
        Me.btnDeleteFrmDevice.Location = New System.Drawing.Point(8, 37)
        Me.btnDeleteFrmDevice.Name = "btnDeleteFrmDevice"
        Me.btnDeleteFrmDevice.Size = New System.Drawing.Size(148, 23)
        Me.btnDeleteFrmDevice.TabIndex = 27
        Me.btnDeleteFrmDevice.Text = "Delete User From Device"
        '
        'btnClearAdmin
        '
        Me.btnClearAdmin.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.btnClearAdmin.Appearance.Options.UseFont = True
        Me.btnClearAdmin.Location = New System.Drawing.Point(8, 95)
        Me.btnClearAdmin.Name = "btnClearAdmin"
        Me.btnClearAdmin.Size = New System.Drawing.Size(148, 23)
        Me.btnClearAdmin.TabIndex = 28
        Me.btnClearAdmin.Text = "Clear Admin"
        Me.btnClearAdmin.Visible = False
        '
        'GroupControl3
        '
        Me.GroupControl3.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl3.AppearanceCaption.Options.UseFont = True
        Me.GroupControl3.Controls.Add(Me.CheckFP)
        Me.GroupControl3.Controls.Add(Me.CheckFace)
        Me.GroupControl3.Controls.Add(Me.LabelControl4)
        Me.GroupControl3.Controls.Add(Me.ToggleCreateEmp)
        Me.GroupControl3.Controls.Add(Me.LabelControl2)
        Me.GroupControl3.Controls.Add(Me.ToggleDownloadAll)
        Me.GroupControl3.Controls.Add(Me.LabelControl1)
        Me.GroupControl3.Controls.Add(Me.TextSelectUser)
        Me.GroupControl3.Controls.Add(Me.btnDownloadFinger)
        Me.GroupControl3.Location = New System.Drawing.Point(602, 12)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(342, 151)
        Me.GroupControl3.TabIndex = 29
        Me.GroupControl3.Text = "Download"
        '
        'CheckFP
        '
        Me.CheckFP.Location = New System.Drawing.Point(254, 116)
        Me.CheckFP.Name = "CheckFP"
        Me.CheckFP.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.CheckFP.Properties.Appearance.Options.UseFont = True
        Me.CheckFP.Properties.Caption = "FP"
        Me.CheckFP.Size = New System.Drawing.Size(56, 20)
        Me.CheckFP.TabIndex = 38
        '
        'CheckFace
        '
        Me.CheckFace.Location = New System.Drawing.Point(189, 116)
        Me.CheckFace.Name = "CheckFace"
        Me.CheckFace.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.CheckFace.Properties.Appearance.Options.UseFont = True
        Me.CheckFace.Properties.Caption = "Face"
        Me.CheckFace.Size = New System.Drawing.Size(56, 20)
        Me.CheckFace.TabIndex = 37
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.LabelControl4.Appearance.Options.UseFont = True
        Me.LabelControl4.Location = New System.Drawing.Point(5, 91)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(140, 16)
        Me.LabelControl4.TabIndex = 24
        Me.LabelControl4.Text = "Create Employee Master"
        '
        'ToggleCreateEmp
        '
        Me.ToggleCreateEmp.Location = New System.Drawing.Point(189, 86)
        Me.ToggleCreateEmp.Name = "ToggleCreateEmp"
        Me.ToggleCreateEmp.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleCreateEmp.Properties.Appearance.Options.UseFont = True
        Me.ToggleCreateEmp.Properties.OffText = "No"
        Me.ToggleCreateEmp.Properties.OnText = "Yes"
        Me.ToggleCreateEmp.Size = New System.Drawing.Size(95, 25)
        Me.ToggleCreateEmp.TabIndex = 23
        '
        'GroupControl4
        '
        Me.GroupControl4.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl4.AppearanceCaption.Options.UseFont = True
        Me.GroupControl4.Controls.Add(Me.LabelControl3)
        Me.GroupControl4.Controls.Add(Me.ToggleMakeAdmin)
        Me.GroupControl4.Controls.Add(Me.btnUploadFinger)
        Me.GroupControl4.Location = New System.Drawing.Point(602, 169)
        Me.GroupControl4.Name = "GroupControl4"
        Me.GroupControl4.Size = New System.Drawing.Size(342, 120)
        Me.GroupControl4.TabIndex = 30
        Me.GroupControl4.Text = "Upload"
        '
        'GroupControl5
        '
        Me.GroupControl5.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl5.AppearanceCaption.Options.UseFont = True
        Me.GroupControl5.Controls.Add(Me.btnLockClose)
        Me.GroupControl5.Controls.Add(Me.btnLockOpen)
        Me.GroupControl5.Controls.Add(Me.TextPassword)
        Me.GroupControl5.Controls.Add(Me.LabelControl5)
        Me.GroupControl5.Controls.Add(Me.btnClearDeviceData)
        Me.GroupControl5.Controls.Add(Me.btnDeleteFrmDevice)
        Me.GroupControl5.Controls.Add(Me.btnDeleteFrmDB)
        Me.GroupControl5.Controls.Add(Me.btnClearAdmin)
        Me.GroupControl5.Location = New System.Drawing.Point(602, 295)
        Me.GroupControl5.Name = "GroupControl5"
        Me.GroupControl5.Size = New System.Drawing.Size(342, 189)
        Me.GroupControl5.TabIndex = 31
        Me.GroupControl5.Text = "Delete"
        '
        'btnLockClose
        '
        Me.btnLockClose.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.btnLockClose.Appearance.Options.UseFont = True
        Me.btnLockClose.Location = New System.Drawing.Point(162, 66)
        Me.btnLockClose.Name = "btnLockClose"
        Me.btnLockClose.Size = New System.Drawing.Size(148, 23)
        Me.btnLockClose.TabIndex = 33
        Me.btnLockClose.Text = "Close Door Lock"
        '
        'btnLockOpen
        '
        Me.btnLockOpen.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.btnLockOpen.Appearance.Options.UseFont = True
        Me.btnLockOpen.Location = New System.Drawing.Point(162, 37)
        Me.btnLockOpen.Name = "btnLockOpen"
        Me.btnLockOpen.Size = New System.Drawing.Size(148, 23)
        Me.btnLockOpen.TabIndex = 32
        Me.btnLockOpen.Text = "Open Door Lock"
        '
        'TextPassword
        '
        Me.TextPassword.Location = New System.Drawing.Point(82, 153)
        Me.TextPassword.Name = "TextPassword"
        Me.TextPassword.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextPassword.Properties.MaxLength = 20
        Me.TextPassword.Properties.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.TextPassword.Size = New System.Drawing.Size(138, 20)
        Me.TextPassword.TabIndex = 31
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.LabelControl5.Appearance.Options.UseFont = True
        Me.LabelControl5.Location = New System.Drawing.Point(8, 154)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(55, 16)
        Me.LabelControl5.TabIndex = 30
        Me.LabelControl5.Text = "Password"
        '
        'btnClearDeviceData
        '
        Me.btnClearDeviceData.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.btnClearDeviceData.Appearance.Options.UseFont = True
        Me.btnClearDeviceData.Location = New System.Drawing.Point(8, 124)
        Me.btnClearDeviceData.Name = "btnClearDeviceData"
        Me.btnClearDeviceData.Size = New System.Drawing.Size(148, 23)
        Me.btnClearDeviceData.TabIndex = 29
        Me.btnClearDeviceData.Text = "Clear Device Data"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.SimpleButton1.Appearance.Options.UseFont = True
        Me.SimpleButton1.Location = New System.Drawing.Point(876, 495)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(68, 23)
        Me.SimpleButton1.TabIndex = 32
        Me.SimpleButton1.Text = "Close"
        '
        'XtraFaceDataMgmt
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(962, 525)
        Me.Controls.Add(Me.SimpleButton1)
        Me.Controls.Add(Me.GroupControl5)
        Me.Controls.Add(Me.GroupControl4)
        Me.Controls.Add(Me.GroupControl3)
        Me.Controls.Add(Me.GroupControl2)
        Me.Controls.Add(Me.GroupControl1)
        Me.LookAndFeel.SkinName = "iMaginary"
        Me.LookAndFeel.UseDefaultLookAndFeel = False
        Me.MaximizeBox = False
        Me.Name = "XtraFaceDataMgmt"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Template Management"
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblMachineBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FptableBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleDownloadAll.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextSelectUser.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleMakeAdmin.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        Me.GroupControl3.PerformLayout()
        CType(Me.CheckFP.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckFace.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleCreateEmp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl4.ResumeLayout(False)
        Me.GroupControl4.PerformLayout()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl5.ResumeLayout(False)
        Me.GroupControl5.PerformLayout()
        CType(Me.TextPassword.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SSSDBDataSet As iAS.SSSDBDataSet
    Friend WithEvents TblMachineBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblMachineTableAdapter As iAS.SSSDBDataSetTableAdapters.tblMachineTableAdapter
    Friend WithEvents TblMachine1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.tblMachine1TableAdapter
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colID_NO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLOCATION As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colbranch As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDeviceType As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GridControl2 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents FptableBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents FptableTableAdapter As iAS.SSSDBDataSetTableAdapters.fptableTableAdapter
    Friend WithEvents Fptable1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.fptable1TableAdapter
    Friend WithEvents colEMachineNumber As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colEnrollNumber As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colUserName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colFingerNumber As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPrivilege As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPassword As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colTemplate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCardnumber As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colcalid As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colauthority As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colcheck_type As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colopendoor_type As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colface_Data1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colface_data2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colface_data3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colface_data4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colface_data5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colface_data6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colface_data7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colface_data8 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colface_data9 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colface_data10 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colface_data11 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colface_data12 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colface_data13 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colface_data14 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colface_data15 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colface_data16 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colface_data17 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colface_data18 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colTemplate_Face As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colTemplate_Tw As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVerifyMode As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents btnDownloadFinger As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ToggleDownloadAll As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextSelectUser As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents btnUploadFinger As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnDeleteFrmDB As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ToggleMakeAdmin As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents btnDeleteFrmDevice As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnClearAdmin As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupControl4 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupControl5 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents colA_R As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colcommkey As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ToggleCreateEmp As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents btnClearDeviceData As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextPassword As DevExpress.XtraEditors.TextEdit
    Friend WithEvents btnLockOpen As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents colHTemplatePath As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents btnLockClose As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents CheckFP As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckFace As DevExpress.XtraEditors.CheckEdit
End Class
