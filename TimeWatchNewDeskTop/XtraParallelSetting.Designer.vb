﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraParallelSetting
    Inherits DevExpress.XtraEditors.XtraUserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(XtraParallelSetting))
        Me.SplitContainerControl1 = New DevExpress.XtraEditors.SplitContainerControl()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.TextName = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl34 = New DevExpress.XtraEditors.LabelControl()
        Me.TextDafVal3 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl32 = New DevExpress.XtraEditors.LabelControl()
        Me.TextDafVal2 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl31 = New DevExpress.XtraEditors.LabelControl()
        Me.TextDafVal1 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl30 = New DevExpress.XtraEditors.LabelControl()
        Me.TextRes3 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl29 = New DevExpress.XtraEditors.LabelControl()
        Me.TextRes2 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl28 = New DevExpress.XtraEditors.LabelControl()
        Me.TextRes1 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl27 = New DevExpress.XtraEditors.LabelControl()
        Me.TextMNoValue = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl24 = New DevExpress.XtraEditors.LabelControl()
        Me.TextMYesValue = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl25 = New DevExpress.XtraEditors.LabelControl()
        Me.TextManual = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl26 = New DevExpress.XtraEditors.LabelControl()
        Me.TextOutValue = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl23 = New DevExpress.XtraEditors.LabelControl()
        Me.TextInValue = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl22 = New DevExpress.XtraEditors.LabelControl()
        Me.TextPunchDirection = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl21 = New DevExpress.XtraEditors.LabelControl()
        Me.TextDateTimeFormat = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl20 = New DevExpress.XtraEditors.LabelControl()
        Me.TextTimeFormat = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl19 = New DevExpress.XtraEditors.LabelControl()
        Me.TextDateFormat = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl18 = New DevExpress.XtraEditors.LabelControl()
        Me.TextDeviceId = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl()
        Me.TextPunchDateTime = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        Me.TextPunchTime = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.TextPunchDate = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.TextPriTextPay = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.TextPriFixLenghtPay = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.CheckPrefixPrePaycode = New DevExpress.XtraEditors.CheckEdit()
        Me.TextPriTextPre = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.TextPriFixLenghtPre = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.CheckPrefixPreCard = New DevExpress.XtraEditors.CheckEdit()
        Me.TextPayCode = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.TextPresentCardNo = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.TextTblName = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.PanelControl4 = New DevExpress.XtraEditors.PanelControl()
        Me.btnSave = New DevExpress.XtraEditors.SimpleButton()
        Me.btnTest = New DevExpress.XtraEditors.SimpleButton()
        Me.PanelControl3 = New DevExpress.XtraEditors.PanelControl()
        Me.TextDBName = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl33 = New DevExpress.XtraEditors.LabelControl()
        Me.TextPwd = New DevExpress.XtraEditors.TextEdit()
        Me.TextUser = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.TextServerName = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.ComboAuthMode = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.ComboDBType = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl()
        Me.ToggleParallel = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.MemoEdit1 = New DevExpress.XtraEditors.MemoEdit()
        Me.DateEditFrmDate = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl35 = New DevExpress.XtraEditors.LabelControl()
        Me.BtnManualAdd = New DevExpress.XtraEditors.SimpleButton()
	Me.CheckC = New DevExpress.XtraEditors.CheckEdit()
        Me.TextTempR = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl36 = New DevExpress.XtraEditors.LabelControl()
        Me.CheckF = New DevExpress.XtraEditors.CheckEdit()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerControl1.SuspendLayout()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.TextName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextDafVal3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextDafVal2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextDafVal1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextRes3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextRes2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextRes1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextMNoValue.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextMYesValue.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextManual.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextOutValue.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextInValue.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextPunchDirection.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextDateTimeFormat.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextTimeFormat.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextDateFormat.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextDeviceId.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextPunchDateTime.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextPunchTime.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextPunchDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextPriTextPay.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextPriFixLenghtPay.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckPrefixPrePaycode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextPriTextPre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextPriFixLenghtPre.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckPrefixPreCard.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextPayCode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextPresentCardNo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextTblName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl4.SuspendLayout()
        CType(Me.PanelControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl3.SuspendLayout()
        CType(Me.TextDBName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextPwd.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextUser.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextServerName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboAuthMode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboDBType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl2.SuspendLayout()
        CType(Me.ToggleParallel.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEditFrmDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEditFrmDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckC.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextTempR.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckF.Properties, System.ComponentModel.ISupportInitialize).BeginInit()        
	Me.SuspendLayout()
        '
        'SplitContainerControl1
        '
        Me.SplitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainerControl1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainerControl1.Name = "SplitContainerControl1"
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.PanelControl1)
        Me.SplitContainerControl1.Panel1.Text = "Panel1"
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.MemoEdit1)
        Me.SplitContainerControl1.Panel2.Text = "Panel2"
        Me.SplitContainerControl1.Size = New System.Drawing.Size(1250, 600)
        Me.SplitContainerControl1.SplitterPosition = 1165
        Me.SplitContainerControl1.TabIndex = 1
        Me.SplitContainerControl1.Text = "SplitContainerControl1"
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.GroupControl1)
        Me.PanelControl1.Controls.Add(Me.PanelControl4)
        Me.PanelControl1.Controls.Add(Me.PanelControl3)
        Me.PanelControl1.Controls.Add(Me.PanelControl2)
        Me.PanelControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelControl1.Location = New System.Drawing.Point(0, 0)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(1165, 600)
        Me.PanelControl1.TabIndex = 26
        '
        'GroupControl1
        '
        Me.GroupControl1.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GroupControl1.AppearanceCaption.ForeColor = System.Drawing.Color.Blue
        Me.GroupControl1.AppearanceCaption.Options.UseFont = True
        Me.GroupControl1.AppearanceCaption.Options.UseForeColor = True
        Me.GroupControl1.Controls.Add(Me.CheckF)
        Me.GroupControl1.Controls.Add(Me.CheckC)
        Me.GroupControl1.Controls.Add(Me.TextTempR)
        Me.GroupControl1.Controls.Add(Me.LabelControl36)
        Me.GroupControl1.Controls.Add(Me.BtnManualAdd)
        Me.GroupControl1.Controls.Add(Me.LabelControl35)
        Me.GroupControl1.Controls.Add(Me.DateEditFrmDate)
        Me.GroupControl1.Controls.Add(Me.TextName)
        Me.GroupControl1.Controls.Add(Me.LabelControl34)
        Me.GroupControl1.Controls.Add(Me.TextDafVal3)
        Me.GroupControl1.Controls.Add(Me.LabelControl32)
        Me.GroupControl1.Controls.Add(Me.TextDafVal2)
        Me.GroupControl1.Controls.Add(Me.LabelControl31)
        Me.GroupControl1.Controls.Add(Me.TextDafVal1)
        Me.GroupControl1.Controls.Add(Me.LabelControl30)
        Me.GroupControl1.Controls.Add(Me.TextRes3)
        Me.GroupControl1.Controls.Add(Me.LabelControl29)
        Me.GroupControl1.Controls.Add(Me.TextRes2)
        Me.GroupControl1.Controls.Add(Me.LabelControl28)
        Me.GroupControl1.Controls.Add(Me.TextRes1)
        Me.GroupControl1.Controls.Add(Me.LabelControl27)
        Me.GroupControl1.Controls.Add(Me.TextMNoValue)
        Me.GroupControl1.Controls.Add(Me.LabelControl24)
        Me.GroupControl1.Controls.Add(Me.TextMYesValue)
        Me.GroupControl1.Controls.Add(Me.LabelControl25)
        Me.GroupControl1.Controls.Add(Me.TextManual)
        Me.GroupControl1.Controls.Add(Me.LabelControl26)
        Me.GroupControl1.Controls.Add(Me.TextOutValue)
        Me.GroupControl1.Controls.Add(Me.LabelControl23)
        Me.GroupControl1.Controls.Add(Me.TextInValue)
        Me.GroupControl1.Controls.Add(Me.LabelControl22)
        Me.GroupControl1.Controls.Add(Me.TextPunchDirection)
        Me.GroupControl1.Controls.Add(Me.LabelControl21)
        Me.GroupControl1.Controls.Add(Me.TextDateTimeFormat)
        Me.GroupControl1.Controls.Add(Me.LabelControl20)
        Me.GroupControl1.Controls.Add(Me.TextTimeFormat)
        Me.GroupControl1.Controls.Add(Me.LabelControl19)
        Me.GroupControl1.Controls.Add(Me.TextDateFormat)
        Me.GroupControl1.Controls.Add(Me.LabelControl18)
        Me.GroupControl1.Controls.Add(Me.TextDeviceId)
        Me.GroupControl1.Controls.Add(Me.LabelControl17)
        Me.GroupControl1.Controls.Add(Me.TextPunchDateTime)
        Me.GroupControl1.Controls.Add(Me.LabelControl16)
        Me.GroupControl1.Controls.Add(Me.TextPunchTime)
        Me.GroupControl1.Controls.Add(Me.LabelControl15)
        Me.GroupControl1.Controls.Add(Me.TextPunchDate)
        Me.GroupControl1.Controls.Add(Me.LabelControl14)
        Me.GroupControl1.Controls.Add(Me.TextPriTextPay)
        Me.GroupControl1.Controls.Add(Me.LabelControl12)
        Me.GroupControl1.Controls.Add(Me.TextPriFixLenghtPay)
        Me.GroupControl1.Controls.Add(Me.LabelControl13)
        Me.GroupControl1.Controls.Add(Me.CheckPrefixPrePaycode)
        Me.GroupControl1.Controls.Add(Me.TextPriTextPre)
        Me.GroupControl1.Controls.Add(Me.LabelControl11)
        Me.GroupControl1.Controls.Add(Me.TextPriFixLenghtPre)
        Me.GroupControl1.Controls.Add(Me.LabelControl10)
        Me.GroupControl1.Controls.Add(Me.CheckPrefixPreCard)
        Me.GroupControl1.Controls.Add(Me.TextPayCode)
        Me.GroupControl1.Controls.Add(Me.LabelControl9)
        Me.GroupControl1.Controls.Add(Me.TextPresentCardNo)
        Me.GroupControl1.Controls.Add(Me.LabelControl8)
        Me.GroupControl1.Controls.Add(Me.TextTblName)
        Me.GroupControl1.Controls.Add(Me.LabelControl7)
        Me.GroupControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupControl1.Location = New System.Drawing.Point(2, 123)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(1161, 440)
        Me.GroupControl1.TabIndex = 3
        Me.GroupControl1.Text = "Field Selection"
        '
        'TextName
        '
        Me.TextName.Location = New System.Drawing.Point(164, 258)
        Me.TextName.Name = "TextName"
        Me.TextName.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextName.Properties.Appearance.Options.UseFont = True
        Me.TextName.Properties.MaxLength = 50
        Me.TextName.Size = New System.Drawing.Size(175, 20)
        Me.TextName.TabIndex = 23
        '
        'LabelControl34
        '
        Me.LabelControl34.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl34.Appearance.Options.UseFont = True
        Me.LabelControl34.Location = New System.Drawing.Point(19, 261)
        Me.LabelControl34.Name = "LabelControl34"
        Me.LabelControl34.Size = New System.Drawing.Size(88, 14)
        Me.LabelControl34.TabIndex = 104
        Me.LabelControl34.Text = "Employee Name"
        '
        'TextDafVal3
        '
        Me.TextDafVal3.Location = New System.Drawing.Point(560, 362)
        Me.TextDafVal3.Name = "TextDafVal3"
        Me.TextDafVal3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextDafVal3.Properties.Appearance.Options.UseFont = True
        Me.TextDafVal3.Properties.MaxLength = 50
        Me.TextDafVal3.Size = New System.Drawing.Size(150, 20)
        Me.TextDafVal3.TabIndex = 32
        '
        'LabelControl32
        '
        Me.LabelControl32.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl32.Appearance.Options.UseFont = True
        Me.LabelControl32.Location = New System.Drawing.Point(426, 365)
        Me.LabelControl32.Name = "LabelControl32"
        Me.LabelControl32.Size = New System.Drawing.Size(73, 14)
        Me.LabelControl32.TabIndex = 102
        Me.LabelControl32.Text = "Default Value"
        '
        'TextDafVal2
        '
        Me.TextDafVal2.Location = New System.Drawing.Point(560, 336)
        Me.TextDafVal2.Name = "TextDafVal2"
        Me.TextDafVal2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextDafVal2.Properties.Appearance.Options.UseFont = True
        Me.TextDafVal2.Properties.MaxLength = 50
        Me.TextDafVal2.Size = New System.Drawing.Size(150, 20)
        Me.TextDafVal2.TabIndex = 30
        '
        'LabelControl31
        '
        Me.LabelControl31.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl31.Appearance.Options.UseFont = True
        Me.LabelControl31.Location = New System.Drawing.Point(426, 339)
        Me.LabelControl31.Name = "LabelControl31"
        Me.LabelControl31.Size = New System.Drawing.Size(73, 14)
        Me.LabelControl31.TabIndex = 100
        Me.LabelControl31.Text = "Default Value"
        '
        'TextDafVal1
        '
        Me.TextDafVal1.Location = New System.Drawing.Point(560, 310)
        Me.TextDafVal1.Name = "TextDafVal1"
        Me.TextDafVal1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextDafVal1.Properties.Appearance.Options.UseFont = True
        Me.TextDafVal1.Properties.MaxLength = 50
        Me.TextDafVal1.Size = New System.Drawing.Size(150, 20)
        Me.TextDafVal1.TabIndex = 28
        '
        'LabelControl30
        '
        Me.LabelControl30.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl30.Appearance.Options.UseFont = True
        Me.LabelControl30.Location = New System.Drawing.Point(426, 313)
        Me.LabelControl30.Name = "LabelControl30"
        Me.LabelControl30.Size = New System.Drawing.Size(73, 14)
        Me.LabelControl30.TabIndex = 98
        Me.LabelControl30.Text = "Default Value"
        '
        'TextRes3
        '
        Me.TextRes3.Location = New System.Drawing.Point(164, 362)
        Me.TextRes3.Name = "TextRes3"
        Me.TextRes3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextRes3.Properties.Appearance.Options.UseFont = True
        Me.TextRes3.Properties.MaxLength = 50
        Me.TextRes3.Size = New System.Drawing.Size(175, 20)
        Me.TextRes3.TabIndex = 31
        '
        'LabelControl29
        '
        Me.LabelControl29.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl29.Appearance.Options.UseFont = True
        Me.LabelControl29.Location = New System.Drawing.Point(19, 365)
        Me.LabelControl29.Name = "LabelControl29"
        Me.LabelControl29.Size = New System.Drawing.Size(89, 14)
        Me.LabelControl29.TabIndex = 96
        Me.LabelControl29.Text = "Reserved Field 3"
        '
        'TextRes2
        '
        Me.TextRes2.Location = New System.Drawing.Point(164, 336)
        Me.TextRes2.Name = "TextRes2"
        Me.TextRes2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextRes2.Properties.Appearance.Options.UseFont = True
        Me.TextRes2.Properties.MaxLength = 50
        Me.TextRes2.Size = New System.Drawing.Size(175, 20)
        Me.TextRes2.TabIndex = 29
        '
        'LabelControl28
        '
        Me.LabelControl28.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl28.Appearance.Options.UseFont = True
        Me.LabelControl28.Location = New System.Drawing.Point(19, 339)
        Me.LabelControl28.Name = "LabelControl28"
        Me.LabelControl28.Size = New System.Drawing.Size(89, 14)
        Me.LabelControl28.TabIndex = 94
        Me.LabelControl28.Text = "Reserved Field 2"
        '
        'TextRes1
        '
        Me.TextRes1.Location = New System.Drawing.Point(164, 310)
        Me.TextRes1.Name = "TextRes1"
        Me.TextRes1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextRes1.Properties.Appearance.Options.UseFont = True
        Me.TextRes1.Properties.MaxLength = 50
        Me.TextRes1.Size = New System.Drawing.Size(175, 20)
        Me.TextRes1.TabIndex = 27
        '
        'LabelControl27
        '
        Me.LabelControl27.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl27.Appearance.Options.UseFont = True
        Me.LabelControl27.Location = New System.Drawing.Point(19, 313)
        Me.LabelControl27.Name = "LabelControl27"
        Me.LabelControl27.Size = New System.Drawing.Size(89, 14)
        Me.LabelControl27.TabIndex = 92
        Me.LabelControl27.Text = "Reserved Field 1"
        '
        'TextMNoValue
        '
        Me.TextMNoValue.Location = New System.Drawing.Point(635, 233)
        Me.TextMNoValue.Name = "TextMNoValue"
        Me.TextMNoValue.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextMNoValue.Properties.Appearance.Options.UseFont = True
        Me.TextMNoValue.Properties.MaxLength = 5
        Me.TextMNoValue.Size = New System.Drawing.Size(40, 20)
        Me.TextMNoValue.TabIndex = 22
        '
        'LabelControl24
        '
        Me.LabelControl24.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl24.Appearance.Options.UseFont = True
        Me.LabelControl24.Location = New System.Drawing.Point(561, 235)
        Me.LabelControl24.Name = "LabelControl24"
        Me.LabelControl24.Size = New System.Drawing.Size(49, 14)
        Me.LabelControl24.TabIndex = 90
        Me.LabelControl24.Text = "No Value"
        '
        'TextMYesValue
        '
        Me.TextMYesValue.Location = New System.Drawing.Point(500, 233)
        Me.TextMYesValue.Name = "TextMYesValue"
        Me.TextMYesValue.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextMYesValue.Properties.Appearance.Options.UseFont = True
        Me.TextMYesValue.Properties.MaxLength = 5
        Me.TextMYesValue.Size = New System.Drawing.Size(40, 20)
        Me.TextMYesValue.TabIndex = 21
        '
        'LabelControl25
        '
        Me.LabelControl25.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl25.Appearance.Options.UseFont = True
        Me.LabelControl25.Location = New System.Drawing.Point(426, 235)
        Me.LabelControl25.Name = "LabelControl25"
        Me.LabelControl25.Size = New System.Drawing.Size(54, 14)
        Me.LabelControl25.TabIndex = 88
        Me.LabelControl25.Text = "Yes Value"
        '
        'TextManual
        '
        Me.TextManual.Location = New System.Drawing.Point(164, 232)
        Me.TextManual.Name = "TextManual"
        Me.TextManual.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextManual.Properties.Appearance.Options.UseFont = True
        Me.TextManual.Properties.MaxLength = 50
        Me.TextManual.Size = New System.Drawing.Size(175, 20)
        Me.TextManual.TabIndex = 20
        '
        'LabelControl26
        '
        Me.LabelControl26.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl26.Appearance.Options.UseFont = True
        Me.LabelControl26.Location = New System.Drawing.Point(19, 235)
        Me.LabelControl26.Name = "LabelControl26"
        Me.LabelControl26.Size = New System.Drawing.Size(50, 14)
        Me.LabelControl26.TabIndex = 86
        Me.LabelControl26.Text = "Is Manual"
        '
        'TextOutValue
        '
        Me.TextOutValue.Location = New System.Drawing.Point(635, 207)
        Me.TextOutValue.Name = "TextOutValue"
        Me.TextOutValue.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextOutValue.Properties.Appearance.Options.UseFont = True
        Me.TextOutValue.Properties.MaxLength = 5
        Me.TextOutValue.Size = New System.Drawing.Size(40, 20)
        Me.TextOutValue.TabIndex = 19
        '
        'LabelControl23
        '
        Me.LabelControl23.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl23.Appearance.Options.UseFont = True
        Me.LabelControl23.Location = New System.Drawing.Point(561, 209)
        Me.LabelControl23.Name = "LabelControl23"
        Me.LabelControl23.Size = New System.Drawing.Size(59, 14)
        Me.LabelControl23.TabIndex = 84
        Me.LabelControl23.Text = "OUT Value"
        '
        'TextInValue
        '
        Me.TextInValue.Location = New System.Drawing.Point(500, 207)
        Me.TextInValue.Name = "TextInValue"
        Me.TextInValue.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextInValue.Properties.Appearance.Options.UseFont = True
        Me.TextInValue.Properties.MaxLength = 5
        Me.TextInValue.Size = New System.Drawing.Size(40, 20)
        Me.TextInValue.TabIndex = 18
        '
        'LabelControl22
        '
        Me.LabelControl22.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl22.Appearance.Options.UseFont = True
        Me.LabelControl22.Location = New System.Drawing.Point(427, 209)
        Me.LabelControl22.Name = "LabelControl22"
        Me.LabelControl22.Size = New System.Drawing.Size(46, 14)
        Me.LabelControl22.TabIndex = 82
        Me.LabelControl22.Text = "IN Value"
        '
        'TextPunchDirection
        '
        Me.TextPunchDirection.Location = New System.Drawing.Point(164, 206)
        Me.TextPunchDirection.Name = "TextPunchDirection"
        Me.TextPunchDirection.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextPunchDirection.Properties.Appearance.Options.UseFont = True
        Me.TextPunchDirection.Properties.MaxLength = 50
        Me.TextPunchDirection.Size = New System.Drawing.Size(175, 20)
        Me.TextPunchDirection.TabIndex = 17
        '
        'LabelControl21
        '
        Me.LabelControl21.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl21.Appearance.Options.UseFont = True
        Me.LabelControl21.Location = New System.Drawing.Point(19, 209)
        Me.LabelControl21.Name = "LabelControl21"
        Me.LabelControl21.Size = New System.Drawing.Size(86, 14)
        Me.LabelControl21.TabIndex = 80
        Me.LabelControl21.Text = "Punch Direction"
        '
        'TextDateTimeFormat
        '
        Me.TextDateTimeFormat.Location = New System.Drawing.Point(561, 155)
        Me.TextDateTimeFormat.Name = "TextDateTimeFormat"
        Me.TextDateTimeFormat.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextDateTimeFormat.Properties.Appearance.Options.UseFont = True
        Me.TextDateTimeFormat.Properties.MaxLength = 50
        Me.TextDateTimeFormat.Size = New System.Drawing.Size(150, 20)
        Me.TextDateTimeFormat.TabIndex = 15
        '
        'LabelControl20
        '
        Me.LabelControl20.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl20.Appearance.Options.UseFont = True
        Me.LabelControl20.Location = New System.Drawing.Point(426, 157)
        Me.LabelControl20.Name = "LabelControl20"
        Me.LabelControl20.Size = New System.Drawing.Size(99, 14)
        Me.LabelControl20.TabIndex = 78
        Me.LabelControl20.Text = "Date Time Format"
        '
        'TextTimeFormat
        '
        Me.TextTimeFormat.Location = New System.Drawing.Point(561, 129)
        Me.TextTimeFormat.Name = "TextTimeFormat"
        Me.TextTimeFormat.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextTimeFormat.Properties.Appearance.Options.UseFont = True
        Me.TextTimeFormat.Properties.MaxLength = 50
        Me.TextTimeFormat.Size = New System.Drawing.Size(150, 20)
        Me.TextTimeFormat.TabIndex = 13
        '
        'LabelControl19
        '
        Me.LabelControl19.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl19.Appearance.Options.UseFont = True
        Me.LabelControl19.Location = New System.Drawing.Point(426, 131)
        Me.LabelControl19.Name = "LabelControl19"
        Me.LabelControl19.Size = New System.Drawing.Size(69, 14)
        Me.LabelControl19.TabIndex = 76
        Me.LabelControl19.Text = "Time Format"
        '
        'TextDateFormat
        '
        Me.TextDateFormat.Location = New System.Drawing.Point(561, 103)
        Me.TextDateFormat.Name = "TextDateFormat"
        Me.TextDateFormat.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextDateFormat.Properties.Appearance.Options.UseFont = True
        Me.TextDateFormat.Properties.MaxLength = 50
        Me.TextDateFormat.Size = New System.Drawing.Size(150, 20)
        Me.TextDateFormat.TabIndex = 11
        '
        'LabelControl18
        '
        Me.LabelControl18.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl18.Appearance.Options.UseFont = True
        Me.LabelControl18.Location = New System.Drawing.Point(426, 105)
        Me.LabelControl18.Name = "LabelControl18"
        Me.LabelControl18.Size = New System.Drawing.Size(68, 14)
        Me.LabelControl18.TabIndex = 74
        Me.LabelControl18.Text = "Date Format"
        '
        'TextDeviceId
        '
        Me.TextDeviceId.Location = New System.Drawing.Point(164, 180)
        Me.TextDeviceId.Name = "TextDeviceId"
        Me.TextDeviceId.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextDeviceId.Properties.Appearance.Options.UseFont = True
        Me.TextDeviceId.Properties.MaxLength = 50
        Me.TextDeviceId.Size = New System.Drawing.Size(175, 20)
        Me.TextDeviceId.TabIndex = 16
        '
        'LabelControl17
        '
        Me.LabelControl17.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl17.Appearance.Options.UseFont = True
        Me.LabelControl17.Location = New System.Drawing.Point(19, 183)
        Me.LabelControl17.Name = "LabelControl17"
        Me.LabelControl17.Size = New System.Drawing.Size(51, 14)
        Me.LabelControl17.TabIndex = 72
        Me.LabelControl17.Text = "Device Id"
        '
        'TextPunchDateTime
        '
        Me.TextPunchDateTime.Location = New System.Drawing.Point(164, 154)
        Me.TextPunchDateTime.Name = "TextPunchDateTime"
        Me.TextPunchDateTime.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextPunchDateTime.Properties.Appearance.Options.UseFont = True
        Me.TextPunchDateTime.Properties.MaxLength = 50
        Me.TextPunchDateTime.Size = New System.Drawing.Size(175, 20)
        Me.TextPunchDateTime.TabIndex = 14
        '
        'LabelControl16
        '
        Me.LabelControl16.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl16.Appearance.Options.UseFont = True
        Me.LabelControl16.Location = New System.Drawing.Point(19, 157)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(95, 14)
        Me.LabelControl16.TabIndex = 70
        Me.LabelControl16.Text = "Punch Date Time"
        '
        'TextPunchTime
        '
        Me.TextPunchTime.Location = New System.Drawing.Point(164, 128)
        Me.TextPunchTime.Name = "TextPunchTime"
        Me.TextPunchTime.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextPunchTime.Properties.Appearance.Options.UseFont = True
        Me.TextPunchTime.Properties.MaxLength = 50
        Me.TextPunchTime.Size = New System.Drawing.Size(175, 20)
        Me.TextPunchTime.TabIndex = 12
        '
        'LabelControl15
        '
        Me.LabelControl15.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl15.Appearance.Options.UseFont = True
        Me.LabelControl15.Location = New System.Drawing.Point(19, 131)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(65, 14)
        Me.LabelControl15.TabIndex = 68
        Me.LabelControl15.Text = "Punch Time"
        '
        'TextPunchDate
        '
        Me.TextPunchDate.Location = New System.Drawing.Point(164, 102)
        Me.TextPunchDate.Name = "TextPunchDate"
        Me.TextPunchDate.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextPunchDate.Properties.Appearance.Options.UseFont = True
        Me.TextPunchDate.Properties.MaxLength = 50
        Me.TextPunchDate.Size = New System.Drawing.Size(175, 20)
        Me.TextPunchDate.TabIndex = 10
        '
        'LabelControl14
        '
        Me.LabelControl14.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl14.Appearance.Options.UseFont = True
        Me.LabelControl14.Location = New System.Drawing.Point(19, 105)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(64, 14)
        Me.LabelControl14.TabIndex = 66
        Me.LabelControl14.Text = "Punch Date"
        '
        'TextPriTextPay
        '
        Me.TextPriTextPay.Location = New System.Drawing.Point(704, 77)
        Me.TextPriTextPay.Name = "TextPriTextPay"
        Me.TextPriTextPay.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextPriTextPay.Properties.Appearance.Options.UseFont = True
        Me.TextPriTextPay.Properties.MaxLength = 1
        Me.TextPriTextPay.Size = New System.Drawing.Size(40, 20)
        Me.TextPriTextPay.TabIndex = 9
        '
        'LabelControl12
        '
        Me.LabelControl12.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl12.Appearance.Options.UseFont = True
        Me.LabelControl12.Location = New System.Drawing.Point(630, 79)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(60, 14)
        Me.LabelControl12.TabIndex = 64
        Me.LabelControl12.Text = "Prefix Text"
        '
        'TextPriFixLenghtPay
        '
        Me.TextPriFixLenghtPay.Location = New System.Drawing.Point(561, 77)
        Me.TextPriFixLenghtPay.Name = "TextPriFixLenghtPay"
        Me.TextPriFixLenghtPay.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextPriFixLenghtPay.Properties.Appearance.Options.UseFont = True
        Me.TextPriFixLenghtPay.Properties.Mask.EditMask = "[0-9]*"
        Me.TextPriFixLenghtPay.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextPriFixLenghtPay.Properties.MaxLength = 2
        Me.TextPriFixLenghtPay.Size = New System.Drawing.Size(40, 20)
        Me.TextPriFixLenghtPay.TabIndex = 8
        '
        'LabelControl13
        '
        Me.LabelControl13.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl13.Appearance.Options.UseFont = True
        Me.LabelControl13.Location = New System.Drawing.Point(520, 79)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(34, 14)
        Me.LabelControl13.TabIndex = 62
        Me.LabelControl13.Text = "Lengh"
        '
        'CheckPrefixPrePaycode
        '
        Me.CheckPrefixPrePaycode.Location = New System.Drawing.Point(426, 77)
        Me.CheckPrefixPrePaycode.Name = "CheckPrefixPrePaycode"
        Me.CheckPrefixPrePaycode.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckPrefixPrePaycode.Properties.Appearance.Options.UseFont = True
        Me.CheckPrefixPrePaycode.Properties.Caption = "Prefix"
        Me.CheckPrefixPrePaycode.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckPrefixPrePaycode.Properties.ValueChecked = "Y"
        Me.CheckPrefixPrePaycode.Properties.ValueUnchecked = "N"
        Me.CheckPrefixPrePaycode.Size = New System.Drawing.Size(59, 19)
        Me.CheckPrefixPrePaycode.TabIndex = 7
        '
        'TextPriTextPre
        '
        Me.TextPriTextPre.Location = New System.Drawing.Point(704, 51)
        Me.TextPriTextPre.Name = "TextPriTextPre"
        Me.TextPriTextPre.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextPriTextPre.Properties.Appearance.Options.UseFont = True
        Me.TextPriTextPre.Properties.MaxLength = 1
        Me.TextPriTextPre.Size = New System.Drawing.Size(40, 20)
        Me.TextPriTextPre.TabIndex = 5
        '
        'LabelControl11
        '
        Me.LabelControl11.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl11.Appearance.Options.UseFont = True
        Me.LabelControl11.Location = New System.Drawing.Point(630, 53)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(60, 14)
        Me.LabelControl11.TabIndex = 59
        Me.LabelControl11.Text = "Prefix Text"
        '
        'TextPriFixLenghtPre
        '
        Me.TextPriFixLenghtPre.Location = New System.Drawing.Point(561, 51)
        Me.TextPriFixLenghtPre.Name = "TextPriFixLenghtPre"
        Me.TextPriFixLenghtPre.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextPriFixLenghtPre.Properties.Appearance.Options.UseFont = True
        Me.TextPriFixLenghtPre.Properties.Mask.EditMask = "[0-9]*"
        Me.TextPriFixLenghtPre.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextPriFixLenghtPre.Properties.MaxLength = 2
        Me.TextPriFixLenghtPre.Size = New System.Drawing.Size(40, 20)
        Me.TextPriFixLenghtPre.TabIndex = 4
        '
        'LabelControl10
        '
        Me.LabelControl10.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl10.Appearance.Options.UseFont = True
        Me.LabelControl10.Location = New System.Drawing.Point(520, 53)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(34, 14)
        Me.LabelControl10.TabIndex = 57
        Me.LabelControl10.Text = "Lengh"
        '
        'CheckPrefixPreCard
        '
        Me.CheckPrefixPreCard.Location = New System.Drawing.Point(426, 51)
        Me.CheckPrefixPreCard.Name = "CheckPrefixPreCard"
        Me.CheckPrefixPreCard.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckPrefixPreCard.Properties.Appearance.Options.UseFont = True
        Me.CheckPrefixPreCard.Properties.Caption = "Prefix"
        Me.CheckPrefixPreCard.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckPrefixPreCard.Properties.ValueChecked = "Y"
        Me.CheckPrefixPreCard.Properties.ValueUnchecked = "N"
        Me.CheckPrefixPreCard.Size = New System.Drawing.Size(59, 19)
        Me.CheckPrefixPreCard.TabIndex = 3
        '
        'TextPayCode
        '
        Me.TextPayCode.Location = New System.Drawing.Point(164, 76)
        Me.TextPayCode.Name = "TextPayCode"
        Me.TextPayCode.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextPayCode.Properties.Appearance.Options.UseFont = True
        Me.TextPayCode.Properties.MaxLength = 50
        Me.TextPayCode.Size = New System.Drawing.Size(175, 20)
        Me.TextPayCode.TabIndex = 6
        '
        'LabelControl9
        '
        Me.LabelControl9.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl9.Appearance.Options.UseFont = True
        Me.LabelControl9.Location = New System.Drawing.Point(19, 79)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(50, 14)
        Me.LabelControl9.TabIndex = 39
        Me.LabelControl9.Text = "Paycode "
        '
        'TextPresentCardNo
        '
        Me.TextPresentCardNo.Location = New System.Drawing.Point(164, 50)
        Me.TextPresentCardNo.Name = "TextPresentCardNo"
        Me.TextPresentCardNo.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextPresentCardNo.Properties.Appearance.Options.UseFont = True
        Me.TextPresentCardNo.Properties.MaxLength = 50
        Me.TextPresentCardNo.Size = New System.Drawing.Size(175, 20)
        Me.TextPresentCardNo.TabIndex = 2
        '
        'LabelControl8
        '
        Me.LabelControl8.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl8.Appearance.Options.UseFont = True
        Me.LabelControl8.Location = New System.Drawing.Point(19, 53)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(75, 14)
        Me.LabelControl8.TabIndex = 37
        Me.LabelControl8.Text = "User Number "
        '
        'TextTblName
        '
        Me.TextTblName.Location = New System.Drawing.Point(164, 24)
        Me.TextTblName.Name = "TextTblName"
        Me.TextTblName.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextTblName.Properties.Appearance.Options.UseFont = True
        Me.TextTblName.Properties.MaxLength = 50
        Me.TextTblName.Size = New System.Drawing.Size(253, 20)
        Me.TextTblName.TabIndex = 1
        '
        'LabelControl7
        '
        Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl7.Appearance.Options.UseFont = True
        Me.LabelControl7.Location = New System.Drawing.Point(19, 27)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(65, 14)
        Me.LabelControl7.TabIndex = 35
        Me.LabelControl7.Text = "Table Name"
        '
        'PanelControl4
        '
        Me.PanelControl4.Controls.Add(Me.btnSave)
        Me.PanelControl4.Controls.Add(Me.btnTest)
        Me.PanelControl4.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.PanelControl4.Location = New System.Drawing.Point(2, 563)
        Me.PanelControl4.Name = "PanelControl4"
        Me.PanelControl4.Size = New System.Drawing.Size(1161, 35)
        Me.PanelControl4.TabIndex = 4
        '
        'btnSave
        '
        Me.btnSave.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.btnSave.Appearance.Options.UseFont = True
        Me.btnSave.Location = New System.Drawing.Point(157, 5)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(87, 25)
        Me.btnSave.TabIndex = 2
        Me.btnSave.Text = "Save Setting"
        '
        'btnTest
        '
        Me.btnTest.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.btnTest.Appearance.Options.UseFont = True
        Me.btnTest.Location = New System.Drawing.Point(5, 5)
        Me.btnTest.Name = "btnTest"
        Me.btnTest.Size = New System.Drawing.Size(146, 25)
        Me.btnTest.TabIndex = 1
        Me.btnTest.Text = "Test Connection"
        '
        'PanelControl3
        '
        Me.PanelControl3.Controls.Add(Me.TextDBName)
        Me.PanelControl3.Controls.Add(Me.LabelControl33)
        Me.PanelControl3.Controls.Add(Me.TextPwd)
        Me.PanelControl3.Controls.Add(Me.TextUser)
        Me.PanelControl3.Controls.Add(Me.LabelControl6)
        Me.PanelControl3.Controls.Add(Me.LabelControl5)
        Me.PanelControl3.Controls.Add(Me.TextServerName)
        Me.PanelControl3.Controls.Add(Me.LabelControl3)
        Me.PanelControl3.Controls.Add(Me.ComboAuthMode)
        Me.PanelControl3.Controls.Add(Me.LabelControl4)
        Me.PanelControl3.Controls.Add(Me.ComboDBType)
        Me.PanelControl3.Controls.Add(Me.LabelControl2)
        Me.PanelControl3.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanelControl3.Location = New System.Drawing.Point(2, 33)
        Me.PanelControl3.Name = "PanelControl3"
        Me.PanelControl3.Size = New System.Drawing.Size(1161, 90)
        Me.PanelControl3.TabIndex = 2
        '
        'TextDBName
        '
        Me.TextDBName.Location = New System.Drawing.Point(164, 58)
        Me.TextDBName.Name = "TextDBName"
        Me.TextDBName.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextDBName.Properties.Appearance.Options.UseFont = True
        Me.TextDBName.Properties.MaxLength = 50
        Me.TextDBName.Size = New System.Drawing.Size(150, 20)
        Me.TextDBName.TabIndex = 4
        '
        'LabelControl33
        '
        Me.LabelControl33.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl33.Appearance.Options.UseFont = True
        Me.LabelControl33.Location = New System.Drawing.Point(19, 61)
        Me.LabelControl33.Name = "LabelControl33"
        Me.LabelControl33.Size = New System.Drawing.Size(85, 14)
        Me.LabelControl33.TabIndex = 39
        Me.LabelControl33.Text = "DataBase Name"
        '
        'TextPwd
        '
        Me.TextPwd.Location = New System.Drawing.Point(536, 58)
        Me.TextPwd.Name = "TextPwd"
        Me.TextPwd.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextPwd.Properties.Appearance.Options.UseFont = True
        Me.TextPwd.Properties.MaxLength = 50
        Me.TextPwd.Properties.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.TextPwd.Size = New System.Drawing.Size(175, 20)
        Me.TextPwd.TabIndex = 6
        '
        'TextUser
        '
        Me.TextUser.Location = New System.Drawing.Point(536, 32)
        Me.TextUser.Name = "TextUser"
        Me.TextUser.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextUser.Properties.Appearance.Options.UseFont = True
        Me.TextUser.Properties.MaxLength = 50
        Me.TextUser.Size = New System.Drawing.Size(175, 20)
        Me.TextUser.TabIndex = 5
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl6.Appearance.Options.UseFont = True
        Me.LabelControl6.Location = New System.Drawing.Point(413, 61)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(51, 14)
        Me.LabelControl6.TabIndex = 36
        Me.LabelControl6.Text = "Password"
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl5.Appearance.Options.UseFont = True
        Me.LabelControl5.Location = New System.Drawing.Point(413, 35)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(59, 14)
        Me.LabelControl5.TabIndex = 35
        Me.LabelControl5.Text = "User Name"
        '
        'TextServerName
        '
        Me.TextServerName.Location = New System.Drawing.Point(164, 32)
        Me.TextServerName.Name = "TextServerName"
        Me.TextServerName.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextServerName.Properties.Appearance.Options.UseFont = True
        Me.TextServerName.Properties.MaxLength = 100
        Me.TextServerName.Size = New System.Drawing.Size(150, 20)
        Me.TextServerName.TabIndex = 3
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl3.Appearance.Options.UseFont = True
        Me.LabelControl3.Location = New System.Drawing.Point(19, 35)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(70, 14)
        Me.LabelControl3.TabIndex = 33
        Me.LabelControl3.Text = "Server Name"
        '
        'ComboAuthMode
        '
        Me.ComboAuthMode.EditValue = "Windows"
        Me.ComboAuthMode.Location = New System.Drawing.Point(536, 6)
        Me.ComboAuthMode.Name = "ComboAuthMode"
        Me.ComboAuthMode.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboAuthMode.Properties.Appearance.Options.UseFont = True
        Me.ComboAuthMode.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboAuthMode.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboAuthMode.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboAuthMode.Properties.DropDownRows = 3
        Me.ComboAuthMode.Properties.Items.AddRange(New Object() {"Windows", "SQL Server"})
        Me.ComboAuthMode.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.ComboAuthMode.Size = New System.Drawing.Size(175, 20)
        Me.ComboAuthMode.TabIndex = 2
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl4.Appearance.Options.UseFont = True
        Me.LabelControl4.Location = New System.Drawing.Point(413, 9)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(115, 14)
        Me.LabelControl4.TabIndex = 31
        Me.LabelControl4.Text = "Authentication Mode"
        '
        'ComboDBType
        '
        Me.ComboDBType.EditValue = "SQL Server"
        Me.ComboDBType.Location = New System.Drawing.Point(164, 6)
        Me.ComboDBType.Name = "ComboDBType"
        Me.ComboDBType.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboDBType.Properties.Appearance.Options.UseFont = True
        Me.ComboDBType.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboDBType.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboDBType.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboDBType.Properties.AppearanceFocused.Options.UseFont = True
        Me.ComboDBType.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboDBType.Properties.Items.AddRange(New Object() {"SQL Server", "Oracle", "MY SQL"})
        Me.ComboDBType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.ComboDBType.Size = New System.Drawing.Size(150, 20)
        Me.ComboDBType.TabIndex = 1
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl2.Appearance.Options.UseFont = True
        Me.LabelControl2.Location = New System.Drawing.Point(19, 9)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(82, 14)
        Me.LabelControl2.TabIndex = 29
        Me.LabelControl2.Text = "Database Type"
        '
        'PanelControl2
        '
        Me.PanelControl2.Controls.Add(Me.ToggleParallel)
        Me.PanelControl2.Controls.Add(Me.LabelControl1)
        Me.PanelControl2.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanelControl2.Location = New System.Drawing.Point(2, 2)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(1161, 31)
        Me.PanelControl2.TabIndex = 1
        '
        'ToggleParallel
        '
        Me.ToggleParallel.Location = New System.Drawing.Point(164, 3)
        Me.ToggleParallel.Name = "ToggleParallel"
        Me.ToggleParallel.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleParallel.Properties.Appearance.Options.UseFont = True
        Me.ToggleParallel.Properties.OffText = "No"
        Me.ToggleParallel.Properties.OnText = "Yes"
        Me.ToggleParallel.Size = New System.Drawing.Size(95, 25)
        Me.ToggleParallel.TabIndex = 1
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Location = New System.Drawing.Point(19, 8)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(65, 14)
        Me.LabelControl1.TabIndex = 28
        Me.LabelControl1.Text = "Parallel Data"
        '
        'MemoEdit1
        '
        Me.MemoEdit1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.MemoEdit1.EditValue = resources.GetString("MemoEdit1.EditValue")
        Me.MemoEdit1.Location = New System.Drawing.Point(0, 0)
        Me.MemoEdit1.Name = "MemoEdit1"
        Me.MemoEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.MemoEdit1.Properties.Appearance.Options.UseFont = True
        Me.MemoEdit1.Properties.MaxLength = 100000
        Me.MemoEdit1.Properties.ReadOnly = True
        Me.MemoEdit1.Size = New System.Drawing.Size(80, 568)
        Me.MemoEdit1.TabIndex = 4
        '
	 'CheckC
        '
        Me.CheckC.Location = New System.Drawing.Point(426, 285)
        Me.CheckC.Name = "CheckC"
        Me.CheckC.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckC.Properties.Appearance.Options.UseFont = True
        Me.CheckC.Properties.Caption = "Celsius"
        Me.CheckC.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckC.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckC.Properties.ValueChecked = "Y"
        Me.CheckC.Properties.ValueGrayed = "N"
        Me.CheckC.Properties.ValueUnchecked = "N"
        Me.CheckC.Size = New System.Drawing.Size(59, 19)
        Me.CheckC.TabIndex = 25
        '
        'TextTempR
        '
        Me.TextTempR.Location = New System.Drawing.Point(164, 284)
        Me.TextTempR.Name = "TextTempR"
        Me.TextTempR.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextTempR.Properties.Appearance.Options.UseFont = True
        Me.TextTempR.Properties.MaxLength = 50
        Me.TextTempR.Size = New System.Drawing.Size(175, 20)
        Me.TextTempR.TabIndex = 24
        '
        'LabelControl36
        '
        Me.LabelControl36.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl36.Appearance.Options.UseFont = True
        Me.LabelControl36.Location = New System.Drawing.Point(19, 287)
        Me.LabelControl36.Name = "LabelControl36"
        Me.LabelControl36.Size = New System.Drawing.Size(72, 14)
        Me.LabelControl36.TabIndex = 110
        Me.LabelControl36.Text = "Temperature"
        '
        'CheckF
        '
        Me.CheckF.EditValue = "Y"
        Me.CheckF.Location = New System.Drawing.Point(561, 285)
        Me.CheckF.Name = "CheckF"
        Me.CheckF.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckF.Properties.Appearance.Options.UseFont = True
        Me.CheckF.Properties.Caption = "Fahrenheit "
        Me.CheckF.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckF.Properties.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked
        Me.CheckF.Properties.ValueChecked = "Y"
        Me.CheckF.Properties.ValueGrayed = "N"
        Me.CheckF.Properties.ValueUnchecked = "N"
        Me.CheckF.Size = New System.Drawing.Size(114, 19)
        Me.CheckF.TabIndex = 26
	
        'DateEditFrmDate
        '
        Me.DateEditFrmDate.EditValue = Nothing
        Me.DateEditFrmDate.Location = New System.Drawing.Point(164, 362)
        Me.DateEditFrmDate.Name = "DateEditFrmDate"
        Me.DateEditFrmDate.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.DateEditFrmDate.Properties.Appearance.Options.UseFont = True
        Me.DateEditFrmDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEditFrmDate.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEditFrmDate.Properties.Mask.EditMask = "yyyy-MM-dd HH:mm:ss"
        Me.DateEditFrmDate.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.DateEditFrmDate.Size = New System.Drawing.Size(175, 20)
        Me.DateEditFrmDate.TabIndex = 105
        '
        'LabelControl35
        '
        Me.LabelControl35.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl35.Appearance.Options.UseFont = True
        Me.LabelControl35.Location = New System.Drawing.Point(19, 365)
        Me.LabelControl35.Name = "LabelControl35"
        Me.LabelControl35.Size = New System.Drawing.Size(132, 14)
        Me.LabelControl35.TabIndex = 106
        Me.LabelControl35.Text = "Manual Insert from Date"
        '
        'BtnManualAdd
        '
        Me.BtnManualAdd.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.BtnManualAdd.Appearance.Options.UseFont = True
        Me.BtnManualAdd.Location = New System.Drawing.Point(427, 359)
        Me.BtnManualAdd.Name = "BtnManualAdd"
        Me.BtnManualAdd.Size = New System.Drawing.Size(87, 25)
        Me.BtnManualAdd.TabIndex = 107
        Me.BtnManualAdd.Text = "Manual Insert"
        '
        'XtraParallelSetting
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.SplitContainerControl1)
        Me.Name = "XtraParallelSetting"
        Me.Size = New System.Drawing.Size(1250, 600)
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerControl1.ResumeLayout(False)
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.TextName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextDafVal3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextDafVal2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextDafVal1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextRes3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextRes2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextRes1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextMNoValue.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextMYesValue.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextManual.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextOutValue.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextInValue.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextPunchDirection.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextDateTimeFormat.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextTimeFormat.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextDateFormat.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextDeviceId.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextPunchDateTime.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextPunchTime.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextPunchDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextPriTextPay.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextPriFixLenghtPay.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckPrefixPrePaycode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextPriTextPre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextPriFixLenghtPre.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckPrefixPreCard.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextPayCode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextPresentCardNo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextTblName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl4.ResumeLayout(False)
        CType(Me.PanelControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl3.ResumeLayout(False)
        Me.PanelControl3.PerformLayout()
        CType(Me.TextDBName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextPwd.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextUser.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextServerName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboAuthMode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboDBType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl2.ResumeLayout(False)
        Me.PanelControl2.PerformLayout()
        CType(Me.ToggleParallel.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEditFrmDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEditFrmDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckC.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextTempR.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckF.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SplitContainerControl1 As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents MemoEdit1 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents ToggleParallel As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PanelControl3 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ComboDBType As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboAuthMode As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextServerName As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextPwd As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextUser As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PanelControl4 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents btnSave As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnTest As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents TextTblName As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextPresentCardNo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextPayCode As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents CheckPrefixPreCard As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents TextPriFixLenghtPre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextPriTextPre As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextPriTextPay As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextPriFixLenghtPay As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents CheckPrefixPrePaycode As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents TextDeviceId As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextPunchDateTime As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextPunchTime As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextPunchDate As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextTimeFormat As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl19 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextDateFormat As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl18 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextDateTimeFormat As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl20 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextOutValue As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl23 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextInValue As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl22 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextPunchDirection As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl21 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextMNoValue As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl24 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextMYesValue As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl25 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextManual As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl26 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextRes3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl29 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextRes2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl28 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextRes1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl27 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextDafVal3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl32 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextDafVal2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl31 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextDafVal1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl30 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextDBName As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl33 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextName As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl34 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents BtnManualAdd As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl35 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents DateEditFrmDate As DevExpress.XtraEditors.DateEdit
    Friend WithEvents CheckF As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckC As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents TextTempR As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl36 As DevExpress.XtraEditors.LabelControl
End Class
