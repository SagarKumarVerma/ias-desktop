﻿Imports System.Resources
Imports System.Globalization
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports System.ComponentModel
Imports DevExpress.XtraGrid.Columns
Imports System.Data.SqlClient
Imports System.Text
Imports System.Data.OleDb
Imports DevExpress.XtraSplashScreen
Imports System.Net

Public Class XtraBulkSMS
    Dim ulf As UserLookAndFeel
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    Public Sub New()
        InitializeComponent()
    End Sub
    Private Sub XtraBulkSMS_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True

        Me.Width = Common.NavWidth 'Me.Parent.Width
        Me.Height = Common.NavHeight 'Me.Parent.Height
        'SplitContainerControl1.Width = Common.splitforMasterMenuWidth 'SplitContainerControl1.Parent.Width
        SplitContainerControl1.SplitterPosition = Common.SplitterPosition '(SplitContainerControl1.Parent.Width) * 85 / 100

        'If Common.servername = "Access" Then
        '    'Me.TblEmployee1TableAdapter1.Fill(Me.SSSDBDataSet.TblEmployee1)
        '    'GridControlEmp.DataSource = SSSDBDataSet.TblEmployee1

        '    Me.Tblbranch1TableAdapter1.Fill(Me.SSSDBDataSet.tblbranch1)
        '    GridControlBranch.DataSource = SSSDBDataSet.tblbranch1
        'Else
        '    'TblEmployeeTableAdapter.Connection.ConnectionString = Common.ConnectionString
        '    'Me.TblEmployeeTableAdapter.Fill(Me.SSSDBDataSet.TblEmployee)
        '    'GridControlEmp.DataSource = SSSDBDataSet.TblEmployee

        '    TblbranchTableAdapter.Connection.ConnectionString = Common.ConnectionString
        '    Me.TblbranchTableAdapter.Fill(Me.SSSDBDataSet.tblbranch)
        '    GridControlBranch.DataSource = SSSDBDataSet.tblbranch
        'End If
        GridControlBranch.DataSource = Common.LocationNonAdmin
        Common.SetGridFont(GridViewEmp, New System.Drawing.Font("Tahoma", 10))
        Common.SetGridFont(GridViewBranch, New System.Drawing.Font("Tahoma", 10))

        setEmpGrid()
    End Sub

    Private Sub SimpleButton2_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton2.Click
        If PopupContainerEditSMS.EditValue.ToString = "" Then
            XtraMessageBox.Show("<size>Please select Employee or Location</size>")
            PopupContainerEditSMS.Select()
            Exit Sub
        End If
        If MemoEdit2.Text.Trim = "" Then
            XtraMessageBox.Show("<size>Please Enter Message</size>")
            MemoEdit2.Select()
            Exit Sub
        End If
        'XtraMaster.LabelControl4.Text = "Sending Message."
        Dim request As HttpWebRequest
        Dim response As HttpWebResponse = Nothing
        'Dim url As String = "https://www.mobtexting.com/app/index.php/api?method=sms.normal&api_key=c666c755fd17adfaaa8382a836d6ee2e9ebffa26&to=" & Trim("8369499622") & "&sender=TMWTCH&message=" & "test SMS" & "&flash=0&unicode=1"
        'request = DirectCast(WebRequest.Create(url), HttpWebRequest)
        'response = DirectCast(request.GetResponse(), HttpWebResponse)
        Dim message As String = MemoEdit2.Text
        Dim url As String
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim ds As DataSet = New DataSet
        Dim str As String
        Dim cn As Common = New Common
        If CheckEmp.Checked = True Then
            Dim EmpArr() As String = PopupContainerEditSMS.EditValue.ToString.Trim.Split(",")
            For i As Integer = 0 To EmpArr.Length - 1
                str = "select TELEPHONE1 from TblEmployee where PAYCODE = '" & EmpArr(i).Trim & "'"
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(str, Common.con1)
                    adapA.Fill(ds)
                Else
                    adap = New SqlDataAdapter(str, Common.con)
                    adap.Fill(ds)
                End If
                'sms code
            Next

            Dim selectedRows As Integer() = GridViewEmp.GetSelectedRows()
            Dim result As Object() = New Object(selectedRows.Length - 1) {}
            For i As Integer = 0 To selectedRows.Length - 1
                Dim rowHandle As Integer = selectedRows(i)
                If Not GridViewEmp.IsGroupRow(rowHandle) Then
                    result(i) = GridViewEmp.GetRowCellValue(rowHandle, "TELEPHONE1")
                    'MsgBox(result(i).ToString)
                    Try
                        url = "https://www.mobtexting.com/app/index.php/api?method=sms.normal&api_key=c666c755fd17adfaaa8382a836d6ee2e9ebffa26&to=" & result(i).ToString.Trim & "&sender=TMWTCH&message=" & message & "&flash=0&unicode=1"
                        cn.sendSMS(result(i), message)
                        'request = DirectCast(WebRequest.Create(url), HttpWebRequest)
                        'response = DirectCast(request.GetResponse(), HttpWebResponse)
                    Catch
                    End Try
                End If
            Next

        Else
            Dim LocArr() As String = PopupContainerEditSMS.EditValue.ToString.Trim.Split(",")
            For i As Integer = 0 To LocArr.Length - 1
                str = "select PAYCODE, TELEPHONE1 from TblEmployee where BRANCHCODE = '" & LocArr(i).Trim & "'"
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(str, Common.con1)
                    adapA.Fill(ds)
                Else
                    adap = New SqlDataAdapter(str, Common.con)
                    adap.Fill(ds)
                End If
                For j As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    If ds.Tables(0).Rows(j).Item("TELEPHONE1").ToString.Trim <> "" Then
                        Try
                            cn.sendSMS(ds.Tables(0).Rows(j).Item("TELEPHONE1").ToString.Trim, message)
                            'url = "https://www.mobtexting.com/app/index.php/api?method=sms.normal&api_key=c666c755fd17adfaaa8382a836d6ee2e9ebffa26&to=" & ds.Tables(0).Rows(j).Item("TELEPHONE1").ToString.Trim & "&sender=TMWTCH&message=" & message & "&flash=0&unicode=1"
                            'request = DirectCast(WebRequest.Create(url), HttpWebRequest)
                            'response = DirectCast(request.GetResponse(), HttpWebResponse)
                        Catch ex As Exception
                        End Try
                        'sms code
                    End If
                Next
            Next
        End If
        Common.LogPost("Bulk SMS Sent")
        Dim msg As New Message("Sent Success", "Messagess has been sent")
        XtraMaster.AlertControl1.Show(XtraMaster, msg.Caption, msg.Text, "", msg.Image, msg)
    End Sub
    Private Sub setEmpGrid()
        Dim gridtblregisterselet As String
        gridtblregisterselet = "select * from TblEmployee where TELEPHONE1 <> '' and ACTIVE='Y'"
        Dim WTDataTable As DataTable
        If Common.servername = "Access" Then
            Dim dataAdapter As New OleDbDataAdapter(gridtblregisterselet, Common.con1)
            WTDataTable = New DataTable("TblEmployee")
            dataAdapter.Fill(WTDataTable)
        Else
            Dim dataAdapter As New SqlClient.SqlDataAdapter(gridtblregisterselet, Common.con)
            WTDataTable = New DataTable("TblEmployee")
            dataAdapter.Fill(WTDataTable)
        End If
        GridControlEmp.DataSource = WTDataTable
    End Sub
    Private Sub CheckEmp_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckEmp.CheckedChanged
        If CheckEmp.Checked = True Then
            PopupContainerEditSMS.EditValue = ""
            LabelControl2.Text = "Select Employee"
            PopupContainerEditSMS.Properties.PopupControl = PopupContainerControlEmp
        Else
            PopupContainerEditSMS.EditValue = ""
            LabelControl2.Text = "Select Location"
            PopupContainerEditSMS.Properties.PopupControl = PopupContainerControlBranch
            'LabelControl2.Visible = False
            'PopupContainerEditEmp.Visible = False
        End If
    End Sub

    'Private Sub CheckLocation_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckLocation.CheckedChanged
    '    If CheckLocation.Checked = True Then
    '        LabelControl8.Visible = True
    '        PopupContainerEditLocation.Visible = True
    '    Else
    '        LabelControl8.Visible = False
    '        PopupContainerEditLocation.Visible = False
    '    End If
    'End Sub
    Private Sub PopupContainerEditEmp_QueryResultValue(sender As System.Object, e As DevExpress.XtraEditors.Controls.QueryResultValueEventArgs) Handles PopupContainerEditSMS.QueryResultValue
        If CheckEmp.Checked = True Then
            Dim selectedRows() As Integer = GridViewEmp.GetSelectedRows
            Dim sb As StringBuilder = New StringBuilder
            For Each selectionRow As Integer In selectedRows
                Dim a As System.Data.DataRowView = GridViewEmp.GetRow(selectionRow)
                If (sb.ToString.Length > 0) Then
                    sb.Append(", ")
                End If
                sb.Append(a.Item("PAYCODE"))
            Next
            e.Value = sb.ToString
        Else
            Dim selectedRows() As Integer = GridViewBranch.GetSelectedRows
            Dim sb As StringBuilder = New StringBuilder
            For Each selectionRow As Integer In selectedRows
                Dim a As System.Data.DataRowView = GridViewBranch.GetRow(selectionRow)
                If (sb.ToString.Length > 0) Then
                    sb.Append(", ")
                End If
                sb.Append(a.Item("BRANCHCODE"))
            Next
            e.Value = sb.ToString
        End If
    End Sub
    Private Sub PopupContainerEditLocation_QueryResultValue(sender As System.Object, e As DevExpress.XtraEditors.Controls.QueryResultValueEventArgs) Handles PopupContainerEditLocation.QueryResultValue
        Dim selectedRows() As Integer = GridViewBranch.GetSelectedRows
        Dim sb As StringBuilder = New StringBuilder
        For Each selectionRow As Integer In selectedRows
            Dim a As System.Data.DataRowView = GridViewBranch.GetRow(selectionRow)
            If (sb.ToString.Length > 0) Then
                sb.Append(", ")
            End If
            sb.Append(a.Item("BRANCHCODE"))
        Next
        e.Value = sb.ToString
    End Sub

    Private Sub CheckLocation_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckLocation.CheckedChanged

    End Sub
End Class
