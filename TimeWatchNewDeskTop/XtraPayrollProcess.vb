﻿Imports System.Resources
Imports System.Globalization
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports System.ComponentModel
Imports DevExpress.XtraGrid.Columns
Imports System.Data.SqlClient
Imports System.Text
Imports System.Data.OleDb
Imports DevExpress.XtraSplashScreen
Imports System.Threading
Imports System.Threading.Tasks
Imports System.Text.RegularExpressions

Public Class XtraPayrollProcess
    Dim servername As String
    Dim ConnectionString As String
    Dim ulf As UserLookAndFeel
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    Dim g_pfmaxlimit As Double
    Public Sub New()
        InitializeComponent()
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True

        If Common.servername = "Access" Then
            'ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\TimeWatch.mdb;Persist Security Info=True;Jet OLEDB:Database Password=SSS"
        Else
            'ConnectionString = "Data Source=" & servername & ";Initial Catalog=SSSDB;Integrated Security=True"
            TblCompanyTableAdapter.Connection.ConnectionString = Common.ConnectionString '"Data Source=DESKTOP-PG6NDV8;Initial Catalog=SSSDB;Integrated Security=True"
            TblDepartmentTableAdapter.Connection.ConnectionString = Common.ConnectionString
            TblbranchTableAdapter.Connection.ConnectionString = Common.ConnectionString
            TblEmployeeTableAdapter.Connection.ConnectionString = Common.ConnectionString
        End If
        Common.SetGridFont(GridViewComp, New Font("Tahoma", 10))
        Common.SetGridFont(GridViewDept, New Font("Tahoma", 10))
        Common.SetGridFont(GridViewLocation, New Font("Tahoma", 10))
        Common.SetGridFont(GridViewEmp, New Font("Tahoma", 10))
    End Sub
    Private Sub XtraPayrollProcess_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Me.Width = Common.NavWidth 'Me.Parent.Width
        Me.Height = Common.NavHeight 'Me.Parent.Height
        'SplitContainerControl1.Width = Common.splitforMasterMenuWidth 'SplitContainerControl1.Parent.Width
        SplitContainerControl1.SplitterPosition = Common.SplitterPosition '(SplitContainerControl1.Parent.Width) * 85 / 100

        If Common.servername = "Access" Then
            Me.TblCompany1TableAdapter1.Fill(Me.SSSDBDataSet.tblCompany1)
            Me.TblDepartment1TableAdapter1.Fill(Me.SSSDBDataSet.tblDepartment1)
            'Me.Tblbranch1TableAdapter1.Fill(Me.SSSDBDataSet.tblbranch1)
            'Me.TblEmployee1TableAdapter1.Fill(Me.SSSDBDataSet.TblEmployee1)
            GridControlComp.DataSource = SSSDBDataSet.tblCompany1
            GridControlDept.DataSource = SSSDBDataSet.tblDepartment1
            'GridControlLocation.DataSource = SSSDBDataSet.tblbranch1
            'GridControlEmp.DataSource = SSSDBDataSet.TblEmployee1
        Else
            Me.TblCompanyTableAdapter.Fill(Me.SSSDBDataSet.tblCompany)
            Me.TblDepartmentTableAdapter.Fill(Me.SSSDBDataSet.tblDepartment)
            'Me.TblbranchTableAdapter.Fill(Me.SSSDBDataSet.tblbranch)
            'Me.TblEmployeeTableAdapter.Fill(Me.SSSDBDataSet.TblEmployee)
            GridControlComp.DataSource = SSSDBDataSet.tblCompany
            GridControlDept.DataSource = SSSDBDataSet.tblDepartment
            'GridControlLocation.DataSource = SSSDBDataSet.tblbranch
            'GridControlEmp.DataSource = SSSDBDataSet.TblEmployee
        End If
        GridControlEmp.DataSource = Common.EmpNonAdmin
        GridControlLocation.DataSource = Common.LocationNonAdmin

        If CheckEdit5.Checked = True Then
            LabelControl5.Visible = False
            LabelControl7.Visible = False
            ComboBoxEdit1.Visible = False
            PopupContainerEdit1.Visible = False
        End If
        'DateEditFrom.DateTime = New DateTime(DateTime.Now.Year, DateTime.Now.AddMonths(-1).Month, 1)
        'Dim DaysInMonth As Integer = Date.DaysInMonth(DateTime.Now.AddMonths(-1).Year, DateTime.Now.AddMonths(-1).Month)
        'DateEditTo.DateTime = New DateTime(DateTime.Now.Year, DateTime.Now.AddMonths(-1).Month, DaysInMonth)

        DateEditFrom.DateTime = Now.AddMonths(-1).ToString("01/MM/yyyy")
        Dim DaysInMonth As Integer = Date.DaysInMonth(DateEditFrom.DateTime.Year, DateEditFrom.DateTime.Month)
        DateEditTo.DateTime = DateEditFrom.DateTime.ToString(DaysInMonth & "/MM/yyyy")

        If Common.IsNepali = "Y" Then
            ComboNepaliYearFrm.Visible = True
            ComboNEpaliMonthFrm.Visible = True
            ComboNepaliDateFrm.Visible = True
            ComboNepaliYearTo.Visible = True
            ComboNEpaliMonthTo.Visible = True
            ComboNepaliDateTo.Visible = True

            DateEditFrom.Visible = False
            DateEditTo.Visible = False

            Dim DC As New DateConverter()
            Dim doj As String = DC.ToBS(New Date(DateEditFrom.DateTime.Year, DateEditFrom.DateTime.Month, DateEditFrom.DateTime.Day))
            Dim dojTmp() As String = doj.Split("-")
            ComboNepaliYearFrm.EditValue = dojTmp(0)
            ComboNEpaliMonthFrm.SelectedIndex = dojTmp(1) - 1
            ComboNepaliDateFrm.EditValue = dojTmp(2)

            doj = DC.ToBS(New Date(DateEditTo.DateTime.Year, DateEditTo.DateTime.Month, DateEditTo.DateTime.Day))         
            dojTmp = doj.Split("-")
            ComboNepaliYearTo.EditValue = dojTmp(0)
            ComboNEpaliMonthTo.SelectedIndex = dojTmp(1) - 1
            ComboNepaliDateTo.EditValue = dojTmp(2)
        Else
            ComboNepaliYearFrm.Visible = False
            ComboNEpaliMonthFrm.Visible = False
            ComboNepaliDateFrm.Visible = False
            ComboNepaliYearTo.Visible = False
            ComboNEpaliMonthTo.Visible = False
            ComboNepaliDateTo.Visible = False
            DateEditFrom.Visible = True
            DateEditTo.Visible = True
        End If

    End Sub
    Private Sub CheckEdit5_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckEdit5.CheckedChanged
        If CheckEdit5.Checked = True Then
            LabelControl5.Visible = False
            LabelControl7.Visible = False
            ComboBoxEdit1.Visible = False
            PopupContainerEdit1.Visible = False
        End If
    End Sub
    Private Sub CheckEdit6_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckEdit6.CheckedChanged
        If CheckEdit6.Checked = True Then
            ComboBoxEdit1.EditValue = "Paycode"
            LabelControl5.Visible = True
            LabelControl7.Visible = True
            ComboBoxEdit1.Visible = True
            PopupContainerEdit1.Visible = True
            selectpopupdropdown()
        End If
    End Sub
    Private Sub ComboBoxEdit1_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBoxEdit1.SelectedIndexChanged
        selectpopupdropdown()
    End Sub
    Private Sub DateEditFrom_Leave(sender As System.Object, e As System.EventArgs) Handles DateEditFrom.Leave
        DateEditTo.DateTime = DateEditFrom.DateTime.AddMonths(1).AddDays(-1)
    End Sub
    Private Sub DateEdit2_Leave(sender As System.Object, e As System.EventArgs) Handles DateEditTo.Leave
        If DateEditTo.DateTime < DateEditFrom.DateTime Then
            DateEditTo.DateTime = DateEditFrom.DateTime.AddMonths(1).AddDays(-1)
        End If
    End Sub
    Private Sub selectpopupdropdown()
        PopupContainerEdit1.EditValue = ""
        If ComboBoxEdit1.EditValue = "Paycode" Then
            LabelControl5.Visible = True
            LabelControl5.Text = "Select Paycode"
            'TextEdit3.Visible = True
            PopupContainerEdit1.Visible = True
            PopupContainerEdit1.Properties.PopupControl = PopupContainerControlEmp
        ElseIf ComboBoxEdit1.EditValue = "Company" Then
            LabelControl5.Visible = True
            LabelControl5.Text = "Select Company"
            PopupContainerEdit1.Visible = True
            PopupContainerEdit1.Properties.PopupControl = PopupContainerControlCompany
        ElseIf ComboBoxEdit1.EditValue = "Location" Then
            LabelControl5.Visible = True
            LabelControl5.Text = "Select Location"
            PopupContainerEdit1.Visible = True
            PopupContainerEdit1.Properties.PopupControl = PopupContainerControlLocation
        ElseIf ComboBoxEdit1.EditValue = "Department" Then
            LabelControl5.Visible = True
            LabelControl5.Text = "Select Department"
            PopupContainerEdit1.Visible = True
            PopupContainerEdit1.Properties.PopupControl = PopupContainerControlDept
        End If
    End Sub
    Private Sub PopupContainerEdit1_QueryPopUp(sender As System.Object, e As System.ComponentModel.CancelEventArgs) Handles PopupContainerEdit1.QueryPopUp
        If ComboBoxEdit1.EditValue = "Paycode" Then
            Dim val As Object = PopupContainerEdit1.EditValue
            If (val Is Nothing) Then
                GridViewEmp.ClearSelection()
            Else
                Dim texts() As String = val.ToString.Split(",")
                For Each text As String In texts
                    If text.Trim.Length = 1 Then
                        text = text.Trim & "  "
                    ElseIf text.Trim.Length = 2 Then
                        text = text.Trim & " "
                    End If
                    Dim rowHandle As Integer = GridViewComp.LocateByValue("PAYCODE", text)
                    GridViewEmp.SelectRow(rowHandle)
                Next
            End If
        ElseIf ComboBoxEdit1.EditValue = "Company" Then
            Dim val As Object = PopupContainerEdit1.EditValue
            If (val Is Nothing) Then
                GridViewComp.ClearSelection()
            Else
                Dim texts() As String = val.ToString.Split(",")
                For Each text As String In texts
                    If text.Trim.Length = 1 Then
                        text = text.Trim & "  "
                    ElseIf text.Trim.Length = 2 Then
                        text = text.Trim & " "
                    End If
                    Dim rowHandle As Integer = GridViewComp.LocateByValue("COMPANYCODE", text)
                    GridViewComp.SelectRow(rowHandle)
                Next
            End If
        ElseIf ComboBoxEdit1.EditValue = "Location" Then
            Dim val As Object = PopupContainerEdit1.EditValue
            If (val Is Nothing) Then
                GridViewLocation.ClearSelection()
            Else
                Dim texts() As String = val.ToString.Split(",")
                For Each text As String In texts
                    If text.Trim.Length = 1 Then
                        text = text.Trim & "  "
                    ElseIf text.Trim.Length = 2 Then
                        text = text.Trim & " "
                    End If
                    Dim rowHandle As Integer = GridViewDept.LocateByValue("BRANCHCODE", text)
                    GridViewLocation.SelectRow(rowHandle)
                Next
            End If
        ElseIf ComboBoxEdit1.EditValue = "Department" Then
            Dim val As Object = PopupContainerEdit1.EditValue
            If (val Is Nothing) Then
                GridViewDept.ClearSelection()
            Else
                Dim texts() As String = val.ToString.Split(",")
                For Each text As String In texts
                    If text.Trim.Length = 1 Then
                        text = text.Trim & "  "
                    ElseIf text.Trim.Length = 2 Then
                        text = text.Trim & " "
                    End If
                    Dim rowHandle As Integer = GridViewDept.LocateByValue("DEPARTMENTCODE", text)
                    GridViewDept.SelectRow(rowHandle)
                Next
            End If
        End If

    End Sub
    Private Sub PopupContainerEdit1_QueryResultValue(sender As System.Object, e As DevExpress.XtraEditors.Controls.QueryResultValueEventArgs) Handles PopupContainerEdit1.QueryResultValue
        If ComboBoxEdit1.EditValue = "Paycode" Then
            Dim selectedRows() As Integer = GridViewEmp.GetSelectedRows
            Dim sb As StringBuilder = New StringBuilder
            For Each selectionRow As Integer In selectedRows
                Dim a As System.Data.DataRowView = GridViewEmp.GetRow(selectionRow)
                If (sb.ToString.Length > 0) Then
                    sb.Append(", ")
                End If
                sb.Append(a.Item("Paycode"))
            Next
            e.Value = sb.ToString
        ElseIf ComboBoxEdit1.EditValue = "Company" Then
            Dim selectedRows() As Integer = GridViewComp.GetSelectedRows
            Dim sb As StringBuilder = New StringBuilder
            For Each selectionRow As Integer In selectedRows
                Dim a As System.Data.DataRowView = GridViewComp.GetRow(selectionRow)
                If (sb.ToString.Length > 0) Then
                    sb.Append(", ")
                End If
                sb.Append(a.Item("COMPANYCODE"))
            Next
            e.Value = sb.ToString
        ElseIf ComboBoxEdit1.EditValue = "Location" Then
            Dim selectedRows() As Integer = GridViewLocation.GetSelectedRows
            Dim sb As StringBuilder = New StringBuilder
            For Each selectionRow As Integer In selectedRows
                Dim a As System.Data.DataRowView = GridViewLocation.GetRow(selectionRow)
                If (sb.ToString.Length > 0) Then
                    sb.Append(", ")
                End If
                sb.Append(a.Item("BRANCHCODE"))
            Next
            e.Value = sb.ToString
        ElseIf ComboBoxEdit1.EditValue = "Department" Then
            Dim selectedRows() As Integer = GridViewDept.GetSelectedRows
            Dim sb As StringBuilder = New StringBuilder
            For Each selectionRow As Integer In selectedRows
                Dim a As System.Data.DataRowView = GridViewDept.GetRow(selectionRow)
                If (sb.ToString.Length > 0) Then
                    sb.Append(", ")
                End If
                sb.Append(a.Item("DEPARTMENTCODE"))
            Next
            e.Value = sb.ToString
        End If

    End Sub
    Private Sub SimpleButtonCapture_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonCapture.Click
        Dim lsEmp As New List(Of String)()
        Dim sSql As String
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        If CheckEdit5.Checked = True Then
            Dim ds As DataSet = New DataSet
            'sSql = "select TblEmployee.PAYCODE, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK from TblEmployee, tblEmployeeShiftMaster where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.ACTIVE = 'Y'"
            sSql = "select PAYCODE from TblEmployee where ACTIVE = 'Y'"
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
            If ds.Tables(0).Rows.Count > 0 Then
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    Dim pay As String = ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim
                    lsEmp.Add(pay)
                Next
            End If
        ElseIf CheckEdit6.Checked = True Then
            'Dim sSql As String
            If ComboBoxEdit1.EditValue = "Paycode" Then
                Dim paycode() As String = PopupContainerEdit1.EditValue.ToString.Split(",")
                For i As Integer = 0 To paycode.Length - 1
                    lsEmp.Add(paycode(i))
                Next
            ElseIf ComboBoxEdit1.EditValue = "Company" Then
                Dim com() As String = PopupContainerEdit1.EditValue.ToString.Split(",")
                Dim ls As New List(Of String)()
                For i As Integer = 0 To com.Length - 1
                    ls.Add(com(i).Trim)
                Next
                'select PAYCODE from TblEmployee where COMPANYCODE IN ('01')  and ACTIVE = 'Y'
                sSql = "select TblEmployee.PAYCODE, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK from TblEmployee, tblEmployeeShiftMaster where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.COMPANYCODE IN ('" & String.Join("', '", ls.ToArray()) & "')  and TblEmployee.ACTIVE = 'Y'"
                Dim ds As DataSet = New DataSet
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(ds)
                Else
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(ds)
                End If
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    lsEmp.Add(ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim())
                Next
            ElseIf ComboBoxEdit1.EditValue = "Department" Then
                Dim com() As String = PopupContainerEdit1.EditValue.ToString.Split(",")
                Dim ls As New List(Of String)()
                For i As Integer = 0 To com.Length - 1
                    ls.Add(com(i).Trim)
                Next
                'select PAYCODE from TblEmployee where COMPANYCODE IN ('01')  and ACTIVE = 'Y'
                sSql = "select TblEmployee.PAYCODE, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK from TblEmployee, tblEmployeeShiftMaster where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.DEPARTMENTCODE IN ('" & String.Join("', '", ls.ToArray()) & "')  and TblEmployee.ACTIVE = 'Y'"
                Dim ds As DataSet = New DataSet
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(ds)
                Else
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(ds)
                End If
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    lsEmp.Add(ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim())
                Next
            ElseIf ComboBoxEdit1.EditValue = "Location" Then
                Dim com() As String = PopupContainerEdit1.EditValue.ToString.Split(",")
                Dim ls As New List(Of String)()
                For i As Integer = 0 To com.Length - 1
                    ls.Add(com(i).Trim)
                Next
                'select PAYCODE from TblEmployee where COMPANYCODE IN ('01')  and ACTIVE = 'Y'
                sSql = "select TblEmployee.PAYCODE, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK from TblEmployee, tblEmployeeShiftMaster where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "')  and TblEmployee.ACTIVE = 'Y'"
                Dim ds As DataSet = New DataSet
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(ds)
                Else
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(ds)
                End If
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    lsEmp.Add(ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim())
                Next
            End If
        End If
        Dim empErr() As String = lsEmp.ToArray

        Dim bDataCapture As Boolean
        Dim bProcess As Boolean
        Dim testtr As DataSet 'ADODB.Recordset
        Dim rsReport As DataSet 'ADODB.Recordset
        Dim rsarrear As DataSet 'ADODB.Recordset
        Dim ProcDate As Date

        If Common.IsNepali = "Y" Then
            Dim DC As New DateConverter()
            Try
                'DateEditFrom.DateTime = DC.ToAD(New Date(ComboNepaliYearFrm.EditValue, ComboNEpaliMonthFrm.SelectedIndex + 1, ComboNepaliDateFrm.EditValue))
                DateEditFrom.DateTime = DC.ToAD(ComboNepaliYearFrm.EditValue & "-" & ComboNEpaliMonthFrm.SelectedIndex + 1 & "-" & ComboNepaliDateFrm.EditValue)
            Catch ex As Exception
                XtraMessageBox.Show(ulf, "<size=10>Invalid From Date</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                ComboNepaliDateFrm.Select()
                Exit Sub
            End Try

            Try
                'DateEditTo.DateTime = DC.ToAD(New Date(ComboNepaliYearTo.EditValue, ComboNEpaliMonthTo.SelectedIndex + 1, ComboNepaliDateTo.EditValue))
                DateEditTo.DateTime = DC.ToAD(ComboNepaliYearTo.EditValue & "-" & ComboNEpaliMonthTo.SelectedIndex + 1 & "-" & ComboNepaliDateTo.EditValue)
            Catch ex As Exception
                XtraMessageBox.Show(ulf, "<size=10>Invalid To Date</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                ComboNepaliDateTo.Select()
                Exit Sub
            End Try
        End If

        If DateEditFrom.ToString = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Invalid Date From Date</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            DateEditFrom.Select()
            Exit Sub
        End If
        If DateEditTo.ToString = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Invalid Date To Date</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            DateEditTo.Select()
            Exit Sub
        End If

        sSql = "select max([mon_year])as [month] from pay_result"
        testtr = New DataSet
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(testtr)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(testtr)
        End If
        'testtr = Cn.Execute(sSql)
        If testtr.Tables(0).Rows(0).Item("month").ToString.Trim <> "" Then '  If testtr.Tables(0).Rows.Count > 0 Then 'If testtr.RecordCount > 0 Then
            If testtr.Tables(0).Rows(0).Item("month").ToString.Trim = "" Then
                ProcDate = Convert.ToDateTime(testtr.Tables(0).Rows(0).Item("month").ToString.Trim)
                If Convert.ToDateTime(testtr.Tables(0).Rows(0).Item("month").ToString.Trim).ToString("yyyyMM") > DateEditTo.DateTime.ToString("yyyyMM") Then 'If Format(testtr("month"), "yyyymm") > Format(txtDateTo.Value, "yyyymm") Then
                    'If MsgBox("Processing Month Already Closed,You Want Recapture as per Current Setup?", vbYesNo) = vbNo Then
                    '    Exit Sub
                    'End If
                    If XtraMessageBox.Show(ulf, "<size=10>Processing Month Already Closed,You Want Recapture as per Current Setup?</size>", Common.res_man.GetString("confirmdeleteion", Common.cul), _
                             MessageBoxButtons.YesNo, MessageBoxIcon.Question) <> DialogResult.Yes Then
                        Exit Sub
                    End If
                ElseIf DateEditTo.DateTime.ToString("yyyyMM") > ProcDate.ToString("yyyyMM") Then '  ElseIf (txtDateTo.Value, "yyyymm") > Format(DateAdd("m", 1, ProcDate), "yyyymm") Then
                    Me.Cursor = Cursors.Default
                    XtraMessageBox.Show(ulf, "<size=10>Previous Month Processing has not been done</size>", "<size=9>Error</size>")
                    'MsgBox("Previous Month Processing has not been done", vbInformation)
                    Exit Sub
                End If
            End If
        End If
        'Screen.MousePointer = vbHourglass
        'Me.Height = 3600
        '**** code for capturing the data from tbltimeregister ****
        'If txtPaycodeFrom.Text <> txtPaycodeTo.Text Then
        'If Len(Trim(txtPaycodeFrom.Text)) > 0 And Len(Trim(txtPaycodeTo.Text)) Then
        testtr = New DataSet
        If Common.servername = "Access" Then
            sSql = "select * FROM PAY_REIMURSH  where PAID='Y' AND FORMAT(Mon_Year,'MM')=" & DateEditTo.DateTime.ToString("MM") & " And FORMAT(Mon_Year,'YYYY')=" & DateEditTo.DateTime.ToString("yyyy") & " AND PAYCODE IN ('" & String.Join("', '", lsEmp.ToArray()) & "')  " '"
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(testtr)
        Else
            sSql = "select * FROM PAY_REIMURSH  where PAID='Y' AND DatePart(mm,Mon_Year)=" & DateEditTo.DateTime.ToString("MM") & " And DatePart(yyyy,Mon_Year)=" & DateEditTo.DateTime.ToString("yyyy") & " AND PAYCODE IN ('" & String.Join("', '", lsEmp.ToArray()) & "') "
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(testtr)
        End If
        'testtr = Cn.Execute(sSql)

        If testtr.Tables(0).Rows.Count > 0 Then '.RecordCount > 0 Then
            Me.Cursor = Cursors.Default
            If Common.IsNepali = "Y" Then
                XtraMessageBox.Show(ulf, "<size=10>Already Reimbursment paid to some body for Month " & ComboNEpaliMonthTo.EditValue & " " & ComboNepaliYearTo.EditValue & ",Can not be Reprocess</size>", "<size=9>Error</size>")
            Else
                XtraMessageBox.Show(ulf, "<size=10>Already Reimbursment paid to some body for Month " & DateEditTo.DateTime.ToString("MMM yyyy") & ",Can not be Reprocess</size>", "<size=9>Error</size>")
            End If
            Exit Sub
        End If

        testtr = New DataSet
        If Common.servername = "Access" Then
            sSql = "Select * From Pay_result Where  FORMAT(Mon_Year,'MM')=" & DateEditFrom.DateTime.ToString("MM") & " And FORMAT(Mon_Year,'YYYY')=" & DateEditFrom.DateTime.ToString("yyyy")
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(testtr)
        Else
            sSql = "Select * From Pay_result Where  DatePart(mm,Mon_Year)=" & DateEditFrom.DateTime.ToString("MM") & " And DatePart(yyyy,Mon_Year)=" & DateEditFrom.DateTime.ToString("yyyy")
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(testtr)
        End If
        'testtr = Cn.Execute(sSql)
        If testtr.Tables(0).Rows.Count > 0 Then '.RecordCount > 0 Then
            'If MsgBox("Data has already been capture for the specified period. Do you want to overwrite it ?", vbYesNo) = vbNo Then
            If XtraMessageBox.Show(ulf, "<size=10>Data has already been capture for the specified period. Do you want to overwrite it ?</size>", Common.res_man.GetString("confirmdeleteion", Common.cul), _
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question) <> DialogResult.Yes Then
                'sSql = "select A.*,B.VEmployeeType,B.pfulimit,C.DATEOFBIRTH from pay_result A,PAY_MASTER B,TBLEMPLOYEE C where A.PAYCODE=B.PAYCODE AND B.PAYCODE=C.PAYCODE AND A.PAYCODE IN ('" & String.Join("', '", lsEmp.ToArray()) & "') And DatePart(mm,A.Mon_Year)=" & DateEditTo.DateTime.ToString("MM") & " And DatePart(yy,A.Mon_Year)=" & DateEditTo.DateTime.ToString("yyyy")
                If Common.servername = "Access" Then
                    sSql = "select A.*,B.VEmployeeType,B.pfulimit,C.DATEOFBIRTH from pay_result A,PAY_MASTER B,TBLEMPLOYEE C where A.PAYCODE=B.PAYCODE AND B.PAYCODE=C.PAYCODE AND A.PAYCODE IN ('" & String.Join("', '", lsEmp.ToArray()) & "') And DatePart('m',A.Mon_Year)=" & DateEditTo.DateTime.ToString("MM") & " And DatePart('yyyy',A.Mon_Year)=" & DateEditTo.DateTime.ToString("yyyy")
                Else
                    sSql = "select A.*,B.VEmployeeType,B.pfulimit,C.DATEOFBIRTH from pay_result A,PAY_MASTER B,TBLEMPLOYEE C where A.PAYCODE=B.PAYCODE AND B.PAYCODE=C.PAYCODE AND A.PAYCODE IN ('" & String.Join("', '", lsEmp.ToArray()) & "') And DatePart(mm,A.Mon_Year)=" & DateEditTo.DateTime.ToString("MM") & " And DatePart(yyyy,A.Mon_Year)=" & DateEditTo.DateTime.ToString("yyyy")
                End If
                GoTo OverWrite
            End If
        End If
        'End If
        If DATACAPTURE(empErr) = False Then
            Me.Cursor = Cursors.Default
            'XtraMessageBox.Show(ulf, "<size=10>No data found in Performance date between " & DateEditFrom.DateTime.ToString("dd/MM/yyyy") & " to " & DateEditTo.DateTime.ToString("dd/MM/yyyy") & Chr(13) & "</size>", "<size=9>Error</size>")
            XtraMessageBox.Show(ulf, "<size=10>No data found in Performance date between Date Range</size>", "<size=9>Error</size>")
            'Progress.Visible = False
            'txtPaycodeFrom.SetFocus()
            Exit Sub
        End If
        'Screen.MousePointer = vbHourglass
        testtr = New DataSet
        If Common.servername = "Access" Then
            sSql = "select A.*,B.VEmployeeType,B.pfulimit,C.DATEOFBIRTH from pay_result A,PAY_MASTER B,TBLEMPLOYEE C where A.PAYCODE=B.PAYCODE AND B.PAYCODE=C.PAYCODE AND A.PAYCODE IN ('" & String.Join("', '", lsEmp.ToArray()) & "') And FORMAT(A.Mon_Year,'MM')=" & DateEditTo.DateTime.ToString("MM") & " And FORMAT(A.Mon_Year,'YYYY')=" & DateEditTo.DateTime.ToString("yyyy")
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(testtr)
        Else
            sSql = "select A.*,B.VEmployeeType,B.pfulimit,C.DATEOFBIRTH from pay_result A,PAY_MASTER B,TBLEMPLOYEE C where A.PAYCODE=B.PAYCODE AND B.PAYCODE=C.PAYCODE AND A.PAYCODE IN ('" & String.Join("', '", lsEmp.ToArray()) & "') And DatePart(mm,A.Mon_Year)=" & DateEditTo.DateTime.ToString("MM") & " And DatePart(yyyy,A.Mon_Year)=" & DateEditTo.DateTime.ToString("yyyy")
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(testtr)
        End If
        'testtr = Cn.Execute(sSql)
        If testtr.Tables(0).Rows.Count = 0 Then '.RecordCount = 0 Then
            Me.Cursor = Cursors.Default
            If Common.IsNepali = "Y" Then
                XtraMessageBox.Show(ulf, "<size=10>Please capture data for " & ComboNEpaliMonthTo.EditValue & "</size>", "<size=9>Error</size>")
            Else
                XtraMessageBox.Show(ulf, "<size=10>Please capture data for " & DateEditTo.DateTime.ToString("MMM yyyy") & "</size>", "<size=9>Error</size>")
            End If
            rsReport = Nothing
            Exit Sub
        End If
OverWrite:
        bProcess = DoProcess(sSql)
        XtraMasterTest.LabelControlStatus.Text = ""
        Application.DoEvents()
        'Screen.MousePointer = vbNormal
        If bProcess Then
            Me.Cursor = Cursors.Default
            If Common.IsNepali = "Y" Then
                XtraMessageBox.Show(ulf, "<size=10>Processing successful of " & ComboNEpaliMonthTo.EditValue & " " & ComboNepaliYearTo.EditValue & "</size>", "<size=9>Success</size>")
            Else
                XtraMessageBox.Show(ulf, "<size=10>Processing successful of " & DateEditTo.DateTime.ToString("MMM yyyy") & "</size>", "<size=9>Success</size>")
            End If
        End If
    End Sub
    ''*************************************************************************
    ''    This function is use for capturing data through tbltimeregister table
    ''    and after captured creat TR00000 table for capture month
    ''*************************************************************************
    Function DATACAPTURE(empErr() As String) As Boolean
        Dim HrMin As hourToMinute = New hourToMinute
        Dim ls As New List(Of String)()
        For i As Integer = 0 To empErr.Length - 1
            ls.Add(empErr(i).Trim)
        Next
        Dim sSql As String
        Dim rsCapture As DataSet = New DataSet 'ADODB.Recordset
        Dim rsCheckTable As DataSet = New DataSet 'ADODB.Recordset
        Dim rsMonthTable As DataSet = New DataSet 'ADODB.Recordset
        Dim rsMonthTable_REIMURSH As DataSet = New DataSet 'ADODB.Recordset
        Dim rspaymaster As DataSet = New DataSet 'ADODB.Recordset
        Dim rsPiecedata As DataSet = New DataSet 'ADODB.Recordset
        Dim sPaycode As String
        Dim iVTotHrsWorked As Integer : Dim iVOTDuration As Double
        Dim iVEarlyDeparture As Integer, iVLateArrival As Integer
        Dim iEarlyDays As Integer, iLateDays As Integer
        Dim iVLossHours As Integer, iPresentValue As Double
        Dim iAbsentValue As Double, iHolidayValue As Double
        Dim iWOValue As Double, sLeaveCode As String '* 3
        Dim iVCL As Double, iVSL As Double
        Dim iVPL_EL As Double, iVOther_LV As Double
        Dim iTotalLeave As Double, iLeaveAmount As Double
        Dim sMonth As String '* 6
        Dim iTotalCount As Long
        Dim iTotalWorkinghrs As Double
        Dim mRsEmployee As DataSet = New DataSet 'ADODB.Recordset
        Dim iTotalDays As Double, mLeaveType As String, mLeaveType1 As String, mLeaveType2 As String
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter

        'On Error GoTo ErrHand
        If Common.servername = "Access" Then
            sSql = "Select a.PAYCODE,a.DATEOFFICE,a.HOURSWORKED,a.OTDURATION,a.EARLYDEPARTURE, e.PRESENTCARDNO," &
           "a.LATEARRIVAL,a.TOTALLOSSHRS,a.LEAVEVALUE,a.PRESENTVALUE,a.ABSENTVALUE," &
           "a.HOLIDAY_VALUE,a.WO_VALUE,a.LEAVECODE,a.LEAVEAMOUNT,a.leavetype1,a.leavetype2,a.leavetype,a.firsthalfleavecode,a.SECONDHALFLEAVECODE,a.leaveamount1,a.leaveamount2,B.VEmployeeType From tblTimeRegister a,pay_master b, tblemployee e " &
           "Where e.paycode=a.paycode and e.paycode=b.paycode and a.paycode=b.paycode and a.PAYCODE IN ('" & String.Join("', '", ls.ToArray()) & "') " &
           "And FORMAT(DATEOFFICE,'yyyy/MM/dd') Between '" & DateEditFrom.DateTime.ToString("yyyy/MM/dd") & "' " &
           "And '" & DateEditTo.DateTime.ToString("yyyy/MM/dd") & "' order by e.PRESENTCARDNO " ', a.DateOffice
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(rsCapture)
        Else
            sSql = "Select a.PAYCODE,a.DATEOFFICE,a.HOURSWORKED,a.OTDURATION,a.EARLYDEPARTURE, e.PRESENTCARDNO," &
           "a.LATEARRIVAL,a.TOTALLOSSHRS,a.LEAVEVALUE,a.PRESENTVALUE,a.ABSENTVALUE," &
           "a.HOLIDAY_VALUE,a.WO_VALUE,a.LEAVECODE,a.LEAVEAMOUNT,a.leavetype1,a.leavetype2,a.leavetype,a.firsthalfleavecode,a.SECONDHALFLEAVECODE,a.leaveamount1,a.leaveamount2,B.VEmployeeType From tblTimeRegister a,pay_master b ,tblemployee e " &
           "Where e.paycode=a.paycode and e.paycode=b.paycode and a.paycode=b.paycode and a.PAYCODE IN ('" & String.Join("', '", ls.ToArray()) & "') " &
           "And DATEOFFICE Between '" & DateEditFrom.DateTime.ToString("yyyy/MM/dd") & "' " &
           "And '" & DateEditTo.DateTime.ToString("yyyy/MM/dd") & "' order by  e.PRESENTCARDNO " ' , a.DateOffice"
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(rsCapture)
        End If
        'rsCapture = Cn.Execute(sSql)
        iTotalCount = rsCapture.Tables(0).Rows.Count 'RecordCount
        If rsCapture.Tables(0).Rows.Count >= 1 Then  'If rsCapture.RecordCount >= 1 Then
            'Screen.MousePointer = vbHourglass
            Me.Cursor = Cursors.WaitCursor
            '*****for toDate table************************
            sMonth = DateEditTo.DateTime.ToString("MMyyyy") 'CStr(Format(txtDateTo.Value, "MMyyyy"))
            'DATACAPTURE = True
            'rsCapture.Sort = "Paycode,DateOffice"
            For i As Integer = 0 To rsCapture.Tables(0).Rows.Count - 1
                'Do While Not rsCapture.EOF
                iTotalWorkinghrs = 0
                sPaycode = rsCapture.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim
                iVTotHrsWorked = 0 : iVOTDuration = 0 : iVEarlyDeparture = 0
                iVLateArrival = 0 : iVLossHours = 0 : iPresentValue = 0
                iAbsentValue = 0 : iHolidayValue = 0 : iWOValue = 0
                sLeaveCode = "" : iVCL = 0 : iVSL = 0
                iVPL_EL = 0 : iVOther_LV = 0 : iTotalLeave = 0
                iLeaveAmount = 0 : iEarlyDays = 0 : iLateDays = 0
                iTotalDays = 0

                XtraMasterTest.LabelControlStatus.Text = "Capturing data for Paycode No.:" & Trim(sPaycode)
                Application.DoEvents()
                'Progress.Value = rsCapture.AbsolutePosition / iTotalCount * 100

                sSql = "select paycode,leavingdate from tblemployee where paycode='" & sPaycode & "' "
                mRsEmployee = New DataSet
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(mRsEmployee)
                Else
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(mRsEmployee)
                End If
                'mRsEmployee = Cn.Execute(sSql)

                Do While Trim(sPaycode) = rsCapture.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim
                    If rsCapture.Tables(0).Rows(i).Item("HOURSWORKED").ToString.Trim <> "" Then
                        iVTotHrsWorked = iVTotHrsWorked + rsCapture.Tables(0).Rows(i).Item("HOURSWORKED")
                    End If
                    'iVTotHrsWorked = iVTotHrsWorked + IIf(rsCapture.Tables(0).Rows(i).Item("HOURSWORKED").ToString.Trim = "", 0, rsCapture.Tables(0).Rows(i).Item("HOURSWORKED"))

                    If rsCapture.Tables(0).Rows(i).Item("OTDURATION").ToString.Trim <> "" Then
                        iVOTDuration = iVOTDuration + rsCapture.Tables(0).Rows(i).Item("OTDURATION")
                    End If
                    'iVOTDuration = iVOTDuration + IIf(rsCapture.Tables(0).Rows(i).Item("OTDURATION").ToString.Trim = "", 0, rsCapture.Tables(0).Rows(i).Item("OTDURATION"))
                    If rsCapture.Tables(0).Rows(i).Item("EARLYDEPARTURE").ToString.Trim <> "" Then
                        iVEarlyDeparture = iVEarlyDeparture + rsCapture.Tables(0).Rows(i).Item("EARLYDEPARTURE")
                    End If
                    'iVEarlyDeparture = iVEarlyDeparture + IIf(rsCapture.Tables(0).Rows(i).Item("EARLYDEPARTURE").ToString.Trim = "", 0, rsCapture.Tables(0).Rows(i).Item("EARLYDEPARTURE"))
                    If rsCapture.Tables(0).Rows(i).Item("EARLYDEPARTURE").ToString.Trim <> "" Then
                        If rsCapture.Tables(0).Rows(i).Item("EARLYDEPARTURE") > 0 Then iEarlyDays = iEarlyDays + 1
                    End If
                    If rsCapture.Tables(0).Rows(i).Item("LATEARRIVAL").ToString.Trim <> "" Then iVLateArrival = iVLateArrival + rsCapture.Tables(0).Rows(i).Item("LATEARRIVAL")
                    'iVLateArrival = iVLateArrival + IIf(rsCapture.Tables(0).Rows(i).Item("LATEARRIVAL").ToString.Trim = "", 0, rsCapture.Tables(0).Rows(i).Item("LATEARRIVAL"))
                    If rsCapture.Tables(0).Rows(i).Item("LATEARRIVAL").ToString.Trim <> "" Then
                        If rsCapture.Tables(0).Rows(i).Item("LATEARRIVAL") > 0 Then iLateDays = iLateDays + 1
                    End If
                    If rsCapture.Tables(0).Rows(i).Item("TOTALLOSSHRS").ToString.Trim <> "" Then iVLossHours = iVLossHours + rsCapture.Tables(0).Rows(i).Item("TOTALLOSSHRS")
                    'iVLossHours = iVLossHours + rsCapture.Tables(0).Rows(i).Item("TOTALLOSSHRS")
                    If rsCapture.Tables(0).Rows(i).Item("PRESENTVALUE").ToString.Trim <> "" Then iPresentValue = iPresentValue + rsCapture.Tables(0).Rows(i).Item("PRESENTVALUE")
                    'iPresentValue = iPresentValue + rsCapture.Tables(0).Rows(i).Item("PRESENTVALUE")
                    If rsCapture.Tables(0).Rows(i).Item("ABSENTVALUE").ToString.Trim <> "" Then iAbsentValue = iAbsentValue + rsCapture.Tables(0).Rows(i).Item("ABSENTVALUE")
                    'iAbsentValue = iAbsentValue + rsCapture.Tables(0).Rows(i).Item("ABSENTVALUE")
                    If rsCapture.Tables(0).Rows(i).Item("HOLIDAY_VALUE").ToString.Trim <> "" Then iHolidayValue = iHolidayValue + rsCapture.Tables(0).Rows(i).Item("HOLIDAY_VALUE")
                    'iHolidayValue = iHolidayValue + rsCapture.Tables(0).Rows(i).Item("HOLIDAY_VALUE")
                    If rsCapture.Tables(0).Rows(i).Item("WO_VALUE").ToString.Trim <> "" Then iWOValue = iWOValue + rsCapture.Tables(0).Rows(i).Item("WO_VALUE")
                    'iWOValue = iWOValue + rsCapture.Tables(0).Rows(i).Item("WO_VALUE")
                    mLeaveType = ""
                    mLeaveType1 = ""
                    mLeaveType2 = ""
                    If rsCapture.Tables(0).Rows(i).Item("leavetype").ToString.Trim <> "" Then
                        mLeaveType = rsCapture.Tables(0).Rows(i).Item("leavetype").ToString.Trim
                    End If
                    If rsCapture.Tables(0).Rows(i).Item("leavetype1").ToString.Trim <> "" Then
                        mLeaveType1 = rsCapture.Tables(0).Rows(i).Item("leavetype1").ToString.Trim
                    End If
                    If rsCapture.Tables(0).Rows(i).Item("leavetype2").ToString.Trim <> "" Then
                        mLeaveType2 = rsCapture.Tables(0).Rows(i).Item("leavetype2").ToString.Trim
                    End If
                    If (UCase(Trim(rsCapture.Tables(0).Rows(i).Item("firsthalfleavecode").ToString.Trim)) <> "" And UCase(Trim(mLeaveType1)) = "L") Or (UCase(Trim(rsCapture.Tables(0).Rows(i).Item("SECONDHALFLEAVECODE").ToString.Trim)) <> "" And UCase(Trim(mLeaveType2)) = "L") Then
                        If UCase(Trim(rsCapture.Tables(0).Rows(i).Item("firsthalfleavecode"))) = "CL" Then iVCL = iVCL + rsCapture.Tables(0).Rows(i).Item("leaveamount1")
                        If UCase(Trim(rsCapture.Tables(0).Rows(i).Item("firsthalfleavecode"))) = "EL" Then iVPL_EL = iVPL_EL + rsCapture.Tables(0).Rows(i).Item("leaveamount1")
                        If mLeaveType = "L" Then
                            If UCase(Trim(rsCapture.Tables(0).Rows(i).Item("firsthalfleavecode").ToString.Trim)) <> "EL" And UCase(Trim(rsCapture.Tables(0).Rows(i).Item("firsthalfleavecode").ToString.Trim)) <> "CL" And UCase(Trim(rsCapture.Tables(0).Rows(i).Item("firsthalfleavecode").ToString.Trim)) <> "PL" And UCase(Trim(rsCapture.Tables(0).Rows(i).Item("firsthalfleavecode").ToString.Trim)) <> "SL" Then
                                iVOther_LV = iVOther_LV + rsCapture.Tables(0).Rows(i).Item("leaveamount1")
                            End If
                        End If
                        If UCase(Trim(rsCapture.Tables(0).Rows(i).Item("firsthalfleavecode").ToString.Trim)) = "PL" Then iVPL_EL = iVPL_EL + rsCapture.Tables(0).Rows(i).Item("leaveamount1")
                        If UCase(Trim(rsCapture.Tables(0).Rows(i).Item("firsthalfleavecode").ToString.Trim)) = "SL" Then iVSL = iVSL + rsCapture.Tables(0).Rows(i).Item("leaveamount1")

                        If UCase(Trim(rsCapture.Tables(0).Rows(i).Item("SECONDHALFLEAVECODE").ToString.Trim)) = "CL" Then iVCL = iVCL + rsCapture.Tables(0).Rows(i).Item("leaveamount2")
                        If UCase(Trim(rsCapture.Tables(0).Rows(i).Item("SECONDHALFLEAVECODE").ToString.Trim)) = "EL" Then iVPL_EL = iVPL_EL + rsCapture.Tables(0).Rows(i).Item("leaveamount2")
                        If mLeaveType = "L" Then
                            If UCase(Trim(rsCapture.Tables(0).Rows(i).Item("SECONDHALFLEAVECODE").ToString.Trim)) <> "EL" And UCase(Trim(rsCapture.Tables(0).Rows(i).Item("SECONDHALFLEAVECODE").ToString.Trim)) <> "CL" And UCase(Trim(rsCapture.Tables(0).Rows(i).Item("SECONDHALFLEAVECODE").ToString.Trim)) <> "PL" And UCase(Trim(rsCapture.Tables(0).Rows(i).Item("SECONDHALFLEAVECODE").ToString.Trim)) <> "SL" Then
                                iVOther_LV = iVOther_LV + rsCapture.Tables(0).Rows(i).Item("leaveamount2")
                            End If
                        End If
                        If UCase(Trim(rsCapture.Tables(0).Rows(i).Item("SECONDHALFLEAVECODE").ToString.Trim)) = "PL" Then iVPL_EL = iVPL_EL + rsCapture.Tables(0).Rows(i).Item("leaveamount2")
                        If UCase(Trim(rsCapture.Tables(0).Rows(i).Item("SECONDHALFLEAVECODE").ToString.Trim)) = "SL" Then iVSL = iVSL + rsCapture.Tables(0).Rows(i).Item("leaveamount2")
                    Else
                        If UCase(Trim(mLeaveType)) = "L" Then
                            If UCase(Trim(rsCapture.Tables(0).Rows(i).Item("leavecode").ToString.Trim)) = "CL" Then iVCL = iVCL + rsCapture.Tables(0).Rows(i).Item("leaveamount")
                            If UCase(Trim(rsCapture.Tables(0).Rows(i).Item("leavecode").ToString.Trim)) = "EL" Then iVPL_EL = iVPL_EL + rsCapture.Tables(0).Rows(i).Item("leaveamount")
                            If UCase(Trim(rsCapture.Tables(0).Rows(i).Item("leavecode").ToString.Trim)) <> "EL" And UCase(Trim(rsCapture.Tables(0).Rows(i).Item("leavecode").ToString.Trim)) <> "CL" And UCase(Trim(rsCapture.Tables(0).Rows(i).Item("leavecode").ToString.Trim)) <> "PL" And UCase(Trim(rsCapture.Tables(0).Rows(i).Item("leavecode").ToString.Trim)) <> "SL" Then
                                iVOther_LV = iVOther_LV + rsCapture.Tables(0).Rows(i).Item("leaveamount")
                            End If
                            If UCase(Trim(rsCapture.Tables(0).Rows(i).Item("leavecode").ToString.Trim)) = "PL" Then iVPL_EL = iVPL_EL + rsCapture.Tables(0).Rows(i).Item("leaveamount")
                            If UCase(Trim(rsCapture.Tables(0).Rows(i).Item("leavecode").ToString.Trim)) = "SL" Then iVSL = iVSL + rsCapture.Tables(0).Rows(i).Item("leaveamount")
                        End If
                    End If
                    If rsCapture.Tables(0).Rows(i).Item("LEAVEVALUE").ToString.Trim = "" Then
                        iLeaveAmount = iLeaveAmount + 0
                    Else
                        iLeaveAmount = iLeaveAmount + rsCapture.Tables(0).Rows(i).Item("LEAVEVALUE")
                    End If
                    'iLeaveAmount = iLeaveAmount + IIf(IsNull(rsCapture.Tables(0).Rows(i).Item("LEAVEVALUE")), 0, rsCapture.Tables(0).Rows(i).Item("LEAVEVALUE"))
                    'rsCapture.MoveNext()
                    i = i + 1
                    If i > rsCapture.Tables(0).Rows.Count - 1 Then
                        Exit Do
                    End If
                    'If rsCapture.AbsolutePosition = adPosEOF Then
                    '    Exit Do
                    'Else
                    '    Progress.Value = rsCapture.AbsolutePosition / iTotalCount * 100
                    'End If
                    If Convert.ToDateTime(mRsEmployee.Tables(0).Rows(0).Item("LeavingDate").ToString.Trim).ToString("yyyy-MM-dd") > "1800-01-01" Then
                        If Convert.ToDateTime(rsCapture.Tables(0).Rows(i).Item("DATEOFFICE").ToString.Trim) > Convert.ToDateTime(mRsEmployee.Tables(0).Rows(0).Item("LeavingDate").ToString.Trim) Then
                            Do While Trim(sPaycode) = Trim(rsCapture.Tables(0).Rows(i).Item("PAYCODE").ToString)
                                i = i + 1
                                'rsCapture.MoveNext()
                                If i > rsCapture.Tables(0).Rows.Count - 1 Then
                                    'If rsCapture.EOF Then
                                    Exit Do
                                End If
                            Loop
                        End If
                    End If
                    'If rsCapture.EOF Then
                    '    Exit Do
                    'End If

                    If i > rsCapture.Tables(0).Rows.Count - 1 Then
                        Exit Do
                    End If
                Loop
                iTotalLeave = iVCL + iVSL + iVPL_EL + iVOther_LV
                iTotalDays = iPresentValue + iWOValue + iHolidayValue + iLeaveAmount

                Try   'nitin
                    iVOTDuration = minutetohour(iVOTDuration)
                Catch ex As Exception
                    'iVOTDuration = minutetohour(iVOTDuration)
                End Try

                rsMonthTable = New DataSet
                If Common.servername = "Access" Then
                    sSql = "Select Paycode From Pay_result" & " Where Paycode='" & Trim(sPaycode) & "' And FORMAT(Mon_Year,'MM')=" & DateEditTo.DateTime.ToString("MM") & " And FORMAT(Mon_Year,'YYYY')=" & DateEditTo.DateTime.ToString("yyyy")
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(rsMonthTable)
                Else
                    sSql = "Select Paycode From Pay_result" & " Where Paycode='" & Trim(sPaycode) & "' And DatePart(mm,Mon_Year)=" & DateEditTo.DateTime.ToString("MM") & " And DatePart(yyyy,Mon_Year)=" & DateEditTo.DateTime.ToString("yyyy")
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(rsMonthTable)
                End If
                'rsMonthTable = Cn.Execute(sSql)

                rsMonthTable_REIMURSH = New DataSet
                If Common.servername = "Access" Then
                    sSql = "Select Paycode From PAY_REIMURSH" & " Where Paycode='" & Trim(sPaycode) & "' And FORMAT(Mon_Year,'MM')=" & DateEditTo.DateTime.ToString("MM") & " And FORMAT(Mon_Year,'YYYY')=" & DateEditTo.DateTime.ToString("yyyy")
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(rsMonthTable_REIMURSH)
                Else
                    sSql = "Select Paycode From PAY_REIMURSH" & " Where Paycode='" & Trim(sPaycode) & "' And DatePart(mm,Mon_Year)=" & DateEditTo.DateTime.ToString("MM") & " And DatePart(yyyy,Mon_Year)=" & DateEditTo.DateTime.ToString("yyyy")
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(rsMonthTable_REIMURSH)
                End If
                'rsMonthTable_REIMURSH = Cn.Execute(sSql)

                rspaymaster = New DataSet
                If Common.servername = "Access" Then
                    sSql = "Select * From Pay_masterT" & " Where Paycode='" & Trim(sPaycode) & "' and " &
                    " EFFECTFROM Between #" & DateEditFrom.DateTime.ToString("yyyy-MM-dd") & "# " &
                    "And #" & DateEditTo.DateTime.ToString("yyyy-MM-dd") & "#"
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(rspaymaster)
                Else
                    sSql = "Select * From Pay_masterT" & " Where Paycode='" & Trim(sPaycode) & "' and " &
                    " EFFECTFROM Between '" & DateEditFrom.DateTime.ToString("yyyy-MM-dd") & "' " &
                    "And '" & DateEditTo.DateTime.ToString("yyyy-MM-dd") & "'"
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(rspaymaster)
                End If
                'rspaymaster = Cn.Execute(sSql)
                If rspaymaster.Tables(0).Rows.Count = 0 Then 'RecordCount = 0 Then
                    rspaymaster = New DataSet
                    If Common.servername = "Access" Then
                        '#" & Format(frmMonthlyattReport.txtFromDate.Value, "MMM dd yyyy") & "#
                        sSql = "Select * From Pay_master" & " Where Paycode='" & Trim(sPaycode) & "' and EFFECTFROM<=#" & DateEditFrom.DateTime.ToString("yyyy-MM-dd") & "#"
                        adapA = New OleDbDataAdapter(sSql, Common.con1)
                        adapA.Fill(rspaymaster)
                    Else
                        sSql = "Select * From Pay_master" & " Where Paycode='" & Trim(sPaycode) & "' and EFFECTFROM<='" & DateEditFrom.DateTime.ToString("yyyy-MM-dd") & "'"
                        adap = New SqlDataAdapter(sSql, Common.con)
                        adap.Fill(rspaymaster)
                    End If
                    'rspaymaster = Cn.Execute(sSql)
                End If
                If rspaymaster.Tables(0).Rows.Count > 0 Then '.RecordCount > 0 Then
                    If rsMonthTable.Tables(0).Rows.Count = 1 Then 'RecordCount = 1 Then
                        If rspaymaster.Tables(0).Rows.Count = 1 Then 'RecordCount = 1 Then
                            sSql = "Update Pay_Result Set VBASIC=" & rspaymaster.Tables(0).Rows(0).Item("VBASIC").ToString.Trim & ",VDA_RATE=" & rspaymaster.Tables(0).Rows(0).Item("VDA_RATE").ToString.Trim & ",VDA_F='" & rspaymaster.Tables(0).Rows(0).Item("VDA_F").ToString.Trim & "'" &
                                ",VCONV_RATE=" & rspaymaster.Tables(0).Rows(0).Item("VCONV_RATE").ToString.Trim & ",VCONV_F='" & rspaymaster.Tables(0).Rows(0).Item("VCONV_F").ToString.Trim & "'" &
                                ",VMED_RATE=" & rspaymaster.Tables(0).Rows(0).Item("VMED_RATE").ToString.Trim & ",VMED_F='" & rspaymaster.Tables(0).Rows(0).Item("VMED_F").ToString.Trim & "'" &
                                ",PF_ALLOWED='" & rspaymaster.Tables(0).Rows(0).Item("PF_ALLOWED").ToString.Trim & "',VPF_ALLOWED='" & rspaymaster.Tables(0).Rows(0).Item("VPF_ALLOWED").ToString.Trim & "'," &
                                "VOT_RATE=" & rspaymaster.Tables(0).Rows(0).Item("VOT_RATE").ToString.Trim & ",VOT_FLAG='" & rspaymaster.Tables(0).Rows(0).Item("VOT_FLAG").ToString.Trim & "'," &
                                "VHRA_RATE=" & rspaymaster.Tables(0).Rows(0).Item("VHRA_RATE").ToString.Trim & ",VHRA_F='" & rspaymaster.Tables(0).Rows(0).Item("VHRA_F").ToString.Trim & "',ESI_ALLOWED='" & rspaymaster.Tables(0).Rows(0).Item("ESI_ALLOWED").ToString.Trim & "'," &
                                "VDT_1='" & rspaymaster.Tables(0).Rows(0).Item("VDT_1").ToString.Trim & "',VD_1=" & rspaymaster.Tables(0).Rows(0).Item("VD_1").ToString.Trim & ",VDT_2='" & rspaymaster.Tables(0).Rows(0).Item("VDT_2").ToString.Trim & "',VD_2=" & rspaymaster.Tables(0).Rows(0).Item("VD_2").ToString.Trim &
                                ",VDT_3='" & rspaymaster.Tables(0).Rows(0).Item("VDT_3").ToString.Trim & "',VD_3=" & rspaymaster.Tables(0).Rows(0).Item("VD_3").ToString.Trim & ",VDT_4='" & rspaymaster.Tables(0).Rows(0).Item("VDT_4").ToString.Trim & "',VD_4=" & rspaymaster.Tables(0).Rows(0).Item("VD_4").ToString.Trim &
                                ",VDT_5='" & rspaymaster.Tables(0).Rows(0).Item("VDT_5").ToString.Trim & "',VD_5=" & rspaymaster.Tables(0).Rows(0).Item("VD_5").ToString.Trim & ",VDT_6='" & rspaymaster.Tables(0).Rows(0).Item("VDT_6").ToString.Trim & "',VD_6=" & rspaymaster.Tables(0).Rows(0).Item("VD_6").ToString.Trim &
                                ",VDT_7='" & rspaymaster.Tables(0).Rows(0).Item("VDT_7").ToString.Trim & "',VD_7=" & rspaymaster.Tables(0).Rows(0).Item("VD_7").ToString.Trim & ",VDT_8='" & rspaymaster.Tables(0).Rows(0).Item("VDT_8").ToString.Trim & "',VD_8=" & rspaymaster.Tables(0).Rows(0).Item("VD_8").ToString.Trim &
                                ",VDT_9='" & rspaymaster.Tables(0).Rows(0).Item("VDT_9").ToString.Trim & "',VD_9=" & rspaymaster.Tables(0).Rows(0).Item("VD_9").ToString.Trim & ",VDT_10='" & rspaymaster.Tables(0).Rows(0).Item("VDT_10").ToString.Trim & "',VD_10=" & rspaymaster.Tables(0).Rows(0).Item("VD_10").ToString.Trim &
                                ",VIT_1='" & rspaymaster.Tables(0).Rows(0).Item("VIT_1").ToString.Trim & "',VI_1=" & rspaymaster.Tables(0).Rows(0).Item("VI_1").ToString.Trim & ",VIT_2='" & rspaymaster.Tables(0).Rows(0).Item("VIT_2").ToString.Trim & "',VI_2=" & rspaymaster.Tables(0).Rows(0).Item("VI_2").ToString.Trim &
                                ",VIT_3='" & rspaymaster.Tables(0).Rows(0).Item("VIT_3").ToString.Trim & "',VI_3=" & rspaymaster.Tables(0).Rows(0).Item("VI_3").ToString.Trim & ",VIT_4='" & rspaymaster.Tables(0).Rows(0).Item("VIT_4").ToString.Trim & "',VI_4=" & rspaymaster.Tables(0).Rows(0).Item("VI_4").ToString.Trim &
                                ",VIT_5='" & rspaymaster.Tables(0).Rows(0).Item("VIT_5").ToString.Trim & "',VI_5=" & rspaymaster.Tables(0).Rows(0).Item("VI_5").ToString.Trim & ",VIT_6='" & rspaymaster.Tables(0).Rows(0).Item("VIT_6").ToString.Trim & "',VI_6=" & rspaymaster.Tables(0).Rows(0).Item("VI_6").ToString.Trim &
                                ",VIT_7='" & rspaymaster.Tables(0).Rows(0).Item("VIT_7").ToString.Trim & "',VI_7=" & rspaymaster.Tables(0).Rows(0).Item("VI_7").ToString.Trim & ",VIT_8='" & rspaymaster.Tables(0).Rows(0).Item("VIT_8").ToString.Trim & "',VI_8=" & rspaymaster.Tables(0).Rows(0).Item("VI_8").ToString.Trim &
                                ",VIT_9='" & rspaymaster.Tables(0).Rows(0).Item("VIT_9").ToString.Trim & "',VI_9=" & rspaymaster.Tables(0).Rows(0).Item("VI_9").ToString.Trim & ",VIT_10='" & rspaymaster.Tables(0).Rows(0).Item("VIT_10").ToString.Trim & "',VI_10=" & rspaymaster.Tables(0).Rows(0).Item("VI_10").ToString.Trim &
                                ",VTDS_RATE=" & rspaymaster.Tables(0).Rows(0).Item("VTDS_RATE").ToString.Trim & ",VTDS_F='" & rspaymaster.Tables(0).Rows(0).Item("VTDS_F").ToString.Trim & "'" &
                                ",PROF_TAX_Allowed='" & rspaymaster.Tables(0).Rows(0).Item("PROF_TAX_AllOwed").ToString.Trim & "'"
                            If Common.servername = "Access" Then
                                sSql = sSql + " where pay_Result.Paycode='" & Trim(sPaycode) & "' And FORMAT(Mon_Year,'mm')=" & DateEditTo.DateTime.ToString("MM") & " And FORMAT(Mon_Year,'YYYY')=" & DateEditTo.DateTime.ToString("yyyy")
                                If Common.con1.State <> ConnectionState.Open Then
                                    Common.con1.Open()
                                End If
                                cmd1 = New OleDbCommand(sSql, Common.con1)
                                cmd1.ExecuteNonQuery()
                                If Common.con1.State <> ConnectionState.Closed Then
                                    Common.con1.Close()
                                End If
                            Else
                                sSql = sSql + " where pay_Result.Paycode='" & Trim(sPaycode) & "' And DatePart(mm,Mon_Year)=" & DateEditTo.DateTime.ToString("MM") & " And DatePart(yyyy,Mon_Year)=" & DateEditTo.DateTime.ToString("yyyy")
                                If Common.con.State <> ConnectionState.Open Then
                                    Common.con.Open()
                                End If
                                cmd = New SqlCommand(sSql, Common.con)
                                cmd.ExecuteNonQuery()
                                If Common.con.State <> ConnectionState.Closed Then
                                    Common.con.Close()
                                End If
                            End If


                            'sSql = "Update Pay_Result Set VOT=" & iVOTDuration &
                            '   ",VT_EARLY=" & iVEarlyDeparture & ",VT_LATE=" & iVLateArrival / 60 &
                            '   ",VPRE=" & iPresentValue & ",VABS=" & iAbsentValue &
                            '   ",VHLD=" & iHolidayValue & ",VCL=" & iVCL & ",VSL=" & iVSL &
                            '   ",VPL_EL=" & iVPL_EL & ",VOTHER_LV=" & iVOther_LV & ",VLEAVE=" & iLeaveAmount &
                            '   ",VLATE=" & iLateDays & ",VEARLY=" & iEarlyDays & ",VWO=" & iWOValue & ",VTDAYS=" & iTotalDays

                            'VT_EARLY = " & iVTotHrsWorked 'changed in in below query for working hours
                            sSql = "Update Pay_Result Set VOT=" & iVOTDuration &
                               ",VT_EARLY=" & iVTotHrsWorked & ",VT_LATE=" & iVLateArrival / 60 &
                               ",VPRE=" & iPresentValue & ",VABS=" & iAbsentValue &
                               ",VHLD=" & iHolidayValue & ",VCL=" & iVCL & ",VSL=" & iVSL &
                               ",VPL_EL=" & iVPL_EL & ",VOTHER_LV=" & iVOther_LV & ",VLEAVE=" & iLeaveAmount &
                               ",VLATE=" & iLateDays & ",VEARLY=" & iEarlyDays & ",VWO=" & iWOValue & ",VTDAYS=" & iTotalDays
                            If Common.servername = "Access" Then
                                sSql = sSql + " where pay_Result.Paycode='" & Trim(sPaycode) & "' And FORMAT(Mon_Year,'MM')=" & DateEditTo.DateTime.ToString("MM") & " And FORMAT(mon_Year,'YYYY')=" & DateEditTo.DateTime.ToString("yyyy")
                                If Common.con1.State <> ConnectionState.Open Then
                                    Common.con1.Open()
                                End If
                                cmd1 = New OleDbCommand(sSql, Common.con1)
                                Dim x As Integer = cmd1.ExecuteNonQuery()
                                If Common.con1.State <> ConnectionState.Closed Then
                                    Common.con1.Close()
                                End If
                            Else
                                sSql = sSql + " where pay_Result.Paycode='" & Trim(sPaycode) & "' And DatePart(mm,Mon_Year)=" & DateEditTo.DateTime.ToString("MM") & " And DatePart(yyyy,Mon_Year)=" & DateEditTo.DateTime.ToString("yyyy")
                                If Common.con.State <> ConnectionState.Open Then
                                    Common.con.Open()
                                End If
                                cmd = New SqlCommand(sSql, Common.con)
                                cmd.ExecuteNonQuery()
                                If Common.con.State <> ConnectionState.Closed Then
                                    Common.con.Close()
                                End If
                            End If
                            'Cn.Execute(sSql)
                        End If
                    Else
                        If rspaymaster.Tables(0).Rows.Count = 1 Then 'RecordCount = 1 Then
                            sSql = "Insert Into Pay_Result(PAYCODE,mon_year) values('" & Trim(sPaycode) & "','" & DateEditTo.DateTime.ToString("yyyy-MM-01") & "')"
                            If Common.servername = "Access" Then
                                If Common.con1.State <> ConnectionState.Open Then
                                    Common.con1.Open()
                                End If
                                Try
                                    cmd1 = New OleDbCommand(sSql, Common.con1)
                                    cmd1.ExecuteNonQuery()
                                Catch ex As Exception

                                End Try

                                If Common.con1.State <> ConnectionState.Closed Then
                                    Common.con1.Close()
                                End If
                            Else
                                If Common.con.State <> ConnectionState.Open Then
                                    Common.con.Open()
                                End If
                                cmd = New SqlCommand(sSql, Common.con)
                                cmd.ExecuteNonQuery()
                                If Common.con.State <> ConnectionState.Closed Then
                                    Common.con.Close()
                                End If
                            End If
                            'Cn.Execute(sSql)

                            If rsMonthTable_REIMURSH.Tables(0).Rows.Count = 0 Then 'RecordCount = 0 Then
                                'sSql = "Insert Into PAY_REIMURSH(PAYCODE,mon_year) values('" & Trim(sPaycode) & "','" & Format("01/" & Format(txtDateTo.Value, "mm/yyyy"), "MMM dd yyyy") & "')"
                                sSql = "Insert Into PAY_REIMURSH(PAYCODE,mon_year) values('" & Trim(sPaycode) & "','" & DateEditTo.DateTime.ToString("yyyy-MM-01") & "')"
                                If Common.servername = "Access" Then
                                    If Common.con1.State <> ConnectionState.Open Then
                                        Common.con1.Open()
                                    End If
                                    cmd1 = New OleDbCommand(sSql, Common.con1)
                                    cmd1.ExecuteNonQuery()
                                    If Common.con1.State <> ConnectionState.Closed Then
                                        Common.con1.Close()
                                    End If
                                Else
                                    If Common.con.State <> ConnectionState.Open Then
                                        Common.con.Open()
                                    End If
                                    cmd = New SqlCommand(sSql, Common.con)
                                    cmd.ExecuteNonQuery()
                                    If Common.con.State <> ConnectionState.Closed Then
                                        Common.con.Close()
                                    End If
                                End If
                                'Cn.Execute(sSql)
                            End If

                            sSql = "Update Pay_Result Set VBASIC=" & rspaymaster.Tables(0).Rows(0).Item("VBASIC").ToString.Trim & ",VDA_RATE=" & rspaymaster.Tables(0).Rows(0).Item("VDA_RATE").ToString.Trim & ",VDA_F='" & rspaymaster.Tables(0).Rows(0).Item("VDA_F").ToString.Trim & "'" &
                                ",VCONV_RATE=" & rspaymaster.Tables(0).Rows(0).Item("VCONV_RATE").ToString.Trim & ",VCONV_F='" & rspaymaster.Tables(0).Rows(0).Item("VCONV_F").ToString.Trim & "'" &
                                ",VMED_RATE=" & rspaymaster.Tables(0).Rows(0).Item("VMED_RATE").ToString.Trim & ",VMED_F='" & rspaymaster.Tables(0).Rows(0).Item("VMED_F").ToString.Trim & "'" &
                                ",PF_ALLOWED='" & rspaymaster.Tables(0).Rows(0).Item("PF_ALLOWED").ToString.Trim & "',VPF_ALLOWED='" & rspaymaster.Tables(0).Rows(0).Item("VPF_ALLOWED").ToString.Trim & "'," &
                                "VOT_RATE=" & rspaymaster.Tables(0).Rows(0).Item("VOT_RATE").ToString.Trim & ",VOT_FLAG='" & rspaymaster.Tables(0).Rows(0).Item("VOT_FLAG").ToString.Trim & "'," &
                                "VHRA_RATE=" & rspaymaster.Tables(0).Rows(0).Item("VHRA_RATE").ToString.Trim & ",VHRA_F='" & rspaymaster.Tables(0).Rows(0).Item("VHRA_F").ToString.Trim & "',ESI_ALLOWED='" & rspaymaster.Tables(0).Rows(0).Item("ESI_ALLOWED").ToString.Trim & "'," &
                                "VDT_1='" & rspaymaster.Tables(0).Rows(0).Item("VDT_1").ToString.Trim & "',VD_1=" & rspaymaster.Tables(0).Rows(0).Item("VD_1").ToString.Trim & ",VDT_2='" & rspaymaster.Tables(0).Rows(0).Item("VDT_2").ToString.Trim & "',VD_2=" & rspaymaster.Tables(0).Rows(0).Item("VD_2").ToString.Trim &
                                ",VDT_3='" & rspaymaster.Tables(0).Rows(0).Item("VDT_3").ToString.Trim & "',VD_3=" & rspaymaster.Tables(0).Rows(0).Item("VD_3").ToString.Trim & ",VDT_4='" & rspaymaster.Tables(0).Rows(0).Item("VDT_4").ToString.Trim & "',VD_4=" & rspaymaster.Tables(0).Rows(0).Item("VD_4").ToString.Trim &
                                ",VDT_5='" & rspaymaster.Tables(0).Rows(0).Item("VDT_5").ToString.Trim & "',VD_5=" & rspaymaster.Tables(0).Rows(0).Item("VD_5").ToString.Trim & ",VDT_6='" & rspaymaster.Tables(0).Rows(0).Item("VDT_6").ToString.Trim & "',VD_6=" & rspaymaster.Tables(0).Rows(0).Item("VD_6").ToString.Trim &
                                ",VDT_7='" & rspaymaster.Tables(0).Rows(0).Item("VDT_7").ToString.Trim & "',VD_7=" & rspaymaster.Tables(0).Rows(0).Item("VD_7").ToString.Trim & ",VDT_8='" & rspaymaster.Tables(0).Rows(0).Item("VDT_8").ToString.Trim & "',VD_8=" & rspaymaster.Tables(0).Rows(0).Item("VD_8").ToString.Trim &
                                ",VDT_9='" & rspaymaster.Tables(0).Rows(0).Item("VDT_9").ToString.Trim & "',VD_9=" & rspaymaster.Tables(0).Rows(0).Item("VD_9").ToString.Trim & ",VDT_10='" & rspaymaster.Tables(0).Rows(0).Item("VDT_10").ToString.Trim & "',VD_10=" & rspaymaster.Tables(0).Rows(0).Item("VD_10").ToString.Trim &
                                ",VIT_1='" & rspaymaster.Tables(0).Rows(0).Item("VIT_1").ToString.Trim & "',VI_1=" & rspaymaster.Tables(0).Rows(0).Item("VI_1").ToString.Trim & ",VIT_2='" & rspaymaster.Tables(0).Rows(0).Item("VIT_2").ToString.Trim & "',VI_2=" & rspaymaster.Tables(0).Rows(0).Item("VI_2").ToString.Trim &
                                ",VIT_3='" & rspaymaster.Tables(0).Rows(0).Item("VIT_3").ToString.Trim & "',VI_3=" & rspaymaster.Tables(0).Rows(0).Item("VI_3").ToString.Trim & ",VIT_4='" & rspaymaster.Tables(0).Rows(0).Item("VIT_4").ToString.Trim & "',VI_4=" & rspaymaster.Tables(0).Rows(0).Item("VI_4").ToString.Trim &
                                ",VIT_5='" & rspaymaster.Tables(0).Rows(0).Item("VIT_5").ToString.Trim & "',VI_5=" & rspaymaster.Tables(0).Rows(0).Item("VI_5").ToString.Trim & ",VIT_6='" & rspaymaster.Tables(0).Rows(0).Item("VIT_6").ToString.Trim & "',VI_6=" & rspaymaster.Tables(0).Rows(0).Item("VI_6").ToString.Trim &
                                ",VIT_7='" & rspaymaster.Tables(0).Rows(0).Item("VIT_7").ToString.Trim & "',VI_7=" & rspaymaster.Tables(0).Rows(0).Item("VI_7").ToString.Trim & ",VIT_8='" & rspaymaster.Tables(0).Rows(0).Item("VIT_8").ToString.Trim & "',VI_8=" & rspaymaster.Tables(0).Rows(0).Item("VI_8").ToString.Trim &
                                ",VIT_9='" & rspaymaster.Tables(0).Rows(0).Item("VIT_9").ToString.Trim & "',VI_9=" & rspaymaster.Tables(0).Rows(0).Item("VI_9").ToString.Trim & ",VIT_10='" & rspaymaster.Tables(0).Rows(0).Item("VIT_10").ToString.Trim & "',VI_10=" & rspaymaster.Tables(0).Rows(0).Item("VI_10").ToString.Trim &
                                ",VTDS_RATE=" & rspaymaster.Tables(0).Rows(0).Item("VTDS_RATE").ToString.Trim & ",VTDS_F='" & rspaymaster.Tables(0).Rows(0).Item("VTDS_F").ToString.Trim & "'" &
                                ",PROF_TAX_Allowed='" & rspaymaster.Tables(0).Rows(0).Item("PROF_TAX_Allowed").ToString.Trim & "'"
                            If Common.servername = "Access" Then
                                sSql = sSql + " where pay_Result.Paycode='" & Trim(sPaycode) & "' And format(Mon_Year,'MM')=" & DateEditTo.DateTime.ToString("MM") & " And FORMAT(Mon_Year,'YYYY')=" & DateEditTo.DateTime.ToString("yyyy")
                                If Common.con1.State <> ConnectionState.Open Then
                                    Common.con1.Open()
                                End If
                                cmd1 = New OleDbCommand(sSql, Common.con1)
                                cmd1.ExecuteNonQuery()
                                If Common.con1.State <> ConnectionState.Closed Then
                                    Common.con1.Close()
                                End If
                            Else
                                sSql = sSql + " where pay_Result.Paycode='" & Trim(sPaycode) & "' And DatePart(mm,Mon_Year)=" & DateEditTo.DateTime.ToString("MM") & " And DatePart(yyyy,Mon_Year)=" & DateEditTo.DateTime.ToString("yyyy")
                                If Common.con.State <> ConnectionState.Open Then
                                    Common.con.Open()
                                End If
                                cmd = New SqlCommand(sSql, Common.con)
                                cmd.ExecuteNonQuery()
                                If Common.con.State <> ConnectionState.Closed Then
                                    Common.con.Close()
                                End If
                            End If
                            'sSql = "Update Pay_Result Set VOT=" & iVOTDuration &
                            '  ",VT_EARLY=" & iVEarlyDeparture & ",VT_LATE=" & iVLateArrival &
                            '  ",VPRE=" & iPresentValue & ",VABS=" & iAbsentValue &
                            '  ",VHLD=" & iHolidayValue & ",VCL=" & iVCL & ",VSL=" & iVSL &
                            '  ",VPL_EL=" & iVPL_EL & ",VOTHER_LV=" & iVOther_LV & ",VLEAVE=" & iLeaveAmount &
                            '  ",VLATE=" & iLateDays & ",VEARLY=" & iEarlyDays & ",VWO=" & iWOValue & ",VTDAYS=" & iTotalDays

                            'VT_EARLY = " & iVTotHrsWorked 'changed in in below query for working hours
                            sSql = "Update Pay_Result Set VOT=" & iVOTDuration &
                              ",VT_EARLY=" & iVTotHrsWorked & ",VT_LATE=" & iVLateArrival &
                              ",VPRE=" & iPresentValue & ",VABS=" & iAbsentValue &
                              ",VHLD=" & iHolidayValue & ",VCL=" & iVCL & ",VSL=" & iVSL &
                              ",VPL_EL=" & iVPL_EL & ",VOTHER_LV=" & iVOther_LV & ",VLEAVE=" & iLeaveAmount &
                              ",VLATE=" & iLateDays & ",VEARLY=" & iEarlyDays & ",VWO=" & iWOValue & ",VTDAYS=" & iTotalDays
                            If Common.servername = "Access" Then
                                sSql = sSql + " where pay_Result.Paycode='" & Trim(sPaycode) & "' And FORMAT(Mon_Year,'MM')=" & DateEditTo.DateTime.ToString("MM") & " And FORMAT(Mon_Year,'YYYY')=" & DateEditTo.DateTime.ToString("yyyy")
                                If Common.con1.State <> ConnectionState.Open Then
                                    Common.con1.Open()
                                End If
                                cmd1 = New OleDbCommand(sSql, Common.con1)
                                cmd1.ExecuteNonQuery()
                                If Common.con1.State <> ConnectionState.Closed Then
                                    Common.con1.Close()
                                End If
                            Else
                                sSql = sSql + " where pay_Result.Paycode='" & Trim(sPaycode) & "' And DatePart(mm,Mon_Year)=" & DateEditTo.DateTime.ToString("MM") & " And DatePart(yyyy,Mon_Year)=" & DateEditTo.DateTime.ToString("yyyy")
                                If Common.con.State <> ConnectionState.Open Then
                                    Common.con.Open()
                                End If
                                cmd = New SqlCommand(sSql, Common.con)
                                cmd.ExecuteNonQuery()
                                If Common.con.State <> ConnectionState.Closed Then
                                    Common.con.Close()
                                End If
                            End If
                            'Cn.Execute(sSql)
                        End If
                    End If
                End If
                'rsCapture.MovePrevious()
                i = i - 1
                If rsCapture.Tables(0).Rows(i).Item("VEmployeeType").ToString.Trim = "P" Then
                    rsPiecedata = New DataSet
                    If Common.servername = "Access" Then
                        sSql = "Select sum(pamount) as amount from tblpiecedata Where PAYCODE='" & Trim(sPaycode) & "' And FORMAT(Mon_Year,'MM')=" & DateEditTo.DateTime.ToString("MM") & " And FORMAT(Mon_Year,'YYYY')=" & DateEditTo.DateTime.ToString("yyyy")
                        adapA = New OleDbDataAdapter(sSql, Common.con1)
                        adapA.Fill(rsPiecedata)
                    Else
                        sSql = "Select sum(pamount) as amount from tblpiecedata Where PAYCODE='" & Trim(sPaycode) & "' And DatePart(mm,Mon_Year)=" & DateEditTo.DateTime.ToString("MM") & " And DatePart(yyyy,Mon_Year)=" & DateEditTo.DateTime.ToString("yyyy")
                        adap = New SqlDataAdapter(sSql, Common.con)
                        adap.Fill(rsPiecedata)
                    End If
                    'rsPiecedata = Cn.Execute(sSql)
                    If rsPiecedata.Tables(0).Rows.Count = 1 Then 'RecordCount = 1 Then
                        If rsPiecedata.Tables(0).Rows(0).Item("amount").ToString.Trim <> "" Then
                            If Common.servername = "Access" Then
                                sSql = "Update Pay_Result Set VBASIC=" & rsPiecedata.Tables(0).Rows(0).Item("amount").ToString.Trim & " where pay_Result.Paycode='" & Trim(sPaycode) & "' And FORMAT(Mon_Year,'MM')=" & DateEditTo.DateTime.ToString("MM") & " And FORMAT(Mon_Year,'YYYY')=" & DateEditTo.DateTime.ToString("yyyy")
                                If Common.con1.State <> ConnectionState.Open Then
                                    Common.con1.Open()
                                End If
                                cmd1 = New OleDbCommand(sSql, Common.con1)
                                cmd1.ExecuteNonQuery()
                                If Common.con1.State <> ConnectionState.Closed Then
                                    Common.con1.Close()
                                End If
                            Else
                                sSql = "Update Pay_Result Set VBASIC=" & rsPiecedata.Tables(0).Rows(0).Item("amount").ToString.Trim & " where pay_Result.Paycode='" & Trim(sPaycode) & "' And DatePart(mm,Mon_Year)=" & DateEditTo.DateTime.ToString("MM") & " And DatePart(yyyy,Mon_Year)=" & DateEditTo.DateTime.ToString("yyyy")
                                If Common.con.State <> ConnectionState.Open Then
                                    Common.con.Open()
                                End If
                                cmd = New SqlCommand(sSql, Common.con)
                                cmd.ExecuteNonQuery()
                                If Common.con.State <> ConnectionState.Closed Then
                                    Common.con.Close()
                                End If
                            End If
                            'Cn.Execute(sSql)
                        End If
                    End If
                End If
                'rsCapture.MoveNext()
                'i = i + 1
                If i > rsCapture.Tables(0).Rows.Count - 1 Then
                    Exit For
                End If
                'If rsCapture.AbsolutePosition = rsCapture.RecordCount Then
                '    rsCapture.MoveNext()
                'End If
                rsMonthTable = Nothing
            Next ' Loop
        Else
            Me.Cursor = Cursors.Default
            'DATACAPTURE = False
            Return False
            'Screen.MousePointer = vbNormal
        End If
        If Common.servername = "Access" Then
            sSql = "delete from Pay_Result where paycode not in (select paycode from pay_master) And FORMAT(Mon_Year,'MM')=" & DateEditTo.DateTime.ToString("MM") & " And FORMAT(Mon_Year,'YYYY')=" & DateEditTo.DateTime.ToString("yyyy")
            If Common.con1.State <> ConnectionState.Open Then
                Common.con1.Open()
            End If
            cmd1 = New OleDbCommand(sSql, Common.con1)
            cmd1.ExecuteNonQuery()
            If Common.con1.State <> ConnectionState.Closed Then
                Common.con1.Close()
            End If
        Else
            sSql = "delete from Pay_Result where paycode not in (select paycode from pay_master) And DatePart(mm,Mon_Year)=" & DateEditTo.DateTime.ToString("MM") & " And DatePart(yyyy,Mon_Year)=" & DateEditTo.DateTime.ToString("yyyy")
            If Common.con.State <> ConnectionState.Open Then
                Common.con.Open()
            End If
            cmd = New SqlCommand(sSql, Common.con)
            cmd.ExecuteNonQuery()
            If Common.con.State <> ConnectionState.Closed Then
                Common.con.Close()
            End If
        End If
        'Cn.Execute(sSql)
        rsCapture = Nothing
        rsCheckTable = Nothing
        rsMonthTable = Nothing
        Return True
        'Exit Function
        'ErrHand:
        '        'Screen.MousePointer = vbNormal
        '        Me.Cursor = Cursors.Default
        '        XtraMessageBox.Show(ulf, "<size=10>" & Err.Number & " -- Function DataCapture " & Chr(10) & Err.Description & "</size>", "<size=9>Error</size>")
        '        Resume Next
        '        rsCapture = Nothing
        '        rsCheckTable = Nothing
        '        rsMonthTable = Nothing
    End Function


    ''********************************************************************
    ''    This function is use for processing purpose of selected employee
    ''    through TR------- table
    ''********************************************************************
    Public Function DoProcess(sSql As String) As Boolean
        Dim sAdvFn As String
        Dim rsProcess As DataSet = New DataSet 'ADODB.Recordset
        Dim rsAdvance As DataSet = New DataSet 'ADODB.Recordset
        Dim rsFine As DataSet = New DataSet 'ADODB.Recordset
        Dim rsProf As DataSet = New DataSet 'ADODB.Recordset
        Dim sWhichFormula As String '* 355
        Dim sPaycode As String '* 10, 
        Dim decpos As Integer
        Dim iPFAMT As Double, iFPFAMT As Double, iOtAmt As Double, iHRAAMT As Double, iESIAMT As Double
        Dim iDedAmt1 As Double, iDedAmt2 As Double, iDedAmt3 As Double, iDedAmt4 As Double, iDedAmt5 As Double, iDedAmt6 As Double, iDedAmt7 As Double, iDedAmt8 As Double, iDedAmt9 As Double, iDedAmt10 As Double
        Dim iErnAmt1 As Double, iErnAmt2 As Double, iErnAmt3 As Double, iErnAmt4 As Double, iErnAmt5 As Double, iErnAmt6 As Double, iErnAmt7 As Double, iErnAmt8 As Double, iErnAmt9 As Double, iErnAmt10 As Double
        Dim iSalary As Double, iNetSal As Double, iGrossSal As Double
        Dim BASIC As Double, DA As Double, CONV As Double, MED As Double, PF As Double, FPF As Double, OTRate As Double
        Dim HRA As Double, ESI As Double, Fine As Double, Advance As Double
        Dim PRE As Double, ABS1 As Double, HLD As Double, LATE As Double
        Dim EARLY As Double, OT As Double, CL As Double, SL As Double, PL_EL As Double
        Dim OTHER_LV As Double, LEAVE As Double, TDAYS As Double, T_LATE As Double, T_EARLY As Double
        Dim D_1TOD_10 As Double, E_1TOE_10 As Double, OT_RATE As Double, MON_DAY As Double
        Dim ided1 As Double, ided2 As Double, ided3 As Double, ided4 As Double, ided5 As Double, ided6 As Double, ided7 As Double, ided8 As Double, ided9 As Double, ided10 As Double
        Dim iern1 As Double, iern2 As Double, iern3 As Double, iern4 As Double, iern5 As Double, iern6 As Double, iern7 As Double, iern8 As Double, iern9 As Double, iern10 As Double
        Dim iRESULT As Double
        Dim iWOValue As Integer
        Dim test As Double, mArrearSalary As Double
        Dim rsarrear As DataSet = New DataSet 'ADODB.Recordset
        Dim rsarrearpayday As DataSet = New DataSet 'ADODB.Recordset
        Dim marrear As Double, marresiamt As Double, marrpfamt As Double, marrearesi As Double, marrearpf As Double, marrearfpf As Double, marrearpfonamt As Double, marrearvpf As Double
        Dim uptodate As Date, arrearpayday As Double, arrearmonday As Double
        Dim amt1_amt As Double, amt2_amt As Double, amt3_amt As Double, amt4_amt As Double, amt5_amt As Double, n As Integer
        Dim conv_amt As Double, medical_amt As Double, tds_amt As Double, prof_tax_amt As Double, amt_on_pf As Double, amt_on_esi As Double, iVPFAMT As Double, iEPFAMT As Double, TDS As Double, PROF_TAX As Double
        Dim rsPaysetup As DataSet = New DataSet 'ADODB.Recordset, 
        Dim MYEAR As Double
        Dim Reimbconv_amt As Double, Reimmedical_amt As Double, ReimErnAmt1 As Double, ReimErnAmt2 As Double, ReimErnAmt3 As Double, ReimErnAmt4 As Double, ReimErnAmt5 As Double, ReimErnAmt6 As Double, ReimErnAmt7 As Double, ReimErnAmt8 As Double, ReimErnAmt9 As Double, ReimErnAmt10 As Double
        Dim TotalHours As Double
        Dim ShiftDur As Double
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter

        'Try
        'rsProcess = Cn.Execute(sSql)
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(rsProcess)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(rsProcess)
        End If
        sSql = "select * from pay_setup"
        rsPaysetup = New DataSet
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(rsPaysetup)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(rsPaysetup)
        End If
        g_pfmaxlimit = rsPaysetup.Tables(0).Rows(0).Item("PFLIMIT")
        'rsPaysetup = Cn.Execute(sSql)
        For i As Integer = 0 To rsProcess.Tables(0).Rows.Count - 1 '  Do While Not rsProcess.EOF
            'lblDisplay.Refresh()
            'lblDisplay.Caption = "Processing of " & rsProcess("PAYCODE") & " number "
            XtraMasterTest.LabelControlStatus.Text = "Processing of " & rsProcess.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim & " number "
            Application.DoEvents()
            'Progress.Visible = True
            'Progress.Value = rsProcess.AbsolutePosition / rsProcess.RecordCount * 100
            Reimbconv_amt = 0 : Reimmedical_amt = 0 : ReimErnAmt1 = 0 : ReimErnAmt2 = 0 : ReimErnAmt3 = 0 : ReimErnAmt4 = 0 : ReimErnAmt5 = 0 : ReimErnAmt6 = 0 : ReimErnAmt7 = 0 : ReimErnAmt8 = 0 : ReimErnAmt9 = 0 : ReimErnAmt10 = 0
            iPFAMT = 0 : iFPFAMT = 0 : iOtAmt = 0 : iHRAAMT = 0 : Advance = 0 : Fine = 0 : iDedAmt1 = 0 : iDedAmt2 = 0 : iDedAmt3 = 0 : iDedAmt4 = 0 : iDedAmt5 = 0 : iDedAmt6 = 0 : iDedAmt7 = 0 : iDedAmt8 = 0 : iDedAmt9 = 0 : iDedAmt10 = 0
            iErnAmt1 = 0 : iErnAmt2 = 0 : iErnAmt3 = 0 : iErnAmt4 = 0 : iErnAmt5 = 0 : iErnAmt6 = 0 : iErnAmt7 = 0 : iErnAmt8 = 0 : iErnAmt9 = 0 : iErnAmt10 = 0 : iESIAMT = 0
            conv_amt = 0 : medical_amt = 0 : tds_amt = 0 : prof_tax_amt = 0 : amt_on_pf = 0 : amt_on_esi = 0 : iVPFAMT = 0 : iEPFAMT = 0
            TotalHours = 0
            ShiftDur = 0
            sPaycode = rsProcess.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim : BASIC = rsProcess.Tables(0).Rows(i).Item("VBASIC").ToString.Trim
            If rsProcess.Tables(0).Rows(i).Item("VDA_RATE").ToString.Trim = "" Then : DA = 0 : Else : DA = rsProcess.Tables(0).Rows(i).Item("VDA_RATE").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VHRA_RATE").ToString.Trim = "" Then : HRA = 0 : Else : HRA = rsProcess.Tables(0).Rows(i).Item("VHRA_RATE").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("Vmed_rate").ToString.Trim = "" Then : MED = 0 : Else : MED = rsProcess.Tables(0).Rows(i).Item("Vmed_rate").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("Vconv_rate").ToString.Trim = "" Then : CONV = 0 : Else : CONV = rsProcess.Tables(0).Rows(i).Item("Vconv_rate").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VPRE").ToString.Trim = "" Then : PRE = 0 : Else : PRE = rsProcess.Tables(0).Rows(i).Item("VPRE").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VABS").ToString.Trim = "" Then : ABS1 = 0 : Else : ABS1 = rsProcess.Tables(0).Rows(i).Item("VABS").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VHLD").ToString.Trim = "" Then : HLD = 0 : Else : HLD = rsProcess.Tables(0).Rows(i).Item("VHLD") : End If
            If rsProcess.Tables(0).Rows(i).Item("VLATE").ToString.Trim = "" Then : LATE = 0 : Else : LATE = rsProcess.Tables(0).Rows(i).Item("VLATE").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VEARLY").ToString.Trim = "" Then : EARLY = 0 : Else : EARLY = rsProcess.Tables(0).Rows(i).Item("VEARLY").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VOT").ToString.Trim = "" Then : OT = 0 : Else : OT = rsProcess.Tables(0).Rows(i).Item("VOT").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VT_EARLY").ToString.Trim = "" Then : TotalHours = 0 : Else : TotalHours = rsProcess.Tables(0).Rows(i).Item("VT_EARLY").ToString.Trim : End If
            ' OT = HrMin.Hr2Min(OT) 'OT * 60 ' Hr2Min(OT)

            Dim values As String() = OT.ToString.Split(".")
            Dim h As Integer = Convert.ToInt32(values(0))
            Dim m As Integer
            Try
                m = Convert.ToInt32(values(1))
            Catch ex As Exception
                m = 0
            End Try
            OT = h * 60 + m

            If rsProcess.Tables(0).Rows(i).Item("VCL").ToString.Trim = "" Then : CL = 0 : Else : CL = rsProcess.Tables(0).Rows(i).Item("VCL").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VSL").ToString.Trim = "" Then : SL = 0 : Else : SL = rsProcess.Tables(0).Rows(i).Item("VSL").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VPL_EL").ToString.Trim = "" Then : PL_EL = 0 : Else : PL_EL = rsProcess.Tables(0).Rows(i).Item("VPL_EL").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VOTHER_LV").ToString.Trim = "" Then : OTHER_LV = 0 : Else : OTHER_LV = rsProcess.Tables(0).Rows(i).Item("VOTHER_LV").ToString.Trim : End If
            LEAVE = CL + SL + PL_EL + OTHER_LV
            iWOValue = IIf(rsProcess.Tables(0).Rows(i).Item("VWO").ToString.Trim = "", 0, rsProcess.Tables(0).Rows(i).Item("VWO").ToString.Trim)
            TDAYS = PRE + HLD + LEAVE + iWOValue
            If rsProcess.Tables(0).Rows(i).Item("VLATE").ToString.Trim = "" Then : T_LATE = 0 : Else : T_LATE = rsProcess.Tables(0).Rows(i).Item("VLATE").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VEARLY").ToString.Trim = "" Then : T_EARLY = 0 : Else : T_EARLY = rsProcess.Tables(0).Rows(i).Item("VEARLY").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("Vtds_rate").ToString.Trim = "" Then : TDS = 0 : Else : TDS = rsProcess.Tables(0).Rows(i).Item("Vtds_rate").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VD_1").ToString.Trim = "" Then : ided1 = 0 : Else : ided1 = rsProcess.Tables(0).Rows(i).Item("VD_1").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VD_2").ToString.Trim = "" Then : ided2 = 0 : Else : ided2 = rsProcess.Tables(0).Rows(i).Item("VD_2").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VD_3").ToString.Trim = "" Then : ided3 = 0 : Else : ided3 = rsProcess.Tables(0).Rows(i).Item("VD_3").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VD_4").ToString.Trim = "" Then : ided4 = 0 : Else : ided4 = rsProcess.Tables(0).Rows(i).Item("VD_4").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VD_5").ToString.Trim = "" Then : ided5 = 0 : Else : ided5 = rsProcess.Tables(0).Rows(i).Item("VD_5").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VD_6").ToString.Trim = "" Then : ided6 = 0 : Else : ided6 = rsProcess.Tables(0).Rows(i).Item("VD_6").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VD_7").ToString.Trim = "" Then : ided7 = 0 : Else : ided7 = rsProcess.Tables(0).Rows(i).Item("VD_7").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VD_8").ToString.Trim = "" Then : ided8 = 0 : Else : ided8 = rsProcess.Tables(0).Rows(i).Item("VD_8").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VD_9").ToString.Trim = "" Then : ided9 = 0 : Else : ided9 = rsProcess.Tables(0).Rows(i).Item("VD_9").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VD_10").ToString.Trim = "" Then : ided10 = 0 : Else : ided10 = rsProcess.Tables(0).Rows(i).Item("VD_10").ToString.Trim : End If

            If rsProcess.Tables(0).Rows(i).Item("VI_1").ToString.Trim = "" Then : iern1 = 0 : Else : iern1 = rsProcess.Tables(0).Rows(i).Item("VI_1").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VI_2").ToString.Trim = "" Then : iern2 = 0 : Else : iern2 = rsProcess.Tables(0).Rows(i).Item("VI_2").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VI_3").ToString.Trim = "" Then : iern3 = 0 : Else : iern3 = rsProcess.Tables(0).Rows(i).Item("VI_3").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VI_4").ToString.Trim = "" Then : iern4 = 0 : Else : iern4 = rsProcess.Tables(0).Rows(i).Item("VI_4").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VI_5").ToString.Trim = "" Then : iern5 = 0 : Else : iern5 = rsProcess.Tables(0).Rows(i).Item("VI_5").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VI_6").ToString.Trim = "" Then : iern6 = 0 : Else : iern6 = rsProcess.Tables(0).Rows(i).Item("VI_6").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VI_7").ToString.Trim = "" Then : iern7 = 0 : Else : iern7 = rsProcess.Tables(0).Rows(i).Item("VI_7").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VI_8").ToString.Trim = "" Then : iern8 = 0 : Else : iern8 = rsProcess.Tables(0).Rows(i).Item("VI_8").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VI_9").ToString.Trim = "" Then : iern9 = 0 : Else : iern9 = rsProcess.Tables(0).Rows(i).Item("VI_9").ToString.Trim : End If
            If rsProcess.Tables(0).Rows(i).Item("VI_10").ToString.Trim = "" Then : iern10 = 0 : Else : iern10 = rsProcess.Tables(0).Rows(i).Item("VI_10").ToString.Trim : End If

            OT_RATE = rsProcess.Tables(0).Rows(i).Item("VOT_RATE").ToString.Trim
            MON_DAY = DateEditTo.DateTime.DaysInMonth(DateEditTo.DateTime.Year, DateEditTo.DateTime.Month) 'NoOfDay(txtDateTo.Value)

            ' For Shift Hours 
            Dim TmpQ As String
            Dim RsShift As DataSet = New DataSet

            Try
                TmpQ = " select SHIFTDURATION from tblShiftMaster  where SHIFT=(select SHIFT from tblEmployeeShiftMaster where PAYCODE='" & rsProcess.Tables(0).Rows(0)("PayCode") & "')"
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(TmpQ, Common.con1)
                    adapA.Fill(RsShift)
                Else
                    adap = New SqlDataAdapter(TmpQ, Common.con)
                    adap.Fill(RsShift)
                End If
                If RsShift.Tables(0).Rows.Count > 0 Then
                    ShiftDur = Convert.ToDouble(RsShift.Tables(0).Rows(0)("SHIFTDURATION").ToString.Trim)
                    ShiftDur = ShiftDur / 60
                Else
                    ShiftDur = 9
                End If

            Catch ex As Exception
                ShiftDur = 9
            End Try
            ' Shift Hours End 

            '******-FOR BASIC SALARY-*************
            If rsProcess.Tables(0).Rows(i).Item("VEmployeeType").ToString.Trim = "D" Then
                iSalary = (BASIC * PRE)
            ElseIf rsProcess.Tables(0).Rows(i).Item("VEmployeeType").ToString.Trim = "P" Then
                iSalary = BASIC
            ElseIf rsProcess.Tables(0).Rows(i).Item("VEmployeeType").ToString.Trim = "H" Then
                iSalary = ((BASIC / MON_DAY) / ShiftDur) / 60 * TotalHours
            Else
                iSalary = (BASIC / MON_DAY) * TDAYS
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("BASIC_RND").ToString.Trim = "Y" Then
                iSalary = Math.Round(iSalary, 0)
            Else
                iSalary = Math.Round(iSalary, 2)
            End If
            '******-FOR DA*************
            If rsProcess.Tables(0).Rows(i).Item("Vda_f").ToString.Trim = "F" Then
                DA = rsProcess.Tables(0).Rows(i).Item("vda_RATE")
            Else
                DA = (rsProcess.Tables(0).Rows(i).Item("vda_RATE") / MON_DAY) * TDAYS
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("DA_RND") = "Y" Then
                DA = Math.Round(DA, 0)
            Else
                DA = Math.Round(DA, 2)
            End If
            '******-FOR HRA -*************
            If rsProcess.Tables(0).Rows(i).Item("VHRA_F").ToString.Trim = "F" Then
                iHRAAMT = rsProcess.Tables(0).Rows(i).Item("VHRA_RATE")
            Else
                iHRAAMT = (rsProcess.Tables(0).Rows(i).Item("vHRA_RATE") / MON_DAY) * TDAYS
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("HRA_RND").ToString.Trim = "Y" Then
                iHRAAMT = Math.Round(iHRAAMT, 0)
            Else
                iHRAAMT = Math.Round(iHRAAMT, 2)
            End If
            '******-FOR CONVEYANCE*************
            If Mid(rsPaysetup.Tables(0).Rows(0).Item("REIMBREL").ToString.Trim, 1, 1).Trim.Trim = "0" Then
                If rsProcess.Tables(0).Rows(i).Item("Vconv_f").ToString.Trim = "F" Then
                    conv_amt = rsProcess.Tables(0).Rows(i).Item("Vconv_rate")
                Else
                    conv_amt = (rsProcess.Tables(0).Rows(i).Item("Vconv_rate") / MON_DAY) * TDAYS
                End If
            Else
                conv_amt = 0
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("CONV_RND").ToString.Trim = "Y" Then
                conv_amt = Math.Round(conv_amt, 0)
            Else
                conv_amt = Math.Round(conv_amt, 2)
            End If
            '******-FOR MEDICAL*************
            If Mid(rsPaysetup.Tables(0).Rows(0).Item("REIMBREL").ToString.Trim, 2, 1) = "0" Then
                If rsProcess.Tables(0).Rows(i).Item("Vmed_f") = "F" Then
                    medical_amt = rsProcess.Tables(0).Rows(i).Item("Vmed_rate")
                Else
                    medical_amt = (rsProcess.Tables(0).Rows(i).Item("Vmed_rate") / MON_DAY) * TDAYS
                End If
            Else
                medical_amt = 0
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("MEDICAL_RND").ToString.Trim = "Y" Then
                medical_amt = Math.Round(medical_amt, 0)
            Else
                medical_amt = Math.Round(medical_amt, 2)
            End If
            '******-FOR Earning   Formula -**********
            If Mid(rsPaysetup.Tables(0).Rows(0).Item("REIMBREL").ToString.Trim, 3, 1) = "0" Then
                If rsProcess.Tables(0).Rows(i).Item("VIT_1").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i).Item("VIT_1").ToString.Trim <> "0" Then
                    sWhichFormula = ProcFormula(rsProcess.Tables(0).Rows(i).Item("VIT_1").ToString)
                    If sWhichFormula <> "" And sWhichFormula <> " " Then
                        iErnAmt1 = FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                    End If
                Else
                    iErnAmt1 = iern1
                End If
            Else
                iErnAmt1 = 0
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("EARN1_RND").ToString.Trim = "Y" Then
                iErnAmt1 = Math.Round(iErnAmt1, 0)
            Else
                iErnAmt1 = Math.Round(iErnAmt1, 2)
            End If
            If Mid(rsPaysetup.Tables(0).Rows(0).Item("REIMBREL").ToString.Trim, 4, 1) = "0" Then
                If (rsProcess.Tables(0).Rows(i).Item("VIT_2").ToString.Trim <> "") And rsProcess.Tables(0).Rows(i).Item("VIT_2").ToString.Trim <> "0" Then
                    sWhichFormula = ProcFormula(rsProcess.Tables(0).Rows(i).Item("VIT_2").ToString)
                    If sWhichFormula <> "" And sWhichFormula <> " " Then
                        iErnAmt2 = FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                    End If
                Else
                    iErnAmt2 = iern2
                End If
            Else
                iErnAmt2 = 0
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("EARN2_RND").ToString.Trim = "Y" Then
                iErnAmt2 = Math.Round(iErnAmt2, 0)
            Else
                iErnAmt2 = Math.Round(iErnAmt2, 2)
            End If
            If Mid(rsPaysetup.Tables(0).Rows(0).Item("REIMBREL").ToString.Trim, 5, 1) = "0" Then
                If Trim(rsProcess.Tables(0).Rows(i).Item("VIT_3").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i).Item("VIT_3").ToString.Trim <> "0") Then
                    sWhichFormula = ProcFormula(rsProcess.Tables(0).Rows(i).Item("VIT_3").ToString)
                    If sWhichFormula <> "" And sWhichFormula <> " " Then
                        iErnAmt3 = FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                    End If
                Else
                    iErnAmt3 = iern3
                End If
            Else
                iErnAmt3 = 0
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("EARN3_RND").ToString.Trim = "Y" Then
                iErnAmt3 = Math.Round(iErnAmt3, 0)
            Else
                iErnAmt3 = Math.Round(iErnAmt3, 2)
            End If
            If Mid(rsPaysetup.Tables(0).Rows(0).Item("REIMBREL").ToString.Trim, 6, 1) = "0" Then
                If rsProcess.Tables(0).Rows(i).Item("VIT_4") <> "" And rsProcess.Tables(0).Rows(i).Item("VIT_4").ToString.Trim <> "0" Then
                    sWhichFormula = ProcFormula(rsProcess.Tables(0).Rows(i).Item("VIT_4").ToString)
                    If sWhichFormula <> "" And sWhichFormula <> " " Then
                        iErnAmt4 = FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                    End If
                Else
                    iErnAmt4 = iern4
                End If
            Else
                iErnAmt4 = 0
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("EARN4_RND").ToString.Trim = "Y" Then
                iErnAmt4 = Math.Round(iErnAmt4, 0)
            Else
                iErnAmt4 = Math.Round(iErnAmt4, 2)
            End If
            If Mid(rsPaysetup.Tables(0).Rows(0).Item("REIMBREL").ToString.Trim, 7, 1) = "0" Then
                If rsProcess.Tables(0).Rows(i).Item("VIT_5").ToString.Trim = "" And rsProcess.Tables(0).Rows(i).Item("VIT_5").ToString.Trim <> "0" Then
                    sWhichFormula = ProcFormula(rsProcess.Tables(0).Rows(i).Item("VIT_5").ToString)
                    If sWhichFormula <> "" And sWhichFormula <> " " Then
                        iErnAmt5 = FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                    End If
                Else
                    iErnAmt5 = iern5
                End If
            Else
                iErnAmt5 = 0
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("EARN5_RND").ToString.Trim = "Y" Then
                iErnAmt5 = Math.Round(iErnAmt5, 0)
            Else
                iErnAmt5 = Math.Round(iErnAmt5, 2)
            End If
            If Mid(rsPaysetup.Tables(0).Rows(0).Item("REIMBREL").ToString.Trim, 8, 1) = "0" Then
                If rsProcess.Tables(0).Rows(i).Item("VIT_6").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i).Item("VIT_6").ToString.Trim <> "0" Then
                    sWhichFormula = ProcFormula(rsProcess.Tables(0).Rows(i).Item("VIT_6").ToString)
                    If sWhichFormula <> "" And sWhichFormula <> " " Then
                        iErnAmt6 = FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                    End If
                Else
                    iErnAmt6 = iern6
                End If
            Else
                iErnAmt6 = 0
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("EARN6_RND").ToString.Trim = "Y" Then
                iErnAmt6 = Math.Round(iErnAmt6, 0)
            Else
                iErnAmt6 = Math.Round(iErnAmt6, 2)
            End If
            If Mid(rsPaysetup.Tables(0).Rows(0).Item("REIMBREL"), 9, 1).ToString.Trim = "0" Then
                If rsProcess.Tables(0).Rows(i).Item("VIT_7").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i).Item("VIT_7").ToString.Trim <> "0" Then
                    sWhichFormula = ProcFormula(rsProcess.Tables(0).Rows(i).Item("VIT_7").ToString)
                    If sWhichFormula <> "" And sWhichFormula <> " " Then
                        iErnAmt7 = FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                    End If
                Else
                    iErnAmt7 = iern7
                End If
            Else
                iErnAmt7 = 0
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("EARN7_RND").ToString.Trim = "Y" Then
                iErnAmt7 = Math.Round(iErnAmt7, 0)
            Else
                iErnAmt7 = Math.Round(iErnAmt7, 2)
            End If
            If Mid(rsPaysetup.Tables(0).Rows(0).Item("REIMBREL"), 10, 1).ToString.Trim = "0" Then
                If rsProcess.Tables(0).Rows(i).Item("VIT_8").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i).Item("VIT_8").ToString.Trim <> "0" Then
                    sWhichFormula = ProcFormula(rsProcess.Tables(0).Rows(i).Item("VIT_8").ToString)
                    If sWhichFormula <> "" And sWhichFormula <> " " Then
                        iErnAmt8 = FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                    End If
                Else
                    iErnAmt8 = iern8
                End If
            Else
                iErnAmt8 = 0
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("EARN8_RND").ToString.Trim = "Y" Then
                iErnAmt8 = Math.Round(iErnAmt8, 0)
            Else
                iErnAmt8 = Math.Round(iErnAmt8, 2)
            End If
            If Mid(rsPaysetup.Tables(0).Rows(0).Item("REIMBREL").ToString.Trim, 11, 1) = "0" Then
                If rsProcess.Tables(0).Rows(i).Item("VIT_9").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i).Item("VIT_9").ToString.Trim <> "0" Then
                    sWhichFormula = ProcFormula(rsProcess.Tables(0).Rows(i).Item("VIT_9").ToString)
                    If sWhichFormula <> "" And sWhichFormula <> " " Then
                        iErnAmt9 = FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                    End If
                Else
                    iErnAmt9 = iern9
                End If
            Else
                iErnAmt9 = 0
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("EARN9_RND").ToString.Trim = "Y" Then
                iErnAmt9 = Math.Round(iErnAmt9, 0)
            Else
                iErnAmt9 = Math.Round(iErnAmt9, 2)
            End If
            If Mid(rsPaysetup.Tables(0).Rows(0).Item("REIMBREL").ToString.Trim, 12, 1) = "0" Then
                If rsProcess.Tables(0).Rows(i).Item("VIT_10").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i).Item("VIT_10").ToString.Trim <> "0" Then
                    sWhichFormula = ProcFormula(rsProcess.Tables(0).Rows(i).Item("VIT_10").ToString)
                    If sWhichFormula <> "" And sWhichFormula <> " " Then
                        iErnAmt10 = FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                    End If
                Else
                    iErnAmt10 = iern10
                End If
            Else
                iErnAmt10 = 0
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("EARN10_RND").ToString.Trim = "Y" Then
                iErnAmt10 = Math.Round(iErnAmt10, 0)
            Else
                iErnAmt10 = Math.Round(iErnAmt10, 2)
            End If
            '******-FOR OT  AMT FORMULA-*************
            iOtAmt = 0
            If rsProcess.Tables(0).Rows(i).Item("VOT_FLAG").ToString.Trim <> "" Then
                OT = OT
                sWhichFormula = ProcFormula(rsProcess.Tables(0).Rows(i).Item("VOT_FLAG").ToString)
                If sWhichFormula <> "" And sWhichFormula <> " " Then
                    iOtAmt = FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                End If
            Else
                iOtAmt = OT_RATE
            End If
            iOtAmt = (iOtAmt / 60) * OT
            If rsPaysetup.Tables(0).Rows(0).Item("OTAMT_RND").ToString.Trim = "Y" Then
                iOtAmt = Math.Round(iOtAmt, 0)
            Else
                iOtAmt = Math.Round(iOtAmt, 2)
            End If
            '******-FOR TDS*************
            If rsProcess.Tables(0).Rows(i).Item("VTDS_F").ToString.Trim <> "" Then
                sWhichFormula = ProcFormula(rsProcess.Tables(0).Rows(i).Item("VTDS_F").ToString)
                If sWhichFormula <> "" And sWhichFormula <> " " Then
                    tds_amt = FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                End If
            Else
                tds_amt = rsProcess.Tables(0).Rows(i).Item("VTDS_RATE")
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("TDS_RND").ToString.Trim = "Y" Then
                tds_amt = Math.Round(tds_amt, 0)
            Else
                tds_amt = Math.Round(tds_amt, 2)
            End If
            '******-FOR Deduction Formula -**********
            D_1TOD_10 = 0
            If rsProcess.Tables(0).Rows(i).Item("VDT_1").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i).Item("VDT_1").ToString.Trim <> "0" Then
                sWhichFormula = ProcFormula(rsProcess.Tables(0).Rows(i).Item("VDT_1").ToString)
                If sWhichFormula <> "" And sWhichFormula <> " " Then
                    iDedAmt1 = FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                End If
            Else
                iDedAmt1 = ided1
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("DED1_RND").ToString.Trim = "Y" Then
                iDedAmt1 = Math.Round(iDedAmt1, 0)
            Else
                iDedAmt1 = Math.Round(iDedAmt1, 2)
            End If
            If rsProcess.Tables(0).Rows(i).Item("VDT_2").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i).Item("VDT_2").ToString.Trim <> "0" Then
                sWhichFormula = ProcFormula(rsProcess.Tables(0).Rows(i).Item("VDT_2").ToString)
                If sWhichFormula <> "" And sWhichFormula <> " " Then
                    iDedAmt2 = FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                End If
            Else
                iDedAmt2 = ided2
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("DED2_RND").ToString.Trim = "Y" Then
                iDedAmt2 = Math.Round(iDedAmt2, 0)
            Else
                iDedAmt2 = Math.Round(iDedAmt2, 2)
            End If
            If rsProcess.Tables(0).Rows(i).Item("VDT_3").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i).Item("VDT_3").ToString.Trim <> "0" Then
                sWhichFormula = ProcFormula(rsProcess.Tables(0).Rows(i).Item("VDT_3").ToString)
                If sWhichFormula <> "" And sWhichFormula <> " " Then
                    iDedAmt3 = FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                End If
            Else
                iDedAmt3 = ided3
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("DED3_RND").ToString.Trim = "Y" Then
                iDedAmt3 = Math.Round(iDedAmt3, 0)
            Else
                iDedAmt3 = Math.Round(iDedAmt3, 2)
            End If
            If rsProcess.Tables(0).Rows(i).Item("VDT_4").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i).Item("VDT_4").ToString.Trim <> "0" Then
                sWhichFormula = ProcFormula(rsProcess.Tables(0).Rows(i).Item("VDT_4").ToString)
                If sWhichFormula <> "" And sWhichFormula <> " " Then
                    iDedAmt4 = FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                End If
            Else
                iDedAmt4 = ided4
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("DED4_RND").ToString.Trim = "Y" Then
                iDedAmt4 = Math.Round(iDedAmt4, 0)
            Else
                iDedAmt4 = Math.Round(iDedAmt4, 2)
            End If
            If rsProcess.Tables(0).Rows(i).Item("VDT_5").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i).Item("VDT_5").ToString.Trim <> "0" Then
                sWhichFormula = ProcFormula(rsProcess.Tables(0).Rows(i).Item("VDT_5").ToString)
                If sWhichFormula <> "" And sWhichFormula <> " " Then
                    iDedAmt5 = FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                End If
            Else
                iDedAmt5 = ided5
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("DED5_RND").ToString.Trim = "Y" Then
                iDedAmt5 = Math.Round(iDedAmt5, 0)
            Else
                iDedAmt5 = Math.Round(iDedAmt5, 2)
            End If
            If rsProcess.Tables(0).Rows(i).Item("VDT_6").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i).Item("VDT_6").ToString.Trim <> "0" Then
                sWhichFormula = ProcFormula(rsProcess.Tables(0).Rows(i).Item("VDT_6").ToString)
                If sWhichFormula <> "" And sWhichFormula <> " " Then
                    iDedAmt6 = FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                End If
            Else
                iDedAmt6 = ided6
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("DED6_RND").ToString.Trim = "Y" Then
                iDedAmt6 = Math.Round(iDedAmt6, 0)
            Else
                iDedAmt6 = Math.Round(iDedAmt6, 2)
            End If
            If rsProcess.Tables(0).Rows(i).Item("VDT_7").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i).Item("VDT_7").ToString.Trim <> "0" Then
                sWhichFormula = ProcFormula(rsProcess.Tables(0).Rows(i).Item("VDT_7").ToString)
                If sWhichFormula <> "" And sWhichFormula <> " " Then
                    iDedAmt7 = FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                End If
            Else
                iDedAmt7 = ided7
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("DED7_RND").ToString.Trim = "Y" Then
                iDedAmt7 = Math.Round(iDedAmt7, 0)
            Else
                iDedAmt7 = Math.Round(iDedAmt7, 2)
            End If
            If rsProcess.Tables(0).Rows(i).Item("VDT_8").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i).Item("VDT_8").ToString.Trim.ToString.Trim <> "0" Then
                sWhichFormula = ProcFormula(rsProcess.Tables(0).Rows(i).Item("VDT_8").ToString)
                If sWhichFormula <> "" And sWhichFormula <> " " Then
                    iDedAmt8 = FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                End If
            Else
                iDedAmt8 = ided8
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("DED8_RND").ToString.Trim = "Y" Then
                iDedAmt8 = Math.Round(iDedAmt8, 0)
            Else
                iDedAmt8 = Math.Round(iDedAmt8, 2)
            End If
            If rsProcess.Tables(0).Rows(i).Item("VDT_9").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i).Item("VDT_9").ToString.Trim <> "0" Then
                sWhichFormula = ProcFormula(rsProcess.Tables(0).Rows(i).Item("VDT_9").ToString)
                If sWhichFormula <> "" And sWhichFormula <> " " Then
                    iDedAmt9 = FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                End If
            Else
                iDedAmt9 = ided9
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("DED9_RND").ToString.Trim = "Y" Then
                iDedAmt9 = Math.Round(iDedAmt9, 0)
            Else
                iDedAmt9 = Math.Round(iDedAmt9, 2)
            End If
            If rsProcess.Tables(0).Rows(i).Item("VDT_10").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i).Item("VDT_10").ToString.Trim <> "0" Then
                sWhichFormula = ProcFormula(rsProcess.Tables(0).Rows(i).Item("VDT_10").ToString)
                If sWhichFormula <> "" And sWhichFormula <> " " Then
                    iDedAmt10 = FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                End If
            Else
                iDedAmt10 = ided10
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("DED10_RND").ToString.Trim = "Y" Then
                iDedAmt10 = Math.Round(iDedAmt10, 0)
            Else
                iDedAmt10 = Math.Round(iDedAmt10, 2)
            End If
            '******-FOR PF AMT FORMULA-**************
            If rsProcess.Tables(0).Rows(i).Item("PF_ALLOWED").ToString.Trim = "Y" Then
                For n = 1 To Len(Trim(rsPaysetup.Tables(0).Rows(0).Item("PFREL").ToString.Trim))
                    If n = 1 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("PFREL").ToString.Trim), n, 1) = "1" Then
                        amt_on_pf = amt_on_pf + iSalary
                    End If
                    If n = 2 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("PFREL").ToString.Trim), n, 1) = "1" Then
                        amt_on_pf = amt_on_pf + iHRAAMT
                    End If
                    If n = 3 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("PFREL").ToString.Trim), n, 1) = "1" Then
                        amt_on_pf = amt_on_pf + DA
                    End If
                    If n = 4 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("PFREL").ToString.Trim), n, 1) = "1" Then
                        amt_on_pf = amt_on_pf + conv_amt
                    End If
                    If n = 5 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("PFREL").ToString.Trim), n, 1) = "1" Then
                        amt_on_pf = amt_on_pf + medical_amt
                    End If
                    If n = 6 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("PFREL").ToString.Trim), n, 1) = "1" Then
                        amt_on_pf = amt_on_pf + iErnAmt1
                    End If
                    If n = 7 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("PFREL").ToString.Trim), n, 1) = "1" Then
                        amt_on_pf = amt_on_pf + iErnAmt2
                    End If
                    If n = 8 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("PFREL").ToString.Trim), n, 1) = "1" Then
                        amt_on_pf = amt_on_pf + iErnAmt3
                    End If
                    If n = 9 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("PFREL").ToString.Trim), n, 1) = "1" Then
                        amt_on_pf = amt_on_pf + iErnAmt4
                    End If
                    If n = 10 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("PFREL").ToString.Trim), n, 1) = "1" Then
                        amt_on_pf = amt_on_pf + iErnAmt5
                    End If
                    If n = 11 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("PFREL").ToString.Trim), n, 1) = "1" Then
                        amt_on_pf = amt_on_pf + iErnAmt6
                    End If
                    If n = 12 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("PFREL").ToString.Trim), n, 1) = "1" Then
                        amt_on_pf = amt_on_pf + iErnAmt7
                    End If
                    If n = 13 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("PFREL").ToString.Trim), n, 1) = "1" Then
                        amt_on_pf = amt_on_pf + iErnAmt8
                    End If
                    If n = 14 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("PFREL").ToString.Trim), n, 1) = "1" Then
                        amt_on_pf = amt_on_pf + iErnAmt9
                    End If
                    If n = 15 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("PFREL")), n, 1) = "1" Then
                        amt_on_pf = amt_on_pf + iErnAmt10
                    End If
                Next n
                If rsProcess.Tables(0).Rows(i).Item("pfulimit").ToString.Trim = "Y" Then
                    If amt_on_pf >= rsPaysetup.Tables(0).Rows(0).Item("PFLIMIT") Then
                        If ABS1 > 0 Then
                            amt_on_pf = Math.Round((rsPaysetup.Tables(0).Rows(0).Item("PFLIMIT") / MON_DAY) * TDAYS, 0)
                        Else
                            amt_on_pf = rsPaysetup.Tables(0).Rows(0).Item("PFLIMIT")
                        End If

                    Else
                        If ABS1 > 0 Then
                            amt_on_pf = Math.Round((rsPaysetup.Tables(0).Rows(0).Item("PFLIMIT") / MON_DAY) * TDAYS, 0)
                        Else
                            amt_on_pf = rsPaysetup.Tables(0).Rows(0).Item("PFLIMIT")
                        End If
                    End If
                End If
                iPFAMT = Math.Round((amt_on_pf * rsPaysetup.Tables(0).Rows(0).Item("PF")) / 100, Convert.ToInt16(rsPaysetup.Tables(0).Rows(0).Item("PFRND").ToString.Trim))
                iEPFAMT = Math.Round((IIf(amt_on_pf > rsPaysetup.Tables(0).Rows(0).Item("PFLIMIT"), rsPaysetup.Tables(0).Rows(0).Item("PFLIMIT"), amt_on_pf) * rsPaysetup.Tables(0).Rows(0).Item("EPF")) / 100, Convert.ToInt16(rsPaysetup.Tables(0).Rows(0).Item("PFRND").ToString.Trim))
                iFPFAMT = iPFAMT - iEPFAMT
                If rsProcess.Tables(0).Rows(i).Item("VPF_ALLOWED") = "Y" Then
                    iVPFAMT = Math.Round((amt_on_pf * rsPaysetup.Tables(0).Rows(0).Item("VPF")) / 100, Convert.ToInt16(rsPaysetup.Tables(0).Rows(0).Item("PFRND").ToString.Trim))
                    iFPFAMT = iFPFAMT + iVPFAMT
                End If
                If rsProcess.Tables(0).Rows(i).Item("DATEOFBIRTH").ToString.Trim <> "" Then
                    'If DateDiff("YYYY", rsProcess.Tables(0).Rows(i).Item("DATEOFBIRTH"), rsProcess.Tables(0).Rows(i).Item("MON_YEAR")) >= 58 Then
                    If Convert.ToDateTime(rsProcess.Tables(0).Rows(i).Item("DATEOFBIRTH").ToString.Trim).Subtract(Convert.ToDateTime(rsProcess.Tables(0).Rows(i).Item("MON_YEAR"))).TotalHours >= 508080 Then ' for 58 years
                        iFPFAMT = iFPFAMT + iEPFAMT
                        iEPFAMT = 0
                    End If
                End If
                'amt_on_pf = IIf(rsPaysetup.Tables(0).Rows(0).Item("PFFIXED") = "Y", IIf(amt_on_pf > rsPaysetup.Tables(0).Rows(0).Item("PFLIMIT"), rsPaysetup.Tables(0).Rows(0).Item("PFLIMIT"), amt_on_pf), amt_on_pf)
            End If
            '******-FOR ESI AMT FORMULA-*************
            If rsProcess.Tables(0).Rows(i).Item("ESI_ALLOWED").ToString.Trim = "Y" Then
                For n = 1 To Len(Trim(rsPaysetup.Tables(0).Rows(0).Item("ESIREL").ToString.Trim))
                    If n = 1 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("ESIREL").ToString.Trim), n, 1) = "1" Then
                        amt_on_esi = amt_on_esi + iSalary
                    End If
                    If n = 2 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("ESIREL").ToString.Trim), n, 1) = "1" Then
                        amt_on_esi = amt_on_esi + iHRAAMT
                    End If
                    If n = 3 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("ESIREL").ToString.Trim), n, 1) = "1" Then
                        amt_on_esi = amt_on_esi + DA
                    End If
                    If n = 4 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("ESIREL").ToString.Trim), n, 1) = "1" Then
                        amt_on_esi = amt_on_esi + conv_amt
                    End If
                    If n = 5 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("ESIREL").ToString.Trim), n, 1) = "1" Then
                        amt_on_esi = amt_on_esi + medical_amt
                    End If
                    If n = 6 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("ESIREL").ToString.Trim), n, 1) = "1" Then
                        amt_on_esi = amt_on_esi + iErnAmt1
                    End If
                    If n = 7 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("ESIREL").ToString.Trim), n, 1) = "1" Then
                        amt_on_esi = amt_on_esi + iErnAmt2
                    End If
                    If n = 8 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("ESIREL").ToString.Trim), n, 1) = "1" Then
                        amt_on_esi = amt_on_esi + iErnAmt3
                    End If
                    If n = 9 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("ESIREL").ToString.Trim), n, 1) = "1" Then
                        amt_on_esi = amt_on_esi + iErnAmt4
                    End If
                    If n = 10 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("ESIREL").ToString.Trim), n, 1) = "1" Then
                        amt_on_esi = amt_on_esi + iErnAmt5
                    End If
                    If n = 11 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("ESIREL").ToString.Trim), n, 1) = "1" Then
                        amt_on_esi = amt_on_esi + iErnAmt6
                    End If
                    If n = 12 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("ESIREL").ToString.Trim), n, 1) = "1" Then
                        amt_on_esi = amt_on_esi + iErnAmt7
                    End If
                    If n = 13 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("ESIREL").ToString.Trim), n, 1) = "1" Then
                        amt_on_esi = amt_on_esi + iErnAmt8
                    End If
                    If n = 14 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("ESIREL").ToString.Trim), n, 1) = "1" Then
                        amt_on_esi = amt_on_esi + iErnAmt9
                    End If
                    If n = 15 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("ESIREL").ToString.Trim), n, 1) = "1" Then
                        amt_on_esi = amt_on_esi + iErnAmt10
                    End If
                    If n = 16 And Mid(Trim(rsPaysetup.Tables(0).Rows(0).Item("ESIREL").ToString.Trim), n, 1) = "1" Then
                        amt_on_esi = amt_on_esi + iOtAmt
                    End If
                Next n
                If Convert.ToDateTime(rsProcess.Tables(0).Rows(i).Item("MON_YEAR").ToString.Trim).ToString("MM") = "04" Or Convert.ToDateTime(rsProcess.Tables(0).Rows(i).Item("MON_YEAR").ToString.Trim).ToString("MM") = "10" And amt_on_esi > rsPaysetup.Tables(0).Rows(0).Item("ESILIMIT").ToString.Trim Then
                    amt_on_esi = 0
                    iESIAMT = 0
                    'Cn.Execute("update pay_master set ESI_ALLOWED='N' where paycode='" & Trim(sPaycode) & "'")
                    If Common.servername = "Access" Then
                        If Common.con1.State <> ConnectionState.Open Then
                            Common.con1.Open()
                        End If
                        cmd1 = New OleDbCommand(sSql, Common.con1)
                        cmd1.ExecuteNonQuery()
                        If Common.con1.State <> ConnectionState.Closed Then
                            Common.con1.Close()
                        End If
                    Else
                        If Common.con.State <> ConnectionState.Open Then
                            Common.con.Open()
                        End If
                        cmd = New SqlCommand(sSql, Common.con)
                        cmd.ExecuteNonQuery()
                        If Common.con.State <> ConnectionState.Closed Then
                            Common.con.Close()
                        End If
                    End If
                Else
                    iESIAMT = IIf(amt_on_esi > rsPaysetup.Tables(0).Rows(0).Item("ESILIMIT"), (rsPaysetup.Tables(0).Rows(0).Item("ESILIMIT") * rsPaysetup.Tables(0).Rows(0).Item("ESIE")) / 100, (amt_on_esi * rsPaysetup.Tables(0).Rows(0).Item("ESIE")) / 100)
                    'iESIAMT = IIf(amt_on_esi > rsPaysetup.Tables(0).Rows(0).Item("ESILIMIT"), (rsPaysetup.Tables(0).Rows(0).Item("ESILIMIT") * rsPaysetup.Tables(0).Rows(0).Item("ESIE")) / 100, (amt_on_esi * rsPaysetup.Tables(0).Rows(0).Item("ESIE")) / 100) + ((iOtAmt * rsPaysetup.Tables(0).Rows(0).Item("ESIE")) / 100)
                    iESIAMT = Math.Round(iESIAMT, 0) + IIf(iESIAMT - Math.Round(iESIAMT, 0) > 0, 1, 0)
                    'iESIAMT = math.round(iESIAMT, rsPaysetup.Tables(0).Rows(0).Item("ESIRND")) - math.round((iOtAmt * rsPaysetup.Tables(0).Rows(0).Item("ESIE")) / 100, rsPaysetup.Tables(0).Rows(0).Item("ESIRND"))
                    If iESIAMT = 0 Then
                        amt_on_esi = 0
                    End If
                End If
            End If

            '******-FOR ADVANCE & FINE Cal.-*********
            rsAdvance = New DataSet
            If Common.servername = "Access" Then
                sAdvFn = "Select Sum(Inst_Amt) as InstAmt From TBLADVANCEDATA " & _
                        "Where Paycode='" & Trim(sPaycode) & "' And FORMAT(Mon_Year,'MM') = " & DateEditTo.DateTime.ToString("MM") & _
                        " And FORMAT(Mon_Year,'YYYY') = " & DateEditTo.DateTime.ToString("yyyy") & " And A_L='" & "A'"
                adapA = New OleDbDataAdapter(sAdvFn, Common.con1)
                adapA.Fill(rsAdvance)
            Else
                sAdvFn = "Select Sum(Inst_Amt) as InstAmt From TBLADVANCEDATA " & "Where Paycode='" & Trim(sPaycode) & _
                        "' And DatePart(mm,Mon_Year) = " & DateEditTo.DateTime.ToString("MM") & " And DatePart(yyyy,Mon_Year) = " & DateEditTo.DateTime.ToString("yyyy") & _
                        " And A_L='" & "A'"
                adap = New SqlDataAdapter(sAdvFn, Common.con)
                adap.Fill(rsAdvance)
            End If
            'rsAdvance = Cn.Execute(sAdvFn)
            'Advance = IIf(IsNull(rsAdvance("InstAmt")), 0, rsAdvance("InstAmt"))
            If rsAdvance.Tables(0).Rows(0).Item("InstAmt").ToString.Trim = "" Then : Advance = 0 : Else : Advance = rsAdvance.Tables(0).Rows(0).Item("InstAmt") : End If
            '**Fine**
            rsFine = New DataSet
            If Common.servername = "Access" Then
                sAdvFn = "Select Sum(Inst_Amt) AS InstAmt From TBLADVANCEDATA " & _
                     "Where Paycode='" & sPaycode & _
                     "' And FORMAT(Mon_Year,'MM')=" & DateEditTo.DateTime.ToString("MM") & _
                     " And FORMAT(Mon_Year,'YYYY')=" & DateEditTo.DateTime.ToString("yyyy") & " And A_L='L'"
                adapA = New OleDbDataAdapter(sAdvFn, Common.con1)
                adapA.Fill(rsFine)
            Else
                sAdvFn = "Select Sum(Inst_Amt) AS InstAmt From TBLADVANCEDATA " & _
                     "Where Paycode='" & sPaycode & _
                     "' And DatePart(mm,Mon_Year)=" & DateEditTo.DateTime.ToString("MM") & _
                     " And DatePart(yyyy,Mon_Year)=" & DateEditTo.DateTime.ToString("yyyy") & " And A_L='L'"
                adap = New SqlDataAdapter(sAdvFn, Common.con)
                adap.Fill(rsFine)
            End If
            'rsFine = Cn.Execute(sAdvFn)
            'Fine = IIf(IsNull(rsFine("InstAmt")), 0, rsFine("InstAmt"))
            If rsFine.Tables(0).Rows(0).Item("InstAmt").ToString.Trim = "" Then : Fine = 0 : Else : Fine = rsFine.Tables(0).Rows(0).Item("InstAmt").ToString.Trim : End If
            'rsAdvance.Close()
            'rsFine.Close()
            '****************************************
            D_1TOD_10 = iDedAmt1 + iDedAmt2 + iDedAmt3 + iDedAmt4 + iDedAmt5 + iDedAmt6 + iDedAmt7 + iDedAmt8 + iDedAmt9 + iDedAmt10
            E_1TOE_10 = iErnAmt1 + iErnAmt2 + iErnAmt3 + iErnAmt4 + iErnAmt5 + iErnAmt6 + iErnAmt7 + iErnAmt8 + iErnAmt9 + iErnAmt10
            iGrossSal = iSalary + DA + iHRAAMT + iOtAmt + E_1TOE_10 + conv_amt + medical_amt
            '******-FOR PROFESSIONAL TAX*************
            If rsProcess.Tables(0).Rows(i).Item("PROF_TAX_ALLOWED").ToString.Trim = "Y" Then
                sSql = "select * from professionaltax where BRANCHCODE=" & "(SELECT BRANCHCODE FROM TBLEMPLOYEE WHERE PAYCODE='" & sPaycode & "') AND  lowerlimit<=" & iGrossSal & " and upperlimit>=" & iGrossSal
                'rsProf = Cn.Execute(sSql)
                rsProf = New DataSet
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(rsProf)
                Else
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(rsProf)
                End If
                If rsProf.Tables(0).Rows.Count > 0 Then
                    prof_tax_amt = rsProf.Tables(0).Rows(0).Item("taxamount")
                End If
                If rsPaysetup.Tables(0).Rows(0).Item("PROF_TAX_RND").ToString.Trim = "Y" Then
                    prof_tax_amt = Math.Round(prof_tax_amt, 0)
                Else
                    prof_tax_amt = Math.Round(prof_tax_amt, 2)
                End If
            End If
            iNetSal = iGrossSal - (D_1TOD_10 + Fine + Advance + iESIAMT + iPFAMT + iFPFAMT + tds_amt + prof_tax_amt)
            ''************** Insert into Pay_Result Table **************

            sSql = "Update pay_result Set AMTONPF=" & amt_on_pf & ",VPF_AMT=" & iPFAMT & ",VEPF_AMT=" & iEPFAMT & ",VFPF_AMT=" & iFPFAMT & ",VVPF_AMT=" & iVPFAMT & _
                 ",VTDS_AMT=" & tds_amt & ",PROF_TAX_AMT=" & prof_tax_amt & ",VHRA_AMT=" & iHRAAMT & ",VCONV_AMT=" & conv_amt & ",VMED_AMT=" & medical_amt & ",AMTONESI=" & amt_on_esi & ",ESIONOT=" & _
                 IIf(Mid(rsPaysetup.Tables(0).Rows(0).Item("ESIREL"), 16, 1) = "0", 0, IIf(iESIAMT > 0, Math.Round((iOtAmt * rsPaysetup.Tables(0).Rows(0).Item("ESIE")) / 100, 0) + IIf((iOtAmt * rsPaysetup.Tables(0).Rows(0).Item("ESIE")) / 100 - Math.Round((iOtAmt * rsPaysetup.Tables(0).Rows(0).Item("ESIE")) / 100, 0) > 0, 1, 0), 0)) & _
                 ",VESI_AMT=" & iESIAMT & ",vot_amt=" & iOtAmt & _
                 ",VFINE=" & Fine & ",VADVANCE=" & Advance & ",VDA_AMT=" & DA & _
                 ",VD_1_AMT=" & iDedAmt1 & ",VD_2_AMT=" & iDedAmt2 & _
                 ",VD_3_AMT=" & iDedAmt3 & ",VD_4_AMT=" & iDedAmt4 & _
                 ",VD_5_AMT=" & iDedAmt5 & ",VD_6_AMT=" & iDedAmt6 & _
                 ",VD_7_AMT=" & iDedAmt7 & ",VD_8_AMT=" & iDedAmt8 & _
                 ",VD_9_AMT=" & iDedAmt9 & ",VD_10_AMT=" & iDedAmt10 & _
                 ",VI_1_AMT=" & iErnAmt1 & ",VI_2_AMT=" & iErnAmt2 & _
                 ",VI_3_AMT=" & iErnAmt3 & ",VI_4_AMT=" & iErnAmt4 & _
                 ",VI_5_AMT=" & iErnAmt5 & ",VI_6_AMT=" & iErnAmt6 & _
                 ",VI_7_AMT=" & iErnAmt7 & ",VI_8_AMT=" & iErnAmt8 & _
                 ",VI_9_AMT=" & iErnAmt9 & ",VI_10_AMT=" & iErnAmt10 & _
                 ",VTOTAL_DAY=" & MON_DAY & ",VLEAVE=" & LEAVE
            If Common.servername = "Access" Then
                sSql = sSql + ",VSALARY=" & iSalary & ",VTDAYS=" & TDAYS & " Where Paycode='" & Trim(sPaycode) & "' And FORMAT(Mon_Year,'MM')=" & DateEditTo.DateTime.ToString("MM") & " And FORMAT(Mon_Year,'YYYY')=" & DateEditTo.DateTime.ToString("yyyy")
                If Common.con1.State <> ConnectionState.Open Then
                    Common.con1.Open()
                End If
                cmd1 = New OleDbCommand(sSql, Common.con1)
                cmd1.ExecuteNonQuery()
                If Common.con1.State <> ConnectionState.Closed Then
                    Common.con1.Close()
                End If
            Else
                sSql = sSql + ",VSALARY=" & iSalary & ",VTDAYS=" & TDAYS & " Where Paycode='" & Trim(sPaycode) & "' And DatePart(mm,Mon_Year)=" & DateEditTo.DateTime.ToString("MM") & " And DatePart(yyyy,Mon_Year)=" & DateEditTo.DateTime.ToString("yyyy")
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
            'Cn.Execute(sSql)
            ''**********************************************************
            ''            REIMBURSHMENT CALCULATION
            '******-FOR CONVEYANCE*************
            If Mid(rsPaysetup.Tables(0).Rows(0).Item("REIMBREL"), 1, 1) = "1" Then
                If rsProcess.Tables(0).Rows(i).Item("Vconv_f") = "F" Then
                    Reimbconv_amt = rsProcess.Tables(0).Rows(i).Item("Vconv_rate")
                Else
                    Reimbconv_amt = (rsProcess.Tables(0).Rows(i).Item("Vconv_rate") / MON_DAY) * TDAYS
                End If
            Else
                Reimbconv_amt = 0
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("CONV_RND").ToString.Trim = "Y" Then
                Reimbconv_amt = Math.Round(Reimbconv_amt, 0)
            Else
                Reimbconv_amt = Math.Round(Reimbconv_amt, 2)
            End If
            '******-FOR MEDICAL*************
            If Mid(rsPaysetup.Tables(0).Rows(0).Item("REIMBREL").ToString.Trim, 2, 1) = "1" Then
                If rsProcess.Tables(0).Rows(i).Item("Vmed_f") = "F" Then
                    Reimmedical_amt = rsProcess.Tables(0).Rows(i).Item("Vmed_rate")
                Else
                    Reimmedical_amt = (rsProcess.Tables(0).Rows(i).Item("Vmed_rate") / MON_DAY) * TDAYS
                End If
            Else
                Reimmedical_amt = 0
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("MEDICAL_RND").ToString.Trim = "Y" Then
                Reimmedical_amt = Math.Round(Reimmedical_amt, 0)
            Else
                Reimmedical_amt = Math.Round(Reimmedical_amt, 2)
            End If
            '******-FOR Earning   Formula -**********
            If Mid(rsPaysetup.Tables(0).Rows(0).Item("REIMBREL").ToString.Trim, 3, 1) = "1" Then
                If rsProcess.Tables(0).Rows(i).Item("VIT_1").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i).Item("VIT_1").ToString.Trim <> "0" Then
                    sWhichFormula = ProcFormula(rsProcess.Tables(0).Rows(i).Item("VIT_1").ToString)
                    If sWhichFormula <> "" And sWhichFormula <> " " Then
                        ReimErnAmt1 = FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                    End If
                Else
                    ReimErnAmt1 = iern1
                End If
            Else
                ReimErnAmt1 = 0
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("EARN1_RND").ToString.Trim = "Y" Then
                ReimErnAmt1 = Math.Round(ReimErnAmt1, 0)
            Else
                ReimErnAmt1 = Math.Round(ReimErnAmt1, 2)
            End If
            If Mid(rsPaysetup.Tables(0).Rows(0).Item("REIMBREL").ToString.Trim, 4, 1) = "1" Then
                If rsProcess.Tables(0).Rows(i).Item("VIT_2").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i).Item("VIT_2").ToString.Trim <> "0" Then
                    sWhichFormula = ProcFormula(rsProcess.Tables(0).Rows(i).Item("VIT_2").ToString)
                    If sWhichFormula <> "" And sWhichFormula <> " " Then
                        ReimErnAmt2 = FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                    End If
                Else
                    ReimErnAmt2 = iern2
                End If
            Else
                ReimErnAmt2 = 0
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("EARN2_RND").ToString.Trim = "Y" Then
                ReimErnAmt2 = Math.Round(ReimErnAmt2, 0)
            Else
                ReimErnAmt2 = Math.Round(ReimErnAmt2, 2)
            End If
            If Mid(rsPaysetup.Tables(0).Rows(0).Item("REIMBREL").ToString.Trim, 5, 1) = "1" Then
                If rsProcess.Tables(0).Rows(i).Item("VIT_3").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i).Item("VIT_3").ToString.Trim <> "0" Then
                    sWhichFormula = ProcFormula(rsProcess.Tables(0).Rows(i).Item("VIT_3").ToString)
                    If sWhichFormula <> "" And sWhichFormula <> " " Then
                        ReimErnAmt3 = FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                    End If
                Else
                    ReimErnAmt3 = iern3
                End If
            Else
                ReimErnAmt3 = 0
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("EARN3_RND").ToString.Trim = "Y" Then
                ReimErnAmt3 = Math.Round(ReimErnAmt3, 0)
            Else
                ReimErnAmt3 = Math.Round(ReimErnAmt3, 2)
            End If
            If Mid(rsPaysetup.Tables(0).Rows(0).Item("REIMBREL").ToString.Trim, 6, 1) = "1" Then
                If rsProcess.Tables(0).Rows(i).Item("VIT_4").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i).Item("VIT_4") <> 0 Then
                    sWhichFormula = ProcFormula(rsProcess.Tables(0).Rows(i).Item("VIT_4").ToString)
                    If sWhichFormula <> "" And sWhichFormula <> " " Then
                        ReimErnAmt4 = FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                    End If
                Else
                    ReimErnAmt4 = iern4
                End If
            Else
                ReimErnAmt4 = 0
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("EARN4_RND").ToString.Trim = "Y" Then
                ReimErnAmt4 = Math.Round(ReimErnAmt4, 0)
            Else
                ReimErnAmt4 = Math.Round(ReimErnAmt4, 2)
            End If
            If Mid(rsPaysetup.Tables(0).Rows(0).Item("REIMBREL").ToString.Trim, 7, 1) = "1" Then
                If rsProcess.Tables(0).Rows(i).Item("VIT_5").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i).Item("VIT_5") <> 0 Then
                    sWhichFormula = ProcFormula(rsProcess.Tables(0).Rows(i).Item("VIT_5").ToString)
                    If sWhichFormula <> "" And sWhichFormula <> " " Then
                        ReimErnAmt5 = FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                    End If
                Else
                    ReimErnAmt5 = iern5
                End If
            Else
                ReimErnAmt5 = 0
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("EARN5_RND").ToString.Trim = "Y" Then
                ReimErnAmt5 = Math.Round(ReimErnAmt5, 0)
            Else
                ReimErnAmt5 = Math.Round(ReimErnAmt5, 2)
            End If
            If Mid(rsPaysetup.Tables(0).Rows(0).Item("REIMBREL").ToString.Trim, 8, 1) = "1" Then
                If rsProcess.Tables(0).Rows(i).Item("VIT_6").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i).Item("VIT_6") <> 0 Then
                    sWhichFormula = ProcFormula(rsProcess.Tables(0).Rows(i).Item("VIT_6").ToString)
                    If sWhichFormula <> "" And sWhichFormula <> " " Then
                        ReimErnAmt6 = FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                    End If
                Else
                    ReimErnAmt6 = iern6
                End If
            Else
                ReimErnAmt6 = 0
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("EARN6_RND").ToString.Trim = "Y" Then
                ReimErnAmt6 = Math.Round(ReimErnAmt6, 0)
            Else
                ReimErnAmt6 = Math.Round(ReimErnAmt6, 2)
            End If
            If Mid(rsPaysetup.Tables(0).Rows(0).Item("REIMBREL").ToString.Trim, 9, 1) = "1" Then
                If rsProcess.Tables(0).Rows(i).Item("VIT_7").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i).Item("VIT_7") <> 0 Then
                    sWhichFormula = ProcFormula(rsProcess.Tables(0).Rows(i).Item("VIT_7").ToString)
                    If sWhichFormula <> "" And sWhichFormula <> " " Then
                        ReimErnAmt7 = FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                    End If
                Else
                    ReimErnAmt7 = iern7
                End If
            Else
                ReimErnAmt7 = 0
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("EARN7_RND").ToString.Trim = "Y" Then
                ReimErnAmt7 = Math.Round(ReimErnAmt7, 0)
            Else
                ReimErnAmt7 = Math.Round(ReimErnAmt7, 2)
            End If
            If Mid(rsPaysetup.Tables(0).Rows(0).Item("REIMBREL"), 10, 1) = "1" Then
                If rsProcess.Tables(0).Rows(i).Item("VIT_8").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i).Item("VIT_8") <> 0 Then
                    sWhichFormula = ProcFormula(rsProcess.Tables(0).Rows(i).Item("VIT_8").ToString)
                    If sWhichFormula <> "" And sWhichFormula <> " " Then
                        ReimErnAmt8 = FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                    End If
                Else
                    ReimErnAmt8 = iern8
                End If
            Else
                ReimErnAmt8 = 0
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("EARN8_RND").ToString.Trim = "Y" Then
                ReimErnAmt8 = Math.Round(ReimErnAmt8, 0)
            Else
                ReimErnAmt8 = Math.Round(ReimErnAmt8, 2)
            End If
            If Mid(rsPaysetup.Tables(0).Rows(0).Item("REIMBREL").ToString.Trim, 11, 1) = "1" Then
                If rsProcess.Tables(0).Rows(i).Item("VIT_9").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i).Item("VIT_9") <> 0 Then
                    sWhichFormula = ProcFormula(rsProcess.Tables(0).Rows(i).Item("VIT_9").ToString)
                    If sWhichFormula <> "" And sWhichFormula <> " " Then
                        ReimErnAmt9 = FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                    End If
                Else
                    ReimErnAmt9 = iern9
                End If
            Else
                ReimErnAmt9 = 0
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("EARN9_RND").ToString.Trim = "Y" Then
                ReimErnAmt9 = Math.Round(ReimErnAmt9, 0)
            Else
                ReimErnAmt9 = Math.Round(ReimErnAmt9, 2)
            End If
            If Mid(rsPaysetup.Tables(0).Rows(0).Item("REIMBREL").ToString.Trim, 12, 1) = "1" Then
                If rsProcess.Tables(0).Rows(i).Item("VIT_10").ToString.Trim <> "" And rsProcess.Tables(0).Rows(i).Item("VIT_10") <> 0 Then
                    sWhichFormula = ProcFormula(rsProcess.Tables(0).Rows(i).Item("VIT_10").ToString)
                    If sWhichFormula <> "" And sWhichFormula <> " " Then
                        ReimErnAmt10 = FormulaCal(sWhichFormula, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
                    End If
                Else
                    ReimErnAmt10 = iern10
                End If
            Else
                ReimErnAmt10 = 0
            End If
            If rsPaysetup.Tables(0).Rows(0).Item("EARN10_RND").ToString.Trim = "Y" Then
                ReimErnAmt10 = Math.Round(ReimErnAmt10, 0)
            Else
                ReimErnAmt10 = Math.Round(ReimErnAmt10, 2)
            End If
            sSql = "Update PAY_REIMURSH Set VCONV_AMT=" & Reimbconv_amt & ",VMED_AMT=" & Reimmedical_amt & _
                     ",VI_1_AMT=" & ReimErnAmt1 & ",VI_2_AMT=" & ReimErnAmt2 & _
                     ",VI_3_AMT=" & ReimErnAmt3 & ",VI_4_AMT=" & ReimErnAmt4 & _
                     ",VI_5_AMT=" & ReimErnAmt5 & ",VI_6_AMT=" & ReimErnAmt6 & _
                     ",VI_7_AMT=" & ReimErnAmt7 & ",VI_8_AMT=" & ReimErnAmt8 & _
                     ",VI_9_AMT=" & ReimErnAmt9 & ",VI_10_AMT=" & ReimErnAmt10
            If Common.servername = "Access" Then
                sSql = sSql + " Where Paycode='" & Trim(sPaycode) & "' And FORMAT(Mon_Year,'MM')=" & DateEditTo.DateTime.ToString("MM") & " And FORMAT(Mon_Year,'YYYY')=" & DateEditTo.DateTime.ToString("yyyy")
                If Common.con1.State <> ConnectionState.Open Then
                    Common.con1.Open()
                End If
                cmd1 = New OleDbCommand(sSql, Common.con1)
                cmd1.ExecuteNonQuery()
                If Common.con1.State <> ConnectionState.Closed Then
                    Common.con1.Close()
                End If
            Else
                sSql = sSql + " Where Paycode='" & Trim(sPaycode) & "' And DatePart(mm,Mon_Year)=" & DateEditTo.DateTime.ToString("MM") & " And DatePart(yyyy,Mon_Year)=" & DateEditTo.DateTime.ToString("yyyy")
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                If Common.con.State <> ConnectionState.Closed Then
                    Common.con.Close()
                End If
            End If
            'Cn.Execute(sSql)
            'rsProcess.Tables(0).Rows(i).Item.MoveNext()
        Next '   Loop
        If Common.servername = "Access" Then
            sSql = "delete from pay_result where FORMAT(mon_year,'yyyy-MM-dd')='" & DateEditTo.DateTime.ToString("yyyy-MM-01") & "' and paycode not in (select paycode from Pay_master)"
            'Cn.Execute(sSql)
            If Common.con1.State <> ConnectionState.Open Then
                Common.con1.Open()
            End If
            cmd1 = New OleDbCommand(sSql, Common.con1)
            cmd1.ExecuteNonQuery()
            sSql = "delete from PAY_REIMURSH where format(mon_year,'yyyy-MM-dd')='" & DateEditTo.DateTime.ToString("yyyy-MM-01") & "' and paycode not in (select paycode from Pay_master)"
            cmd1 = New OleDbCommand(sSql, Common.con1)
            cmd1.ExecuteNonQuery()
            If Common.con1.State <> ConnectionState.Closed Then
                Common.con1.Close()
            End If
            'Cn.Execute(sSql)
        Else
            sSql = "delete from pay_result where mon_year='" & DateEditTo.DateTime.ToString("yyyy-MM-01") & "' and paycode not in (select paycode from Pay_master)"
            'Cn.Execute(sSql)
            If Common.con.State <> ConnectionState.Open Then
                Common.con.Open()
            End If
            cmd = New SqlCommand(sSql, Common.con)
            cmd.ExecuteNonQuery()
            sSql = "delete from PAY_REIMURSH where mon_year='" & DateEditTo.DateTime.ToString("yyyy-MM-01") & "' and paycode not in (select paycode from Pay_master)"
            cmd = New SqlCommand(sSql, Common.con)
            cmd.ExecuteNonQuery()
            If Common.con.State <> ConnectionState.Closed Then
                Common.con.Close()
            End If
            'Cn.Execute(sSql)
        End If
        Me.Cursor = Cursors.Default
        rsProcess = Nothing
        rsAdvance = Nothing
        rsFine = Nothing
        'DoProcess = True
        Return True
        'Exit Function
        'Catch ex As Exception
        '    Return False
        'End Try

        'MsgBox(Err.Number & " --> AKDoProcess " & Chr(13) & Err.Description)
        'Resume Next
        'rsProcess = Nothing
        'rsAdvance = Nothing
        'rsFine = Nothing

        'DoProcess = False
    End Function
    Public Function minutetohour(ByVal min As String) As String
        Dim totallate As String
        Try
            Dim latehours As Double = Convert.ToInt32(Math.Truncate(Convert.ToDouble(min) / 60))
            Dim latemin As Double = Convert.ToInt32(Math.Truncate(Convert.ToDouble(min) Mod 60))
            If latemin < 10 Then
                totallate = (latehours.ToString + (".0" & latemin.ToString))
            Else
                totallate = (latehours.ToString + ("." & latemin.ToString))
            End If
        Catch ex As Exception
            totallate = 0
        End Try
        Return totallate
    End Function
    ''****************************************************************
    ''    This function is use for checking "If" statement in formula.
    ''****************************************************************
    Public Function ProcFormula(sFORMULA As String) As Object
        Dim rsFormula As DataSet = New DataSet 'ADODB.Recordset
        Dim sSql As String
        Dim iPos As Integer
        Dim iPosThen As Integer
        Dim iPosElse As Integer
        Dim sPOS As String
        Dim sTHEN As String
        Dim sELSE As String
        Dim sEndIf As String
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        'On Error GoTo ErrHand
        sSql = "Select CODE,FORM From Pay_Formula Where Code='" & sFORMULA & "'"
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(rsFormula)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(rsFormula)
        End If
        'rsFormula = Cn.Execute(sSql)
        If rsFormula.Tables(0).Rows.Count = 1 Then 'RecordCount = 1 Then
            ProcFormula = Trim(rsFormula.Tables(0).Rows(0).Item("FORM").ToString.Trim)
        Else
            ProcFormula = ""
        End If
        Return ProcFormula
        'Exit Function
        'ErrHand:
        '        MsgBox(Err.Number & " -> AKProcFormula " & Chr(13) & Err.Description, vbCritical)
    End Function

    ''**************************************************************************************
    ''    This function is use for calculating Division,Multiplication,Addition,Subtraction
    ''    in a formula.
    ''**************************************************************************************
    Public Function FormulaCal(a As String, BASIC As Double, DA As Double, HRA As Double, PF As Double, FPF As Double, ESI As Double, PRE As Double, ABS1 As Double, HLD As Double, LATE As Double, EARLY As Double, OT As Double, CL As Double, SL As Double, PL_EL As Double, OTHER_LV As Double, LEAVE As Double, TDAYS As Double, T_LATE As Double, T_EARLY As Double, OT_RATE As Double, MON_DAY As Double, ided1 As Double, ided2 As Double, ided3 As Double, ided4 As Double, ided5 As Double, ided6 As Double, ided7 As Double, ided8 As Double, ided9 As Double, ided10 As Double, iern1 As Double, iern2 As Double, iern3 As Double, iern4 As Double, iern5 As Double, iern6 As Double, iern7 As Double, iern8 As Double, iern9 As Double, iern10 As Double, iSalary As Double, iHRAAMT As Double, iOtAmt As Double, iErnAmt1 As Double, iErnAmt2 As Double, iErnAmt3 As Double, iErnAmt4 As Double, iErnAmt5 As Double, iErnAmt6 As Double, iErnAmt7 As Double, iErnAmt8 As Double, iErnAmt9 As Double, iErnAmt10 As Double, _
        TDS As Double, PROF_TAX As Double, CONV As Double, MED As Double) As Double
        Dim B As String
        Dim Operand As String '* 1
        Dim iLeftPos1 As Integer
        Dim iRightPos1 As Integer
        Dim iPos As Integer, iPos1 As Integer, iPos2 As Integer
        Dim iLen As Integer
        Dim AK As Integer
        Dim ak1, AK2, AK3, AK4
        Dim bAkIf As Boolean
        Dim mValue As Double
        On Error GoTo ErrHand
        ak1 = FormulaValueSet(a, BASIC, DA, HRA, PF, FPF, ESI, PRE, ABS1, HLD, LATE, EARLY, OT, CL, SL, PL_EL, OTHER_LV, LEAVE, TDAYS, T_LATE, T_EARLY, OT_RATE, MON_DAY, ided1, ided2, ided3, ided4, ided5, ided6, ided7, ided8, ided9, ided10, iern1, iern2, iern3, iern4, iern5, iern6, iern7, iern8, iern9, iern10, iSalary, iHRAAMT, iOtAmt, iErnAmt1, iErnAmt2, iErnAmt3, iErnAmt4, iErnAmt5, iErnAmt6, iErnAmt7, iErnAmt8, iErnAmt9, iErnAmt10, TDS, PROF_TAX, CONV, MED)
        mValue = New DataTable().Compute(ak1, Nothing) 'Trim(ak1) ' frmCapture.ScriptControl1.Eval(Trim(ak1))
        'mValue = Evaluate(Mid(Trim(ak1), 1, Trim(Len(ak1))))
        FormulaCal = mValue
        Exit Function
ErrHand:
        XtraMessageBox.Show(ulf, "<size=10>" & Err.Number & "  -> AKFormulaCal " & Chr(13) & Err.Description & "</size>", "<size=9>Error<siez>")
    End Function

    Function FormulaValueSet(a As String, BASIC As Double, DA As Double, HRA As Double, PF As Double, FPF As Double, ESI As Double, PRE As Double, ABS1 As Double, HLD As Double, LATE As Double, EARLY As Double, OT As Double, CL As Double, SL As Double, PL_EL As Double, OTHER_LV As Double, LEAVE As Double, TDAYS As Double, T_LATE As Double, T_EARLY As Double, OT_RATE As Double, MON_DAY As Double, ided1 As Double, ided2 As Double, ided3 As Double, ided4 As Double, ided5 As Double, ided6 As Double, ided7 As Double, ided8 As Double, ided9 As Double, ided10 As Double, iern1 As Double, iern2 As Double, iern3 As Double, iern4 As Double, iern5 As Double, iern6 As Double, iern7 As Double, iern8 As Double, iern9 As Double, iern10 As Double, iSalary As Double, iHRAAMT As Double, iOtAmt As Double, iErnAmt1 As Double, iErnAmt2 As Double, iErnAmt3 As Double, iErnAmt4 As Double, iErnAmt5 As Double, iErnAmt6 As Double, iErnAmt7 As Double, iErnAmt8 As Double, iErnAmt9 As Double, iErnAmt10 As Double, _
TDS As Double, PROF_TAX As Double, CONV As Double, MED As Double) As String

        Dim d As Integer
        Dim mform As DataSet = New DataSet 'ADODB.Recordset
        Dim sSql As String
        Dim fhalf As String
        Dim lhalf As String
        Dim formulalen As Integer
        Dim bm As String, a1 As String, m As Integer, mifChk As Boolean
        Dim St As String, mCtr As Integer, Rt As String, Tstring As String, Fstring As String, lCnt As Integer, Pcnt As Integer
        bm = a
        a = a & " "
        'On Error GoTo ErrHand
a:      If InStr(1, UCase(a), "#A#") <> 0 Then
            'd = InStr(1, UCase(a), "#A#")
            'sSql = "select * from pay_formula where code='A'"
            'mform = Cn.Execute(sSql)
            'formulalen = Len(mform!Form)
            'fhalf = Mid(a, 1, d - 1)
            'formulalen = d + 3
            'lhalf = Mid(a, formulalen)
            a = selectQuery(a, "A") 'fhalf & "[ " & Trim(mform!Form) & " ]" & lhalf
            If InStr(1, UCase(a), "#A#") <> 0 Then GoTo a
        End If

B:      If InStr(1, UCase(a), "#B#") <> 0 Then
            'd = InStr(1, UCase(a), "#B#")
            'sSql = "select * from pay_formula where code='B'"
            'mform = Cn.Execute(sSql)
            'formulalen = Len(mform!Form)
            'fhalf = Mid(a, 1, d - 1)
            'formulalen = d + 3
            'lhalf = Mid(a, formulalen)
            a = selectQuery(a, "B") 'fhalf & "[ " & Trim(mform!Form) & " ]" & lhalf
            If InStr(1, UCase(a), "#B#") <> 0 Then GoTo B
        End If
C:      If InStr(1, UCase(a), "#C#") <> 0 Then
            'd = InStr(1, UCase(a), "#C#")
            'sSql = "select * from pay_formula where code='C'"
            'mform = Cn.Execute(sSql)
            'formulalen = Len(mform!Form)
            'fhalf = Mid(a, 1, d - 1)
            'formulalen = d + 3
            'lhalf = Mid(a, formulalen)
            a = selectQuery(a, "C") 'fhalf & "[ " & Trim(mform!Form) & " ]" & lhalf
            If InStr(1, UCase(a), "#C#") <> 0 Then GoTo C
        End If

d:      If InStr(1, UCase(a), "#D#") <> 0 Then
            'd = InStr(1, UCase(a), "#D#")
            'sSql = "select * from pay_formula where code='D'"
            'mform = Cn.Execute(sSql)
            'formulalen = Len(mform!Form)
            'fhalf = Mid(a, 1, d - 1)
            'formulalen = d + 3
            'lhalf = Mid(a, formulalen)
            a = selectQuery(a, "D") 'fhalf & "[ " & Trim(mform!Form) & " ]" & lhalf
            If InStr(1, UCase(a), "#D#") <> 0 Then GoTo d
        End If

e:      If InStr(1, UCase(a), "#E#") <> 0 Then
            'd = InStr(1, UCase(a), "#E#")
            'sSql = "select * from pay_formula where code='E'"
            'mform = Cn.Execute(sSql)
            'formulalen = Len(mform!Form)
            'fhalf = Mid(a, 1, d - 1)
            'formulalen = d + 3
            'lhalf = Mid(a, formulalen)
            a = selectQuery(a, "E") 'fhalf & "[ " & Trim(mform!Form) & " ]" & lhalf
            If InStr(1, UCase(a), "#E#") <> 0 Then GoTo e
        End If

f:      If InStr(1, UCase(a), "#F#") <> 0 Then
            'd = InStr(1, UCase(a), "#F#")
            'sSql = "select * from pay_formula where code='F'"
            'mform = Cn.Execute(sSql)
            'formulalen = Len(mform!Form)
            'fhalf = Mid(a, 1, d - 1)
            'formulalen = d + 3
            'lhalf = Mid(a, formulalen)
            a = selectQuery(a, "F") 'fhalf & "[ " & Trim(mform!Form) & " ]" & lhalf
            If InStr(1, UCase(a), "#F#") <> 0 Then GoTo f
        End If

G:      If InStr(1, UCase(a), "#G#") <> 0 Then
            'd = InStr(1, UCase(a), "#G#")
            'sSql = "select * from pay_formula where code='G'"
            'mform = Cn.Execute(sSql)
            'formulalen = Len(mform!Form)
            'fhalf = Mid(a, 1, d - 1)
            'formulalen = d + 3
            'lhalf = Mid(a, formulalen)
            a = selectQuery(a, "G") 'fhalf & "[ " & Trim(mform!Form) & " ]" & lhalf
            If InStr(1, UCase(a), "#G#") <> 0 Then GoTo G
        End If

h:      If InStr(1, UCase(a), "#H#") <> 0 Then
            'd = InStr(1, UCase(a), "#H#")
            'sSql = "select * from pay_formula where code='H'"
            'mform = Cn.Execute(sSql)
            'formulalen = Len(mform!Form)
            'fhalf = Mid(a, 1, d - 1)
            'formulalen = d + 3
            'lhalf = Mid(a, formulalen)
            a = selectQuery(a, "H") ' fhalf & "[ " & Trim(mform!Form) & " ]" & lhalf
            If InStr(1, UCase(a), "#H#") <> 0 Then GoTo h
        End If

i:      If InStr(1, UCase(a), "#I#") <> 0 Then
            'd = InStr(1, UCase(a), "#I#")
            'sSql = "select * from pay_formula where code='I'"
            'mform = Cn.Execute(sSql)
            'formulalen = Len(mform!Form)
            'fhalf = Mid(a, 1, d - 1)
            'formulalen = d + 3
            'lhalf = Mid(a, formulalen)
            a = selectQuery(a, "I") 'fhalf & "[ " & Trim(mform!Form) & " ]" & lhalf
            If InStr(1, UCase(a), "#I#") <> 0 Then GoTo i
        End If

j:      If InStr(1, UCase(a), "#J#") <> 0 Then
            'd = InStr(1, UCase(a), "#J#")
            'sSql = "select * from pay_formula where code='J'"
            'mform = Cn.Execute(sSql)
            'formulalen = Len(mform!Form)
            'fhalf = Mid(a, 1, d - 1)
            'formulalen = d + 3
            'lhalf = Mid(a, formulalen)
            a = selectQuery(a, "J") 'fhalf & "[ " & Trim(mform!Form) & " ]" & lhalf
            If InStr(1, UCase(a), "#J#") <> 0 Then GoTo j
        End If

k:      If InStr(1, UCase(a), "#K#") <> 0 Then
            'd = InStr(1, UCase(a), "#K#")
            'sSql = "select * from pay_formula where code='K'"
            'mform = Cn.Execute(sSql)
            'formulalen = Len(mform!Form)
            'fhalf = Mid(a, 1, d - 1)
            'formulalen = d + 3
            'lhalf = Mid(a, formulalen)
            a = selectQuery(a, "K") 'fhalf & "[ " & Trim(mform!Form) & " ]" & lhalf
            If InStr(1, UCase(a), "#K#") <> 0 Then GoTo k
        End If

l:      If InStr(1, UCase(a), "#L#") <> 0 Then
            'd = InStr(1, UCase(a), "#L#")
            'sSql = "select * from pay_formula where code='L'"
            'mform = Cn.Execute(sSql)
            'formulalen = Len(mform!Form)
            'fhalf = Mid(a, 1, d - 1)
            'formulalen = d + 3
            'lhalf = Mid(a, formulalen)
            a = selectQuery(a, "L") 'fhalf & "[ " & Trim(mform!Form) & " ]" & lhalf
            If InStr(1, UCase(a), "#L#") <> 0 Then GoTo l
        End If

m:      If InStr(1, UCase(a), "#M#") <> 0 Then
            'd = InStr(1, UCase(a), "#M#")
            'sSql = "select * from pay_formula where code='M'"
            'mform = Cn.Execute(sSql)
            'formulalen = Len(mform!Form)
            'fhalf = Mid(a, 1, d - 1)
            'formulalen = d + 3
            'lhalf = Mid(a, formulalen)
            a = selectQuery(a, "M") 'fhalf & "[ " & Trim(mform!Form) & " ]" & lhalf
            If InStr(1, UCase(a), "#M#") <> 0 Then GoTo m
        End If

n:      If InStr(1, UCase(a), "#N#") <> 0 Then
            'd = InStr(1, UCase(a), "#N#")
            'sSql = "select * from pay_formula where code='N'"
            'mform = Cn.Execute(sSql)
            'formulalen = Len(mform!Form)
            'fhalf = Mid(a, 1, d - 1)
            'formulalen = d + 3
            'lhalf = Mid(a, formulalen)
            a = selectQuery(a, "N") 'fhalf & "[ " & Trim(mform!Form) & " ]" & lhalf
            If InStr(1, UCase(a), "#N#") <> 0 Then GoTo n
        End If

o:      If InStr(1, UCase(a), "#O#") <> 0 Then
            'd = InStr(1, UCase(a), "#O#")
            'sSql = "select * from pay_formula where code='O'"
            'mform = Cn.Execute(sSql)
            'formulalen = Len(mform!Form)
            'fhalf = Mid(a, 1, d - 1)
            'formulalen = d + 3
            'lhalf = Mid(a, formulalen)
            a = selectQuery(a, "O") 'fhalf & "[ " & Trim(mform!Form) & " ]" & lhalf
            If InStr(1, UCase(a), "#O#") <> 0 Then GoTo o
        End If

p:      If InStr(1, UCase(a), "#P#") <> 0 Then
            'd = InStr(1, UCase(a), "#P#")
            'sSql = "select * from pay_formula where code='P'"
            'mform = Cn.Execute(sSql)
            'formulalen = Len(mform!Form)
            'fhalf = Mid(a, 1, d - 1)
            'formulalen = d + 3
            'lhalf = Mid(a, formulalen)
            a = selectQuery(a, "P") 'fhalf & "[ " & Trim(mform!Form) & " ]" & lhalf
            If InStr(1, UCase(a), "#P#") <> 0 Then GoTo p
        End If

q:      If InStr(1, UCase(a), "#Q#") <> 0 Then
            'd = InStr(1, UCase(a), "#Q#")
            'sSql = "select * from pay_formula where code='Q'"
            'mform = Cn.Execute(sSql)
            'formulalen = Len(mform!Form)
            'fhalf = Mid(a, 1, d - 1)
            'formulalen = d + 3
            'lhalf = Mid(a, formulalen)
            a = selectQuery(a, "Q") 'fhalf & "[ " & Trim(mform!Form) & " ]" & lhalf
            If InStr(1, UCase(a), "#Q#") <> 0 Then GoTo q
        End If

R:      If InStr(1, UCase(a), "#R#") <> 0 Then
            'd = InStr(1, UCase(a), "#R#")
            'sSql = "select * from pay_formula where code='R'"
            'mform = Cn.Execute(sSql)
            'formulalen = Len(mform!Form)
            'fhalf = Mid(a, 1, d - 1)
            'formulalen = d + 3
            'lhalf = Mid(a, formulalen)
            a = selectQuery(a, "R") 'fhalf & "[ " & Trim(mform!Form) & " ]" & lhalf
            If InStr(1, UCase(a), "#R#") <> 0 Then GoTo R
        End If

s:      If InStr(1, UCase(a), "#S#") <> 0 Then
            'd = InStr(1, UCase(a), "#S#")
            'sSql = "select * from pay_formula where code='S'"
            'mform = Cn.Execute(sSql)
            'formulalen = Len(mform!Form)
            'fhalf = Mid(a, 1, d - 1)
            'formulalen = d + 3
            'lhalf = Mid(a, formulalen)
            a = selectQuery(a, "S") 'fhalf & "[ " & Trim(mform!Form) & " ]" & lhalf
            If InStr(1, UCase(a), "#S#") <> 0 Then GoTo s
        End If

t:      If InStr(1, UCase(a), "#T#") <> 0 Then
            'd = InStr(1, UCase(a), "#T#")
            'sSql = "select * from pay_formula where code='T'"
            'mform = Cn.Execute(sSql)
            'formulalen = Len(mform!Form)
            'fhalf = Mid(a, 1, d - 1)
            'formulalen = d + 3
            'lhalf = Mid(a, formulalen)
            a = selectQuery(a, "T") 'fhalf & "[ " & Trim(mform!Form) & " ]" & lhalf
            If InStr(1, UCase(a), "#T#") <> 0 Then GoTo t
        End If

u:      If InStr(1, UCase(a), "#U#") <> 0 Then
            'd = InStr(1, UCase(a), "#U#")
            'sSql = "select * from pay_formula where code='U'"
            'mform = Cn.Execute(sSql)
            'formulalen = Len(mform!Form)
            'fhalf = Mid(a, 1, d - 1)
            'formulalen = d + 3
            'lhalf = Mid(a, formulalen)
            a = selectQuery(a, "U") 'fhalf & "[ " & Trim(mform!Form) & " ]" & lhalf
            If InStr(1, UCase(a), "#U#") <> 0 Then GoTo u
        End If

v:      If InStr(1, UCase(a), "#V#") <> 0 Then
            'd = InStr(1, UCase(a), "#V#")
            'sSql = "select * from pay_formula where code='V'"
            'mform = Cn.Execute(sSql)
            'formulalen = Len(mform!Form)
            'fhalf = Mid(a, 1, d - 1)
            'formulalen = d + 3
            'lhalf = Mid(a, formulalen)
            a = selectQuery(a, "V") 'fhalf & "[ " & Trim(mform!Form) & " ]" & lhalf
            If InStr(1, UCase(a), "#V#") <> 0 Then GoTo v
        End If

W:      If InStr(1, UCase(a), "#W#") <> 0 Then
            'd = InStr(1, UCase(a), "#W#")
            'sSql = "select * from pay_formula where code='W'"
            'mform = Cn.Execute(sSql)
            'formulalen = Len(mform!Form)
            'fhalf = Mid(a, 1, d - 1)
            'formulalen = d + 3
            'lhalf = Mid(a, formulalen)
            a = selectQuery(a, "W") 'fhalf & "[ " & Trim(mform!Form) & " ]" & lhalf
            If InStr(1, UCase(a), "#W#") <> 0 Then GoTo W
        End If

X:      If InStr(1, UCase(a), "#X#") <> 0 Then
            'd = InStr(1, UCase(a), "#X#")
            'sSql = "select * from pay_formula where code='X'"
            'mform = Cn.Execute(sSql)
            'formulalen = Len(mform!Form)
            'fhalf = Mid(a, 1, d - 1)
            'formulalen = d + 3
            'lhalf = Mid(a, formulalen)
            a = selectQuery(a, "X") 'fhalf & "[ " & Trim(mform!Form) & " ]" & lhalf
            If InStr(1, UCase(a), "#X#") <> 0 Then GoTo X
        End If

y:      If InStr(1, UCase(a), "#Y#") <> 0 Then
            'd = InStr(1, UCase(a), "#Y#")
            'sSql = "select * from pay_formula where code='Y'"
            'mform = Cn.Execute(sSql)
            'formulalen = Len(mform!Form)
            'fhalf = Mid(a, 1, d - 1)
            'formulalen = d + 3
            'lhalf = Mid(a, formulalen)
            a = selectQuery(a, "Y") ' fhalf & "[ " & Trim(mform!Form) & " ]" & lhalf
            If InStr(1, UCase(a), "#Y#") <> 0 Then GoTo y
        End If

z:      If InStr(1, UCase(a), "#Z#") <> 0 Then
            'd = InStr(1, UCase(a), "#Z#")
            'sSql = "select * from pay_formula where code='Z'"
            'mform = Cn.Execute(sSql)
            'formulalen = Len(mform!Form)
            'fhalf = Mid(a, 1, d - 1)
            'formulalen = d + 3
            'lhalf = Mid(a, formulalen)
            a = selectQuery(a, "Z") 'fhalf & "[ " & Trim(mform!Form) & " ]" & lhalf
            If InStr(1, UCase(a), "#Z#") <> 0 Then GoTo z
        End If

1:      If InStr(1, UCase(a), "BASIC") <> 0 Then
            a = Regex.Replace(a, "\bBASIC\b", Format(BASIC, "000000"))
        End If
2:      If InStr(1, UCase(a), "HRA") <> 0 Then
            a = Regex.Replace(a, "\bHRA\b", Format(HRA, "000000"))
        End If
3:      If InStr(1, UCase(a), "DA") <> 0 Then
            a = Regex.Replace(a, "\bDA\b", Format(DA, "000000"))
        End If
4:      If InStr(1, UCase(a), "PF") <> 0 Then
            a = Regex.Replace(a, "\bPF\b", Format(PF, "00.00"))
        End If
5:      If InStr(1, UCase(a), "FPF") <> 0 Then
            a = Regex.Replace(a, "\bFPF\b", Format(FPF, "00.00"))
        End If
6:      If InStr(1, UCase(a), "ESI") <> 0 Then
            a = Regex.Replace(a, "\bESI\b", Format(ESI, "00.00"))
        End If
7:      If InStr(1, UCase(a), "PRE") <> 0 Then
            a = Regex.Replace(a, "\bPRE\b", Format(PRE, "00.00"))
        End If
8:      If InStr(1, UCase(a), "ABS") <> 0 Then
            a = Regex.Replace(a, "\bABS\b", Format(ABS1, "00.00"))
        End If
9:      If InStr(1, UCase(a), "HLD") <> 0 Then
            a = Regex.Replace(a, "\bHLD\b", Format(HLD, "00.00"))
        End If
10:     If InStr(1, UCase(a), "LATE") <> 0 Then
            a = Regex.Replace(a, "\bLATE\b", Format(LATE, "0000"))
        End If
11:     If InStr(1, UCase(a), "EARLY") <> 0 Then
            a = Regex.Replace(a, "\bEARLY\b", Format(EARLY, "0000"))
        End If
12:     If InStr(1, UCase(a), "O.T.") <> 0 Then
            a = Regex.Replace(a, "\bO.T.\b", Format(OT, "000.00"))
        End If
13:     If InStr(1, UCase(a), "C.L.") <> 0 Then
            a = Regex.Replace(a, "\bC.L.\b", Format(CL, "00.00"))
        End If
14:     If InStr(1, UCase(a), "S.L.") <> 0 Then
            a = Regex.Replace(a, "\bS.L.\b", Format(SL, "00.00"))
        End If
15:     If InStr(1, UCase(a), "PL_EL") <> 0 Then
            a = Regex.Replace(a, "\bPL_EL\b", Format(PL_EL, "00.00"))
        End If
16:     If InStr(1, UCase(a), "OTHER_LV") <> 0 Then
            a = Regex.Replace(a, "\bOTHER_LV\b", Format(OTHER_LV, "00.00"))
        End If
17:     If InStr(1, UCase(a), "LEAVE") <> 0 Then
            a = Regex.Replace(a, "\bLEAVE\b", Format(LEAVE, "00.00"))
        End If
18:     If InStr(1, UCase(a), "TDAYS") <> 0 Then
            a = Regex.Replace(a, "\bTDAYS\b", Format(TDAYS, "00.00"))
        End If
19:     If InStr(1, UCase(a), "T_LATE") <> 0 Then
            a = Regex.Replace(a, "\bT_LATE\b", Format(T_LATE, "000.00"))
        End If
20:     If InStr(1, UCase(a), "T_EARLY") <> 0 Then
            a = Regex.Replace(a, "\bT_EARLY\b", Format(T_EARLY, "000.00"))
        End If
21:     If InStr(1, UCase(a), "EARN_1") <> 0 Then
            a = Regex.Replace(a, "\bEARN_1\b", Format(iern1, "00000.00"))
        End If
211:    If InStr(1, UCase(a), "EARN_2") <> 0 Then
            a = Regex.Replace(a, "\bEARN_2\b", Format(iern2, "00000.00"))
        End If

212:    If InStr(1, UCase(a), "EARN_3") <> 0 Then
            a = Regex.Replace(a, "\bEARN_3\b", Format(iern3, "00000.00"))
        End If

213:    If InStr(1, UCase(a), "EARN_4") <> 0 Then
            a = Regex.Replace(a, "\bEARN_4\b", Format(iern4, "00000.00"))
        End If

214:    If InStr(1, UCase(a), "EARN_5") <> 0 Then
            a = Regex.Replace(a, "\bEARN_5\b", Format(iern5, "00000.00"))
        End If

215:    If InStr(1, UCase(a), "EARN_6") <> 0 Then
            a = Regex.Replace(a, "\bEARN_6\b", Format(iern6, "00000.00"))
        End If

216:    If InStr(1, UCase(a), "EARN_7") <> 0 Then
            a = Regex.Replace(a, "\bEARN_7\b", Format(iern7, "00000.00"))
        End If

217:    If InStr(1, UCase(a), "EARN_8") <> 0 Then
            a = Regex.Replace(a, "\bEARN_8\b", Format(iern8, "00000.00"))
        End If

218:    If InStr(1, UCase(a), "EARN_9") <> 0 Then
            a = Regex.Replace(a, "\bEARN_9\b", Format(iern9, "00000.00"))
        End If

219:    If InStr(1, UCase(a), "EARN_10") <> 0 Then
            a = Regex.Replace(a, "\bEARN_10\b", Format(iern10, "00000.00"))
        End If

221:    If InStr(1, UCase(a), "DEDUCT_1") <> 0 Then
            a = Regex.Replace(a, "\bDEDUCT_1\b", Format(ided1, "00000.00"))
        End If
222:    If InStr(1, UCase(a), "DEDUCT_2") <> 0 Then
            a = Regex.Replace(a, "\bDEDUCT_2\b", Format(ided2, "00000.00"))
        End If

223:    If InStr(1, UCase(a), "DEDUCT_3") <> 0 Then
            a = Regex.Replace(a, "\bDEDUCT_3\b", Format(ided3, "00000.00"))
        End If

224:    If InStr(1, UCase(a), "DEDUCT_4") <> 0 Then
            a = Regex.Replace(a, "\bDEDUCT_4\b", Format(ided4, "00000.00"))
        End If

225:    If InStr(1, UCase(a), "DEDUCT_5") <> 0 Then
            a = Regex.Replace(a, "\bDEDUCT_5\b", Format(ided5, "00000.00"))
        End If

226:    If InStr(1, UCase(a), "DEDUCT_6") <> 0 Then
            a = Regex.Replace(a, "\bDEDUCT_6\b", Format(ided6, "00000.00"))
        End If

227:    If InStr(1, UCase(a), "DEDUCT_7") <> 0 Then
            a = Regex.Replace(a, "\bDEDUCT_7\b", Format(ided7, "00000.00"))
        End If

228:    If InStr(1, UCase(a), "DEDUCT_8") <> 0 Then
            a = Regex.Replace(a, "\bDEDUCT_8\b", Format(ided8, "00000.00"))
        End If

229:    If InStr(1, UCase(a), "DEDUCT_9") <> 0 Then
            a = Regex.Replace(a, "\bDEDUCT_9\b", Format(ided9, "00000.00"))
        End If

230:    If InStr(1, UCase(a), "DEDUCT_10") <> 0 Then
            a = Regex.Replace(a, "\bDEDUCT_10\b", Format(ided10, "00000.00"))
        End If

23:     If InStr(1, UCase(a), "OT_RATE") <> 0 Then
            a = Regex.Replace(a, "\bOT_RATE\b", Format(OT_RATE, "0000.00"))
        End If
24:     If InStr(1, UCase(a), "MON_DAY") <> 0 Then
            a = Regex.Replace(a, "\bMON_DAY\b", Format(MON_DAY, "0000"))
        End If
25:     If InStr(1, UCase(a), "CONV") <> 0 Then
            a = Regex.Replace(a, "\bCONV\b", Format(CONV, "000000"))
        End If
26:     If InStr(1, UCase(a), "MED") <> 0 Then
            a = Regex.Replace(a, "\bMED\b", Format(MED, "000000"))
        End If

27:     If InStr(1, UCase(a), "TDS") <> 0 Then
            a = Regex.Replace(a, "\bTDS\b", Format(TDS, "000000"))
        End If
28:     If InStr(1, UCase(a), "PROF_TAX") <> 0 Then
            a = Regex.Replace(a, "\bPROF_TAX\b", Format(PROF_TAX, "000000"))
        End If

99:     If InStr(1, UCase(a), "ISALARY") <> 0 Then
            a = Regex.Replace(a, "\bISALARY\b", Format(iSalary, "000000.00"))
        End If
98:     If InStr(1, UCase(a), "IOTAMT") <> 0 Then
            a = Regex.Replace(a, "\bIOTAMT\b", Format(iOtAmt, "00000.00"))
        End If
97:     If InStr(1, UCase(a), "IHRAAMT") <> 0 Then
            a = Regex.Replace(a, "\bIHRAAMT\b", Format(iHRAAMT, "000000.00"))
        End If
96:     If InStr(1, UCase(a), "IERNAMT1") <> 0 Then
            a = Regex.Replace(a, "\bIERNAMT1\b", Format(iErnAmt1, "0000000.00"))
        End If
95:     If InStr(1, UCase(a), "IERNAMT2") <> 0 Then
            a = Regex.Replace(a, "\bIERNAMT2\b", Format(iErnAmt2, "0000000.00"))
        End If
94:     If InStr(1, UCase(a), "IERNAMT3") <> 0 Then
            a = Regex.Replace(a, "\bIERNAMT3\b", Format(iErnAmt3, "0000000.00"))
        End If
93:     If InStr(1, UCase(a), "IERNAMT4") <> 0 Then
            a = Regex.Replace(a, "\bIERNAMT4\b", Format(iErnAmt4, "0000000.00"))
        End If
92:     If InStr(1, UCase(a), "IERNAMT5") <> 0 Then
            a = Regex.Replace(a, "\bIERNAMT5\b", Format(iErnAmt5, "0000000.00"))
        End If
91:     If InStr(1, UCase(a), "IERNAMT6") <> 0 Then
            a = Regex.Replace(a, "\bIERNAMT6\b", Format(iErnAmt6, "0000000.00"))
        End If
90:     If InStr(1, UCase(a), "IERNAMT7") <> 0 Then
            a = Regex.Replace(a, "\bIERNAMT7\b", Format(iErnAmt7, "0000000.00"))
        End If
89:     If InStr(1, UCase(a), "IERNAMT8") <> 0 Then
            a = Regex.Replace(a, "\bIERNAMT8\b", Format(iErnAmt8, "0000000.00"))
        End If
87:     If InStr(1, UCase(a), "IERNAMT9") <> 0 Then
            a = Regex.Replace(a, "\bIERNAMT9\b", Format(iErnAmt9, "0000000.00"))
        End If
86:     If InStr(1, UCase(a), "IERNAMT10") <> 0 Then
            a = Regex.Replace(a, "\bIERNAMT10\b", Format(iErnAmt10, "0000000.00"))
        End If
PF:     If InStr(1, UCase(a), "PFMAX") <> 0 Then
            a = Regex.Replace(a, "\bPFMAX\b", Format(g_pfmaxlimit, "0000000"))
        End If
        a1 = ""
        m = 1
        For m = 1 To Len(Trim(a))
            If Mid(Trim(a), m, 1) <> " " Then
                a1 = a1 & Mid(Trim(a), m, 1)
            End If
        Next
333:    If Mid(Trim(a1), 1, 2) = "IF" Then
            lCnt = 0
            Pcnt = 0
            mCtr = 1
            Do While InStr(mCtr, Trim(a1), "IF") <> 0
                lCnt = InStr(mCtr, Trim(a1), "IF")
                mCtr = lCnt + 1
            Loop
            St = Mid(Trim(a1), 1, lCnt - 1)
            If lCnt > 1 Then
                mCtr = lCnt
                Do While InStr(mCtr, Trim(a1), "]") <> 0
                    Pcnt = Pcnt + 1
                    If Pcnt = 2 Then
                        Pcnt = InStr(mCtr, Trim(a1), "]")
                        Exit Do
                    End If
                    mCtr = InStr(mCtr, Trim(a1), "]") + 1
                Loop
                Rt = Mid(Trim(a1), Pcnt + 2, Len(Trim(a1)) - (Pcnt + 1))
                a1 = Mid(Trim(a1), lCnt, Pcnt + 2 - lCnt)
            Else
                Rt = ""
            End If
            mifChk = False
            a = Mid(Trim(a1), InStr(1, Trim(a1), "(") + 1, InStr(1, Trim(a1), ",") - (InStr(1, Trim(a1), "(") + 1))
            mifChk = Mid(a, 1, Len(a)) 'frmCapture.ScriptControl1.Eval(Mid(a, 1, Len(a)))
            a = Mid(Trim(a1), InStr(1, Trim(a1), ",") + 1, Len(Trim(a1)) - (InStr(1, Trim(a1), ",") + 1))
            Tstring = Mid(Trim(a), 2, (InStr(1, Trim(a), ",") - 3))
            Fstring = Mid(Trim(a), (InStr(1, Trim(a), ",") + 2), (Len(Trim(a)) - (InStr(1, Trim(a), ",") + 2)))
            If mifChk Then
                a1 = Tstring
            Else
                a1 = Fstring
            End If
            a = St + a1 + Rt
            a1 = a
            GoTo 333
        End If
        FormulaValueSet = a1

        '        Exit Function
        'ErrHand:
        '        MsgBox(Err.Number & " -> AKFormulaValueSet " & Err.Description, vbCritical)
        '        Resume Next
    End Function

    Private Function selectQuery(a As String, formula As String) As String
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim d As Integer, formulalen As Integer
        Dim fhalf As String
        Dim lhalf As String
        Dim mform As DataSet = New DataSet
        d = InStr(1, UCase(a), "#" & formula & "#")
        Dim sSql As String = "select * from pay_formula where code='" & formula & "'"
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(mform)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(mform)
        End If
        'mform = Cn.Execute(sSql)
        formulalen = Len(mform.Tables(0).Rows(0).Item("Form").ToString.Trim)
        fhalf = Mid(a, 1, d - 1)
        formulalen = d + 3
        lhalf = Mid(a, formulalen)
        a = fhalf & "[ " & mform.Tables(0).Rows(0).Item("Form").ToString.Trim & " ]" & lhalf
    End Function
End Class
