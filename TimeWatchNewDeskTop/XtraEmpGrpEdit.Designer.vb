﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraEmpGrpEdit
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.TabPane1 = New DevExpress.XtraBars.Navigation.TabPane()
        Me.TabNavigationPage1 = New DevExpress.XtraBars.Navigation.TabNavigationPage()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.ToggleSwitch6 = New DevExpress.XtraEditors.ToggleSwitch()
        Me.GroupControl4 = New DevExpress.XtraEditors.GroupControl()
        Me.TextEdit9 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.ToggleSwitch5 = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.GroupControl3 = New DevExpress.XtraEditors.GroupControl()
        Me.CheckEdit7 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit6 = New DevExpress.XtraEditors.CheckEdit()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.CheckEdit5 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit4 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit3 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit2 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit1 = New DevExpress.XtraEditors.CheckEdit()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEdit8 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit7 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit6 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.ToggleSwitch4 = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.ToggleSwitch3 = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.ToggleSwitch2 = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.ToggleSwitch1 = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEdit5 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEdit4 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit1 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEdit3 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit2 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.TabNavigationPage2 = New DevExpress.XtraBars.Navigation.TabNavigationPage()
        Me.GroupControl6 = New DevExpress.XtraEditors.GroupControl()
        Me.GroupControl7 = New DevExpress.XtraEditors.GroupControl()
        Me.CheckEdit12 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit11 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit10 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit9 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit8 = New DevExpress.XtraEditors.CheckEdit()
        Me.ComboBoxEdit4 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboBoxEdit3 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboBoxEdit2 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.GridLookUpEdit2 = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.TblShiftMasterBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SSSDBDataSet = New iAS.SSSDBDataSet()
        Me.GridView3 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn16 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn17 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn18 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn19 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn20 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn21 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn22 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn23 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn24 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn25 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn26 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn27 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn28 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn29 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn30 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.LabelControl26 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl25 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl24 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl23 = New DevExpress.XtraEditors.LabelControl()
        Me.GroupControl5 = New DevExpress.XtraEditors.GroupControl()
        Me.ToggleAllowCmpOffMlt = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl53 = New DevExpress.XtraEditors.LabelControl()
        Me.GridLookUpEditMultiShif = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.GridView6 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn61 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn62 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn63 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn64 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn65 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn66 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn67 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn68 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn69 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn70 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn71 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn72 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn73 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn74 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn75 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.SimpleButton4 = New DevExpress.XtraEditors.SimpleButton()
        Me.GridLookUpEdit4 = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.GridView5 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn46 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn47 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn48 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn49 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn50 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn51 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn52 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn53 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn54 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn55 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn56 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn57 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn58 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn59 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn60 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.SimpleButton3 = New DevExpress.XtraEditors.SimpleButton()
        Me.GridLookUpEdit3 = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.GridView4 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn31 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn32 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn33 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn34 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn35 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn36 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn37 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn38 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn39 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn40 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn41 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn42 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn43 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn44 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn45 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.TextEditRotation = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit11 = New DevExpress.XtraEditors.TextEdit()
        Me.TextEdit10 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl22 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl21 = New DevExpress.XtraEditors.LabelControl()
        Me.PopupContainerEdit2 = New DevExpress.XtraEditors.PopupContainerEdit()
        Me.PopupContainerControl2 = New DevExpress.XtraEditors.PopupContainerControl()
        Me.GridControl2 = New DevExpress.XtraGrid.GridControl()
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTimeEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn8 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn9 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn10 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn11 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn12 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn13 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn14 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn15 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.LabelControl20 = New DevExpress.XtraEditors.LabelControl()
        Me.ToggleSwitchAutoShift = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl19 = New DevExpress.XtraEditors.LabelControl()
        Me.PopupContainerEdit1 = New DevExpress.XtraEditors.PopupContainerEdit()
        Me.PopupContainerControl1 = New DevExpress.XtraEditors.PopupContainerControl()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colSHIFT1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSTARTTIME1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTimeEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit()
        Me.colENDTIME1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLUNCHTIME1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLUNCHDURATION1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLUNCHENDTIME1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colORDERINF121 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colOTSTARTAFTER1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colOTDEDUCTHRS1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLUNCHDEDUCTION1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSHIFTPOSITION1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSHIFTDURATION1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colOTDEDUCTAFTER1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLastModifiedBy1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLastModifiedDate1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.LabelControl18 = New DevExpress.XtraEditors.LabelControl()
        Me.GridLookUpEditDefaltShift = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.GridLookUpEdit1View = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colSHIFT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSTARTTIME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colENDTIME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLUNCHTIME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLUNCHDURATION = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLUNCHENDTIME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colORDERINF12 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colOTSTARTAFTER = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colOTDEDUCTHRS = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLUNCHDEDUCTION = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSHIFTPOSITION = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSHIFTDURATION = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colOTDEDUCTAFTER = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLastModifiedBy = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLastModifiedDate = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl()
        Me.ComboBoxShiftType = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        Me.TabNavigationPage3 = New DevExpress.XtraBars.Navigation.TabNavigationPage()
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl()
        Me.ToggleCmpPOWPOH = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl54 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl35 = New DevExpress.XtraEditors.LabelControl()
        Me.ToggleSwitchMarkHlf = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl39 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl27 = New DevExpress.XtraEditors.LabelControl()
        Me.ToggleOutWorkMins = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl37 = New DevExpress.XtraEditors.LabelControl()
        Me.ToggleWeekOffIncludeInDUtyRoster = New DevExpress.XtraEditors.ToggleSwitch()
        Me.ToggleMarkMisAsAbsent = New DevExpress.XtraEditors.ToggleSwitch()
        Me.ToggleMarkWOasAbsent = New DevExpress.XtraEditors.ToggleSwitch()
        Me.ToggleAutoShiftAllowed = New DevExpress.XtraEditors.ToggleSwitch()
        Me.ToggleIsAutoAbsentAllowed = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl34 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl44 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl45 = New DevExpress.XtraEditors.LabelControl()
        Me.TxtMaxEarlyDeparture = New DevExpress.XtraEditors.TextEdit()
        Me.TxtNoOffpresentOnweekOff = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControlMarkWO = New DevExpress.XtraEditors.LabelControl()
        Me.TxtMaxWorkingMin = New DevExpress.XtraEditors.TextEdit()
        Me.TxtMaxLateArrivalMin = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl47 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl36 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl46 = New DevExpress.XtraEditors.LabelControl()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.TogglePWAasPAA = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl58 = New DevExpress.XtraEditors.LabelControl()
        Me.ToggleAWPasAAP = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl57 = New DevExpress.XtraEditors.LabelControl()
        Me.ToggleWAasAA = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl56 = New DevExpress.XtraEditors.LabelControl()
        Me.ToggleAWasAA = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl55 = New DevExpress.XtraEditors.LabelControl()
        Me.ToggleIsPResentOnHLD = New DevExpress.XtraEditors.ToggleSwitch()
        Me.ToggleIsPresentOnWeekOff = New DevExpress.XtraEditors.ToggleSwitch()
        Me.TxtPermisLateMinAutoShift = New DevExpress.XtraEditors.TextEdit()
        Me.TxtPermisEarlyMinAutoShift = New DevExpress.XtraEditors.TextEdit()
        Me.TxtEndTimeForOutPunch = New DevExpress.XtraEditors.TextEdit()
        Me.TxtInTimeForINPunch = New DevExpress.XtraEditors.TextEdit()
        Me.TxtDuplicateCheck = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl29 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl30 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl31 = New DevExpress.XtraEditors.LabelControl()
        Me.ToggleMarkAWAasAAA = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControlETOP = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl32 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl33 = New DevExpress.XtraEditors.LabelControl()
        Me.ToggleFourPunchNight = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl38 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl40 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl48 = New DevExpress.XtraEditors.LabelControl()
        Me.TabNavigationPage4 = New DevExpress.XtraBars.Navigation.TabNavigationPage()
        Me.GroupControl11 = New DevExpress.XtraEditors.GroupControl()
        Me.TxtOtRestrictDur = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl50 = New DevExpress.XtraEditors.LabelControl()
        Me.TxtOtLateDur = New DevExpress.XtraEditors.TextEdit()
        Me.TxtOtEarlyDur = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl51 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl52 = New DevExpress.XtraEditors.LabelControl()
        Me.GroupControl10 = New DevExpress.XtraEditors.GroupControl()
        Me.TxtDeductOTinWO = New DevExpress.XtraEditors.TextEdit()
        Me.TxtDeductOTinHLD = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl41 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl49 = New DevExpress.XtraEditors.LabelControl()
        Me.GroupControl9 = New DevExpress.XtraEditors.GroupControl()
        Me.ToggleRoundOverTime = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl28 = New DevExpress.XtraEditors.LabelControl()
        Me.ToggleWhetherOTinMinus = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl43 = New DevExpress.XtraEditors.LabelControl()
        Me.ToggleOTisAllowedInEarlyComing = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl42 = New DevExpress.XtraEditors.LabelControl()
        Me.GroupControl8 = New DevExpress.XtraEditors.GroupControl()
        Me.CheckEditOT3 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditOT2 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditOT1 = New DevExpress.XtraEditors.CheckEdit()
        Me.SimpleButtonSave = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButtonCancel = New DevExpress.XtraEditors.SimpleButton()
        Me.TblShiftMasterTableAdapter = New iAS.SSSDBDataSetTableAdapters.tblShiftMasterTableAdapter()
        Me.TblShiftMaster1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.tblShiftMaster1TableAdapter()
        CType(Me.TabPane1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPane1.SuspendLayout()
        Me.TabNavigationPage1.SuspendLayout()
        CType(Me.ToggleSwitch6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl4.SuspendLayout()
        CType(Me.TextEdit9.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleSwitch5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.CheckEdit7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEdit6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.CheckEdit5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEdit4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEdit3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.TextEdit8.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit7.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit6.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleSwitch4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleSwitch3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleSwitch2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleSwitch1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabNavigationPage2.SuspendLayout()
        CType(Me.GroupControl6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl6.SuspendLayout()
        CType(Me.GroupControl7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl7.SuspendLayout()
        CType(Me.CheckEdit12.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEdit11.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEdit10.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEdit9.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEdit8.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEdit4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEdit3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblShiftMasterBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl5.SuspendLayout()
        CType(Me.ToggleAllowCmpOffMlt.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEditMultiShif.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEdit4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEdit3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditRotation.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit11.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit10.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PopupContainerControl2.SuspendLayout()
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTimeEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleSwitchAutoShift.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PopupContainerControl1.SuspendLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTimeEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEditDefaltShift.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEdit1View, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxShiftType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabNavigationPage3.SuspendLayout()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl2.SuspendLayout()
        CType(Me.ToggleCmpPOWPOH.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleSwitchMarkHlf.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleOutWorkMins.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleWeekOffIncludeInDUtyRoster.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleMarkMisAsAbsent.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleMarkWOasAbsent.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleAutoShiftAllowed.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleIsAutoAbsentAllowed.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtMaxEarlyDeparture.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtNoOffpresentOnweekOff.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtMaxWorkingMin.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtMaxLateArrivalMin.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.TogglePWAasPAA.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleAWPasAAP.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleWAasAA.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleAWasAA.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleIsPResentOnHLD.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleIsPresentOnWeekOff.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtPermisLateMinAutoShift.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtPermisEarlyMinAutoShift.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtEndTimeForOutPunch.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtInTimeForINPunch.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtDuplicateCheck.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleMarkAWAasAAA.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleFourPunchNight.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabNavigationPage4.SuspendLayout()
        CType(Me.GroupControl11, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl11.SuspendLayout()
        CType(Me.TxtOtRestrictDur.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtOtLateDur.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtOtEarlyDur.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl10, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl10.SuspendLayout()
        CType(Me.TxtDeductOTinWO.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TxtDeductOTinHLD.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl9, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl9.SuspendLayout()
        CType(Me.ToggleRoundOverTime.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleWhetherOTinMinus.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleOTisAllowedInEarlyComing.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl8, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl8.SuspendLayout()
        CType(Me.CheckEditOT3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditOT2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditOT1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TabPane1
        '
        Me.TabPane1.AllowTransitionAnimation = DevExpress.Utils.DefaultBoolean.[True]
        Me.TabPane1.Appearance.FontStyleDelta = System.Drawing.FontStyle.Italic
        Me.TabPane1.Appearance.Options.UseFont = True
        Me.TabPane1.AppearanceButton.Hovered.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.TabPane1.AppearanceButton.Hovered.Options.UseFont = True
        Me.TabPane1.AppearanceButton.Normal.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TabPane1.AppearanceButton.Normal.Options.UseFont = True
        Me.TabPane1.AppearanceButton.Pressed.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TabPane1.AppearanceButton.Pressed.Options.UseFont = True
        Me.TabPane1.Controls.Add(Me.TabNavigationPage1)
        Me.TabPane1.Controls.Add(Me.TabNavigationPage2)
        Me.TabPane1.Controls.Add(Me.TabNavigationPage3)
        Me.TabPane1.Controls.Add(Me.TabNavigationPage4)
        Me.TabPane1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabPane1.Location = New System.Drawing.Point(0, 0)
        Me.TabPane1.LookAndFeel.SkinName = "iMaginary"
        Me.TabPane1.Name = "TabPane1"
        Me.TabPane1.Pages.AddRange(New DevExpress.XtraBars.Navigation.NavigationPageBase() {Me.TabNavigationPage1, Me.TabNavigationPage2, Me.TabNavigationPage3, Me.TabNavigationPage4})
        Me.TabPane1.RegularSize = New System.Drawing.Size(724, 511)
        Me.TabPane1.SelectedPage = Me.TabNavigationPage1
        Me.TabPane1.Size = New System.Drawing.Size(724, 511)
        Me.TabPane1.TabIndex = 0
        Me.TabPane1.Text = "TabPane1"
        Me.TabPane1.TransitionType = DevExpress.Utils.Animation.Transitions.Cover
        '
        'TabNavigationPage1
        '
        Me.TabNavigationPage1.Caption = "General           "
        Me.TabNavigationPage1.Controls.Add(Me.LabelControl15)
        Me.TabNavigationPage1.Controls.Add(Me.ToggleSwitch6)
        Me.TabNavigationPage1.Controls.Add(Me.GroupControl4)
        Me.TabNavigationPage1.Controls.Add(Me.GroupControl3)
        Me.TabNavigationPage1.Controls.Add(Me.GroupControl2)
        Me.TabNavigationPage1.Controls.Add(Me.GroupControl1)
        Me.TabNavigationPage1.Name = "TabNavigationPage1"
        Me.TabNavigationPage1.Size = New System.Drawing.Size(704, 469)
        '
        'LabelControl15
        '
        Me.LabelControl15.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl15.Appearance.Options.UseFont = True
        Me.LabelControl15.Location = New System.Drawing.Point(376, 377)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(106, 14)
        Me.LabelControl15.TabIndex = 14
        Me.LabelControl15.Text = "Overstay Applicable"
        '
        'ToggleSwitch6
        '
        Me.ToggleSwitch6.Location = New System.Drawing.Point(505, 371)
        Me.ToggleSwitch6.Name = "ToggleSwitch6"
        Me.ToggleSwitch6.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleSwitch6.Properties.Appearance.Options.UseFont = True
        Me.ToggleSwitch6.Properties.OffText = "No"
        Me.ToggleSwitch6.Properties.OnText = "Yes"
        Me.ToggleSwitch6.Size = New System.Drawing.Size(95, 25)
        Me.ToggleSwitch6.TabIndex = 5
        '
        'GroupControl4
        '
        Me.GroupControl4.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GroupControl4.AppearanceCaption.Options.UseFont = True
        Me.GroupControl4.Controls.Add(Me.TextEdit9)
        Me.GroupControl4.Controls.Add(Me.LabelControl14)
        Me.GroupControl4.Controls.Add(Me.ToggleSwitch5)
        Me.GroupControl4.Controls.Add(Me.LabelControl13)
        Me.GroupControl4.Location = New System.Drawing.Point(369, 265)
        Me.GroupControl4.Name = "GroupControl4"
        Me.GroupControl4.Size = New System.Drawing.Size(231, 100)
        Me.GroupControl4.TabIndex = 4
        Me.GroupControl4.Text = "Overtime"
        '
        'TextEdit9
        '
        Me.TextEdit9.EditValue = "000.00"
        Me.TextEdit9.Location = New System.Drawing.Point(155, 61)
        Me.TextEdit9.Name = "TextEdit9"
        Me.TextEdit9.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEdit9.Properties.Appearance.Options.UseFont = True
        Me.TextEdit9.Properties.Mask.EditMask = "###.##"
        Me.TextEdit9.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEdit9.Properties.MaxLength = 6
        Me.TextEdit9.Size = New System.Drawing.Size(69, 20)
        Me.TextEdit9.TabIndex = 3
        '
        'LabelControl14
        '
        Me.LabelControl14.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl14.Appearance.Options.UseFont = True
        Me.LabelControl14.Location = New System.Drawing.Point(6, 63)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(131, 14)
        Me.LabelControl14.TabIndex = 2
        Me.LabelControl14.Text = "Overtime Rate Per Hour"
        '
        'ToggleSwitch5
        '
        Me.ToggleSwitch5.Location = New System.Drawing.Point(133, 25)
        Me.ToggleSwitch5.Name = "ToggleSwitch5"
        Me.ToggleSwitch5.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleSwitch5.Properties.Appearance.Options.UseFont = True
        Me.ToggleSwitch5.Properties.OffText = "No"
        Me.ToggleSwitch5.Properties.OnText = "Yes"
        Me.ToggleSwitch5.Size = New System.Drawing.Size(95, 25)
        Me.ToggleSwitch5.TabIndex = 1
        '
        'LabelControl13
        '
        Me.LabelControl13.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl13.Appearance.Options.UseFont = True
        Me.LabelControl13.Location = New System.Drawing.Point(6, 30)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(108, 14)
        Me.LabelControl13.TabIndex = 0
        Me.LabelControl13.Text = "Overtime Applicable"
        '
        'GroupControl3
        '
        Me.GroupControl3.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GroupControl3.AppearanceCaption.Options.UseFont = True
        Me.GroupControl3.Controls.Add(Me.CheckEdit7)
        Me.GroupControl3.Controls.Add(Me.CheckEdit6)
        Me.GroupControl3.Location = New System.Drawing.Point(369, 175)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(231, 80)
        Me.GroupControl3.TabIndex = 3
        Me.GroupControl3.Text = "Single Punch Only"
        '
        'CheckEdit7
        '
        Me.CheckEdit7.EditValue = True
        Me.CheckEdit7.Location = New System.Drawing.Point(6, 47)
        Me.CheckEdit7.Name = "CheckEdit7"
        Me.CheckEdit7.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEdit7.Properties.Appearance.Options.UseFont = True
        Me.CheckEdit7.Properties.Caption = "Overwrite"
        Me.CheckEdit7.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEdit7.Properties.RadioGroupIndex = 1
        Me.CheckEdit7.Size = New System.Drawing.Size(130, 19)
        Me.CheckEdit7.TabIndex = 1
        '
        'CheckEdit6
        '
        Me.CheckEdit6.Location = New System.Drawing.Point(6, 22)
        Me.CheckEdit6.Name = "CheckEdit6"
        Me.CheckEdit6.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEdit6.Properties.Appearance.Options.UseFont = True
        Me.CheckEdit6.Properties.Caption = "Fixed Out Time"
        Me.CheckEdit6.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEdit6.Properties.RadioGroupIndex = 1
        Me.CheckEdit6.Size = New System.Drawing.Size(130, 19)
        Me.CheckEdit6.TabIndex = 0
        Me.CheckEdit6.TabStop = False
        '
        'GroupControl2
        '
        Me.GroupControl2.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GroupControl2.AppearanceCaption.Options.UseFont = True
        Me.GroupControl2.Controls.Add(Me.CheckEdit5)
        Me.GroupControl2.Controls.Add(Me.CheckEdit4)
        Me.GroupControl2.Controls.Add(Me.CheckEdit3)
        Me.GroupControl2.Controls.Add(Me.CheckEdit2)
        Me.GroupControl2.Controls.Add(Me.CheckEdit1)
        Me.GroupControl2.Location = New System.Drawing.Point(369, 4)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(231, 161)
        Me.GroupControl2.TabIndex = 2
        Me.GroupControl2.Text = "Punches Required in a Day"
        '
        'CheckEdit5
        '
        Me.CheckEdit5.Location = New System.Drawing.Point(6, 126)
        Me.CheckEdit5.Name = "CheckEdit5"
        Me.CheckEdit5.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEdit5.Properties.Appearance.Options.UseFont = True
        Me.CheckEdit5.Properties.Caption = "Multiple Punch"
        Me.CheckEdit5.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEdit5.Properties.RadioGroupIndex = 0
        Me.CheckEdit5.Size = New System.Drawing.Size(130, 19)
        Me.CheckEdit5.TabIndex = 4
        Me.CheckEdit5.TabStop = False
        '
        'CheckEdit4
        '
        Me.CheckEdit4.EditValue = True
        Me.CheckEdit4.Location = New System.Drawing.Point(6, 101)
        Me.CheckEdit4.Name = "CheckEdit4"
        Me.CheckEdit4.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEdit4.Properties.Appearance.Options.UseFont = True
        Me.CheckEdit4.Properties.Caption = "Four Punches"
        Me.CheckEdit4.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEdit4.Properties.RadioGroupIndex = 0
        Me.CheckEdit4.Size = New System.Drawing.Size(130, 19)
        Me.CheckEdit4.TabIndex = 3
        '
        'CheckEdit3
        '
        Me.CheckEdit3.Location = New System.Drawing.Point(6, 76)
        Me.CheckEdit3.Name = "CheckEdit3"
        Me.CheckEdit3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEdit3.Properties.Appearance.Options.UseFont = True
        Me.CheckEdit3.Properties.Caption = "Two Punches"
        Me.CheckEdit3.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEdit3.Properties.RadioGroupIndex = 0
        Me.CheckEdit3.Size = New System.Drawing.Size(130, 19)
        Me.CheckEdit3.TabIndex = 2
        Me.CheckEdit3.TabStop = False
        '
        'CheckEdit2
        '
        Me.CheckEdit2.Location = New System.Drawing.Point(6, 51)
        Me.CheckEdit2.Name = "CheckEdit2"
        Me.CheckEdit2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEdit2.Properties.Appearance.Options.UseFont = True
        Me.CheckEdit2.Properties.Caption = "Single Punch Only"
        Me.CheckEdit2.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEdit2.Properties.RadioGroupIndex = 0
        Me.CheckEdit2.Size = New System.Drawing.Size(130, 19)
        Me.CheckEdit2.TabIndex = 1
        Me.CheckEdit2.TabStop = False
        '
        'CheckEdit1
        '
        Me.CheckEdit1.Location = New System.Drawing.Point(6, 26)
        Me.CheckEdit1.Name = "CheckEdit1"
        Me.CheckEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEdit1.Properties.Appearance.Options.UseFont = True
        Me.CheckEdit1.Properties.Caption = "No Punch "
        Me.CheckEdit1.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEdit1.Properties.RadioGroupIndex = 0
        Me.CheckEdit1.Size = New System.Drawing.Size(130, 19)
        Me.CheckEdit1.TabIndex = 0
        Me.CheckEdit1.TabStop = False
        '
        'GroupControl1
        '
        Me.GroupControl1.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GroupControl1.AppearanceCaption.Options.UseFont = True
        Me.GroupControl1.Controls.Add(Me.LabelControl12)
        Me.GroupControl1.Controls.Add(Me.LabelControl11)
        Me.GroupControl1.Controls.Add(Me.TextEdit8)
        Me.GroupControl1.Controls.Add(Me.TextEdit7)
        Me.GroupControl1.Controls.Add(Me.TextEdit6)
        Me.GroupControl1.Controls.Add(Me.LabelControl10)
        Me.GroupControl1.Controls.Add(Me.ToggleSwitch4)
        Me.GroupControl1.Controls.Add(Me.LabelControl9)
        Me.GroupControl1.Controls.Add(Me.ToggleSwitch3)
        Me.GroupControl1.Controls.Add(Me.LabelControl8)
        Me.GroupControl1.Controls.Add(Me.ToggleSwitch2)
        Me.GroupControl1.Controls.Add(Me.LabelControl7)
        Me.GroupControl1.Controls.Add(Me.ToggleSwitch1)
        Me.GroupControl1.Controls.Add(Me.LabelControl6)
        Me.GroupControl1.Controls.Add(Me.TextEdit5)
        Me.GroupControl1.Controls.Add(Me.LabelControl5)
        Me.GroupControl1.Controls.Add(Me.LabelControl1)
        Me.GroupControl1.Controls.Add(Me.TextEdit4)
        Me.GroupControl1.Controls.Add(Me.TextEdit1)
        Me.GroupControl1.Controls.Add(Me.LabelControl4)
        Me.GroupControl1.Controls.Add(Me.LabelControl2)
        Me.GroupControl1.Controls.Add(Me.TextEdit3)
        Me.GroupControl1.Controls.Add(Me.TextEdit2)
        Me.GroupControl1.Controls.Add(Me.LabelControl3)
        Me.GroupControl1.Location = New System.Drawing.Point(3, 3)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(343, 406)
        Me.GroupControl1.TabIndex = 1
        Me.GroupControl1.Text = "General"
        '
        'LabelControl12
        '
        Me.LabelControl12.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl12.Appearance.Options.UseFont = True
        Me.LabelControl12.Location = New System.Drawing.Point(6, 347)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(210, 14)
        Me.LabelControl12.TabIndex = 24
        Me.LabelControl12.Text = "Maximum Working Hours for Short day"
        '
        'LabelControl11
        '
        Me.LabelControl11.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl11.Appearance.Options.UseFont = True
        Me.LabelControl11.Location = New System.Drawing.Point(6, 320)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(199, 14)
        Me.LabelControl11.TabIndex = 23
        Me.LabelControl11.Text = "Maximum Working Hours for half day"
        '
        'TextEdit8
        '
        Me.TextEdit8.EditValue = "00:00"
        Me.TextEdit8.Location = New System.Drawing.Point(235, 346)
        Me.TextEdit8.Name = "TextEdit8"
        Me.TextEdit8.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEdit8.Properties.Appearance.Options.UseFont = True
        Me.TextEdit8.Properties.Mask.EditMask = "HH:mm"
        Me.TextEdit8.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TextEdit8.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEdit8.Properties.MaxLength = 5
        Me.TextEdit8.Size = New System.Drawing.Size(72, 20)
        Me.TextEdit8.TabIndex = 12
        '
        'TextEdit7
        '
        Me.TextEdit7.EditValue = "00:00"
        Me.TextEdit7.Location = New System.Drawing.Point(235, 319)
        Me.TextEdit7.Name = "TextEdit7"
        Me.TextEdit7.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEdit7.Properties.Appearance.Options.UseFont = True
        Me.TextEdit7.Properties.Mask.EditMask = "HH:mm"
        Me.TextEdit7.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TextEdit7.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEdit7.Properties.MaxLength = 5
        Me.TextEdit7.Size = New System.Drawing.Size(72, 20)
        Me.TextEdit7.TabIndex = 11
        '
        'TextEdit6
        '
        Me.TextEdit6.EditValue = "04:00"
        Me.TextEdit6.Location = New System.Drawing.Point(235, 290)
        Me.TextEdit6.Name = "TextEdit6"
        Me.TextEdit6.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEdit6.Properties.Appearance.Options.UseFont = True
        Me.TextEdit6.Properties.Mask.EditMask = "HH:mm"
        Me.TextEdit6.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TextEdit6.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEdit6.Properties.MaxLength = 5
        Me.TextEdit6.Size = New System.Drawing.Size(72, 20)
        Me.TextEdit6.TabIndex = 10
        '
        'LabelControl10
        '
        Me.LabelControl10.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl10.Appearance.Options.UseFont = True
        Me.LabelControl10.Location = New System.Drawing.Point(5, 292)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(137, 14)
        Me.LabelControl10.TabIndex = 19
        Me.LabelControl10.Text = "Present Marking Duration"
        '
        'ToggleSwitch4
        '
        Me.ToggleSwitch4.Location = New System.Drawing.Point(212, 259)
        Me.ToggleSwitch4.Name = "ToggleSwitch4"
        Me.ToggleSwitch4.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleSwitch4.Properties.Appearance.Options.UseFont = True
        Me.ToggleSwitch4.Properties.OffText = "Off"
        Me.ToggleSwitch4.Properties.OnText = "On"
        Me.ToggleSwitch4.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ToggleSwitch4.Size = New System.Drawing.Size(95, 25)
        Me.ToggleSwitch4.TabIndex = 9
        '
        'LabelControl9
        '
        Me.LabelControl9.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl9.Appearance.Options.UseFont = True
        Me.LabelControl9.Location = New System.Drawing.Point(5, 265)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(108, 14)
        Me.LabelControl9.TabIndex = 17
        Me.LabelControl9.Text = "Short leave marking"
        '
        'ToggleSwitch3
        '
        Me.ToggleSwitch3.Location = New System.Drawing.Point(212, 231)
        Me.ToggleSwitch3.Name = "ToggleSwitch3"
        Me.ToggleSwitch3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleSwitch3.Properties.Appearance.Options.UseFont = True
        Me.ToggleSwitch3.Properties.OffText = "Off"
        Me.ToggleSwitch3.Properties.OnText = "On"
        Me.ToggleSwitch3.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ToggleSwitch3.Size = New System.Drawing.Size(95, 25)
        Me.ToggleSwitch3.TabIndex = 8
        '
        'LabelControl8
        '
        Me.LabelControl8.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl8.Appearance.Options.UseFont = True
        Me.LabelControl8.Location = New System.Drawing.Point(6, 234)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(90, 14)
        Me.LabelControl8.TabIndex = 15
        Me.LabelControl8.Text = "Half Day marking"
        '
        'ToggleSwitch2
        '
        Me.ToggleSwitch2.EditValue = True
        Me.ToggleSwitch2.Location = New System.Drawing.Point(212, 201)
        Me.ToggleSwitch2.Name = "ToggleSwitch2"
        Me.ToggleSwitch2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleSwitch2.Properties.Appearance.Options.UseFont = True
        Me.ToggleSwitch2.Properties.OffText = "Off"
        Me.ToggleSwitch2.Properties.OnText = "On"
        Me.ToggleSwitch2.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ToggleSwitch2.Size = New System.Drawing.Size(95, 25)
        Me.ToggleSwitch2.TabIndex = 7
        '
        'LabelControl7
        '
        Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl7.Appearance.Options.UseFont = True
        Me.LabelControl7.Location = New System.Drawing.Point(5, 204)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(108, 14)
        Me.LabelControl7.TabIndex = 13
        Me.LabelControl7.Text = "Consider Time Loss "
        '
        'ToggleSwitch1
        '
        Me.ToggleSwitch1.Location = New System.Drawing.Point(213, 171)
        Me.ToggleSwitch1.Name = "ToggleSwitch1"
        Me.ToggleSwitch1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleSwitch1.Properties.Appearance.Options.UseFont = True
        Me.ToggleSwitch1.Properties.OffText = "Off"
        Me.ToggleSwitch1.Properties.OnText = "On"
        Me.ToggleSwitch1.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.ToggleSwitch1.Size = New System.Drawing.Size(95, 25)
        Me.ToggleSwitch1.TabIndex = 6
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl6.Appearance.Options.UseFont = True
        Me.LabelControl6.Location = New System.Drawing.Point(5, 175)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(142, 14)
        Me.LabelControl6.TabIndex = 11
        Me.LabelControl6.Text = "Round The Clock Working"
        '
        'TextEdit5
        '
        Me.TextEdit5.EditValue = "23:59"
        Me.TextEdit5.Location = New System.Drawing.Point(233, 145)
        Me.TextEdit5.Name = "TextEdit5"
        Me.TextEdit5.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEdit5.Properties.Appearance.Options.UseFont = True
        Me.TextEdit5.Properties.Mask.EditMask = "HH:mm"
        Me.TextEdit5.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TextEdit5.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEdit5.Properties.MaxLength = 5
        Me.TextEdit5.Size = New System.Drawing.Size(72, 20)
        Me.TextEdit5.TabIndex = 5
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl5.Appearance.Options.UseFont = True
        Me.LabelControl5.Location = New System.Drawing.Point(5, 148)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(181, 14)
        Me.LabelControl5.TabIndex = 9
        Me.LabelControl5.Text = "Maximum Working Hours in a Day"
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Location = New System.Drawing.Point(5, 42)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(59, 14)
        Me.LabelControl1.TabIndex = 0
        Me.LabelControl1.Text = "Group Id *"
        '
        'TextEdit4
        '
        Me.TextEdit4.EditValue = "00:10"
        Me.TextEdit4.Location = New System.Drawing.Point(233, 119)
        Me.TextEdit4.Name = "TextEdit4"
        Me.TextEdit4.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEdit4.Properties.Appearance.Options.UseFont = True
        Me.TextEdit4.Properties.Mask.EditMask = "HH:mm"
        Me.TextEdit4.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TextEdit4.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEdit4.Properties.MaxLength = 5
        Me.TextEdit4.Size = New System.Drawing.Size(72, 20)
        Me.TextEdit4.TabIndex = 4
        '
        'TextEdit1
        '
        Me.TextEdit1.Location = New System.Drawing.Point(204, 39)
        Me.TextEdit1.Name = "TextEdit1"
        Me.TextEdit1.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextEdit1.Properties.Mask.EditMask = "[A-Z0-9]*"
        Me.TextEdit1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEdit1.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEdit1.Properties.MaxLength = 3
        Me.TextEdit1.Size = New System.Drawing.Size(100, 20)
        Me.TextEdit1.TabIndex = 1
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl4.Appearance.Options.UseFont = True
        Me.LabelControl4.Location = New System.Drawing.Point(6, 122)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(146, 14)
        Me.LabelControl4.TabIndex = 7
        Me.LabelControl4.Text = "Permissible Early Departure"
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl2.Appearance.Options.UseFont = True
        Me.LabelControl2.Location = New System.Drawing.Point(5, 68)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(79, 14)
        Me.LabelControl2.TabIndex = 2
        Me.LabelControl2.Text = "Group Name *"
        '
        'TextEdit3
        '
        Me.TextEdit3.EditValue = "00:10"
        Me.TextEdit3.Location = New System.Drawing.Point(232, 91)
        Me.TextEdit3.Name = "TextEdit3"
        Me.TextEdit3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEdit3.Properties.Appearance.Options.UseFont = True
        Me.TextEdit3.Properties.Mask.EditMask = "HH:mm"
        Me.TextEdit3.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TextEdit3.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEdit3.Properties.MaxLength = 5
        Me.TextEdit3.Size = New System.Drawing.Size(72, 20)
        Me.TextEdit3.TabIndex = 3
        '
        'TextEdit2
        '
        Me.TextEdit2.Location = New System.Drawing.Point(205, 65)
        Me.TextEdit2.Name = "TextEdit2"
        Me.TextEdit2.Properties.Mask.EditMask = "[A-Z0-9]*"
        Me.TextEdit2.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEdit2.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEdit2.Properties.MaxLength = 30
        Me.TextEdit2.Size = New System.Drawing.Size(100, 20)
        Me.TextEdit2.TabIndex = 2
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl3.Appearance.Options.UseFont = True
        Me.LabelControl3.Location = New System.Drawing.Point(6, 94)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(122, 14)
        Me.LabelControl3.TabIndex = 4
        Me.LabelControl3.Text = "Permissible Late Arrival"
        '
        'TabNavigationPage2
        '
        Me.TabNavigationPage2.Caption = "Shift             "
        Me.TabNavigationPage2.Controls.Add(Me.GroupControl6)
        Me.TabNavigationPage2.Controls.Add(Me.GroupControl5)
        Me.TabNavigationPage2.Name = "TabNavigationPage2"
        Me.TabNavigationPage2.Size = New System.Drawing.Size(711, 464)
        '
        'GroupControl6
        '
        Me.GroupControl6.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.GroupControl6.AppearanceCaption.Options.UseFont = True
        Me.GroupControl6.Controls.Add(Me.GroupControl7)
        Me.GroupControl6.Controls.Add(Me.ComboBoxEdit4)
        Me.GroupControl6.Controls.Add(Me.ComboBoxEdit3)
        Me.GroupControl6.Controls.Add(Me.ComboBoxEdit2)
        Me.GroupControl6.Controls.Add(Me.GridLookUpEdit2)
        Me.GroupControl6.Controls.Add(Me.LabelControl26)
        Me.GroupControl6.Controls.Add(Me.LabelControl25)
        Me.GroupControl6.Controls.Add(Me.LabelControl24)
        Me.GroupControl6.Controls.Add(Me.LabelControl23)
        Me.GroupControl6.Location = New System.Drawing.Point(382, 4)
        Me.GroupControl6.Name = "GroupControl6"
        Me.GroupControl6.Size = New System.Drawing.Size(300, 380)
        Me.GroupControl6.TabIndex = 1
        Me.GroupControl6.Text = "Weekly Off Details"
        '
        'GroupControl7
        '
        Me.GroupControl7.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GroupControl7.AppearanceCaption.Options.UseFont = True
        Me.GroupControl7.Controls.Add(Me.CheckEdit12)
        Me.GroupControl7.Controls.Add(Me.CheckEdit11)
        Me.GroupControl7.Controls.Add(Me.CheckEdit10)
        Me.GroupControl7.Controls.Add(Me.CheckEdit9)
        Me.GroupControl7.Controls.Add(Me.CheckEdit8)
        Me.GroupControl7.Location = New System.Drawing.Point(10, 158)
        Me.GroupControl7.Name = "GroupControl7"
        Me.GroupControl7.Size = New System.Drawing.Size(269, 194)
        Me.GroupControl7.TabIndex = 5
        Me.GroupControl7.Text = "Second Weekly Off Days"
        '
        'CheckEdit12
        '
        Me.CheckEdit12.Location = New System.Drawing.Point(5, 131)
        Me.CheckEdit12.Name = "CheckEdit12"
        Me.CheckEdit12.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEdit12.Properties.Appearance.Options.UseFont = True
        Me.CheckEdit12.Properties.Caption = "V"
        Me.CheckEdit12.Size = New System.Drawing.Size(250, 19)
        Me.CheckEdit12.TabIndex = 4
        '
        'CheckEdit11
        '
        Me.CheckEdit11.Location = New System.Drawing.Point(5, 106)
        Me.CheckEdit11.Name = "CheckEdit11"
        Me.CheckEdit11.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEdit11.Properties.Appearance.Options.UseFont = True
        Me.CheckEdit11.Properties.Caption = "IV"
        Me.CheckEdit11.Size = New System.Drawing.Size(250, 19)
        Me.CheckEdit11.TabIndex = 3
        '
        'CheckEdit10
        '
        Me.CheckEdit10.Location = New System.Drawing.Point(6, 81)
        Me.CheckEdit10.Name = "CheckEdit10"
        Me.CheckEdit10.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEdit10.Properties.Appearance.Options.UseFont = True
        Me.CheckEdit10.Properties.Caption = "III"
        Me.CheckEdit10.Size = New System.Drawing.Size(250, 19)
        Me.CheckEdit10.TabIndex = 2
        '
        'CheckEdit9
        '
        Me.CheckEdit9.Location = New System.Drawing.Point(6, 56)
        Me.CheckEdit9.Name = "CheckEdit9"
        Me.CheckEdit9.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEdit9.Properties.Appearance.Options.UseFont = True
        Me.CheckEdit9.Properties.Caption = "II"
        Me.CheckEdit9.Size = New System.Drawing.Size(250, 19)
        Me.CheckEdit9.TabIndex = 1
        '
        'CheckEdit8
        '
        Me.CheckEdit8.Location = New System.Drawing.Point(6, 31)
        Me.CheckEdit8.Name = "CheckEdit8"
        Me.CheckEdit8.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEdit8.Properties.Appearance.Options.UseFont = True
        Me.CheckEdit8.Properties.Caption = "I"
        Me.CheckEdit8.Size = New System.Drawing.Size(250, 19)
        Me.CheckEdit8.TabIndex = 0
        '
        'ComboBoxEdit4
        '
        Me.ComboBoxEdit4.EditValue = "Full"
        Me.ComboBoxEdit4.Location = New System.Drawing.Point(159, 94)
        Me.ComboBoxEdit4.Name = "ComboBoxEdit4"
        Me.ComboBoxEdit4.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEdit4.Properties.Appearance.Options.UseFont = True
        Me.ComboBoxEdit4.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEdit4.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboBoxEdit4.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEdit4.Properties.AppearanceFocused.Options.UseFont = True
        Me.ComboBoxEdit4.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEdit4.Properties.Items.AddRange(New Object() {"Full", "Half"})
        Me.ComboBoxEdit4.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.ComboBoxEdit4.Size = New System.Drawing.Size(120, 20)
        Me.ComboBoxEdit4.TabIndex = 3
        '
        'ComboBoxEdit3
        '
        Me.ComboBoxEdit3.EditValue = "NONE"
        Me.ComboBoxEdit3.Location = New System.Drawing.Point(159, 63)
        Me.ComboBoxEdit3.Name = "ComboBoxEdit3"
        Me.ComboBoxEdit3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEdit3.Properties.Appearance.Options.UseFont = True
        Me.ComboBoxEdit3.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEdit3.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboBoxEdit3.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEdit3.Properties.AppearanceFocused.Options.UseFont = True
        Me.ComboBoxEdit3.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEdit3.Properties.DropDownRows = 8
        Me.ComboBoxEdit3.Properties.Items.AddRange(New Object() {"NONE", "SUNDAY", "MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY"})
        Me.ComboBoxEdit3.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.ComboBoxEdit3.Size = New System.Drawing.Size(120, 20)
        Me.ComboBoxEdit3.TabIndex = 2
        '
        'ComboBoxEdit2
        '
        Me.ComboBoxEdit2.EditValue = "SUNDAY"
        Me.ComboBoxEdit2.Location = New System.Drawing.Point(159, 34)
        Me.ComboBoxEdit2.Name = "ComboBoxEdit2"
        Me.ComboBoxEdit2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEdit2.Properties.Appearance.Options.UseFont = True
        Me.ComboBoxEdit2.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEdit2.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboBoxEdit2.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEdit2.Properties.AppearanceFocused.Options.UseFont = True
        Me.ComboBoxEdit2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEdit2.Properties.DropDownRows = 8
        Me.ComboBoxEdit2.Properties.Items.AddRange(New Object() {"NONE", "SUNDAY", "MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY"})
        Me.ComboBoxEdit2.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.ComboBoxEdit2.Size = New System.Drawing.Size(120, 20)
        Me.ComboBoxEdit2.TabIndex = 1
        '
        'GridLookUpEdit2
        '
        Me.GridLookUpEdit2.EditValue = ""
        Me.GridLookUpEdit2.Location = New System.Drawing.Point(159, 124)
        Me.GridLookUpEdit2.Name = "GridLookUpEdit2"
        Me.GridLookUpEdit2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEdit2.Properties.Appearance.Options.UseFont = True
        Me.GridLookUpEdit2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GridLookUpEdit2.Properties.DataSource = Me.TblShiftMasterBindingSource
        Me.GridLookUpEdit2.Properties.DisplayMember = "SHIFT"
        Me.GridLookUpEdit2.Properties.ValueMember = "SHIFT"
        Me.GridLookUpEdit2.Properties.View = Me.GridView3
        Me.GridLookUpEdit2.Size = New System.Drawing.Size(120, 20)
        Me.GridLookUpEdit2.TabIndex = 4
        '
        'TblShiftMasterBindingSource
        '
        Me.TblShiftMasterBindingSource.DataMember = "tblShiftMaster"
        Me.TblShiftMasterBindingSource.DataSource = Me.SSSDBDataSet
        '
        'SSSDBDataSet
        '
        Me.SSSDBDataSet.DataSetName = "SSSDBDataSet"
        Me.SSSDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GridView3
        '
        Me.GridView3.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn16, Me.GridColumn17, Me.GridColumn18, Me.GridColumn19, Me.GridColumn20, Me.GridColumn21, Me.GridColumn22, Me.GridColumn23, Me.GridColumn24, Me.GridColumn25, Me.GridColumn26, Me.GridColumn27, Me.GridColumn28, Me.GridColumn29, Me.GridColumn30})
        Me.GridView3.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView3.Name = "GridView3"
        Me.GridView3.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView3.OptionsView.ShowGroupPanel = False
        '
        'GridColumn16
        '
        Me.GridColumn16.FieldName = "SHIFT"
        Me.GridColumn16.Name = "GridColumn16"
        Me.GridColumn16.Visible = True
        Me.GridColumn16.VisibleIndex = 0
        '
        'GridColumn17
        '
        Me.GridColumn17.DisplayFormat.FormatString = "HH:mm"
        Me.GridColumn17.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.GridColumn17.FieldName = "STARTTIME"
        Me.GridColumn17.Name = "GridColumn17"
        Me.GridColumn17.Visible = True
        Me.GridColumn17.VisibleIndex = 1
        '
        'GridColumn18
        '
        Me.GridColumn18.DisplayFormat.FormatString = "HH:mm"
        Me.GridColumn18.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.GridColumn18.FieldName = "ENDTIME"
        Me.GridColumn18.Name = "GridColumn18"
        Me.GridColumn18.Visible = True
        Me.GridColumn18.VisibleIndex = 2
        '
        'GridColumn19
        '
        Me.GridColumn19.FieldName = "LUNCHTIME"
        Me.GridColumn19.Name = "GridColumn19"
        '
        'GridColumn20
        '
        Me.GridColumn20.FieldName = "LUNCHDURATION"
        Me.GridColumn20.Name = "GridColumn20"
        '
        'GridColumn21
        '
        Me.GridColumn21.FieldName = "LUNCHENDTIME"
        Me.GridColumn21.Name = "GridColumn21"
        '
        'GridColumn22
        '
        Me.GridColumn22.FieldName = "ORDERINF12"
        Me.GridColumn22.Name = "GridColumn22"
        '
        'GridColumn23
        '
        Me.GridColumn23.FieldName = "OTSTARTAFTER"
        Me.GridColumn23.Name = "GridColumn23"
        '
        'GridColumn24
        '
        Me.GridColumn24.FieldName = "OTDEDUCTHRS"
        Me.GridColumn24.Name = "GridColumn24"
        '
        'GridColumn25
        '
        Me.GridColumn25.FieldName = "LUNCHDEDUCTION"
        Me.GridColumn25.Name = "GridColumn25"
        '
        'GridColumn26
        '
        Me.GridColumn26.FieldName = "SHIFTPOSITION"
        Me.GridColumn26.Name = "GridColumn26"
        '
        'GridColumn27
        '
        Me.GridColumn27.FieldName = "SHIFTDURATION"
        Me.GridColumn27.Name = "GridColumn27"
        '
        'GridColumn28
        '
        Me.GridColumn28.FieldName = "OTDEDUCTAFTER"
        Me.GridColumn28.Name = "GridColumn28"
        '
        'GridColumn29
        '
        Me.GridColumn29.FieldName = "LastModifiedBy"
        Me.GridColumn29.Name = "GridColumn29"
        '
        'GridColumn30
        '
        Me.GridColumn30.FieldName = "LastModifiedDate"
        Me.GridColumn30.Name = "GridColumn30"
        '
        'LabelControl26
        '
        Me.LabelControl26.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl26.Appearance.Options.UseFont = True
        Me.LabelControl26.Location = New System.Drawing.Point(5, 127)
        Me.LabelControl26.Name = "LabelControl26"
        Me.LabelControl26.Size = New System.Drawing.Size(73, 14)
        Me.LabelControl26.TabIndex = 4
        Me.LabelControl26.Text = "Half Day Shift"
        '
        'LabelControl25
        '
        Me.LabelControl25.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl25.Appearance.Options.UseFont = True
        Me.LabelControl25.Location = New System.Drawing.Point(5, 96)
        Me.LabelControl25.Name = "LabelControl25"
        Me.LabelControl25.Size = New System.Drawing.Size(96, 14)
        Me.LabelControl25.TabIndex = 3
        Me.LabelControl25.Text = "Second Wo Type"
        '
        'LabelControl24
        '
        Me.LabelControl24.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl24.Appearance.Options.UseFont = True
        Me.LabelControl24.Location = New System.Drawing.Point(5, 64)
        Me.LabelControl24.Name = "LabelControl24"
        Me.LabelControl24.Size = New System.Drawing.Size(107, 14)
        Me.LabelControl24.TabIndex = 2
        Me.LabelControl24.Text = "Second Weekly Off"
        '
        'LabelControl23
        '
        Me.LabelControl23.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl23.Appearance.Options.UseFont = True
        Me.LabelControl23.Location = New System.Drawing.Point(5, 37)
        Me.LabelControl23.Name = "LabelControl23"
        Me.LabelControl23.Size = New System.Drawing.Size(91, 14)
        Me.LabelControl23.TabIndex = 1
        Me.LabelControl23.Text = "First Weekly Off "
        '
        'GroupControl5
        '
        Me.GroupControl5.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GroupControl5.AppearanceCaption.Options.UseFont = True
        Me.GroupControl5.Controls.Add(Me.ToggleAllowCmpOffMlt)
        Me.GroupControl5.Controls.Add(Me.LabelControl53)
        Me.GroupControl5.Controls.Add(Me.GridLookUpEditMultiShif)
        Me.GroupControl5.Controls.Add(Me.SimpleButton4)
        Me.GroupControl5.Controls.Add(Me.GridLookUpEdit4)
        Me.GroupControl5.Controls.Add(Me.SimpleButton3)
        Me.GroupControl5.Controls.Add(Me.GridLookUpEdit3)
        Me.GroupControl5.Controls.Add(Me.TextEditRotation)
        Me.GroupControl5.Controls.Add(Me.TextEdit11)
        Me.GroupControl5.Controls.Add(Me.TextEdit10)
        Me.GroupControl5.Controls.Add(Me.LabelControl22)
        Me.GroupControl5.Controls.Add(Me.LabelControl21)
        Me.GroupControl5.Controls.Add(Me.PopupContainerEdit2)
        Me.GroupControl5.Controls.Add(Me.LabelControl20)
        Me.GroupControl5.Controls.Add(Me.ToggleSwitchAutoShift)
        Me.GroupControl5.Controls.Add(Me.LabelControl19)
        Me.GroupControl5.Controls.Add(Me.PopupContainerEdit1)
        Me.GroupControl5.Controls.Add(Me.LabelControl18)
        Me.GroupControl5.Controls.Add(Me.GridLookUpEditDefaltShift)
        Me.GroupControl5.Controls.Add(Me.LabelControl17)
        Me.GroupControl5.Controls.Add(Me.ComboBoxShiftType)
        Me.GroupControl5.Controls.Add(Me.LabelControl16)
        Me.GroupControl5.Location = New System.Drawing.Point(4, 4)
        Me.GroupControl5.Name = "GroupControl5"
        Me.GroupControl5.Size = New System.Drawing.Size(352, 380)
        Me.GroupControl5.TabIndex = 0
        Me.GroupControl5.Text = "Shift Details"
        '
        'ToggleAllowCmpOffMlt
        '
        Me.ToggleAllowCmpOffMlt.Location = New System.Drawing.Point(226, 171)
        Me.ToggleAllowCmpOffMlt.Name = "ToggleAllowCmpOffMlt"
        Me.ToggleAllowCmpOffMlt.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleAllowCmpOffMlt.Properties.Appearance.Options.UseFont = True
        Me.ToggleAllowCmpOffMlt.Properties.OffText = "No"
        Me.ToggleAllowCmpOffMlt.Properties.OnText = "Yes"
        Me.ToggleAllowCmpOffMlt.Size = New System.Drawing.Size(95, 25)
        Me.ToggleAllowCmpOffMlt.TabIndex = 9
        Me.ToggleAllowCmpOffMlt.Visible = False
        '
        'LabelControl53
        '
        Me.LabelControl53.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl53.Appearance.Options.UseFont = True
        Me.LabelControl53.Location = New System.Drawing.Point(6, 176)
        Me.LabelControl53.Name = "LabelControl53"
        Me.LabelControl53.Size = New System.Drawing.Size(139, 14)
        Me.LabelControl53.TabIndex = 24
        Me.LabelControl53.Text = "Allow CompOff Multi Shift"
        Me.LabelControl53.Visible = False
        '
        'GridLookUpEditMultiShif
        '
        Me.GridLookUpEditMultiShif.EditValue = ""
        Me.GridLookUpEditMultiShif.Location = New System.Drawing.Point(107, 101)
        Me.GridLookUpEditMultiShif.Name = "GridLookUpEditMultiShif"
        Me.GridLookUpEditMultiShif.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEditMultiShif.Properties.Appearance.Options.UseFont = True
        Me.GridLookUpEditMultiShif.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEditMultiShif.Properties.AppearanceDropDown.Options.UseFont = True
        Me.GridLookUpEditMultiShif.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GridLookUpEditMultiShif.Properties.DataSource = Me.TblShiftMasterBindingSource
        Me.GridLookUpEditMultiShif.Properties.DisplayMember = "SHIFT"
        Me.GridLookUpEditMultiShif.Properties.ValueMember = "SHIFT"
        Me.GridLookUpEditMultiShif.Properties.View = Me.GridView6
        Me.GridLookUpEditMultiShif.Size = New System.Drawing.Size(215, 20)
        Me.GridLookUpEditMultiShif.TabIndex = 8
        Me.GridLookUpEditMultiShif.Visible = False
        '
        'GridView6
        '
        Me.GridView6.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn61, Me.GridColumn62, Me.GridColumn63, Me.GridColumn64, Me.GridColumn65, Me.GridColumn66, Me.GridColumn67, Me.GridColumn68, Me.GridColumn69, Me.GridColumn70, Me.GridColumn71, Me.GridColumn72, Me.GridColumn73, Me.GridColumn74, Me.GridColumn75})
        Me.GridView6.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView6.Name = "GridView6"
        Me.GridView6.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView6.OptionsView.ShowGroupPanel = False
        '
        'GridColumn61
        '
        Me.GridColumn61.FieldName = "SHIFT"
        Me.GridColumn61.Name = "GridColumn61"
        Me.GridColumn61.Visible = True
        Me.GridColumn61.VisibleIndex = 0
        '
        'GridColumn62
        '
        Me.GridColumn62.DisplayFormat.FormatString = "HH:mm"
        Me.GridColumn62.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.GridColumn62.FieldName = "STARTTIME"
        Me.GridColumn62.Name = "GridColumn62"
        Me.GridColumn62.Visible = True
        Me.GridColumn62.VisibleIndex = 1
        '
        'GridColumn63
        '
        Me.GridColumn63.DisplayFormat.FormatString = "HH:mm"
        Me.GridColumn63.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.GridColumn63.FieldName = "ENDTIME"
        Me.GridColumn63.Name = "GridColumn63"
        Me.GridColumn63.Visible = True
        Me.GridColumn63.VisibleIndex = 2
        '
        'GridColumn64
        '
        Me.GridColumn64.FieldName = "LUNCHTIME"
        Me.GridColumn64.Name = "GridColumn64"
        '
        'GridColumn65
        '
        Me.GridColumn65.FieldName = "LUNCHDURATION"
        Me.GridColumn65.Name = "GridColumn65"
        '
        'GridColumn66
        '
        Me.GridColumn66.FieldName = "LUNCHENDTIME"
        Me.GridColumn66.Name = "GridColumn66"
        '
        'GridColumn67
        '
        Me.GridColumn67.FieldName = "ORDERINF12"
        Me.GridColumn67.Name = "GridColumn67"
        '
        'GridColumn68
        '
        Me.GridColumn68.FieldName = "OTSTARTAFTER"
        Me.GridColumn68.Name = "GridColumn68"
        '
        'GridColumn69
        '
        Me.GridColumn69.FieldName = "OTDEDUCTHRS"
        Me.GridColumn69.Name = "GridColumn69"
        '
        'GridColumn70
        '
        Me.GridColumn70.FieldName = "LUNCHDEDUCTION"
        Me.GridColumn70.Name = "GridColumn70"
        '
        'GridColumn71
        '
        Me.GridColumn71.FieldName = "SHIFTPOSITION"
        Me.GridColumn71.Name = "GridColumn71"
        '
        'GridColumn72
        '
        Me.GridColumn72.FieldName = "SHIFTDURATION"
        Me.GridColumn72.Name = "GridColumn72"
        '
        'GridColumn73
        '
        Me.GridColumn73.FieldName = "OTDEDUCTAFTER"
        Me.GridColumn73.Name = "GridColumn73"
        '
        'GridColumn74
        '
        Me.GridColumn74.FieldName = "LastModifiedBy"
        Me.GridColumn74.Name = "GridColumn74"
        '
        'GridColumn75
        '
        Me.GridColumn75.FieldName = "LastModifiedDate"
        Me.GridColumn75.Name = "GridColumn75"
        '
        'SimpleButton4
        '
        Me.SimpleButton4.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButton4.Appearance.Options.UseFont = True
        Me.SimpleButton4.Location = New System.Drawing.Point(233, 142)
        Me.SimpleButton4.Name = "SimpleButton4"
        Me.SimpleButton4.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton4.TabIndex = 7
        Me.SimpleButton4.Text = "Remove"
        '
        'GridLookUpEdit4
        '
        Me.GridLookUpEdit4.EditValue = ""
        Me.GridLookUpEdit4.Location = New System.Drawing.Point(107, 142)
        Me.GridLookUpEdit4.Name = "GridLookUpEdit4"
        Me.GridLookUpEdit4.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEdit4.Properties.Appearance.Options.UseFont = True
        Me.GridLookUpEdit4.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GridLookUpEdit4.Properties.DataSource = Me.TblShiftMasterBindingSource
        Me.GridLookUpEdit4.Properties.DisplayMember = "SHIFT"
        Me.GridLookUpEdit4.Properties.ValueMember = "SHIFT"
        Me.GridLookUpEdit4.Properties.View = Me.GridView5
        Me.GridLookUpEdit4.Size = New System.Drawing.Size(120, 20)
        Me.GridLookUpEdit4.TabIndex = 6
        '
        'GridView5
        '
        Me.GridView5.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn46, Me.GridColumn47, Me.GridColumn48, Me.GridColumn49, Me.GridColumn50, Me.GridColumn51, Me.GridColumn52, Me.GridColumn53, Me.GridColumn54, Me.GridColumn55, Me.GridColumn56, Me.GridColumn57, Me.GridColumn58, Me.GridColumn59, Me.GridColumn60})
        Me.GridView5.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView5.Name = "GridView5"
        Me.GridView5.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView5.OptionsView.ShowGroupPanel = False
        '
        'GridColumn46
        '
        Me.GridColumn46.FieldName = "SHIFT"
        Me.GridColumn46.Name = "GridColumn46"
        Me.GridColumn46.Visible = True
        Me.GridColumn46.VisibleIndex = 0
        '
        'GridColumn47
        '
        Me.GridColumn47.DisplayFormat.FormatString = "HH:mm"
        Me.GridColumn47.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.GridColumn47.FieldName = "STARTTIME"
        Me.GridColumn47.Name = "GridColumn47"
        Me.GridColumn47.Visible = True
        Me.GridColumn47.VisibleIndex = 1
        '
        'GridColumn48
        '
        Me.GridColumn48.DisplayFormat.FormatString = "HH:mm"
        Me.GridColumn48.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.GridColumn48.FieldName = "ENDTIME"
        Me.GridColumn48.Name = "GridColumn48"
        Me.GridColumn48.Visible = True
        Me.GridColumn48.VisibleIndex = 2
        '
        'GridColumn49
        '
        Me.GridColumn49.FieldName = "LUNCHTIME"
        Me.GridColumn49.Name = "GridColumn49"
        '
        'GridColumn50
        '
        Me.GridColumn50.FieldName = "LUNCHDURATION"
        Me.GridColumn50.Name = "GridColumn50"
        '
        'GridColumn51
        '
        Me.GridColumn51.FieldName = "LUNCHENDTIME"
        Me.GridColumn51.Name = "GridColumn51"
        '
        'GridColumn52
        '
        Me.GridColumn52.FieldName = "ORDERINF12"
        Me.GridColumn52.Name = "GridColumn52"
        '
        'GridColumn53
        '
        Me.GridColumn53.FieldName = "OTSTARTAFTER"
        Me.GridColumn53.Name = "GridColumn53"
        '
        'GridColumn54
        '
        Me.GridColumn54.FieldName = "OTDEDUCTHRS"
        Me.GridColumn54.Name = "GridColumn54"
        '
        'GridColumn55
        '
        Me.GridColumn55.FieldName = "LUNCHDEDUCTION"
        Me.GridColumn55.Name = "GridColumn55"
        '
        'GridColumn56
        '
        Me.GridColumn56.FieldName = "SHIFTPOSITION"
        Me.GridColumn56.Name = "GridColumn56"
        '
        'GridColumn57
        '
        Me.GridColumn57.FieldName = "SHIFTDURATION"
        Me.GridColumn57.Name = "GridColumn57"
        '
        'GridColumn58
        '
        Me.GridColumn58.FieldName = "OTDEDUCTAFTER"
        Me.GridColumn58.Name = "GridColumn58"
        '
        'GridColumn59
        '
        Me.GridColumn59.FieldName = "LastModifiedBy"
        Me.GridColumn59.Name = "GridColumn59"
        '
        'GridColumn60
        '
        Me.GridColumn60.FieldName = "LastModifiedDate"
        Me.GridColumn60.Name = "GridColumn60"
        '
        'SimpleButton3
        '
        Me.SimpleButton3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButton3.Appearance.Options.UseFont = True
        Me.SimpleButton3.Location = New System.Drawing.Point(233, 89)
        Me.SimpleButton3.Name = "SimpleButton3"
        Me.SimpleButton3.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton3.TabIndex = 4
        Me.SimpleButton3.Text = "Add"
        '
        'GridLookUpEdit3
        '
        Me.GridLookUpEdit3.EditValue = ""
        Me.GridLookUpEdit3.Location = New System.Drawing.Point(107, 90)
        Me.GridLookUpEdit3.Name = "GridLookUpEdit3"
        Me.GridLookUpEdit3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEdit3.Properties.Appearance.Options.UseFont = True
        Me.GridLookUpEdit3.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GridLookUpEdit3.Properties.DataSource = Me.TblShiftMasterBindingSource
        Me.GridLookUpEdit3.Properties.DisplayMember = "SHIFT"
        Me.GridLookUpEdit3.Properties.ValueMember = "SHIFT"
        Me.GridLookUpEdit3.Properties.View = Me.GridView4
        Me.GridLookUpEdit3.Size = New System.Drawing.Size(120, 20)
        Me.GridLookUpEdit3.TabIndex = 3
        '
        'GridView4
        '
        Me.GridView4.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn31, Me.GridColumn32, Me.GridColumn33, Me.GridColumn34, Me.GridColumn35, Me.GridColumn36, Me.GridColumn37, Me.GridColumn38, Me.GridColumn39, Me.GridColumn40, Me.GridColumn41, Me.GridColumn42, Me.GridColumn43, Me.GridColumn44, Me.GridColumn45})
        Me.GridView4.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView4.Name = "GridView4"
        Me.GridView4.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView4.OptionsView.ShowGroupPanel = False
        '
        'GridColumn31
        '
        Me.GridColumn31.FieldName = "SHIFT"
        Me.GridColumn31.Name = "GridColumn31"
        Me.GridColumn31.Visible = True
        Me.GridColumn31.VisibleIndex = 0
        '
        'GridColumn32
        '
        Me.GridColumn32.DisplayFormat.FormatString = "HH:mm"
        Me.GridColumn32.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.GridColumn32.FieldName = "STARTTIME"
        Me.GridColumn32.Name = "GridColumn32"
        Me.GridColumn32.Visible = True
        Me.GridColumn32.VisibleIndex = 1
        '
        'GridColumn33
        '
        Me.GridColumn33.DisplayFormat.FormatString = "HH:mm"
        Me.GridColumn33.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.GridColumn33.FieldName = "ENDTIME"
        Me.GridColumn33.Name = "GridColumn33"
        Me.GridColumn33.Visible = True
        Me.GridColumn33.VisibleIndex = 2
        '
        'GridColumn34
        '
        Me.GridColumn34.FieldName = "LUNCHTIME"
        Me.GridColumn34.Name = "GridColumn34"
        '
        'GridColumn35
        '
        Me.GridColumn35.FieldName = "LUNCHDURATION"
        Me.GridColumn35.Name = "GridColumn35"
        '
        'GridColumn36
        '
        Me.GridColumn36.FieldName = "LUNCHENDTIME"
        Me.GridColumn36.Name = "GridColumn36"
        '
        'GridColumn37
        '
        Me.GridColumn37.FieldName = "ORDERINF12"
        Me.GridColumn37.Name = "GridColumn37"
        '
        'GridColumn38
        '
        Me.GridColumn38.FieldName = "OTSTARTAFTER"
        Me.GridColumn38.Name = "GridColumn38"
        '
        'GridColumn39
        '
        Me.GridColumn39.FieldName = "OTDEDUCTHRS"
        Me.GridColumn39.Name = "GridColumn39"
        '
        'GridColumn40
        '
        Me.GridColumn40.FieldName = "LUNCHDEDUCTION"
        Me.GridColumn40.Name = "GridColumn40"
        '
        'GridColumn41
        '
        Me.GridColumn41.FieldName = "SHIFTPOSITION"
        Me.GridColumn41.Name = "GridColumn41"
        '
        'GridColumn42
        '
        Me.GridColumn42.FieldName = "SHIFTDURATION"
        Me.GridColumn42.Name = "GridColumn42"
        '
        'GridColumn43
        '
        Me.GridColumn43.FieldName = "OTDEDUCTAFTER"
        Me.GridColumn43.Name = "GridColumn43"
        '
        'GridColumn44
        '
        Me.GridColumn44.FieldName = "LastModifiedBy"
        Me.GridColumn44.Name = "GridColumn44"
        '
        'GridColumn45
        '
        Me.GridColumn45.FieldName = "LastModifiedDate"
        Me.GridColumn45.Name = "GridColumn45"
        '
        'TextEditRotation
        '
        Me.TextEditRotation.Location = New System.Drawing.Point(107, 116)
        Me.TextEditRotation.Name = "TextEditRotation"
        Me.TextEditRotation.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditRotation.Properties.Appearance.Options.UseFont = True
        Me.TextEditRotation.Properties.AppearanceReadOnly.BackColor = System.Drawing.Color.White
        Me.TextEditRotation.Properties.AppearanceReadOnly.Options.UseBackColor = True
        Me.TextEditRotation.Properties.ReadOnly = True
        Me.TextEditRotation.Size = New System.Drawing.Size(197, 20)
        Me.TextEditRotation.TabIndex = 5
        '
        'TextEdit11
        '
        Me.TextEdit11.Location = New System.Drawing.Point(262, 316)
        Me.TextEdit11.Name = "TextEdit11"
        Me.TextEdit11.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEdit11.Properties.Appearance.Options.UseFont = True
        Me.TextEdit11.Properties.Mask.EditMask = "##"
        Me.TextEdit11.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEdit11.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEdit11.Properties.MaxLength = 2
        Me.TextEdit11.Size = New System.Drawing.Size(60, 20)
        Me.TextEdit11.TabIndex = 13
        '
        'TextEdit10
        '
        Me.TextEdit10.EditValue = "00"
        Me.TextEdit10.Location = New System.Drawing.Point(261, 290)
        Me.TextEdit10.Name = "TextEdit10"
        Me.TextEdit10.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEdit10.Properties.Appearance.Options.UseFont = True
        Me.TextEdit10.Properties.Mask.EditMask = "##"
        Me.TextEdit10.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Numeric
        Me.TextEdit10.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEdit10.Properties.MaxLength = 2
        Me.TextEdit10.Size = New System.Drawing.Size(60, 20)
        Me.TextEdit10.TabIndex = 12
        '
        'LabelControl22
        '
        Me.LabelControl22.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl22.Appearance.Options.UseFont = True
        Me.LabelControl22.Location = New System.Drawing.Point(6, 320)
        Me.LabelControl22.Name = "LabelControl22"
        Me.LabelControl22.Size = New System.Drawing.Size(192, 14)
        Me.LabelControl22.TabIndex = 15
        Me.LabelControl22.Text = "Shift Change After How Many Days"
        '
        'LabelControl21
        '
        Me.LabelControl21.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl21.Appearance.Options.UseFont = True
        Me.LabelControl21.Location = New System.Drawing.Point(6, 290)
        Me.LabelControl21.Name = "LabelControl21"
        Me.LabelControl21.Size = New System.Drawing.Size(113, 14)
        Me.LabelControl21.TabIndex = 14
        Me.LabelControl21.Text = "Shift Remaining Days"
        '
        'PopupContainerEdit2
        '
        Me.PopupContainerEdit2.Location = New System.Drawing.Point(106, 255)
        Me.PopupContainerEdit2.Name = "PopupContainerEdit2"
        Me.PopupContainerEdit2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.PopupContainerEdit2.Properties.PopupControl = Me.PopupContainerControl2
        Me.PopupContainerEdit2.Size = New System.Drawing.Size(215, 20)
        Me.PopupContainerEdit2.TabIndex = 11
        '
        'PopupContainerControl2
        '
        Me.PopupContainerControl2.Controls.Add(Me.GridControl2)
        Me.PopupContainerControl2.Location = New System.Drawing.Point(10, 473)
        Me.PopupContainerControl2.Name = "PopupContainerControl2"
        Me.PopupContainerControl2.Size = New System.Drawing.Size(300, 300)
        Me.PopupContainerControl2.TabIndex = 10
        '
        'GridControl2
        '
        Me.GridControl2.DataSource = Me.TblShiftMasterBindingSource
        Me.GridControl2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControl2.Location = New System.Drawing.Point(0, 0)
        Me.GridControl2.MainView = Me.GridView2
        Me.GridControl2.Name = "GridControl2"
        Me.GridControl2.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTimeEdit2})
        Me.GridControl2.Size = New System.Drawing.Size(300, 300)
        Me.GridControl2.TabIndex = 6
        Me.GridControl2.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView2})
        '
        'GridView2
        '
        Me.GridView2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn2, Me.GridColumn3, Me.GridColumn4, Me.GridColumn5, Me.GridColumn6, Me.GridColumn7, Me.GridColumn8, Me.GridColumn9, Me.GridColumn10, Me.GridColumn11, Me.GridColumn12, Me.GridColumn13, Me.GridColumn14, Me.GridColumn15})
        Me.GridView2.GridControl = Me.GridControl2
        Me.GridView2.Name = "GridView2"
        Me.GridView2.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView2.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView2.OptionsBehavior.Editable = False
        Me.GridView2.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridView2.OptionsSelection.MultiSelect = True
        Me.GridView2.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        Me.GridView2.OptionsView.ColumnAutoWidth = False
        '
        'GridColumn1
        '
        Me.GridColumn1.FieldName = "SHIFT"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 1
        '
        'GridColumn2
        '
        Me.GridColumn2.ColumnEdit = Me.RepositoryItemTimeEdit2
        Me.GridColumn2.FieldName = "STARTTIME"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 2
        '
        'RepositoryItemTimeEdit2
        '
        Me.RepositoryItemTimeEdit2.AutoHeight = False
        Me.RepositoryItemTimeEdit2.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemTimeEdit2.Mask.EditMask = "HH:mm"
        Me.RepositoryItemTimeEdit2.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemTimeEdit2.Name = "RepositoryItemTimeEdit2"
        '
        'GridColumn3
        '
        Me.GridColumn3.ColumnEdit = Me.RepositoryItemTimeEdit2
        Me.GridColumn3.FieldName = "ENDTIME"
        Me.GridColumn3.Name = "GridColumn3"
        Me.GridColumn3.Visible = True
        Me.GridColumn3.VisibleIndex = 3
        '
        'GridColumn4
        '
        Me.GridColumn4.FieldName = "LUNCHTIME"
        Me.GridColumn4.Name = "GridColumn4"
        '
        'GridColumn5
        '
        Me.GridColumn5.FieldName = "LUNCHDURATION"
        Me.GridColumn5.Name = "GridColumn5"
        '
        'GridColumn6
        '
        Me.GridColumn6.FieldName = "LUNCHENDTIME"
        Me.GridColumn6.Name = "GridColumn6"
        '
        'GridColumn7
        '
        Me.GridColumn7.FieldName = "ORDERINF12"
        Me.GridColumn7.Name = "GridColumn7"
        '
        'GridColumn8
        '
        Me.GridColumn8.FieldName = "OTSTARTAFTER"
        Me.GridColumn8.Name = "GridColumn8"
        '
        'GridColumn9
        '
        Me.GridColumn9.FieldName = "OTDEDUCTHRS"
        Me.GridColumn9.Name = "GridColumn9"
        '
        'GridColumn10
        '
        Me.GridColumn10.FieldName = "LUNCHDEDUCTION"
        Me.GridColumn10.Name = "GridColumn10"
        '
        'GridColumn11
        '
        Me.GridColumn11.FieldName = "SHIFTPOSITION"
        Me.GridColumn11.Name = "GridColumn11"
        '
        'GridColumn12
        '
        Me.GridColumn12.FieldName = "SHIFTDURATION"
        Me.GridColumn12.Name = "GridColumn12"
        '
        'GridColumn13
        '
        Me.GridColumn13.FieldName = "OTDEDUCTAFTER"
        Me.GridColumn13.Name = "GridColumn13"
        '
        'GridColumn14
        '
        Me.GridColumn14.FieldName = "LastModifiedBy"
        Me.GridColumn14.Name = "GridColumn14"
        '
        'GridColumn15
        '
        Me.GridColumn15.FieldName = "LastModifiedDate"
        Me.GridColumn15.Name = "GridColumn15"
        '
        'LabelControl20
        '
        Me.LabelControl20.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl20.Appearance.Options.UseFont = True
        Me.LabelControl20.Location = New System.Drawing.Point(4, 257)
        Me.LabelControl20.Name = "LabelControl20"
        Me.LabelControl20.Size = New System.Drawing.Size(87, 14)
        Me.LabelControl20.TabIndex = 12
        Me.LabelControl20.Text = "Add Auto Shifts"
        '
        'ToggleSwitchAutoShift
        '
        Me.ToggleSwitchAutoShift.Location = New System.Drawing.Point(226, 216)
        Me.ToggleSwitchAutoShift.Name = "ToggleSwitchAutoShift"
        Me.ToggleSwitchAutoShift.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleSwitchAutoShift.Properties.Appearance.Options.UseFont = True
        Me.ToggleSwitchAutoShift.Properties.OffText = "No"
        Me.ToggleSwitchAutoShift.Properties.OnText = "Yes"
        Me.ToggleSwitchAutoShift.Size = New System.Drawing.Size(95, 25)
        Me.ToggleSwitchAutoShift.TabIndex = 10
        '
        'LabelControl19
        '
        Me.LabelControl19.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl19.Appearance.Options.UseFont = True
        Me.LabelControl19.Location = New System.Drawing.Point(5, 227)
        Me.LabelControl19.Name = "LabelControl19"
        Me.LabelControl19.Size = New System.Drawing.Size(81, 14)
        Me.LabelControl19.TabIndex = 10
        Me.LabelControl19.Text = "Run Auto Shift"
        '
        'PopupContainerEdit1
        '
        Me.PopupContainerEdit1.Location = New System.Drawing.Point(106, 355)
        Me.PopupContainerEdit1.Name = "PopupContainerEdit1"
        Me.PopupContainerEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEdit1.Properties.Appearance.Options.UseFont = True
        Me.PopupContainerEdit1.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEdit1.Properties.AppearanceDropDown.Options.UseFont = True
        Me.PopupContainerEdit1.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEdit1.Properties.AppearanceFocused.Options.UseFont = True
        Me.PopupContainerEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.PopupContainerEdit1.Properties.PopupControl = Me.PopupContainerControl1
        Me.PopupContainerEdit1.Size = New System.Drawing.Size(215, 20)
        Me.PopupContainerEdit1.TabIndex = 9
        Me.PopupContainerEdit1.Visible = False
        '
        'PopupContainerControl1
        '
        Me.PopupContainerControl1.Controls.Add(Me.GridControl1)
        Me.PopupContainerControl1.Location = New System.Drawing.Point(232, 470)
        Me.PopupContainerControl1.Name = "PopupContainerControl1"
        Me.PopupContainerControl1.Size = New System.Drawing.Size(300, 300)
        Me.PopupContainerControl1.TabIndex = 9
        '
        'GridControl1
        '
        Me.GridControl1.DataSource = Me.TblShiftMasterBindingSource
        Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControl1.Location = New System.Drawing.Point(0, 0)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTimeEdit1})
        Me.GridControl1.Size = New System.Drawing.Size(300, 300)
        Me.GridControl1.TabIndex = 6
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colSHIFT1, Me.colSTARTTIME1, Me.colENDTIME1, Me.colLUNCHTIME1, Me.colLUNCHDURATION1, Me.colLUNCHENDTIME1, Me.colORDERINF121, Me.colOTSTARTAFTER1, Me.colOTDEDUCTHRS1, Me.colLUNCHDEDUCTION1, Me.colSHIFTPOSITION1, Me.colSHIFTDURATION1, Me.colOTDEDUCTAFTER1, Me.colLastModifiedBy1, Me.colLastModifiedDate1})
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.Editable = False
        Me.GridView1.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridView1.OptionsSelection.MultiSelect = True
        Me.GridView1.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        Me.GridView1.OptionsView.ColumnAutoWidth = False
        '
        'colSHIFT1
        '
        Me.colSHIFT1.FieldName = "SHIFT"
        Me.colSHIFT1.Name = "colSHIFT1"
        Me.colSHIFT1.Visible = True
        Me.colSHIFT1.VisibleIndex = 1
        '
        'colSTARTTIME1
        '
        Me.colSTARTTIME1.ColumnEdit = Me.RepositoryItemTimeEdit1
        Me.colSTARTTIME1.FieldName = "STARTTIME"
        Me.colSTARTTIME1.Name = "colSTARTTIME1"
        Me.colSTARTTIME1.Visible = True
        Me.colSTARTTIME1.VisibleIndex = 2
        '
        'RepositoryItemTimeEdit1
        '
        Me.RepositoryItemTimeEdit1.AutoHeight = False
        Me.RepositoryItemTimeEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemTimeEdit1.Mask.EditMask = "HH:mm"
        Me.RepositoryItemTimeEdit1.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemTimeEdit1.Name = "RepositoryItemTimeEdit1"
        '
        'colENDTIME1
        '
        Me.colENDTIME1.ColumnEdit = Me.RepositoryItemTimeEdit1
        Me.colENDTIME1.FieldName = "ENDTIME"
        Me.colENDTIME1.Name = "colENDTIME1"
        Me.colENDTIME1.Visible = True
        Me.colENDTIME1.VisibleIndex = 3
        '
        'colLUNCHTIME1
        '
        Me.colLUNCHTIME1.FieldName = "LUNCHTIME"
        Me.colLUNCHTIME1.Name = "colLUNCHTIME1"
        '
        'colLUNCHDURATION1
        '
        Me.colLUNCHDURATION1.FieldName = "LUNCHDURATION"
        Me.colLUNCHDURATION1.Name = "colLUNCHDURATION1"
        '
        'colLUNCHENDTIME1
        '
        Me.colLUNCHENDTIME1.FieldName = "LUNCHENDTIME"
        Me.colLUNCHENDTIME1.Name = "colLUNCHENDTIME1"
        '
        'colORDERINF121
        '
        Me.colORDERINF121.FieldName = "ORDERINF12"
        Me.colORDERINF121.Name = "colORDERINF121"
        '
        'colOTSTARTAFTER1
        '
        Me.colOTSTARTAFTER1.FieldName = "OTSTARTAFTER"
        Me.colOTSTARTAFTER1.Name = "colOTSTARTAFTER1"
        '
        'colOTDEDUCTHRS1
        '
        Me.colOTDEDUCTHRS1.FieldName = "OTDEDUCTHRS"
        Me.colOTDEDUCTHRS1.Name = "colOTDEDUCTHRS1"
        '
        'colLUNCHDEDUCTION1
        '
        Me.colLUNCHDEDUCTION1.FieldName = "LUNCHDEDUCTION"
        Me.colLUNCHDEDUCTION1.Name = "colLUNCHDEDUCTION1"
        '
        'colSHIFTPOSITION1
        '
        Me.colSHIFTPOSITION1.FieldName = "SHIFTPOSITION"
        Me.colSHIFTPOSITION1.Name = "colSHIFTPOSITION1"
        '
        'colSHIFTDURATION1
        '
        Me.colSHIFTDURATION1.FieldName = "SHIFTDURATION"
        Me.colSHIFTDURATION1.Name = "colSHIFTDURATION1"
        '
        'colOTDEDUCTAFTER1
        '
        Me.colOTDEDUCTAFTER1.FieldName = "OTDEDUCTAFTER"
        Me.colOTDEDUCTAFTER1.Name = "colOTDEDUCTAFTER1"
        '
        'colLastModifiedBy1
        '
        Me.colLastModifiedBy1.FieldName = "LastModifiedBy"
        Me.colLastModifiedBy1.Name = "colLastModifiedBy1"
        '
        'colLastModifiedDate1
        '
        Me.colLastModifiedDate1.FieldName = "LastModifiedDate"
        Me.colLastModifiedDate1.Name = "colLastModifiedDate1"
        '
        'LabelControl18
        '
        Me.LabelControl18.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl18.Appearance.Options.UseFont = True
        Me.LabelControl18.Location = New System.Drawing.Point(5, 104)
        Me.LabelControl18.Name = "LabelControl18"
        Me.LabelControl18.Size = New System.Drawing.Size(96, 14)
        Me.LabelControl18.TabIndex = 6
        Me.LabelControl18.Text = "Shifts for rotation"
        '
        'GridLookUpEditDefaltShift
        '
        Me.GridLookUpEditDefaltShift.EditValue = ""
        Me.GridLookUpEditDefaltShift.Location = New System.Drawing.Point(107, 65)
        Me.GridLookUpEditDefaltShift.Name = "GridLookUpEditDefaltShift"
        Me.GridLookUpEditDefaltShift.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEditDefaltShift.Properties.Appearance.Options.UseFont = True
        Me.GridLookUpEditDefaltShift.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEditDefaltShift.Properties.AppearanceDropDown.Options.UseFont = True
        Me.GridLookUpEditDefaltShift.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GridLookUpEditDefaltShift.Properties.DataSource = Me.TblShiftMasterBindingSource
        Me.GridLookUpEditDefaltShift.Properties.DisplayMember = "SHIFT"
        Me.GridLookUpEditDefaltShift.Properties.ValueMember = "SHIFT"
        Me.GridLookUpEditDefaltShift.Properties.View = Me.GridLookUpEdit1View
        Me.GridLookUpEditDefaltShift.Size = New System.Drawing.Size(215, 20)
        Me.GridLookUpEditDefaltShift.TabIndex = 2
        '
        'GridLookUpEdit1View
        '
        Me.GridLookUpEdit1View.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colSHIFT, Me.colSTARTTIME, Me.colENDTIME, Me.colLUNCHTIME, Me.colLUNCHDURATION, Me.colLUNCHENDTIME, Me.colORDERINF12, Me.colOTSTARTAFTER, Me.colOTDEDUCTHRS, Me.colLUNCHDEDUCTION, Me.colSHIFTPOSITION, Me.colSHIFTDURATION, Me.colOTDEDUCTAFTER, Me.colLastModifiedBy, Me.colLastModifiedDate})
        Me.GridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridLookUpEdit1View.Name = "GridLookUpEdit1View"
        Me.GridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridLookUpEdit1View.OptionsView.ShowGroupPanel = False
        '
        'colSHIFT
        '
        Me.colSHIFT.FieldName = "SHIFT"
        Me.colSHIFT.Name = "colSHIFT"
        Me.colSHIFT.Visible = True
        Me.colSHIFT.VisibleIndex = 0
        '
        'colSTARTTIME
        '
        Me.colSTARTTIME.DisplayFormat.FormatString = "HH:mm"
        Me.colSTARTTIME.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.colSTARTTIME.FieldName = "STARTTIME"
        Me.colSTARTTIME.Name = "colSTARTTIME"
        Me.colSTARTTIME.Visible = True
        Me.colSTARTTIME.VisibleIndex = 1
        '
        'colENDTIME
        '
        Me.colENDTIME.DisplayFormat.FormatString = "HH:mm"
        Me.colENDTIME.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.colENDTIME.FieldName = "ENDTIME"
        Me.colENDTIME.Name = "colENDTIME"
        Me.colENDTIME.Visible = True
        Me.colENDTIME.VisibleIndex = 2
        '
        'colLUNCHTIME
        '
        Me.colLUNCHTIME.FieldName = "LUNCHTIME"
        Me.colLUNCHTIME.Name = "colLUNCHTIME"
        '
        'colLUNCHDURATION
        '
        Me.colLUNCHDURATION.FieldName = "LUNCHDURATION"
        Me.colLUNCHDURATION.Name = "colLUNCHDURATION"
        '
        'colLUNCHENDTIME
        '
        Me.colLUNCHENDTIME.FieldName = "LUNCHENDTIME"
        Me.colLUNCHENDTIME.Name = "colLUNCHENDTIME"
        '
        'colORDERINF12
        '
        Me.colORDERINF12.FieldName = "ORDERINF12"
        Me.colORDERINF12.Name = "colORDERINF12"
        '
        'colOTSTARTAFTER
        '
        Me.colOTSTARTAFTER.FieldName = "OTSTARTAFTER"
        Me.colOTSTARTAFTER.Name = "colOTSTARTAFTER"
        '
        'colOTDEDUCTHRS
        '
        Me.colOTDEDUCTHRS.FieldName = "OTDEDUCTHRS"
        Me.colOTDEDUCTHRS.Name = "colOTDEDUCTHRS"
        '
        'colLUNCHDEDUCTION
        '
        Me.colLUNCHDEDUCTION.FieldName = "LUNCHDEDUCTION"
        Me.colLUNCHDEDUCTION.Name = "colLUNCHDEDUCTION"
        '
        'colSHIFTPOSITION
        '
        Me.colSHIFTPOSITION.FieldName = "SHIFTPOSITION"
        Me.colSHIFTPOSITION.Name = "colSHIFTPOSITION"
        '
        'colSHIFTDURATION
        '
        Me.colSHIFTDURATION.FieldName = "SHIFTDURATION"
        Me.colSHIFTDURATION.Name = "colSHIFTDURATION"
        '
        'colOTDEDUCTAFTER
        '
        Me.colOTDEDUCTAFTER.FieldName = "OTDEDUCTAFTER"
        Me.colOTDEDUCTAFTER.Name = "colOTDEDUCTAFTER"
        '
        'colLastModifiedBy
        '
        Me.colLastModifiedBy.FieldName = "LastModifiedBy"
        Me.colLastModifiedBy.Name = "colLastModifiedBy"
        '
        'colLastModifiedDate
        '
        Me.colLastModifiedDate.FieldName = "LastModifiedDate"
        Me.colLastModifiedDate.Name = "colLastModifiedDate"
        '
        'LabelControl17
        '
        Me.LabelControl17.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl17.Appearance.Options.UseFont = True
        Me.LabelControl17.Location = New System.Drawing.Point(6, 69)
        Me.LabelControl17.Name = "LabelControl17"
        Me.LabelControl17.Size = New System.Drawing.Size(79, 14)
        Me.LabelControl17.TabIndex = 2
        Me.LabelControl17.Text = "Default Shift *"
        '
        'ComboBoxShiftType
        '
        Me.ComboBoxShiftType.EditValue = "Fixed"
        Me.ComboBoxShiftType.Location = New System.Drawing.Point(107, 31)
        Me.ComboBoxShiftType.Name = "ComboBoxShiftType"
        Me.ComboBoxShiftType.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxShiftType.Properties.Appearance.Options.UseFont = True
        Me.ComboBoxShiftType.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxShiftType.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboBoxShiftType.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxShiftType.Properties.AppearanceFocused.Options.UseFont = True
        Me.ComboBoxShiftType.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxShiftType.Properties.Items.AddRange(New Object() {"Fixed", "Rotational", "Ignore", "Flexi", "MultiShift"})
        Me.ComboBoxShiftType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.ComboBoxShiftType.Size = New System.Drawing.Size(100, 20)
        Me.ComboBoxShiftType.TabIndex = 1
        '
        'LabelControl16
        '
        Me.LabelControl16.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl16.Appearance.Options.UseFont = True
        Me.LabelControl16.Location = New System.Drawing.Point(6, 34)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(68, 14)
        Me.LabelControl16.TabIndex = 0
        Me.LabelControl16.Text = "Shift Type *"
        '
        'TabNavigationPage3
        '
        Me.TabNavigationPage3.Caption = "Time Office Policy"
        Me.TabNavigationPage3.Controls.Add(Me.PanelControl2)
        Me.TabNavigationPage3.Controls.Add(Me.PanelControl1)
        Me.TabNavigationPage3.Name = "TabNavigationPage3"
        Me.TabNavigationPage3.Size = New System.Drawing.Size(704, 469)
        '
        'PanelControl2
        '
        Me.PanelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.PanelControl2.Controls.Add(Me.ToggleCmpPOWPOH)
        Me.PanelControl2.Controls.Add(Me.LabelControl54)
        Me.PanelControl2.Controls.Add(Me.LabelControl35)
        Me.PanelControl2.Controls.Add(Me.ToggleSwitchMarkHlf)
        Me.PanelControl2.Controls.Add(Me.LabelControl39)
        Me.PanelControl2.Controls.Add(Me.LabelControl27)
        Me.PanelControl2.Controls.Add(Me.ToggleOutWorkMins)
        Me.PanelControl2.Controls.Add(Me.LabelControl37)
        Me.PanelControl2.Controls.Add(Me.ToggleWeekOffIncludeInDUtyRoster)
        Me.PanelControl2.Controls.Add(Me.ToggleMarkMisAsAbsent)
        Me.PanelControl2.Controls.Add(Me.ToggleMarkWOasAbsent)
        Me.PanelControl2.Controls.Add(Me.ToggleAutoShiftAllowed)
        Me.PanelControl2.Controls.Add(Me.ToggleIsAutoAbsentAllowed)
        Me.PanelControl2.Controls.Add(Me.LabelControl34)
        Me.PanelControl2.Controls.Add(Me.LabelControl44)
        Me.PanelControl2.Controls.Add(Me.LabelControl45)
        Me.PanelControl2.Controls.Add(Me.TxtMaxEarlyDeparture)
        Me.PanelControl2.Controls.Add(Me.TxtNoOffpresentOnweekOff)
        Me.PanelControl2.Controls.Add(Me.LabelControlMarkWO)
        Me.PanelControl2.Controls.Add(Me.TxtMaxWorkingMin)
        Me.PanelControl2.Controls.Add(Me.TxtMaxLateArrivalMin)
        Me.PanelControl2.Controls.Add(Me.LabelControl47)
        Me.PanelControl2.Controls.Add(Me.LabelControl36)
        Me.PanelControl2.Controls.Add(Me.LabelControl46)
        Me.PanelControl2.Location = New System.Drawing.Point(352, 3)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(340, 416)
        Me.PanelControl2.TabIndex = 2
        '
        'ToggleCmpPOWPOH
        '
        Me.ToggleCmpPOWPOH.Location = New System.Drawing.Point(233, 231)
        Me.ToggleCmpPOWPOH.Name = "ToggleCmpPOWPOH"
        Me.ToggleCmpPOWPOH.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleCmpPOWPOH.Properties.Appearance.Options.UseFont = True
        Me.ToggleCmpPOWPOH.Properties.OffText = "No"
        Me.ToggleCmpPOWPOH.Properties.OnText = "Yes"
        Me.ToggleCmpPOWPOH.Size = New System.Drawing.Size(95, 25)
        Me.ToggleCmpPOWPOH.TabIndex = 8
        '
        'LabelControl54
        '
        Me.LabelControl54.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl54.Appearance.Options.UseFont = True
        Me.LabelControl54.Location = New System.Drawing.Point(5, 236)
        Me.LabelControl54.Name = "LabelControl54"
        Me.LabelControl54.Size = New System.Drawing.Size(161, 14)
        Me.LabelControl54.TabIndex = 34
        Me.LabelControl54.Text = "Allow CompOff for POW/POH"
        '
        'LabelControl35
        '
        Me.LabelControl35.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl35.Appearance.Options.UseFont = True
        Me.LabelControl35.Location = New System.Drawing.Point(1, 284)
        Me.LabelControl35.Name = "LabelControl35"
        Me.LabelControl35.Size = New System.Drawing.Size(172, 14)
        Me.LabelControl35.TabIndex = 9
        Me.LabelControl35.Text = "Maximum Working Min to Verify"
        Me.LabelControl35.Visible = False
        '
        'ToggleSwitchMarkHlf
        '
        Me.ToggleSwitchMarkHlf.Location = New System.Drawing.Point(233, 169)
        Me.ToggleSwitchMarkHlf.Name = "ToggleSwitchMarkHlf"
        Me.ToggleSwitchMarkHlf.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleSwitchMarkHlf.Properties.Appearance.Options.UseFont = True
        Me.ToggleSwitchMarkHlf.Properties.OffText = "No"
        Me.ToggleSwitchMarkHlf.Properties.OnText = "Yes"
        Me.ToggleSwitchMarkHlf.Size = New System.Drawing.Size(95, 25)
        Me.ToggleSwitchMarkHlf.TabIndex = 6
        '
        'LabelControl39
        '
        Me.LabelControl39.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl39.Appearance.Options.UseFont = True
        Me.LabelControl39.Location = New System.Drawing.Point(-1, 336)
        Me.LabelControl39.Name = "LabelControl39"
        Me.LabelControl39.Size = New System.Drawing.Size(159, 14)
        Me.LabelControl39.TabIndex = 10
        Me.LabelControl39.Text = "Max Early Departure Duration"
        Me.LabelControl39.Visible = False
        '
        'LabelControl27
        '
        Me.LabelControl27.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl27.Appearance.Options.UseFont = True
        Me.LabelControl27.Location = New System.Drawing.Point(5, 174)
        Me.LabelControl27.Name = "LabelControl27"
        Me.LabelControl27.Size = New System.Drawing.Size(114, 14)
        Me.LabelControl27.TabIndex = 33
        Me.LabelControl27.Text = "Mark MIS As Half Day"
        '
        'ToggleOutWorkMins
        '
        Me.ToggleOutWorkMins.Location = New System.Drawing.Point(233, 200)
        Me.ToggleOutWorkMins.Name = "ToggleOutWorkMins"
        Me.ToggleOutWorkMins.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleOutWorkMins.Properties.Appearance.Options.UseFont = True
        Me.ToggleOutWorkMins.Properties.OffText = "No"
        Me.ToggleOutWorkMins.Properties.OnText = "Yes"
        Me.ToggleOutWorkMins.Size = New System.Drawing.Size(95, 25)
        Me.ToggleOutWorkMins.TabIndex = 7
        '
        'LabelControl37
        '
        Me.LabelControl37.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl37.Appearance.Options.UseFont = True
        Me.LabelControl37.Location = New System.Drawing.Point(1, 310)
        Me.LabelControl37.Name = "LabelControl37"
        Me.LabelControl37.Size = New System.Drawing.Size(139, 14)
        Me.LabelControl37.TabIndex = 29
        Me.LabelControl37.Text = "Max Late Arrival Duration "
        Me.LabelControl37.Visible = False
        '
        'ToggleWeekOffIncludeInDUtyRoster
        '
        Me.ToggleWeekOffIncludeInDUtyRoster.Location = New System.Drawing.Point(231, 62)
        Me.ToggleWeekOffIncludeInDUtyRoster.Name = "ToggleWeekOffIncludeInDUtyRoster"
        Me.ToggleWeekOffIncludeInDUtyRoster.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleWeekOffIncludeInDUtyRoster.Properties.Appearance.Options.UseFont = True
        Me.ToggleWeekOffIncludeInDUtyRoster.Properties.OffText = "No"
        Me.ToggleWeekOffIncludeInDUtyRoster.Properties.OnText = "Yes"
        Me.ToggleWeekOffIncludeInDUtyRoster.Size = New System.Drawing.Size(95, 25)
        Me.ToggleWeekOffIncludeInDUtyRoster.TabIndex = 3
        '
        'ToggleMarkMisAsAbsent
        '
        Me.ToggleMarkMisAsAbsent.Location = New System.Drawing.Point(233, 138)
        Me.ToggleMarkMisAsAbsent.Name = "ToggleMarkMisAsAbsent"
        Me.ToggleMarkMisAsAbsent.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleMarkMisAsAbsent.Properties.Appearance.Options.UseFont = True
        Me.ToggleMarkMisAsAbsent.Properties.OffText = "No"
        Me.ToggleMarkMisAsAbsent.Properties.OnText = "Yes"
        Me.ToggleMarkMisAsAbsent.Size = New System.Drawing.Size(95, 25)
        Me.ToggleMarkMisAsAbsent.TabIndex = 5
        '
        'ToggleMarkWOasAbsent
        '
        Me.ToggleMarkWOasAbsent.Location = New System.Drawing.Point(231, 93)
        Me.ToggleMarkWOasAbsent.Name = "ToggleMarkWOasAbsent"
        Me.ToggleMarkWOasAbsent.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleMarkWOasAbsent.Properties.Appearance.Options.UseFont = True
        Me.ToggleMarkWOasAbsent.Properties.OffText = "No"
        Me.ToggleMarkWOasAbsent.Properties.OnText = "Yes"
        Me.ToggleMarkWOasAbsent.Size = New System.Drawing.Size(95, 25)
        Me.ToggleMarkWOasAbsent.TabIndex = 4
        '
        'ToggleAutoShiftAllowed
        '
        Me.ToggleAutoShiftAllowed.Location = New System.Drawing.Point(232, 359)
        Me.ToggleAutoShiftAllowed.Name = "ToggleAutoShiftAllowed"
        Me.ToggleAutoShiftAllowed.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleAutoShiftAllowed.Properties.Appearance.Options.UseFont = True
        Me.ToggleAutoShiftAllowed.Properties.OffText = "No"
        Me.ToggleAutoShiftAllowed.Properties.OnText = "Yes"
        Me.ToggleAutoShiftAllowed.Size = New System.Drawing.Size(95, 25)
        Me.ToggleAutoShiftAllowed.TabIndex = 13
        Me.ToggleAutoShiftAllowed.Visible = False
        '
        'ToggleIsAutoAbsentAllowed
        '
        Me.ToggleIsAutoAbsentAllowed.Location = New System.Drawing.Point(231, 31)
        Me.ToggleIsAutoAbsentAllowed.Name = "ToggleIsAutoAbsentAllowed"
        Me.ToggleIsAutoAbsentAllowed.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleIsAutoAbsentAllowed.Properties.Appearance.Options.UseFont = True
        Me.ToggleIsAutoAbsentAllowed.Properties.OffText = "No"
        Me.ToggleIsAutoAbsentAllowed.Properties.OnText = "Yes"
        Me.ToggleIsAutoAbsentAllowed.Size = New System.Drawing.Size(95, 25)
        Me.ToggleIsAutoAbsentAllowed.TabIndex = 2
        '
        'LabelControl34
        '
        Me.LabelControl34.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl34.Appearance.Options.UseFont = True
        Me.LabelControl34.Location = New System.Drawing.Point(1, 364)
        Me.LabelControl34.Name = "LabelControl34"
        Me.LabelControl34.Size = New System.Drawing.Size(103, 14)
        Me.LabelControl34.TabIndex = 16
        Me.LabelControl34.Text = "Auto Shift Allowed"
        Me.LabelControl34.Visible = False
        '
        'LabelControl44
        '
        Me.LabelControl44.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl44.Appearance.Options.UseFont = True
        Me.LabelControl44.Location = New System.Drawing.Point(5, 205)
        Me.LabelControl44.Name = "LabelControl44"
        Me.LabelControl44.Size = New System.Drawing.Size(203, 14)
        Me.LabelControl44.TabIndex = 31
        Me.LabelControl44.Text = "Out Work Minus From Working Hours"
        '
        'LabelControl45
        '
        Me.LabelControl45.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl45.Appearance.Options.UseFont = True
        Me.LabelControl45.Location = New System.Drawing.Point(5, 143)
        Me.LabelControl45.Name = "LabelControl45"
        Me.LabelControl45.Size = New System.Drawing.Size(107, 14)
        Me.LabelControl45.TabIndex = 30
        Me.LabelControl45.Text = "Mark MIS as Absent"
        '
        'TxtMaxEarlyDeparture
        '
        Me.TxtMaxEarlyDeparture.EditValue = "240"
        Me.TxtMaxEarlyDeparture.Location = New System.Drawing.Point(232, 333)
        Me.TxtMaxEarlyDeparture.Name = "TxtMaxEarlyDeparture"
        Me.TxtMaxEarlyDeparture.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TxtMaxEarlyDeparture.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtMaxEarlyDeparture.Properties.Appearance.Options.UseFont = True
        Me.TxtMaxEarlyDeparture.Properties.Mask.EditMask = "[0-9]*"
        Me.TxtMaxEarlyDeparture.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TxtMaxEarlyDeparture.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtMaxEarlyDeparture.Properties.MaxLength = 3
        Me.TxtMaxEarlyDeparture.Size = New System.Drawing.Size(72, 20)
        Me.TxtMaxEarlyDeparture.TabIndex = 12
        Me.TxtMaxEarlyDeparture.Visible = False
        '
        'TxtNoOffpresentOnweekOff
        '
        Me.TxtNoOffpresentOnweekOff.EditValue = "3"
        Me.TxtNoOffpresentOnweekOff.Location = New System.Drawing.Point(231, 5)
        Me.TxtNoOffpresentOnweekOff.Name = "TxtNoOffpresentOnweekOff"
        Me.TxtNoOffpresentOnweekOff.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TxtNoOffpresentOnweekOff.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtNoOffpresentOnweekOff.Properties.Appearance.Options.UseFont = True
        Me.TxtNoOffpresentOnweekOff.Properties.Mask.EditMask = "[0-9]*"
        Me.TxtNoOffpresentOnweekOff.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TxtNoOffpresentOnweekOff.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtNoOffpresentOnweekOff.Properties.MaxLength = 1
        Me.TxtNoOffpresentOnweekOff.Size = New System.Drawing.Size(72, 20)
        Me.TxtNoOffpresentOnweekOff.TabIndex = 1
        '
        'LabelControlMarkWO
        '
        Me.LabelControlMarkWO.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControlMarkWO.Appearance.Options.UseFont = True
        Me.LabelControlMarkWO.Location = New System.Drawing.Point(5, 94)
        Me.LabelControlMarkWO.Name = "LabelControlMarkWO"
        Me.LabelControlMarkWO.Size = New System.Drawing.Size(352, 14)
        Me.LabelControlMarkWO.TabIndex = 28
        Me.LabelControlMarkWO.Text = "Mark WO as Absent when No of Present<No of Present for WO"
        '
        'TxtMaxWorkingMin
        '
        Me.TxtMaxWorkingMin.EditValue = "1020"
        Me.TxtMaxWorkingMin.Location = New System.Drawing.Point(232, 281)
        Me.TxtMaxWorkingMin.Name = "TxtMaxWorkingMin"
        Me.TxtMaxWorkingMin.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TxtMaxWorkingMin.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtMaxWorkingMin.Properties.Appearance.Options.UseFont = True
        Me.TxtMaxWorkingMin.Properties.Mask.EditMask = "[0-9]*"
        Me.TxtMaxWorkingMin.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TxtMaxWorkingMin.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtMaxWorkingMin.Properties.MaxLength = 4
        Me.TxtMaxWorkingMin.Size = New System.Drawing.Size(72, 20)
        Me.TxtMaxWorkingMin.TabIndex = 10
        Me.TxtMaxWorkingMin.Visible = False
        '
        'TxtMaxLateArrivalMin
        '
        Me.TxtMaxLateArrivalMin.EditValue = "240"
        Me.TxtMaxLateArrivalMin.Location = New System.Drawing.Point(232, 307)
        Me.TxtMaxLateArrivalMin.Name = "TxtMaxLateArrivalMin"
        Me.TxtMaxLateArrivalMin.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TxtMaxLateArrivalMin.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtMaxLateArrivalMin.Properties.Appearance.Options.UseFont = True
        Me.TxtMaxLateArrivalMin.Properties.Mask.EditMask = "[0-9]*"
        Me.TxtMaxLateArrivalMin.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TxtMaxLateArrivalMin.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtMaxLateArrivalMin.Properties.MaxLength = 3
        Me.TxtMaxLateArrivalMin.Size = New System.Drawing.Size(72, 20)
        Me.TxtMaxLateArrivalMin.TabIndex = 11
        Me.TxtMaxLateArrivalMin.Visible = False
        '
        'LabelControl47
        '
        Me.LabelControl47.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl47.Appearance.Options.UseFont = True
        Me.LabelControl47.Location = New System.Drawing.Point(5, 35)
        Me.LabelControl47.Name = "LabelControl47"
        Me.LabelControl47.Size = New System.Drawing.Size(130, 14)
        Me.LabelControl47.TabIndex = 25
        Me.LabelControl47.Text = "Is Auto Absent Allowed"
        '
        'LabelControl36
        '
        Me.LabelControl36.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl36.Appearance.Options.UseFont = True
        Me.LabelControl36.Location = New System.Drawing.Point(6, 67)
        Me.LabelControl36.Name = "LabelControl36"
        Me.LabelControl36.Size = New System.Drawing.Size(215, 14)
        Me.LabelControl36.TabIndex = 28
        Me.LabelControl36.Text = "Week Off include or not in Duty Roster"
        '
        'LabelControl46
        '
        Me.LabelControl46.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl46.Appearance.Options.UseFont = True
        Me.LabelControl46.Location = New System.Drawing.Point(5, 11)
        Me.LabelControl46.Name = "LabelControl46"
        Me.LabelControl46.Size = New System.Drawing.Size(120, 14)
        Me.LabelControl46.TabIndex = 16
        Me.LabelControl46.Text = "No of Present for WO"
        '
        'PanelControl1
        '
        Me.PanelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.PanelControl1.Controls.Add(Me.TogglePWAasPAA)
        Me.PanelControl1.Controls.Add(Me.LabelControl58)
        Me.PanelControl1.Controls.Add(Me.ToggleAWPasAAP)
        Me.PanelControl1.Controls.Add(Me.LabelControl57)
        Me.PanelControl1.Controls.Add(Me.ToggleWAasAA)
        Me.PanelControl1.Controls.Add(Me.LabelControl56)
        Me.PanelControl1.Controls.Add(Me.ToggleAWasAA)
        Me.PanelControl1.Controls.Add(Me.LabelControl55)
        Me.PanelControl1.Controls.Add(Me.ToggleIsPResentOnHLD)
        Me.PanelControl1.Controls.Add(Me.ToggleIsPresentOnWeekOff)
        Me.PanelControl1.Controls.Add(Me.TxtPermisLateMinAutoShift)
        Me.PanelControl1.Controls.Add(Me.TxtPermisEarlyMinAutoShift)
        Me.PanelControl1.Controls.Add(Me.TxtEndTimeForOutPunch)
        Me.PanelControl1.Controls.Add(Me.TxtInTimeForINPunch)
        Me.PanelControl1.Controls.Add(Me.TxtDuplicateCheck)
        Me.PanelControl1.Controls.Add(Me.LabelControl29)
        Me.PanelControl1.Controls.Add(Me.LabelControl30)
        Me.PanelControl1.Controls.Add(Me.LabelControl31)
        Me.PanelControl1.Controls.Add(Me.ToggleMarkAWAasAAA)
        Me.PanelControl1.Controls.Add(Me.LabelControlETOP)
        Me.PanelControl1.Controls.Add(Me.LabelControl32)
        Me.PanelControl1.Controls.Add(Me.LabelControl33)
        Me.PanelControl1.Controls.Add(Me.ToggleFourPunchNight)
        Me.PanelControl1.Controls.Add(Me.LabelControl38)
        Me.PanelControl1.Controls.Add(Me.LabelControl40)
        Me.PanelControl1.Controls.Add(Me.LabelControl48)
        Me.PanelControl1.Location = New System.Drawing.Point(3, 3)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(343, 416)
        Me.PanelControl1.TabIndex = 1
        '
        'TogglePWAasPAA
        '
        Me.TogglePWAasPAA.Location = New System.Drawing.Point(241, 375)
        Me.TogglePWAasPAA.Name = "TogglePWAasPAA"
        Me.TogglePWAasPAA.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TogglePWAasPAA.Properties.Appearance.Options.UseFont = True
        Me.TogglePWAasPAA.Properties.OffText = "No"
        Me.TogglePWAasPAA.Properties.OnText = "Yes"
        Me.TogglePWAasPAA.Properties.ValueOff = "N"
        Me.TogglePWAasPAA.Properties.ValueOn = "Y"
        Me.TogglePWAasPAA.Size = New System.Drawing.Size(95, 25)
        Me.TogglePWAasPAA.TabIndex = 13
        Me.TogglePWAasPAA.Visible = False
        '
        'LabelControl58
        '
        Me.LabelControl58.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl58.Appearance.Options.UseFont = True
        Me.LabelControl58.Location = New System.Drawing.Point(9, 380)
        Me.LabelControl58.Name = "LabelControl58"
        Me.LabelControl58.Size = New System.Drawing.Size(127, 14)
        Me.LabelControl58.TabIndex = 39
        Me.LabelControl58.Text = "Mark P W/H A as P A A"
        Me.LabelControl58.Visible = False
        '
        'ToggleAWPasAAP
        '
        Me.ToggleAWPasAAP.Location = New System.Drawing.Point(241, 344)
        Me.ToggleAWPasAAP.Name = "ToggleAWPasAAP"
        Me.ToggleAWPasAAP.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleAWPasAAP.Properties.Appearance.Options.UseFont = True
        Me.ToggleAWPasAAP.Properties.OffText = "No"
        Me.ToggleAWPasAAP.Properties.OnText = "Yes"
        Me.ToggleAWPasAAP.Properties.ValueOff = "N"
        Me.ToggleAWPasAAP.Properties.ValueOn = "Y"
        Me.ToggleAWPasAAP.Size = New System.Drawing.Size(95, 25)
        Me.ToggleAWPasAAP.TabIndex = 12
        Me.ToggleAWPasAAP.Visible = False
        '
        'LabelControl57
        '
        Me.LabelControl57.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl57.Appearance.Options.UseFont = True
        Me.LabelControl57.Location = New System.Drawing.Point(9, 349)
        Me.LabelControl57.Name = "LabelControl57"
        Me.LabelControl57.Size = New System.Drawing.Size(127, 14)
        Me.LabelControl57.TabIndex = 37
        Me.LabelControl57.Text = "Mark A W/H P as A A P"
        Me.LabelControl57.Visible = False
        '
        'ToggleWAasAA
        '
        Me.ToggleWAasAA.Location = New System.Drawing.Point(241, 313)
        Me.ToggleWAasAA.Name = "ToggleWAasAA"
        Me.ToggleWAasAA.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleWAasAA.Properties.Appearance.Options.UseFont = True
        Me.ToggleWAasAA.Properties.OffText = "No"
        Me.ToggleWAasAA.Properties.OnText = "Yes"
        Me.ToggleWAasAA.Properties.ValueOff = "N"
        Me.ToggleWAasAA.Properties.ValueOn = "Y"
        Me.ToggleWAasAA.Size = New System.Drawing.Size(95, 25)
        Me.ToggleWAasAA.TabIndex = 11
        '
        'LabelControl56
        '
        Me.LabelControl56.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl56.Appearance.Options.UseFont = True
        Me.LabelControl56.Location = New System.Drawing.Point(9, 318)
        Me.LabelControl56.Name = "LabelControl56"
        Me.LabelControl56.Size = New System.Drawing.Size(105, 14)
        Me.LabelControl56.TabIndex = 35
        Me.LabelControl56.Text = "Mark W/H A as A A"
        '
        'ToggleAWasAA
        '
        Me.ToggleAWasAA.Location = New System.Drawing.Point(241, 282)
        Me.ToggleAWasAA.Name = "ToggleAWasAA"
        Me.ToggleAWasAA.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleAWasAA.Properties.Appearance.Options.UseFont = True
        Me.ToggleAWasAA.Properties.OffText = "No"
        Me.ToggleAWasAA.Properties.OnText = "Yes"
        Me.ToggleAWasAA.Properties.ValueOff = "N"
        Me.ToggleAWasAA.Properties.ValueOn = "Y"
        Me.ToggleAWasAA.Size = New System.Drawing.Size(95, 25)
        Me.ToggleAWasAA.TabIndex = 10
        '
        'LabelControl55
        '
        Me.LabelControl55.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl55.Appearance.Options.UseFont = True
        Me.LabelControl55.Location = New System.Drawing.Point(9, 287)
        Me.LabelControl55.Name = "LabelControl55"
        Me.LabelControl55.Size = New System.Drawing.Size(105, 14)
        Me.LabelControl55.TabIndex = 33
        Me.LabelControl55.Text = "Mark A W/H as A A"
        '
        'ToggleIsPResentOnHLD
        '
        Me.ToggleIsPResentOnHLD.Location = New System.Drawing.Point(241, 220)
        Me.ToggleIsPResentOnHLD.Name = "ToggleIsPResentOnHLD"
        Me.ToggleIsPResentOnHLD.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleIsPResentOnHLD.Properties.Appearance.Options.UseFont = True
        Me.ToggleIsPResentOnHLD.Properties.OffText = "No"
        Me.ToggleIsPResentOnHLD.Properties.OnText = "Yes"
        Me.ToggleIsPResentOnHLD.Size = New System.Drawing.Size(95, 25)
        Me.ToggleIsPResentOnHLD.TabIndex = 8
        '
        'ToggleIsPresentOnWeekOff
        '
        Me.ToggleIsPresentOnWeekOff.Location = New System.Drawing.Point(241, 189)
        Me.ToggleIsPresentOnWeekOff.Name = "ToggleIsPresentOnWeekOff"
        Me.ToggleIsPresentOnWeekOff.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleIsPresentOnWeekOff.Properties.Appearance.Options.UseFont = True
        Me.ToggleIsPresentOnWeekOff.Properties.OffText = "No"
        Me.ToggleIsPresentOnWeekOff.Properties.OnText = "Yes"
        Me.ToggleIsPresentOnWeekOff.Size = New System.Drawing.Size(95, 25)
        Me.ToggleIsPresentOnWeekOff.TabIndex = 7
        '
        'TxtPermisLateMinAutoShift
        '
        Me.TxtPermisLateMinAutoShift.EditValue = "240"
        Me.TxtPermisLateMinAutoShift.Location = New System.Drawing.Point(241, 163)
        Me.TxtPermisLateMinAutoShift.Name = "TxtPermisLateMinAutoShift"
        Me.TxtPermisLateMinAutoShift.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TxtPermisLateMinAutoShift.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtPermisLateMinAutoShift.Properties.Appearance.Options.UseFont = True
        Me.TxtPermisLateMinAutoShift.Properties.Mask.EditMask = "[0-9]*"
        Me.TxtPermisLateMinAutoShift.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TxtPermisLateMinAutoShift.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtPermisLateMinAutoShift.Properties.MaxLength = 3
        Me.TxtPermisLateMinAutoShift.Size = New System.Drawing.Size(72, 20)
        Me.TxtPermisLateMinAutoShift.TabIndex = 6
        '
        'TxtPermisEarlyMinAutoShift
        '
        Me.TxtPermisEarlyMinAutoShift.EditValue = "240"
        Me.TxtPermisEarlyMinAutoShift.Location = New System.Drawing.Point(241, 137)
        Me.TxtPermisEarlyMinAutoShift.Name = "TxtPermisEarlyMinAutoShift"
        Me.TxtPermisEarlyMinAutoShift.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TxtPermisEarlyMinAutoShift.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtPermisEarlyMinAutoShift.Properties.Appearance.Options.UseFont = True
        Me.TxtPermisEarlyMinAutoShift.Properties.Mask.EditMask = "[0-9]*"
        Me.TxtPermisEarlyMinAutoShift.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TxtPermisEarlyMinAutoShift.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtPermisEarlyMinAutoShift.Properties.MaxLength = 3
        Me.TxtPermisEarlyMinAutoShift.Size = New System.Drawing.Size(72, 20)
        Me.TxtPermisEarlyMinAutoShift.TabIndex = 5
        '
        'TxtEndTimeForOutPunch
        '
        Me.TxtEndTimeForOutPunch.EditValue = "05:00"
        Me.TxtEndTimeForOutPunch.Location = New System.Drawing.Point(241, 92)
        Me.TxtEndTimeForOutPunch.Name = "TxtEndTimeForOutPunch"
        Me.TxtEndTimeForOutPunch.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtEndTimeForOutPunch.Properties.Appearance.Options.UseFont = True
        Me.TxtEndTimeForOutPunch.Properties.Mask.EditMask = "HH:mm"
        Me.TxtEndTimeForOutPunch.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TxtEndTimeForOutPunch.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtEndTimeForOutPunch.Properties.MaxLength = 5
        Me.TxtEndTimeForOutPunch.Size = New System.Drawing.Size(72, 20)
        Me.TxtEndTimeForOutPunch.TabIndex = 4
        '
        'TxtInTimeForINPunch
        '
        Me.TxtInTimeForINPunch.EditValue = "05:00"
        Me.TxtInTimeForINPunch.Location = New System.Drawing.Point(241, 66)
        Me.TxtInTimeForINPunch.Name = "TxtInTimeForINPunch"
        Me.TxtInTimeForINPunch.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtInTimeForINPunch.Properties.Appearance.Options.UseFont = True
        Me.TxtInTimeForINPunch.Properties.Mask.EditMask = "HH:mm"
        Me.TxtInTimeForINPunch.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TxtInTimeForINPunch.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtInTimeForINPunch.Properties.MaxLength = 5
        Me.TxtInTimeForINPunch.Size = New System.Drawing.Size(72, 20)
        Me.TxtInTimeForINPunch.TabIndex = 3
        '
        'TxtDuplicateCheck
        '
        Me.TxtDuplicateCheck.EditValue = "5"
        Me.TxtDuplicateCheck.Location = New System.Drawing.Point(241, 9)
        Me.TxtDuplicateCheck.Name = "TxtDuplicateCheck"
        Me.TxtDuplicateCheck.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TxtDuplicateCheck.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtDuplicateCheck.Properties.Appearance.Options.UseFont = True
        Me.TxtDuplicateCheck.Properties.Mask.EditMask = "[0-9]*"
        Me.TxtDuplicateCheck.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TxtDuplicateCheck.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtDuplicateCheck.Properties.MaxLength = 2
        Me.TxtDuplicateCheck.Size = New System.Drawing.Size(72, 20)
        Me.TxtDuplicateCheck.TabIndex = 1
        '
        'LabelControl29
        '
        Me.LabelControl29.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl29.Appearance.Options.UseFont = True
        Me.LabelControl29.Location = New System.Drawing.Point(8, 40)
        Me.LabelControl29.Name = "LabelControl29"
        Me.LabelControl29.Size = New System.Drawing.Size(137, 14)
        Me.LabelControl29.TabIndex = 3
        Me.LabelControl29.Text = "Four Punch in Night Shift"
        '
        'LabelControl30
        '
        Me.LabelControl30.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl30.Appearance.Options.UseFont = True
        Me.LabelControl30.Location = New System.Drawing.Point(9, 69)
        Me.LabelControl30.Name = "LabelControl30"
        Me.LabelControl30.Size = New System.Drawing.Size(125, 14)
        Me.LabelControl30.TabIndex = 5
        Me.LabelControl30.Text = "End Time for IN punch"
        '
        'LabelControl31
        '
        Me.LabelControl31.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl31.Appearance.Options.UseFont = True
        Me.LabelControl31.Location = New System.Drawing.Point(11, 225)
        Me.LabelControl31.Name = "LabelControl31"
        Me.LabelControl31.Size = New System.Drawing.Size(147, 14)
        Me.LabelControl31.TabIndex = 31
        Me.LabelControl31.Text = "Is Present On HLD Present"
        '
        'ToggleMarkAWAasAAA
        '
        Me.ToggleMarkAWAasAAA.Location = New System.Drawing.Point(241, 251)
        Me.ToggleMarkAWAasAAA.Name = "ToggleMarkAWAasAAA"
        Me.ToggleMarkAWAasAAA.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleMarkAWAasAAA.Properties.Appearance.Options.UseFont = True
        Me.ToggleMarkAWAasAAA.Properties.OffText = "No"
        Me.ToggleMarkAWAasAAA.Properties.OnText = "Yes"
        Me.ToggleMarkAWAasAAA.Properties.ValueOff = "N"
        Me.ToggleMarkAWAasAAA.Properties.ValueOn = "Y"
        Me.ToggleMarkAWAasAAA.Size = New System.Drawing.Size(95, 25)
        Me.ToggleMarkAWAasAAA.TabIndex = 9
        '
        'LabelControlETOP
        '
        Me.LabelControlETOP.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControlETOP.Appearance.Options.UseFont = True
        Me.LabelControlETOP.Appearance.Options.UseTextOptions = True
        Me.LabelControlETOP.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.LabelControlETOP.Location = New System.Drawing.Point(9, 92)
        Me.LabelControlETOP.Name = "LabelControlETOP"
        Me.LabelControlETOP.Size = New System.Drawing.Size(413, 14)
        Me.LabelControlETOP.TabIndex = 7
        Me.LabelControlETOP.Text = "End Time for Out punch(Next Date) for RTC Employee with Multiple Punch"
        '
        'LabelControl32
        '
        Me.LabelControl32.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl32.Appearance.Options.UseFont = True
        Me.LabelControl32.Location = New System.Drawing.Point(8, 12)
        Me.LabelControl32.Name = "LabelControl32"
        Me.LabelControl32.Size = New System.Drawing.Size(109, 14)
        Me.LabelControl32.TabIndex = 2
        Me.LabelControl32.Text = "Duplicate Check Min"
        '
        'LabelControl33
        '
        Me.LabelControl33.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl33.Appearance.Options.UseFont = True
        Me.LabelControl33.Location = New System.Drawing.Point(11, 194)
        Me.LabelControl33.Name = "LabelControl33"
        Me.LabelControl33.Size = New System.Drawing.Size(146, 14)
        Me.LabelControl33.TabIndex = 30
        Me.LabelControl33.Text = "Is Present On WO Present"
        '
        'ToggleFourPunchNight
        '
        Me.ToggleFourPunchNight.Location = New System.Drawing.Point(241, 35)
        Me.ToggleFourPunchNight.Name = "ToggleFourPunchNight"
        Me.ToggleFourPunchNight.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleFourPunchNight.Properties.Appearance.Options.UseFont = True
        Me.ToggleFourPunchNight.Properties.OffText = "No"
        Me.ToggleFourPunchNight.Properties.OnText = "Yes"
        Me.ToggleFourPunchNight.Size = New System.Drawing.Size(95, 25)
        Me.ToggleFourPunchNight.TabIndex = 2
        '
        'LabelControl38
        '
        Me.LabelControl38.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl38.Appearance.Options.UseFont = True
        Me.LabelControl38.Location = New System.Drawing.Point(8, 140)
        Me.LabelControl38.Name = "LabelControl38"
        Me.LabelControl38.Size = New System.Drawing.Size(146, 14)
        Me.LabelControl38.TabIndex = 25
        Me.LabelControl38.Text = "Permis Early Min Auto Shift"
        '
        'LabelControl40
        '
        Me.LabelControl40.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl40.Appearance.Options.UseFont = True
        Me.LabelControl40.Location = New System.Drawing.Point(9, 166)
        Me.LabelControl40.Name = "LabelControl40"
        Me.LabelControl40.Size = New System.Drawing.Size(145, 14)
        Me.LabelControl40.TabIndex = 27
        Me.LabelControl40.Text = "Permis Late Min Auto Shift"
        '
        'LabelControl48
        '
        Me.LabelControl48.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl48.Appearance.Options.UseFont = True
        Me.LabelControl48.Location = New System.Drawing.Point(9, 256)
        Me.LabelControl48.Name = "LabelControl48"
        Me.LabelControl48.Size = New System.Drawing.Size(129, 14)
        Me.LabelControl48.TabIndex = 27
        Me.LabelControl48.Text = "Mark A W/H A as A A A"
        '
        'TabNavigationPage4
        '
        Me.TabNavigationPage4.Caption = "Over Time Setting"
        Me.TabNavigationPage4.Controls.Add(Me.GroupControl11)
        Me.TabNavigationPage4.Controls.Add(Me.GroupControl10)
        Me.TabNavigationPage4.Controls.Add(Me.ToggleWhetherOTinMinus)
        Me.TabNavigationPage4.Controls.Add(Me.GroupControl9)
        Me.TabNavigationPage4.Controls.Add(Me.LabelControl43)
        Me.TabNavigationPage4.Controls.Add(Me.GroupControl8)
        Me.TabNavigationPage4.Name = "TabNavigationPage4"
        Me.TabNavigationPage4.Size = New System.Drawing.Size(704, 469)
        '
        'GroupControl11
        '
        Me.GroupControl11.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl11.AppearanceCaption.Options.UseFont = True
        Me.GroupControl11.Controls.Add(Me.TxtOtRestrictDur)
        Me.GroupControl11.Controls.Add(Me.LabelControl50)
        Me.GroupControl11.Controls.Add(Me.TxtOtLateDur)
        Me.GroupControl11.Controls.Add(Me.TxtOtEarlyDur)
        Me.GroupControl11.Controls.Add(Me.LabelControl51)
        Me.GroupControl11.Controls.Add(Me.LabelControl52)
        Me.GroupControl11.Location = New System.Drawing.Point(342, 14)
        Me.GroupControl11.Name = "GroupControl11"
        Me.GroupControl11.Size = New System.Drawing.Size(335, 111)
        Me.GroupControl11.TabIndex = 8
        Me.GroupControl11.Text = "OT Durations"
        '
        'TxtOtRestrictDur
        '
        Me.TxtOtRestrictDur.EditValue = "0"
        Me.TxtOtRestrictDur.Location = New System.Drawing.Point(232, 76)
        Me.TxtOtRestrictDur.Name = "TxtOtRestrictDur"
        Me.TxtOtRestrictDur.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TxtOtRestrictDur.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtOtRestrictDur.Properties.Appearance.Options.UseFont = True
        Me.TxtOtRestrictDur.Properties.Mask.EditMask = "[0-9]*"
        Me.TxtOtRestrictDur.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TxtOtRestrictDur.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtOtRestrictDur.Properties.MaxLength = 3
        Me.TxtOtRestrictDur.Size = New System.Drawing.Size(72, 20)
        Me.TxtOtRestrictDur.TabIndex = 3
        '
        'LabelControl50
        '
        Me.LabelControl50.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl50.Appearance.Options.UseFont = True
        Me.LabelControl50.Location = New System.Drawing.Point(6, 79)
        Me.LabelControl50.Name = "LabelControl50"
        Me.LabelControl50.Size = New System.Drawing.Size(137, 14)
        Me.LabelControl50.TabIndex = 69
        Me.LabelControl50.Text = "OT Restrict End Duration"
        '
        'TxtOtLateDur
        '
        Me.TxtOtLateDur.EditValue = "0"
        Me.TxtOtLateDur.Location = New System.Drawing.Point(232, 50)
        Me.TxtOtLateDur.Name = "TxtOtLateDur"
        Me.TxtOtLateDur.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TxtOtLateDur.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtOtLateDur.Properties.Appearance.Options.UseFont = True
        Me.TxtOtLateDur.Properties.Mask.EditMask = "[0-9]*"
        Me.TxtOtLateDur.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TxtOtLateDur.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtOtLateDur.Properties.MaxLength = 3
        Me.TxtOtLateDur.Size = New System.Drawing.Size(72, 20)
        Me.TxtOtLateDur.TabIndex = 2
        '
        'TxtOtEarlyDur
        '
        Me.TxtOtEarlyDur.EditValue = "0"
        Me.TxtOtEarlyDur.Location = New System.Drawing.Point(232, 24)
        Me.TxtOtEarlyDur.Name = "TxtOtEarlyDur"
        Me.TxtOtEarlyDur.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TxtOtEarlyDur.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtOtEarlyDur.Properties.Appearance.Options.UseFont = True
        Me.TxtOtEarlyDur.Properties.Mask.EditMask = "[0-9]*"
        Me.TxtOtEarlyDur.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TxtOtEarlyDur.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtOtEarlyDur.Properties.MaxLength = 3
        Me.TxtOtEarlyDur.Size = New System.Drawing.Size(72, 20)
        Me.TxtOtEarlyDur.TabIndex = 1
        '
        'LabelControl51
        '
        Me.LabelControl51.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl51.Appearance.Options.UseFont = True
        Me.LabelControl51.Location = New System.Drawing.Point(6, 55)
        Me.LabelControl51.Name = "LabelControl51"
        Me.LabelControl51.Size = New System.Drawing.Size(139, 14)
        Me.LabelControl51.TabIndex = 64
        Me.LabelControl51.Text = "OT Late Coming Duration"
        '
        'LabelControl52
        '
        Me.LabelControl52.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl52.Appearance.Options.UseFont = True
        Me.LabelControl52.Location = New System.Drawing.Point(6, 30)
        Me.LabelControl52.Name = "LabelControl52"
        Me.LabelControl52.Size = New System.Drawing.Size(140, 14)
        Me.LabelControl52.TabIndex = 62
        Me.LabelControl52.Text = "OT Early Coming Duration"
        '
        'GroupControl10
        '
        Me.GroupControl10.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl10.AppearanceCaption.Options.UseFont = True
        Me.GroupControl10.Controls.Add(Me.TxtDeductOTinWO)
        Me.GroupControl10.Controls.Add(Me.TxtDeductOTinHLD)
        Me.GroupControl10.Controls.Add(Me.LabelControl41)
        Me.GroupControl10.Controls.Add(Me.LabelControl49)
        Me.GroupControl10.Location = New System.Drawing.Point(14, 241)
        Me.GroupControl10.Name = "GroupControl10"
        Me.GroupControl10.Size = New System.Drawing.Size(322, 85)
        Me.GroupControl10.TabIndex = 7
        Me.GroupControl10.Text = "OT Deductions"
        '
        'TxtDeductOTinWO
        '
        Me.TxtDeductOTinWO.EditValue = "0"
        Me.TxtDeductOTinWO.Location = New System.Drawing.Point(217, 50)
        Me.TxtDeductOTinWO.Name = "TxtDeductOTinWO"
        Me.TxtDeductOTinWO.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TxtDeductOTinWO.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtDeductOTinWO.Properties.Appearance.Options.UseFont = True
        Me.TxtDeductOTinWO.Properties.Mask.EditMask = "[0-9]*"
        Me.TxtDeductOTinWO.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TxtDeductOTinWO.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtDeductOTinWO.Properties.MaxLength = 2
        Me.TxtDeductOTinWO.Size = New System.Drawing.Size(72, 20)
        Me.TxtDeductOTinWO.TabIndex = 2
        '
        'TxtDeductOTinHLD
        '
        Me.TxtDeductOTinHLD.EditValue = "0"
        Me.TxtDeductOTinHLD.Location = New System.Drawing.Point(217, 24)
        Me.TxtDeductOTinHLD.Name = "TxtDeductOTinHLD"
        Me.TxtDeductOTinHLD.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TxtDeductOTinHLD.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TxtDeductOTinHLD.Properties.Appearance.Options.UseFont = True
        Me.TxtDeductOTinHLD.Properties.Mask.EditMask = "[0-9]*"
        Me.TxtDeductOTinHLD.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TxtDeductOTinHLD.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TxtDeductOTinHLD.Properties.MaxLength = 2
        Me.TxtDeductOTinHLD.Size = New System.Drawing.Size(72, 20)
        Me.TxtDeductOTinHLD.TabIndex = 1
        '
        'LabelControl41
        '
        Me.LabelControl41.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl41.Appearance.Options.UseFont = True
        Me.LabelControl41.Location = New System.Drawing.Point(6, 55)
        Me.LabelControl41.Name = "LabelControl41"
        Me.LabelControl41.Size = New System.Drawing.Size(104, 14)
        Me.LabelControl41.TabIndex = 64
        Me.LabelControl41.Text = "Deduct OT on WO"
        '
        'LabelControl49
        '
        Me.LabelControl49.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl49.Appearance.Options.UseFont = True
        Me.LabelControl49.Location = New System.Drawing.Point(6, 30)
        Me.LabelControl49.Name = "LabelControl49"
        Me.LabelControl49.Size = New System.Drawing.Size(100, 14)
        Me.LabelControl49.TabIndex = 62
        Me.LabelControl49.Text = "Deduct OT in HLD"
        '
        'GroupControl9
        '
        Me.GroupControl9.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl9.AppearanceCaption.Options.UseFont = True
        Me.GroupControl9.Controls.Add(Me.ToggleRoundOverTime)
        Me.GroupControl9.Controls.Add(Me.LabelControl28)
        Me.GroupControl9.Controls.Add(Me.ToggleOTisAllowedInEarlyComing)
        Me.GroupControl9.Controls.Add(Me.LabelControl42)
        Me.GroupControl9.Location = New System.Drawing.Point(14, 131)
        Me.GroupControl9.Name = "GroupControl9"
        Me.GroupControl9.Size = New System.Drawing.Size(322, 104)
        Me.GroupControl9.TabIndex = 6
        Me.GroupControl9.Text = "OT Parameter Options"
        '
        'ToggleRoundOverTime
        '
        Me.ToggleRoundOverTime.Location = New System.Drawing.Point(217, 60)
        Me.ToggleRoundOverTime.Name = "ToggleRoundOverTime"
        Me.ToggleRoundOverTime.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleRoundOverTime.Properties.Appearance.Options.UseFont = True
        Me.ToggleRoundOverTime.Properties.OffText = "No"
        Me.ToggleRoundOverTime.Properties.OnText = "Yes"
        Me.ToggleRoundOverTime.Size = New System.Drawing.Size(95, 25)
        Me.ToggleRoundOverTime.TabIndex = 3
        '
        'LabelControl28
        '
        Me.LabelControl28.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl28.Appearance.Options.UseFont = True
        Me.LabelControl28.Location = New System.Drawing.Point(6, 61)
        Me.LabelControl28.Name = "LabelControl28"
        Me.LabelControl28.Size = New System.Drawing.Size(96, 14)
        Me.LabelControl28.TabIndex = 66
        Me.LabelControl28.Text = "Round Over Time"
        '
        'ToggleWhetherOTinMinus
        '
        Me.ToggleWhetherOTinMinus.Location = New System.Drawing.Point(574, 150)
        Me.ToggleWhetherOTinMinus.Name = "ToggleWhetherOTinMinus"
        Me.ToggleWhetherOTinMinus.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleWhetherOTinMinus.Properties.Appearance.Options.UseFont = True
        Me.ToggleWhetherOTinMinus.Properties.OffText = "No"
        Me.ToggleWhetherOTinMinus.Properties.OnText = "Yes"
        Me.ToggleWhetherOTinMinus.Size = New System.Drawing.Size(95, 25)
        Me.ToggleWhetherOTinMinus.TabIndex = 2
        Me.ToggleWhetherOTinMinus.Visible = False
        '
        'LabelControl43
        '
        Me.LabelControl43.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl43.Appearance.Options.UseFont = True
        Me.LabelControl43.Location = New System.Drawing.Point(363, 150)
        Me.LabelControl43.Name = "LabelControl43"
        Me.LabelControl43.Size = New System.Drawing.Size(185, 14)
        Me.LabelControl43.TabIndex = 64
        Me.LabelControl43.Text = "Whether OT in Minus ( - ) Figures"
        Me.LabelControl43.Visible = False
        '
        'ToggleOTisAllowedInEarlyComing
        '
        Me.ToggleOTisAllowedInEarlyComing.Location = New System.Drawing.Point(217, 29)
        Me.ToggleOTisAllowedInEarlyComing.Name = "ToggleOTisAllowedInEarlyComing"
        Me.ToggleOTisAllowedInEarlyComing.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleOTisAllowedInEarlyComing.Properties.Appearance.Options.UseFont = True
        Me.ToggleOTisAllowedInEarlyComing.Properties.OffText = "No"
        Me.ToggleOTisAllowedInEarlyComing.Properties.OnText = "Yes"
        Me.ToggleOTisAllowedInEarlyComing.Size = New System.Drawing.Size(95, 25)
        Me.ToggleOTisAllowedInEarlyComing.TabIndex = 1
        '
        'LabelControl42
        '
        Me.LabelControl42.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl42.Appearance.Options.UseFont = True
        Me.LabelControl42.Location = New System.Drawing.Point(6, 30)
        Me.LabelControl42.Name = "LabelControl42"
        Me.LabelControl42.Size = New System.Drawing.Size(197, 14)
        Me.LabelControl42.TabIndex = 62
        Me.LabelControl42.Text = "OT is allowed incase of early coming"
        '
        'GroupControl8
        '
        Me.GroupControl8.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl8.AppearanceCaption.Options.UseFont = True
        Me.GroupControl8.Controls.Add(Me.CheckEditOT3)
        Me.GroupControl8.Controls.Add(Me.CheckEditOT2)
        Me.GroupControl8.Controls.Add(Me.CheckEditOT1)
        Me.GroupControl8.Location = New System.Drawing.Point(14, 14)
        Me.GroupControl8.Name = "GroupControl8"
        Me.GroupControl8.Size = New System.Drawing.Size(322, 111)
        Me.GroupControl8.TabIndex = 5
        Me.GroupControl8.Text = " OT Options"
        '
        'CheckEditOT3
        '
        Me.CheckEditOT3.Location = New System.Drawing.Point(6, 76)
        Me.CheckEditOT3.Name = "CheckEditOT3"
        Me.CheckEditOT3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditOT3.Properties.Appearance.Options.UseFont = True
        Me.CheckEditOT3.Properties.Caption = "OT = Early Coming + Late Dep"
        Me.CheckEditOT3.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEditOT3.Properties.RadioGroupIndex = 0
        Me.CheckEditOT3.Size = New System.Drawing.Size(213, 19)
        Me.CheckEditOT3.TabIndex = 2
        Me.CheckEditOT3.TabStop = False
        '
        'CheckEditOT2
        '
        Me.CheckEditOT2.EditValue = True
        Me.CheckEditOT2.Location = New System.Drawing.Point(6, 51)
        Me.CheckEditOT2.Name = "CheckEditOT2"
        Me.CheckEditOT2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditOT2.Properties.Appearance.Options.UseFont = True
        Me.CheckEditOT2.Properties.Caption = "OT = Working Hrs - ShiftHrs "
        Me.CheckEditOT2.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEditOT2.Properties.RadioGroupIndex = 0
        Me.CheckEditOT2.Size = New System.Drawing.Size(213, 19)
        Me.CheckEditOT2.TabIndex = 1
        '
        'CheckEditOT1
        '
        Me.CheckEditOT1.Location = New System.Drawing.Point(6, 26)
        Me.CheckEditOT1.Name = "CheckEditOT1"
        Me.CheckEditOT1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditOT1.Properties.Appearance.Options.UseFont = True
        Me.CheckEditOT1.Properties.Caption = "OT = OutTime - ShiftEndTime"
        Me.CheckEditOT1.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEditOT1.Properties.RadioGroupIndex = 0
        Me.CheckEditOT1.Size = New System.Drawing.Size(213, 19)
        Me.CheckEditOT1.TabIndex = 0
        Me.CheckEditOT1.TabStop = False
        '
        'SimpleButtonSave
        '
        Me.SimpleButtonSave.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonSave.Appearance.Options.UseFont = True
        Me.SimpleButtonSave.Location = New System.Drawing.Point(538, 473)
        Me.SimpleButtonSave.Name = "SimpleButtonSave"
        Me.SimpleButtonSave.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButtonSave.TabIndex = 6
        Me.SimpleButtonSave.Text = "Save"
        '
        'SimpleButtonCancel
        '
        Me.SimpleButtonCancel.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonCancel.Appearance.Options.UseFont = True
        Me.SimpleButtonCancel.Location = New System.Drawing.Point(626, 473)
        Me.SimpleButtonCancel.Name = "SimpleButtonCancel"
        Me.SimpleButtonCancel.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButtonCancel.TabIndex = 7
        Me.SimpleButtonCancel.Text = "Cancel"
        '
        'TblShiftMasterTableAdapter
        '
        Me.TblShiftMasterTableAdapter.ClearBeforeFill = True
        '
        'TblShiftMaster1TableAdapter1
        '
        Me.TblShiftMaster1TableAdapter1.ClearBeforeFill = True
        '
        'XtraEmpGrpEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(724, 511)
        Me.Controls.Add(Me.PopupContainerControl2)
        Me.Controls.Add(Me.PopupContainerControl1)
        Me.Controls.Add(Me.SimpleButtonCancel)
        Me.Controls.Add(Me.SimpleButtonSave)
        Me.Controls.Add(Me.TabPane1)
        Me.LookAndFeel.SkinName = "iMaginary"
        Me.LookAndFeel.UseDefaultLookAndFeel = False
        Me.Name = "XtraEmpGrpEdit"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        CType(Me.TabPane1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPane1.ResumeLayout(False)
        Me.TabNavigationPage1.ResumeLayout(False)
        Me.TabNavigationPage1.PerformLayout()
        CType(Me.ToggleSwitch6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl4.ResumeLayout(False)
        Me.GroupControl4.PerformLayout()
        CType(Me.TextEdit9.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleSwitch5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        CType(Me.CheckEdit7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEdit6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        CType(Me.CheckEdit5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEdit4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEdit3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.TextEdit8.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit7.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit6.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleSwitch4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleSwitch3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleSwitch2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleSwitch1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabNavigationPage2.ResumeLayout(False)
        CType(Me.GroupControl6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl6.ResumeLayout(False)
        Me.GroupControl6.PerformLayout()
        CType(Me.GroupControl7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl7.ResumeLayout(False)
        CType(Me.CheckEdit12.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEdit11.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEdit10.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEdit9.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEdit8.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEdit4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEdit3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblShiftMasterBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl5.ResumeLayout(False)
        Me.GroupControl5.PerformLayout()
        CType(Me.ToggleAllowCmpOffMlt.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEditMultiShif.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEdit4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEdit3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditRotation.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit11.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit10.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PopupContainerControl2.ResumeLayout(False)
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTimeEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleSwitchAutoShift.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PopupContainerControl1.ResumeLayout(False)
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTimeEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEditDefaltShift.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEdit1View, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxShiftType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabNavigationPage3.ResumeLayout(False)
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl2.ResumeLayout(False)
        Me.PanelControl2.PerformLayout()
        CType(Me.ToggleCmpPOWPOH.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleSwitchMarkHlf.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleOutWorkMins.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleWeekOffIncludeInDUtyRoster.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleMarkMisAsAbsent.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleMarkWOasAbsent.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleAutoShiftAllowed.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleIsAutoAbsentAllowed.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtMaxEarlyDeparture.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtNoOffpresentOnweekOff.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtMaxWorkingMin.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtMaxLateArrivalMin.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.TogglePWAasPAA.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleAWPasAAP.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleWAasAA.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleAWasAA.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleIsPResentOnHLD.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleIsPresentOnWeekOff.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtPermisLateMinAutoShift.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtPermisEarlyMinAutoShift.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtEndTimeForOutPunch.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtInTimeForINPunch.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtDuplicateCheck.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleMarkAWAasAAA.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleFourPunchNight.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabNavigationPage4.ResumeLayout(False)
        Me.TabNavigationPage4.PerformLayout()
        CType(Me.GroupControl11, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl11.ResumeLayout(False)
        Me.GroupControl11.PerformLayout()
        CType(Me.TxtOtRestrictDur.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtOtLateDur.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtOtEarlyDur.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl10, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl10.ResumeLayout(False)
        Me.GroupControl10.PerformLayout()
        CType(Me.TxtDeductOTinWO.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TxtDeductOTinHLD.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl9, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl9.ResumeLayout(False)
        Me.GroupControl9.PerformLayout()
        CType(Me.ToggleRoundOverTime.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleWhetherOTinMinus.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleOTisAllowedInEarlyComing.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl8, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl8.ResumeLayout(False)
        CType(Me.CheckEditOT3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditOT2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditOT1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TabPane1 As DevExpress.XtraBars.Navigation.TabPane
    Friend WithEvents TabNavigationPage1 As DevExpress.XtraBars.Navigation.TabNavigationPage
    Friend WithEvents TabNavigationPage2 As DevExpress.XtraBars.Navigation.TabNavigationPage
    Friend WithEvents TextEdit1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEdit2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEdit3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents TextEdit5 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ToggleSwitch1 As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ToggleSwitch2 As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ToggleSwitch3 As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ToggleSwitch4 As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEdit8 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit7 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit6 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CheckEdit5 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit4 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit3 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit2 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit1 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CheckEdit6 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit7 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents GroupControl4 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents TextEdit9 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ToggleSwitch5 As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ToggleSwitch6 As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents SimpleButtonSave As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButtonCancel As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GroupControl6 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupControl5 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents ComboBoxShiftType As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SSSDBDataSet As iAS.SSSDBDataSet
    Friend WithEvents TblShiftMasterBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblShiftMasterTableAdapter As iAS.SSSDBDataSetTableAdapters.tblShiftMasterTableAdapter
    Friend WithEvents GridLookUpEditDefaltShift As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridLookUpEdit1View As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colSHIFT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSTARTTIME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colENDTIME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLUNCHTIME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLUNCHDURATION As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLUNCHENDTIME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colORDERINF12 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colOTSTARTAFTER As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colOTDEDUCTHRS As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLUNCHDEDUCTION As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSHIFTPOSITION As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSHIFTDURATION As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colOTDEDUCTAFTER As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLastModifiedBy As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLastModifiedDate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LabelControl18 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PopupContainerEdit1 As DevExpress.XtraEditors.PopupContainerEdit
    Friend WithEvents PopupContainerControl1 As DevExpress.XtraEditors.PopupContainerControl
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colSHIFT1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSTARTTIME1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colENDTIME1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLUNCHTIME1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLUNCHDURATION1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLUNCHENDTIME1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colORDERINF121 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colOTSTARTAFTER1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colOTDEDUCTHRS1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLUNCHDEDUCTION1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSHIFTPOSITION1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSHIFTDURATION1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colOTDEDUCTAFTER1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLastModifiedBy1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLastModifiedDate1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemTimeEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit
    Friend WithEvents PopupContainerEdit2 As DevExpress.XtraEditors.PopupContainerEdit
    Friend WithEvents LabelControl20 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ToggleSwitchAutoShift As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents LabelControl19 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PopupContainerControl2 As DevExpress.XtraEditors.PopupContainerControl
    Friend WithEvents GridControl2 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemTimeEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn8 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn9 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn10 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn11 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn12 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn13 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn14 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn15 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents TextEdit11 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEdit10 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl22 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl21 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ComboBoxEdit2 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents GridLookUpEdit2 As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridView3 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn16 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn17 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn18 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn19 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn20 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn21 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn22 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn23 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn24 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn25 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn26 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn27 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn28 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn29 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn30 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LabelControl26 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl25 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl24 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl23 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ComboBoxEdit3 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboBoxEdit4 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents GroupControl7 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CheckEdit12 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit11 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit10 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit9 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit8 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents TblShiftMaster1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.tblShiftMaster1TableAdapter
    Friend WithEvents TextEditRotation As DevExpress.XtraEditors.TextEdit
    Friend WithEvents GridLookUpEdit3 As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridView4 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn31 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn32 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn33 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn34 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn35 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn36 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn37 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn38 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn39 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn40 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn41 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn42 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn43 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn44 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn45 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents SimpleButton4 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GridLookUpEdit4 As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridView5 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn46 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn47 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn48 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn49 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn50 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn51 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn52 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn53 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn54 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn55 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn56 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn57 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn58 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn59 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn60 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents SimpleButton3 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents TabNavigationPage3 As DevExpress.XtraBars.Navigation.TabNavigationPage
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents ToggleIsPResentOnHLD As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents ToggleIsPresentOnWeekOff As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents ToggleWeekOffIncludeInDUtyRoster As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents TxtPermisLateMinAutoShift As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtPermisEarlyMinAutoShift As DevExpress.XtraEditors.TextEdit
    Friend WithEvents ToggleAutoShiftAllowed As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents TxtEndTimeForOutPunch As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtInTimeForINPunch As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtDuplicateCheck As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl29 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl30 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl31 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControlETOP As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl32 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl33 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl34 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ToggleFourPunchNight As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents LabelControl37 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl35 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl36 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl38 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl39 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl40 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents ToggleOutWorkMins As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents ToggleMarkMisAsAbsent As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents ToggleMarkWOasAbsent As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents ToggleMarkAWAasAAA As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents LabelControl44 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl45 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControlMarkWO As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl48 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ToggleIsAutoAbsentAllowed As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents TxtNoOffpresentOnweekOff As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl46 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl47 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TabNavigationPage4 As DevExpress.XtraBars.Navigation.TabNavigationPage
    Friend WithEvents GroupControl8 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents CheckEditOT3 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditOT2 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditOT1 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents GroupControl9 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents ToggleRoundOverTime As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents LabelControl28 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ToggleWhetherOTinMinus As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents LabelControl43 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ToggleOTisAllowedInEarlyComing As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents LabelControl42 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GroupControl10 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents TxtDeductOTinWO As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtDeductOTinHLD As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl41 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl49 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GroupControl11 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents TxtOtRestrictDur As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl50 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TxtOtLateDur As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtOtEarlyDur As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl51 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl52 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TxtMaxEarlyDeparture As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtMaxWorkingMin As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TxtMaxLateArrivalMin As DevExpress.XtraEditors.TextEdit
    Friend WithEvents ToggleSwitchMarkHlf As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents LabelControl27 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GridLookUpEditMultiShif As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridView6 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn61 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn62 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn63 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn64 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn65 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn66 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn67 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn68 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn69 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn70 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn71 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn72 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn73 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn74 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn75 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents ToggleAllowCmpOffMlt As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents LabelControl53 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ToggleCmpPOWPOH As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents LabelControl54 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ToggleAWPasAAP As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents LabelControl57 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ToggleWAasAA As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents LabelControl56 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ToggleAWasAA As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents LabelControl55 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TogglePWAasPAA As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents LabelControl58 As DevExpress.XtraEditors.LabelControl
End Class
