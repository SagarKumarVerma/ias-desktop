﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraEmployee
    Inherits DevExpress.XtraEditors.XtraUserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(XtraEmployee))
        Me.SplitContainerControl1 = New DevExpress.XtraEditors.SplitContainerControl()
        Me.GridControlExport = New DevExpress.XtraGrid.GridControl()
        Me.GridViewExport = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.BarManager2 = New DevExpress.XtraBars.BarManager()
        Me.Bar2 = New DevExpress.XtraBars.Bar()
        Me.BarSubItem1 = New DevExpress.XtraBars.BarSubItem()
        Me.BarButtonItemImExport = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItemFullExport = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem4 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem1 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem5 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem2 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem6 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem9 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem10 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem11 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarDockControl1 = New DevExpress.XtraBars.BarDockControl()
        Me.BarDockControl2 = New DevExpress.XtraBars.BarDockControl()
        Me.BarDockControl3 = New DevExpress.XtraBars.BarDockControl()
        Me.BarDockControl4 = New DevExpress.XtraBars.BarDockControl()
        Me.BarButtonItem3 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem7 = New DevExpress.XtraBars.BarButtonItem()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colACTIVE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPAYCODE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPRESENTCARDNO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colEMPNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colGUARDIANNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSEX = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCOMPANYCODE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colEmployeeGroupId = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBRANCHCODE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDEPARTMENTCODE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCAT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDESIGNATION = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDivisionCode = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDateOFBIRTH = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDateOFJOIN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDESPANSARYCODE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colISMARRIED = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colGradeCode = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBUS = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colQUALIFICATION = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colEXPERIENCE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colADDRESS1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPINCODE1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colTELEPHONE1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colE_MAIL1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colADDRESS2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPINCODE2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colTELEPHONE2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBLOODGROUP = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colEMPPHOTO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colEMPSIGNATURE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLeavingdate = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLeavingReason = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVehicleNo = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPFNO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPF_NO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colESINO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colAUTHORISEDMACHINE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colEMPTYPE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBankAcc = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colbankCODE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colReporting1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colReporting2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colValidityStartDate = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colValidityEndDate = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLastModifiedBy = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLastModifiedDate = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.MemoEdit1 = New DevExpress.XtraEditors.MemoEdit()
        Me.TblEmployeeBindingSource = New System.Windows.Forms.BindingSource()
        Me.SSSDBDataSet = New iAS.SSSDBDataSet()
        Me.TblEmployeeTableAdapter = New iAS.SSSDBDataSetTableAdapters.TblEmployeeTableAdapter()
        Me.TblEmployee1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.TblEmployee1TableAdapter()
        Me.Bar1 = New DevExpress.XtraBars.Bar()
        Me.BarButtonItem8 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem12 = New DevExpress.XtraBars.BarButtonItem()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerControl1.SuspendLayout()
        CType(Me.GridControlExport, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewExport, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BarManager2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblEmployeeBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SplitContainerControl1
        '
        Me.SplitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainerControl1.Location = New System.Drawing.Point(0, 62)
        Me.SplitContainerControl1.Name = "SplitContainerControl1"
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.GridControlExport)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.GridControl1)
        Me.SplitContainerControl1.Panel1.Text = "Panel1"
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.MemoEdit1)
        Me.SplitContainerControl1.Panel2.Text = "Panel2"
        Me.SplitContainerControl1.Size = New System.Drawing.Size(1250, 506)
        Me.SplitContainerControl1.SplitterPosition = 1165
        Me.SplitContainerControl1.TabIndex = 0
        Me.SplitContainerControl1.Text = "SplitContainerControl1"
        '
        'GridControlExport
        '
        Me.GridControlExport.Location = New System.Drawing.Point(278, 165)
        Me.GridControlExport.MainView = Me.GridViewExport
        Me.GridControlExport.MenuManager = Me.BarManager2
        Me.GridControlExport.Name = "GridControlExport"
        Me.GridControlExport.Size = New System.Drawing.Size(400, 200)
        Me.GridControlExport.TabIndex = 1
        Me.GridControlExport.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewExport})
        Me.GridControlExport.Visible = False
        '
        'GridViewExport
        '
        Me.GridViewExport.GridControl = Me.GridControlExport
        Me.GridViewExport.Name = "GridViewExport"
        Me.GridViewExport.OptionsView.ColumnAutoWidth = False
        '
        'BarManager2
        '
        Me.BarManager2.AllowShowToolbarsPopup = False
        Me.BarManager2.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar2})
        Me.BarManager2.DockControls.Add(Me.BarDockControl1)
        Me.BarManager2.DockControls.Add(Me.BarDockControl2)
        Me.BarManager2.DockControls.Add(Me.BarDockControl3)
        Me.BarManager2.DockControls.Add(Me.BarDockControl4)
        Me.BarManager2.Form = Me
        Me.BarManager2.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.BarButtonItem3, Me.BarButtonItem4, Me.BarButtonItem1, Me.BarButtonItem2, Me.BarButtonItem5, Me.BarButtonItem6, Me.BarButtonItem7, Me.BarButtonItem9, Me.BarButtonItem10, Me.BarSubItem1, Me.BarButtonItemImExport, Me.BarButtonItemFullExport, Me.BarButtonItem11, Me.BarButtonItem12})
        Me.BarManager2.MainMenu = Me.Bar2
        Me.BarManager2.MaxItemId = 14
        '
        'Bar2
        '
        Me.Bar2.BarAppearance.Hovered.Font = New System.Drawing.Font("Tahoma", 15.0!)
        Me.Bar2.BarAppearance.Hovered.ForeColor = System.Drawing.Color.SteelBlue
        Me.Bar2.BarAppearance.Hovered.Options.UseFont = True
        Me.Bar2.BarAppearance.Hovered.Options.UseForeColor = True
        Me.Bar2.BarAppearance.Normal.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.Bar2.BarAppearance.Normal.ForeColor = System.Drawing.Color.SteelBlue
        Me.Bar2.BarAppearance.Normal.Options.UseFont = True
        Me.Bar2.BarAppearance.Normal.Options.UseForeColor = True
        Me.Bar2.BarAppearance.Pressed.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.Bar2.BarAppearance.Pressed.ForeColor = System.Drawing.Color.SteelBlue
        Me.Bar2.BarAppearance.Pressed.Options.UseFont = True
        Me.Bar2.BarAppearance.Pressed.Options.UseForeColor = True
        Me.Bar2.BarName = "Main menu"
        Me.Bar2.DockCol = 0
        Me.Bar2.DockRow = 0
        Me.Bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.Bar2.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarSubItem1), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem4), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem1), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem5), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem2), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem6), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem9), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem10), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem11)})
        Me.Bar2.OptionsBar.AllowQuickCustomization = False
        Me.Bar2.OptionsBar.DrawDragBorder = False
        Me.Bar2.OptionsBar.MultiLine = True
        Me.Bar2.OptionsBar.UseWholeRow = True
        Me.Bar2.Text = "Main menu"
        '
        'BarSubItem1
        '
        Me.BarSubItem1.Caption = "Export"
        Me.BarSubItem1.Id = 9
        Me.BarSubItem1.ImageOptions.Image = CType(resources.GetObject("BarSubItem1.ImageOptions.Image"), System.Drawing.Image)
        Me.BarSubItem1.ImageOptions.LargeImage = CType(resources.GetObject("BarSubItem1.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarSubItem1.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemImExport), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItemFullExport), New DevExpress.XtraBars.LinkPersistInfo(Me.BarButtonItem12)})
        Me.BarSubItem1.MenuAppearance.AppearanceMenu.Hovered.Font = New System.Drawing.Font("Tahoma", 14.0!)
        Me.BarSubItem1.MenuAppearance.AppearanceMenu.Hovered.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarSubItem1.MenuAppearance.AppearanceMenu.Hovered.Options.UseFont = True
        Me.BarSubItem1.MenuAppearance.AppearanceMenu.Hovered.Options.UseForeColor = True
        Me.BarSubItem1.MenuAppearance.AppearanceMenu.Normal.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.BarSubItem1.MenuAppearance.AppearanceMenu.Normal.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarSubItem1.MenuAppearance.AppearanceMenu.Normal.Options.UseFont = True
        Me.BarSubItem1.MenuAppearance.AppearanceMenu.Normal.Options.UseForeColor = True
        Me.BarSubItem1.MenuAppearance.AppearanceMenu.Pressed.Font = New System.Drawing.Font("Tahoma", 12.0!)
        Me.BarSubItem1.MenuAppearance.AppearanceMenu.Pressed.ForeColor = System.Drawing.Color.SteelBlue
        Me.BarSubItem1.MenuAppearance.AppearanceMenu.Pressed.Options.UseFont = True
        Me.BarSubItem1.MenuAppearance.AppearanceMenu.Pressed.Options.UseForeColor = True
        Me.BarSubItem1.Name = "BarSubItem1"
        Me.BarSubItem1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'BarButtonItemImExport
        '
        Me.BarButtonItemImExport.Caption = "Export As Per Import Format "
        Me.BarButtonItemImExport.Id = 10
        Me.BarButtonItemImExport.ImageOptions.Image = CType(resources.GetObject("BarButtonItemImExport.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItemImExport.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItemImExport.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItemImExport.Name = "BarButtonItemImExport"
        Me.BarButtonItemImExport.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'BarButtonItemFullExport
        '
        Me.BarButtonItemFullExport.Caption = "Full Export"
        Me.BarButtonItemFullExport.Id = 11
        Me.BarButtonItemFullExport.ImageOptions.Image = CType(resources.GetObject("BarButtonItemFullExport.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItemFullExport.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItemFullExport.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItemFullExport.Name = "BarButtonItemFullExport"
        Me.BarButtonItemFullExport.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'BarButtonItem4
        '
        Me.BarButtonItem4.Caption = "Import"
        Me.BarButtonItem4.Id = 1
        Me.BarButtonItem4.ImageOptions.Image = CType(resources.GetObject("BarButtonItem4.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem4.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItem4.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItem4.Name = "BarButtonItem4"
        Me.BarButtonItem4.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'BarButtonItem1
        '
        Me.BarButtonItem1.Caption = "Export to Device"
        Me.BarButtonItem1.Id = 2
        Me.BarButtonItem1.ImageOptions.Image = CType(resources.GetObject("BarButtonItem1.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem1.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItem1.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItem1.Name = "BarButtonItem1"
        Me.BarButtonItem1.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'BarButtonItem5
        '
        Me.BarButtonItem5.Caption = "Import From Device"
        Me.BarButtonItem5.Id = 4
        Me.BarButtonItem5.ImageOptions.Image = CType(resources.GetObject("BarButtonItem5.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem5.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItem5.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItem5.Name = "BarButtonItem5"
        Me.BarButtonItem5.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'BarButtonItem2
        '
        Me.BarButtonItem2.Caption = "Time Zone (ZK/Bio Pro)"
        Me.BarButtonItem2.Id = 3
        Me.BarButtonItem2.ImageOptions.Image = CType(resources.GetObject("BarButtonItem2.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem2.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItem2.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItem2.Name = "BarButtonItem2"
        Me.BarButtonItem2.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'BarButtonItem6
        '
        Me.BarButtonItem6.Caption = "Bulk Delete"
        Me.BarButtonItem6.Id = 5
        Me.BarButtonItem6.Name = "BarButtonItem6"
        Me.BarButtonItem6.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        '
        'BarButtonItem9
        '
        Me.BarButtonItem9.Caption = "Time Zone (Bio)"
        Me.BarButtonItem9.Id = 7
        Me.BarButtonItem9.ImageOptions.Image = CType(resources.GetObject("BarButtonItem9.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem9.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItem9.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItem9.Name = "BarButtonItem9"
        Me.BarButtonItem9.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'BarButtonItem10
        '
        Me.BarButtonItem10.Caption = "Quick Update"
        Me.BarButtonItem10.Id = 8
        Me.BarButtonItem10.ImageOptions.Image = CType(resources.GetObject("BarButtonItem10.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem10.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItem10.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItem10.Name = "BarButtonItem10"
        Me.BarButtonItem10.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'BarButtonItem11
        '
        Me.BarButtonItem11.Caption = "Validity Expiring"
        Me.BarButtonItem11.Id = 12
        Me.BarButtonItem11.Name = "BarButtonItem11"
        '
        'BarDockControl1
        '
        Me.BarDockControl1.CausesValidation = False
        Me.BarDockControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.BarDockControl1.Location = New System.Drawing.Point(0, 0)
        Me.BarDockControl1.Manager = Me.BarManager2
        Me.BarDockControl1.Size = New System.Drawing.Size(1250, 62)
        '
        'BarDockControl2
        '
        Me.BarDockControl2.CausesValidation = False
        Me.BarDockControl2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.BarDockControl2.Location = New System.Drawing.Point(0, 568)
        Me.BarDockControl2.Manager = Me.BarManager2
        Me.BarDockControl2.Size = New System.Drawing.Size(1250, 0)
        '
        'BarDockControl3
        '
        Me.BarDockControl3.CausesValidation = False
        Me.BarDockControl3.Dock = System.Windows.Forms.DockStyle.Left
        Me.BarDockControl3.Location = New System.Drawing.Point(0, 62)
        Me.BarDockControl3.Manager = Me.BarManager2
        Me.BarDockControl3.Size = New System.Drawing.Size(0, 506)
        '
        'BarDockControl4
        '
        Me.BarDockControl4.CausesValidation = False
        Me.BarDockControl4.Dock = System.Windows.Forms.DockStyle.Right
        Me.BarDockControl4.Location = New System.Drawing.Point(1250, 62)
        Me.BarDockControl4.Manager = Me.BarManager2
        Me.BarDockControl4.Size = New System.Drawing.Size(0, 506)
        '
        'BarButtonItem3
        '
        Me.BarButtonItem3.Caption = "Export"
        Me.BarButtonItem3.Id = 0
        Me.BarButtonItem3.ImageOptions.Image = CType(resources.GetObject("BarButtonItem3.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem3.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItem3.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItem3.Name = "BarButtonItem3"
        Me.BarButtonItem3.PaintStyle = DevExpress.XtraBars.BarItemPaintStyle.CaptionGlyph
        '
        'BarButtonItem7
        '
        Me.BarButtonItem7.Caption = "BarButtonItem7"
        Me.BarButtonItem7.Id = 6
        Me.BarButtonItem7.Name = "BarButtonItem7"
        '
        'GridControl1
        '
        Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControl1.EmbeddedNavigator.Buttons.Append.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.Edit.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = False
        Me.GridControl1.Location = New System.Drawing.Point(0, 0)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(1165, 506)
        Me.GridControl1.TabIndex = 0
        Me.GridControl1.UseEmbeddedNavigator = True
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.Appearance.TopNewRow.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.GridView1.Appearance.TopNewRow.ForeColor = System.Drawing.Color.Blue
        Me.GridView1.Appearance.TopNewRow.Options.UseFont = True
        Me.GridView1.Appearance.TopNewRow.Options.UseForeColor = True
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colACTIVE, Me.colPAYCODE, Me.colPRESENTCARDNO, Me.colEMPNAME, Me.colGUARDIANNAME, Me.colSEX, Me.colCOMPANYCODE, Me.colEmployeeGroupId, Me.colBRANCHCODE, Me.colDEPARTMENTCODE, Me.colCAT, Me.colDESIGNATION, Me.colDivisionCode, Me.colDateOFBIRTH, Me.colDateOFJOIN, Me.colDESPANSARYCODE, Me.colISMARRIED, Me.colGradeCode, Me.colBUS, Me.colQUALIFICATION, Me.colEXPERIENCE, Me.colADDRESS1, Me.colPINCODE1, Me.colTELEPHONE1, Me.colE_MAIL1, Me.colADDRESS2, Me.colPINCODE2, Me.colTELEPHONE2, Me.colBLOODGROUP, Me.colEMPPHOTO, Me.colEMPSIGNATURE, Me.colLeavingdate, Me.colLeavingReason, Me.colVehicleNo, Me.colPFNO, Me.colPF_NO, Me.colESINO, Me.colAUTHORISEDMACHINE, Me.colEMPTYPE, Me.colBankAcc, Me.colbankCODE, Me.colReporting1, Me.colReporting2, Me.colValidityStartDate, Me.colValidityEndDate, Me.colLastModifiedBy, Me.colLastModifiedDate})
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.NewItemRowText = "Click here to add new Employee"
        Me.GridView1.OptionsBehavior.EditingMode = DevExpress.XtraGrid.Views.Grid.GridEditingMode.EditForm
        Me.GridView1.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridView1.OptionsSelection.MultiSelect = True
        Me.GridView1.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        Me.GridView1.OptionsView.ColumnAutoWidth = False
        Me.GridView1.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top
        Me.GridView1.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colPRESENTCARDNO, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colACTIVE
        '
        Me.colACTIVE.Caption = "Active Status"
        Me.colACTIVE.FieldName = "ACTIVE"
        Me.colACTIVE.Name = "colACTIVE"
        Me.colACTIVE.Visible = True
        Me.colACTIVE.VisibleIndex = 1
        '
        'colPAYCODE
        '
        Me.colPAYCODE.Caption = "Paycode"
        Me.colPAYCODE.FieldName = "PAYCODE"
        Me.colPAYCODE.Name = "colPAYCODE"
        Me.colPAYCODE.Visible = True
        Me.colPAYCODE.VisibleIndex = 2
        Me.colPAYCODE.Width = 120
        '
        'colPRESENTCARDNO
        '
        Me.colPRESENTCARDNO.Caption = "Present Card No."
        Me.colPRESENTCARDNO.FieldName = "PRESENTCARDNO"
        Me.colPRESENTCARDNO.Name = "colPRESENTCARDNO"
        Me.colPRESENTCARDNO.Visible = True
        Me.colPRESENTCARDNO.VisibleIndex = 3
        Me.colPRESENTCARDNO.Width = 120
        '
        'colEMPNAME
        '
        Me.colEMPNAME.Caption = "Emp Name"
        Me.colEMPNAME.FieldName = "EMPNAME"
        Me.colEMPNAME.Name = "colEMPNAME"
        Me.colEMPNAME.Visible = True
        Me.colEMPNAME.VisibleIndex = 4
        Me.colEMPNAME.Width = 120
        '
        'colGUARDIANNAME
        '
        Me.colGUARDIANNAME.Caption = "Guardian Name"
        Me.colGUARDIANNAME.FieldName = "GUARDIANNAME"
        Me.colGUARDIANNAME.Name = "colGUARDIANNAME"
        Me.colGUARDIANNAME.Width = 100
        '
        'colSEX
        '
        Me.colSEX.Caption = "Gender"
        Me.colSEX.FieldName = "SEX"
        Me.colSEX.Name = "colSEX"
        Me.colSEX.Visible = True
        Me.colSEX.VisibleIndex = 5
        '
        'colCOMPANYCODE
        '
        Me.colCOMPANYCODE.Caption = "Company"
        Me.colCOMPANYCODE.FieldName = "COMPANYCODE"
        Me.colCOMPANYCODE.Name = "colCOMPANYCODE"
        Me.colCOMPANYCODE.Visible = True
        Me.colCOMPANYCODE.VisibleIndex = 6
        '
        'colEmployeeGroupId
        '
        Me.colEmployeeGroupId.Caption = "Emp Group"
        Me.colEmployeeGroupId.FieldName = "EmployeeGroupId"
        Me.colEmployeeGroupId.Name = "colEmployeeGroupId"
        Me.colEmployeeGroupId.Visible = True
        Me.colEmployeeGroupId.VisibleIndex = 7
        Me.colEmployeeGroupId.Width = 100
        '
        'colBRANCHCODE
        '
        Me.colBRANCHCODE.Caption = "Location"
        Me.colBRANCHCODE.FieldName = "BRANCHCODE"
        Me.colBRANCHCODE.Name = "colBRANCHCODE"
        Me.colBRANCHCODE.Visible = True
        Me.colBRANCHCODE.VisibleIndex = 8
        '
        'colDEPARTMENTCODE
        '
        Me.colDEPARTMENTCODE.Caption = "Department"
        Me.colDEPARTMENTCODE.FieldName = "DEPARTMENTCODE"
        Me.colDEPARTMENTCODE.Name = "colDEPARTMENTCODE"
        Me.colDEPARTMENTCODE.Visible = True
        Me.colDEPARTMENTCODE.VisibleIndex = 9
        '
        'colCAT
        '
        Me.colCAT.Caption = "Category"
        Me.colCAT.FieldName = "CAT"
        Me.colCAT.Name = "colCAT"
        Me.colCAT.Visible = True
        Me.colCAT.VisibleIndex = 10
        '
        'colDESIGNATION
        '
        Me.colDESIGNATION.AppearanceCell.Options.UseTextOptions = True
        Me.colDESIGNATION.AppearanceCell.TextOptions.Trimming = DevExpress.Utils.Trimming.Word
        Me.colDESIGNATION.Caption = "Designation"
        Me.colDESIGNATION.FieldName = "DESIGNATION"
        Me.colDESIGNATION.Name = "colDESIGNATION"
        Me.colDESIGNATION.Visible = True
        Me.colDESIGNATION.VisibleIndex = 11
        '
        'colDivisionCode
        '
        Me.colDivisionCode.Caption = "Division"
        Me.colDivisionCode.FieldName = "DivisionCode"
        Me.colDivisionCode.Name = "colDivisionCode"
        '
        'colDateOFBIRTH
        '
        Me.colDateOFBIRTH.Caption = "Date of Birth"
        Me.colDateOFBIRTH.FieldName = "DateOFBIRTH"
        Me.colDateOFBIRTH.Name = "colDateOFBIRTH"
        Me.colDateOFBIRTH.Width = 100
        '
        'colDateOFJOIN
        '
        Me.colDateOFJOIN.Caption = "Date of Join"
        Me.colDateOFJOIN.FieldName = "DateOFJOIN"
        Me.colDateOFJOIN.Name = "colDateOFJOIN"
        Me.colDateOFJOIN.Visible = True
        Me.colDateOFJOIN.VisibleIndex = 12
        Me.colDateOFJOIN.Width = 100
        '
        'colDESPANSARYCODE
        '
        Me.colDESPANSARYCODE.Caption = "Despensary"
        Me.colDESPANSARYCODE.FieldName = "DESPANSARYCODE"
        Me.colDESPANSARYCODE.Name = "colDESPANSARYCODE"
        '
        'colISMARRIED
        '
        Me.colISMARRIED.Caption = "Is Married"
        Me.colISMARRIED.FieldName = "ISMARRIED"
        Me.colISMARRIED.Name = "colISMARRIED"
        '
        'colGradeCode
        '
        Me.colGradeCode.AppearanceCell.Options.UseTextOptions = True
        Me.colGradeCode.AppearanceCell.TextOptions.Trimming = DevExpress.Utils.Trimming.Word
        Me.colGradeCode.Caption = "Grade"
        Me.colGradeCode.FieldName = "GradeCode"
        Me.colGradeCode.Name = "colGradeCode"
        Me.colGradeCode.Visible = True
        Me.colGradeCode.VisibleIndex = 13
        '
        'colBUS
        '
        Me.colBUS.Caption = "Bus Rout"
        Me.colBUS.FieldName = "BUS"
        Me.colBUS.Name = "colBUS"
        '
        'colQUALIFICATION
        '
        Me.colQUALIFICATION.Caption = "Qualification"
        Me.colQUALIFICATION.FieldName = "QUALIFICATION"
        Me.colQUALIFICATION.Name = "colQUALIFICATION"
        '
        'colEXPERIENCE
        '
        Me.colEXPERIENCE.Caption = "Experince"
        Me.colEXPERIENCE.FieldName = "EXPERIENCE"
        Me.colEXPERIENCE.Name = "colEXPERIENCE"
        '
        'colADDRESS1
        '
        Me.colADDRESS1.Caption = "Address"
        Me.colADDRESS1.FieldName = "ADDRESS1"
        Me.colADDRESS1.Name = "colADDRESS1"
        Me.colADDRESS1.Width = 100
        '
        'colPINCODE1
        '
        Me.colPINCODE1.Caption = "Pincode"
        Me.colPINCODE1.FieldName = "PINCODE1"
        Me.colPINCODE1.Name = "colPINCODE1"
        '
        'colTELEPHONE1
        '
        Me.colTELEPHONE1.AppearanceCell.Options.UseTextOptions = True
        Me.colTELEPHONE1.AppearanceCell.TextOptions.Trimming = DevExpress.Utils.Trimming.Word
        Me.colTELEPHONE1.Caption = "Telephone"
        Me.colTELEPHONE1.FieldName = "TELEPHONE1"
        Me.colTELEPHONE1.Name = "colTELEPHONE1"
        Me.colTELEPHONE1.Visible = True
        Me.colTELEPHONE1.VisibleIndex = 14
        '
        'colE_MAIL1
        '
        Me.colE_MAIL1.AppearanceCell.Options.UseTextOptions = True
        Me.colE_MAIL1.AppearanceCell.TextOptions.Trimming = DevExpress.Utils.Trimming.Word
        Me.colE_MAIL1.Caption = "Mail Id"
        Me.colE_MAIL1.FieldName = "E_MAIL1"
        Me.colE_MAIL1.Name = "colE_MAIL1"
        Me.colE_MAIL1.Visible = True
        Me.colE_MAIL1.VisibleIndex = 15
        '
        'colADDRESS2
        '
        Me.colADDRESS2.FieldName = "ADDRESS2"
        Me.colADDRESS2.Name = "colADDRESS2"
        Me.colADDRESS2.Width = 100
        '
        'colPINCODE2
        '
        Me.colPINCODE2.Caption = "PAN"
        Me.colPINCODE2.FieldName = "PINCODE2"
        Me.colPINCODE2.Name = "colPINCODE2"
        '
        'colTELEPHONE2
        '
        Me.colTELEPHONE2.Caption = "UID"
        Me.colTELEPHONE2.FieldName = "TELEPHONE2"
        Me.colTELEPHONE2.Name = "colTELEPHONE2"
        '
        'colBLOODGROUP
        '
        Me.colBLOODGROUP.Caption = "Blood Group"
        Me.colBLOODGROUP.FieldName = "BLOODGROUP"
        Me.colBLOODGROUP.Name = "colBLOODGROUP"
        '
        'colEMPPHOTO
        '
        Me.colEMPPHOTO.FieldName = "EMPPHOTO"
        Me.colEMPPHOTO.Name = "colEMPPHOTO"
        '
        'colEMPSIGNATURE
        '
        Me.colEMPSIGNATURE.FieldName = "EMPSIGNATURE"
        Me.colEMPSIGNATURE.Name = "colEMPSIGNATURE"
        '
        'colLeavingdate
        '
        Me.colLeavingdate.Caption = "Leaving Date"
        Me.colLeavingdate.FieldName = "Leavingdate"
        Me.colLeavingdate.Name = "colLeavingdate"
        '
        'colLeavingReason
        '
        Me.colLeavingReason.FieldName = "LeavingReason"
        Me.colLeavingReason.Name = "colLeavingReason"
        '
        'colVehicleNo
        '
        Me.colVehicleNo.FieldName = "VehicleNo"
        Me.colVehicleNo.Name = "colVehicleNo"
        '
        'colPFNO
        '
        Me.colPFNO.Caption = "PF No"
        Me.colPFNO.FieldName = "PFNO"
        Me.colPFNO.Name = "colPFNO"
        '
        'colPF_NO
        '
        Me.colPF_NO.FieldName = "PF_NO"
        Me.colPF_NO.Name = "colPF_NO"
        '
        'colESINO
        '
        Me.colESINO.Caption = "ESI No"
        Me.colESINO.FieldName = "ESINO"
        Me.colESINO.Name = "colESINO"
        '
        'colAUTHORISEDMACHINE
        '
        Me.colAUTHORISEDMACHINE.Caption = "Authorised Machine"
        Me.colAUTHORISEDMACHINE.FieldName = "AUTHORISEDMACHINE"
        Me.colAUTHORISEDMACHINE.Name = "colAUTHORISEDMACHINE"
        '
        'colEMPTYPE
        '
        Me.colEMPTYPE.Caption = "Emp Type"
        Me.colEMPTYPE.FieldName = "EMPTYPE"
        Me.colEMPTYPE.Name = "colEMPTYPE"
        '
        'colBankAcc
        '
        Me.colBankAcc.FieldName = "BankAcc"
        Me.colBankAcc.Name = "colBankAcc"
        '
        'colbankCODE
        '
        Me.colbankCODE.FieldName = "bankCODE"
        Me.colbankCODE.Name = "colbankCODE"
        '
        'colReporting1
        '
        Me.colReporting1.FieldName = "Reporting1"
        Me.colReporting1.Name = "colReporting1"
        '
        'colReporting2
        '
        Me.colReporting2.FieldName = "Reporting2"
        Me.colReporting2.Name = "colReporting2"
        '
        'colValidityStartDate
        '
        Me.colValidityStartDate.FieldName = "ValidityStartDate"
        Me.colValidityStartDate.Name = "colValidityStartDate"
        Me.colValidityStartDate.Visible = True
        Me.colValidityStartDate.VisibleIndex = 16
        '
        'colValidityEndDate
        '
        Me.colValidityEndDate.FieldName = "ValidityEndDate"
        Me.colValidityEndDate.Name = "colValidityEndDate"
        Me.colValidityEndDate.Visible = True
        Me.colValidityEndDate.VisibleIndex = 17
        '
        'colLastModifiedBy
        '
        Me.colLastModifiedBy.FieldName = "LastModifiedBy"
        Me.colLastModifiedBy.Name = "colLastModifiedBy"
        '
        'colLastModifiedDate
        '
        Me.colLastModifiedDate.FieldName = "LastModifiedDate"
        Me.colLastModifiedDate.Name = "colLastModifiedDate"
        '
        'MemoEdit1
        '
        Me.MemoEdit1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.MemoEdit1.EditValue = resources.GetString("MemoEdit1.EditValue")
        Me.MemoEdit1.Location = New System.Drawing.Point(0, 0)
        Me.MemoEdit1.Name = "MemoEdit1"
        Me.MemoEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.MemoEdit1.Properties.Appearance.Options.UseFont = True
        Me.MemoEdit1.Properties.MaxLength = 100000
        Me.MemoEdit1.Properties.ReadOnly = True
        Me.MemoEdit1.Size = New System.Drawing.Size(77, 506)
        Me.MemoEdit1.TabIndex = 3
        '
        'TblEmployeeBindingSource
        '
        Me.TblEmployeeBindingSource.DataMember = "TblEmployee"
        Me.TblEmployeeBindingSource.DataSource = Me.SSSDBDataSet
        '
        'SSSDBDataSet
        '
        Me.SSSDBDataSet.DataSetName = "SSSDBDataSet"
        Me.SSSDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'TblEmployeeTableAdapter
        '
        Me.TblEmployeeTableAdapter.ClearBeforeFill = True
        '
        'TblEmployee1TableAdapter1
        '
        Me.TblEmployee1TableAdapter1.ClearBeforeFill = True
        '
        'Bar1
        '
        Me.Bar1.BarName = "Main menu"
        Me.Bar1.DockCol = 0
        Me.Bar1.DockRow = 0
        Me.Bar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.Bar1.OptionsBar.MultiLine = True
        Me.Bar1.OptionsBar.UseWholeRow = True
        Me.Bar1.Text = "Main menu"
        '
        'BarButtonItem8
        '
        Me.BarButtonItem8.Caption = "Bulk Delete"
        Me.BarButtonItem8.Id = 5
        Me.BarButtonItem8.ItemAppearance.Normal.Font = New System.Drawing.Font("Tahoma", 15.0!)
        Me.BarButtonItem8.ItemAppearance.Normal.Options.UseFont = True
        Me.BarButtonItem8.Name = "BarButtonItem8"
        '
        'BarButtonItem12
        '
        Me.BarButtonItem12.Caption = "Excel To Device(Ultra)"
        Me.BarButtonItem12.Id = 13
        Me.BarButtonItem12.ImageOptions.Image = CType(resources.GetObject("BarButtonItem12.ImageOptions.Image"), System.Drawing.Image)
        Me.BarButtonItem12.ImageOptions.LargeImage = CType(resources.GetObject("BarButtonItem12.ImageOptions.LargeImage"), System.Drawing.Image)
        Me.BarButtonItem12.Name = "BarButtonItem12"
        '
        'XtraEmployee
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.SplitContainerControl1)
        Me.Controls.Add(Me.BarDockControl3)
        Me.Controls.Add(Me.BarDockControl4)
        Me.Controls.Add(Me.BarDockControl2)
        Me.Controls.Add(Me.BarDockControl1)
        Me.LookAndFeel.SkinName = "iMaginary"
        Me.LookAndFeel.UseDefaultLookAndFeel = False
        Me.Name = "XtraEmployee"
        Me.Size = New System.Drawing.Size(1250, 568)
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerControl1.ResumeLayout(False)
        CType(Me.GridControlExport, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewExport, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BarManager2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblEmployeeBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents SplitContainerControl1 As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents MemoEdit1 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents TblEmployeeBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SSSDBDataSet As iAS.SSSDBDataSet
    Friend WithEvents colACTIVE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPAYCODE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colEMPNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colGUARDIANNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDateOFBIRTH As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDateOFJOIN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPRESENTCARDNO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCOMPANYCODE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDEPARTMENTCODE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCAT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSEX As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colISMARRIED As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBUS As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colQUALIFICATION As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colEXPERIENCE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDESIGNATION As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colADDRESS1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPINCODE1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colTELEPHONE1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colE_MAIL1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colADDRESS2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPINCODE2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colTELEPHONE2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBLOODGROUP As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colEMPPHOTO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colEMPSIGNATURE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDivisionCode As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colGradeCode As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLeavingdate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLeavingReason As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVehicleNo As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPFNO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPF_NO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colESINO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colAUTHORISEDMACHINE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colEMPTYPE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBankAcc As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colbankCODE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colReporting1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colReporting2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBRANCHCODE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDESPANSARYCODE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colValidityStartDate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colValidityEndDate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colEmployeeGroupId As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLastModifiedBy As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLastModifiedDate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents TblEmployeeTableAdapter As iAS.SSSDBDataSetTableAdapters.TblEmployeeTableAdapter
    Friend WithEvents TblEmployee1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.TblEmployee1TableAdapter
    Friend WithEvents Bar1 As DevExpress.XtraBars.Bar
    Friend WithEvents BarManager2 As DevExpress.XtraBars.BarManager
    Friend WithEvents Bar2 As DevExpress.XtraBars.Bar
    Friend WithEvents BarButtonItem3 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem4 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarDockControl1 As DevExpress.XtraBars.BarDockControl
    Friend WithEvents BarDockControl2 As DevExpress.XtraBars.BarDockControl
    Friend WithEvents BarDockControl3 As DevExpress.XtraBars.BarDockControl
    Friend WithEvents BarDockControl4 As DevExpress.XtraBars.BarDockControl
    Friend WithEvents BarButtonItem1 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem2 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem5 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem6 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem9 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem7 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem8 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem10 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarSubItem1 As DevExpress.XtraBars.BarSubItem
    Friend WithEvents BarButtonItemImExport As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItemFullExport As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents GridControlExport As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridViewExport As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents BarButtonItem11 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem12 As DevExpress.XtraBars.BarButtonItem
End Class
