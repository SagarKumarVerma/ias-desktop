﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraPfBank
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(XtraPfBank))
        Me.SimpleButtonVerify = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButtonSave = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditChNo = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditToBank = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.TextEditBName = New DevExpress.XtraEditors.TextEdit()
        Me.TextEditBranch = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.DateEdit1 = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditChAmt = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditDepositor = New DevExpress.XtraEditors.TextEdit()
        CType(Me.TextEditChNo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditToBank.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditBName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditBranch.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit1.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditChAmt.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditDepositor.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SimpleButtonVerify
        '
        Me.SimpleButtonVerify.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonVerify.Appearance.Options.UseFont = True
        Me.SimpleButtonVerify.Location = New System.Drawing.Point(395, 205)
        Me.SimpleButtonVerify.Name = "SimpleButtonVerify"
        Me.SimpleButtonVerify.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButtonVerify.TabIndex = 28
        Me.SimpleButtonVerify.Text = "Activate"
        Me.SimpleButtonVerify.Visible = False
        '
        'SimpleButtonSave
        '
        Me.SimpleButtonSave.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonSave.Appearance.Options.UseFont = True
        Me.SimpleButtonSave.Location = New System.Drawing.Point(166, 205)
        Me.SimpleButtonSave.Name = "SimpleButtonSave"
        Me.SimpleButtonSave.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButtonSave.TabIndex = 8
        Me.SimpleButtonSave.Text = "OK"
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl6.Appearance.Options.UseFont = True
        Me.LabelControl6.Location = New System.Drawing.Point(12, 181)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(52, 14)
        Me.LabelControl6.TabIndex = 32
        Me.LabelControl6.Text = "Depositor"
        '
        'TextEditChNo
        '
        Me.TextEditChNo.Location = New System.Drawing.Point(166, 101)
        Me.TextEditChNo.Name = "TextEditChNo"
        Me.TextEditChNo.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditChNo.Properties.Appearance.Options.UseFont = True
        Me.TextEditChNo.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEditChNo.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEditChNo.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditChNo.Properties.MaxLength = 50
        Me.TextEditChNo.Size = New System.Drawing.Size(133, 20)
        Me.TextEditChNo.TabIndex = 4
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl3.Appearance.Options.UseFont = True
        Me.LabelControl3.Location = New System.Drawing.Point(12, 103)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(61, 14)
        Me.LabelControl3.TabIndex = 23
        Me.LabelControl3.Text = "Cheque No"
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl2.Appearance.Options.UseFont = True
        Me.LabelControl2.Location = New System.Drawing.Point(12, 51)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(61, 14)
        Me.LabelControl2.TabIndex = 31
        Me.LabelControl2.Text = "Bank Name"
        '
        'TextEditToBank
        '
        Me.TextEditToBank.Location = New System.Drawing.Point(166, 23)
        Me.TextEditToBank.Name = "TextEditToBank"
        Me.TextEditToBank.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditToBank.Properties.Appearance.Options.UseFont = True
        Me.TextEditToBank.Properties.MaxLength = 50
        Me.TextEditToBank.Size = New System.Drawing.Size(304, 20)
        Me.TextEditToBank.TabIndex = 1
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Location = New System.Drawing.Point(13, 26)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(80, 14)
        Me.LabelControl1.TabIndex = 19
        Me.LabelControl1.Text = "To Bank Name"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButton1.Appearance.Options.UseFont = True
        Me.SimpleButton1.Location = New System.Drawing.Point(247, 204)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton1.TabIndex = 9
        Me.SimpleButton1.Text = "Close"
        '
        'TextEditBName
        '
        Me.TextEditBName.Location = New System.Drawing.Point(166, 49)
        Me.TextEditBName.Name = "TextEditBName"
        Me.TextEditBName.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditBName.Properties.Appearance.Options.UseFont = True
        Me.TextEditBName.Properties.MaxLength = 50
        Me.TextEditBName.Size = New System.Drawing.Size(304, 20)
        Me.TextEditBName.TabIndex = 2
        '
        'TextEditBranch
        '
        Me.TextEditBranch.Location = New System.Drawing.Point(166, 75)
        Me.TextEditBranch.Name = "TextEditBranch"
        Me.TextEditBranch.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditBranch.Properties.Appearance.Options.UseFont = True
        Me.TextEditBranch.Properties.MaxLength = 50
        Me.TextEditBranch.Size = New System.Drawing.Size(304, 20)
        Me.TextEditBranch.TabIndex = 3
        '
        'LabelControl7
        '
        Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl7.Appearance.Options.UseFont = True
        Me.LabelControl7.Location = New System.Drawing.Point(12, 78)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(67, 14)
        Me.LabelControl7.TabIndex = 42
        Me.LabelControl7.Text = "Bank Branch"
        '
        'DateEdit1
        '
        Me.DateEdit1.EditValue = Nothing
        Me.DateEdit1.Location = New System.Drawing.Point(166, 127)
        Me.DateEdit1.Name = "DateEdit1"
        Me.DateEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.DateEdit1.Properties.Appearance.Options.UseFont = True
        Me.DateEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit1.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit1.Size = New System.Drawing.Size(133, 20)
        Me.DateEdit1.TabIndex = 5
        '
        'LabelControl8
        '
        Me.LabelControl8.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl8.Appearance.Options.UseFont = True
        Me.LabelControl8.Location = New System.Drawing.Point(12, 130)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(33, 14)
        Me.LabelControl8.TabIndex = 44
        Me.LabelControl8.Text = "Dated"
        '
        'TextEditChAmt
        '
        Me.TextEditChAmt.Location = New System.Drawing.Point(166, 153)
        Me.TextEditChAmt.Name = "TextEditChAmt"
        Me.TextEditChAmt.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditChAmt.Properties.Appearance.Options.UseFont = True
        Me.TextEditChAmt.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEditChAmt.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEditChAmt.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditChAmt.Properties.MaxLength = 50
        Me.TextEditChAmt.Size = New System.Drawing.Size(133, 20)
        Me.TextEditChAmt.TabIndex = 6
        '
        'LabelControl9
        '
        Me.LabelControl9.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl9.Appearance.Options.UseFont = True
        Me.LabelControl9.Location = New System.Drawing.Point(12, 155)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(90, 14)
        Me.LabelControl9.TabIndex = 46
        Me.LabelControl9.Text = "Cheque Amount"
        '
        'TextEditDepositor
        '
        Me.TextEditDepositor.Location = New System.Drawing.Point(166, 179)
        Me.TextEditDepositor.Name = "TextEditDepositor"
        Me.TextEditDepositor.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditDepositor.Properties.Appearance.Options.UseFont = True
        Me.TextEditDepositor.Properties.MaxLength = 50
        Me.TextEditDepositor.Size = New System.Drawing.Size(304, 20)
        Me.TextEditDepositor.TabIndex = 7
        '
        'XtraPfBank
        '
        Me.AcceptButton = Me.SimpleButtonSave
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(541, 254)
        Me.ControlBox = False
        Me.Controls.Add(Me.TextEditDepositor)
        Me.Controls.Add(Me.TextEditChAmt)
        Me.Controls.Add(Me.LabelControl9)
        Me.Controls.Add(Me.LabelControl8)
        Me.Controls.Add(Me.DateEdit1)
        Me.Controls.Add(Me.LabelControl7)
        Me.Controls.Add(Me.TextEditBranch)
        Me.Controls.Add(Me.TextEditBName)
        Me.Controls.Add(Me.SimpleButton1)
        Me.Controls.Add(Me.SimpleButtonVerify)
        Me.Controls.Add(Me.SimpleButtonSave)
        Me.Controls.Add(Me.LabelControl6)
        Me.Controls.Add(Me.TextEditChNo)
        Me.Controls.Add(Me.LabelControl3)
        Me.Controls.Add(Me.LabelControl2)
        Me.Controls.Add(Me.TextEditToBank)
        Me.Controls.Add(Me.LabelControl1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.LookAndFeel.SkinName = "iMaginary"
        Me.LookAndFeel.UseDefaultLookAndFeel = False
        Me.Name = "XtraPfBank"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "PF Details"
        CType(Me.TextEditChNo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditToBank.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditBName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditBranch.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit1.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditChAmt.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditDepositor.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents SimpleButtonVerify As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButtonSave As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditChNo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditToBank As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents TextEditBName As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextEditBranch As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents DateEdit1 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditChAmt As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditDepositor As DevExpress.XtraEditors.TextEdit
End Class
