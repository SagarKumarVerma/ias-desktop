﻿Imports System.IO
Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.Resources
Imports System.Globalization
Imports DevExpress.XtraEditors
Imports DevExpress.LookAndFeel
Imports System.Net.Sockets
Imports System.Threading
Imports System.Net
Imports System.Text
Imports ZKPushDemoNitinCSharp
Imports DevExpress.XtraCharts
Imports DevExpress.XtraReports.UI

Public Class XtraHomeDashBoard
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    Dim ulf As UserLookAndFeel
    Dim adap As SqlDataAdapter
    Dim adapA As OleDbDataAdapter
    Dim ds As DataSet = New DataSet
    Dim Paycode As String
    'Dim Name As String
    Dim VerifyMode As String
    Dim PunchDate As String
    Dim PunchTime As String
    Dim Cn As Common = New Common

    Dim laodDeviceListThread As Thread
    Dim laodDeptAttenThread As Thread
    Dim WOCountTmp As String

    Structure DashBoardEmp
        Dim IN1 As String
        Dim WO_Value As Double
        Dim Leavevalue As Double
        Dim LATEARRIVAL As Double
    End Structure
    Public Sub New()
        InitializeComponent()
        'Common.SetGridFont(GridView1, New Font("Tahoma", 12))
    End Sub
    Private Sub XtraHomeDashBoard_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

        If license.TrialPerioad = True And license.TrialExpired = False Then
            'LabelAppName.Text = "Trial Version. " '& vbCrLf & "Max Device 5" & ", " & "Max Employees 500 " 'Remaining  Days: " & 30 - Now.Subtract(license.InstallDate).Days & "  "
            'LabelCount.Text = "Max Device 5" & ", " & "Max Employees 500 "
            'LabelAppName.Visible = True

            LabelCount.Text = "Trial Version. " & vbCrLf & "Max Device 5" & ", " & "Max Employees 500 "
        Else
            'LabelAppName.Text = ("Integrated Attendance System ").PadRight(44, " ") & "." & vbCrLf & license.InstalledCompName.PadRight(45, " ")
            'LabelCount.Text = ("Max Device " & license.NoOfDevices & ", " & "Max Employees " & license.NoOfUsers & " ").PadRight(40, " ")
            'LabelAppName.Visible = True


            ' '& "." &
            LabelCount.Text = ("Integrated Attendance System ").PadRight(40, " ") &
            vbCrLf & license.InstalledCompName.PadRight(40, " ") &
            vbCrLf & ("Max Device " & license.NoOfDevices & ", " & "Max Employees " & license.NoOfUsers & " ").PadRight(40, " ")

        End If
        'LabelAppName.Text = LabelAppName.Text.PadRight(80, " ")
        'LabelCount.Text = LabelCount.Text.PadRight(40, " ")
        'Application.DoEvents
        'If LabelCount.Text.Length > LabelAppName.Text.Substring(0, vbCrLf).Length Then
        '    LabelAppName.Text = LabelAppName.Text.PadRight(LabelCount.Text.Length, " ")
        'Else
        '    LabelCount.Text = LabelCount.Text.PadRight(LabelAppName.Text.Length, " ")
        'End If
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True
        'Me.Width = My.Computer.Screen.Bounds.Width
        'Me.Height = My.Computer.Screen.Bounds.Height

        Me.Width = Me.Parent.Width
        Me.Height = Me.Parent.Height


        SidePanelPie.Width = My.Computer.Screen.Bounds.Width / 2
        SidePanelPie.Height = Me.Height
        SidePanelShortCut.Height = Me.Height
        SidePanelImg1.Height = SidePanelShortCut.Height / 3
        SidePanelImg2.Height = SidePanelImg1.Height

        SidePanelImg11.Width = SidePanelImg1.Width / 3
        SidePanelImg21.Width = SidePanelImg1.Width / 3
        SidePanelImg22.Width = SidePanelImg1.Width / 3
        SidePanelImg33.Width = SidePanelImg1.Width / 3
        SidePanelImg32.Width = SidePanelImg1.Width / 3

        SidePanelPie1.Height = SidePanelPie.Height / 3
        SidePanelPie2.Height = SidePanelPie1.Height 'SidePanelPie.Height / 3
        SidePanelPie3.Height = SidePanelPie1.Height

        SidePanelPie11.Width = SidePanelPie1.Width / 2
        SidePanelPie21.Width = SidePanelPie1.Width / 2
        SidePanelPie31.Width = SidePanelPie1.Width / 2

        'If Common.HomeLoad = True Then
        ''laodDeptAtten()
        'Application.DoEvents()
        LoadDashBoard()
        Application.DoEvents()
        TimerRefreshDashBoard.Enabled = True
        'End If

        Common.HomeLoad = True
    End Sub
    Private Sub TimerRefreshDashBoard_Tick(sender As System.Object, e As System.EventArgs) Handles TimerRefreshDashBoard.Tick
        Try
            TimerRefreshDashBoard.Enabled = False
            'LoadDashBoard()
            ''laodDeptAtten()
            'TimerRefreshDashBoard.Enabled = True
            'Me.Refresh()
            ''Me.laodDeptAttenThread = New Thread(New ThreadStart(AddressOf Me.laodDeptAtten))
            ''Me.laodDeptAttenThread.Start()
        Catch ex As Exception
            TimerRefreshDashBoard.Enabled = True
        End Try
    End Sub
    Public Delegate Sub laodDeptAttenCallback()

    Private Sub LoadDashBoard()  'with struct
        'Me.Refresh()
        Dim adapA As OleDbDataAdapter
        Dim adap As SqlDataAdapter
        Dim ds As DataSet
        ds = New DataSet
        Dim ssql As String = "" '"select COUNT(PAYCODE) from TblEmployee where active='Y'"       
        ds = New DataSet
        Dim DashBoardEmpArr() As DashBoardEmp
        Dim DashBoardEmpLst As New List(Of DashBoardEmp)
        'Application.DoEvents()
        If Common.USERTYPE = "A" Then
            ssql = "select tblTimeRegister.IN1, tblTimeRegister.WO_VALUE, tblTimeRegister.leavevalue, tblTimeRegister.LATEARRIVAL from tblTimeRegister,TblEmployee WHERE TblEmployee.PAYCODE=tblTimeRegister.PAYCODE and tblTimeRegister.DateOFFICE='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and TblEmployee.ACTIVE='Y'"
        Else
            ssql = "select tblTimeRegister.IN1, tblTimeRegister.WO_VALUE, tblTimeRegister.leavevalue, tblTimeRegister.LATEARRIVAL from tblTimeRegister,TblEmployee WHERE TblEmployee.PAYCODE=tblTimeRegister.PAYCODE and tblTimeRegister.DateOFFICE='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and  TblEmployee.PAYCODE IN ('" & String.Join("', '", Common.EmpArr) & "') and  TblEmployee.ACTIVE='Y'"
        End If

        If Common.servername = "Access" Then
            'ssql = "select tblTimeRegister.IN1, tblTimeRegister.WO_VALUE, tblTimeRegister.leavevalue, tblTimeRegister.LATEARRIVAL from tblTimeRegister,TblEmployee WHERE  TblEmployee.PAYCODE=tblTimeRegister.PAYCODE and FORMAT(tblTimeRegister.DateOFFICE,'yyyy-MM-dd 00:00:00')='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and  TblEmployee.PAYCODE IN ('" & String.Join("', '", Common.EmpArr) & "')  and  TblEmployee.ACTIVE='Y'"
            ssql = ssql.Replace("tblTimeRegister.DateOFFICE", "FORMAT(tblTimeRegister.DateOFFICE,'yyyy-MM-dd 00:00:00')")
            adapA = New OleDbDataAdapter(ssql, Common.con1)
            adapA.Fill(ds)
        Else
            'ssql = "select tblTimeRegister.IN1, tblTimeRegister.WO_VALUE, tblTimeRegister.leavevalue, tblTimeRegister.LATEARRIVAL from tblTimeRegister,TblEmployee WHERE TblEmployee.PAYCODE=tblTimeRegister.PAYCODE and tblTimeRegister.DateOFFICE='" & Now.ToString("yyyy-MM-dd 00:00:00") & "' and  TblEmployee.PAYCODE IN ('" & String.Join("', '", Common.EmpArr) & "') and  TblEmployee.ACTIVE='Y'"
            adap = New SqlDataAdapter(ssql, Common.con)
            adap.Fill(ds)
        End If
        Application.DoEvents()
        For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
            Dim x As DashBoardEmp = New DashBoardEmp
            x.IN1 = ds.Tables(0).Rows(i).Item("IN1").ToString.Trim
            x.WO_Value = ds.Tables(0).Rows(i).Item("WO_Value").ToString.Trim
            If ds.Tables(0).Rows(i).Item("Leavevalue").ToString.Trim = "" Then
                x.Leavevalue = 0
            Else
                x.Leavevalue = ds.Tables(0).Rows(i).Item("Leavevalue").ToString.Trim
            End If
            If ds.Tables(0).Rows(i).Item("LATEARRIVAL").ToString.Trim = "" Then
                x.LATEARRIVAL = 0
            Else
                x.LATEARRIVAL = ds.Tables(0).Rows(i).Item("LATEARRIVAL").ToString.Trim
            End If
            DashBoardEmpLst.Add(x)
        Next
        DashBoardEmpArr = DashBoardEmpLst.ToArray
        Application.DoEvents()

        Dim PresenCount As Double = 0
        Dim AbsentCount As Double = 0
        Dim WOCount As Double = 0
        Dim LeaveCount As Double = 0
        Dim LateCount As Double = 0

        For i As Integer = 0 To DashBoardEmpArr.Count - 1
            If DashBoardEmpArr(i).IN1 <> "" Then
                PresenCount = PresenCount + 1
            End If
            If DashBoardEmpArr(i).IN1 = "" Then
                AbsentCount = AbsentCount + 1
            End If
            If DashBoardEmpArr(i).WO_Value > 0 Then
                WOCount = WOCount + 1
            End If
            If DashBoardEmpArr(i).Leavevalue > 0 Then
                LeaveCount = LeaveCount + 1
            End If
            If DashBoardEmpArr(i).LATEARRIVAL > 0 Then
                LateCount = LateCount + 1
            End If
        Next
        WOCountTmp = WOCount
        Application.DoEvents()

        'TileItemPresent.Text = PresenCount
        'TileItemAbsent.Text = AbsentCount
        'TileItemWO.Text = WOCount
        'TileItemLeave.Text = LeaveCount
        'TileItemLate.Text = LateCount
        'TileItemTotal.Text = DashBoardEmpArr.Count

        Dim PresenCountTakk As Double = 100 * PresenCount / DashBoardEmpArr.Count
        Dim AbsentCountTakk As Double = 100 * AbsentCount / DashBoardEmpArr.Count
        Dim WOCountTakk As Double = 100 * WOCount / DashBoardEmpArr.Count
        Dim LeaveCountTakk As Double = 100 * LeaveCount / DashBoardEmpArr.Count
        Dim LateCountTakk As Double = 100 * LateCount / DashBoardEmpArr.Count

        'Total
        Dim series1 As New Series("Series 1", ViewType.Doughnut)
        Dim PieSeriesLabel1 As DevExpress.XtraCharts.PieSeriesLabel = New DevExpress.XtraCharts.PieSeriesLabel()
        Dim SeriesPoint1 As DevExpress.XtraCharts.SeriesPoint = New DevExpress.XtraCharts.SeriesPoint("Active Head", New Object() {CType(DashBoardEmpArr.Count, Object)}, 0)
        'Dim SeriesPoint2 As DevExpress.XtraCharts.SeriesPoint = New DevExpress.XtraCharts.SeriesPoint("Absent", New Object() {CType(AbsentCount, Object)}, 1)
        'Dim SeriesPoint3 As DevExpress.XtraCharts.SeriesPoint = New DevExpress.XtraCharts.SeriesPoint("Week Off", New Object() {CType(WOCount, Object)}, 2)
        'Dim SeriesPoint4 As DevExpress.XtraCharts.SeriesPoint = New DevExpress.XtraCharts.SeriesPoint("Leave", New Object() {CType(LeaveCount, Object)}, 3)
        'Dim SeriesPoint5 As DevExpress.XtraCharts.SeriesPoint = New DevExpress.XtraCharts.SeriesPoint("Late", New Object() {CType(LateCount, Object)}, 4)
        Dim PieSeriesView1 As DevExpress.XtraCharts.DoughnutSeriesView = New DevExpress.XtraCharts.DoughnutSeriesView()
        'Dim PieSeriesView1 As DevExpress.XtraCharts.PieSeriesView = New DevExpress.XtraCharts.PieSeriesView()
        series1.Label.BackColor = Color.Transparent
        series1.Label.TextColor = Color.Transparent
        series1.Label.Border.Color = Color.Transparent
        series1.Label.TextPattern = "{V}" '{V}"      
        CType(series1.Label, DoughnutSeriesLabel).Position = PieSeriesLabelPosition.Inside
        SeriesPoint1.ColorSerializable = "#4AAECE" '"#32CD33" '"#FF8080"
        'SeriesPoint1.Color = System.Drawing.ColorTranslator.FromHtml("#4AAECE")            
        series1.Points.AddRange(New DevExpress.XtraCharts.SeriesPoint() {SeriesPoint1})
        PieSeriesView1.FillStyle.FillMode = FillMode.Solid
        series1.View = PieSeriesView1
        series1.Label.FillStyle.FillMode = FillMode.Empty
        Me.ChartControlTotal.SeriesSerializable = New DevExpress.XtraCharts.Series() {series1}
        ChartControlTotal.SelectionMode = ElementSelectionMode.Single
        'ChartControlTotal.BorderOptions.Visibility = DevExpress.Utils.DefaultBoolean.False

        Dim lablTotal As New DevExpress.XtraEditors.LabelControl
        lablTotal.Name = "lablTotal"
        lablTotal.Text = DashBoardEmpArr.Count
        lablTotal.Font = New Font("Tahoma", 14, FontStyle.Regular)
        'FontStyle.Bold Or FontStyle.Underline)
        lablTotal.ForeColor = System.Drawing.ColorTranslator.FromHtml("#4AAECE")
        lablTotal.AutoSize = True
        ChartControlTotal.Controls.Add(lablTotal)
        lablTotal.Left = (lablTotal.Parent.Width \ 2) - (lablTotal.Width \ 2)
        lablTotal.Top = (lablTotal.Parent.Height \ 2) - (lablTotal.Height \ 2)

        Dim lablTotalText As New DevExpress.XtraEditors.LabelControl
        lablTotalText.Name = "lablTotalText"
        lablTotalText.Text = "Active Head"
        lablTotalText.Font = New Font("Tahoma", 14, FontStyle.Regular)
        'FontStyle.Bold Or FontStyle.Underline)
        lablTotalText.ForeColor = ColorTranslator.FromHtml("#4AAECE") 'Color.SteelBlue
        lablTotalText.AutoSize = True
        SidePanelTotal.Controls.Add(lablTotalText)
        lablTotalText.Left = (lablTotalText.Parent.Width \ 2) - (lablTotalText.Width \ 2)
        lablTotalText.Top = (lablTotalText.Parent.Height \ 2) - (lablTotalText.Height \ 2)


        'Present
        series1 = New Series("Series 1", ViewType.Doughnut)
        PieSeriesLabel1 = New DevExpress.XtraCharts.PieSeriesLabel()
        SeriesPoint1 = New DevExpress.XtraCharts.SeriesPoint("Active Head", New Object() {CType(100 - PresenCountTakk, Object)}, 0)
        Dim SeriesPoint2 As DevExpress.XtraCharts.SeriesPoint = New DevExpress.XtraCharts.SeriesPoint("Present", New Object() {CType(PresenCountTakk, Object)}, 0)
        PieSeriesView1 = New DevExpress.XtraCharts.DoughnutSeriesView()
        series1.Label.BackColor = Color.Transparent
        series1.Label.TextColor = Color.Transparent
        series1.Label.Border.Color = Color.Transparent
        series1.Label.TextPattern = "{V}" ' {V}"      
        CType(series1.Label, DoughnutSeriesLabel).Position = PieSeriesLabelPosition.Inside
        SeriesPoint1.ColorSerializable = "#EBECEC" '"#32CD33" '"#FF8080"
        SeriesPoint2.ColorSerializable = "#8FB830" '"Red" '"#FFFF80"       
        series1.Points.AddRange(New DevExpress.XtraCharts.SeriesPoint() {SeriesPoint1, SeriesPoint2})
        PieSeriesView1.FillStyle.FillMode = FillMode.Solid
        series1.View = PieSeriesView1
        Me.ChartControlPresent.SeriesSerializable = New DevExpress.XtraCharts.Series() {series1}
        ChartControlPresent.SelectionMode = ElementSelectionMode.Single

        Dim lablPre As New DevExpress.XtraEditors.LabelControl
        lablPre.Name = "lablPre"
        lablPre.Text = PresenCount
        lablPre.Font = New Font("Tahoma", 14, FontStyle.Regular)
        'FontStyle.Bold Or FontStyle.Underline)
        lablPre.ForeColor = ColorTranslator.FromHtml("#8FB830")
        lablPre.AutoSize = True
        ChartControlPresent.Controls.Add(lablPre)
        lablPre.Left = (lablPre.Parent.Width \ 2) - (lablPre.Width \ 2)
        lablPre.Top = (lablPre.Parent.Height \ 2) - (lablPre.Height \ 2)

        Dim lablPreText As New DevExpress.XtraEditors.LabelControl
        lablPreText.Name = "lablPreText"
        lablPreText.Text = "Present"
        lablPreText.Font = New Font("Tahoma", 14, FontStyle.Regular)
        'FontStyle.Bold Or FontStyle.Underline)
        lablPreText.ForeColor = ColorTranslator.FromHtml("#8FB830")
        lablPreText.AutoSize = True
        SidePanelPresent.Controls.Add(lablPreText)
        lablPreText.Left = (lablPreText.Parent.Width \ 2) - (lablPreText.Width \ 2)
        lablPreText.Top = (lablPreText.Parent.Height \ 2) - (lablPreText.Height \ 2)



        'Absent
        series1 = New Series("Series 1", ViewType.Doughnut)
        PieSeriesLabel1 = New DevExpress.XtraCharts.PieSeriesLabel()
        SeriesPoint1 = New DevExpress.XtraCharts.SeriesPoint("Active Head", New Object() {CType(100 - AbsentCountTakk, Object)}, 0)
        SeriesPoint2 = New DevExpress.XtraCharts.SeriesPoint("Absent", New Object() {CType(AbsentCountTakk, Object)}, 0)
        PieSeriesView1 = New DevExpress.XtraCharts.DoughnutSeriesView()
        series1.Label.BackColor = Color.Transparent
        series1.Label.TextColor = Color.Transparent
        series1.Label.Border.Color = Color.Transparent
        series1.Label.TextPattern = "{V}" ' {V}"      
        CType(series1.Label, DoughnutSeriesLabel).Position = PieSeriesLabelPosition.Inside
        SeriesPoint1.ColorSerializable = "#EBECEC" '"#32CD33" '"#FF8080"
        SeriesPoint2.ColorSerializable = "#D94E51" '"Red" '"#FFFF80"       
        series1.Points.AddRange(New DevExpress.XtraCharts.SeriesPoint() {SeriesPoint1, SeriesPoint2})
        PieSeriesView1.FillStyle.FillMode = FillMode.Solid
        series1.View = PieSeriesView1
        Me.ChartControlAbs.SeriesSerializable = New DevExpress.XtraCharts.Series() {series1}
        ChartControlAbs.SelectionMode = ElementSelectionMode.Single

        Dim lablAbs As New DevExpress.XtraEditors.LabelControl
        lablAbs.Name = "lablAbs"
        lablAbs.Text = AbsentCount
        lablAbs.Font = New Font("Tahoma", 14, FontStyle.Regular)
        'FontStyle.Bold Or FontStyle.Underline)
        lablAbs.ForeColor = ColorTranslator.FromHtml("#D94E51")
        lablAbs.AutoSize = True
        ChartControlAbs.Controls.Add(lablAbs)
        lablAbs.Left = (lablAbs.Parent.Width \ 2) - (lablAbs.Width \ 2)
        lablAbs.Top = (lablAbs.Parent.Height \ 2) - (lablAbs.Height \ 2)

        Dim lablAbsText As New DevExpress.XtraEditors.LabelControl
        lablAbsText.Name = "lablAbsText"
        lablAbsText.Text = "Absent"
        lablAbsText.Font = New Font("Tahoma", 14, FontStyle.Regular)
        'FontStyle.Bold Or FontStyle.Underline)
        lablAbsText.ForeColor = ColorTranslator.FromHtml("#D94E51")
        lablAbsText.AutoSize = True
        SidePanelAbs.Controls.Add(lablAbsText)
        lablAbsText.Left = (lablAbsText.Parent.Width \ 2) - (lablAbsText.Width \ 2)
        lablAbsText.Top = (lablAbsText.Parent.Height \ 2) - (lablAbsText.Height \ 2)


        'Late
        series1 = New Series("Series 1", ViewType.Doughnut)
        PieSeriesLabel1 = New DevExpress.XtraCharts.PieSeriesLabel()
        SeriesPoint1 = New DevExpress.XtraCharts.SeriesPoint("Active Head", New Object() {CType(100 - LateCountTakk, Object)}, 0)
        SeriesPoint2 = New DevExpress.XtraCharts.SeriesPoint("Late", New Object() {CType(LateCountTakk, Object)}, 0)
        PieSeriesView1 = New DevExpress.XtraCharts.DoughnutSeriesView()
        series1.Label.BackColor = Color.Transparent
        series1.Label.TextColor = Color.Transparent
        series1.Label.Border.Color = Color.Transparent
        series1.Label.TextPattern = "{V}" ' {V}"      
        CType(series1.Label, DoughnutSeriesLabel).Position = PieSeriesLabelPosition.Inside
        SeriesPoint1.ColorSerializable = "#EBECEC" '"#32CD33" '"#FF8080"
        SeriesPoint2.ColorSerializable = "#16A085" '"Red" '"#FFFF80"       
        series1.Points.AddRange(New DevExpress.XtraCharts.SeriesPoint() {SeriesPoint1, SeriesPoint2})
        PieSeriesView1.FillStyle.FillMode = FillMode.Solid
        series1.View = PieSeriesView1
        Me.ChartControlLate.SeriesSerializable = New DevExpress.XtraCharts.Series() {series1}
        ChartControlLate.SelectionMode = ElementSelectionMode.Single

        Dim labllate As New DevExpress.XtraEditors.LabelControl
        labllate.Name = "lablLate"
        labllate.Text = LateCount
        labllate.Font = New Font("Tahoma", 14, FontStyle.Regular)
        'FontStyle.Bold Or FontStyle.Underline)
        labllate.ForeColor = ColorTranslator.FromHtml("#16A085")
        labllate.AutoSize = True
        ChartControlLate.Controls.Add(labllate)
        labllate.Left = (labllate.Parent.Width \ 2) - (labllate.Width \ 2)
        labllate.Top = (labllate.Parent.Height \ 2) - (labllate.Height \ 2)

        Dim labllateText As New DevExpress.XtraEditors.LabelControl
        labllateText.Name = "lablLateText"
        labllateText.Text = "Late"
        labllateText.Font = New Font("Tahoma", 14, FontStyle.Regular)
        'FontStyle.Bold Or FontStyle.Underline)
        labllateText.ForeColor = ColorTranslator.FromHtml("#16A085")
        labllateText.AutoSize = True
        SidePanelLate.Controls.Add(labllateText)
        labllateText.Left = (labllateText.Parent.Width \ 2) - (labllateText.Width \ 2)
        labllateText.Top = (labllateText.Parent.Height \ 2) - (labllateText.Height \ 2)


        'Leave
        series1 = New Series("Series 1", ViewType.Doughnut)
        PieSeriesLabel1 = New DevExpress.XtraCharts.PieSeriesLabel()
        SeriesPoint1 = New DevExpress.XtraCharts.SeriesPoint("Active Head", New Object() {CType(100 - LeaveCountTakk, Object)}, 0)
        SeriesPoint2 = New DevExpress.XtraCharts.SeriesPoint("Leave", New Object() {CType(LeaveCountTakk, Object)}, 0)
        PieSeriesView1 = New DevExpress.XtraCharts.DoughnutSeriesView()
        series1.Label.BackColor = Color.Transparent
        series1.Label.TextColor = Color.Transparent
        series1.Label.Border.Color = Color.Transparent
        series1.Label.TextPattern = "{V}" ' {V}"      
        CType(series1.Label, DoughnutSeriesLabel).Position = PieSeriesLabelPosition.Inside
        SeriesPoint1.ColorSerializable = "#EBECEC" '"#32CD33" '"#FF8080"
        SeriesPoint2.ColorSerializable = "#EDB407" '"Red" '"#FFFF80"       
        series1.Points.AddRange(New DevExpress.XtraCharts.SeriesPoint() {SeriesPoint1, SeriesPoint2})
        PieSeriesView1.FillStyle.FillMode = FillMode.Solid
        series1.View = PieSeriesView1
        Me.ChartControlLeave.SeriesSerializable = New DevExpress.XtraCharts.Series() {series1}
        ChartControlLeave.SelectionMode = ElementSelectionMode.Single

        Dim lablLeave As New DevExpress.XtraEditors.LabelControl
        lablLeave.Name = "lablLeave"
        lablLeave.Text = LeaveCount
        lablLeave.Font = New Font("Tahoma", 14, FontStyle.Regular)
        'FontStyle.Bold Or FontStyle.Underline)
        lablLeave.ForeColor = ColorTranslator.FromHtml("#EDB407")
        lablLeave.AutoSize = True
        ChartControlLeave.Controls.Add(lablLeave)
        lablLeave.Left = (lablLeave.Parent.Width \ 2) - (lablLeave.Width \ 2)
        lablLeave.Top = (lablLeave.Parent.Height \ 2) - (lablLeave.Height \ 2)

        Dim lablleaveText As New DevExpress.XtraEditors.LabelControl
        lablleaveText.Name = "lablLeaveText"
        lablleaveText.Text = "Leave"
        lablleaveText.Font = New Font("Tahoma", 14, FontStyle.Regular)
        'FontStyle.Bold Or FontStyle.Underline)
        lablleaveText.ForeColor = ColorTranslator.FromHtml("#EDB407")
        lablleaveText.AutoSize = True
        SidePanelLeave.Controls.Add(lablleaveText)
        lablleaveText.Left = (lablleaveText.Parent.Width \ 2) - (lablleaveText.Width \ 2)
        lablleaveText.Top = (lablleaveText.Parent.Height \ 2) - (lablleaveText.Height \ 2)



        'WO
        series1 = New Series("Series 1", ViewType.Doughnut)
        PieSeriesLabel1 = New DevExpress.XtraCharts.PieSeriesLabel()
        SeriesPoint1 = New DevExpress.XtraCharts.SeriesPoint("Active Head", New Object() {CType(100 - WOCountTakk, Object)}, 0)
        SeriesPoint2 = New DevExpress.XtraCharts.SeriesPoint("Week Off", New Object() {CType(WOCountTakk, Object)}, 0)
        PieSeriesView1 = New DevExpress.XtraCharts.DoughnutSeriesView()
        series1.Label.BackColor = Color.Transparent
        series1.Label.TextColor = Color.Transparent
        series1.Label.Border.Color = Color.Transparent
        series1.Label.TextPattern = "{V}" ' {V}"      
        CType(series1.Label, DoughnutSeriesLabel).Position = PieSeriesLabelPosition.Inside
        SeriesPoint1.ColorSerializable = "#EBECEC" '"#32CD33" '"#FF8080"
        SeriesPoint2.ColorSerializable = "#AF76F0" '"Red" '"#FFFF80"       
        series1.Points.AddRange(New DevExpress.XtraCharts.SeriesPoint() {SeriesPoint1, SeriesPoint2})
        PieSeriesView1.FillStyle.FillMode = FillMode.Solid
        series1.View = PieSeriesView1
        Me.ChartControlWO.SeriesSerializable = New DevExpress.XtraCharts.Series() {series1}
        ChartControlWO.SelectionMode = ElementSelectionMode.Single

        Dim lablWO As New DevExpress.XtraEditors.LabelControl
        lablWO.Name = "lablWO"
        lablWO.Text = WOCount
        lablWO.Font = New Font("Tahoma", 14, FontStyle.Regular)
        'FontStyle.Bold Or FontStyle.Underline)
        lablWO.ForeColor = ColorTranslator.FromHtml("#AF76F0")
        lablWO.AutoSize = True
        ChartControlWO.Controls.Add(lablWO)
        lablWO.Left = (lablWO.Parent.Width \ 2) - (lablWO.Width \ 2)
        lablWO.Top = (lablWO.Parent.Height \ 2) - (lablWO.Height \ 2)

        Dim lablWOText As New DevExpress.XtraEditors.LabelControl
        lablWOText.Name = "lablWOText"
        lablWOText.Text = "Week Off"
        lablWOText.Font = New Font("Tahoma", 14, FontStyle.Regular)
        'FontStyle.Bold Or FontStyle.Underline)
        lablWOText.ForeColor = ColorTranslator.FromHtml("#AF76F0")
        lablWOText.AutoSize = True
        SidePanelWO.Controls.Add(lablWOText)
        lablWOText.Left = (lablWOText.Parent.Width \ 2) - (lablWOText.Width \ 2)
        lablWOText.Top = (lablWOText.Parent.Height \ 2) - (lablWOText.Height \ 2)
    End Sub
    Private Sub XtraHomeDashBoard_Leave(sender As System.Object, e As System.EventArgs) Handles MyBase.Leave
        TimerRefreshDashBoard.Enabled = False
        ''MsgBox("leave")
        'Dim svrPort As Integer = 0
        'svrPort = Convert.ToInt32(TextEditPortBio.Text.Trim)
        'If (Me.mbOpenFlag = True) Then
        '    AxRealSvrOcxTcp1.CloseNetwork(svrPort)
        '    Me.mbOpenFlag = False
        '    SimpleButtonStart.Enabled = True
        '    'Me.EnableButtons(True)
        'End If
        'Try
        '    tcp.Stop()   'for zk
        'Catch ex As Exception
        'End Try
    End Sub
    ' Get Host IP
    Protected Function GetIP() As String
        Dim ipHost As IPHostEntry = Dns.Resolve(Dns.GetHostName)
        Dim ipAddr As IPAddress = ipHost.AddressList(0)
        Return ipAddr.ToString
    End Function
    Private Sub TileItemPresent_ItemClick(sender As System.Object, e As DevExpress.XtraEditors.TileItemEventArgs)
        Me.Cursor = Cursors.WaitCursor
        Common.dashBoardClick = "Present"
        Dim daily As XtraReportsDaily = New XtraReportsDaily
        daily.SpotXl_PresentGrid("")
        Common.dashBoardClick = ""
        Me.Cursor = Cursors.Default
    End Sub
    Private Sub TileItemAbsent_ItemClick(sender As System.Object, e As DevExpress.XtraEditors.TileItemEventArgs)
        Me.Cursor = Cursors.WaitCursor
        Common.dashBoardClick = "Absent"
        Dim daily As XtraReportsDaily = New XtraReportsDaily
        daily.SpotXl_AbsenteeismGrid("")
        Common.dashBoardClick = ""
        Me.Cursor = Cursors.Default
    End Sub
    Private Sub TileItemWO_ItemClick(sender As System.Object, e As DevExpress.XtraEditors.TileItemEventArgs)
        'If TileItemWO.Text = "0" Then
        '    XtraMessageBox.Show(ulf, "<size=10>No Data Available For this Report.</size>", "<size=9>iAS</size>")
        '    Exit Sub
        'End If
        Me.Cursor = Cursors.WaitCursor
        Common.dashBoardClick = "WO"
        XtraDashBoardClickGrid.ShowDialog()
        Common.dashBoardClick = ""
        Me.Cursor = Cursors.Default
    End Sub
    Private Sub TileItemLeave_ItemClick(sender As System.Object, e As DevExpress.XtraEditors.TileItemEventArgs)
        Me.Cursor = Cursors.WaitCursor
        Common.dashBoardClick = "Leave"
        Dim daily As XtraLeaveReport = New XtraLeaveReport
        daily.MonthlyXl_SanctionedLeave("")
        Common.dashBoardClick = ""
        Me.Cursor = Cursors.Default
    End Sub
    Private Sub TileItemLate_ItemClick(sender As System.Object, e As DevExpress.XtraEditors.TileItemEventArgs)
        Me.Cursor = Cursors.WaitCursor
        Common.dashBoardClick = "Late"
        Dim daily As XtraReportsDaily = New XtraReportsDaily
        daily.SpotXl_LateArrivalGrid("")
        Common.dashBoardClick = ""
        Me.Cursor = Cursors.Default
    End Sub
    Private Sub XtraHomeNew_VisibleChanged(sender As System.Object, e As System.EventArgs) Handles MyBase.VisibleChanged
        If MyBase.Visible = False Then
            TimerRefreshDashBoard.Enabled = False
        End If
    End Sub
    Private Sub TileItemTotal_ItemClick(sender As System.Object, e As DevExpress.XtraEditors.TileItemEventArgs)
        XtraMasterTest.SidePanelTitle.Visible = True
        XtraMasterTest.LabelTitle.Text = " Employee"
        XtraMasterTest.SidePanelMainFormShow.Controls.Clear()
        Dim form As UserControl = New XtraEmployee
        'form.TopLevel = False
        'form.FormBorderStyle = FormBorderStyle.None
        form.Dock = DockStyle.Fill
        XtraMasterTest.SidePanelMainFormShow.Controls.Add(form)
        form.Show()
    End Sub
    Private Sub SidePanelImg11_Click(sender As System.Object, e As System.EventArgs) Handles SidePanelImg11.Click
        XtraMasterTest.SidePanelTitle.Visible = True
        XtraMasterTest.LabelTitle.Text = " Shift"
        XtraMasterTest.SidePanelMainFormShow.Controls.Clear()
        Dim form As UserControl = New XtraShift
        'form.TopLevel = False
        'form.FormBorderStyle = FormBorderStyle.None
        form.Dock = DockStyle.Fill
        XtraMasterTest.SidePanelMainFormShow.Controls.Add(form)
        form.Show()
    End Sub
    Private Sub SidePanelImg21_Click(sender As System.Object, e As System.EventArgs) Handles SidePanelImg21.Click
        XtraMasterTest.SidePanelTitle.Visible = True
        XtraMasterTest.LabelTitle.Text = " Employee Group"
        XtraMasterTest.SidePanelMainFormShow.Controls.Clear()
        Dim form As UserControl = New XtraEmployeeGroup
        'form.TopLevel = False
        'form.FormBorderStyle = FormBorderStyle.None
        form.Dock = DockStyle.Fill
        XtraMasterTest.SidePanelMainFormShow.Controls.Add(form)
        form.Show()
    End Sub
    Private Sub SidePanelImg22_Click(sender As System.Object, e As System.EventArgs) Handles SidePanelImg22.Click
        XtraMasterTest.SidePanelTitle.Visible = True
        XtraMasterTest.LabelTitle.Text = " Employee"
        XtraMasterTest.SidePanelMainFormShow.Controls.Clear()
        Dim form As UserControl = New XtraEmployee
        'form.TopLevel = False
        'form.FormBorderStyle = FormBorderStyle.None
        form.Dock = DockStyle.Fill
        XtraMasterTest.SidePanelMainFormShow.Controls.Add(form)
        form.Show()
    End Sub
    Private Sub SidePanelImg32_Click(sender As System.Object, e As System.EventArgs) Handles SidePanelImg32.Click
        XtraMasterTest.SidePanelTitle.Visible = True
        XtraMasterTest.LabelTitle.Text = " Device Management iAS"
        XtraMasterTest.SidePanelMainFormShow.Controls.Clear()
        Dim form As UserControl = New XtraDevice
        'form.TopLevel = False
        'form.FormBorderStyle = FormBorderStyle.None
        form.Dock = DockStyle.Fill
        XtraMasterTest.SidePanelMainFormShow.Controls.Add(form)
        form.Show()
    End Sub
    Private Sub SidePanelImg33_Click(sender As System.Object, e As System.EventArgs) Handles SidePanelImg33.Click
        XtraMasterTest.SidePanelTitle.Visible = True
        XtraMasterTest.LabelTitle.Text = " Monthly Reports"
        XtraMasterTest.SidePanelMainFormShow.Controls.Clear()
        Dim form As UserControl = New XtraReportsMonthly
        'form.TopLevel = False
        'form.FormBorderStyle = FormBorderStyle.None
        form.Dock = DockStyle.Fill
        XtraMasterTest.SidePanelMainFormShow.Controls.Add(form)
        form.Show()
    End Sub
    Private Sub ChartControlTotal_Click(sender As System.Object, e As System.EventArgs) Handles ChartControlTotal.Click
        Dim hi As ChartHitInfo = ChartControlTotal.CalcHitInfo(ChartControlTotal.PointToClient(MousePosition))
        If hi.InSeriesPoint Then
            XtraMasterTest.SidePanelTitle.Visible = True
            XtraMasterTest.LabelTitle.Text = " Employee"
            XtraMasterTest.SidePanelMainFormShow.Controls.Clear()
            Dim form As UserControl = New XtraEmployee
            form.Dock = DockStyle.Fill
            XtraMasterTest.SidePanelMainFormShow.Controls.Add(form)
            form.Show()
        End If
    End Sub
    Private Sub ChartControlTotal_ObjectSelected(sender As Object, e As HotTrackEventArgs) Handles ChartControlTotal.ObjectSelected
        'If Not (TypeOf e.[Object] Is Series) Then
        e.Cancel = True
        'End If
    End Sub
    Private Sub ChartControlTotal_ObjectHotTracked(sender As Object, e As HotTrackEventArgs) Handles ChartControlTotal.ObjectHotTracked
        If Not (TypeOf e.[Object] Is Series) Then
            e.Cancel = True
        End If
    End Sub
    Private Sub ChartControlPresent_ObjectSelected(sender As Object, e As HotTrackEventArgs) Handles ChartControlPresent.ObjectSelected
        e.Cancel = True
        If (TypeOf e.[Object] Is Series) Then
            Me.Cursor = Cursors.WaitCursor
            Common.dashBoardClick = "Present"
            Dim daily As XtraReportsDaily = New XtraReportsDaily
            daily.SpotXl_PresentGrid("")
            Common.dashBoardClick = ""
            Me.Cursor = Cursors.Default
        End If
    End Sub
    Private Sub ChartControlPresent_ObjectHotTracked(sender As Object, e As HotTrackEventArgs) Handles ChartControlPresent.ObjectHotTracked
        If Not (TypeOf e.[Object] Is Series) Then
            e.Cancel = True
        End If
    End Sub
    Private Sub ChartControlAbs_ObjectSelected(sender As Object, e As HotTrackEventArgs) Handles ChartControlAbs.ObjectSelected
        e.Cancel = True
        If (TypeOf e.[Object] Is Series) Then
            Me.Cursor = Cursors.WaitCursor
            Common.dashBoardClick = "Absent"
            Dim daily As XtraReportsDaily = New XtraReportsDaily
            daily.SpotXl_AbsenteeismGrid("")
            Common.dashBoardClick = ""
            Me.Cursor = Cursors.Default
        End If
    End Sub
    Private Sub ChartControlAbs_ObjectHotTracked(sender As Object, e As HotTrackEventArgs) Handles ChartControlAbs.ObjectHotTracked
        If Not (TypeOf e.[Object] Is Series) Then
            e.Cancel = True
        End If
    End Sub
    Private Sub ChartControlLate_ObjectSelected(sender As Object, e As HotTrackEventArgs) Handles ChartControlLate.ObjectSelected
        e.Cancel = True
        If (TypeOf e.[Object] Is Series) Then
            Me.Cursor = Cursors.WaitCursor
            Common.dashBoardClick = "Late"
            Dim daily As XtraReportsDaily = New XtraReportsDaily
            daily.SpotXl_LateArrivalGrid("")
            Common.dashBoardClick = ""
            Me.Cursor = Cursors.Default
        End If
    End Sub
    Private Sub ChartControlLate_ObjectHotTracked(sender As Object, e As HotTrackEventArgs) Handles ChartControlLate.ObjectHotTracked
        If Not (TypeOf e.[Object] Is Series) Then
            e.Cancel = True
        End If
    End Sub
    Private Sub ChartControlLeave_ObjectSelected(sender As Object, e As HotTrackEventArgs) Handles ChartControlLeave.ObjectSelected
        e.Cancel = True
        If (TypeOf e.[Object] Is Series) Then
            Me.Cursor = Cursors.WaitCursor
            Common.dashBoardClick = "Leave"
            Dim daily As XtraLeaveReport = New XtraLeaveReport
            daily.MonthlyXl_SanctionedLeave("")
            Common.dashBoardClick = ""
            Me.Cursor = Cursors.Default
        End If
    End Sub
    Private Sub ChartControlLeave_ObjectHotTracked(sender As Object, e As HotTrackEventArgs) Handles ChartControlLeave.ObjectHotTracked
        If Not (TypeOf e.[Object] Is Series) Then
            e.Cancel = True
        End If
    End Sub
    Private Sub ChartControlWO_ObjectSelected(sender As Object, e As HotTrackEventArgs) Handles ChartControlWO.ObjectSelected
        e.Cancel = True
        If (TypeOf e.[Object] Is Series) Then
            If WOCountTmp = "0" Then
                XtraMessageBox.Show(ulf, "<size=10>No Data Available For this Report.</size>", "<size=9>iAS</size>")
                Exit Sub
            End If
            Me.Cursor = Cursors.WaitCursor
            Common.dashBoardClick = "WO"
            XtraDashBoardClickGrid.ShowDialog()
            Common.dashBoardClick = ""
            Me.Cursor = Cursors.Default
        End If
    End Sub
    Private Sub ChartControlWO_ObjectHotTracked(sender As Object, e As HotTrackEventArgs) Handles ChartControlWO.ObjectHotTracked
        If Not (TypeOf e.[Object] Is Series) Then
            e.Cancel = True
        End If
    End Sub
    Private Sub ChartControlPresent_Click(sender As System.Object, e As System.EventArgs) Handles ChartControlPresent.Click
        'Dim hi As ChartHitInfo = ChartControlTotal.CalcHitInfo(ChartControlTotal.PointToClient(MousePosition))
        'If hi.InSeriesPoint Then
        '    Me.Cursor = Cursors.WaitCursor
        '    Common.dashBoardClick = "Present"
        '    Dim daily As XtraReportsDaily = New XtraReportsDaily
        '    daily.SpotXl_PresentGrid("")
        '    Common.dashBoardClick = ""
        '    Me.Cursor = Cursors.Default
        'End If
    End Sub
    Private Sub ChartControlAbs_Click(sender As System.Object, e As System.EventArgs) Handles ChartControlAbs.Click
        'Dim hi As ChartHitInfo = ChartControlTotal.CalcHitInfo(ChartControlTotal.PointToClient(MousePosition))
        'If hi.InSeriesPoint Then
        '    Me.Cursor = Cursors.WaitCursor
        '    Common.dashBoardClick = "Absent"
        '    Dim daily As XtraReportsDaily = New XtraReportsDaily
        '    daily.SpotXl_AbsenteeismGrid("")
        '    Common.dashBoardClick = ""
        '    Me.Cursor = Cursors.Default
        'End If
    End Sub
    Private Sub Labeltrial_Click(sender As System.Object, e As System.EventArgs)
        XtraCompanyInfo.ShowDialog()
    End Sub
End Class
