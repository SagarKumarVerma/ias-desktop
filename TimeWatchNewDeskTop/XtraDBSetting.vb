﻿Imports System.Data.OleDb
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.Data.SqlClient
Imports System.IO
Imports System.Resources
Imports System.Globalization
Imports System.Threading

Public Class XtraDBSetting
    Dim connection As SqlConnection
    Dim ConnectionString As String
    Dim Con1 As OleDbConnection
    Dim ulf As UserLookAndFeel

    Private Sub XtraDBSetting_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        'for xtramessage box
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True


        Me.Width = Common.NavWidth 'Me.Parent.Width
        Me.Height = Common.NavHeight 'Me.Parent.Height
        'SplitContainerControl1.Width = Common.splitforMasterMenuWidth 'SplitContainerControl1.Parent.Width
        SplitContainerControl1.SplitterPosition = Common.SplitterPosition '(SplitContainerControl1.Parent.Width) * 85 / 100
        'Common.res_man = New ResourceManager("TimeWatchNewDeskTop.Res", GetType(XtraCompany).Assembly)
        'Common.cul = CultureInfo.CreateSpecificCulture("en")

        'res_man = New ResourceManager("TimeWatchNewDeskTop.Res", GetType(XtraDB).Assembly)
        'cul = CultureInfo.CreateSpecificCulture("en")

        Me.Text = Common.res_man.GetString("db_setting", Common.cul)
        LabelControl1.Text = "Server Name" ' Common.res_man.GetString("dns_name", Common.cul)
        LabelControl2.Text = Common.res_man.GetString("database_name", Common.cul)
        LabelControl3.Text = Common.res_man.GetString("database_type", Common.cul)
        SimpleButton1.Text = Common.res_man.GetString("test_connection", Common.cul)
        SimpleButton2.Text = Common.res_man.GetString("save", Common.cul)
        'SimpleButton3.Text = Common.res_man.GetString("close", Common.cul)

        ComboBoxEdit1.Properties.Items.Add(Common.res_man.GetString("access", Common.cul))
        ComboBoxEdit1.Properties.Items.Add(Common.res_man.GetString("sql", Common.cul))

        Dim servername As String = ""
        Dim fs As FileStream = New FileStream("db.txt", FileMode.Open, FileAccess.Read)
        Dim sr As StreamReader = New StreamReader(fs)
        Dim str As String
        Dim str1() As String
        Do While sr.Peek <> -1
            str = sr.ReadLine
            str1 = str.Split(",")
            servername = str1(0)
        Loop
        sr.Close()
        fs.Close()
        If servername = "Access" Then
            ComboBoxEdit1.SelectedIndex = 0
            TextEdit1.Enabled = False
            TextEdit2.Enabled = False
            TextEdit1.Text = ""
            TextEdit2.Text = ""
            ComboBoxEdit2.Visible = False
            LabelControl4.Visible = False
            LabelControl5.Visible = False
            LabelControl6.Visible = False
            TextEdit3.Visible = False
            TextEdit4.Visible = False
        Else
            ComboBoxEdit1.SelectedIndex = 1
            If str1(2) = "SQL" Then
                ComboBoxEdit2.SelectedIndex = 1
                TextEdit3.Enabled = True
                TextEdit3.Visible = True
                TextEdit3.Text = str1(3)
                TextEdit4.Enabled = True
                TextEdit4.Visible = True
                TextEdit4.Text = str1(4)
            ElseIf str1(2) = "Win" Then
                ComboBoxEdit2.SelectedIndex = 0
                TextEdit3.Visible = False
                TextEdit3.Enabled = False
                TextEdit3.Text = ""
                TextEdit4.Visible = False
                TextEdit4.Enabled = False
                TextEdit4.Text = ""
            End If
            TextEdit1.Text = str1(0)
            TextEdit2.Text = str1(1)
            ComboBoxEdit2.Enabled = True
            ComboBoxEdit2.Visible = True
        End If
    End Sub
    Private Sub SimpleButton1_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton1.Click
        If ComboBoxEdit1.Text = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Select database type</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            ComboBoxEdit1.Select()
        Else
            If ComboBoxEdit1.Text = Common.res_man.GetString("access", Common.cul) Then
                Me.Cursor = Cursors.WaitCursor
                Try
                    Dim constr As String = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\TimeWatch.mdb;Persist Security Info=True;Jet OLEDB:Database Password=SSS"
                    Con1 = New OleDbConnection(constr)
                    Con1.Open()
                    If Con1.State = ConnectionState.Open Then
                        Con1.Close()
                        XtraMessageBox.Show(ulf, "<size=10>Connection Established Successfully</size>", "Success")
                        Common.servername = "Access"
                    Else
                        Con1.Close()
                        Me.Cursor = Cursors.Default
                        XtraMessageBox.Show(ulf, "<size=10>Connection Not Establish</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    End If
                Catch
                    If Con1.State = ConnectionState.Open Then
                        Con1.Close()
                        Me.Cursor = Cursors.Default
                        XtraMessageBox.Show(ulf, "<size=10>Connection Not Establish</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    End If
                End Try
            ElseIf ComboBoxEdit1.Text = Common.res_man.GetString("sql", Common.cul) Then
                If TextEdit1.Text = "" Then
                    XtraMessageBox.Show(ulf, "<size=10>Enter Server Name</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    TextEdit1.Select()
                ElseIf TextEdit2.Text = "" Then
                    XtraMessageBox.Show(ulf, "<size=10>Enter Database Name</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    TextEdit2.Select()
                ElseIf ComboBoxEdit2.EditValue <> "Windows" And TextEdit3.Text = "" Then
                    XtraMessageBox.Show(ulf, "<size=10>Enter User Name</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    TextEdit3.Select()
                ElseIf ComboBoxEdit2.EditValue <> "Windows" And TextEdit4.Text = "" Then
                    XtraMessageBox.Show(ulf, "<size=10>Enter Password</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    TextEdit4.Select()
                Else
                    Me.Cursor = Cursors.WaitCursor
                    Dim ret As Boolean = Common.CheckDatabaseExists(TextEdit1.Text.Trim, TextEdit2.Text.Trim, ComboBoxEdit2.SelectedIndex, TextEdit3.Text.Trim, TextEdit4.Text.Trim)
                    If ret = False Then
                        Dim ret1 As Boolean = Common.checkAndAttachDB(TextEdit1.Text.Trim, TextEdit2.Text.Trim, ComboBoxEdit2.SelectedIndex, TextEdit3.Text.Trim, TextEdit4.Text.Trim)
                        If ret1 = False Then
                            Me.Cursor = Cursors.Default
                            XtraMessageBox.Show(ulf, "<size=10>    DB Attach fail. </size>", "Fail")
                            Exit Sub
                        End If
                    End If
                    Try
                        If ComboBoxEdit2.EditValue = "Windows" Then
                            ConnectionString = "server = '" & TextEdit1.Text & "' ;Initial Catalog= '" & TextEdit2.Text & "';Integrated Security=True"
                        Else
                            ConnectionString = "server = '" & TextEdit1.Text & "' ;Initial Catalog= '" & TextEdit2.Text & "';User Id=" & TextEdit3.Text & ";Password=" & TextEdit4.Text & ";"
                        End If
                        'MsgBox(ConnectionString)
                        connection = New SqlConnection(ConnectionString)
                        connection.Open()
                        If connection.State = ConnectionState.Open Then
                            XtraMessageBox.Show(ulf, "<size=10>Connection Established Successfully</size>", "Success")
                            connection.Close()
                            Common.servername = "TextEdit1.Text"
                        Else
                            Me.Cursor = Cursors.Default
                            XtraMessageBox.Show(ulf, "<size=10>Connection Not Establish</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            connection.Close()
                        End If
                    Catch ex As Exception
                        If connection.State = ConnectionState.Open Then
                            connection.Close()
                        End If
                        Me.Cursor = Cursors.Default
                        XtraMessageBox.Show(ulf, "<size=10>Connection Not Establish</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    End Try
                    Me.Cursor = Cursors.Default
                End If
            End If
        End If
        Me.Cursor = Cursors.WaitCursor
    End Sub

    Private Sub SimpleButton2_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton2.Click
        If ComboBoxEdit1.Text = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Select database type</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            ComboBoxEdit1.Select()
        Else
            If ComboBoxEdit1.Text = Common.res_man.GetString("access", Common.cul) Then
                Me.Cursor = Cursors.WaitCursor
                Try
                    Dim constr As String = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\TimeWatch.mdb;Persist Security Info=True;Jet OLEDB:Database Password=SSS"
                    Con1 = New OleDbConnection(constr)
                    Con1.Open()
                    If Con1.State = ConnectionState.Open Then
                        Con1.Close()
                        If XtraMessageBox.Show(ulf, "<size=10>Are you sure to save the setting?</size>", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) <> Windows.Forms.DialogResult.Yes Then
                            Exit Sub
                        End If
                        Dim fs As FileStream = New FileStream("db.txt", FileMode.Create, FileAccess.Write)
                        Dim sw As StreamWriter = New StreamWriter(fs)
                        Dim str As String = "Access"
                        sw.Write(str)
                        sw.Flush()
                        sw.Close()
                        fs.Close()
                        XtraMessageBox.Show(ulf, "<size=10>Saved</size>", "Success")
                        If Application.OpenForms().OfType(Of XtraRealTimePunches).Any Then  'to check real time is runing or not
                            XtraRealTimePunches.Close()
                            XtraRealTimePunchesUltra.Close()
                        End If
                        Application.Exit()
                        Thread.Sleep(500)
                        Process.Start(Application.ExecutablePath)
                        'Application.Restart()
                        'Me.Close()
                    End If
                Catch
                    If Con1.State = ConnectionState.Open Then
                        Con1.Close()
                        Me.Cursor = Cursors.Default
                        XtraMessageBox.Show(ulf, "<size=10>Connection Not Establish</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    End If
                End Try
            ElseIf ComboBoxEdit1.Text = Common.res_man.GetString("sql", Common.cul) Then
                If TextEdit1.Text = "" Then
                    XtraMessageBox.Show(ulf, "<size=10>Enter Server Name</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    TextEdit1.Select()
                ElseIf TextEdit1.Text = "" Then
                    XtraMessageBox.Show(ulf, "<size=10>Enter Database Name</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    TextEdit2.Select()
                ElseIf ComboBoxEdit2.EditValue <> "Windows" And TextEdit3.Text = "" Then
                    XtraMessageBox.Show(ulf, "<size=10>Enter User Name</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    TextEdit3.Select()
                ElseIf ComboBoxEdit2.EditValue <> "Windows" And TextEdit4.Text = "" Then
                    XtraMessageBox.Show(ulf, "<size=10>Enter Password</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    TextEdit4.Select()
                Else
                    Me.Cursor = Cursors.WaitCursor
                    Dim ret As Boolean = Common.CheckDatabaseExists(TextEdit1.Text.Trim, TextEdit2.Text.Trim, ComboBoxEdit2.SelectedIndex, TextEdit3.Text.Trim, TextEdit4.Text.Trim)
                    If ret = False Then
                        Dim ret1 As Boolean = Common.checkAndAttachDB(TextEdit1.Text.Trim, TextEdit2.Text.Trim, ComboBoxEdit2.SelectedIndex, TextEdit3.Text.Trim, TextEdit4.Text.Trim)
                        If ret1 = False Then
                            Me.Cursor = Cursors.Default
                            XtraMessageBox.Show(ulf, "<size=10>    DB Attach fail. </size>", "Fail")
                            Exit Sub
                        End If
                    End If
                    Try
                        If ComboBoxEdit2.EditValue = "Windows" Then
                            ConnectionString = "server = '" & TextEdit1.Text & "' ;Initial Catalog= '" & TextEdit2.Text & "';Integrated Security=True"
                        Else
                            ConnectionString = "server = '" & TextEdit1.Text & "' ;Initial Catalog= '" & TextEdit2.Text & "';User Id=" & TextEdit3.Text & ";Password=" & TextEdit4.Text & ";"
                            'ConnectionString = "server = '" & TextEdit1.Text & "' ;Initial Catalog= '" & TextEdit2.Text & "';User Id=" & TextEdit3.Text & ";Password=" & TextEdit4.Text & ""
                        End If
                        'MsgBox(ConnectionString)
                        connection = New SqlConnection(ConnectionString)
                        connection.Open()
                        If connection.State = ConnectionState.Open Then
                            connection.Close()
                            If XtraMessageBox.Show(ulf, "<size=10>Are you sure to save the setting?</size>", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) <> Windows.Forms.DialogResult.Yes Then
                                Me.Cursor = Cursors.Default
                                Exit Sub
                            End If
                            Dim ApplicationType As String = "S"
                            If CheckClient.Checked Then
                                ApplicationType = "C"
                            Else
                                ApplicationType = "S"
                            End If
                            Common.ApplicationType = ApplicationType

                            Dim fs As FileStream = New FileStream("db.txt", FileMode.Create, FileAccess.Write)
                            Dim sw As StreamWriter = New StreamWriter(fs)
                            Dim str As String = ""
                            If ComboBoxEdit2.EditValue = "Windows" Then
                                str = "" & TextEdit1.Text & "," & TextEdit2.Text & "," & "Win," & ApplicationType
                            Else
                                str = "" & TextEdit1.Text & "," & TextEdit2.Text & "," & "SQL" & "," & TextEdit3.Text & "," & TextEdit4.Text & "," & ApplicationType
                            End If

                            sw.Write(str)
                            sw.Flush()
                            sw.Close()
                            fs.Close()
                            XtraMessageBox.Show(ulf, "<size=10>Saved</size>", "Success")
                            If Application.OpenForms().OfType(Of XtraRealTimePunches).Any Then  'to check real time is runing or not
                                XtraRealTimePunches.Close()
                                XtraRealTimePunchesUltra.Close()
                            End If
                            Application.Exit()
                            Thread.Sleep(500)
                            Process.Start(Application.ExecutablePath)
                            'Application.Restart()
                            'Me.Close()
                        Else
                            XtraMessageBox.Show(ulf, "<size=10>Connection Not Establish</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        End If
                    Catch ex As Exception
                        'MsgBox(ex.Message)
                        If connection.State = ConnectionState.Open Then
                            connection.Close()
                        End If
                        Me.Cursor = Cursors.Default
                        XtraMessageBox.Show(ulf, "<size=10>Connection Not Establish</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    End Try
                End If
            End If
        End If
        Common.LogPost("DataBase Setting Save")
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub ComboBoxEdit1_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBoxEdit1.SelectedIndexChanged
        If ComboBoxEdit1.Text = Common.res_man.GetString("access", Common.cul) Then
            LabelControl1.Enabled = False
            TextEdit1.Enabled = False
            LabelControl2.Enabled = False
            TextEdit2.Enabled = False
            TextEdit1.Enabled = False
            TextEdit2.Enabled = False
            ComboBoxEdit2.Visible = False
            LabelControl4.Visible = False
            LabelControl5.Visible = False
            LabelControl6.Visible = False
            TextEdit3.Visible = False
            TextEdit4.Visible = False
            CheckServer.Visible = False
            CheckClient.Visible = False
            'SimpleButton1.Enabled = False

        ElseIf ComboBoxEdit1.Text = Common.res_man.GetString("sql", Common.cul) Then
            LabelControl1.Enabled = True
            TextEdit1.Enabled = True
            LabelControl2.Enabled = True
            TextEdit2.Enabled = True

            TextEdit1.Enabled = True
            TextEdit2.Enabled = True
            ComboBoxEdit2.Visible = True
            ComboBoxEdit2.Enabled = True
            LabelControl4.Visible = True
            If ComboBoxEdit2.SelectedIndex = 0 Then
                LabelControl5.Visible = False
                LabelControl6.Visible = False
                TextEdit3.Visible = False
                TextEdit4.Visible = False
            ElseIf ComboBoxEdit2.SelectedIndex = 1 Then
                LabelControl5.Visible = True
                LabelControl6.Visible = True
                TextEdit3.Visible = True
                TextEdit4.Visible = True
            End If
            CheckServer.Visible = True
            CheckClient.Visible = True
            'SimpleButton1.Enabled = True
        End If
    End Sub

    Private Sub ComboBoxEdit2_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBoxEdit2.SelectedIndexChanged
        If ComboBoxEdit2.EditValue = "Windows" Then
            LabelControl5.Visible = False
            LabelControl6.Visible = False
            TextEdit3.Visible = False
            TextEdit4.Visible = False
        Else
            LabelControl5.Visible = True
            LabelControl6.Visible = True
            TextEdit3.Visible = True
            TextEdit4.Visible = True
            TextEdit3.Enabled = True
            TextEdit4.Enabled = True
        End If
    End Sub
End Class
