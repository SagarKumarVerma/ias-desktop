﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraConpanyInfo
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(XtraConpanyInfo))
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditCompanyName = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditContact = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditEmail = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditUserKey = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditLicense = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.SimpleButtonSave = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButtonVerify = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.MemoEditAddress = New DevExpress.XtraEditors.MemoEdit()
        CType(Me.TextEditCompanyName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditContact.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditEmail.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditUserKey.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditLicense.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MemoEditAddress.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Location = New System.Drawing.Point(13, 26)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(85, 14)
        Me.LabelControl1.TabIndex = 0
        Me.LabelControl1.Text = "Company Name"
        '
        'TextEditCompanyName
        '
        Me.TextEditCompanyName.Location = New System.Drawing.Point(167, 24)
        Me.TextEditCompanyName.Name = "TextEditCompanyName"
        Me.TextEditCompanyName.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditCompanyName.Properties.Appearance.Options.UseFont = True
        Me.TextEditCompanyName.Properties.MaxLength = 50
        Me.TextEditCompanyName.Size = New System.Drawing.Size(304, 20)
        Me.TextEditCompanyName.TabIndex = 1
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl2.Appearance.Options.UseFont = True
        Me.LabelControl2.Location = New System.Drawing.Point(13, 52)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(97, 14)
        Me.LabelControl2.TabIndex = 10
        Me.LabelControl2.Text = "Company Address"
        '
        'TextEditContact
        '
        Me.TextEditContact.Location = New System.Drawing.Point(167, 152)
        Me.TextEditContact.Name = "TextEditContact"
        Me.TextEditContact.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditContact.Properties.Appearance.Options.UseFont = True
        Me.TextEditContact.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEditContact.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEditContact.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditContact.Properties.MaxLength = 50
        Me.TextEditContact.Size = New System.Drawing.Size(304, 20)
        Me.TextEditContact.TabIndex = 3
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl3.Appearance.Options.UseFont = True
        Me.LabelControl3.Location = New System.Drawing.Point(13, 154)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(97, 14)
        Me.LabelControl3.TabIndex = 4
        Me.LabelControl3.Text = "Company Contact"
        '
        'TextEditEmail
        '
        Me.TextEditEmail.Location = New System.Drawing.Point(167, 178)
        Me.TextEditEmail.Name = "TextEditEmail"
        Me.TextEditEmail.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditEmail.Properties.Appearance.Options.UseFont = True
        Me.TextEditEmail.Properties.MaxLength = 50
        Me.TextEditEmail.Size = New System.Drawing.Size(304, 20)
        Me.TextEditEmail.TabIndex = 4
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl4.Appearance.Options.UseFont = True
        Me.LabelControl4.Location = New System.Drawing.Point(13, 180)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(81, 14)
        Me.LabelControl4.TabIndex = 6
        Me.LabelControl4.Text = "Company Email"
        '
        'TextEditUserKey
        '
        Me.TextEditUserKey.Location = New System.Drawing.Point(167, 204)
        Me.TextEditUserKey.Name = "TextEditUserKey"
        Me.TextEditUserKey.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditUserKey.Properties.Appearance.Options.UseFont = True
        Me.TextEditUserKey.Properties.MaxLength = 50
        Me.TextEditUserKey.Size = New System.Drawing.Size(304, 20)
        Me.TextEditUserKey.TabIndex = 5
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl5.Appearance.Options.UseFont = True
        Me.LabelControl5.Location = New System.Drawing.Point(13, 206)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(48, 14)
        Me.LabelControl5.TabIndex = 8
        Me.LabelControl5.Text = "User Key"
        '
        'TextEditLicense
        '
        Me.TextEditLicense.Location = New System.Drawing.Point(167, 230)
        Me.TextEditLicense.Name = "TextEditLicense"
        Me.TextEditLicense.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditLicense.Properties.Appearance.Options.UseFont = True
        Me.TextEditLicense.Properties.MaxLength = 50
        Me.TextEditLicense.Properties.ReadOnly = True
        Me.TextEditLicense.Size = New System.Drawing.Size(304, 20)
        Me.TextEditLicense.TabIndex = 6
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl6.Appearance.Options.UseFont = True
        Me.LabelControl6.Location = New System.Drawing.Point(13, 232)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(40, 14)
        Me.LabelControl6.TabIndex = 10
        Me.LabelControl6.Text = "License"
        '
        'SimpleButtonSave
        '
        Me.SimpleButtonSave.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonSave.Appearance.Options.UseFont = True
        Me.SimpleButtonSave.Enabled = False
        Me.SimpleButtonSave.Location = New System.Drawing.Point(329, 266)
        Me.SimpleButtonSave.Name = "SimpleButtonSave"
        Me.SimpleButtonSave.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButtonSave.TabIndex = 8
        Me.SimpleButtonSave.Text = "Save"
        '
        'SimpleButtonVerify
        '
        Me.SimpleButtonVerify.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonVerify.Appearance.Options.UseFont = True
        Me.SimpleButtonVerify.Location = New System.Drawing.Point(167, 266)
        Me.SimpleButtonVerify.Name = "SimpleButtonVerify"
        Me.SimpleButtonVerify.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButtonVerify.TabIndex = 7
        Me.SimpleButtonVerify.Text = "Activate"
        '
        'LabelControl7
        '
        Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl7.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl7.Appearance.Options.UseFont = True
        Me.LabelControl7.Appearance.Options.UseForeColor = True
        Me.LabelControl7.Location = New System.Drawing.Point(105, 27)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(7, 14)
        Me.LabelControl7.TabIndex = 14
        Me.LabelControl7.Text = "*"
        '
        'LabelControl8
        '
        Me.LabelControl8.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl8.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl8.Appearance.Options.UseFont = True
        Me.LabelControl8.Appearance.Options.UseForeColor = True
        Me.LabelControl8.Location = New System.Drawing.Point(116, 52)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(7, 14)
        Me.LabelControl8.TabIndex = 15
        Me.LabelControl8.Text = "*"
        '
        'LabelControl9
        '
        Me.LabelControl9.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl9.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl9.Appearance.Options.UseFont = True
        Me.LabelControl9.Appearance.Options.UseForeColor = True
        Me.LabelControl9.Location = New System.Drawing.Point(116, 154)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(7, 14)
        Me.LabelControl9.TabIndex = 16
        Me.LabelControl9.Text = "*"
        '
        'LabelControl10
        '
        Me.LabelControl10.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl10.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl10.Appearance.Options.UseFont = True
        Me.LabelControl10.Appearance.Options.UseForeColor = True
        Me.LabelControl10.Location = New System.Drawing.Point(100, 180)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(7, 14)
        Me.LabelControl10.TabIndex = 17
        Me.LabelControl10.Text = "*"
        '
        'LabelControl11
        '
        Me.LabelControl11.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl11.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl11.Appearance.Options.UseFont = True
        Me.LabelControl11.Appearance.Options.UseForeColor = True
        Me.LabelControl11.Location = New System.Drawing.Point(67, 207)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(7, 14)
        Me.LabelControl11.TabIndex = 18
        Me.LabelControl11.Text = "*"
        '
        'MemoEditAddress
        '
        Me.MemoEditAddress.Location = New System.Drawing.Point(167, 51)
        Me.MemoEditAddress.Name = "MemoEditAddress"
        Me.MemoEditAddress.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.MemoEditAddress.Properties.Appearance.Options.UseFont = True
        Me.MemoEditAddress.Properties.MaxLength = 250
        Me.MemoEditAddress.Size = New System.Drawing.Size(304, 95)
        Me.MemoEditAddress.TabIndex = 2
        '
        'XtraConpanyInfo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(541, 334)
        Me.Controls.Add(Me.MemoEditAddress)
        Me.Controls.Add(Me.LabelControl11)
        Me.Controls.Add(Me.LabelControl10)
        Me.Controls.Add(Me.LabelControl9)
        Me.Controls.Add(Me.LabelControl8)
        Me.Controls.Add(Me.LabelControl7)
        Me.Controls.Add(Me.SimpleButtonVerify)
        Me.Controls.Add(Me.SimpleButtonSave)
        Me.Controls.Add(Me.TextEditLicense)
        Me.Controls.Add(Me.LabelControl6)
        Me.Controls.Add(Me.TextEditUserKey)
        Me.Controls.Add(Me.LabelControl5)
        Me.Controls.Add(Me.TextEditEmail)
        Me.Controls.Add(Me.LabelControl4)
        Me.Controls.Add(Me.TextEditContact)
        Me.Controls.Add(Me.LabelControl3)
        Me.Controls.Add(Me.LabelControl2)
        Me.Controls.Add(Me.TextEditCompanyName)
        Me.Controls.Add(Me.LabelControl1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.LookAndFeel.SkinName = "iMaginary"
        Me.LookAndFeel.UseDefaultLookAndFeel = False
        Me.Name = "XtraConpanyInfo"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Integrated Attendance Software"
        CType(Me.TextEditCompanyName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditContact.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditEmail.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditUserKey.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditLicense.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MemoEditAddress.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditCompanyName As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditContact As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditEmail As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditUserKey As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditLicense As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SimpleButtonSave As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButtonVerify As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents MemoEditAddress As DevExpress.XtraEditors.MemoEdit
End Class
