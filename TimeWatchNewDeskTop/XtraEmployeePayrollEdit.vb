﻿Imports System.Resources
Imports System.Globalization
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports System.ComponentModel
Imports DevExpress.XtraGrid.Columns
Imports System.Data.SqlClient
Imports System.Text
Imports System.Data.OleDb
Public Class S
    Dim ulf As UserLookAndFeel
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    Dim EpId As String
    Dim DateEditEffFrmI As DateTime
    Public Sub New()
        InitializeComponent()
        If Common.servername = "Access" Then
            'Me.TblEmployee1TableAdapter1.Fill(Me.SSSDBDataSet.TblEmployee1)
            Me.TblBank1TableAdapter1.Fill(Me.SSSDBDataSet.tblBank1)
            GridLookUpEditBank.Properties.DataSource = SSSDBDataSet.tblBank1
            Me.PaY_FORMULA1TableAdapter1.Fill(Me.SSSDBDataSet.PAY_FORMULA1)
            GridLookUpEditFormula.Properties.DataSource = SSSDBDataSet.PAY_FORMULA1
        Else
            TblEmployeeTableAdapter.Connection.ConnectionString = Common.ConnectionString
            'Me.TblEmployeeTableAdapter.Fill(Me.SSSDBDataSet.TblEmployee)

            TblBankTableAdapter.Connection.ConnectionString = Common.ConnectionString
            Me.TblBankTableAdapter.Fill(Me.SSSDBDataSet.tblBank)
            GridLookUpEditBank.Properties.DataSource = SSSDBDataSet.tblBank

            PAY_FORMULATableAdapter.Connection.ConnectionString = Common.ConnectionString
            Me.PAY_FORMULATableAdapter.Fill(Me.SSSDBDataSet.PAY_FORMULA)
            GridLookUpEditFormula.Properties.DataSource = SSSDBDataSet.PAY_FORMULA
        End If
        Common.SetGridFont(GridView1, New Font("Tahoma", 10))
    End Sub
    Private Sub XtraEmployeePayrollEdit_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        EpId = XtraEmployeePayroll.EmpId
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True

        If Common.servername = "Access" Then
            'Me.TblEmployee1TableAdapter1.Fill(Me.SSSDBDataSet.TblEmployee1)
            LookUpEdit1.Properties.DataSource = Common.EmpNonAdmin ' SSSDBDataSet.TblEmployee1
            Me.TblBank1TableAdapter1.Fill(Me.SSSDBDataSet.tblBank1)
            GridLookUpEditBank.Properties.DataSource = SSSDBDataSet.tblBank1

            Me.PaY_FORMULA1TableAdapter1.Fill(Me.SSSDBDataSet.PAY_FORMULA1)
            GridLookUpEditFormula.Properties.DataSource = SSSDBDataSet.PAY_FORMULA1

            'Me.PaY_FORMULA1TableAdapter1.Fill(Me.SSSDBDataSet.PAY_FORMULA1)
            GridLookUpEditFormulaE1.Properties.DataSource = SSSDBDataSet.PAY_FORMULA1
            GridLookUpEditFormulaE2.Properties.DataSource = SSSDBDataSet.PAY_FORMULA1
            GridLookUpEditFormulaE3.Properties.DataSource = SSSDBDataSet.PAY_FORMULA1
            GridLookUpEditFormulaE4.Properties.DataSource = SSSDBDataSet.PAY_FORMULA1
            GridLookUpEditFormulaE5.Properties.DataSource = SSSDBDataSet.PAY_FORMULA1
            GridLookUpEditFormulaE6.Properties.DataSource = SSSDBDataSet.PAY_FORMULA1
            GridLookUpEditFormulaE7.Properties.DataSource = SSSDBDataSet.PAY_FORMULA1
            GridLookUpEditFormulaE8.Properties.DataSource = SSSDBDataSet.PAY_FORMULA1
            GridLookUpEditFormulaE9.Properties.DataSource = SSSDBDataSet.PAY_FORMULA1
            GridLookUpEditFormulaE10.Properties.DataSource = SSSDBDataSet.PAY_FORMULA1

            GridLookUpEditFormulaD1.Properties.DataSource = SSSDBDataSet.PAY_FORMULA1
            GridLookUpEditFormulaD2.Properties.DataSource = SSSDBDataSet.PAY_FORMULA1
            GridLookUpEditFormulaD3.Properties.DataSource = SSSDBDataSet.PAY_FORMULA1
            GridLookUpEditFormulaD4.Properties.DataSource = SSSDBDataSet.PAY_FORMULA1
            GridLookUpEditFormulaD5.Properties.DataSource = SSSDBDataSet.PAY_FORMULA1
            GridLookUpEditFormulaD6.Properties.DataSource = SSSDBDataSet.PAY_FORMULA1
            GridLookUpEditFormulaD7.Properties.DataSource = SSSDBDataSet.PAY_FORMULA1
            GridLookUpEditFormulaD8.Properties.DataSource = SSSDBDataSet.PAY_FORMULA1
            GridLookUpEditFormulaD9.Properties.DataSource = SSSDBDataSet.PAY_FORMULA1
            GridLookUpEditFormulaD10.Properties.DataSource = SSSDBDataSet.PAY_FORMULA1
        Else
            'Me.TblEmployeeTableAdapter.Fill(Me.SSSDBDataSet.TblEmployee)
            LookUpEdit1.Properties.DataSource = Common.EmpNonAdmin 'SSSDBDataSet.TblEmployee

            Me.TblBankTableAdapter.Fill(Me.SSSDBDataSet.tblBank)
            GridLookUpEditBank.Properties.DataSource = SSSDBDataSet.tblBank

            Me.PAY_FORMULATableAdapter.Fill(Me.SSSDBDataSet.PAY_FORMULA)
            GridLookUpEditFormula.Properties.DataSource = SSSDBDataSet.PAY_FORMULA

            GridLookUpEditFormulaE1.Properties.DataSource = SSSDBDataSet.PAY_FORMULA
            GridLookUpEditFormulaE2.Properties.DataSource = SSSDBDataSet.PAY_FORMULA
            GridLookUpEditFormulaE3.Properties.DataSource = SSSDBDataSet.PAY_FORMULA
            GridLookUpEditFormulaE4.Properties.DataSource = SSSDBDataSet.PAY_FORMULA
            GridLookUpEditFormulaE5.Properties.DataSource = SSSDBDataSet.PAY_FORMULA
            GridLookUpEditFormulaE6.Properties.DataSource = SSSDBDataSet.PAY_FORMULA
            GridLookUpEditFormulaE7.Properties.DataSource = SSSDBDataSet.PAY_FORMULA
            GridLookUpEditFormulaE8.Properties.DataSource = SSSDBDataSet.PAY_FORMULA
            GridLookUpEditFormulaE9.Properties.DataSource = SSSDBDataSet.PAY_FORMULA
            GridLookUpEditFormulaE10.Properties.DataSource = SSSDBDataSet.PAY_FORMULA

            GridLookUpEditFormulaD1.Properties.DataSource = SSSDBDataSet.PAY_FORMULA
            GridLookUpEditFormulaD2.Properties.DataSource = SSSDBDataSet.PAY_FORMULA
            GridLookUpEditFormulaD3.Properties.DataSource = SSSDBDataSet.PAY_FORMULA
            GridLookUpEditFormulaD4.Properties.DataSource = SSSDBDataSet.PAY_FORMULA
            GridLookUpEditFormulaD5.Properties.DataSource = SSSDBDataSet.PAY_FORMULA
            GridLookUpEditFormulaD6.Properties.DataSource = SSSDBDataSet.PAY_FORMULA
            GridLookUpEditFormulaD7.Properties.DataSource = SSSDBDataSet.PAY_FORMULA
            GridLookUpEditFormulaD8.Properties.DataSource = SSSDBDataSet.PAY_FORMULA
            GridLookUpEditFormulaD9.Properties.DataSource = SSSDBDataSet.PAY_FORMULA
            GridLookUpEditFormulaD10.Properties.DataSource = SSSDBDataSet.PAY_FORMULA
        End If

        If Common.IsNepali = "Y" Then
            DateEditEffFrm.Visible = False
            ComboNEpaliMonth.Visible = True
            ComboNepaliYear.Visible = True
        Else
            DateEditEffFrm.Visible = True
            ComboNEpaliMonth.Visible = False
            ComboNepaliYear.Visible = False
        End If
        If EpId.Length = 0 Then
            SetDefaultValue()
        Else
            SetFormValue()
        End If
        SidePanel2.Width = Me.Width / 2
        SidePanel3.Width = Me.Width / 2
    End Sub
    Private Sub SetDefaultValue()
        'DateEditEffFrm.Enabled = True
    End Sub
    Private Sub SetFormValue()

        If Common.IsFullPayroll Then
            GroupControlAllow.Visible = True
        Else
            GroupControlAllow.Visible = False
        End If
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim ds As DataSet = New DataSet
        Dim sSql As String

        sSql = "select * from Pay_Master where paycode='" & EpId & "'"
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(ds)
        End If
        If ds.Tables(0).Rows.Count = 0 Then
            DateEditEffFrm.Enabled = True
            ComboNEpaliMonth.Enabled = True
            ComboNepaliYear.Enabled = True
        Else
            DateEditEffFrm.Enabled = False
            ComboNEpaliMonth.Enabled = False
            ComboNepaliYear.Enabled = False
        End If
        If XtraFullPayrollMenuMaster.PayEmpMster = "I" Then
            DateEditEffFrm.Enabled = True
        End If
        sSql = "select * from tblemployee where paycode='" & EpId & "'"
        ds = New DataSet
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(ds)
        End If



        TextEditPaycode.Text = EpId
        TextEditCardNo.Text = ds.Tables(0).Rows(0).Item("PRESENTCARDNO").ToString
        LabelControlName.Text = ds.Tables(0).Rows(0).Item("EMPNAME").ToString
        TextEditPf.Text = ds.Tables(0).Rows(0).Item("PFNO").ToString
        TextEditPf1.Text = ds.Tables(0).Rows(0).Item("PF_NO").ToString
        TextEditESI.Text = ds.Tables(0).Rows(0).Item("ESINO").ToString
        TextEditPAN.Text = ds.Tables(0).Rows(0).Item("PINCODE2").ToString
        GridLookUpEditBank.EditValue = ds.Tables(0).Rows(0).Item("bankCODE").ToString
        TextEditAccNo.Text = ds.Tables(0).Rows(0).Item("BankAcc").ToString

        sSql = "select * from tblDepartment where DEPARTMENTCODE='" & ds.Tables(0).Rows(0).Item("DEPARTMENTCODE").ToString.Trim & "'"
        ds = New DataSet
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(ds)
        End If
        LabelControlDept.Text = ds.Tables(0).Rows(0).Item("DEPARTMENTNAME").ToString

        sSql = "Select * from Pay_setup"
        Dim rsPaysetup As DataSet = New DataSet
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(rsPaysetup)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(rsPaysetup)
        End If

        TextEditDescp1.Text = rsPaysetup.Tables(0).Rows(0).Item("VDES_1").ToString.Trim
        TextEditDescp2.Text = rsPaysetup.Tables(0).Rows(0).Item("VDES_2").ToString.Trim
        TextEditDescp3.Text = rsPaysetup.Tables(0).Rows(0).Item("VDES_3").ToString.Trim
        TextEditDescp4.Text = rsPaysetup.Tables(0).Rows(0).Item("VDES_4").ToString.Trim
        TextEditDescp5.Text = rsPaysetup.Tables(0).Rows(0).Item("VDES_5").ToString.Trim
        TextEditDescp6.Text = rsPaysetup.Tables(0).Rows(0).Item("VDES_6").ToString.Trim
        TextEditDescp7.Text = rsPaysetup.Tables(0).Rows(0).Item("VDES_7").ToString.Trim
        TextEditDescp8.Text = rsPaysetup.Tables(0).Rows(0).Item("VDES_8").ToString.Trim
        TextEditDescp9.Text = rsPaysetup.Tables(0).Rows(0).Item("VDES_9").ToString.Trim
        TextEditDescp10.Text = rsPaysetup.Tables(0).Rows(0).Item("VDES_10").ToString.Trim


        TextEditDescpE1.Text = rsPaysetup.Tables(0).Rows(0).Item("VIES_1").ToString.Trim
        TextEditDescpE2.Text = rsPaysetup.Tables(0).Rows(0).Item("VIES_2").ToString.Trim
        TextEditDescpE3.Text = rsPaysetup.Tables(0).Rows(0).Item("VIES_3").ToString.Trim
        TextEditDescpE4.Text = rsPaysetup.Tables(0).Rows(0).Item("VIES_4").ToString.Trim
        TextEditDescpE5.Text = rsPaysetup.Tables(0).Rows(0).Item("VIES_5").ToString.Trim
        TextEditDescpE6.Text = rsPaysetup.Tables(0).Rows(0).Item("VIES_6").ToString.Trim
        TextEditDescpE7.Text = rsPaysetup.Tables(0).Rows(0).Item("VIES_7").ToString.Trim
        TextEditDescpE8.Text = rsPaysetup.Tables(0).Rows(0).Item("VIES_8").ToString.Trim
        TextEditDescpE9.Text = rsPaysetup.Tables(0).Rows(0).Item("VIES_9").ToString.Trim
        TextEditDescpE10.Text = rsPaysetup.Tables(0).Rows(0).Item("VIES_10").ToString.Trim

        GridLookUpEditFormulaE1.EditValue = rsPaysetup.Tables(0).Rows(0).Item("VIES_1").ToString.Trim
        GridLookUpEditFormulaE2.EditValue = rsPaysetup.Tables(0).Rows(0).Item("VIES_2").ToString.Trim
        GridLookUpEditFormulaE3.EditValue = rsPaysetup.Tables(0).Rows(0).Item("VIES_3").ToString.Trim
        GridLookUpEditFormulaE4.EditValue = rsPaysetup.Tables(0).Rows(0).Item("VIES_4").ToString.Trim
        GridLookUpEditFormulaE5.EditValue = rsPaysetup.Tables(0).Rows(0).Item("VIES_5").ToString.Trim
        GridLookUpEditFormulaE6.EditValue = rsPaysetup.Tables(0).Rows(0).Item("VIES_6").ToString.Trim
        GridLookUpEditFormulaE7.EditValue = rsPaysetup.Tables(0).Rows(0).Item("VIES_7").ToString.Trim
        GridLookUpEditFormulaE8.EditValue = rsPaysetup.Tables(0).Rows(0).Item("VIES_8").ToString.Trim
        GridLookUpEditFormulaE9.EditValue = rsPaysetup.Tables(0).Rows(0).Item("VIES_9").ToString.Trim
        GridLookUpEditFormulaE10.EditValue = rsPaysetup.Tables(0).Rows(0).Item("VIES_10").ToString.Trim


        GridLookUpEditFormulaD1.EditValue = rsPaysetup.Tables(0).Rows(0).Item("VDES_1").ToString.Trim
        GridLookUpEditFormulaD2.EditValue = rsPaysetup.Tables(0).Rows(0).Item("VDES_2").ToString.Trim
        GridLookUpEditFormulaD3.EditValue = rsPaysetup.Tables(0).Rows(0).Item("VDES_3").ToString.Trim
        GridLookUpEditFormulaD4.EditValue = rsPaysetup.Tables(0).Rows(0).Item("VDES_4").ToString.Trim
        GridLookUpEditFormulaD5.EditValue = rsPaysetup.Tables(0).Rows(0).Item("VDES_5").ToString.Trim
        GridLookUpEditFormulaD6.EditValue = rsPaysetup.Tables(0).Rows(0).Item("VDES_6").ToString.Trim
        GridLookUpEditFormulaD7.EditValue = rsPaysetup.Tables(0).Rows(0).Item("VDES_7").ToString.Trim
        GridLookUpEditFormulaD8.EditValue = rsPaysetup.Tables(0).Rows(0).Item("VDES_8").ToString.Trim
        GridLookUpEditFormulaD9.EditValue = rsPaysetup.Tables(0).Rows(0).Item("VDES_9").ToString.Trim
        GridLookUpEditFormulaD10.EditValue = rsPaysetup.Tables(0).Rows(0).Item("VDES_10").ToString.Trim

        sSql = "select * from Pay_Master where PAYCODE='" & EpId & "'"
        ds = New DataSet
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(ds)
        End If
        If ds.Tables(0).Rows.Count > 0 Then
            If XtraFullPayrollMenuMaster.PayEmpMster = "I" Then
                DateEditEffFrm.DateTime = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("EFFECTFROM").ToString).AddMonths(1)
                DateEditEffFrmI = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("EFFECTFROM").ToString)
            Else
                DateEditEffFrm.DateTime = Convert.ToDateTime(ds.Tables(0).Rows(0).Item("EFFECTFROM").ToString)

            End If
            TextEditGross.Text = ds.Tables(0).Rows(0).Item("VGross").ToString
            TextEditBasic.Text = ds.Tables(0).Rows(0).Item("VBASIC").ToString
            If ds.Tables(0).Rows(0).Item("VPaymentby").ToString = "C" Then
                ComboBoxEditPayBy.SelectedIndex = 0
            Else
                ComboBoxEditPayBy.SelectedIndex = 1
            End If

            If ds.Tables(0).Rows(0).Item("VEmployeeType").ToString = "O" Then
                ComboBoxEditEmpType.SelectedIndex = 0
            ElseIf ds.Tables(0).Rows(0).Item("VEmployeeType").ToString = "D" Then
                ComboBoxEditEmpType.SelectedIndex = 1
            ElseIf ds.Tables(0).Rows(0).Item("VEmployeeType").ToString = "P" Then
                ComboBoxEditEmpType.SelectedIndex = 2
            ElseIf ds.Tables(0).Rows(0).Item("VEmployeeType").ToString = "H" Then
                ComboBoxEditEmpType.SelectedIndex = 3
            End If
            TextEditDA.Text = ds.Tables(0).Rows(0).Item("VDA_RATE").ToString.Trim
            If ds.Tables(0).Rows(0).Item("VDA_F").ToString.Trim = "F" Then
                ComboBoxEditDA.SelectedIndex = 0
            Else
                ComboBoxEditDA.SelectedIndex = 1
            End If
            If ds.Tables(0).Rows(0).Item("VCONV_F").ToString.Trim = "F" Then
                ComboBoxEditConvence.SelectedIndex = 0
            Else
                ComboBoxEditConvence.SelectedIndex = 1
            End If
            If ds.Tables(0).Rows(0).Item("VMED_F").ToString.Trim = "F" Then
                ComboBoxEditMedical.SelectedIndex = 0
            Else
                ComboBoxEditMedical.SelectedIndex = 1
            End If
            If ds.Tables(0).Rows(0).Item("VHRA_F").ToString.Trim = "F" Then
                ComboBoxEditHRA.SelectedIndex = 0
            Else
                ComboBoxEditHRA.SelectedIndex = 1
            End If
            GridLookUpEditFormula.EditValue = ds.Tables(0).Rows(0).Item("VOT_FLAG").ToString.Trim

            TextEditConvence.Text = ds.Tables(0).Rows(0).Item("VCONV_RATE").ToString.Trim
            TextEditMedical.Text = ds.Tables(0).Rows(0).Item("VMED_RATE").ToString.Trim
            TextEditHRA.Text = ds.Tables(0).Rows(0).Item("VHRA_RATE").ToString.Trim
            TextEditOT.Text = ds.Tables(0).Rows(0).Item("VOT_RATE").ToString.Trim
            TextEditTDS.Text = ds.Tables(0).Rows(0).Item("VTDS_RATE").ToString.Trim

            CheckEditProfTax.EditValue = ds.Tables(0).Rows(0).Item("PROF_TAX_ALLOWED").ToString.Trim
            CheckEditPF.EditValue = ds.Tables(0).Rows(0).Item("PF_ALLOWED").ToString.Trim
            CheckEditUnderLim.EditValue = ds.Tables(0).Rows(0).Item("PFulimit").ToString.Trim
            CheckEditESI.EditValue = ds.Tables(0).Rows(0).Item("ESI_ALLOWED").ToString.Trim
            CheckEditVPF.EditValue = ds.Tables(0).Rows(0).Item("VPF_ALLOWED").ToString.Trim
            CheckEditBonus.EditValue = ds.Tables(0).Rows(0).Item("BONUS_ALLOWED").ToString.Trim
            CheckEditGraduty.EditValue = ds.Tables(0).Rows(0).Item("GRADUTY_ALLOWED").ToString.Trim

            'deduction
            TextEditRate1.Text = ds.Tables(0).Rows(0).Item("VD_1").ToString.Trim
            TextEditRate2.Text = ds.Tables(0).Rows(0).Item("VD_2").ToString.Trim
            TextEditRate3.Text = ds.Tables(0).Rows(0).Item("VD_3").ToString.Trim
            TextEditRate4.Text = ds.Tables(0).Rows(0).Item("VD_4").ToString.Trim
            TextEditRate5.Text = ds.Tables(0).Rows(0).Item("VD_5").ToString.Trim
            TextEditRate6.Text = ds.Tables(0).Rows(0).Item("VD_6").ToString.Trim
            TextEditRate7.Text = ds.Tables(0).Rows(0).Item("VD_7").ToString.Trim
            TextEditRate8.Text = ds.Tables(0).Rows(0).Item("VD_8").ToString.Trim
            TextEditRate9.Text = ds.Tables(0).Rows(0).Item("VD_9").ToString.Trim
            TextEditRate10.Text = ds.Tables(0).Rows(0).Item("VD_10").ToString.Trim
            'end deduction

            'earning
            TextEditRateE1.Text = ds.Tables(0).Rows(0).Item("VI_1").ToString.Trim
            TextEditRateE2.Text = ds.Tables(0).Rows(0).Item("VI_2").ToString.Trim
            TextEditRateE3.Text = ds.Tables(0).Rows(0).Item("VI_3").ToString.Trim
            TextEditRateE4.Text = ds.Tables(0).Rows(0).Item("VI_4").ToString.Trim
            TextEditRateE5.Text = ds.Tables(0).Rows(0).Item("VI_5").ToString.Trim
            TextEditRateE6.Text = ds.Tables(0).Rows(0).Item("VI_6").ToString.Trim
            TextEditRateE7.Text = ds.Tables(0).Rows(0).Item("VI_7").ToString.Trim
            TextEditRateE8.Text = ds.Tables(0).Rows(0).Item("VI_8").ToString.Trim
            TextEditRateE9.Text = ds.Tables(0).Rows(0).Item("VI_9").ToString.Trim
            TextEditRateE10.Text = ds.Tables(0).Rows(0).Item("VI_10").ToString.Trim
            'end earning 



            'deduction


            GridLookUpEditFormulaD1.EditValue = ds.Tables(0).Rows(0).Item("VDT_1").ToString.Trim
            GridLookUpEditFormulaD2.EditValue = ds.Tables(0).Rows(0).Item("VDT_2").ToString.Trim
            GridLookUpEditFormulaD3.EditValue = ds.Tables(0).Rows(0).Item("VDT_3").ToString.Trim
            GridLookUpEditFormulaD4.EditValue = ds.Tables(0).Rows(0).Item("VDT_4").ToString.Trim
            GridLookUpEditFormulaD5.EditValue = ds.Tables(0).Rows(0).Item("VDT_5").ToString.Trim
            GridLookUpEditFormulaD6.EditValue = ds.Tables(0).Rows(0).Item("VDT_6").ToString.Trim
            GridLookUpEditFormulaD7.EditValue = ds.Tables(0).Rows(0).Item("VDT_7").ToString.Trim
            GridLookUpEditFormulaD8.EditValue = ds.Tables(0).Rows(0).Item("VDT_8").ToString.Trim
            GridLookUpEditFormulaD9.EditValue = ds.Tables(0).Rows(0).Item("VDT_9").ToString.Trim
            GridLookUpEditFormulaD10.EditValue = ds.Tables(0).Rows(0).Item("VDT_10").ToString.Trim
            'end deduction

            'earning

            GridLookUpEditFormulaE1.EditValue = ds.Tables(0).Rows(0).Item("VIT_1").ToString.Trim
            GridLookUpEditFormulaE2.EditValue = ds.Tables(0).Rows(0).Item("VIT_2").ToString.Trim
            GridLookUpEditFormulaE3.EditValue = ds.Tables(0).Rows(0).Item("VIT_3").ToString.Trim
            GridLookUpEditFormulaE4.EditValue = ds.Tables(0).Rows(0).Item("VIT_4").ToString.Trim
            GridLookUpEditFormulaE5.EditValue = ds.Tables(0).Rows(0).Item("VIT_5").ToString.Trim
            GridLookUpEditFormulaE6.EditValue = ds.Tables(0).Rows(0).Item("VIT_6").ToString.Trim
            GridLookUpEditFormulaE7.EditValue = ds.Tables(0).Rows(0).Item("VIT_7").ToString.Trim
            GridLookUpEditFormulaE8.EditValue = ds.Tables(0).Rows(0).Item("VIT_8").ToString.Trim
            GridLookUpEditFormulaE9.EditValue = ds.Tables(0).Rows(0).Item("VIT_9").ToString.Trim
            GridLookUpEditFormulaE10.EditValue = ds.Tables(0).Rows(0).Item("VIT_10").ToString.Trim
            'end earning          

        Else
            'deduction
            DateEditEffFrm.DateTime = Now
            TextEditGross.Text = ""
            TextEditBasic.Text = ""
            ComboBoxEditPayBy.SelectedIndex = 0

            ComboBoxEditEmpType.SelectedIndex = 0
            TextEditDA.Text = ""
            ComboBoxEditDA.SelectedIndex = 0
            ComboBoxEditConvence.SelectedIndex = 0
            ComboBoxEditMedical.SelectedIndex = 0
            ComboBoxEditHRA.SelectedIndex = 0
            GridLookUpEditFormula.EditValue = ""
            TextEditConvence.Text = ""
            TextEditMedical.Text = ""
            TextEditHRA.Text = ""
            TextEditOT.Text = ""
            'TextEditTDS.Text = ""
            CheckEditProfTax.EditValue = "N"
            CheckEditPF.EditValue = "N"
            CheckEditUnderLim.EditValue = "N"
            CheckEditESI.EditValue = "N"
            CheckEditVPF.EditValue = "N"
            CheckEditBonus.EditValue = "N"
            CheckEditGraduty.EditValue = "N"
            TextEditRate1.Text = rsPaysetup.Tables(0).Rows(0).Item("VD_1").ToString.Trim
            TextEditRate2.Text = rsPaysetup.Tables(0).Rows(0).Item("VD_2").ToString.Trim
            TextEditRate3.Text = rsPaysetup.Tables(0).Rows(0).Item("VD_3").ToString.Trim
            TextEditRate4.Text = rsPaysetup.Tables(0).Rows(0).Item("VD_4").ToString.Trim
            TextEditRate5.Text = rsPaysetup.Tables(0).Rows(0).Item("VD_5").ToString.Trim
            TextEditRate6.Text = rsPaysetup.Tables(0).Rows(0).Item("VD_6").ToString.Trim
            TextEditRate7.Text = rsPaysetup.Tables(0).Rows(0).Item("VD_7").ToString.Trim
            TextEditRate8.Text = rsPaysetup.Tables(0).Rows(0).Item("VD_8").ToString.Trim
            TextEditRate9.Text = rsPaysetup.Tables(0).Rows(0).Item("VD_9").ToString.Trim
            TextEditRate10.Text = rsPaysetup.Tables(0).Rows(0).Item("VD_10").ToString.Trim
            'end deduction

            'earning
            TextEditRateE1.Text = rsPaysetup.Tables(0).Rows(0).Item("VI_1").ToString.Trim
            TextEditRateE2.Text = rsPaysetup.Tables(0).Rows(0).Item("VI_2").ToString.Trim
            TextEditRateE3.Text = rsPaysetup.Tables(0).Rows(0).Item("VI_3").ToString.Trim
            TextEditRateE4.Text = rsPaysetup.Tables(0).Rows(0).Item("VI_4").ToString.Trim
            TextEditRateE5.Text = rsPaysetup.Tables(0).Rows(0).Item("VI_5").ToString.Trim
            TextEditRateE6.Text = rsPaysetup.Tables(0).Rows(0).Item("VI_6").ToString.Trim
            TextEditRateE7.Text = rsPaysetup.Tables(0).Rows(0).Item("VI_7").ToString.Trim
            TextEditRateE8.Text = rsPaysetup.Tables(0).Rows(0).Item("VI_8").ToString.Trim
            TextEditRateE9.Text = rsPaysetup.Tables(0).Rows(0).Item("VI_9").ToString.Trim
            TextEditRateE10.Text = rsPaysetup.Tables(0).Rows(0).Item("VI_10").ToString.Trim
            'end earning 


            'deduction


            GridLookUpEditFormulaD1.EditValue = rsPaysetup.Tables(0).Rows(0).Item("VDT_1").ToString.Trim
            GridLookUpEditFormulaD2.EditValue = rsPaysetup.Tables(0).Rows(0).Item("VDT_2").ToString.Trim
            GridLookUpEditFormulaD3.EditValue = rsPaysetup.Tables(0).Rows(0).Item("VDT_3").ToString.Trim
            GridLookUpEditFormulaD4.EditValue = rsPaysetup.Tables(0).Rows(0).Item("VDT_4").ToString.Trim
            GridLookUpEditFormulaD5.EditValue = rsPaysetup.Tables(0).Rows(0).Item("VDT_5").ToString.Trim
            GridLookUpEditFormulaD6.EditValue = rsPaysetup.Tables(0).Rows(0).Item("VDT_6").ToString.Trim
            GridLookUpEditFormulaD7.EditValue = rsPaysetup.Tables(0).Rows(0).Item("VDT_7").ToString.Trim
            GridLookUpEditFormulaD8.EditValue = rsPaysetup.Tables(0).Rows(0).Item("VDT_8").ToString.Trim
            GridLookUpEditFormulaD9.EditValue = rsPaysetup.Tables(0).Rows(0).Item("VDT_9").ToString.Trim
            GridLookUpEditFormulaD10.EditValue = rsPaysetup.Tables(0).Rows(0).Item("VDT_10").ToString.Trim
            'end deduction

            'earning


            GridLookUpEditFormulaE1.EditValue = rsPaysetup.Tables(0).Rows(0).Item("VIT_1").ToString.Trim
            GridLookUpEditFormulaE2.EditValue = rsPaysetup.Tables(0).Rows(0).Item("VIT_2").ToString.Trim
            GridLookUpEditFormulaE3.EditValue = rsPaysetup.Tables(0).Rows(0).Item("VIT_3").ToString.Trim
            GridLookUpEditFormulaE4.EditValue = rsPaysetup.Tables(0).Rows(0).Item("VIT_4").ToString.Trim
            GridLookUpEditFormulaE5.EditValue = rsPaysetup.Tables(0).Rows(0).Item("VIT_5").ToString.Trim
            GridLookUpEditFormulaE6.EditValue = rsPaysetup.Tables(0).Rows(0).Item("VIT_6").ToString.Trim
            GridLookUpEditFormulaE7.EditValue = rsPaysetup.Tables(0).Rows(0).Item("VIT_7").ToString.Trim
            GridLookUpEditFormulaE8.EditValue = rsPaysetup.Tables(0).Rows(0).Item("VIT_8").ToString.Trim
            GridLookUpEditFormulaE9.EditValue = rsPaysetup.Tables(0).Rows(0).Item("VIT_9").ToString.Trim
            GridLookUpEditFormulaE10.EditValue = rsPaysetup.Tables(0).Rows(0).Item("VIT_10").ToString.Trim
            'end earning
            TextEditTDS.Text = rsPaysetup.Tables(0).Rows(0).Item("TDS_Rate").ToString.Trim


        End If
        If Common.IsNepali = "Y" Then
            Dim DC As New DateConverter()
            Dim doj As String = DC.ToBS(New Date(DateEditEffFrm.DateTime.Year, DateEditEffFrm.DateTime.Month, 1))
            Dim dojTmp() As String = doj.Split("-")
            ComboNepaliYear.EditValue = dojTmp(0)
            ComboNEpaliMonth.SelectedIndex = dojTmp(1) - 1
        End If

        ComboBoxEditTDS.EditValue = rsPaysetup.Tables(0).Rows(0).Item("TDS_F").ToString.Trim

        If TextEditDescp1.Text.Trim <> "" Then
            TextEditRate1.ReadOnly = False
        End If

        If TextEditDescp2.Text.Trim <> "" Then
            TextEditRate2.ReadOnly = False
        End If
        If TextEditDescp3.Text.Trim <> "" Then
            TextEditRate3.ReadOnly = False
        End If
        If TextEditDescp4.Text.Trim <> "" Then
            TextEditRate4.ReadOnly = False
        End If
        If TextEditDescp5.Text.Trim <> "" Then
            TextEditRate5.ReadOnly = False
        End If
        If TextEditDescp6.Text.Trim <> "" Then
            TextEditRate6.ReadOnly = False
        End If
        If TextEditDescp7.Text.Trim <> "" Then
            TextEditRate7.ReadOnly = False
        End If
        If TextEditDescp8.Text.Trim <> "" Then
            TextEditRate8.ReadOnly = False
        End If
        If TextEditDescp9.Text.Trim <> "" Then
            TextEditRate9.ReadOnly = False
        End If
        If TextEditDescp10.Text.Trim <> "" Then
            TextEditRate10.ReadOnly = False
        End If

        If TextEditDescpE1.Text.Trim <> "" Then
            TextEditRateE1.ReadOnly = False
        End If
        If TextEditDescpE2.Text.Trim <> "" Then
            TextEditRateE2.ReadOnly = False
        End If
        If TextEditDescpE3.Text.Trim <> "" Then
            TextEditRateE3.ReadOnly = False
        End If
        If TextEditDescpE4.Text.Trim <> "" Then
            TextEditRateE4.ReadOnly = False
        End If
        If TextEditDescpE5.Text.Trim <> "" Then
            TextEditRateE5.ReadOnly = False
        End If
        If TextEditDescpE6.Text.Trim <> "" Then
            TextEditRateE6.ReadOnly = False
        End If
        If TextEditDescpE7.Text.Trim <> "" Then
            TextEditRateE7.ReadOnly = False
        End If
        If TextEditDescpE8.Text.Trim <> "" Then
            TextEditRateE8.ReadOnly = False
        End If
        If TextEditDescpE9.Text.Trim <> "" Then
            TextEditRateE9.ReadOnly = False
        End If
        If TextEditDescpE10.Text.Trim <> "" Then
            TextEditRateE10.ReadOnly = False
        End If

        If XtraFullPayrollMenuMaster.PayEmpMster = "I" Then
            TextEditGross.Text = ""
        End If
    End Sub
    Private Sub GridLookUpEditBank_CustomDisplayText(sender As System.Object, e As DevExpress.XtraEditors.Controls.CustomDisplayTextEventArgs) Handles GridLookUpEditBank.CustomDisplayText
        Try
            Dim gridLookUpEdit = CType(sender, GridLookUpEdit)
            Dim dataRowView = CType(gridLookUpEdit.Properties.GetRowByKeyValue(e.Value), DataRowView)
            Dim row = CType(dataRowView.Row, DataRow)
            e.DisplayText = (row("BANKCODE").ToString + ("  " + row("BANKNAME").ToString))
        Catch
            'MsgBox("Catch")
        End Try
    End Sub
    Private Sub TextEditBasic_Leave(sender As System.Object, e As System.EventArgs) Handles TextEditBasic.Leave
        'TextEditBasic.Text = Math.Round(Convert.ToDouble(TextEditBasic.Text))
    End Sub
    Private Sub SimpleButton2_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton2.Click
        Me.Close()
    End Sub
    Private Sub SimpleButtonSave_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonSave.Click
        If Common.IsNepali = "Y" Then
            Dim DC As New DateConverter()
            Try
                'DateEditEffFrm.DateTime = DC.ToAD(New Date(ComboNepaliYear.EditValue, ComboNEpaliMonth.SelectedIndex + 1, 1))
                DateEditEffFrm.DateTime = DC.ToAD(ComboNepaliYear.EditValue & "-" & ComboNEpaliMonth.SelectedIndex + 1 & "-" & 1)
            Catch ex As Exception
                XtraMessageBox.Show(ulf, "<size=10>Invalid Effected From Date</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                ComboNEpaliMonth.Select()
                Exit Sub
            End Try
        End If

        Dim adap1 As SqlDataAdapter
        Dim adapA1 As OleDbDataAdapter
        Dim sSql1 As String
        Dim ds1 As DataSet = New DataSet
        sSql1 = "select * from tblemployee where paycode='" & EpId & "'"
        ds1 = New DataSet
        If Common.servername = "Access" Then
            adapA1 = New OleDbDataAdapter(sSql1, Common.con1)
            adapA1.Fill(ds1)
        Else
            adap1 = New SqlDataAdapter(sSql1, Common.con)
            adap1.Fill(ds1)
        End If
        If Convert.ToDateTime(ds1.Tables(0).Rows(0).Item("DateOFJOIN").ToString.Trim).Date > DateEditEffFrm.DateTime.Date Then
            XtraMessageBox.Show(ulf, "<size=10>Effected From cannot be less that Date of Joining of Employee</size>", "<size=9>Error</size>")
            Exit Sub
        End If

        Dim PAYCODE As String = TextEditPaycode.Text.Trim 'LookUpEdit1.EditValue.ToString.Trim
        Dim VBASIC As String '= TextEditBasic.Text.Trim
        If TextEditBasic.Text.Trim = "" Then
            VBASIC = "0"
        Else
            VBASIC = TextEditBasic.Text.Trim
        End If

        Dim VPaymentby As String
        If ComboBoxEditPayBy.SelectedIndex = 0 Then
            VPaymentby = "C"
        Else
            VPaymentby = "R"
        End If
        Dim VBankAcc As String = TextEditAccNo.Text.Trim
        Dim VEmployeeType As String
        If ComboBoxEditEmpType.SelectedIndex = 0 Then
            VEmployeeType = "O"
        ElseIf ComboBoxEditEmpType.SelectedIndex = 1 Then
            VEmployeeType = "D"
        ElseIf ComboBoxEditEmpType.SelectedIndex = 2 Then
            VEmployeeType = "P"
        ElseIf ComboBoxEditEmpType.SelectedIndex = 3 Then
            VEmployeeType = "H"
        End If

        Dim VCPF_NO As String = TextEditPf.Text.Trim
        Dim VPF_NO As String = TextEditPf1.Text.Trim
        If TextEditPf1.Text.Trim = "" Then
            VPF_NO = "NULL"
        End If
        Dim VESI_NO As String = TextEditESI.Text.Trim
        Dim VPAN_NO As String = TextEditPAN.Text.Trim
        Dim VDA_RATE As String = TextEditDA.Text.Trim
        If VDA_RATE = "" Then : VDA_RATE = "0" : End If
        Dim VDA_F As String = ComboBoxEditDA.EditValue.Substring(0, 1)
        Dim VHRA_RATE As String = TextEditHRA.Text.Trim
        If VHRA_RATE = "" Then : VHRA_RATE = "0" : End If
        Dim VHRA_F As String = ComboBoxEditHRA.EditValue.Substring(0, 1)
        Dim VCONV_RATE As String = TextEditConvence.Text.Trim
        If VCONV_RATE = "" Then : VCONV_RATE = "0" : End If
        Dim VCONV_F As String = ComboBoxEditConvence.EditValue.Substring(0, 1)
        Dim VMED_RATE As String = TextEditMedical.Text.Trim
        If VMED_RATE = "" Then : VMED_RATE = "0" : End If
        Dim VMED_F As String = ComboBoxEditMedical.EditValue.Substring(0, 1)
        Dim VOT_RATE As String = TextEditOT.Text.Trim
        If VOT_RATE = "" Then : VOT_RATE = "0" : End If
        Dim VOT_FLAG As String = GridLookUpEditFormula.EditValue
        Dim VI_1 As String = TextEditRateE1.Text.Trim
        If VI_1 = "" Then : VI_1 = "0" : End If
        Dim VIT_1 As String = GridLookUpEditFormulaE1.EditValue
        Dim VI_2 As String = TextEditRateE2.Text.Trim
        If VI_2 = "" Then : VI_2 = "0" : End If
        Dim VIT_2 As String = GridLookUpEditFormulaE2.EditValue
        Dim VI_3 As String = TextEditRateE3.Text.Trim
        If VI_3 = "" Then : VI_3 = "0" : End If
        Dim VIT_3 As String = GridLookUpEditFormulaE3.EditValue
        Dim VI_4 As String = TextEditRateE4.Text.Trim
        If VI_4 = "" Then : VI_4 = "0" : End If
        Dim VIT_4 As String = GridLookUpEditFormulaE4.EditValue
        Dim VI_5 As String = TextEditRateE5.Text.Trim
        If VI_5 = "" Then : VI_5 = "0" : End If
        Dim VIT_5 As String = GridLookUpEditFormulaE5.EditValue
        Dim VI_6 As String = TextEditRateE6.Text.Trim
        If VI_6 = "" Then : VI_6 = "0" : End If
        Dim VIT_6 As String = GridLookUpEditFormulaE6.EditValue
        Dim VI_7 As String = TextEditRateE7.Text.Trim
        If VI_7 = "" Then : VI_7 = "0" : End If
        Dim VIT_7 As String = GridLookUpEditFormulaE7.EditValue
        Dim VI_8 As String = TextEditRateE8.Text.Trim
        If VI_8 = "" Then : VI_8 = "0" : End If
        Dim VIT_8 As String = GridLookUpEditFormulaE8.EditValue
        Dim VI_9 As String = TextEditRateE9.Text.Trim
        If VI_9 = "" Then : VI_9 = "0" : End If
        Dim VIT_9 As String = GridLookUpEditFormulaE9.EditValue
        Dim VI_10 As String = TextEditRateE10.Text.Trim
        If VI_10 = "" Then : VI_10 = "0" : End If
        Dim VIT_10 As String = GridLookUpEditFormulaE10.EditValue
        Dim PF_ALLOWED As String = CheckEditPF.EditValue
        Dim ESI_ALLOWED As String = CheckEditESI.EditValue
        Dim VPF_ALLOWED As String = CheckEditVPF.EditValue
        Dim BONUS_ALLOWED As String = CheckEditBonus.EditValue
        Dim GRADUTY_ALLOWED As String = CheckEditGraduty.EditValue
        Dim PROF_TAX_ALLOWED As String = CheckEditProfTax.EditValue
        Dim VDT_1 As String = GridLookUpEditFormulaD1.EditValue
        Dim VD_1 As String = TextEditRate1.Text.Trim
        If VD_1 = "" Then : VD_1 = "0" : End If
        Dim VDT_2 As String = GridLookUpEditFormulaD2.EditValue
        Dim VD_2 As String = TextEditRate2.Text.Trim
        If VD_2 = "" Then : VD_2 = "0" : End If
        Dim VDT_3 As String = GridLookUpEditFormulaD3.EditValue
        Dim VD_3 As String = TextEditRate3.Text.Trim
        If VD_3 = "" Then : VD_3 = "0" : End If
        Dim VDT_4 As String = GridLookUpEditFormulaD4.EditValue
        Dim VD_4 As String = TextEditRate4.Text.Trim
        If VD_4 = "" Then : VD_4 = "0" : End If
        Dim VDT_5 As String = GridLookUpEditFormulaD5.EditValue
        Dim VD_5 As String = TextEditRate5.Text.Trim
        If VD_5 = "" Then : VD_5 = "0" : End If
        Dim VDT_6 As String = GridLookUpEditFormulaD6.EditValue
        Dim VD_6 As String = TextEditRate6.Text.Trim
        If VD_6 = "" Then : VD_6 = "0" : End If
        Dim VDT_7 As String = GridLookUpEditFormulaD7.EditValue
        Dim VD_7 As String = TextEditRate7.Text.Trim
        If VD_7 = "" Then : VD_7 = "0" : End If
        Dim VDT_8 As String = GridLookUpEditFormulaD8.EditValue
        Dim VD_8 As String = TextEditRate8.Text.Trim
        If VD_8 = "" Then : VD_8 = "0" : End If
        Dim VDT_9 As String = GridLookUpEditFormulaD9.EditValue
        Dim VD_9 As String = TextEditRate9.Text.Trim
        If VD_9 = "" Then : VD_9 = "0" : End If
        Dim VDT_10 As String = GridLookUpEditFormulaD10.EditValue
        Dim VD_10 As String = TextEditRate10.Text.Trim
        If VD_10 = "" Then : VD_10 = "0" : End If
        Dim VTDS_RATE As String = TextEditTDS.Text.Trim
        If VTDS_RATE = "" Then : VTDS_RATE = "0" : End If
        Dim VTDS_F As String
        If ComboBoxEditTDS.EditValue = "" Then
            VTDS_F = ""
        Else
            VTDS_F = ComboBoxEditTDS.EditValue.substring(0, 1)
        End If
        Dim bankCODE As String = GridLookUpEditBank.EditValue
        Dim EFFECTFROM As String = DateEditEffFrm.DateTime.ToString("yyyy-MM-01 00:00:00")

        If XtraFullPayrollMenuMaster.PayEmpMster = "I" Then
            If DateEditEffFrmI > DateEditEffFrm.DateTime.Date Then
                XtraMessageBox.Show(ulf, "<size=10>Effect from Month should not be less then Previous Effect Month</size>", "<size=9>Error</size>")
                Exit Sub
            End If
        End If

        Dim VGross As String = TextEditGross.Text.Trim
        If VGross = "" Then : VGross = "0" : End If
        'Dim VIncrement As String
        Dim pfUlimit As String = CheckEditUnderLim.EditValue

        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim sSql As String = "select * from Pay_Master where PAYCODE='" & PAYCODE & "'"
        Dim ds As DataSet = New DataSet
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(ds)
        End If
        If ds.Tables(0).Rows.Count > 0 Then
            'update
            sSql = " update Pay_Master set PAYCODE='" & PAYCODE & "' ,VBASIC=" & VBASIC & " ,VPaymentby='" & VPaymentby & _
                "' ,VBankAcc='" & VBankAcc & "' ,VEmployeeType='" & VEmployeeType & "' ,VCPF_NO='" & VCPF_NO & "' ,VPF_NO=" & VPF_NO & _
                " ,VESI_NO='" & VESI_NO & "' ,VPAN_NO='" & VPAN_NO & "' ,VDA_RATE=" & VDA_RATE & " ,VDA_F='" & VDA_F & _
                "' ,VHRA_RATE=" & VHRA_RATE & " ,VHRA_F='" & VHRA_F & "' ,VCONV_RATE=" & VCONV_RATE & " ,VCONV_F='" & VCONV_F & _
                "' ,VMED_RATE=" & VMED_RATE & " ,VMED_F='" & VMED_F & "' ,VOT_RATE=" & VOT_RATE & " ,VOT_FLAG='" & VOT_FLAG & _
                "' ,VI_1=" & VI_1 & " ,VIT_1='" & VIT_1 & "' ,VI_2=" & VI_2 & " ,VIT_2='" & VIT_2 & "' ,VI_3=" & VI_3 & _
                " ,VIT_3='" & VIT_3 & "' ,VI_4=" & VI_4 & " ,VIT_4='" & VIT_4 & "' ,VI_5=" & VI_5 & " ,VIT_5='" & VIT_5 & _
                "' ,VI_6=" & VI_6 & " ,VIT_6='" & VIT_6 & "' ,VI_7=" & VI_7 & " ,VIT_7='" & VIT_7 & "' ,VI_8=" & VI_8 & _
                " ,VIT_8='" & VIT_8 & "' ,VI_9=" & VI_9 & " ,VIT_9='" & VIT_9 & "' ,VI_10=" & VI_10 & " ,VIT_10='" & VIT_10 & _
                "' ,PF_ALLOWED='" & PF_ALLOWED & "' ,ESI_ALLOWED='" & ESI_ALLOWED & "' ,VPF_ALLOWED='" & VPF_ALLOWED & _
                "' ,BONUS_ALLOWED='" & BONUS_ALLOWED & "' ,GRADUTY_ALLOWED='" & GRADUTY_ALLOWED & "' ,PROF_TAX_ALLOWED='" & PROF_TAX_ALLOWED & _
                "' ,VDT_1='" & VDT_1 & "' ,VD_1=" & VD_1 & " ,VDT_2='" & VDT_2 & "' ,VD_2=" & VD_2 & " ,VDT_3='" & VDT_3 & _
                "' ,VD_3=" & VD_3 & " ,VDT_4='" & VDT_4 & "' ,VD_4=" & VD_4 & " ,VDT_5='" & VDT_5 & "' ,VD_5=" & VD_5 & _
                " ,VDT_6='" & VDT_6 & "' ,VD_6=" & VD_6 & " ,VDT_7='" & VDT_7 & "' ,VD_7=" & VD_7 & " ,VDT_8='" & VDT_8 & _
                "' ,VD_8=" & VD_8 & " ,VDT_9='" & VDT_9 & "' ,VD_9=" & VD_9 & " ,VDT_10='" & VDT_10 & "' ,VD_10=" & VD_10 & _
                " ,VTDS_RATE='" & VTDS_RATE & "' ,VTDS_F='" & VTDS_F & "' ,bankCODE='" & bankCODE & "' ,EFFECTFROM='" & EFFECTFROM & _
                "' ,VGross=" & VGross & " ,pfUlimit='" & pfUlimit & "' where paycode='" & PAYCODE & "'"
        Else
            'insert
            'VPF_NO = "NULL"
            sSql = "insert into Pay_Master (PAYCODE, VBASIC, VPaymentby, VBankAcc, VEmployeeType, VCPF_NO, VPF_NO, VESI_NO, VPAN_NO, VDA_RATE, " & _
                "VDA_F, VHRA_RATE, VHRA_F, VCONV_RATE, VCONV_F, VMED_RATE, VMED_F, VOT_RATE, VOT_FLAG, VI_1, VIT_1, VI_2, VIT_2, VI_3, VIT_3, " & _
                "VI_4, VIT_4, VI_5, VIT_5, VI_6, VIT_6, VI_7, VIT_7, VI_8, VIT_8, VI_9, VIT_9, VI_10, VIT_10, PF_ALLOWED, ESI_ALLOWED, VPF_ALLOWED, " & _
                "BONUS_ALLOWED, GRADUTY_ALLOWED, PROF_TAX_ALLOWED, VDT_1, VD_1, VDT_2, VD_2, VDT_3, VD_3, VDT_4, VD_4, VDT_5, VD_5, VDT_6, VD_6, VDT_7, " & _
                "VD_7, VDT_8, VD_8, VDT_9, VD_9, VDT_10, VD_10, VTDS_RATE, VTDS_F, bankCODE, EFFECTFROM, VGross, pfUlimit) " & _
                "values('" & PAYCODE & "'," & VBASIC & ",'" & VPaymentby & "','" & VBankAcc & "','" & VEmployeeType & "','" & VCPF_NO & "'," & VPF_NO & _
                ",'" & VESI_NO & "','" & VPAN_NO & "'," & VDA_RATE & ",'" & VDA_F & "'," & VHRA_RATE & ",'" & VHRA_F & _
                "'," & VCONV_RATE & ",'" & VCONV_F & "'," & VMED_RATE & ",'" & VMED_F & "'," & VOT_RATE & ",'" & VOT_FLAG & _
                "'," & VI_1 & ",'" & VIT_1 & "'," & VI_2 & ",'" & VIT_2 & "'," & VI_3 & ",'" & VIT_3 & "'," & VI_4 & _
                ",'" & VIT_4 & "'," & VI_5 & ",'" & VIT_5 & "'," & VI_6 & ",'" & VIT_6 & "'," & VI_7 & ",'" & VIT_7 & _
                "'," & VI_8 & ",'" & VIT_8 & "'," & VI_9 & ",'" & VIT_9 & "'," & VI_10 & ",'" & VIT_10 & "','" & PF_ALLOWED & _
                "','" & ESI_ALLOWED & "','" & VPF_ALLOWED & "','" & BONUS_ALLOWED & "','" & GRADUTY_ALLOWED & "','" & PROF_TAX_ALLOWED & _
                "','" & VDT_1 & "'," & VD_1 & ",'" & VDT_2 & "'," & VD_2 & ",'" & VDT_3 & "'," & VD_3 & ",'" & VDT_4 & "'," & VD_4 & _
                ",'" & VDT_5 & "'," & VD_5 & ",'" & VDT_6 & "'," & VD_6 & ",'" & VDT_7 & "'," & VD_7 & ",'" & VDT_8 & "'," & VD_8 & _
                ",'" & VDT_9 & "'," & VD_9 & ",'" & VDT_10 & "'," & VD_10 & "," & VTDS_RATE & ",'" & VTDS_F & "','" & bankCODE.Trim & _
                "','" & EFFECTFROM & "'," & VGross & ",'" & pfUlimit & "')"
        End If
        If Common.servername = "Access" Then
            If Common.con1.State <> ConnectionState.Open Then
                Common.con1.Open()
            End If
            cmd1 = New OleDbCommand(sSql, Common.con1)
            cmd1.ExecuteNonQuery()
            If Common.con1.State <> ConnectionState.Closed Then
                Common.con1.Close()
            End If
        Else
            If Common.con.State <> ConnectionState.Open Then
                Common.con.Open()
            End If
            cmd = New SqlCommand(sSql, Common.con)
            cmd.ExecuteNonQuery()
            If Common.con.State <> ConnectionState.Closed Then
                Common.con.Close()
            End If
        End If
        XtraMessageBox.Show(ulf, "<size=10>Saved Successfully</size>", "<size=9>Success</size>")
        Me.Close()
    End Sub
    Private Sub SimpleButtonCal_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonCal.Click
        If TextEditGross.Text.Trim = "" Then
            Exit Sub
        End If
        Dim sSql As String = "Select * from Pay_setup"
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim rsPaysetup As DataSet = New DataSet
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(rsPaysetup)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(rsPaysetup)
        End If
        Dim mskGross As String = TextEditGross.Text.Trim
        If XtraFullPayrollMenuMaster.PayEmpMster = "E" Then           
            TextEditBasic.Text = (mskGross * rsPaysetup.Tables(0).Rows(0).Item("gBASIC").ToString.Trim / 100)
            TextEditDA.Text = (mskGross * rsPaysetup.Tables(0).Rows(0).Item("gda") / 100)
            TextEditConvence.Text = (mskGross * rsPaysetup.Tables(0).Rows(0).Item("gconv") / 100)
            TextEditMedical.Text = (mskGross * rsPaysetup.Tables(0).Rows(0).Item("gmed") / 100)
            TextEditHRA.Text = (mskGross * rsPaysetup.Tables(0).Rows(0).Item("ghra") / 100)
            TextEditRateE1.Text = (mskGross * rsPaysetup.Tables(0).Rows(0).Item("gEARN1") / 100)
            TextEditRateE2.Text = (mskGross * rsPaysetup.Tables(0).Rows(0).Item("gEARN2") / 100)
            TextEditRateE3.Text = (mskGross * rsPaysetup.Tables(0).Rows(0).Item("gEARN3") / 100)
            TextEditRateE4.Text = (mskGross * rsPaysetup.Tables(0).Rows(0).Item("gEARN4") / 100)
            TextEditRateE5.Text = (mskGross * rsPaysetup.Tables(0).Rows(0).Item("gEARN5") / 100)
            TextEditRateE6.Text = (mskGross * rsPaysetup.Tables(0).Rows(0).Item("gEARN6") / 100)
            TextEditRateE7.Text = (mskGross * rsPaysetup.Tables(0).Rows(0).Item("gEARN7") / 100)
            TextEditRateE8.Text = (mskGross * rsPaysetup.Tables(0).Rows(0).Item("gEARN8") / 100)
            TextEditRateE9.Text = (mskGross * rsPaysetup.Tables(0).Rows(0).Item("gEARN9") / 100)
            TextEditRateE10.Text = (mskGross * rsPaysetup.Tables(0).Rows(0).Item("gEARN10") / 100)
        ElseIf XtraFullPayrollMenuMaster.PayEmpMster = "I" Then
            TextEditBasic.Text = mskGross * rsPaysetup.Tables(0).Rows(0).Item("gBASIC") / 100 + Val(TextEditBasic.Text)
            TextEditDA.Text = mskGross * rsPaysetup.Tables(0).Rows(0).Item("gda") / 100 + Val(TextEditDA.Text)
            TextEditConvence.Text = mskGross * rsPaysetup.Tables(0).Rows(0).Item("gconv") / 100 + Val(TextEditConvence.Text)
            TextEditMedical.Text = mskGross * rsPaysetup.Tables(0).Rows(0).Item("gmed") / 100 + Val(TextEditMedical.Text)
            TextEditHRA.Text = mskGross * rsPaysetup.Tables(0).Rows(0).Item("ghra") / 100 + Val(TextEditHRA.Text)
            TextEditRateE1.Text = mskGross * rsPaysetup.Tables(0).Rows(0).Item("gEARN1") / 100 + Val(TextEditRateE1.Text)
            TextEditRateE2.Text = mskGross * rsPaysetup.Tables(0).Rows(0).Item("gEARN2") / 100 + Val(TextEditRateE2.Text)
            TextEditRateE3.Text = mskGross * rsPaysetup.Tables(0).Rows(0).Item("gEARN3") / 100 + Val(TextEditRateE3.Text)
            TextEditRateE4.Text = mskGross * rsPaysetup.Tables(0).Rows(0).Item("gEARN4") / 100 + Val(TextEditRateE4.Text)
            TextEditRateE5.Text = mskGross * rsPaysetup.Tables(0).Rows(0).Item("gEARN5") / 100 + Val(TextEditRateE5.Text)
            TextEditRateE6.Text = mskGross * rsPaysetup.Tables(0).Rows(0).Item("gEARN6") / 100 + Val(TextEditRateE6.Text)
            TextEditRateE7.Text = mskGross * rsPaysetup.Tables(0).Rows(0).Item("gEARN7") / 100 + Val(TextEditRateE7.Text)
            TextEditRateE8.Text = mskGross * rsPaysetup.Tables(0).Rows(0).Item("gEARN8") / 100 + Val(TextEditRateE8.Text)
            TextEditRateE9.Text = mskGross * rsPaysetup.Tables(0).Rows(0).Item("gEARN9") / 100 + Val(TextEditRateE9.Text)
            TextEditRateE10.Text = mskGross * rsPaysetup.Tables(0).Rows(0).Item("gEARN10") / 100 + Val(TextEditRateE10.Text)
        End If
    End Sub
    Private Sub PictureEdit1_Click(sender As System.Object, e As System.EventArgs) Handles PictureEdit1.Click
        GridLookUpEditFormulaD1.EditValue = ""
    End Sub
    Private Sub PictureEdit2_EditValueChanged(sender As System.Object, e As System.EventArgs) Handles PictureEdit2.Click
        GridLookUpEditFormulaD2.EditValue = ""
    End Sub
    Private Sub PictureEdit4_EditValueChanged(sender As System.Object, e As System.EventArgs) Handles PictureEdit4.Click
        GridLookUpEditFormulaD3.EditValue = ""
    End Sub
    Private Sub PictureEdit3_EditValueChanged(sender As System.Object, e As System.EventArgs) Handles PictureEdit3.Click
        GridLookUpEditFormulaD4.EditValue = ""
    End Sub
    Private Sub PictureEdit5_EditValueChanged(sender As System.Object, e As System.EventArgs) Handles PictureEdit5.Click
        GridLookUpEditFormulaD5.EditValue = ""
    End Sub
    Private Sub PictureEdit6_EditValueChanged(sender As System.Object, e As System.EventArgs) Handles PictureEdit6.Click
        GridLookUpEditFormulaD6.EditValue = ""
    End Sub
    Private Sub PictureEdit9_EditValueChanged(sender As System.Object, e As System.EventArgs) Handles PictureEdit9.Click
        GridLookUpEditFormulaD9.EditValue = ""
    End Sub
    Private Sub PictureEdit8_EditValueChanged(sender As System.Object, e As System.EventArgs) Handles PictureEdit8.Click
        GridLookUpEditFormulaD8.EditValue = ""
    End Sub
    Private Sub PictureEdit7_EditValueChanged(sender As System.Object, e As System.EventArgs) Handles PictureEdit7.Click
        GridLookUpEditFormulaD7.EditValue = ""
    End Sub
    Private Sub PictureEdit10_EditValueChanged(sender As System.Object, e As System.EventArgs) Handles PictureEdit10.Click
        GridLookUpEditFormulaD10.EditValue = ""
    End Sub
    Private Sub PictureEdit11_EditValueChanged(sender As System.Object, e As System.EventArgs) Handles PictureEdit11.Click
        GridLookUpEditFormulaE1.EditValue = ""
    End Sub
    Private Sub PictureEdit12_EditValueChanged(sender As System.Object, e As System.EventArgs) Handles PictureEdit12.Click
        GridLookUpEditFormulaE2.EditValue = ""
    End Sub
    Private Sub PictureEdit13_EditValueChanged(sender As System.Object, e As System.EventArgs) Handles PictureEdit13.Click
        GridLookUpEditFormulaE3.EditValue = ""
    End Sub
    Private Sub PictureEdit14_EditValueChanged(sender As System.Object, e As System.EventArgs) Handles PictureEdit14.Click
        GridLookUpEditFormulaE4.EditValue = ""
    End Sub
    Private Sub PictureEdit15_EditValueChanged(sender As System.Object, e As System.EventArgs) Handles PictureEdit15.Click
        GridLookUpEditFormulaE5.EditValue = ""
    End Sub
    Private Sub PictureEdit16_EditValueChanged(sender As System.Object, e As System.EventArgs) Handles PictureEdit16.Click
        GridLookUpEditFormulaE6.EditValue = ""
    End Sub
    Private Sub PictureEdit17_EditValueChanged(sender As System.Object, e As System.EventArgs) Handles PictureEdit17.Click
        GridLookUpEditFormulaE7.EditValue = ""
    End Sub
    Private Sub PictureEdit18_EditValueChanged(sender As System.Object, e As System.EventArgs) Handles PictureEdit18.Click
        GridLookUpEditFormulaE8.EditValue = ""
    End Sub
    Private Sub PictureEdit19_EditValueChanged(sender As System.Object, e As System.EventArgs) Handles PictureEdit19.Click
        GridLookUpEditFormulaE9.EditValue = ""
    End Sub
    Private Sub PictureEdit20_EditValueChanged(sender As System.Object, e As System.EventArgs) Handles PictureEdit20.Click
        GridLookUpEditFormulaE10.EditValue = ""
    End Sub
End Class