﻿Imports Excel = Microsoft.Office.Interop.Excel
Imports DevExpress.LookAndFeel
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports DevExpress.XtraEditors

Public Class XtraEmployeeUpload
    Dim APP As New Excel.Application
    Dim worksheet As Excel.Worksheet
    Dim workbook As Excel.Workbook
    Dim numRow As Integer
    Dim numCol As Integer
    Dim ulf As UserLookAndFeel
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    Dim comclass As Common = New Common
    Public Sub New()
        InitializeComponent()
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True
    End Sub
    Private Sub XtraEmployeeUpload_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        TextEdit1.Text = ""
    End Sub
    Private Sub SimpleButton1_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton1.Click
        If TextEdit1.Text.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Please select the file to upload</size>", "<size>Error</size>")
        Else
            CmdEmployeeUpdate()
            Common.LogPost("Employee Excel Import")
        End If
    End Sub
    Private Sub TextEdit1_Click(sender As System.Object, e As System.EventArgs) Handles TextEdit1.Click
        Dim dlg As New OpenFileDialog()
        ' Filter by Office Files
        dlg.Filter = "Office Files|*.xlsx;*.xls;"
        dlg.ShowDialog()
        TextEdit1.Text = dlg.FileName
    End Sub
    Private Sub CmdEmployeeUpdate()
        Dim sSql As String
        Dim appWorldtmp As Excel.Application
        Dim wbWorld As Excel.Workbook
        Dim mFile, mLine, mDate, mPath
        Dim mCnt As Integer
        Dim rsTmp As DataSet 'ADODB.Recordset
        Dim rsA As DataSet 'ADODB.Recordset
        Dim aFileNum As Integer
        Dim sFileName As String
        Dim mCARDNO As String
        Dim mDate1 As Object
        Dim bCardRepl As Boolean
        Dim mrescd As String
        Dim TempCardNo As String
        Dim i As Integer
        Dim mReaderNo As String
        Dim rowcnt As Integer
        Dim mTime As Object
        Dim mTime1 As Object
        Dim cellcnt As Integer
        Dim mShift As String
        Dim mEmpName As String
        Dim mDept As String
        Dim mBranch As String
        Dim desig As String
        Dim doj As Object
        Dim gender As String
        Dim rsEmp As DataSet, rsComp As DataSet, rsdept As DataSet, rscat As DataSet, RsGrade As DataSet, RsDivision As DataSet, rsShift As DataSet
        Dim mDeptCode As String, tmpPath As String, strsql As String, mupdate As Integer, madded As Integer, mtotrec As Integer
        Dim intFile As Integer
        Dim mPaycode As String
        'Dim cn1 As New ADODB.connection
        Dim mDesig As String, mmgrade As String, mmcat As String
        Dim mDateofjoin As Object
        Dim mdateofbirth As Object
        'Dim mpfno As String 
        Dim MMSEX As String
        Dim mfather As String
        Dim mAddress1 As String
        'Dim mAddress2 As String
        Dim mtel1 As String
        'Dim mtel2 As String
        Dim mComp As String
        Dim mPrintedCardno As String
        Dim mOtApplicable As String
        'Dim mMarital As String
        Dim UserType As String
        Dim Hod As String
        Dim Email As String

        Dim mUid As String
        Dim mPan As String
        Dim MachineCard As String

        Dim adapA As OleDbDataAdapter
        Dim adap As SqlDataAdapter
        Dim ds As DataSet
        Dim mpinCode As String
        'LBLEMP.Visible = True
        madded = 0
        mupdate = 0
        mtotrec = 0
        'DoEvents()
        rowcnt = 0
        'On Error Resume Next
        Me.Cursor = Cursors.WaitCursor
        mPath = TextEdit1.Text.Trim
        Dim appWorld As Excel.Worksheet
        Try
            appWorldtmp = GetObject(, "Excel.Application")  'look for a running copy of Excel
            If Err.Number <> 0 Then 'If Excel is not running then
                appWorldtmp = CreateObject("Excel.Application") 'run it
            End If
            bCardRepl = False
            i = 0
            intFile = FreeFile()
            Err.Clear()
            wbWorld = appWorldtmp.Workbooks.Open(mPath)
            rowcnt = 2
        Catch ex As Exception
            XtraMessageBox.Show(ulf, "<size=10>" & ex.Message.ToString.Trim & "</size>", "<size=9>Error</size>")
            Exit Sub
        End Try
        'appWorld = wbWorld.Worksheets("sheet1")
        Try
            appWorld = wbWorld.Worksheets(1)
        Catch ex As Exception
            appWorldtmp.Quit()
            Me.Cursor = Cursors.Default
            XtraMessageBox.Show(ulf, "<size=10>Sheet not found</size>", "<size=9>Success</size>")
            Exit Sub
        End Try

        'MsgBox(appWorld.Cells(1, 1))
        Try
            For Each c As Control In Me.Controls
                c.Enabled = False
            Next

            Do While appWorld.Cells(rowcnt, 1).value <> Nothing ' Do While appWorld.Cells(rowcnt, 1).ToString.Trim <> ""
                mtotrec = mtotrec + 3
                If IsNumeric(appWorld.Cells(rowcnt, 1).value.ToString) = True Then
                    TempCardNo = Convert.ToInt64(appWorld.Cells(rowcnt, 1).value.ToString).ToString("000000000000")
                Else
                    TempCardNo = (appWorld.Cells(rowcnt, 1).value.ToString)
                End If
                If Trim(TempCardNo) = "" Then GoTo 1
                mPaycode = Trim(appWorld.Cells(rowcnt, 2).value)
                XtraMasterTest.LabelControlStatus.Text = "Uploading Employee " & mPaycode
                Application.DoEvents()
                mEmpName = Mid(appWorld.Cells(rowcnt, 3).value, 1, 25)

                mfather = Mid(appWorld.Cells(rowcnt, 4).value, 1, 30)
                Try
                    mDateofjoin = Convert.ToDateTime(Trim(appWorld.Cells(rowcnt, 10).value)).ToString("yyyy-MM-dd")
                Catch ex As Exception
                    mDateofjoin = Now.AddDays(-(Now.Day - 1)).ToString("yyyy-MM-dd HH:mm:ss")
                End Try
                Try
                    mdateofbirth = Convert.ToDateTime(Trim(appWorld.Cells(rowcnt, 11).value)).ToString("yyyy-MM-dd")
                Catch ex As Exception
                    mdateofbirth = Convert.ToDateTime("1800-01-01 00:00:00").ToString("yyyy-MM-dd")
                End Try

                MMSEX = Mid(appWorld.Cells(rowcnt, 13).value, 1, 1)
                'mBranch = Mid(Trim(appWorld.Cells(rowcnt, 14).value), 1, 25)
                mDesig = Mid(Trim(appWorld.Cells(rowcnt, 12).value), 1, 25)
                Email = Mid(Trim(appWorld.Cells(rowcnt, 15).value), 1, 50) ' Mid(Trim(appWorld.Cells(rowcnt, 21).value), 1, 50)
                mAddress1 = Mid(Trim(appWorld.Cells(rowcnt, 16).value), 1, 100) 'Mid(Trim(appWorld.Cells(rowcnt, 24).value), 1, 100)
                mAddress1 = mAddress1.Replace("'", "").Replace("`", "").Replace(",", " ")
                mpinCode = Mid(Trim(appWorld.Cells(rowcnt, 17).value), 1, 8) 'Mid(Trim(appWorld.Cells(rowcnt, 26).value), 1, 8)
                mtel1 = Mid(Trim(appWorld.Cells(rowcnt, 18).value), 1, 20) 'Mid(Trim(appWorld.Cells(rowcnt, 27).value), 1, 20)
                mUid = Mid(Trim(appWorld.Cells(rowcnt, 19).value), 1, 20)
                mPan = Mid(Trim(appWorld.Cells(rowcnt, 20).value), 1, 20)
                MachineCard = Mid(Trim(appWorld.Cells(rowcnt, 21).value), 1, 20)

                mShift = "S01" 'Trim(appWorld.Cells(rowcnt, 13))
                mOtApplicable = "N" ' Trim(appWorld.Cells(rowcnt, 16))


                'Company
                If Trim(appWorld.Cells(rowcnt, 5).value) = "" Or Trim(appWorld.Cells(rowcnt, 5).value).ToUpper = "NONE" Then
                    sSql = "select * from tblcompany"
                Else
                    sSql = "select * from tblcompany where companyname='" & Trim(appWorld.Cells(rowcnt, 5).value) & "'"
                End If

                rsdept = New DataSet
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(rsdept)
                Else
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(rsdept)
                End If
                'rsdept = Cn.Execute(sSql)
                If rsdept.Tables(0).Rows.Count > 0 Then
                    mComp = rsdept.Tables(0).Rows(0).Item("companycode").ToString.Trim
                Else
                    Try
                        'sSql = "select companycode from tblcompany"
                        rscat = New DataSet
                        If Common.servername = "Access" Then
                            sSql = "select max(val(companycode)) from tblcompany"
                            adapA = New OleDbDataAdapter(sSql, Common.con1)
                            adapA.Fill(rscat)
                        Else
                            sSql = "select max(companycode) from tblcompany"
                            adap = New SqlDataAdapter(sSql, Common.con)
                            adap.Fill(rscat)
                        End If
                        'rscat = Cn.Execute(sSql)
                        'mComp = rscat.Tables(0).Rows(0).Item(0).ToString.Trim
                        If rscat.Tables(0).Rows(0).Item(0).ToString.Trim = "" Or rscat.Tables(0).Rows(0).Item(0).ToString.Trim = "0" Then 'If IsNull(rscat(0)) Then
                            mComp = "001"
                        Else
                            mComp = (Convert.ToInt16(rscat.Tables(0).Rows(0).Item(0).ToString) + 1).ToString("000") 'Format(Val(rscat(0)) + 1, "000")
                        End If
                        sSql = "insert into tblcompany(companycode,companyname) values('" & mComp & "','" & Trim(appWorld.Cells(rowcnt, 5).value) & "')"
                        If Common.servername = "Access" Then
                            If Common.con1.State <> ConnectionState.Open Then
                                Common.con1.Open()
                            End If
                            cmd1 = New OleDbCommand(sSql, Common.con1)
                            cmd1.ExecuteNonQuery()
                            Common.con1.Close()
                        Else
                            If Common.con.State <> ConnectionState.Open Then
                                Common.con.Open()
                            End If
                            cmd = New SqlCommand(sSql, Common.con)
                            cmd.ExecuteNonQuery()
                            Common.con.Close()
                        End If
                    Catch ex As Exception
                        sSql = "select companycode from tblcompany"
                        rscat = New DataSet
                        If Common.servername = "Access" Then
                            adapA = New OleDbDataAdapter(sSql, Common.con1)
                            adapA.Fill(rscat)
                        Else
                            adap = New SqlDataAdapter(sSql, Common.con)
                            adap.Fill(rscat)
                        End If
                        mComp = rscat.Tables(0).Rows(0).Item(0).ToString.Trim
                    End Try
                    'Cn.Execute(sSql)
                End If

                '  ''department
                '            If Trim(appWorld.Cells(rowcnt, 7)) <> UCase("JUNIOR-PROGRAM") Then Stop
                If Trim(appWorld.Cells(rowcnt, 6).value) = "" Or Trim(appWorld.Cells(rowcnt, 6).value).ToUpper = "NONE" Then
                    sSql = "select * from tbldepartment"
                Else
                    sSql = "select * from tbldepartment where departmentname='" & Trim(appWorld.Cells(rowcnt, 6).value) & "'"
                End If
                rsdept = New DataSet
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(rsdept)
                Else
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(rsdept)
                End If
                'rsdept = Cn.Execute(sSql)
                If rsdept.Tables(0).Rows.Count > 0 Then
                    mDept = rsdept.Tables(0).Rows(0).Item("DepartmentCode").ToString.Trim
                Else
                    Try
                        rscat = New DataSet
                        If Common.servername = "Access" Then
                            sSql = "select max(val(departmentcode)) from tbldepartment"
                            adapA = New OleDbDataAdapter(sSql, Common.con1)
                            adapA.Fill(rscat)
                        Else
                            sSql = "select max(departmentcode) from tbldepartment"
                            adap = New SqlDataAdapter(sSql, Common.con)
                            adap.Fill(rscat)
                        End If
                        'rscat = Cn.Execute(sSql)
                        'mDept = rscat.Tables(0).Rows(0).Item(0).ToString.Trim
                        If rscat.Tables(0).Rows(0).Item(0).ToString.Trim = "" Or rscat.Tables(0).Rows(0).Item(0).ToString.Trim = "0" Then ' If IsNull(rscat(0)) Then
                            mDept = "001"
                        Else
                            mDept = (Convert.ToInt16(rscat.Tables(0).Rows(0).Item(0).ToString.Trim) + 1).ToString("000") 'Format(Val(rscat(0)) + 1, "000")
                        End If
                        'mDept = rsdept!DepartmentCode
                        sSql = "insert into tbldepartment(departmentcode,departmentname) values('" & mDept & "','" & Trim(appWorld.Cells(rowcnt, 6).value) & "')"
                        'Cn.Execute(sSql)
                        If Common.servername = "Access" Then
                            If Common.con1.State <> ConnectionState.Open Then
                                Common.con1.Open()
                            End If
                            cmd1 = New OleDbCommand(sSql, Common.con1)
                            cmd1.ExecuteNonQuery()
                            Common.con1.Close()
                        Else
                            If Common.con.State <> ConnectionState.Open Then
                                Common.con.Open()
                            End If
                            cmd = New SqlCommand(sSql, Common.con)
                            cmd.ExecuteNonQuery()
                            Common.con.Close()
                        End If
                    Catch ex As Exception
                        sSql = "select departmentcode from tbldepartment"
                        rscat = New DataSet
                        If Common.servername = "Access" Then
                            adapA = New OleDbDataAdapter(sSql, Common.con1)
                            adapA.Fill(rscat)
                        Else
                            adap = New SqlDataAdapter(sSql, Common.con)
                            adap.Fill(rscat)
                        End If
                        mDept = rscat.Tables(0).Rows(0).Item(0).ToString.Trim
                    End Try
                End If

                '  ''location
                If Trim(appWorld.Cells(rowcnt, 14).value) = "" Or Trim(appWorld.Cells(rowcnt, 14).value).ToUpper = "NONE" Then
                    sSql = "select * from tblbranch"
                Else
                    sSql = "select * from tblbranch where BRANCHNAME='" & Trim(appWorld.Cells(rowcnt, 14).value) & "'"
                End If
                rsdept = New DataSet
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(rsdept)
                Else
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(rsdept)
                End If
                'rsdept = Cn.Execute(sSql)
                If rsdept.Tables(0).Rows.Count > 0 Then
                    mBranch = rsdept.Tables(0).Rows(0).Item("BRANCHCODE").ToString.Trim
                Else
                    Try
                        'sSql = "select departmentcode from tbldepartment"
                        rscat = New DataSet
                        If Common.servername = "Access" Then
                            sSql = "select max(val(BRANCHCODE)) from tblbranch"
                            adapA = New OleDbDataAdapter(sSql, Common.con1)
                            adapA.Fill(rscat)
                        Else
                            sSql = "select max(BRANCHCODE) from tblbranch"
                            adap = New SqlDataAdapter(sSql, Common.con)
                            adap.Fill(rscat)
                        End If
                        'rscat = Cn.Execute(sSql)
                        'mDept = rscat.Tables(0).Rows(0).Item(0).ToString.Trim
                        If rscat.Tables(0).Rows(0).Item(0).ToString.Trim = "" Or rscat.Tables(0).Rows(0).Item(0).ToString.Trim = "0" Then ' If IsNull(rscat(0)) Then
                            mBranch = "001"
                        Else
                            mBranch = (Convert.ToInt16(rscat.Tables(0).Rows(0).Item(0).ToString.Trim) + 1).ToString("000") 'Format(Val(rscat(0)) + 1, "000")
                        End If
                        'mDept = rsdept!DepartmentCode
                        sSql = "insert into tblbranch(BRANCHCODE,BRANCHNAME) values('" & mBranch & "','" & Trim(appWorld.Cells(rowcnt, 14).value) & "')"
                        'Cn.Execute(sSql)
                        If Common.servername = "Access" Then
                            If Common.con1.State <> ConnectionState.Open Then
                                Common.con1.Open()
                            End If
                            cmd1 = New OleDbCommand(sSql, Common.con1)
                            cmd1.ExecuteNonQuery()
                            Common.con1.Close()
                        Else
                            If Common.con.State <> ConnectionState.Open Then
                                Common.con.Open()
                            End If
                            cmd = New SqlCommand(sSql, Common.con)
                            cmd.ExecuteNonQuery()
                            Common.con.Close()
                        End If
                    Catch ex As Exception
                        sSql = "select BRANCHCODE from tblbranch"
                        rscat = New DataSet
                        If Common.servername = "Access" Then
                            adapA = New OleDbDataAdapter(sSql, Common.con1)
                            adapA.Fill(rscat)
                        Else
                            adap = New SqlDataAdapter(sSql, Common.con)
                            adap.Fill(rscat)
                        End If
                        mBranch = rscat.Tables(0).Rows(0).Item(0).ToString.Trim
                    End Try
                End If



                '     mmgrade = "001"

                'Employee Group
                Dim mDiv As String
                Dim mGrpID As String
                sSql = "select * from EmployeeGroup where GroupName='" & Trim(appWorld.Cells(rowcnt, 7).value) & "'"
                'rsdept = Cn.Execute(sSql)
                rsdept = New DataSet
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(rsdept)
                Else
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(rsdept)
                End If
                If rsdept.Tables(0).Rows.Count > 0 Then
                    mDiv = rsdept.Tables(0).Rows(0).Item("GroupId").ToString.Trim
                    mGrpID = rsdept.Tables(0).Rows(0).Item("Id").ToString.Trim
                Else
                    'sSql = "select max(DivisionCode) from tbldivision"
                    sSql = "select GroupId from EmployeeGroup order by Id"
                    rscat = New DataSet
                    If Common.servername = "Access" Then
                        adapA = New OleDbDataAdapter(sSql, Common.con1)
                        adapA.Fill(rscat)
                    Else
                        adap = New SqlDataAdapter(sSql, Common.con)
                        adap.Fill(rscat)
                    End If
                    mDiv = rscat.Tables(0).Rows(0).Item(0).ToString.Trim
                    'If rscat.Tables(0).Rows(0).Item(0).ToString.Trim = "" Or rscat.Tables(0).Rows(0).Item(0).ToString.Trim = "0" Then ' If IsNull(rscat(0)) Then
                    '    mDiv = "001"
                    'Else
                    '    mDiv = (Convert.ToInt16(rscat.Tables(0).Rows(0).Item(0).ToString.Trim) + 1).ToString("000") ' Format(Val(rscat(0)) + 1, "000")
                    'End If
                    ''mmcat = rsdept!cat
                    ''mDiv = Trim(appWorld.Cells(rowcnt, 14))
                    'sSql = "insert into tbldivision(divisioncode,divisionname) values('" & mDiv & "','" & Trim(appWorld.Cells(rowcnt, 7)) & "')"
                    ''Cn.Execute(sSql)
                    'If Common.servername = "Access" Then
                    '    If Common.con1.State <> ConnectionState.Open Then
                    '        Common.con1.Open()
                    '    End If
                    '    cmd1 = New OleDbCommand(sSql, Common.con1)
                    '    cmd1.ExecuteNonQuery()
                    '    Common.con1.Close()
                    'Else
                    '    If Common.con.State <> ConnectionState.Open Then
                    '        Common.con.Open()
                    '    End If
                    '    cmd = New SqlCommand(sSql, Common.con)
                    '    cmd.ExecuteNonQuery()
                    '    Common.con.Close()
                    'End If
                End If



                'grade
                If Trim(appWorld.Cells(rowcnt, 8).value) = "" Or Trim(appWorld.Cells(rowcnt, 8).value).ToUpper = "NONE" Then
                    sSql = "select * from tblgrade"
                Else
                    sSql = "select * from tblgrade where gradename='" & Trim(appWorld.Cells(rowcnt, 8).value) & "'"
                End If
                'rsdept = Cn.Execute(sSql)
                rsdept = New DataSet
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(rsdept)
                Else
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(rsdept)
                End If
                If rsdept.Tables(0).Rows.Count > 0 Then
                    mmgrade = rsdept.Tables(0).Rows(0).Item("Gradecode").ToString.Trim
                Else
                    Try
                        rscat = New DataSet
                        If Common.servername = "Access" Then
                            sSql = "select max(val(gradecode)) from tblgrade"
                            adapA = New OleDbDataAdapter(sSql, Common.con1)
                            adapA.Fill(rscat)
                        Else
                            sSql = "select max(gradecode) from tblgrade"
                            adap = New SqlDataAdapter(sSql, Common.con)
                            adap.Fill(rscat)
                        End If
                        'mmgrade = rscat.Tables(0).Rows(0).Item(0).ToString.Trim
                        If rscat.Tables(0).Rows(0).Item(0).ToString.Trim = "" Or rscat.Tables(0).Rows(0).Item(0).ToString.Trim = "0" Then '  If IsNull(rscat(0)) Then
                            mmgrade = "001"
                        Else
                            mmgrade = (Convert.ToInt16(rscat.Tables(0).Rows(0).Item(0).ToString.Trim) + 1).ToString("000") 'Format(Val(rscat(0)) + 1, "000")
                        End If
                        ' mmgrade = Trim(appWorld.Cells(rowcnt, 15))
                        sSql = "insert into tblgrade(gradecode,gradename) values('" & mmgrade & "','" & Trim(appWorld.Cells(rowcnt, 8).value) & "')"
                        'Cn.Execute(sSql)
                        If Common.servername = "Access" Then
                            If Common.con1.State <> ConnectionState.Open Then
                                Common.con1.Open()
                            End If
                            cmd1 = New OleDbCommand(sSql, Common.con1)
                            cmd1.ExecuteNonQuery()
                            Common.con1.Close()
                        Else
                            If Common.con.State <> ConnectionState.Open Then
                                Common.con.Open()
                            End If
                            cmd = New SqlCommand(sSql, Common.con)
                            cmd.ExecuteNonQuery()
                            Common.con.Close()
                        End If
                    Catch ex As Exception
                        sSql = "select gradecode from tblgrade"
                        rscat = New DataSet
                        If Common.servername = "Access" Then
                            adapA = New OleDbDataAdapter(sSql, Common.con1)
                            adapA.Fill(rscat)
                        Else
                            adap = New SqlDataAdapter(sSql, Common.con)
                            adap.Fill(rscat)
                        End If
                        mmgrade = rscat.Tables(0).Rows(0).Item(0).ToString.Trim
                    End Try
                End If

                'category
                mmcat = "001"
                If Trim(appWorld.Cells(rowcnt, 9).value) = "" Or Trim(appWorld.Cells(rowcnt, 9).value).ToUpper = "NONE" Then
                    sSql = "select * from tblcatagory"
                Else
                    sSql = "select * from tblcatagory where catagoryname='" & Trim(appWorld.Cells(rowcnt, 9).value) & "'"
                End If
                'rsdept = Cn.Execute(sSql)
                rsdept = New DataSet
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(rsdept)
                Else
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(rsdept)
                End If
                If rsdept.Tables(0).Rows.Count > 0 Then
                    mmcat = rsdept.Tables(0).Rows(0).Item("Cat").ToString.Trim
                Else
                    Try
                        rscat = New DataSet
                        If Common.servername = "Access" Then
                            sSql = "select max(val(cat)) from tblcatagory"
                            adapA = New OleDbDataAdapter(sSql, Common.con1)
                            adapA.Fill(rscat)
                        Else
                            sSql = "select max(cat) from tblcatagory"
                            adap = New SqlDataAdapter(sSql, Common.con)
                            adap.Fill(rscat)
                        End If
                        'mmcat = rscat.Tables(0).Rows(0).Item(0).ToString.Trim
                        If rscat.Tables(0).Rows(0).Item(0).ToString.Trim = "" Or rscat.Tables(0).Rows(0).Item(0).ToString.Trim = "0" Then '   If IsNull(rscat(0)) Then
                            mmcat = "001"
                        Else
                            mmcat = (Convert.ToInt16(rscat.Tables(0).Rows(0).Item(0).ToString.Trim) + 1).ToString("000") 'Format(Val(rscat(0)) + 1, "000")
                        End If
                        'mmcat = Trim(appWorld.Cells(rowcnt, 16))
                        sSql = "insert into tblcatagory(cat,catagoryname) values('" & mmcat & "','" & Trim(appWorld.Cells(rowcnt, 9).value) & "')"
                        'Cn.Execute(sSql)
                        If Common.servername = "Access" Then
                            If Common.con1.State <> ConnectionState.Open Then
                                Common.con1.Open()
                            End If
                            cmd1 = New OleDbCommand(sSql, Common.con1)
                            cmd1.ExecuteNonQuery()
                            Common.con1.Close()
                        Else
                            If Common.con.State <> ConnectionState.Open Then
                                Common.con.Open()
                            End If
                            cmd = New SqlCommand(sSql, Common.con)
                            cmd.ExecuteNonQuery()
                            Common.con.Close()
                        End If
                    Catch ex As Exception
                        sSql = "select cat from tblcatagory"
                        rscat = New DataSet
                        If Common.servername = "Access" Then
                            adapA = New OleDbDataAdapter(sSql, Common.con1)
                            adapA.Fill(rscat)
                        Else
                            adap = New SqlDataAdapter(sSql, Common.con)
                            adap.Fill(rscat)
                        End If
                        mmcat = rscat.Tables(0).Rows(0).Item(0).ToString.Trim
                    End Try
                End If

                mPrintedCardno = TempCardNo
                If Common.servername = "Access" Then
                    sSql = "select * from tblemployee where active='Y'"
                    'rsA = Cn.Execute(sSql)
                    rsA = New DataSet
                    If Common.servername = "Access" Then
                        adapA = New OleDbDataAdapter(sSql, Common.con1)
                        adapA.Fill(rsA)
                    Else
                        adap = New SqlDataAdapter(sSql, Common.con)
                        adap.Fill(rsA)
                    End If

                    If rsA.Tables(0).Rows.Count > 500 Then
                        XtraMessageBox.Show(ulf, "<size=10>You can't add more than 500 employee in MS Access Database</size>", "<size=9TimeWatch</size>")
                        Me.Cursor = Cursors.Default
                        XtraMasterTest.LabelControlStatus.Text = ""
                        Application.DoEvents()
                        TextEdit1.Text = ""
                        Exit Sub
                    End If
                End If
                mEmpName = mEmpName.Replace("'", "''")
                'Emp_Update(mPaycode, TempCardNo, mComp, mDept, mmcat, mmgrade, mShift, mEmpName, mDesig, mDateofjoin, mdateofbirth, MMSEX, mfather, mAddress1, mAddress2, mtel1, mtel2, mDiv, mOtApplicable, mPrintedCardno, Email, Hod, UserType, mMarital, mesi, mblood, mQualification, mExperience, mbus, mvehicle, mpinCode, mpfno, mBranch, mUid, mPan)
                'Dim card As String = Trim(appWorld.Cells(rowcnt, 14).value)
                Emp_Update(mPaycode, TempCardNo, mComp, mDept, mmcat, mmgrade, mShift, mEmpName, mDesig, mDateofjoin, mdateofbirth, MMSEX, mfather, mAddress1, mtel1, mDiv, mOtApplicable, mPrintedCardno, Email, Hod, UserType, mpinCode, mBranch, mUid, mPan, mGrpID, MachineCard)
1:
                rowcnt = rowcnt + 1
            Loop
        Catch ex As Exception
            appWorldtmp.Quit()
            Me.Cursor = Cursors.Default
            For Each c As Control In Me.Controls
                c.Enabled = True
            Next
            XtraMasterTest.LabelControlStatus.Text = ""
            Application.DoEvents()
            XtraMessageBox.Show(ulf, "<size=10> Paycode: " & mPaycode & vbCrLf & ex.Message.ToString.Trim & "</size>", "<size=9>Error</size>")
            'XtraMessageBox.Show(ulf, "<size=10>Error Occurred! " & vbCrLf & "Please close the sheet and try again</size>", "<size=9>Error</size>")
            Exit Sub
        End Try
        appWorldtmp.Quit()


        Common.loadEmp()
        Common.loadCompany()
        Common.loadLocation()
        Common.loadDevice()
        Common.SetEmpGrpId()
        Common.LoadGroupStruct()
        
        Me.Cursor = Cursors.Default
        For Each c As Control In Me.Controls
            c.Enabled = True
        Next
        XtraMasterTest.LabelControlStatus.Text = ""
        Application.DoEvents()
        XtraMessageBox.Show(ulf, "<size=10>Employee Master Updation Completed</size>", "<size=9>Success</size>")
        Me.Close()
    End Sub
    Private Sub Emp_Update(mPaycode As String, mCARDNO As String, mComp As String, mDept As String, mCat As String, mgrade As String, mShift As String, mName As String, mDesig As String, mDateofjoin As Object, mdateofbirth As Object, MMSEX As String, mfather As String, mAddress1 As String, mtel1 As String, EmpGrp As String, mOtApplicable As String, mPrintedCardno As String, mEmail As String, mHOD As String, mUsert As String, mpinCode As String, mBranch As String, mUid As String, mPan As String, mGrpID As String, MachineCard As String)
        Dim sSql As String, strsql As String, i As Integer, M_PAYCODE As String
        Dim m_ispunchall As String, m_InOnly As String, m_two As String, m_isoutwork As String
        Dim m_isautoshift As String, m_IsRoundtheclockwork As String, m_istimelossallowed
        Dim m_isot As String, m_isos As String, m_isHalfDay As String, m_isshort As String
        Dim m_time As String, m_half As String, m_short As String, m_Auth_Shifts As String
        Dim m_isosminus As String, m_Weekdays As String, M_secondwotype As String
        Dim M_DATEOFBIRTH As String, M_SHIFT As String, m_halfdayshift As String, m_shifttype As String, m_LeavingDate As String, m_reader As String, mUserType As String

        If Trim(mUsert) = "" Then
            mUsert = "U"
        End If

        'For Employee Shift START
        mCARDNO = Trim(mCARDNO)

        M_DATEOFBIRTH = mdateofbirth
        m_ispunchall = "Y"
        m_InOnly = "N"
        m_two = "N"
        m_isoutwork = "Y"
        m_IsRoundtheclockwork = "N"
        m_istimelossallowed = "Y"
        m_isHalfDay = "N"
        m_isshort = "N"
        m_half = 0
        m_short = 0
        m_time = 0
        m_isautoshift = "N"
        m_Auth_Shifts = ""
        m_reader = ""
        m_isot = "N"
        m_isos = "N"
        m_isosminus = "N"
        m_shifttype = "F"
        M_SHIFT = mShift
        M_secondwotype = ""
        m_halfdayshift = ""
        m_Weekdays = m_Weekdays & Str(i)
        M_PAYCODE = mPaycode


        mAddress1 = mAddress1.Replace("'", "").Replace("`", "").Replace(",", " ")
        mName = mName.Replace("'", "").Replace("`", "").Replace(",", " ")
        Dim k As Integer

        k = 0

        If Common.servername = "Access" Then
            If Common.con1.State <> ConnectionState.Open Then
                Common.con1.Open()
            End If
        Else
            If Common.con.State <> ConnectionState.Open Then
                Common.con.Open()
            End If
        End If

        'card upload  in  fptable
        'sSql = "insert into fptable (EMachineNumber,EnrollNumber,UserName,FingerNumber,Privilege,Cardnumber) VALUES ('1','" & mCARDNO & "','" & mName & "','11','0','" & card & "')"
        'If Common.servername = "Access" Then
        '    cmd1 = New OleDbCommand(sSql, Common.con1)
        '    cmd1.ExecuteNonQuery()
        'Else
        '    cmd = New SqlCommand(sSql, Common.con)
        '    cmd.ExecuteNonQuery()
        'End If

        '  If frmBrowse.m_bModeNew Then
        sSql = "Insert Into tblEmployee(Paycode,presentcardno, LastModifiedBy, LastModifiedDate) Values('" & mPaycode & "','" & mCARDNO & "','Excel','" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "')"
        'Cn.Execute (sSql), k
        Try
            If Common.servername = "Access" Then
                cmd1 = New OleDbCommand(sSql, Common.con1)
                cmd1.ExecuteNonQuery()
            Else
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
            End If
        Catch ex As Exception
            'If Common.servername = "Access" Then
            '    If Common.con1.State <> ConnectionState.Closed Then
            '        Common.con1.Close()
            '    End If
            'Else
            '    If Common.con.State <> ConnectionState.Closed Then
            '        Common.con.Close()
            '    End If
            'End If
            'Exit Sub
        End Try

        'sSql = "Insert Into tblEmployeeShiftMaster(Paycode,CardNo) Values('" & mPaycode & "','" & mCARDNO & "')"
        'Cn.Execute(sSql)
        k = 0
        'If Common.servername = "Access" Then
        '    sSql = "update tblemployee set EMPNAME = '" & mName & _
        '        "',GUARDIANNAME = '" & mfather & "',PRESENTCARDNO ='" & mCARDNO & _
        '        "',ACTIVE = 'Y',COMPANYCODE = '" & mComp & _
        '        "',DEPARTMENTCODE ='" & mDept & "',CAT = '" & mCat & "',gradecode = '" & mgrade & "',EmployeeGroupId='" & EmpGrp & "',ISMARRIED = '" & mMarital & "' " & _
        '        ",BLOODGROUP = '" & mblood & "', DATEOFJOIN ='" & mDateofjoin & "' ,LEAVINGDATE = Null ,DATEOFBIRTH ='" & mdateofbirth & "',QUALIFICATION ='" & mQualification & "',EXPERIENCE ='" & mExperience & "',DESIGNATION ='" & Mid(mDesig, 1, 25) & "',SEX ='" & MMSEX & "',ADDRESS1 = '" & mAddress1 & "',PINCODE1 ='" & mpinCode & "'" & _
        '        ",TELEPHONE1 ='" & mtel1 & "',PFNO='" & mPf & "',ESINO='" & mesi & "',VehicleNo='" & mvehicle & "',E_MAIL1 ='" & mEmail & "',BUS = '" & mbus & "',ADDRESS2 ='" & mAddress2 & "',PINCODE2 = '',TELEPHONE2 ='" & mtel2 & "', BRANCHCODE='" & mBranch & "' where PayCode='" & mPaycode & "'"
        '    'Cn.Execute(sSql)
        '    cmd1 = New OleDbCommand(sSql, Common.con1)
        '    cmd1.ExecuteNonQuery()
        'Else
        '    sSql = "update tblemployee set EMPNAME = '" & mName & _
        '        "',GUARDIANNAME = '" & mfather & "',PRESENTCARDNO ='" & mCARDNO & _
        '        "',ACTIVE = 'Y',COMPANYCODE = '" & mComp & _
        '        "',DEPARTMENTCODE ='" & mDept & "',CAT = '" & mCat & "',gradecode = '" & mgrade & "',EmployeeGroupId='" & EmpGrp & "',ISMARRIED = '" & mMarital & "' " & _
        '        ",BLOODGROUP = '" & mblood & "',DATEOFJOIN ='" & mDateofjoin & "',DATEOFBIRTH ='" & mdateofbirth & "',QUALIFICATION ='" & mQualification & "',EXPERIENCE ='" & mExperience & "',DESIGNATION ='" & Mid(mDesig, 1, 25) & "',SEX ='" & MMSEX & "',ADDRESS1 = '" & mAddress1 & "',PINCODE1 ='" & mpinCode & "'" & _
        '        ",TELEPHONE1 ='" & mtel1 & "',PFNO='" & mPf & "',ESINO='" & mesi & "', VehicleNo='" & mvehicle & "',E_MAIL1 ='" & mEmail & "',BUS = '" & mbus & "',ADDRESS2 ='" & mAddress2 & "',PINCODE2 = '',TELEPHONE2 ='" & mtel2 & "', BRANCHCODE='" & mBranch & "' where PayCode='" & mPaycode & "'"
        '    'Cn.Execute(sSql)
        '    cmd = New SqlCommand(sSql, Common.con)
        '    cmd.ExecuteNonQuery()
        'End If
        If mfather.ToUpper = "NONE" Then
            mfather = ""
        End If
        If Common.servername = "Access" Then
            sSql = "update tblemployee set EMPNAME = '" & mName & "',GUARDIANNAME = '" & mfather & "',PRESENTCARDNO ='" & mCARDNO & _
                "',ACTIVE = 'Y',COMPANYCODE = '" & mComp & _
                "',DEPARTMENTCODE ='" & mDept & "',CAT = '" & mCat & "',gradecode = '" & mgrade & "',EmployeeGroupId='" & EmpGrp & "' " & _
                ", DATEOFJOIN ='" & mDateofjoin & "' ,LEAVINGDATE = '1800-01-01 00:00:00' ,DATEOFBIRTH ='" & mdateofbirth & "',DESIGNATION ='" & Mid(mDesig, 1, 25) & "',SEX ='" & MMSEX & "',ADDRESS1 = '" & mAddress1 & "',PINCODE1 ='" & mpinCode & "'" & _
                ",TELEPHONE1 ='" & mtel1 & "',E_MAIL1 ='" & mEmail & "', BRANCHCODE='" & mBranch & "', TELEPHONE2 = '" & mUid & "', PINCODE2='" & mPan & _
                "', ValidityStartDate='2018-01-01 00:00:00', ValidityEndDate='2029-12-31 00:00:00', MachineCard='" & MachineCard & "' where PayCode='" & mPaycode & "'"
            'Cn.Execute(sSql)
            cmd1 = New OleDbCommand(sSql, Common.con1)
            cmd1.ExecuteNonQuery()
        Else
            sSql = "update tblemployee set EMPNAME = '" & mName & "',GUARDIANNAME = '" & mfather & "',PRESENTCARDNO ='" & mCARDNO & _
                "',ACTIVE = 'Y',COMPANYCODE = '" & mComp & _
                "',DEPARTMENTCODE ='" & mDept & "',CAT = '" & mCat & "',gradecode = '" & mgrade & "',EmployeeGroupId='" & EmpGrp & "' " & _
                ",DATEOFJOIN ='" & mDateofjoin & "',LEAVINGDATE = '1800-01-01 00:00:00', DATEOFBIRTH ='" & mdateofbirth & "',DESIGNATION ='" & Mid(mDesig, 1, 25) & "',SEX ='" & MMSEX & "',ADDRESS1 = '" & mAddress1 & "',PINCODE1 ='" & mpinCode & "'" & _
                ",TELEPHONE1 ='" & mtel1 & "',E_MAIL1 ='" & mEmail & "', BRANCHCODE='" & mBranch & "', TELEPHONE2 = '" & mUid & "', PINCODE2='" & mPan & _
                "', ValidityStartDate='2018-01-01 00:00:00', ValidityEndDate='2029-12-31 00:00:00', MachineCard='" & MachineCard & "' where PayCode='" & mPaycode & "'"
            'Cn.Execute(sSql)
            cmd = New SqlCommand(sSql, Common.con)
            cmd.ExecuteNonQuery()
        End If


        Dim adap2 As SqlDataAdapter
        Dim adapA2 As OleDbDataAdapter
        Dim ds2 As DataSet = New DataSet
        sSql = "select * from EmployeeGroup where GroupId = '" & EmpGrp & "' "
        If Common.servername = "Access" Then
            adapA2 = New OleDbDataAdapter(sSql, Common.con1)
            adapA2.Fill(ds2)
        Else
            adap2 = New SqlDataAdapter(sSql, Common.con)
            adap2.Fill(ds2)
        End If

        Try
            If Common.servername = "Access" Then
                sSql = "insert into tblEmployeeShiftMaster (PAYCODE,CARDNO,SHIFT,SHIFTTYPE,SHIFTPATTERN,SHIFTREMAINDAYS,LASTSHIFTPERFORMED,INONLY,ISPUNCHALL,ISTIMELOSSALLOWED,ALTERNATE_OFF_DAYS,CDAYS,ISROUNDTHECLOCKWORK,ISOT,OTRATE,FIRSTOFFDAY,SECONDOFFTYPE,HALFDAYSHIFT,SECONDOFFDAY,PERMISLATEARRIVAL,PERMISEARLYDEPRT,ISAUTOSHIFT,ISOUTWORK,MAXDAYMIN,ISOS,AUTH_SHIFTS,TIME1,SHORT1,HALF,ISHALFDAY,ISSHORT,TWO, MSHIFT) values ( '" & mPaycode & "','" & mCARDNO & "','" & ds2.Tables(0).Rows(0).Item("SHIFT").ToString() & "','" & ds2.Tables(0).Rows(0).Item("SHIFTTYPE").ToString() & "','" & ds2.Tables(0).Rows(0).Item("SHIFTPATTERN").ToString() & "','" & ds2.Tables(0).Rows(0).Item("SHIFTREMAINDAYS").ToString() & "','" & ds2.Tables(0).Rows(0).Item("LASTSHIFTPERFORMED").ToString() & "','" & ds2.Tables(0).Rows(0).Item("INONLY").ToString() & "','" & ds2.Tables(0).Rows(0).Item("ISPUNCHALL").ToString() & "','" & ds2.Tables(0).Rows(0).Item("ISTIMELOSSALLOWED").ToString() & "','" & ds2.Tables(0).Rows(0).Item("ALTERNATE_OFF_DAYS").ToString() & "','" & ds2.Tables(0).Rows(0).Item("CDAYS").ToString() & "','" & ds2.Tables(0).Rows(0).Item("ISROUNDTHECLOCKWORK").ToString() & "','" & ds2.Tables(0).Rows(0).Item("ISOT").ToString() & "','" & ds2.Tables(0).Rows(0).Item("OTRATE").ToString() & "','" & ds2.Tables(0).Rows(0).Item("FIRSTOFFDAY").ToString() & "','" & ds2.Tables(0).Rows(0).Item("SECONDOFFTYPE").ToString() & "','" & ds2.Tables(0).Rows(0).Item("HALFDAYSHIFT").ToString() & "','" & ds2.Tables(0).Rows(0).Item("SECONDOFFDAY").ToString() & "','" & ds2.Tables(0).Rows(0).Item("PERMISLATEARRIVAL").ToString() & "','" & ds2.Tables(0).Rows(0).Item("PERMISEARLYDEPRT").ToString() & "','" & ds2.Tables(0).Rows(0).Item("ISAUTOSHIFT").ToString() & "','" & ds2.Tables(0).Rows(0).Item("ISOUTWORK").ToString() & "','" & ds2.Tables(0).Rows(0).Item("MAXDAYMIN").ToString() & "','" & ds2.Tables(0).Rows(0).Item("ISOS").ToString() & "','" & ds2.Tables(0).Rows(0).Item("AUTH_SHIFTS").ToString() & "','" & ds2.Tables(0).Rows(0).Item("TIME1").ToString() & "','" & ds2.Tables(0).Rows(0).Item("SHORT1").ToString() & "','" & ds2.Tables(0).Rows(0).Item("HALF").ToString() & "','" & ds2.Tables(0).Rows(0).Item("ISHALFDAY").ToString() & "','" & ds2.Tables(0).Rows(0).Item("ISSHORT").ToString() & "','" & ds2.Tables(0).Rows(0).Item("TWO").ToString() & "','" & ds2.Tables(0).Rows(0).Item("MSHIFT").ToString() & "')"
                cmd1 = New OleDbCommand(sSql, Common.con1)
                cmd1.ExecuteNonQuery()
            Else
                sSql = "insert into tblEmployeeShiftMaster (PAYCODE,CARDNO,SHIFT,SHIFTTYPE,SHIFTPATTERN,SHIFTREMAINDAYS,LASTSHIFTPERFORMED,INONLY,ISPUNCHALL,ISTIMELOSSALLOWED,ALTERNATE_OFF_DAYS,CDAYS,ISROUNDTHECLOCKWORK,ISOT,OTRATE,FIRSTOFFDAY,SECONDOFFTYPE,HALFDAYSHIFT,SECONDOFFDAY,PERMISLATEARRIVAL,PERMISEARLYDEPRT,ISAUTOSHIFT,ISOUTWORK,MAXDAYMIN,ISOS,AUTH_SHIFTS,TIME,SHORT,HALF,ISHALFDAY,ISSHORT,TWO,  MSHIFT) values ( '" & mPaycode & "','" & mCARDNO & "','" & ds2.Tables(0).Rows(0).Item("SHIFT").ToString() & "','" & ds2.Tables(0).Rows(0).Item("SHIFTTYPE").ToString() & "','" & ds2.Tables(0).Rows(0).Item("SHIFTPATTERN").ToString() & "','" & ds2.Tables(0).Rows(0).Item("SHIFTREMAINDAYS").ToString() & "','" & ds2.Tables(0).Rows(0).Item("LASTSHIFTPERFORMED").ToString() & "','" & ds2.Tables(0).Rows(0).Item("INONLY").ToString() & "','" & ds2.Tables(0).Rows(0).Item("ISPUNCHALL").ToString() & "','" & ds2.Tables(0).Rows(0).Item("ISTIMELOSSALLOWED").ToString() & "','" & ds2.Tables(0).Rows(0).Item("ALTERNATE_OFF_DAYS").ToString() & "','" & ds2.Tables(0).Rows(0).Item("CDAYS").ToString() & "','" & ds2.Tables(0).Rows(0).Item("ISROUNDTHECLOCKWORK").ToString() & "','" & ds2.Tables(0).Rows(0).Item("ISOT").ToString() & "','" & ds2.Tables(0).Rows(0).Item("OTRATE").ToString() & "','" & ds2.Tables(0).Rows(0).Item("FIRSTOFFDAY").ToString() & "','" & ds2.Tables(0).Rows(0).Item("SECONDOFFTYPE").ToString() & "','" & ds2.Tables(0).Rows(0).Item("HALFDAYSHIFT").ToString() & "','" & ds2.Tables(0).Rows(0).Item("SECONDOFFDAY").ToString() & "','" & ds2.Tables(0).Rows(0).Item("PERMISLATEARRIVAL").ToString() & "','" & ds2.Tables(0).Rows(0).Item("PERMISEARLYDEPRT").ToString() & "','" & ds2.Tables(0).Rows(0).Item("ISAUTOSHIFT").ToString() & "','" & ds2.Tables(0).Rows(0).Item("ISOUTWORK").ToString() & "','" & ds2.Tables(0).Rows(0).Item("MAXDAYMIN").ToString() & "','" & ds2.Tables(0).Rows(0).Item("ISOS").ToString() & "','" & ds2.Tables(0).Rows(0).Item("AUTH_SHIFTS").ToString() & "','" & ds2.Tables(0).Rows(0).Item("TIME").ToString() & "','" & ds2.Tables(0).Rows(0).Item("SHORT").ToString() & "','" & ds2.Tables(0).Rows(0).Item("HALF").ToString() & "','" & ds2.Tables(0).Rows(0).Item("ISHALFDAY").ToString() & "','" & ds2.Tables(0).Rows(0).Item("ISSHORT").ToString() & "','" & ds2.Tables(0).Rows(0).Item("TWO").ToString() & "','" & ds2.Tables(0).Rows(0).Item("MSHIFT").ToString() & "')"
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
            End If
        Catch ex As Exception
            Try
                If Common.servername = "Access" Then
                    sSql = "udate tblEmployeeShiftMaster set SHIFT = '" & ds2.Tables(0).Rows(0).Item("SHIFT").ToString() & "'" & _
    ",SHIFTTYPE  = '" & ds2.Tables(0).Rows(0).Item("SHIFTTYPE").ToString() & "',SHIFTPATTERN  = '" & ds2.Tables(0).Rows(0).Item("SHIFTPATTERN").ToString() & "'" & _
    ",SHIFTREMAINDAYS  = '" & ds2.Tables(0).Rows(0).Item("SHIFTREMAINDAYS").ToString() & "',LASTSHIFTPERFORMED  = '" & ds2.Tables(0).Rows(0).Item("LASTSHIFTPERFORMED").ToString() & "'" & _
    ",INONLY  = '" & ds2.Tables(0).Rows(0).Item("INONLY").ToString() & "'" & _
    ",ISPUNCHALL  = '" & ds2.Tables(0).Rows(0).Item("ISPUNCHALL").ToString() & "'" & _
    ",ISTIMELOSSALLOWED  = '" & ds2.Tables(0).Rows(0).Item("ISTIMELOSSALLOWED").ToString() & "'" & _
    ",ALTERNATE_OFF_DAYS  = '" & ds2.Tables(0).Rows(0).Item("ALTERNATE_OFF_DAYS").ToString() & "'" & _
    ",CDAYS  = '" & ds2.Tables(0).Rows(0).Item("CDAYS").ToString() & "'" & _
    ",ISROUNDTHECLOCKWORK  = '" & ds2.Tables(0).Rows(0).Item("ISROUNDTHECLOCKWORK").ToString() & "'" & _
    ",ISOT  = '" & ds2.Tables(0).Rows(0).Item("ISOT").ToString() & "'" & _
    ",OTRATE  = '" & ds2.Tables(0).Rows(0).Item("OTRATE").ToString() & "'" & _
    ",FIRSTOFFDAY  = '" & ds2.Tables(0).Rows(0).Item("FIRSTOFFDAY").ToString() & "'" & _
    ",SECONDOFFTYPE  = '" & ds2.Tables(0).Rows(0).Item("SECONDOFFTYPE").ToString() & "'" & _
    ",HALFDAYSHIFT  = '" & ds2.Tables(0).Rows(0).Item("HALFDAYSHIFT").ToString() & "'" & _
    ",SECONDOFFDAY  = '" & ds2.Tables(0).Rows(0).Item("SECONDOFFDAY").ToString() & "'" & _
    ",PERMISLATEARRIVAL  = '" & ds2.Tables(0).Rows(0).Item("PERMISLATEARRIVAL").ToString() & "'" & _
    ",PERMISEARLYDEPRT  = '" & ds2.Tables(0).Rows(0).Item("PERMISEARLYDEPRT").ToString() & "'" & _
    ",ISAUTOSHIFT  = '" & ds2.Tables(0).Rows(0).Item("ISAUTOSHIFT").ToString() & "'" & _
    ",ISOUTWORK  = '" & ds2.Tables(0).Rows(0).Item("ISOUTWORK").ToString() & "'" & _
    ",MAXDAYMIN  = '" & ds2.Tables(0).Rows(0).Item("MAXDAYMIN").ToString() & "'" & _
    ",ISOS  = '" & ds2.Tables(0).Rows(0).Item("ISOS").ToString() & "'" & _
    ",AUTH_SHIFTS  = '" & ds2.Tables(0).Rows(0).Item("AUTH_SHIFTS").ToString() & "'" & _
    ",TIME1  = '" & ds2.Tables(0).Rows(0).Item("TIME1").ToString() & "'" & _
    ",SHORT1  = '" & ds2.Tables(0).Rows(0).Item("SHORT1").ToString() & "'" & _
    ",HALF  = '" & ds2.Tables(0).Rows(0).Item("HALF").ToString() & "'" & _
    ",ISHALFDAY  = '" & ds2.Tables(0).Rows(0).Item("ISHALFDAY").ToString() & "'" & _
    ",ISSHORT  = '" & ds2.Tables(0).Rows(0).Item("ISSHORT").ToString() & "'" & _
    ",TWO  = '" & ds2.Tables(0).Rows(0).Item("TWO").ToString() & "'" & _
    ", MSHIFT = '" & ds2.Tables(0).Rows(0).Item("MSHIFT").ToString() & "'" & _
    ") where PAYCODE  = '" & mPaycode & "'"
                    cmd1 = New OleDbCommand(sSql, Common.con1)
                    cmd1.ExecuteNonQuery()
                Else
                    sSql = "udate tblEmployeeShiftMaster set " & _
    "SHIFT = '" & ds2.Tables(0).Rows(0).Item("SHIFT").ToString() & "'" & _
    ",SHIFTTYPE  = '" & ds2.Tables(0).Rows(0).Item("SHIFTTYPE").ToString() & "'" & _
    ",SHIFTPATTERN  = '" & ds2.Tables(0).Rows(0).Item("SHIFTPATTERN").ToString() & "'" & _
    ",SHIFTREMAINDAYS  = '" & ds2.Tables(0).Rows(0).Item("SHIFTREMAINDAYS").ToString() & "'" & _
    ",LASTSHIFTPERFORMED  = '" & ds2.Tables(0).Rows(0).Item("LASTSHIFTPERFORMED").ToString() & "'" & _
    ",INONLY  = '" & ds2.Tables(0).Rows(0).Item("INONLY").ToString() & "'" & _
    ",ISPUNCHALL  = '" & ds2.Tables(0).Rows(0).Item("ISPUNCHALL").ToString() & "'" & _
    ",ISTIMELOSSALLOWED  = '" & ds2.Tables(0).Rows(0).Item("ISTIMELOSSALLOWED").ToString() & "'" & _
    ",ALTERNATE_OFF_DAYS  = '" & ds2.Tables(0).Rows(0).Item("ALTERNATE_OFF_DAYS").ToString() & "'" & _
    ",CDAYS  = '" & ds2.Tables(0).Rows(0).Item("CDAYS").ToString() & "'" & _
    ",ISROUNDTHECLOCKWORK  = '" & ds2.Tables(0).Rows(0).Item("ISROUNDTHECLOCKWORK").ToString() & "'" & _
    ",ISOT  = '" & ds2.Tables(0).Rows(0).Item("ISOT").ToString() & "'" & _
    ",OTRATE  = '" & ds2.Tables(0).Rows(0).Item("OTRATE").ToString() & "'" & _
    ",FIRSTOFFDAY  = '" & ds2.Tables(0).Rows(0).Item("FIRSTOFFDAY").ToString() & "'" & _
    ",SECONDOFFTYPE  = '" & ds2.Tables(0).Rows(0).Item("SECONDOFFTYPE").ToString() & "'" & _
    ",HALFDAYSHIFT  = '" & ds2.Tables(0).Rows(0).Item("HALFDAYSHIFT").ToString() & "'" & _
    ",SECONDOFFDAY  = '" & ds2.Tables(0).Rows(0).Item("SECONDOFFDAY").ToString() & "'" & _
    ",PERMISLATEARRIVAL  = '" & ds2.Tables(0).Rows(0).Item("PERMISLATEARRIVAL").ToString() & "'" & _
    ",PERMISEARLYDEPRT  = '" & ds2.Tables(0).Rows(0).Item("PERMISEARLYDEPRT").ToString() & "'" & _
    ",ISAUTOSHIFT  = '" & ds2.Tables(0).Rows(0).Item("ISAUTOSHIFT").ToString() & "'" & _
    ",ISOUTWORK  = '" & ds2.Tables(0).Rows(0).Item("ISOUTWORK").ToString() & "'" & _
    ",MAXDAYMIN  = '" & ds2.Tables(0).Rows(0).Item("MAXDAYMIN").ToString() & "'" & _
    ",ISOS  = '" & ds2.Tables(0).Rows(0).Item("ISOS").ToString() & "'" & _
    ",AUTH_SHIFTS  = '" & ds2.Tables(0).Rows(0).Item("AUTH_SHIFTS").ToString() & "'" & _
    ",TIME  = '" & ds2.Tables(0).Rows(0).Item("TIME").ToString() & "'" & _
    ",SHORT  = '" & ds2.Tables(0).Rows(0).Item("SHORT").ToString() & "'" & _
    ",HALF  = '" & ds2.Tables(0).Rows(0).Item("HALF").ToString() & "'" & _
    ",ISHALFDAY  = '" & ds2.Tables(0).Rows(0).Item("ISHALFDAY").ToString() & "'" & _
    ",ISSHORT  = '" & ds2.Tables(0).Rows(0).Item("ISSHORT").ToString() & "'" & _
    ",TWO  = '" & ds2.Tables(0).Rows(0).Item("TWO").ToString() & "'" & _
     ", MSHIFT = '" & ds2.Tables(0).Rows(0).Item("MSHIFT").ToString() & "'" & _
    ") where PAYCODE  = '" & mPaycode & "'"
                    cmd = New SqlCommand(sSql, Common.con)
                    cmd.ExecuteNonQuery()
                End If
            Catch ex1 As Exception

            End Try

        End Try

        ' --update for tblemployeeshiftmaster

        'If Common.servername = "Access" Then
        '    strsql = " update tblemployeeshiftmaster set Cardno='" & mPrintedCardno & "', ispunchall ='" & m_ispunchall & "', InOnly ='" & _
        '    m_InOnly & "', two = '" & m_two & "', isoutwork = '" & m_isoutwork & "',isautoshift = '" & _
        '    m_isautoshift & "',IsRoundtheclockwork = '" & m_IsRoundtheclockwork & "',istimelossallowed ='" & _
        '    m_istimelossallowed & "',isHalfDay ='" & m_isHalfDay & "',isshort = '" & m_isshort & "',half = '" & _
        '     m_half & "',Auth_Shifts = '" & m_Auth_Shifts & _
        '    "',isot ='" & m_isot & "', isos ='" & m_isos & "',MaxDayMin = '960'" & _
        '    " " & _
        '    ",Shift ='" & M_SHIFT & "',ShiftType = '" & m_shifttype & "',ShiftPattern ='" & _
        '    "',ShiftRemainDays ='0',alternate_off_days ='',CDays ='0" & _
        '    "',FirstOffDay ='SUN',secondOffDay =' " & _
        '    "',SecondOfftype ='F" & _
        '    "', HalfDayShift ='" & m_halfdayshift & "',OTRate ='00',PERMISEARLYDEPRT ='10" & _
        '    "',PermisLateArrival ='10' where payCode ='" & M_PAYCODE & "'"
        '    Cn.Execute(strsql)
        'Else
        '    strsql = " update tblemployeeshiftmaster set Cardno='" & mPrintedCardno & "', ispunchall ='" & m_ispunchall & "', InOnly ='" & _
        '    m_InOnly & "', two = '" & m_two & "', isoutwork = '" & m_isoutwork & "',isautoshift = '" & _
        '    m_isautoshift & "',IsRoundtheclockwork = '" & m_IsRoundtheclockwork & "',istimelossallowed ='" & _
        '    m_istimelossallowed & "',isHalfDay ='" & m_isHalfDay & "',isshort = '" & m_isshort & "',time = '" & _
        '    m_time & "', short = '" & m_short & "',half = '" & m_half & "',Auth_Shifts = '" & m_Auth_Shifts & _
        '    "',isot ='" & m_isot & "', isos ='" & m_isos & "',MaxDayMin = '960'" & _
        '    " " & _
        '    ",Shift ='" & M_SHIFT & "',ShiftType = '" & m_shifttype & "',ShiftPattern ='" & _
        '    "',ShiftRemainDays ='0',alternate_off_days ='',CDays ='0" & _
        '    "',FirstOffDay ='SUN',secondOffDay =' " & _
        '    "',SecondOfftype ='F" & _
        '    "', HalfDayShift ='" & m_halfdayshift & "',OTRate ='00',PERMISEARLYDEPRT ='10" & _
        '    "',PermisLateArrival ='10' where payCode ='" & M_PAYCODE & "'"
        '    Cn.Execute(strsql)
        'End If
        'Cn.Execute(sSql)
        'Cn.Execute(strsql)

        'sSql = "Insert into tblleaveledger (paycode,lyear,L01,L02,L03,L04,L05,L06,L07,L08,L09,L10,L11,L12,L13,L14,L15,L16,L17,L18,L19,L20,L01_Add,L02_Add,L03_Add,L04_Add,L05_Add,L06_Add,L07_Add,L08_Add,L09_Add,L10_Add,L11_Add,L12_Add,L13_Add,L14_Add,L15_Add,L16_Add,L17_Add,L18_Add,L19_Add,L20_Add,L01_CASH,L02_CASH,L03_CASH,L04_CASH,L05_CASH,L06_CASH,L07_CASH,L08_CASH,L09_CASH,L10_CASH,L11_CASH,L12_CASH,L13_CASH,L14_CASH,L15_CASH,L16_CASH,L17_CASH,L18_CASH,L19_CASH,L20_CASH) values('" & M_PAYCODE & "'," & Year(Now()) & ",0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)"
        'Cn.Execute(sSql)

        If Common.servername = "Access" Then
            sSql = "UPDATE TBLEMPLOYEESHIFTMASTER SET  TIME1='" & m_time & "', short1 = '" & m_short & "' WHERE PAYCODE='" & M_PAYCODE & "'"
            'Cn.Execute(sSql)
            If Common.servername = "Access" Then
                cmd1 = New OleDbCommand(sSql, Common.con1)
                cmd1.ExecuteNonQuery()
            Else
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
            End If
        End If
        If CheckEdit1.Checked = True Then
            sSql = "Insert into tbluser (user_r,userdescriprion, usertype,paycode,password) values('" & M_PAYCODE & "', '" & mName & "','" & mUsert & "','" & M_PAYCODE & "','" & M_PAYCODE & "')"
            'Cn.Execute(sSql)
            If Common.servername = "Access" Then
                cmd1 = New OleDbCommand(sSql, Common.con1)
                cmd1.ExecuteNonQuery()
            Else
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
            End If
        End If

        If Common.servername = "Access" Then
            If Common.con1.State <> ConnectionState.Closed Then
                Common.con1.Close()
            End If
        Else
            If Common.con.State <> ConnectionState.Closed Then
                Common.con.Close()
            End If
        End If
        'Dim adapS As SqlDataAdapter
        'Dim adapAc As OleDbDataAdapter
        'Dim Rs As DataSet = New DataSet
        'sSql = " Select * from tblSetUp where setupid =(Select Convert(varchar(10),Max(Convert(int,Setupid))) from tblsetup )"
        'If Common.servername = "Access" Then
        '    sSql = " Select * from tblSetUp where setupid =(Select CVar(Max(CInt(Setupid))) from tblsetup )"
        '    adapAc = New OleDbDataAdapter(sSql, Common.con1)
        '    adapAc.Fill(Rs)
        'Else
        '    adapS = New SqlDataAdapter(sSql, Common.con)
        '    adapS.Fill(Rs)
        'End If
        Dim g_Wo_Include As String = Common.EmpGrpArr(mGrpID).g_Wo_Include 'Rs.Tables(0).Rows(0).Item("WOInclude").ToString.Trim
        Dim mmin_date As DateTime = Now.ToString("01/" & Now.Month & "/" & Now.Year)
        Try
            If Convert.ToDateTime(mDateofjoin) > mmin_date Then
                mmin_date = Convert.ToDateTime(mDateofjoin)
            End If

        Catch ex As Exception

        End Try
        comclass.DutyRoster(mPaycode, mmin_date, g_Wo_Include)
    End Sub
End Class