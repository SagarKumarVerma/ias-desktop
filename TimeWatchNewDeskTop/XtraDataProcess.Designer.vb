﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraDataProcess
    Inherits DevExpress.XtraEditors.XtraUserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(XtraDataProcess))
        Me.SplitContainerControl1 = New DevExpress.XtraEditors.SplitContainerControl()
        Me.GroupControlLeave = New DevExpress.XtraEditors.GroupControl()
        Me.DateToLeave = New DevExpress.XtraEditors.DateEdit()
        Me.LabelLeaveTo = New DevExpress.XtraEditors.LabelControl()
        Me.DateFromLeave = New DevExpress.XtraEditors.DateEdit()
        Me.LabelLeaveFrm = New DevExpress.XtraEditors.LabelControl()
        Me.BtnOracle = New DevExpress.XtraEditors.SimpleButton()
        Me.GroupControl5 = New DevExpress.XtraEditors.GroupControl()
        Me.ComboNepaliYearVal = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboNEpaliMonthVal = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.CheckEditUnMark = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditMark = New DevExpress.XtraEditors.CheckEdit()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.CheckEditLate = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditWO = New DevExpress.XtraEditors.CheckEdit()
        Me.DateEditVerification = New DevExpress.XtraEditors.DateEdit()
        Me.SimpleButtonVerification = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.PopupContainerControlEmp = New DevExpress.XtraEditors.PopupContainerControl()
        Me.GridControlEmp = New DevExpress.XtraGrid.GridControl()
        Me.TblEmployeeBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SSSDBDataSet = New iAS.SSSDBDataSet()
        Me.GridViewEmp = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colPAYCODE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colEMPNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.PopupContainerControlCompany = New DevExpress.XtraEditors.PopupContainerControl()
        Me.GridControlComp = New DevExpress.XtraGrid.GridControl()
        Me.TblCompanyBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GridViewComp = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTimeEdit4 = New DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit()
        Me.GroupControl4 = New DevExpress.XtraEditors.GroupControl()
        Me.CheckDuplicate = New DevExpress.XtraEditors.CheckEdit()
        Me.ComboNepaliYear = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboNEpaliMonth = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboNepaliDate = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.PopupContainerEdit1 = New DevExpress.XtraEditors.PopupContainerEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.ComboBoxEdit1 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.DateEdit3 = New DevExpress.XtraEditors.DateEdit()
        Me.CheckAll = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckSelective = New DevExpress.XtraEditors.CheckEdit()
        Me.PopupContainerControlLocation = New DevExpress.XtraEditors.PopupContainerControl()
        Me.GridControlLocation = New DevExpress.XtraGrid.GridControl()
        Me.TblbranchBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GridViewLocation = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colBRANCHCODE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBRANCHNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTimeEdit3 = New DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit()
        Me.PopupContainerControlDept = New DevExpress.XtraEditors.PopupContainerControl()
        Me.GridControlDept = New DevExpress.XtraGrid.GridControl()
        Me.TblDepartmentBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GridViewDept = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colDEPARTMENTCODE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDEPARTMENTNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTimeEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit()
        Me.GroupControl3 = New DevExpress.XtraEditors.GroupControl()
        Me.SimpleButton2 = New DevExpress.XtraEditors.SimpleButton()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.ToggleEmp = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LookUpEditDevice = New DevExpress.XtraEditors.LookUpEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditUploadLogs = New DevExpress.XtraEditors.TextEdit()
        Me.SimpleButtonProcess = New DevExpress.XtraEditors.SimpleButton()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.TextEdit1 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.MemoEdit1 = New DevExpress.XtraEditors.MemoEdit()
        Me.TblCompanyTableAdapter = New iAS.SSSDBDataSetTableAdapters.tblCompanyTableAdapter()
        Me.TblCompany1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.tblCompany1TableAdapter()
        Me.TblDepartmentTableAdapter = New iAS.SSSDBDataSetTableAdapters.tblDepartmentTableAdapter()
        Me.TblDepartment1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.tblDepartment1TableAdapter()
        Me.TblbranchTableAdapter = New iAS.SSSDBDataSetTableAdapters.tblbranchTableAdapter()
        Me.Tblbranch1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.tblbranch1TableAdapter()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colCOMPANYCODE1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCOMPANYNAME1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCOMPANYADDRESS = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSHORTNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPANNUM = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colTANNUMBER = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colTDSCIRCLE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLCNO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPFNO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLastModifiedBy = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLastModifiedDate = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colGSTIN = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTimeEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit()
        Me.colCOMPANYCODE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCOMPANYNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.TblEmployeeTableAdapter = New iAS.SSSDBDataSetTableAdapters.TblEmployeeTableAdapter()
        Me.TblEmployee1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.TblEmployee1TableAdapter()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerControl1.SuspendLayout()
        CType(Me.GroupControlLeave, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControlLeave.SuspendLayout()
        CType(Me.DateToLeave.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateToLeave.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateFromLeave.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateFromLeave.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl5.SuspendLayout()
        CType(Me.ComboNepaliYearVal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboNEpaliMonthVal.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditUnMark.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditMark.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.CheckEditLate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditWO.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEditVerification.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEditVerification.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerControlEmp, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PopupContainerControlEmp.SuspendLayout()
        CType(Me.GridControlEmp, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblEmployeeBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewEmp, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerControlCompany, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PopupContainerControlCompany.SuspendLayout()
        CType(Me.GridControlComp, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblCompanyBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewComp, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTimeEdit4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl4.SuspendLayout()
        CType(Me.CheckDuplicate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboNepaliYear.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboNEpaliMonth.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboNepaliDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit3.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckAll.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckSelective.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerControlLocation, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PopupContainerControlLocation.SuspendLayout()
        CType(Me.GridControlLocation, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblbranchBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewLocation, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTimeEdit3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerControlDept, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PopupContainerControlDept.SuspendLayout()
        CType(Me.GridControlDept, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblDepartmentBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewDept, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTimeEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.ToggleEmp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LookUpEditDevice.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditUploadLogs.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTimeEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SplitContainerControl1
        '
        Me.SplitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainerControl1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainerControl1.Name = "SplitContainerControl1"
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.GroupControlLeave)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.GroupControl5)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.PopupContainerControlEmp)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.PopupContainerControlCompany)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.GroupControl4)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.PopupContainerControlLocation)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.PopupContainerControlDept)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.GroupControl3)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.GroupControl2)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.GroupControl1)
        Me.SplitContainerControl1.Panel1.Text = "Panel1"
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.MemoEdit1)
        Me.SplitContainerControl1.Panel2.Text = "Panel2"
        Me.SplitContainerControl1.Size = New System.Drawing.Size(1250, 568)
        Me.SplitContainerControl1.SplitterPosition = 1165
        Me.SplitContainerControl1.TabIndex = 0
        Me.SplitContainerControl1.Text = "SplitContainerControl1"
        '
        'GroupControlLeave
        '
        Me.GroupControlLeave.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControlLeave.AppearanceCaption.ForeColor = System.Drawing.Color.Blue
        Me.GroupControlLeave.AppearanceCaption.Options.UseFont = True
        Me.GroupControlLeave.AppearanceCaption.Options.UseForeColor = True
        Me.GroupControlLeave.Controls.Add(Me.DateToLeave)
        Me.GroupControlLeave.Controls.Add(Me.LabelLeaveTo)
        Me.GroupControlLeave.Controls.Add(Me.DateFromLeave)
        Me.GroupControlLeave.Controls.Add(Me.LabelLeaveFrm)
        Me.GroupControlLeave.Controls.Add(Me.BtnOracle)
        Me.GroupControlLeave.Location = New System.Drawing.Point(502, 306)
        Me.GroupControlLeave.Name = "GroupControlLeave"
        Me.GroupControlLeave.Size = New System.Drawing.Size(430, 153)
        Me.GroupControlLeave.TabIndex = 34
        Me.GroupControlLeave.Text = "Leave Process"
        Me.GroupControlLeave.Visible = False
        '
        'DateToLeave
        '
        Me.DateToLeave.EditValue = Nothing
        Me.DateToLeave.Location = New System.Drawing.Point(104, 66)
        Me.DateToLeave.Name = "DateToLeave"
        Me.DateToLeave.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.DateToLeave.Properties.Appearance.Options.UseFont = True
        Me.DateToLeave.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateToLeave.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateToLeave.Size = New System.Drawing.Size(117, 20)
        Me.DateToLeave.TabIndex = 29
        '
        'LabelLeaveTo
        '
        Me.LabelLeaveTo.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelLeaveTo.Appearance.Options.UseFont = True
        Me.LabelLeaveTo.Location = New System.Drawing.Point(19, 69)
        Me.LabelLeaveTo.Name = "LabelLeaveTo"
        Me.LabelLeaveTo.Size = New System.Drawing.Size(45, 14)
        Me.LabelLeaveTo.TabIndex = 28
        Me.LabelLeaveTo.Text = "To Date"
        '
        'DateFromLeave
        '
        Me.DateFromLeave.EditValue = Nothing
        Me.DateFromLeave.Location = New System.Drawing.Point(104, 40)
        Me.DateFromLeave.Name = "DateFromLeave"
        Me.DateFromLeave.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.DateFromLeave.Properties.Appearance.Options.UseFont = True
        Me.DateFromLeave.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateFromLeave.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateFromLeave.Size = New System.Drawing.Size(117, 20)
        Me.DateFromLeave.TabIndex = 26
        '
        'LabelLeaveFrm
        '
        Me.LabelLeaveFrm.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelLeaveFrm.Appearance.Options.UseFont = True
        Me.LabelLeaveFrm.Location = New System.Drawing.Point(19, 43)
        Me.LabelLeaveFrm.Name = "LabelLeaveFrm"
        Me.LabelLeaveFrm.Size = New System.Drawing.Size(57, 14)
        Me.LabelLeaveFrm.TabIndex = 25
        Me.LabelLeaveFrm.Text = "From Date"
        '
        'BtnOracle
        '
        Me.BtnOracle.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.BtnOracle.Appearance.Options.UseFont = True
        Me.BtnOracle.Location = New System.Drawing.Point(104, 92)
        Me.BtnOracle.Name = "BtnOracle"
        Me.BtnOracle.Size = New System.Drawing.Size(75, 23)
        Me.BtnOracle.TabIndex = 6
        Me.BtnOracle.Text = "Process"
        '
        'GroupControl5
        '
        Me.GroupControl5.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl5.AppearanceCaption.ForeColor = System.Drawing.Color.Blue
        Me.GroupControl5.AppearanceCaption.Options.UseFont = True
        Me.GroupControl5.AppearanceCaption.Options.UseForeColor = True
        Me.GroupControl5.Controls.Add(Me.ComboNepaliYearVal)
        Me.GroupControl5.Controls.Add(Me.ComboNEpaliMonthVal)
        Me.GroupControl5.Controls.Add(Me.CheckEditUnMark)
        Me.GroupControl5.Controls.Add(Me.CheckEditMark)
        Me.GroupControl5.Controls.Add(Me.PanelControl1)
        Me.GroupControl5.Controls.Add(Me.DateEditVerification)
        Me.GroupControl5.Controls.Add(Me.SimpleButtonVerification)
        Me.GroupControl5.Controls.Add(Me.LabelControl1)
        Me.GroupControl5.Location = New System.Drawing.Point(21, 282)
        Me.GroupControl5.Name = "GroupControl5"
        Me.GroupControl5.Size = New System.Drawing.Size(475, 237)
        Me.GroupControl5.TabIndex = 32
        Me.GroupControl5.Text = "Verification"
        '
        'ComboNepaliYearVal
        '
        Me.ComboNepaliYearVal.Location = New System.Drawing.Point(387, 36)
        Me.ComboNepaliYearVal.Name = "ComboNepaliYearVal"
        Me.ComboNepaliYearVal.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.ComboNepaliYearVal.Properties.Appearance.Options.UseFont = True
        Me.ComboNepaliYearVal.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliYearVal.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNepaliYearVal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNepaliYearVal.Properties.Items.AddRange(New Object() {"2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037", "2038", "2039", "2040", "2041", "2042", "2043", "2044", "2045", "2046", "2047", "2048", "2049", "2050", "2051", "2052", "2053", "2054", "2055", "2056", "2057", "2058", "2059", "2060", "2061", "2062", "2063", "2064", "2065", "2066", "2067", "2068", "2069", "2070", "2071", "2072", "2073", "2074", "2075", "2076", "2077", "2078", "2079", "2080", "2081", "2082", "2083", "2084", "2085", "2086", "2087", "2088", "2089"})
        Me.ComboNepaliYearVal.Size = New System.Drawing.Size(61, 24)
        Me.ComboNepaliYearVal.TabIndex = 37
        '
        'ComboNEpaliMonthVal
        '
        Me.ComboNEpaliMonthVal.Location = New System.Drawing.Point(288, 36)
        Me.ComboNEpaliMonthVal.Name = "ComboNEpaliMonthVal"
        Me.ComboNEpaliMonthVal.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.ComboNEpaliMonthVal.Properties.Appearance.Options.UseFont = True
        Me.ComboNEpaliMonthVal.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNEpaliMonthVal.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNEpaliMonthVal.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNEpaliMonthVal.Properties.Items.AddRange(New Object() {"Baishakh", "Jestha", "Asar", "Shrawan", "Bhadau", "Aswin", "Kartik", "Mansir", "Poush", "Magh", "Falgun", "Chaitra"})
        Me.ComboNEpaliMonthVal.Size = New System.Drawing.Size(93, 24)
        Me.ComboNEpaliMonthVal.TabIndex = 36
        '
        'CheckEditUnMark
        '
        Me.CheckEditUnMark.EditValue = True
        Me.CheckEditUnMark.Location = New System.Drawing.Point(240, 124)
        Me.CheckEditUnMark.Name = "CheckEditUnMark"
        Me.CheckEditUnMark.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.CheckEditUnMark.Properties.Appearance.Options.UseFont = True
        Me.CheckEditUnMark.Properties.Caption = "UnMark"
        Me.CheckEditUnMark.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEditUnMark.Properties.RadioGroupIndex = 4
        Me.CheckEditUnMark.Size = New System.Drawing.Size(150, 22)
        Me.CheckEditUnMark.TabIndex = 20
        '
        'CheckEditMark
        '
        Me.CheckEditMark.Location = New System.Drawing.Point(240, 96)
        Me.CheckEditMark.Name = "CheckEditMark"
        Me.CheckEditMark.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.CheckEditMark.Properties.Appearance.Options.UseFont = True
        Me.CheckEditMark.Properties.Caption = "Mark"
        Me.CheckEditMark.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEditMark.Properties.RadioGroupIndex = 4
        Me.CheckEditMark.Size = New System.Drawing.Size(150, 22)
        Me.CheckEditMark.TabIndex = 19
        Me.CheckEditMark.TabStop = False
        '
        'PanelControl1
        '
        Me.PanelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.PanelControl1.Controls.Add(Me.CheckEditLate)
        Me.PanelControl1.Controls.Add(Me.CheckEditWO)
        Me.PanelControl1.Location = New System.Drawing.Point(15, 81)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(200, 77)
        Me.PanelControl1.TabIndex = 17
        '
        'CheckEditLate
        '
        Me.CheckEditLate.EditValue = True
        Me.CheckEditLate.Location = New System.Drawing.Point(8, 43)
        Me.CheckEditLate.Name = "CheckEditLate"
        Me.CheckEditLate.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.CheckEditLate.Properties.Appearance.Options.UseFont = True
        Me.CheckEditLate.Properties.Caption = "Late Verification"
        Me.CheckEditLate.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEditLate.Properties.RadioGroupIndex = 3
        Me.CheckEditLate.Size = New System.Drawing.Size(150, 22)
        Me.CheckEditLate.TabIndex = 19
        Me.CheckEditLate.Visible = False
        '
        'CheckEditWO
        '
        Me.CheckEditWO.Location = New System.Drawing.Point(8, 15)
        Me.CheckEditWO.Name = "CheckEditWO"
        Me.CheckEditWO.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.CheckEditWO.Properties.Appearance.Options.UseFont = True
        Me.CheckEditWO.Properties.Caption = "WOff Verification"
        Me.CheckEditWO.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEditWO.Properties.RadioGroupIndex = 3
        Me.CheckEditWO.Size = New System.Drawing.Size(150, 22)
        Me.CheckEditWO.TabIndex = 18
        Me.CheckEditWO.TabStop = False
        Me.CheckEditWO.Visible = False
        '
        'DateEditVerification
        '
        Me.DateEditVerification.EditValue = New Date(2018, 1, 1, 16, 13, 14, 0)
        Me.DateEditVerification.Location = New System.Drawing.Point(288, 36)
        Me.DateEditVerification.Name = "DateEditVerification"
        Me.DateEditVerification.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.DateEditVerification.Properties.Appearance.Options.UseFont = True
        Me.DateEditVerification.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEditVerification.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEditVerification.Properties.Mask.EditMask = "MM/yyyy"
        Me.DateEditVerification.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.DateEditVerification.Size = New System.Drawing.Size(135, 24)
        Me.DateEditVerification.TabIndex = 16
        '
        'SimpleButtonVerification
        '
        Me.SimpleButtonVerification.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.SimpleButtonVerification.Appearance.Options.UseFont = True
        Me.SimpleButtonVerification.Enabled = False
        Me.SimpleButtonVerification.Location = New System.Drawing.Point(316, 180)
        Me.SimpleButtonVerification.Name = "SimpleButtonVerification"
        Me.SimpleButtonVerification.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButtonVerification.TabIndex = 6
        Me.SimpleButtonVerification.Text = "OK"
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Location = New System.Drawing.Point(15, 39)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(266, 18)
        Me.LabelControl1.TabIndex = 14
        Me.LabelControl1.Text = "Processing Month and Year (MM/YYYY)"
        '
        'PopupContainerControlEmp
        '
        Me.PopupContainerControlEmp.Controls.Add(Me.GridControlEmp)
        Me.PopupContainerControlEmp.Location = New System.Drawing.Point(921, 507)
        Me.PopupContainerControlEmp.Name = "PopupContainerControlEmp"
        Me.PopupContainerControlEmp.Size = New System.Drawing.Size(300, 300)
        Me.PopupContainerControlEmp.TabIndex = 31
        '
        'GridControlEmp
        '
        Me.GridControlEmp.DataSource = Me.TblEmployeeBindingSource
        Me.GridControlEmp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControlEmp.Location = New System.Drawing.Point(0, 0)
        Me.GridControlEmp.MainView = Me.GridViewEmp
        Me.GridControlEmp.Name = "GridControlEmp"
        Me.GridControlEmp.Size = New System.Drawing.Size(300, 300)
        Me.GridControlEmp.TabIndex = 6
        Me.GridControlEmp.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewEmp})
        '
        'TblEmployeeBindingSource
        '
        Me.TblEmployeeBindingSource.DataMember = "TblEmployee"
        Me.TblEmployeeBindingSource.DataSource = Me.SSSDBDataSet
        '
        'SSSDBDataSet
        '
        Me.SSSDBDataSet.DataSetName = "SSSDBDataSet"
        Me.SSSDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GridViewEmp
        '
        Me.GridViewEmp.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colPAYCODE, Me.colEMPNAME})
        Me.GridViewEmp.GridControl = Me.GridControlEmp
        Me.GridViewEmp.Name = "GridViewEmp"
        Me.GridViewEmp.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewEmp.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewEmp.OptionsBehavior.Editable = False
        Me.GridViewEmp.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridViewEmp.OptionsSelection.MultiSelect = True
        Me.GridViewEmp.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        '
        'colPAYCODE
        '
        Me.colPAYCODE.Caption = "Paycode"
        Me.colPAYCODE.FieldName = "PAYCODE"
        Me.colPAYCODE.Name = "colPAYCODE"
        Me.colPAYCODE.Visible = True
        Me.colPAYCODE.VisibleIndex = 1
        '
        'colEMPNAME
        '
        Me.colEMPNAME.Caption = "Employee Name"
        Me.colEMPNAME.FieldName = "EMPNAME"
        Me.colEMPNAME.Name = "colEMPNAME"
        Me.colEMPNAME.Visible = True
        Me.colEMPNAME.VisibleIndex = 2
        '
        'PopupContainerControlCompany
        '
        Me.PopupContainerControlCompany.Controls.Add(Me.GridControlComp)
        Me.PopupContainerControlCompany.Location = New System.Drawing.Point(615, 510)
        Me.PopupContainerControlCompany.Name = "PopupContainerControlCompany"
        Me.PopupContainerControlCompany.Size = New System.Drawing.Size(300, 300)
        Me.PopupContainerControlCompany.TabIndex = 30
        '
        'GridControlComp
        '
        Me.GridControlComp.DataSource = Me.TblCompanyBindingSource
        Me.GridControlComp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControlComp.Location = New System.Drawing.Point(0, 0)
        Me.GridControlComp.MainView = Me.GridViewComp
        Me.GridControlComp.Name = "GridControlComp"
        Me.GridControlComp.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTimeEdit4})
        Me.GridControlComp.Size = New System.Drawing.Size(300, 300)
        Me.GridControlComp.TabIndex = 6
        Me.GridControlComp.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewComp})
        '
        'TblCompanyBindingSource
        '
        Me.TblCompanyBindingSource.DataMember = "tblCompany"
        Me.TblCompanyBindingSource.DataSource = Me.SSSDBDataSet
        '
        'GridViewComp
        '
        Me.GridViewComp.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn2})
        Me.GridViewComp.GridControl = Me.GridControlComp
        Me.GridViewComp.Name = "GridViewComp"
        Me.GridViewComp.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewComp.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewComp.OptionsBehavior.Editable = False
        Me.GridViewComp.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridViewComp.OptionsSelection.MultiSelect = True
        Me.GridViewComp.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Company Code"
        Me.GridColumn1.FieldName = "COMPANYCODE"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 1
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Company Name"
        Me.GridColumn2.FieldName = "COMPANYNAME"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 2
        '
        'RepositoryItemTimeEdit4
        '
        Me.RepositoryItemTimeEdit4.AutoHeight = False
        Me.RepositoryItemTimeEdit4.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemTimeEdit4.Mask.EditMask = "HH:mm"
        Me.RepositoryItemTimeEdit4.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemTimeEdit4.Name = "RepositoryItemTimeEdit4"
        '
        'GroupControl4
        '
        Me.GroupControl4.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl4.AppearanceCaption.ForeColor = System.Drawing.Color.Blue
        Me.GroupControl4.AppearanceCaption.Options.UseFont = True
        Me.GroupControl4.AppearanceCaption.Options.UseForeColor = True
        Me.GroupControl4.Controls.Add(Me.CheckDuplicate)
        Me.GroupControl4.Controls.Add(Me.ComboNepaliYear)
        Me.GroupControl4.Controls.Add(Me.ComboNEpaliMonth)
        Me.GroupControl4.Controls.Add(Me.ComboNepaliDate)
        Me.GroupControl4.Controls.Add(Me.LabelControl7)
        Me.GroupControl4.Controls.Add(Me.PopupContainerEdit1)
        Me.GroupControl4.Controls.Add(Me.LabelControl6)
        Me.GroupControl4.Controls.Add(Me.LabelControl5)
        Me.GroupControl4.Controls.Add(Me.ComboBoxEdit1)
        Me.GroupControl4.Controls.Add(Me.DateEdit3)
        Me.GroupControl4.Controls.Add(Me.CheckAll)
        Me.GroupControl4.Controls.Add(Me.CheckSelective)
        Me.GroupControl4.Location = New System.Drawing.Point(21, 15)
        Me.GroupControl4.Name = "GroupControl4"
        Me.GroupControl4.Size = New System.Drawing.Size(911, 109)
        Me.GroupControl4.TabIndex = 29
        Me.GroupControl4.Text = "Selection Criteria"
        '
        'CheckDuplicate
        '
        Me.CheckDuplicate.Location = New System.Drawing.Point(502, 31)
        Me.CheckDuplicate.Name = "CheckDuplicate"
        Me.CheckDuplicate.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.CheckDuplicate.Properties.Appearance.Options.UseFont = True
        Me.CheckDuplicate.Properties.Caption = "Remove Duplicate Punches"
        Me.CheckDuplicate.Size = New System.Drawing.Size(221, 22)
        Me.CheckDuplicate.TabIndex = 36
        '
        'ComboNepaliYear
        '
        Me.ComboNepaliYear.Location = New System.Drawing.Point(266, 27)
        Me.ComboNepaliYear.Name = "ComboNepaliYear"
        Me.ComboNepaliYear.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.ComboNepaliYear.Properties.Appearance.Options.UseFont = True
        Me.ComboNepaliYear.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliYear.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNepaliYear.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNepaliYear.Properties.Items.AddRange(New Object() {"2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037", "2038", "2039", "2040", "2041", "2042", "2043", "2044", "2045", "2046", "2047", "2048", "2049", "2050", "2051", "2052", "2053", "2054", "2055", "2056", "2057", "2058", "2059", "2060", "2061", "2062", "2063", "2064", "2065", "2066", "2067", "2068", "2069", "2070", "2071", "2072", "2073", "2074", "2075", "2076", "2077", "2078", "2079", "2080", "2081", "2082", "2083", "2084", "2085", "2086", "2087", "2088", "2089"})
        Me.ComboNepaliYear.Size = New System.Drawing.Size(61, 24)
        Me.ComboNepaliYear.TabIndex = 35
        '
        'ComboNEpaliMonth
        '
        Me.ComboNEpaliMonth.Location = New System.Drawing.Point(159, 27)
        Me.ComboNEpaliMonth.Name = "ComboNEpaliMonth"
        Me.ComboNEpaliMonth.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.ComboNEpaliMonth.Properties.Appearance.Options.UseFont = True
        Me.ComboNEpaliMonth.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNEpaliMonth.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNEpaliMonth.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNEpaliMonth.Properties.Items.AddRange(New Object() {"Baishakh", "Jestha", "Asar", "Shrawan", "Bhadau", "Aswin", "Kartik", "Mansir", "Poush", "Magh", "Falgun", "Chaitra"})
        Me.ComboNEpaliMonth.Size = New System.Drawing.Size(101, 24)
        Me.ComboNEpaliMonth.TabIndex = 34
        '
        'ComboNepaliDate
        '
        Me.ComboNepaliDate.Location = New System.Drawing.Point(111, 27)
        Me.ComboNepaliDate.Name = "ComboNepaliDate"
        Me.ComboNepaliDate.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.ComboNepaliDate.Properties.Appearance.Options.UseFont = True
        Me.ComboNepaliDate.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliDate.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNepaliDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNepaliDate.Properties.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32"})
        Me.ComboNepaliDate.Size = New System.Drawing.Size(42, 24)
        Me.ComboNepaliDate.TabIndex = 33
        '
        'LabelControl7
        '
        Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.LabelControl7.Appearance.Options.UseFont = True
        Me.LabelControl7.Location = New System.Drawing.Point(15, 74)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(77, 18)
        Me.LabelControl7.TabIndex = 26
        Me.LabelControl7.Text = "Select Type"
        '
        'PopupContainerEdit1
        '
        Me.PopupContainerEdit1.Location = New System.Drawing.Point(455, 71)
        Me.PopupContainerEdit1.Name = "PopupContainerEdit1"
        Me.PopupContainerEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.PopupContainerEdit1.Properties.Appearance.Options.UseFont = True
        Me.PopupContainerEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.PopupContainerEdit1.Size = New System.Drawing.Size(165, 24)
        Me.PopupContainerEdit1.TabIndex = 25
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.LabelControl6.Appearance.Options.UseFont = True
        Me.LabelControl6.Location = New System.Drawing.Point(15, 29)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(70, 18)
        Me.LabelControl6.TabIndex = 16
        Me.LabelControl6.Text = "From Date"
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.LabelControl5.Appearance.Options.UseFont = True
        Me.LabelControl5.Location = New System.Drawing.Point(316, 74)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(98, 18)
        Me.LabelControl5.TabIndex = 21
        Me.LabelControl5.Text = "Select Paycode"
        '
        'ComboBoxEdit1
        '
        Me.ComboBoxEdit1.EditValue = "Paycode"
        Me.ComboBoxEdit1.Location = New System.Drawing.Point(112, 71)
        Me.ComboBoxEdit1.Name = "ComboBoxEdit1"
        Me.ComboBoxEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.ComboBoxEdit1.Properties.Appearance.Options.UseFont = True
        Me.ComboBoxEdit1.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.ComboBoxEdit1.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboBoxEdit1.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.ComboBoxEdit1.Properties.AppearanceFocused.Options.UseFont = True
        Me.ComboBoxEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEdit1.Properties.Items.AddRange(New Object() {"Paycode", "Company", "Location", "Department"})
        Me.ComboBoxEdit1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.ComboBoxEdit1.Size = New System.Drawing.Size(135, 24)
        Me.ComboBoxEdit1.TabIndex = 24
        '
        'DateEdit3
        '
        Me.DateEdit3.EditValue = New Date(2018, 1, 1, 16, 13, 14, 0)
        Me.DateEdit3.Location = New System.Drawing.Point(111, 27)
        Me.DateEdit3.Name = "DateEdit3"
        Me.DateEdit3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.DateEdit3.Properties.Appearance.Options.UseFont = True
        Me.DateEdit3.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit3.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit3.Properties.Mask.EditMask = "dd/MM/yyyy"
        Me.DateEdit3.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.DateEdit3.Size = New System.Drawing.Size(135, 24)
        Me.DateEdit3.TabIndex = 15
        '
        'CheckAll
        '
        Me.CheckAll.EditValue = True
        Me.CheckAll.Location = New System.Drawing.Point(343, 29)
        Me.CheckAll.Name = "CheckAll"
        Me.CheckAll.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.CheckAll.Properties.Appearance.Options.UseFont = True
        Me.CheckAll.Properties.Caption = "All"
        Me.CheckAll.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckAll.Properties.RadioGroupIndex = 2
        Me.CheckAll.Size = New System.Drawing.Size(45, 22)
        Me.CheckAll.TabIndex = 17
        '
        'CheckSelective
        '
        Me.CheckSelective.Location = New System.Drawing.Point(397, 31)
        Me.CheckSelective.Name = "CheckSelective"
        Me.CheckSelective.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.CheckSelective.Properties.Appearance.Options.UseFont = True
        Me.CheckSelective.Properties.Caption = "Selective"
        Me.CheckSelective.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckSelective.Properties.RadioGroupIndex = 2
        Me.CheckSelective.Size = New System.Drawing.Size(99, 22)
        Me.CheckSelective.TabIndex = 18
        Me.CheckSelective.TabStop = False
        '
        'PopupContainerControlLocation
        '
        Me.PopupContainerControlLocation.Controls.Add(Me.GridControlLocation)
        Me.PopupContainerControlLocation.Location = New System.Drawing.Point(309, 507)
        Me.PopupContainerControlLocation.Name = "PopupContainerControlLocation"
        Me.PopupContainerControlLocation.Size = New System.Drawing.Size(300, 300)
        Me.PopupContainerControlLocation.TabIndex = 28
        '
        'GridControlLocation
        '
        Me.GridControlLocation.DataSource = Me.TblbranchBindingSource
        Me.GridControlLocation.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControlLocation.Location = New System.Drawing.Point(0, 0)
        Me.GridControlLocation.MainView = Me.GridViewLocation
        Me.GridControlLocation.Name = "GridControlLocation"
        Me.GridControlLocation.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTimeEdit3})
        Me.GridControlLocation.Size = New System.Drawing.Size(300, 300)
        Me.GridControlLocation.TabIndex = 6
        Me.GridControlLocation.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewLocation})
        '
        'TblbranchBindingSource
        '
        Me.TblbranchBindingSource.DataMember = "tblbranch"
        Me.TblbranchBindingSource.DataSource = Me.SSSDBDataSet
        '
        'GridViewLocation
        '
        Me.GridViewLocation.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colBRANCHCODE, Me.colBRANCHNAME})
        Me.GridViewLocation.GridControl = Me.GridControlLocation
        Me.GridViewLocation.Name = "GridViewLocation"
        Me.GridViewLocation.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewLocation.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewLocation.OptionsBehavior.Editable = False
        Me.GridViewLocation.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridViewLocation.OptionsSelection.MultiSelect = True
        Me.GridViewLocation.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        '
        'colBRANCHCODE
        '
        Me.colBRANCHCODE.Caption = "Location Code"
        Me.colBRANCHCODE.FieldName = "BRANCHCODE"
        Me.colBRANCHCODE.Name = "colBRANCHCODE"
        Me.colBRANCHCODE.Visible = True
        Me.colBRANCHCODE.VisibleIndex = 1
        '
        'colBRANCHNAME
        '
        Me.colBRANCHNAME.Caption = "Location Name"
        Me.colBRANCHNAME.FieldName = "BRANCHNAME"
        Me.colBRANCHNAME.Name = "colBRANCHNAME"
        Me.colBRANCHNAME.Visible = True
        Me.colBRANCHNAME.VisibleIndex = 2
        '
        'RepositoryItemTimeEdit3
        '
        Me.RepositoryItemTimeEdit3.AutoHeight = False
        Me.RepositoryItemTimeEdit3.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemTimeEdit3.Mask.EditMask = "HH:mm"
        Me.RepositoryItemTimeEdit3.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemTimeEdit3.Name = "RepositoryItemTimeEdit3"
        '
        'PopupContainerControlDept
        '
        Me.PopupContainerControlDept.Controls.Add(Me.GridControlDept)
        Me.PopupContainerControlDept.Location = New System.Drawing.Point(3, 510)
        Me.PopupContainerControlDept.Name = "PopupContainerControlDept"
        Me.PopupContainerControlDept.Size = New System.Drawing.Size(300, 300)
        Me.PopupContainerControlDept.TabIndex = 27
        '
        'GridControlDept
        '
        Me.GridControlDept.DataSource = Me.TblDepartmentBindingSource
        Me.GridControlDept.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControlDept.Location = New System.Drawing.Point(0, 0)
        Me.GridControlDept.MainView = Me.GridViewDept
        Me.GridControlDept.Name = "GridControlDept"
        Me.GridControlDept.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTimeEdit2})
        Me.GridControlDept.Size = New System.Drawing.Size(300, 300)
        Me.GridControlDept.TabIndex = 6
        Me.GridControlDept.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewDept})
        '
        'TblDepartmentBindingSource
        '
        Me.TblDepartmentBindingSource.DataMember = "tblDepartment"
        Me.TblDepartmentBindingSource.DataSource = Me.SSSDBDataSet
        '
        'GridViewDept
        '
        Me.GridViewDept.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colDEPARTMENTCODE, Me.colDEPARTMENTNAME})
        Me.GridViewDept.GridControl = Me.GridControlDept
        Me.GridViewDept.Name = "GridViewDept"
        Me.GridViewDept.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewDept.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewDept.OptionsBehavior.Editable = False
        Me.GridViewDept.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridViewDept.OptionsSelection.MultiSelect = True
        Me.GridViewDept.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        '
        'colDEPARTMENTCODE
        '
        Me.colDEPARTMENTCODE.Caption = "Department Code"
        Me.colDEPARTMENTCODE.FieldName = "DEPARTMENTCODE"
        Me.colDEPARTMENTCODE.Name = "colDEPARTMENTCODE"
        Me.colDEPARTMENTCODE.Visible = True
        Me.colDEPARTMENTCODE.VisibleIndex = 1
        '
        'colDEPARTMENTNAME
        '
        Me.colDEPARTMENTNAME.Caption = "Department Name"
        Me.colDEPARTMENTNAME.FieldName = "DEPARTMENTNAME"
        Me.colDEPARTMENTNAME.Name = "colDEPARTMENTNAME"
        Me.colDEPARTMENTNAME.Visible = True
        Me.colDEPARTMENTNAME.VisibleIndex = 2
        '
        'RepositoryItemTimeEdit2
        '
        Me.RepositoryItemTimeEdit2.AutoHeight = False
        Me.RepositoryItemTimeEdit2.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemTimeEdit2.Mask.EditMask = "HH:mm"
        Me.RepositoryItemTimeEdit2.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemTimeEdit2.Name = "RepositoryItemTimeEdit2"
        '
        'GroupControl3
        '
        Me.GroupControl3.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl3.AppearanceCaption.ForeColor = System.Drawing.Color.Blue
        Me.GroupControl3.AppearanceCaption.Options.UseFont = True
        Me.GroupControl3.AppearanceCaption.Options.UseForeColor = True
        Me.GroupControl3.Controls.Add(Me.SimpleButton2)
        Me.GroupControl3.Location = New System.Drawing.Point(327, 145)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(300, 125)
        Me.GroupControl3.TabIndex = 2
        Me.GroupControl3.Text = "Attendance Roster Updation"
        '
        'SimpleButton2
        '
        Me.SimpleButton2.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.SimpleButton2.Appearance.Options.UseFont = True
        Me.SimpleButton2.Location = New System.Drawing.Point(27, 43)
        Me.SimpleButton2.Name = "SimpleButton2"
        Me.SimpleButton2.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton2.TabIndex = 6
        Me.SimpleButton2.Text = "Update"
        '
        'GroupControl2
        '
        Me.GroupControl2.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl2.AppearanceCaption.ForeColor = System.Drawing.Color.Blue
        Me.GroupControl2.AppearanceCaption.Options.UseFont = True
        Me.GroupControl2.AppearanceCaption.Options.UseForeColor = True
        Me.GroupControl2.Controls.Add(Me.LabelControl4)
        Me.GroupControl2.Controls.Add(Me.ToggleEmp)
        Me.GroupControl2.Controls.Add(Me.LookUpEditDevice)
        Me.GroupControl2.Controls.Add(Me.LabelControl3)
        Me.GroupControl2.Controls.Add(Me.TextEditUploadLogs)
        Me.GroupControl2.Controls.Add(Me.SimpleButtonProcess)
        Me.GroupControl2.Location = New System.Drawing.Point(633, 145)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(299, 155)
        Me.GroupControl2.TabIndex = 1
        Me.GroupControl2.Text = "Back Day Processing"
        '
        'ToggleEmp
        '
        Me.ToggleEmp.Location = New System.Drawing.Point(166, 116)
        Me.ToggleEmp.Name = "ToggleEmp"
        Me.ToggleEmp.Properties.OffText = "Off"
        Me.ToggleEmp.Properties.OnText = "On"
        Me.ToggleEmp.Size = New System.Drawing.Size(95, 24)
        Me.ToggleEmp.TabIndex = 24
        '
        'LookUpEditDevice
        '
        Me.LookUpEditDevice.Location = New System.Drawing.Point(119, 86)
        Me.LookUpEditDevice.Name = "LookUpEditDevice"
        Me.LookUpEditDevice.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LookUpEditDevice.Properties.Appearance.Options.UseFont = True
        Me.LookUpEditDevice.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LookUpEditDevice.Properties.AppearanceDropDown.Options.UseFont = True
        Me.LookUpEditDevice.Properties.AppearanceDropDownHeader.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LookUpEditDevice.Properties.AppearanceDropDownHeader.Options.UseFont = True
        Me.LookUpEditDevice.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LookUpEditDevice.Properties.AppearanceFocused.Options.UseFont = True
        Me.LookUpEditDevice.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.LookUpEditDevice.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.LookUpEditDevice.Properties.Columns.AddRange(New DevExpress.XtraEditors.Controls.LookUpColumnInfo() {New DevExpress.XtraEditors.Controls.LookUpColumnInfo("ID_NO", "DeviceId", 30, DevExpress.Utils.FormatType.None, "", True, DevExpress.Utils.HorzAlignment.Near), New DevExpress.XtraEditors.Controls.LookUpColumnInfo("branch", "Location")})
        Me.LookUpEditDevice.Properties.DisplayMember = "ID_NO"
        Me.LookUpEditDevice.Properties.MaxLength = 12
        Me.LookUpEditDevice.Properties.NullText = ""
        Me.LookUpEditDevice.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.Standard
        Me.LookUpEditDevice.Properties.ValueMember = "ID_NO"
        Me.LookUpEditDevice.Size = New System.Drawing.Size(175, 20)
        Me.LookUpEditDevice.TabIndex = 33
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl3.Appearance.Options.UseFont = True
        Me.LabelControl3.Location = New System.Drawing.Point(7, 121)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(133, 14)
        Me.LabelControl3.TabIndex = 23
        Me.LabelControl3.Text = "Create Employee Master"
        '
        'TextEditUploadLogs
        '
        Me.TextEditUploadLogs.Location = New System.Drawing.Point(5, 60)
        Me.TextEditUploadLogs.Name = "TextEditUploadLogs"
        Me.TextEditUploadLogs.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditUploadLogs.Properties.Appearance.Options.UseFont = True
        Me.TextEditUploadLogs.Size = New System.Drawing.Size(289, 20)
        Me.TextEditUploadLogs.TabIndex = 22
        '
        'SimpleButtonProcess
        '
        Me.SimpleButtonProcess.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.SimpleButtonProcess.Appearance.Options.UseFont = True
        Me.SimpleButtonProcess.Location = New System.Drawing.Point(207, 31)
        Me.SimpleButtonProcess.Name = "SimpleButtonProcess"
        Me.SimpleButtonProcess.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButtonProcess.TabIndex = 21
        Me.SimpleButtonProcess.Text = "Process"
        '
        'GroupControl1
        '
        Me.GroupControl1.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl1.AppearanceCaption.ForeColor = System.Drawing.Color.Blue
        Me.GroupControl1.AppearanceCaption.Options.UseFont = True
        Me.GroupControl1.AppearanceCaption.Options.UseForeColor = True
        Me.GroupControl1.Controls.Add(Me.SimpleButton1)
        Me.GroupControl1.Controls.Add(Me.TextEdit1)
        Me.GroupControl1.Controls.Add(Me.LabelControl2)
        Me.GroupControl1.Location = New System.Drawing.Point(21, 145)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(300, 125)
        Me.GroupControl1.TabIndex = 0
        Me.GroupControl1.Text = "Attendance Roster Creation"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.SimpleButton1.Appearance.Options.UseFont = True
        Me.SimpleButton1.Location = New System.Drawing.Point(23, 43)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton1.TabIndex = 6
        Me.SimpleButton1.Text = "Create"
        '
        'TextEdit1
        '
        Me.TextEdit1.Location = New System.Drawing.Point(94, 134)
        Me.TextEdit1.Name = "TextEdit1"
        Me.TextEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.TextEdit1.Properties.Appearance.Options.UseFont = True
        Me.TextEdit1.Properties.MaxLength = 12
        Me.TextEdit1.Size = New System.Drawing.Size(135, 24)
        Me.TextEdit1.TabIndex = 5
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 11.0!)
        Me.LabelControl2.Appearance.Options.UseFont = True
        Me.LabelControl2.Location = New System.Drawing.Point(23, 137)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(62, 18)
        Me.LabelControl2.TabIndex = 14
        Me.LabelControl2.Text = "Pay Code"
        '
        'MemoEdit1
        '
        Me.MemoEdit1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.MemoEdit1.EditValue = resources.GetString("MemoEdit1.EditValue")
        Me.MemoEdit1.Location = New System.Drawing.Point(0, 0)
        Me.MemoEdit1.Name = "MemoEdit1"
        Me.MemoEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.MemoEdit1.Properties.Appearance.Options.UseFont = True
        Me.MemoEdit1.Properties.MaxLength = 100000
        Me.MemoEdit1.Properties.ReadOnly = True
        Me.MemoEdit1.Size = New System.Drawing.Size(77, 568)
        Me.MemoEdit1.TabIndex = 4
        '
        'TblCompanyTableAdapter
        '
        Me.TblCompanyTableAdapter.ClearBeforeFill = True
        '
        'TblCompany1TableAdapter1
        '
        Me.TblCompany1TableAdapter1.ClearBeforeFill = True
        '
        'TblDepartmentTableAdapter
        '
        Me.TblDepartmentTableAdapter.ClearBeforeFill = True
        '
        'TblDepartment1TableAdapter1
        '
        Me.TblDepartment1TableAdapter1.ClearBeforeFill = True
        '
        'TblbranchTableAdapter
        '
        Me.TblbranchTableAdapter.ClearBeforeFill = True
        '
        'Tblbranch1TableAdapter1
        '
        Me.Tblbranch1TableAdapter1.ClearBeforeFill = True
        '
        'GridControl1
        '
        Me.GridControl1.DataSource = Me.TblCompanyBindingSource
        Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControl1.Location = New System.Drawing.Point(0, 0)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(300, 300)
        Me.GridControl1.TabIndex = 6
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colCOMPANYCODE1, Me.colCOMPANYNAME1, Me.colCOMPANYADDRESS, Me.colSHORTNAME, Me.colPANNUM, Me.colTANNUMBER, Me.colTDSCIRCLE, Me.colLCNO, Me.colPFNO, Me.colLastModifiedBy, Me.colLastModifiedDate, Me.colGSTIN})
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.Editable = False
        Me.GridView1.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridView1.OptionsSelection.MultiSelect = True
        Me.GridView1.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        '
        'colCOMPANYCODE1
        '
        Me.colCOMPANYCODE1.FieldName = "COMPANYCODE"
        Me.colCOMPANYCODE1.Name = "colCOMPANYCODE1"
        Me.colCOMPANYCODE1.Visible = True
        Me.colCOMPANYCODE1.VisibleIndex = 1
        '
        'colCOMPANYNAME1
        '
        Me.colCOMPANYNAME1.FieldName = "COMPANYNAME"
        Me.colCOMPANYNAME1.Name = "colCOMPANYNAME1"
        Me.colCOMPANYNAME1.Visible = True
        Me.colCOMPANYNAME1.VisibleIndex = 2
        '
        'colCOMPANYADDRESS
        '
        Me.colCOMPANYADDRESS.FieldName = "COMPANYADDRESS"
        Me.colCOMPANYADDRESS.Name = "colCOMPANYADDRESS"
        Me.colCOMPANYADDRESS.Visible = True
        Me.colCOMPANYADDRESS.VisibleIndex = 3
        '
        'colSHORTNAME
        '
        Me.colSHORTNAME.FieldName = "SHORTNAME"
        Me.colSHORTNAME.Name = "colSHORTNAME"
        Me.colSHORTNAME.Visible = True
        Me.colSHORTNAME.VisibleIndex = 4
        '
        'colPANNUM
        '
        Me.colPANNUM.FieldName = "PANNUM"
        Me.colPANNUM.Name = "colPANNUM"
        Me.colPANNUM.Visible = True
        Me.colPANNUM.VisibleIndex = 5
        '
        'colTANNUMBER
        '
        Me.colTANNUMBER.FieldName = "TANNUMBER"
        Me.colTANNUMBER.Name = "colTANNUMBER"
        Me.colTANNUMBER.Visible = True
        Me.colTANNUMBER.VisibleIndex = 6
        '
        'colTDSCIRCLE
        '
        Me.colTDSCIRCLE.FieldName = "TDSCIRCLE"
        Me.colTDSCIRCLE.Name = "colTDSCIRCLE"
        Me.colTDSCIRCLE.Visible = True
        Me.colTDSCIRCLE.VisibleIndex = 7
        '
        'colLCNO
        '
        Me.colLCNO.FieldName = "LCNO"
        Me.colLCNO.Name = "colLCNO"
        Me.colLCNO.Visible = True
        Me.colLCNO.VisibleIndex = 8
        '
        'colPFNO
        '
        Me.colPFNO.FieldName = "PFNO"
        Me.colPFNO.Name = "colPFNO"
        Me.colPFNO.Visible = True
        Me.colPFNO.VisibleIndex = 9
        '
        'colLastModifiedBy
        '
        Me.colLastModifiedBy.FieldName = "LastModifiedBy"
        Me.colLastModifiedBy.Name = "colLastModifiedBy"
        Me.colLastModifiedBy.Visible = True
        Me.colLastModifiedBy.VisibleIndex = 10
        '
        'colLastModifiedDate
        '
        Me.colLastModifiedDate.FieldName = "LastModifiedDate"
        Me.colLastModifiedDate.Name = "colLastModifiedDate"
        Me.colLastModifiedDate.Visible = True
        Me.colLastModifiedDate.VisibleIndex = 11
        '
        'colGSTIN
        '
        Me.colGSTIN.FieldName = "GSTIN"
        Me.colGSTIN.Name = "colGSTIN"
        Me.colGSTIN.Visible = True
        Me.colGSTIN.VisibleIndex = 12
        '
        'RepositoryItemTimeEdit1
        '
        Me.RepositoryItemTimeEdit1.AutoHeight = False
        Me.RepositoryItemTimeEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemTimeEdit1.Mask.EditMask = "HH:mm"
        Me.RepositoryItemTimeEdit1.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemTimeEdit1.Name = "RepositoryItemTimeEdit1"
        '
        'colCOMPANYCODE
        '
        Me.colCOMPANYCODE.FieldName = "COMPANYCODE"
        Me.colCOMPANYCODE.Name = "colCOMPANYCODE"
        Me.colCOMPANYCODE.Visible = True
        Me.colCOMPANYCODE.VisibleIndex = 1
        '
        'colCOMPANYNAME
        '
        Me.colCOMPANYNAME.FieldName = "COMPANYNAME"
        Me.colCOMPANYNAME.Name = "colCOMPANYNAME"
        Me.colCOMPANYNAME.Visible = True
        Me.colCOMPANYNAME.VisibleIndex = 2
        '
        'TblEmployeeTableAdapter
        '
        Me.TblEmployeeTableAdapter.ClearBeforeFill = True
        '
        'TblEmployee1TableAdapter1
        '
        Me.TblEmployee1TableAdapter1.ClearBeforeFill = True
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl4.Appearance.Options.UseFont = True
        Me.LabelControl4.Location = New System.Drawing.Point(7, 89)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(74, 14)
        Me.LabelControl4.TabIndex = 35
        Me.LabelControl4.Text = "Select Device"
        '
        'XtraDataProcess
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.SplitContainerControl1)
        Me.LookAndFeel.SkinName = "iMaginary"
        Me.LookAndFeel.UseDefaultLookAndFeel = False
        Me.Name = "XtraDataProcess"
        Me.Size = New System.Drawing.Size(1250, 568)
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerControl1.ResumeLayout(False)
        CType(Me.GroupControlLeave, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControlLeave.ResumeLayout(False)
        Me.GroupControlLeave.PerformLayout()
        CType(Me.DateToLeave.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateToLeave.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateFromLeave.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateFromLeave.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl5.ResumeLayout(False)
        Me.GroupControl5.PerformLayout()
        CType(Me.ComboNepaliYearVal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboNEpaliMonthVal.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditUnMark.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditMark.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        CType(Me.CheckEditLate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditWO.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEditVerification.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEditVerification.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerControlEmp, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PopupContainerControlEmp.ResumeLayout(False)
        CType(Me.GridControlEmp, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblEmployeeBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewEmp, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerControlCompany, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PopupContainerControlCompany.ResumeLayout(False)
        CType(Me.GridControlComp, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblCompanyBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewComp, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTimeEdit4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl4.ResumeLayout(False)
        Me.GroupControl4.PerformLayout()
        CType(Me.CheckDuplicate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboNepaliYear.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboNEpaliMonth.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboNepaliDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit3.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckAll.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckSelective.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerControlLocation, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PopupContainerControlLocation.ResumeLayout(False)
        CType(Me.GridControlLocation, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblbranchBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewLocation, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTimeEdit3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerControlDept, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PopupContainerControlDept.ResumeLayout(False)
        CType(Me.GridControlDept, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblDepartmentBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewDept, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTimeEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        Me.GroupControl2.PerformLayout()
        CType(Me.ToggleEmp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LookUpEditDevice.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditUploadLogs.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        Me.GroupControl1.PerformLayout()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTimeEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SplitContainerControl1 As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents MemoEdit1 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents TextEdit1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents SimpleButton2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButtonProcess As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents CheckSelective As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckAll As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents DateEdit3 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ComboBoxEdit1 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents PopupContainerEdit1 As DevExpress.XtraEditors.PopupContainerEdit
    Friend WithEvents TblCompanyBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SSSDBDataSet As iAS.SSSDBDataSet
    Friend WithEvents TblCompanyTableAdapter As iAS.SSSDBDataSetTableAdapters.tblCompanyTableAdapter
    Friend WithEvents TblCompany1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.tblCompany1TableAdapter
    Friend WithEvents PopupContainerControlDept As DevExpress.XtraEditors.PopupContainerControl
    Friend WithEvents GridControlDept As DevExpress.XtraGrid.GridControl
    Friend WithEvents TblDepartmentBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents GridViewDept As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colDEPARTMENTCODE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDEPARTMENTNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemTimeEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit
    Friend WithEvents TblDepartmentTableAdapter As iAS.SSSDBDataSetTableAdapters.tblDepartmentTableAdapter
    Friend WithEvents TblDepartment1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.tblDepartment1TableAdapter
    Friend WithEvents PopupContainerControlLocation As DevExpress.XtraEditors.PopupContainerControl
    Friend WithEvents GridControlLocation As DevExpress.XtraGrid.GridControl
    Friend WithEvents TblbranchBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents GridViewLocation As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colBRANCHCODE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBRANCHNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemTimeEdit3 As DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit
    Friend WithEvents TblbranchTableAdapter As iAS.SSSDBDataSetTableAdapters.tblbranchTableAdapter
    Friend WithEvents Tblbranch1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.tblbranch1TableAdapter
    Friend WithEvents GroupControl4 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents RepositoryItemTimeEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit
    Friend WithEvents colCOMPANYCODE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCOMPANYNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCOMPANYCODE1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCOMPANYNAME1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCOMPANYADDRESS As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSHORTNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPANNUM As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colTANNUMBER As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colTDSCIRCLE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLCNO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPFNO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLastModifiedBy As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLastModifiedDate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colGSTIN As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents PopupContainerControlCompany As DevExpress.XtraEditors.PopupContainerControl
    Friend WithEvents GridControlComp As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridViewComp As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemTimeEdit4 As DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit
    Friend WithEvents PopupContainerControlEmp As DevExpress.XtraEditors.PopupContainerControl
    Friend WithEvents GridControlEmp As DevExpress.XtraGrid.GridControl
    Friend WithEvents TblEmployeeBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents GridViewEmp As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colPAYCODE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colEMPNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents TblEmployeeTableAdapter As iAS.SSSDBDataSetTableAdapters.TblEmployeeTableAdapter
    Friend WithEvents TblEmployee1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.TblEmployee1TableAdapter
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GroupControl5 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents DateEditVerification As DevExpress.XtraEditors.DateEdit
    Friend WithEvents SimpleButtonVerification As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents CheckEditWO As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditLate As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditUnMark As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditMark As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents TextEditUploadLogs As DevExpress.XtraEditors.TextEdit
    Friend WithEvents ComboNepaliYear As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboNEpaliMonth As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboNepaliDate As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboNepaliYearVal As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboNEpaliMonthVal As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ToggleEmp As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents CheckDuplicate As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents GroupControlLeave As DevExpress.XtraEditors.GroupControl
    Friend WithEvents DateToLeave As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelLeaveTo As DevExpress.XtraEditors.LabelControl
    Friend WithEvents DateFromLeave As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelLeaveFrm As DevExpress.XtraEditors.LabelControl
    Friend WithEvents BtnOracle As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LookUpEditDevice As DevExpress.XtraEditors.LookUpEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
End Class
