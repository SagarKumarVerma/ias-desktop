﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraCompanyInfo
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(XtraCompanyInfo))
        Me.MemoEditAddress = New DevExpress.XtraEditors.MemoEdit()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.SimpleButtonVerify = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButtonSave = New DevExpress.XtraEditors.SimpleButton()
        Me.TextEditLicense = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditUserKey = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditEmail = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditContact = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditCompanyName = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.SimpleButtonClose = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditUserNo = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditDeviceNO = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditUpload = New DevExpress.XtraEditors.TextEdit()
        Me.SimpleButtonUpload = New DevExpress.XtraEditors.SimpleButton()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.LabelControl27 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl19 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl18 = New DevExpress.XtraEditors.LabelControl()
        Me.SimpleButtonDownload = New DevExpress.XtraEditors.SimpleButton()
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
        Me.LabelControl20 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditCity = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl21 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl22 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditState = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl23 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl24 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditPincode = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl25 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl26 = New DevExpress.XtraEditors.LabelControl()
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl()
        Me.HyperlinkLabelControl1 = New DevExpress.XtraEditors.HyperlinkLabelControl()
        CType(Me.MemoEditAddress.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditLicense.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditUserKey.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditEmail.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditContact.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditCompanyName.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditUserNo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditDeviceNO.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditUpload.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.TextEditCity.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditState.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditPincode.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl2.SuspendLayout()
        Me.SuspendLayout()
        '
        'MemoEditAddress
        '
        Me.MemoEditAddress.Location = New System.Drawing.Point(166, 50)
        Me.MemoEditAddress.Name = "MemoEditAddress"
        Me.MemoEditAddress.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.MemoEditAddress.Properties.Appearance.Options.UseFont = True
        Me.MemoEditAddress.Properties.MaxLength = 250
        Me.MemoEditAddress.Size = New System.Drawing.Size(304, 64)
        Me.MemoEditAddress.TabIndex = 2
        '
        'LabelControl11
        '
        Me.LabelControl11.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl11.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl11.Appearance.Options.UseFont = True
        Me.LabelControl11.Appearance.Options.UseForeColor = True
        Me.LabelControl11.Location = New System.Drawing.Point(66, 253)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(7, 14)
        Me.LabelControl11.TabIndex = 37
        Me.LabelControl11.Text = "*"
        Me.LabelControl11.Visible = False
        '
        'LabelControl10
        '
        Me.LabelControl10.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl10.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl10.Appearance.Options.UseFont = True
        Me.LabelControl10.Appearance.Options.UseForeColor = True
        Me.LabelControl10.Location = New System.Drawing.Point(60, 227)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(7, 14)
        Me.LabelControl10.TabIndex = 36
        Me.LabelControl10.Text = "*"
        '
        'LabelControl9
        '
        Me.LabelControl9.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl9.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl9.Appearance.Options.UseFont = True
        Me.LabelControl9.Appearance.Options.UseForeColor = True
        Me.LabelControl9.Location = New System.Drawing.Point(80, 201)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(7, 14)
        Me.LabelControl9.TabIndex = 35
        Me.LabelControl9.Text = "*"
        '
        'LabelControl8
        '
        Me.LabelControl8.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl8.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl8.Appearance.Options.UseFont = True
        Me.LabelControl8.Appearance.Options.UseForeColor = True
        Me.LabelControl8.Location = New System.Drawing.Point(61, 52)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(7, 14)
        Me.LabelControl8.TabIndex = 34
        Me.LabelControl8.Text = "*"
        '
        'LabelControl7
        '
        Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl7.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl7.Appearance.Options.UseFont = True
        Me.LabelControl7.Appearance.Options.UseForeColor = True
        Me.LabelControl7.Location = New System.Drawing.Point(106, 26)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(7, 14)
        Me.LabelControl7.TabIndex = 33
        Me.LabelControl7.Text = "*"
        '
        'SimpleButtonVerify
        '
        Me.SimpleButtonVerify.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonVerify.Appearance.Options.UseFont = True
        Me.SimpleButtonVerify.Location = New System.Drawing.Point(154, 6)
        Me.SimpleButtonVerify.Name = "SimpleButtonVerify"
        Me.SimpleButtonVerify.Size = New System.Drawing.Size(156, 23)
        Me.SimpleButtonVerify.TabIndex = 15
        Me.SimpleButtonVerify.Text = "Request For Activation"
        Me.SimpleButtonVerify.UseWaitCursor = True
        '
        'SimpleButtonSave
        '
        Me.SimpleButtonSave.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonSave.Appearance.Options.UseFont = True
        Me.SimpleButtonSave.Location = New System.Drawing.Point(166, 480)
        Me.SimpleButtonSave.Name = "SimpleButtonSave"
        Me.SimpleButtonSave.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButtonSave.TabIndex = 13
        Me.SimpleButtonSave.Text = "Save"
        '
        'TextEditLicense
        '
        Me.TextEditLicense.Location = New System.Drawing.Point(166, 276)
        Me.TextEditLicense.Name = "TextEditLicense"
        Me.TextEditLicense.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditLicense.Properties.Appearance.Options.UseFont = True
        Me.TextEditLicense.Properties.MaxLength = 50
        Me.TextEditLicense.Size = New System.Drawing.Size(304, 20)
        Me.TextEditLicense.TabIndex = 9
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl6.Appearance.Options.UseFont = True
        Me.LabelControl6.Location = New System.Drawing.Point(12, 278)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(40, 14)
        Me.LabelControl6.TabIndex = 32
        Me.LabelControl6.Text = "License"
        '
        'TextEditUserKey
        '
        Me.TextEditUserKey.Location = New System.Drawing.Point(166, 250)
        Me.TextEditUserKey.Name = "TextEditUserKey"
        Me.TextEditUserKey.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditUserKey.Properties.Appearance.Options.UseFont = True
        Me.TextEditUserKey.Properties.MaxLength = 50
        Me.TextEditUserKey.Properties.ReadOnly = True
        Me.TextEditUserKey.Size = New System.Drawing.Size(304, 20)
        Me.TextEditUserKey.TabIndex = 8
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl5.Appearance.Options.UseFont = True
        Me.LabelControl5.Location = New System.Drawing.Point(12, 252)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(48, 14)
        Me.LabelControl5.TabIndex = 29
        Me.LabelControl5.Text = "User Key"
        '
        'TextEditEmail
        '
        Me.TextEditEmail.Location = New System.Drawing.Point(166, 224)
        Me.TextEditEmail.Name = "TextEditEmail"
        Me.TextEditEmail.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditEmail.Properties.Appearance.Options.UseFont = True
        Me.TextEditEmail.Properties.MaxLength = 50
        Me.TextEditEmail.Size = New System.Drawing.Size(304, 20)
        Me.TextEditEmail.TabIndex = 7
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl4.Appearance.Options.UseFont = True
        Me.LabelControl4.Location = New System.Drawing.Point(12, 226)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(42, 14)
        Me.LabelControl4.TabIndex = 26
        Me.LabelControl4.Text = "Email Id"
        '
        'TextEditContact
        '
        Me.TextEditContact.Location = New System.Drawing.Point(166, 198)
        Me.TextEditContact.Name = "TextEditContact"
        Me.TextEditContact.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditContact.Properties.Appearance.Options.UseFont = True
        Me.TextEditContact.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEditContact.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEditContact.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditContact.Properties.MaxLength = 50
        Me.TextEditContact.Size = New System.Drawing.Size(304, 20)
        Me.TextEditContact.TabIndex = 6
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl3.Appearance.Options.UseFont = True
        Me.LabelControl3.Location = New System.Drawing.Point(12, 200)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(62, 14)
        Me.LabelControl3.TabIndex = 23
        Me.LabelControl3.Text = "Contact No"
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl2.Appearance.Options.UseFont = True
        Me.LabelControl2.Location = New System.Drawing.Point(12, 51)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(43, 14)
        Me.LabelControl2.TabIndex = 31
        Me.LabelControl2.Text = "Address"
        '
        'TextEditCompanyName
        '
        Me.TextEditCompanyName.Location = New System.Drawing.Point(166, 23)
        Me.TextEditCompanyName.Name = "TextEditCompanyName"
        Me.TextEditCompanyName.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditCompanyName.Properties.Appearance.Options.UseFont = True
        Me.TextEditCompanyName.Properties.MaxLength = 50
        Me.TextEditCompanyName.Size = New System.Drawing.Size(304, 20)
        Me.TextEditCompanyName.TabIndex = 1
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Location = New System.Drawing.Point(13, 26)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(87, 14)
        Me.LabelControl1.TabIndex = 19
        Me.LabelControl1.Text = "Customer Name"
        '
        'LabelControl12
        '
        Me.LabelControl12.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl12.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl12.Appearance.Options.UseFont = True
        Me.LabelControl12.Appearance.Options.UseForeColor = True
        Me.LabelControl12.Location = New System.Drawing.Point(58, 279)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(7, 14)
        Me.LabelControl12.TabIndex = 38
        Me.LabelControl12.Text = "*"
        Me.LabelControl12.Visible = False
        '
        'SimpleButtonClose
        '
        Me.SimpleButtonClose.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonClose.Appearance.Options.UseFont = True
        Me.SimpleButtonClose.Location = New System.Drawing.Point(247, 479)
        Me.SimpleButtonClose.Name = "SimpleButtonClose"
        Me.SimpleButtonClose.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButtonClose.TabIndex = 14
        Me.SimpleButtonClose.Text = "Close"
        '
        'LabelControl13
        '
        Me.LabelControl13.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl13.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl13.Appearance.Options.UseFont = True
        Me.LabelControl13.Appearance.Options.UseForeColor = True
        Me.LabelControl13.Location = New System.Drawing.Point(81, 305)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(7, 14)
        Me.LabelControl13.TabIndex = 42
        Me.LabelControl13.Text = "*"
        Me.LabelControl13.Visible = False
        '
        'TextEditUserNo
        '
        Me.TextEditUserNo.Location = New System.Drawing.Point(166, 302)
        Me.TextEditUserNo.Name = "TextEditUserNo"
        Me.TextEditUserNo.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditUserNo.Properties.Appearance.Options.UseFont = True
        Me.TextEditUserNo.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEditUserNo.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEditUserNo.Properties.MaxLength = 5
        Me.TextEditUserNo.Size = New System.Drawing.Size(138, 20)
        Me.TextEditUserNo.TabIndex = 10
        '
        'LabelControl14
        '
        Me.LabelControl14.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl14.Appearance.Options.UseFont = True
        Me.LabelControl14.Location = New System.Drawing.Point(12, 304)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(63, 14)
        Me.LabelControl14.TabIndex = 41
        Me.LabelControl14.Text = "No of Users"
        '
        'LabelControl15
        '
        Me.LabelControl15.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl15.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl15.Appearance.Options.UseFont = True
        Me.LabelControl15.Appearance.Options.UseForeColor = True
        Me.LabelControl15.Location = New System.Drawing.Point(93, 330)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(7, 14)
        Me.LabelControl15.TabIndex = 45
        Me.LabelControl15.Text = "*"
        Me.LabelControl15.Visible = False
        '
        'TextEditDeviceNO
        '
        Me.TextEditDeviceNO.Location = New System.Drawing.Point(166, 328)
        Me.TextEditDeviceNO.Name = "TextEditDeviceNO"
        Me.TextEditDeviceNO.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditDeviceNO.Properties.Appearance.Options.UseFont = True
        Me.TextEditDeviceNO.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEditDeviceNO.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEditDeviceNO.Properties.MaxLength = 2
        Me.TextEditDeviceNO.Size = New System.Drawing.Size(138, 20)
        Me.TextEditDeviceNO.TabIndex = 11
        '
        'LabelControl16
        '
        Me.LabelControl16.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl16.Appearance.Options.UseFont = True
        Me.LabelControl16.Location = New System.Drawing.Point(12, 330)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(75, 14)
        Me.LabelControl16.TabIndex = 44
        Me.LabelControl16.Text = "No of Devices"
        '
        'LabelControl17
        '
        Me.LabelControl17.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl17.Appearance.Options.UseFont = True
        Me.LabelControl17.Location = New System.Drawing.Point(5, 5)
        Me.LabelControl17.Name = "LabelControl17"
        Me.LabelControl17.Size = New System.Drawing.Size(70, 14)
        Me.LabelControl17.TabIndex = 46
        Me.LabelControl17.Text = "Offline mode"
        '
        'TextEditUpload
        '
        Me.TextEditUpload.Location = New System.Drawing.Point(153, 52)
        Me.TextEditUpload.Name = "TextEditUpload"
        Me.TextEditUpload.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditUpload.Properties.Appearance.Options.UseFont = True
        Me.TextEditUpload.Size = New System.Drawing.Size(304, 20)
        Me.TextEditUpload.TabIndex = 2
        '
        'SimpleButtonUpload
        '
        Me.SimpleButtonUpload.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonUpload.Appearance.Options.UseFont = True
        Me.SimpleButtonUpload.Location = New System.Drawing.Point(463, 51)
        Me.SimpleButtonUpload.Name = "SimpleButtonUpload"
        Me.SimpleButtonUpload.Size = New System.Drawing.Size(87, 23)
        Me.SimpleButtonUpload.TabIndex = 3
        Me.SimpleButtonUpload.Text = "Upload"
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.HyperlinkLabelControl1)
        Me.PanelControl1.Controls.Add(Me.LabelControl27)
        Me.PanelControl1.Controls.Add(Me.LabelControl19)
        Me.PanelControl1.Controls.Add(Me.LabelControl18)
        Me.PanelControl1.Controls.Add(Me.SimpleButtonUpload)
        Me.PanelControl1.Controls.Add(Me.SimpleButtonDownload)
        Me.PanelControl1.Controls.Add(Me.TextEditUpload)
        Me.PanelControl1.Controls.Add(Me.LabelControl17)
        Me.PanelControl1.Location = New System.Drawing.Point(13, 354)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(566, 120)
        Me.PanelControl1.TabIndex = 12
        '
        'LabelControl27
        '
        Me.LabelControl27.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl27.Appearance.Options.UseFont = True
        Me.LabelControl27.Location = New System.Drawing.Point(5, 84)
        Me.LabelControl27.Name = "LabelControl27"
        Me.LabelControl27.Size = New System.Drawing.Size(281, 14)
        Me.LabelControl27.TabIndex = 51
        Me.LabelControl27.Text = "Click On Link you Upload file and get the license file"
        '
        'LabelControl19
        '
        Me.LabelControl19.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl19.Appearance.Options.UseFont = True
        Me.LabelControl19.Location = New System.Drawing.Point(5, 27)
        Me.LabelControl19.Name = "LabelControl19"
        Me.LabelControl19.Size = New System.Drawing.Size(117, 14)
        Me.LabelControl19.TabIndex = 50
        Me.LabelControl19.Text = "Download License file"
        '
        'LabelControl18
        '
        Me.LabelControl18.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl18.Appearance.Options.UseFont = True
        Me.LabelControl18.Location = New System.Drawing.Point(5, 55)
        Me.LabelControl18.Name = "LabelControl18"
        Me.LabelControl18.Size = New System.Drawing.Size(100, 14)
        Me.LabelControl18.TabIndex = 47
        Me.LabelControl18.Text = "Upload License file"
        '
        'SimpleButtonDownload
        '
        Me.SimpleButtonDownload.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonDownload.Appearance.Options.UseFont = True
        Me.SimpleButtonDownload.Location = New System.Drawing.Point(153, 23)
        Me.SimpleButtonDownload.Name = "SimpleButtonDownload"
        Me.SimpleButtonDownload.Size = New System.Drawing.Size(87, 23)
        Me.SimpleButtonDownload.TabIndex = 1
        Me.SimpleButtonDownload.Text = "Download"
        '
        'SaveFileDialog1
        '
        Me.SaveFileDialog1.Filter = "|*.lic"
        Me.SaveFileDialog1.Title = "Download License file"
        '
        'LabelControl20
        '
        Me.LabelControl20.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl20.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl20.Appearance.Options.UseFont = True
        Me.LabelControl20.Appearance.Options.UseForeColor = True
        Me.LabelControl20.Location = New System.Drawing.Point(39, 123)
        Me.LabelControl20.Name = "LabelControl20"
        Me.LabelControl20.Size = New System.Drawing.Size(7, 14)
        Me.LabelControl20.TabIndex = 52
        Me.LabelControl20.Text = "*"
        '
        'TextEditCity
        '
        Me.TextEditCity.Location = New System.Drawing.Point(166, 120)
        Me.TextEditCity.Name = "TextEditCity"
        Me.TextEditCity.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditCity.Properties.Appearance.Options.UseFont = True
        Me.TextEditCity.Properties.MaxLength = 50
        Me.TextEditCity.Size = New System.Drawing.Size(304, 20)
        Me.TextEditCity.TabIndex = 3
        '
        'LabelControl21
        '
        Me.LabelControl21.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl21.Appearance.Options.UseFont = True
        Me.LabelControl21.Location = New System.Drawing.Point(13, 123)
        Me.LabelControl21.Name = "LabelControl21"
        Me.LabelControl21.Size = New System.Drawing.Size(20, 14)
        Me.LabelControl21.TabIndex = 51
        Me.LabelControl21.Text = "City"
        '
        'LabelControl22
        '
        Me.LabelControl22.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl22.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl22.Appearance.Options.UseFont = True
        Me.LabelControl22.Appearance.Options.UseForeColor = True
        Me.LabelControl22.Location = New System.Drawing.Point(49, 149)
        Me.LabelControl22.Name = "LabelControl22"
        Me.LabelControl22.Size = New System.Drawing.Size(7, 14)
        Me.LabelControl22.TabIndex = 55
        Me.LabelControl22.Text = "*"
        '
        'TextEditState
        '
        Me.TextEditState.Location = New System.Drawing.Point(166, 146)
        Me.TextEditState.Name = "TextEditState"
        Me.TextEditState.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditState.Properties.Appearance.Options.UseFont = True
        Me.TextEditState.Properties.MaxLength = 50
        Me.TextEditState.Size = New System.Drawing.Size(304, 20)
        Me.TextEditState.TabIndex = 4
        '
        'LabelControl23
        '
        Me.LabelControl23.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl23.Appearance.Options.UseFont = True
        Me.LabelControl23.Location = New System.Drawing.Point(13, 149)
        Me.LabelControl23.Name = "LabelControl23"
        Me.LabelControl23.Size = New System.Drawing.Size(30, 14)
        Me.LabelControl23.TabIndex = 54
        Me.LabelControl23.Text = "State"
        '
        'LabelControl24
        '
        Me.LabelControl24.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl24.Appearance.ForeColor = System.Drawing.Color.Red
        Me.LabelControl24.Appearance.Options.UseFont = True
        Me.LabelControl24.Appearance.Options.UseForeColor = True
        Me.LabelControl24.Location = New System.Drawing.Point(62, 175)
        Me.LabelControl24.Name = "LabelControl24"
        Me.LabelControl24.Size = New System.Drawing.Size(7, 14)
        Me.LabelControl24.TabIndex = 58
        Me.LabelControl24.Text = "*"
        '
        'TextEditPincode
        '
        Me.TextEditPincode.Location = New System.Drawing.Point(166, 172)
        Me.TextEditPincode.Name = "TextEditPincode"
        Me.TextEditPincode.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditPincode.Properties.Appearance.Options.UseFont = True
        Me.TextEditPincode.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEditPincode.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEditPincode.Properties.MaxLength = 50
        Me.TextEditPincode.Size = New System.Drawing.Size(304, 20)
        Me.TextEditPincode.TabIndex = 5
        '
        'LabelControl25
        '
        Me.LabelControl25.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl25.Appearance.Options.UseFont = True
        Me.LabelControl25.Location = New System.Drawing.Point(13, 175)
        Me.LabelControl25.Name = "LabelControl25"
        Me.LabelControl25.Size = New System.Drawing.Size(43, 14)
        Me.LabelControl25.TabIndex = 57
        Me.LabelControl25.Text = "Pincode"
        '
        'LabelControl26
        '
        Me.LabelControl26.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl26.Appearance.Options.UseFont = True
        Me.LabelControl26.Location = New System.Drawing.Point(6, 10)
        Me.LabelControl26.Name = "LabelControl26"
        Me.LabelControl26.Size = New System.Drawing.Size(69, 14)
        Me.LabelControl26.TabIndex = 59
        Me.LabelControl26.Text = "Online mode"
        Me.LabelControl26.UseWaitCursor = True
        '
        'PanelControl2
        '
        Me.PanelControl2.Controls.Add(Me.SimpleButtonVerify)
        Me.PanelControl2.Controls.Add(Me.LabelControl26)
        Me.PanelControl2.Location = New System.Drawing.Point(339, 483)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(567, 36)
        Me.PanelControl2.TabIndex = 60
        Me.PanelControl2.UseWaitCursor = True
        Me.PanelControl2.Visible = False
        '
        'HyperlinkLabelControl1
        '
        Me.HyperlinkLabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.HyperlinkLabelControl1.Appearance.Options.UseFont = True
        Me.HyperlinkLabelControl1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.HyperlinkLabelControl1.Location = New System.Drawing.Point(326, 84)
        Me.HyperlinkLabelControl1.Name = "HyperlinkLabelControl1"
        Me.HyperlinkLabelControl1.Size = New System.Drawing.Size(168, 14)
        Me.HyperlinkLabelControl1.TabIndex = 52
        Me.HyperlinkLabelControl1.Tag = "http://crm.mytimewatch.com/"
        Me.HyperlinkLabelControl1.Text = "http://crm.mytimewatch.com/"
        Me.HyperlinkLabelControl1.ToolTip = "http://crm.mytimewatch.com/"
        '
        'XtraCompanyInfo
        '
        Me.AcceptButton = Me.SimpleButtonSave
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(586, 521)
        Me.ControlBox = False
        Me.Controls.Add(Me.PanelControl2)
        Me.Controls.Add(Me.LabelControl24)
        Me.Controls.Add(Me.TextEditPincode)
        Me.Controls.Add(Me.LabelControl25)
        Me.Controls.Add(Me.LabelControl22)
        Me.Controls.Add(Me.TextEditState)
        Me.Controls.Add(Me.LabelControl23)
        Me.Controls.Add(Me.LabelControl20)
        Me.Controls.Add(Me.TextEditCity)
        Me.Controls.Add(Me.LabelControl21)
        Me.Controls.Add(Me.PanelControl1)
        Me.Controls.Add(Me.LabelControl15)
        Me.Controls.Add(Me.TextEditDeviceNO)
        Me.Controls.Add(Me.LabelControl16)
        Me.Controls.Add(Me.LabelControl13)
        Me.Controls.Add(Me.TextEditUserNo)
        Me.Controls.Add(Me.LabelControl14)
        Me.Controls.Add(Me.SimpleButtonClose)
        Me.Controls.Add(Me.LabelControl12)
        Me.Controls.Add(Me.MemoEditAddress)
        Me.Controls.Add(Me.LabelControl11)
        Me.Controls.Add(Me.LabelControl10)
        Me.Controls.Add(Me.LabelControl9)
        Me.Controls.Add(Me.LabelControl8)
        Me.Controls.Add(Me.LabelControl7)
        Me.Controls.Add(Me.SimpleButtonSave)
        Me.Controls.Add(Me.TextEditLicense)
        Me.Controls.Add(Me.LabelControl6)
        Me.Controls.Add(Me.TextEditUserKey)
        Me.Controls.Add(Me.LabelControl5)
        Me.Controls.Add(Me.TextEditEmail)
        Me.Controls.Add(Me.LabelControl4)
        Me.Controls.Add(Me.TextEditContact)
        Me.Controls.Add(Me.LabelControl3)
        Me.Controls.Add(Me.LabelControl2)
        Me.Controls.Add(Me.TextEditCompanyName)
        Me.Controls.Add(Me.LabelControl1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.LookAndFeel.SkinName = "iMaginary"
        Me.LookAndFeel.UseDefaultLookAndFeel = False
        Me.Name = "XtraCompanyInfo"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Company Information"
        CType(Me.MemoEditAddress.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditLicense.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditUserKey.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditEmail.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditContact.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditCompanyName.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditUserNo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditDeviceNO.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditUpload.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.TextEditCity.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditState.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditPincode.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl2.ResumeLayout(False)
        Me.PanelControl2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MemoEditAddress As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SimpleButtonVerify As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButtonSave As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents TextEditLicense As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditUserKey As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditEmail As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditContact As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditCompanyName As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SimpleButtonClose As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditUserNo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditDeviceNO As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditUpload As DevExpress.XtraEditors.TextEdit
    Friend WithEvents SimpleButtonUpload As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LabelControl18 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl19 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SimpleButtonDownload As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
    Friend WithEvents LabelControl20 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditCity As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl21 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl22 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditState As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl23 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl24 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditPincode As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl25 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl26 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LabelControl27 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents HyperlinkLabelControl1 As DevExpress.XtraEditors.HyperlinkLabelControl
End Class
