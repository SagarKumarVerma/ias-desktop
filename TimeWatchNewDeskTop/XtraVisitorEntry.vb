﻿Imports System.Resources
Imports System.Globalization
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid.Views.Base
Imports DevExpress.XtraEditors.Repository
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports DevExpress.XtraGrid
Imports System.Drawing.Imaging


Imports libzkfpcsharp
Imports System.Runtime.InteropServices
Imports System.Threading
Imports Sample
Imports System.Text
Imports DevExpress.XtraLayout.Customization

Public Class XtraVisitorEntry
    Dim ulf As UserLookAndFeel
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand

    'for finger print
    Dim IsRegister As Boolean = False
    Dim RegisterCount As Integer = 0
    Dim cbRegTmp As Integer = 0
    Dim mDBHandle As IntPtr = IntPtr.Zero
    Dim mDevHandle As IntPtr = IntPtr.Zero
    Dim FormHandle As IntPtr = IntPtr.Zero
    Dim iFid As Integer = 1
    Dim RegTmps()() As Byte = New Byte(3 - 1)() {}
    Dim mfpWidth As Integer = 0
    Dim mfpHeight As Integer = 0
    Dim FPBuffer() As Byte
    Dim bIsTimeToDie As Boolean = False
    Dim cbCapTmp As Integer = 2048
    Dim CapTmp() As Byte = New Byte(2048 - 1) {}
    Dim RegTmp() As Byte = New Byte(2048 - 1) {}
    Dim REGISTER_FINGER_COUNT As Integer = 3
    Dim bIdentify As Boolean = True

    Dim MESSAGE_CAPTURED_OK As Integer = (1024 + 6)
    Dim strBase64 As String


    Public Shared Vimage As Image
    Public Shared VName As String
    Public Shared VCom As String
    Public Shared VToMeet As String
    Public Shared VDept As String
    Public Shared VDate As String
    Public Shared VTime As String
    Public Shared VSrNo As String

    Public Shared VCovistor2 As String
    Public Shared VPhone As String

    <DllImport("user32.dll", EntryPoint:="SendMessageA")>
    Public Shared Function SendMessage(ByVal hwnd As IntPtr, ByVal wMsg As Integer, ByVal wParam As IntPtr, ByVal lParam As IntPtr) As Integer
    End Function
    'Public Declare Function SendMessage Lib "user32.dll" Alias "SendMessageA" (ByVal hwnd As IntPtr, ByVal wMsg As Integer, ByVal wParam As IntPtr, ByVal lParam As IntPtr) As Integer
    'End for finger print
    Public Sub New()
        InitializeComponent()
        If Common.servername = "Access" Then
            Me.TblCompany1TableAdapter1.Fill(Me.SSSDBDataSet.tblCompany1)
            'Me.TblEmployee1TableAdapter1.Fill(Me.SSSDBDataSet.TblEmployee1)
            Me.TblDepartment1TableAdapter1.Fill(Me.SSSDBDataSet.tblDepartment1)
            'Me.TblMachine1TableAdapter1.Fill(Me.SSSDBDataSet.tblMachine1)
        Else
            TblCompanyTableAdapter.Connection.ConnectionString = Common.ConnectionString '"Data Source=DESKTOP-PG6NDV8;Initial Catalog=SSSDB;Integrated Security=True"
            Me.TblCompanyTableAdapter.Fill(Me.SSSDBDataSet.tblCompany)
            TblEmployeeTableAdapter.Connection.ConnectionString = Common.ConnectionString
            'Me.TblEmployeeTableAdapter.Fill(Me.SSSDBDataSet.TblEmployee)
            TblDepartmentTableAdapter.Connection.ConnectionString = Common.ConnectionString
            Me.TblDepartmentTableAdapter.Fill(Me.SSSDBDataSet.tblDepartment)
            TblMachineTableAdapter.Connection.ConnectionString = Common.ConnectionString
            'Me.TblMachineTableAdapter.Fill(Me.SSSDBDataSet.tblMachine)
        End If
    End Sub
    Private Sub XtraVisitorEntry_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        'Me.Width = My.Computer.Screen.WorkingArea.Width
        Me.Width = Common.NavWidth 'Me.Parent.Width
        Me.Height = Common.NavHeight 'Me.Parent.Height
        SplitContainerControl1.Width = SplitContainerControl1.Parent.Width
        SplitContainerControl1.SplitterPosition = (SplitContainerControl1.Parent.Width) * 85 / 100
        'not to do in all pages
        Common.splitforMasterMenuWidth = SplitContainerControl1.Width
        Common.SplitterPosition = SplitContainerControl1.SplitterPosition
        'for xtramessage box
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True
        If Common.servername = "Access" Then
            Me.TblCompany1TableAdapter1.Fill(Me.SSSDBDataSet.tblCompany1)
            LookUpEditComp.Properties.DataSource = SSSDBDataSet.tblCompany1
            'Me.TblEmployee1TableAdapter1.Fill(Me.SSSDBDataSet.TblEmployee1)
            'LookUpEditEmp.Properties.DataSource = SSSDBDataSet.TblEmployee1
            Me.TblDepartment1TableAdapter1.Fill(Me.SSSDBDataSet.tblDepartment1)
            LookUpEditDept.Properties.DataSource = SSSDBDataSet.tblDepartment1
            'Me.TblMachine1TableAdapter1.Fill(Me.SSSDBDataSet.tblMachine1)
            'GridControl1.DataSource = SSSDBDataSet.tblMachine1
        Else
            Me.TblCompanyTableAdapter.Fill(Me.SSSDBDataSet.tblCompany)
            LookUpEditComp.Properties.DataSource = SSSDBDataSet.tblCompany
            'Me.TblEmployeeTableAdapter.Fill(Me.SSSDBDataSet.TblEmployee)
            'LookUpEditEmp.Properties.DataSource = SSSDBDataSet.TblEmployee
            Me.TblDepartmentTableAdapter.Fill(Me.SSSDBDataSet.tblDepartment)
            LookUpEditDept.Properties.DataSource = SSSDBDataSet.tblDepartment
            'Me.TblMachineTableAdapter.Fill(Me.SSSDBDataSet.tblMachine)
            'GridControl1.DataSource = SSSDBDataSet.tblMachine
        End If
        LookUpEditEmp.Properties.DataSource = Common.EmpNonAdmin
        GridControl1.DataSource = Common.MachineNonAdmin

        FormHandle = Me.Handle  'used for finger print
        setDefault()
        setDeviceGrid()

        'Dim adap As SqlDataAdapter
        'Dim adapA As OleDbDataAdapter
        'Dim ds As DataSet = New DataSet
        'Dim sSql As String = "SELECT * from tblVisitorTransaction where VISIT_NO = '20180515192137'"
        'If Common.servername = "Access" Then
        '    adapA = New OleDbDataAdapter(sSql, Common.con1)
        '    adapA.Fill(ds)
        'Else
        '    adap = New SqlDataAdapter(sSql, Common.con)
        '    adap.Fill(ds)
        'End If
        'Dim imageIn As System.Drawing.Image
        'Dim cat As Category = CType(ds.Tables(0).Rows(0).Item(0).Current, Category)
        'Dim imageData() As Byte = cat.Picture.ToArray() 'ds.Tables(0).Rows(0).Item(0)
        'Dim imageDataStream As New MemoryStream()
        'Dim offset As Integer = 78

        'imageDataStream.Write(imageData, offset, imageData.Length - offset)
        'PictureEdit1.Image = Image.FromStream(imageDataStream)
    End Sub
    Private Sub setDefault()
        LookUpEditComp.ItemIndex = 0
        TextEditPhone.Text = ""
        TextEditName.Text = ""
        TextEditVComp.Text = ""
        MemoEditAddress.Text = ""
        TextEditConNo.Text = ""
        TextEditVehiNo.Text = ""
        TextEditSlNo.Text = Now.ToString("yyyyMMddHHmmss")
        TextEditCoVisi1.Text = ""
        TextEditCoVisi2.Text = ""
        'LookUpEditEmp.ItemIndex = -1
        'LookUpEditDept.ItemIndex = -1
        LookUpEditEmp.EditValue = ""
        LookUpEditDept.EditValue = ""
        TextEditInTime.Text = Now.ToString("HH:mm")
        TextEditIssuedBy.Text = Common.USER_R
        MemoEditTools.Text = ""
        VisitorPictureEdit.Image = Nothing
        CheckEditOfficial.Checked = True
        SimpleButtonStop.Enabled = False
        PopupContainerEdit1.EditValue = ""
        'SimpleButtonFPCopy.Enabled = False
        VisitorPictureEdit.Image = Nothing
        picFPImg.Image = Nothing
        DateEditStart.EditValue = Now
        DateEditEnd.EditValue = Now.AddDays(1)
    End Sub
    Private Sub setDeviceGrid()
        Dim gridtblregisterselet As String
        gridtblregisterselet = "select * from tblMachine where DeviceType = 'ZK(TFT)' or  DeviceType = 'Bio-1Pro/ATF305Pro/ATF686Pro' "
        Dim WTDataTable As DataTable
        If Common.servername = "Access" Then
            Dim dataAdapter As New OleDbDataAdapter(gridtblregisterselet, Common.con1)
            WTDataTable = New DataTable("tblMachine")
            dataAdapter.Fill(WTDataTable)
        Else
            Dim dataAdapter As New SqlClient.SqlDataAdapter(gridtblregisterselet, Common.con)
            WTDataTable = New DataTable("tblMachine")
            dataAdapter.Fill(WTDataTable)
        End If
        GridControl1.DataSource = WTDataTable
    End Sub
    Public Sub InvokeDefaultCameraDialog()
        Dim dialog As DevExpress.XtraEditors.Camera.TakePictureDialog = New DevExpress.XtraEditors.Camera.TakePictureDialog
        If dialog.ShowDialog = DialogResult.OK Then
            Dim image As System.Drawing.Image = dialog.Image
            Using stream As MemoryStream = New MemoryStream
                image.Save(stream, ImageFormat.Jpeg)
                VisitorPictureEdit.EditValue = stream.ToArray
            End Using
        End If
    End Sub
    Private Sub PictureEdit1_DoubleClick(sender As System.Object, e As System.EventArgs) Handles VisitorPictureEdit.DoubleClick
        InvokeDefaultCameraDialog()
    End Sub
    Private Sub TextEditPhone_Leave(sender As System.Object, e As System.EventArgs) Handles TextEditPhone.Leave
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim ds As DataSet = New DataSet
        Dim sSql As String = "SELECT * from tblVisitorTransaction where FV_No = '" & TextEditPhone.Text.Trim & "'"
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(ds)
        End If
        If ds.Tables(0).Rows.Count > 0 Then
            TextEditName.Text = ds.Tables(0).Rows(0).Item("Visitor_Name").ToString.Trim
            TextEditVComp.Text = ds.Tables(0).Rows(0).Item("VCompanyName").ToString.Trim
            MemoEditAddress.Text = ds.Tables(0).Rows(0).Item("VCompanyaDDRESS").ToString.Trim
            TextEditConNo.Text = ds.Tables(0).Rows(0).Item("ContactNO").ToString.Trim
            TextEditVehiNo.Text = ds.Tables(0).Rows(0).Item("VEHICLENO").ToString.Trim
        Else
            TextEditName.Text = ""
            TextEditVComp.Text = ""
            MemoEditAddress.Text = ""
            TextEditConNo.Text = ""
            TextEditVehiNo.Text = ""
        End If
    End Sub
    Private Sub PopupContainerEdit1_QueryPopUp(sender As System.Object, e As System.ComponentModel.CancelEventArgs) Handles PopupContainerEdit1.QueryPopUp
        Dim val As Object = PopupContainerEdit1.EditValue
        If (val Is Nothing) Then
            GridView1.ClearSelection()
        Else
            'Dim texts() As String = val.ToString.Split(Microsoft.VisualBasic.ChrW(44))
            Dim texts() As String = val.ToString.Split(",")
            For Each text As String In texts
                If text.Trim.Length = 1 Then
                    text = text.Trim & "  "
                ElseIf text.Trim.Length = 2 Then
                    text = text.Trim & " "
                End If
                'MsgBox(text & "  " & text.Length & " " & GridView1.LocateByValue("SHIFT", text))
                Dim rowHandle As Integer = GridView1.LocateByValue("ID_NO", text)
                GridView1.SelectRow(rowHandle)
            Next
        End If
    End Sub
    Private Sub PopupContainerEdit1_QueryResultValue(sender As System.Object, e As DevExpress.XtraEditors.Controls.QueryResultValueEventArgs) Handles PopupContainerEdit1.QueryResultValue
        Dim selectedRows() As Integer = GridView1.GetSelectedRows
        Dim sb As StringBuilder = New StringBuilder
        For Each selectionRow As Integer In selectedRows
            Dim a As System.Data.DataRowView = GridView1.GetRow(selectionRow)
            If (sb.ToString.Length > 0) Then
                sb.Append(", ")
            End If
            sb.Append(a.Item("ID_NO"))
        Next
        e.Value = sb.ToString
    End Sub
    Private Sub LookUpEditEmp_Leave(sender As System.Object, e As System.EventArgs) Handles LookUpEditEmp.Leave
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim ds As DataSet = New DataSet
        Try
            If LookUpEditEmp.EditValue.ToString.Trim = "" Then
                XtraMessageBox.Show(ulf, "<size=10>Please select Employee</size>", "<size=9>Error</size>")
                LookUpEditEmp.Select()
                Exit Sub
            End If
        Catch ex As Exception
            LookUpEditEmp.Select()
            Exit Sub
        End Try

        Dim sSql As String = "SELECT * from tblEmployee where PAYCODE = '" & LookUpEditEmp.EditValue.ToString.Trim & "'"
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(ds)
        End If
        If ds.Tables(0).Rows.Count > 0 Then
            LookUpEditDept.EditValue = ds.Tables(0).Rows(0).Item("DEPARTMENTCODE").ToString.Trim
        End If
    End Sub
    Private Sub SimpleButtonSave_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonSave.Click
        Dim visitor_image() As Byte
        'Dim x As String
        Try
            If VisitorPictureEdit.EditValue = Nothing Then
                If XtraMessageBox.Show(ulf, "<size=10>Are you sure you dont want to Add visitor Image</size>", "<size=9>Confirm</size>", _
                              MessageBoxButtons.YesNo, MessageBoxIcon.Question) <> DialogResult.Yes Then
                    'x = Nothing
                End If
                'XtraMessageBox.Show(ulf, "<size=10>Please click picture of visitor</size>", "<size=9></size>")
                'Exit Sub
            Else
                visitor_image = imageToByteArray(VisitorPictureEdit.Image)
            End If
        Catch ex As Exception
            visitor_image = imageToByteArray(VisitorPictureEdit.Image)
        End Try
        'insert
        Dim VISIT_NO As String = TextEditSlNo.Text.Trim
        Dim Visit_Date As String = Now.ToString("yyyy-MM-dd 00:00:00")

        Dim VEHICLENO As String = TextEditVehiNo.Text.Trim
        Dim ContactNO As String = TextEditConNo.Text.Trim
        If TextEditPhone.Text.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Visitor Phone Number cannot be empty</size>", "<size=9>Error</size>")
            TextEditPhone.Select()
            Exit Sub
        End If
        Dim Visitor_Name As String = TextEditName.Text.Trim
        If TextEditName.Text.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Visitor Name cannot be empty</size>", "<size=9>Error</size>")
            TextEditName.Select()
            Exit Sub
        End If
        Dim VCompanyName As String = TextEditVComp.Text.Trim
        If TextEditVComp.Text.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Visitor Company cannot be empty</size>", "<size=9>Error</size>")
            TextEditVComp.Select()
            Exit Sub
        End If
        Dim VCompanyaDDRESS As String = MemoEditAddress.Text.Trim
        If MemoEditAddress.Text.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Visitor Address be empty</size>", "<size=9>Error</size>")
            MemoEditAddress.Select()
            Exit Sub
        End If
        Dim FV_No As String = TextEditPhone.Text.Trim
        If LookUpEditEmp.EditValue.ToString.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Please select Employee whom to Meet fail</size>", "<size=9>Error</size>")
            LookUpEditEmp.Select()
            Exit Sub
        End If
        Dim EMPLOYEECODE As String = LookUpEditEmp.EditValue.ToString.Trim

        If LookUpEditDept.EditValue.ToString.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Please select Department</size>", "<size=9>Error</size>")
            LookUpEditDept.Select()
            Exit Sub
        End If

        Dim DEPARTMENTcode As String = LookUpEditDept.EditValue.ToString.Trim
        Dim CO_VISITOR1 As String = TextEditCoVisi1.Text.Trim
        Dim CO_VISITOR2 As String = TextEditCoVisi2.Text.Trim
        Dim USER_R As String = TextEditIssuedBy.Text.Trim
        Dim INTIME As String = TextEditInTime.Text.Trim
        Dim OUTTIME As String
        Dim purpose As String
        If CheckEditOfficial.Checked = True Then
            purpose = "Official"
        Else
            purpose = "Personal"
        End If
        Dim APPTIME As String = TextEditAppTime.Text.Trim
        Dim ENTRYTIME As String = TextEditInTime.Text.Trim

        'Dim visitor_image() As Byte = PictureEdit1.EditValue
        ' MsgBox(visitor_image.ToString)
        Dim TOOLS As String = MemoEditTools.Text.Trim
        Dim TIMESPEND As String
        Dim BlackListed As String
        Dim vFinger As String
        Dim vFingerImage() As Byte
        Dim fingerImageString As String = Encoding.UTF8.GetString(CapTmp)
        'MsgBox(fingerImageString)

        Dim sSql As String
        Try
            If VisitorPictureEdit.EditValue.ToString = "" Then
                sSql = "insert into tblVisitorTransaction( VISIT_NO, Visit_Date, Visitor_Name, VCompanyName, VCompanyaDDRESS, VEHICLENO, ContactNO, FV_No, DEPARTMENTcode, EMPLOYEECODE, CO_VISITOR1, CO_VISITOR2, USER_R, INTIME, OUTTIME, purpose, APPTIME, ENTRYTIME, TOOLS, TIMESPEND, BlackListed, vFinger, vFingerImage) values " & _
                 "('" & VISIT_NO & "','" & Visit_Date & "','" & Visitor_Name & "','" & VCompanyName & "','" & VCompanyaDDRESS & "','" & VEHICLENO & "','" & ContactNO & "','" & FV_No & "','" & DEPARTMENTcode & "','" & EMPLOYEECODE & "','" & CO_VISITOR1 & "','" & CO_VISITOR2 & "','" & USER_R & "','" & INTIME & "','" & OUTTIME & "','" & purpose & "','" & APPTIME & "','" & ENTRYTIME & "','" & TOOLS & "','" & TIMESPEND & "','" & BlackListed & "','" & vFinger & "',@vFingerImage)"
            Else
                sSql = "insert into tblVisitorTransaction( VISIT_NO, Visit_Date, Visitor_Name, VCompanyName, VCompanyaDDRESS, VEHICLENO, ContactNO, FV_No, DEPARTMENTcode, EMPLOYEECODE, CO_VISITOR1, CO_VISITOR2, USER_R, INTIME, OUTTIME, purpose, APPTIME, ENTRYTIME, TOOLS, TIMESPEND, BlackListed, vFinger, vFingerImage, visitor_image) values " & _
                 "('" & VISIT_NO & "','" & Visit_Date & "','" & Visitor_Name & "','" & VCompanyName & "','" & VCompanyaDDRESS & "','" & VEHICLENO & "','" & ContactNO & "','" & FV_No & "','" & DEPARTMENTcode & "','" & EMPLOYEECODE & "','" & CO_VISITOR1 & "','" & CO_VISITOR2 & "','" & USER_R & "','" & INTIME & "','" & OUTTIME & "','" & purpose & "','" & APPTIME & "','" & ENTRYTIME & "','" & TOOLS & "','" & TIMESPEND & "','" & BlackListed & "','" & vFinger & "',@vFingerImage,@visitor_image)"
            End If
        Catch
            sSql = "insert into tblVisitorTransaction( VISIT_NO, Visit_Date, Visitor_Name, VCompanyName, VCompanyaDDRESS, VEHICLENO, ContactNO, FV_No, DEPARTMENTcode, EMPLOYEECODE, CO_VISITOR1, CO_VISITOR2, USER_R, INTIME, OUTTIME, purpose, APPTIME, ENTRYTIME, TOOLS, TIMESPEND, BlackListed, vFinger, vFingerImage) values " & _
            "('" & VISIT_NO & "','" & Visit_Date & "','" & Visitor_Name & "','" & VCompanyName & "','" & VCompanyaDDRESS & "','" & VEHICLENO & "','" & ContactNO & "','" & FV_No & "','" & DEPARTMENTcode & "','" & EMPLOYEECODE & "','" & CO_VISITOR1 & "','" & CO_VISITOR2 & "','" & USER_R & "','" & INTIME & "','" & OUTTIME & "','" & purpose & "','" & APPTIME & "','" & ENTRYTIME & "','" & TOOLS & "','" & TIMESPEND & "','" & BlackListed & "','" & vFinger & "',@vFingerImage)"
        End Try

        Dim tmp As String
        Dim tmpValue As String

        If picFPImg.EditValue Is Nothing And VisitorPictureEdit.EditValue Is Nothing Then
            tmp = ""
            tmpValue = ""
        ElseIf picFPImg.EditValue Is Nothing And VisitorPictureEdit.EditValue IsNot Nothing Then
            tmp = ", visitor_image"
            tmpValue = ", @visitor_image"
        ElseIf picFPImg.EditValue IsNot Nothing And VisitorPictureEdit.EditValue Is Nothing Then
            tmp = ", vFingerImage"
            tmpValue = ", @vFingerImage"
        ElseIf picFPImg.EditValue IsNot Nothing And VisitorPictureEdit.EditValue IsNot Nothing Then
            tmp = ", visitor_image , vFingerImage"
            tmpValue = ", @visitor_image , @vFingerImage"
        End If

        sSql = "insert into tblVisitorTransaction( VISIT_NO, Visit_Date, Visitor_Name, VCompanyName, VCompanyaDDRESS, VEHICLENO, ContactNO, FV_No, DEPARTMENTcode, EMPLOYEECODE, CO_VISITOR1, CO_VISITOR2, USER_R, INTIME, OUTTIME, purpose, APPTIME, ENTRYTIME, TOOLS, TIMESPEND, BlackListed" & tmp & ") values " & _
                "('" & VISIT_NO & "','" & Visit_Date & "','" & Visitor_Name & "','" & VCompanyName & "','" & VCompanyaDDRESS & "','" & VEHICLENO & "','" & ContactNO & "','" & FV_No & "','" & DEPARTMENTcode & "','" & EMPLOYEECODE & "','" & CO_VISITOR1 & "','" & CO_VISITOR2 & "','" & USER_R & "','" & INTIME & "','" & OUTTIME & "','" & purpose & "','" & APPTIME & "','" & ENTRYTIME & "','" & TOOLS & "','" & TIMESPEND & "','" & BlackListed & "'" & tmpValue & ")"

        If Common.servername = "Access" Then
            If Common.con1.State <> ConnectionState.Open Then
                Common.con1.Open()
            End If
            cmd1 = New OleDbCommand(sSql, Common.con1)
            'Using picture As Image = Image.FromFile("./fb")
            Using stream As New IO.MemoryStream
                'picture.Save(stream, Imaging.ImageFormat.Jpeg)
                If picFPImg.EditValue IsNot Nothing Then
                    cmd1.Parameters.Add("@vFingerImage", OleDb.OleDbType.VarChar).Value = strBase64 ' CapTmp ' RegTmp 
                End If
                If VisitorPictureEdit.EditValue IsNot Nothing Then
                    cmd1.Parameters.Add("@visitor_image", SqlDbType.Image).Value = visitor_image 'vbytEnrollData
                End If
                'Try
                '    If PictureEdit1.EditValue.ToString = "" Then
                '    Else
                '        cmd1.Parameters.Add("@visitor_image", SqlDbType.VarChar).Value = x ' visitor_image 'vbytEnrollData
                '    End If
                'Catch
                '    'cmd1.Parameters.Add("@visitor_image", SqlDbType.VarChar).Value = x ' visitor_image 'vbytEnrollData
                'End Try
                cmd1.ExecuteNonQuery()
                If Common.con1.State <> ConnectionState.Closed Then
                    Common.con1.Close()
                End If
            End Using
            'End Using
        Else
            If Common.con.State <> ConnectionState.Open Then
                Common.con.Open()
            End If
            cmd = New SqlCommand(sSql, Common.con)
            Using stream As New IO.MemoryStream
                If picFPImg.EditValue IsNot Nothing Then
                    cmd.Parameters.Add("@vFingerImage", SqlDbType.VarChar).Value = strBase64 ' CapTmp ' RegTmp 
                End If
                If VisitorPictureEdit.EditValue IsNot Nothing Then
                    cmd.Parameters.Add("@visitor_image", SqlDbType.Image).Value = visitor_image 'vbytEnrollData
                End If

                'cmd.Parameters.Add("@vFingerImage", SqlDbType.VarChar).Value = strBase64 'CapTmp 'RegTmp
                'Try
                '    If PictureEdit1.EditValue.ToString = "" Then
                '    Else
                '        cmd.Parameters.Add("@visitor_image", SqlDbType.VarChar).Value = x ' visitor_image 'vbytEnrollData
                '    End If
                'Catch
                '    'cmd.Parameters.Add("@visitor_image", SqlDbType.VarChar).Value = x ' visitor_image 'vbytEnrollData
                'End Try
                cmd.ExecuteNonQuery()
            End Using
            If Common.con.State <> ConnectionState.Closed Then
                Common.con.Close()
            End If
        End If
        If picFPImg.EditValue IsNot Nothing Then
            If PopupContainerEdit1.EditValue = "" Then
                If XtraMessageBox.Show(ulf, "<size=10>Are you sure you dont want to copy Finger Print to Biomatric Device</size>", "<size=9>Confirm</size>",
                                  MessageBoxButtons.YesNo, MessageBoxIcon.Question) <> DialogResult.Yes Then
                    PopupContainerEdit1.Select()
                    Exit Sub
                End If
            Else
                Try
                    FPCopy()
                Catch ex As Exception

                End Try

            End If
        ElseIf TextEditCard.Text.Trim <> "" Then
            Try
                FPCopy()
            Catch ex As Exception

            End Try
        End If
        printVisitor()
        setDefault()
    End Sub
    Private Sub SimpleButtonInit_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonInit.Click
        Dim ret As Integer '= zkfperrdef.ZKFP_ERR_OK
        Try
            ret = zkfp2.Init()
        Catch ex As Exception
            XtraMessageBox.Show(ulf, "<size=10>Driver Not Found!</size>", "<size=9>Error</size>")
            Exit Sub
        End Try
        'If (ret = zkfp2.Init()) = zkfperrdef.ZKFP_ERR_OK Then
        If ret = zkfperrdef.ZKFP_ERR_OK Then
            Dim nCount As Integer = zkfp2.GetDeviceCount()
            If nCount = 0 Then
                zkfp2.Terminate()
                XtraMessageBox.Show(ulf, "<size=10>No device connected!</size>", "<size=9>Error</size>")
            Else
                bnOpen()
                bnEnroll()
                SimpleButtonInit.Enabled = False
                SimpleButtonStop.Enabled = True
            End If
        Else
            XtraMessageBox.Show(ulf, "<size=10>No Device Found!</size>", "<size=9>Error</size>")
            'XtraMessageBox.Show(ulf, "<size=10>Initialize fail, ret=" & ret & " !</size>", "<size=9>Error</size>")
        End If
    End Sub
    Private Sub bnOpen()
        Dim ret As Integer = zkfp.ZKFP_ERR_OK
        mDevHandle = zkfp2.OpenDevice(0)
        If (IntPtr.Zero = mDevHandle) Then '0 is the index of device connected, we have only one device so 0
            XtraMessageBox.Show(ulf, "<size=10>OpenDevice fail</size>", "<size=9>Error</size>")
            'MessageBox.Show("OpenDevice fail")
            Return
        End If
        mDBHandle = zkfp2.DBInit()
        If (IntPtr.Zero = mDBHandle) Then
            XtraMessageBox.Show(ulf, "<size=10>Init DB fail</size>", "<size=9>Error</size>")
            'MessageBox.Show("Init DB fail")
            zkfp2.CloseDevice(mDevHandle)
            mDevHandle = IntPtr.Zero
            Return
        End If

        RegisterCount = 0
        cbRegTmp = 0
        iFid = 1

        'Do While (i < 3)
        '    RegTmps(i) = New Byte(2048 - 1) {}
        '    i = (i + 1)
        'Loop
        For i = 0 To 2
            RegTmps(i) = New Byte(2048 - 1) {}
        Next
        Dim paramValue() As Byte = New Byte(4 - 1) {}
        Dim size As Integer = 4
        zkfp2.GetParameters(mDevHandle, 1, paramValue, size)
        zkfp2.ByteArray2Int(paramValue, mfpWidth)
        size = 4
        zkfp2.GetParameters(mDevHandle, 2, paramValue, size)
        zkfp2.ByteArray2Int(paramValue, mfpHeight)
        FPBuffer = New Byte((mfpWidth * mfpHeight) - 1) {}
        Dim captureThread As Thread = New Thread(New ThreadStart(AddressOf DoCapture))
        captureThread.IsBackground = True
        captureThread.Start()
        bIsTimeToDie = False
        textRes.Text = "Open succ"
    End Sub
    Private Sub DoCapture()
        While Not bIsTimeToDie
            cbCapTmp = 2048
            Dim ret As Integer = zkfp2.AcquireFingerprint(mDevHandle, FPBuffer, CapTmp, cbCapTmp)
            If (ret = zkfp.ZKFP_ERR_OK) Then
                SendMessage(FormHandle, MESSAGE_CAPTURED_OK, IntPtr.Zero, IntPtr.Zero)
            End If
            Thread.Sleep(200)
        End While

    End Sub
    Private Sub bnEnroll()
        If Not IsRegister Then
            IsRegister = True
            RegisterCount = 0
            cbRegTmp = 0
            textRes.Text = "Please press your finger 3 times!"
        End If
    End Sub
    Protected Overrides Sub DefWndProc(ByRef m As System.Windows.Forms.Message)
        Select Case (m.Msg)
            Case MESSAGE_CAPTURED_OK
                Dim ms As System.IO.MemoryStream = New System.IO.MemoryStream
                Sample.BitmapFormat.GetBitmap(FPBuffer, mfpWidth, mfpHeight, ms)
                Dim bmp As Bitmap = New Bitmap(ms)
                Me.picFPImg.Image = bmp
                Me.PictureBox1.Image = bmp
                If IsRegister Then
                    Dim ret As Integer = zkfp.ZKFP_ERR_OK
                    Dim score As Integer = 0
                    Dim fid As Integer = 0
                    ret = zkfp2.DBIdentify(mDBHandle, CapTmp, fid, score)
                    If (zkfp.ZKFP_ERR_OK = ret) Then
                        textRes.Text = ("This finger was already register by " & (fid & "!"))
                        Return
                    End If

                    'If ((RegisterCount > 0) And (zkfp2.DBMatch(mDBHandle, CapTmp, RegTmps((RegisterCount - 1))) <= 0)) Then
                    If (RegisterCount > 0) Then
                        If zkfp2.DBMatch(mDBHandle, CapTmp, RegTmps(RegisterCount - 1)) <= 0 Then
                            textRes.Text = "Please press the same finger 3 times for the enrollment"
                            Return
                        End If
                    End If

                    Array.Copy(CapTmp, RegTmps(RegisterCount), cbCapTmp)
                    strBase64 = zkfp2.BlobToBase64(CapTmp, cbCapTmp)
                    Dim blob() As Byte = zkfp2.Base64ToBlob(strBase64)
                    RegisterCount = (RegisterCount + 1)
                    If (RegisterCount >= REGISTER_FINGER_COUNT) Then
                        RegisterCount = 0
                        If ((zkfp.ZKFP_ERR_OK = zkfp2.DBMerge(mDBHandle, RegTmps(0), RegTmps(1), RegTmps(2), RegTmp, cbRegTmp)) _
                                    AndAlso (zkfp.ZKFP_ERR_OK = zkfp2.DBAdd(mDBHandle, iFid, RegTmp))) Then
                            iFid = (iFid + 1)
                            textRes.Text = "enroll succ"
                            SimpleButtonFPCopy.Enabled = True
                        Else
                            textRes.Text = ("enroll fail, error code=" & ret)
                        End If

                        IsRegister = False
                        Return
                    Else
                        textRes.Text = ("You need to press the " & ((REGISTER_FINGER_COUNT - RegisterCount) & " times fingerprint"))
                    End If
                Else
                    If (cbRegTmp <= 0) Then
                        textRes.Text = "Please register your finger first!"
                        Return
                    End If

                    If bIdentify Then
                        Dim ret As Integer = zkfp.ZKFP_ERR_OK
                        Dim score As Integer = 0
                        Dim fid As Integer = 0
                        ret = zkfp2.DBIdentify(mDBHandle, CapTmp, fid, score)
                        If (zkfp.ZKFP_ERR_OK = ret) Then
                            textRes.Text = ("Identify succ, fid= " & fid & ",score=" & score & "!")
                            Return
                        Else
                            textRes.Text = ("Identify fail, ret= " & ret)
                            Return
                        End If

                    Else
                        Dim ret As Integer = zkfp2.DBMatch(mDBHandle, CapTmp, RegTmp)
                        If (0 < ret) Then
                            textRes.Text = ("Match finger succ, score=" & (ret & "!"))
                            Return
                        Else
                            textRes.Text = ("Match finger fail, ret= " & ret)
                            Return
                        End If
                    End If
                End If
                'Me.picFPImg.EditValue = RegTmps(RegisterCount) 'CapTmp   'nitin
            Case Else
                MyBase.DefWndProc(m)
        End Select
    End Sub

    Private Sub SimpleButtonStop_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonStop.Click
        zkfp2.Terminate()
        SimpleButtonInit.Enabled = True
        SimpleButtonStop.Enabled = False
    End Sub
    Private Sub XtraVisitorEntry_Leave(sender As System.Object, e As System.EventArgs) Handles MyBase.Leave
        zkfp2.Terminate()
        SimpleButtonInit.Enabled = True
        SimpleButtonStop.Enabled = False
    End Sub
    Private Sub FPCopy() 'Handles SimpleButtonFPCopy.Click
        Dim selectedRows As Integer() = GridView1.GetSelectedRows()
        Dim result As Object() = New Object(selectedRows.Length - 1) {}
        Dim LstMachineId As String
        'Dim adap As SqlDataAdapter
        'Dim adapA As OleDbDataAdapter
        'Dim dsRecord As DataSet = New DataSet
        'Dim sSql As String = "select * from tblVisitorTransaction where FV_No='" & TextEditPhone.Text.Trim & "'"
        'dsRecord = New DataSet
        'If Common.servername = "Access" Then
        '    adapA = New OleDbDataAdapter(sSql, Common.con1)
        '    adapA.Fill(dsRecord)
        'Else
        '    adap = New SqlDataAdapter(sSql, Common.con)
        '    adap.Fill(dsRecord)
        'End If

        For i = 0 To selectedRows.Length - 1
            Dim rowHandle As Integer = selectedRows(i)
            Dim commkey As Integer
            If Not GridView1.IsGroupRow(rowHandle) Then
                LstMachineId = GridView1.GetRowCellValue(rowHandle, "ID_NO").ToString.Trim
                commkey = Convert.ToDouble(GridView1.GetRowCellValue(rowHandle, "commkey").ToString.Trim)
                Dim sdwEnrollNumber As String
                Dim sName As String = ""
                Dim sPassword As String = ""
                Dim iPrivilege As Integer
                Dim idwFingerIndex As Integer
                Dim sTmpData As String = ""
                Dim sEnabled As String = ""
                Dim bEnabled As Boolean = False
                Dim iflag As Integer

                Dim lpszIPAddress As String = GridView1.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim 'Trim(rstm!Location)
                Dim vpszIPAddress As String = Trim(lpszIPAddress)

                Dim bIsConnected = False
                Dim iMachineNumber As Integer = result(i)
                Dim idwErrorCode As Integer
                Dim com As Common = New Common
                Dim axCZKEM1 As New zkemkeeper.CZKEM
                axCZKEM1.SetCommPassword(commkey)  'to check device commkey and db commkey matches
                bIsConnected = axCZKEM1.Connect_Net(vpszIPAddress, 4370)
                If bIsConnected = True Then
                    axCZKEM1.EnableDevice(iMachineNumber, False)
                    XtraMasterTest.LabelControlStatus.Text = "Uploading Template " & TextEditPhone.Text.Trim
                    Application.DoEvents()

                    sdwEnrollNumber = TextEditPhone.Text.Trim 'Convert.ToInt32(RsFp.Tables(0).Rows(0)("enrollnumber").ToString())
                    sName = TextEditName.Text.Trim ' dstmp.Tables(0).Rows(0)("EMPNAME").ToString().Trim()
                    idwFingerIndex = 6 ' Convert.ToInt32(RsFp.Tables(0).Rows(0)("FingerNumber").ToString().Trim())
                    sTmpData = strBase64 'Encoding.UTF8.GetString(dsRecord.Tables(0).Rows(0).Item("vFingerImage"), 0, dsRecord.Tables(0).Rows(0).Item("vFingerImage").Length) 'dsRecord.Tables(0).Rows(0).Item("vFingerImage")
                    'Dim sTmpDataByte() As Byte = dsRecord.Tables(0).Rows(0).Item("vFingerImage")
                    iPrivilege = 0 'Convert.ToInt32(RsFp.Tables(0).Rows(0)("Privilege").ToString().Trim()) 'told by ajitesh(used for admin)
                    sPassword = "" 'RsFp.Tables(0).Rows(0)("Password").ToString().Trim()
                    sEnabled = "true" 'RsFp.Tables(0).Rows(0)(6).ToString().Trim()
                    iflag = 0 'Convert.ToInt32(RsFp.Tables(0).Rows(0)(7).ToString())' as per old s/w
                    'Dim icard As String = "" 'RsFp.Tables(0).Rows(0)("CardNumber").ToString().Trim()
                    If sEnabled.ToString().ToLower() = "true" Then
                        bEnabled = True
                    Else
                        bEnabled = False
                    End If
                    Dim iBackupNumber As String
                    Dim rs As Integer
                    Dim strCardno = ""
                    If TextEditCard.Text.Trim <> "" Then
                        axCZKEM1.SetStrCardNumber(TextEditCard.Text.Trim)
                    Else
                        axCZKEM1.SetStrCardNumber(strCardno)
                    End If
                    If axCZKEM1.SSR_SetUserInfo(iMachineNumber, sdwEnrollNumber, sName, sPassword, iPrivilege, bEnabled) Then 'upload user information to the device
                        If picFPImg.EditValue IsNot Nothing Then
                            rs = axCZKEM1.SetUserTmpExStr(iMachineNumber, sdwEnrollNumber, idwFingerIndex, iflag, sTmpData) 'upload templates information to the device
                        End If


                        axCZKEM1.SetUserValidDate(iMachineNumber, sdwEnrollNumber, 1, 0, DateEditStart.DateTime.ToString("yyyy-MM-dd HH:mm:00"), DateEditEnd.DateTime.ToString("yyyy-MM-dd HH:mm:59"))  'add valdity
                        axCZKEM1.RefreshData(iMachineNumber)
                        'for time zone validity
                        'Dim sSunTime As String = "00" & "00" & "00" & "00"
                        'Dim sMonTime As String = "00" & "00" & "00" & "00"
                        'Dim sTueTime As String = "00" & "00" & "00" & "00"
                        'Dim sWenTime As String = "00" & "00" & "00" & "00"
                        'Dim sThuTime As String = "00" & "00" & "00" & "00"
                        'Dim sFriTime As String = "00" & "00" & "00" & "00"
                        'Dim sSatTime As String = "00" & "00" & "00" & "00"

                        'If Now.DayOfWeek = DayOfWeek.Sunday Then
                        '    sSunTime = DateEditStart.DateTime.ToString("HHmm") & DateEditEnd.DateTime.ToString("HHmm")
                        'ElseIf Now.DayOfWeek = DayOfWeek.Monday Then
                        '    sMonTime = DateEditStart.DateTime.ToString("HHmm") & DateEditEnd.DateTime.ToString("HHmm")
                        'ElseIf Now.DayOfWeek = DayOfWeek.Tuesday Then
                        '    sTueTime = DateEditStart.DateTime.ToString("HHmm") & DateEditEnd.DateTime.ToString("HHmm")
                        'ElseIf Now.DayOfWeek = DayOfWeek.Wednesday Then
                        '    sWenTime = DateEditStart.DateTime.ToString("HHmm") & DateEditEnd.DateTime.ToString("HHmm")
                        'ElseIf Now.DayOfWeek = DayOfWeek.Thursday Then
                        '    sThuTime = DateEditStart.DateTime.ToString("HHmm") & DateEditEnd.DateTime.ToString("HHmm")
                        'ElseIf Now.DayOfWeek = DayOfWeek.Friday Then
                        '    sFriTime = DateEditStart.DateTime.ToString("HHmm") & DateEditEnd.DateTime.ToString("HHmm")
                        'ElseIf Now.DayOfWeek = DayOfWeek.Saturday Then
                        '    sSatTime = DateEditStart.DateTime.ToString("HHmm") & DateEditEnd.DateTime.ToString("HHmm")
                        'End If
                        'Dim iTimeZoneID As Integer = 0
                        'Dim sTimeZone As String = sSunTime & sMonTime & sTueTime & sWenTime & sThuTime & sFriTime & sSatTime
                        'axCZKEM1.SetTZInfo(iMachineNumber, iTimeZoneID, sTimeZone)
                    Else
                        axCZKEM1.GetLastError(idwErrorCode)
                        'MsgBox("Operation failed,ErrorCode=" & idwErrorCode.ToString(), MsgBoxStyle.Exclamation, "Error")
                        axCZKEM1.EnableDevice(iMachineNumber, True)
                        'Cursor = Cursors.Default
                        Return
                    End If
                    axCZKEM1.EnableDevice(iMachineNumber, True)
                Else
                    axCZKEM1.GetLastError(idwErrorCode)
                    Continue For
                End If
            End If
        Next
        XtraMasterTest.LabelControlStatus.Text = ""
        Application.DoEvents()
    End Sub
    Public Function imageToByteArray(ByVal imageIn As System.Drawing.Image) As Byte()
        Dim ms As MemoryStream = New MemoryStream
        imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp)
        Return ms.ToArray
    End Function
    Private Sub SimpleButtonFPCopy_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonFPCopy.Click
        FPCopy()
    End Sub
    Private Sub printVisitor()
        Vimage = VisitorPictureEdit.Image
        VName = TextEditName.Text.Trim
        VCom = TextEditVComp.Text.Trim
        VToMeet = LookUpEditEmp.EditValue.ToString '("Empname").ToString.Trim
        VDept = LookUpEditDept.EditValue.ToString 'GetColumnValue("DEPARTMENTNAME").ToString.Trim
        VDate = Now.ToString("yyyy-MM-dd")
        VTime = TextEditInTime.Text.Trim
        VSrNo = TextEditSlNo.Text.Trim
        VCovistor2 = TextEditCoVisi2.Text.Trim
        VPhone = TextEditPhone.Text.Trim
        XtraVisitorPrint.ShowDialog()
    End Sub
End Class
