﻿Imports System.Data.OleDb
Imports System.Data.SqlClient
Imports System.IO
Imports System.Net
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports Newtonsoft.Json

Public Class XtraAppInfo
    Dim ulf As UserLookAndFeel
    Public Sub New()
        InitializeComponent()
    End Sub
    Private Sub SimpleButton2_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton2.Click
        Me.Close()
    End Sub

    Private Sub XtraAppInfo_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True
        
        lblVersion.Text = getVersion()
        lblKey.Text = Common.LicenseStr
        'If Common.LicenseStr = "" Then
        '    lblType.Text = "Standard"
        'Else
        '    lblType.Text = "Licenced"
        'End If

        If license.TrialPerioad = True And license.TrialExpired = False Then
            lblType.Text = "Trial Version. Remaining  Days: " & 30 - Now.Subtract(license.InstallDate).Days & "  "
        Else
            lblType.Text = "Standard"
        End If

        Try
            lblInstDate.Text = Convert.ToDateTime(Common.InstalledDate).ToString("dd MMM yyyy HH:mm")
        Catch ex As Exception
            lblInstDate.Text = Common.InstalledDate
        End Try
        Try
            lblUpdDate.Text = Convert.ToDateTime(Common.LastUpdated).ToString("dd MMM yyyy HH:mm")
        Catch ex As Exception
            lblUpdDate.Text = Common.LastUpdated
        End Try

    End Sub

    Private Sub SimpleButton1_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton1.Click
        Dim errorMsg As String = ""
        Dim newVersion As String = ""
        If checkUpdate(errorMsg, newVersion) Then
            If XtraMessageBox.Show(ulf, "<size=10>Newer Version Is Available. " & vbCrLf & "Do you wish to download " & newVersion & "?</size>", "<size=9>Confirm</size>",
                              MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No Then
                Exit Sub
            Else
                Application.Exit()
                Process.Start(My.Application.Info.DirectoryPath & "\iASUpdater.exe")
            End If
        Else
            If errorMsg = "" Then
                XtraMessageBox.Show(ulf, "<size=10>Newer Version Is Already Installed.</size>", "<size=10>iAS</size>")
            Else
                XtraMessageBox.Show(ulf, "<size=10>" & errorMsg & "</size>", "<size=10>iAS</size>")
            End If
            Me.Close()
        End If
    End Sub
    Public Class VersionInfo
        Public Property GetVsersionResult As String
    End Class
    Public Shared Function checkUpdate(ByRef errorMsg As String, ByRef newVersion As String) As Boolean
        Try
            Dim proxy As WebClient = New WebClient
            Dim serviceURL As String = String.Format("http://137.59.201.60:5011/Service1.svc/GetVsersion/Current_Version_Merged")
            Dim data() As Byte = proxy.DownloadData(serviceURL)
            Dim stream As Stream = New MemoryStream(data)
            Dim OutPut As String = System.Text.Encoding.UTF8.GetString(data)
            Dim JsonCardInfoCount As VersionInfo = New VersionInfo()
            JsonCardInfoCount = JsonConvert.DeserializeObject(Of VersionInfo)(OutPut)
            Dim ServerVersion As String = JsonCardInfoCount.GetVsersionResult
            If XtraAppInfo.getVersion <> ServerVersion Then
                newVersion = ServerVersion
                errorMsg = ""
                Return True
            Else
                errorMsg = ""
                Return False
            End If
        Catch ex As Exception
            errorMsg = ex.Message.ToString.Trim
            Return False
        End Try
    End Function
    Public Shared Function getVersion() As String
        Try
            Return FileVersionInfo.GetVersionInfo(Application.ExecutablePath).FileVersion
        Catch ex As Exception
            Return ""
        End Try
    End Function
End Class