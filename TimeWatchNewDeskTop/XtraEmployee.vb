﻿Imports System.Resources
Imports System.Globalization
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports System.ComponentModel
Imports DevExpress.XtraGrid.Columns
Imports DevExpress.XtraGrid.Views.Base
Imports System.Data.SqlClient
Imports System.Data.OleDb

Public Class XtraEmployee
    'Dim res_man As ResourceManager     'declare Resource manager to access to specific cultureinfo
    'Dim cul As CultureInfo     'declare culture info
    'Dim servername As String
    'Dim ConnectionString As String
    Dim ulf As UserLookAndFeel
    Public Shared EmpId As String
    'Dim con As SqlConnection
    'Dim con1 As OleDbConnection
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    Public Shared EmpQuickList As New List(Of String)()

    Public Sub New()
        InitializeComponent()
        If Common.servername = "Access" Then
            'Me.TblEmployee1TableAdapter1.Fill(Me.SSSDBDataSet.TblEmployee1)
            'GridControl1.DataSource = SSSDBDataSet.TblEmployee1
        Else
            TblEmployeeTableAdapter.Connection.ConnectionString = Common.ConnectionString '"Data Source=DESKTOP-PG6NDV8;Initial Catalog=SSSDB;Integrated Security=True"           
            'Me.TblEmployeeTableAdapter.Fill(Me.SSSDBDataSet.TblEmployee)
            'GridControl1.DataSource = SSSDBDataSet.TblEmployee
        End If
        Common.SetGridFont(GridView1, New Font("Tahoma", 11))
    End Sub
    Private Sub XtraEmployee_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        'for xtramessage box
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True

        'Me.Width = My.Computer.Screen.WorkingArea.Width
        'SplitContainerControl1.Width = My.Computer.Screen.WorkingArea.Width
        'SplitContainerControl1.SplitterPosition = (My.Computer.Screen.WorkingArea.Width) * 90 / 100

        Me.Width = Common.NavWidth 'Me.Parent.Width
        Me.Height = Common.NavHeight 'Me.Parent.Height
        'SplitContainerControl1.Width = Common.splitforMasterMenuWidth 'SplitContainerControl1.Parent.Width
        SplitContainerControl1.SplitterPosition = Common.SplitterPosition '(SplitContainerControl1.Parent.Width) * 85 / 100

        'res_man = New ResourceManager("TimeWatchNewDeskTop.Res", GetType(XtraEmployee).Assembly)
        'cul = CultureInfo.CreateSpecificCulture("en")

        'If Common.servername = "Access" Then   'to refresh the grid
        '    Me.TblEmployee1TableAdapter1.Fill(Me.SSSDBDataSet.TblEmployee1)
        'Else
        '    Me.TblEmployeeTableAdapter.Fill(Me.SSSDBDataSet.TblEmployee)          
        'End If

        Common.loadEmp()
        loadEmp()

        If Common.EmpDel <> "Y" Then
            GridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = False
        Else
            GridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = True
        End If
        If Common.IsNepali = "Y" Then
            colValidityEndDate.Visible = False
            colValidityStartDate.Visible = False
        Else
            colValidityEndDate.Visible = True
            colValidityStartDate.Visible = True
        End If
        If Common.ApplicationType = "C" Then
            BarButtonItem1.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
            BarButtonItem5.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        End If
    End Sub
    Private Sub loadEmp()
        GridControl1.DataSource = Common.EmpNonAdmin
        ' If Common.USERTYPE <> "A" Then
        '    If Common.servername = "Access" Then   'to refresh the grid
        '        Me.TblEmployee1TableAdapter1.Fill(Me.SSSDBDataSet.TblEmployee1)
        '        GridControl1.DataSource = SSSDBDataSet.TblEmployee1
        '    Else
        '        Me.TblEmployeeTableAdapter.Fill(Me.SSSDBDataSet.TblEmployee)
        '        GridControl1.DataSource = SSSDBDataSet.TblEmployee
        '    End If
        'Else


        'Dim emp() As String = Common.Auth_Branch.Split(",")
        'Dim ls As New List(Of String)()
        'For x As Integer = 0 To emp.Length - 1
        '    ls.Add(emp(x).Trim)
        'Next
        'Dim gridselet As String = "select * from TBLEmployee where BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "')"
        'If Common.servername = "Access" Then
        '    Dim dataAdapter As New OleDbDataAdapter(gridselet, Common.con1)
        '    Dim WTDataTable As New DataTable("TBLEmployee")
        '    dataAdapter.Fill(WTDataTable)
        '    GridControl1.DataSource = WTDataTable
        'Else
        '    Dim dataAdapter As New SqlClient.SqlDataAdapter(gridselet, Common.con)
        '    Dim WTDataTable As New DataTable("TBLEmployee")
        '    dataAdapter.Fill(WTDataTable)
        '    GridControl1.DataSource = WTDataTable
        'End If
        'End If
    End Sub
    Private Sub GridView1_InitNewRow(sender As System.Object, e As DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs) Handles GridView1.InitNewRow
        Dim view As GridView = CType(sender, GridView)
        view.SetRowCellValue(e.RowHandle, "LastModifiedDate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"))
        view.SetRowCellValue(e.RowHandle, "LastModifiedBy", "admin")
    End Sub
    Private Sub GridView1_RowDeleted(sender As System.Object, e As DevExpress.Data.RowDeletedEventArgs) Handles GridView1.RowDeleted
        Me.TblEmployeeTableAdapter.Update(Me.SSSDBDataSet.TblEmployee)
        Me.TblEmployee1TableAdapter1.Update(Me.SSSDBDataSet.TblEmployee1)
        XtraMessageBox.Show(ulf, "<size=10>" & Common.res_man.GetString("deletesuccess", Common.cul) & "</size>", Common.res_man.GetString("msgsuccess", Common.cul))
    End Sub
    Private Sub GridView1_RowUpdated(sender As System.Object, e As DevExpress.XtraGrid.Views.Base.RowObjectEventArgs) Handles GridView1.RowUpdated
        Me.TblEmployeeTableAdapter.Update(Me.SSSDBDataSet.TblEmployee)
        Me.TblEmployee1TableAdapter1.Update(Me.SSSDBDataSet.TblEmployee1)
    End Sub
    Private Sub GridControl1_EmbeddedNavigator_ButtonClick(sender As System.Object, e As DevExpress.XtraEditors.NavigatorButtonClickEventArgs) Handles GridControl1.EmbeddedNavigator.ButtonClick
        If e.Button.ButtonType = DevExpress.XtraEditors.NavigatorButtonType.Remove Then
            'Try
            If XtraMessageBox.Show(ulf, "<size=10>" & Common.res_man.GetString("askdelete", Common.cul) & "</size>", Common.res_man.GetString("confirmdeleteion", Common.cul), _
                              MessageBoxButtons.YesNo, MessageBoxIcon.Question) <> DialogResult.Yes Then
                Me.Validate()
                e.Handled = True
                'MsgBox("Your records have been saved and updated successfully!")
            Else
                e.Handled = True
                Me.Cursor = Cursors.WaitCursor
                Dim selectedRows As Integer() = GridView1.GetSelectedRows()
                Dim result As Object() = New Object(selectedRows.Length - 1) {}
                For i = 0 To selectedRows.Length - 1
                    Dim rowHandle As Integer = selectedRows(i)
                    If Not GridView1.IsGroupRow(rowHandle) Then
                        Dim paycode As String = GridView1.GetRowCellValue(rowHandle, "PAYCODE").ToString.Trim
                        XtraMasterTest.LabelControlStatus.Text = "Deleting Employee " & paycode
                        Application.DoEvents()
                        ' Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
                        If Common.servername = "Access" Then
                            If Common.con1.State <> ConnectionState.Open Then
                                Common.con1.Open()
                            End If
                            cmd1 = New OleDbCommand("delete from tblEmployee where PAYCODE='" & paycode & "'", Common.con1)
                            cmd1.ExecuteNonQuery()

                            cmd1 = New OleDbCommand("delete from tblEmployeeShiftMaster where PAYCODE='" & paycode & "'", Common.con1)
                            cmd1.ExecuteNonQuery()

                            cmd1 = New OleDbCommand("delete from tblLeaveLedger where PAYCODE='" & paycode & "'", Common.con1)
                            cmd1.ExecuteNonQuery()

                            cmd1 = New OleDbCommand("delete from tblTimeRegister where PAYCODE='" & paycode & "'", Common.con1)
                            cmd1.ExecuteNonQuery()

                            cmd1 = New OleDbCommand("delete from MachineRawPunch where PAYCODE='" & paycode & "'", Common.con1)
                            cmd1.ExecuteNonQuery()

                            cmd1 = New OleDbCommand("delete from MachineRawPunchAll where PAYCODE='" & paycode & "'", Common.con1)
                            cmd1.ExecuteNonQuery()

                            cmd1 = New OleDbCommand("delete from LeaveApplication where PAYCODE='" & paycode & "'", Common.con1)
                            cmd1.ExecuteNonQuery()

                            cmd1 = New OleDbCommand("DELETE from Pay_Master where PAYCODE = '" & paycode & "'", Common.con1)
                            cmd1.ExecuteNonQuery()

                            cmd1 = New OleDbCommand("DELETE from PAY_RESULT where PAYCODE = '" & paycode & "'", Common.con1)
                            cmd1.ExecuteNonQuery()

                            Common.con1.Close()
                        Else
                            If Common.con.State <> ConnectionState.Open Then
                                Common.con.Open()
                            End If

                            cmd = New SqlCommand("delete from tblEmployee where PAYCODE='" & paycode & "'", Common.con)
                            cmd.ExecuteNonQuery()

                            cmd = New SqlCommand("delete from tblEmployeeShiftMaster where PAYCODE='" & paycode & "'", Common.con)
                            cmd.ExecuteNonQuery()

                            cmd = New SqlCommand("delete from tblLeaveLedger where PAYCODE='" & paycode & "'", Common.con)
                            cmd.ExecuteNonQuery()

                            cmd = New SqlCommand("delete from tblTimeRegister where PAYCODE='" & paycode & "'", Common.con)
                            cmd.ExecuteNonQuery()

                            cmd = New SqlCommand("delete from MachineRawPunch where PAYCODE='" & paycode & "'", Common.con)
                            cmd.ExecuteNonQuery()

                            cmd = New SqlCommand("delete from MachineRawPunchAll where PAYCODE='" & paycode & "'", Common.con)
                            cmd.ExecuteNonQuery()

                            cmd = New SqlCommand("delete from LeaveApplication where PAYCODE='" & paycode & "'", Common.con)
                            cmd.ExecuteNonQuery()

                            cmd = New SqlCommand("DELETE from Pay_Master where PAYCODE = '" & paycode & "'", Common.con)
                            cmd.ExecuteNonQuery()

                            cmd = New SqlCommand("DELETE from PAY_RESULT where PAYCODE = '" & paycode & "'", Common.con)
                            cmd.ExecuteNonQuery()

                            Common.con.Close()
                        End If
                        If System.IO.File.Exists("./EmpImages/" & paycode & ".jpg") = True Then
                            My.Computer.FileSystem.DeleteFile("./EmpImages/" & paycode & ".jpg")
                        End If
                        Common.LogPost("Employee Delete;  Paycode='" & paycode)
                    End If
                Next
                XtraMasterTest.LabelControlStatus.Text = ""
                Application.DoEvents()
                Common.loadEmp()
                loadEmp()
                'If Common.servername = "Access" Then
                '    Me.TblEmployee1TableAdapter1.Fill(Me.SSSDBDataSet.TblEmployee1)
                '    GridControl1.DataSource = SSSDBDataSet.TblEmployee1
                'Else
                '    TblEmployeeTableAdapter.Connection.ConnectionString = Common.ConnectionString '"Data Source=DESKTOP-PG6NDV8;Initial Catalog=SSSDB;Integrated Security=True"
                '    Me.TblEmployeeTableAdapter.Fill(Me.SSSDBDataSet.TblEmployee)
                '    GridControl1.DataSource = SSSDBDataSet.TblEmployee
                'End If
                Me.Cursor = Cursors.Default
                XtraMessageBox.Show(ulf, "<size=10>" & Common.res_man.GetString("deletesuccess", Common.cul) & "</size>", Common.res_man.GetString("msgsuccess", Common.cul))
            End If
            'Catch ex As Exception

            'End Try
        End If
    End Sub
    Private Sub GridView1_EditFormShowing(sender As System.Object, e As DevExpress.XtraGrid.Views.Grid.EditFormShowingEventArgs) Handles GridView1.EditFormShowing
        If GridView1.IsNewItemRow(GridView1.FocusedRowHandle) = True Then
            If Common.EmpAdd <> "Y" Then
                e.Allow = False
                Exit Sub
            End If
        Else
            If Common.EmpModi <> "Y" Then
                e.Allow = False
                Exit Sub
            End If
        End If

        If Common.EmpNonAdmin.Rows.Count >= license.NoOfUsers Then
            XtraMessageBox.Show(ulf, "<size=10>Cannot add new Employee. Max Employee Limite Reached.</size>", "<size=9>iAS</size>")
            e.Allow = False
            Exit Sub
        End If

        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
        Try
            EmpId = row("PAYCODE").ToString.Trim
        Catch ex As Exception
            EmpId = ""
        End Try
        e.Allow = False

        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim ds As DataSet
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter("select count(*) from tblCompany", Common.con1)
            ds = New DataSet
            adapA.Fill(ds)
            If ds.Tables(0).Rows(0).Item(0).ToString = 0 Then
                XtraMessageBox.Show(ulf, "<size=10>Company master is empty. Cannot add new employee</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If

            adapA = New OleDbDataAdapter("select count(*) from tblbranch", Common.con1)
            ds = New DataSet
            adapA.Fill(ds)
            If ds.Tables(0).Rows(0).Item(0).ToString = 0 Then
                XtraMessageBox.Show(ulf, "<size=10>Location master is empty. Cannot add new employee</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If

            adapA = New OleDbDataAdapter("select count(*) from tblDepartment", Common.con1)
            ds = New DataSet
            adapA.Fill(ds)
            If ds.Tables(0).Rows(0).Item(0).ToString = 0 Then
                XtraMessageBox.Show(ulf, "<size=10>Department master is empty. Cannot add new employee</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If

            adapA = New OleDbDataAdapter("select count(*) from tblCatagory", Common.con1)
            ds = New DataSet
            adapA.Fill(ds)
            If ds.Tables(0).Rows(0).Item(0).ToString = 0 Then
                XtraMessageBox.Show(ulf, "<size=10>Catagory master is empty. Cannot add new employee</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If

            adapA = New OleDbDataAdapter("select count(*) from EmployeeGroup", Common.con1)
            ds = New DataSet
            adapA.Fill(ds)
            If ds.Tables(0).Rows(0).Item(0).ToString = 0 Then
                XtraMessageBox.Show(ulf, "<size=10>Employee Group master is empty. Cannot add new employee</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If

            adapA = New OleDbDataAdapter("select count(*) from tblGrade", Common.con1)
            ds = New DataSet
            adapA.Fill(ds)
            If ds.Tables(0).Rows(0).Item(0).ToString = 0 Then
                XtraMessageBox.Show(ulf, "<size=10>Grade master is empty. Cannot add new employee</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If

        Else
            adap = New SqlDataAdapter("select count(*) from tblCompany", Common.con)
            ds = New DataSet
            adap.Fill(ds)
            If ds.Tables(0).Rows(0).Item(0).ToString = 0 Then
                XtraMessageBox.Show(ulf, "<size=10>Company master is empty. Cannot add new employee</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If

            adap = New SqlDataAdapter("select count(*) from tblbranch", Common.con)
            ds = New DataSet
            adap.Fill(ds)
            If ds.Tables(0).Rows(0).Item(0).ToString = 0 Then
                XtraMessageBox.Show(ulf, "<size=10>Location master is empty. Cannot add new employee</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If

            adap = New SqlDataAdapter("select count(*) from tblDepartment", Common.con)
            ds = New DataSet
            adap.Fill(ds)
            If ds.Tables(0).Rows(0).Item(0).ToString = 0 Then
                XtraMessageBox.Show(ulf, "<size=10>Department master is empty. Cannot add new employee</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If

            adap = New SqlDataAdapter("select count(*) from tblCatagory", Common.con)
            ds = New DataSet
            adap.Fill(ds)
            If ds.Tables(0).Rows(0).Item(0).ToString = 0 Then
                XtraMessageBox.Show(ulf, "<size=10>Catagory master is empty. Cannot add new employee</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If

            adap = New SqlDataAdapter("select count(*) from EmployeeGroup", Common.con)
            ds = New DataSet
            adap.Fill(ds)
            If ds.Tables(0).Rows(0).Item(0).ToString = 0 Then
                XtraMessageBox.Show(ulf, "<size=10>Employee Group master is empty. Cannot add new employee</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If

            adap = New SqlDataAdapter("select count(*) from tblGrade", Common.con)
            ds = New DataSet
            adap.Fill(ds)
            If ds.Tables(0).Rows(0).Item(0).ToString = 0 Then
                XtraMessageBox.Show(ulf, "<size=10>Grade master is empty. Cannot add new employee</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Exit Sub
            End If
        End If

        XtraEmployeeEdit.ShowDialog()
        Common.loadEmp()
        loadEmp()
        'If Common.servername = "Access" Then   'to refresh the grid
        '    Me.TblEmployee1TableAdapter1.Fill(Me.SSSDBDataSet.TblEmployee1)
        'Else
        '    Me.TblEmployeeTableAdapter.Fill(Me.SSSDBDataSet.TblEmployee)
        'End If
    End Sub
    Private Sub GridView1_CustomColumnDisplayText(sender As System.Object, e As DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs) Handles GridView1.CustomColumnDisplayText
        Dim DC As New DateConverter()
        'Try
        '    Dim view As ColumnView = TryCast(sender, ColumnView)
        '    If e.Column.FieldName = "DateOFBIRTH" Then
        '        Dim coll As String = Convert.ToDateTime(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "DateOFBIRTH")).ToString("yyyy-MM-dd HH:mm:ss")
        '        If coll = "1800-01-01 00:00:00" Then
        '            e.DisplayText = ""
        '        Else
        '            If Common.IsNepali = "Y" Then
        '                Dim doj As String = DC.ToBS(New Date(Convert.ToDateTime(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "DateOFBIRTH")).Year, Convert.ToDateTime(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "DateOFBIRTH")).Month, Convert.ToDateTime(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "DateOFBIRTH")).Day))
        '                Dim dojTmp() As String = doj.Split("-")
        '                e.DisplayText = dojTmp(2) & "/" & Common.NepaliMonth(dojTmp(1) - 1) & "/" & dojTmp(0)
        '            End If
        '        End If
        '    End If
        'Catch ex As Exception
        'End Try
        Try
            Dim view As ColumnView = TryCast(sender, ColumnView)
            If e.Column.FieldName = "DateOFJOIN" Then
                Dim coll As String = Convert.ToDateTime(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "DateOFJOIN")).ToString("yyyy-MM-dd HH:mm:ss")
                If coll = "1800-01-01 00:00:00" Then
                    e.DisplayText = ""
                Else
                    If Common.IsNepali = "Y" Then
                        Dim doj As String = DC.ToBS(New Date(Convert.ToDateTime(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "DateOFJOIN")).Year, Convert.ToDateTime(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "DateOFJOIN")).Month, Convert.ToDateTime(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "DateOFJOIN")).Day))
                        Dim dojTmp() As String = doj.Split("-")
                        e.DisplayText = dojTmp(2) & "/" & Common.NepaliMonth(dojTmp(1) - 1) & "/" & dojTmp(0)
                    End If
                End If
            End If
        Catch ex As Exception
        End Try
        'Try
        '    Dim view As ColumnView = TryCast(sender, ColumnView)
        '    If e.Column.FieldName = "Leavingdate" Then
        '        Dim coll As String = Convert.ToDateTime(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "Leavingdate")).ToString("yyyy-MM-dd HH:mm:ss")
        '        If coll = "1800-01-01 00:00:00" Then
        '            e.DisplayText = ""
        '        Else
        '            If Common.IsNepali = "Y" Then
        '                Dim doj As String = DC.ToBS(New Date(Convert.ToDateTime(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "Leavingdate")).Year, Convert.ToDateTime(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "Leavingdate")).Month, Convert.ToDateTime(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "Leavingdate")).Day))
        '                'e.DisplayText = doj.Day & "/" & Common.NepaliMonth(doj.Month - 1) & "/" & doj.Year
        '                Dim dojTmp() As String = doj.Split("-")
        '                e.DisplayText = dojTmp(2) & "/" & Common.NepaliMonth(dojTmp(1) - 1) & "/" & dojTmp(0)
        '            End If
        '        End If
        '    End If
        'Catch ex As Exception
        'End Try
        Try
            Dim view As ColumnView = TryCast(sender, ColumnView)
            If e.Column.FieldName = "ValidityStartDate" Then
                Dim coll As String = Convert.ToDateTime(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "ValidityStartDate")).ToString("yyyy-MM-dd HH:mm:ss")
                If coll = "1800-01-01 00:00:00" Then
                    e.DisplayText = ""
                Else
                    'If Common.IsNepali = "Y" Then
                    '    Dim doj As String = DC.ToBS(New Date(Convert.ToDateTime(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "ValidityStartDate")).Year, Convert.ToDateTime(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "ValidityStartDate")).Month, Convert.ToDateTime(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "ValidityStartDate")).Day))
                    '    'e.DisplayText = doj.Day & "/" & Common.NepaliMonth(doj.Month - 1) & "/" & doj.Year
                    '    Dim dojTmp() As String = doj.Split("-")
                    '    e.DisplayText = dojTmp(2) & "/" & Common.NepaliMonth(dojTmp(1) - 1) & "/" & dojTmp(0)
                    'End If
                End If
            End If
        Catch ex As Exception
        End Try
        Try
            Dim view As ColumnView = TryCast(sender, ColumnView)
            If e.Column.FieldName = "ValidityEndDate" Then
                Dim coll As String = Convert.ToDateTime(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "ValidityEndDate")).ToString("yyyy-MM-dd HH:mm:ss")
                If coll = "1800-01-01 00:00:00" Then
                    e.DisplayText = ""
                Else
                    'If Common.IsNepali = "Y" Then
                    '    Dim doj As String = DC.ToBS(New Date(Convert.ToDateTime(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "ValidityEndDate")).Year, Convert.ToDateTime(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "ValidityEndDate")).Month, Convert.ToDateTime(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "ValidityEndDate")).Day))
                    '    'e.DisplayText = doj.Day & "/" & Common.NepaliMonth(doj.Month - 1) & "/" & doj.Year
                    '    Dim dojTmp() As String = doj.Split("-")
                    '    e.DisplayText = dojTmp(2) & "/" & Common.NepaliMonth(dojTmp(1) - 1) & "/" & dojTmp(0)
                    'End If
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub
    Private Sub BarButtonItem3_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem3.ItemClick
        Master_EmployeeMasterExcel("")
    End Sub
    Sub Master_EmployeeMasterExcel(ByVal strsortorder As String)
        Dim filepath As String
        Dim dlg As New SaveFileDialog()
        ' Filter by Office Files
        dlg.Filter = "Office Files|*.xlsx;"
        dlg.ShowDialog()
        filepath = dlg.FileName
        Dim intPrintLineCounter As Integer
        Dim intFile As Integer
        Dim strsql As String
        Dim strDeptDivCode As String
        Dim strCurrentDeptDivCode As String
        Dim blnDeptAvailable As Boolean
        Dim MstrAddress2 As String
        Dim MstrAddress1 As String
        Dim strGuardianName As String '* 25
        Dim MstrAddress2_80 As String '* 80
        Dim MstrAddress1_80 As String '* 80
        Dim msrl As Integer
        'Dim xlapp As Excel.Application
        'Dim xlwb As Excel.Workbook
        'Dim xlst As Excel.Sheets
        Dim rowcnt As Integer
        'xlapp = CreateObject("Excel.Application")
        'xlwb = xlapp.Workbooks.Add

        Dim mDummy As Integer
        msrl = 0

        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet
        strsql = "Select tblEmployee.PresentCardNo,tblEmployee.PayCode,tblEmployee.empname,tblcompany.companyname," & _
          "tblDepartment.DepartmentName,tblgrade.gradename,tblcatagory.catagoryname," & _
          "tblEmployee.DateOfBirth,tblEmployee.Sex,tblEmployee.BloodGroup," & _
          "GuardianName,tblEmployee.Designation,tblEmployee.Address1," & _
          "tblEmployee.Address2,tblemployee.telephone2,tblEmployee.DateOfJoin,tblEmployee.Qualification," & _
          "tblEmployee.Experience,tblEmployee.Bus,tblCatagory.Cat," & _
          "tblCatagory.CatagoryName, EmployeeGroup.GroupName, tblbranch.BRANCHNAME as Location, TblEmployee.E_MAIL1," & _
          "TblEmployee.PINCODE1 as PIN, TblEmployee.TELEPHONE1 as Mobile, TblEmployee.TELEPHONE2 as UID, TblEmployee.PINCODE2 as PAN, " & _
          "TblEmployee.MachineCard " & _
          "From tblEmployee, tblCompany, tblDepartment, tblgrade, tblCatagory, EmployeeGroup, tblbranch " & _
          "Where tblgrade.gradeCode = tblEmployee.GradeCode And tblCatagory.Cat = tblEmployee.Cat And " & _
          "TblEmployee.EmployeeGroupId = EmployeeGroup.GroupId And tblEmployee.CompanyCode = tblCompany.CompanyCode And " & _
          "tblEmployee.DepartmentCode = tblDepartment.DepartmentCode and TblEmployee.BRANCHCODE=tblbranch.BRANCHCODE"
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(strsql, Common.con1)
            adapA.Fill(Rs_Report)
        Else
            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(Rs_Report)
        End If
        If Rs_Report.Tables(0).Rows.Count < 1 Then
            XtraMessageBox.Show(ulf, "<size=10>No Data Available For this Report.</size>", "<size=9>iAS</size>")
            Exit Sub
        End If

        rowcnt = 1
        rowcnt = rowcnt + 1

        Dim tbl As New Data.DataTable()
        Me.Cursor = Cursors.WaitCursor
        tbl = New Data.DataTable()
        tbl.Columns.Add("User Number", GetType(String))
        tbl.Columns.Add("PayCode", GetType(String))
        tbl.Columns.Add("Employee Name", GetType(String))
        tbl.Columns.Add("Guardian's Name", GetType(String))
        tbl.Columns.Add("Company", GetType(String))
        tbl.Columns.Add("Department", GetType(String))
        tbl.Columns.Add("Employee Group", GetType(String))
        tbl.Columns.Add("Grade", GetType(String))
        tbl.Columns.Add("Category", GetType(String))
        tbl.Columns.Add("Date Of Join", GetType(String))
        tbl.Columns.Add("Date Of Birth", GetType(String))
        tbl.Columns.Add("Designation", GetType(String))
        tbl.Columns.Add("Gender", GetType(String))
        tbl.Columns.Add("Location", GetType(String))
        tbl.Columns.Add("Email", GetType(String))
        tbl.Columns.Add("Address", GetType(String))
        tbl.Columns.Add("PIN", GetType(String))
        tbl.Columns.Add("Mobile", GetType(String))
        tbl.Columns.Add("UID", GetType(String))
        tbl.Columns.Add("PAN", GetType(String))
        tbl.Columns.Add("Machine Card", GetType(String))

        'With Rs_Report
        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1 '  Do While Not .EOF
            MstrAddress1 = ""
            MstrAddress2 = ""

            If Rs_Report.Tables(0).Rows(i).Item("address1").ToString.Trim <> "" Then
                For mDummy = 1 To 80
                    If Mid(Rs_Report.Tables(0).Rows(i).Item("address1").ToString.Trim, mDummy, 1) <> vbCr And Mid(Rs_Report.Tables(0).Rows(i).Item("address1").ToString.Trim, mDummy, 1) <> vbLf Then
                        MstrAddress1 = MstrAddress1 + Mid(Rs_Report.Tables(0).Rows(i).Item("address1").ToString.Trim, mDummy, 1)
                    Else
                        MstrAddress1 = MstrAddress1 + " "
                    End If
                Next mDummy
            Else
                MstrAddress1 = "" 'Space(80)
            End If
            MstrAddress1_80 = MstrAddress1
            If Rs_Report.Tables(0).Rows(i).Item("address2").ToString.Trim <> "" Then
                For mDummy = 1 To 80
                    If Mid(Rs_Report.Tables(0).Rows(i).Item("address2").ToString.Trim, mDummy, 1) <> vbCr And Mid(Rs_Report.Tables(0).Rows(i).Item("address2").ToString.Trim, mDummy, 1) <> vbLf Then
                        MstrAddress2 = MstrAddress2 + Mid(Rs_Report.Tables(0).Rows(i).Item("address2").ToString.Trim, mDummy, 1)
                    Else
                        MstrAddress2 = MstrAddress2 + " "
                    End If
                Next mDummy
            Else
                MstrAddress2 = "" ' Space(80)
            End If
            MstrAddress2_80 = MstrAddress2
            Dim address As String = MstrAddress1 & " " & MstrAddress2
            strGuardianName = ""
            If Rs_Report.Tables(0).Rows(i).Item("GuardianName").ToString.Trim <> "" Then strGuardianName = Rs_Report.Tables(0).Rows(i).Item("GuardianName").ToString.Trim
            'rowcnt = rowcnt + 1
            msrl = msrl + 1
            rowcnt = rowcnt + 1
            tbl.Rows.Add(Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim, _
                    Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim, _
                    Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim, _
                    strGuardianName, _
                    Rs_Report.Tables(0).Rows(i).Item("CompanyName").ToString.Trim, _
                    Rs_Report.Tables(0).Rows(i).Item("DepartmentName").ToString.Trim, _
                    Rs_Report.Tables(0).Rows(i).Item("GroupName").ToString.Trim, _
                    Rs_Report.Tables(0).Rows(i).Item("GradeName").ToString.Trim, _
                    Rs_Report.Tables(0).Rows(i).Item("CatagoryName").ToString.Trim, _
                    Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("DateOfJoin").ToString.Trim).ToString("dd/MM/yyyy"), _
                    Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("DateOfbirth").ToString.Trim).ToString("dd/MM/yyyy"), _
                    Rs_Report.Tables(0).Rows(i).Item("Designation").ToString.Trim, _
                    Rs_Report.Tables(0).Rows(i).Item("Sex").ToString.Trim, _
                    Rs_Report.Tables(0).Rows(i).Item("Location").ToString.Trim, _
                    Rs_Report.Tables(0).Rows(i).Item("E_MAIL1").ToString.Trim, _
                    address, _
                    Rs_Report.Tables(0).Rows(i).Item("PIN").ToString.Trim, _
                    Rs_Report.Tables(0).Rows(i).Item("Mobile").ToString.Trim, _
                    Rs_Report.Tables(0).Rows(i).Item("UID").ToString.Trim, _
                    Rs_Report.Tables(0).Rows(i).Item("PAN").ToString.Trim, _
                    Rs_Report.Tables(0).Rows(i).Item("MachineCard").ToString.Trim _
                    )
        Next
        'Dim reportName = My.Application.Info.DirectoryPath & "\Reports\Employee_" & Now.ToString("yyyyMMddHHmmss") & ".xlsx"
        Dim reportName = filepath
        GridControlExport.DataSource = tbl
        For i As Integer = 0 To tbl.Columns.Count - 1
            GridViewExport.Columns(i).Width = 100
        Next
        Application.DoEvents()
        Try
            GridControlExport.ExportToXlsx(reportName)
            Process.Start(reportName)
        Catch ex As Exception

        End Try

        GridViewExport.Columns.Clear()
        Me.Cursor = Cursors.Default

    End Sub
    Sub Master_EmployeeMasterExcelFull(ByVal strsortorder As String)
        Dim filepath As String
        Dim dlg As New SaveFileDialog()
        ' Filter by Office Files
        dlg.Filter = "Office Files|*.xlsx;"
        dlg.ShowDialog()
        filepath = dlg.FileName

        Dim intPrintLineCounter As Integer
        Dim intFile As Integer
        Dim strsql As String
        Dim strDeptDivCode As String
        Dim strCurrentDeptDivCode As String
        Dim blnDeptAvailable As Boolean
        Dim MstrAddress2 As String
        Dim MstrAddress1 As String
        Dim strGuardianName As String '* 25
        Dim MstrAddress2_80 As String '* 80
        Dim MstrAddress1_80 As String '* 80
        Dim msrl As Integer
        'Dim xlapp As Excel.Application
        'Dim xlwb As Excel.Workbook
        'Dim xlst As Excel.Sheets
        Dim rowcnt As Integer
        'xlapp = CreateObject("Excel.Application")
        'xlwb = xlapp.Workbooks.Add

        Dim mDummy As Integer
        msrl = 0

        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet
        strsql = "Select tblEmployee.PresentCardNo, " & _
                "tblEmployee.PayCode," & _
                "tblEmployee.empname," & _
                "tblEmployee.DateOfBirth," & _
                "tblEmployee.DateOfJoin," & _
                "tblEmployee.Sex," & _
                "tblEmployee.BloodGroup," & _
                "tblEmployee.GuardianName," & _
                "tblEmployee.Designation," & _
                "tblEmployee.Address1," & _
                "tblEmployee.Address2," & _
                "tblemployee.telephone2," & _
                "tblEmployee.Qualification," & _
                "tblEmployee.Experience," & _
                "tblEmployee.Bus," & _
                "TblEmployee.E_MAIL1," & _
                "TblEmployee.PINCODE1 as PIN, " & _
                "TblEmployee.TELEPHONE1 as Mobile, " & _
                "TblEmployee.TELEPHONE2 as UID, " & _
                "TblEmployee.PINCODE2 as PAN, " & _
                "TblEmployee.MachineCard," & _
                "TblEmployee.ISMARRIED," & _
                "TblEmployee.Leavingdate," & _
                "TblEmployee.LeavingReason," & _
                "TblEmployee.VehicleNo," & _
                "TblEmployee.PFNO," & _
                "TblEmployee.PF_NO," & _
                "TblEmployee.ESINO," & _
                "TblEmployee.BankAcc," & _
                "TblEmployee.bankCODE," & _
                "TblEmployee.DESPANSARYCODE," & _
                "TblEmployee.ValidityStartDate," & _
                "TblEmployee.ValidityEndDate," & _
                "tblcompany.companyname," & _
                "tblCatagory.Cat,tblCatagory.CatagoryName," & _
                "tblDepartment.DepartmentName,tblgrade.gradename,tblcatagory.catagoryname," & _
                "EmployeeGroup.GroupName, tblbranch.BRANCHNAME as Location  " & _
                " From tblEmployee, tblCompany, tblDepartment, tblgrade, tblCatagory, EmployeeGroup, tblbranch " & _
                " Where(tblgrade.gradeCode = tblEmployee.GradeCode And tblCatagory.Cat = tblEmployee.Cat And " & _
                " TblEmployee.EmployeeGroupId = EmployeeGroup.GroupId And tblEmployee.CompanyCode = tblCompany.CompanyCode And" & _
                " tblEmployee.DepartmentCode = tblDepartment.DepartmentCode And TblEmployee.BRANCHCODE = tblbranch.BRANCHCODE) "
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(strsql, Common.con1)
            adapA.Fill(Rs_Report)
        Else
            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(Rs_Report)
        End If
        If Rs_Report.Tables(0).Rows.Count < 1 Then
            XtraMessageBox.Show(ulf, "<size=10>No Data Available For this Report.</size>", "<size=9>iAS</size>")
            Exit Sub
        End If

        rowcnt = 1
        rowcnt = rowcnt + 1

        Dim tbl As New Data.DataTable()
        Me.Cursor = Cursors.WaitCursor
        tbl = New Data.DataTable()
        tbl.Columns.Add("User Number", GetType(String))
        tbl.Columns.Add("PayCode", GetType(String))
        tbl.Columns.Add("Employee Name", GetType(String))
        tbl.Columns.Add("Guardian's Name", GetType(String))
        tbl.Columns.Add("Company", GetType(String))
        tbl.Columns.Add("Department", GetType(String))
        tbl.Columns.Add("Employee Group", GetType(String))
        tbl.Columns.Add("Grade", GetType(String))
        tbl.Columns.Add("Category", GetType(String))
        tbl.Columns.Add("Date Of Join", GetType(String))
        tbl.Columns.Add("Date Of Birth", GetType(String))
        tbl.Columns.Add("Designation", GetType(String))
        tbl.Columns.Add("Gender", GetType(String))
        tbl.Columns.Add("Location", GetType(String))
        tbl.Columns.Add("Email", GetType(String))
        tbl.Columns.Add("Address", GetType(String))
        tbl.Columns.Add("PIN", GetType(String))
        tbl.Columns.Add("Mobile", GetType(String))
        tbl.Columns.Add("UID", GetType(String))
        tbl.Columns.Add("PAN", GetType(String))
        tbl.Columns.Add("Machine Card", GetType(String))


        tbl.Columns.Add("ISMARRIED", GetType(String))
        tbl.Columns.Add("Leavingdate", GetType(String))
        tbl.Columns.Add("LeavingReason", GetType(String))
        tbl.Columns.Add("VehicleNo", GetType(String))
        tbl.Columns.Add("PFNO", GetType(String))
        tbl.Columns.Add("PF_NO", GetType(String))
        tbl.Columns.Add("ESINO", GetType(String))
        tbl.Columns.Add("BankAcc", GetType(String))
        tbl.Columns.Add("bankCODE", GetType(String))
        tbl.Columns.Add("DESPANSARYCODE", GetType(String))
        tbl.Columns.Add("ValidityStartDate", GetType(String))
        tbl.Columns.Add("ValidityEndDate", GetType(String))
        'With Rs_Report
        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1 '  Do While Not .EOF
            MstrAddress1 = ""
            MstrAddress2 = ""

            If Rs_Report.Tables(0).Rows(i).Item("address1").ToString.Trim <> "" Then
                For mDummy = 1 To 80
                    If Mid(Rs_Report.Tables(0).Rows(i).Item("address1").ToString.Trim, mDummy, 1) <> vbCr And Mid(Rs_Report.Tables(0).Rows(i).Item("address1").ToString.Trim, mDummy, 1) <> vbLf Then
                        MstrAddress1 = MstrAddress1 + Mid(Rs_Report.Tables(0).Rows(i).Item("address1").ToString.Trim, mDummy, 1)
                    Else
                        MstrAddress1 = MstrAddress1 + " "
                    End If
                Next mDummy
            Else
                MstrAddress1 = "" 'Space(80)
            End If
            MstrAddress1_80 = MstrAddress1
            If Rs_Report.Tables(0).Rows(i).Item("address2").ToString.Trim <> "" Then
                For mDummy = 1 To 80
                    If Mid(Rs_Report.Tables(0).Rows(i).Item("address2").ToString.Trim, mDummy, 1) <> vbCr And Mid(Rs_Report.Tables(0).Rows(i).Item("address2").ToString.Trim, mDummy, 1) <> vbLf Then
                        MstrAddress2 = MstrAddress2 + Mid(Rs_Report.Tables(0).Rows(i).Item("address2").ToString.Trim, mDummy, 1)
                    Else
                        MstrAddress2 = MstrAddress2 + " "
                    End If
                Next mDummy
            Else
                MstrAddress2 = "" ' Space(80)
            End If
            MstrAddress2_80 = MstrAddress2
            Dim address As String = MstrAddress1 & " " & MstrAddress2
            strGuardianName = ""
            If Rs_Report.Tables(0).Rows(i).Item("GuardianName").ToString.Trim <> "" Then strGuardianName = Rs_Report.Tables(0).Rows(i).Item("GuardianName").ToString.Trim
            'rowcnt = rowcnt + 1
            msrl = msrl + 1
            rowcnt = rowcnt + 1

            Dim Leavingdate As String = ""
            Try
                If Rs_Report.Tables(0).Rows(i).Item("Leavingdate").ToString.Trim = "" Then : Leavingdate = "" : Else : Leavingdate = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("Leavingdate").ToString.Trim).ToString("dd/MM/yyyy") : End If
            Catch ex As Exception
            End Try

            Dim DateOfJoin As String = ""
            Try
                If Rs_Report.Tables(0).Rows(i).Item("DateOfJoin").ToString.Trim = "" Then : Leavingdate = "" : Else : Leavingdate = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("DateOfJoin").ToString.Trim).ToString("dd/MM/yyyy") : End If
            Catch ex As Exception
            End Try


            Dim DateOfbirth As String = ""
            Try
                If Rs_Report.Tables(0).Rows(i).Item("DateOfbirth").ToString.Trim = "" Then : Leavingdate = "" : Else : Leavingdate = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("DateOfbirth").ToString.Trim).ToString("dd/MM/yyyy") : End If
            Catch ex As Exception
            End Try

            Dim ValidityStartDate As String = ""
            Try
                If Rs_Report.Tables(0).Rows(i).Item("ValidityStartDate").ToString.Trim = "" Then : Leavingdate = "" : Else : Leavingdate = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("ValidityStartDate").ToString.Trim).ToString("dd/MM/yyyy") : End If
            Catch ex As Exception
            End Try

            Dim ValidityEndDate As String = ""
            Try
                If Rs_Report.Tables(0).Rows(i).Item("ValidityEndDate").ToString.Trim = "" Then : Leavingdate = "" : Else : Leavingdate = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("ValidityEndDate").ToString.Trim).ToString("dd/MM/yyyy") : End If
            Catch ex As Exception
            End Try

            tbl.Rows.Add(Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim, _
                    Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim, _
                    Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim, _
                    strGuardianName, _
                    Rs_Report.Tables(0).Rows(i).Item("CompanyName").ToString.Trim, _
                    Rs_Report.Tables(0).Rows(i).Item("DepartmentName").ToString.Trim, _
                    Rs_Report.Tables(0).Rows(i).Item("GroupName").ToString.Trim, _
                    Rs_Report.Tables(0).Rows(i).Item("GradeName").ToString.Trim, _
                    Rs_Report.Tables(0).Rows(i).Item("CatagoryName").ToString.Trim, _
                    DateOfJoin, _
                    DateOfbirth, _
                    Rs_Report.Tables(0).Rows(i).Item("Designation").ToString.Trim, _
                    Rs_Report.Tables(0).Rows(i).Item("Sex").ToString.Trim, _
                    Rs_Report.Tables(0).Rows(i).Item("Location").ToString.Trim, _
                    Rs_Report.Tables(0).Rows(i).Item("E_MAIL1").ToString.Trim, _
                    address, _
                    Rs_Report.Tables(0).Rows(i).Item("PIN").ToString.Trim, _
                    Rs_Report.Tables(0).Rows(i).Item("Mobile").ToString.Trim, _
                    Rs_Report.Tables(0).Rows(i).Item("UID").ToString.Trim, _
                    Rs_Report.Tables(0).Rows(i).Item("PAN").ToString.Trim, _
                    Rs_Report.Tables(0).Rows(i).Item("MachineCard").ToString.Trim, _
                    Rs_Report.Tables(0).Rows(i).Item("ISMARRIED").ToString.Trim, _
                    Leavingdate, _
                    Rs_Report.Tables(0).Rows(i).Item("LeavingReason").ToString.Trim, _
                    Rs_Report.Tables(0).Rows(i).Item("VehicleNo").ToString.Trim, _
                    Rs_Report.Tables(0).Rows(i).Item("PFNO").ToString.Trim, _
                    Rs_Report.Tables(0).Rows(i).Item("PF_NO").ToString.Trim, _
                    Rs_Report.Tables(0).Rows(i).Item("ESINO").ToString.Trim, _
                    Rs_Report.Tables(0).Rows(i).Item("BankAcc").ToString.Trim, _
                    Rs_Report.Tables(0).Rows(i).Item("bankCODE").ToString.Trim, _
                    Rs_Report.Tables(0).Rows(i).Item("DESPANSARYCODE").ToString.Trim, _
                    ValidityStartDate, _
                    ValidityEndDate _
                    )
        Next
        'Dim reportName = My.Application.Info.DirectoryPath & "\Reports\Employee_" & Now.ToString("yyyyMMddHHmmss") & ".xlsx"
        Dim reportName = filepath '& ".xlsx"
        GridViewExport.Columns.Clear()
        GridControlExport.DataSource = tbl
        For i As Integer = 0 To tbl.Columns.Count - 1
            Try
                GridViewExport.Columns(i).Width = 100
            Catch ex As Exception
            End Try
        Next
        Application.DoEvents()

        Try
            GridControlExport.ExportToXlsx(reportName)
            Process.Start(reportName)
        Catch ex As Exception
        End Try

        Me.Cursor = Cursors.Default
        GridViewExport.Columns.Clear()

    End Sub
    Private Sub BarButtonItem4_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem4.ItemClick
        XtraEmployeeUpload.ShowDialog()
        Me.Cursor = Cursors.WaitCursor
        Common.loadEmp()
        loadEmp()
        'If Common.servername = "Access" Then   'to refresh the grid
        '    Me.TblEmployee1TableAdapter1.Fill(Me.SSSDBDataSet.TblEmployee1)
        'Else
        '    Me.TblEmployeeTableAdapter.Fill(Me.SSSDBDataSet.TblEmployee)
        'End If
        Me.Cursor = Cursors.Default
    End Sub
    Private Sub BarButtonItem1_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem1.ItemClick
        XtraEmployeeCopyToDevice.ShowDialog()
        Me.Cursor = Cursors.WaitCursor
        Common.loadEmp()
        loadEmp()
        'If Common.servername = "Access" Then   'to refresh the grid
        '    Me.TblEmployee1TableAdapter1.Fill(Me.SSSDBDataSet.TblEmployee1)
        'Else
        '    Me.TblEmployeeTableAdapter.Fill(Me.SSSDBDataSet.TblEmployee)
        'End If
        Me.Cursor = Cursors.Default
    End Sub
    Private Sub BarButtonItem2_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem2.ItemClick
        XtraTimeZoneZK.ShowDialog()
        Me.Cursor = Cursors.WaitCursor
        Common.loadEmp()
        loadEmp()
        'If Common.servername = "Access" Then   'to refresh the grid
        '    Me.TblEmployee1TableAdapter1.Fill(Me.SSSDBDataSet.TblEmployee1)
        'Else
        '    Me.TblEmployeeTableAdapter.Fill(Me.SSSDBDataSet.TblEmployee)
        'End If
        Me.Cursor = Cursors.Default
    End Sub
    Private Sub BarButtonItem5_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem5.ItemClick
        XtraEmployeeCopyFromDevice.ShowDialog()
        Me.Cursor = Cursors.WaitCursor
        Common.loadEmp()
        loadEmp()
        'If Common.servername = "Access" Then   'to refresh the grid
        '    Me.TblEmployee1TableAdapter1.Fill(Me.SSSDBDataSet.TblEmployee1)
        'Else
        '    Me.TblEmployeeTableAdapter.Fill(Me.SSSDBDataSet.TblEmployee)
        'End If
        Me.Cursor = Cursors.Default
    End Sub
    Private Sub BarButtonItem6_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem6.ItemClick
        XtraBulkDelete.ShowDialog()
        Me.Cursor = Cursors.WaitCursor
        Common.loadEmp()
        loadEmp()
        'If Common.servername = "Access" Then   'to refresh the grid
        '    Me.TblEmployee1TableAdapter1.Fill(Me.SSSDBDataSet.TblEmployee1)
        'Else
        '    Me.TblEmployeeTableAdapter.Fill(Me.SSSDBDataSet.TblEmployee)
        'End If
        Me.Cursor = Cursors.Default
    End Sub
    Private Sub BarButtonItem9_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem9.ItemClick
        XtraTimeZoneBio.ShowDialog()
        Me.Cursor = Cursors.WaitCursor
        Common.loadEmp()
        loadEmp()
        'If Common.servername = "Access" Then   'to refresh the grid
        '    Me.TblEmployee1TableAdapter1.Fill(Me.SSSDBDataSet.TblEmployee1)
        'Else
        '    Me.TblEmployeeTableAdapter.Fill(Me.SSSDBDataSet.TblEmployee)
        'End If
        Me.Cursor = Cursors.Default
    End Sub
    Private Sub BarButtonItem10_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem10.ItemClick

        Dim selectedRows As Integer() = GridView1.GetSelectedRows()
        Dim result As Object() = New Object(selectedRows.Length - 1) {}
        For i = 0 To selectedRows.Length - 1
            Dim rowHandle As Integer = selectedRows(i)
            If Not GridView1.IsGroupRow(rowHandle) Then
                EmpQuickList.Add(GridView1.GetRowCellValue(rowHandle, "PAYCODE").ToString.Trim)
            End If
        Next
        If EmpQuickList.Count = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>No Employees Selected</size>", "<size=9>iAS</size>")
            XtraMasterTest.LabelControlStatus.Text = ""
            Exit Sub
        End If
        XtraEmployeeQuickEdit.ShowDialog()
        EmpQuickList.Clear()
        XtraMasterTest.LabelControlStatus.Text = ""
        Application.DoEvents()
        Common.loadEmp()
        loadEmp()
    End Sub
    Private Sub BarButtonItemImExport_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemImExport.ItemClick
        Master_EmployeeMasterExcel("")
        Common.LogPost("Employee Export As Per Import Format")
    End Sub
    Private Sub BarButtonItemFullExport_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItemFullExport.ItemClick
        Master_EmployeeMasterExcelFull("")
        Common.LogPost("Employee Full Export")
    End Sub
    Private Sub BarButtonItem11_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem11.ItemClick
        XtraEmployeeExperiedValidity.ShowDialog()
    End Sub

    Private Sub BarButtonItem12_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem12.ItemClick
        XtraEmployeeExcelToDevice.ShowDialog()
    End Sub
End Class
