﻿Imports System.Runtime.InteropServices
Imports System.Threading.Tasks
Imports System.IO
Imports System.Windows.Forms
Imports iAS.AcsDemo
Imports iAS.AscDemo

Public Class HikvisionTest
    Inherits Form
    Dim DeviceIP As String = Nothing
    Dim m_lUserID As Integer = 0
    Dim m_iDeviceIndex As Integer = 0
    Dim m_struAcsEventCond As CHCNetSDK.NET_DVR_ACS_EVENT_COND = New CHCNetSDK.NET_DVR_ACS_EVENT_COND
    Dim MinorType As String = Nothing
    Dim MajorType As String = Nothing
    Dim m_lLogNum As Integer = 0
    Dim m_lGetAcsEvent As Integer = 0
    'Dim g_formList As DeviceLogList = DeviceLogList.Instance
    Dim g_fGetAcsEventCallback As CHCNetSDK.RemoteConfigCallback = Nothing
    Dim CsTemp As String = Nothing
    Dim m_bInitSDK As Boolean = False
    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()
        m_bInitSDK = CHCNetSDK.NET_DVR_Init()
        If m_bInitSDK = False Then
            MsgBox("Init false")
        End If
        m_struAcsEventCond.Init()
        ' Add any initialization after the InitializeComponent() call.

    End Sub
    Private Sub HikvisionTest_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

    End Sub
    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        Dim cn As Common = New Common
        'Dim m_struDeviceInfo As CHCNetSDK.NET_DVR_DEVICEINFO_V30 = New CHCNetSDK.NET_DVR_DEVICEINFO_V30
        Dim struDeviceInfoV40 As CHCNetSDK.NET_DVR_DEVICEINFO_V40 = New CHCNetSDK.NET_DVR_DEVICEINFO_V40
        Dim DeviceAdd As String = "192.168.0.64", userName As String = "admin", pwd As String = "12345678a"
        Dim lUserID As Integer = -1
        Dim failReason
        Dim logistatus = cn.HikvisionLogin(DeviceAdd, userName, pwd, struDeviceInfoV40, lUserID, failReason)
        If logistatus = False Then
            MsgBox("connection fail")
            Return
        End If
        m_lUserID = lUserID
        Dim serialNo As String = System.Text.Encoding.UTF8.GetString(struDeviceInfoV40.struDeviceV30.sSerialNumber).TrimEnd(Microsoft.VisualBasic.ChrW(92))
        listViewEvent.Items.Clear()
        m_struAcsEventCond.dwSize = CType(Marshal.SizeOf(m_struAcsEventCond), UInteger)
        MajorType = "All" 'comboBoxMainType.SelectedItem.ToString
        m_struAcsEventCond.dwMajor = GetAcsEventType.ReturnMajorTypeValue(MajorType)
        MinorType = "All" 'comboBoxSecondType.SelectedItem.ToString
        m_struAcsEventCond.dwMinor = GetAcsEventType.ReturnMinorTypeValue(MinorType)
        m_struAcsEventCond.struStartTime.dwYear = 2019 'dateTimeStart.Value.Year
        m_struAcsEventCond.struStartTime.dwMonth = 3 'dateTimeStart.Value.Month
        m_struAcsEventCond.struStartTime.dwDay = 20 'dateTimeStart.Value.Day
        m_struAcsEventCond.struStartTime.dwHour = 21 'dateTimeStart.Value.Hour
        m_struAcsEventCond.struStartTime.dwMinute = 0 'dateTimeStart.Value.Minute
        m_struAcsEventCond.struStartTime.dwSecond = 1 'dateTimeStart.Value.Second
        m_struAcsEventCond.struEndTime.dwYear = 2019 ' dateTimeEnd.Value.Year
        m_struAcsEventCond.struEndTime.dwMonth = 3 'dateTimeEnd.Value.Month
        m_struAcsEventCond.struEndTime.dwDay = 24 'dateTimeEnd.Value.Day
        m_struAcsEventCond.struEndTime.dwHour = 22 'dateTimeEnd.Value.Hour
        m_struAcsEventCond.struEndTime.dwMinute = 40 'dateTimeEnd.Value.Minute
        m_struAcsEventCond.struEndTime.dwSecond = 0 'dateTimeEnd.Value.Second
        'If Not StrToByteArray(m_struAcsEventCond.byCardNo, textBoxCardNo.Text) Then
        If Not cn.StrToByteArray(m_struAcsEventCond.byCardNo, "") Then
            Return
        End If

        'If Not StrToByteArray(m_struAcsEventCond.byName, textBoxName.Text) Then
        If Not cn.StrToByteArray(m_struAcsEventCond.byName, "") Then
            Return
        End If

        'If (True = Me.checkBoxPicEnable.Checked) Then
        'm_struAcsEventCond.byPicEnable = 1
        'Else
        m_struAcsEventCond.byPicEnable = 0
        'End If

        Dim BeginSerialNo As UInteger = 0

        'UInteger.TryParse(textBoxBeginSerialNo.Text, BeginSerialNo)
        m_struAcsEventCond.dwBeginSerialNo = BeginSerialNo
        Dim EndSerialNo As UInteger = 0

        'UInteger.TryParse(textBoxEndSerialNo.Text, EndSerialNo)
        m_struAcsEventCond.dwEndSerialNo = EndSerialNo
        m_struAcsEventCond.dwIOTChannelNo = 0 ' UInteger.Parse(textBoxChanNo.Text)
        'If (comboBoxEventType.SelectedIndex = -1) Then
        '    m_struAcsEventCond.wInductiveEventType = 65535
        'Else
        '    m_struAcsEventCond.wInductiveEventType = GetAcsEventType.ReturnInductiveEventTypeValue(comboBoxEventType.SelectedItem.ToString)
        'End If
        m_struAcsEventCond.wInductiveEventType = GetAcsEventType.ReturnInductiveEventTypeValue("all")

        'If (comboBoxSearchType.SelectedIndex = -1) Then
        m_struAcsEventCond.bySearchType = 0
        'Else
        '    m_struAcsEventCond.bySearchType = CType(GetAcsEventType.ReturnSearchValue(comboBoxSearchType.SelectedItem.ToString), Byte)
        'End If

        m_struAcsEventCond.szMonitorID = "" 'textBoxMonitorID.Text
        'If (comboBoxTimeType.SelectedIndex <= 0) Then
        '    m_struAcsEventCond.byTimeType = 0
        'Else
        '    m_struAcsEventCond.byTimeType = 1
        'End If
        m_struAcsEventCond.byTimeType = 0
        'If Not StrToByteArray(m_struAcsEventCond.byEmployeeNo, textBoxEmployeeNo.Text) Then
        If Not cn.StrToByteArray(m_struAcsEventCond.byEmployeeNo, "") Then
            Return
        End If

        m_lLogNum = 0
        Dim dwSize As UInteger = CType(Marshal.SizeOf(m_struAcsEventCond), UInteger)
        Dim ptrCond As IntPtr = Marshal.AllocHGlobal(CType(dwSize, Integer))
        Marshal.StructureToPtr(m_struAcsEventCond, ptrCond, False)
        'Dim g_fGetAcsEventCallback As CHCNetSDK.RemoteConfigCallback = New CHCNetSDK.RemoteConfigCallback(AddressOf ProcessGetAcsEventCallback)
        g_fGetAcsEventCallback = New CHCNetSDK.RemoteConfigCallback(AddressOf ProcessGetAcsEventCallback)
        m_lGetAcsEvent = CHCNetSDK.NET_DVR_StartRemoteConfig(m_lUserID, CHCNetSDK.NET_DVR_GET_ACS_EVENT, ptrCond, CType(dwSize, Integer), g_fGetAcsEventCallback, Handle)
        If (-1 = m_lGetAcsEvent) Then
            'g_formList.AddLog(m_iDeviceIndex, AcsDemoPublic.OPERATION_FAIL_T, "NET_DVR_GET_ACS_EVENT")
            Marshal.FreeHGlobal(ptrCond)
        Else
            'g_formList.AddLog(m_iDeviceIndex, AcsDemoPublic.OPERATION_SUCC_T, "NET_DVR_GET_ACS_EVENT")
            Marshal.FreeHGlobal(ptrCond)
        End If
        logistatus = cn.HikvisionLogOut(lUserID)
        If logistatus = False Then
            MsgBox("logout fail")
            Return
        End If
    End Sub

    Private Sub ProcessGetAcsEventCallback(ByVal dwType As UInteger, ByVal lpBuffer As IntPtr, ByVal dwBufLen As UInteger, ByVal pUserData As IntPtr)
        If dwType = CUInt(CHCNetSDK.NET_SDK_CALLBACK_TYPE.NET_SDK_CALLBACK_TYPE_DATA) Then
            Dim lpAcsEventCfg As CHCNetSDK.NET_DVR_ACS_EVENT_CFG = New CHCNetSDK.NET_DVR_ACS_EVENT_CFG()
            lpAcsEventCfg = CType(Marshal.PtrToStructure(lpBuffer, GetType(CHCNetSDK.NET_DVR_ACS_EVENT_CFG)), CHCNetSDK.NET_DVR_ACS_EVENT_CFG)
            Dim ptrAcsEventCfg As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(lpAcsEventCfg))
            Marshal.StructureToPtr(lpAcsEventCfg, ptrAcsEventCfg, True)
            'AddAcsEventToList(lpAcsEventCfg)
            CHCNetSDK.PostMessage(pUserData, CHCNetSDK.WM_MSG_ADD_ACS_EVENT_TOLIST, CInt(ptrAcsEventCfg), 0)
        ElseIf dwType = CUInt(CHCNetSDK.NET_SDK_CALLBACK_TYPE.NET_SDK_CALLBACK_TYPE_STATUS) Then
            Dim dwStatus As Integer = Marshal.ReadInt32(lpBuffer)
            If dwStatus = CUInt(CHCNetSDK.NET_SDK_CALLBACK_STATUS_NORMAL.NET_SDK_CALLBACK_STATUS_SUCCESS) Then
                CHCNetSDK.PostMessage(pUserData, CHCNetSDK.WM_MSG_GET_ACS_EVENT_FINISH, 0, 0)
            ElseIf dwStatus = CUInt(CHCNetSDK.NET_SDK_CALLBACK_STATUS_NORMAL.NET_SDK_CALLBACK_STATUS_FAILED) Then
                'g_formList.AddLog(m_iDeviceIndex, AcsDemoPublic.OPERATION_FAIL_T, "NET_DVR_GET_ACS_EVENT failed")
            End If
        End If
    End Sub
    Protected Overrides Sub DefWndProc(ByRef m As System.Windows.Forms.Message)
        Select Case (m.Msg)
            Case CHCNetSDK.WM_MSG_ADD_ACS_EVENT_TOLIST
                Dim pAcsEventCfg As IntPtr = CType(m.WParam.ToInt32, IntPtr)
                Dim struEventCfg As CHCNetSDK.NET_DVR_ACS_EVENT_CFG = New CHCNetSDK.NET_DVR_ACS_EVENT_CFG
                struEventCfg = CType(Marshal.PtrToStructure(pAcsEventCfg, GetType(CHCNetSDK.NET_DVR_ACS_EVENT_CFG)), CHCNetSDK.NET_DVR_ACS_EVENT_CFG)
                Marshal.FreeHGlobal(pAcsEventCfg)
                AddAcsEventToList(struEventCfg)
            Case CHCNetSDK.WM_MSG_GET_ACS_EVENT_FINISH
                GetAcsEventFinish()
            Case Else
                MyBase.DefWndProc(m)
        End Select
    End Sub
    Private Sub GetAcsEventFinish()
        CHCNetSDK.NET_DVR_StopRemoteConfig(m_lGetAcsEvent)
        m_lGetAcsEvent = -1
        m_lLogNum = 0
        'g_formList.AddLog(m_iDeviceIndex, AcsDemoPublic.OPERATION_SUCC_T, "NET_DVR_GET_ACS_EVENT finish")
        Return
    End Sub
    Public Delegate Sub SetTextCallback(ByRef struEventCfg As CHCNetSDK.NET_DVR_ACS_EVENT_CFG)
    Private Sub AddAcsEventToList(ByRef struEventCfg As CHCNetSDK.NET_DVR_ACS_EVENT_CFG)
        If Me.listViewEvent.InvokeRequired = True Then
            Dim d As SetTextCallback = New SetTextCallback(AddressOf Me.AddAcsEventToList)
            Me.Invoke(d, New Object() {struEventCfg})
        Else
            Dim cn As Common = New Common
            Me.listViewEvent.BeginUpdate()
            Dim Item As ListViewItem = New ListViewItem
            Item.Text = (m_lLogNum + 1).ToString
            Dim LogTime As String = cn.GetStrLogTime(struEventCfg.struTime)
            Item.SubItems.Add(LogTime)
            Dim Major As String = cn.ProcessMajorType(struEventCfg.dwMajor)
            Item.SubItems.Add(Major)
            CsTemp = cn.ProcessMinorType(struEventCfg)
            Item.SubItems.Add(CsTemp)
            CsTemp = System.Text.Encoding.UTF8.GetString(struEventCfg.struAcsEventInfo.byCardNo)
            Item.SubItems.Add(CsTemp)
            CsTemp = cn.CardTypeMap(struEventCfg)
            Item.SubItems.Add(CsTemp)
            Item.SubItems.Add(struEventCfg.struAcsEventInfo.byWhiteListNo.ToString)
            'WhiteList
            CsTemp = cn.ProcessReportChannel(struEventCfg)
            Item.SubItems.Add(CsTemp)
            CsTemp = cn.ProcessCardReader(struEventCfg)
            Item.SubItems.Add(CsTemp)
            CsTemp = struEventCfg.struAcsEventInfo.dwCardReaderNo.ToString
            Item.SubItems.Add(CsTemp)
            Item.SubItems.Add(struEventCfg.struAcsEventInfo.dwDoorNo.ToString)
            Item.SubItems.Add(struEventCfg.struAcsEventInfo.dwVerifyNo.ToString)
            Item.SubItems.Add(struEventCfg.struAcsEventInfo.dwAlarmInNo.ToString)
            Item.SubItems.Add(struEventCfg.struAcsEventInfo.dwAlarmOutNo.ToString)
            Item.SubItems.Add(struEventCfg.struAcsEventInfo.dwCaseSensorNo.ToString)
            Item.SubItems.Add(struEventCfg.struAcsEventInfo.dwRs485No.ToString)
            Item.SubItems.Add(struEventCfg.struAcsEventInfo.dwMultiCardGroupNo.ToString)
            Item.SubItems.Add(struEventCfg.struAcsEventInfo.wAccessChannel.ToString)
            Item.SubItems.Add(struEventCfg.struAcsEventInfo.byDeviceNo.ToString)
            Item.SubItems.Add(struEventCfg.struAcsEventInfo.dwEmployeeNo.ToString)
            Item.SubItems.Add(struEventCfg.struAcsEventInfo.byDistractControlNo.ToString)
            Item.SubItems.Add(struEventCfg.struAcsEventInfo.wLocalControllerID.ToString)
            CsTemp = cn.ProcessInternatAccess(struEventCfg)
            Item.SubItems.Add(CsTemp)
            CsTemp = cn.ProcessByType(struEventCfg)
            Item.SubItems.Add(CsTemp)
            CsTemp = cn.ProcessMacAdd(struEventCfg)
            Item.SubItems.Add(CsTemp)
            CsTemp = cn.ProcessSwipeCard(struEventCfg)
            Item.SubItems.Add(CsTemp)
            Item.SubItems.Add(struEventCfg.struAcsEventInfo.dwSerialNo.ToString)
            Item.SubItems.Add(struEventCfg.struAcsEventInfo.byChannelControllerID.ToString)
            Item.SubItems.Add(struEventCfg.struAcsEventInfo.byChannelControllerLampID.ToString)
            Item.SubItems.Add(struEventCfg.struAcsEventInfo.byChannelControllerIRAdaptorID.ToString)
            Item.SubItems.Add(struEventCfg.struAcsEventInfo.byChannelControllerIREmitterID.ToString)
            If (struEventCfg.wInductiveEventType < CType(GetAcsEventType.NumOfInductiveEvent, System.UInt16)) Then
                Item.SubItems.Add(GetAcsEventType.FindKeyOfInductive(struEventCfg.wInductiveEventType))
            Else
                Item.SubItems.Add("Invalid")
            End If

            Item.SubItems.Add("0")
            'RecordChannelNum
            CsTemp = cn.ProcessbyUserType(struEventCfg)
            Item.SubItems.Add(CsTemp)
            CsTemp = cn.ProcessVerifyMode(struEventCfg)
            Item.SubItems.Add(CsTemp)
            CsTemp = System.Text.Encoding.UTF8.GetString(struEventCfg.struAcsEventInfo.byEmployeeNo)
            Item.SubItems.Add(CsTemp)
            CsTemp = Nothing
            If Item.SubItems.Item(4).Text.Trim <> "" Then
                Me.listViewEvent.Items.Add(Item)
            End If
            'Me.listViewEvent.Items.Add(Item)
            Me.listViewEvent.EndUpdate()
            cn.ProcessPicData(struEventCfg)
        End If
    End Sub

End Class