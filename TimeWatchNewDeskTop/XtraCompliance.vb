﻿Imports DevExpress.XtraGrid
Imports System.ComponentModel
Imports DevExpress.XtraGrid.Columns
Imports System.Data.SqlClient
Imports System.Text
Imports System.Data.OleDb
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors

Public Class XtraCompliance
    Dim ulf As UserLookAndFeel
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    Dim comclass As Common = New Common
    Public Sub New()
        InitializeComponent()
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True
    End Sub

    Private Sub SimpleButtonProcess_Click(sender As Object, e As EventArgs) Handles SimpleButtonProcess.Click
        Dim sSql As String
        Dim rsSetup As DataSet
        Dim rsEmp As DataSet
        Dim rsActData As DataSet
        Dim rsDuplData As DataSet
        Dim motlogic As String, motPerDay As Integer, motPerWeek As Integer, motPerMonth As Integer
        Dim mValue As Integer, mPerDay As Integer, mPerWeek As Integer, mPerMonth As Integer
        Dim mShiftTime As Integer, PUNCHTIME1 As Date, mPaycode As String, misOk As Boolean
        Dim rsTime As New DataSet
        misOk = True
        Dim adapA As OleDbDataAdapter
        Dim adap As SqlDataAdapter
        Dim mIn1 As String
        Dim mIn2 As String
        Dim mout1 As String
        Dim mout2 As String
        Dim mReasonCode As String = "" : Dim strFirstPunchReasonCode As String = "" : Dim strLastPunchReasonCode As String = ""
        Dim mShiftStartTime As String = "Null" : Dim mShiftEndTime As String = "Null"
        Dim mLunchStartTime As String = "Null" : Dim mLunchEndTime As String = "Null"
        Dim mIn1Mannual As String = "N" : Dim mIn2Mannual As String = "N"
        Dim mOut1Mannual As String = "N" : Dim mOut2Mannual As String = "N"

        Dim HCOMPANYCODE As String
        Dim HDEPARTMENTCODE As String
        Dim mShiftAttended As String

        Dim mmin_date As DateTime = DateEdit3.DateTime.ToString("01/MM/yyyy")
        Dim mmax_date As DateTime = DateAdd("M", 1, mmin_date)
        mmax_date = DateAdd("d", -1, mmax_date)
        mmax_date = Format(mmax_date, "dd/MM/yyyy")
        If CDate(Format(mmax_date, "dd/MM/yyyy")) > CDate(Format(Now(), "dd/MM/yyyy")) Then
            mmax_date = Format(Now(), "dd/MM/yyyy")
        End If
        sSql = "select * from otsetup"
        rsSetup = New DataSet
        Dim cn As Common = New Common
        'Set rsSetup = Cn.Execute(sSql)
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(rsSetup)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(rsSetup)
        End If
        If rsSetup.Tables(0).Rows.Count > 0 Then
            motPerMonth = rsSetup.Tables(0).Rows(0)("OTMONTHLY")
            motPerDay = Val(rsSetup.Tables(0).Rows(0)("OTPERDAY"))
            motPerWeek = Val(rsSetup.Tables(0).Rows(0)("OTPERWEEK"))
            mValue = 0
            mPerDay = 0
            mPerWeek = 0
            mPerMonth = 0
            misOk = True
        Else
            XtraMessageBox.Show(ulf, "<size=10>Ot Setup is not Entered Properly</size>", "<size=9>Error</size>")
            Exit Sub
        End If
        Dim mOtDuration As Integer = 0
        If misOk = True Then
            'XtraMasterTest.LabelControlStatus.Text = "Capturing Data From  " & Format(mmin_date, "dd/MM") & "  To  " & Format(mmax_date, "dd/MM")
            'Application.DoEvents()

            'g_OTFormulae = 1
            'If OptSelection(1).Value Then
            '    sSql = "Select a.* ,b.companycode,b.departmentcode from tblemployeeshiftmaster a,tblemployee b where a.paycode=b.paycode and a.paycode='" & Trim(txtPaycode.Text) & "' and " & gcompclauseD & " and " & gdeptclauseD
            'Else
            Dim emp() As String = Common.Auth_Branch.Split(",")
            Dim ls As New List(Of String)()
            For x As Integer = 0 To emp.Length - 1
                ls.Add(emp(x).Trim)
            Next

            Dim com() As String = Common.auth_comp.Split(",")
            Dim lsc As New List(Of String)()
            For x As Integer = 0 To com.Length - 1
                lsc.Add(com(x).Trim)
            Next

            sSql = "Select a.*,b.companycode,b.departmentcode,b.branchcode, EmployeeGroup.ID from tblemployeeshiftmaster a,tblemployee b, EmployeeGroup where a.paycode=b.paycode  and b.EmployeeGroupId = EmployeeGroup.GroupId 
and BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "') and COMPANYCODE IN ('" & String.Join("', '", lsc.ToArray()) & "')"
            'End If
            rsEmp = New DataSet
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(rsEmp)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(rsEmp)
            End If
            If rsEmp.Tables(0).Rows.Count > 0 Then
                'DoEvents
                'rsEmp.MoveFirst
                DevExpress.XtraSplashScreen.SplashScreenManager.ShowForm(Me, GetType(WaitForm1), True, True, False)
                For i As Integer = 0 To rsEmp.Tables(0).Rows.Count - 1 ' Do While Not rsEmp.EOF
                    XtraMasterTest.LabelControlStatus.Text = "Processing From  " & Format(mmin_date, "dd/MM") & "  To  " & Format(mmax_date, "dd/MM")
                    Application.DoEvents()
                    'frmMdiMain.pb.Value = (rsEmp.AbsolutePosition / rsEmp.RecordCount) * 100
                    'frmMdiMain.lblpercent.Caption = Trim(Str(Round(frmMdiMain.pb.Value))) & "%"
                    mPerWeek = 0
                    mPerMonth = 0
                    mmin_date = Convert.ToDateTime(DateEdit3.DateTime.ToString("yyyy-MM-01 HH:mm:ss")) ' Format("01/" & Format(dtpProcDate.Value, "Mmm/Yyyy"), "dd/Mmm/Yyyy")
                    Do While CDate(mmin_date) <= CDate(mmax_date)
                        XtraMasterTest.LabelControlStatus.Text = "Processing for Paycode " & rsEmp.Tables(0).Rows(i)("paycode").ToString.Trim & "  Date: " & mmin_date.ToString("dd/MM/yyyy")
                        Application.DoEvents()
                        sSql = "Select * from tbltimeregister where paycode='" & rsEmp.Tables(0).Rows(i)("paycode").ToString.Trim _
                            & "' and dateoffice= '" & mmin_date.ToString("yyyy-MM-dd") & "'"
                        'Set rsTime = Cn.Execute(sSql)
                        rsTime = New DataSet
                        If Common.servername = "Access" Then
                            adapA = New OleDbDataAdapter(sSql, Common.con1)
                            adapA.Fill(rsTime)
                        Else
                            adap = New SqlDataAdapter(sSql, Common.con)
                            adap.Fill(rsTime)
                        End If
                        mReasonCode = "" : strFirstPunchReasonCode = "" : strLastPunchReasonCode = ""
                        If rsTime.Tables(0).Rows.Count > 0 Then
                            XtraMasterTest.LabelControlStatus.Text = "Processing data of Date From  " & Format(rsTime.Tables(0).Rows(0)("DATEOFFICE"), "dd/MM/yyyy") & "  of Paycode : " & rsTime.Tables(0).Rows(0)("paycode")
                            mIn1 = "Null" : mIn2 = "Null" : mout1 = "Null" : mout2 = "Null"
                            mShiftStartTime = "Null" : mShiftEndTime = "Null"
                            mLunchStartTime = "Null" : mLunchEndTime = "Null"
                            mIn1Mannual = "N" : mIn2Mannual = "N"
                            mOut1Mannual = "N" : mOut2Mannual = "N"
                            If rsTime.Tables(0).Rows(0)("HOLIDAY_VALUE") > 0 Or rsTime.Tables(0).Rows(0)("WO_VALUE") > 0 Or rsTime.Tables(0).Rows(0)("LEAVEVALUE") > 0 Or rsTime.Tables(0).Rows(0)("Shift").ToString.Trim = "OFF" Then 'Format(rsTime.Tables(0).Rows(0)("DATEOFFICE"), "dd/MM/yyyy") <> Format(rsTime.Tables(0).Rows(0)("In1"), "dd/MM/yyyy") Or
                                GoTo AsgIt
                            Else
                                Try
                                    mIn1 = Convert.ToDateTime(rsTime.Tables(0).Rows(0)("in1")).ToString("yyyy-MM-dd HH:mm:ss")
                                Catch ex As Exception

                                End Try

                                Try
                                    mIn2 = Convert.ToDateTime(rsTime.Tables(0).Rows(0)("in2")).ToString("yyyy-MM-dd HH:mm:ss")
                                Catch ex As Exception

                                End Try
                                Try
                                    mout1 = Convert.ToDateTime(rsTime.Tables(0).Rows(0)("out1")).ToString("yyyy-MM-dd HH:mm:ss")
                                Catch ex As Exception

                                End Try
                                Try
                                    mout2 = Convert.ToDateTime(rsTime.Tables(0).Rows(0)("out2")).ToString("yyyy-MM-dd HH:mm:ss")
                                Catch ex As Exception

                                End Try
                                If mIn1 = "Null" Or mout2 = "Null" Then
                                    'mmin_date = DateAdd("d", 1, Format(mmin_date, "dd/MM/yyyy"))
                                    'mmin_date = Format(mmin_date, "dd/MM/yyyy")
                                    GoTo AsgIt
                                    'Continue Do
                                End If
                                If rsTime.Tables(0).Rows(0)("OtDuration") > (motPerDay * 60) And mPerWeek + motPerDay <= motPerWeek And mPerMonth + motPerDay <= motPerMonth Then
                                    mShiftEndTime = rsTime.Tables(0).Rows(0)("shiftEndTime")
                                    PUNCHTIME1 = Convert.ToDateTime(rsTime.Tables(0).Rows(0)("shiftEndTime")).ToString("yyyy-MM-dd HH:mm:ss")
                                    mValue = Val(Mid(Format(rsTime.Tables(0).Rows(0)("out2"), "HH:mm"), 4, 2))
                                    If mValue > 14 Then
                                        mValue = Val(Mid(Format(rsTime.Tables(0).Rows(0)("out2"), "HH:mm"), 5, 1))
                                    End If
                                    'If motPerDay > 0 Then

                                    ' mValue = (motPerDay * 60) - mValue
                                    ' End If
                                    mout2 = Format(DateAdd("N", mValue, CDate(PUNCHTIME1)), "yyyy-MM-dd HH:mm:ss")
                                    GoTo AsgIt
                                Else
                                    If (mPerWeek + Min2Hr(rsTime.Tables(0).Rows(0)("OtDuration") > motPerWeek) Or (mPerMonth + Min2Hr(rsTime.Tables(0).Rows(0)("OtDuration")) > motPerMonth)) Then
                                        mShiftEndTime = rsTime.Tables(0).Rows(0)("shiftEndTime")
                                        PUNCHTIME1 = Convert.ToDateTime(rsTime.Tables(0).Rows(0)("shiftEndTime")).ToString("yyyy-MM-dd HH:mm:ss")
                                        mValue = Val(Mid(Format(rsTime.Tables(0).Rows(0)("out2"), "HH:mm"), 4, 2))
                                        If mValue > 14 Then
                                            mValue = Val(Mid(Format(rsTime.Tables(0).Rows(0)("out2"), "HH:mm"), 5, 1))
                                        End If
                                        mValue = mValue
                                        mout2 = Format(DateAdd("N", mValue, CDate(PUNCHTIME1)), "yyyy-MM-dd HH:mm:ss")
                                        GoTo AsgIt
                                    End If
                                End If
                            End If
AsgIt:
                            HCOMPANYCODE = rsEmp.Tables(0).Rows(i)("companycode")
                            HDEPARTMENTCODE = rsEmp.Tables(0).Rows(i)("departmentcode")
                            'Load_HolidaySql()
                            cn.Load_HolidaySql(HCOMPANYCODE, HDEPARTMENTCODE, rsEmp.Tables(0).Rows(i)("BRANCHCODE").ToString.Trim)
                            'mShiftAttended = cn.AutoShift2(rsTime)
                            mShiftAttended = cn.AutoShift2(rsTime, 0, rsEmp, 0, mIn1, mIn2, mout1, mout2, rsEmp.Tables(0).Rows(i)("Id"))
                            'cn.Upd2(rsTime)

                            cn.Upd2Compliance(rsTime, 0, rsEmp, i, mIn1, mIn2, mout1, mout2, mShiftAttended, rsEmp.Tables(0).Rows(i)("Id"), mOtDuration)
                            '    If mShiftStartTime <> "Null" Then
                            '        mShiftStartTime = "'" & mShiftStartTime & "'"
                            '    End If
                            '    If mShiftEndTime <> "Null" Then
                            '        mShiftEndTime = "'" & mShiftEndTime & "'"
                            '    End If
                            '    If mLunchStartTime <> "Null" Then
                            '        mLunchStartTime = "'" & mLunchStartTime & "'"
                            '    End If
                            '    If mLunchEndTime <> "Null" Then
                            '        mLunchEndTime = "'" & mLunchEndTime & "'"
                            '    End If
                            '    If mIn1 <> "Null" Then
                            '        mIn1 = "'" & mIn1 & "'"
                            '    End If
                            '    If mout1 <> "Null" Then
                            '        mout1 = "'" & mout1 & "'"
                            '    End If
                            '    If mIn2 <> "Null" Then
                            '        mIn2 = "'" & mIn2 & "'"
                            '    End If
                            '    If mout2 <> "Null" Then
                            '        mout2 = "'" & mout2 & "'"
                            '    End If
                            '    sSql = "Update tblTimeRegisterD Set leaveamount='" & rsTime.Tables(0).Rows(0)("leaveamount") & "',leavecode='" & rsTime.Tables(0).Rows(0)("Leavecode") & "',LEAVETYPE='" & rsTime.Tables(0).Rows(0)("LEAVETYPE") & "', ShiftStartTime=" & mShiftStartTime & "" _
                            '        & ", ShiftEndTime=" & mShiftEndTime & ",LunchStartTime=" _
                            '        & mLunchStartTime & ", LunchEndTime=" & mLunchEndTime _
                            '        & ", HoursWorked=" & mHoursworked & ",ExcLunchHours=" _
                            '        & mExcLunchHours & ",OtDuration=" & mOtDuration _
                            '        & ", OsDuration=" & mOSDuration & ",OtAmount=" & motamount _
                            '        & ", EarlyArrival=" & mEarlyArrival & ",EarlyDeparture=" _
                            '        & mearlydeparture & ",LateArrival=" & mLateArrival _
                            '        & ", LunchEarlyDeparture=" & mLunchEarlyDeparture _
                            '        & ", LunchLateArrival=" & mLunchLateArrival & ",TotalLossHrs=" _
                            '        & mTotalLossHrs & ",Status='" & mStatus & "',ShiftAttended='" _
                            '        & mShiftAttended & "',In1=" & mIn1 & ",In2=" & mIn2 & ",out1=" _
                            '        & mout1 & ",Out2=" & mout2 & ",In1Mannual='" & mIn1Mannual _
                            '        & "',In2Mannual='" & mIn2Mannual & "',Out1Mannual='" & mOut1Mannual _
                            '        & "',Out2Mannual='" & mOut2Mannual & "',LeaveValue=" & mLeaveValue _
                            '        & ",PresentValue=" & mPresentValue & ",AbsentValue=" & mAbsentValue _
                            '        & ",Holiday_Value=" & mHoliday_Value & ",Wo_Value=" & mWO_Value _
                            '        & ",OutWorkDuration=" & mOutWorkDuration & " where paycode='" + Trim(rsEmp.Tables(0).Rows(i)("Paycode")) _
                            '        & "' and dateoffice = '" + mmin_date + "'"
                            '    'Cn.Execute sSql, 64
                            '    If Common.servername = "Access" Then
                            '        If Common.con1.State <> ConnectionState.Open Then
                            '            Common.con1.Open()
                            '        End If
                            '        cmd1 = New OleDbCommand(sSql, Common.con1)
                            '        cmd1.ExecuteNonQuery()
                            '        If Common.con1.State <> ConnectionState.Closed Then
                            '            Common.con1.Close()
                            '        End If
                            '    Else
                            '        If Common.con.State <> ConnectionState.Open Then
                            '            Common.con.Open()
                            '        End If
                            '        Dim cmd As SqlCommand
                            '        cmd = New SqlCommand(sSql, Common.con)
                            '        cmd.ExecuteNonQuery()
                            '        If Common.con.State <> ConnectionState.Closed Then
                            '            Common.con.Close()
                            '        End If
                            '    End If
                            '    'rsTime.Close
                        End If
                        mPerWeek = mPerWeek + Min2Hr(mOtDuration)
                        mPerMonth = mPerMonth + Min2Hr(mOtDuration)
                        mmin_date = DateAdd("d", 1, Format(mmin_date, "dd/MM/yyyy"))
                        mmin_date = Format(mmin_date, "dd/MM/yyyy")
                        If mmin_date.Day = 8 Or mmin_date.Day = 15 Or mmin_date.Day = 22 Or mmin_date.Day = 29 Then
                            mPerWeek = 0
                        End If
                    Loop
                    'rsEmp.MoveNext
                Next ' Loop 
                DevExpress.XtraSplashScreen.SplashScreenManager.CloseForm(False)
                XtraMasterTest.LabelControlStatus.Text = ""
                Application.DoEvents()
                XtraMessageBox.Show(ulf, "<size=10>Processed Successfully</size>", "Success")
                Me.Close()
            Else
                XtraMessageBox.Show(ulf, "<size=10>No Employee Available</size>", "iAS")
            End If
        End If
    End Sub
    Function Min2Hr(ByVal mMIN As String) As Double
        If mMIN = "" Then
            mMIN = 0
        End If
        If Val(mMIN) >= 0 Then
            Min2Hr = (Int(Val(mMIN) / 60) + ((Val(mMIN) Mod 60) / 100))
        Else
            Min2Hr = 0 ' -(Int(Val(Abs(mMIN)) / 60) + ((Abs(mMIN) Mod 60) / 100))
        End If
    End Function

    Private Sub XtraCompliance_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        DateEdit3.DateTime = Now.ToString("01/MM/yyyy")

        If Common.IsNepali = "Y" Then
            DateEdit3.Visible = False
            ComboNepaliYear.Visible = True
            ComboNEpaliMonth.Visible = True
            ComboNepaliDate.Visible = True

            Dim DC As New DateConverter()
            Dim doj As String = DC.ToBS(New Date(DateEdit3.DateTime.Year, DateEdit3.DateTime.Month, 1))
            Dim dojTmp() As String = doj.Split("-")
            ComboNepaliYear.EditValue = dojTmp(0)
            ComboNEpaliMonth.SelectedIndex = dojTmp(1) - 1
            ComboNepaliDate.EditValue = dojTmp(2)

        Else
            DateEdit3.Visible = True

            ComboNepaliYear.Visible = False
            ComboNEpaliMonth.Visible = False
            ComboNepaliDate.Visible = False
        End If
    End Sub

    Private Sub SimpleButton1_Click(sender As Object, e As EventArgs) Handles SimpleButton1.Click
        Me.Close()
    End Sub
End Class