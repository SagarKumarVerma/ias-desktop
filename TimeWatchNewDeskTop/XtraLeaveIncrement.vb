﻿Imports System.Resources
Imports System.Globalization
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.IO
Imports System.Data.SqlClient
Imports System.Text
Imports System.Data.OleDb

Public Class XtraLeaveIncrement
    Dim ulf As UserLookAndFeel
    Public Sub New()
        InitializeComponent()
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True
    End Sub
    Private Sub XtraLeaveIncrement_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        DateEdit.DateTime = Now.ToString("yyyy-MM-01")
        If Common.IsNepali = "Y" Then
            DateEdit.Visible = False           
            ComboBoxEditNepaliYear.Visible = True
            ComboBoxEditNEpaliMonth.Visible = True
           
            Dim DC As New DateConverter()
            'Dim doj As DateTime = DC.ToBS(New Date(DateEdit.DateTime.Year, DateEdit.DateTime.Month, 1))
            'ComboBoxEditNepaliYear.EditValue = doj.Year
            'ComboBoxEditNEpaliMonth.SelectedIndex = doj.Month - 1

            Dim doj As String = DC.ToBS(New Date(DateEdit.DateTime.Year, DateEdit.DateTime.Month, 1))
            Dim dojTmp() As String = doj.Split("-")
            ComboBoxEditNepaliYear.EditValue = dojTmp(0)
            ComboBoxEditNEpaliMonth.SelectedIndex = dojTmp(1) - 1            
        Else
            DateEdit.Visible = True            
            ComboBoxEditNepaliYear.Visible = False
            ComboBoxEditNEpaliMonth.Visible = False           
        End If
    End Sub

    Private Sub SimpleButtonSave_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonSave.Click
        If Common.IsNepali = "Y" Then
            Dim DC As New DateConverter()
            Try
                'DateEdit.DateTime = DC.ToAD(New Date(ComboBoxEditNepaliYear.EditValue, ComboBoxEditNEpaliMonth.SelectedIndex + 1, 1))
                DateEdit.DateTime = DC.ToAD(ComboBoxEditNepaliYear.EditValue & "-" & ComboBoxEditNEpaliMonth.SelectedIndex + 1 & "-" & 1)
            Catch ex As Exception
                XtraMessageBox.Show(ulf, "<size=10>Invalid Date</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                ComboBoxEditNEpaliMonth.Select()
                Exit Sub
            End Try
        End If
        Dim conSQL As SqlConnection
        Dim con1Access As OleDbConnection
        If Common.servername = "Access" Then
            con1Access = New OleDbConnection(Common.ConnectionString)
        Else
            conSQL = New SqlConnection(Common.ConnectionString)
        End If

        Dim adapA As OleDbDataAdapter
        Dim adap As SqlDataAdapter
        Dim cmd As SqlCommand
        Dim cmd1 As OleDbCommand
        Dim ForMonth As Date
        Dim mCode As String, iFlag As Boolean, iCtr As Integer, iD As Integer, bleave As Double, pLeave As Double, nleave As Double, lcode As String, lcode1 As String
        Dim Rs As DataSet, rsData As DataSet, rsLeaveLedger As DataSet, RSLeaveLedgerb As DataSet, rsLeaveMaster As DataSet, rsTime As DataSet
        Dim RsLBal As DataSet
        Dim mArr(365) As Object, PrvYearLv As Double
        Dim Pyear As String



        Dim sSql As String = "Select Paycode,PRESENTCARDNO from tblEmployee where PAYCODE IN ('" & String.Join("', '", Common.EmpArr) & "')  Order by PRESENTCARDNO"
            Rs = New DataSet
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, con1Access)
                adapA.Fill(Rs)
            Else
                adap = New SqlDataAdapter(sSql, conSQL)
                adap.Fill(Rs)
            End If
            'Rs = Cn.Execute(sSql)
            rsLeaveMaster = New DataSet
            sSql = "Select * from Tblleavemaster order by leavefield"
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, con1Access)
                adapA.Fill(rsLeaveMaster)
            Else
                adap = New SqlDataAdapter(sSql, conSQL)
                adap.Fill(rsLeaveMaster)
            End If
        'rsLeaveMaster = Cn.Execute(sSql)
        If CheckEditM.Checked Then
            ForMonth = "01/" & Format(DateEdit.DateTime, "MM/yyyy")
            If XtraMessageBox.Show(ulf, "<size=10>Do you want to update the Leave for the month of " & Format(DateEdit.DateTime, "MMM yyyy") & "</size>", "<size=9>Confirmation</size>",
                               MessageBoxButtons.YesNo, MessageBoxIcon.Question) <> DialogResult.Yes Then
                Exit Sub
            End If
            If Common.gLeaveFinancialYear = "Y" Then
                If Val(Month(DateEdit.DateTime)) <= 3 Then
                    Pyear = Year(DateEdit.DateTime) - 1
                Else
                    Pyear = Year(DateEdit.DateTime)
                End If
            Else
                Pyear = Year(DateEdit.DateTime)
            End If
            'Do While Not Rs.EOF
            Dim ssql1 As String
            If Common.servername = "Access" Then
                If con1Access.State <> ConnectionState.Open Then
                    con1Access.Open()
                End If
            Else
                If conSQL.State <> ConnectionState.Open Then
                    conSQL.Open()
                End If
            End If
            For i As Integer = 0 To Rs.Tables(0).Rows.Count - 1
                XtraMasterTest.LabelControlCount.Text = "Processing " & i + 1 & " of " & Rs.Tables(0).Rows.Count
                XtraMasterTest.LabelControlStatus.Text = "Processing for " & Rs.Tables(0).Rows(i)("Paycode").ToString.Trim
                Application.DoEvents()
                RsLBal = New DataSet
                If Common.servername = "Access" Then
                    ssql1 = "select * from leavecredit where Paycode='" & Rs.Tables(0).Rows(i)("Paycode").ToString.Trim & "' and Format(ForMonth,'yyyy-MM-dd') ='" & Format(ForMonth, "yyyy-MM-dd") & "' "
                    adapA = New OleDbDataAdapter(ssql1, con1Access)
                    adapA.Fill(RsLBal)
                Else
                    ssql1 = "select * from leavecredit where Paycode='" & Rs.Tables(0).Rows(i)("Paycode").ToString.Trim & "' and ForMonth ='" & Format(ForMonth, "yyyy-MM-dd") & "' "
                    adap = New SqlDataAdapter(ssql1, conSQL)
                    adap.Fill(RsLBal)
                End If
                'RsLBal = Cn.Execute(ssql1)
                If RsLBal.Tables(0).Rows.Count > 0 Then
                    Continue For
                    'XtraMessageBox.Show(ulf, "<size=10>Leave balance already credited for " & Format(ForMonth, "MMM-yyyy") & "</size>", "<size=9>iAS</size>")
                    'GoTo tmp
                End If
                If RsLBal.Tables(0).Rows.Count = 0 Then
                    sSql = "select * from tblleaveledger where paycode='" & Rs.Tables(0).Rows(i)("Paycode").ToString.Trim & "' and lyear=" & Pyear
                    rsLeaveLedger = New DataSet
                    If Common.servername = "Access" Then
                        adapA = New OleDbDataAdapter(sSql, con1Access)
                        adapA.Fill(rsLeaveLedger)
                    Else
                        adap = New SqlDataAdapter(sSql, conSQL)
                        adap.Fill(rsLeaveLedger)
                    End If
                    'rsLeaveLedger = Cn.Execute(sSql)
                    If rsLeaveLedger.Tables(0).Rows.Count > 0 Then
                        sSql = "update tblleaveledger set acc_flag='Y' where paycode='" & Rs.Tables(0).Rows(i)("Paycode").ToString.Trim & "' and lyear=" & Pyear
                    Else
                        sSql = "Insert into tblleaveledger (paycode,lyear,L01,L02,L03,L04,L05,L06,L07,L08,L09,L10,L11,L12,L13,L14,L15,L16,L17,L18,L19,L20,L01_Add,L02_Add,L03_Add,L04_Add,L05_Add,L06_Add,L07_Add,L08_Add,L09_Add,L10_Add,L11_Add,L12_Add,L13_Add,L14_Add,L15_Add,L16_Add,L17_Add,L18_Add,L19_Add,L20_Add,acc_flag) values('" & Rs.Tables(0).Rows(i)("Paycode").ToString.Trim & "'," & Pyear & ",0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,'Y')"
                    End If
                    If Common.servername = "Access" Then
                        cmd1 = New OleDbCommand(sSql, con1Access)
                        cmd1.ExecuteNonQuery()
                    Else
                        cmd = New SqlCommand(sSql, conSQL)
                        cmd.ExecuteNonQuery()
                    End If
                    'Cn.Execute(sSql)
                    'rsLeaveMaster.MoveFirst()
                    'Do While Not rsLeaveMaster.EOF
                    For j As Integer = 0 To rsLeaveMaster.Tables(0).Rows.Count - 1
                        lcode = rsLeaveMaster.Tables(0).Rows(j)("leavefield").ToString.Trim
                        lcode1 = rsLeaveMaster.Tables(0).Rows(j)("leavefield").ToString.Trim & "_add"
                        If rsLeaveMaster.Tables(0).Rows(j)("isleaveaccrual").ToString.Trim = "Y" Then
                            If rsLeaveMaster.Tables(0).Rows(j)("isMonthly").ToString.Trim = "Y" Then
                                nleave = rsLeaveMaster.Tables(0).Rows(j)("leave")
                                sSql = "Update tblLeaveLedger Set " & lcode1 & "=" & lcode1 & " + " & nleave & " where paycode='" & Rs.Tables(0).Rows(i)("Paycode").ToString.Trim & "' and lyear=" & Pyear
                                If Common.servername = "Access" Then
                                    cmd1 = New OleDbCommand(sSql, con1Access)
                                    cmd1.ExecuteNonQuery()
                                Else
                                    cmd = New SqlCommand(sSql, conSQL)
                                    cmd.ExecuteNonQuery()
                                End If
                                'Cn.Execute (sSql), x

                                If j > 0 Then
                                    sSql = "insert into LeaveCredit(Paycode, ForMonth, LeaveCode, CreditAmount, RunDate) Values ( " &
                                " '" & Rs.Tables(0).Rows(i)("Paycode").ToString.Trim & "','" & Format(ForMonth, "yyyy-MM-dd") & "','" & rsLeaveMaster.Tables(0).Rows(j)("Leavecode").ToString.Trim & "'," & nleave & ",'" & Format(System.DateTime.Now, "yyyy-MM-dd") & "')"
                                    If Common.servername = "Access" Then
                                        cmd1 = New OleDbCommand(sSql, con1Access)
                                        cmd1.ExecuteNonQuery()
                                    Else
                                        cmd = New SqlCommand(sSql, conSQL)
                                        cmd.ExecuteNonQuery()
                                    End If
                                    'Cn.Execute (sSql), x
                                    If j <= 0 Then     '' cancel leave increement for this paycode
                                        sSql = "Update tblLeaveLedger Set " & lcode1 & "=" & lcode1 - nleave & " where paycode='" & Rs.Tables(0).Rows(i)("Paycode").ToString.Trim & "' and lyear=" & Pyear
                                        If Common.servername = "Access" Then
                                            cmd1 = New OleDbCommand(sSql, con1Access)
                                            cmd1.ExecuteNonQuery()
                                        Else
                                            cmd = New SqlCommand(sSql, conSQL)
                                            cmd.ExecuteNonQuery()
                                        End If
                                        'Cn.Execute (sSql), x                                   
                                    End If
                                End If
                            End If
                        End If
                        'rsLeaveMaster.MoveNext()
                        'If rsLeaveMaster.EOF Then
                        '    Exit Do
                        'End If
                    Next
                    'Loop
                End If
                'ProgressBar1.Value = (Rs.AbsolutePosition / Rs.RecordCount) * 100
                'Rs.MoveNext()
            Next
        ElseIf CheckEditY.Checked Then
            If XtraMessageBox.Show(ulf, "<size=10>Do you want to update the Leave for the Year of " & Format(DateEdit.DateTime, "yyyy") & "</size>", "<size=9>Confirmation</size>",
                               MessageBoxButtons.YesNo, MessageBoxIcon.Question) <> DialogResult.Yes Then
                Exit Sub
            End If

            Dim ssql1 As String
            If Common.servername = "Access" Then
                If con1Access.State <> ConnectionState.Open Then
                    con1Access.Open()
                End If
            Else
                If conSQL.State <> ConnectionState.Open Then
                    conSQL.Open()
                End If
            End If

            'Do While Not Rs.EOF
            For i As Integer = 0 To Rs.Tables(0).Rows.Count - 1
                XtraMasterTest.LabelControlCount.Text = "Processing " & i + 1 & " of " & Rs.Tables(0).Rows.Count
                XtraMasterTest.LabelControlStatus.Text = "Processing for " & Rs.Tables(0).Rows(i)("Paycode").ToString.Trim
                Application.DoEvents()
                sSql = "select * from tblleaveledger where paycode='" & Rs.Tables(0).Rows(i)("Paycode").ToString.Trim & "' and lyear=" & DateEdit.DateTime.ToString("yyyy")
                'Set rsLeaveLedger = Cn.Execute(sSql)
                rsLeaveLedger = New DataSet
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql, con1Access)
                    adapA.Fill(rsLeaveLedger)
                Else
                    adap = New SqlDataAdapter(sSql, conSQL)
                    adap.Fill(rsLeaveLedger)
                End If

                If rsLeaveLedger.Tables(0).Rows.Count > 0 Then
                    sSql = "update tblleaveledger set acc_flag='Y' where paycode='" & Rs.Tables(0).Rows(i)("Paycode").ToString.Trim & "' and lyear=" & DateEdit.DateTime.ToString("yyyy")
                Else
                    sSql = "Insert into tblleaveledger (paycode,lyear,L01,L02,L03,L04,L05,L06,L07,L08,L09,L10,L11,L12,L13,L14,L15,L16,L17,L18,L19,L20,L01_Add,L02_Add,L03_Add,L04_Add,L05_Add,L06_Add,L07_Add,L08_Add,L09_Add,L10_Add,L11_Add,L12_Add,L13_Add,L14_Add,L15_Add,L16_Add,L17_Add,L18_Add,L19_Add,L20_Add,acc_flag) values('" & Rs.Tables(0).Rows(i)("paycode").ToString.Trim & "'," & DateEdit.DateTime.ToString("yyyy") & ",0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,'Y')"
                End If
                'Cn.Execute(sSql)
                If Common.servername = "Access" Then
                    cmd1 = New OleDbCommand(sSql, con1Access)
                    cmd1.ExecuteNonQuery()
                Else
                    cmd = New SqlCommand(sSql, conSQL)
                    cmd.ExecuteNonQuery()
                End If
                'rsLeaveMaster.MoveFirst
                'Do While Not rsLeaveMaster.EOF
                For j As Integer = 0 To rsLeaveMaster.Tables(0).Rows.Count - 1
                    lcode = rsLeaveMaster.Tables(0).Rows(j)("leavefield").ToString.Trim
                    lcode1 = rsLeaveMaster.Tables(0).Rows(j)("leavefield").ToString.Trim & "_add"
                    If rsLeaveMaster.Tables(0).Rows(j)("isleaveaccrual").ToString.Trim = "Y" Then
                        If rsLeaveMaster.Tables(0).Rows(j)("fixed").ToString.Trim = "N" Then
                            sSql = "select * from tblleaveledger where paycode='" & Rs.Tables(0).Rows(i)("Paycode").ToString.Trim & "' and lyear=" & DateEdit.DateTime.ToString("yyyy") - 1
                            'Set RSLeaveLedgerb = Cn.Execute(sSql)
                            RSLeaveLedgerb = New DataSet
                            If Common.servername = "Access" Then
                                adapA = New OleDbDataAdapter(sSql, con1Access)
                                adapA.Fill(RSLeaveLedgerb)
                            Else
                                adap = New SqlDataAdapter(sSql, conSQL)
                                adap.Fill(RSLeaveLedgerb)
                            End If
                            If RSLeaveLedgerb.Tables(0).Rows.Count > 0 Then
                                bleave = RSLeaveLedgerb.Tables(0).Rows(0)(lcode1) - RSLeaveLedgerb.Tables(0).Rows(0)(lcode)
                            Else
                                bleave = 0
                            End If
                            If Common.servername = "Access" Then
                                sSql = "select sum(presentvalue) as mpresent from tbltimeregister where paycode='" & Rs.Tables(0).Rows(i)("Paycode").ToString.Trim & "' and Format(dateoffice,'yyyy')=" & DateEdit.DateTime.ToString("yyyy")
                            Else
                                sSql = "select sum(presentvalue) mpresent from tbltimeregister where paycode='" & Rs.Tables(0).Rows(i)("Paycode").ToString.Trim & "' and datepart(yyyy,dateoffice)=" & DateEdit.DateTime.ToString("yyyy")
                            End If
                            'Set rsData = Cn.Execute(sSql)
                            rsData = New DataSet
                            If Common.servername = "Access" Then
                                adapA = New OleDbDataAdapter(sSql, con1Access)
                                adapA.Fill(rsData)
                            Else
                                adap = New SqlDataAdapter(sSql, conSQL)
                                adap.Fill(rsData)
                            End If
                            If rsData.Tables(0).Rows.Count > 0 Then
                                If rsData.Tables(0).Rows(0)("mpresent").ToString.Trim <> "" Then
                                    If rsData.Tables(0).Rows(0)("mpresent") > 0 And rsLeaveMaster.Tables(0).Rows(j)("present") > 0 And rsLeaveMaster.Tables(0).Rows(j)("leave") > 0 Then
                                        pLeave = Math.Round((rsData.Tables(0).Rows(0)("mpresent") / rsLeaveMaster.Tables(0).Rows(j)("present")) * rsLeaveMaster.Tables(0).Rows(j)("leave"))
                                    Else
                                        pLeave = 0
                                    End If
                                Else
                                    pLeave = 0
                                End If
                            Else
                                pLeave = 0
                            End If
                            If (pLeave + bleave) > rsLeaveMaster.Tables(0).Rows(j)("leavelimit") Then
                                nleave = rsLeaveMaster.Tables(0).Rows(j)("leavelimit")
                            Else
                                nleave = pLeave + bleave
                            End If
                            sSql = "Update tblLeaveLedger Set " & lcode1 & "=" & nleave & " where paycode='" & Rs.Tables(0).Rows(i)("Paycode").ToString.Trim & "' and lyear=" & DateEdit.DateTime.ToString("yyyy")
                            If Common.servername = "Access" Then
                                cmd1 = New OleDbCommand(sSql, con1Access)
                                cmd1.ExecuteNonQuery()
                            Else
                                cmd = New SqlCommand(sSql, conSQL)
                                cmd.ExecuteNonQuery()
                            End If
                        Else
                            'sSql = "select * from tblleaveledger where paycode='" & Rs.Tables(0).Rows(i)("Paycode").ToString.Trim & "' and LYEAR=" & DateEdit.DateTime.ToString("yyyy") - 1
                            ''Set RSLeaveLedgerb = Cn.Execute(sSql)
                            'RSLeaveLedgerb = New DataSet
                            'If Common.servername = "Access" Then
                            '    adapA = New OleDbDataAdapter(sSql, con1Access)
                            '    adapA.Fill(RSLeaveLedgerb)
                            'Else
                            '    adap = New SqlDataAdapter(sSql, conSQL)
                            '    adap.Fill(RSLeaveLedgerb)
                            'End If
                            'If RSLeaveLedgerb.Tables(0).Rows.Count > 0 Then
                            '    nleave = RSLeaveLedgerb.Tables(0).Rows(0)(lcode1) - RSLeaveLedgerb.Tables(0).Rows(0)(lcode)
                            'Else
                            '    nleave = 0
                            'End If
                            'sSql = "Update tblLeaveLedger Set " & lcode1 & "=" & nleave & " where paycode='" & Rs.Tables(0).Rows(i)("Paycode").ToString.Trim & "' and lyear=" & DateEdit.DateTime.ToString("yyyy")
                            'If Common.servername = "Access" Then
                            '    cmd1 = New OleDbCommand(sSql, con1Access)
                            '    cmd1.ExecuteNonQuery()
                            'Else
                            '    cmd = New SqlCommand(sSql, conSQL)
                            '    cmd.ExecuteNonQuery()
                            'End If
                        End If
                    End If
                    'rsLeaveMaster.MoveNext
                    'If rsLeaveMaster.EOF Then
                    '    Exit Do
                    'End If
                Next '  Loop

                'Rs.MoveNext
                'If Rs.EOF Then
                '    Err.Description = "Auto Accrual Completed"
                '    Exit Do
                'End If
            Next 'Loop
        End If
        XtraMasterTest.LabelControlCount.Text = ""
        XtraMasterTest.LabelControlStatus.Text = ""
        Application.DoEvents()
tmp:    If Common.servername = "Access" Then
            If con1Access.State <> ConnectionState.Closed Then
                con1Access.Close()
            End If
        Else
            If conSQL.State <> ConnectionState.Closed Then
                conSQL.Close()
            End If
        End If
        Common.LogPost("Leave Increment; Month of " & Format(DateEdit.DateTime, "MMM yyyy"))
        Me.Cursor = Cursors.Default
        XtraMessageBox.Show(ulf, "<size=10>Processed Successfully</size>", "Success")
    End Sub

    Private Sub SimpleButton1_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton1.Click
        Me.Close()
    End Sub
End Class