﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class XtraDeviceEditUltra
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim GridLevelNode1 As DevExpress.XtraGrid.GridLevelNode = New DevExpress.XtraGrid.GridLevelNode()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEdit1 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.ComboBoxEdit1 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.ComboBoxEdit2 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.ComboBoxEdit3 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEdit2 = New DevExpress.XtraEditors.TextEdit()
        Me.GridLookUpEdit1 = New DevExpress.XtraEditors.GridLookUpEdit()
        Me.TblbranchBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SSSDBDataSet = New iAS.SSSDBDataSet()
        Me.GridView7 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.TextSrNo = New DevExpress.XtraEditors.TextEdit()
        Me.SimpleButton2 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.ComboBoxEdit4 = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.SimpleButtonGetSr = New DevExpress.XtraEditors.SimpleButton()
        Me.TextLED = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.TexHikLogin = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.TextHikPass = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.TblbranchTableAdapter = New iAS.SSSDBDataSetTableAdapters.tblbranchTableAdapter()
        Me.Tblbranch1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.tblbranch1TableAdapter()
        Me.TabPane1 = New DevExpress.XtraBars.Navigation.TabPane()
        Me.TabNavigationPage1 = New DevExpress.XtraBars.Navigation.TabNavigationPage()
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl()
        Me.LabelControl22 = New DevExpress.XtraEditors.LabelControl()
        Me.BtnVlidate = New DevExpress.XtraEditors.SimpleButton()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.LabelControl23 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl24 = New DevExpress.XtraEditors.LabelControl()
        Me.SimpleButtonUpload = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButtonDownload = New DevExpress.XtraEditors.SimpleButton()
        Me.TextEditUpload = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl25 = New DevExpress.XtraEditors.LabelControl()
        Me.TabNavigationPage2 = New DevExpress.XtraBars.Navigation.TabNavigationPage()
        Me.GroupCenterCom = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl21 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl20 = New DevExpress.XtraEditors.LabelControl()
        Me.GroupCenterCombox = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl19 = New DevExpress.XtraEditors.LabelControl()
        Me.btnGet = New DevExpress.XtraEditors.SimpleButton()
        Me.btnServerSave = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl18 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl()
        Me.TextAccPwd = New DevExpress.XtraEditors.TextEdit()
        Me.TextAcc = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl16 = New DevExpress.XtraEditors.LabelControl()
        Me.ComboISUPV = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.TextServerPort = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl15 = New DevExpress.XtraEditors.LabelControl()
        Me.TextServerIP = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl14 = New DevExpress.XtraEditors.LabelControl()
        Me.ComboServerType = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.textRes = New DevExpress.XtraEditors.TextEdit()
        Me.GroupControl5 = New DevExpress.XtraEditors.GroupControl()
        Me.GridControlZKDevice = New DevExpress.XtraGrid.GridControl()
        Me.GridViewZKDevice = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn4 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn5 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn6 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn7 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn8 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEditComm = New DevExpress.XtraEditors.TextEdit()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEdit3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridLookUpEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblbranchBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextSrNo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEdit4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextLED.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TexHikLogin.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextHikPass.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TabPane1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPane1.SuspendLayout()
        Me.TabNavigationPage1.SuspendLayout()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl2.SuspendLayout()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.TextEditUpload.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabNavigationPage2.SuspendLayout()
        CType(Me.GroupCenterCom.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupCenterCombox.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextAccPwd.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextAcc.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboISUPV.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextServerPort.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextServerIP.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboServerType.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.textRes.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl5.SuspendLayout()
        CType(Me.GridControlZKDevice, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewZKDevice, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEditComm.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Location = New System.Drawing.Point(17, 13)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(68, 14)
        Me.LabelControl1.TabIndex = 1
        Me.LabelControl1.Text = "Controller ID"
        '
        'TextEdit1
        '
        Me.TextEdit1.Location = New System.Drawing.Point(142, 10)
        Me.TextEdit1.Name = "TextEdit1"
        Me.TextEdit1.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TextEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEdit1.Properties.Appearance.Options.UseFont = True
        Me.TextEdit1.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEdit1.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEdit1.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEdit1.Properties.MaxLength = 3
        Me.TextEdit1.Size = New System.Drawing.Size(100, 20)
        Me.TextEdit1.TabIndex = 3
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl2.Appearance.Options.UseFont = True
        Me.LabelControl2.Location = New System.Drawing.Point(339, 65)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(45, 14)
        Me.LabelControl2.TabIndex = 4
        Me.LabelControl2.Text = "Protocol"
        Me.LabelControl2.Visible = False
        '
        'ComboBoxEdit1
        '
        Me.ComboBoxEdit1.EditValue = "TCP/IP"
        Me.ComboBoxEdit1.Location = New System.Drawing.Point(464, 62)
        Me.ComboBoxEdit1.Name = "ComboBoxEdit1"
        Me.ComboBoxEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEdit1.Properties.Appearance.Options.UseFont = True
        Me.ComboBoxEdit1.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEdit1.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboBoxEdit1.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEdit1.Properties.AppearanceFocused.Options.UseFont = True
        Me.ComboBoxEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEdit1.Properties.Items.AddRange(New Object() {"TCP/IP", "USB"})
        Me.ComboBoxEdit1.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.ComboBoxEdit1.Size = New System.Drawing.Size(100, 20)
        Me.ComboBoxEdit1.TabIndex = 4
        Me.ComboBoxEdit1.Visible = False
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl3.Appearance.Options.UseFont = True
        Me.LabelControl3.Location = New System.Drawing.Point(17, 39)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(28, 14)
        Me.LabelControl3.TabIndex = 18
        Me.LabelControl3.Text = "Type"
        '
        'ComboBoxEdit2
        '
        Me.ComboBoxEdit2.EditValue = "IN"
        Me.ComboBoxEdit2.Location = New System.Drawing.Point(142, 36)
        Me.ComboBoxEdit2.Name = "ComboBoxEdit2"
        Me.ComboBoxEdit2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEdit2.Properties.Appearance.Options.UseFont = True
        Me.ComboBoxEdit2.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEdit2.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboBoxEdit2.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEdit2.Properties.AppearanceFocused.Options.UseFont = True
        Me.ComboBoxEdit2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEdit2.Properties.Items.AddRange(New Object() {"IN", "OUT", "IN/OUT"})
        Me.ComboBoxEdit2.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.ComboBoxEdit2.Size = New System.Drawing.Size(100, 20)
        Me.ComboBoxEdit2.TabIndex = 5
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl4.Appearance.Options.UseFont = True
        Me.LabelControl4.Location = New System.Drawing.Point(327, 13)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(72, 14)
        Me.LabelControl4.TabIndex = 20
        Me.LabelControl4.Text = "Device Model"
        Me.LabelControl4.Visible = False
        '
        'ComboBoxEdit3
        '
        Me.ComboBoxEdit3.EditValue = "HKSeries"
        Me.ComboBoxEdit3.Location = New System.Drawing.Point(452, 10)
        Me.ComboBoxEdit3.Name = "ComboBoxEdit3"
        Me.ComboBoxEdit3.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEdit3.Properties.Appearance.Options.UseFont = True
        Me.ComboBoxEdit3.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEdit3.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboBoxEdit3.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEdit3.Properties.AppearanceFocused.Options.UseFont = True
        Me.ComboBoxEdit3.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEdit3.Properties.Items.AddRange(New Object() {"HKSeries"})
        Me.ComboBoxEdit3.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.ComboBoxEdit3.Size = New System.Drawing.Size(247, 20)
        Me.ComboBoxEdit3.TabIndex = 6
        Me.ComboBoxEdit3.Visible = False
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl5.Appearance.Options.UseFont = True
        Me.LabelControl5.Location = New System.Drawing.Point(17, 65)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(51, 14)
        Me.LabelControl5.TabIndex = 22
        Me.LabelControl5.Text = "Device IP"
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl6.Appearance.Options.UseFont = True
        Me.LabelControl6.Location = New System.Drawing.Point(17, 91)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(46, 14)
        Me.LabelControl6.TabIndex = 23
        Me.LabelControl6.Text = "Location"
        '
        'LabelControl7
        '
        Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl7.Appearance.Options.UseFont = True
        Me.LabelControl7.Location = New System.Drawing.Point(17, 169)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(47, 14)
        Me.LabelControl7.TabIndex = 24
        Me.LabelControl7.Text = "Serial No"
        '
        'TextEdit2
        '
        Me.TextEdit2.Location = New System.Drawing.Point(142, 62)
        Me.TextEdit2.Name = "TextEdit2"
        Me.TextEdit2.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TextEdit2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEdit2.Properties.Appearance.Options.UseFont = True
        Me.TextEdit2.Size = New System.Drawing.Size(179, 20)
        Me.TextEdit2.TabIndex = 7
        '
        'GridLookUpEdit1
        '
        Me.GridLookUpEdit1.Location = New System.Drawing.Point(142, 88)
        Me.GridLookUpEdit1.Name = "GridLookUpEdit1"
        Me.GridLookUpEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEdit1.Properties.Appearance.Options.UseFont = True
        Me.GridLookUpEdit1.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEdit1.Properties.AppearanceDropDown.Options.UseFont = True
        Me.GridLookUpEdit1.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridLookUpEdit1.Properties.AppearanceFocused.Options.UseFont = True
        Me.GridLookUpEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GridLookUpEdit1.Properties.DataSource = Me.TblbranchBindingSource
        Me.GridLookUpEdit1.Properties.DisplayMember = "BRANCHNAME"
        Me.GridLookUpEdit1.Properties.NullText = ""
        Me.GridLookUpEdit1.Properties.ValueMember = "BRANCHNAME"
        Me.GridLookUpEdit1.Properties.View = Me.GridView7
        Me.GridLookUpEdit1.Size = New System.Drawing.Size(179, 20)
        Me.GridLookUpEdit1.TabIndex = 8
        '
        'TblbranchBindingSource
        '
        Me.TblbranchBindingSource.DataMember = "tblbranch"
        Me.TblbranchBindingSource.DataSource = Me.SSSDBDataSet
        '
        'SSSDBDataSet
        '
        Me.SSSDBDataSet.DataSetName = "SSSDBDataSet"
        Me.SSSDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GridView7
        '
        Me.GridView7.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn2})
        Me.GridView7.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView7.Name = "GridView7"
        Me.GridView7.OptionsSelection.EnableAppearanceFocusedCell = False
        Me.GridView7.OptionsView.ShowGroupPanel = False
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Location Code"
        Me.GridColumn1.FieldName = "BRANCHCODE"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 0
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Location Name"
        Me.GridColumn2.FieldName = "BRANCHNAME"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 1
        '
        'TextSrNo
        '
        Me.TextSrNo.Location = New System.Drawing.Point(142, 166)
        Me.TextSrNo.Name = "TextSrNo"
        Me.TextSrNo.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TextSrNo.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextSrNo.Properties.Appearance.Options.UseFont = True
        Me.TextSrNo.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextSrNo.Properties.MaxLength = 50
        Me.TextSrNo.Size = New System.Drawing.Size(326, 20)
        Me.TextSrNo.TabIndex = 12
        '
        'SimpleButton2
        '
        Me.SimpleButton2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButton2.Appearance.Options.UseFont = True
        Me.SimpleButton2.Location = New System.Drawing.Point(223, 331)
        Me.SimpleButton2.Name = "SimpleButton2"
        Me.SimpleButton2.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton2.TabIndex = 15
        Me.SimpleButton2.Text = "Cancel"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButton1.Appearance.Options.UseFont = True
        Me.SimpleButton1.Location = New System.Drawing.Point(142, 331)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton1.TabIndex = 14
        Me.SimpleButton1.Text = "Save"
        '
        'ComboBoxEdit4
        '
        Me.ComboBoxEdit4.EditValue = "Attendance"
        Me.ComboBoxEdit4.Location = New System.Drawing.Point(464, 114)
        Me.ComboBoxEdit4.Name = "ComboBoxEdit4"
        Me.ComboBoxEdit4.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEdit4.Properties.Appearance.Options.UseFont = True
        Me.ComboBoxEdit4.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEdit4.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboBoxEdit4.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEdit4.Properties.AppearanceFocused.Options.UseFont = True
        Me.ComboBoxEdit4.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEdit4.Properties.Items.AddRange(New Object() {"Attendance", "Canteen"})
        Me.ComboBoxEdit4.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.ComboBoxEdit4.Size = New System.Drawing.Size(179, 20)
        Me.ComboBoxEdit4.TabIndex = 9
        Me.ComboBoxEdit4.Visible = False
        '
        'LabelControl8
        '
        Me.LabelControl8.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl8.Appearance.Options.UseFont = True
        Me.LabelControl8.Location = New System.Drawing.Point(339, 117)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(44, 14)
        Me.LabelControl8.TabIndex = 35
        Me.LabelControl8.Text = "Purpose"
        Me.LabelControl8.Visible = False
        '
        'SimpleButtonGetSr
        '
        Me.SimpleButtonGetSr.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonGetSr.Appearance.Options.UseFont = True
        Me.SimpleButtonGetSr.Location = New System.Drawing.Point(474, 165)
        Me.SimpleButtonGetSr.Name = "SimpleButtonGetSr"
        Me.SimpleButtonGetSr.Size = New System.Drawing.Size(50, 23)
        Me.SimpleButtonGetSr.TabIndex = 13
        Me.SimpleButtonGetSr.Text = "Get"
        '
        'TextLED
        '
        Me.TextLED.Location = New System.Drawing.Point(464, 36)
        Me.TextLED.Name = "TextLED"
        Me.TextLED.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TextLED.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextLED.Properties.Appearance.Options.UseFont = True
        Me.TextLED.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextLED.Properties.MaxLength = 50
        Me.TextLED.Size = New System.Drawing.Size(179, 20)
        Me.TextLED.TabIndex = 15
        Me.TextLED.Visible = False
        '
        'LabelControl10
        '
        Me.LabelControl10.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl10.Appearance.Options.UseFont = True
        Me.LabelControl10.Location = New System.Drawing.Point(339, 39)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(36, 14)
        Me.LabelControl10.TabIndex = 40
        Me.LabelControl10.Text = "LED IP"
        Me.LabelControl10.Visible = False
        '
        'TexHikLogin
        '
        Me.TexHikLogin.Location = New System.Drawing.Point(142, 114)
        Me.TexHikLogin.Name = "TexHikLogin"
        Me.TexHikLogin.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TexHikLogin.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TexHikLogin.Properties.Appearance.Options.UseFont = True
        Me.TexHikLogin.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TexHikLogin.Properties.MaxLength = 50
        Me.TexHikLogin.Size = New System.Drawing.Size(179, 20)
        Me.TexHikLogin.TabIndex = 10
        '
        'LabelControl11
        '
        Me.LabelControl11.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl11.Appearance.Options.UseFont = True
        Me.LabelControl11.Location = New System.Drawing.Point(17, 117)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(44, 14)
        Me.LabelControl11.TabIndex = 42
        Me.LabelControl11.Text = "Login Id"
        '
        'TextHikPass
        '
        Me.TextHikPass.Location = New System.Drawing.Point(142, 140)
        Me.TextHikPass.Name = "TextHikPass"
        Me.TextHikPass.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TextHikPass.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextHikPass.Properties.Appearance.Options.UseFont = True
        Me.TextHikPass.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextHikPass.Properties.MaxLength = 50
        Me.TextHikPass.Properties.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.TextHikPass.Size = New System.Drawing.Size(179, 20)
        Me.TextHikPass.TabIndex = 11
        '
        'LabelControl12
        '
        Me.LabelControl12.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl12.Appearance.Options.UseFont = True
        Me.LabelControl12.Location = New System.Drawing.Point(17, 143)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(51, 14)
        Me.LabelControl12.TabIndex = 44
        Me.LabelControl12.Text = "Password"
        '
        'TblbranchTableAdapter
        '
        Me.TblbranchTableAdapter.ClearBeforeFill = True
        '
        'Tblbranch1TableAdapter1
        '
        Me.Tblbranch1TableAdapter1.ClearBeforeFill = True
        '
        'TabPane1
        '
        Me.TabPane1.AllowTransitionAnimation = DevExpress.Utils.DefaultBoolean.[True]
        Me.TabPane1.Appearance.FontStyleDelta = System.Drawing.FontStyle.Italic
        Me.TabPane1.Appearance.Options.UseFont = True
        Me.TabPane1.AppearanceButton.Hovered.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Underline)
        Me.TabPane1.AppearanceButton.Hovered.Options.UseFont = True
        Me.TabPane1.AppearanceButton.Normal.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TabPane1.AppearanceButton.Normal.Options.UseFont = True
        Me.TabPane1.AppearanceButton.Pressed.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TabPane1.AppearanceButton.Pressed.Options.UseFont = True
        Me.TabPane1.Controls.Add(Me.TabNavigationPage1)
        Me.TabPane1.Controls.Add(Me.TabNavigationPage2)
        Me.TabPane1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabPane1.Location = New System.Drawing.Point(0, 0)
        Me.TabPane1.LookAndFeel.SkinName = "iMaginary"
        Me.TabPane1.Name = "TabPane1"
        Me.TabPane1.Pages.AddRange(New DevExpress.XtraBars.Navigation.NavigationPageBase() {Me.TabNavigationPage1, Me.TabNavigationPage2})
        Me.TabPane1.RegularSize = New System.Drawing.Size(623, 416)
        Me.TabPane1.SelectedPage = Me.TabNavigationPage1
        Me.TabPane1.Size = New System.Drawing.Size(623, 416)
        Me.TabPane1.TabIndex = 106
        Me.TabPane1.Text = "Server Details"
        Me.TabPane1.TransitionType = DevExpress.Utils.Animation.Transitions.Cover
        '
        'TabNavigationPage1
        '
        Me.TabNavigationPage1.Caption = "Device Details"
        Me.TabNavigationPage1.Controls.Add(Me.PanelControl2)
        Me.TabNavigationPage1.Controls.Add(Me.PanelControl1)
        Me.TabNavigationPage1.Controls.Add(Me.LabelControl1)
        Me.TabNavigationPage1.Controls.Add(Me.TextHikPass)
        Me.TabNavigationPage1.Controls.Add(Me.TextEdit1)
        Me.TabNavigationPage1.Controls.Add(Me.LabelControl12)
        Me.TabNavigationPage1.Controls.Add(Me.LabelControl2)
        Me.TabNavigationPage1.Controls.Add(Me.TexHikLogin)
        Me.TabNavigationPage1.Controls.Add(Me.ComboBoxEdit1)
        Me.TabNavigationPage1.Controls.Add(Me.LabelControl11)
        Me.TabNavigationPage1.Controls.Add(Me.LabelControl3)
        Me.TabNavigationPage1.Controls.Add(Me.TextLED)
        Me.TabNavigationPage1.Controls.Add(Me.ComboBoxEdit2)
        Me.TabNavigationPage1.Controls.Add(Me.LabelControl10)
        Me.TabNavigationPage1.Controls.Add(Me.LabelControl4)
        Me.TabNavigationPage1.Controls.Add(Me.TextEditComm)
        Me.TabNavigationPage1.Controls.Add(Me.ComboBoxEdit3)
        Me.TabNavigationPage1.Controls.Add(Me.LabelControl9)
        Me.TabNavigationPage1.Controls.Add(Me.LabelControl5)
        Me.TabNavigationPage1.Controls.Add(Me.SimpleButtonGetSr)
        Me.TabNavigationPage1.Controls.Add(Me.LabelControl6)
        Me.TabNavigationPage1.Controls.Add(Me.ComboBoxEdit4)
        Me.TabNavigationPage1.Controls.Add(Me.LabelControl7)
        Me.TabNavigationPage1.Controls.Add(Me.LabelControl8)
        Me.TabNavigationPage1.Controls.Add(Me.TextEdit2)
        Me.TabNavigationPage1.Controls.Add(Me.SimpleButton2)
        Me.TabNavigationPage1.Controls.Add(Me.GridLookUpEdit1)
        Me.TabNavigationPage1.Controls.Add(Me.SimpleButton1)
        Me.TabNavigationPage1.Controls.Add(Me.TextSrNo)
        Me.TabNavigationPage1.Name = "TabNavigationPage1"
        Me.TabNavigationPage1.Size = New System.Drawing.Size(603, 371)
        '
        'PanelControl2
        '
        Me.PanelControl2.Controls.Add(Me.LabelControl22)
        Me.PanelControl2.Controls.Add(Me.BtnVlidate)
        Me.PanelControl2.Location = New System.Drawing.Point(11, 289)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(566, 36)
        Me.PanelControl2.TabIndex = 53
        '
        'LabelControl22
        '
        Me.LabelControl22.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl22.Appearance.Options.UseFont = True
        Me.LabelControl22.Location = New System.Drawing.Point(5, 9)
        Me.LabelControl22.Name = "LabelControl22"
        Me.LabelControl22.Size = New System.Drawing.Size(69, 14)
        Me.LabelControl22.TabIndex = 51
        Me.LabelControl22.Text = "Online mode"
        '
        'BtnVlidate
        '
        Me.BtnVlidate.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.BtnVlidate.Appearance.Options.UseFont = True
        Me.BtnVlidate.Enabled = False
        Me.BtnVlidate.Location = New System.Drawing.Point(153, 5)
        Me.BtnVlidate.Name = "BtnVlidate"
        Me.BtnVlidate.Size = New System.Drawing.Size(75, 23)
        Me.BtnVlidate.TabIndex = 13
        Me.BtnVlidate.Text = "Activate"
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.LabelControl23)
        Me.PanelControl1.Controls.Add(Me.LabelControl24)
        Me.PanelControl1.Controls.Add(Me.SimpleButtonUpload)
        Me.PanelControl1.Controls.Add(Me.SimpleButtonDownload)
        Me.PanelControl1.Controls.Add(Me.TextEditUpload)
        Me.PanelControl1.Controls.Add(Me.LabelControl25)
        Me.PanelControl1.Location = New System.Drawing.Point(11, 194)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(566, 89)
        Me.PanelControl1.TabIndex = 52
        '
        'LabelControl23
        '
        Me.LabelControl23.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl23.Appearance.Options.UseFont = True
        Me.LabelControl23.Location = New System.Drawing.Point(5, 29)
        Me.LabelControl23.Name = "LabelControl23"
        Me.LabelControl23.Size = New System.Drawing.Size(113, 14)
        Me.LabelControl23.TabIndex = 50
        Me.LabelControl23.Text = "Download Device file"
        '
        'LabelControl24
        '
        Me.LabelControl24.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl24.Appearance.Options.UseFont = True
        Me.LabelControl24.Location = New System.Drawing.Point(5, 57)
        Me.LabelControl24.Name = "LabelControl24"
        Me.LabelControl24.Size = New System.Drawing.Size(96, 14)
        Me.LabelControl24.TabIndex = 47
        Me.LabelControl24.Text = "Upload Device file"
        '
        'SimpleButtonUpload
        '
        Me.SimpleButtonUpload.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonUpload.Appearance.Options.UseFont = True
        Me.SimpleButtonUpload.Location = New System.Drawing.Point(463, 53)
        Me.SimpleButtonUpload.Name = "SimpleButtonUpload"
        Me.SimpleButtonUpload.Size = New System.Drawing.Size(87, 23)
        Me.SimpleButtonUpload.TabIndex = 13
        Me.SimpleButtonUpload.Text = "Upload"
        '
        'SimpleButtonDownload
        '
        Me.SimpleButtonDownload.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonDownload.Appearance.Options.UseFont = True
        Me.SimpleButtonDownload.Location = New System.Drawing.Point(153, 25)
        Me.SimpleButtonDownload.Name = "SimpleButtonDownload"
        Me.SimpleButtonDownload.Size = New System.Drawing.Size(87, 23)
        Me.SimpleButtonDownload.TabIndex = 14
        Me.SimpleButtonDownload.Text = "Download"
        '
        'TextEditUpload
        '
        Me.TextEditUpload.Location = New System.Drawing.Point(153, 54)
        Me.TextEditUpload.Name = "TextEditUpload"
        Me.TextEditUpload.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditUpload.Properties.Appearance.Options.UseFont = True
        Me.TextEditUpload.Size = New System.Drawing.Size(304, 20)
        Me.TextEditUpload.TabIndex = 12
        '
        'LabelControl25
        '
        Me.LabelControl25.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl25.Appearance.Options.UseFont = True
        Me.LabelControl25.Location = New System.Drawing.Point(5, 5)
        Me.LabelControl25.Name = "LabelControl25"
        Me.LabelControl25.Size = New System.Drawing.Size(70, 14)
        Me.LabelControl25.TabIndex = 46
        Me.LabelControl25.Text = "Offline mode"
        '
        'TabNavigationPage2
        '
        Me.TabNavigationPage2.Appearance.BorderColor = System.Drawing.Color.Black
        Me.TabNavigationPage2.Appearance.Options.UseBorderColor = True
        Me.TabNavigationPage2.Caption = "Server Details"
        Me.TabNavigationPage2.Controls.Add(Me.GroupCenterCom)
        Me.TabNavigationPage2.Controls.Add(Me.LabelControl21)
        Me.TabNavigationPage2.Controls.Add(Me.LabelControl20)
        Me.TabNavigationPage2.Controls.Add(Me.GroupCenterCombox)
        Me.TabNavigationPage2.Controls.Add(Me.LabelControl19)
        Me.TabNavigationPage2.Controls.Add(Me.btnGet)
        Me.TabNavigationPage2.Controls.Add(Me.btnServerSave)
        Me.TabNavigationPage2.Controls.Add(Me.LabelControl18)
        Me.TabNavigationPage2.Controls.Add(Me.LabelControl17)
        Me.TabNavigationPage2.Controls.Add(Me.TextAccPwd)
        Me.TabNavigationPage2.Controls.Add(Me.TextAcc)
        Me.TabNavigationPage2.Controls.Add(Me.LabelControl16)
        Me.TabNavigationPage2.Controls.Add(Me.ComboISUPV)
        Me.TabNavigationPage2.Controls.Add(Me.TextServerPort)
        Me.TabNavigationPage2.Controls.Add(Me.LabelControl15)
        Me.TabNavigationPage2.Controls.Add(Me.TextServerIP)
        Me.TabNavigationPage2.Controls.Add(Me.LabelControl14)
        Me.TabNavigationPage2.Controls.Add(Me.ComboServerType)
        Me.TabNavigationPage2.Controls.Add(Me.LabelControl13)
        Me.TabNavigationPage2.Controls.Add(Me.textRes)
        Me.TabNavigationPage2.Controls.Add(Me.GroupControl5)
        Me.TabNavigationPage2.Name = "TabNavigationPage2"
        Me.TabNavigationPage2.Size = New System.Drawing.Size(603, 371)
        '
        'GroupCenterCom
        '
        Me.GroupCenterCom.EditValue = ""
        Me.GroupCenterCom.Location = New System.Drawing.Point(244, 246)
        Me.GroupCenterCom.Name = "GroupCenterCom"
        Me.GroupCenterCom.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GroupCenterCom.Properties.Appearance.Options.UseFont = True
        Me.GroupCenterCom.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GroupCenterCom.Properties.AppearanceDropDown.Options.UseFont = True
        Me.GroupCenterCom.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GroupCenterCom.Properties.AppearanceFocused.Options.UseFont = True
        Me.GroupCenterCom.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GroupCenterCom.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.GroupCenterCom.Size = New System.Drawing.Size(130, 20)
        Me.GroupCenterCom.TabIndex = 2
        Me.GroupCenterCom.Visible = False
        '
        'LabelControl21
        '
        Me.LabelControl21.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl21.Appearance.Options.UseFont = True
        Me.LabelControl21.Location = New System.Drawing.Point(121, 249)
        Me.LabelControl21.Name = "LabelControl21"
        Me.LabelControl21.Size = New System.Drawing.Size(115, 14)
        Me.LabelControl21.TabIndex = 80
        Me.LabelControl21.Text = "Upload Center Group"
        Me.LabelControl21.Visible = False
        '
        'LabelControl20
        '
        Me.LabelControl20.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl20.Appearance.ForeColor = System.Drawing.Color.Blue
        Me.LabelControl20.Appearance.Options.UseFont = True
        Me.LabelControl20.Appearance.Options.UseForeColor = True
        Me.LabelControl20.Location = New System.Drawing.Point(17, 226)
        Me.LabelControl20.Name = "LabelControl20"
        Me.LabelControl20.Size = New System.Drawing.Size(244, 14)
        Me.LabelControl20.TabIndex = 78
        Me.LabelControl20.Text = "Please Get first then Enter all values and Set"
        Me.LabelControl20.Visible = False
        '
        'GroupCenterCombox
        '
        Me.GroupCenterCombox.EditValue = "CENTER1"
        Me.GroupCenterCombox.Enabled = False
        Me.GroupCenterCombox.Location = New System.Drawing.Point(140, 9)
        Me.GroupCenterCombox.Name = "GroupCenterCombox"
        Me.GroupCenterCombox.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GroupCenterCombox.Properties.Appearance.Options.UseFont = True
        Me.GroupCenterCombox.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GroupCenterCombox.Properties.AppearanceDropDown.Options.UseFont = True
        Me.GroupCenterCombox.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GroupCenterCombox.Properties.AppearanceFocused.Options.UseFont = True
        Me.GroupCenterCombox.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.GroupCenterCombox.Properties.Items.AddRange(New Object() {"CENTER1"})
        Me.GroupCenterCombox.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.GroupCenterCombox.Size = New System.Drawing.Size(130, 20)
        Me.GroupCenterCombox.TabIndex = 1
        '
        'LabelControl19
        '
        Me.LabelControl19.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl19.Appearance.Options.UseFont = True
        Me.LabelControl19.Location = New System.Drawing.Point(17, 12)
        Me.LabelControl19.Name = "LabelControl19"
        Me.LabelControl19.Size = New System.Drawing.Size(74, 14)
        Me.LabelControl19.TabIndex = 77
        Me.LabelControl19.Text = "Center Group"
        '
        'btnGet
        '
        Me.btnGet.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.btnGet.Appearance.Options.UseFont = True
        Me.btnGet.Location = New System.Drawing.Point(152, 194)
        Me.btnGet.Name = "btnGet"
        Me.btnGet.Size = New System.Drawing.Size(75, 23)
        Me.btnGet.TabIndex = 9
        Me.btnGet.Text = "Get"
        '
        'btnServerSave
        '
        Me.btnServerSave.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.btnServerSave.Appearance.Options.UseFont = True
        Me.btnServerSave.Location = New System.Drawing.Point(233, 194)
        Me.btnServerSave.Name = "btnServerSave"
        Me.btnServerSave.Size = New System.Drawing.Size(75, 23)
        Me.btnServerSave.TabIndex = 10
        Me.btnServerSave.Text = "Set"
        '
        'LabelControl18
        '
        Me.LabelControl18.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl18.Appearance.Options.UseFont = True
        Me.LabelControl18.Location = New System.Drawing.Point(17, 168)
        Me.LabelControl18.Name = "LabelControl18"
        Me.LabelControl18.Size = New System.Drawing.Size(101, 14)
        Me.LabelControl18.TabIndex = 74
        Me.LabelControl18.Text = "Account Password"
        Me.LabelControl18.Visible = False
        '
        'LabelControl17
        '
        Me.LabelControl17.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl17.Appearance.Options.UseFont = True
        Me.LabelControl17.Location = New System.Drawing.Point(17, 142)
        Me.LabelControl17.Name = "LabelControl17"
        Me.LabelControl17.Size = New System.Drawing.Size(81, 14)
        Me.LabelControl17.TabIndex = 73
        Me.LabelControl17.Text = "Account Name"
        '
        'TextAccPwd
        '
        Me.TextAccPwd.Enabled = False
        Me.TextAccPwd.Location = New System.Drawing.Point(140, 165)
        Me.TextAccPwd.Name = "TextAccPwd"
        Me.TextAccPwd.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TextAccPwd.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextAccPwd.Properties.Appearance.Options.UseFont = True
        Me.TextAccPwd.Properties.MaxLength = 20
        Me.TextAccPwd.Size = New System.Drawing.Size(179, 20)
        Me.TextAccPwd.TabIndex = 8
        Me.TextAccPwd.Visible = False
        '
        'TextAcc
        '
        Me.TextAcc.Enabled = False
        Me.TextAcc.Location = New System.Drawing.Point(140, 139)
        Me.TextAcc.Name = "TextAcc"
        Me.TextAcc.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TextAcc.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextAcc.Properties.Appearance.Options.UseFont = True
        Me.TextAcc.Properties.MaxLength = 12
        Me.TextAcc.Size = New System.Drawing.Size(179, 20)
        Me.TextAcc.TabIndex = 7
        '
        'LabelControl16
        '
        Me.LabelControl16.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl16.Appearance.Options.UseFont = True
        Me.LabelControl16.Location = New System.Drawing.Point(17, 116)
        Me.LabelControl16.Name = "LabelControl16"
        Me.LabelControl16.Size = New System.Drawing.Size(40, 14)
        Me.LabelControl16.TabIndex = 70
        Me.LabelControl16.Text = "Version"
        '
        'ComboISUPV
        '
        Me.ComboISUPV.EditValue = "V5.0"
        Me.ComboISUPV.Enabled = False
        Me.ComboISUPV.Location = New System.Drawing.Point(140, 113)
        Me.ComboISUPV.Name = "ComboISUPV"
        Me.ComboISUPV.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboISUPV.Properties.Appearance.Options.UseFont = True
        Me.ComboISUPV.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboISUPV.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboISUPV.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboISUPV.Properties.AppearanceFocused.Options.UseFont = True
        Me.ComboISUPV.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboISUPV.Properties.Items.AddRange(New Object() {"None", "V2.0", "V4.0", "V5.0"})
        Me.ComboISUPV.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.ComboISUPV.Size = New System.Drawing.Size(130, 20)
        Me.ComboISUPV.TabIndex = 6
        '
        'TextServerPort
        '
        Me.TextServerPort.Enabled = False
        Me.TextServerPort.Location = New System.Drawing.Point(140, 87)
        Me.TextServerPort.Name = "TextServerPort"
        Me.TextServerPort.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TextServerPort.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextServerPort.Properties.Appearance.Options.UseFont = True
        Me.TextServerPort.Properties.Mask.EditMask = "[0-9]*"
        Me.TextServerPort.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextServerPort.Properties.MaxLength = 6
        Me.TextServerPort.Size = New System.Drawing.Size(179, 20)
        Me.TextServerPort.TabIndex = 5
        '
        'LabelControl15
        '
        Me.LabelControl15.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl15.Appearance.Options.UseFont = True
        Me.LabelControl15.Location = New System.Drawing.Point(17, 90)
        Me.LabelControl15.Name = "LabelControl15"
        Me.LabelControl15.Size = New System.Drawing.Size(62, 14)
        Me.LabelControl15.TabIndex = 67
        Me.LabelControl15.Text = "Server Port"
        '
        'TextServerIP
        '
        Me.TextServerIP.Location = New System.Drawing.Point(140, 61)
        Me.TextServerIP.Name = "TextServerIP"
        Me.TextServerIP.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TextServerIP.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextServerIP.Properties.Appearance.Options.UseFont = True
        Me.TextServerIP.Size = New System.Drawing.Size(179, 20)
        Me.TextServerIP.TabIndex = 4
        '
        'LabelControl14
        '
        Me.LabelControl14.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl14.Appearance.Options.UseFont = True
        Me.LabelControl14.Location = New System.Drawing.Point(17, 64)
        Me.LabelControl14.Name = "LabelControl14"
        Me.LabelControl14.Size = New System.Drawing.Size(50, 14)
        Me.LabelControl14.TabIndex = 65
        Me.LabelControl14.Text = "Server IP"
        '
        'ComboServerType
        '
        Me.ComboServerType.EditValue = "IP/IP6"
        Me.ComboServerType.Enabled = False
        Me.ComboServerType.Location = New System.Drawing.Point(140, 35)
        Me.ComboServerType.Name = "ComboServerType"
        Me.ComboServerType.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboServerType.Properties.Appearance.Options.UseFont = True
        Me.ComboServerType.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboServerType.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboServerType.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboServerType.Properties.AppearanceFocused.Options.UseFont = True
        Me.ComboServerType.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboServerType.Properties.Items.AddRange(New Object() {"None", "IP/IP6", "Domain Name"})
        Me.ComboServerType.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor
        Me.ComboServerType.Size = New System.Drawing.Size(130, 20)
        Me.ComboServerType.TabIndex = 3
        '
        'LabelControl13
        '
        Me.LabelControl13.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl13.Appearance.Options.UseFont = True
        Me.LabelControl13.Location = New System.Drawing.Point(17, 38)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(67, 14)
        Me.LabelControl13.TabIndex = 63
        Me.LabelControl13.Text = "Server Type"
        '
        'textRes
        '
        Me.textRes.Location = New System.Drawing.Point(10, 454)
        Me.textRes.Name = "textRes"
        Me.textRes.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.textRes.Properties.Appearance.Options.UseFont = True
        Me.textRes.Properties.ReadOnly = True
        Me.textRes.Size = New System.Drawing.Size(190, 20)
        Me.textRes.TabIndex = 62
        Me.textRes.Visible = False
        '
        'GroupControl5
        '
        Me.GroupControl5.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl5.AppearanceCaption.Options.UseFont = True
        Me.GroupControl5.Controls.Add(Me.GridControlZKDevice)
        Me.GroupControl5.Location = New System.Drawing.Point(151, 361)
        Me.GroupControl5.Name = "GroupControl5"
        Me.GroupControl5.Size = New System.Drawing.Size(149, 120)
        Me.GroupControl5.TabIndex = 47
        Me.GroupControl5.Text = "Device List"
        Me.GroupControl5.Visible = False
        '
        'GridControlZKDevice
        '
        Me.GridControlZKDevice.Dock = System.Windows.Forms.DockStyle.Fill
        GridLevelNode1.RelationName = "Level1"
        Me.GridControlZKDevice.LevelTree.Nodes.AddRange(New DevExpress.XtraGrid.GridLevelNode() {GridLevelNode1})
        Me.GridControlZKDevice.Location = New System.Drawing.Point(2, 23)
        Me.GridControlZKDevice.MainView = Me.GridViewZKDevice
        Me.GridControlZKDevice.Name = "GridControlZKDevice"
        Me.GridControlZKDevice.Size = New System.Drawing.Size(145, 95)
        Me.GridControlZKDevice.TabIndex = 1
        Me.GridControlZKDevice.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewZKDevice})
        '
        'GridViewZKDevice
        '
        Me.GridViewZKDevice.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn4, Me.GridColumn5, Me.GridColumn6, Me.GridColumn7, Me.GridColumn8})
        Me.GridViewZKDevice.GridControl = Me.GridControlZKDevice
        Me.GridViewZKDevice.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always
        Me.GridViewZKDevice.Name = "GridViewZKDevice"
        Me.GridViewZKDevice.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewZKDevice.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewZKDevice.OptionsBehavior.Editable = False
        Me.GridViewZKDevice.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridViewZKDevice.OptionsSelection.MultiSelect = True
        Me.GridViewZKDevice.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        Me.GridViewZKDevice.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.GridColumn4, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'GridColumn4
        '
        Me.GridColumn4.Caption = "Controller Id"
        Me.GridColumn4.FieldName = "ID_NO"
        Me.GridColumn4.Name = "GridColumn4"
        Me.GridColumn4.Visible = True
        Me.GridColumn4.VisibleIndex = 1
        '
        'GridColumn5
        '
        Me.GridColumn5.Caption = "Device IP"
        Me.GridColumn5.FieldName = "LOCATION"
        Me.GridColumn5.Name = "GridColumn5"
        Me.GridColumn5.Visible = True
        Me.GridColumn5.VisibleIndex = 2
        '
        'GridColumn6
        '
        Me.GridColumn6.Caption = "Location"
        Me.GridColumn6.FieldName = "branch"
        Me.GridColumn6.Name = "GridColumn6"
        Me.GridColumn6.Visible = True
        Me.GridColumn6.VisibleIndex = 3
        '
        'GridColumn7
        '
        Me.GridColumn7.Caption = "Device Type"
        Me.GridColumn7.FieldName = "DeviceType"
        Me.GridColumn7.Name = "GridColumn7"
        Me.GridColumn7.Visible = True
        Me.GridColumn7.VisibleIndex = 4
        '
        'GridColumn8
        '
        Me.GridColumn8.FieldName = "A_R"
        Me.GridColumn8.Name = "GridColumn8"
        '
        'LabelControl9
        '
        Me.LabelControl9.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl9.Appearance.Options.UseFont = True
        Me.LabelControl9.Location = New System.Drawing.Point(339, 91)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(58, 14)
        Me.LabelControl9.TabIndex = 38
        Me.LabelControl9.Text = "Comm Key"
        Me.LabelControl9.Visible = False
        '
        'TextEditComm
        '
        Me.TextEditComm.Location = New System.Drawing.Point(464, 88)
        Me.TextEditComm.Name = "TextEditComm"
        Me.TextEditComm.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.[False]
        Me.TextEditComm.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEditComm.Properties.Appearance.Options.UseFont = True
        Me.TextEditComm.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEditComm.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextEditComm.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEditComm.Properties.MaxLength = 6
        Me.TextEditComm.Size = New System.Drawing.Size(179, 20)
        Me.TextEditComm.TabIndex = 12
        Me.TextEditComm.Visible = False
        '
        'XtraDeviceEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(623, 416)
        Me.Controls.Add(Me.TabPane1)
        Me.LookAndFeel.SkinName = "iMaginary"
        Me.LookAndFeel.UseDefaultLookAndFeel = False
        Me.Name = "XtraDeviceEdit"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEdit3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridLookUpEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblbranchBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextSrNo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEdit4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextLED.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TexHikLogin.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextHikPass.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TabPane1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPane1.ResumeLayout(False)
        Me.TabNavigationPage1.ResumeLayout(False)
        Me.TabNavigationPage1.PerformLayout()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl2.ResumeLayout(False)
        Me.PanelControl2.PerformLayout()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.TextEditUpload.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabNavigationPage2.ResumeLayout(False)
        Me.TabNavigationPage2.PerformLayout()
        CType(Me.GroupCenterCom.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupCenterCombox.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextAccPwd.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextAcc.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboISUPV.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextServerPort.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextServerIP.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboServerType.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.textRes.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl5.ResumeLayout(False)
        CType(Me.GridControlZKDevice, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewZKDevice, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEditComm.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEdit1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ComboBoxEdit1 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ComboBoxEdit2 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ComboBoxEdit3 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEdit2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents GridLookUpEdit1 As DevExpress.XtraEditors.GridLookUpEdit
    Friend WithEvents GridView7 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents TextSrNo As DevExpress.XtraEditors.TextEdit
    Friend WithEvents SimpleButton2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SSSDBDataSet As iAS.SSSDBDataSet
    Friend WithEvents TblbranchBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblbranchTableAdapter As iAS.SSSDBDataSetTableAdapters.tblbranchTableAdapter
    Friend WithEvents Tblbranch1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.tblbranch1TableAdapter
    Friend WithEvents ComboBoxEdit4 As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SimpleButtonGetSr As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents TextLED As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TexHikLogin As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextHikPass As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TabPane1 As DevExpress.XtraBars.Navigation.TabPane
    Friend WithEvents TabNavigationPage1 As DevExpress.XtraBars.Navigation.TabNavigationPage
    Friend WithEvents TabNavigationPage2 As DevExpress.XtraBars.Navigation.TabNavigationPage
    Friend WithEvents textRes As DevExpress.XtraEditors.TextEdit
    Friend WithEvents GroupControl5 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GridControlZKDevice As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridViewZKDevice As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn8 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents ComboServerType As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextServerIP As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl14 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextServerPort As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl15 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl18 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextAccPwd As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextAcc As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl16 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ComboISUPV As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents btnGet As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnServerSave As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GroupCenterCombox As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents LabelControl19 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl20 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents GroupCenterCom As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents LabelControl21 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LabelControl22 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents BtnVlidate As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents LabelControl23 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl24 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SimpleButtonUpload As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButtonDownload As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents TextEditUpload As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl25 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextEditComm As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
End Class
