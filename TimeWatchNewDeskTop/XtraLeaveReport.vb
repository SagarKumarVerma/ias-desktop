﻿Imports System.Resources
Imports System.Globalization
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports System.ComponentModel
Imports DevExpress.XtraGrid.Columns
Imports System.Data.SqlClient
Imports System.Text
Imports System.Data.OleDb
Imports DevExpress.XtraSplashScreen
Imports System.Runtime.CompilerServices
Imports Microsoft.Office.Interop
Imports Microsoft.Office.Interop.Excel
Public Class XtraLeaveReport
    Dim ulf As UserLookAndFeel
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    Dim g_LinesPerPage As Integer
    Dim mStrProcName As String
    Dim mblnCheckReport As Boolean
    Dim mintPageNo As Integer
    Dim mintLine As Integer
    Dim g_WhereClause As String
    Dim mFileNumber As Integer
    Dim mstrFile_Name As String
    Dim g_SkipAfterDept As Boolean
    Dim WhichReport As Integer
    Dim FromDateTodate As String
    Dim tmpNepali As String
    Public Sub New()
        InitializeComponent()
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True
    End Sub
    Private Sub XtraLeaveReport_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Me.Width = Common.NavWidth 'Me.Parent.Width
        Me.Height = Common.NavHeight 'Me.Parent.Height
        'SplitContainerControl1.Width = Common.splitforMasterMenuWidth 'SplitContainerControl1.Parent.Width
        SplitContainerControl1.SplitterPosition = Common.SplitterPosition '(SplitContainerControl1.Parent.Width) * 85 / 100

        DateEdit1.DateTime = New DateTime(DateTime.Now.Year, DateTime.Now.Month, 1)
        DateEdit2.EditValue = Now
        TextEdit2.Text = Now.Year

        If Common.IsNepali = "Y" Then
            ComboNepaliYearFrm.Visible = True
            ComboNEpaliMonthFrm.Visible = True
            ComboNepaliDateFrm.Visible = True
            ComboNepaliYearTo.Visible = True
            ComboNEpaliMonthTo.Visible = True
            ComboNepaliDateTo.Visible = True

            DateEdit1.Visible = False
            DateEdit2.Visible = False

            ''original
            Dim DC As New DateConverter()
            'Dim doj As DateTime = DC.ToBS(New Date(DateEdit1.DateTime.Year, DateEdit1.DateTime.Month, DateEdit1.DateTime.Day))
            'ComboNepaliYearFrm.EditValue = doj.Year
            'ComboNEpaliMonthFrm.SelectedIndex = doj.Month - 1
            'ComboNepaliDateFrm.EditValue = doj.Day

            'doj = DC.ToBS(New Date(DateEdit2.DateTime.Year, DateEdit2.DateTime.Month, DateEdit2.DateTime.Day))
            'ComboNepaliYearTo.EditValue = doj.Year
            'ComboNEpaliMonthTo.SelectedIndex = doj.Month - 1
            'ComboNepaliDateTo.EditValue = doj.Day

            Dim doj As String = DC.ToBS(New Date(DateEdit1.DateTime.Year, DateEdit1.DateTime.Month, DateEdit1.DateTime.Day))
            Dim dojTmp() As String = doj.Split("-")
            ComboNepaliYearFrm.EditValue = dojTmp(0)
            ComboNEpaliMonthFrm.SelectedIndex = dojTmp(1) - 1
            ComboNepaliDateFrm.EditValue = dojTmp(2)

            doj = DC.ToBS(New Date(DateEdit2.DateTime.Year, DateEdit2.DateTime.Month, DateEdit2.DateTime.Day))
            dojTmp = doj.Split("-")
            ComboNepaliYearTo.EditValue = dojTmp(0)
            ComboNEpaliMonthTo.SelectedIndex = dojTmp(1) - 1
            ComboNepaliDateTo.EditValue = dojTmp(2)

            TextEdit2.Text = ComboNepaliYearFrm.EditValue
        Else
            ComboNepaliYearFrm.Visible = False
            ComboNEpaliMonthFrm.Visible = False
            ComboNepaliDateFrm.Visible = False
            ComboNepaliYearTo.Visible = False
            ComboNEpaliMonthTo.Visible = False
            ComboNepaliDateTo.Visible = False
            DateEdit1.Visible = True
            DateEdit2.Visible = True
        End If
        If Common.servername = "Access" Then
            Me.TblLeaveMaster1TableAdapter1.Fill(Me.SSSDBDataSet.tblLeaveMaster1)
            GridLookUpEdit1.Properties.DataSource = SSSDBDataSet.tblLeaveMaster1
        Else
            TblLeaveMasterTableAdapter.Connection.ConnectionString = Common.ConnectionString
            Me.TblLeaveMasterTableAdapter.Fill(Me.SSSDBDataSet.tblLeaveMaster)
            GridLookUpEdit1.Properties.DataSource = SSSDBDataSet.tblLeaveMaster
        End If

        If Common.servername = "Access" Then
            'Me.TblCompany1TableAdapter1.Fill(Me.SSSDBDataSet.tblCompany1)
            'GridControlComp.DataSource = SSSDBDataSet.tblCompany1

            'Me.TblEmployee1TableAdapter1.Fill(Me.SSSDBDataSet.TblEmployee1)
            'GridControlEmp.DataSource = SSSDBDataSet.TblEmployee1

            Me.TblDepartment1TableAdapter1.Fill(Me.SSSDBDataSet.tblDepartment1)
            GridControlDept.DataSource = SSSDBDataSet.tblDepartment1

            Me.TblCatagory1TableAdapter1.Fill(Me.SSSDBDataSet.tblCatagory1)
            GridControlCat.DataSource = SSSDBDataSet.tblCatagory1

            Me.TblShiftMaster1TableAdapter1.Fill(Me.SSSDBDataSet.tblShiftMaster1)
            GridControlShift.DataSource = SSSDBDataSet.tblShiftMaster1

            Me.TblGrade1TableAdapter1.Fill(Me.SSSDBDataSet.tblGrade1)
            GridControlGrade.DataSource = SSSDBDataSet.tblGrade1

            'Me.Tblbranch1TableAdapter1.Fill(Me.SSSDBDataSet.tblbranch1)
            'GridControlBranch.DataSource = SSSDBDataSet.tblbranch1
        Else
            'TblCompanyTableAdapter.Connection.ConnectionString = Common.ConnectionString
            'Me.TblCompanyTableAdapter.Fill(Me.SSSDBDataSet.tblCompany)
            'GridControlComp.DataSource = SSSDBDataSet.tblCompany

            TblEmployeeTableAdapter.Connection.ConnectionString = Common.ConnectionString
            'Me.TblEmployeeTableAdapter.Fill(Me.SSSDBDataSet.TblEmployee)
            'GridControlEmp.DataSource = SSSDBDataSet.TblEmployee

            TblDepartmentTableAdapter.Connection.ConnectionString = Common.ConnectionString
            Me.TblDepartmentTableAdapter.Fill(Me.SSSDBDataSet.tblDepartment)
            GridControlDept.DataSource = SSSDBDataSet.tblDepartment

            TblCatagoryTableAdapter.Connection.ConnectionString = Common.ConnectionString
            Me.TblCatagoryTableAdapter.Fill(Me.SSSDBDataSet.tblCatagory)
            GridControlCat.DataSource = SSSDBDataSet.tblCatagory

            TblShiftMasterTableAdapter.Connection.ConnectionString = Common.ConnectionString
            Me.TblShiftMasterTableAdapter.Fill(Me.SSSDBDataSet.tblShiftMaster)
            GridControlShift.DataSource = SSSDBDataSet.tblShiftMaster

            TblGradeTableAdapter.Connection.ConnectionString = Common.ConnectionString
            Me.TblGradeTableAdapter.Fill(Me.SSSDBDataSet.tblGrade)
            GridControlGrade.DataSource = SSSDBDataSet.tblGrade

            TblbranchTableAdapter.Connection.ConnectionString = Common.ConnectionString
            'Me.TblbranchTableAdapter.Fill(Me.SSSDBDataSet.tblbranch)
            'GridControlBranch.DataSource = SSSDBDataSet.tblbranch
        End If

        GridControlBranch.DataSource = Common.LocationNonAdmin
        GridControlEmp.DataSource = Common.EmpNonAdmin
        GridControlComp.DataSource = Common.CompanyNonAdmin

        Common.SetGridFont(GridViewComp, New System.Drawing.Font("Tahoma", 10))
        Common.SetGridFont(GridViewEmp, New System.Drawing.Font("Tahoma", 10))
        Common.SetGridFont(GridViewCat, New System.Drawing.Font("Tahoma", 10))
        Common.SetGridFont(GridViewDept, New System.Drawing.Font("Tahoma", 10))
        Common.SetGridFont(GridViewGrade, New System.Drawing.Font("Tahoma", 10))
        Common.SetGridFont(GridViewShift, New System.Drawing.Font("Tahoma", 10))
        Common.SetGridFont(GridViewBranch, New System.Drawing.Font("Tahoma", 10))

    End Sub
    Private Sub GridLookUpEdit1_CustomDisplayText(sender As System.Object, e As DevExpress.XtraEditors.Controls.CustomDisplayTextEventArgs) Handles GridLookUpEdit1.CustomDisplayText
        Try
            Dim gridLookUpEdit = CType(sender, GridLookUpEdit)
            Dim dataRowView = CType(gridLookUpEdit.Properties.GetRowByKeyValue(e.Value), DataRowView)
            Dim row = CType(dataRowView.Row, DataRow)
            e.DisplayText = (row("LEAVECODE").ToString + ("  " + row("LEAVEDESCRIPTION").ToString))
        Catch
            'MsgBox("Catch")
        End Try
    End Sub
    Private Sub CheckSanctionedLeaves_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckSanctionedLeaves.CheckedChanged
        If CheckSanctionedLeaves.Checked = True Then
            LabelControl1.Visible = True
            GridLookUpEdit1.Visible = True
            LabelControl2.Text = "From Date"
            TextEdit2.Visible = False
            LabelControl3.Visible = True
            If Common.IsNepali = "Y" Then
                DateEdit1.Visible = False
                DateEdit2.Visible = False
                ComboNepaliDateFrm.Visible = True
                ComboNEpaliMonthFrm.Visible = True
                ComboNepaliYearFrm.Visible = True
                ComboNepaliDateTo.Visible = True
                ComboNEpaliMonthTo.Visible = True
                ComboNepaliYearTo.Visible = True
            Else
                DateEdit1.Visible = True
                DateEdit2.Visible = True
                ComboNepaliDateFrm.Visible = False
                ComboNEpaliMonthFrm.Visible = False
                ComboNepaliYearFrm.Visible = False
                ComboNepaliDateTo.Visible = False
                ComboNEpaliMonthTo.Visible = False
                ComboNepaliYearTo.Visible = False
            End If
        Else
            LabelControl1.Visible = False
            GridLookUpEdit1.Visible = False
            LabelControl2.Text = "Year"
            TextEdit2.Visible = True
            DateEdit1.Visible = False
            DateEdit2.Visible = False
            LabelControl3.Visible = False

            ComboNepaliDateFrm.Visible = False
            ComboNEpaliMonthFrm.Visible = False
            ComboNepaliYearFrm.Visible = False
            ComboNepaliDateTo.Visible = False
            ComboNEpaliMonthTo.Visible = False
            ComboNepaliYearTo.Visible = False
        End If
    End Sub
    Private Sub SimpleButton1_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton1.Click
        If Common.IsNepali = "Y" Then
            tmpNepali = TextEdit2.Text
            Dim DC As New DateConverter()
            'Dim tmpNow As DateTime = DC.ToBS(New Date(Now.Year, Now.Month, Now.Day))
            'Common.runningDateTime = tmpNow.Day & "/" & Common.NepaliMonth(tmpNow.Month - 1) & "/" & tmpNow.Year & " " & Now.ToString("HH:mm")
            'FromDateTodate = ComboNepaliDateFrm.EditValue & "/" & ComboNEpaliMonthFrm.EditValue.ToString.Trim & "/" & ComboNepaliYearFrm.EditValue & " To " & ComboNepaliDateTo.EditValue & "/" & ComboNEpaliMonthTo.EditValue.ToString.Trim & "/" & ComboNepaliYearTo.EditValue
            Dim tmpNow As String = DC.ToBS(New Date(Now.Year, Now.Month, Now.Day))
            Dim dojTmp() As String = tmpNow.Split("-")
            Common.runningDateTime = dojTmp(2) & "/" & Common.NepaliMonth(dojTmp(1) - 1) & "/" & dojTmp(0) & " " & Now.ToString("HH:mm")
            FromDateTodate = ComboNepaliDateFrm.EditValue & "/" & ComboNEpaliMonthFrm.EditValue.ToString.Trim & "/" & ComboNepaliYearFrm.EditValue & " To " & ComboNepaliDateTo.EditValue & "/" & ComboNEpaliMonthTo.EditValue.ToString.Trim & "/" & ComboNepaliYearTo.EditValue
            Try
                'DateEdit1.DateTime = DC.ToAD(New Date(ComboNepaliYearFrm.EditValue, ComboNEpaliMonthFrm.SelectedIndex + 1, ComboNepaliDateFrm.EditValue))
                DateEdit1.DateTime = DC.ToAD(ComboNepaliYearFrm.EditValue & "-" & ComboNEpaliMonthFrm.SelectedIndex + 1 & "-" & ComboNepaliDateFrm.EditValue)
            Catch ex As Exception
                XtraMessageBox.Show(ulf, "<size=10>Invalid From Date</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                ComboNepaliDateFrm.Select()
                Exit Sub
            End Try

            Try
                'DateEdit2.DateTime = DC.ToAD(New Date(ComboNepaliYearTo.EditValue, ComboNEpaliMonthTo.SelectedIndex + 1, ComboNepaliDateTo.EditValue))
                DateEdit2.DateTime = DC.ToAD(ComboNepaliYearTo.EditValue & "-" & ComboNEpaliMonthTo.SelectedIndex + 1 & "-" & ComboNepaliDateTo.EditValue)
            Catch ex As Exception
                XtraMessageBox.Show(ulf, "<size=10>Invalid To Date</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                ComboNepaliDateTo.Select()
                Exit Sub
            End Try

            Dim tmp As DateTime = DC.ToAD(TextEdit2.Text & "-1-1")
            TextEdit2.Text = tmp.Year
        Else
            Common.runningDateTime = Now.ToString("dd/MM/yyyy HH:mm")
            FromDateTodate = DateEdit1.DateTime.ToString("dd/MM/yyyy") & " To " & DateEdit2.DateTime.ToString("dd/MM/yyyy")
        End If

        If license.TrialPerioad And DateEdit1.DateTime.ToString("yyyy-MM-dd") < Convert.ToDateTime(Common.InstalledDate).ToString("yyyy-MM-dd") Then
            XtraMessageBox.Show(ulf, "<size=10>Cannot View Older than Installed Date Report in Trial Version</size>", "<size=9>iAS</size>")
            DateEdit1.Select()
            Exit Sub
        End If

        g_LinesPerPage = TextEdit1.Text
        If CheckDeptSkip.Checked = True Then
            g_SkipAfterDept = True
        Else
            g_SkipAfterDept = False
        End If
        Set_Filter_Crystal()
        If SidePanelSelection.Visible = False Then
            g_WhereClause = ""
        End If
        If CheckSanctionedLeaves.Checked = True Then
            If CheckText.Checked = True Then
                Monthly_SanctionedLeave("")
                mStrProcName = "Monthly_SanctionedLeave"
            Else
                MonthlyXl_SanctionedLeave("")
                mStrProcName = "MonthlyXl_SanctionedLeave"
                mblnCheckReport = False
                'mReportPrintStatus = False
            End If
        ElseIf CheckLeaveCard.Checked = True Then
            If CheckText.Checked = True Then
                Monthly_LeaveCard("")
                mStrProcName = "Monthly_LeaveCard"
            Else
                'MonthlyXl_LeaveCard("")
                MonthlyXl_LeaveCardGrid("")
                mStrProcName = "MonthlyXl_LeaveCard"
                mblnCheckReport = False
                'mReportPrintStatus = False
            End If
        ElseIf CheckAccruedLeaves.Checked = True Then
            If CheckText.Checked = True Then
                Monthly_AccruedLeave("")
                mStrProcName = "Monthly_AccruedLeave"
            Else
                'MonthlyXl_AccruedLeave("")
                MonthlyXl_AccruedLeaveGrid("")
                mStrProcName = "MonthlyXl_AccruedLeave"
                mblnCheckReport = False
                'mReportPrintStatus = False
            End If
        ElseIf CheckConsumedLeaves.Checked = True Then
            If CheckText.Checked = True Then
                Monthly_ConsumedLeave("")
                mStrProcName = "Monthly_ConsumedLeave"
            Else
                'MonthlyXl_ConsumedLeave("")
                MonthlyXl_ConsumedLeaveGrid("")
                mStrProcName = "MonthlyXl_ConsumedLeave"
                mblnCheckReport = False
                'mReportPrintStatus = False
            End If
        ElseIf CheckBalanceLeaves.Checked = True Then
            If CheckText.Checked = True Then
                Monthly_BalanceLeave("")
                mStrProcName = "Monthly_BalanceLeave"
            Else
                'MonthlyXl_BalanceLeave("")
                MonthlyXl_BalanceLeaveGrid("")
                mStrProcName = "MonthlyXl_BalanceLeave"
                mblnCheckReport = False
                'mReportPrintStatus = False
            End If
        ElseIf CheckLeaveRegister.Checked = True Then
            If CheckText.Checked = True Then
                Monthly_LeaveRegister("")
                mStrProcName = "Monthly_LeaveRegister"
            Else
                'MonthlyXl_LeaveRegister("")
                'mStrProcName = "MonthlyXl_LeaveRegister"
                'mblnCheckReport = False
                'mReportPrintStatus = False
            End If
        End If
        If Common.IsNepali = "Y" Then
            TextEdit2.Text = ComboNepaliYearFrm.EditValue
        End If
    End Sub
    Sub Monthly_SanctionedLeave(ByVal strsortorder As String)
        'On Error GoTo ErrorGen
        Dim strDeptDivCode As String
        Dim strCurrentDeptDivCode As String
        Dim intFile As Integer
        Dim strsql As String
        Dim strDepartmentCode As String
        Dim intPrintLineCounter As Integer
        Dim blnDeptAvailable As Boolean, mFirst As Boolean

        Dim strPayCode As String
        Dim strLeaveCode As String
        Dim strLeaveCode1 As String
        Dim dtLFromDate As Date
        Dim dtLToDate As Date
        Dim dblLeaveAmount As Double
        Dim strEmpCD As String
        Dim strEmpName As String
        Dim strDesig As String
        Dim strVN As String
        Dim strReason As String
        Dim strVoucher As String

        Dim DC As New DateConverter() 'nepali
        Dim tmpFrmDate As String 'for nepali
        Dim tmpToDate As String 'for nepali

        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If
        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet

        mblnCheckReport = False
        mFirst = True
        mintPageNo = 1
        mintLine = 1

        'If Left(strsortorder, 1) = "D" Or Left(strsortorder, 1) = "C" Then blnDeptAvailable = True
        'If strsortorder = "" Then
        'If g_Report_view Then
        If GridLookUpEdit1.EditValue = "" Then   'for all
            If Common.servername = "Access" Then
                strsql = "select tblCatagory.CatagoryName, tbltimeregister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tblTimeRegister.PayCode,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tblTimeRegister.DateOffice,tblTimeRegister.Voucher_No,tblTimeRegister.LeaveCode,tblTimeRegister.firsthalfLeaveCode,tblTimeRegister.SecondhalfLeaveCode,tblTimeRegister.Leaveamount1,tblTimeRegister.Leaveamount2,tblTimeRegister.LeaveAmount,tblTimeRegister.Reason,tblEmployee.EmpName,tblEmployee.PresentCardNo,tblEmployee.Designation" & _
                 " from tblCatagory, tblTimeRegister,tblEmployee,tblCompany,tblDepartment" & _
                " Where tblCatagory.Cat = tblEmployee.Cat And tblTimeRegister.PayCode = tblEmployee.PayCode And tblTimeRegister.LeaveAmount>0 And FORMAT(tblTimeRegister.DateOffice,'yyyy-MM-dd') Between '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' AND '" & DateEdit2.DateTime.ToString("yyyy-MM-dd") & "'" & _
                " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
                " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause)
                adapA = New OleDbDataAdapter(strsql, Common.con1)
                adapA.Fill(Rs_Report)
            Else
                strsql = "select tblCatagory.CatagoryName, tbltimeregister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tblTimeRegister.PayCode,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tblTimeRegister.DateOffice,tblTimeRegister.Voucher_No,tblTimeRegister.LeaveCode,tblTimeRegister.firsthalfLeaveCode,tblTimeRegister.SecondhalfLeaveCode,tblTimeRegister.Leaveamount1,tblTimeRegister.Leaveamount2,tblTimeRegister.LeaveAmount,tblTimeRegister.Reason,tblEmployee.EmpName,tblEmployee.PresentCardNo,tblEmployee.Designation" & _
                 " from tblCatagory, tblTimeRegister,tblEmployee,tblCompany,tblDepartment" & _
                " Where tblCatagory.Cat = tblEmployee.Cat And tblTimeRegister.PayCode = tblEmployee.PayCode And tblTimeRegister.LeaveAmount>0 And tblTimeRegister.DateOffice Between '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' AND '" & DateEdit2.DateTime.ToString("yyyy-MM-dd") & "'" & _
                " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
                " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause)
                adap = New SqlDataAdapter(strsql, Common.con)
                adap.Fill(Rs_Report)
            End If
        Else
            If Common.servername = "Access" Then
                strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tbltimeregister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tblTimeRegister.PayCode,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tblTimeRegister.DateOffice,tblTimeRegister.Voucher_No,tblTimeRegister.LeaveCode,tblTimeRegister.firsthalfLeaveCode,tblTimeRegister.SecondhalfLeaveCode,tblTimeRegister.Leaveamount1,tblTimeRegister.Leaveamount2,tblTimeRegister.LeaveAmount,tblTimeRegister.Reason,tblEmployee.EmpName,tblEmployee.PresentCardNo,tblEmployee.Designation" & _
                 " from tblCatagory,tblDivision,  tblTimeRegister,tblEmployee,tblCompany,tblDepartment" & _
                " Where tblTimeRegister.LeaveCode='" & GridLookUpEdit1.EditValue.ToString.Trim & "' and  tblCatagory.Cat = tblEmployee.Cat And tblDivision.DivisionCode = tblEmployee.DivisionCode And tblTimeRegister.PayCode = tblEmployee.PayCode And tblTimeRegister.LeaveAmount>0 And FORMAT(tblTimeRegister.DateOffice,'yyyy-MM-dd') Between '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' AND '" & DateEdit2.DateTime.ToString("yyyy-MM-dd") & "'" & _
                " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
                " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause)

                adapA = New OleDbDataAdapter(strsql, Common.con1)
                adapA.Fill(Rs_Report)
            Else
                strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tbltimeregister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tblTimeRegister.PayCode,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tblTimeRegister.DateOffice,tblTimeRegister.Voucher_No,tblTimeRegister.LeaveCode,tblTimeRegister.firsthalfLeaveCode,tblTimeRegister.SecondhalfLeaveCode,tblTimeRegister.Leaveamount1,tblTimeRegister.Leaveamount2,tblTimeRegister.LeaveAmount,tblTimeRegister.Reason,tblEmployee.EmpName,tblEmployee.PresentCardNo,tblEmployee.Designation" & _
                 " from tblCatagory,tblDivision,  tblTimeRegister,tblEmployee,tblCompany,tblDepartment" & _
                " Where tblTimeRegister.LeaveCode='" & GridLookUpEdit1.EditValue.ToString.Trim & "' and  tblCatagory.Cat = tblEmployee.Cat And tblDivision.DivisionCode = tblEmployee.DivisionCode And tblTimeRegister.PayCode = tblEmployee.PayCode And tblTimeRegister.LeaveAmount>0 And tblTimeRegister.DateOffice Between '" & DateEdit2.DateTime.ToString("yyyy-MM-dd") & "' AND '" & DateEdit2.DateTime.ToString("yyyy-MM-dd") & "'" & _
                " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
                " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause)
                adap = New SqlDataAdapter(strsql, Common.con)
                adap.Fill(Rs_Report)
            End If
        End If
        'Else
        '    strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tbltimeregisterD.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregisterD.PayCode,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tbltimeregisterD.DateOffice,tbltimeregisterD.Voucher_No,tbltimeregisterD.LeaveCode,tbltimeregisterD.firsthalfLeaveCode,tbltimeregisterD.SecondhalfLeaveCode,tbltimeregisterD.Leaveamount1,tbltimeregisterD.Leaveamount2,tbltimeregisterD.LeaveAmount,tbltimeregisterD.Reason,tblEmployee.EmpName,tblEmployee.PresentCardNo,tblEmployee.Designation" & _
        '         " from tblCatagory,tblDivision,  tbltimeregisterD,tblEmployee,tblCompany,tblDepartment" & _
        '         " Where tblCatagory.Cat = tblEmployee.Cat And tblDivision.DivisionCode = tblEmployee.DivisionCode And tbltimeregisterD.PayCode = tblEmployee.PayCode And tbltimeregisterD.LeaveAmount>0 And tbltimeregisterD.DateOffice Between '" & Format(frmLeaveReport.txtFromDate.Value, "MMM dd yyyy") & "' AND '" & Format(frmLeaveReport.TxtToDate.Value, "MMM dd yyyy") & "'" & _
        '         " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
        '         " And (tblemployee.LeavingDate>='" & Format(frmLeaveReport.txtFromDate, "MMM dd yyyy") & "' or tblemployee.LeavingDate is null)" & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause)
        'End If
        'Rs_Report = Cn.Execute(strsql)
        If Rs_Report.Tables(0).Rows.Count < 1 Then
            XtraMessageBox.Show(ulf, "<size=10>No Data Available For this Report.</size>", "<size=9>iAS</size>")
            Exit Sub
        End If

        mFileNumber = FreeFile()
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".SRP"
        Dim objWriter As New System.IO.StreamWriter(mstrFile_Name, True)
        'With Rs_Report
        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            If mFirst Or mintLine > g_LinesPerPage Or (strsortorder.Trim = "Department" And strDeptDivCode <> Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim And g_SkipAfterDept) Then
                'If mFirst Or mintLine > g_LinesPerPage Or (Left(strsortorder, 10) = "Department" And strDeptDivCode <> !DepartmentCode And g_SkipAfterDept) Then
1:              If Not mFirst Then
                    objWriter.WriteLine(Space(2) & vbFormFeed)
                Else
                    mFirst = False
                End If
                objWriter.WriteLine(Space(2) & Space(40) & CommonReport.g_CompanyNames)
                objWriter.WriteLine(Space(2) & "")
                objWriter.WriteLine(Space(2) & "Page No." & mintPageNo)
                objWriter.WriteLine(Space(2) & "                                                                                                                  Run Date & Time : " & Common.runningDateTime) 'Format(Now(), "dd/MM/yyyy HH:mm"))
                objWriter.WriteLine(Space(2) & Space(40) & "Sanctioned Leave Report from " & FromDateTodate) 'DateEdit1.DateTime.ToString("dd/MM/yyyy") & " To " & DateEdit2.DateTime.ToString("dd/MM/yyyy"))
                'objWriter.WriteLine(Space(2) & "----------------------------------------------------------------------------------------------------------------------------------------------------------------")
                'objWriter.WriteLine(Space(2) & "Paycode         Card         Employee Name             DESIGNATION                 ---------Date---------  Voucher    Leave Leave   Posted  Remarks")
                'objWriter.WriteLine(Space(2) & "                No.                                                                From        To          No         Type  Amount")
                'objWriter.WriteLine(Space(2) & "----------------------------------------------------------------------------------------------------------------------------------------------------------------")
                objWriter.WriteLine(Space(2) & "----------------------------------------------------------------------------------------------------------------------------------------------------------------")
                objWriter.WriteLine(Space(2) & "Paycode         Card         Employee Name             DESIGNATION                 --------------Date--------------  Voucher    Leave Leave   Posted  Remarks")
                objWriter.WriteLine(Space(2) & "                No.                                                                From             To               No         Type  Amount")
                objWriter.WriteLine(Space(2) & "----------------------------------------------------------------------------------------------------------------------------------------------------------------")
                mintLine = 9
                mintPageNo = mintPageNo + 1
            End If
            'If blnDeptAvailable And Not .EOF Then
            '    If Left(strsortorder, 10) = "Department" And strDeptDivCode <> !DepartmentCode Then
            '        If mintLine + 3 > g_LinesPerPage Then GoTo 1
            '        Print #intFile, Space(2) + ""
            '        strDeptDivCode = !DepartmentCode
            '        Print #intFile, Space(2) + "** Department Code & Name : " & !DepartmentCode & "  " & !DepartmentName
            '        Print #intFile, Space(2) + ""
            '        mintLine = mintLine + 3
            '    ElseIf Left(strsortorder, 8) = "Division" And strDeptDivCode <> !DivisionCode Then
            '        If mintLine + 3 > g_LinesPerPage Then GoTo 1
            '        Print #intFile, Space(2) + ""
            '        strDeptDivCode = !DivisionCode
            '        Print #intFile, Space(2) + "** Section Code & Name : " & !DivisionCode & "  " & !DivisionName
            '        Print #intFile, Space(2) + ""
            '        mintLine = mintLine + 3
            '    ElseIf Left(strsortorder, 3) = "Cat" And strDeptDivCode <> !Cat Then
            '        If mintLine + 3 > g_LinesPerPage Then GoTo 1
            '        Print #intFile, Space(2) + ""
            '        strDeptDivCode = !Cat
            '        Print #intFile, Space(2) + "** Category Code & Name : " & !Cat & "  " & !CatagoryName
            '        Print #intFile, Space(2) + ""
            '        mintLine = mintLine + 3
            '    End If
            'End If

            strPayCode = Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim
            strEmpCD = Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim
            strEmpName = Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim
            If Rs_Report.Tables(0).Rows(i).Item("Designation").ToString.Trim = "" Then
                strDesig = Space(20)
            Else
                strDesig = Rs_Report.Tables(0).Rows(i).Item("Designation").ToString.Trim & Space(25 - Len(Rs_Report.Tables(0).Rows(i).Item("Designation").ToString.Trim))
            End If
            strVN = IIf(Rs_Report.Tables(0).Rows(i).Item("Voucher_No").ToString.Trim = "", Space(10), Rs_Report.Tables(0).Rows(i).Item("Voucher_No").ToString.Trim)
            strReason = IIf(Rs_Report.Tables(0).Rows(i).Item("reason").ToString.Trim = "", "", Rs_Report.Tables(0).Rows(i).Item("reason").ToString.Trim)

            strLeaveCode = Rs_Report.Tables(0).Rows(i).Item("Leavecode").ToString.Trim
            dtLFromDate = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("DATEOFFICE").ToString.Trim)
            dblLeaveAmount = 0
            strVoucher = strVN
            strLeaveCode1 = ""
            If Rs_Report.Tables(0).Rows(i).Item("FIRSTHALFLEAVECODE").ToString.Trim <> "" And Rs_Report.Tables(0).Rows(i).Item("SECONDHALFLEAVECODE").ToString.Trim <> "" Then
                strLeaveCode1 = Rs_Report.Tables(0).Rows(i).Item("FIRSTHALFLEAVECODE").ToString.Trim
                If strLeaveCode1 = strLeaveCode Then
                    strLeaveCode1 = Rs_Report.Tables(0).Rows(i).Item("SECONDHALFLEAVECODE").ToString.Trim
                End If
            End If
            Do While strLeaveCode = Rs_Report.Tables(0).Rows(i).Item("Leavecode").ToString.Trim And strVoucher = strVN
                If strPayCode = Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim Then
                    If Rs_Report.Tables(0).Rows(i).Item("leaveamount").ToString.Trim = "" Then
                        dblLeaveAmount = dblLeaveAmount + 0
                    Else
                        dblLeaveAmount = dblLeaveAmount + Rs_Report.Tables(0).Rows(i).Item("leaveamount").ToString.Trim
                    End If

                    dtLToDate = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("DATEOFFICE").ToString.Trim)
                    i = i + 1
                    '.MoveNext()
                    'If .EOF Or strVoucher = strVN Then
                    '    Exit Do
                    'End If
                    If i > Rs_Report.Tables(0).Rows.Count - 1 Then
                        i = i - 1
                        GoTo tmp
                        Exit Do
                    End If
                    strVN = IIf(Rs_Report.Tables(0).Rows(i).Item("Voucher_No").ToString.Trim = "", Space(10), Rs_Report.Tables(0).Rows(i).Item("Voucher_No").ToString.Trim)
                    If DateDiff("d", dtLToDate, Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("DATEOFFICE").ToString.Trim)) > 1 Then Exit Do
                Else
                    Exit Do
                End If
            Loop
            i = i - 1
            If i > Rs_Report.Tables(0).Rows.Count - 1 Then
                Exit For
            End If
tmp:        If Common.IsNepali = "Y" Then
                Dim Vstart As String = DC.ToBS(New Date(dtLFromDate.Year, dtLFromDate.Month, dtLFromDate.Day))
                Dim dojTmp() As String = Vstart.Split("-")
                tmpFrmDate = dojTmp(2) & "/" & Common.NepaliMonth(dojTmp(1) - 1) & "/" & dojTmp(0)
                Vstart = DC.ToBS(New Date(dtLToDate.Year, dtLToDate.Month, dtLToDate.Day))
                dojTmp = Vstart.Split("-")
                tmpToDate = dojTmp(2) & "/" & Common.NepaliMonth(dojTmp(1) - 1) & "/" & dojTmp(0)
            Else
                tmpFrmDate = dtLFromDate.ToString("dd/MM/yyyy")
                tmpToDate = dtLToDate.ToString("dd/MM/yyyy")
            End If
            If strLeaveCode1 <> "" Then
                'objWriter.WriteLine(Space(2) & strPayCode.PadRight(12) & Space(2) & Space(2) & strEmpCD.PadRight(12) & Space(1) & Space(1) & strEmpName.PadRight(25) & Space(1) & Space(1) & _
                '            strDesig.PadRight(25) & Space(1) & Space(1) & dtLFromDate & Space(2) & dtLToDate & Space(2) & _
                '            strVN & Space(1) & IIf(Rs_Report.Tables(0).Rows(i).Item("strLeaveCode").ToString.Trim = "", Space(3), strLeaveCode) & Space(3) & _
                '            Length7(CDbl(0.5)) & Space(5) & "Y" & Space(6) & strReason)
                objWriter.WriteLine(Space(2) & strPayCode.PadRight(12) & Space(2) & Space(2) & strEmpCD.PadRight(12) & Space(1) & Space(1) & strEmpName.PadRight(25) & Space(1) & Space(1) & _
                         strDesig.PadRight(25) & Space(1) & Space(1) & tmpFrmDate.PadRight(15) & Space(2) & tmpToDate.PadRight(15) & Space(2) & _
                         strVN & Space(1) & IIf(Rs_Report.Tables(0).Rows(i).Item("strLeaveCode").ToString.Trim = "", Space(3), strLeaveCode) & Space(3) & _
                         Length7(CDbl(0.5)) & Space(5) & "Y" & Space(6) & strReason)

                'objWriter.WriteLine(Space(2) & strPayCode.PadRight(12) & Space(1) & Space(2) & strEmpCD.PadRight(12) & Space(1) & Space(1) & strEmpName.PadRight(25) & Space(1) & Space(1) & _
                '             strDesig.PadRight(25) & Space(1) & Space(1) & dtLFromDate & Space(2) & dtLToDate & Space(2) & _
                '             strVN & Space(1) & IIf(Rs_Report.Tables(0).Rows(i).Item("strLeaveCode1").ToString.Trim = "", Space(3), strLeaveCode1.PadRight(3)) & Space(3) & _
                '             Length7(CDbl(0.5)) & Space(5) & "Y" & Space(6) & strReason)
                objWriter.WriteLine(Space(2) & strPayCode.PadRight(12) & Space(1) & Space(2) & strEmpCD.PadRight(12) & Space(1) & Space(1) & strEmpName.PadRight(25) & Space(1) & Space(1) & _
                             strDesig.PadRight(25) & Space(1) & Space(1) & tmpFrmDate.PadRight(15) & Space(2) & tmpToDate.PadRight(15) & Space(2) & _
                             strVN & Space(1) & IIf(Rs_Report.Tables(0).Rows(i).Item("strLeaveCode1").ToString.Trim = "", Space(3), strLeaveCode1.PadRight(3)) & Space(3) & _
                             Length7(CDbl(0.5)) & Space(5) & "Y" & Space(6) & strReason)
                mintLine = mintLine + 2
            Else
                'objWriter.WriteLine(Space(2) & strPayCode.PadRight(12) & Space(1) & Space(2) & strEmpCD.PadRight(12) & Space(1) & Space(1) & strEmpName.PadRight(25) & Space(1) & Space(1) & _
                '            strDesig.PadRight(25) & Space(1) & Space(1) & dtLFromDate & Space(2) & dtLToDate & Space(2) & _
                '            strVN & Space(1) & IIf(Rs_Report.Tables(0).Rows(i).Item("LeaveCode").ToString.Trim = "", Space(3), strLeaveCode.PadRight(3)) & Space(3) & _
                '            Length7(CDbl(dblLeaveAmount)) & Space(5) & "Y" & Space(6) & strReason)
                objWriter.WriteLine(Space(2) & strPayCode.PadRight(12) & Space(1) & Space(2) & strEmpCD.PadRight(12) & Space(1) & Space(1) & strEmpName.PadRight(25) & Space(1) & Space(1) & _
                           strDesig.PadRight(25) & Space(1) & Space(1) & tmpFrmDate.PadRight(15) & Space(2) & tmpToDate.PadRight(15) & Space(2) & _
                           strVN & Space(1) & IIf(Rs_Report.Tables(0).Rows(i).Item("LeaveCode").ToString.Trim = "", Space(3), strLeaveCode.PadRight(3)) & Space(3) & _
                           Length7(CDbl(dblLeaveAmount)) & Space(5) & "Y" & Space(6) & strReason)
                mintLine = mintLine + 1

            End If
            mblnCheckReport = True

            'If .EOF Then
            '    Exit Do
            'End If
            If i > Rs_Report.Tables(0).Rows.Count - 1 Then
                Exit For
            End If
        Next
        objWriter.Close()
        Try
            Process.Start(mstrFile_Name)
        Catch ex As Exception
            Process.Start("notepad.exe", mstrFile_Name)
        End Try
    End Sub
    Sub MonthlyXl_SanctionedLeave(ByVal strsortorder As String)
        Dim whereClause As String = ""
        If Common.dashBoardClick = "Leave" Then
            whereClause = " TBLEmployee.ACTIVE='Y'"
            DateEdit1.DateTime = Now
            DateEdit2.DateTime = Now
            GridLookUpEdit1.EditValue = ""
            If Common.IsNepali = "Y" Then
                Dim DC1 As New DateConverter()
                'Dim tmpNow As String = DC1.ToBS(New Date(Now.Year, Now.Month, Now.Day))
                'Dim dojTmp() As String = tmpNow.Split("-")
                'Common.runningDateTime = dojTmp(2) & "/" & Common.NepaliMonth(dojTmp(1) - 1) & "/" & dojTmp(0) & " " & Now.ToString("HH:mm")
                'Dim Vstart As String = DC1.ToBS(New Date(DateEdit1.DateTime.Year, DateEdit1.DateTime.Month, DateEdit1.DateTime.Day))
                'dojTmp = Vstart.Split("-")
                'FromDateTodate = dojTmp(2) & "/" & Common.NepaliMonth(dojTmp(1) - 1) & "/" & dojTmp(0) & " To " & dojTmp(2) & "/" & Common.NepaliMonth(dojTmp(1) - 1) & "/" & dojTmp(0)

                Dim tmpNow As String = DC1.ToBS(New Date(Now.Year, Now.Month, Now.Day))
                Dim dojTmp() As String = tmpNow.Split("-")
                Common.runningDateTime = dojTmp(2) & "/" & Common.NepaliMonth(dojTmp(1) - 1) & "/" & dojTmp(0) & " " & Now.ToString("HH:mm")
                Dim Vstart As String = DC1.ToBS(New Date(DateEdit1.DateTime.Year, DateEdit1.DateTime.Month, DateEdit1.DateTime.Day))
                dojTmp = Vstart.Split("-")
                FromDateTodate = dojTmp(2) & "/" & Common.NepaliMonth(dojTmp(1) - 1) & "/" & dojTmp(0) & " To " & dojTmp(2) & "/" & Common.NepaliMonth(dojTmp(1) - 1) & "/" & dojTmp(0)


            Else
                FromDateTodate = DateEdit1.DateTime.ToString("dd/MM/yyyy") & " To " & DateEdit2.DateTime.ToString("dd/MM/yyyy")
                Common.runningDateTime = Now.ToString("dd/MM/yyyy HH:mm")
            End If
        Else
            whereClause = g_WhereClause
        End If


        'If Common.USERTYPE <> "A" Then
        '    Dim emp() As String = Common.Auth_Branch.Split(",")
        '    Dim ls As New List(Of String)()
        '    For x As Integer = 0 To emp.Length - 1
        '        ls.Add(emp(x).Trim)
        '    Next
        '    If whereClause = "" Then
        '        whereClause = " TBLEmployee.BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "')"
        '    Else
        '        whereClause = whereClause & " and TBLEmployee.BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "')"
        '    End If
        'End If

        If Common.USERTYPE <> "A" Then
            Dim emp() As String = Common.Auth_Branch.Split(",")
            Dim ls As New List(Of String)()
            For x As Integer = 0 To emp.Length - 1
                ls.Add(emp(x).Trim)
            Next

            Dim com() As String = Common.auth_comp.Split(",")
            Dim lsC As New List(Of String)()
            For x As Integer = 0 To com.Length - 1
                lsC.Add(com(x).Trim)
            Next

            If whereClause = "" Then
                whereClause = " TBLEmployee.BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "')  and TBLEmployee.COMPANYCODE IN ('" & String.Join("', '", lsC.ToArray()) & "')"
            Else
                whereClause = whereClause & " and TBLEmployee.BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "')  and TBLEmployee.COMPANYCODE IN ('" & String.Join("', '", lsC.ToArray()) & "')"
            End If
        End If

        'On Error GoTo ErrorGen
        Dim DC As New DateConverter() 'nepali
        Dim tmpFrmDate As String 'for nepali
        Dim tmpToDate As String 'for nepali

        Dim strDeptDivCode As String
        Dim strCurrentDeptDivCode As String
        Dim intFile As Integer
        Dim strsql As String
        Dim strDepartmentCode As String
        Dim intPrintLineCounter As Integer
        Dim blnDeptAvailable As Boolean

        Dim strPayCode As String
        Dim strLeaveCode As String
        Dim dtLFromDate As Date
        Dim dtLToDate As Date
        Dim dblLeaveAmount As Double
        Dim strEmpCD As String
        Dim strEmpName As String
        Dim strDesig As String
        Dim strVN As String
        Dim strReason As String
        Dim strVoucher As String
        Dim xlapp As Excel.Application
        Dim xlwb As Excel.Workbook
        Dim xlst As Excel.Sheets
        Dim rowcnt As Integer
        xlapp = CreateObject("Excel.Application")
        xlwb = xlapp.Workbooks.Add
        'If UCase(Dir(App.Path & "\TimeWatch.xls")) = UCase("TimeWatch.xls") Then
        '    Kill(App.Path & "\TimeWatch.xls")
        'End If



        mblnCheckReport = False
        mintPageNo = 1
        mintLine = 1
        If XtraShortOrder.g_SortOrder <> "" Then
            strsortorder = XtraShortOrder.g_SortOrder & " , "
        Else
            strsortorder = " Order By "
        End If
        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If
        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".xls"

        'If Left(strsortorder, 1) = "D" Or Left(strsortorder, 1) = "C" Then blnDeptAvailable = True
        'If g_Report_view Then
        If GridLookUpEdit1.EditValue = "" Then   'for all
            If Common.servername = "Access" Then
                strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tbltimeregister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tblTimeRegister.PayCode,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tblTimeRegister.DateOffice,tblTimeRegister.Voucher_No,tblTimeRegister.LeaveCode,tblTimeRegister.firsthalfLeaveCode,tblTimeRegister.SecondhalfLeaveCode,tblTimeRegister.Leaveamount1,tblTimeRegister.Leaveamount2,tblTimeRegister.LeaveAmount,tblTimeRegister.Reason,tblEmployee.EmpName,tblEmployee.PresentCardNo,tblEmployee.Designation" & _
                 " from tblCatagory,tblDivision,  tblTimeRegister,tblEmployee,tblCompany,tblDepartment" & _
                " Where tblCatagory.Cat = tblEmployee.Cat And tblTimeRegister.PayCode = tblEmployee.PayCode And tblTimeRegister.LeaveAmount>0 And FORMAT(tblTimeRegister.DateOffice,'yyyy-MM-dd') Between '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' AND '" & DateEdit2.DateTime.ToString("yyyy-MM-dd") & "'" & _
                " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
                " " & IIf(Len(Trim(whereClause)) = 0, "", " AND " & whereClause) & strsortorder & " tblTimeRegister.DateOffice, tblTimeRegister.LeaveCode"
                adapA = New OleDbDataAdapter(strsql, Common.con1)
                adapA.Fill(Rs_Report)
            Else
                strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tbltimeregister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tblTimeRegister.PayCode,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tblTimeRegister.DateOffice,tblTimeRegister.Voucher_No,tblTimeRegister.LeaveCode,tblTimeRegister.firsthalfLeaveCode,tblTimeRegister.SecondhalfLeaveCode,tblTimeRegister.Leaveamount1,tblTimeRegister.Leaveamount2,tblTimeRegister.LeaveAmount,tblTimeRegister.Reason,tblEmployee.EmpName,tblEmployee.PresentCardNo,tblEmployee.Designation" & _
                 " from tblCatagory,tblDivision,  tblTimeRegister,tblEmployee,tblCompany,tblDepartment" & _
                " Where tblCatagory.Cat = tblEmployee.Cat And  tblTimeRegister.PayCode = tblEmployee.PayCode And tblTimeRegister.LeaveAmount>0 And tblTimeRegister.DateOffice Between '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' AND '" & DateEdit2.DateTime.ToString("yyyy-MM-dd") & "'" & _
                " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
                "" & IIf(Len(Trim(whereClause)) = 0, "", " AND " & whereClause) & strsortorder & " tblTimeRegister.DateOffice, tblTimeRegister.LeaveCode"
                adap = New SqlDataAdapter(strsql, Common.con)
                adap.Fill(Rs_Report)
            End If
        Else
            If Common.servername = "Access" Then
                strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tbltimeregister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tblTimeRegister.PayCode,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tblTimeRegister.DateOffice,tblTimeRegister.Voucher_No,tblTimeRegister.LeaveCode,tblTimeRegister.firsthalfLeaveCode,tblTimeRegister.SecondhalfLeaveCode,tblTimeRegister.Leaveamount1,tblTimeRegister.Leaveamount2,tblTimeRegister.LeaveAmount,tblTimeRegister.Reason,tblEmployee.EmpName,tblEmployee.PresentCardNo,tblEmployee.Designation" & _
                 " from tblCatagory,tblDivision,  tblTimeRegister,tblEmployee,tblCompany,tblDepartment" & _
                " Where tblTimeRegister.LeaveCode='" & GridLookUpEdit1.EditValue.ToString.Trim & "' and  tblCatagory.Cat = tblEmployee.Cat And tblDivision.DivisionCode = tblEmployee.DivisionCode And tblTimeRegister.PayCode = tblEmployee.PayCode And tblTimeRegister.LeaveAmount>0 And FORMAT(tblTimeRegister.DateOffice,'yyyy-MM-dd') Between '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' AND '" & DateEdit2.DateTime.ToString("yyyy-MM-dd") & "'" & _
                " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
                " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & strsortorder & ", DateOffice,LeaveCode"
                adapA = New OleDbDataAdapter(strsql, Common.con1)
                adapA.Fill(Rs_Report)
            Else
                strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tbltimeregister.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tblTimeRegister.PayCode,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tblTimeRegister.DateOffice,tblTimeRegister.Voucher_No,tblTimeRegister.LeaveCode,tblTimeRegister.firsthalfLeaveCode,tblTimeRegister.SecondhalfLeaveCode,tblTimeRegister.Leaveamount1,tblTimeRegister.Leaveamount2,tblTimeRegister.LeaveAmount,tblTimeRegister.Reason,tblEmployee.EmpName,tblEmployee.PresentCardNo,tblEmployee.Designation" & _
                 " from tblCatagory,tblDivision,  tblTimeRegister,tblEmployee,tblCompany,tblDepartment" & _
                " Where tblTimeRegister.LeaveCode='" & GridLookUpEdit1.EditValue.ToString.Trim & "' and  tblCatagory.Cat = tblEmployee.Cat And tblDivision.DivisionCode = tblEmployee.DivisionCode And tblTimeRegister.PayCode = tblEmployee.PayCode And tblTimeRegister.LeaveAmount>0 And tblTimeRegister.DateOffice Between '" & DateEdit1.DateTime.ToString("yyyy-MM-dd") & "' AND '" & DateEdit2.DateTime.ToString("yyyy-MM-dd") & "'" & _
                " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
                " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & strsortorder & ", DateOffice,LeaveCode"
                adap = New SqlDataAdapter(strsql, Common.con)
                adap.Fill(Rs_Report)
            End If
        End If
        'Else
        '    strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tbltimeregisterD.Shift, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tbltimeregisterD.PayCode,tblEmployee.DepartmentCode,tblDepartment.DepartmentName,tbltimeregisterD.DateOffice,tbltimeregisterD.Voucher_No,tbltimeregisterD.LeaveCode,tbltimeregisterD.LeaveAmount,tbltimeregisterD.Reason,tblEmployee.EmpName,tblEmployee.PresentCardNo,tblEmployee.Designation" & _
        '         " from tblCatagory,tblDivision,  tbltimeregisterD,tblEmployee,tblCompany,tblDepartment" & _
        '         " Where tblCatagory.Cat = tblEmployee.Cat And tblDivision.DivisionCode = tblEmployee.DivisionCode And tbltimeregisterD.PayCode = tblEmployee.PayCode And tbltimeregisterD.LeaveAmount > 0 And tbltimeregisterD.DateOffice Between '" & Format(frmLeaveReport.txtFromDate.Value, "MMM dd yyyy") & "' AND '" & Format(frmLeaveReport.TxtToDate.Value, "MMM dd yyyy") & "'" & _
        '         " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
        '         " And (tblemployee.LeavingDate>='" & Format(frmLeaveReport.txtFromDate, "MMM dd yyyy") & "' or tblemployee.LeavingDate is null)" & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause)
        'End If
        'Rs_Report = Cn.Execute(strsql)
        If Rs_Report.Tables(0).Rows.Count < 1 Then
            XtraMessageBox.Show(ulf, "<size=10>No Data Available For this Report.</size>", "<size=9>iAS</size>")
            Exit Sub
        End If

        rowcnt = 1
        xlapp.Visible = True
        xlapp.Columns.ColumnWidth = 10
        xlapp.Columns.Font.Name = "Tahoma"
        xlapp.Cells(rowcnt, 5).Font.Bold = True
        xlapp.Cells(rowcnt, 5) = "Company Name:" & CommonReport.g_CompanyNames
        rowcnt = rowcnt + 1
        xlapp.Cells(rowcnt, 5).Font.Bold = True
        xlapp.Cells(rowcnt, 5) = "Run Date & Time :" & Common.runningDateTime 'Now.ToString("dd/MM/yyyy HH:mm")
        rowcnt = rowcnt + 2
        xlapp.Cells.Font.Size = 8
        xlapp.Cells(rowcnt, 4).Font.Bold = True
        'xlapp.Cells(rowcnt, 4) = " SANCTIONED LEAVE FROM : " & DateEdit1.DateTime.ToString("dd/MM/yyyy") & "  TO :  " & DateEdit2.DateTime.ToString("dd/MM/yyyy")
        xlapp.Cells(rowcnt, 4) = " SANCTIONED LEAVE FROM : " & FromDateTodate 'DateEdit1.DateTime.ToString("dd/MM/yyyy") & "  TO :  " & DateEdit2.DateTime.ToString("dd/MM/yyyy")
        rowcnt = rowcnt + 1

        rowcnt = rowcnt + 1
        xlapp.Cells(rowcnt, 2).Font.Bold = True
        xlapp.Cells(rowcnt, 2).Font.ColorIndex = 2 ' vbWhite
        xlapp.Cells(rowcnt, 2).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 2).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 2) = "Paycode"
        xlapp.Cells(rowcnt, 3).Font.Bold = True
        xlapp.Cells(rowcnt, 3).Font.ColorIndex = 2 ' vbWhite
        xlapp.Cells(rowcnt, 3).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 3).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 3) = "Card No."
        xlapp.Cells(rowcnt, 4).Font.Bold = True
        xlapp.Cells(rowcnt, 4).Font.ColorIndex = 2 ' vbWhite
        xlapp.Cells(rowcnt, 4).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 4).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 4) = " Employee Name "
        xlapp.Cells(rowcnt, 5).Font.Bold = True
        xlapp.Cells(rowcnt, 5).Font.ColorIndex = 2 'vbWhite
        xlapp.Cells(rowcnt, 5).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 5).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 6).Font.Bold = True
        xlapp.Cells(rowcnt, 6).Font.ColorIndex = 2 ' vbWhite
        xlapp.Cells(rowcnt, 6).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 6).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 6) = "Designation"
        xlapp.Cells(rowcnt, 7).Font.Bold = True
        xlapp.Cells(rowcnt, 7).Font.ColorIndex = 2 ' vbWhite
        xlapp.Cells(rowcnt, 7).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 7).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 7) = "Date From "
        xlapp.Cells(rowcnt, 8).Font.Bold = True
        xlapp.Cells(rowcnt, 8).Font.ColorIndex = 2 ' vbWhite
        xlapp.Cells(rowcnt, 8).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 8).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 8) = "Date To "
        xlapp.Cells(rowcnt, 9).Font.Bold = True
        xlapp.Cells(rowcnt, 9).Font.ColorIndex = 2 ' vbWhite
        xlapp.Cells(rowcnt, 9).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 9).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 9) = "Voucher No."
        xlapp.Cells(rowcnt, 10).Font.Bold = True
        xlapp.Cells(rowcnt, 10).Font.ColorIndex = 2 ' vbWhite
        xlapp.Cells(rowcnt, 10).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 10).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 10) = " Leave Type"
        xlapp.Cells(rowcnt, 11).Font.Bold = True
        xlapp.Cells(rowcnt, 11).Font.ColorIndex = 2 'vbWhite
        xlapp.Cells(rowcnt, 11).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 11).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 11) = " Leave Amount "
        xlapp.Cells(rowcnt, 12).Font.Bold = True
        xlapp.Cells(rowcnt, 12).Font.ColorIndex = 2 'vbWhite
        xlapp.Cells(rowcnt, 12).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 12).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 12) = " Posted"
        xlapp.Cells(rowcnt, 13).Font.Bold = True
        xlapp.Cells(rowcnt, 13).Font.ColorIndex = 2 'vbWhite
        xlapp.Cells(rowcnt, 13).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 13).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 13).ColumnWidth = 14
        xlapp.Cells(rowcnt, 13) = " Remarks"
        'rowcnt = rowcnt + 1


        mintLine = 9
        'With Rs_Report
        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            If blnDeptAvailable And Not i > Rs_Report.Tables(0).Rows.Count - 1 Then
                rowcnt = rowcnt + 1
                If strsortorder.Contains("Department") Then
                    xlapp.Cells(rowcnt, 1) = "** Department Code & Name : " & Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim & "  " & Rs_Report.Tables(0).Rows(i).Item("DepartmentName").ToString.Trim
                ElseIf strsortorder.Contains("Division") Then
                    xlapp.Cells(rowcnt, 1) = "** Section Code & Name : " & Rs_Report.Tables(0).Rows(i).Item("DivisionCode").ToString.Trim & "  " & Rs_Report.Tables(0).Rows(i).Item("DivisionName").ToString.Trim
                Else
                    xlapp.Cells(rowcnt, 1) = "** Category Code & Name : " & Rs_Report.Tables(0).Rows(i).Item("Cat").ToString.Trim & "  " & Rs_Report.Tables(0).Rows(i).Item("CatagoryName").ToString.Trim
                End If
                rowcnt = rowcnt + 1
                mintLine = mintLine + 3
            End If
            'Do While Not .EOF
            mblnCheckReport = True
            If strsortorder.Contains("Department") Then
                strDeptDivCode = Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim
            ElseIf strsortorder.Contains("Division") Then
                strDeptDivCode = Rs_Report.Tables(0).Rows(i).Item("DivisionCode").ToString.Trim
            Else
                strDeptDivCode = Rs_Report.Tables(0).Rows(i).Item("Cat").ToString.Trim
            End If

            strPayCode = Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim
            strEmpCD = Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim
            strEmpName = Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim
            If Rs_Report.Tables(0).Rows(i).Item("Designation").ToString.Trim = "" Then
                strDesig = Space(20)
            Else
                strDesig = Rs_Report.Tables(0).Rows(i).Item("Designation").ToString.Trim
            End If
            strVN = IIf(Rs_Report.Tables(0).Rows(i).Item("Voucher_No").ToString.Trim = "", Space(10), Rs_Report.Tables(0).Rows(i).Item("Voucher_No").ToString.Trim)
            strReason = IIf(Rs_Report.Tables(0).Rows(i).Item("reason").ToString.Trim = "", "", Rs_Report.Tables(0).Rows(i).Item("reason").ToString.Trim)
            strLeaveCode = Rs_Report.Tables(0).Rows(i).Item("Leavecode").ToString.Trim
            dtLFromDate = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("DATEOFFICE").ToString.Trim)
            dblLeaveAmount = 0
            strVoucher = strVN
            Do While strLeaveCode = Rs_Report.Tables(0).Rows(i).Item("Leavecode").ToString.Trim
                If strPayCode = Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim And strVoucher = strVN Then
                    If Rs_Report.Tables(0).Rows(i).Item("leaveamount").ToString.Trim = "" Then
                        dblLeaveAmount = dblLeaveAmount + 0
                    Else
                        dblLeaveAmount = dblLeaveAmount + Rs_Report.Tables(0).Rows(i).Item("leaveamount").ToString.Trim
                    End If
                    dtLToDate = Convert.ToDateTime(Rs_Report.Tables(0).Rows(i).Item("DATEOFFICE").ToString.Trim)
                    i = i + 1
                    '.MoveNext()
                    'If .EOF Then
                    '    Exit Do
                    'End If
                    If i > Rs_Report.Tables(0).Rows.Count - 1 Then
                        i = i - 1
                        GoTo tmp
                        Exit Do
                    End If
                    strVN = IIf(Rs_Report.Tables(0).Rows(i).Item("Voucher_No").ToString.Trim = "", Space(10), Rs_Report.Tables(0).Rows(i).Item("Voucher_No").ToString.Trim)
                Else
                    Exit Do
                End If
            Loop
            i = i - 1
            If i > Rs_Report.Tables(0).Rows.Count - 1 Then
                Exit For
            End If
tmp:        rowcnt = rowcnt + 1

            If Common.IsNepali = "Y" Then
                Dim Vstart As String = DC.ToBS(New Date(dtLFromDate.Year, dtLFromDate.Month, dtLFromDate.Day))
                Dim dojTmp() As String = Vstart.Split("-")
                tmpFrmDate = dojTmp(2) & "/" & Common.NepaliMonth(dojTmp(1) - 1) & "/" & dojTmp(0)
                Vstart = DC.ToBS(New Date(dtLToDate.Year, dtLToDate.Month, dtLToDate.Day))
                dojTmp = Vstart.Split("-")
                tmpToDate = dojTmp(2) & "/" & Common.NepaliMonth(dojTmp(1) - 1) & "/" & dojTmp(0)


            Else
                tmpFrmDate = dtLFromDate.ToString("dd/MM/yyyy")
                tmpToDate = dtLToDate.ToString("dd/MM/yyyy")
            End If

            xlapp.Cells(rowcnt, 2).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 2) = "'" & strPayCode
            xlapp.Cells(rowcnt, 3).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 3) = "'" & strEmpCD
            xlapp.Cells(rowcnt, 4).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 4) = strEmpName
            xlapp.Cells(rowcnt, 5).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 6) = strDesig
            xlapp.Cells(rowcnt, 6).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 7).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 7) = tmpFrmDate 'dtLFromDate
            xlapp.Cells(rowcnt, 8).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 8) = tmpToDate 'dtLToDate
            xlapp.Cells(rowcnt, 9).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 9) = "'" & strVN
            xlapp.Cells(rowcnt, 10).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 10) = strLeaveCode
            xlapp.Cells(rowcnt, 11).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 11) = Length7(CDbl(dblLeaveAmount))
            xlapp.Cells(rowcnt, 12).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 12) = " Y"
            xlapp.Cells(rowcnt, 13).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 13).ColumnWidth = 14
            xlapp.Cells(rowcnt, 13) = strReason

            mintLine = mintLine + 1

            'If .EOF Then
            '    Exit Do
            'End If
            If blnDeptAvailable And Not i > Rs_Report.Tables(0).Rows.Count - 1 Then
                If strsortorder.Contains("Department") Then
                    strCurrentDeptDivCode = Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim
                ElseIf strsortorder.Contains("Division") Then
                    strCurrentDeptDivCode = Rs_Report.Tables(0).Rows(i).Item("DivisionCode").ToString.Trim
                Else
                    strCurrentDeptDivCode = Rs_Report.Tables(0).Rows(i).Item("Cat").ToString.Trim
                End If

                If strDeptDivCode <> strCurrentDeptDivCode Then
                    rowcnt = rowcnt + 1
                    mintLine = 9
                    If strsortorder.Contains("Department") Then
                        xlapp.Cells(rowcnt, 1) = "** Department Code & Name : " & Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim & "  " & Rs_Report.Tables(0).Rows(i).Item("DepartmentName").ToString.Trim
                    ElseIf strsortorder.Contains("Division") Then
                        xlapp.Cells(rowcnt, 1) = "** Section Code & Name : " & Rs_Report.Tables(0).Rows(i).Item("DivisionCode").ToString.Trim & "  " & Rs_Report.Tables(0).Rows(i).Item("DivisionName").ToString.Trim
                    Else
                        xlapp.Cells(rowcnt, 1) = "** Category Code & Name : " & Rs_Report.Tables(0).Rows(i).Item("Cat").ToString.Trim & "  " & Rs_Report.Tables(0).Rows(i).Item("CatagoryName").ToString.Trim
                    End If
                    rowcnt = rowcnt + 1
                    mintLine = mintLine + 3
                End If
            End If
        Next
        'Loop
        '        End With
        '        xlwb.SaveAs(App.Path & "\TimeWatch.xls")
        '        Exit Sub
        'ErrorGen:
        '        MsgBox(Err.Description, vbInformation + vbOKOnly, "Report Generation Information")
        '        Screen.MousePointer = vbDefault
        xlwb.SaveAs(mstrFile_Name)
    End Sub
    Sub Monthly_LeaveCard(ByVal strsortorder As String)

        'Dim tmpNepali As String = TextEdit2.Text
        'If Common.IsNepali = "Y" Then
        '    Dim DC As New DateConverter()
        '    Dim tmpNow As DateTime = DC.ToBS(New Date(Now.Year, Now.Month, Now.Day))
        '    Common.runningDateTime = tmpNow.Day & "/" & Common.NepaliMonth(tmpNow.Month - 1) & "/" & tmpNow.Year & " " & Now.ToString("HH:mm")
        '    Try
        '        Dim tmp As DateTime = DC.ToAD(New Date(TextEdit2.Text, 1, 1))
        '        TextEdit2.Text = tmp.Year
        '    Catch ex As Exception
        '        XtraMessageBox.Show(ulf, "<size=10>Invalid From Date</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        '        ComboNepaliDateFrm.Select()
        '        Exit Sub
        '    End Try
        'End If

        'On Error GoTo ErrorGen
        Dim strDeptDivCode As String
        Dim strCurrentDeptDivCode As String
        Dim intPrintLineCounter As Integer
        Dim intFile As Integer
        Dim strsql As String
        Dim strSqlLeaveMaster As String
        Dim rs_report_ReportLeaveMaster As DataSet 'ADODB.Recordset
        Dim strLeaveField3() As String ' * 3
        Dim strLeaveDescription50() As String '* 50
        Dim intRecCountLeaveMaster As Integer
        Dim intCounter As Integer
        Dim intCounter1 As Integer
        Dim intCounter1Temp As Integer
        Dim strLeaveWholeLine() As String
        Dim dblConsumedLeave() As Double
        Dim dblAccruedLeave() As Double
        Dim dblConsumedLeaveTotal As Double
        Dim dblAccruedLeaveTotal As Double
        Dim dblLeaveBalanceTotal As Double
        Dim strLvDesc50 As String '* 50
        Dim mFirst As Boolean, blnDeptAvailable As Boolean
        Dim dblL01 As Double, dblL02 As Double, dblL03 As Double, dblL04 As Double, dblL05 As Double
        Dim dblL06 As Double, dblL07 As Double, dblL08 As Double, dblL09 As Double, dblL10 As Double
        Dim dblL11 As Double, dblL12 As Double, dblL13 As Double, dblL14 As Double, dblL15 As Double
        Dim dblL16 As Double, dblL17 As Double, dblL18 As Double, dblL19 As Double, dblL20 As Double

        Dim dblL01_ADD As Double, dblL02_ADD As Double, dblL03_ADD As Double, dblL04_ADD As Double, dblL05_ADD As Double
        Dim dblL06_ADD As Double, dblL07_ADD As Double, dblL08_ADD As Double, dblL09_ADD As Double, dblL10_ADD As Double
        Dim dblL11_ADD As Double, dblL12_ADD As Double, dblL13_ADD As Double, dblL14_ADD As Double, dblL15_ADD As Double
        Dim dblL16_ADD As Double, dblL17_ADD As Double, dblL18_ADD As Double, dblL19_ADD As Double, dblL20_ADD As Double

        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If
        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet

        blnDeptAvailable = False
        mblnCheckReport = False
        mFirst = True
        mintPageNo = 1
        mintLine = 1

        'If strsortorder = "" Then
        If Common.servername = "Access" Then
            strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tblLeaveLedger.PayCode,tblLeaveLedger.L01,tblLeaveLedger.L02,tblLeaveLedger.L03,tblLeaveLedger.L04,tblLeaveLedger.L05,tblLeaveLedger.L06,tblLeaveLedger.L07,tblLeaveLedger.L08,tblLeaveLedger.L09,tblLeaveLedger.L10,tblLeaveLedger.L11,tblLeaveLedger.L12,tblLeaveLedger.L13,tblLeaveLedger.L14,tblLeaveLedger.L15,tblLeaveLedger.L16,tblLeaveLedger.L17,tblLeaveLedger.L18,tblLeaveLedger.L19,tblLeaveLedger.L20," & _
             "tblLeaveLedger.L01_ADD,tblLeaveLedger.L02_ADD,tblLeaveLedger.L03_ADD,tblLeaveLedger.L04_ADD,tblLeaveLedger.L05_ADD,tblLeaveLedger.L06_ADD,tblLeaveLedger.L07_ADD,tblLeaveLedger.L08_ADD,tblLeaveLedger.L09_ADD,tblLeaveLedger.L10_ADD,tblLeaveLedger.L11_ADD,tblLeaveLedger.L12_ADD,tblLeaveLedger.L13_ADD,tblLeaveLedger.L14_ADD,tblLeaveLedger.L15_ADD,tblLeaveLedger.L16_ADD,tblLeaveLedger.L17_ADD,tblLeaveLedger.L18_ADD,tblLeaveLedger.L19_ADD,tblLeaveLedger.L20_ADD," & _
             "tblEmployee.EmpName,tblEmployee.PresentCardNo,tblDepartment.DepartmentName,tblEmployee.DepartmentCode" & _
             " from tblCatagory,tblDivision,  tblLeaveLedger,tblEmployee,tblCompany,tblDepartment" & _
             " Where tblCatagory.Cat = tblEmployee.Cat And tblLeaveLedger.PayCode = tblEmployee.PayCode And (tblLeaveLedger.L01_ADD <> 0 Or tblLeaveLedger.L02_ADD <> 0 Or tblLeaveLedger.L03_ADD <> 0 Or tblLeaveLedger.L04_ADD <> 0 Or tblLeaveLedger.L05_ADD <> 0 Or tblLeaveLedger.L06_ADD <> 0 Or tblLeaveLedger.L07_ADD <> 0 Or tblLeaveLedger.L08_ADD <> 0 Or tblLeaveLedger.L09_ADD <> 0 Or tblLeaveLedger.L10_ADD <> 0 Or tblLeaveLedger.L11_ADD <> 0 Or tblLeaveLedger.L12_ADD <> 0 Or tblLeaveLedger.L13_ADD <> 0 Or tblLeaveLedger.L14_ADD <> 0 Or tblLeaveLedger.L15_ADD <> 0 Or tblLeaveLedger.L16_ADD <> 0 Or tblLeaveLedger.L17_ADD <> 0 Or tblLeaveLedger.L18_ADD <> 0 Or tblLeaveLedger.L19_ADD <> 0 Or tblLeaveLedger.L20_ADD <> 0)" & _
             " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
             " and TBLLEAVELEDGER.LYEAR = " & TextEdit2.Text & " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause)
            adapA = New OleDbDataAdapter(strsql, Common.con1)
            adapA.Fill(Rs_Report)
        Else
            strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tblLeaveLedger.PayCode,tblLeaveLedger.L01,tblLeaveLedger.L02,tblLeaveLedger.L03,tblLeaveLedger.L04,tblLeaveLedger.L05,tblLeaveLedger.L06,tblLeaveLedger.L07,tblLeaveLedger.L08,tblLeaveLedger.L09,tblLeaveLedger.L10,tblLeaveLedger.L11,tblLeaveLedger.L12,tblLeaveLedger.L13,tblLeaveLedger.L14,tblLeaveLedger.L15,tblLeaveLedger.L16,tblLeaveLedger.L17,tblLeaveLedger.L18,tblLeaveLedger.L19,tblLeaveLedger.L20," & _
             "tblLeaveLedger.L01_ADD,tblLeaveLedger.L02_ADD,tblLeaveLedger.L03_ADD,tblLeaveLedger.L04_ADD,tblLeaveLedger.L05_ADD,tblLeaveLedger.L06_ADD,tblLeaveLedger.L07_ADD,tblLeaveLedger.L08_ADD,tblLeaveLedger.L09_ADD,tblLeaveLedger.L10_ADD,tblLeaveLedger.L11_ADD,tblLeaveLedger.L12_ADD,tblLeaveLedger.L13_ADD,tblLeaveLedger.L14_ADD,tblLeaveLedger.L15_ADD,tblLeaveLedger.L16_ADD,tblLeaveLedger.L17_ADD,tblLeaveLedger.L18_ADD,tblLeaveLedger.L19_ADD,tblLeaveLedger.L20_ADD," & _
             "tblEmployee.EmpName,tblEmployee.PresentCardNo,tblDepartment.DepartmentName,tblEmployee.DepartmentCode" & _
             " from tblCatagory,tblDivision,  tblLeaveLedger,tblEmployee,tblCompany,tblDepartment" & _
             " Where tblCatagory.Cat = tblEmployee.Cat And tblLeaveLedger.PayCode = tblEmployee.PayCode And (tblLeaveLedger.L01_ADD <> 0 Or tblLeaveLedger.L02_ADD <> 0 Or tblLeaveLedger.L03_ADD <> 0 Or tblLeaveLedger.L04_ADD <> 0 Or tblLeaveLedger.L05_ADD <> 0 Or tblLeaveLedger.L06_ADD <> 0 Or tblLeaveLedger.L07_ADD <> 0 Or tblLeaveLedger.L08_ADD <> 0 Or tblLeaveLedger.L09_ADD <> 0 Or tblLeaveLedger.L10_ADD <> 0 Or tblLeaveLedger.L11_ADD <> 0 Or tblLeaveLedger.L12_ADD <> 0 Or tblLeaveLedger.L13_ADD <> 0 Or tblLeaveLedger.L14_ADD <> 0 Or tblLeaveLedger.L15_ADD <> 0 Or tblLeaveLedger.L16_ADD <> 0 Or tblLeaveLedger.L17_ADD <> 0 Or tblLeaveLedger.L18_ADD <> 0 Or tblLeaveLedger.L19_ADD <> 0 Or tblLeaveLedger.L20_ADD <> 0)" & _
             " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
             " and TBLLEAVELEDGER.LYEAR = " & TextEdit2.Text & " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause)
            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(Rs_Report)
        End If
        'Rs_Report = Cn.Execute(strsql)
        If Rs_Report.Tables(0).Rows.Count < 1 Then
            XtraMessageBox.Show(ulf, "<size=10>No Data Available For this Report.</size>", "<size=9>iAS</size>")
            Exit Sub
        End If
        strSqlLeaveMaster = " Select LeaveField,LeaveDescription from tblLeaveMaster order by LeaveField"
        rs_report_ReportLeaveMaster = New DataSet
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(strSqlLeaveMaster, Common.con1)
            adapA.Fill(rs_report_ReportLeaveMaster)
        Else
            adap = New SqlDataAdapter(strSqlLeaveMaster, Common.con)
            adap.Fill(rs_report_ReportLeaveMaster)
        End If
        'rs_report_ReportLeaveMaster = Cn.Execute(strSqlLeaveMaster)
        'rs_report_ReportLeaveMaster.Sort = "LeaveField"

        intRecCountLeaveMaster = rs_report_ReportLeaveMaster.Tables(0).Rows.Count

        ReDim strLeaveField3(intRecCountLeaveMaster)
        ReDim strLeaveDescription50(intRecCountLeaveMaster)
        ReDim strLeaveWholeLine(intRecCountLeaveMaster)
        ReDim dblConsumedLeave(intRecCountLeaveMaster)
        ReDim dblAccruedLeave(intRecCountLeaveMaster)
        'rs_report_ReportLeaveMaster.MoveFirst()

        'With rs_report_ReportLeaveMaster
        For i As Integer = 0 To rs_report_ReportLeaveMaster.Tables(0).Rows.Count - 1 ' While Not .EOF
            intCounter = intCounter + 1
            strLeaveField3(intCounter) = rs_report_ReportLeaveMaster.Tables(0).Rows(i).Item("leavefield").ToString.Trim
            strLvDesc50 = ""
            If rs_report_ReportLeaveMaster.Tables(0).Rows(i).Item("LeaveDescription").ToString.Trim <> "" Then strLvDesc50 = rs_report_ReportLeaveMaster.Tables(0).Rows(i).Item("LeaveDescription").ToString.Trim
            strLeaveDescription50(intCounter) = strLvDesc50
            '.MoveNext()
        Next
        'End While
        'End With

        mFileNumber = FreeFile()
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".SRP"
        Dim objWriter As New System.IO.StreamWriter(mstrFile_Name, True)
        'With Rs_Report
        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1 '  Do While Not .EOF
            If mFirst Or mintLine > g_LinesPerPage Or (strsortorder.Trim = "Department" And strDeptDivCode <> Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim And g_SkipAfterDept) Then
                'If mFirst Or mintLine > g_LinesPerPage Or (Left(strsortorder, 10) = "Department" And strDeptDivCode <> !DepartmentCode And g_SkipAfterDept) Then
1:              If Not mFirst Then
                    objWriter.WriteLine(Space(2) & vbFormFeed)
                Else
                    mFirst = False
                End If
                objWriter.WriteLine(Space(2) & Space(40) & CommonReport.g_CompanyNames)
                objWriter.WriteLine(Space(2) & "Run Date & Time :" & Common.runningDateTime) 'Now.ToString("dd/MM/yyyy HH:mm"))
                objWriter.WriteLine(Space(2) & "Page No." & mintPageNo)
                If Common.IsNepali = "Y" Then
                    objWriter.WriteLine(Space(2) & Space(40) & "Leave Card for the Year :- " & tmpNepali)
                Else
                    objWriter.WriteLine(Space(2) & Space(40) & "Leave Card for the Year :- " & TextEdit2.Text)
                End If

                mintLine = 4
                mintPageNo = mintPageNo + 1
            End If

            'If blnDeptAvailable And Not .EOF Then
            '    If Left(strsortorder, 10) = "Department" And strDeptDivCode <> !DepartmentCode Then
            '        If mintLine + 3 > g_LinesPerPage Then GoTo 1
            '        Print #intFile, Space(2) + ""
            '        strDeptDivCode = !DepartmentCode
            '        Print #intFile, Space(2) + "** Department Code & Name : " & !DepartmentCode & "  " & !DepartmentName
            '        Print #intFile, Space(2) + ""
            '        mintLine = mintLine + 3
            '    ElseIf Left(strsortorder, 8) = "Division" And strDeptDivCode <> !DivisionCode Then
            '        If mintLine + 3 > g_LinesPerPage Then GoTo 1
            '        Print #intFile, Space(2) + ""
            '        strDeptDivCode = !DivisionCode
            '        Print #intFile, Space(2) + "** Section Code & Name : " & !DivisionCode & "  " & !DivisionName
            '        Print #intFile, Space(2) + ""
            '        mintLine = mintLine + 3
            '    ElseIf Left(strsortorder, 3) = "Cat" And strDeptDivCode <> !Cat Then
            '        If mintLine + 3 > g_LinesPerPage Then GoTo 1
            '        Print #intFile, Space(2) + ""
            '        strDeptDivCode = !Cat
            '        Print #intFile, Space(2) + "** Category Code & Name : " & !Cat & "  " & !CatagoryName
            '        Print #intFile, Space(2) + ""
            '        mintLine = mintLine + 3
            '    End If
            'End If
            objWriter.WriteLine(Space(2) & "------------------------------------------------------------------------------------------------------------")
            objWriter.WriteLine(Space(2) & "")
            objWriter.WriteLine(Space(2) & "")
            objWriter.WriteLine(Space(2) & "Employee's Card No             : " & Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim)
            objWriter.WriteLine(Space(2) & "Employee's PayRoll Code & Name : " & Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim & Space(2) & Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim)
            objWriter.WriteLine(Space(2) & "Department Code & Name         : " & Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim & Space(2) & Rs_Report.Tables(0).Rows(i).Item("DepartmentName").ToString.Trim)
            objWriter.WriteLine(Space(2) & "")
            objWriter.WriteLine(Space(2) & "Leave Type                                             Leave Accrued       Leave Consumed      Leave Balance")
            objWriter.WriteLine(Space(2) & "")
            mintLine = mintLine + 9

            intCounter = 1
            While intCounter <= intRecCountLeaveMaster
                Select Case strLeaveField3(intCounter)
                    Case "L01"
                        dblL01 = 0
                        dblL01_ADD = 0
                        If Rs_Report.Tables(0).Rows(i).Item("L01").ToString.Trim <> "" Then dblL01 = Rs_Report.Tables(0).Rows(i).Item("L01").ToString.Trim
                        If Rs_Report.Tables(0).Rows(i).Item("L01_ADD").ToString.Trim <> "" Then dblL01_ADD = Rs_Report.Tables(0).Rows(i).Item("L01_ADD").ToString.Trim
                        dblConsumedLeave(intCounter) = dblL01
                        dblAccruedLeave(intCounter) = dblL01_ADD
                    Case "L02"
                        dblL02 = 0
                        dblL02_ADD = 0
                        If Rs_Report.Tables(0).Rows(i).Item("L02").ToString.Trim <> "" Then dblL02 = Rs_Report.Tables(0).Rows(i).Item("L02").ToString.Trim
                        If Rs_Report.Tables(0).Rows(i).Item("L02_ADD").ToString.Trim <> "" Then dblL02_ADD = Rs_Report.Tables(0).Rows(i).Item("L02_ADD").ToString.Trim
                        dblConsumedLeave(intCounter) = dblL02
                        dblAccruedLeave(intCounter) = dblL02_ADD
                    Case "L03"
                        dblL03 = 0
                        dblL03_ADD = 0
                        If Rs_Report.Tables(0).Rows(i).Item("L03").ToString.Trim <> "" Then dblL03 = Rs_Report.Tables(0).Rows(i).Item("L03").ToString.Trim
                        If Rs_Report.Tables(0).Rows(i).Item("L03_ADD").ToString.Trim <> "" Then dblL03_ADD = Rs_Report.Tables(0).Rows(i).Item("L03_ADD").ToString.Trim
                        dblConsumedLeave(intCounter) = dblL03
                        dblAccruedLeave(intCounter) = dblL03_ADD
                    Case "L04"
                        dblL04 = 0
                        dblL04_ADD = 0
                        If Rs_Report.Tables(0).Rows(i).Item("L04").ToString.Trim <> "" Then dblL04 = Rs_Report.Tables(0).Rows(i).Item("L04").ToString.Trim
                        If Rs_Report.Tables(0).Rows(i).Item("L04_ADD").ToString.Trim <> "" Then dblL04_ADD = Rs_Report.Tables(0).Rows(i).Item("L04_ADD").ToString.Trim
                        dblConsumedLeave(intCounter) = dblL04
                        dblAccruedLeave(intCounter) = dblL04_ADD
                    Case "L05"
                        dblL05 = 0
                        dblL05_ADD = 0
                        If Rs_Report.Tables(0).Rows(i).Item("L05").ToString.Trim <> "" Then dblL05 = Rs_Report.Tables(0).Rows(i).Item("L05").ToString.Trim
                        If Rs_Report.Tables(0).Rows(i).Item("L05_ADD").ToString.Trim <> "" Then dblL05_ADD = Rs_Report.Tables(0).Rows(i).Item("L05_ADD").ToString.Trim
                        dblConsumedLeave(intCounter) = dblL05
                        dblAccruedLeave(intCounter) = dblL05_ADD
                    Case "L06"
                        dblL06 = 0
                        dblL06_ADD = 0
                        If Rs_Report.Tables(0).Rows(i).Item("L06").ToString.Trim <> "" Then dblL06 = Rs_Report.Tables(0).Rows(i).Item("L06").ToString.Trim
                        If Rs_Report.Tables(0).Rows(i).Item("L06_ADD").ToString.Trim <> "" Then dblL06_ADD = Rs_Report.Tables(0).Rows(i).Item("L06_ADD").ToString.Trim
                        dblConsumedLeave(intCounter) = dblL06
                        dblAccruedLeave(intCounter) = dblL06_ADD
                    Case "L07"
                        dblL07 = 0
                        dblL07_ADD = 0
                        If Rs_Report.Tables(0).Rows(i).Item("L07").ToString.Trim <> "" Then dblL07 = Rs_Report.Tables(0).Rows(i).Item("L07").ToString.Trim
                        If Not Rs_Report.Tables(0).Rows(i).Item("L07_ADD").ToString.Trim <> "" Then dblL07_ADD = Rs_Report.Tables(0).Rows(i).Item("L07_ADD").ToString.Trim
                        dblConsumedLeave(intCounter) = dblL07
                        dblAccruedLeave(intCounter) = dblL07_ADD
                    Case "L08"
                        dblL08 = 0
                        dblL08_ADD = 0
                        If Rs_Report.Tables(0).Rows(i).Item("L08").ToString.Trim <> "" Then dblL08 = Rs_Report.Tables(0).Rows(i).Item("L08").ToString.Trim
                        If Rs_Report.Tables(0).Rows(i).Item("L08_ADD").ToString.Trim <> "" Then dblL08_ADD = Rs_Report.Tables(0).Rows(i).Item("L08_ADD").ToString.Trim
                        dblConsumedLeave(intCounter) = dblL08
                        dblAccruedLeave(intCounter) = dblL08_ADD
                    Case "L09"
                        dblL09 = 0
                        dblL09_ADD = 0
                        If Rs_Report.Tables(0).Rows(i).Item("L09").ToString.Trim <> "" Then dblL09 = Rs_Report.Tables(0).Rows(i).Item("L09").ToString.Trim
                        If Rs_Report.Tables(0).Rows(i).Item("L09_ADD").ToString.Trim <> "" Then dblL09_ADD = Rs_Report.Tables(0).Rows(i).Item("L09_ADD").ToString.Trim
                        dblConsumedLeave(intCounter) = dblL09
                        dblAccruedLeave(intCounter) = dblL09_ADD
                    Case "L10"
                        dblL10 = 0
                        dblL10_ADD = 0
                        If Not Rs_Report.Tables(0).Rows(i).Item("L10").ToString.Trim <> "" Then dblL10 = Rs_Report.Tables(0).Rows(i).Item("L10").ToString.Trim
                        If Not Rs_Report.Tables(0).Rows(i).Item("L10_ADD").ToString.Trim <> "" Then dblL10_ADD = Rs_Report.Tables(0).Rows(i).Item("L10_ADD").ToString.Trim
                        dblConsumedLeave(intCounter) = dblL10
                        dblAccruedLeave(intCounter) = dblL10_ADD
                    Case "L11"
                        dblL11 = 0
                        dblL11_ADD = 0
                        If Rs_Report.Tables(0).Rows(i).Item("L11").ToString.Trim <> "" Then dblL11 = Rs_Report.Tables(0).Rows(i).Item("L11").ToString.Trim
                        If Rs_Report.Tables(0).Rows(i).Item("L11_ADD").ToString.Trim <> "" Then dblL11_ADD = Rs_Report.Tables(0).Rows(i).Item("L11_ADD").ToString.Trim
                        dblConsumedLeave(intCounter) = dblL11
                        dblAccruedLeave(intCounter) = dblL11_ADD
                    Case "L12"
                        dblL12 = 0
                        dblL12_ADD = 0
                        If Rs_Report.Tables(0).Rows(i).Item("L12").ToString.Trim <> "" Then dblL12 = Rs_Report.Tables(0).Rows(i).Item("L12").ToString.Trim
                        If Rs_Report.Tables(0).Rows(i).Item("L12_ADD").ToString.Trim <> "" Then dblL12_ADD = Rs_Report.Tables(0).Rows(i).Item("L12_ADD").ToString.Trim
                        dblConsumedLeave(intCounter) = dblL12
                        dblAccruedLeave(intCounter) = dblL12_ADD
                    Case "L13"
                        dblL13 = 0
                        dblL13_ADD = 0
                        If Rs_Report.Tables(0).Rows(i).Item("L13").ToString.Trim <> "" Then dblL13 = Rs_Report.Tables(0).Rows(i).Item("L13").ToString.Trim
                        If Rs_Report.Tables(0).Rows(i).Item("L13_ADD").ToString.Trim <> "" Then dblL13_ADD = Rs_Report.Tables(0).Rows(i).Item("L13_ADD").ToString.Trim
                        dblConsumedLeave(intCounter) = dblL13
                        dblAccruedLeave(intCounter) = dblL13_ADD
                    Case "L14"
                        dblL14 = 0
                        dblL14_ADD = 0
                        If Rs_Report.Tables(0).Rows(i).Item("L14").ToString.Trim <> "" Then dblL14 = Rs_Report.Tables(0).Rows(i).Item("L14").ToString.Trim
                        If Rs_Report.Tables(0).Rows(i).Item("L14_ADD").ToString.Trim <> "" Then dblL14_ADD = Rs_Report.Tables(0).Rows(i).Item("L14_ADD").ToString.Trim
                        dblConsumedLeave(intCounter) = dblL14
                        dblAccruedLeave(intCounter) = dblL14_ADD
                    Case "L15"
                        dblL15 = 0
                        dblL15_ADD = 0
                        If Rs_Report.Tables(0).Rows(i).Item("L15").ToString.Trim <> "" Then dblL15 = Rs_Report.Tables(0).Rows(i).Item("L15").ToString.Trim
                        If Rs_Report.Tables(0).Rows(i).Item("L15_ADD").ToString.Trim <> "" Then dblL15_ADD = Rs_Report.Tables(0).Rows(i).Item("L15_ADD").ToString.Trim
                        dblConsumedLeave(intCounter) = dblL15
                        dblAccruedLeave(intCounter) = dblL15_ADD
                    Case "L16"
                        dblL16 = 0
                        dblL16_ADD = 0
                        If Rs_Report.Tables(0).Rows(i).Item("L16").ToString.Trim <> "" Then dblL16 = Rs_Report.Tables(0).Rows(i).Item("L16").ToString.Trim
                        If Rs_Report.Tables(0).Rows(i).Item("L16_ADD").ToString.Trim <> "" Then dblL16_ADD = Rs_Report.Tables(0).Rows(i).Item("L16_ADD").ToString.Trim
                        dblConsumedLeave(intCounter) = dblL16
                        dblAccruedLeave(intCounter) = dblL16_ADD
                    Case "L17"
                        dblL17 = 0
                        dblL17_ADD = 0
                        If Rs_Report.Tables(0).Rows(i).Item("L17").ToString.Trim <> "" Then dblL17 = Rs_Report.Tables(0).Rows(i).Item("L17").ToString.Trim
                        If Rs_Report.Tables(0).Rows(i).Item("L17_ADD").ToString.Trim <> "" Then dblL17_ADD = Rs_Report.Tables(0).Rows(i).Item("L17_ADD").ToString.Trim
                        dblConsumedLeave(intCounter) = dblL17
                        dblAccruedLeave(intCounter) = dblL17_ADD
                    Case "L18"
                        dblL18 = 0
                        dblL18_ADD = 0
                        If Rs_Report.Tables(0).Rows(i).Item("L18").ToString.Trim <> "" Then dblL18 = Rs_Report.Tables(0).Rows(i).Item("L18").ToString.Trim
                        If Rs_Report.Tables(0).Rows(i).Item("L18_ADD").ToString.Trim <> "" Then dblL18_ADD = Rs_Report.Tables(0).Rows(i).Item("L18_ADD").ToString.Trim
                        dblConsumedLeave(intCounter) = dblL18
                        dblAccruedLeave(intCounter) = dblL18_ADD
                    Case "L19"
                        dblL19 = 0
                        dblL19_ADD = 0
                        If Rs_Report.Tables(0).Rows(i).Item("L19").ToString.Trim <> "" Then dblL19 = Rs_Report.Tables(0).Rows(i).Item("L19").ToString.Trim
                        If Rs_Report.Tables(0).Rows(i).Item("L19_ADD").ToString.Trim <> "" Then dblL19_ADD = Rs_Report.Tables(0).Rows(i).Item("L19_ADD").ToString.Trim
                        dblConsumedLeave(intCounter) = dblL19
                        dblAccruedLeave(intCounter) = dblL19_ADD
                    Case "L20"
                        dblL20 = 0
                        dblL20_ADD = 0
                        If Rs_Report.Tables(0).Rows(i).Item("L20").ToString.Trim <> "" Then dblL20 = Rs_Report.Tables(0).Rows(i).Item("L20").ToString.Trim
                        If Rs_Report.Tables(0).Rows(i).Item("L20_ADD").ToString.Trim <> "" Then dblL20_ADD = Rs_Report.Tables(0).Rows(i).Item("L20_ADD").ToString.Trim
                        dblConsumedLeave(intCounter) = dblL20
                        dblAccruedLeave(intCounter) = dblL20_ADD
                End Select
                intCounter = intCounter + 1
            End While
            intCounter1 = 1
            intCounter1Temp = 0
            While intCounter1 <= intRecCountLeaveMaster
                If dblAccruedLeave(intCounter1) <> 0 Then
                    intCounter1Temp = intCounter1Temp + 1
                End If
                intCounter1 = intCounter1 + 1
            End While
            mblnCheckReport = True
            intCounter1 = 1
            dblConsumedLeaveTotal = 0
            dblAccruedLeaveTotal = 0
            dblLeaveBalanceTotal = 0
            While intCounter1 <= intRecCountLeaveMaster
                If dblAccruedLeave(intCounter1) <> 0 Then
                    'Print #intFile, Space(2) + strLeaveDescription50(intCounter1) & Space(5) & _
                    '                    RightAlignWithLenght10(dblAccruedLeave(intCounter1)) & Space(10) & _
                    '                    RightAlignWithLenght10(dblConsumedLeave(intCounter1)) & Space(10) & _
                    '                    RightAlignWithLenght10(dblAccruedLeave(intCounter1) - dblConsumedLeave(intCounter1))

                    objWriter.WriteLine(Space(2) & strLeaveDescription50(intCounter1).PadRight(50) & Space(5) & _
                                      (dblAccruedLeave(intCounter1)).ToString.PadRight(10) & Space(10) & _
                                      (dblConsumedLeave(intCounter1)).ToString.PadRight(10) & Space(10) & _
                                      (dblAccruedLeave(intCounter1) - dblConsumedLeave(intCounter1)).ToString.PadRight(10))
                    dblConsumedLeaveTotal = dblConsumedLeaveTotal + dblConsumedLeave(intCounter1)
                    dblAccruedLeaveTotal = dblAccruedLeaveTotal + dblAccruedLeave(intCounter1)
                    dblLeaveBalanceTotal = dblLeaveBalanceTotal + (dblAccruedLeave(intCounter1) - dblConsumedLeave(intCounter1))
                End If
                intCounter1 = intCounter1 + 1
            End While
            objWriter.WriteLine(Space(2) & Space(55) & "----------" & Space(10) & "----------" & Space(10) & "----------")
            objWriter.WriteLine(Space(2) & Space(40) & " Total :- " & Space(5) & _
                                    (dblAccruedLeaveTotal).ToString.PadRight(10) & Space(10) & _
                                    (dblConsumedLeaveTotal).ToString.PadRight(10) & Space(10) & _
                                    (dblLeaveBalanceTotal).ToString.PadRight(10))
            objWriter.WriteLine(Space(2) & Space(55) & "----------" & Space(10) & "----------" & Space(10) & "----------")
            mintLine = mintLine + intCounter1Temp + 3
        Next
        'If Common.IsNepali = "Y" Then
        '    TextEdit2.Text = ComboNepaliYearFrm.EditValue
        'End If
        objWriter.Close()
        Try
            Process.Start(mstrFile_Name)
        Catch ex As Exception
            Process.Start("notepad.exe", mstrFile_Name)
        End Try
    End Sub
    Sub MonthlyXl_LeaveCard(ByVal strsortorder As String)
        Dim strsql As String
        Dim strSqlLeaveMaster As String
        Dim rs_report_ReportLeaveMaster As DataSet 'ADODB.Recordset
        Dim strLeaveField3() As String '* 3
        Dim strLeaveDescription50() As String '* 50
        Dim intRecCountLeaveMaster As Integer
        Dim intCounter As Integer
        Dim intCounter1 As Integer
        Dim intCounter1Temp As Integer
        Dim strLeaveWholeLine() As String
        Dim dblConsumedLeave() As Double
        Dim dblAccruedLeave() As Double
        Dim dblConsumedLeaveTotal As Double
        Dim dblAccruedLeaveTotal As Double
        Dim dblLeaveBalanceTotal As Double
        Dim strLvDesc50 As String '* 50
        Dim dblL01 As Double, dblL02 As Double, dblL03 As Double, dblL04 As Double, dblL05 As Double
        Dim dblL06 As Double, dblL07 As Double, dblL08 As Double, dblL09 As Double, dblL10 As Double
        Dim dblL11 As Double, dblL12 As Double, dblL13 As Double, dblL14 As Double, dblL15 As Double
        Dim dblL16 As Double, dblL17 As Double, dblL18 As Double, dblL19 As Double, dblL20 As Double

        Dim dblL01_ADD As Double, dblL02_ADD As Double, dblL03_ADD As Double, dblL04_ADD As Double, dblL05_ADD As Double
        Dim dblL06_ADD As Double, dblL07_ADD As Double, dblL08_ADD As Double, dblL09_ADD As Double, dblL10_ADD As Double
        Dim dblL11_ADD As Double, dblL12_ADD As Double, dblL13_ADD As Double, dblL14_ADD As Double, dblL15_ADD As Double
        Dim dblL16_ADD As Double, dblL17_ADD As Double, dblL18_ADD As Double, dblL19_ADD As Double, dblL20_ADD As Double

        Dim blnDeptAvailable As Boolean

        Dim xlapp As Excel.Application
        Dim xlwb As Excel.Workbook
        Dim xlst As Excel.Sheets
        Dim rowcnt As Integer
        xlapp = CreateObject("Excel.Application")
        xlwb = xlapp.Workbooks.Add
        'If UCase(Dir(App.Path & "\TimeWatch.xls")) = UCase("TimeWatch.xls") Then
        '    Kill(App.Path & "\TimeWatch.xls")
        'End If

        mblnCheckReport = False
        mintPageNo = 1
        mintLine = 1
        mintLine = 1
        If XtraShortOrder.g_SortOrder <> "" Then
            strsortorder = XtraShortOrder.g_SortOrder
        End If
        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If
        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".xls"

        If Common.servername = "Access" Then
            strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tblLeaveLedger.PayCode,tblLeaveLedger.L01,tblLeaveLedger.L02,tblLeaveLedger.L03,tblLeaveLedger.L04,tblLeaveLedger.L05,tblLeaveLedger.L06,tblLeaveLedger.L07,tblLeaveLedger.L08,tblLeaveLedger.L09,tblLeaveLedger.L10,tblLeaveLedger.L11,tblLeaveLedger.L12,tblLeaveLedger.L13,tblLeaveLedger.L14,tblLeaveLedger.L15,tblLeaveLedger.L16,tblLeaveLedger.L17,tblLeaveLedger.L18,tblLeaveLedger.L19,tblLeaveLedger.L20," & _
             "tblLeaveLedger.L01_ADD,tblLeaveLedger.L02_ADD,tblLeaveLedger.L03_ADD,tblLeaveLedger.L04_ADD,tblLeaveLedger.L05_ADD,tblLeaveLedger.L06_ADD,tblLeaveLedger.L07_ADD,tblLeaveLedger.L08_ADD,tblLeaveLedger.L09_ADD,tblLeaveLedger.L10_ADD,tblLeaveLedger.L11_ADD,tblLeaveLedger.L12_ADD,tblLeaveLedger.L13_ADD,tblLeaveLedger.L14_ADD,tblLeaveLedger.L15_ADD,tblLeaveLedger.L16_ADD,tblLeaveLedger.L17_ADD,tblLeaveLedger.L18_ADD,tblLeaveLedger.L19_ADD,tblLeaveLedger.L20_ADD," & _
             "tblEmployee.EmpName,tblEmployee.PresentCardNo,tblDepartment.DepartmentName,tblEmployee.DepartmentCode" & _
             " from tblCatagory,tblDivision,  tblLeaveLedger,tblEmployee,tblCompany,tblDepartment" & _
             " Where tblCatagory.Cat = tblEmployee.Cat And tblLeaveLedger.PayCode = tblEmployee.PayCode And (tblLeaveLedger.L01_ADD <> 0 Or tblLeaveLedger.L02_ADD <> 0 Or tblLeaveLedger.L03_ADD <> 0 Or tblLeaveLedger.L04_ADD <> 0 Or tblLeaveLedger.L05_ADD <> 0 Or tblLeaveLedger.L06_ADD <> 0 Or tblLeaveLedger.L07_ADD <> 0 Or tblLeaveLedger.L08_ADD <> 0 Or tblLeaveLedger.L09_ADD <> 0 Or tblLeaveLedger.L10_ADD <> 0 Or tblLeaveLedger.L11_ADD <> 0 Or tblLeaveLedger.L12_ADD <> 0 Or tblLeaveLedger.L13_ADD <> 0 Or tblLeaveLedger.L14_ADD <> 0 Or tblLeaveLedger.L15_ADD <> 0 Or tblLeaveLedger.L16_ADD <> 0 Or tblLeaveLedger.L17_ADD <> 0 Or tblLeaveLedger.L18_ADD <> 0 Or tblLeaveLedger.L19_ADD <> 0 Or tblLeaveLedger.L20_ADD <> 0)" & _
             " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
             " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & strsortorder
            'format(tblemployee.LeavingDate,'MMM DD YYYY')
            adapA = New OleDbDataAdapter(strsql, Common.con1)
            adapA.Fill(Rs_Report)
        Else
            strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tblLeaveLedger.PayCode,tblLeaveLedger.L01,tblLeaveLedger.L02,tblLeaveLedger.L03,tblLeaveLedger.L04,tblLeaveLedger.L05,tblLeaveLedger.L06,tblLeaveLedger.L07,tblLeaveLedger.L08,tblLeaveLedger.L09,tblLeaveLedger.L10,tblLeaveLedger.L11,tblLeaveLedger.L12,tblLeaveLedger.L13,tblLeaveLedger.L14,tblLeaveLedger.L15,tblLeaveLedger.L16,tblLeaveLedger.L17,tblLeaveLedger.L18,tblLeaveLedger.L19,tblLeaveLedger.L20," & _
             "tblLeaveLedger.L01_ADD,tblLeaveLedger.L02_ADD,tblLeaveLedger.L03_ADD,tblLeaveLedger.L04_ADD,tblLeaveLedger.L05_ADD,tblLeaveLedger.L06_ADD,tblLeaveLedger.L07_ADD,tblLeaveLedger.L08_ADD,tblLeaveLedger.L09_ADD,tblLeaveLedger.L10_ADD,tblLeaveLedger.L11_ADD,tblLeaveLedger.L12_ADD,tblLeaveLedger.L13_ADD,tblLeaveLedger.L14_ADD,tblLeaveLedger.L15_ADD,tblLeaveLedger.L16_ADD,tblLeaveLedger.L17_ADD,tblLeaveLedger.L18_ADD,tblLeaveLedger.L19_ADD,tblLeaveLedger.L20_ADD," & _
             "tblEmployee.EmpName,tblEmployee.PresentCardNo,tblDepartment.DepartmentName,tblEmployee.DepartmentCode" & _
             " from tblCatagory,tblDivision,  tblLeaveLedger,tblEmployee,tblCompany,tblDepartment" & _
             " Where tblCatagory.Cat = tblEmployee.Cat And tblLeaveLedger.PayCode = tblEmployee.PayCode And (tblLeaveLedger.L01_ADD <> 0 Or tblLeaveLedger.L02_ADD <> 0 Or tblLeaveLedger.L03_ADD <> 0 Or tblLeaveLedger.L04_ADD <> 0 Or tblLeaveLedger.L05_ADD <> 0 Or tblLeaveLedger.L06_ADD <> 0 Or tblLeaveLedger.L07_ADD <> 0 Or tblLeaveLedger.L08_ADD <> 0 Or tblLeaveLedger.L09_ADD <> 0 Or tblLeaveLedger.L10_ADD <> 0 Or tblLeaveLedger.L11_ADD <> 0 Or tblLeaveLedger.L12_ADD <> 0 Or tblLeaveLedger.L13_ADD <> 0 Or tblLeaveLedger.L14_ADD <> 0 Or tblLeaveLedger.L15_ADD <> 0 Or tblLeaveLedger.L16_ADD <> 0 Or tblLeaveLedger.L17_ADD <> 0 Or tblLeaveLedger.L18_ADD <> 0 Or tblLeaveLedger.L19_ADD <> 0 Or tblLeaveLedger.L20_ADD <> 0)" & _
             " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
             " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & strsortorder
            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(Rs_Report)
        End If
        If Rs_Report.Tables(0).Rows.Count < 1 Then
            XtraMessageBox.Show(ulf, "<size=10>No Data Available For this Report.</size>", "<size=9>iAS</size>")
            Exit Sub
        End If

        strSqlLeaveMaster = " Select LeaveField,LeaveDescription from tblLeaveMaster"
        'rs_report_ReportLeaveMaster = Cn.Execute(strSqlLeaveMaster)
        mintLine = 4
        rs_report_ReportLeaveMaster = New DataSet
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(strSqlLeaveMaster, Common.con1)
            adapA.Fill(rs_report_ReportLeaveMaster)
        Else
            adap = New SqlDataAdapter(strSqlLeaveMaster, Common.con)
            adap.Fill(rs_report_ReportLeaveMaster)
        End If
        intRecCountLeaveMaster = rs_report_ReportLeaveMaster.Tables(0).Rows.Count

        ReDim strLeaveField3(intRecCountLeaveMaster)
        ReDim strLeaveDescription50(intRecCountLeaveMaster)
        ReDim strLeaveWholeLine(intRecCountLeaveMaster)
        ReDim dblConsumedLeave(intRecCountLeaveMaster)
        ReDim dblAccruedLeave(intRecCountLeaveMaster)
        'rs_report_ReportLeaveMaster.MoveFirst()

        For i As Integer = 0 To rs_report_ReportLeaveMaster.Tables(0).Rows.Count - 1 ' While Not .EOF
            intCounter = intCounter + 1
            strLeaveField3(intCounter) = rs_report_ReportLeaveMaster.Tables(0).Rows(i).Item("leavefield").ToString.Trim
            strLvDesc50 = ""
            If rs_report_ReportLeaveMaster.Tables(0).Rows(i).Item("LeaveDescription").ToString.Trim <> "" Then strLvDesc50 = rs_report_ReportLeaveMaster.Tables(0).Rows(i).Item("LeaveDescription").ToString.Trim
            strLeaveDescription50(intCounter) = strLvDesc50
        Next
        rowcnt = rowcnt + 1
        xlapp.Visible = True
        xlapp.Columns.ColumnWidth = 10
        xlapp.Columns.Font.Name = "Tahoma"
        xlapp.Cells(rowcnt, 5).Font.Bold = True
        xlapp.Cells(rowcnt, 5) = "Company Name:" & CommonReport.g_CompanyNames
        rowcnt = rowcnt + 1
        xlapp.Cells(rowcnt, 5).Font.Bold = True
        xlapp.Cells(rowcnt, 5) = "Run Date & Time :" & Format(Now(), "dd/MM/yyyy HH:MM")
        rowcnt = rowcnt + 2
        xlapp.Cells.Font.Size = 8
        xlapp.Cells(rowcnt, 4).Font.Bold = True
        xlapp.Cells(rowcnt, 4) = " LEAVE CARD FOR THE YEAR : " & TextEdit2.Text
        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1 '  Do While Not .EOF
            'Do While Not .EOF
            intCounter = 1
            While intCounter <= intRecCountLeaveMaster
                Select Case strLeaveField3(intCounter)
                    Case "L01"
                        dblL01 = 0
                        dblL01_ADD = 0
                        If Rs_Report.Tables(0).Rows(i).Item("L01").ToString.Trim <> "" Then dblL01 = Rs_Report.Tables(0).Rows(i).Item("L01").ToString.Trim
                        If Rs_Report.Tables(0).Rows(i).Item("L01_ADD").ToString.Trim <> "" Then dblL01_ADD = Rs_Report.Tables(0).Rows(i).Item("L01_ADD").ToString.Trim
                        dblConsumedLeave(intCounter) = dblL01
                        dblAccruedLeave(intCounter) = dblL01_ADD
                    Case "L02"
                        dblL02 = 0
                        dblL02_ADD = 0
                        If Rs_Report.Tables(0).Rows(i).Item("L02").ToString.Trim <> "" Then dblL02 = Rs_Report.Tables(0).Rows(i).Item("L02").ToString.Trim
                        If Rs_Report.Tables(0).Rows(i).Item("L02_ADD").ToString.Trim <> "" Then dblL02_ADD = Rs_Report.Tables(0).Rows(i).Item("L02_ADD").ToString.Trim
                        dblConsumedLeave(intCounter) = dblL02
                        dblAccruedLeave(intCounter) = dblL02_ADD
                    Case "L03"
                        dblL03 = 0
                        dblL03_ADD = 0
                        If Rs_Report.Tables(0).Rows(i).Item("L03").ToString.Trim <> "" Then dblL03 = Rs_Report.Tables(0).Rows(i).Item("L03").ToString.Trim
                        If Rs_Report.Tables(0).Rows(i).Item("L03_ADD").ToString.Trim <> "" Then dblL03_ADD = Rs_Report.Tables(0).Rows(i).Item("L03_ADD").ToString.Trim
                        dblConsumedLeave(intCounter) = dblL03
                        dblAccruedLeave(intCounter) = dblL03_ADD
                    Case "L04"
                        dblL04 = 0
                        dblL04_ADD = 0
                        If Rs_Report.Tables(0).Rows(i).Item("L04").ToString.Trim <> "" Then dblL04 = Rs_Report.Tables(0).Rows(i).Item("L04").ToString.Trim
                        If Rs_Report.Tables(0).Rows(i).Item("L04_ADD").ToString.Trim <> "" Then dblL04_ADD = Rs_Report.Tables(0).Rows(i).Item("L04_ADD").ToString.Trim
                        dblConsumedLeave(intCounter) = dblL04
                        dblAccruedLeave(intCounter) = dblL04_ADD
                    Case "L05"
                        dblL05 = 0
                        dblL05_ADD = 0
                        If Rs_Report.Tables(0).Rows(i).Item("L05").ToString.Trim <> "" Then dblL05 = Rs_Report.Tables(0).Rows(i).Item("L05").ToString.Trim
                        If Rs_Report.Tables(0).Rows(i).Item("L05_ADD").ToString.Trim <> "" Then dblL05_ADD = Rs_Report.Tables(0).Rows(i).Item("L05_ADD").ToString.Trim
                        dblConsumedLeave(intCounter) = dblL05
                        dblAccruedLeave(intCounter) = dblL05_ADD
                    Case "L06"
                        dblL06 = 0
                        dblL06_ADD = 0
                        If Rs_Report.Tables(0).Rows(i).Item("L06").ToString.Trim <> "" Then dblL06 = Rs_Report.Tables(0).Rows(i).Item("L06").ToString.Trim
                        If Rs_Report.Tables(0).Rows(i).Item("L06_ADD").ToString.Trim <> "" Then dblL06_ADD = Rs_Report.Tables(0).Rows(i).Item("L06_ADD").ToString.Trim
                        dblConsumedLeave(intCounter) = dblL06
                        dblAccruedLeave(intCounter) = dblL06_ADD
                    Case "L07"
                        dblL07 = 0
                        dblL07_ADD = 0
                        If Rs_Report.Tables(0).Rows(i).Item("L07").ToString.Trim <> "" Then dblL07 = Rs_Report.Tables(0).Rows(i).Item("L07").ToString.Trim
                        If Not Rs_Report.Tables(0).Rows(i).Item("L07_ADD").ToString.Trim <> "" Then dblL07_ADD = Rs_Report.Tables(0).Rows(i).Item("L07_ADD").ToString.Trim
                        dblConsumedLeave(intCounter) = dblL07
                        dblAccruedLeave(intCounter) = dblL07_ADD
                    Case "L08"
                        dblL08 = 0
                        dblL08_ADD = 0
                        If Rs_Report.Tables(0).Rows(i).Item("L08").ToString.Trim <> "" Then dblL08 = Rs_Report.Tables(0).Rows(i).Item("L08").ToString.Trim
                        If Rs_Report.Tables(0).Rows(i).Item("L08_ADD").ToString.Trim <> "" Then dblL08_ADD = Rs_Report.Tables(0).Rows(i).Item("L08_ADD").ToString.Trim
                        dblConsumedLeave(intCounter) = dblL08
                        dblAccruedLeave(intCounter) = dblL08_ADD
                    Case "L09"
                        dblL09 = 0
                        dblL09_ADD = 0
                        If Rs_Report.Tables(0).Rows(i).Item("L09").ToString.Trim <> "" Then dblL09 = Rs_Report.Tables(0).Rows(i).Item("L09").ToString.Trim
                        If Rs_Report.Tables(0).Rows(i).Item("L09_ADD").ToString.Trim <> "" Then dblL09_ADD = Rs_Report.Tables(0).Rows(i).Item("L09_ADD").ToString.Trim
                        dblConsumedLeave(intCounter) = dblL09
                        dblAccruedLeave(intCounter) = dblL09_ADD
                    Case "L10"
                        dblL10 = 0
                        dblL10_ADD = 0
                        If Not Rs_Report.Tables(0).Rows(i).Item("L10").ToString.Trim <> "" Then dblL10 = Rs_Report.Tables(0).Rows(i).Item("L10").ToString.Trim
                        If Not Rs_Report.Tables(0).Rows(i).Item("L10_ADD").ToString.Trim <> "" Then dblL10_ADD = Rs_Report.Tables(0).Rows(i).Item("L10_ADD").ToString.Trim
                        dblConsumedLeave(intCounter) = dblL10
                        dblAccruedLeave(intCounter) = dblL10_ADD
                    Case "L11"
                        dblL11 = 0
                        dblL11_ADD = 0
                        If Rs_Report.Tables(0).Rows(i).Item("L11").ToString.Trim <> "" Then dblL11 = Rs_Report.Tables(0).Rows(i).Item("L11").ToString.Trim
                        If Rs_Report.Tables(0).Rows(i).Item("L11_ADD").ToString.Trim <> "" Then dblL11_ADD = Rs_Report.Tables(0).Rows(i).Item("L11_ADD").ToString.Trim
                        dblConsumedLeave(intCounter) = dblL11
                        dblAccruedLeave(intCounter) = dblL11_ADD
                    Case "L12"
                        dblL12 = 0
                        dblL12_ADD = 0
                        If Rs_Report.Tables(0).Rows(i).Item("L12").ToString.Trim <> "" Then dblL12 = Rs_Report.Tables(0).Rows(i).Item("L12").ToString.Trim
                        If Rs_Report.Tables(0).Rows(i).Item("L12_ADD").ToString.Trim <> "" Then dblL12_ADD = Rs_Report.Tables(0).Rows(i).Item("L12_ADD").ToString.Trim
                        dblConsumedLeave(intCounter) = dblL12
                        dblAccruedLeave(intCounter) = dblL12_ADD
                    Case "L13"
                        dblL13 = 0
                        dblL13_ADD = 0
                        If Rs_Report.Tables(0).Rows(i).Item("L13").ToString.Trim <> "" Then dblL13 = Rs_Report.Tables(0).Rows(i).Item("L13").ToString.Trim
                        If Rs_Report.Tables(0).Rows(i).Item("L13_ADD").ToString.Trim <> "" Then dblL13_ADD = Rs_Report.Tables(0).Rows(i).Item("L13_ADD").ToString.Trim
                        dblConsumedLeave(intCounter) = dblL13
                        dblAccruedLeave(intCounter) = dblL13_ADD
                    Case "L14"
                        dblL14 = 0
                        dblL14_ADD = 0
                        If Rs_Report.Tables(0).Rows(i).Item("L14").ToString.Trim <> "" Then dblL14 = Rs_Report.Tables(0).Rows(i).Item("L14").ToString.Trim
                        If Rs_Report.Tables(0).Rows(i).Item("L14_ADD").ToString.Trim <> "" Then dblL14_ADD = Rs_Report.Tables(0).Rows(i).Item("L14_ADD").ToString.Trim
                        dblConsumedLeave(intCounter) = dblL14
                        dblAccruedLeave(intCounter) = dblL14_ADD
                    Case "L15"
                        dblL15 = 0
                        dblL15_ADD = 0
                        If Rs_Report.Tables(0).Rows(i).Item("L15").ToString.Trim <> "" Then dblL15 = Rs_Report.Tables(0).Rows(i).Item("L15").ToString.Trim
                        If Rs_Report.Tables(0).Rows(i).Item("L15_ADD").ToString.Trim <> "" Then dblL15_ADD = Rs_Report.Tables(0).Rows(i).Item("L15_ADD").ToString.Trim
                        dblConsumedLeave(intCounter) = dblL15
                        dblAccruedLeave(intCounter) = dblL15_ADD
                    Case "L16"
                        dblL16 = 0
                        dblL16_ADD = 0
                        If Rs_Report.Tables(0).Rows(i).Item("L16").ToString.Trim <> "" Then dblL16 = Rs_Report.Tables(0).Rows(i).Item("L16").ToString.Trim
                        If Rs_Report.Tables(0).Rows(i).Item("L16_ADD").ToString.Trim <> "" Then dblL16_ADD = Rs_Report.Tables(0).Rows(i).Item("L16_ADD").ToString.Trim
                        dblConsumedLeave(intCounter) = dblL16
                        dblAccruedLeave(intCounter) = dblL16_ADD
                    Case "L17"
                        dblL17 = 0
                        dblL17_ADD = 0
                        If Rs_Report.Tables(0).Rows(i).Item("L17").ToString.Trim <> "" Then dblL17 = Rs_Report.Tables(0).Rows(i).Item("L17").ToString.Trim
                        If Rs_Report.Tables(0).Rows(i).Item("L17_ADD").ToString.Trim <> "" Then dblL17_ADD = Rs_Report.Tables(0).Rows(i).Item("L17_ADD").ToString.Trim
                        dblConsumedLeave(intCounter) = dblL17
                        dblAccruedLeave(intCounter) = dblL17_ADD
                    Case "L18"
                        dblL18 = 0
                        dblL18_ADD = 0
                        If Rs_Report.Tables(0).Rows(i).Item("L18").ToString.Trim <> "" Then dblL18 = Rs_Report.Tables(0).Rows(i).Item("L18").ToString.Trim
                        If Rs_Report.Tables(0).Rows(i).Item("L18_ADD").ToString.Trim <> "" Then dblL18_ADD = Rs_Report.Tables(0).Rows(i).Item("L18_ADD").ToString.Trim
                        dblConsumedLeave(intCounter) = dblL18
                        dblAccruedLeave(intCounter) = dblL18_ADD
                    Case "L19"
                        dblL19 = 0
                        dblL19_ADD = 0
                        If Rs_Report.Tables(0).Rows(i).Item("L19").ToString.Trim <> "" Then dblL19 = Rs_Report.Tables(0).Rows(i).Item("L19").ToString.Trim
                        If Rs_Report.Tables(0).Rows(i).Item("L19_ADD").ToString.Trim <> "" Then dblL19_ADD = Rs_Report.Tables(0).Rows(i).Item("L19_ADD").ToString.Trim
                        dblConsumedLeave(intCounter) = dblL19
                        dblAccruedLeave(intCounter) = dblL19_ADD
                    Case "L20"
                        dblL20 = 0
                        dblL20_ADD = 0
                        If Rs_Report.Tables(0).Rows(i).Item("L20").ToString.Trim <> "" Then dblL20 = Rs_Report.Tables(0).Rows(i).Item("L20").ToString.Trim
                        If Rs_Report.Tables(0).Rows(i).Item("L20_ADD").ToString.Trim <> "" Then dblL20_ADD = Rs_Report.Tables(0).Rows(i).Item("L20_ADD").ToString.Trim
                        dblConsumedLeave(intCounter) = dblL20
                        dblAccruedLeave(intCounter) = dblL20_ADD
                End Select
                intCounter = intCounter + 1
            End While
            intCounter1 = 1
            intCounter1Temp = 0
            While intCounter1 <= intRecCountLeaveMaster
                If dblAccruedLeave(intCounter1) <> 0 Then
                    intCounter1Temp = intCounter1Temp + 1
                End If
                intCounter1 = intCounter1 + 1
            End While


            rowcnt = rowcnt + 1
            xlapp.Cells(rowcnt, 2).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 3).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 4).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 5).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 6).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 7).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 8).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 9).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 2).Font.Bold = True
            xlapp.Cells(rowcnt, 2) = "Employee's Card No                      : " & Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim
            rowcnt = rowcnt + 1
            xlapp.Cells(rowcnt, 2).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 3).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 4).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 5).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 6).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 7).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 8).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 9).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 2).Font.Bold = True
            xlapp.Cells(rowcnt, 2) = "Employee's PayRoll Code & Name : " & Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim & Space(2) & Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim
            rowcnt = rowcnt + 1
            xlapp.Cells(rowcnt, 2).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 3).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 4).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 5).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 6).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 7).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 7).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 8).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 9).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 2).Font.Bold = True
            xlapp.Cells(rowcnt, 2) = "Department Code & Name             : " & Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim & Space(2) & Rs_Report.Tables(0).Rows(i).Item("DepartmentName").ToString.Trim
            rowcnt = rowcnt + 1
            xlapp.Cells(rowcnt, 2).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 3).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 4).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 5).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 6).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 7).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 8).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 9).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 2).Font.Bold = True
            xlapp.Cells(rowcnt, 2).Font.ColorIndex = 2 'vbWhite
            xlapp.Cells(rowcnt, 2).Interior.ColorIndex = 51
            xlapp.Cells(rowcnt, 5).Font.Bold = True
            xlapp.Cells(rowcnt, 5).Font.ColorIndex = 2 'vbWhite
            xlapp.Cells(rowcnt, 5).Interior.ColorIndex = 51
            xlapp.Cells(rowcnt, 7).Font.Bold = True
            xlapp.Cells(rowcnt, 7).Font.ColorIndex = 2 ' vbWhite
            xlapp.Cells(rowcnt, 7).Interior.ColorIndex = 51
            xlapp.Cells(rowcnt, 9).Font.Bold = True
            xlapp.Cells(rowcnt, 9).Font.ColorIndex = 2 'vbWhite
            xlapp.Cells(rowcnt, 9).Interior.ColorIndex = 51
            xlapp.Cells(rowcnt, 2) = "Leave Type"
            xlapp.Cells(rowcnt, 5) = "Leave Accrued"
            xlapp.Cells(rowcnt, 7) = "Leave Consumed"
            xlapp.Cells(rowcnt, 9) = "Leave Balance"
            rowcnt = rowcnt + 1


            mintLine = 13
            mblnCheckReport = True
            intCounter1 = 1
            dblConsumedLeaveTotal = 0
            dblAccruedLeaveTotal = 0
            dblLeaveBalanceTotal = 0
            While intCounter1 <= intRecCountLeaveMaster
                If dblAccruedLeave(intCounter1) <> 0 Then
                    rowcnt = rowcnt + 1
                    xlapp.Cells(rowcnt, 2).Borders.LineStyle = XlLineStyle.xlContinuous
                    xlapp.Cells(rowcnt, 3).Borders.LineStyle = XlLineStyle.xlContinuous
                    xlapp.Cells(rowcnt, 4).Borders.LineStyle = XlLineStyle.xlContinuous
                    xlapp.Cells(rowcnt, 2).Font.Bold = True
                    xlapp.Cells(rowcnt, 2) = strLeaveDescription50(intCounter1)
                    xlapp.Cells(rowcnt, 5).Borders.LineStyle = XlLineStyle.xlContinuous
                    xlapp.Cells(rowcnt, 6).Borders.LineStyle = XlLineStyle.xlContinuous
                    xlapp.Cells(rowcnt, 2).Font.Bold = True
                    xlapp.Cells(rowcnt, 5) = (dblAccruedLeave(intCounter1))
                    xlapp.Cells(rowcnt, 7).Borders.LineStyle = XlLineStyle.xlContinuous
                    xlapp.Cells(rowcnt, 2).Font.Bold = True
                    xlapp.Cells(rowcnt, 7) = (dblConsumedLeave(intCounter1))
                    xlapp.Cells(rowcnt, 8).Borders.LineStyle = XlLineStyle.xlContinuous
                    xlapp.Cells(rowcnt, 9).Borders.LineStyle = XlLineStyle.xlContinuous
                    xlapp.Cells(rowcnt, 2).Font.Bold = True
                    xlapp.Cells(rowcnt, 9) = (dblAccruedLeave(intCounter1) - dblConsumedLeave(intCounter1))
                    rowcnt = rowcnt + 1

                    dblConsumedLeaveTotal = dblConsumedLeaveTotal + dblConsumedLeave(intCounter1)
                    dblAccruedLeaveTotal = dblAccruedLeaveTotal + dblAccruedLeave(intCounter1)
                    dblLeaveBalanceTotal = dblLeaveBalanceTotal + (dblAccruedLeave(intCounter1) - dblConsumedLeave(intCounter1))
                End If
                intCounter1 = intCounter1 + 1
            End While
            rowcnt = rowcnt + 1
            xlapp.Cells(rowcnt, 2).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 2).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 3).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 4).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 8).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 6).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 2).Font.Bold = True
            xlapp.Cells(rowcnt, 2).Font.ColorIndex = 2 'vbWhite
            xlapp.Cells(rowcnt, 2).Interior.ColorIndex = 51
            xlapp.Cells(rowcnt, 2) = "Total :"
            xlapp.Cells(rowcnt, 5).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 5).Font.Bold = True
            xlapp.Cells(rowcnt, 5).Font.ColorIndex = 2 'vbWhite
            xlapp.Cells(rowcnt, 5).Interior.ColorIndex = 51
            xlapp.Cells(rowcnt, 5) = (dblAccruedLeaveTotal)
            xlapp.Cells(rowcnt, 7).Font.Bold = True
            xlapp.Cells(rowcnt, 7).Font.ColorIndex = 2 'vbWhite
            xlapp.Cells(rowcnt, 7).Interior.ColorIndex = 51
            xlapp.Cells(rowcnt, 7).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 7) = (dblConsumedLeaveTotal)
            xlapp.Cells(rowcnt, 9).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 9).Font.Bold = True
            xlapp.Cells(rowcnt, 9).Font.ColorIndex = 2 'vbWhite
            xlapp.Cells(rowcnt, 9).Interior.ColorIndex = 51
            xlapp.Cells(rowcnt, 9) = (dblLeaveBalanceTotal)
            mintLine = mintLine + intCounter1Temp + 3

        Next
        xlwb.SaveAs(mstrFile_Name)
    End Sub
    Sub MonthlyXl_LeaveCardGrid(ByVal strsortorder As String)
        Dim strsql As String
        Dim strSqlLeaveMaster As String
        Dim rs_report_ReportLeaveMaster As DataSet 'ADODB.Recordset
        Dim strLeaveField3() As String '* 3
        Dim strLeaveDescription50() As String '* 50
        Dim intRecCountLeaveMaster As Integer
        Dim intCounter As Integer
        Dim intCounter1 As Integer
        Dim intCounter1Temp As Integer
        Dim strLeaveWholeLine() As String
        Dim dblConsumedLeave() As Double
        Dim dblAccruedLeave() As Double
        Dim dblConsumedLeaveTotal As Double
        Dim dblAccruedLeaveTotal As Double
        Dim dblLeaveBalanceTotal As Double
        Dim strLvDesc50 As String '* 50
        Dim dblL01 As Double, dblL02 As Double, dblL03 As Double, dblL04 As Double, dblL05 As Double
        Dim dblL06 As Double, dblL07 As Double, dblL08 As Double, dblL09 As Double, dblL10 As Double
        Dim dblL11 As Double, dblL12 As Double, dblL13 As Double, dblL14 As Double, dblL15 As Double
        Dim dblL16 As Double, dblL17 As Double, dblL18 As Double, dblL19 As Double, dblL20 As Double

        Dim dblL01_ADD As Double, dblL02_ADD As Double, dblL03_ADD As Double, dblL04_ADD As Double, dblL05_ADD As Double
        Dim dblL06_ADD As Double, dblL07_ADD As Double, dblL08_ADD As Double, dblL09_ADD As Double, dblL10_ADD As Double
        Dim dblL11_ADD As Double, dblL12_ADD As Double, dblL13_ADD As Double, dblL14_ADD As Double, dblL15_ADD As Double
        Dim dblL16_ADD As Double, dblL17_ADD As Double, dblL18_ADD As Double, dblL19_ADD As Double, dblL20_ADD As Double
        'Dim xlapp As Excel.Application
        'Dim xlwb As Excel.Workbook
        'Dim xlst As Excel.Sheets
        Dim rowcnt As Integer
        'xlapp = CreateObject("Excel.Application")
        'xlwb = xlapp.Workbooks.Add
        mblnCheckReport = False
        mintPageNo = 1
        mintLine = 1
        mintLine = 1
        If XtraShortOrder.g_SortOrder <> "" Then
            strsortorder = XtraShortOrder.g_SortOrder
        End If
        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If
        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".xls"

        If Common.servername = "Access" Then
            strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tblLeaveLedger.PayCode,tblLeaveLedger.L01,tblLeaveLedger.L02,tblLeaveLedger.L03,tblLeaveLedger.L04,tblLeaveLedger.L05,tblLeaveLedger.L06,tblLeaveLedger.L07,tblLeaveLedger.L08,tblLeaveLedger.L09,tblLeaveLedger.L10,tblLeaveLedger.L11,tblLeaveLedger.L12,tblLeaveLedger.L13,tblLeaveLedger.L14,tblLeaveLedger.L15,tblLeaveLedger.L16,tblLeaveLedger.L17,tblLeaveLedger.L18,tblLeaveLedger.L19,tblLeaveLedger.L20," & _
             "tblLeaveLedger.L01_ADD,tblLeaveLedger.L02_ADD,tblLeaveLedger.L03_ADD,tblLeaveLedger.L04_ADD,tblLeaveLedger.L05_ADD,tblLeaveLedger.L06_ADD,tblLeaveLedger.L07_ADD,tblLeaveLedger.L08_ADD,tblLeaveLedger.L09_ADD,tblLeaveLedger.L10_ADD,tblLeaveLedger.L11_ADD,tblLeaveLedger.L12_ADD,tblLeaveLedger.L13_ADD,tblLeaveLedger.L14_ADD,tblLeaveLedger.L15_ADD,tblLeaveLedger.L16_ADD,tblLeaveLedger.L17_ADD,tblLeaveLedger.L18_ADD,tblLeaveLedger.L19_ADD,tblLeaveLedger.L20_ADD," & _
             "tblEmployee.EmpName,tblEmployee.PresentCardNo,tblDepartment.DepartmentName,tblEmployee.DepartmentCode" & _
             " from tblCatagory,tblDivision,  tblLeaveLedger,tblEmployee,tblCompany,tblDepartment" & _
             " Where tblCatagory.Cat = tblEmployee.Cat And tblLeaveLedger.PayCode = tblEmployee.PayCode And (tblLeaveLedger.L01_ADD <> 0 Or tblLeaveLedger.L02_ADD <> 0 Or tblLeaveLedger.L03_ADD <> 0 Or tblLeaveLedger.L04_ADD <> 0 Or tblLeaveLedger.L05_ADD <> 0 Or tblLeaveLedger.L06_ADD <> 0 Or tblLeaveLedger.L07_ADD <> 0 Or tblLeaveLedger.L08_ADD <> 0 Or tblLeaveLedger.L09_ADD <> 0 Or tblLeaveLedger.L10_ADD <> 0 Or tblLeaveLedger.L11_ADD <> 0 Or tblLeaveLedger.L12_ADD <> 0 Or tblLeaveLedger.L13_ADD <> 0 Or tblLeaveLedger.L14_ADD <> 0 Or tblLeaveLedger.L15_ADD <> 0 Or tblLeaveLedger.L16_ADD <> 0 Or tblLeaveLedger.L17_ADD <> 0 Or tblLeaveLedger.L18_ADD <> 0 Or tblLeaveLedger.L19_ADD <> 0 Or tblLeaveLedger.L20_ADD <> 0)" & _
             " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
             " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & strsortorder
            adapA = New OleDbDataAdapter(strsql, Common.con1)
            adapA.Fill(Rs_Report)
        Else
            strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tblLeaveLedger.PayCode,tblLeaveLedger.L01,tblLeaveLedger.L02,tblLeaveLedger.L03,tblLeaveLedger.L04,tblLeaveLedger.L05,tblLeaveLedger.L06,tblLeaveLedger.L07,tblLeaveLedger.L08,tblLeaveLedger.L09,tblLeaveLedger.L10,tblLeaveLedger.L11,tblLeaveLedger.L12,tblLeaveLedger.L13,tblLeaveLedger.L14,tblLeaveLedger.L15,tblLeaveLedger.L16,tblLeaveLedger.L17,tblLeaveLedger.L18,tblLeaveLedger.L19,tblLeaveLedger.L20," & _
             "tblLeaveLedger.L01_ADD,tblLeaveLedger.L02_ADD,tblLeaveLedger.L03_ADD,tblLeaveLedger.L04_ADD,tblLeaveLedger.L05_ADD,tblLeaveLedger.L06_ADD,tblLeaveLedger.L07_ADD,tblLeaveLedger.L08_ADD,tblLeaveLedger.L09_ADD,tblLeaveLedger.L10_ADD,tblLeaveLedger.L11_ADD,tblLeaveLedger.L12_ADD,tblLeaveLedger.L13_ADD,tblLeaveLedger.L14_ADD,tblLeaveLedger.L15_ADD,tblLeaveLedger.L16_ADD,tblLeaveLedger.L17_ADD,tblLeaveLedger.L18_ADD,tblLeaveLedger.L19_ADD,tblLeaveLedger.L20_ADD," & _
             "tblEmployee.EmpName,tblEmployee.PresentCardNo,tblDepartment.DepartmentName,tblEmployee.DepartmentCode" & _
             " from tblCatagory,tblDivision,  tblLeaveLedger,tblEmployee,tblCompany,tblDepartment" & _
             " Where tblCatagory.Cat = tblEmployee.Cat And tblLeaveLedger.PayCode = tblEmployee.PayCode And (tblLeaveLedger.L01_ADD <> 0 Or tblLeaveLedger.L02_ADD <> 0 Or tblLeaveLedger.L03_ADD <> 0 Or tblLeaveLedger.L04_ADD <> 0 Or tblLeaveLedger.L05_ADD <> 0 Or tblLeaveLedger.L06_ADD <> 0 Or tblLeaveLedger.L07_ADD <> 0 Or tblLeaveLedger.L08_ADD <> 0 Or tblLeaveLedger.L09_ADD <> 0 Or tblLeaveLedger.L10_ADD <> 0 Or tblLeaveLedger.L11_ADD <> 0 Or tblLeaveLedger.L12_ADD <> 0 Or tblLeaveLedger.L13_ADD <> 0 Or tblLeaveLedger.L14_ADD <> 0 Or tblLeaveLedger.L15_ADD <> 0 Or tblLeaveLedger.L16_ADD <> 0 Or tblLeaveLedger.L17_ADD <> 0 Or tblLeaveLedger.L18_ADD <> 0 Or tblLeaveLedger.L19_ADD <> 0 Or tblLeaveLedger.L20_ADD <> 0)" & _
             " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
             " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & strsortorder
            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(Rs_Report)
        End If
        If Rs_Report.Tables(0).Rows.Count < 1 Then
            XtraMessageBox.Show(ulf, "<size=10>No Data Available For this Report.</size>", "<size=9>iAS</size>")
            Exit Sub
        End If

        strSqlLeaveMaster = " Select LeaveField,LeaveDescription from tblLeaveMaster"
        mintLine = 4
        rs_report_ReportLeaveMaster = New DataSet
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(strSqlLeaveMaster, Common.con1)
            adapA.Fill(rs_report_ReportLeaveMaster)
        Else
            adap = New SqlDataAdapter(strSqlLeaveMaster, Common.con)
            adap.Fill(rs_report_ReportLeaveMaster)
        End If
        intRecCountLeaveMaster = rs_report_ReportLeaveMaster.Tables(0).Rows.Count

        ReDim strLeaveField3(intRecCountLeaveMaster)
        ReDim strLeaveDescription50(intRecCountLeaveMaster)
        ReDim strLeaveWholeLine(intRecCountLeaveMaster)
        ReDim dblConsumedLeave(intRecCountLeaveMaster)
        ReDim dblAccruedLeave(intRecCountLeaveMaster)

        For i As Integer = 0 To rs_report_ReportLeaveMaster.Tables(0).Rows.Count - 1 ' While Not .EOF
            intCounter = intCounter + 1
            strLeaveField3(intCounter) = rs_report_ReportLeaveMaster.Tables(0).Rows(i).Item("leavefield").ToString.Trim
            strLvDesc50 = ""
            If rs_report_ReportLeaveMaster.Tables(0).Rows(i).Item("LeaveDescription").ToString.Trim <> "" Then strLvDesc50 = rs_report_ReportLeaveMaster.Tables(0).Rows(i).Item("LeaveDescription").ToString.Trim
            strLeaveDescription50(intCounter) = strLvDesc50
        Next
        rowcnt = rowcnt + 1

        If Common.IsNepali = "Y" Then
            Common.frodatetodatetoReportGrid = " LEAVE CARD FOR THE YEAR : " & tmpNepali
        Else
            Common.frodatetodatetoReportGrid = " LEAVE CARD FOR THE YEAR : " & TextEdit2.Text
        End If


        Me.Cursor = Cursors.WaitCursor
        Common.tbl = New Data.DataTable()
        Common.tbl.Columns.Add("Leave Type", GetType(String))
        Common.tbl.Columns.Add(" ", GetType(String))
        Common.tbl.Columns.Add("  ", GetType(String))
        Common.tbl.Columns.Add("Leave Accrued", GetType(String))
        Common.tbl.Columns.Add("   ", GetType(String))
        Common.tbl.Columns.Add("Leave Consumed", GetType(String))
        Common.tbl.Columns.Add("    ", GetType(String))
        Common.tbl.Columns.Add("Leave Balance", GetType(String))

        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1 '  Do While Not .EOF
            'Do While Not .EOF
            intCounter = 1
            While intCounter <= intRecCountLeaveMaster
                Select Case strLeaveField3(intCounter)
                    Case "L01"
                        dblL01 = 0
                        dblL01_ADD = 0
                        If Rs_Report.Tables(0).Rows(i).Item("L01").ToString.Trim <> "" Then dblL01 = Rs_Report.Tables(0).Rows(i).Item("L01").ToString.Trim
                        If Rs_Report.Tables(0).Rows(i).Item("L01_ADD").ToString.Trim <> "" Then dblL01_ADD = Rs_Report.Tables(0).Rows(i).Item("L01_ADD").ToString.Trim
                        dblConsumedLeave(intCounter) = dblL01
                        dblAccruedLeave(intCounter) = dblL01_ADD
                    Case "L02"
                        dblL02 = 0
                        dblL02_ADD = 0
                        If Rs_Report.Tables(0).Rows(i).Item("L02").ToString.Trim <> "" Then dblL02 = Rs_Report.Tables(0).Rows(i).Item("L02").ToString.Trim
                        If Rs_Report.Tables(0).Rows(i).Item("L02_ADD").ToString.Trim <> "" Then dblL02_ADD = Rs_Report.Tables(0).Rows(i).Item("L02_ADD").ToString.Trim
                        dblConsumedLeave(intCounter) = dblL02
                        dblAccruedLeave(intCounter) = dblL02_ADD
                    Case "L03"
                        dblL03 = 0
                        dblL03_ADD = 0
                        If Rs_Report.Tables(0).Rows(i).Item("L03").ToString.Trim <> "" Then dblL03 = Rs_Report.Tables(0).Rows(i).Item("L03").ToString.Trim
                        If Rs_Report.Tables(0).Rows(i).Item("L03_ADD").ToString.Trim <> "" Then dblL03_ADD = Rs_Report.Tables(0).Rows(i).Item("L03_ADD").ToString.Trim
                        dblConsumedLeave(intCounter) = dblL03
                        dblAccruedLeave(intCounter) = dblL03_ADD
                    Case "L04"
                        dblL04 = 0
                        dblL04_ADD = 0
                        If Rs_Report.Tables(0).Rows(i).Item("L04").ToString.Trim <> "" Then dblL04 = Rs_Report.Tables(0).Rows(i).Item("L04").ToString.Trim
                        If Rs_Report.Tables(0).Rows(i).Item("L04_ADD").ToString.Trim <> "" Then dblL04_ADD = Rs_Report.Tables(0).Rows(i).Item("L04_ADD").ToString.Trim
                        dblConsumedLeave(intCounter) = dblL04
                        dblAccruedLeave(intCounter) = dblL04_ADD
                    Case "L05"
                        dblL05 = 0
                        dblL05_ADD = 0
                        If Rs_Report.Tables(0).Rows(i).Item("L05").ToString.Trim <> "" Then dblL05 = Rs_Report.Tables(0).Rows(i).Item("L05").ToString.Trim
                        If Rs_Report.Tables(0).Rows(i).Item("L05_ADD").ToString.Trim <> "" Then dblL05_ADD = Rs_Report.Tables(0).Rows(i).Item("L05_ADD").ToString.Trim
                        dblConsumedLeave(intCounter) = dblL05
                        dblAccruedLeave(intCounter) = dblL05_ADD
                    Case "L06"
                        dblL06 = 0
                        dblL06_ADD = 0
                        If Rs_Report.Tables(0).Rows(i).Item("L06").ToString.Trim <> "" Then dblL06 = Rs_Report.Tables(0).Rows(i).Item("L06").ToString.Trim
                        If Rs_Report.Tables(0).Rows(i).Item("L06_ADD").ToString.Trim <> "" Then dblL06_ADD = Rs_Report.Tables(0).Rows(i).Item("L06_ADD").ToString.Trim
                        dblConsumedLeave(intCounter) = dblL06
                        dblAccruedLeave(intCounter) = dblL06_ADD
                    Case "L07"
                        dblL07 = 0
                        dblL07_ADD = 0
                        If Rs_Report.Tables(0).Rows(i).Item("L07").ToString.Trim <> "" Then dblL07 = Rs_Report.Tables(0).Rows(i).Item("L07").ToString.Trim
                        If Not Rs_Report.Tables(0).Rows(i).Item("L07_ADD").ToString.Trim <> "" Then dblL07_ADD = Rs_Report.Tables(0).Rows(i).Item("L07_ADD").ToString.Trim
                        dblConsumedLeave(intCounter) = dblL07
                        dblAccruedLeave(intCounter) = dblL07_ADD
                    Case "L08"
                        dblL08 = 0
                        dblL08_ADD = 0
                        If Rs_Report.Tables(0).Rows(i).Item("L08").ToString.Trim <> "" Then dblL08 = Rs_Report.Tables(0).Rows(i).Item("L08").ToString.Trim
                        If Rs_Report.Tables(0).Rows(i).Item("L08_ADD").ToString.Trim <> "" Then dblL08_ADD = Rs_Report.Tables(0).Rows(i).Item("L08_ADD").ToString.Trim
                        dblConsumedLeave(intCounter) = dblL08
                        dblAccruedLeave(intCounter) = dblL08_ADD
                    Case "L09"
                        dblL09 = 0
                        dblL09_ADD = 0
                        If Rs_Report.Tables(0).Rows(i).Item("L09").ToString.Trim <> "" Then dblL09 = Rs_Report.Tables(0).Rows(i).Item("L09").ToString.Trim
                        If Rs_Report.Tables(0).Rows(i).Item("L09_ADD").ToString.Trim <> "" Then dblL09_ADD = Rs_Report.Tables(0).Rows(i).Item("L09_ADD").ToString.Trim
                        dblConsumedLeave(intCounter) = dblL09
                        dblAccruedLeave(intCounter) = dblL09_ADD
                    Case "L10"
                        dblL10 = 0
                        dblL10_ADD = 0
                        If Not Rs_Report.Tables(0).Rows(i).Item("L10").ToString.Trim <> "" Then dblL10 = Rs_Report.Tables(0).Rows(i).Item("L10").ToString.Trim
                        If Not Rs_Report.Tables(0).Rows(i).Item("L10_ADD").ToString.Trim <> "" Then dblL10_ADD = Rs_Report.Tables(0).Rows(i).Item("L10_ADD").ToString.Trim
                        dblConsumedLeave(intCounter) = dblL10
                        dblAccruedLeave(intCounter) = dblL10_ADD
                    Case "L11"
                        dblL11 = 0
                        dblL11_ADD = 0
                        If Rs_Report.Tables(0).Rows(i).Item("L11").ToString.Trim <> "" Then dblL11 = Rs_Report.Tables(0).Rows(i).Item("L11").ToString.Trim
                        If Rs_Report.Tables(0).Rows(i).Item("L11_ADD").ToString.Trim <> "" Then dblL11_ADD = Rs_Report.Tables(0).Rows(i).Item("L11_ADD").ToString.Trim
                        dblConsumedLeave(intCounter) = dblL11
                        dblAccruedLeave(intCounter) = dblL11_ADD
                    Case "L12"
                        dblL12 = 0
                        dblL12_ADD = 0
                        If Rs_Report.Tables(0).Rows(i).Item("L12").ToString.Trim <> "" Then dblL12 = Rs_Report.Tables(0).Rows(i).Item("L12").ToString.Trim
                        If Rs_Report.Tables(0).Rows(i).Item("L12_ADD").ToString.Trim <> "" Then dblL12_ADD = Rs_Report.Tables(0).Rows(i).Item("L12_ADD").ToString.Trim
                        dblConsumedLeave(intCounter) = dblL12
                        dblAccruedLeave(intCounter) = dblL12_ADD
                    Case "L13"
                        dblL13 = 0
                        dblL13_ADD = 0
                        If Rs_Report.Tables(0).Rows(i).Item("L13").ToString.Trim <> "" Then dblL13 = Rs_Report.Tables(0).Rows(i).Item("L13").ToString.Trim
                        If Rs_Report.Tables(0).Rows(i).Item("L13_ADD").ToString.Trim <> "" Then dblL13_ADD = Rs_Report.Tables(0).Rows(i).Item("L13_ADD").ToString.Trim
                        dblConsumedLeave(intCounter) = dblL13
                        dblAccruedLeave(intCounter) = dblL13_ADD
                    Case "L14"
                        dblL14 = 0
                        dblL14_ADD = 0
                        If Rs_Report.Tables(0).Rows(i).Item("L14").ToString.Trim <> "" Then dblL14 = Rs_Report.Tables(0).Rows(i).Item("L14").ToString.Trim
                        If Rs_Report.Tables(0).Rows(i).Item("L14_ADD").ToString.Trim <> "" Then dblL14_ADD = Rs_Report.Tables(0).Rows(i).Item("L14_ADD").ToString.Trim
                        dblConsumedLeave(intCounter) = dblL14
                        dblAccruedLeave(intCounter) = dblL14_ADD
                    Case "L15"
                        dblL15 = 0
                        dblL15_ADD = 0
                        If Rs_Report.Tables(0).Rows(i).Item("L15").ToString.Trim <> "" Then dblL15 = Rs_Report.Tables(0).Rows(i).Item("L15").ToString.Trim
                        If Rs_Report.Tables(0).Rows(i).Item("L15_ADD").ToString.Trim <> "" Then dblL15_ADD = Rs_Report.Tables(0).Rows(i).Item("L15_ADD").ToString.Trim
                        dblConsumedLeave(intCounter) = dblL15
                        dblAccruedLeave(intCounter) = dblL15_ADD
                    Case "L16"
                        dblL16 = 0
                        dblL16_ADD = 0
                        If Rs_Report.Tables(0).Rows(i).Item("L16").ToString.Trim <> "" Then dblL16 = Rs_Report.Tables(0).Rows(i).Item("L16").ToString.Trim
                        If Rs_Report.Tables(0).Rows(i).Item("L16_ADD").ToString.Trim <> "" Then dblL16_ADD = Rs_Report.Tables(0).Rows(i).Item("L16_ADD").ToString.Trim
                        dblConsumedLeave(intCounter) = dblL16
                        dblAccruedLeave(intCounter) = dblL16_ADD
                    Case "L17"
                        dblL17 = 0
                        dblL17_ADD = 0
                        If Rs_Report.Tables(0).Rows(i).Item("L17").ToString.Trim <> "" Then dblL17 = Rs_Report.Tables(0).Rows(i).Item("L17").ToString.Trim
                        If Rs_Report.Tables(0).Rows(i).Item("L17_ADD").ToString.Trim <> "" Then dblL17_ADD = Rs_Report.Tables(0).Rows(i).Item("L17_ADD").ToString.Trim
                        dblConsumedLeave(intCounter) = dblL17
                        dblAccruedLeave(intCounter) = dblL17_ADD
                    Case "L18"
                        dblL18 = 0
                        dblL18_ADD = 0
                        If Rs_Report.Tables(0).Rows(i).Item("L18").ToString.Trim <> "" Then dblL18 = Rs_Report.Tables(0).Rows(i).Item("L18").ToString.Trim
                        If Rs_Report.Tables(0).Rows(i).Item("L18_ADD").ToString.Trim <> "" Then dblL18_ADD = Rs_Report.Tables(0).Rows(i).Item("L18_ADD").ToString.Trim
                        dblConsumedLeave(intCounter) = dblL18
                        dblAccruedLeave(intCounter) = dblL18_ADD
                    Case "L19"
                        dblL19 = 0
                        dblL19_ADD = 0
                        If Rs_Report.Tables(0).Rows(i).Item("L19").ToString.Trim <> "" Then dblL19 = Rs_Report.Tables(0).Rows(i).Item("L19").ToString.Trim
                        If Rs_Report.Tables(0).Rows(i).Item("L19_ADD").ToString.Trim <> "" Then dblL19_ADD = Rs_Report.Tables(0).Rows(i).Item("L19_ADD").ToString.Trim
                        dblConsumedLeave(intCounter) = dblL19
                        dblAccruedLeave(intCounter) = dblL19_ADD
                    Case "L20"
                        dblL20 = 0
                        dblL20_ADD = 0
                        If Rs_Report.Tables(0).Rows(i).Item("L20").ToString.Trim <> "" Then dblL20 = Rs_Report.Tables(0).Rows(i).Item("L20").ToString.Trim
                        If Rs_Report.Tables(0).Rows(i).Item("L20_ADD").ToString.Trim <> "" Then dblL20_ADD = Rs_Report.Tables(0).Rows(i).Item("L20_ADD").ToString.Trim
                        dblConsumedLeave(intCounter) = dblL20
                        dblAccruedLeave(intCounter) = dblL20_ADD
                End Select
                intCounter = intCounter + 1
            End While
            intCounter1 = 1
            intCounter1Temp = 0
            While intCounter1 <= intRecCountLeaveMaster
                If dblAccruedLeave(intCounter1) <> 0 Then
                    intCounter1Temp = intCounter1Temp + 1
                End If
                intCounter1 = intCounter1 + 1
            End While

            rowcnt = rowcnt + 1

            Common.tbl.Rows.Add("Employee's Card No   ", Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim)
            Common.tbl.Rows.Add("Employee's PayRoll", "Code & Name : ", Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim, Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim)
            Common.tbl.Rows.Add("Department Code  ", " & Name      : ", Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim & Space(2), Rs_Report.Tables(0).Rows(i).Item("DepartmentName").ToString.Trim)
            Common.tbl.Rows.Add("Leave Type", "", "", "Leave Accrued", "", "Leave Consumed", "", "Leave Balance")

            'xlapp.Cells(rowcnt, 2) = "Employee's Card No                      : " & Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim
            'rowcnt = rowcnt + 1
            'xlapp.Cells(rowcnt, 2) = "Employee's PayRoll Code & Name : " & Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim & Space(2) & Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim
            'rowcnt = rowcnt + 1
            'xlapp.Cells(rowcnt, 2) = "Department Code & Name             : " & Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim & Space(2) & Rs_Report.Tables(0).Rows(i).Item("DepartmentName").ToString.Trim
            'rowcnt = rowcnt + 1
            'xlapp.Cells(rowcnt, 2) = "Leave Type"
            'xlapp.Cells(rowcnt, 5) = "Leave Accrued"
            'xlapp.Cells(rowcnt, 7) = "Leave Consumed"
            'xlapp.Cells(rowcnt, 9) = "Leave Balance"
            rowcnt = rowcnt + 1


            mintLine = 13
            mblnCheckReport = True
            intCounter1 = 1
            dblConsumedLeaveTotal = 0
            dblAccruedLeaveTotal = 0
            dblLeaveBalanceTotal = 0
            While intCounter1 <= intRecCountLeaveMaster
                If dblAccruedLeave(intCounter1) <> 0 Then
                    rowcnt = rowcnt + 1
                    'xlapp.Cells(rowcnt, 2) = strLeaveDescription50(intCounter1)
                    'xlapp.Cells(rowcnt, 5) = (dblAccruedLeave(intCounter1))
                    'xlapp.Cells(rowcnt, 7) = (dblConsumedLeave(intCounter1))
                    'xlapp.Cells(rowcnt, 9) = (dblAccruedLeave(intCounter1) - dblConsumedLeave(intCounter1))
                    rowcnt = rowcnt + 1

                    Common.tbl.Rows.Add(strLeaveDescription50(intCounter1), "", "", dblAccruedLeave(intCounter1), dblAccruedLeave(intCounter1) - dblConsumedLeave(intCounter1))

                    dblConsumedLeaveTotal = dblConsumedLeaveTotal + dblConsumedLeave(intCounter1)
                    dblAccruedLeaveTotal = dblAccruedLeaveTotal + dblAccruedLeave(intCounter1)
                    dblLeaveBalanceTotal = dblLeaveBalanceTotal + (dblAccruedLeave(intCounter1) - dblConsumedLeave(intCounter1))
                End If
                intCounter1 = intCounter1 + 1
            End While
            rowcnt = rowcnt + 1

            Common.tbl.Rows.Add("Total : ", "", "", dblAccruedLeaveTotal, "", dblConsumedLeaveTotal, "", dblLeaveBalanceTotal)
            'xlapp.Cells(rowcnt, 2) = "Total :"
            'xlapp.Cells(rowcnt, 5) = (dblAccruedLeaveTotal)
            'xlapp.Cells(rowcnt, 7) = (dblConsumedLeaveTotal)
            'xlapp.Cells(rowcnt, 9) = (dblLeaveBalanceTotal)
            mintLine = mintLine + intCounter1Temp + 3

        Next
        Me.Cursor = Cursors.Default
        XtraReportGrid.ShowDialog()
    End Sub
    Sub Monthly_AccruedLeave(ByVal strsortorder As String)
        Dim strDeptDivCode As String
        Dim strCurrentDeptDivCode As String
        Dim strsql As String
        Dim strSqlLeaveMaster As String
        Dim rs_report_ReportLeaveMaster As DataSet ' ADODB.Recordset
        Dim strLeaveField3() As String '* 3
        Dim strLeaveDescription50() As String '* 50
        Dim intRecCountLeaveMaster As Integer
        Dim intCounter As Integer
        Dim intFile As Integer
        Dim strLeaveWholeLine(20) As String '* 10
        Dim blnLeaveCheck1 As Boolean
        Dim blnLeaveCheck2 As Boolean
        Dim blnLeaveCheck3 As Boolean
        Dim blnLeaveCheck4 As Boolean
        Dim blnLeaveCheck5 As Boolean
        Dim blnLeaveCheck6 As Boolean
        Dim blnLeaveCheck7 As Boolean
        Dim blnLeaveCheck8 As Boolean
        Dim blnLeaveCheck9 As Boolean
        Dim blnLeaveCheck10 As Boolean
        Dim blnLeaveCheck11 As Boolean
        Dim blnLeaveCheck12 As Boolean
        Dim blnLeaveCheck13 As Boolean
        Dim blnLeaveCheck14 As Boolean
        Dim blnLeaveCheck15 As Boolean
        Dim blnLeaveCheck16 As Boolean
        Dim blnLeaveCheck17 As Boolean
        Dim blnLeaveCheck18 As Boolean
        Dim blnLeaveCheck19 As Boolean
        Dim blnLeaveCheck20 As Boolean
        Dim mFirst As Boolean
        Dim msrl As Integer
        msrl = 1

        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If
        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet

        mblnCheckReport = False
        mFirst = True
        mintPageNo = 1
        mintLine = 1

        If Common.servername = "Access" Then
            strsql = "select tblCatagory.CatagoryName, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tblLeaveLedger.PayCode,tblLeaveLedger.L01_ADD,tblLeaveLedger.L02_ADD,tblLeaveLedger.L03_ADD,tblLeaveLedger.L04_ADD,tblLeaveLedger.L05_ADD,tblLeaveLedger.L06_ADD,tblLeaveLedger.L07_ADD,tblLeaveLedger.L08_ADD,tblLeaveLedger.L09_ADD,tblLeaveLedger.L10_ADD,tblLeaveLedger.L11_ADD,tblLeaveLedger.L12_ADD,tblLeaveLedger.L13_ADD,tblLeaveLedger.L14_ADD,tblLeaveLedger.L15_ADD,tblLeaveLedger.L16_ADD,tblLeaveLedger.L17_ADD,tblLeaveLedger.L18_ADD,tblLeaveLedger.L19_ADD,tblLeaveLedger.L20_ADD," & _
            "tblEmployee.EmpName,tblEmployee.PresentCardNo,tblDepartment.DepartmentName,tblEmployee.DepartmentCode" & _
            " from tblCatagory, tblLeaveLedger,tblEmployee,tblCompany,tblDepartment" & _
            " Where tblCatagory.Cat = tblEmployee.Cat And tblLeaveLedger.PayCode = tblEmployee.PayCode" & _
             " And tblLeaveLedger.LYEAR = " & TextEdit2.Text.Trim & " " & _
            " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
            " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause)
            adapA = New OleDbDataAdapter(strsql, Common.con1)
            adapA.Fill(Rs_Report)
        Else
            strsql = "select tblCatagory.CatagoryName,tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tblLeaveLedger.PayCode,tblLeaveLedger.L01_ADD,tblLeaveLedger.L02_ADD,tblLeaveLedger.L03_ADD,tblLeaveLedger.L04_ADD,tblLeaveLedger.L05_ADD,tblLeaveLedger.L06_ADD,tblLeaveLedger.L07_ADD,tblLeaveLedger.L08_ADD,tblLeaveLedger.L09_ADD,tblLeaveLedger.L10_ADD,tblLeaveLedger.L11_ADD,tblLeaveLedger.L12_ADD,tblLeaveLedger.L13_ADD,tblLeaveLedger.L14_ADD,tblLeaveLedger.L15_ADD,tblLeaveLedger.L16_ADD,tblLeaveLedger.L17_ADD,tblLeaveLedger.L18_ADD,tblLeaveLedger.L19_ADD,tblLeaveLedger.L20_ADD," & _
            "tblEmployee.EmpName,tblEmployee.PresentCardNo,tblDepartment.DepartmentName,tblEmployee.DepartmentCode" & _
            " from tblCatagory, tblLeaveLedger,tblEmployee,tblCompany,tblDepartment" & _
            " Where tblCatagory.Cat = tblEmployee.Cat  And tblLeaveLedger.PayCode = tblEmployee.PayCode" & _
            " And tblLeaveLedger.LYEAR = '" & TextEdit2.Text.Trim & "'" & _
            " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
            " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause)
            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(Rs_Report)
        End If
        If Rs_Report.Tables(0).Rows.Count < 1 Then
            XtraMessageBox.Show(ulf, "<size=10>No Data Available For this Report.</size>", "<size=9>iAS</size>")
            Exit Sub
        End If

        strSqlLeaveMaster = " Select LeaveField,LeaveDescription from tblLeaveMaster order by LeaveField"
        'rs_report_ReportLeaveMaster = Cn.Execute(strSqlLeaveMaster)
        'rs_report_ReportLeaveMaster.Sort = "LeaveField"
        rs_report_ReportLeaveMaster = New DataSet
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(strSqlLeaveMaster, Common.con1)
            adapA.Fill(rs_report_ReportLeaveMaster)
        Else
            adap = New SqlDataAdapter(strSqlLeaveMaster, Common.con)
            adap.Fill(rs_report_ReportLeaveMaster)
        End If
        For i As Integer = 0 To rs_report_ReportLeaveMaster.Tables(0).Rows.Count - 1 ' While Not rs_report_ReportLeaveMaster.EOF
            intRecCountLeaveMaster = intRecCountLeaveMaster + 1
            'rs_report_ReportLeaveMaster.MoveNext()
        Next
        'End While
        ReDim strLeaveField3(intRecCountLeaveMaster)
        ReDim strLeaveDescription50(intRecCountLeaveMaster)
        'rs_report_ReportLeaveMaster.MoveFirst()
        For i As Integer = 0 To rs_report_ReportLeaveMaster.Tables(0).Rows.Count - 1 ' While Not rs_report_ReportLeaveMaster.EOF
            intCounter = intCounter + 1
            strLeaveField3(intCounter) = rs_report_ReportLeaveMaster.Tables(0).Rows(i).Item("leavefield").ToString.Trim
            strLeaveDescription50(intCounter) = rs_report_ReportLeaveMaster.Tables(0).Rows(i).Item("LeaveDescription").ToString.Trim
            'rs_report_ReportLeaveMaster.MoveNext()
        Next
        'End While

        intFile = FreeFile()
        mFileNumber = FreeFile()
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".SRP"
        Dim objWriter As New System.IO.StreamWriter(mstrFile_Name, True)
        'With Rs_Report
        'Do While Not .EOF
        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            If mFirst Or mintLine > g_LinesPerPage Or (strsortorder.Trim = "Department" And strDeptDivCode <> Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim And g_SkipAfterDept) Then
1:              If Not mFirst Then
                    objWriter.WriteLine(Space(2) & vbFormFeed)
                Else
                    mFirst = False
                End If
                objWriter.WriteLine(Space(2) & Space(40) & CommonReport.g_CompanyNames)
                objWriter.WriteLine(Space(2) & "")
                objWriter.WriteLine(Space(2) & "Page No." & mintPageNo)
                objWriter.WriteLine(Space(2) & "                                                                                                                                                            Run Date & Time : " & Common.runningDateTime) 'Now.ToString("dd/MM/yyyy HH:mm"))
                If Common.IsNepali = "Y" Then
                    objWriter.WriteLine(Space(2) & Space(40) & "Accrued Leaves Report for the Year " & tmpNepali)
                Else
                    objWriter.WriteLine(Space(2) & Space(40) & "Accrued Leaves Report for the Year " & TextEdit2.Text)
                End If
                objWriter.WriteLine(Space(2) & "-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------")
                objWriter.WriteLine(Space(2) & "Srl  Payroll    Employee Name              ------------------------------------------------------------------------------Accrued-----------------------------------------------------------------------------------------------------")
                objWriter.WriteLine(Space(2) & "No.                                             L01      L02      L03      L04      L05      L06      L07      L08      L09      L10      L11      L12      L13      L14      L15      L16      L17      L18      L19      L20")
                objWriter.WriteLine(Space(2) & "-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------")
                mintLine = 9
                mintPageNo = mintPageNo + 1
            End If

            intCounter = 1
            blnLeaveCheck1 = True
            blnLeaveCheck2 = True
            blnLeaveCheck3 = True
            blnLeaveCheck4 = True
            blnLeaveCheck5 = True
            blnLeaveCheck6 = True
            blnLeaveCheck7 = True
            blnLeaveCheck8 = True
            blnLeaveCheck9 = True
            blnLeaveCheck10 = True
            blnLeaveCheck11 = True
            blnLeaveCheck12 = True
            blnLeaveCheck13 = True
            blnLeaveCheck14 = True
            blnLeaveCheck15 = True
            blnLeaveCheck16 = True
            blnLeaveCheck17 = True
            blnLeaveCheck18 = True
            blnLeaveCheck19 = True
            blnLeaveCheck20 = True
            While intCounter <= intRecCountLeaveMaster
                If blnLeaveCheck1 Then
                    If strLeaveField3(intCounter) = "L01" And Rs_Report.Tables(0).Rows(i).Item("L01_ADD").ToString.Trim <> "" Then
                        strLeaveWholeLine(1) = Rs_Report.Tables(0).Rows(i).Item("L01_ADD").ToString.Trim
                        blnLeaveCheck1 = False
                    Else
                        strLeaveWholeLine(1) = ""
                    End If
                End If
                If blnLeaveCheck2 Then
                    If strLeaveField3(intCounter) = "L02" And Rs_Report.Tables(0).Rows(i).Item("L02_ADD").ToString.Trim <> "" Then
                        strLeaveWholeLine(2) = CStr(Rs_Report.Tables(0).Rows(i).Item("L02_ADD").ToString.Trim)
                        blnLeaveCheck2 = False
                    Else
                        strLeaveWholeLine(2) = ""
                    End If
                End If
                If blnLeaveCheck3 Then
                    If strLeaveField3(intCounter) = "L03" And Rs_Report.Tables(0).Rows(i).Item("L03_ADD").ToString.Trim <> "" Then
                        strLeaveWholeLine(3) = Rs_Report.Tables(0).Rows(i).Item("L03_ADD").ToString.Trim
                        blnLeaveCheck3 = False
                    Else
                        strLeaveWholeLine(3) = ""
                    End If
                End If
                If blnLeaveCheck4 Then
                    If strLeaveField3(intCounter) = "L04" And Rs_Report.Tables(0).Rows(i).Item("L04_ADD").ToString.Trim <> "" Then
                        strLeaveWholeLine(4) = Rs_Report.Tables(0).Rows(i).Item("L04_ADD").ToString.Trim
                        blnLeaveCheck4 = False
                    Else
                        strLeaveWholeLine(4) = ""
                    End If
                End If
                If blnLeaveCheck5 Then
                    If strLeaveField3(intCounter) = "L05" And Rs_Report.Tables(0).Rows(i).Item("L05_ADD").ToString.Trim <> "" Then
                        strLeaveWholeLine(5) = Rs_Report.Tables(0).Rows(i).Item("L05_ADD").ToString.Trim
                        blnLeaveCheck5 = False
                    Else
                        strLeaveWholeLine(5) = ""
                    End If
                End If
                If blnLeaveCheck6 Then
                    If strLeaveField3(intCounter) = "L06" And Rs_Report.Tables(0).Rows(i).Item("L06_ADD").ToString.Trim <> "" Then
                        strLeaveWholeLine(6) = Rs_Report.Tables(0).Rows(i).Item("L06_ADD").ToString.Trim
                        blnLeaveCheck6 = False
                    Else
                        strLeaveWholeLine(6) = ""
                    End If
                End If
                If blnLeaveCheck7 Then
                    If strLeaveField3(intCounter) = "L07" And Rs_Report.Tables(0).Rows(i).Item("L07_ADD").ToString.Trim <> "" Then
                        strLeaveWholeLine(7) = Rs_Report.Tables(0).Rows(i).Item("L07_ADD").ToString.Trim
                        blnLeaveCheck7 = False
                    Else
                        strLeaveWholeLine(7) = ""
                    End If
                End If
                If blnLeaveCheck8 Then
                    If strLeaveField3(intCounter) = "L08" And Rs_Report.Tables(0).Rows(i).Item("L08_ADD").ToString.Trim <> "" Then
                        strLeaveWholeLine(8) = Rs_Report.Tables(0).Rows(i).Item("L08_ADD").ToString.Trim
                        blnLeaveCheck8 = False
                    Else
                        strLeaveWholeLine(8) = ""
                    End If
                End If
                If blnLeaveCheck9 Then
                    If strLeaveField3(intCounter) = "L09" And Rs_Report.Tables(0).Rows(i).Item("L09_ADD").ToString.Trim <> "" Then
                        strLeaveWholeLine(9) = Rs_Report.Tables(0).Rows(i).Item("L09_ADD").ToString.Trim
                        blnLeaveCheck9 = False
                    Else
                        strLeaveWholeLine(9) = ""
                    End If
                End If
                If blnLeaveCheck10 Then
                    If strLeaveField3(intCounter) = "L10" And Rs_Report.Tables(0).Rows(i).Item("L10_ADD").ToString.Trim <> "" Then
                        strLeaveWholeLine(10) = Rs_Report.Tables(0).Rows(i).Item("L10_ADD").ToString.Trim
                        blnLeaveCheck10 = False
                    Else
                        strLeaveWholeLine(10) = ""
                    End If
                End If
                If blnLeaveCheck11 Then
                    If strLeaveField3(intCounter) = "L11" And Rs_Report.Tables(0).Rows(i).Item("L11_ADD").ToString.Trim <> "" Then
                        strLeaveWholeLine(11) = Rs_Report.Tables(0).Rows(i).Item("L11_ADD").ToString.Trim
                        blnLeaveCheck11 = False
                    Else
                        strLeaveWholeLine(11) = ""
                    End If
                End If
                If blnLeaveCheck12 Then
                    If strLeaveField3(intCounter) = "L12" And Rs_Report.Tables(0).Rows(i).Item("L12_ADD").ToString.Trim <> "" Then
                        strLeaveWholeLine(12) = Rs_Report.Tables(0).Rows(i).Item("L12_ADD").ToString.Trim
                        blnLeaveCheck12 = False
                    Else
                        strLeaveWholeLine(12) = ""
                    End If
                End If
                If blnLeaveCheck13 Then
                    If strLeaveField3(intCounter) = "L13" And Rs_Report.Tables(0).Rows(i).Item("L13_ADD").ToString.Trim <> "" Then
                        strLeaveWholeLine(13) = Rs_Report.Tables(0).Rows(i).Item("L13_ADD").ToString.Trim
                        blnLeaveCheck13 = False
                    Else
                        strLeaveWholeLine(13) = ""
                    End If
                End If
                If blnLeaveCheck14 Then
                    If strLeaveField3(intCounter) = "L14" And Rs_Report.Tables(0).Rows(i).Item("L14_ADD").ToString.Trim <> "" Then
                        strLeaveWholeLine(14) = Rs_Report.Tables(0).Rows(i).Item("L14_ADD").ToString.Trim
                        blnLeaveCheck14 = False
                    Else
                        strLeaveWholeLine(14) = ""
                    End If
                End If
                If blnLeaveCheck15 Then
                    If strLeaveField3(intCounter) = "L15" And Rs_Report.Tables(0).Rows(i).Item("L15_ADD").ToString.Trim <> "" Then
                        strLeaveWholeLine(15) = Rs_Report.Tables(0).Rows(i).Item("L15_ADD").ToString.Trim
                        blnLeaveCheck15 = False
                    Else
                        strLeaveWholeLine(15) = ""
                    End If
                End If
                If blnLeaveCheck16 Then
                    If strLeaveField3(intCounter) = "L16" And Rs_Report.Tables(0).Rows(i).Item("L16_ADD").ToString.Trim <> "" Then
                        strLeaveWholeLine(16) = Rs_Report.Tables(0).Rows(i).Item("L16_ADD").ToString.Trim
                        blnLeaveCheck16 = False
                    Else
                        strLeaveWholeLine(16) = ""
                    End If
                End If
                If blnLeaveCheck17 Then
                    If strLeaveField3(intCounter) = "L17" And Rs_Report.Tables(0).Rows(i).Item("L17_ADD").ToString.Trim <> "" Then
                        strLeaveWholeLine(17) = Rs_Report.Tables(0).Rows(i).Item("L17_ADD").ToString.Trim
                        blnLeaveCheck17 = False
                    Else
                        strLeaveWholeLine(17) = ""
                    End If
                End If
                If blnLeaveCheck18 Then
                    If strLeaveField3(intCounter) = "L18" And Rs_Report.Tables(0).Rows(i).Item("L18_ADD").ToString.Trim <> "" Then
                        strLeaveWholeLine(18) = Rs_Report.Tables(0).Rows(i).Item("L18_ADD").ToString.Trim
                        blnLeaveCheck18 = False
                    Else
                        strLeaveWholeLine(18) = ""
                    End If
                End If
                If blnLeaveCheck19 Then
                    If strLeaveField3(intCounter) = "L19" And Rs_Report.Tables(0).Rows(i).Item("L19_ADD").ToString.Trim <> "" Then
                        strLeaveWholeLine(19) = Rs_Report.Tables(0).Rows(i).Item("L19_ADD").ToString.Trim
                        blnLeaveCheck19 = False
                    Else
                        strLeaveWholeLine(19) = ""
                    End If
                End If
                If blnLeaveCheck20 Then
                    If strLeaveField3(intCounter) = "L20" And Rs_Report.Tables(0).Rows(i).Item("L20_ADD").ToString.Trim <> "" Then
                        strLeaveWholeLine(20) = Rs_Report.Tables(0).Rows(i).Item("L20_ADD").ToString.Trim
                        blnLeaveCheck20 = False
                    Else
                        strLeaveWholeLine(20) = ""
                    End If
                End If
                intCounter = intCounter + 1
            End While

            objWriter.WriteLine(Space(2) & msrl.ToString.PadRight(5) & " " & Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim.PadRight(12) & Space(1) & Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim.PadRight(25) & Space(1) & _
                Space(3) & strLeaveWholeLine(1).PadRight(6) & Space(3) & strLeaveWholeLine(2).PadRight(6) & Space(3) & strLeaveWholeLine(3).PadRight(6) & Space(3) & strLeaveWholeLine(4).PadRight(6) & Space(3) & _
                Space(3) & strLeaveWholeLine(5).PadRight(6) & Space(3) & strLeaveWholeLine(6).PadRight(6) & Space(3) & strLeaveWholeLine(7).PadRight(6) & Space(3) & strLeaveWholeLine(8).PadRight(6) & Space(3) & _
                Space(3) & strLeaveWholeLine(9).PadRight(6) & Space(3) & strLeaveWholeLine(10).PadRight(6) & Space(3) & strLeaveWholeLine(11).PadRight(6) & Space(3) & strLeaveWholeLine(12).PadRight(6) & Space(3) & _
                Space(3) & strLeaveWholeLine(13).PadRight(6) & Space(3) & strLeaveWholeLine(14).PadRight(6) & Space(3) & strLeaveWholeLine(15).PadRight(6) & Space(3) & strLeaveWholeLine(16).PadRight(6) & Space(3) & _
                Space(3) & strLeaveWholeLine(17).PadRight(6) & Space(3) & strLeaveWholeLine(18).PadRight(6) & Space(3) & strLeaveWholeLine(19).PadRight(6) & Space(3) & strLeaveWholeLine(20).PadRight(6))

            msrl = msrl + 1
            mintLine = mintLine + 1
            mblnCheckReport = True
            '    .MoveNext()
            'Loop
        Next
        intCounter = 1
        objWriter.WriteLine(Space(2) & "")
        objWriter.WriteLine(Space(2) & "")
        While intCounter <= intRecCountLeaveMaster
            objWriter.WriteLine(Space(2) & strLeaveField3(intCounter) & " " & "-" & " " & strLeaveDescription50(intCounter) & Space(4))
            If intCounter Mod 3 = 0 Then objWriter.WriteLine(Space(2) & "")
            intCounter = intCounter + 1
        End While
        objWriter.Close()
        Try
            Process.Start(mstrFile_Name)
        Catch ex As Exception
            Process.Start("notepad.exe", mstrFile_Name)
        End Try      
    End Sub
    Sub MonthlyXl_AccruedLeave(ByVal strsortorder As String)
        'On Error GoTo ErrorGen
        Dim strDeptDivCode As String
        Dim strCurrentDeptDivCode As String
        Dim strsql As String
        Dim strSqlLeaveMaster As String
        Dim rs_report_ReportLeaveMaster As DataSet 'ADODB.Recordset
        Dim strLeaveField3() As String '* 3
        Dim strLeaveDescription50() As String ' * 50
        Dim intRecCountLeaveMaster As Integer
        Dim intCounter As Integer
        Dim intFile As Integer
        Dim strLeaveWholeLine(20) As String '* 10
        Dim blnLeaveCheck1 As Boolean
        Dim blnLeaveCheck2 As Boolean
        Dim blnLeaveCheck3 As Boolean
        Dim blnLeaveCheck4 As Boolean
        Dim blnLeaveCheck5 As Boolean
        Dim blnLeaveCheck6 As Boolean
        Dim blnLeaveCheck7 As Boolean
        Dim blnLeaveCheck8 As Boolean
        Dim blnLeaveCheck9 As Boolean
        Dim blnLeaveCheck10 As Boolean
        Dim blnLeaveCheck11 As Boolean
        Dim blnLeaveCheck12 As Boolean
        Dim blnLeaveCheck13 As Boolean
        Dim blnLeaveCheck14 As Boolean
        Dim blnLeaveCheck15 As Boolean
        Dim blnLeaveCheck16 As Boolean
        Dim blnLeaveCheck17 As Boolean
        Dim blnLeaveCheck18 As Boolean
        Dim blnLeaveCheck19 As Boolean
        Dim blnLeaveCheck20 As Boolean
        Dim blnPrintLine As Boolean
        Dim strDepartmentCode As String
        Dim intPrintLineCounter As Integer
        Dim blnDeptAvailable As Boolean, mFirst As Boolean
        Dim msrl As Integer
        Dim xlapp As Excel.Application
        Dim xlwb As Excel.Workbook
        Dim xlst As Excel.Sheets
        Dim rowcnt As Integer

        xlapp = CreateObject("Excel.Application")
        xlwb = xlapp.Workbooks.Add
        'If UCase(Dir(App.Path & "\TimeWatch.xls")) = UCase("TimeWatch.xls") Then
        '    Kill(App.Path & "\TimeWatch.xls")
        'End If

        mblnCheckReport = False
        mintPageNo = 1
        mintLine = 1
        If XtraShortOrder.g_SortOrder <> "" Then
            strsortorder = XtraShortOrder.g_SortOrder
        End If
        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If
        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".xls"

        If Common.servername = "Access" Then
            strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName,  tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tblLeaveLedger.PayCode,tblLeaveLedger.L01_ADD,tblLeaveLedger.L02_ADD,tblLeaveLedger.L03_ADD,tblLeaveLedger.L04_ADD,tblLeaveLedger.L05_ADD,tblLeaveLedger.L06_ADD,tblLeaveLedger.L07_ADD,tblLeaveLedger.L08_ADD,tblLeaveLedger.L09_ADD,tblLeaveLedger.L10_ADD,tblLeaveLedger.L11_ADD,tblLeaveLedger.L12_ADD,tblLeaveLedger.L13_ADD,tblLeaveLedger.L14_ADD,tblLeaveLedger.L15_ADD,tblLeaveLedger.L16_ADD,tblLeaveLedger.L17_ADD,tblLeaveLedger.L18_ADD,tblLeaveLedger.L19_ADD,tblLeaveLedger.L20_ADD," & _
            "tblEmployee.EmpName,tblEmployee.PresentCardNo,tblDepartment.DepartmentName,tblEmployee.DepartmentCode" & _
            " from tblCatagory,tblDivision,  tblLeaveLedger,tblEmployee,tblCompany,tblDepartment" & _
            " Where tblCatagory.Cat = tblEmployee.Cat And tblDivision.DivisionCode = tblEmployee.DivisionCode And tblLeaveLedger.PayCode = tblEmployee.PayCode" & _
            " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
            " and TBLLEAVELEDGER.LYEAR = " & TextEdit2.Text & " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & strsortorder
            adapA = New OleDbDataAdapter(strsql, Common.con1)
            adapA.Fill(Rs_Report)
        Else
            strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName,  tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tblLeaveLedger.PayCode,tblLeaveLedger.L01_ADD,tblLeaveLedger.L02_ADD,tblLeaveLedger.L03_ADD,tblLeaveLedger.L04_ADD,tblLeaveLedger.L05_ADD,tblLeaveLedger.L06_ADD,tblLeaveLedger.L07_ADD,tblLeaveLedger.L08_ADD,tblLeaveLedger.L09_ADD,tblLeaveLedger.L10_ADD,tblLeaveLedger.L11_ADD,tblLeaveLedger.L12_ADD,tblLeaveLedger.L13_ADD,tblLeaveLedger.L14_ADD,tblLeaveLedger.L15_ADD,tblLeaveLedger.L16_ADD,tblLeaveLedger.L17_ADD,tblLeaveLedger.L18_ADD,tblLeaveLedger.L19_ADD,tblLeaveLedger.L20_ADD," & _
            "tblEmployee.EmpName,tblEmployee.PresentCardNo,tblDepartment.DepartmentName,tblEmployee.DepartmentCode" & _
            " from tblCatagory,tblDivision,  tblLeaveLedger,tblEmployee,tblCompany,tblDepartment" & _
            " Where tblCatagory.Cat = tblEmployee.Cat And tblDivision.DivisionCode = tblEmployee.DivisionCode And tblLeaveLedger.PayCode = tblEmployee.PayCode" & _
            " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
            " and TBLLEAVELEDGER.LYEAR = " & TextEdit2.Text & " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & strsortorder
            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(Rs_Report)
        End If
        If Rs_Report.Tables(0).Rows.Count < 1 Then
            XtraMessageBox.Show(ulf, "<size=10>TNo Data Available For this Report.</size>", "<size=9>iAS</size>")
            Exit Sub
        End If

        strSqlLeaveMaster = " Select LeaveField,LeaveDescription from tblLeaveMaster order by LeaveField"
        rs_report_ReportLeaveMaster = New DataSet
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(strSqlLeaveMaster, Common.con1)
            adapA.Fill(rs_report_ReportLeaveMaster)
        Else
            adap = New SqlDataAdapter(strSqlLeaveMaster, Common.con)
            adap.Fill(rs_report_ReportLeaveMaster)
        End If
        For i As Integer = 0 To rs_report_ReportLeaveMaster.Tables(0).Rows.Count - 1 ' While Not rs_report_ReportLeaveMaster.EOF
            intRecCountLeaveMaster = intRecCountLeaveMaster + 1
            'rs_report_ReportLeaveMaster.MoveNext()
        Next
        'End While
        ReDim strLeaveField3(intRecCountLeaveMaster)
        ReDim strLeaveDescription50(intRecCountLeaveMaster)
        'rs_report_ReportLeaveMaster.MoveFirst()
        For i As Integer = 0 To rs_report_ReportLeaveMaster.Tables(0).Rows.Count - 1 ' While Not rs_report_ReportLeaveMaster.EOF
            intCounter = intCounter + 1
            strLeaveField3(intCounter) = rs_report_ReportLeaveMaster.Tables(0).Rows(i).Item("leavefield").ToString.Trim
            strLeaveDescription50(intCounter) = rs_report_ReportLeaveMaster.Tables(0).Rows(i).Item("LeaveDescription").ToString.Trim
            'rs_report_ReportLeaveMaster.MoveNext()
        Next
        'End While
        'With Rs_Report
        rowcnt = rowcnt + 1
        xlapp.Visible = True
        xlapp.Columns.ColumnWidth = 10
        xlapp.Columns.Font.Name = "Tahoma"
        xlapp.Cells(rowcnt, 5).Font.Bold = True
        xlapp.Cells(rowcnt, 5) = "Company Name:" & CommonReport.g_CompanyNames
        rowcnt = rowcnt + 1
        xlapp.Cells(rowcnt, 5).Font.Bold = True
        xlapp.Cells(rowcnt, 5) = "Run Date & Time :" & Format(Now(), "dd/MM/yyyy HH:MM")
        rowcnt = rowcnt + 2
        xlapp.Cells.Font.Size = 8
        xlapp.Cells(rowcnt, 4).Font.Bold = True
        xlapp.Cells(rowcnt, 4) = " ACCRUAL LEAVE FOR THE YEAR : " & TextEdit2.Text
        rowcnt = rowcnt + 2
        xlapp.Cells(rowcnt, 1).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 1).Font.Bold = True
        xlapp.Cells(rowcnt, 1).Font.ColorIndex = 2 'bWhite
        xlapp.Cells(rowcnt, 1).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 1) = "Srl No."
        xlapp.Cells(rowcnt, 2).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 2).Font.Bold = True
        xlapp.Cells(rowcnt, 2).Font.ColorIndex = 2 ' vbWhite
        xlapp.Cells(rowcnt, 2).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 2) = "Paycode."
        xlapp.Cells(rowcnt, 3).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 3).Font.Bold = True
        xlapp.Cells(rowcnt, 3).Font.ColorIndex = 2 'vbWhite
        xlapp.Cells(rowcnt, 3).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 3) = "Employee Name"
        xlapp.Cells(rowcnt, 4).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 4).Font.Bold = True
        xlapp.Cells(rowcnt, 4).Font.ColorIndex = 2 ' vbWhite
        xlapp.Cells(rowcnt, 4).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 5).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 5).Font.Bold = True
        xlapp.Cells(rowcnt, 5).Font.ColorIndex = 2 ' vbWhite
        xlapp.Cells(rowcnt, 5).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 5) = "L01"
        xlapp.Cells(rowcnt, 6).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 6).Font.Bold = True
        xlapp.Cells(rowcnt, 6).Font.ColorIndex = 2 'vbWhite
        xlapp.Cells(rowcnt, 6).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 6) = "L02"
        xlapp.Cells(rowcnt, 7).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 7).Font.Bold = True
        xlapp.Cells(rowcnt, 7).Font.ColorIndex = 2 'vbWhite
        xlapp.Cells(rowcnt, 7).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 7) = "L03"
        xlapp.Cells(rowcnt, 8).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 8).Font.Bold = True
        xlapp.Cells(rowcnt, 8).Font.ColorIndex = 2 'vbWhite
        xlapp.Cells(rowcnt, 8).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 8) = "L04"
        xlapp.Cells(rowcnt, 9).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 9).Font.Bold = True
        xlapp.Cells(rowcnt, 9).Font.ColorIndex = 2 'vbWhite
        xlapp.Cells(rowcnt, 9).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 9) = "L05"
        xlapp.Cells(rowcnt, 10).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 10).Font.Bold = True
        xlapp.Cells(rowcnt, 10).Font.ColorIndex = 2 ' vbWhite
        xlapp.Cells(rowcnt, 10).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 10) = "L06"
        xlapp.Cells(rowcnt, 11).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 11).Font.Bold = True
        xlapp.Cells(rowcnt, 11).Font.ColorIndex = 2 'vbWhite
        xlapp.Cells(rowcnt, 11).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 11) = "L07"
        xlapp.Cells(rowcnt, 12).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 12).Font.Bold = True
        xlapp.Cells(rowcnt, 12).Font.ColorIndex = 2 'vbWhite
        xlapp.Cells(rowcnt, 12).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 12) = "L08"
        xlapp.Cells(rowcnt, 13).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 13).Font.Bold = True
        xlapp.Cells(rowcnt, 13).Font.ColorIndex = 2 ' vbWhite
        xlapp.Cells(rowcnt, 13).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 13) = "L09"
        xlapp.Cells(rowcnt, 14).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 14).Font.Bold = True
        xlapp.Cells(rowcnt, 14).Font.ColorIndex = 2 'vbWhite
        xlapp.Cells(rowcnt, 14).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 14) = "L10"
        xlapp.Cells(rowcnt, 15).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 15).Font.Bold = True
        xlapp.Cells(rowcnt, 15).Font.ColorIndex = 2 'vbWhite
        xlapp.Cells(rowcnt, 15).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 15) = "L11"
        xlapp.Cells(rowcnt, 16).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 16).Font.Bold = True
        xlapp.Cells(rowcnt, 16).Font.ColorIndex = 2 'vbWhite
        xlapp.Cells(rowcnt, 16).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 16) = "L12"
        xlapp.Cells(rowcnt, 17).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 17).Font.Bold = True
        xlapp.Cells(rowcnt, 17).Font.ColorIndex = 2 'white
        xlapp.Cells(rowcnt, 17).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 17) = "L13"
        xlapp.Cells(rowcnt, 18).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 18).Font.Bold = True
        xlapp.Cells(rowcnt, 18).Font.ColorIndex = 2 ' vbWhite
        xlapp.Cells(rowcnt, 18).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 18) = "L14"
        xlapp.Cells(rowcnt, 19).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 19).Font.Bold = True
        xlapp.Cells(rowcnt, 19).Font.ColorIndex = 2 ' vbWhite
        xlapp.Cells(rowcnt, 19).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 19) = "L15"
        xlapp.Cells(rowcnt, 20).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 20).Font.Bold = True
        xlapp.Cells(rowcnt, 20).Font.ColorIndex = 2 ' vbWhite
        xlapp.Cells(rowcnt, 20).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 20) = "L16"
        xlapp.Cells(rowcnt, 21).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 21).Font.Bold = True
        xlapp.Cells(rowcnt, 21).Font.ColorIndex = 2 ' vbWhite
        xlapp.Cells(rowcnt, 21).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 21) = "L17"
        xlapp.Cells(rowcnt, 22).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 22).Font.Bold = True
        xlapp.Cells(rowcnt, 22).Font.ColorIndex = 2 'vbWhite
        xlapp.Cells(rowcnt, 22).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 22) = "L18"
        xlapp.Cells(rowcnt, 23).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 23).Font.Bold = True
        xlapp.Cells(rowcnt, 23).Font.ColorIndex = 2 ' vbWhite
        xlapp.Cells(rowcnt, 23).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 23) = "L19"
        xlapp.Cells(rowcnt, 24).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 24).Font.Bold = True
        xlapp.Cells(rowcnt, 24).Font.ColorIndex = 2 ' vbWhite
        xlapp.Cells(rowcnt, 24).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 24) = "L20"

        'If blnDeptAvailable And Not .EOF Then
        '    rowcnt = rowcnt + 1
        '    If Left(strsortorder, 10) = "Department" Or frmSorting.optDeptEmpName.Value Then
        '        xlapp.Cells(rowcnt, 1) = "** Department Code & Name : " & !DepartmentCode & "  " & !DepartmentName
        '    ElseIf Left(strsortorder, 1) = "Division" Or frmSorting.optSectionPaycode.Value Then
        '        xlapp.Cells(rowcnt, 1) = "** Section Code & Name : " & !DivisionCode & "  " & !DivisionName
        '    Else
        '        xlapp.Cells(rowcnt, 1) = "** Category Code & Name : " & !Cat & "  " & !CatagoryName
        '    End If
        '    rowcnt = rowcnt + 1
        '    mintLine = mintLine + 3
        'End If

        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            'If Left(strsortorder, 10) = "Department" Then
            '    strDeptDivCode = !DepartmentCode
            'ElseIf Left(strsortorder, 8) = "Division" Then
            '    strDeptDivCode = !DivisionCode
            'Else
            '    strDeptDivCode = !Cat
            'End If
            intCounter = 1
            blnLeaveCheck1 = True
            blnLeaveCheck2 = True
            blnLeaveCheck3 = True
            blnLeaveCheck4 = True
            blnLeaveCheck5 = True
            blnLeaveCheck6 = True
            blnLeaveCheck7 = True
            blnLeaveCheck8 = True
            blnLeaveCheck9 = True
            blnLeaveCheck10 = True
            blnLeaveCheck11 = True
            blnLeaveCheck12 = True
            blnLeaveCheck13 = True
            blnLeaveCheck14 = True
            blnLeaveCheck15 = True
            blnLeaveCheck16 = True
            blnLeaveCheck17 = True
            blnLeaveCheck18 = True
            blnLeaveCheck19 = True
            blnLeaveCheck20 = True
            While intCounter <= intRecCountLeaveMaster
                If blnLeaveCheck1 Then
                    If strLeaveField3(intCounter) = "L01" And Rs_Report.Tables(0).Rows(i).Item("L01_ADD").ToString.Trim <> "" Then
                        strLeaveWholeLine(1) = Rs_Report.Tables(0).Rows(i).Item("L01_ADD").ToString.Trim
                        blnLeaveCheck1 = False
                    Else
                        strLeaveWholeLine(1) = ""
                    End If
                End If
                If blnLeaveCheck2 Then
                    If strLeaveField3(intCounter) = "L02" And Rs_Report.Tables(0).Rows(i).Item("L02_ADD").ToString.Trim <> "" Then
                        strLeaveWholeLine(2) = CStr(Rs_Report.Tables(0).Rows(i).Item("L02_ADD").ToString.Trim)
                        blnLeaveCheck2 = False
                    Else
                        strLeaveWholeLine(2) = ""
                    End If
                End If
                If blnLeaveCheck3 Then
                    If strLeaveField3(intCounter) = "L03" And Rs_Report.Tables(0).Rows(i).Item("L03_ADD").ToString.Trim <> "" Then
                        strLeaveWholeLine(3) = Rs_Report.Tables(0).Rows(i).Item("L03_ADD").ToString.Trim
                        blnLeaveCheck3 = False
                    Else
                        strLeaveWholeLine(3) = ""
                    End If
                End If
                If blnLeaveCheck4 Then
                    If strLeaveField3(intCounter) = "L04" And Rs_Report.Tables(0).Rows(i).Item("L04_ADD").ToString.Trim <> "" Then
                        strLeaveWholeLine(4) = Rs_Report.Tables(0).Rows(i).Item("L04_ADD").ToString.Trim
                        blnLeaveCheck4 = False
                    Else
                        strLeaveWholeLine(4) = ""
                    End If
                End If
                If blnLeaveCheck5 Then
                    If strLeaveField3(intCounter) = "L05" And Rs_Report.Tables(0).Rows(i).Item("L05_ADD").ToString.Trim <> "" Then
                        strLeaveWholeLine(5) = Rs_Report.Tables(0).Rows(i).Item("L05_ADD").ToString.Trim
                        blnLeaveCheck5 = False
                    Else
                        strLeaveWholeLine(5) = ""
                    End If
                End If
                If blnLeaveCheck6 Then
                    If strLeaveField3(intCounter) = "L06" And Rs_Report.Tables(0).Rows(i).Item("L06_ADD").ToString.Trim <> "" Then
                        strLeaveWholeLine(6) = Rs_Report.Tables(0).Rows(i).Item("L06_ADD").ToString.Trim
                        blnLeaveCheck6 = False
                    Else
                        strLeaveWholeLine(6) = ""
                    End If
                End If
                If blnLeaveCheck7 Then
                    If strLeaveField3(intCounter) = "L07" And Rs_Report.Tables(0).Rows(i).Item("L07_ADD").ToString.Trim <> "" Then
                        strLeaveWholeLine(7) = Rs_Report.Tables(0).Rows(i).Item("L07_ADD").ToString.Trim
                        blnLeaveCheck7 = False
                    Else
                        strLeaveWholeLine(7) = ""
                    End If
                End If
                If blnLeaveCheck8 Then
                    If strLeaveField3(intCounter) = "L08" And Rs_Report.Tables(0).Rows(i).Item("L08_ADD").ToString.Trim <> "" Then
                        strLeaveWholeLine(8) = Rs_Report.Tables(0).Rows(i).Item("L08_ADD").ToString.Trim
                        blnLeaveCheck8 = False
                    Else
                        strLeaveWholeLine(8) = ""
                    End If
                End If
                If blnLeaveCheck9 Then
                    If strLeaveField3(intCounter) = "L09" And Rs_Report.Tables(0).Rows(i).Item("L09_ADD").ToString.Trim <> "" Then
                        strLeaveWholeLine(9) = Rs_Report.Tables(0).Rows(i).Item("L09_ADD").ToString.Trim
                        blnLeaveCheck9 = False
                    Else
                        strLeaveWholeLine(9) = ""
                    End If
                End If
                If blnLeaveCheck10 Then
                    If strLeaveField3(intCounter) = "L10" And Rs_Report.Tables(0).Rows(i).Item("L10_ADD").ToString.Trim <> "" Then
                        strLeaveWholeLine(10) = Rs_Report.Tables(0).Rows(i).Item("L10_ADD").ToString.Trim
                        blnLeaveCheck10 = False
                    Else
                        strLeaveWholeLine(10) = ""
                    End If
                End If
                If blnLeaveCheck11 Then
                    If strLeaveField3(intCounter) = "L11" And Rs_Report.Tables(0).Rows(i).Item("L11_ADD").ToString.Trim <> "" Then
                        strLeaveWholeLine(11) = Rs_Report.Tables(0).Rows(i).Item("L11_ADD").ToString.Trim
                        blnLeaveCheck11 = False
                    Else
                        strLeaveWholeLine(11) = ""
                    End If
                End If
                If blnLeaveCheck12 Then
                    If strLeaveField3(intCounter) = "L12" And Rs_Report.Tables(0).Rows(i).Item("L12_ADD").ToString.Trim <> "" Then
                        strLeaveWholeLine(12) = Rs_Report.Tables(0).Rows(i).Item("L12_ADD").ToString.Trim
                        blnLeaveCheck12 = False
                    Else
                        strLeaveWholeLine(12) = ""
                    End If
                End If
                If blnLeaveCheck13 Then
                    If strLeaveField3(intCounter) = "L13" And Rs_Report.Tables(0).Rows(i).Item("L13_ADD").ToString.Trim <> "" Then
                        strLeaveWholeLine(13) = Rs_Report.Tables(0).Rows(i).Item("L13_ADD").ToString.Trim
                        blnLeaveCheck13 = False
                    Else
                        strLeaveWholeLine(13) = ""
                    End If
                End If
                If blnLeaveCheck14 Then
                    If strLeaveField3(intCounter) = "L14" And Rs_Report.Tables(0).Rows(i).Item("L14_ADD").ToString.Trim <> "" Then
                        strLeaveWholeLine(14) = Rs_Report.Tables(0).Rows(i).Item("L14_ADD").ToString.Trim
                        blnLeaveCheck14 = False
                    Else
                        strLeaveWholeLine(14) = ""
                    End If
                End If
                If blnLeaveCheck15 Then
                    If strLeaveField3(intCounter) = "L15" And Rs_Report.Tables(0).Rows(i).Item("L15_ADD").ToString.Trim <> "" Then
                        strLeaveWholeLine(15) = Rs_Report.Tables(0).Rows(i).Item("L15_ADD").ToString.Trim
                        blnLeaveCheck15 = False
                    Else
                        strLeaveWholeLine(15) = ""
                    End If
                End If
                If blnLeaveCheck16 Then
                    If strLeaveField3(intCounter) = "L16" And Rs_Report.Tables(0).Rows(i).Item("L16_ADD").ToString.Trim <> "" Then
                        strLeaveWholeLine(16) = Rs_Report.Tables(0).Rows(i).Item("L16_ADD").ToString.Trim
                        blnLeaveCheck16 = False
                    Else
                        strLeaveWholeLine(16) = ""
                    End If
                End If
                If blnLeaveCheck17 Then
                    If strLeaveField3(intCounter) = "L17" And Rs_Report.Tables(0).Rows(i).Item("L17_ADD").ToString.Trim <> "" Then
                        strLeaveWholeLine(17) = Rs_Report.Tables(0).Rows(i).Item("L17_ADD").ToString.Trim
                        blnLeaveCheck17 = False
                    Else
                        strLeaveWholeLine(17) = ""
                    End If
                End If
                If blnLeaveCheck18 Then
                    If strLeaveField3(intCounter) = "L18" And Rs_Report.Tables(0).Rows(i).Item("L18_ADD").ToString.Trim <> "" Then
                        strLeaveWholeLine(18) = Rs_Report.Tables(0).Rows(i).Item("L18_ADD").ToString.Trim
                        blnLeaveCheck18 = False
                    Else
                        strLeaveWholeLine(18) = ""
                    End If
                End If
                If blnLeaveCheck19 Then
                    If strLeaveField3(intCounter) = "L19" And Rs_Report.Tables(0).Rows(i).Item("L19_ADD").ToString.Trim <> "" Then
                        strLeaveWholeLine(19) = Rs_Report.Tables(0).Rows(i).Item("L19_ADD").ToString.Trim
                        blnLeaveCheck19 = False
                    Else
                        strLeaveWholeLine(19) = ""
                    End If
                End If
                If blnLeaveCheck20 Then
                    If strLeaveField3(intCounter) = "L20" And Rs_Report.Tables(0).Rows(i).Item("L20_ADD").ToString.Trim <> "" Then
                        strLeaveWholeLine(20) = Rs_Report.Tables(0).Rows(i).Item("L20_ADD").ToString.Trim
                        blnLeaveCheck20 = False
                    Else
                        strLeaveWholeLine(20) = ""
                    End If
                End If
                intCounter = intCounter + 1
            End While
            rowcnt = rowcnt + 1
            msrl = msrl + 1
            xlapp.Cells(rowcnt, 1).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 1) = "'" & msrl
            xlapp.Cells(rowcnt, 2).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 2) = "'" & Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim
            xlapp.Cells(rowcnt, 3).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 3) = Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim
            xlapp.Cells(rowcnt, 4).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 5).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 5) = strLeaveWholeLine(1)
            xlapp.Cells(rowcnt, 6).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 6) = strLeaveWholeLine(2)
            xlapp.Cells(rowcnt, 7).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 7) = strLeaveWholeLine(3)
            xlapp.Cells(rowcnt, 8).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 8) = strLeaveWholeLine(4)
            xlapp.Cells(rowcnt, 9).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 9) = strLeaveWholeLine(5)
            xlapp.Cells(rowcnt, 10).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 10) = strLeaveWholeLine(6)
            xlapp.Cells(rowcnt, 11).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 11) = strLeaveWholeLine(7)
            xlapp.Cells(rowcnt, 12).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 12) = strLeaveWholeLine(8)
            xlapp.Cells(rowcnt, 13).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 13) = strLeaveWholeLine(9)
            xlapp.Cells(rowcnt, 14).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 14) = strLeaveWholeLine(10)
            xlapp.Cells(rowcnt, 15).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 15) = strLeaveWholeLine(11)
            xlapp.Cells(rowcnt, 16).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 16) = strLeaveWholeLine(12)
            xlapp.Cells(rowcnt, 17).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 17) = strLeaveWholeLine(13)
            xlapp.Cells(rowcnt, 18).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 18) = strLeaveWholeLine(14)
            xlapp.Cells(rowcnt, 19).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 19) = strLeaveWholeLine(15)
            xlapp.Cells(rowcnt, 20).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 20) = strLeaveWholeLine(16)
            xlapp.Cells(rowcnt, 21).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 21) = strLeaveWholeLine(17)
            xlapp.Cells(rowcnt, 22).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 22) = strLeaveWholeLine(18)
            xlapp.Cells(rowcnt, 23).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 23) = strLeaveWholeLine(19)
            xlapp.Cells(rowcnt, 24).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 24) = strLeaveWholeLine(20)

            mintLine = mintLine + 1
            mblnCheckReport = True
            '.MoveNext()
            'If blnDeptAvailable And Not .EOF Then
            '    If Left(strsortorder, 10) = "Department" Then
            '        strCurrentDeptDivCode = !DepartmentCode
            '    ElseIf Left(strsortorder, 8) = "Division" Then
            '        strCurrentDeptDivCode = !DivisionCode
            '    Else
            '        strCurrentDeptDivCode = !Cat
            '    End If
            '    If strDeptDivCode <> strCurrentDeptDivCode Then
            '        mintLine = 9
            '        rowcnt = rowcnt + 1
            '        If Left(strsortorder, 10) = "Department" Or frmSorting.optDeptEmpName.Value Then
            '            xlapp.Cells(rowcnt, 1) = "** Department Code & Name : " & !DepartmentCode & "  " & !DepartmentName
            '        ElseIf Left(strsortorder, 1) = "Division" Or frmSorting.optSectionPaycode.Value Then
            '            xlapp.Cells(rowcnt, 1) = "** Section Code & Name : " & !DivisionCode & "  " & !DivisionName
            '        Else
            '            xlapp.Cells(rowcnt, 1) = "** Category Code & Name : " & !Cat & "  " & !CatagoryName
            '        End If
            '        rowcnt = rowcnt + 1
            '        mintLine = mintLine + 3
            '    End If
            'End If
            'Loop
        Next
        rowcnt = rowcnt + 1
        intCounter = 1
        While intCounter <= intRecCountLeaveMaster
            xlapp.Cells(rowcnt, 1).Font.Bold = True
            xlapp.Cells(rowcnt, 1) = strLeaveField3(intCounter) & " " & "-" & " " & strLeaveDescription50(intCounter) & Space(4)
            rowcnt = rowcnt + 1
            intCounter = intCounter + 1
        End While
        xlwb.SaveAs(mstrFile_Name)
    End Sub
    Sub MonthlyXl_AccruedLeaveGrid(ByVal strsortorder As String)
        'On Error GoTo ErrorGen
        Dim strsql As String
        Dim strSqlLeaveMaster As String
        Dim rs_report_ReportLeaveMaster As DataSet 'ADODB.Recordset
        Dim strLeaveField3() As String '* 3
        Dim strLeaveDescription50() As String ' * 50
        Dim intRecCountLeaveMaster As Integer
        Dim intCounter As Integer
        Dim strLeaveWholeLine(20) As String '* 10
        Dim blnLeaveCheck1 As Boolean
        Dim blnLeaveCheck2 As Boolean
        Dim blnLeaveCheck3 As Boolean
        Dim blnLeaveCheck4 As Boolean
        Dim blnLeaveCheck5 As Boolean
        Dim blnLeaveCheck6 As Boolean
        Dim blnLeaveCheck7 As Boolean
        Dim blnLeaveCheck8 As Boolean
        Dim blnLeaveCheck9 As Boolean
        Dim blnLeaveCheck10 As Boolean
        Dim blnLeaveCheck11 As Boolean
        Dim blnLeaveCheck12 As Boolean
        Dim blnLeaveCheck13 As Boolean
        Dim blnLeaveCheck14 As Boolean
        Dim blnLeaveCheck15 As Boolean
        Dim blnLeaveCheck16 As Boolean
        Dim blnLeaveCheck17 As Boolean
        Dim blnLeaveCheck18 As Boolean
        Dim blnLeaveCheck19 As Boolean
        Dim blnLeaveCheck20 As Boolean

        Dim msrl As Integer
        'Dim xlapp As Excel.Application
        'Dim xlwb As Excel.Workbook
        Dim rowcnt As Integer

        'xlapp = CreateObject("Excel.Application")
        'xlwb = xlapp.Workbooks.Add

        mblnCheckReport = False
        mintPageNo = 1
        mintLine = 1
        If XtraShortOrder.g_SortOrder <> "" Then
            strsortorder = XtraShortOrder.g_SortOrder
        End If
        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If
        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".xls"

        If Common.servername = "Access" Then
            strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName,  tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tblLeaveLedger.PayCode,tblLeaveLedger.L01_ADD,tblLeaveLedger.L02_ADD,tblLeaveLedger.L03_ADD,tblLeaveLedger.L04_ADD,tblLeaveLedger.L05_ADD,tblLeaveLedger.L06_ADD,tblLeaveLedger.L07_ADD,tblLeaveLedger.L08_ADD,tblLeaveLedger.L09_ADD,tblLeaveLedger.L10_ADD,tblLeaveLedger.L11_ADD,tblLeaveLedger.L12_ADD,tblLeaveLedger.L13_ADD,tblLeaveLedger.L14_ADD,tblLeaveLedger.L15_ADD,tblLeaveLedger.L16_ADD,tblLeaveLedger.L17_ADD,tblLeaveLedger.L18_ADD,tblLeaveLedger.L19_ADD,tblLeaveLedger.L20_ADD," & _
            "tblEmployee.EmpName,tblEmployee.PresentCardNo,tblDepartment.DepartmentName,tblEmployee.DepartmentCode" & _
            " from tblCatagory,tblDivision,  tblLeaveLedger,tblEmployee,tblCompany,tblDepartment" & _
            " Where tblCatagory.Cat = tblEmployee.Cat And tblLeaveLedger.PayCode = tblEmployee.PayCode" & _
            " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
            " and TBLLEAVELEDGER.LYEAR = " & TextEdit2.Text & " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & strsortorder
            adapA = New OleDbDataAdapter(strsql, Common.con1)
            adapA.Fill(Rs_Report)
        Else
            strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName,  tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tblLeaveLedger.PayCode,tblLeaveLedger.L01_ADD,tblLeaveLedger.L02_ADD,tblLeaveLedger.L03_ADD,tblLeaveLedger.L04_ADD,tblLeaveLedger.L05_ADD,tblLeaveLedger.L06_ADD,tblLeaveLedger.L07_ADD,tblLeaveLedger.L08_ADD,tblLeaveLedger.L09_ADD,tblLeaveLedger.L10_ADD,tblLeaveLedger.L11_ADD,tblLeaveLedger.L12_ADD,tblLeaveLedger.L13_ADD,tblLeaveLedger.L14_ADD,tblLeaveLedger.L15_ADD,tblLeaveLedger.L16_ADD,tblLeaveLedger.L17_ADD,tblLeaveLedger.L18_ADD,tblLeaveLedger.L19_ADD,tblLeaveLedger.L20_ADD," & _
            "tblEmployee.EmpName,tblEmployee.PresentCardNo,tblDepartment.DepartmentName,tblEmployee.DepartmentCode" & _
            " from tblCatagory,tblDivision,  tblLeaveLedger,tblEmployee,tblCompany,tblDepartment" & _
            " Where tblCatagory.Cat = tblEmployee.Cat And tblLeaveLedger.PayCode = tblEmployee.PayCode" & _
            " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
            " and TBLLEAVELEDGER.LYEAR = " & TextEdit2.Text & " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & strsortorder
            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(Rs_Report)
        End If
        If Rs_Report.Tables(0).Rows.Count < 1 Then
            XtraMessageBox.Show(ulf, "<size=10>No Data Available For this Report.</size>", "<size=9>iAS</size>")
            Exit Sub
        End If

        strSqlLeaveMaster = " Select LeaveField,LeaveDescription from tblLeaveMaster order by LeaveField"
        rs_report_ReportLeaveMaster = New DataSet
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(strSqlLeaveMaster, Common.con1)
            adapA.Fill(rs_report_ReportLeaveMaster)
        Else
            adap = New SqlDataAdapter(strSqlLeaveMaster, Common.con)
            adap.Fill(rs_report_ReportLeaveMaster)
        End If
        For i As Integer = 0 To rs_report_ReportLeaveMaster.Tables(0).Rows.Count - 1 ' While Not rs_report_ReportLeaveMaster.EOF
            intRecCountLeaveMaster = intRecCountLeaveMaster + 1
        Next
        ReDim strLeaveField3(intRecCountLeaveMaster)
        ReDim strLeaveDescription50(intRecCountLeaveMaster)
        For i As Integer = 0 To rs_report_ReportLeaveMaster.Tables(0).Rows.Count - 1 ' While Not rs_report_ReportLeaveMaster.EOF
            intCounter = intCounter + 1
            strLeaveField3(intCounter) = rs_report_ReportLeaveMaster.Tables(0).Rows(i).Item("leavefield").ToString.Trim
            strLeaveDescription50(intCounter) = rs_report_ReportLeaveMaster.Tables(0).Rows(i).Item("LeaveDescription").ToString.Trim
        Next
        rowcnt = rowcnt + 1
        If Common.IsNepali = "Y" Then
            Common.frodatetodatetoReportGrid = " ACCRUAL LEAVE FOR THE YEAR : " & tmpNepali
        Else
            Common.frodatetodatetoReportGrid = " ACCRUAL LEAVE FOR THE YEAR : " & TextEdit2.Text
        End If
        rowcnt = rowcnt + 2

        Me.Cursor = Cursors.WaitCursor
        Common.tbl = New Data.DataTable()
        Common.tbl.Columns.Add("Srl", GetType(String))
        Common.tbl.Columns.Add("Paycode", GetType(String))
        Common.tbl.Columns.Add("Employee Name", GetType(String))
        Common.tbl.Columns.Add(" ", GetType(String))
        Common.tbl.Columns.Add("L01", GetType(String))
        Common.tbl.Columns.Add("L02", GetType(String))
        Common.tbl.Columns.Add("L03", GetType(String))
        Common.tbl.Columns.Add("L04", GetType(String))
        Common.tbl.Columns.Add("L05", GetType(String))
        Common.tbl.Columns.Add("L06", GetType(String))
        Common.tbl.Columns.Add("L07", GetType(String))
        Common.tbl.Columns.Add("L08", GetType(String))
        Common.tbl.Columns.Add("L09", GetType(String))
        Common.tbl.Columns.Add("L10", GetType(String))
        Common.tbl.Columns.Add("L11", GetType(String))
        Common.tbl.Columns.Add("L12", GetType(String))
        Common.tbl.Columns.Add("L13", GetType(String))
        Common.tbl.Columns.Add("L14", GetType(String))
        Common.tbl.Columns.Add("L15", GetType(String))
        Common.tbl.Columns.Add("L16", GetType(String))
        Common.tbl.Columns.Add("L17", GetType(String))
        Common.tbl.Columns.Add("L18", GetType(String))
        Common.tbl.Columns.Add("L19", GetType(String))
        Common.tbl.Columns.Add("L20", GetType(String))

        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            intCounter = 1
            blnLeaveCheck1 = True
            blnLeaveCheck2 = True
            blnLeaveCheck3 = True
            blnLeaveCheck4 = True
            blnLeaveCheck5 = True
            blnLeaveCheck6 = True
            blnLeaveCheck7 = True
            blnLeaveCheck8 = True
            blnLeaveCheck9 = True
            blnLeaveCheck10 = True
            blnLeaveCheck11 = True
            blnLeaveCheck12 = True
            blnLeaveCheck13 = True
            blnLeaveCheck14 = True
            blnLeaveCheck15 = True
            blnLeaveCheck16 = True
            blnLeaveCheck17 = True
            blnLeaveCheck18 = True
            blnLeaveCheck19 = True
            blnLeaveCheck20 = True
            While intCounter <= intRecCountLeaveMaster
                If blnLeaveCheck1 Then
                    If strLeaveField3(intCounter) = "L01" And Rs_Report.Tables(0).Rows(i).Item("L01_ADD").ToString.Trim <> "" Then
                        strLeaveWholeLine(1) = Rs_Report.Tables(0).Rows(i).Item("L01_ADD").ToString.Trim
                        blnLeaveCheck1 = False
                    Else
                        strLeaveWholeLine(1) = ""
                    End If
                End If
                If blnLeaveCheck2 Then
                    If strLeaveField3(intCounter) = "L02" And Rs_Report.Tables(0).Rows(i).Item("L02_ADD").ToString.Trim <> "" Then
                        strLeaveWholeLine(2) = Rs_Report.Tables(0).Rows(i).Item("L02_ADD").ToString.Trim
                        blnLeaveCheck2 = False
                    Else
                        strLeaveWholeLine(2) = ""
                    End If
                End If
                If blnLeaveCheck3 Then
                    If strLeaveField3(intCounter) = "L03" And Rs_Report.Tables(0).Rows(i).Item("L03_ADD").ToString.Trim <> "" Then
                        strLeaveWholeLine(3) = Rs_Report.Tables(0).Rows(i).Item("L03_ADD").ToString.Trim
                        blnLeaveCheck3 = False
                    Else
                        strLeaveWholeLine(3) = ""
                    End If
                End If
                If blnLeaveCheck4 Then
                    If strLeaveField3(intCounter) = "L04" And Rs_Report.Tables(0).Rows(i).Item("L04_ADD").ToString.Trim <> "" Then
                        strLeaveWholeLine(4) = Rs_Report.Tables(0).Rows(i).Item("L04_ADD").ToString.Trim
                        blnLeaveCheck4 = False
                    Else
                        strLeaveWholeLine(4) = ""
                    End If
                End If
                If blnLeaveCheck5 Then
                    If strLeaveField3(intCounter) = "L05" And Rs_Report.Tables(0).Rows(i).Item("L05_ADD").ToString.Trim <> "" Then
                        strLeaveWholeLine(5) = Rs_Report.Tables(0).Rows(i).Item("L05_ADD").ToString.Trim
                        blnLeaveCheck5 = False
                    Else
                        strLeaveWholeLine(5) = ""
                    End If
                End If
                If blnLeaveCheck6 Then
                    If strLeaveField3(intCounter) = "L06" And Rs_Report.Tables(0).Rows(i).Item("L06_ADD").ToString.Trim <> "" Then
                        strLeaveWholeLine(6) = Rs_Report.Tables(0).Rows(i).Item("L06_ADD").ToString.Trim
                        blnLeaveCheck6 = False
                    Else
                        strLeaveWholeLine(6) = ""
                    End If
                End If
                If blnLeaveCheck7 Then
                    If strLeaveField3(intCounter) = "L07" And Rs_Report.Tables(0).Rows(i).Item("L07_ADD").ToString.Trim <> "" Then
                        strLeaveWholeLine(7) = Rs_Report.Tables(0).Rows(i).Item("L07_ADD").ToString.Trim
                        blnLeaveCheck7 = False
                    Else
                        strLeaveWholeLine(7) = ""
                    End If
                End If
                If blnLeaveCheck8 Then
                    If strLeaveField3(intCounter) = "L08" And Rs_Report.Tables(0).Rows(i).Item("L08_ADD").ToString.Trim <> "" Then
                        strLeaveWholeLine(8) = Rs_Report.Tables(0).Rows(i).Item("L08_ADD").ToString.Trim
                        blnLeaveCheck8 = False
                    Else
                        strLeaveWholeLine(8) = ""
                    End If
                End If
                If blnLeaveCheck9 Then
                    If strLeaveField3(intCounter) = "L09" And Rs_Report.Tables(0).Rows(i).Item("L09_ADD").ToString.Trim <> "" Then
                        strLeaveWholeLine(9) = Rs_Report.Tables(0).Rows(i).Item("L09_ADD").ToString.Trim
                        blnLeaveCheck9 = False
                    Else
                        strLeaveWholeLine(9) = ""
                    End If
                End If
                If blnLeaveCheck10 Then
                    If strLeaveField3(intCounter) = "L10" And Rs_Report.Tables(0).Rows(i).Item("L10_ADD").ToString.Trim <> "" Then
                        strLeaveWholeLine(10) = Rs_Report.Tables(0).Rows(i).Item("L10_ADD").ToString.Trim
                        blnLeaveCheck10 = False
                    Else
                        strLeaveWholeLine(10) = ""
                    End If
                End If
                If blnLeaveCheck11 Then
                    If strLeaveField3(intCounter) = "L11" And Rs_Report.Tables(0).Rows(i).Item("L11_ADD").ToString.Trim <> "" Then
                        strLeaveWholeLine(11) = Rs_Report.Tables(0).Rows(i).Item("L11_ADD").ToString.Trim
                        blnLeaveCheck11 = False
                    Else
                        strLeaveWholeLine(11) = ""
                    End If
                End If
                If blnLeaveCheck12 Then
                    If strLeaveField3(intCounter) = "L12" And Rs_Report.Tables(0).Rows(i).Item("L12_ADD").ToString.Trim <> "" Then
                        strLeaveWholeLine(12) = Rs_Report.Tables(0).Rows(i).Item("L12_ADD").ToString.Trim
                        blnLeaveCheck12 = False
                    Else
                        strLeaveWholeLine(12) = ""
                    End If
                End If
                If blnLeaveCheck13 Then
                    If strLeaveField3(intCounter) = "L13" And Rs_Report.Tables(0).Rows(i).Item("L13_ADD").ToString.Trim <> "" Then
                        strLeaveWholeLine(13) = Rs_Report.Tables(0).Rows(i).Item("L13_ADD").ToString.Trim
                        blnLeaveCheck13 = False
                    Else
                        strLeaveWholeLine(13) = ""
                    End If
                End If
                If blnLeaveCheck14 Then
                    If strLeaveField3(intCounter) = "L14" And Rs_Report.Tables(0).Rows(i).Item("L14_ADD").ToString.Trim <> "" Then
                        strLeaveWholeLine(14) = Rs_Report.Tables(0).Rows(i).Item("L14_ADD").ToString.Trim
                        blnLeaveCheck14 = False
                    Else
                        strLeaveWholeLine(14) = ""
                    End If
                End If
                If blnLeaveCheck15 Then
                    If strLeaveField3(intCounter) = "L15" And Rs_Report.Tables(0).Rows(i).Item("L15_ADD").ToString.Trim <> "" Then
                        strLeaveWholeLine(15) = Rs_Report.Tables(0).Rows(i).Item("L15_ADD").ToString.Trim
                        blnLeaveCheck15 = False
                    Else
                        strLeaveWholeLine(15) = ""
                    End If
                End If
                If blnLeaveCheck16 Then
                    If strLeaveField3(intCounter) = "L16" And Rs_Report.Tables(0).Rows(i).Item("L16_ADD").ToString.Trim <> "" Then
                        strLeaveWholeLine(16) = Rs_Report.Tables(0).Rows(i).Item("L16_ADD").ToString.Trim
                        blnLeaveCheck16 = False
                    Else
                        strLeaveWholeLine(16) = ""
                    End If
                End If
                If blnLeaveCheck17 Then
                    If strLeaveField3(intCounter) = "L17" And Rs_Report.Tables(0).Rows(i).Item("L17_ADD").ToString.Trim <> "" Then
                        strLeaveWholeLine(17) = Rs_Report.Tables(0).Rows(i).Item("L17_ADD").ToString.Trim
                        blnLeaveCheck17 = False
                    Else
                        strLeaveWholeLine(17) = ""
                    End If
                End If
                If blnLeaveCheck18 Then
                    If strLeaveField3(intCounter) = "L18" And Rs_Report.Tables(0).Rows(i).Item("L18_ADD").ToString.Trim <> "" Then
                        strLeaveWholeLine(18) = Rs_Report.Tables(0).Rows(i).Item("L18_ADD").ToString.Trim
                        blnLeaveCheck18 = False
                    Else
                        strLeaveWholeLine(18) = ""
                    End If
                End If
                If blnLeaveCheck19 Then
                    If strLeaveField3(intCounter) = "L19" And Rs_Report.Tables(0).Rows(i).Item("L19_ADD").ToString.Trim <> "" Then
                        strLeaveWholeLine(19) = Rs_Report.Tables(0).Rows(i).Item("L19_ADD").ToString.Trim
                        blnLeaveCheck19 = False
                    Else
                        strLeaveWholeLine(19) = ""
                    End If
                End If
                If blnLeaveCheck20 Then
                    If strLeaveField3(intCounter) = "L20" And Rs_Report.Tables(0).Rows(i).Item("L20_ADD").ToString.Trim <> "" Then
                        strLeaveWholeLine(20) = Rs_Report.Tables(0).Rows(i).Item("L20_ADD").ToString.Trim
                        blnLeaveCheck20 = False
                    Else
                        strLeaveWholeLine(20) = ""
                    End If
                End If
                intCounter = intCounter + 1
            End While
            rowcnt = rowcnt + 1
            msrl = msrl + 1

            Common.tbl.Rows.Add(msrl, Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim, Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim, "", _
                                strLeaveWholeLine(1), strLeaveWholeLine(2), strLeaveWholeLine(3), strLeaveWholeLine(4), strLeaveWholeLine(5), _
                                strLeaveWholeLine(6), strLeaveWholeLine(7), strLeaveWholeLine(8), strLeaveWholeLine(9), strLeaveWholeLine(10), _
                                strLeaveWholeLine(11), strLeaveWholeLine(12), strLeaveWholeLine(13), strLeaveWholeLine(14), strLeaveWholeLine(15), _
                                strLeaveWholeLine(16), strLeaveWholeLine(17), strLeaveWholeLine(18), strLeaveWholeLine(19), strLeaveWholeLine(20))
            mintLine = mintLine + 1
            mblnCheckReport = True
        Next
        rowcnt = rowcnt + 1
        intCounter = 1
        Common.tbl.Rows.Add("")
        While intCounter <= intRecCountLeaveMaster
            Common.tbl.Rows.Add(strLeaveField3(intCounter) & " " & "-" & " ", strLeaveDescription50(intCounter))
            rowcnt = rowcnt + 1
            intCounter = intCounter + 1
        End While
        Me.Cursor = Cursors.Default
        XtraReportGrid.ShowDialog()
    End Sub
    Sub Monthly_ConsumedLeave(ByVal strsortorder As String)
        Dim strDeptDivCode As String
        Dim strCurrentDeptDivCode As String
        Dim intFile As Integer
        Dim strsql As String
        Dim strSqlLeaveMaster As String
        Dim rs_report_ReportLeaveMaster As DataSet 'ADODB.Recordset
        Dim strLeaveField3() As String '* 3
        Dim strLeaveDescription50() As String '* 50
        Dim intRecCountLeaveMaster As Integer
        Dim intCounter As Integer
        Dim strLeaveWholeLine(20) As String '* 10
        Dim blnLeaveCheck1 As Boolean
        Dim blnLeaveCheck2 As Boolean
        Dim blnLeaveCheck3 As Boolean
        Dim blnLeaveCheck4 As Boolean
        Dim blnLeaveCheck5 As Boolean
        Dim blnLeaveCheck6 As Boolean
        Dim blnLeaveCheck7 As Boolean
        Dim blnLeaveCheck8 As Boolean
        Dim blnLeaveCheck9 As Boolean
        Dim blnLeaveCheck10 As Boolean
        Dim blnLeaveCheck11 As Boolean
        Dim blnLeaveCheck12 As Boolean
        Dim blnLeaveCheck13 As Boolean
        Dim blnLeaveCheck14 As Boolean
        Dim blnLeaveCheck15 As Boolean
        Dim blnLeaveCheck16 As Boolean
        Dim blnLeaveCheck17 As Boolean
        Dim blnLeaveCheck18 As Boolean
        Dim blnLeaveCheck19 As Boolean
        Dim blnLeaveCheck20 As Boolean
        Dim blnPrintLine As Boolean
        Dim strDepartmentCode As String
        Dim intPrintLineCounter As Integer
        Dim blnDeptAvailable As Boolean, mFirst As Boolean
        Dim msrl As Integer
        msrl = 1

        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If
        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet

        mblnCheckReport = False
        mFirst = True
        mintPageNo = 1
        mintLine = 1

        If Common.servername = "Access" Then
            strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName,  tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tblLeaveLedger.PayCode,tblLeaveLedger.L01,tblLeaveLedger.L02,tblLeaveLedger.L03,tblLeaveLedger.L04,tblLeaveLedger.L05,tblLeaveLedger.L06,tblLeaveLedger.L07,tblLeaveLedger.L08,tblLeaveLedger.L09,tblLeaveLedger.L10,tblLeaveLedger.L11,tblLeaveLedger.L12,tblLeaveLedger.L13,tblLeaveLedger.L14," & _
                 "tblLeaveLedger.L15,tblLeaveLedger.L16,tblLeaveLedger.L17,tblLeaveLedger.L18,tblLeaveLedger.L19,tblLeaveLedger.L20,tblEmployee.EmpName,tblEmployee.PresentCardNo,tblDepartment.DepartmentName,tblEmployee.departmentcode" & _
                 " from tblCatagory,tblDivision,  tblLeaveLedger,tblEmployee,tblCompany,tblDepartment" & _
                 " Where tblCatagory.Cat = tblEmployee.Cat And tblLeaveLedger.PayCode = tblEmployee.PayCode" & _
                 " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
                 " and TBLLEAVELEDGER.LYEAR = " & TextEdit2.Text & " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & " order by tblEmployee.PayCode"
            adapA = New OleDbDataAdapter(strsql, Common.con1)
            adapA.Fill(Rs_Report)
        Else
            strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName,  tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tblLeaveLedger.PayCode,tblLeaveLedger.L01,tblLeaveLedger.L02,tblLeaveLedger.L03,tblLeaveLedger.L04,tblLeaveLedger.L05,tblLeaveLedger.L06,tblLeaveLedger.L07,tblLeaveLedger.L08,tblLeaveLedger.L09,tblLeaveLedger.L10,tblLeaveLedger.L11,tblLeaveLedger.L12,tblLeaveLedger.L13,tblLeaveLedger.L14," & _
                 "tblLeaveLedger.L15,tblLeaveLedger.L16,tblLeaveLedger.L17,tblLeaveLedger.L18,tblLeaveLedger.L19,tblLeaveLedger.L20,tblEmployee.EmpName,tblEmployee.PresentCardNo,tblDepartment.DepartmentName,tblEmployee.departmentcode" & _
                 " from tblCatagory,tblDivision,  tblLeaveLedger,tblEmployee,tblCompany,tblDepartment" & _
                 " Where tblCatagory.Cat = tblEmployee.Cat And tblLeaveLedger.PayCode = tblEmployee.PayCode" & _
                 " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
                 " and TBLLEAVELEDGER.LYEAR = " & TextEdit2.Text & " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & " order by tblEmployee.PayCode"
            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(Rs_Report)
        End If
        If Rs_Report.Tables(0).Rows.Count < 1 Then
            XtraMessageBox.Show(ulf, "<size=10>No Data Available For this Report.</size>", "<size=9>iAS</size>")
            Exit Sub
        End If

        strSqlLeaveMaster = " Select LeaveField,LeaveDescription from tblLeaveMaster order by LeaveField"
        rs_report_ReportLeaveMaster = New DataSet
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(strSqlLeaveMaster, Common.con1)
            adapA.Fill(rs_report_ReportLeaveMaster)
        Else
            adap = New SqlDataAdapter(strSqlLeaveMaster, Common.con)
            adap.Fill(rs_report_ReportLeaveMaster)
        End If
        For i As Integer = 0 To rs_report_ReportLeaveMaster.Tables(0).Rows.Count - 1 ' While Not rs_report_ReportLeaveMaster.EOF
            intRecCountLeaveMaster = intRecCountLeaveMaster + 1
        Next
        ReDim strLeaveField3(intRecCountLeaveMaster)
        ReDim strLeaveDescription50(intRecCountLeaveMaster)
        For i As Integer = 0 To rs_report_ReportLeaveMaster.Tables(0).Rows.Count - 1 ' While Not rs_report_ReportLeaveMaster.EOF
            intCounter = intCounter + 1
            strLeaveField3(intCounter) = rs_report_ReportLeaveMaster.Tables(0).Rows(i).Item("leavefield").ToString.Trim
            strLeaveDescription50(intCounter) = rs_report_ReportLeaveMaster.Tables(0).Rows(i).Item("LeaveDescription").ToString.Trim
        Next


        intFile = FreeFile()
        mFileNumber = FreeFile()
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".SRP"
        Dim objWriter As New System.IO.StreamWriter(mstrFile_Name, True)
        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            If mFirst Or mintLine > g_LinesPerPage Or (strsortorder.Trim = "Department" And strDeptDivCode <> Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim And g_SkipAfterDept) Then
1:              If Not mFirst Then
                    objWriter.WriteLine(Space(2) & vbFormFeed)
                Else
                    mFirst = False
                End If
                objWriter.WriteLine(Space(2) & Space(40) & CommonReport.g_CompanyNames)
                objWriter.WriteLine(Space(2) & "")
                objWriter.WriteLine(Space(2) & "Page No." & mintPageNo)
                objWriter.WriteLine(Space(2) & "                                                                                                                        Run Date & Time : " & Common.runningDateTime) 'Now.ToString("dd/MM/yyyy HH:mm"))
                If Common.IsNepali = "Y" Then
                    objWriter.WriteLine(Space(2) & Space(40) & "Consumed Leaves for the Year " & tmpNepali)
                Else
                    objWriter.WriteLine(Space(2) & Space(40) & "Consumed Leaves for the Year " & TextEdit2.Text & "")
                End If
                objWriter.WriteLine(Space(2) & "----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------")
                objWriter.WriteLine(Space(2) & "Srl  Payroll    Employee Name              -------------------------------------------------------------------------------Consumed------------------------------------------------------------------------------------------------")
                objWriter.WriteLine(Space(2) & "No.                                             L01      L02      L03      L04      L05      L06      L07      L08      L09      L10      L11      L12      L13      L14      L15      L16      L17      L18      L19      L20")
                objWriter.WriteLine(Space(2) & "----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------")
                mintLine = 9
                mintPageNo = mintPageNo + 1
            End If

            intCounter = 1
            blnLeaveCheck1 = True
            blnLeaveCheck2 = True
            blnLeaveCheck3 = True
            blnLeaveCheck4 = True
            blnLeaveCheck5 = True
            blnLeaveCheck6 = True
            blnLeaveCheck7 = True
            blnLeaveCheck8 = True
            blnLeaveCheck9 = True
            blnLeaveCheck10 = True
            blnLeaveCheck11 = True
            blnLeaveCheck12 = True
            blnLeaveCheck13 = True
            blnLeaveCheck14 = True
            blnLeaveCheck15 = True
            blnLeaveCheck16 = True
            blnLeaveCheck17 = True
            blnLeaveCheck18 = True
            blnLeaveCheck19 = True
            blnLeaveCheck20 = True
            While intCounter <= intRecCountLeaveMaster
                If blnLeaveCheck1 Then
                    If strLeaveField3(intCounter) = "L01" And Rs_Report.Tables(0).Rows(i).Item("L01").ToString.Trim <> "" Then
                        strLeaveWholeLine(1) = Rs_Report.Tables(0).Rows(i).Item("L01").ToString.Trim
                        blnLeaveCheck1 = False
                    Else
                        strLeaveWholeLine(1) = ""
                    End If
                End If
                If blnLeaveCheck2 Then
                    If strLeaveField3(intCounter) = "L02" And Rs_Report.Tables(0).Rows(i).Item("L02").ToString.Trim <> "" Then
                        strLeaveWholeLine(2) = Rs_Report.Tables(0).Rows(i).Item("L02").ToString.Trim
                        blnLeaveCheck2 = False
                    Else
                        strLeaveWholeLine(2) = ""
                    End If
                End If
                If blnLeaveCheck3 Then
                    If strLeaveField3(intCounter) = "L03" And Rs_Report.Tables(0).Rows(i).Item("L03").ToString.Trim <> "" Then
                        strLeaveWholeLine(3) = Rs_Report.Tables(0).Rows(i).Item("L03").ToString.Trim
                        blnLeaveCheck3 = False
                    Else
                        strLeaveWholeLine(3) = ""
                    End If
                End If
                If blnLeaveCheck4 Then
                    If strLeaveField3(intCounter) = "L04" And Rs_Report.Tables(0).Rows(i).Item("L04").ToString.Trim <> "" Then
                        strLeaveWholeLine(4) = Rs_Report.Tables(0).Rows(i).Item("L04").ToString.Trim
                        blnLeaveCheck4 = False
                    Else
                        strLeaveWholeLine(4) = ""
                    End If
                End If
                If blnLeaveCheck5 Then
                    If strLeaveField3(intCounter) = "L05" And Rs_Report.Tables(0).Rows(i).Item("L05").ToString.Trim <> "" Then
                        strLeaveWholeLine(5) = Rs_Report.Tables(0).Rows(i).Item("L05").ToString.Trim
                        blnLeaveCheck5 = False
                    Else
                        strLeaveWholeLine(5) = ""
                    End If
                End If
                If blnLeaveCheck6 Then
                    If strLeaveField3(intCounter) = "L06" And Rs_Report.Tables(0).Rows(i).Item("L06").ToString.Trim <> "" Then
                        strLeaveWholeLine(6) = Rs_Report.Tables(0).Rows(i).Item("L06").ToString.Trim
                        blnLeaveCheck6 = False
                    Else
                        strLeaveWholeLine(6) = ""
                    End If
                End If
                If blnLeaveCheck7 Then
                    If strLeaveField3(intCounter) = "L07" And Rs_Report.Tables(0).Rows(i).Item("L07").ToString.Trim <> "" Then
                        strLeaveWholeLine(7) = Rs_Report.Tables(0).Rows(i).Item("L07").ToString.Trim
                        blnLeaveCheck7 = False
                    Else
                        strLeaveWholeLine(7) = ""
                    End If
                End If
                If blnLeaveCheck8 Then
                    If strLeaveField3(intCounter) = "L08" And Rs_Report.Tables(0).Rows(i).Item("L08").ToString.Trim <> "" Then
                        strLeaveWholeLine(8) = Rs_Report.Tables(0).Rows(i).Item("L08").ToString.Trim
                        blnLeaveCheck8 = False
                    Else
                        strLeaveWholeLine(8) = ""
                    End If
                End If
                If blnLeaveCheck9 Then
                    If strLeaveField3(intCounter) = "L09" And Rs_Report.Tables(0).Rows(i).Item("L09").ToString.Trim <> "" Then
                        strLeaveWholeLine(9) = Rs_Report.Tables(0).Rows(i).Item("L09").ToString.Trim
                        blnLeaveCheck9 = False
                    Else
                        strLeaveWholeLine(9) = ""
                    End If
                End If
                If blnLeaveCheck10 Then
                    If strLeaveField3(intCounter) = "L10" And Rs_Report.Tables(0).Rows(i).Item("L10").ToString.Trim <> "" Then
                        strLeaveWholeLine(10) = Rs_Report.Tables(0).Rows(i).Item("L10").ToString.Trim
                        blnLeaveCheck10 = False
                    Else
                        strLeaveWholeLine(10) = ""
                    End If
                End If
                If blnLeaveCheck11 Then
                    If strLeaveField3(intCounter) = "L11" And Rs_Report.Tables(0).Rows(i).Item("L11").ToString.Trim <> "" Then
                        strLeaveWholeLine(11) = Rs_Report.Tables(0).Rows(i).Item("L11").ToString.Trim
                        blnLeaveCheck11 = False
                    Else
                        strLeaveWholeLine(11) = ""
                    End If
                End If
                If blnLeaveCheck12 Then
                    If strLeaveField3(intCounter) = "L12" And Rs_Report.Tables(0).Rows(i).Item("L12").ToString.Trim <> "" Then
                        strLeaveWholeLine(12) = Rs_Report.Tables(0).Rows(i).Item("L12").ToString.Trim
                        blnLeaveCheck12 = False
                    Else
                        strLeaveWholeLine(12) = ""
                    End If
                End If
                If blnLeaveCheck13 Then
                    If strLeaveField3(intCounter) = "L13" And Rs_Report.Tables(0).Rows(i).Item("L13").ToString.Trim <> "" Then
                        strLeaveWholeLine(13) = Rs_Report.Tables(0).Rows(i).Item("L13").ToString.Trim
                        blnLeaveCheck13 = False
                    Else
                        strLeaveWholeLine(13) = ""
                    End If
                End If
                If blnLeaveCheck14 Then
                    If strLeaveField3(intCounter) = "L14" And Rs_Report.Tables(0).Rows(i).Item("L14").ToString.Trim <> "" Then
                        strLeaveWholeLine(14) = Rs_Report.Tables(0).Rows(i).Item("L14").ToString.Trim
                        blnLeaveCheck14 = False
                    Else
                        strLeaveWholeLine(14) = ""
                    End If
                End If
                If blnLeaveCheck15 Then
                    If strLeaveField3(intCounter) = "L15" And Rs_Report.Tables(0).Rows(i).Item("L15").ToString.Trim <> "" Then
                        strLeaveWholeLine(15) = Rs_Report.Tables(0).Rows(i).Item("L15").ToString.Trim
                        blnLeaveCheck15 = False
                    Else
                        strLeaveWholeLine(15) = ""
                    End If
                End If
                If blnLeaveCheck16 Then
                    If strLeaveField3(intCounter) = "L16" And Rs_Report.Tables(0).Rows(i).Item("L16").ToString.Trim <> "" Then
                        strLeaveWholeLine(16) = Rs_Report.Tables(0).Rows(i).Item("L16").ToString.Trim
                        blnLeaveCheck16 = False
                    Else
                        strLeaveWholeLine(16) = ""
                    End If
                End If
                If blnLeaveCheck17 Then
                    If strLeaveField3(intCounter) = "L17" And Rs_Report.Tables(0).Rows(i).Item("L17").ToString.Trim <> "" Then
                        strLeaveWholeLine(17) = Rs_Report.Tables(0).Rows(i).Item("L17").ToString.Trim
                        blnLeaveCheck17 = False
                    Else
                        strLeaveWholeLine(17) = ""
                    End If
                End If
                If blnLeaveCheck18 Then
                    If strLeaveField3(intCounter) = "L18" And Rs_Report.Tables(0).Rows(i).Item("L18").ToString.Trim <> "" Then
                        strLeaveWholeLine(18) = Rs_Report.Tables(0).Rows(i).Item("L18").ToString.Trim
                        blnLeaveCheck18 = False
                    Else
                        strLeaveWholeLine(18) = ""
                    End If
                End If
                If blnLeaveCheck19 Then
                    If strLeaveField3(intCounter) = "L19" And Rs_Report.Tables(0).Rows(i).Item("L19").ToString.Trim <> "" Then
                        strLeaveWholeLine(19) = Rs_Report.Tables(0).Rows(i).Item("L19").ToString.Trim
                        blnLeaveCheck19 = False
                    Else
                        strLeaveWholeLine(19) = ""
                    End If
                End If
                If blnLeaveCheck20 Then
                    If strLeaveField3(intCounter) = "L20" And Rs_Report.Tables(0).Rows(i).Item("L20").ToString.Trim <> "" Then
                        strLeaveWholeLine(20) = Rs_Report.Tables(0).Rows(i).Item("L20").ToString.Trim
                        blnLeaveCheck20 = False
                    Else
                        strLeaveWholeLine(20) = ""
                    End If
                End If
                intCounter = intCounter + 1
            End While

            'objWriter.WriteLine(Space(2) & msrl.ToString.Trim.PadRight(5) & " " & Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim.PadRight(12) & Space(1) & Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim.PadRight(25) & Space(1) & _
            '   Space(3) & Trim(Format(strLeaveWholeLine(1), "0.00")) & Space(3) & Space(3) & Trim(Format(strLeaveWholeLine(2), "0.00")) & Space(3) & Space(3) & Trim(Format(strLeaveWholeLine(3), "0.00")) & Space(3) & Space(3) & Trim(Format(strLeaveWholeLine(4), "0.00")) & Space(3) & _
            '   Space(3) & Trim(Format(strLeaveWholeLine(5), "0.00")) & Space(3) & Space(3) & Trim(Format(strLeaveWholeLine(6), "0.00")) & Space(3) & Space(3) & Trim(Format(strLeaveWholeLine(7), "0.00")) & Space(3) & Space(3) & Trim(Format(strLeaveWholeLine(8), "0.00")) & Space(3) & _
            '   Space(3) & Trim(Format(strLeaveWholeLine(9), "0.00")) & Space(3) & Space(3) & Trim(Format(strLeaveWholeLine(10), "0.00")) & Space(3) & Space(3) & Trim(Format(strLeaveWholeLine(11), "0.00")) & Space(3) & Space(3) & Trim(Format(strLeaveWholeLine(12), "0.00")) & Space(3) & _
            '   Space(3) & Trim(Format(strLeaveWholeLine(13), "0.00")) & Space(3) & Space(3) & Trim(Format(strLeaveWholeLine(14), "0.00")) & Space(3) & Space(3) & Trim(Format(strLeaveWholeLine(15), "0.00")) & Space(3) & Space(3) & Trim(Format(strLeaveWholeLine(16), "0.00")) & Space(3) & _
            '   Space(3) & Trim(Format(strLeaveWholeLine(17), "0.00")) & Space(3) & Space(3) & Trim(Format(strLeaveWholeLine(18), "0.00")) & Space(3) & Space(3) & Trim(Format(strLeaveWholeLine(19), "0.00")) & Space(3) & Space(3) & Trim(Format(strLeaveWholeLine(20), "0.00")))


            objWriter.WriteLine(Space(2) & msrl.ToString.PadRight(5) & " " & Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim.PadRight(12) & Space(1) & Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim.PadRight(25) & Space(1) & _
               Space(3) & strLeaveWholeLine(1).PadRight(6) & Space(3) & strLeaveWholeLine(2).PadRight(6) & Space(3) & strLeaveWholeLine(3).PadRight(6) & Space(3) & strLeaveWholeLine(4).PadRight(6) & Space(3) & _
               Space(3) & strLeaveWholeLine(5).PadRight(6) & Space(3) & strLeaveWholeLine(6).PadRight(6) & Space(3) & strLeaveWholeLine(7).PadRight(6) & Space(3) & strLeaveWholeLine(8).PadRight(6) & Space(3) & _
               Space(3) & strLeaveWholeLine(9).PadRight(6) & Space(3) & strLeaveWholeLine(10).PadRight(6) & Space(3) & strLeaveWholeLine(11).PadRight(6) & Space(3) & strLeaveWholeLine(12).PadRight(6) & Space(3) & _
               Space(3) & strLeaveWholeLine(13).PadRight(6) & Space(3) & strLeaveWholeLine(14).PadRight(6) & Space(3) & strLeaveWholeLine(15).PadRight(6) & Space(3) & strLeaveWholeLine(16).PadRight(6) & Space(3) & _
               Space(3) & strLeaveWholeLine(17).PadRight(6) & Space(3) & strLeaveWholeLine(18).PadRight(6) & Space(3) & strLeaveWholeLine(19).PadRight(6) & Space(3) & strLeaveWholeLine(20).PadRight(6))



            msrl = msrl + 1
            mintLine = mintLine + 1
            mblnCheckReport = True
        Next

        intCounter = 1
        objWriter.WriteLine(Space(2) & "")
        objWriter.WriteLine(Space(2) & "")
        While intCounter <= intRecCountLeaveMaster
            objWriter.WriteLine(Space(2) & strLeaveField3(intCounter) & " " & "-" & " " & strLeaveDescription50(intCounter) & Space(4))
            If intCounter Mod 3 = 0 Then objWriter.WriteLine(Space(2) & "")
            intCounter = intCounter + 1
        End While
        objWriter.Close()
        Try
            Process.Start(mstrFile_Name)
        Catch ex As Exception
            Process.Start("notepad.exe", mstrFile_Name)
        End Try
    End Sub
    Sub MonthlyXl_ConsumedLeave(ByVal strsortorder As String)
        Dim strDeptDivCode As String
        Dim strCurrentDeptDivCode As String
        Dim strsql As String
        Dim strSqlLeaveMaster As String
        Dim rs_report_ReportLeaveMaster As DataSet 'ADODB.Recordset
        Dim strLeaveField3() As String ' * 3
        Dim strLeaveDescription50() As String '* 50
        Dim intRecCountLeaveMaster As Integer
        Dim intCounter As Integer
        Dim intFile As Integer
        Dim strLeaveWholeLine(20) As String ' 10
        Dim blnLeaveCheck1 As Boolean
        Dim blnLeaveCheck2 As Boolean
        Dim blnLeaveCheck3 As Boolean
        Dim blnLeaveCheck4 As Boolean
        Dim blnLeaveCheck5 As Boolean
        Dim blnLeaveCheck6 As Boolean
        Dim blnLeaveCheck7 As Boolean
        Dim blnLeaveCheck8 As Boolean
        Dim blnLeaveCheck9 As Boolean
        Dim blnLeaveCheck10 As Boolean
        Dim blnLeaveCheck11 As Boolean
        Dim blnLeaveCheck12 As Boolean
        Dim blnLeaveCheck13 As Boolean
        Dim blnLeaveCheck14 As Boolean
        Dim blnLeaveCheck15 As Boolean
        Dim blnLeaveCheck16 As Boolean
        Dim blnLeaveCheck17 As Boolean
        Dim blnLeaveCheck18 As Boolean
        Dim blnLeaveCheck19 As Boolean
        Dim blnLeaveCheck20 As Boolean
        Dim blnPrintLine As Boolean
        Dim strDepartmentCode As String
        Dim intPrintLineCounter As Integer
        Dim blnDeptAvailable As Boolean, mFirst As Boolean
        Dim msrl As Integer
        Dim xlapp As Excel.Application
        Dim xlwb As Excel.Workbook
        Dim xlst As Excel.Sheets
        Dim rowcnt As Integer

        xlapp = CreateObject("Excel.Application")
        xlwb = xlapp.Workbooks.Add
        'If UCase(Dir(App.Path & "\TimeWatch.xls")) = UCase("TimeWatch.xls") Then
        '    Kill(App.Path & "\TimeWatch.xls")
        'End If
        mblnCheckReport = False
        mintPageNo = 1
        mintLine = 1
        If XtraShortOrder.g_SortOrder <> "" Then
            strsortorder = XtraShortOrder.g_SortOrder
        End If
        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If
        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".xls"

        If Common.servername = "Access" Then
            strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName,  tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tblLeaveLedger.PayCode,tblLeaveLedger.L01,tblLeaveLedger.L02,tblLeaveLedger.L03,tblLeaveLedger.L04,tblLeaveLedger.L05,tblLeaveLedger.L06,tblLeaveLedger.L07,tblLeaveLedger.L08,tblLeaveLedger.L09,tblLeaveLedger.L10,tblLeaveLedger.L11,tblLeaveLedger.L12,tblLeaveLedger.L13,tblLeaveLedger.L14," & _
                 "tblLeaveLedger.L15,tblLeaveLedger.L16,tblLeaveLedger.L17,tblLeaveLedger.L18,tblLeaveLedger.L19,tblLeaveLedger.L20,tblEmployee.EmpName,tblEmployee.PresentCardNo,tblDepartment.DepartmentName,tblEmployee.departmentcode" & _
                 " from tblCatagory,tblDivision,  tblLeaveLedger,tblEmployee,tblCompany,tblDepartment" & _
                 " Where tblCatagory.Cat = tblEmployee.Cat And tblDivision.DivisionCode = tblEmployee.DivisionCode And tblLeaveLedger.PayCode = tblEmployee.PayCode" & _
                 " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
                 " and TBLLEAVELEDGER.LYEAR = " & TextEdit2.Text & " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & strsortorder
            adapA = New OleDbDataAdapter(strsql, Common.con1)
            adapA.Fill(Rs_Report)
        Else
            strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName,  tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tblLeaveLedger.PayCode,tblLeaveLedger.L01,tblLeaveLedger.L02,tblLeaveLedger.L03,tblLeaveLedger.L04,tblLeaveLedger.L05,tblLeaveLedger.L06,tblLeaveLedger.L07,tblLeaveLedger.L08,tblLeaveLedger.L09,tblLeaveLedger.L10,tblLeaveLedger.L11,tblLeaveLedger.L12,tblLeaveLedger.L13,tblLeaveLedger.L14," & _
                 "tblLeaveLedger.L15,tblLeaveLedger.L16,tblLeaveLedger.L17,tblLeaveLedger.L18,tblLeaveLedger.L19,tblLeaveLedger.L20,tblEmployee.EmpName,tblEmployee.PresentCardNo,tblDepartment.DepartmentName,tblEmployee.departmentcode" & _
                 " from tblCatagory,tblDivision,  tblLeaveLedger,tblEmployee,tblCompany,tblDepartment" & _
                 " Where tblCatagory.Cat = tblEmployee.Cat And tblDivision.DivisionCode = tblEmployee.DivisionCode And tblLeaveLedger.PayCode = tblEmployee.PayCode" & _
                 " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
                 " and TBLLEAVELEDGER.LYEAR = " & TextEdit2.Text & " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & strsortorder
            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(Rs_Report)
        End If
        If Rs_Report.Tables(0).Rows.Count < 1 Then
            XtraMessageBox.Show(ulf, "<size=10>TNo Data Available For this Report.</size>", "<size=9>iAS</size>")
            Exit Sub
        End If

        strSqlLeaveMaster = " Select LeaveField,LeaveDescription from tblLeaveMaster order by LeaveField"
        rs_report_ReportLeaveMaster = New DataSet
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(strSqlLeaveMaster, Common.con1)
            adapA.Fill(rs_report_ReportLeaveMaster)
        Else
            adap = New SqlDataAdapter(strSqlLeaveMaster, Common.con)
            adap.Fill(rs_report_ReportLeaveMaster)
        End If
        For i As Integer = 0 To rs_report_ReportLeaveMaster.Tables(0).Rows.Count - 1 ' While Not rs_report_ReportLeaveMaster.EOF
            intRecCountLeaveMaster = intRecCountLeaveMaster + 1
        Next
        ReDim strLeaveField3(intRecCountLeaveMaster)
        ReDim strLeaveDescription50(intRecCountLeaveMaster)
        For i As Integer = 0 To rs_report_ReportLeaveMaster.Tables(0).Rows.Count - 1 ' While Not rs_report_ReportLeaveMaster.EOF
            intCounter = intCounter + 1
            strLeaveField3(intCounter) = rs_report_ReportLeaveMaster.Tables(0).Rows(i).Item("leavefield").ToString.Trim
            strLeaveDescription50(intCounter) = rs_report_ReportLeaveMaster.Tables(0).Rows(i).Item("LeaveDescription").ToString.Trim
        Next

        'With Rs_Report
        rowcnt = rowcnt + 1
        xlapp.Visible = True
        xlapp.Columns.ColumnWidth = 10
        xlapp.Cells(rowcnt, 5).Font.Bold = True
        xlapp.Cells(rowcnt, 5) = "Company Name:" & CommonReport.g_CompanyNames
        rowcnt = rowcnt + 1
        xlapp.Cells(rowcnt, 5).Font.Bold = True
        xlapp.Cells(rowcnt, 5) = "Run Date & Time :" & Format(Now(), "dd/MM/yyyy HH:mm")
        rowcnt = rowcnt + 2
        xlapp.Cells.Font.Size = 8
        xlapp.Cells.Font.Name = "tahoma"
        xlapp.Cells(rowcnt, 4).Font.Bold = True
        xlapp.Cells(rowcnt, 4) = " CONSUMED LEAVE FOR THE YEAR : " & TextEdit2.Text
        rowcnt = rowcnt + 2
        xlapp.Cells(rowcnt, 1).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 1).Font.Bold = True
        xlapp.Cells(rowcnt, 1).Font.ColorIndex = 2 'vbWhite
        xlapp.Cells(rowcnt, 1).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 1) = "Srl No."
        xlapp.Cells(rowcnt, 2).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 2).Font.Bold = True
        xlapp.Cells(rowcnt, 2).Font.ColorIndex = 2 'vbWhite
        xlapp.Cells(rowcnt, 2).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 2) = "Paycode."
        xlapp.Cells(rowcnt, 3).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 3).Font.Bold = True
        xlapp.Cells(rowcnt, 3).Font.ColorIndex = 2 ' vbWhite
        xlapp.Cells(rowcnt, 3).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 3) = "Employee Name"
        xlapp.Cells(rowcnt, 4).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 4).Font.Bold = True
        xlapp.Cells(rowcnt, 4).Font.ColorIndex = 2 'vbWhite
        xlapp.Cells(rowcnt, 4).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 5).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 5).Font.Bold = True
        xlapp.Cells(rowcnt, 5).Font.ColorIndex = 2 ' vbWhite
        xlapp.Cells(rowcnt, 5).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 5) = "L01"
        xlapp.Cells(rowcnt, 6).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 6).Font.Bold = True
        xlapp.Cells(rowcnt, 6).Font.ColorIndex = 2 ' vbWhite
        xlapp.Cells(rowcnt, 6).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 6) = "L02"
        xlapp.Cells(rowcnt, 7).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 7).Font.Bold = True
        xlapp.Cells(rowcnt, 7).Font.ColorIndex = 2 'vbWhite
        xlapp.Cells(rowcnt, 7).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 7) = "L03"
        xlapp.Cells(rowcnt, 8).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 8).Font.Bold = True
        xlapp.Cells(rowcnt, 8).Font.ColorIndex = 2 ' vbWhite
        xlapp.Cells(rowcnt, 8).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 8) = "L04"
        xlapp.Cells(rowcnt, 9).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 9).Font.Bold = True
        xlapp.Cells(rowcnt, 9).Font.ColorIndex = 2 ' vbWhite
        xlapp.Cells(rowcnt, 9).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 9) = "L05"
        xlapp.Cells(rowcnt, 10).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 10).Font.Bold = True
        xlapp.Cells(rowcnt, 10).Font.ColorIndex = 2 'vbWhite
        xlapp.Cells(rowcnt, 10).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 10) = "L06"
        xlapp.Cells(rowcnt, 11).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 11).Font.Bold = True
        xlapp.Cells(rowcnt, 11).Font.ColorIndex = 2 'vbWhite
        xlapp.Cells(rowcnt, 11).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 11) = "L07"
        xlapp.Cells(rowcnt, 12).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 12).Font.Bold = True
        xlapp.Cells(rowcnt, 12).Font.ColorIndex = 2 'vbWhite
        xlapp.Cells(rowcnt, 12).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 12) = "L08"
        xlapp.Cells(rowcnt, 13).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 13).Font.Bold = True
        xlapp.Cells(rowcnt, 13).Font.ColorIndex = 2 ' vbWhite
        xlapp.Cells(rowcnt, 13).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 13) = "L09"
        xlapp.Cells(rowcnt, 14).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 14).Font.Bold = True
        xlapp.Cells(rowcnt, 14).Font.ColorIndex = 2 ' vbWhite
        xlapp.Cells(rowcnt, 14).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 14) = "L10"
        xlapp.Cells(rowcnt, 15).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 15).Font.Bold = True
        xlapp.Cells(rowcnt, 15).Font.ColorIndex = 2 'vbWhite
        xlapp.Cells(rowcnt, 15).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 15) = "L11"
        xlapp.Cells(rowcnt, 16).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 16).Font.Bold = True
        xlapp.Cells(rowcnt, 16).Font.ColorIndex = 2 ' vbWhite
        xlapp.Cells(rowcnt, 16).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 16) = "L12"
        xlapp.Cells(rowcnt, 17).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 17).Font.Bold = True
        xlapp.Cells(rowcnt, 17).Font.ColorIndex = 2 ' vbWhite
        xlapp.Cells(rowcnt, 17).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 17) = "L13"
        xlapp.Cells(rowcnt, 18).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 18).Font.Bold = True
        xlapp.Cells(rowcnt, 18).Font.ColorIndex = 2 'vbWhite
        xlapp.Cells(rowcnt, 18).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 18) = "L14"
        xlapp.Cells(rowcnt, 19).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 19).Font.Bold = True
        xlapp.Cells(rowcnt, 19).Font.ColorIndex = 2 'vbWhite
        xlapp.Cells(rowcnt, 19).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 19) = "L15"
        xlapp.Cells(rowcnt, 20).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 20).Font.Bold = True
        xlapp.Cells(rowcnt, 20).Font.ColorIndex = 2 'vbWhite
        xlapp.Cells(rowcnt, 20).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 20) = "L16"
        xlapp.Cells(rowcnt, 21).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 21).Font.Bold = True
        xlapp.Cells(rowcnt, 21).Font.ColorIndex = 2 'vbWhite
        xlapp.Cells(rowcnt, 21).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 21) = "L17"
        xlapp.Cells(rowcnt, 22).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 22).Font.Bold = True
        xlapp.Cells(rowcnt, 22).Font.ColorIndex = 2 'vbWhite
        xlapp.Cells(rowcnt, 22).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 22) = "L18"
        xlapp.Cells(rowcnt, 23).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 23).Font.Bold = True
        xlapp.Cells(rowcnt, 23).Font.ColorIndex = 2 ' vbWhite
        xlapp.Cells(rowcnt, 23).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 23) = "L19"
        xlapp.Cells(rowcnt, 24).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 24).Font.Bold = True
        xlapp.Cells(rowcnt, 24).Font.ColorIndex = 2 'vbWhite
        xlapp.Cells(rowcnt, 24).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 24) = "L20"


        'If blnDeptAvailable And Not .EOF Then
        '    rowcnt = rowcnt + 1
        '    rowcnt = rowcnt + 1
        '    mintLine = mintLine + 3
        'End If

        'Do While Not .EOF
        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            intCounter = 1
            blnLeaveCheck1 = True
            blnLeaveCheck2 = True
            blnLeaveCheck3 = True
            blnLeaveCheck4 = True
            blnLeaveCheck5 = True
            blnLeaveCheck6 = True
            blnLeaveCheck7 = True
            blnLeaveCheck8 = True
            blnLeaveCheck9 = True
            blnLeaveCheck10 = True
            blnLeaveCheck11 = True
            blnLeaveCheck12 = True
            blnLeaveCheck13 = True
            blnLeaveCheck14 = True
            blnLeaveCheck15 = True
            blnLeaveCheck16 = True
            blnLeaveCheck17 = True
            blnLeaveCheck18 = True
            blnLeaveCheck19 = True
            blnLeaveCheck20 = True
            While intCounter <= intRecCountLeaveMaster

                If blnLeaveCheck1 Then
                    If strLeaveField3(intCounter) = "L01" And Rs_Report.Tables(0).Rows(i).Item("L01").ToString.Trim <> "" Then
                        strLeaveWholeLine(1) = Rs_Report.Tables(0).Rows(i).Item("L01").ToString.Trim
                        blnLeaveCheck1 = False
                    Else
                        strLeaveWholeLine(1) = ""
                    End If
                End If
                If blnLeaveCheck2 Then
                    If strLeaveField3(intCounter) = "L02" And Rs_Report.Tables(0).Rows(i).Item("L02").ToString.Trim <> "" Then
                        strLeaveWholeLine(2) = CStr(Rs_Report.Tables(0).Rows(i).Item("L02").ToString.Trim)
                        blnLeaveCheck2 = False
                    Else
                        strLeaveWholeLine(2) = ""
                    End If
                End If
                If blnLeaveCheck3 Then
                    If strLeaveField3(intCounter) = "L03" And Rs_Report.Tables(0).Rows(i).Item("L03").ToString.Trim <> "" Then
                        strLeaveWholeLine(3) = Rs_Report.Tables(0).Rows(i).Item("L03").ToString.Trim
                        blnLeaveCheck3 = False
                    Else
                        strLeaveWholeLine(3) = ""
                    End If
                End If
                If blnLeaveCheck4 Then
                    If strLeaveField3(intCounter) = "L04" And Rs_Report.Tables(0).Rows(i).Item("L04").ToString.Trim <> "" Then
                        strLeaveWholeLine(4) = Rs_Report.Tables(0).Rows(i).Item("L04").ToString.Trim
                        blnLeaveCheck4 = False
                    Else
                        strLeaveWholeLine(4) = ""
                    End If
                End If
                If blnLeaveCheck5 Then
                    If strLeaveField3(intCounter) = "L05" And Rs_Report.Tables(0).Rows(i).Item("L05").ToString.Trim <> "" Then
                        strLeaveWholeLine(5) = Rs_Report.Tables(0).Rows(i).Item("L05").ToString.Trim
                        blnLeaveCheck5 = False
                    Else
                        strLeaveWholeLine(5) = ""
                    End If
                End If
                If blnLeaveCheck6 Then
                    If strLeaveField3(intCounter) = "L06" And Rs_Report.Tables(0).Rows(i).Item("L06").ToString.Trim <> "" Then
                        strLeaveWholeLine(6) = Rs_Report.Tables(0).Rows(i).Item("L06").ToString.Trim
                        blnLeaveCheck6 = False
                    Else
                        strLeaveWholeLine(6) = ""
                    End If
                End If
                If blnLeaveCheck7 Then
                    If strLeaveField3(intCounter) = "L07" And Rs_Report.Tables(0).Rows(i).Item("L07").ToString.Trim <> "" Then
                        strLeaveWholeLine(7) = Rs_Report.Tables(0).Rows(i).Item("L07").ToString.Trim
                        blnLeaveCheck7 = False
                    Else
                        strLeaveWholeLine(7) = ""
                    End If
                End If
                If blnLeaveCheck8 Then
                    If strLeaveField3(intCounter) = "L08" And Rs_Report.Tables(0).Rows(i).Item("L08").ToString.Trim <> "" Then
                        strLeaveWholeLine(8) = Rs_Report.Tables(0).Rows(i).Item("L08").ToString.Trim
                        blnLeaveCheck8 = False
                    Else
                        strLeaveWholeLine(8) = ""
                    End If
                End If
                If blnLeaveCheck9 Then
                    If strLeaveField3(intCounter) = "L09" And Rs_Report.Tables(0).Rows(i).Item("L09").ToString.Trim <> "" Then
                        strLeaveWholeLine(9) = Rs_Report.Tables(0).Rows(i).Item("L09").ToString.Trim
                        blnLeaveCheck9 = False
                    Else
                        strLeaveWholeLine(9) = ""
                    End If
                End If
                If blnLeaveCheck10 Then
                    If strLeaveField3(intCounter) = "L10" And Rs_Report.Tables(0).Rows(i).Item("L10").ToString.Trim <> "" Then
                        strLeaveWholeLine(10) = Rs_Report.Tables(0).Rows(i).Item("L10").ToString.Trim
                        blnLeaveCheck10 = False
                    Else
                        strLeaveWholeLine(10) = ""
                    End If
                End If
                If blnLeaveCheck11 Then
                    If strLeaveField3(intCounter) = "L11" And Rs_Report.Tables(0).Rows(i).Item("L11").ToString.Trim <> "" Then
                        strLeaveWholeLine(11) = Rs_Report.Tables(0).Rows(i).Item("L11").ToString.Trim
                        blnLeaveCheck11 = False
                    Else
                        strLeaveWholeLine(11) = ""
                    End If
                End If
                If blnLeaveCheck12 Then
                    If strLeaveField3(intCounter) = "L12" And Rs_Report.Tables(0).Rows(i).Item("L12").ToString.Trim <> "" Then
                        strLeaveWholeLine(12) = Rs_Report.Tables(0).Rows(i).Item("L12").ToString.Trim
                        blnLeaveCheck12 = False
                    Else
                        strLeaveWholeLine(12) = ""
                    End If
                End If
                If blnLeaveCheck13 Then
                    If strLeaveField3(intCounter) = "L13" And Rs_Report.Tables(0).Rows(i).Item("L13").ToString.Trim <> "" Then
                        strLeaveWholeLine(13) = Rs_Report.Tables(0).Rows(i).Item("L13").ToString.Trim
                        blnLeaveCheck13 = False
                    Else
                        strLeaveWholeLine(13) = ""
                    End If
                End If
                If blnLeaveCheck14 Then
                    If strLeaveField3(intCounter) = "L14" And Rs_Report.Tables(0).Rows(i).Item("L14").ToString.Trim <> "" Then
                        strLeaveWholeLine(14) = Rs_Report.Tables(0).Rows(i).Item("L14").ToString.Trim
                        blnLeaveCheck14 = False
                    Else
                        strLeaveWholeLine(14) = ""
                    End If
                End If
                If blnLeaveCheck15 Then
                    If strLeaveField3(intCounter) = "L15" And Rs_Report.Tables(0).Rows(i).Item("L15").ToString.Trim <> "" Then
                        strLeaveWholeLine(15) = Rs_Report.Tables(0).Rows(i).Item("L15").ToString.Trim
                        blnLeaveCheck15 = False
                    Else
                        strLeaveWholeLine(15) = ""
                    End If
                End If
                If blnLeaveCheck16 Then
                    If strLeaveField3(intCounter) = "L16" And Rs_Report.Tables(0).Rows(i).Item("L16").ToString.Trim <> "" Then
                        strLeaveWholeLine(16) = Rs_Report.Tables(0).Rows(i).Item("L16").ToString.Trim
                        blnLeaveCheck16 = False
                    Else
                        strLeaveWholeLine(16) = ""
                    End If
                End If
                If blnLeaveCheck17 Then
                    If strLeaveField3(intCounter) = "L17" And Rs_Report.Tables(0).Rows(i).Item("L17").ToString.Trim <> "" Then
                        strLeaveWholeLine(17) = Rs_Report.Tables(0).Rows(i).Item("L17").ToString.Trim
                        blnLeaveCheck17 = False
                    Else
                        strLeaveWholeLine(17) = ""
                    End If
                End If
                If blnLeaveCheck18 Then
                    If strLeaveField3(intCounter) = "L18" And Rs_Report.Tables(0).Rows(i).Item("L18").ToString.Trim <> "" Then
                        strLeaveWholeLine(18) = Rs_Report.Tables(0).Rows(i).Item("L18").ToString.Trim
                        blnLeaveCheck18 = False
                    Else
                        strLeaveWholeLine(18) = ""
                    End If
                End If
                If blnLeaveCheck19 Then
                    If strLeaveField3(intCounter) = "L19" And Rs_Report.Tables(0).Rows(i).Item("L19").ToString.Trim <> "" Then
                        strLeaveWholeLine(19) = Rs_Report.Tables(0).Rows(i).Item("L19").ToString.Trim
                        blnLeaveCheck19 = False
                    Else
                        strLeaveWholeLine(19) = ""
                    End If
                End If
                If blnLeaveCheck20 Then
                    If strLeaveField3(intCounter) = "L20" And Rs_Report.Tables(0).Rows(i).Item("L20").ToString.Trim <> "" Then
                        strLeaveWholeLine(20) = Rs_Report.Tables(0).Rows(i).Item("L20").ToString.Trim
                        blnLeaveCheck20 = False
                    Else
                        strLeaveWholeLine(20) = ""
                    End If
                End If
                intCounter = intCounter + 1
            End While
            rowcnt = rowcnt + 1
            msrl = msrl + 1
            xlapp.Cells(rowcnt, 1).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 1) = msrl
            xlapp.Cells(rowcnt, 2).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 2) = "'" & Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim
            xlapp.Cells(rowcnt, 3).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 3) = Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim
            xlapp.Cells(rowcnt, 4).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 5).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 5) = "'" & (strLeaveWholeLine(1))
            xlapp.Cells(rowcnt, 6).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 6) = "'" & (strLeaveWholeLine(2))
            xlapp.Cells(rowcnt, 7).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 7) = "'" & (strLeaveWholeLine(3))
            xlapp.Cells(rowcnt, 8).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 8) = "'" & (strLeaveWholeLine(4))
            xlapp.Cells(rowcnt, 9).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 9) = "'" & (strLeaveWholeLine(5))
            xlapp.Cells(rowcnt, 10).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 10) = "'" & (strLeaveWholeLine(6))
            xlapp.Cells(rowcnt, 11).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 11) = "'" & (strLeaveWholeLine(7))
            xlapp.Cells(rowcnt, 12).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 12) = "'" & (strLeaveWholeLine(8))
            xlapp.Cells(rowcnt, 13).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 13) = "'" & (strLeaveWholeLine(9))
            xlapp.Cells(rowcnt, 14).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 14) = "'" & (strLeaveWholeLine(10))
            xlapp.Cells(rowcnt, 15).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 15) = "'" & (strLeaveWholeLine(11))
            xlapp.Cells(rowcnt, 16).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 16) = "'" & (strLeaveWholeLine(12))
            xlapp.Cells(rowcnt, 17).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 17) = "'" & (strLeaveWholeLine(13))
            xlapp.Cells(rowcnt, 18).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 18) = "'" & (strLeaveWholeLine(14))
            xlapp.Cells(rowcnt, 19).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 19) = "'" & (strLeaveWholeLine(15))
            xlapp.Cells(rowcnt, 20).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 20) = "'" & (strLeaveWholeLine(16))
            xlapp.Cells(rowcnt, 21).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 21) = "'" & (strLeaveWholeLine(17))
            xlapp.Cells(rowcnt, 22).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 22) = "'" & (strLeaveWholeLine(18))
            xlapp.Cells(rowcnt, 23).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 23) = "'" & (strLeaveWholeLine(19))
            xlapp.Cells(rowcnt, 24).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 24) = "'" & (strLeaveWholeLine(20))
            mintLine = mintLine + 1
            mblnCheckReport = True
            'Loop
        Next
        rowcnt = rowcnt + 1
        intCounter = 1
        While intCounter <= intRecCountLeaveMaster
            xlapp.Cells(rowcnt, 1).Font.Bold = True
            xlapp.Cells(rowcnt, 1) = strLeaveField3(intCounter) & " " & "-" & " " & strLeaveDescription50(intCounter) & Space(4)
            rowcnt = rowcnt + 1
            intCounter = intCounter + 1
        End While
        xlwb.SaveAs(mstrFile_Name)
    End Sub
    Sub MonthlyXl_ConsumedLeaveGrid(ByVal strsortorder As String)
        Dim strsql As String
        Dim strSqlLeaveMaster As String
        Dim rs_report_ReportLeaveMaster As DataSet 'ADODB.Recordset
        Dim strLeaveField3() As String ' * 3
        Dim strLeaveDescription50() As String '* 50
        Dim intRecCountLeaveMaster As Integer
        Dim intCounter As Integer
        Dim strLeaveWholeLine(20) As String ' 10
        Dim blnLeaveCheck1 As Boolean
        Dim blnLeaveCheck2 As Boolean
        Dim blnLeaveCheck3 As Boolean
        Dim blnLeaveCheck4 As Boolean
        Dim blnLeaveCheck5 As Boolean
        Dim blnLeaveCheck6 As Boolean
        Dim blnLeaveCheck7 As Boolean
        Dim blnLeaveCheck8 As Boolean
        Dim blnLeaveCheck9 As Boolean
        Dim blnLeaveCheck10 As Boolean
        Dim blnLeaveCheck11 As Boolean
        Dim blnLeaveCheck12 As Boolean
        Dim blnLeaveCheck13 As Boolean
        Dim blnLeaveCheck14 As Boolean
        Dim blnLeaveCheck15 As Boolean
        Dim blnLeaveCheck16 As Boolean
        Dim blnLeaveCheck17 As Boolean
        Dim blnLeaveCheck18 As Boolean
        Dim blnLeaveCheck19 As Boolean
        Dim blnLeaveCheck20 As Boolean
        Dim msrl As Integer
        'Dim xlapp As Excel.Application
        'Dim xlwb As Excel.Workbook
        Dim rowcnt As Integer
        'xlapp = CreateObject("Excel.Application")
        'xlwb = xlapp.Workbooks.Add
        mblnCheckReport = False
        mintPageNo = 1
        mintLine = 1
        If XtraShortOrder.g_SortOrder <> "" Then
            strsortorder = XtraShortOrder.g_SortOrder
        End If
        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If
        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".xls"
        If Common.servername = "Access" Then
            strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName,  tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tblLeaveLedger.PayCode,tblLeaveLedger.L01,tblLeaveLedger.L02,tblLeaveLedger.L03,tblLeaveLedger.L04,tblLeaveLedger.L05,tblLeaveLedger.L06,tblLeaveLedger.L07,tblLeaveLedger.L08,tblLeaveLedger.L09,tblLeaveLedger.L10,tblLeaveLedger.L11,tblLeaveLedger.L12,tblLeaveLedger.L13,tblLeaveLedger.L14," & _
                 "tblLeaveLedger.L15,tblLeaveLedger.L16,tblLeaveLedger.L17,tblLeaveLedger.L18,tblLeaveLedger.L19,tblLeaveLedger.L20,tblEmployee.EmpName,tblEmployee.PresentCardNo,tblDepartment.DepartmentName,tblEmployee.departmentcode" & _
                 " from tblCatagory,tblDivision,  tblLeaveLedger,tblEmployee,tblCompany,tblDepartment" & _
                 " Where tblCatagory.Cat = tblEmployee.Cat And tblLeaveLedger.PayCode = tblEmployee.PayCode" & _
                 " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
                 " and TBLLEAVELEDGER.LYEAR = " & TextEdit2.Text & " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & strsortorder
            adapA = New OleDbDataAdapter(strsql, Common.con1)
            adapA.Fill(Rs_Report)
        Else
            strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName,  tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tblLeaveLedger.PayCode,tblLeaveLedger.L01,tblLeaveLedger.L02,tblLeaveLedger.L03,tblLeaveLedger.L04,tblLeaveLedger.L05,tblLeaveLedger.L06,tblLeaveLedger.L07,tblLeaveLedger.L08,tblLeaveLedger.L09,tblLeaveLedger.L10,tblLeaveLedger.L11,tblLeaveLedger.L12,tblLeaveLedger.L13,tblLeaveLedger.L14," & _
                 "tblLeaveLedger.L15,tblLeaveLedger.L16,tblLeaveLedger.L17,tblLeaveLedger.L18,tblLeaveLedger.L19,tblLeaveLedger.L20,tblEmployee.EmpName,tblEmployee.PresentCardNo,tblDepartment.DepartmentName,tblEmployee.departmentcode" & _
                 " from tblCatagory,tblDivision,  tblLeaveLedger,tblEmployee,tblCompany,tblDepartment" & _
                 " Where tblCatagory.Cat = tblEmployee.Cat And tblLeaveLedger.PayCode = tblEmployee.PayCode" & _
                 " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
                 " and TBLLEAVELEDGER.LYEAR = " & TextEdit2.Text & " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & strsortorder
            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(Rs_Report)
        End If
        If Rs_Report.Tables(0).Rows.Count < 1 Then
            XtraMessageBox.Show(ulf, "<size=10>No Data Available For this Report.</size>", "<size=9>iAS</size>")
            Exit Sub
        End If
        strSqlLeaveMaster = " Select LeaveField,LeaveDescription from tblLeaveMaster order by LeaveField"
        rs_report_ReportLeaveMaster = New DataSet
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(strSqlLeaveMaster, Common.con1)
            adapA.Fill(rs_report_ReportLeaveMaster)
        Else
            adap = New SqlDataAdapter(strSqlLeaveMaster, Common.con)
            adap.Fill(rs_report_ReportLeaveMaster)
        End If
        For i As Integer = 0 To rs_report_ReportLeaveMaster.Tables(0).Rows.Count - 1 ' While Not rs_report_ReportLeaveMaster.EOF
            intRecCountLeaveMaster = intRecCountLeaveMaster + 1
        Next
        ReDim strLeaveField3(intRecCountLeaveMaster)
        ReDim strLeaveDescription50(intRecCountLeaveMaster)
        For i As Integer = 0 To rs_report_ReportLeaveMaster.Tables(0).Rows.Count - 1 ' While Not rs_report_ReportLeaveMaster.EOF
            intCounter = intCounter + 1
            strLeaveField3(intCounter) = rs_report_ReportLeaveMaster.Tables(0).Rows(i).Item("leavefield").ToString.Trim
            strLeaveDescription50(intCounter) = rs_report_ReportLeaveMaster.Tables(0).Rows(i).Item("LeaveDescription").ToString.Trim
        Next

        If Common.IsNepali = "Y" Then
            Common.frodatetodatetoReportGrid = " CONSUMED LEAVE FOR THE YEAR : " & tmpNepali
        Else
            Common.frodatetodatetoReportGrid = " CONSUMED LEAVE FOR THE YEAR : " & TextEdit2.Text
        End If

        rowcnt = rowcnt + 2

        Me.Cursor = Cursors.WaitCursor
        Common.tbl = New Data.DataTable()
        Common.tbl.Columns.Add("Srl", GetType(String))
        Common.tbl.Columns.Add("Paycode", GetType(String))
        Common.tbl.Columns.Add("Employee Name", GetType(String))
        Common.tbl.Columns.Add(" ", GetType(String))
        Common.tbl.Columns.Add("L01", GetType(String))
        Common.tbl.Columns.Add("L02", GetType(String))
        Common.tbl.Columns.Add("L03", GetType(String))
        Common.tbl.Columns.Add("L04", GetType(String))
        Common.tbl.Columns.Add("L05", GetType(String))
        Common.tbl.Columns.Add("L06", GetType(String))
        Common.tbl.Columns.Add("L07", GetType(String))
        Common.tbl.Columns.Add("L08", GetType(String))
        Common.tbl.Columns.Add("L09", GetType(String))
        Common.tbl.Columns.Add("L10", GetType(String))
        Common.tbl.Columns.Add("L11", GetType(String))
        Common.tbl.Columns.Add("L12", GetType(String))
        Common.tbl.Columns.Add("L13", GetType(String))
        Common.tbl.Columns.Add("L14", GetType(String))
        Common.tbl.Columns.Add("L15", GetType(String))
        Common.tbl.Columns.Add("L16", GetType(String))
        Common.tbl.Columns.Add("L17", GetType(String))
        Common.tbl.Columns.Add("L18", GetType(String))
        Common.tbl.Columns.Add("L19", GetType(String))
        Common.tbl.Columns.Add("L20", GetType(String))

        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            intCounter = 1
            blnLeaveCheck1 = True
            blnLeaveCheck2 = True
            blnLeaveCheck3 = True
            blnLeaveCheck4 = True
            blnLeaveCheck5 = True
            blnLeaveCheck6 = True
            blnLeaveCheck7 = True
            blnLeaveCheck8 = True
            blnLeaveCheck9 = True
            blnLeaveCheck10 = True
            blnLeaveCheck11 = True
            blnLeaveCheck12 = True
            blnLeaveCheck13 = True
            blnLeaveCheck14 = True
            blnLeaveCheck15 = True
            blnLeaveCheck16 = True
            blnLeaveCheck17 = True
            blnLeaveCheck18 = True
            blnLeaveCheck19 = True
            blnLeaveCheck20 = True
            While intCounter <= intRecCountLeaveMaster

                If blnLeaveCheck1 Then
                    If strLeaveField3(intCounter) = "L01" And Rs_Report.Tables(0).Rows(i).Item("L01").ToString.Trim <> "" Then
                        strLeaveWholeLine(1) = Rs_Report.Tables(0).Rows(i).Item("L01").ToString.Trim
                        blnLeaveCheck1 = False
                    Else
                        strLeaveWholeLine(1) = ""
                    End If
                End If
                If blnLeaveCheck2 Then
                    If strLeaveField3(intCounter) = "L02" And Rs_Report.Tables(0).Rows(i).Item("L02").ToString.Trim <> "" Then
                        strLeaveWholeLine(2) = CStr(Rs_Report.Tables(0).Rows(i).Item("L02").ToString.Trim)
                        blnLeaveCheck2 = False
                    Else
                        strLeaveWholeLine(2) = ""
                    End If
                End If
                If blnLeaveCheck3 Then
                    If strLeaveField3(intCounter) = "L03" And Rs_Report.Tables(0).Rows(i).Item("L03").ToString.Trim <> "" Then
                        strLeaveWholeLine(3) = Rs_Report.Tables(0).Rows(i).Item("L03").ToString.Trim
                        blnLeaveCheck3 = False
                    Else
                        strLeaveWholeLine(3) = ""
                    End If
                End If
                If blnLeaveCheck4 Then
                    If strLeaveField3(intCounter) = "L04" And Rs_Report.Tables(0).Rows(i).Item("L04").ToString.Trim <> "" Then
                        strLeaveWholeLine(4) = Rs_Report.Tables(0).Rows(i).Item("L04").ToString.Trim
                        blnLeaveCheck4 = False
                    Else
                        strLeaveWholeLine(4) = ""
                    End If
                End If
                If blnLeaveCheck5 Then
                    If strLeaveField3(intCounter) = "L05" And Rs_Report.Tables(0).Rows(i).Item("L05").ToString.Trim <> "" Then
                        strLeaveWholeLine(5) = Rs_Report.Tables(0).Rows(i).Item("L05").ToString.Trim
                        blnLeaveCheck5 = False
                    Else
                        strLeaveWholeLine(5) = ""
                    End If
                End If
                If blnLeaveCheck6 Then
                    If strLeaveField3(intCounter) = "L06" And Rs_Report.Tables(0).Rows(i).Item("L06").ToString.Trim <> "" Then
                        strLeaveWholeLine(6) = Rs_Report.Tables(0).Rows(i).Item("L06").ToString.Trim
                        blnLeaveCheck6 = False
                    Else
                        strLeaveWholeLine(6) = ""
                    End If
                End If
                If blnLeaveCheck7 Then
                    If strLeaveField3(intCounter) = "L07" And Rs_Report.Tables(0).Rows(i).Item("L07").ToString.Trim <> "" Then
                        strLeaveWholeLine(7) = Rs_Report.Tables(0).Rows(i).Item("L07").ToString.Trim
                        blnLeaveCheck7 = False
                    Else
                        strLeaveWholeLine(7) = ""
                    End If
                End If
                If blnLeaveCheck8 Then
                    If strLeaveField3(intCounter) = "L08" And Rs_Report.Tables(0).Rows(i).Item("L08").ToString.Trim <> "" Then
                        strLeaveWholeLine(8) = Rs_Report.Tables(0).Rows(i).Item("L08").ToString.Trim
                        blnLeaveCheck8 = False
                    Else
                        strLeaveWholeLine(8) = ""
                    End If
                End If
                If blnLeaveCheck9 Then
                    If strLeaveField3(intCounter) = "L09" And Rs_Report.Tables(0).Rows(i).Item("L09").ToString.Trim <> "" Then
                        strLeaveWholeLine(9) = Rs_Report.Tables(0).Rows(i).Item("L09").ToString.Trim
                        blnLeaveCheck9 = False
                    Else
                        strLeaveWholeLine(9) = ""
                    End If
                End If
                If blnLeaveCheck10 Then
                    If strLeaveField3(intCounter) = "L10" And Rs_Report.Tables(0).Rows(i).Item("L10").ToString.Trim <> "" Then
                        strLeaveWholeLine(10) = Rs_Report.Tables(0).Rows(i).Item("L10").ToString.Trim
                        blnLeaveCheck10 = False
                    Else
                        strLeaveWholeLine(10) = ""
                    End If
                End If
                If blnLeaveCheck11 Then
                    If strLeaveField3(intCounter) = "L11" And Rs_Report.Tables(0).Rows(i).Item("L11").ToString.Trim <> "" Then
                        strLeaveWholeLine(11) = Rs_Report.Tables(0).Rows(i).Item("L11").ToString.Trim
                        blnLeaveCheck11 = False
                    Else
                        strLeaveWholeLine(11) = ""
                    End If
                End If
                If blnLeaveCheck12 Then
                    If strLeaveField3(intCounter) = "L12" And Rs_Report.Tables(0).Rows(i).Item("L12").ToString.Trim <> "" Then
                        strLeaveWholeLine(12) = Rs_Report.Tables(0).Rows(i).Item("L12").ToString.Trim
                        blnLeaveCheck12 = False
                    Else
                        strLeaveWholeLine(12) = ""
                    End If
                End If
                If blnLeaveCheck13 Then
                    If strLeaveField3(intCounter) = "L13" And Rs_Report.Tables(0).Rows(i).Item("L13").ToString.Trim <> "" Then
                        strLeaveWholeLine(13) = Rs_Report.Tables(0).Rows(i).Item("L13").ToString.Trim
                        blnLeaveCheck13 = False
                    Else
                        strLeaveWholeLine(13) = ""
                    End If
                End If
                If blnLeaveCheck14 Then
                    If strLeaveField3(intCounter) = "L14" And Rs_Report.Tables(0).Rows(i).Item("L14").ToString.Trim <> "" Then
                        strLeaveWholeLine(14) = Rs_Report.Tables(0).Rows(i).Item("L14").ToString.Trim
                        blnLeaveCheck14 = False
                    Else
                        strLeaveWholeLine(14) = ""
                    End If
                End If
                If blnLeaveCheck15 Then
                    If strLeaveField3(intCounter) = "L15" And Rs_Report.Tables(0).Rows(i).Item("L15").ToString.Trim <> "" Then
                        strLeaveWholeLine(15) = Rs_Report.Tables(0).Rows(i).Item("L15").ToString.Trim
                        blnLeaveCheck15 = False
                    Else
                        strLeaveWholeLine(15) = ""
                    End If
                End If
                If blnLeaveCheck16 Then
                    If strLeaveField3(intCounter) = "L16" And Rs_Report.Tables(0).Rows(i).Item("L16").ToString.Trim <> "" Then
                        strLeaveWholeLine(16) = Rs_Report.Tables(0).Rows(i).Item("L16").ToString.Trim
                        blnLeaveCheck16 = False
                    Else
                        strLeaveWholeLine(16) = ""
                    End If
                End If
                If blnLeaveCheck17 Then
                    If strLeaveField3(intCounter) = "L17" And Rs_Report.Tables(0).Rows(i).Item("L17").ToString.Trim <> "" Then
                        strLeaveWholeLine(17) = Rs_Report.Tables(0).Rows(i).Item("L17").ToString.Trim
                        blnLeaveCheck17 = False
                    Else
                        strLeaveWholeLine(17) = ""
                    End If
                End If
                If blnLeaveCheck18 Then
                    If strLeaveField3(intCounter) = "L18" And Rs_Report.Tables(0).Rows(i).Item("L18").ToString.Trim <> "" Then
                        strLeaveWholeLine(18) = Rs_Report.Tables(0).Rows(i).Item("L18").ToString.Trim
                        blnLeaveCheck18 = False
                    Else
                        strLeaveWholeLine(18) = ""
                    End If
                End If
                If blnLeaveCheck19 Then
                    If strLeaveField3(intCounter) = "L19" And Rs_Report.Tables(0).Rows(i).Item("L19").ToString.Trim <> "" Then
                        strLeaveWholeLine(19) = Rs_Report.Tables(0).Rows(i).Item("L19").ToString.Trim
                        blnLeaveCheck19 = False
                    Else
                        strLeaveWholeLine(19) = ""
                    End If
                End If
                If blnLeaveCheck20 Then
                    If strLeaveField3(intCounter) = "L20" And Rs_Report.Tables(0).Rows(i).Item("L20").ToString.Trim <> "" Then
                        strLeaveWholeLine(20) = Rs_Report.Tables(0).Rows(i).Item("L20").ToString.Trim
                        blnLeaveCheck20 = False
                    Else
                        strLeaveWholeLine(20) = ""
                    End If
                End If
                intCounter = intCounter + 1
            End While
            rowcnt = rowcnt + 1
            msrl = msrl + 1
            Common.tbl.Rows.Add(msrl, Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim, Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim, "", _
                                strLeaveWholeLine(1), strLeaveWholeLine(2), strLeaveWholeLine(3), strLeaveWholeLine(4), strLeaveWholeLine(5), _
                                strLeaveWholeLine(6), strLeaveWholeLine(7), strLeaveWholeLine(8), strLeaveWholeLine(9), strLeaveWholeLine(10), _
                                strLeaveWholeLine(11), strLeaveWholeLine(12), strLeaveWholeLine(13), strLeaveWholeLine(14), strLeaveWholeLine(15), _
                                strLeaveWholeLine(16), strLeaveWholeLine(17), strLeaveWholeLine(18), strLeaveWholeLine(19), strLeaveWholeLine(20))
            mintLine = mintLine + 1
            mblnCheckReport = True
            'Loop
        Next
        rowcnt = rowcnt + 1
        intCounter = 1
        Common.tbl.Rows.Add("")
        While intCounter <= intRecCountLeaveMaster
            Common.tbl.Rows.Add(strLeaveField3(intCounter) & " " & "-" & " ", strLeaveDescription50(intCounter))
            rowcnt = rowcnt + 1
            intCounter = intCounter + 1
        End While
        Me.Cursor = Cursors.Default
        XtraReportGrid.ShowDialog()
    End Sub
    Sub Monthly_BalanceLeave(ByVal strsortorder As String)
        Dim strDeptDivCode As String
        Dim strCurrentDeptDivCode As String
        Dim intFile As Integer
        Dim strsql As String
        Dim strSqlLeaveMaster As String
        Dim rs_report_ReportLeaveMaster As DataSet 'ADODB.Recordset
        Dim strLeaveField3() As String '* 3
        Dim strLeaveDescription50() As String '* 50
        Dim intRecCountLeaveMaster As Integer
        Dim intCounter As Integer
        Dim strLeaveWholeLine(20) As String '* 10
        Dim blnLeaveCheck1 As Boolean, blnLeaveCheck2 As Boolean, blnLeaveCheck3 As Boolean, blnLeaveCheck4 As Boolean
        Dim blnLeaveCheck5 As Boolean, blnLeaveCheck6 As Boolean, blnLeaveCheck7 As Boolean, blnLeaveCheck8 As Boolean
        Dim blnLeaveCheck9 As Boolean, blnLeaveCheck10 As Boolean, blnLeaveCheck11 As Boolean, blnLeaveCheck12 As Boolean
        Dim blnLeaveCheck13 As Boolean, blnLeaveCheck14 As Boolean, blnLeaveCheck15 As Boolean, blnLeaveCheck16 As Boolean
        Dim blnLeaveCheck17 As Boolean, blnLeaveCheck18 As Boolean, blnLeaveCheck19 As Boolean, blnLeaveCheck20 As Boolean
        Dim blnPrintLine As Boolean
        Dim strDepartmentCode As String
        Dim intPrintLineCounter As Integer
        Dim blnDeptAvailable As Boolean, mFirst As Boolean
        Dim msrl As Integer
        msrl = 1

        mblnCheckReport = False
        mFirst = True
        mintPageNo = 1
        mintLine = 1

        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If
        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet
        If Common.servername = "Access" Then
            strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tblLeaveLedger.PayCode,tblLeaveLedger.L01,tblLeaveLedger.L02,tblLeaveLedger.L03,tblLeaveLedger.L04,tblLeaveLedger.L05,tblLeaveLedger.L06,tblLeaveLedger.L07,tblLeaveLedger.L08,tblLeaveLedger.L09,tblLeaveLedger.L10,tblLeaveLedger.L11,tblLeaveLedger.L12,tblLeaveLedger.L13,tblLeaveLedger.L14,tblLeaveLedger.L15,tblLeaveLedger.L16,tblLeaveLedger.L17,tblLeaveLedger.L18,tblLeaveLedger.L19,tblLeaveLedger.L20," & _
                 "tblLeaveLedger.L01_ADD,tblLeaveLedger.L02_ADD,tblLeaveLedger.L03_ADD,tblLeaveLedger.L04_ADD,tblLeaveLedger.L05_ADD,tblLeaveLedger.L06_ADD,tblLeaveLedger.L07_ADD,tblLeaveLedger.L08_ADD,tblLeaveLedger.L09_ADD,tblLeaveLedger.L10_ADD,tblLeaveLedger.L11_ADD,tblLeaveLedger.L12_ADD,tblLeaveLedger.L13_ADD,tblLeaveLedger.L14_ADD," & _
                 "tblLeaveLedger.L15_ADD,tblLeaveLedger.L16_ADD,tblLeaveLedger.L17_ADD,tblLeaveLedger.L18_ADD,tblLeaveLedger.L19_ADD,tblLeaveLedger.L20_ADD,tblEmployee.EmpName,tblEmployee.PresentCardNo,tblDepartment.DepartmentName,tblEmployee.departmentcode" & _
                 " from tblCatagory,tblDivision,  tblLeaveLedger,tblEmployee,tblCompany,tblDepartment" & _
                " Where tblCatagory.Cat = tblEmployee.Cat And tblLeaveLedger.PayCode = tblEmployee.PayCode" & _
                " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
                " and TBLLEAVELEDGER.LYEAR = " & TextEdit2.Text & " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & " order by tblEmployee.PayCode"
            adapA = New OleDbDataAdapter(strsql, Common.con1)
            adapA.Fill(Rs_Report)
        Else
            strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tblLeaveLedger.PayCode,tblLeaveLedger.L01,tblLeaveLedger.L02,tblLeaveLedger.L03,tblLeaveLedger.L04,tblLeaveLedger.L05,tblLeaveLedger.L06,tblLeaveLedger.L07,tblLeaveLedger.L08,tblLeaveLedger.L09,tblLeaveLedger.L10,tblLeaveLedger.L11,tblLeaveLedger.L12,tblLeaveLedger.L13,tblLeaveLedger.L14,tblLeaveLedger.L15,tblLeaveLedger.L16,tblLeaveLedger.L17,tblLeaveLedger.L18,tblLeaveLedger.L19,tblLeaveLedger.L20," & _
                 "tblLeaveLedger.L01_ADD,tblLeaveLedger.L02_ADD,tblLeaveLedger.L03_ADD,tblLeaveLedger.L04_ADD,tblLeaveLedger.L05_ADD,tblLeaveLedger.L06_ADD,tblLeaveLedger.L07_ADD,tblLeaveLedger.L08_ADD,tblLeaveLedger.L09_ADD,tblLeaveLedger.L10_ADD,tblLeaveLedger.L11_ADD,tblLeaveLedger.L12_ADD,tblLeaveLedger.L13_ADD,tblLeaveLedger.L14_ADD," & _
                 "tblLeaveLedger.L15_ADD,tblLeaveLedger.L16_ADD,tblLeaveLedger.L17_ADD,tblLeaveLedger.L18_ADD,tblLeaveLedger.L19_ADD,tblLeaveLedger.L20_ADD,tblEmployee.EmpName,tblEmployee.PresentCardNo,tblDepartment.DepartmentName,tblEmployee.departmentcode" & _
                 " from tblCatagory,tblDivision,  tblLeaveLedger,tblEmployee,tblCompany,tblDepartment" & _
                " Where tblCatagory.Cat = tblEmployee.Cat And tblLeaveLedger.PayCode = tblEmployee.PayCode" & _
                " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
                " and TBLLEAVELEDGER.LYEAR = " & TextEdit2.Text & " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & " order by tblEmployee.PayCode"
            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(Rs_Report)
        End If
        If Rs_Report.Tables(0).Rows.Count < 1 Then
            XtraMessageBox.Show(ulf, "<size=10>No Data Available For this Report.</size>", "<size=9>iAS</size>")
            Exit Sub
        End If

        strSqlLeaveMaster = " Select LeaveField,LeaveDescription from tblLeaveMaster order by LeaveField"
        rs_report_ReportLeaveMaster = New DataSet
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(strSqlLeaveMaster, Common.con1)
            adapA.Fill(rs_report_ReportLeaveMaster)
        Else
            adap = New SqlDataAdapter(strSqlLeaveMaster, Common.con)
            adap.Fill(rs_report_ReportLeaveMaster)
        End If
        For i As Integer = 0 To rs_report_ReportLeaveMaster.Tables(0).Rows.Count - 1 ' While Not rs_report_ReportLeaveMaster.EOF
            intRecCountLeaveMaster = intRecCountLeaveMaster + 1
        Next
        ReDim strLeaveField3(intRecCountLeaveMaster)
        ReDim strLeaveDescription50(intRecCountLeaveMaster)
        For i As Integer = 0 To rs_report_ReportLeaveMaster.Tables(0).Rows.Count - 1 ' While Not rs_report_ReportLeaveMaster.EOF
            intCounter = intCounter + 1
            strLeaveField3(intCounter) = rs_report_ReportLeaveMaster.Tables(0).Rows(i).Item("leavefield").ToString.Trim
            strLeaveDescription50(intCounter) = rs_report_ReportLeaveMaster.Tables(0).Rows(i).Item("LeaveDescription").ToString.Trim
        Next
        mFileNumber = FreeFile()
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".SRP"
        Dim objWriter As New System.IO.StreamWriter(mstrFile_Name, True)

        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            If mFirst Or mintLine > g_LinesPerPage Or (strsortorder.Trim = "Department" And strDeptDivCode <> Rs_Report.Tables(0).Rows(i).Item("DepartmentCode").ToString.Trim And g_SkipAfterDept) Then
1:              If Not mFirst Then
                    objWriter.WriteLine(Space(2) & vbFormFeed)
                Else
                    mFirst = False
                End If
                objWriter.WriteLine(Space(42) & CommonReport.g_CompanyNames)
                objWriter.WriteLine(Space(2) & "")
                objWriter.WriteLine(Space(2) & "                                                                                                                                                         Run Date & Time : " & Common.runningDateTime) 'Now.ToString("dd/MM/yyyy HH:mm"))
                If Common.IsNepali = "Y" Then
                    objWriter.WriteLine(Space(2) & Space(40) & "Balance Leaves Report for the Year " & tmpNepali)
                Else
                    objWriter.WriteLine(Space(2) & Space(40) & "Balance Leaves Report for the Year " & TextEdit2.Text & "")
                End If
                objWriter.WriteLine(Space(2) & "-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------")
                objWriter.WriteLine(Space(2) & "Srl  Payroll    Employee Name              -------------------------------------------------------------------------------Balance----------------------------------------------------------------------------------------------------")
                objWriter.WriteLine(Space(2) & "No.                                             L01      L02      L03      L04      L05      L06      L07      L08      L09      L10      L11      L12      L13      L14      L15      L16      L17      L18      L19      L20")
                objWriter.WriteLine(Space(2) & "-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------")
                mintLine = 9
                mintPageNo = mintPageNo + 1
            End If

            intCounter = 1
            blnLeaveCheck1 = True
            blnLeaveCheck2 = True
            blnLeaveCheck3 = True
            blnLeaveCheck4 = True
            blnLeaveCheck5 = True
            blnLeaveCheck6 = True
            blnLeaveCheck7 = True
            blnLeaveCheck8 = True
            blnLeaveCheck9 = True
            blnLeaveCheck10 = True
            blnLeaveCheck11 = True
            blnLeaveCheck12 = True
            blnLeaveCheck13 = True
            blnLeaveCheck14 = True
            blnLeaveCheck15 = True
            blnLeaveCheck16 = True
            blnLeaveCheck17 = True
            blnLeaveCheck18 = True
            blnLeaveCheck19 = True
            blnLeaveCheck20 = True
            While intCounter <= intRecCountLeaveMaster
                If blnLeaveCheck1 Then
                    If strLeaveField3(intCounter) = "L01" And Rs_Report.Tables(0).Rows(i).Item("L01").ToString.Trim <> "" Then
                        strLeaveWholeLine(1) = Rs_Report.Tables(0).Rows(i).Item("L01_ADD").ToString.Trim - Rs_Report.Tables(0).Rows(i).Item("L01").ToString.Trim
                        blnLeaveCheck1 = False
                    Else
                        strLeaveWholeLine(1) = ""
                    End If
                End If
                If blnLeaveCheck2 Then
                    If strLeaveField3(intCounter) = "L02" And Rs_Report.Tables(0).Rows(i).Item("L02").ToString.Trim <> "" Then
                        strLeaveWholeLine(2) = Rs_Report.Tables(0).Rows(i).Item("L02_ADD").ToString.Trim - Rs_Report.Tables(0).Rows(i).Item("L02").ToString.Trim
                        blnLeaveCheck2 = False
                    Else
                        strLeaveWholeLine(2) = ""
                    End If
                End If
                If blnLeaveCheck3 Then
                    If strLeaveField3(intCounter) = "L03" And Rs_Report.Tables(0).Rows(i).Item("L03").ToString.Trim <> "" Then
                        strLeaveWholeLine(3) = Rs_Report.Tables(0).Rows(i).Item("L03_ADD").ToString.Trim - Rs_Report.Tables(0).Rows(i).Item("L03").ToString.Trim
                        blnLeaveCheck3 = False
                    Else
                        strLeaveWholeLine(3) = ""
                    End If
                End If
                If blnLeaveCheck4 Then
                    If strLeaveField3(intCounter) = "L04" And Rs_Report.Tables(0).Rows(i).Item("L04").ToString.Trim <> "" Then
                        strLeaveWholeLine(4) = Rs_Report.Tables(0).Rows(i).Item("L04_ADD").ToString.Trim - Rs_Report.Tables(0).Rows(i).Item("L04").ToString.Trim
                        blnLeaveCheck4 = False
                    Else
                        strLeaveWholeLine(4) = ""
                    End If
                End If
                If blnLeaveCheck5 Then
                    If strLeaveField3(intCounter) = "L05" And Rs_Report.Tables(0).Rows(i).Item("L05").ToString.Trim <> "" Then
                        strLeaveWholeLine(5) = Rs_Report.Tables(0).Rows(i).Item("L05_ADD").ToString.Trim - Rs_Report.Tables(0).Rows(i).Item("L05").ToString.Trim
                        blnLeaveCheck5 = False
                    Else
                        strLeaveWholeLine(5) = ""
                    End If
                End If
                If blnLeaveCheck6 Then
                    If strLeaveField3(intCounter) = "L06" And Rs_Report.Tables(0).Rows(i).Item("L06").ToString.Trim <> "" Then
                        strLeaveWholeLine(6) = Rs_Report.Tables(0).Rows(i).Item("L06_ADD").ToString.Trim - Rs_Report.Tables(0).Rows(i).Item("L06").ToString.Trim
                        blnLeaveCheck6 = False
                    Else
                        strLeaveWholeLine(6) = ""
                    End If
                End If
                If blnLeaveCheck7 Then
                    If strLeaveField3(intCounter) = "L07" And Rs_Report.Tables(0).Rows(i).Item("L07").ToString.Trim <> "" Then
                        strLeaveWholeLine(7) = Rs_Report.Tables(0).Rows(i).Item("L07_ADD").ToString.Trim - Rs_Report.Tables(0).Rows(i).Item("L07").ToString.Trim
                        blnLeaveCheck7 = False
                    Else
                        strLeaveWholeLine(7) = ""
                    End If
                End If
                If blnLeaveCheck8 Then
                    If strLeaveField3(intCounter) = "L08" And Rs_Report.Tables(0).Rows(i).Item("L08").ToString.Trim <> "" Then
                        strLeaveWholeLine(8) = Rs_Report.Tables(0).Rows(i).Item("L08_ADD").ToString.Trim - Rs_Report.Tables(0).Rows(i).Item("L08").ToString.Trim
                        blnLeaveCheck8 = False
                    Else
                        strLeaveWholeLine(8) = ""
                    End If
                End If
                If blnLeaveCheck9 Then
                    If strLeaveField3(intCounter) = "L09" And Rs_Report.Tables(0).Rows(i).Item("L09").ToString.Trim <> "" Then
                        strLeaveWholeLine(9) = Rs_Report.Tables(0).Rows(i).Item("L09_ADD").ToString.Trim - Rs_Report.Tables(0).Rows(i).Item("L09").ToString.Trim
                        blnLeaveCheck9 = False
                    Else
                        strLeaveWholeLine(9) = ""
                    End If
                End If
                If blnLeaveCheck10 Then
                    If strLeaveField3(intCounter) = "L10" And Rs_Report.Tables(0).Rows(i).Item("L10").ToString.Trim <> "" Then
                        strLeaveWholeLine(10) = Rs_Report.Tables(0).Rows(i).Item("L10_ADD").ToString.Trim - Rs_Report.Tables(0).Rows(i).Item("L10").ToString.Trim
                        blnLeaveCheck10 = False
                    Else
                        strLeaveWholeLine(10) = ""
                    End If
                End If
                If blnLeaveCheck11 Then
                    If strLeaveField3(intCounter) = "L11" And Rs_Report.Tables(0).Rows(i).Item("L11").ToString.Trim <> "" Then
                        strLeaveWholeLine(11) = Rs_Report.Tables(0).Rows(i).Item("L11_ADD").ToString.Trim - Rs_Report.Tables(0).Rows(i).Item("L11").ToString.Trim
                        blnLeaveCheck11 = False
                    Else
                        strLeaveWholeLine(11) = ""
                    End If
                End If
                If blnLeaveCheck12 Then
                    If strLeaveField3(intCounter) = "L12" And Rs_Report.Tables(0).Rows(i).Item("L12").ToString.Trim <> "" Then
                        strLeaveWholeLine(12) = Rs_Report.Tables(0).Rows(i).Item("L12_ADD").ToString.Trim - Rs_Report.Tables(0).Rows(i).Item("L12").ToString.Trim
                        blnLeaveCheck12 = False
                    Else
                        strLeaveWholeLine(12) = ""
                    End If
                End If
                If blnLeaveCheck13 Then
                    If strLeaveField3(intCounter) = "L13" And Rs_Report.Tables(0).Rows(i).Item("L13").ToString.Trim <> "" Then
                        strLeaveWholeLine(13) = Rs_Report.Tables(0).Rows(i).Item("L13_ADD").ToString.Trim - Rs_Report.Tables(0).Rows(i).Item("L13").ToString.Trim
                        blnLeaveCheck13 = False
                    Else
                        strLeaveWholeLine(13) = ""
                    End If
                End If
                If blnLeaveCheck14 Then
                    If strLeaveField3(intCounter) = "L14" And Rs_Report.Tables(0).Rows(i).Item("L14").ToString.Trim <> "" Then
                        strLeaveWholeLine(14) = Rs_Report.Tables(0).Rows(i).Item("L14_ADD").ToString.Trim - Rs_Report.Tables(0).Rows(i).Item("L14").ToString.Trim
                        blnLeaveCheck14 = False
                    Else
                        strLeaveWholeLine(14) = ""
                    End If
                End If
                If blnLeaveCheck15 Then
                    If strLeaveField3(intCounter) = "L15" And Rs_Report.Tables(0).Rows(i).Item("L15").ToString.Trim <> "" Then
                        strLeaveWholeLine(15) = Rs_Report.Tables(0).Rows(i).Item("L15_ADD").ToString.Trim - Rs_Report.Tables(0).Rows(i).Item("L15").ToString.Trim
                        blnLeaveCheck15 = False
                    Else
                        strLeaveWholeLine(15) = ""
                    End If
                End If
                If blnLeaveCheck16 Then
                    If strLeaveField3(intCounter) = "L16" And Rs_Report.Tables(0).Rows(i).Item("L16").ToString.Trim <> "" Then
                        strLeaveWholeLine(16) = Rs_Report.Tables(0).Rows(i).Item("L16_ADD").ToString.Trim - Rs_Report.Tables(0).Rows(i).Item("L16").ToString.Trim
                        blnLeaveCheck16 = False
                    Else
                        strLeaveWholeLine(16) = ""
                    End If
                End If
                If blnLeaveCheck17 Then
                    If strLeaveField3(intCounter) = "L17" And Rs_Report.Tables(0).Rows(i).Item("L17").ToString.Trim <> "" Then
                        strLeaveWholeLine(17) = Rs_Report.Tables(0).Rows(i).Item("L17_ADD").ToString.Trim - Rs_Report.Tables(0).Rows(i).Item("L17").ToString.Trim
                        blnLeaveCheck17 = False
                    Else
                        strLeaveWholeLine(17) = ""
                    End If
                End If
                If blnLeaveCheck18 Then
                    If strLeaveField3(intCounter) = "L18" And Rs_Report.Tables(0).Rows(i).Item("L18").ToString.Trim <> "" Then
                        strLeaveWholeLine(18) = Rs_Report.Tables(0).Rows(i).Item("L18_ADD").ToString.Trim - Rs_Report.Tables(0).Rows(i).Item("L18").ToString.Trim
                        blnLeaveCheck18 = False
                    Else
                        strLeaveWholeLine(18) = ""
                    End If
                End If
                If blnLeaveCheck19 Then
                    If strLeaveField3(intCounter) = "L19" And Rs_Report.Tables(0).Rows(i).Item("L19").ToString.Trim <> "" Then
                        strLeaveWholeLine(19) = Rs_Report.Tables(0).Rows(i).Item("L19_ADD").ToString.Trim - Rs_Report.Tables(0).Rows(i).Item("L19").ToString.Trim
                        blnLeaveCheck19 = False
                    Else
                        strLeaveWholeLine(19) = ""
                    End If
                End If
                If blnLeaveCheck20 Then
                    If strLeaveField3(intCounter) = "L20" And Rs_Report.Tables(0).Rows(i).Item("L20").ToString.Trim <> "" Then
                        strLeaveWholeLine(20) = Rs_Report.Tables(0).Rows(i).Item("L20_ADD").ToString.Trim - Rs_Report.Tables(0).Rows(i).Item("L20").ToString.Trim
                        blnLeaveCheck20 = False
                    Else
                        strLeaveWholeLine(20) = ""
                    End If
                End If
                intCounter = intCounter + 1
            End While

            objWriter.WriteLine(Space(2) & msrl.ToString.PadRight(5) & " " & Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim.PadRight(12) & Space(1) & Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim.PadRight(25) & Space(1) & _
             Space(3) & strLeaveWholeLine(1).PadRight(6) & Space(3) & strLeaveWholeLine(2).PadRight(6) & Space(3) & strLeaveWholeLine(3).PadRight(6) & Space(3) & strLeaveWholeLine(4).PadRight(6) & Space(3) & _
             Space(3) & strLeaveWholeLine(5).PadRight(6) & Space(3) & strLeaveWholeLine(6).PadRight(6) & Space(3) & strLeaveWholeLine(7).PadRight(6) & Space(3) & strLeaveWholeLine(8).PadRight(6) & Space(3) & _
             Space(3) & strLeaveWholeLine(9).PadRight(6) & Space(3) & strLeaveWholeLine(10).PadRight(6) & Space(3) & strLeaveWholeLine(11).PadRight(6) & Space(3) & strLeaveWholeLine(12).PadRight(6) & Space(3) & _
             Space(3) & strLeaveWholeLine(13).PadRight(6) & Space(3) & strLeaveWholeLine(14).PadRight(6) & Space(3) & strLeaveWholeLine(15).PadRight(6) & Space(3) & strLeaveWholeLine(16).PadRight(6) & Space(3) & _
             Space(3) & strLeaveWholeLine(17).PadRight(6) & Space(3) & strLeaveWholeLine(18).PadRight(6) & Space(3) & strLeaveWholeLine(19).PadRight(6) & Space(3) & strLeaveWholeLine(20).PadRight(6))

            'Print #intFile, Space(2) + Format(msrl, "@@@@") & " " & !paycode & Space(1) & !EmpName & Space(1) & _
            '    String(7 - Len(Trim(Format(strLeaveWholeLine(1), "0.00"))), " ") & Trim(Format(strLeaveWholeLine(1), "0.00")) & Space(3) & String(7 - Len(Trim(Format(strLeaveWholeLine(2), "0.00"))), " ") & Trim(Format(strLeaveWholeLine(2), "0.00")) & Space(3) & String(7 - Len(Trim(Format(strLeaveWholeLine(3), "0.00"))), " ") & Trim(Format(strLeaveWholeLine(3), "0.00")) & Space(3) & String(7 - Len(Trim(Format(strLeaveWholeLine(4), "0.00"))), " ") & Trim(Format(strLeaveWholeLine(4), "0.00")) & Space(3) & _
            '    String(7 - Len(Trim(Format(strLeaveWholeLine(5), "0.00"))), " ") & Trim(Format(strLeaveWholeLine(5), "0.00")) & Space(3) & String(7 - Len(Trim(Format(strLeaveWholeLine(6), "0.00"))), " ") & Trim(Format(strLeaveWholeLine(6), "0.00")) & Space(3) & String(7 - Len(Trim(Format(strLeaveWholeLine(7), "0.00"))), " ") & Trim(Format(strLeaveWholeLine(7), "0.00")) & Space(3) & String(7 - Len(Trim(Format(strLeaveWholeLine(8), "0.00"))), " ") & Trim(Format(strLeaveWholeLine(8), "0.00")) & Space(3) & _
            '    String(7 - Len(Trim(Format(strLeaveWholeLine(9), "0.00"))), " ") & Trim(Format(strLeaveWholeLine(9), "0.00")) & Space(3) & String(7 - Len(Trim(Format(strLeaveWholeLine(10), "0.00"))), " ") & Trim(Format(strLeaveWholeLine(10), "0.00")) & Space(3) & String(7 - Len(Trim(Format(strLeaveWholeLine(11), "0.00"))), " ") & Trim(Format(strLeaveWholeLine(11), "0.00")) & Space(3) & String(7 - Len(Trim(Format(strLeaveWholeLine(12), "0.00"))), " ") & Trim(Format(strLeaveWholeLine(12), "0.00")) & Space(3) & _
            'String(7 - Len(Trim(Format(strLeaveWholeLine(13), "0.00"))), " ") & Trim(Format(strLeaveWholeLine(13), "0.00")) & Space(3) & String(7 - Len(Trim(Format(strLeaveWholeLine(14), "0.00"))), " ") & Trim(Format(strLeaveWholeLine(14), "0.00")) & Space(3) & String(7 - Len(Trim(Format(strLeaveWholeLine(15), "0.00"))), " ") & Trim(Format(strLeaveWholeLine(15), "0.00")) & Space(3) & String(7 - Len(Trim(Format(strLeaveWholeLine(16), "0.00"))), " ") & Trim(Format(strLeaveWholeLine(16), "0.00")) & Space(3) & _
            'String(7 - Len(Trim(Format(strLeaveWholeLine(17), "0.00"))), " ") & Trim(Format(strLeaveWholeLine(17), "0.00")) & Space(3) & String(7 - Len(Trim(Format(strLeaveWholeLine(18), "0.00"))), " ") & Trim(Format(strLeaveWholeLine(18), "0.00")) & Space(3) & String(7 - Len(Trim(Format(strLeaveWholeLine(19), "0.00"))), " ") & Trim(Format(strLeaveWholeLine(19), "0.00")) & Space(3) & String(7 - Len(Trim(Format(strLeaveWholeLine(20), "0.00"))), " ") & Trim(Format(strLeaveWholeLine(20), "0.00"))
            msrl = msrl + 1
            mintLine = mintLine + 1
            mblnCheckReport = True
            '.MoveNext()
            'Loop
        Next
        intCounter = 1
        objWriter.WriteLine(Space(2) & "")
        objWriter.WriteLine(Space(2) & "")
        While intCounter <= intRecCountLeaveMaster
            objWriter.WriteLine(Space(2) & strLeaveField3(intCounter) & " " & "-" & " " & strLeaveDescription50(intCounter) & Space(4))
            If intCounter Mod 3 = 0 Then objWriter.WriteLine(Space(2) & "")
            intCounter = intCounter + 1
        End While
        objWriter.Close()
        Try
            Process.Start(mstrFile_Name)
        Catch ex As Exception
            Process.Start("notepad.exe", mstrFile_Name)
        End Try
    End Sub
    Sub MonthlyXl_BalanceLeave(ByVal strsortorder As String)
        Dim strDeptDivCode As String
        Dim strCurrentDeptDivCode As String
        Dim strsql As String
        Dim strSqlLeaveMaster As String
        Dim rs_report_ReportLeaveMaster As DataSet 'ADODB.Recordset
        Dim strLeaveField3() As String '* 3
        Dim strLeaveDescription50() As String '* 50
        Dim intRecCountLeaveMaster As Integer
        Dim intCounter As Integer
        Dim intFile As Integer
        Dim strLeaveWholeLine(20) As String '* 10
        Dim blnLeaveCheck1 As Boolean
        Dim blnLeaveCheck2 As Boolean
        Dim blnLeaveCheck3 As Boolean
        Dim blnLeaveCheck4 As Boolean
        Dim blnLeaveCheck5 As Boolean
        Dim blnLeaveCheck6 As Boolean
        Dim blnLeaveCheck7 As Boolean
        Dim blnLeaveCheck8 As Boolean
        Dim blnLeaveCheck9 As Boolean
        Dim blnLeaveCheck10 As Boolean
        Dim blnLeaveCheck11 As Boolean
        Dim blnLeaveCheck12 As Boolean
        Dim blnLeaveCheck13 As Boolean
        Dim blnLeaveCheck14 As Boolean
        Dim blnLeaveCheck15 As Boolean
        Dim blnLeaveCheck16 As Boolean
        Dim blnLeaveCheck17 As Boolean
        Dim blnLeaveCheck18 As Boolean
        Dim blnLeaveCheck19 As Boolean
        Dim blnLeaveCheck20 As Boolean
        Dim blnPrintLine As Boolean
        Dim strDepartmentCode As String
        Dim intPrintLineCounter As Integer
        Dim blnDeptAvailable As Boolean, mFirst As Boolean
        Dim msrl As Integer
        Dim xlapp As Excel.Application
        Dim xlwb As Excel.Workbook
        Dim xlst As Excel.Sheets
        Dim rowcnt As Integer

        xlapp = CreateObject("Excel.Application")
        xlwb = xlapp.Workbooks.Add
        'If UCase(Dir(App.Path & "\TimeWatch.xls")) = UCase("TimeWatch.xls") Then
        '    Kill(App.Path & "\TimeWatch.xls")
        'End If
        mblnCheckReport = False
        mintPageNo = 1
        mintLine = 1
        If XtraShortOrder.g_SortOrder <> "" Then
            strsortorder = XtraShortOrder.g_SortOrder
        End If
        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If
        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".xls"

        If Common.servername = "Access" Then
            strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tblLeaveLedger.PayCode,tblLeaveLedger.L01,tblLeaveLedger.L02,tblLeaveLedger.L03,tblLeaveLedger.L04,tblLeaveLedger.L05,tblLeaveLedger.L06,tblLeaveLedger.L07,tblLeaveLedger.L08,tblLeaveLedger.L09,tblLeaveLedger.L10,tblLeaveLedger.L11,tblLeaveLedger.L12,tblLeaveLedger.L13,tblLeaveLedger.L14,tblLeaveLedger.L15,tblLeaveLedger.L16,tblLeaveLedger.L17,tblLeaveLedger.L18,tblLeaveLedger.L19,tblLeaveLedger.L20," & _
                 "tblLeaveLedger.L01_ADD,tblLeaveLedger.L02_ADD,tblLeaveLedger.L03_ADD,tblLeaveLedger.L04_ADD,tblLeaveLedger.L05_ADD,tblLeaveLedger.L06_ADD,tblLeaveLedger.L07_ADD,tblLeaveLedger.L08_ADD,tblLeaveLedger.L09_ADD,tblLeaveLedger.L10_ADD,tblLeaveLedger.L11_ADD,tblLeaveLedger.L12_ADD,tblLeaveLedger.L13_ADD,tblLeaveLedger.L14_ADD," & _
                 "tblLeaveLedger.L15_ADD,tblLeaveLedger.L16_ADD,tblLeaveLedger.L17_ADD,tblLeaveLedger.L18_ADD,tblLeaveLedger.L19_ADD,tblLeaveLedger.L20_ADD,tblEmployee.EmpName,tblEmployee.PresentCardNo,tblDepartment.DepartmentName,tblEmployee.departmentcode" & _
                 " from tblCatagory,tblDivision,  tblLeaveLedger,tblEmployee,tblCompany,tblDepartment" & _
                " Where tblCatagory.Cat = tblEmployee.Cat And tblDivision.DivisionCode = tblEmployee.DivisionCode And tblLeaveLedger.PayCode = tblEmployee.PayCode" & _
                " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
                " and TBLLEAVELEDGER.LYEAR = " & TextEdit2.Text & " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & strsortorder
            adapA = New OleDbDataAdapter(strsql, Common.con1)
            adapA.Fill(Rs_Report)
        Else
            strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tblLeaveLedger.PayCode,tblLeaveLedger.L01,tblLeaveLedger.L02,tblLeaveLedger.L03,tblLeaveLedger.L04,tblLeaveLedger.L05,tblLeaveLedger.L06,tblLeaveLedger.L07,tblLeaveLedger.L08,tblLeaveLedger.L09,tblLeaveLedger.L10,tblLeaveLedger.L11,tblLeaveLedger.L12,tblLeaveLedger.L13,tblLeaveLedger.L14,tblLeaveLedger.L15,tblLeaveLedger.L16,tblLeaveLedger.L17,tblLeaveLedger.L18,tblLeaveLedger.L19,tblLeaveLedger.L20," & _
                 "tblLeaveLedger.L01_ADD,tblLeaveLedger.L02_ADD,tblLeaveLedger.L03_ADD,tblLeaveLedger.L04_ADD,tblLeaveLedger.L05_ADD,tblLeaveLedger.L06_ADD,tblLeaveLedger.L07_ADD,tblLeaveLedger.L08_ADD,tblLeaveLedger.L09_ADD,tblLeaveLedger.L10_ADD,tblLeaveLedger.L11_ADD,tblLeaveLedger.L12_ADD,tblLeaveLedger.L13_ADD,tblLeaveLedger.L14_ADD," & _
                 "tblLeaveLedger.L15_ADD,tblLeaveLedger.L16_ADD,tblLeaveLedger.L17_ADD,tblLeaveLedger.L18_ADD,tblLeaveLedger.L19_ADD,tblLeaveLedger.L20_ADD,tblEmployee.EmpName,tblEmployee.PresentCardNo,tblDepartment.DepartmentName,tblEmployee.departmentcode" & _
                 " from tblCatagory,tblDivision,  tblLeaveLedger,tblEmployee,tblCompany,tblDepartment" & _
                " Where tblCatagory.Cat = tblEmployee.Cat And tblDivision.DivisionCode = tblEmployee.DivisionCode And tblLeaveLedger.PayCode = tblEmployee.PayCode" & _
                " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
                " and TBLLEAVELEDGER.LYEAR = " & TextEdit2.Text & " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & strsortorder
            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(Rs_Report)
        End If
        If Rs_Report.Tables(0).Rows.Count < 1 Then
            XtraMessageBox.Show(ulf, "<size=10>No Data Available For this Report.</size>", "<size=9>iAS</size>")
            Exit Sub
        End If

        strSqlLeaveMaster = " Select LeaveField,LeaveDescription from tblLeaveMaster order by LeaveField"
        rs_report_ReportLeaveMaster = New DataSet
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(strSqlLeaveMaster, Common.con1)
            adapA.Fill(rs_report_ReportLeaveMaster)
        Else
            adap = New SqlDataAdapter(strSqlLeaveMaster, Common.con)
            adap.Fill(rs_report_ReportLeaveMaster)
        End If
        For i As Integer = 0 To rs_report_ReportLeaveMaster.Tables(0).Rows.Count - 1 ' While Not rs_report_ReportLeaveMaster.EOF
            intRecCountLeaveMaster = intRecCountLeaveMaster + 1
        Next
        ReDim strLeaveField3(intRecCountLeaveMaster)
        ReDim strLeaveDescription50(intRecCountLeaveMaster)
        For i As Integer = 0 To rs_report_ReportLeaveMaster.Tables(0).Rows.Count - 1 ' While Not rs_report_ReportLeaveMaster.EOF
            intCounter = intCounter + 1
            strLeaveField3(intCounter) = rs_report_ReportLeaveMaster.Tables(0).Rows(i).Item("leavefield").ToString.Trim
            strLeaveDescription50(intCounter) = rs_report_ReportLeaveMaster.Tables(0).Rows(i).Item("LeaveDescription").ToString.Trim
        Next

        rowcnt = rowcnt + 1
        xlapp.Visible = True
        xlapp.Columns.ColumnWidth = 10
        xlapp.Cells(rowcnt, 5).Font.Bold = True
        xlapp.Cells(rowcnt, 5) = "Company Name:" & CommonReport.g_CompanyNames
        rowcnt = rowcnt + 1
        xlapp.Cells(rowcnt, 5).Font.Bold = True
        xlapp.Cells(rowcnt, 5) = "Run Date & Time :" & Format(Now(), "dd/MM/yyyy HH:mm")
        rowcnt = rowcnt + 2
        xlapp.Cells.Font.Size = 8
        xlapp.Cells.Font.Name = "TAHOMA"
        xlapp.Cells(rowcnt, 4).Font.Bold = True
        xlapp.Cells(rowcnt, 4) = " BALANCE LEAVE FOR THE YEAR : " & TextEdit2.Text
        rowcnt = rowcnt + 2
        xlapp.Cells(rowcnt, 1).Font.Bold = True
        xlapp.Cells(rowcnt, 1).Font.ColorIndex = 2 'vbWhite
        xlapp.Cells(rowcnt, 1).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 1).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 1) = "Srl No."
        xlapp.Cells(rowcnt, 2).Font.Bold = True
        xlapp.Cells(rowcnt, 2).Font.ColorIndex = 2 'vbWhite
        xlapp.Cells(rowcnt, 2).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 2).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 2) = "Paycode."
        xlapp.Cells(rowcnt, 3).Font.Bold = True
        xlapp.Cells(rowcnt, 3).Font.ColorIndex = 2 'vbWhite
        xlapp.Cells(rowcnt, 3).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 3).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 3) = "Employee Name"
        xlapp.Cells(rowcnt, 4).Font.Bold = True
        xlapp.Cells(rowcnt, 4).Font.ColorIndex = 2 'vbWhite
        xlapp.Cells(rowcnt, 4).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 4).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 5).Font.Bold = True
        xlapp.Cells(rowcnt, 5).Font.ColorIndex = 2 ' vbWhite
        xlapp.Cells(rowcnt, 5).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 5).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 5) = "L01"
        xlapp.Cells(rowcnt, 6).Font.Bold = True
        xlapp.Cells(rowcnt, 6).Font.ColorIndex = 2 'vbWhite
        xlapp.Cells(rowcnt, 6).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 6).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 6) = "L02"
        xlapp.Cells(rowcnt, 7).Font.Bold = True
        xlapp.Cells(rowcnt, 7).Font.ColorIndex = 2 'vbWhite
        xlapp.Cells(rowcnt, 7).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 7).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 7) = "L03"
        xlapp.Cells(rowcnt, 8).Font.Bold = True
        xlapp.Cells(rowcnt, 8).Font.ColorIndex = 2 'vbWhite
        xlapp.Cells(rowcnt, 8).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 8).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 8) = "L04"
        xlapp.Cells(rowcnt, 9).Font.Bold = True
        xlapp.Cells(rowcnt, 9).Font.ColorIndex = 2 'vbWhite
        xlapp.Cells(rowcnt, 9).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 9).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 9) = "L05"
        xlapp.Cells(rowcnt, 10).Font.Bold = True
        xlapp.Cells(rowcnt, 10).Font.ColorIndex = 2 'vbWhite
        xlapp.Cells(rowcnt, 10).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 10).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 10) = "L06"
        xlapp.Cells(rowcnt, 11).Font.Bold = True
        xlapp.Cells(rowcnt, 11).Font.ColorIndex = 2 'vbWhite
        xlapp.Cells(rowcnt, 11).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 11).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 11) = "L07"
        xlapp.Cells(rowcnt, 12).Font.Bold = True
        xlapp.Cells(rowcnt, 12).Font.ColorIndex = 2 ' vbWhite
        xlapp.Cells(rowcnt, 12).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 12).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 12) = "L08"
        xlapp.Cells(rowcnt, 13).Font.Bold = True
        xlapp.Cells(rowcnt, 13).Font.ColorIndex = 2 'vbWhite
        xlapp.Cells(rowcnt, 13).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 13).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 13) = "L09"
        xlapp.Cells(rowcnt, 14).Font.Bold = True
        xlapp.Cells(rowcnt, 14).Font.ColorIndex = 2 'vbWhite
        xlapp.Cells(rowcnt, 14).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 14).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 14) = "L10"
        xlapp.Cells(rowcnt, 15).Font.Bold = True
        xlapp.Cells(rowcnt, 15).Font.ColorIndex = 2 'vbWhite
        xlapp.Cells(rowcnt, 15).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 15).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 15) = "L11"
        xlapp.Cells(rowcnt, 16).Font.Bold = True
        xlapp.Cells(rowcnt, 16).Font.ColorIndex = 2 'vbWhite
        xlapp.Cells(rowcnt, 16).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 16).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 16) = "L12"
        xlapp.Cells(rowcnt, 17).Font.Bold = True
        xlapp.Cells(rowcnt, 17).Font.ColorIndex = 2 'vbWhite
        xlapp.Cells(rowcnt, 17).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 17).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 17) = "L13"
        xlapp.Cells(rowcnt, 18).Font.Bold = True
        xlapp.Cells(rowcnt, 18).Font.ColorIndex = 2 'vbWhite
        xlapp.Cells(rowcnt, 18).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 18).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 18) = "L14"
        xlapp.Cells(rowcnt, 19).Font.Bold = True
        xlapp.Cells(rowcnt, 19).Font.ColorIndex = 2 'vbWhite
        xlapp.Cells(rowcnt, 19).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 19).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 19) = "L15"
        xlapp.Cells(rowcnt, 20).Font.Bold = True
        xlapp.Cells(rowcnt, 20).Font.ColorIndex = 2 'vbWhite
        xlapp.Cells(rowcnt, 20).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 20).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 20) = "L16"
        xlapp.Cells(rowcnt, 21).Font.Bold = True
        xlapp.Cells(rowcnt, 21).Font.ColorIndex = 2 'vbWhite
        xlapp.Cells(rowcnt, 21).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 21).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 21) = "L17"
        xlapp.Cells(rowcnt, 22).Font.Bold = True
        xlapp.Cells(rowcnt, 22).Font.ColorIndex = 2 'vbWhite
        xlapp.Cells(rowcnt, 22).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 22).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 22) = "L18"
        xlapp.Cells(rowcnt, 23).Font.Bold = True
        xlapp.Cells(rowcnt, 23).Font.ColorIndex = 2 'vbWhite
        xlapp.Cells(rowcnt, 23).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 23).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 23) = "L19"
        xlapp.Cells(rowcnt, 24).Font.Bold = True
        xlapp.Cells(rowcnt, 24).Font.ColorIndex = 2 'vbWhite
        xlapp.Cells(rowcnt, 24).Interior.ColorIndex = 51
        xlapp.Cells(rowcnt, 24).Borders.LineStyle = XlLineStyle.xlContinuous
        xlapp.Cells(rowcnt, 24) = "L20"

        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            intCounter = 1
            blnLeaveCheck1 = True
            blnLeaveCheck2 = True
            blnLeaveCheck3 = True
            blnLeaveCheck4 = True
            blnLeaveCheck5 = True
            blnLeaveCheck6 = True
            blnLeaveCheck7 = True
            blnLeaveCheck8 = True
            blnLeaveCheck9 = True
            blnLeaveCheck10 = True
            blnLeaveCheck11 = True
            blnLeaveCheck12 = True
            blnLeaveCheck13 = True
            blnLeaveCheck14 = True
            blnLeaveCheck15 = True
            blnLeaveCheck16 = True
            blnLeaveCheck17 = True
            blnLeaveCheck18 = True
            blnLeaveCheck19 = True
            blnLeaveCheck20 = True
            While intCounter <= intRecCountLeaveMaster
                If blnLeaveCheck1 Then
                    If strLeaveField3(intCounter) = "L01" And Rs_Report.Tables(0).Rows(i).Item("L01").ToString.Trim <> "" Then
                        strLeaveWholeLine(1) = Rs_Report.Tables(0).Rows(i).Item("L01_ADD").ToString.Trim - Rs_Report.Tables(0).Rows(i).Item("L01").ToString.Trim
                        blnLeaveCheck1 = False
                    Else
                        strLeaveWholeLine(1) = ""
                    End If
                End If
                If blnLeaveCheck2 Then
                    If strLeaveField3(intCounter) = "L02" And Rs_Report.Tables(0).Rows(i).Item("L02").ToString.Trim <> "" Then
                        strLeaveWholeLine(2) = Rs_Report.Tables(0).Rows(i).Item("L02_ADD").ToString.Trim - Rs_Report.Tables(0).Rows(i).Item("L02").ToString.Trim
                        blnLeaveCheck2 = False
                    Else
                        strLeaveWholeLine(2) = ""
                    End If
                End If
                If blnLeaveCheck3 Then
                    If strLeaveField3(intCounter) = "L03" And Rs_Report.Tables(0).Rows(i).Item("L03").ToString.Trim <> "" Then
                        strLeaveWholeLine(3) = Rs_Report.Tables(0).Rows(i).Item("L03_ADD").ToString.Trim - Rs_Report.Tables(0).Rows(i).Item("L03").ToString.Trim
                        blnLeaveCheck3 = False
                    Else
                        strLeaveWholeLine(3) = ""
                    End If
                End If
                If blnLeaveCheck4 Then
                    If strLeaveField3(intCounter) = "L04" And Rs_Report.Tables(0).Rows(i).Item("L04").ToString.Trim <> "" Then
                        strLeaveWholeLine(4) = Rs_Report.Tables(0).Rows(i).Item("L04_ADD").ToString.Trim - Rs_Report.Tables(0).Rows(i).Item("L04").ToString.Trim
                        blnLeaveCheck4 = False
                    Else
                        strLeaveWholeLine(4) = ""
                    End If
                End If
                If blnLeaveCheck5 Then
                    If strLeaveField3(intCounter) = "L05" And Rs_Report.Tables(0).Rows(i).Item("L05").ToString.Trim <> "" Then
                        strLeaveWholeLine(5) = Rs_Report.Tables(0).Rows(i).Item("L05_ADD").ToString.Trim - Rs_Report.Tables(0).Rows(i).Item("L05").ToString.Trim
                        blnLeaveCheck5 = False
                    Else
                        strLeaveWholeLine(5) = ""
                    End If
                End If
                If blnLeaveCheck6 Then
                    If strLeaveField3(intCounter) = "L06" And Rs_Report.Tables(0).Rows(i).Item("L06").ToString.Trim <> "" Then
                        strLeaveWholeLine(6) = Rs_Report.Tables(0).Rows(i).Item("L06_ADD").ToString.Trim - Rs_Report.Tables(0).Rows(i).Item("L06").ToString.Trim
                        blnLeaveCheck6 = False
                    Else
                        strLeaveWholeLine(6) = ""
                    End If
                End If
                If blnLeaveCheck7 Then
                    If strLeaveField3(intCounter) = "L07" And Rs_Report.Tables(0).Rows(i).Item("L07").ToString.Trim <> "" Then
                        strLeaveWholeLine(7) = Rs_Report.Tables(0).Rows(i).Item("L07_ADD").ToString.Trim - Rs_Report.Tables(0).Rows(i).Item("L07").ToString.Trim
                        blnLeaveCheck7 = False
                    Else
                        strLeaveWholeLine(7) = ""
                    End If
                End If
                If blnLeaveCheck8 Then
                    If strLeaveField3(intCounter) = "L08" And Rs_Report.Tables(0).Rows(i).Item("L08").ToString.Trim <> "" Then
                        strLeaveWholeLine(8) = Rs_Report.Tables(0).Rows(i).Item("L08_ADD").ToString.Trim - Rs_Report.Tables(0).Rows(i).Item("L08").ToString.Trim
                        blnLeaveCheck8 = False
                    Else
                        strLeaveWholeLine(8) = ""
                    End If
                End If
                If blnLeaveCheck9 Then
                    If strLeaveField3(intCounter) = "L09" And Rs_Report.Tables(0).Rows(i).Item("L09").ToString.Trim <> "" Then
                        strLeaveWholeLine(9) = Rs_Report.Tables(0).Rows(i).Item("L09_ADD").ToString.Trim - Rs_Report.Tables(0).Rows(i).Item("L09").ToString.Trim
                        blnLeaveCheck9 = False
                    Else
                        strLeaveWholeLine(9) = ""
                    End If
                End If
                If blnLeaveCheck10 Then
                    If strLeaveField3(intCounter) = "L10" And Rs_Report.Tables(0).Rows(i).Item("L10").ToString.Trim <> "" Then
                        strLeaveWholeLine(10) = Rs_Report.Tables(0).Rows(i).Item("L10_ADD").ToString.Trim - Rs_Report.Tables(0).Rows(i).Item("L10").ToString.Trim
                        blnLeaveCheck10 = False
                    Else
                        strLeaveWholeLine(10) = ""
                    End If
                End If
                If blnLeaveCheck11 Then
                    If strLeaveField3(intCounter) = "L11" And Rs_Report.Tables(0).Rows(i).Item("L11").ToString.Trim <> "" Then
                        strLeaveWholeLine(11) = Rs_Report.Tables(0).Rows(i).Item("L11_ADD").ToString.Trim - Rs_Report.Tables(0).Rows(i).Item("L11").ToString.Trim
                        blnLeaveCheck11 = False
                    Else
                        strLeaveWholeLine(11) = ""
                    End If
                End If
                If blnLeaveCheck12 Then
                    If strLeaveField3(intCounter) = "L12" And Rs_Report.Tables(0).Rows(i).Item("L12").ToString.Trim <> "" Then
                        strLeaveWholeLine(12) = Rs_Report.Tables(0).Rows(i).Item("L12_ADD").ToString.Trim - Rs_Report.Tables(0).Rows(i).Item("L12").ToString.Trim
                        blnLeaveCheck12 = False
                    Else
                        strLeaveWholeLine(12) = ""
                    End If
                End If
                If blnLeaveCheck13 Then
                    If strLeaveField3(intCounter) = "L13" And Rs_Report.Tables(0).Rows(i).Item("L13").ToString.Trim <> "" Then
                        strLeaveWholeLine(13) = Rs_Report.Tables(0).Rows(i).Item("L13_ADD").ToString.Trim - Rs_Report.Tables(0).Rows(i).Item("L13").ToString.Trim
                        blnLeaveCheck13 = False
                    Else
                        strLeaveWholeLine(13) = ""
                    End If
                End If
                If blnLeaveCheck14 Then
                    If strLeaveField3(intCounter) = "L14" And Rs_Report.Tables(0).Rows(i).Item("L14").ToString.Trim <> "" Then
                        strLeaveWholeLine(14) = Rs_Report.Tables(0).Rows(i).Item("L14_ADD").ToString.Trim - Rs_Report.Tables(0).Rows(i).Item("L14").ToString.Trim
                        blnLeaveCheck14 = False
                    Else
                        strLeaveWholeLine(14) = ""
                    End If
                End If
                If blnLeaveCheck15 Then
                    If strLeaveField3(intCounter) = "L15" And Rs_Report.Tables(0).Rows(i).Item("L15").ToString.Trim <> "" Then
                        strLeaveWholeLine(15) = Rs_Report.Tables(0).Rows(i).Item("L15_ADD").ToString.Trim - Rs_Report.Tables(0).Rows(i).Item("L15").ToString.Trim
                        blnLeaveCheck15 = False
                    Else
                        strLeaveWholeLine(15) = ""
                    End If
                End If
                If blnLeaveCheck16 Then
                    If strLeaveField3(intCounter) = "L16" And Rs_Report.Tables(0).Rows(i).Item("L16").ToString.Trim <> "" Then
                        strLeaveWholeLine(16) = Rs_Report.Tables(0).Rows(i).Item("L16_ADD").ToString.Trim - Rs_Report.Tables(0).Rows(i).Item("L16").ToString.Trim
                        blnLeaveCheck16 = False
                    Else
                        strLeaveWholeLine(16) = ""
                    End If
                End If
                If blnLeaveCheck17 Then
                    If strLeaveField3(intCounter) = "L17" And Rs_Report.Tables(0).Rows(i).Item("L17").ToString.Trim <> "" Then
                        strLeaveWholeLine(17) = Rs_Report.Tables(0).Rows(i).Item("L17_ADD").ToString.Trim - Rs_Report.Tables(0).Rows(i).Item("L17").ToString.Trim
                        blnLeaveCheck17 = False
                    Else
                        strLeaveWholeLine(17) = ""
                    End If
                End If
                If blnLeaveCheck18 Then
                    If strLeaveField3(intCounter) = "L18" And Rs_Report.Tables(0).Rows(i).Item("L18").ToString.Trim <> "" Then
                        strLeaveWholeLine(18) = Rs_Report.Tables(0).Rows(i).Item("L18_ADD").ToString.Trim - Rs_Report.Tables(0).Rows(i).Item("L18").ToString.Trim
                        blnLeaveCheck18 = False
                    Else
                        strLeaveWholeLine(18) = ""
                    End If
                End If
                If blnLeaveCheck19 Then
                    If strLeaveField3(intCounter) = "L19" And Rs_Report.Tables(0).Rows(i).Item("L19").ToString.Trim <> "" Then
                        strLeaveWholeLine(19) = Rs_Report.Tables(0).Rows(i).Item("L19_ADD").ToString.Trim - Rs_Report.Tables(0).Rows(i).Item("L19").ToString.Trim
                        blnLeaveCheck19 = False
                    Else
                        strLeaveWholeLine(19) = ""
                    End If
                End If
                If blnLeaveCheck20 Then
                    If strLeaveField3(intCounter) = "L20" And Rs_Report.Tables(0).Rows(i).Item("L20").ToString.Trim <> "" Then
                        strLeaveWholeLine(20) = Rs_Report.Tables(0).Rows(i).Item("L20_ADD").ToString.Trim - Rs_Report.Tables(0).Rows(i).Item("L20").ToString.Trim
                        blnLeaveCheck20 = False
                    Else
                        strLeaveWholeLine(20) = ""
                    End If
                End If
                intCounter = intCounter + 1
            End While
            rowcnt = rowcnt + 1
            msrl = msrl + 1
            xlapp.Cells(rowcnt, 1).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 1) = "'" & msrl
            xlapp.Cells(rowcnt, 2).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 2) = "'" & Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim
            xlapp.Cells(rowcnt, 3).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 3) = Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim
            xlapp.Cells(rowcnt, 4).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 5).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 5) = "'" & strLeaveWholeLine(1)
            xlapp.Cells(rowcnt, 6).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 6) = "'" & (strLeaveWholeLine(2))
            xlapp.Cells(rowcnt, 7).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 7) = "'" & (strLeaveWholeLine(3))
            xlapp.Cells(rowcnt, 8).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 8) = "'" & (strLeaveWholeLine(4))
            xlapp.Cells(rowcnt, 9).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 9) = "'" & (strLeaveWholeLine(5))
            xlapp.Cells(rowcnt, 10).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 10) = "'" & (strLeaveWholeLine(6))
            xlapp.Cells(rowcnt, 11).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 11) = "'" & (strLeaveWholeLine(7))
            xlapp.Cells(rowcnt, 12).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 12) = "'" & (strLeaveWholeLine(8))
            xlapp.Cells(rowcnt, 13).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 13) = "'" & (strLeaveWholeLine(9))
            xlapp.Cells(rowcnt, 14).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 14) = "'" & (strLeaveWholeLine(10))
            xlapp.Cells(rowcnt, 15).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 15) = "'" & (strLeaveWholeLine(11))
            xlapp.Cells(rowcnt, 16).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 16) = "'" & (strLeaveWholeLine(12))
            xlapp.Cells(rowcnt, 17).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 17) = "'" & (strLeaveWholeLine(13))
            xlapp.Cells(rowcnt, 18).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 18) = "'" & (strLeaveWholeLine(14))
            xlapp.Cells(rowcnt, 19).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 19) = "'" & (strLeaveWholeLine(15))
            xlapp.Cells(rowcnt, 20).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 20) = "'" & (strLeaveWholeLine(16))
            xlapp.Cells(rowcnt, 21).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 21) = "'" & (strLeaveWholeLine(17))
            xlapp.Cells(rowcnt, 22).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 22) = "'" & (strLeaveWholeLine(18))
            xlapp.Cells(rowcnt, 23).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 23) = "'" & (strLeaveWholeLine(19))
            xlapp.Cells(rowcnt, 24).Borders.LineStyle = XlLineStyle.xlContinuous
            xlapp.Cells(rowcnt, 24) = "'" & (strLeaveWholeLine(20))
            mintLine = mintLine + 1
            mblnCheckReport = True
            'Loop
        Next
        rowcnt = rowcnt + 1
        intCounter = 1
        While intCounter <= intRecCountLeaveMaster
            xlapp.Cells(rowcnt, 1).Font.Bold = True
            xlapp.Cells(rowcnt, 1) = strLeaveField3(intCounter) & " " & "-" & " " & strLeaveDescription50(intCounter) & Space(4)
            rowcnt = rowcnt + 1
            intCounter = intCounter + 1
        End While
        xlwb.SaveAs(mstrFile_Name)
    End Sub
    Sub MonthlyXl_BalanceLeaveGrid(ByVal strsortorder As String)
        Dim strDeptDivCode As String
        Dim strCurrentDeptDivCode As String
        Dim strsql As String
        Dim strSqlLeaveMaster As String
        Dim rs_report_ReportLeaveMaster As DataSet 'ADODB.Recordset
        Dim strLeaveField3() As String '* 3
        Dim strLeaveDescription50() As String '* 50
        Dim intRecCountLeaveMaster As Integer
        Dim intCounter As Integer
        Dim intFile As Integer
        Dim strLeaveWholeLine(20) As String '* 10
        Dim blnLeaveCheck1 As Boolean
        Dim blnLeaveCheck2 As Boolean
        Dim blnLeaveCheck3 As Boolean
        Dim blnLeaveCheck4 As Boolean
        Dim blnLeaveCheck5 As Boolean
        Dim blnLeaveCheck6 As Boolean
        Dim blnLeaveCheck7 As Boolean
        Dim blnLeaveCheck8 As Boolean
        Dim blnLeaveCheck9 As Boolean
        Dim blnLeaveCheck10 As Boolean
        Dim blnLeaveCheck11 As Boolean
        Dim blnLeaveCheck12 As Boolean
        Dim blnLeaveCheck13 As Boolean
        Dim blnLeaveCheck14 As Boolean
        Dim blnLeaveCheck15 As Boolean
        Dim blnLeaveCheck16 As Boolean
        Dim blnLeaveCheck17 As Boolean
        Dim blnLeaveCheck18 As Boolean
        Dim blnLeaveCheck19 As Boolean
        Dim blnLeaveCheck20 As Boolean
        Dim blnPrintLine As Boolean
        Dim strDepartmentCode As String
        Dim intPrintLineCounter As Integer
        Dim blnDeptAvailable As Boolean, mFirst As Boolean
        Dim msrl As Integer
        'Dim xlapp As Excel.Application
        'Dim xlwb As Excel.Workbook
        'Dim xlst As Excel.Sheets
        Dim rowcnt As Integer

        'xlapp = CreateObject("Excel.Application")
        'xlwb = xlapp.Workbooks.Add
        mblnCheckReport = False
        mintPageNo = 1
        mintLine = 1
        If XtraShortOrder.g_SortOrder <> "" Then
            strsortorder = XtraShortOrder.g_SortOrder
        End If
        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If
        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".xls"

        If Common.servername = "Access" Then
            strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tblLeaveLedger.PayCode,tblLeaveLedger.L01,tblLeaveLedger.L02,tblLeaveLedger.L03,tblLeaveLedger.L04,tblLeaveLedger.L05,tblLeaveLedger.L06,tblLeaveLedger.L07,tblLeaveLedger.L08,tblLeaveLedger.L09,tblLeaveLedger.L10,tblLeaveLedger.L11,tblLeaveLedger.L12,tblLeaveLedger.L13,tblLeaveLedger.L14,tblLeaveLedger.L15,tblLeaveLedger.L16,tblLeaveLedger.L17,tblLeaveLedger.L18,tblLeaveLedger.L19,tblLeaveLedger.L20," & _
                 "tblLeaveLedger.L01_ADD,tblLeaveLedger.L02_ADD,tblLeaveLedger.L03_ADD,tblLeaveLedger.L04_ADD,tblLeaveLedger.L05_ADD,tblLeaveLedger.L06_ADD,tblLeaveLedger.L07_ADD,tblLeaveLedger.L08_ADD,tblLeaveLedger.L09_ADD,tblLeaveLedger.L10_ADD,tblLeaveLedger.L11_ADD,tblLeaveLedger.L12_ADD,tblLeaveLedger.L13_ADD,tblLeaveLedger.L14_ADD," & _
                 "tblLeaveLedger.L15_ADD,tblLeaveLedger.L16_ADD,tblLeaveLedger.L17_ADD,tblLeaveLedger.L18_ADD,tblLeaveLedger.L19_ADD,tblLeaveLedger.L20_ADD,tblEmployee.EmpName,tblEmployee.PresentCardNo,tblDepartment.DepartmentName,tblEmployee.departmentcode" & _
                 " from tblCatagory,tblDivision,  tblLeaveLedger,tblEmployee,tblCompany,tblDepartment" & _
                " Where tblCatagory.Cat = tblEmployee.Cat And tblLeaveLedger.PayCode = tblEmployee.PayCode" & _
                " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
                " and TBLLEAVELEDGER.LYEAR = " & TextEdit2.Text & " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & strsortorder
            adapA = New OleDbDataAdapter(strsql, Common.con1)
            adapA.Fill(Rs_Report)
        Else
            strsql = "select tblCatagory.CatagoryName, tblDivision.DivisionName, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat,   tblLeaveLedger.PayCode,tblLeaveLedger.L01,tblLeaveLedger.L02,tblLeaveLedger.L03,tblLeaveLedger.L04,tblLeaveLedger.L05,tblLeaveLedger.L06,tblLeaveLedger.L07,tblLeaveLedger.L08,tblLeaveLedger.L09,tblLeaveLedger.L10,tblLeaveLedger.L11,tblLeaveLedger.L12,tblLeaveLedger.L13,tblLeaveLedger.L14,tblLeaveLedger.L15,tblLeaveLedger.L16,tblLeaveLedger.L17,tblLeaveLedger.L18,tblLeaveLedger.L19,tblLeaveLedger.L20," & _
                 "tblLeaveLedger.L01_ADD,tblLeaveLedger.L02_ADD,tblLeaveLedger.L03_ADD,tblLeaveLedger.L04_ADD,tblLeaveLedger.L05_ADD,tblLeaveLedger.L06_ADD,tblLeaveLedger.L07_ADD,tblLeaveLedger.L08_ADD,tblLeaveLedger.L09_ADD,tblLeaveLedger.L10_ADD,tblLeaveLedger.L11_ADD,tblLeaveLedger.L12_ADD,tblLeaveLedger.L13_ADD,tblLeaveLedger.L14_ADD," & _
                 "tblLeaveLedger.L15_ADD,tblLeaveLedger.L16_ADD,tblLeaveLedger.L17_ADD,tblLeaveLedger.L18_ADD,tblLeaveLedger.L19_ADD,tblLeaveLedger.L20_ADD,tblEmployee.EmpName,tblEmployee.PresentCardNo,tblDepartment.DepartmentName,tblEmployee.departmentcode" & _
                 " from tblCatagory,tblDivision,  tblLeaveLedger,tblEmployee,tblCompany,tblDepartment" & _
                " Where tblCatagory.Cat = tblEmployee.Cat And tblLeaveLedger.PayCode = tblEmployee.PayCode" & _
                " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
                " and TBLLEAVELEDGER.LYEAR = " & TextEdit2.Text & " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & strsortorder
            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(Rs_Report)
        End If
        If Rs_Report.Tables(0).Rows.Count < 1 Then
            XtraMessageBox.Show(ulf, "<size=10>No Data Available For this Report.</size>", "<size=9>iAS</size>")
            Exit Sub
        End If
        strSqlLeaveMaster = " Select LeaveField,LeaveDescription from tblLeaveMaster order by LeaveField"
        rs_report_ReportLeaveMaster = New DataSet
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(strSqlLeaveMaster, Common.con1)
            adapA.Fill(rs_report_ReportLeaveMaster)
        Else
            adap = New SqlDataAdapter(strSqlLeaveMaster, Common.con)
            adap.Fill(rs_report_ReportLeaveMaster)
        End If
        For i As Integer = 0 To rs_report_ReportLeaveMaster.Tables(0).Rows.Count - 1 ' While Not rs_report_ReportLeaveMaster.EOF
            intRecCountLeaveMaster = intRecCountLeaveMaster + 1
        Next
        ReDim strLeaveField3(intRecCountLeaveMaster)
        ReDim strLeaveDescription50(intRecCountLeaveMaster)
        For i As Integer = 0 To rs_report_ReportLeaveMaster.Tables(0).Rows.Count - 1 ' While Not rs_report_ReportLeaveMaster.EOF
            intCounter = intCounter + 1
            strLeaveField3(intCounter) = rs_report_ReportLeaveMaster.Tables(0).Rows(i).Item("leavefield").ToString.Trim
            strLeaveDescription50(intCounter) = rs_report_ReportLeaveMaster.Tables(0).Rows(i).Item("LeaveDescription").ToString.Trim
        Next

        rowcnt = rowcnt + 1

        If Common.IsNepali = "Y" Then
            Common.frodatetodatetoReportGrid = " BALANCE LEAVE FOR THE YEAR : " & tmpNepali
        Else
            Common.frodatetodatetoReportGrid = " BALANCE LEAVE FOR THE YEAR : " & TextEdit2.Text
        End If

        Me.Cursor = Cursors.WaitCursor
        Common.tbl = New Data.DataTable()
        Common.tbl.Columns.Add("Srl", GetType(String))
        Common.tbl.Columns.Add("Paycode", GetType(String))
        Common.tbl.Columns.Add("Employee Name", GetType(String))
        Common.tbl.Columns.Add(" ", GetType(String))
        Common.tbl.Columns.Add("L01", GetType(String))
        Common.tbl.Columns.Add("L02", GetType(String))
        Common.tbl.Columns.Add("L03", GetType(String))
        Common.tbl.Columns.Add("L04", GetType(String))
        Common.tbl.Columns.Add("L05", GetType(String))
        Common.tbl.Columns.Add("L06", GetType(String))
        Common.tbl.Columns.Add("L07", GetType(String))
        Common.tbl.Columns.Add("L08", GetType(String))
        Common.tbl.Columns.Add("L09", GetType(String))
        Common.tbl.Columns.Add("L10", GetType(String))
        Common.tbl.Columns.Add("L11", GetType(String))
        Common.tbl.Columns.Add("L12", GetType(String))
        Common.tbl.Columns.Add("L13", GetType(String))
        Common.tbl.Columns.Add("L14", GetType(String))
        Common.tbl.Columns.Add("L15", GetType(String))
        Common.tbl.Columns.Add("L16", GetType(String))
        Common.tbl.Columns.Add("L17", GetType(String))
        Common.tbl.Columns.Add("L18", GetType(String))
        Common.tbl.Columns.Add("L19", GetType(String))
        Common.tbl.Columns.Add("L20", GetType(String))

        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            intCounter = 1
            blnLeaveCheck1 = True
            blnLeaveCheck2 = True
            blnLeaveCheck3 = True
            blnLeaveCheck4 = True
            blnLeaveCheck5 = True
            blnLeaveCheck6 = True
            blnLeaveCheck7 = True
            blnLeaveCheck8 = True
            blnLeaveCheck9 = True
            blnLeaveCheck10 = True
            blnLeaveCheck11 = True
            blnLeaveCheck12 = True
            blnLeaveCheck13 = True
            blnLeaveCheck14 = True
            blnLeaveCheck15 = True
            blnLeaveCheck16 = True
            blnLeaveCheck17 = True
            blnLeaveCheck18 = True
            blnLeaveCheck19 = True
            blnLeaveCheck20 = True
            While intCounter <= intRecCountLeaveMaster
                If blnLeaveCheck1 Then
                    If strLeaveField3(intCounter) = "L01" And Rs_Report.Tables(0).Rows(i).Item("L01").ToString.Trim <> "" Then
                        strLeaveWholeLine(1) = Rs_Report.Tables(0).Rows(i).Item("L01_ADD").ToString.Trim - Rs_Report.Tables(0).Rows(i).Item("L01").ToString.Trim
                        blnLeaveCheck1 = False
                    Else
                        strLeaveWholeLine(1) = ""
                    End If
                End If
                If blnLeaveCheck2 Then
                    If strLeaveField3(intCounter) = "L02" And Rs_Report.Tables(0).Rows(i).Item("L02").ToString.Trim <> "" Then
                        strLeaveWholeLine(2) = Rs_Report.Tables(0).Rows(i).Item("L02_ADD").ToString.Trim - Rs_Report.Tables(0).Rows(i).Item("L02").ToString.Trim
                        blnLeaveCheck2 = False
                    Else
                        strLeaveWholeLine(2) = ""
                    End If
                End If
                If blnLeaveCheck3 Then
                    If strLeaveField3(intCounter) = "L03" And Rs_Report.Tables(0).Rows(i).Item("L03").ToString.Trim <> "" Then
                        strLeaveWholeLine(3) = Rs_Report.Tables(0).Rows(i).Item("L03_ADD").ToString.Trim - Rs_Report.Tables(0).Rows(i).Item("L03").ToString.Trim
                        blnLeaveCheck3 = False
                    Else
                        strLeaveWholeLine(3) = ""
                    End If
                End If
                If blnLeaveCheck4 Then
                    If strLeaveField3(intCounter) = "L04" And Rs_Report.Tables(0).Rows(i).Item("L04").ToString.Trim <> "" Then
                        strLeaveWholeLine(4) = Rs_Report.Tables(0).Rows(i).Item("L04_ADD").ToString.Trim - Rs_Report.Tables(0).Rows(i).Item("L04").ToString.Trim
                        blnLeaveCheck4 = False
                    Else
                        strLeaveWholeLine(4) = ""
                    End If
                End If
                If blnLeaveCheck5 Then
                    If strLeaveField3(intCounter) = "L05" And Rs_Report.Tables(0).Rows(i).Item("L05").ToString.Trim <> "" Then
                        strLeaveWholeLine(5) = Rs_Report.Tables(0).Rows(i).Item("L05_ADD").ToString.Trim - Rs_Report.Tables(0).Rows(i).Item("L05").ToString.Trim
                        blnLeaveCheck5 = False
                    Else
                        strLeaveWholeLine(5) = ""
                    End If
                End If
                If blnLeaveCheck6 Then
                    If strLeaveField3(intCounter) = "L06" And Rs_Report.Tables(0).Rows(i).Item("L06").ToString.Trim <> "" Then
                        strLeaveWholeLine(6) = Rs_Report.Tables(0).Rows(i).Item("L06_ADD").ToString.Trim - Rs_Report.Tables(0).Rows(i).Item("L06").ToString.Trim
                        blnLeaveCheck6 = False
                    Else
                        strLeaveWholeLine(6) = ""
                    End If
                End If
                If blnLeaveCheck7 Then
                    If strLeaveField3(intCounter) = "L07" And Rs_Report.Tables(0).Rows(i).Item("L07").ToString.Trim <> "" Then
                        strLeaveWholeLine(7) = Rs_Report.Tables(0).Rows(i).Item("L07_ADD").ToString.Trim - Rs_Report.Tables(0).Rows(i).Item("L07").ToString.Trim
                        blnLeaveCheck7 = False
                    Else
                        strLeaveWholeLine(7) = ""
                    End If
                End If
                If blnLeaveCheck8 Then
                    If strLeaveField3(intCounter) = "L08" And Rs_Report.Tables(0).Rows(i).Item("L08").ToString.Trim <> "" Then
                        strLeaveWholeLine(8) = Rs_Report.Tables(0).Rows(i).Item("L08_ADD").ToString.Trim - Rs_Report.Tables(0).Rows(i).Item("L08").ToString.Trim
                        blnLeaveCheck8 = False
                    Else
                        strLeaveWholeLine(8) = ""
                    End If
                End If
                If blnLeaveCheck9 Then
                    If strLeaveField3(intCounter) = "L09" And Rs_Report.Tables(0).Rows(i).Item("L09").ToString.Trim <> "" Then
                        strLeaveWholeLine(9) = Rs_Report.Tables(0).Rows(i).Item("L09_ADD").ToString.Trim - Rs_Report.Tables(0).Rows(i).Item("L09").ToString.Trim
                        blnLeaveCheck9 = False
                    Else
                        strLeaveWholeLine(9) = ""
                    End If
                End If
                If blnLeaveCheck10 Then
                    If strLeaveField3(intCounter) = "L10" And Rs_Report.Tables(0).Rows(i).Item("L10").ToString.Trim <> "" Then
                        strLeaveWholeLine(10) = Rs_Report.Tables(0).Rows(i).Item("L10_ADD").ToString.Trim - Rs_Report.Tables(0).Rows(i).Item("L10").ToString.Trim
                        blnLeaveCheck10 = False
                    Else
                        strLeaveWholeLine(10) = ""
                    End If
                End If
                If blnLeaveCheck11 Then
                    If strLeaveField3(intCounter) = "L11" And Rs_Report.Tables(0).Rows(i).Item("L11").ToString.Trim <> "" Then
                        strLeaveWholeLine(11) = Rs_Report.Tables(0).Rows(i).Item("L11_ADD").ToString.Trim - Rs_Report.Tables(0).Rows(i).Item("L11").ToString.Trim
                        blnLeaveCheck11 = False
                    Else
                        strLeaveWholeLine(11) = ""
                    End If
                End If
                If blnLeaveCheck12 Then
                    If strLeaveField3(intCounter) = "L12" And Rs_Report.Tables(0).Rows(i).Item("L12").ToString.Trim <> "" Then
                        strLeaveWholeLine(12) = Rs_Report.Tables(0).Rows(i).Item("L12_ADD").ToString.Trim - Rs_Report.Tables(0).Rows(i).Item("L12").ToString.Trim
                        blnLeaveCheck12 = False
                    Else
                        strLeaveWholeLine(12) = ""
                    End If
                End If
                If blnLeaveCheck13 Then
                    If strLeaveField3(intCounter) = "L13" And Rs_Report.Tables(0).Rows(i).Item("L13").ToString.Trim <> "" Then
                        strLeaveWholeLine(13) = Rs_Report.Tables(0).Rows(i).Item("L13_ADD").ToString.Trim - Rs_Report.Tables(0).Rows(i).Item("L13").ToString.Trim
                        blnLeaveCheck13 = False
                    Else
                        strLeaveWholeLine(13) = ""
                    End If
                End If
                If blnLeaveCheck14 Then
                    If strLeaveField3(intCounter) = "L14" And Rs_Report.Tables(0).Rows(i).Item("L14").ToString.Trim <> "" Then
                        strLeaveWholeLine(14) = Rs_Report.Tables(0).Rows(i).Item("L14_ADD").ToString.Trim - Rs_Report.Tables(0).Rows(i).Item("L14").ToString.Trim
                        blnLeaveCheck14 = False
                    Else
                        strLeaveWholeLine(14) = ""
                    End If
                End If
                If blnLeaveCheck15 Then
                    If strLeaveField3(intCounter) = "L15" And Rs_Report.Tables(0).Rows(i).Item("L15").ToString.Trim <> "" Then
                        strLeaveWholeLine(15) = Rs_Report.Tables(0).Rows(i).Item("L15_ADD").ToString.Trim - Rs_Report.Tables(0).Rows(i).Item("L15").ToString.Trim
                        blnLeaveCheck15 = False
                    Else
                        strLeaveWholeLine(15) = ""
                    End If
                End If
                If blnLeaveCheck16 Then
                    If strLeaveField3(intCounter) = "L16" And Rs_Report.Tables(0).Rows(i).Item("L16").ToString.Trim <> "" Then
                        strLeaveWholeLine(16) = Rs_Report.Tables(0).Rows(i).Item("L16_ADD").ToString.Trim - Rs_Report.Tables(0).Rows(i).Item("L16").ToString.Trim
                        blnLeaveCheck16 = False
                    Else
                        strLeaveWholeLine(16) = ""
                    End If
                End If
                If blnLeaveCheck17 Then
                    If strLeaveField3(intCounter) = "L17" And Rs_Report.Tables(0).Rows(i).Item("L17").ToString.Trim <> "" Then
                        strLeaveWholeLine(17) = Rs_Report.Tables(0).Rows(i).Item("L17_ADD").ToString.Trim - Rs_Report.Tables(0).Rows(i).Item("L17").ToString.Trim
                        blnLeaveCheck17 = False
                    Else
                        strLeaveWholeLine(17) = ""
                    End If
                End If
                If blnLeaveCheck18 Then
                    If strLeaveField3(intCounter) = "L18" And Rs_Report.Tables(0).Rows(i).Item("L18").ToString.Trim <> "" Then
                        strLeaveWholeLine(18) = Rs_Report.Tables(0).Rows(i).Item("L18_ADD").ToString.Trim - Rs_Report.Tables(0).Rows(i).Item("L18").ToString.Trim
                        blnLeaveCheck18 = False
                    Else
                        strLeaveWholeLine(18) = ""
                    End If
                End If
                If blnLeaveCheck19 Then
                    If strLeaveField3(intCounter) = "L19" And Rs_Report.Tables(0).Rows(i).Item("L19").ToString.Trim <> "" Then
                        strLeaveWholeLine(19) = Rs_Report.Tables(0).Rows(i).Item("L19_ADD").ToString.Trim - Rs_Report.Tables(0).Rows(i).Item("L19").ToString.Trim
                        blnLeaveCheck19 = False
                    Else
                        strLeaveWholeLine(19) = ""
                    End If
                End If
                If blnLeaveCheck20 Then
                    If strLeaveField3(intCounter) = "L20" And Rs_Report.Tables(0).Rows(i).Item("L20").ToString.Trim <> "" Then
                        strLeaveWholeLine(20) = Rs_Report.Tables(0).Rows(i).Item("L20_ADD").ToString.Trim - Rs_Report.Tables(0).Rows(i).Item("L20").ToString.Trim
                        blnLeaveCheck20 = False
                    Else
                        strLeaveWholeLine(20) = ""
                    End If
                End If
                intCounter = intCounter + 1
            End While
            rowcnt = rowcnt + 1
            msrl = msrl + 1

            mintLine = mintLine + 1
            mblnCheckReport = True

            Common.tbl.Rows.Add(msrl, Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim, Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim, "", _
                               strLeaveWholeLine(1), strLeaveWholeLine(2), strLeaveWholeLine(3), strLeaveWholeLine(4), strLeaveWholeLine(5), _
                               strLeaveWholeLine(6), strLeaveWholeLine(7), strLeaveWholeLine(8), strLeaveWholeLine(9), strLeaveWholeLine(10), _
                               strLeaveWholeLine(11), strLeaveWholeLine(12), strLeaveWholeLine(13), strLeaveWholeLine(14), strLeaveWholeLine(15), _
                               strLeaveWholeLine(16), strLeaveWholeLine(17), strLeaveWholeLine(18), strLeaveWholeLine(19), strLeaveWholeLine(20))
        Next
        rowcnt = rowcnt + 1
        intCounter = 1
        Common.tbl.Rows.Add("")
        While intCounter <= intRecCountLeaveMaster
            Common.tbl.Rows.Add(strLeaveField3(intCounter) & " " & "-" & " ", strLeaveDescription50(intCounter))
            rowcnt = rowcnt + 1
            intCounter = intCounter + 1
        End While
        Me.Cursor = Cursors.Default
        XtraReportGrid.ShowDialog()
    End Sub
    Sub Monthly_LeaveRegister(ByVal strsortorder As String)
        Dim strDeptDivCode As String
        Dim strCurrentDeptDivCode As String
        Dim strsql As String
        Dim intFile As Integer
        Dim strLeaveDescription50 As String '* 50
        Dim strLeaveCode As String
        Dim dblAccruedLeave As Double
        Dim dblTotalLeaveAmount As Double
        Dim dblTotalBalance As Double
        Dim strAccruedLeave As String
        Dim strLeaveAmount As String
        Dim strTotaLBalance As String
        Dim strVoucher_No10 As String '* 10
        Dim strPayCode As String, strSpace As String
        Dim strGuardianName30 As String '* 30
        Dim dblL01_ADD As Double, dblL02_ADD As Double, dblL03_ADD As Double, dblL04_ADD As Double, dblL05_ADD As Double
        Dim dblL06_ADD As Double, dblL07_ADD As Double, dblL08_ADD As Double, dblL09_ADD As Double, dblL10_ADD As Double
        Dim dblL11_ADD As Double, dblL12_ADD As Double, dblL13_ADD As Double, dblL14_ADD As Double, dblL15_ADD As Double
        Dim dblL16_ADD As Double, dblL17_ADD As Double, dblL18_ADD As Double, dblL19_ADD As Double, dblL20_ADD As Double
        Dim strDepartmentCode As String
        Dim blnDeptAvailable As Boolean
        Dim intMaxLine As Integer
        Dim mLvfiedl As String, mLvfield1 As String, mLvAcc As Integer
        Dim mStartDate As Date
        Dim mEndDate As Date
        mblnCheckReport = False

        If Len(Trim(CommonReport.g_CompanyNames)) = 0 Then
            CommonReport.GetCompanies()
        End If
        Dim g_HODDepartmentCode As String = "" 'nitin
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim Rs_Report As DataSet = New DataSet
        Dim Rs_Lvdata As DataSet = New DataSet
        Dim Rs_Lvdesc As DataSet = New DataSet

        Dim gLeaveFinancialYear As String
        mintPageNo = 1
        mintLine = 1

        If Len(strsortorder) > 0 Then
            blnDeptAvailable = True
            intMaxLine = 46
        Else
            intMaxLine = 49
        End If

        Dim adapS As SqlDataAdapter
        Dim adapAc As OleDbDataAdapter
        Dim Rs As DataSet = New DataSet
        Dim sSql As String = " Select LeaveFinancialYear from tblSetUp where setupid =(Select Convert(varchar(10),Max(Convert(int,Setupid))) from tblsetup )"
        If Common.servername = "Access" Then
            sSql = " Select * from tblSetUp where setupid =(Select CVar(Max(CInt(Setupid))) from tblsetup )"
            adapAc = New OleDbDataAdapter(sSql, Common.con1)
            adapAc.Fill(Rs)
        Else
            adapS = New SqlDataAdapter(sSql, Common.con)
            adapS.Fill(Rs)
        End If
        gLeaveFinancialYear = Rs.Tables(0).Rows(0).Item("LeaveFinancialYear").ToString.Trim

        If gLeaveFinancialYear = "Y" Then

        End If
        mStartDate = "01/04/" & TextEdit2.Text
        mEndDate = "31/03/" & Convert.ToInt16(TextEdit2.Text) + 1

        'If g_Report_view Then
        If Common.servername = "Access" Then
            If gLeaveFinancialYear = "Y" Then
                strsql = " select * from tbltimeregister  where LeaveCode is not Null And FORMAT(tblTimeRegister.DateOffice,'yyyy-MM-dd') Between #" & Format(mStartDate, "yyyy-MM-dd") & "# AND #" & Format(mEndDate, "yyyy-MM-dd") & "# " & " Order by PayCode,LeaveCode,DateOffice"
            Else
                strsql = " select * from tbltimeregister  where LeaveCode is not Null And FORMAT(dateoffice,'YYYY')=" & TextEdit2.Text & " Order by PayCode,LeaveCode,DateOffice"
            End If
            adapA = New OleDbDataAdapter(strsql, Common.con1)
            adapA.Fill(Rs_Lvdata)
        Else
            If gLeaveFinancialYear = "Y" Then
                strsql = " select * from tbltimeregister  where LeaveCode is not Null And tblTimeRegister.DateOffice Between '" & Format(mStartDate, "yyyy-MM-dd") & "' AND '" & Format(mEndDate, "yyyy-MM-dd") & "'" & " Order by PayCode,LeaveCode,DateOffice"
            Else
                strsql = " select * from tbltimeregister  where LeaveCode is not Null And year(dateoffice)=" & TextEdit2.Text & " Order by PayCode,LeaveCode,DateOffice"
            End If
            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(Rs_Lvdata)
        End If
        'Else
        '    strsql = " select * from tbltimeregisterD  where LeaveCode is not Null And year(dateoffice)=" & frmLeaveReport.txtOnlyYear.Text
        'End If
        'Rs_Lvdata = Cn.Execute(strsql)

        strsql = "select * from tblleavemaster order by LeaveCode"
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(strsql, Common.con1)
            adapA.Fill(Rs_Lvdesc)
        Else
            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(Rs_Lvdesc)
        End If

        'Rs_Lvdesc = Cn.Execute(strsql)
        If Common.servername = "Access" Then
            strsql = " select tblleaveledger.*,tblcatagory.CatagoryName,tblDivision.DivisionName, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat," & _
                 " tblEmployee.EmpName,tblEmployee.PresentCardNo,MID(tblemployee.GuardianName, 1, 25) AS GuardianName ,tblDepartment.DepartmentName,tblEmployee.departmentcode" & _
                 " from tblCatagory,tblDivision,  tblLeaveLedger,tblEmployee,tblCompany,tblDepartment" & _
                 " Where tblCatagory.Cat = tblEmployee.Cat And tblLeaveLedger.PayCode = tblEmployee.PayCode " & _
                 " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
                 " And TBLLEAVELEDGER.LYEAR = " & TextEdit2.Text & " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & " order by tblEmployee.PayCode"
            adapA = New OleDbDataAdapter(strsql, Common.con1)
            adapA.Fill(Rs_Report)
        Else
            strsql = " select tblleaveledger.*,tblcatagory.CatagoryName,tblDivision.DivisionName, tblEmployee.Divisioncode, tblEmployee.Gradecode, tblEmployee.Companycode, tblEmployee.Cat," & _
                 " tblEmployee.EmpName,tblEmployee.PresentCardNo,GuardianName = substring(tblemployee.GuardianName, 1, 25),tblDepartment.DepartmentName,tblEmployee.departmentcode" & _
                 " from tblCatagory,tblDivision,  tblLeaveLedger,tblEmployee,tblCompany,tblDepartment" & _
                 " Where tblCatagory.Cat = tblEmployee.Cat And tblLeaveLedger.PayCode = tblEmployee.PayCode " & _
                 " And tblEmployee.CompanyCode = tblCompany.CompanyCode And tblEmployee.DepartmentCode = tblDepartment.DepartmentCode" & g_HODDepartmentCode & _
                 " And TBLLEAVELEDGER.LYEAR = " & TextEdit2.Text & " " & IIf(Len(Trim(g_WhereClause)) = 0, "", " AND " & g_WhereClause) & " order by tblEmployee.PayCode"
            adap = New SqlDataAdapter(strsql, Common.con)
            adap.Fill(Rs_Report)
        End If

        If Rs_Report.Tables(0).Rows.Count < 1 Then
            XtraMessageBox.Show(ulf, "<size=10>No Data Available For this Report.</size>", "<size=9>iAS</size>")
            Exit Sub
        End If
        'Rs_Lvdata.Sort = "PayCode,LeaveCode,DateOffice"
        'Rs_Lvdesc.Sort = "LeaveCode"


        strDeptDivCode = ""

        mFileNumber = FreeFile()
        mstrFile_Name = My.Application.Info.DirectoryPath & "\Reports\iAS_" & Now.ToString("yyyyMMddHHmmss") & ".SRP"
        Dim objWriter As New System.IO.StreamWriter(mstrFile_Name, True)

        'With Rs_Report
        For i As Integer = 0 To Rs_Report.Tables(0).Rows.Count - 1
            objWriter.WriteLine(Space(2) & Space(40) & CommonReport.g_CompanyNames)
            objWriter.WriteLine(Space(2) & "")
            objWriter.WriteLine(Space(2) & "Page No." & mintPageNo)
            objWriter.WriteLine(Space(2) & "                                                     Run Date & Time : " & Now.ToString("dd/MM/yyyy HH:mm"))
            objWriter.WriteLine(Space(2) & "")
            objWriter.WriteLine(Space(2) & "                     Leave Register for the Year :- " & TextEdit2.Text & "")
            mintLine = 6
            mblnCheckReport = True
            strPayCode = Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim
            strGuardianName30 = ""
            If Rs_Report.Tables(0).Rows(i).Item("GuardianName").ToString.Trim <> "" Then
                If Len(Rs_Report.Tables(0).Rows(i).Item("GuardianName").ToString.Trim) > 0 Then strGuardianName30 = "C/o  " & Rs_Report.Tables(0).Rows(i).Item("GuardianName").ToString.Trim
            End If

            objWriter.WriteLine(Space(2) & "---------------------------------------------------------------------------------------")
            objWriter.WriteLine(Space(2) & Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim & "    " & Rs_Report.Tables(0).Rows(i).Item("presentcardno").ToString.Trim & "    " & Rs_Report.Tables(0).Rows(i).Item("EmpName").ToString.Trim & "      " & strGuardianName30)
            objWriter.WriteLine(Space(2) & "---------------------------------------------------------------------------------------")
            mintLine = mintLine + 3
            With Rs_Lvdesc
                'Rs_Lvdesc.MoveFirst()
1:              For j As Integer = 0 To Rs_Lvdesc.Tables(0).Rows.Count - 1 'Do While Not .EOF
                    mLvfiedl = Rs_Lvdesc.Tables(0).Rows(j).Item("leavefield").ToString.Trim
                    mLvfield1 = mLvfiedl & "_add"
                    strAccruedLeave = Rs_Report.Tables(0).Rows(i).Item("mLvfield1").ToString.Trim
                    objWriter.WriteLine(Space(2) & Rs_Lvdesc.Tables(0).Rows(j).Item("Leavecode").ToString.Trim & "  -  " & Rs_Lvdesc.Tables(0).Rows(j).Item("LeaveDescription").ToString.Trim & "  [" & Rs_Lvdesc.Tables(0).Rows(j).Item("LEAVETYPE").ToString.Trim & "]   Accrued :  " & Space(1) & Trim(CStr(Length7(strAccruedLeave))))
                    objWriter.WriteLine(Space(2) & "")
                    mintLine = mintLine + 2
                    For k As Integer = 0 To Rs_Lvdata.Tables(0).Rows.Count - 1 ' With Rs_Lvdata
                        'Rs_Lvdata.MoveFirst()
                        strsql = "PayCode='" & Rs_Report.Tables(0).Rows(i).Item("paycode").ToString.Trim & "'"
                        Rs_Lvdata.Tables(0).Rows.Find(strsql)
                        If Rs_Lvdata.Tables(0).Rows.Count Then 'If Rs_Lvdata.EOF Then
                            strTotaLBalance = Rs_Report.Tables(0).Rows(i).Item("mLvfiedl").ToString.Trim
                            strTotaLBalance = strAccruedLeave - strTotaLBalance
                            objWriter.WriteLine(Space(2) & Space(59) & "                  ---------")
                            objWriter.WriteLine(Space(2) & Space(64) & "  Balance :  " & strSpace & Space(2) & strTotaLBalance.ToString)
                            objWriter.WriteLine(Space(2) & Space(59) & "                  ---------")
                            mintLine = mintLine + 3
                            j = j + 1
                            'Rs_Lvdesc.MoveNext()
                            GoTo 1
                        End If
                        Do While Rs_Lvdata.Tables(0).Rows(k).Item("paycode").ToString.Trim = strPayCode
                            If Rs_Lvdesc.Tables(0).Rows(j).Item("leavecode").ToString.Trim = Rs_Lvdata.Tables(0).Rows(k).Item("FIRSTHALFLEAVECODE").ToString.Trim Then
                                objWriter.WriteLine(Space(2) & Convert.ToDateTime(Rs_Lvdata.Tables(0).Rows(k).Item("DATEOFFICE").ToString.Trim).ToString("dd/MM/yyyy") & "  " & Rs_Lvdata.Tables(0).Rows(k).Item("Voucher_No").ToString.Trim & Space(40) & "    [Posted]   " & Space(1) & Rs_Lvdata.Tables(0).Rows(k).Item("leaveamount1").ToString.Trim)
                            ElseIf Rs_Lvdesc.Tables(0).Rows(j).Item("leavecode").ToString.Trim = Rs_Lvdata.Tables(0).Rows(k).Item("SECONDHALFLEAVECODE").ToString.Trim Then
                                objWriter.WriteLine(Space(2) & Convert.ToDateTime(Rs_Lvdata.Tables(0).Rows(k).Item("DATEOFFICE").ToString.Trim).ToString("dd/MM/yyyy") & "  " & Rs_Lvdata.Tables(0).Rows(k).Item("Voucher_No").ToString.Trim & Space(40) & "    [Posted]   " & Space(1) & Rs_Lvdata.Tables(0).Rows(k).Item("leaveamount2").ToString.Trim)
                            ElseIf Rs_Lvdesc.Tables(0).Rows(j).Item("leavecode").ToString.Trim = Rs_Lvdata.Tables(0).Rows(k).Item("Leavecode").ToString.Trim Then
                                objWriter.WriteLine(Space(2) & Convert.ToDateTime(Rs_Lvdata.Tables(0).Rows(k).Item("DATEOFFICE").ToString.Trim).ToString("dd/MM/yyyy") & "  " & Rs_Lvdata.Tables(0).Rows(k).Item("Voucher_No").ToString.Trim & Space(40) & "    [Posted]   " & Space(1) & Rs_Lvdata.Tables(0).Rows(k).Item("leaveamount").ToString.Trim)
                            End If
                            mintLine = mintLine + 1
                            k = k + 1
                            '.MoveNext()
                            'If .EOF Then
                            '    Exit Do
                            'End If
                        Loop
                        strTotaLBalance = Rs_Report.Tables(0).Rows(i).Item("mLvfiedl").ToString.Trim
                        strTotaLBalance = strAccruedLeave - strTotaLBalance
                        objWriter.WriteLine(Space(2) & Space(59) & "                  ---------")
                        objWriter.WriteLine(Space(2) & Space(64) & "  Balance :  " & strSpace & Space(1) & strTotaLBalance.ToString)
                        objWriter.WriteLine(Space(2) & Space(59) & "                  ---------")
                        mintLine = mintLine + 3
                        If mintLine >= intMaxLine Then
                            objWriter.WriteLine(Space(2) & vbFormFeed)
                            mintLine = 1
                        End If
                    Next 'End With
                    '.MoveNext()
                Next '  Loop
            End With
            '.MoveNext()
        Next
        objWriter.Close()
        Try
            Process.Start(mstrFile_Name)
        Catch ex As Exception
            Process.Start("notepad.exe", mstrFile_Name)
        End Try
    End Sub



    Private Sub Set_Filter_Crystal() 'Optional ByVal Selection As String
        'On Error GoTo ErrorGen
        'Dim i As Integer
        Dim mCompanyString As String, mDepartmentString As String, mCatagoryString As String
        Dim mEmployeeString As String, r_CompantStr As String, mShiftString As String, mLocationString As String
        Dim mDivisionString As String, mGradeString As String, mActive As String

        mCompanyString = ""
        mActive = ""
        mDepartmentString = ""
        mCatagoryString = ""
        mEmployeeString = ""
        mShiftString = ""
        mDivisionString = ""
        mGradeString = ""
        mLocationString = ""
        Dim g_CompanyNames As String = ""
        'For CompanyCode
        If GridViewComp.GetSelectedRows.Count = 0 Then
            GridViewComp.SelectAll()
        End If
        Dim selectedRowsComp As Integer() = GridViewComp.GetSelectedRows()
        Dim resultComp As Object() = New Object(selectedRowsComp.Length - 1) {}
        For i As Integer = 0 To selectedRowsComp.Length - 1
            Dim rowHandle As Integer = selectedRowsComp(i)
            If Not GridViewComp.IsGroupRow(rowHandle) Then
                resultComp(i) = GridViewComp.GetRowCellValue(rowHandle, "COMPANYCODE")
                If i <> 0 Then
                    mCompanyString = mCompanyString + " OR "
                End If
                mCompanyString = mCompanyString + "tblEmployee.CompanyCode = "
                mCompanyString = mCompanyString + "'" + resultComp(i) + "'"
                g_CompanyNames = g_CompanyNames & GridViewComp.GetRowCellValue(rowHandle, "COMPANYNAME") & ", "
            End If
        Next
        If GridViewComp.GetSelectedRows.Count = 0 Then
            GridViewComp.ClearSelection()
        End If
        CommonReport.g_CompanyNames = g_CompanyNames.Trim.TrimStart(",").TrimEnd(",")
        'For i = 0 To (LstCompanyTarget.ListCount - 1)
        '    LstCompanyTarget.ListIndex = i
        '    If i <> 0 Then
        '        mCompanyString = mCompanyString + " OR "
        '    End If
        '    mCompanyString = mCompanyString + "tblEmployee.CompanyCode = "
        '    mCompanyString = mCompanyString + "'" + Left(LstCompanyTarget.Text, 3) + "'"
        '    g_CompanyNames = g_CompanyNames & Trim(Mid(LstCompanyTarget.Text, 6)) & ", "
        'Next i

        'For DepartmentCode
        Dim selectedRowsDept As Integer() = GridViewDept.GetSelectedRows()
        Dim resultDept As Object() = New Object(selectedRowsDept.Length - 1) {}
        For i As Integer = 0 To selectedRowsDept.Length - 1
            Dim rowHandle As Integer = selectedRowsDept(i)
            If Not GridViewDept.IsGroupRow(rowHandle) Then
                resultDept(i) = GridViewDept.GetRowCellValue(rowHandle, "DEPARTMENTCODE")
                If i <> 0 Then
                    mDepartmentString = mDepartmentString + " OR "
                End If
                mDepartmentString = mDepartmentString + "tblEmployee.DepartmentCode = "
                mDepartmentString = mDepartmentString + "'" + resultDept(i) + "'"
            End If
        Next

        'For CatagoryCode
        Dim selectedRowsCat As Integer() = GridViewCat.GetSelectedRows()
        Dim resultCat As Object() = New Object(selectedRowsCat.Length - 1) {}
        For i As Integer = 0 To selectedRowsCat.Length - 1
            Dim rowHandle As Integer = selectedRowsCat(i)
            If Not GridViewCat.IsGroupRow(rowHandle) Then
                resultCat(i) = GridViewCat.GetRowCellValue(rowHandle, "CAT")
                If i <> 0 Then
                    mCatagoryString = mCatagoryString + " OR "
                End If
                mCatagoryString = mCatagoryString + "tblEmployee.CAT = "
                mCatagoryString = mCatagoryString + "'" + resultCat(i) + "'"
            End If
        Next

        ' For Division Code
        'For i = 0 To (lstDivisionTarget.ListCount - 1)
        '    lstDivisionTarget.ListIndex = i
        '    If i <> 0 Then
        '        mDivisionString = mDivisionString + " OR "
        '    End If
        '    mDivisionString = mDivisionString + "tblEmployee.DivisionCode = "
        '    mDivisionString = mDivisionString + "'" + Left(lstDivisionTarget.Text, 3) + "'"
        'Next i

        ' For Grade Code
        Dim selectedRowsGrade As Integer() = GridViewGrade.GetSelectedRows()
        Dim resultGrade As Object() = New Object(selectedRowsGrade.Length - 1) {}
        For i As Integer = 0 To selectedRowsGrade.Length - 1
            Dim rowHandle As Integer = selectedRowsGrade(i)
            If Not GridViewGrade.IsGroupRow(rowHandle) Then
                resultGrade(i) = GridViewGrade.GetRowCellValue(rowHandle, "GradeCode")
                If i <> 0 Then
                    mGradeString = mGradeString + " OR "
                End If
                mGradeString = mGradeString + "tblEmployee.GradeCode = "
                mGradeString = mGradeString + "'" + resultGrade(i) + "'"
            End If
        Next

        'For EmployeeCode  - Check for late arrival
        Dim selectedRowsEmp As Integer() = GridViewEmp.GetSelectedRows()
        Dim resultEmp As Object() = New Object(selectedRowsEmp.Length - 1) {}
        For i As Integer = 0 To selectedRowsEmp.Length - 1
            Dim rowHandle As Integer = selectedRowsEmp(i)
            If Not GridViewEmp.IsGroupRow(rowHandle) Then
                resultEmp(i) = GridViewEmp.GetRowCellValue(rowHandle, "PAYCODE")
                Dim emp As String = resultEmp(i)
                If i <> 0 Then
                    mEmployeeString = mEmployeeString + " OR "
                End If
                mEmployeeString = mEmployeeString & "tblEmployee.Paycode  = "
                mEmployeeString = mEmployeeString & "'" & emp & "'"
            End If
        Next

        'For Shift
        Dim selectedRowsShift As Integer() = GridViewShift.GetSelectedRows()
        Dim resultShift As Object() = New Object(selectedRowsShift.Length - 1) {}
        For i As Integer = 0 To selectedRowsShift.Length - 1
            Dim rowHandle As Integer = selectedRowsShift(i)
            If Not GridViewShift.IsGroupRow(rowHandle) Then
                resultShift(i) = GridViewShift.GetRowCellValue(rowHandle, "SHIFT")
                If i <> 0 Then
                    mShiftString = mShiftString + " OR "
                End If
                If WhichReport = 1 Then
                    mShiftString = mShiftString + "tblTimeRegister.ShiftAttended = "
                Else
                    mShiftString = mShiftString + "tblTimeRegister.ShiftAttended = "
                End If
                mShiftString = mShiftString + "'" + resultShift(i) + "'"
            End If
        Next

        'For Location
        Dim selectedRowsLocation As Integer() = GridViewBranch.GetSelectedRows()
        Dim resultLocation As Object() = New Object(selectedRowsLocation.Length - 1) {}
        For i As Integer = 0 To selectedRowsLocation.Length - 1
            Dim rowHandle As Integer = selectedRowsLocation(i)
            If Not GridViewBranch.IsGroupRow(rowHandle) Then
                resultLocation(i) = GridViewBranch.GetRowCellValue(rowHandle, "BRANCHCODE")
                If i <> 0 Then
                    mLocationString = mLocationString + " OR "
                End If
                If WhichReport = 1 Then
                    mLocationString = mLocationString + "tblEmployee.BRANCHCODE = "
                Else
                    mLocationString = mLocationString + "tblEmployee.BRANCHCODE = "
                End If
                mLocationString = mLocationString + "'" + resultLocation(i) + "'"
            End If
        Next
        'For i = 0 To (LstShiftTarget.ListCount - 1)
        '    LstShiftTarget.ListIndex = i
        '    If i <> 0 Then
        '        mShiftString = mShiftString + " OR "
        '    End If
        '    If WhichReport = 1 Then
        '        'If g_Report_view Then
        '        mShiftString = mShiftString + "tblTimeRegister.ShiftAttended = "
        '        'Else
        '        '    mShiftString = mShiftString + "tblTimeRegisterd.ShiftAttended = "
        '        'End If
        '    Else
        '        'If g_Report_view Then
        '        mShiftString = mShiftString + "tblTimeRegister.ShiftAttended = "
        '        'Else
        '        '    mShiftString = mShiftString + "tblTimeRegisterd.ShiftAttended = "
        '        'End If
        '    End If
        '    mShiftString = mShiftString + "'" + Left(LstShiftTarget.Text, 3) + "'"
        'Next i

        If CheckEdit1.Checked And CheckEdit2.Checked Then
            mActive = ""
        Else
            If CheckEdit1.Checked Then
                mActive = "tblEmployee.Active = 'Y'"
            End If
            If CheckEdit2.Checked Then
                mActive = "tblEmployee.Active = 'N'"
            End If
        End If
        Dim mCondition As String
        'Dim g_WhereClause As String
        mCondition = IIf(Len(Trim(mEmployeeString)) = 0, "(", "(" & Trim(mEmployeeString) & ") AND (") & _
                     IIf(Len(Trim(mCatagoryString)) = 0, "", " " & Trim(mCatagoryString) & ") AND (") & _
                     IIf(Len(Trim(mDepartmentString)) = 0, "", " " & Trim(mDepartmentString) & ") AND (") & _
                     IIf(Len(Trim(mCompanyString)) = 0, "", " " & Trim(mCompanyString) & ") AND (") & _
                     IIf(Len(Trim(mShiftString)) = 0, "", " " & Trim(mShiftString) & ") AND (") & _
                     IIf(Len(Trim(mGradeString)) = 0, "", " " & Trim(mGradeString) & ") AND (") & _
                     IIf(Len(Trim(mLocationString)) = 0, "", " " & Trim(mLocationString) & ") AND (") & _
                     IIf(Len(Trim(mActive)) = 0, "", " " & Trim(mActive) & ") AND (")

        If mCondition = "(" Then
            mCondition = ""
        End If
        If Not Len(Trim(mCondition)) = 0 Then
            mCondition = mCondition.Remove(mCondition.Trim.Length - 5)
            'mCondition = LTrim(mCondition, Len(Trim(mCondition)) - 5)
        End If
        g_WhereClause = mCondition

        If Common.USERTYPE <> "A" Then
            Dim emp() As String = Common.Auth_Branch.Split(",")
            Dim ls As New List(Of String)()
            For x As Integer = 0 To emp.Length - 1
                ls.Add(emp(x).Trim)
            Next
            If g_WhereClause = "" Then
                g_WhereClause = " TBLEmployee.BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "')"
            Else
                g_WhereClause = g_WhereClause & " and TBLEmployee.BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "')"
            End If
        End If
        Exit Sub
        'ErrorGen:
        '        MsgBox(Err.Description, vbInformation, "TimeWatch")
    End Sub
    Private Sub PopupContainerEditEmp_QueryResultValue(sender As System.Object, e As DevExpress.XtraEditors.Controls.QueryResultValueEventArgs) Handles PopupContainerEditEmp.QueryResultValue
        Dim selectedRows() As Integer = GridViewEmp.GetSelectedRows
        Dim sb As StringBuilder = New StringBuilder
        For Each selectionRow As Integer In selectedRows
            Dim a As System.Data.DataRowView = GridViewEmp.GetRow(selectionRow)
            If (sb.ToString.Length > 0) Then
                sb.Append(", ")
            End If
            sb.Append(a.Item("PAYCODE"))
        Next
        e.Value = sb.ToString
    End Sub
    Private Sub PopupContainerEditComp_QueryResultValue(sender As System.Object, e As DevExpress.XtraEditors.Controls.QueryResultValueEventArgs) Handles PopupContainerEditComp.QueryResultValue
        Dim selectedRows() As Integer = GridViewComp.GetSelectedRows
        Dim sb As StringBuilder = New StringBuilder
        For Each selectionRow As Integer In selectedRows
            Dim a As System.Data.DataRowView = GridViewComp.GetRow(selectionRow)
            If (sb.ToString.Length > 0) Then
                sb.Append(", ")
            End If
            sb.Append(a.Item("COMPANYCODE"))
        Next
        e.Value = sb.ToString
    End Sub
    Private Sub PopupContainerEditDept_QueryResultValue(sender As System.Object, e As DevExpress.XtraEditors.Controls.QueryResultValueEventArgs) Handles PopupContainerEditDept.QueryResultValue
        Dim selectedRows() As Integer = GridViewDept.GetSelectedRows
        Dim sb As StringBuilder = New StringBuilder
        For Each selectionRow As Integer In selectedRows
            Dim a As System.Data.DataRowView = GridViewDept.GetRow(selectionRow)
            If (sb.ToString.Length > 0) Then
                sb.Append(", ")
            End If
            sb.Append(a.Item("DEPARTMENTCODE"))
        Next
        e.Value = sb.ToString
    End Sub
    Private Sub PopupContainerEditShift_QueryResultValue(sender As System.Object, e As DevExpress.XtraEditors.Controls.QueryResultValueEventArgs) Handles PopupContainerEditShift.QueryResultValue
        Dim selectedRows() As Integer = GridViewShift.GetSelectedRows
        Dim sb As StringBuilder = New StringBuilder
        For Each selectionRow As Integer In selectedRows
            Dim a As System.Data.DataRowView = GridViewShift.GetRow(selectionRow)
            If (sb.ToString.Length > 0) Then
                sb.Append(", ")
            End If
            sb.Append(a.Item("SHIFT"))
        Next
        e.Value = sb.ToString
    End Sub
    Private Sub PopupContainerEditCat_QueryResultValue(sender As System.Object, e As DevExpress.XtraEditors.Controls.QueryResultValueEventArgs) Handles PopupContainerEditCat.QueryResultValue
        Dim selectedRows() As Integer = GridViewCat.GetSelectedRows
        Dim sb As StringBuilder = New StringBuilder
        For Each selectionRow As Integer In selectedRows
            Dim a As System.Data.DataRowView = GridViewCat.GetRow(selectionRow)
            If (sb.ToString.Length > 0) Then
                sb.Append(", ")
            End If
            sb.Append(a.Item("CAT"))
        Next
        e.Value = sb.ToString
    End Sub
    Private Sub PopupContainerEditGrade_QueryResultValue(sender As System.Object, e As DevExpress.XtraEditors.Controls.QueryResultValueEventArgs) Handles PopupContainerEditGrade.QueryResultValue
        Dim selectedRows() As Integer = GridViewGrade.GetSelectedRows
        Dim sb As StringBuilder = New StringBuilder
        For Each selectionRow As Integer In selectedRows
            Dim a As System.Data.DataRowView = GridViewGrade.GetRow(selectionRow)
            If (sb.ToString.Length > 0) Then
                sb.Append(", ")
            End If
            sb.Append(a.Item("GradeCode"))
        Next
        e.Value = sb.ToString
    End Sub
    Private Sub PopupContainerEditLocation_QueryResultValue(sender As System.Object, e As DevExpress.XtraEditors.Controls.QueryResultValueEventArgs) Handles PopupContainerEditLocation.QueryResultValue
        Dim selectedRows() As Integer = GridViewBranch.GetSelectedRows
        Dim sb As StringBuilder = New StringBuilder
        For Each selectionRow As Integer In selectedRows
            Dim a As System.Data.DataRowView = GridViewBranch.GetRow(selectionRow)
            If (sb.ToString.Length > 0) Then
                sb.Append(", ")
            End If
            sb.Append(a.Item("BRANCHCODE"))
        Next
        e.Value = sb.ToString
    End Sub
    Function Length7(ByVal dblA As Double) As String
        Dim strB As String '* 7
        strB = Format(dblA, "0.00")
        Length7 = strB  '.Substring(0, 6)
        'MsgBox("Length7 " & Length7)
    End Function
    Function Length5(ByVal dblA As Double) As String
        Dim strB As String '* 5
        strB = Format(dblA, "0.00")
        Length5 = strB  '.Substring(0, 5)
        'MsgBox("Length5 " & Length5)
    End Function
    Private Sub CheckExcel_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckExcel.CheckedChanged
        If CheckExcel.Checked = True Then
            XtraShortOrder.ShowDialog()
        End If
    End Sub
    Private Sub CheckExcel_VisibleChanged(sender As System.Object, e As System.EventArgs) Handles CheckExcel.VisibleChanged
        If CheckExcel.Visible = False Then
            CheckText.Checked = True
        End If
    End Sub
End Class
