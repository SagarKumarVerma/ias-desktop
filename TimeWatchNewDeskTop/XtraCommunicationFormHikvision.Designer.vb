﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class CommunicationFormHikvision
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.ComboNYearEnd = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboNMonthEnd = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboNDateEnd = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboNYearStr = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboNMonthStr = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboNDateStr = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.DateEditToDate = New DevExpress.XtraEditors.DateEdit()
        Me.DateEditFrmDate = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.CheckEditTimePerioad = New DevExpress.XtraEditors.CheckEdit()
        Me.SimpleButton4 = New DevExpress.XtraEditors.SimpleButton()
        Me.PopupContainerControl1 = New DevExpress.XtraEditors.PopupContainerControl()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.TblMachineBindingSource = New System.Windows.Forms.BindingSource()
        Me.SSSDBDataSet = New iAS.SSSDBDataSet()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colID_NO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLOCATION = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colbranch = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDeviceType = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colA_R = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colcommkey = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colIN_OUT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colHLogin = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colHPassword = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl()
        Me.PopupContainerEdit1 = New DevExpress.XtraEditors.PopupContainerEdit()
        Me.CheckEdit2 = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEdit1 = New DevExpress.XtraEditors.CheckEdit()
        Me.TextEdit1 = New DevExpress.XtraEditors.TextEdit()
        Me.ProgressBarControl1 = New DevExpress.XtraEditors.ProgressBarControl()
        Me.ToggleSwitch2 = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.ToggleSwitch1 = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.SimpleButtonDownload = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButtonRead = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButtonSet = New DevExpress.XtraEditors.SimpleButton()
        Me.TblMachine1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.tblMachine1TableAdapter()
        Me.TblMachineTableAdapter1 = New iAS.SSSDBDataSetTableAdapters.tblMachineTableAdapter()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.ComboNYearEnd.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboNMonthEnd.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboNDateEnd.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboNYearStr.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboNMonthStr.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboNDateStr.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEditToDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEditToDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEditFrmDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEditFrmDate.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditTimePerioad.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PopupContainerControl1.SuspendLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblMachineBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl2.SuspendLayout()
        CType(Me.PopupContainerEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProgressBarControl1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleSwitch2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleSwitch1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.ComboNYearEnd)
        Me.PanelControl1.Controls.Add(Me.ComboNMonthEnd)
        Me.PanelControl1.Controls.Add(Me.ComboNDateEnd)
        Me.PanelControl1.Controls.Add(Me.ComboNYearStr)
        Me.PanelControl1.Controls.Add(Me.ComboNMonthStr)
        Me.PanelControl1.Controls.Add(Me.ComboNDateStr)
        Me.PanelControl1.Controls.Add(Me.DateEditToDate)
        Me.PanelControl1.Controls.Add(Me.DateEditFrmDate)
        Me.PanelControl1.Controls.Add(Me.LabelControl5)
        Me.PanelControl1.Controls.Add(Me.LabelControl4)
        Me.PanelControl1.Controls.Add(Me.CheckEditTimePerioad)
        Me.PanelControl1.Controls.Add(Me.SimpleButton4)
        Me.PanelControl1.Controls.Add(Me.PopupContainerControl1)
        Me.PanelControl1.Controls.Add(Me.PanelControl2)
        Me.PanelControl1.Controls.Add(Me.SimpleButtonDownload)
        Me.PanelControl1.Controls.Add(Me.SimpleButtonRead)
        Me.PanelControl1.Controls.Add(Me.SimpleButtonSet)
        Me.PanelControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelControl1.Location = New System.Drawing.Point(0, 0)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(620, 262)
        Me.PanelControl1.TabIndex = 0
        '
        'ComboNYearEnd
        '
        Me.ComboNYearEnd.Location = New System.Drawing.Point(533, 60)
        Me.ComboNYearEnd.Name = "ComboNYearEnd"
        Me.ComboNYearEnd.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNYearEnd.Properties.Appearance.Options.UseFont = True
        Me.ComboNYearEnd.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNYearEnd.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNYearEnd.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNYearEnd.Properties.Items.AddRange(New Object() {"2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037", "2038", "2039", "2040", "2041", "2042", "2043", "2044", "2045", "2046", "2047", "2048", "2049", "2050", "2051", "2052", "2053", "2054", "2055", "2056", "2057", "2058", "2059", "2060", "2061", "2062", "2063", "2064", "2065", "2066", "2067", "2068", "2069", "2070", "2071", "2072", "2073", "2074", "2075", "2076", "2077", "2078", "2079", "2080", "2081", "2082", "2083", "2084", "2085", "2086", "2087", "2088", "2089"})
        Me.ComboNYearEnd.Size = New System.Drawing.Size(61, 20)
        Me.ComboNYearEnd.TabIndex = 38
        '
        'ComboNMonthEnd
        '
        Me.ComboNMonthEnd.Location = New System.Drawing.Point(461, 60)
        Me.ComboNMonthEnd.Name = "ComboNMonthEnd"
        Me.ComboNMonthEnd.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNMonthEnd.Properties.Appearance.Options.UseFont = True
        Me.ComboNMonthEnd.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNMonthEnd.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNMonthEnd.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNMonthEnd.Properties.Items.AddRange(New Object() {"Baishakh", "Jestha", "Asar", "Shrawan", "Bhadau", "Aswin", "Kartik", "Mansir", "Poush", "Magh", "Falgun", "Chaitra"})
        Me.ComboNMonthEnd.Size = New System.Drawing.Size(66, 20)
        Me.ComboNMonthEnd.TabIndex = 37
        '
        'ComboNDateEnd
        '
        Me.ComboNDateEnd.Location = New System.Drawing.Point(413, 60)
        Me.ComboNDateEnd.Name = "ComboNDateEnd"
        Me.ComboNDateEnd.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNDateEnd.Properties.Appearance.Options.UseFont = True
        Me.ComboNDateEnd.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNDateEnd.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNDateEnd.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNDateEnd.Properties.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32"})
        Me.ComboNDateEnd.Size = New System.Drawing.Size(42, 20)
        Me.ComboNDateEnd.TabIndex = 36
        '
        'ComboNYearStr
        '
        Me.ComboNYearStr.Location = New System.Drawing.Point(533, 34)
        Me.ComboNYearStr.Name = "ComboNYearStr"
        Me.ComboNYearStr.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNYearStr.Properties.Appearance.Options.UseFont = True
        Me.ComboNYearStr.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNYearStr.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNYearStr.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNYearStr.Properties.Items.AddRange(New Object() {"2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037", "2038", "2039", "2040", "2041", "2042", "2043", "2044", "2045", "2046", "2047", "2048", "2049", "2050", "2051", "2052", "2053", "2054", "2055", "2056", "2057", "2058", "2059", "2060", "2061", "2062", "2063", "2064", "2065", "2066", "2067", "2068", "2069", "2070", "2071", "2072", "2073", "2074", "2075", "2076", "2077", "2078", "2079", "2080", "2081", "2082", "2083", "2084", "2085", "2086", "2087", "2088", "2089"})
        Me.ComboNYearStr.Size = New System.Drawing.Size(61, 20)
        Me.ComboNYearStr.TabIndex = 35
        '
        'ComboNMonthStr
        '
        Me.ComboNMonthStr.Location = New System.Drawing.Point(461, 34)
        Me.ComboNMonthStr.Name = "ComboNMonthStr"
        Me.ComboNMonthStr.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNMonthStr.Properties.Appearance.Options.UseFont = True
        Me.ComboNMonthStr.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNMonthStr.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNMonthStr.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNMonthStr.Properties.Items.AddRange(New Object() {"Baishakh", "Jestha", "Asar", "Shrawan", "Bhadau", "Aswin", "Kartik", "Mansir", "Poush", "Magh", "Falgun", "Chaitra"})
        Me.ComboNMonthStr.Size = New System.Drawing.Size(66, 20)
        Me.ComboNMonthStr.TabIndex = 34
        '
        'ComboNDateStr
        '
        Me.ComboNDateStr.Location = New System.Drawing.Point(413, 34)
        Me.ComboNDateStr.Name = "ComboNDateStr"
        Me.ComboNDateStr.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNDateStr.Properties.Appearance.Options.UseFont = True
        Me.ComboNDateStr.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNDateStr.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNDateStr.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNDateStr.Properties.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32"})
        Me.ComboNDateStr.Size = New System.Drawing.Size(42, 20)
        Me.ComboNDateStr.TabIndex = 33
        '
        'DateEditToDate
        '
        Me.DateEditToDate.EditValue = Nothing
        Me.DateEditToDate.Location = New System.Drawing.Point(412, 60)
        Me.DateEditToDate.Name = "DateEditToDate"
        Me.DateEditToDate.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.DateEditToDate.Properties.Appearance.Options.UseFont = True
        Me.DateEditToDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEditToDate.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEditToDate.Properties.Mask.EditMask = "yyyy-MM-dd HH:mm:ss"
        Me.DateEditToDate.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.DateEditToDate.Size = New System.Drawing.Size(169, 20)
        Me.DateEditToDate.TabIndex = 19
        '
        'DateEditFrmDate
        '
        Me.DateEditFrmDate.EditValue = Nothing
        Me.DateEditFrmDate.Location = New System.Drawing.Point(412, 34)
        Me.DateEditFrmDate.Name = "DateEditFrmDate"
        Me.DateEditFrmDate.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.DateEditFrmDate.Properties.Appearance.Options.UseFont = True
        Me.DateEditFrmDate.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEditFrmDate.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEditFrmDate.Properties.Mask.EditMask = "yyyy-MM-dd HH:mm:ss"
        Me.DateEditFrmDate.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.DateEditFrmDate.Size = New System.Drawing.Size(169, 20)
        Me.DateEditFrmDate.TabIndex = 18
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl5.Appearance.Options.UseFont = True
        Me.LabelControl5.Location = New System.Drawing.Point(378, 61)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(15, 14)
        Me.LabelControl5.TabIndex = 17
        Me.LabelControl5.Text = "To"
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl4.Appearance.Options.UseFont = True
        Me.LabelControl4.Location = New System.Drawing.Point(378, 34)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(27, 14)
        Me.LabelControl4.TabIndex = 16
        Me.LabelControl4.Text = "From"
        '
        'CheckEditTimePerioad
        '
        Me.CheckEditTimePerioad.Location = New System.Drawing.Point(414, 12)
        Me.CheckEditTimePerioad.Name = "CheckEditTimePerioad"
        Me.CheckEditTimePerioad.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditTimePerioad.Properties.Appearance.Options.UseFont = True
        Me.CheckEditTimePerioad.Properties.Caption = "Time Period (ZK)"
        Me.CheckEditTimePerioad.Size = New System.Drawing.Size(150, 19)
        Me.CheckEditTimePerioad.TabIndex = 15
        Me.CheckEditTimePerioad.Visible = False
        '
        'SimpleButton4
        '
        Me.SimpleButton4.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButton4.Appearance.Options.UseFont = True
        Me.SimpleButton4.Location = New System.Drawing.Point(378, 216)
        Me.SimpleButton4.Name = "SimpleButton4"
        Me.SimpleButton4.Size = New System.Drawing.Size(150, 23)
        Me.SimpleButton4.TabIndex = 14
        Me.SimpleButton4.Text = "Close"
        '
        'PopupContainerControl1
        '
        Me.PopupContainerControl1.Controls.Add(Me.GridControl1)
        Me.PopupContainerControl1.Location = New System.Drawing.Point(5, 222)
        Me.PopupContainerControl1.Name = "PopupContainerControl1"
        Me.PopupContainerControl1.Size = New System.Drawing.Size(340, 175)
        Me.PopupContainerControl1.TabIndex = 13
        '
        'GridControl1
        '
        Me.GridControl1.DataSource = Me.TblMachineBindingSource
        Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControl1.EmbeddedNavigator.Buttons.Append.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.Edit.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.Remove.Visible = False
        Me.GridControl1.Location = New System.Drawing.Point(0, 0)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(340, 175)
        Me.GridControl1.TabIndex = 0
        Me.GridControl1.UseEmbeddedNavigator = True
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'TblMachineBindingSource
        '
        Me.TblMachineBindingSource.DataMember = "tblMachine"
        Me.TblMachineBindingSource.DataSource = Me.SSSDBDataSet
        '
        'SSSDBDataSet
        '
        Me.SSSDBDataSet.DataSetName = "SSSDBDataSet"
        Me.SSSDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colID_NO, Me.colLOCATION, Me.colbranch, Me.colDeviceType, Me.colA_R, Me.GridColumn1, Me.colcommkey, Me.colIN_OUT, Me.colHLogin, Me.colHPassword})
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.Editable = False
        Me.GridView1.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridView1.OptionsSelection.MultiSelect = True
        Me.GridView1.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        Me.GridView1.OptionsView.ColumnAutoWidth = False
        Me.GridView1.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colID_NO, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colID_NO
        '
        Me.colID_NO.FieldName = "ID_NO"
        Me.colID_NO.Name = "colID_NO"
        Me.colID_NO.Visible = True
        Me.colID_NO.VisibleIndex = 1
        '
        'colLOCATION
        '
        Me.colLOCATION.FieldName = "LOCATION"
        Me.colLOCATION.Name = "colLOCATION"
        Me.colLOCATION.Visible = True
        Me.colLOCATION.VisibleIndex = 3
        Me.colLOCATION.Width = 120
        '
        'colbranch
        '
        Me.colbranch.Caption = "GridColumn1"
        Me.colbranch.FieldName = "branch"
        Me.colbranch.Name = "colbranch"
        Me.colbranch.Visible = True
        Me.colbranch.VisibleIndex = 2
        '
        'colDeviceType
        '
        Me.colDeviceType.Caption = "GridColumn1"
        Me.colDeviceType.FieldName = "DeviceType"
        Me.colDeviceType.Name = "colDeviceType"
        '
        'colA_R
        '
        Me.colA_R.FieldName = "A_R"
        Me.colA_R.Name = "colA_R"
        '
        'GridColumn1
        '
        Me.GridColumn1.FieldName = "Purpose"
        Me.GridColumn1.Name = "GridColumn1"
        '
        'colcommkey
        '
        Me.colcommkey.Caption = "GridColumn2"
        Me.colcommkey.FieldName = "commkey"
        Me.colcommkey.Name = "colcommkey"
        '
        'colIN_OUT
        '
        Me.colIN_OUT.Caption = "GridColumn2"
        Me.colIN_OUT.FieldName = "IN_OUT"
        Me.colIN_OUT.Name = "colIN_OUT"
        '
        'colHLogin
        '
        Me.colHLogin.Caption = "HLogin"
        Me.colHLogin.FieldName = "HLogin"
        Me.colHLogin.Name = "colHLogin"
        '
        'colHPassword
        '
        Me.colHPassword.Caption = "HPassword"
        Me.colHPassword.FieldName = "HPassword"
        Me.colHPassword.Name = "colHPassword"
        '
        'PanelControl2
        '
        Me.PanelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.PanelControl2.Controls.Add(Me.PopupContainerEdit1)
        Me.PanelControl2.Controls.Add(Me.CheckEdit2)
        Me.PanelControl2.Controls.Add(Me.CheckEdit1)
        Me.PanelControl2.Controls.Add(Me.TextEdit1)
        Me.PanelControl2.Controls.Add(Me.ProgressBarControl1)
        Me.PanelControl2.Controls.Add(Me.ToggleSwitch2)
        Me.PanelControl2.Controls.Add(Me.LabelControl3)
        Me.PanelControl2.Controls.Add(Me.ToggleSwitch1)
        Me.PanelControl2.Controls.Add(Me.LabelControl2)
        Me.PanelControl2.Controls.Add(Me.LabelControl1)
        Me.PanelControl2.Location = New System.Drawing.Point(12, 12)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(349, 222)
        Me.PanelControl2.TabIndex = 9
        '
        'PopupContainerEdit1
        '
        Me.PopupContainerEdit1.Location = New System.Drawing.Point(5, 46)
        Me.PopupContainerEdit1.Name = "PopupContainerEdit1"
        Me.PopupContainerEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEdit1.Properties.Appearance.Options.UseFont = True
        Me.PopupContainerEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.PopupContainerEdit1.Properties.PopupControl = Me.PopupContainerControl1
        Me.PopupContainerEdit1.Size = New System.Drawing.Size(339, 20)
        Me.PopupContainerEdit1.TabIndex = 9
        '
        'CheckEdit2
        '
        Me.CheckEdit2.EditValue = True
        Me.CheckEdit2.Location = New System.Drawing.Point(11, 100)
        Me.CheckEdit2.Name = "CheckEdit2"
        Me.CheckEdit2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEdit2.Properties.Appearance.Options.UseFont = True
        Me.CheckEdit2.Properties.Caption = "New Logs"
        Me.CheckEdit2.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEdit2.Properties.RadioGroupIndex = 0
        Me.CheckEdit2.Size = New System.Drawing.Size(75, 19)
        Me.CheckEdit2.TabIndex = 4
        Me.CheckEdit2.Visible = False
        '
        'CheckEdit1
        '
        Me.CheckEdit1.Location = New System.Drawing.Point(173, 100)
        Me.CheckEdit1.Name = "CheckEdit1"
        Me.CheckEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEdit1.Properties.Appearance.Options.UseFont = True
        Me.CheckEdit1.Properties.AppearanceFocused.BorderColor = System.Drawing.Color.Transparent
        Me.CheckEdit1.Properties.AppearanceFocused.Options.UseBorderColor = True
        Me.CheckEdit1.Properties.Caption = "All Logs"
        Me.CheckEdit1.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEdit1.Properties.RadioGroupIndex = 0
        Me.CheckEdit1.Size = New System.Drawing.Size(75, 19)
        Me.CheckEdit1.TabIndex = 3
        Me.CheckEdit1.TabStop = False
        Me.CheckEdit1.Visible = False
        '
        'TextEdit1
        '
        Me.TextEdit1.Location = New System.Drawing.Point(11, 161)
        Me.TextEdit1.Name = "TextEdit1"
        Me.TextEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEdit1.Properties.Appearance.Options.UseFont = True
        Me.TextEdit1.Size = New System.Drawing.Size(322, 20)
        Me.TextEdit1.TabIndex = 7
        '
        'ProgressBarControl1
        '
        Me.ProgressBarControl1.Location = New System.Drawing.Point(11, 186)
        Me.ProgressBarControl1.Name = "ProgressBarControl1"
        Me.ProgressBarControl1.Size = New System.Drawing.Size(322, 18)
        Me.ProgressBarControl1.TabIndex = 6
        Me.ProgressBarControl1.Visible = False
        '
        'ToggleSwitch2
        '
        Me.ToggleSwitch2.Location = New System.Drawing.Point(198, 131)
        Me.ToggleSwitch2.Name = "ToggleSwitch2"
        Me.ToggleSwitch2.Properties.OffText = "Off"
        Me.ToggleSwitch2.Properties.OnText = "On"
        Me.ToggleSwitch2.Size = New System.Drawing.Size(95, 24)
        Me.ToggleSwitch2.TabIndex = 5
        Me.ToggleSwitch2.Visible = False
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl3.Appearance.Options.UseFont = True
        Me.LabelControl3.Location = New System.Drawing.Point(5, 136)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(74, 14)
        Me.LabelControl3.TabIndex = 4
        Me.LabelControl3.Text = "LabelControl3"
        Me.LabelControl3.Visible = False
        '
        'ToggleSwitch1
        '
        Me.ToggleSwitch1.Location = New System.Drawing.Point(198, 81)
        Me.ToggleSwitch1.Name = "ToggleSwitch1"
        Me.ToggleSwitch1.Properties.OffText = "Off"
        Me.ToggleSwitch1.Properties.OnText = "On"
        Me.ToggleSwitch1.Size = New System.Drawing.Size(95, 24)
        Me.ToggleSwitch1.TabIndex = 2
        Me.ToggleSwitch1.Visible = False
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl2.Appearance.Options.UseFont = True
        Me.LabelControl2.Location = New System.Drawing.Point(6, 81)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(74, 14)
        Me.LabelControl2.TabIndex = 2
        Me.LabelControl2.Text = "LabelControl2"
        Me.LabelControl2.Visible = False
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Location = New System.Drawing.Point(119, 22)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(74, 14)
        Me.LabelControl1.TabIndex = 1
        Me.LabelControl1.Text = "LabelControl1"
        '
        'SimpleButtonDownload
        '
        Me.SimpleButtonDownload.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonDownload.Appearance.Options.UseFont = True
        Me.SimpleButtonDownload.Location = New System.Drawing.Point(378, 178)
        Me.SimpleButtonDownload.Name = "SimpleButtonDownload"
        Me.SimpleButtonDownload.Size = New System.Drawing.Size(150, 23)
        Me.SimpleButtonDownload.TabIndex = 12
        Me.SimpleButtonDownload.Text = "download"
        '
        'SimpleButtonRead
        '
        Me.SimpleButtonRead.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonRead.Appearance.Options.UseFont = True
        Me.SimpleButtonRead.Location = New System.Drawing.Point(378, 99)
        Me.SimpleButtonRead.Name = "SimpleButtonRead"
        Me.SimpleButtonRead.Size = New System.Drawing.Size(150, 23)
        Me.SimpleButtonRead.TabIndex = 10
        Me.SimpleButtonRead.Text = "readdate"
        '
        'SimpleButtonSet
        '
        Me.SimpleButtonSet.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonSet.Appearance.Options.UseFont = True
        Me.SimpleButtonSet.Location = New System.Drawing.Point(378, 138)
        Me.SimpleButtonSet.Name = "SimpleButtonSet"
        Me.SimpleButtonSet.Size = New System.Drawing.Size(150, 23)
        Me.SimpleButtonSet.TabIndex = 11
        Me.SimpleButtonSet.Text = "setdate"
        '
        'TblMachine1TableAdapter1
        '
        Me.TblMachine1TableAdapter1.ClearBeforeFill = True
        '
        'TblMachineTableAdapter1
        '
        Me.TblMachineTableAdapter1.ClearBeforeFill = True
        '
        'CommunicationFormHikvision
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(620, 262)
        Me.ControlBox = False
        Me.Controls.Add(Me.PanelControl1)
        Me.LookAndFeel.SkinName = "iMaginary"
        Me.LookAndFeel.UseDefaultLookAndFeel = False
        Me.Name = "CommunicationFormHikvision"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Logs Management"
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.ComboNYearEnd.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboNMonthEnd.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboNDateEnd.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboNYearStr.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboNMonthStr.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboNDateStr.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEditToDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEditToDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEditFrmDate.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEditFrmDate.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditTimePerioad.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PopupContainerControl1.ResumeLayout(False)
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblMachineBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl2.ResumeLayout(False)
        Me.PanelControl2.PerformLayout()
        CType(Me.PopupContainerEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProgressBarControl1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleSwitch2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleSwitch1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents CheckEdit2 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit1 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents TextEdit1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents ProgressBarControl1 As DevExpress.XtraEditors.ProgressBarControl
    Friend WithEvents ToggleSwitch2 As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ToggleSwitch1 As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SimpleButtonDownload As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButtonRead As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButtonSet As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SSSDBDataSet As iAS.SSSDBDataSet
    Friend WithEvents TblMachine1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.tblMachine1TableAdapter
    Friend WithEvents TblMachineTableAdapter1 As iAS.SSSDBDataSetTableAdapters.tblMachineTableAdapter
    Friend WithEvents TblMachineBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PopupContainerEdit1 As DevExpress.XtraEditors.PopupContainerEdit
    Friend WithEvents PopupContainerControl1 As DevExpress.XtraEditors.PopupContainerControl
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colID_NO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLOCATION As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents SimpleButton4 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents colbranch As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDeviceType As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colA_R As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents CheckEditTimePerioad As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents DateEditToDate As DevExpress.XtraEditors.DateEdit
    Friend WithEvents DateEditFrmDate As DevExpress.XtraEditors.DateEdit
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents ComboNYearEnd As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboNMonthEnd As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboNDateEnd As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboNYearStr As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboNMonthStr As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboNDateStr As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents colcommkey As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colIN_OUT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colHLogin As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colHPassword As DevExpress.XtraGrid.Columns.GridColumn
End Class
