﻿Imports System.Resources
Imports System.Globalization
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports System.ComponentModel
Imports DevExpress.XtraGrid.Columns
Imports DevExpress.XtraGrid.Views.Base
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports System.Threading
Imports iAS.AscDemo
Imports System.Runtime.InteropServices
Imports iAS.AcsDemo

'Imports DevExpress.XtraRichEdit.Forms.SearchHelper

Public Class XtraAutoDownloadLogs
    Dim ulf As UserLookAndFeel
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    Public Delegate Sub invokeDelegate()
    Public Sub New()
        InitializeComponent()
        If Common.servername = "Access" Then
            'ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\TimeWatch.mdb;Persist Security Info=True;Jet OLEDB:Database Password=SSS"
            Me.TblMachine1TableAdapter1.Fill(Me.SSSDBDataSet.tblMachine1)
            'GridControl1.DataSource = SSSDBDataSet.tblMachine1
        Else
            TblMachineTableAdapter.Connection.ConnectionString = Common.ConnectionString
            Me.TblMachineTableAdapter.Fill(Me.SSSDBDataSet.tblMachine)
            'GridControl1.DataSource = SSSDBDataSet.tblMachine
        End If
    End Sub
    Private Sub XtraAutoDownloadLogs_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        'If Common.servername = "Access" Then
        '    'ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\TimeWatch.mdb;Persist Security Info=True;Jet OLEDB:Database Password=SSS"
        '    Me.TblMachine1TableAdapter1.Fill(Me.SSSDBDataSet.tblMachine1)
        '    GridControl1.DataSource = SSSDBDataSet.tblMachine1
        'Else
        '    'ConnectionString = "Data Source=" & servername & ";Initial Catalog=SSSDB;Integrated Security=True"
        '    TblMachineTableAdapter.Connection.ConnectionString = Common.ConnectionString
        '    Me.TblMachineTableAdapter.Fill(Me.SSSDBDataSet.tblMachine)
        '    GridControl1.DataSource = SSSDBDataSet.tblMachine
        'End If
        Common.loadDevice()
        GridControl1.DataSource = Common.MachineNonAdmin

        LabelControl1.Text = ""
        Timer1.Enabled = True
    End Sub
    Private Sub downloadLogsThread()
        Dim datafile As String = System.Environment.CurrentDirectory & "\Data\" & Now.ToString("yyyyMMddHHmmss") & ".txt"
        Dim vnMachineNumber As Integer
        Dim vnLicense As Long
        Dim vpszIPAddress As String
        Dim vpszNetPort As Integer
        Dim vpszNetPassword As Integer
        Dim vnTimeOut As Long
        Dim vnProtocolType As Long
        Dim vnReadMark As Integer = 1 'new logs

        Dim RowHandles() As Integer = GridView1.GetSelectedRows
        Dim IP, ID_NO, DeviceType As String
        Dim cn As Common = New Common
        Dim failIP As New List(Of String)()

        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim dsM As DataSet = New DataSet
        Dim sSql As String = "select * from tblMachine"
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(dsM)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(dsM)
        End If

        For k As Integer = 0 To dsM.Tables(0).Rows.Count - 1
            IP = dsM.Tables(0).Rows(k).Item("LOCATION").ToString.Trim
            ID_NO = dsM.Tables(0).Rows(k).Item("ID_NO").ToString.Trim
            DeviceType = dsM.Tables(0).Rows(k).Item("DeviceType").ToString.Trim
            Dim IN_OUT As String = dsM.Tables(0).Rows(k).Item("IN_OUT").ToString.Trim
            LabelControl1.Text = "Downloading data from " & IP & "  ..."
            Application.DoEvents()
            If DeviceType.ToString.Trim = "Bio-1/3/4/5/6/7/ATF-203/ATF-395/ACR-872" Then
                vnMachineNumber = ID_NO.ToString  '1
                vpszIPAddress = IP.ToString    '"192.168.0.111"
                vpszNetPort = 5005
                vpszNetPassword = 0
                vnTimeOut = 5000
                vnProtocolType = PROTOCOL_TCPIP
                vnLicense = 1261

                Dim result As String = cn.funcGetGeneralLogData(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense, vnReadMark, False, "T", "A", IN_OUT, ID_NO, datafile, False)
                'MsgBox("Result " & result)
                If result <> "Success" Then
                    If result.Contains("Invalid Serial number") Then
                        'SplashScreenManager.CloseForm(False)
                        XtraMessageBox.Show(ulf, "<size=10>" & result & "</size>", "<size=9>Error</size>")
                        DevExpress.XtraSplashScreen.SplashScreenManager.ShowForm(Me, GetType(WaitForm1), True, True, False)
                        Continue For
                    Else
                        failIP.Add(IP.ToString)
                    End If
                End If
            ElseIf DeviceType.ToString.Trim = "ZK(TFT)" Or DeviceType.ToString.Trim = "Bio-1Pro/ATF305Pro/ATF686Pro" Then
                'Dim sdwEnrollNumber As String = ""
                'Dim idwVerifyMode As Integer
                'Dim idwInOutMode As Integer
                'Dim idwYear As Integer
                'Dim idwMonth As Integer
                'Dim idwDay As Integer
                'Dim idwHour As Integer
                'Dim idwMinute As Integer
                'Dim idwSecond As Integer
                'Dim idwWorkcode As Integer

                'Dim comZK As CommonZK = New CommonZK
                'Dim bIsConnected = False
                'Dim iMachineNumber As Integer
                'Dim idwErrorCode As Integer
                'Dim com As Common = New Common
                'Dim axCZKEM1 As New zkemkeeper.CZKEM
                'bIsConnected = axCZKEM1.Connect_Net(IP, 4370)
                'If bIsConnected = True Then
                '    iMachineNumber = 1 'In fact,when you are using the tcp/ip communication,this parameter will be ignored,that is any integer will all right.Here we use 1.
                '    axCZKEM1.RegEvent(iMachineNumber, 65535) 'Here you can register the realtime events that you want to be triggered(the parameters 65535 means registering all)
                '    axCZKEM1.EnableDevice(iMachineNumber, False) 'disable the device
                '    If axCZKEM1.ReadGeneralLogData(iMachineNumber) Then 'read all the attendance records to the memory
                '        XtraMaster.LabelControl4.Text = "Downloading from " & IP & "..."
                '        Application.DoEvents()
                '        'get records from the memory
                '        Dim x As Integer = 0
                '        Dim startdate As DateTime

                '        Dim paycodelist As New List(Of String)()
                '        While axCZKEM1.SSR_GetGeneralLogData(iMachineNumber, sdwEnrollNumber, idwVerifyMode, idwInOutMode, idwYear, idwMonth, idwDay, idwHour, idwMinute, idwSecond, idwWorkcode)
                '            paycodelist.Add(sdwEnrollNumber)
                '            Dim punchdate = idwYear.ToString() & "-" + idwMonth.ToString("00") & "-" & idwDay.ToString("00") & " " & idwHour.ToString("00") & ":" & idwMinute.ToString("00") & ":" & idwSecond.ToString("00")
                '            If x = 0 Then
                '                startdate = punchdate
                '            End If
                '            com.funcGetGeneralLogDataZK(sdwEnrollNumber, idwVerifyMode, idwInOutMode, punchdate, idwWorkcode)
                '            x = x + 1
                '        End While
                '        Dim paycodeArray = paycodelist.ToArray
                '        'Dim adapA As OleDbDataAdapter
                '        'Dim adap As SqlDataAdapter
                '        'Dim ds As DataSet = New DataSet
                '        For i As Integer = 0 To paycodeArray.Length - 1
                '            Dim sSqltmp As String = "select TblEmployee.PAYCODE, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK from TblEmployee, tblEmployeeShiftMaster where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.ACTIVE = 'Y' and TblEmployee.PRESENTCARDNO = '" & paycodeArray(i) & "'"
                '            ds = New DataSet
                '            If Common.servername = "Access" Then
                '                adapA = New OleDbDataAdapter(sSqltmp, Common.con1)
                '                adapA.Fill(ds)
                '            Else
                '                adap = New SqlDataAdapter(sSqltmp, Common.con)
                '                adap.Fill(ds)
                '            End If

                '            If ds.Tables(0).Rows(i).Item("ISROUNDTHECLOCKWORK").ToString = "Y" Then
                '                com.Process_AllRTC(startdate.AddDays(-1), Now, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim)
                '            Else
                '                com.Process_AllnonRTC(startdate, Now, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(i).Item("PAYCODE").ToString.Trim)
                '            End If
                '        Next
                '    Else
                '        Cursor = Cursors.Default
                '        axCZKEM1.GetLastError(idwErrorCode)
                '        If idwErrorCode <> 0 Then
                '            'MsgBox("Reading data from terminal failed,ErrorCode: " & idwErrorCode, MsgBoxStyle.Exclamation, "Error")
                '            failIP.Add(IP.ToString)
                '        Else
                '            'MsgBox("No data from terminal returns!", MsgBoxStyle.Exclamation, "Error")
                '        End If
                '    End If
                '    axCZKEM1.EnableDevice(iMachineNumber, True)
                'Else
                '    axCZKEM1.GetLastError(idwErrorCode)
                '    'MsgBox("Unable to connect the device,ErrorCode=" & idwErrorCode, MsgBoxStyle.Exclamation, "Error")
                '    failIP.Add(IP.ToString)
                '    Continue For
                'End If
                'axCZKEM1.Disconnect()
                'If Common.servername = "Access" Then
                '    If Common.con1.State <> ConnectionState.Open Then
                '        Common.con1.Open()
                '    End If
                '    cmd1 = New OleDbCommand("delete from Rawdata", Common.con1)
                '    cmd1.ExecuteNonQuery()
                '    If Common.con1.State <> ConnectionState.Closed Then
                '        Common.con1.Close()
                '    End If
                'Else
                '    If Common.con.State <> ConnectionState.Open Then
                '        Common.con.Open()
                '    End If
                '    cmd = New SqlCommand("delete from Rawdata", Common.con)
                '    cmd.ExecuteNonQuery()
                '    If Common.con.State <> ConnectionState.Closed Then
                '        Common.con.Close()
                '    End If
                'End If

                Dim sdwEnrollNumber As String = ""
                Dim idwVerifyMode As Integer
                Dim idwInOutMode As Integer
                Dim idwYear As Integer
                Dim idwMonth As Integer
                Dim idwDay As Integer
                Dim idwHour As Integer
                Dim idwMinute As Integer
                Dim idwSecond As Integer
                Dim idwWorkcode As Integer

                Dim comZK As CommonZK = New CommonZK
                Dim bIsConnected = False
                Dim iMachineNumber As Integer
                Dim idwErrorCode As Integer
                Dim com As Common = New Common
                Dim axCZKEM1 As New zkemkeeper.CZKEM
                bIsConnected = axCZKEM1.Connect_Net(IP, 4370)
                If bIsConnected = True Then
                    iMachineNumber = 1 'In fact,when you are using the tcp/ip communication,this parameter will be ignored,that is any integer will all right.Here we use 1.
                    axCZKEM1.RegEvent(iMachineNumber, 65535) 'Here you can register the realtime events that you want to be triggered(the parameters 65535 means registering all)
                    axCZKEM1.EnableDevice(iMachineNumber, False) 'disable the device
                    If axCZKEM1.ReadGeneralLogData(iMachineNumber) Then 'read all the attendance records to the memory
                        XtraMasterTest.LabelControlStatus.Text = "Downloading from " & IP & "..."
                        Application.DoEvents()
                        'get records from the memory
                        Dim x As Integer = 0
                        Dim startdate As DateTime
                        'Dim LogResult As Boolean = axCZKEM1.ReadNewGLogData(iMachineNumber)
                        Dim paycodelist As New List(Of String)()
                        While axCZKEM1.SSR_GetGeneralLogData(iMachineNumber, sdwEnrollNumber, idwVerifyMode, idwInOutMode, idwYear, idwMonth, idwDay, idwHour, idwMinute, idwSecond, idwWorkcode)
                            paycodelist.Add(sdwEnrollNumber)
                            Dim punchdate = idwYear.ToString() & "-" & idwMonth.ToString("00") & "-" & idwDay.ToString("00") & " " & idwHour.ToString("00") & ":" & idwMinute.ToString("00") & ":" & idwSecond.ToString("00")
                            If x = 0 Then
                                startdate = punchdate
                            End If
                            com.funcGetGeneralLogDataZK(sdwEnrollNumber, idwVerifyMode, idwInOutMode, punchdate, idwWorkcode, "A", IN_OUT, ID_NO, datafile)
                            x = x + 1
                        End While

                        Dim xsSql As String = "UPDATE tblMachine set  [LastDownloaded] ='" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "', Status ='Online' where ID_NO='" & ID_NO & "'"
                        If Common.servername = "Access" Then
                            If Common.con1.State <> ConnectionState.Open Then
                                Common.con1.Open()
                            End If
                            cmd1 = New OleDbCommand(xsSql, Common.con1)
                            cmd1.ExecuteNonQuery()
                            If Common.con1.State <> ConnectionState.Closed Then
                                Common.con1.Close()
                            End If
                        Else
                            If Common.con.State <> ConnectionState.Open Then
                                Common.con.Open()
                            End If
                            cmd = New SqlCommand(xsSql, Common.con)
                            cmd.ExecuteNonQuery()
                            If Common.con.State <> ConnectionState.Closed Then
                                Common.con.Close()
                            End If
                        End If

                        Dim paycodeArray = paycodelist.ToArray
                        'Dim adapA As OleDbDataAdapter
                        'Dim adap As SqlDataAdapter
                        Dim ds As DataSet = New DataSet
                        'ds = New DataSet
                        For i As Integer = 0 To paycodeArray.Length - 1
                            Dim PRESENTCARDNO As String
                            If IsNumeric(paycodeArray(i)) Then
                                PRESENTCARDNO = Convert.ToInt64(paycodeArray(i)).ToString("000000000000")
                            Else
                                PRESENTCARDNO = paycodeArray(i)
                            End If
                            Dim sSqltmp As String = "select TblEmployee.PAYCODE, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK, EmployeeGroup.Id from TblEmployee, tblEmployeeShiftMaster, EmployeeGroup where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.ACTIVE = 'Y' and TblEmployee.PRESENTCARDNO = '" & PRESENTCARDNO & "' and EmployeeGroup.GroupId=TblEmployee.EmployeeGroupId"
                            ' "select TblEmployee.PAYCODE, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK from TblEmployee, tblEmployeeShiftMaster where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.ACTIVE = 'Y' and TblEmployee.PRESENTCARDNO = '" & PRESENTCARDNO & "'"
                            ds = New DataSet
                            If Common.servername = "Access" Then
                                adapA = New OleDbDataAdapter(sSqltmp, Common.con1)
                                adapA.Fill(ds)
                            Else
                                adap = New SqlDataAdapter(sSqltmp, Common.con)
                                adap.Fill(ds)
                            End If
                            If ds.Tables(0).Rows.Count > 0 Then
                                com.Remove_Duplicate_Punches(startdate, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("Id"))
                                If ds.Tables(0).Rows(0).Item("ISROUNDTHECLOCKWORK").ToString = "Y" Then
                                    com.Process_AllRTC(startdate.AddDays(-1), Now, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("Id"))
                                Else
                                    com.Process_AllnonRTC(startdate, Now, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("Id"))
                                    If Common.EmpGrpArr(ds.Tables(0).Rows(0).Item("Id")).SHIFTTYPE = "M" Then
                                        com.Process_AllnonRTCMulti(startdate, Now, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("Id"))
                                    End If
                                End If
                                XtraMasterTest.LabelControlStatus.Text = ""
                                Application.DoEvents()
                            End If
                        Next
                    Else
                        Cursor = Cursors.Default
                        axCZKEM1.GetLastError(idwErrorCode)
                        If idwErrorCode <> 0 Then
                            failIP.Add(IP.ToString)
                        Else
                            'MsgBox("No data from terminal returns!", MsgBoxStyle.Exclamation, "Error")
                        End If
                    End If
                    'axCZKEM1.ClearGLog(iMachineNumber) 'only for javle bank

                    axCZKEM1.EnableDevice(iMachineNumber, True)
                Else
                    axCZKEM1.GetLastError(idwErrorCode)
                    'MsgBox("Unable to connect the device,ErrorCode=" & idwErrorCode, MsgBoxStyle.Exclamation, "Error")
                    failIP.Add(IP.ToString)
                    Continue For
                End If
                axCZKEM1.Disconnect()
                If Common.servername = "Access" Then
                    If Common.con1.State <> ConnectionState.Open Then
                        Common.con1.Open()
                    End If
                    cmd1 = New OleDbCommand("delete from Rawdata", Common.con1)
                    cmd1.ExecuteNonQuery()
                    If Common.con1.State <> ConnectionState.Closed Then
                        Common.con1.Close()
                    End If
                Else
                    If Common.con.State <> ConnectionState.Open Then
                        Common.con.Open()
                    End If
                    cmd = New SqlCommand("delete from Rawdata", Common.con)
                    cmd.ExecuteNonQuery()
                    If Common.con.State <> ConnectionState.Closed Then
                        Common.con.Close()
                    End If
                End If
            ElseIf DeviceType.ToString.Trim = "F9" Then
                vnMachineNumber = ID_NO.ToString  '1
                vpszIPAddress = IP.ToString    '"192.168.0.111"
                vpszNetPort = 5005
                vpszNetPassword = 0
                vnTimeOut = 5000
                vnProtocolType = PROTOCOL_TCPIP
                vnLicense = 7881

                Dim result As String = cn.funcGetGeneralLogData_f9(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense, vnReadMark, False, "T", "A", IN_OUT, ID_NO, datafile, False)
                'MsgBox("Result " & result)
                If result <> "Success" Then
                    failIP.Add(IP.ToString)
                End If
            End If
        Next
        Me.Close()
        'XtraMasterDashBoard.Show()
    End Sub
    Private Sub downloadLogsHikvision()
        Dim RowHandles() As Integer = GridView1.GetSelectedRows
        Dim IP, ID_NO, DeviceType As String
        Dim cn As Common = New Common
        Dim failIP As New List(Of String)()

        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim dsM As DataSet = New DataSet

        Dim sSql As String = "select top 1 * from machinerawpunchAll order by OFFICEPUNCH desc "
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(dsM)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(dsM)
        End If
        Dim StartDate As DateTime
        Dim EndDate As DateTime = Now
        If dsM.Tables(0).Rows.Count = 0 Then
            StartDate = Convert.ToDateTime("2020-01-01 00:00:00")
        Else
            StartDate = Convert.ToDateTime(dsM.Tables(0).Rows(0).Item("OFFICEPUNCH"))
        End If

        dsM = New DataSet
        sSql = "select * from tblMachine"
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(dsM)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(dsM)
        End If
        If dsM.Tables(0).Rows.Count = 0 Then
            Return
        End If
        Dim datafile As String = System.Environment.CurrentDirectory & "\Data\" & Now.ToString("yyyyMMddHHmmss") & ".txt"
        For k As Integer = 0 To dsM.Tables(0).Rows.Count - 1
            IP = dsM.Tables(0).Rows(k).Item("LOCATION").ToString.Trim
            ID_NO = dsM.Tables(0).Rows(k).Item("ID_NO").ToString.Trim
            DeviceType = dsM.Tables(0).Rows(k).Item("DeviceType").ToString.Trim
            Dim IN_OUT As String = dsM.Tables(0).Rows(k).Item("IN_OUT").ToString.Trim
            LabelControl1.Text = "Downloading data from " & IP & "  ..."
            Application.DoEvents()
            If DeviceType.ToString.Trim = "HKSeries" Then
                Dim struDeviceInfoV40 As CHCNetSDK.NET_DVR_DEVICEINFO_V40 = New CHCNetSDK.NET_DVR_DEVICEINFO_V40
                Dim DeviceAdd As String = IP
                Dim userName As String = dsM.Tables(0).Rows(k).Item("HLogin").ToString.Trim
                Dim pwd As String = dsM.Tables(0).Rows(k).Item("HPassword").ToString.Trim
                Dim lUserID As Integer = -1
                Dim failReason As String = ""
                Dim logistatus = cn.HikvisionLogin(DeviceAdd, userName, pwd, struDeviceInfoV40, lUserID, failReason)
                If logistatus = False Then
                    failIP.Add(IP)
                    Continue For
                Else
                    If failReason <> "" Then
                        XtraMessageBox.Show(ulf, "<size=10>" & failReason & "</size>", "Failed")
                        XtraMasterTest.LabelControlStatus.Text = ""
                        Application.DoEvents()
                        logistatus = cn.HikvisionLogOut(lUserID)
                        Continue For
                    End If
                    'serialNo = System.Text.Encoding.UTF8.GetString(struDeviceInfoV40.struDeviceV30.sSerialNumber).TrimEnd(Microsoft.VisualBasic.ChrW(92))
                    datafile = System.Environment.CurrentDirectory & "\Data\" & Now.ToString("yyyyMMddHHmmss") & ".txt"
                    Dim m_lLogNum = 0
                    Dim struCond As CHCNetSDK.NET_DVR_ACS_EVENT_COND = New CHCNetSDK.NET_DVR_ACS_EVENT_COND()
                    struCond.Init()
                    struCond.dwSize = CUInt(Marshal.SizeOf(struCond))
                    Dim MajorType As String = "Event" 'comboBoxMainType.SelectedItem.ToString()
                    struCond.dwMajor = GetAcsEventType.ReturnMajorTypeValue(MajorType)
                    Dim MinorType As String = "FINGERPRINT_COMPARE_PASS" 'comboBoxSecondType.SelectedItem.ToString()
                    struCond.dwMinor = GetAcsEventType.ReturnMinorTypeValue(MinorType)

                    struCond.struStartTime.dwYear = StartDate.Year
                    struCond.struStartTime.dwMonth = StartDate.Month
                    struCond.struStartTime.dwDay = StartDate.Day
                    struCond.struStartTime.dwHour = StartDate.Hour
                    struCond.struStartTime.dwMinute = StartDate.Minute
                    struCond.struStartTime.dwSecond = StartDate.Second
                    struCond.struEndTime.dwYear = EndDate.Year
                    struCond.struEndTime.dwMonth = EndDate.Month
                    struCond.struEndTime.dwDay = EndDate.Day
                    struCond.struEndTime.dwHour = EndDate.Hour
                    struCond.struEndTime.dwMinute = EndDate.Minute
                    struCond.struEndTime.dwSecond = EndDate.Second
                    struCond.byPicEnable = 0
                    struCond.szMonitorID = ""
                    struCond.wInductiveEventType = 65535
                    Dim dwSize As UInteger = struCond.dwSize
                    Dim ptrCond As IntPtr = Marshal.AllocHGlobal(CInt(dwSize))
                    Marshal.StructureToPtr(struCond, ptrCond, False)
                    Dim m_lGetAcsEventHandle As Integer = CHCNetSDK.NET_DVR_StartRemoteConfig(lUserID, CHCNetSDK.NET_DVR_GET_ACS_EVENT, ptrCond, CInt(dwSize), Nothing, IntPtr.Zero)

                    If -1 = m_lGetAcsEventHandle Then
                        failIP.Add(IP)
                        Marshal.FreeHGlobal(ptrCond)
                        Continue For
                        'MessageBox.Show("NET_DVR_StartRemoteConfig FAIL, ERROR CODE" & CHCNetSDK.NET_DVR_GetLastError().ToString(), "Error", MessageBoxButtons.OK)
                        'Return
                    End If

                    Dim dwStatus As Integer = 0
                    Dim Flag As Boolean = True
                    Dim struCFG As CHCNetSDK.NET_DVR_ACS_EVENT_CFG = New CHCNetSDK.NET_DVR_ACS_EVENT_CFG()
                    struCFG.dwSize = CUInt(Marshal.SizeOf(struCFG))
                    Dim dwOutBuffSize As Integer = CInt(struCFG.dwSize)
                    struCFG.init()

                    Dim ptr As IntPtr = Marshal.AllocHGlobal(1024)
                    For n As Integer = 0 To 1024 - 1
                        Marshal.WriteByte(ptr, n, 0)
                    Next

                    While Flag
                        dwStatus = CHCNetSDK.NET_DVR_GetNextRemoteConfig(m_lGetAcsEventHandle, ptr, dwOutBuffSize)

                        Dim StructData As CHCNetSDK.NET_DVR_ACS_EVENT_CFG = New CHCNetSDK.NET_DVR_ACS_EVENT_CFG()
                        'Marshal.PtrToStructure(ptr, StructData)
                        '(CHCNetSDK.NET_DVR_ACS_EVENT_CFG)Marshal.PtrToStructure(ptr, typeof(CHCNetSDK.NET_DVR_ACS_EVENT_CFG))
                        StructData = Marshal.PtrToStructure(ptr, GetType(CHCNetSDK.NET_DVR_ACS_EVENT_CFG))
                        Select Case dwStatus
                            Case CHCNetSDK.NET_SDK_GET_NEXT_STATUS_SUCCESS
                                Try
                                    AddAcsEventToList(StructData, IN_OUT, ID_NO, datafile)
                                Catch ex As Exception
                                    Flag = False
                                End Try
                            Case CHCNetSDK.NET_SDK_GET_NEXT_STATUS_NEED_WAIT
                                Thread.Sleep(200)
                            Case CHCNetSDK.NET_SDK_GET_NEXT_STATUS_FAILED
                                CHCNetSDK.NET_DVR_StopRemoteConfig(m_lGetAcsEventHandle)
                                'MessageBox.Show("NET_SDK_GET_NEXT_STATUS_FAILED" & CHCNetSDK.NET_DVR_GetLastError().ToString(), "Error", MessageBoxButtons.OK)
                                Flag = False
                            Case CHCNetSDK.NET_SDK_GET_NEXT_STATUS_FINISH
                                CHCNetSDK.NET_DVR_StopRemoteConfig(m_lGetAcsEventHandle)
                                Flag = False
                            Case Else
                                'MessageBox.Show("NET_SDK_GET_NEXT_STATUS_UNKOWN" & CHCNetSDK.NET_DVR_GetLastError().ToString(), "Error", MessageBoxButtons.OK)
                                Flag = False
                                CHCNetSDK.NET_DVR_StopRemoteConfig(m_lGetAcsEventHandle)
                        End Select
                    End While

                    Marshal.FreeHGlobal(ptrCond)
                    logistatus = cn.HikvisionLogOut(lUserID)
                End If
            End If
        Next
    End Sub
    Private Sub AddAcsEventToList(ByRef struEventCfg As CHCNetSDK.NET_DVR_ACS_EVENT_CFG, MachineIN_OUT As String, ID_NO As String, datafile As String)
        'If InvokeRequired = True Then
        '    Dim d As SetTextCallback = New SetTextCallback(AddressOf Me.AddAcsEventToList)
        '    Me.Invoke(d, New Object() {struEventCfg})
        '    'Dim tmp = IN_OUT
        'Else
        'Threading.Thread.MemoryBarrier()
        'Thread.Sleep(20)
        'MsgBox(struEventCfg.struAcsEventInfo.dwSerialNo.ToString)
        Dim cn As Common = New Common
        Dim con1 As OleDbConnection
        Dim con As SqlConnection
        If Common.servername = "Access" Then
            con1 = New OleDbConnection(Common.ConnectionString)
        Else
            con = New SqlConnection(Common.ConnectionString)
        End If

        Dim CARDNO As String = System.Text.Encoding.UTF8.GetString(struEventCfg.struAcsEventInfo.byCardNo).Trim
        Dim EMPNO As String = System.Text.Encoding.UTF8.GetString(struEventCfg.struAcsEventInfo.byEmployeeNo).Trim
        If CARDNO = "" And EMPNO.Trim = "" Then
            Exit Sub
        ElseIf CARDNO = "" And EMPNO <> "" Then
            CARDNO = EMPNO
        End If

        If IsNumeric(CARDNO) Then
            CARDNO = Convert.ToInt64(CARDNO).ToString("000000000000")
        End If


        Dim OFFICEPUNCH As String = Convert.ToDateTime(cn.GetStrLogTime(struEventCfg.struTime)).ToString("yyyy-MM-dd HH:mm:ss")
        XtraMasterTest.LabelControlStatus.Text = "Downloading " & CARDNO & " " & OFFICEPUNCH
        Application.DoEvents()

        'Dim IP As String = struEventCfg.struRemoteHostAddr.sIpV4

        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim ds As DataSet = New DataSet
        Dim sSql As String

        'insert in text file
        Dim fs As FileStream = New FileStream(datafile, FileMode.OpenOrCreate, FileAccess.Write)
        Dim sw As StreamWriter = New StreamWriter(fs)
        'find the end of the underlying filestream
        sw.BaseStream.Seek(0, SeekOrigin.End)
        sw.WriteLine(CARDNO & " " & OFFICEPUNCH & " " & ID_NO)
        sw.Flush()
        sw.Close()
        'end insert in text file


        'sSql = "Select Paycode  from tblEmployee where presentcardno='" & CARDNO & "'"
        'sSql = "select TblEmployee.PAYCODE, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK from TblEmployee, tblEmployeeShiftMaster where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.ACTIVE = 'Y' and TblEmployee.presentcardno = '" & CARDNO & "'"
        sSql = "select TblEmployee.PAYCODE, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK, EmployeeGroup.Id from TblEmployee, tblEmployeeShiftMaster, EmployeeGroup where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.ACTIVE = 'Y' and TblEmployee.PRESENTCARDNO = '" & CARDNO & "' and TblEmployee.EmployeeGroupId = EmployeeGroup.GroupId"

        ds = New DataSet
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, con)
            adap.Fill(ds)
        End If
        If ds.Tables(0).Rows.Count > 0 Then
            Dim paycode As String = ds.Tables(0).Rows(0)("Paycode").ToString.Trim
            Dim ISROUNDTHECLOCKWORK As String = ds.Tables(0).Rows(0)("ISROUNDTHECLOCKWORK").ToString.Trim
            'MsgBox(serialNo)
            'Dim sSql2 As String = "select * from tblMachine where MAC_ADDRESS ='" & serialNo.Trim & "'"
            'Dim adap1 As SqlDataAdapter
            'Dim adapA1 As OleDbDataAdapter
            'Dim ds3 As DataSet = New DataSet()
            'If Common.servername = "Access" Then
            '    adapA1 = New OleDbDataAdapter(sSql2, con1)
            '    adapA1.Fill(ds3)
            'Else
            '    adap1 = New SqlDataAdapter(sSql2, con)
            '    adap1.Fill(ds3)
            'End If
            Dim IN_OUT As String
            If MachineIN_OUT = "B" Then
                Dim ds2 As DataSet = New DataSet
                Dim sSql1 = "select count (CARDNO) from MachineRawPunch where CARDNO = '" & CARDNO & "' and FORMAT(OFFICEPUNCH, 'yyyy-MM-dd') = '" & Convert.ToDateTime(OFFICEPUNCH).ToString("yyyy-MM-dd") & "'"

                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql1, con1)
                    adapA.Fill(ds2)
                Else
                    sSql1 = "select count (CARDNO) from MachineRawPunch where CARDNO = '" & CARDNO & "' and convert(varchar, OFFICEPUNCH, 23) = '" & Convert.ToDateTime(OFFICEPUNCH).ToString("yyyy-MM-dd") & "'"
                    adap = New SqlDataAdapter(sSql1, con)
                    adap.Fill(ds2)
                End If

                If ds2.Tables(0).Rows(0).Item(0) = 0 Then
                    IN_OUT = "I"
                Else
                    If ds2.Tables(0).Rows(0).Item(0) Mod 2 = 0 Then
                        IN_OUT = "I"
                    Else
                        IN_OUT = "O"
                    End If
                End If
            Else
                IN_OUT = MachineIN_OUT
            End If
            Try

                Dim del As invokeDelegate = Function()
                                                XtraMasterTest.LabelControlStatus.Text = "Downloading " & CARDNO & " " & OFFICEPUNCH
                                            End Function
                Invoke(del)
                Thread.Sleep(200)
                If Common.servername = "Access" Then
                    con1.Open()
                    cmd1 = New OleDbCommand("insert into MachineRawPunch ([CARDNO],[OFFICEPUNCH],[P_DAY],[ISMANUAL],[ReasonCode],[MC_NO],[INOUT],[PAYCODE]) values('" & CARDNO & "','" & OFFICEPUNCH & "','N', 'N','','" & ID_NO & "','" & IN_OUT & "','" & paycode & "' ) ", con1)
                    cmd1.ExecuteNonQuery()
                    cmd1 = New OleDbCommand("insert into MachineRawPunchAll ([CARDNO],[OFFICEPUNCH],[P_DAY],[ISMANUAL],[ReasonCode],[MC_NO],[INOUT],[PAYCODE]) values('" & CARDNO & "','" & OFFICEPUNCH & "','N', 'N','','" & ID_NO & "','" & IN_OUT & "','" & paycode & "' ) ", con1)
                    cmd1.ExecuteNonQuery()
                    con1.Close()
                Else
                    con.Open()
                    cmd = New SqlCommand("insert into MachineRawPunch ([CARDNO],[OFFICEPUNCH],[P_DAY],[ISMANUAL],[ReasonCode],[MC_NO],[INOUT],[PAYCODE]) values('" & CARDNO & "','" & OFFICEPUNCH & "','N', 'N','','" & ID_NO & "','" & IN_OUT & "','" & paycode & "' ) ", con)
                    cmd.ExecuteNonQuery()
                    cmd = New SqlCommand("insert into MachineRawPunchAll ([CARDNO],[OFFICEPUNCH],[P_DAY],[ISMANUAL],[ReasonCode],[MC_NO],[INOUT],[PAYCODE]) values('" & CARDNO & "','" & OFFICEPUNCH & "','N', 'N','','" & ID_NO & "','" & IN_OUT & "','" & paycode & "' ) ", con)
                    cmd.ExecuteNonQuery()
                    con.Close()
                End If
                Dim pDate As DateTime = Convert.ToDateTime(OFFICEPUNCH)
                cn.Remove_Duplicate_Punches(pDate, paycode, ds.Tables(0).Rows(0).Item("Id"))
                If ISROUNDTHECLOCKWORK = "Y" Then
                    If Common.PrcessMode = "M" Then
                        cn.Process_AllnonRTCINOUT(pDate.AddDays(-1), Now.Date, paycode, paycode, ds.Tables(0).Rows(0).Item("Id"), ISROUNDTHECLOCKWORK)
                    Else
                        cn.Process_AllRTC(pDate.AddDays(-1), Now.Date, paycode, paycode, ds.Tables(0).Rows(0).Item("Id"))
                    End If
                    'cn.Process_AllRTC(pDate.AddDays(-1), Now.Date, paycode, paycode, ds.Tables(0).Rows(0).Item("Id"))
                Else
                    If Common.PrcessMode = "M" Then
                        cn.Process_AllnonRTCINOUT(pDate, Now.Date, paycode, paycode, ds.Tables(0).Rows(0).Item("Id"), ISROUNDTHECLOCKWORK)
                    Else
                        cn.Process_AllnonRTC(pDate, Now.Date, paycode, paycode, ds.Tables(0).Rows(0).Item("Id"))
                    End If
                    'cn.Process_AllnonRTC(pDate, Now.Date, paycode, paycode, ds.Tables(0).Rows(0).Item("Id"))
                End If
                'sms
                If Common.g_SMSApplicable = "" Then
                    Common.Load_SMS_Policy()
                End If
                If Common.g_SMSApplicable = "Y" Then
                    If Common.g_isAllSMS = "Y" Then
                        Try
                            If ds.Tables(0).Rows(0).Item("TELEPHONE1").ToString.Trim <> "" Then
                                Dim SmsContent As String = Common.g_AllContent1 & " " & OFFICEPUNCH & " " & Common.g_AllContent2
                                cn.sendSMS(ds.Tables(0).Rows(0).Item("TELEPHONE1").ToString.Trim, SmsContent)
                            End If
                        Catch ex As Exception

                        End Try
                    End If
                End If
                'sms end
            Catch ex As Exception
            End Try
        End If
        XtraMasterTest.LabelControlStatus.Text = ""
        Application.DoEvents()
    End Sub
    Private Sub Timer1_Tick(sender As System.Object, e As System.EventArgs) Handles Timer1.Tick
        Timer1.Enabled = False
        If Common.LogDownLoadCounter = 1 Then  'if it is from login page
            'Dim downloadLogsThread = New Thread(AddressOf Me.downloadLogsThread)
            downloadLogsThread()
            Common.LogDownLoadCounter = 0
        End If
    End Sub
End Class