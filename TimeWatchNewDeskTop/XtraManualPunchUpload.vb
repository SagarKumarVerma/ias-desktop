﻿Imports Excel = Microsoft.Office.Interop.Excel
Imports DevExpress.LookAndFeel
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports DevExpress.XtraEditors

Public Class XtraManualPunchUpload
    Dim APP As New Excel.Application
    Dim worksheet As Excel.Worksheet
    Dim workbook As Excel.Workbook
    Dim numRow As Integer
    Dim numCol As Integer
    Dim ulf As UserLookAndFeel
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    Dim comclass As Common = New Common
    Public Sub New()
        InitializeComponent()
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True
    End Sub
    Private Sub XtraEmployeeUpload_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        TextEdit1.Text = ""
    End Sub
    Private Sub SimpleButton1_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton1.Click
        If TextEdit1.Text.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Please select the file to upload</size>", "<size>Error</size>")
        Else
            ManualPunchUpdate()
        End If
    End Sub
    Private Sub TextEdit1_Click(sender As System.Object, e As System.EventArgs) Handles TextEdit1.Click
        Dim dlg As New OpenFileDialog()
        ' Filter by Office Files
        dlg.Filter = "Office Files|*.xlsx;*.xls;"
        dlg.ShowDialog()
        TextEdit1.Text = dlg.FileName
    End Sub
    Private Sub ManualPunchUpdate()
        Dim sSql As String
        Dim appWorldtmp As Excel.Application
        Dim wbWorld As Excel.Workbook
        Dim mPath       
        Dim rsA As DataSet 'ADODB.Recordset
        Dim bCardRepl As Boolean       
        Dim TempCardNo As String
        Dim i As Integer     
        Dim rowcnt As Integer       
        Dim mShift As String
        Dim mEmpName As String
        Dim mDept As String
        Dim mBranch As String       
        Dim rsEmp As DataSet, rsComp As DataSet, rsdept As DataSet, rscat As DataSet, RsGrade As DataSet, RsDivision As DataSet, rsShift As DataSet
        Dim mDeptCode As String, tmpPath As String, strsql As String, mupdate As Integer, madded As Integer, mtotrec As Integer
        Dim intFile As Integer
        Dim mPaycode As String
        'Dim cn1 As New ADODB.connection
        Dim mDesig As String, mmgrade As String, mmcat As String
        Dim mDate As DateTime
        Dim mPunchTime As Object
        'Dim mpfno As String 
        Dim MMSEX As String
        Dim mfather As String
        Dim mAddress1 As String
        'Dim mAddress2 As String
        Dim mtel1 As String
        'Dim mtel2 As String
        Dim mComp As String
        Dim mPrintedCardno As String
        Dim mOtApplicable As String
        'Dim mMarital As String
        Dim UserType As String
        Dim Hod As String
        Dim Email As String

        Dim mUid As String
        Dim mPan As String

        Dim adapA As OleDbDataAdapter
        Dim adap As SqlDataAdapter
        'Dim ds As DataSet
        Dim mpinCode As String
        'LBLEMP.Visible = True
        madded = 0
        mupdate = 0
        mtotrec = 0
        'DoEvents()
        rowcnt = 0
        'On Error Resume Next
        Me.Cursor = Cursors.WaitCursor
        mPath = TextEdit1.Text.Trim

        Dim appWorld As Excel.Worksheet
        Try
            appWorldtmp = GetObject(, "Excel.Application")  'look for a running copy of Excel
            If Err.Number <> 0 Then 'If Excel is not running then
                appWorldtmp = CreateObject("Excel.Application") 'run it
            End If
            bCardRepl = False
            i = 0
            intFile = FreeFile()
            Err.Clear()
            wbWorld = appWorldtmp.Workbooks.Open(mPath)
            rowcnt = 2
        Catch ex As Exception
            XtraMessageBox.Show(ulf, "<size=10>" & ex.Message.ToString.Trim & "</size>", "<size=9>Error</size>")
            Exit Sub
        End Try
        Try
            appWorld = wbWorld.Worksheets(1)
        Catch ex As Exception
            appWorldtmp.Quit()
            Me.Cursor = Cursors.Default
            XtraMessageBox.Show(ulf, "<size=10>Sheet not found</size>", "<size=9>Success</size>")
            Exit Sub
        End Try
        Try
            For Each c As Control In Me.Controls
                c.Enabled = False
            Next
            Do While appWorld.Cells(rowcnt, 1).value <> Nothing ' Do While appWorld.Cells(rowcnt, 1).ToString.Trim <> ""
                mtotrec = mtotrec + 3
                mPaycode = Trim(appWorld.Cells(rowcnt, 1).value)
                If Trim(mPaycode) = "" Then GoTo 1                            
                'Try
                mDate = Convert.ToDateTime(Trim(appWorld.Cells(rowcnt, 2).value)).ToString("yyyy-MM-dd")
                '        Catch ex As Exception
                '    mDate = Now.AddDays(-(Now.Day - 1)).ToString("yyyy-MM-dd HH:mm:ss")
                'End Try
                mPunchTime = Trim(appWorld.Cells(rowcnt, 3).value)
                Dim OFFICEPUNCH As DateTime = mDate.ToString("yyyy-MM-dd") & " " & mPunchTime & ":00"
                XtraMasterTest.LabelControlStatus.Text = "Uploading Punch " & mPaycode & " " & OFFICEPUNCH.ToString("dd/MM/yyyy HH:mm")
                Application.DoEvents()

                'sSql = "select tblemployee.PRESENTCARDNO,EmployeeGroup.Id from tblemployee,EmployeeGroup where  EmployeeGroup.GroupId=tblemployee.EmployeeGroupId and tblemployee.active='Y' and tblemployee.PAYCODE ='" & mPaycode & "'"
                sSql = "select TblEmployee.PRESENTCARDNO,tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK,EmployeeGroup.ID, TblEmployee.EMPNAME from tblEmployeeShiftMaster, TblEmployee, EmployeeGroup where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.ACTIVE = 'Y' and TblEmployee.EmployeeGroupId = EmployeeGroup.GroupId and tblemployee.PAYCODE ='" & mPaycode & "'"
                rsA = New DataSet
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(rsA)
                Else
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(rsA)
                End If
                If rsA.Tables(0).Rows.Count = 0 Then
                    Continue Do
                End If
                Try
                    Dim CARDNO As String = rsA.Tables(0).Rows(0).Item("PRESENTCARDNO").ToString.Trim

                    sSql = "Insert Into MachineRawPunch (CARDNO, OFFICEPUNCH,P_DAY,ISMANUAL,PAYCODE) values('" & CARDNO & "','" & OFFICEPUNCH.ToString("yyyy-MM-dd HH:mm:ss") & "','N','Y','" & mPaycode & "')"
                    Dim sSql1 As String = "Insert Into MachineRawPunchAll (CARDNO, OFFICEPUNCH,P_DAY,ISMANUAL,PAYCODE) values('" & CARDNO & "','" & OFFICEPUNCH.ToString("yyyy-MM-dd HH:mm:ss") & "','N','Y','" & mPaycode & "')"
                    If Common.servername = "Access" Then
                        If Common.con1.State <> ConnectionState.Open Then
                            Common.con1.Open()
                        End If
                        cmd1 = New OleDbCommand(sSql, Common.con1)
                        cmd1.ExecuteNonQuery()
                        Try
                            cmd1 = New OleDbCommand(sSql1, Common.con1)
                            cmd1.ExecuteNonQuery()
                        Catch ex As Exception

                        End Try
                        If Common.con1.State <> ConnectionState.Closed Then
                            Common.con1.Close()
                        End If
                    Else
                        If Common.con.State <> ConnectionState.Open Then
                            Common.con.Open()
                        End If
                        cmd = New SqlCommand(sSql, Common.con)
                        cmd.ExecuteNonQuery()
                        Try
                            cmd = New SqlCommand(sSql1, Common.con)
                            cmd.ExecuteNonQuery()
                        Catch ex As Exception

                        End Try
                        If Common.con.State <> ConnectionState.Closed Then
                            Common.con.Close()
                        End If
                    End If

                    If Common.IsParallel = "Y" Then
                        Dim cardNoTmp As String
                        If IsNumeric(CARDNO) Then
                            cardNoTmp = Convert.ToDouble(CARDNO)
                        Else
                            cardNoTmp = CARDNO
                        End If
                        Common.parallelInsert(cardNoTmp, mPaycode, OFFICEPUNCH, "N", "Y", rsA.Tables(0).Rows(0).Item("EMPNAME").ToString.Trim, "0", "0")
                        'Common.parallelInsertVisinEdge(cardNoTmp, mPaycode, OFFICEPUNCH, "N", "Y", rsA.Tables(0).Rows(0).Item("EMPNAME").ToString.Trim, "0", "0", "")
                    End If
                    comclass.Remove_Duplicate_Punches(OFFICEPUNCH, mPaycode, rsA.Tables(0).Rows(i).Item("Id"))
                    If rsA.Tables(0).Rows(i).Item("ISROUNDTHECLOCKWORK").ToString = "Y" Then
                        If Common.PrcessMode = "M" Then
                            comclass.Process_AllnonRTCINOUT(OFFICEPUNCH.AddDays(-1), Now.Date, mPaycode, mPaycode, rsA.Tables(0).Rows(i).Item("Id"), rsA.Tables(0).Rows(i).Item("ISROUNDTHECLOCKWORK").ToString)
                        Else
                            comclass.Process_AllRTC(OFFICEPUNCH.AddDays(-1), Now.Date, mPaycode, mPaycode, rsA.Tables(0).Rows(i).Item("Id"))
                        End If
                        'comclass.Process_AllRTC(OFFICEPUNCH.AddDays(-1), Now.Date, mPaycode, mPaycode, rsA.Tables(0).Rows(i).Item("Id"))
                    Else
                        If Common.PrcessMode = "M" Then
                            comclass.Process_AllnonRTCINOUT(OFFICEPUNCH, Now.Date, mPaycode, mPaycode, rsA.Tables(0).Rows(i).Item("Id"), rsA.Tables(0).Rows(i).Item("ISROUNDTHECLOCKWORK").ToString)
                        Else
                            comclass.Process_AllnonRTC(OFFICEPUNCH, Now.Date, mPaycode, mPaycode, rsA.Tables(0).Rows(i).Item("Id"))
                        End If
                        'comclass.Process_AllnonRTC(OFFICEPUNCH, Now.Date, mPaycode, mPaycode, rsA.Tables(0).Rows(i).Item("Id"))
                        If Common.EmpGrpArr(rsA.Tables(0).Rows(i).Item("Id")).SHIFTTYPE = "M" Then
                            comclass.Process_AllnonRTCMulti(OFFICEPUNCH, Now.Date, mPaycode, mPaycode, rsA.Tables(0).Rows(i).Item("Id"))
                        End If
                    End If
                Catch ex As Exception

                End Try
1:
                rowcnt = rowcnt + 1
            Loop
            Common.LogPost("Manual Punch Excel Upload")
        Catch ex As Exception
            appWorldtmp.Quit()
            Me.Cursor = Cursors.Default
            For Each c As Control In Me.Controls
                c.Enabled = True
            Next
            XtraMasterTest.LabelControlStatus.Text = ""
            Application.DoEvents()
            XtraMessageBox.Show(ulf, "<size=10> Paycode: " & mPaycode & vbCrLf & ex.Message.ToString.Trim & "</size>", "<size=9>Error</size>")
            Exit Sub
        End Try
        appWorldtmp.Quit()
        Me.Cursor = Cursors.Default
        For Each c As Control In Me.Controls
            c.Enabled = True
        Next
        XtraMasterTest.LabelControlStatus.Text = ""
        Application.DoEvents()
        XtraMessageBox.Show(ulf, "<size=10>Upload Completed</size>", "<size=9>Success</size>")
        Me.Close()
    End Sub
End Class