﻿Imports DevExpress.XtraGrid.Views.Grid
Imports System.Resources
Imports System.Globalization
Imports DevExpress.XtraEditors
Imports DevExpress.XtraGrid.Views.Base
Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports System.IO
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraSplashScreen
Imports iAS.AscDemo
Imports DevExpress.Utils
Imports System.Runtime.InteropServices
Imports Newtonsoft.Json
Imports iAS.SADP_Ability_Tool

Public Class XtraDeviceUltra
    'Dim res_man As ResourceManager     'declare Resource manager to access to specific cultureinfo
    'Dim cul As CultureInfo     'declare culture info

    'Dim servername As String
    'Dim con As SqlConnection
    'Dim con1 As OleDbConnection
    'Dim ConnectionString As String
    Dim adap1, adap As SqlDataAdapter
    Dim ds As DataSet
    Dim ulf As UserLookAndFeel
    Public Shared DeviceID As String
    Dim SearchSerial As New List(Of String)()

    Public Sub New()
        InitializeComponent()
        'for xtramessage box
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True
        RepositoryItemLookUpEdit1.DataSource = Common.LocationNonAdmin
        Common.SetGridFont(GridView1, New Font("Tahoma", 11))
        Common.SetGridFont(GridViewSearch, New Font("Tahoma", 11))
    End Sub

    Private Sub FillByToolStripButton_Click(sender As System.Object, e As System.EventArgs)
        Try
            Me.TblMachineTableAdapter.FillBy(Me.SSSDBDataSet.tblMachine)
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub XtraDevice_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Me.Width = Me.Parent.Width
        Me.Height = Me.Parent.Height
        'SplitContainerControl1.Width = SplitContainerControl1.Parent.Width
        SplitContainerControl1.SplitterPosition = (SplitContainerControl1.Parent.Width) * 85 / 100
        'MsgBox(SplitContainerControl1.SplitterPosition)
        'not to do in all pages
        Common.splitforMasterMenuWidth = SplitContainerControl1.Width
        Common.SplitterPosition = SplitContainerControl1.SplitterPosition
        'MsgBox(Common.SplitterPosition)

        'res_man = New ResourceManager("TimeWatchNewDeskTop.Res", GetType(XtraDevice).Assembly)
        'cul = CultureInfo.CreateSpecificCulture("en")
        Me.Text = Common.res_man.GetString("home_title", Common.cul)
        'NavigationPage1.Caption = Common.res_man.GetString("device_tl", Common.cul)
        'NavigationPage2.Caption = Common.res_man.GetString("communication_tl", Common.cul)

        GridView1.Columns.Item(0).Caption = Common.res_man.GetString("controller_id", Common.cul)
        GridView1.Columns.Item(1).Caption = Common.res_man.GetString("protocol", Common.cul)
        GridView1.Columns.Item(2).Caption = Common.res_man.GetString("inout_device_type", Common.cul)
        'GridView1.Columns.Item(3).Caption = Common.res_man.GetString("device_type", Common.cul)
        GridView1.Columns.Item(4).Caption = Common.res_man.GetString("device_ip", Common.cul)
        GridView1.Columns.Item(5).Caption = Common.res_man.GetString("location", Common.cul)
        GridView1.Columns.Item(6).Caption = Common.res_man.GetString("com_key", Common.cul)
        GridView1.Columns.Item(7).Caption = "Serial No" 'Common.res_man.GetString("macadd", Common.cul)

        GridControl1.DataSource = Common.MachineNonAdminUltra
        RepositoryItemLookUpEdit1.DataSource = Common.LocationNonAdmin
        If Common.DeviceDelete <> "Y" Then
            GridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = False
        Else
            GridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = True
        End If

        SidePanelSearch.Visible = False
        SearchSerial.Clear()
    End Sub

    Private Sub GridView1_RowDeleted(sender As System.Object, e As DevExpress.Data.RowDeletedEventArgs) Handles GridView1.RowDeleted
        'Me.TblMachineTableAdapter.Delete(Me.SSSDBDataSet.tblMachine.ID_NOColumn)
        Me.TblMachineTableAdapter.Update(Me.SSSDBDataSet.tblMachine)
        Me.TblMachine1TableAdapter1.Update(Me.SSSDBDataSet.tblMachine1)
        XtraMessageBox.Show(ulf, "<size=10>" & Common.res_man.GetString("deletesuccess", Common.cul) & "</size>", Common.res_man.GetString("msgsuccess", Common.cul))
    End Sub
    Private Sub GridView1_RowUpdated(sender As System.Object, e As DevExpress.XtraGrid.Views.Base.RowObjectEventArgs) Handles GridView1.RowUpdated
        Me.TblMachineTableAdapter.Update(Me.SSSDBDataSet.tblMachine)
        Me.TblMachine1TableAdapter1.Update(Me.SSSDBDataSet.tblMachine1)
    End Sub

    Private Sub GridView1_EditFormShowing(sender As System.Object, e As DevExpress.XtraGrid.Views.Grid.EditFormShowingEventArgs) Handles GridView1.EditFormShowing
        If GridView1.IsNewItemRow(GridView1.FocusedRowHandle) = True Then
            If Common.DeviceAdd <> "Y" Then
                e.Allow = False
                Exit Sub
            End If
        Else
            If Common.DeviceModi <> "Y" Then
                e.Allow = False
                Exit Sub
            End If
        End If

        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
        Try
            DeviceID = row("ID_NO").ToString.Trim
        Catch ex As Exception
            DeviceID = ""
        End Try

        If license.DeviceSerialNo.Count = 0 And license.TrialPerioad = True Then
            e.Allow = False
            Common.CallCompInfoFromDevice = True
            XtraCompanyInfo.ShowDialog()
            Common.CallCompInfoFromDevice = False
            e.Allow = False
            Exit Sub
        Else
            If DeviceID = "" And license.DeviceSerialNo.Count >= license.NoOfDevices Then
                    XtraMessageBox.Show(ulf, "<size=10>Cannot add new Device. Max Device Limite Reached.</size>", "<size=9>iAS</size>")
                    e.Allow = False
                    Exit Sub
                End If
            End If

            e.Allow = False
        XtraDeviceEditUltra.ShowDialog()
        Common.loadDevice()
        GridControl1.DataSource = Common.MachineNonAdminUltra
        For rowHandle As Integer = 0 To GridView1.RowCount - 1
            GridView1.SetRowCellValue(rowHandle, "Status", "")
        Next
        'If Common.servername = "Access" Then   'to refresh the grid
        '    Me.TblMachine1TableAdapter1.Fill(Me.SSSDBDataSet.tblMachine1)
        'Else
        '    Me.TblMachineTableAdapter.Fill(Me.SSSDBDataSet.tblMachine)
        'End If

        'Try
        '    'Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
        '    Dim CellProtocol As String = row(1).ToString.Trim

        '    If CellProtocol = "T" Then
        '        row(1) = "TCP/IP"
        '    ElseIf CellProtocol = "S" Then
        '        row(1) = "USB"
        '    End If

        '    'MsgBox(CellProtocol)
        '    Dim CellInOutType = row(2).ToString.Trim
        '    If CellInOutType = "I" Then
        '        row(2) = "IN"
        '    ElseIf CellInOutType = "O" Then
        '        row(2) = "OUT"
        '    End If
        'Catch
        'End Try
    End Sub
    Private Sub GridView1_CustomColumnDisplayText(sender As System.Object, e As DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs) Handles GridView1.CustomColumnDisplayText
        Try
            Dim view As ColumnView = TryCast(sender, ColumnView)
            If e.Column.FieldName = "A_R" Then
                Dim coll As String = view.GetListSourceRowCellValue(e.ListSourceRowIndex, "A_R")
                If coll = "T" Then
                    e.DisplayText = "TCP/IP"
                ElseIf coll = "S" Then
                    e.DisplayText = "USB"
                End If
            End If

            If e.Column.FieldName = "IN_OUT" Then
                Dim coll As String = view.GetListSourceRowCellValue(e.ListSourceRowIndex, "IN_OUT")
                If coll = "I" Then
                    e.DisplayText = "IN"
                ElseIf coll = "O" Then
                    e.DisplayText = "OUT"
                Else
                    e.DisplayText = "IN/OUT"
                End If
            End If

            If e.Column.FieldName = "Purpose" Then
                Dim coll As String = view.GetListSourceRowCellValue(e.ListSourceRowIndex, "Purpose")
                If coll = "C" Then
                    e.DisplayText = "Canteen"
                ElseIf coll = "A" Then
                    e.DisplayText = "Attendance"
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub
    'Private Sub NavigationPane1_SelectedPageIndexChanged(sender As System.Object, e As System.EventArgs)
    '    If NavigationPane1.SelectedPageIndex = 1 Then
    '        NavigationPage2.Controls.Clear()
    '        Dim form As UserControl = New XtraCommunication
    '        form.Dock = DockStyle.Fill
    '        NavigationPage2.Controls.Add(form)
    '        form.Show()
    '    End If
    'End Sub
    Private Sub GridControl1_EmbeddedNavigator_ButtonClick(sender As System.Object, e As DevExpress.XtraEditors.NavigatorButtonClickEventArgs) Handles GridControl1.EmbeddedNavigator.ButtonClick, GridControlSearch.EmbeddedNavigator.ButtonClick
        If e.Button.ButtonType = DevExpress.XtraEditors.NavigatorButtonType.Remove Then

            If XtraMessageBox.Show(ulf, "<size=10>" & Common.res_man.GetString("askdelete", Common.cul) & "</size>", Common.res_man.GetString("confirmdeleteion", Common.cul),
                                MessageBoxButtons.YesNo, MessageBoxIcon.Question) <> DialogResult.Yes Then
                Me.Validate()
                e.Handled = True
                'MsgBox("Your records have been saved and updated successfully!")
            Else
                Dim sSql As String
                Dim cmd As SqlCommand
                Dim cmd1 As OleDbCommand
                e.Handled = True
                Dim selectedRows As Integer() = GridView1.GetSelectedRows()
                Dim result As Object() = New Object(selectedRows.Length - 1) {}
                For i = 0 To selectedRows.Length - 1
                    Dim rowHandle As Integer = selectedRows(i)
                    If Not GridView1.IsGroupRow(rowHandle) Then
                        Dim ID_NO As String = GridView1.GetRowCellValue(rowHandle, "ID_NO").ToString.Trim
                        sSql = "DELETE from tblMachine where ID_NO ='" & ID_NO & "'"
                        If Common.servername = "Access" Then
                            If Common.con1.State <> ConnectionState.Open Then
                                Common.con1.Open()
                            End If
                            cmd1 = New OleDbCommand(sSql, Common.con1)
                            cmd1.ExecuteNonQuery()
                            If Common.con1.State <> ConnectionState.Closed Then
                                Common.con1.Close()
                            End If
                        Else
                            If Common.con.State <> ConnectionState.Open Then
                                Common.con.Open()
                            End If
                            cmd = New SqlCommand(sSql, Common.con)
                            cmd.ExecuteNonQuery()
                            If Common.con.State <> ConnectionState.Closed Then
                                Common.con.Close()
                            End If
                        End If
                    End If
                Next
                Common.loadDevice()
                GridControl1.DataSource = Common.MachineNonAdmin
                For rowHandle As Integer = 0 To GridView1.RowCount - 1
                    GridView1.SetRowCellValue(rowHandle, "Status", "")
                Next
                'If Common.servername = "Access" Then
                '    Me.TblMachine1TableAdapter1.Fill(Me.SSSDBDataSet.tblMachine1)
                '    GridControl1.DataSource = SSSDBDataSet.tblMachine1
                'Else
                '    TblMachineTableAdapter.Connection.ConnectionString = Common.ConnectionString '"Data Source=DESKTOP-PG6NDV8;Initial Catalog=SSSDB;Integrated Security=True"
                '    Me.TblMachineTableAdapter.Fill(Me.SSSDBDataSet.tblMachine)
                '    GridControl1.DataSource = SSSDBDataSet.tblMachine
                'End If
                XtraMessageBox.Show(ulf, "<size=10>" & Common.res_man.GetString("deletesuccess", Common.cul) & "</size>", Common.res_man.GetString("msgsuccess", Common.cul))

            End If
        End If

    End Sub
    Private Sub GridView1_EditFormPrepared(sender As System.Object, e As DevExpress.XtraGrid.Views.Grid.EditFormPreparedEventArgs) Handles GridView1.EditFormPrepared
        For Each control As Control In e.Panel.Controls
            For Each button As Control In control.Controls
                If (button.Text = "Update") Then
                    button.Text = Common.res_man.GetString("save", Common.cul)
                End If
                If (button.Text = "Cancel") Then
                    button.Text = Common.res_man.GetString("cancel", Common.cul)
                End If
            Next
        Next
    End Sub
    Private Sub BarButtonItem3_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem3.ItemClick
        XtraCommunicationForm.ShowDialog()
        Me.Cursor = Cursors.WaitCursor
        Common.loadDevice()
        GridControl1.DataSource = Common.MachineNonAdmin
        Application.DoEvents()
        Me.Cursor = Cursors.Default
    End Sub
    Private Sub GridView1_ShowingPopupEditForm(sender As System.Object, e As DevExpress.XtraGrid.Views.Grid.ShowingPopupEditFormEventArgs) Handles GridView1.ShowingPopupEditForm
        For Each control As Control In e.EditForm.Controls
            Common.SetFont(control, 9)
        Next control
        e.EditForm.StartPosition = FormStartPosition.CenterParent
    End Sub
    Private Sub BarButtonItem4_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem4.ItemClick
        'XtraFringerDataMgmt.ShowDialog()  'nitin test
    End Sub
    Private Sub BarButtonItem5_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs)
        'XtraRealTimePunches.Show()
    End Sub
    Private Sub BarButtonNewLogs_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonNewLogs.ItemClick
        downloadLogs("N")
        Common.loadDevice()
        GridControl1.DataSource = Common.MachineNonAdmin
        Application.DoEvents()
    End Sub
    Private Sub BarButtonOldLogs_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonOldLogs.ItemClick
        downloadLogs("A")
    End Sub
    Private Sub downloadLogs(ByVal type As Char)
        Dim datafile As String = System.Environment.CurrentDirectory & "\Data\" & Now.ToString("yyyyMMddHHmmss") & ".txt"
        If GridView1.SelectedRowsCount = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>Please select device</size>", "Failed")
            Exit Sub
        End If

        Dim vnMachineNumber As Integer
        Dim vnLicense As Long
        Dim vpszIPAddress As String
        Dim vpszNetPort As Integer
        Dim vpszNetPassword As Integer
        Dim vnTimeOut As Long
        Dim vnProtocolType As Long
        Dim vnReadMark As Integer

        Dim cmd1 As OleDbCommand
        Dim cmd As SqlCommand

        If type = "A" Then   'All logs
            vnReadMark = 0
        ElseIf type = "N" Then   'new logs
            vnReadMark = 1
        End If
        Dim clearLog As Boolean = False

        Dim RowHandles() As Integer = GridView1.GetSelectedRows
        Dim IP, ID_NO, DeviceType, A_R, Purpose As Object
        Dim commkey As Integer
        Dim IN_OUT As String
        Dim cn As Common = New Common
        Dim failIP As New List(Of String)()
        DevExpress.XtraSplashScreen.SplashScreenManager.ShowForm(Me, GetType(WaitForm1), True, True, False)
        For Each rowHandle As Integer In RowHandles
            IP = GridView1.GetRowCellValue(rowHandle, GridView1.Columns("LOCATION"))
            IN_OUT = GridView1.GetRowCellValue(rowHandle, GridView1.Columns("IN_OUT"))
            XtraMasterTest.LabelControlStatus.Text = "Connecting " & IP & "..."
            Application.DoEvents()

            ID_NO = GridView1.GetRowCellValue(rowHandle, GridView1.Columns("ID_NO"))
            DeviceType = GridView1.GetRowCellValue(rowHandle, GridView1.Columns("DeviceType"))
            A_R = GridView1.GetRowCellValue(rowHandle, GridView1.Columns("A_R"))
            Purpose = GridView1.GetRowCellValue(rowHandle, GridView1.Columns("Purpose"))
            If DeviceType.ToString.Trim = "Bio-1/3/4/5/6/7/ATF-203/ATF-395/ACR-872" Then
                'vnMachineNumber = ID_NO.ToString  '1
                'vpszIPAddress = IP.ToString '"192.168.0.111"
                'vpszNetPort = 5005
                'vpszNetPassword = 0
                'vnTimeOut = 5000
                'vnProtocolType = PROTOCOL_TCPIP
                'vnLicense = 1261

                'Dim result As String = cn.funcGetGeneralLogData(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense, vnReadMark, clearLog, A_R, Purpose, IN_OUT, ID_NO, datafile)
                ''MsgBox("Result " & result)
                'If result <> "Success" Then
                '    failIP.Add(IP.ToString)
                'End If
            ElseIf DeviceType.ToString.Trim = "ZK(TFT)" Or DeviceType.ToString.Trim = "Bio-1Pro/ATF305Pro/ATF686Pro" Then
                'Dim adap, adap1 As SqlDataAdapter
                'Dim adapA, adapA1 As OleDbDataAdapter
                'Dim ds, ds1 As DataSet
                'Dim sdwEnrollNumber As String = ""
                'Dim idwVerifyMode As Integer
                'Dim idwInOutMode As Integer
                'Dim idwYear As Integer
                'Dim idwMonth As Integer
                'Dim idwDay As Integer
                'Dim idwHour As Integer
                'Dim idwMinute As Integer
                'Dim idwSecond As Integer
                'Dim idwWorkcode As Integer
                'commkey = Convert.ToInt32(GridView1.GetRowCellValue(rowHandle, GridView1.Columns("commkey")))
                'Dim comZK As CommonZK = New CommonZK
                'Dim bIsConnected = False
                'Dim iMachineNumber As Integer
                'Dim idwErrorCode As Integer
                'Dim com As Common = New Common
                'Dim axCZKEM1 As New zkemkeeper.CZKEM
                'axCZKEM1.SetCommPassword(Convert.ToInt32(commkey))  'to check device commkey and db commkey matches
                'bIsConnected = axCZKEM1.Connect_Net(IP, 4370)
                'If bIsConnected = True Then
                '    iMachineNumber = 1 'In fact,when you are using the tcp/ip communication,this parameter will be ignored,that is any integer will all right.Here we use 1.
                '    axCZKEM1.RegEvent(iMachineNumber, 65535) 'Here you can register the realtime events that you want to be triggered(the parameters 65535 means registering all)
                '    axCZKEM1.EnableDevice(iMachineNumber, False) 'disable the device
                '    Dim McSrno As String = ""
                '    Dim vRet As Boolean = axCZKEM1.GetSerialNumber(iMachineNumber, McSrno)
                '    'If McSrno.Substring(0, 3) = "AF6" Or McSrno.Substring(0, 3) = "AEH" Or McSrno.Substring(0, 3) = "AJV" Or McSrno.Substring(0, 3) = "AOO" Or McSrno.Substring(0, 3) = "719" Or McSrno.Substring(0, 3) = "ALM" Or McSrno.Substring(0, 3) = "701" Or McSrno.Substring(0, 3) = "BYR" Or McSrno.Substring(0, 3) = "OIN" Or McSrno.Substring(0, 3) = "AF6" Or McSrno.Substring(0, 3) = "BYX" Or McSrno.Substring(0, 3) = "AIJ" Or McSrno.Substring(0, 3) = "A6G" Or McSrno.Substring(0, 3) = "AEH" Or McSrno.Substring(0, 3) = "CCG" Or McSrno.Substring(0, 3) = "BJV" _
                '    '    Or Mid(Trim(McSrno), 1, 1) = "A" Or Mid(Trim(McSrno), 1, 3) = "OIN" Or Mid(Trim(McSrno), 1, 5) = "FPCTA" Or Mid(Trim(McSrno), 1, 4) = "0000" Or Mid(Trim(McSrno), 1, 4) = "1808" Or Mid(Trim(McSrno), 1, 4) = "ATPL" Or Mid(Trim(McSrno), 1, 4) = "TIPL" Or Mid(Trim(McSrno), 1, 4) = "ZXJK" Or Mid(Trim(McSrno), 1, 8) = "10122013" Or Mid(Trim(McSrno), 1, 8) = "76140122" Or Mid(Trim(McSrno), 1, 8) = "17614012" Or Mid(Trim(McSrno), 1, 8) = "17214012" Or Mid(Trim(McSrno), 1, 8) = "27022014" Or Mid(Trim(McSrno), 1, 8) = "24042014" Or Mid(Trim(McSrno), 1, 8) = "25042014" _
                '    '    Or Mid(Trim(McSrno), 1, 8) = "26042014" Or Mid(Trim(McSrno), 1, 7) = "SN:0000" Or Mid(Trim(McSrno), 1, 4) = "BOCK" Or McSrno.Substring(0, 1) = "B" Or McSrno.Substring(0, 4) = "2455" Or McSrno.Substring(0, 4) = "CDSL" _
                '    '     Or McSrno = "6583151100400" Then
                '    If Common.SerialNo.Contains(McSrno.Substring(0, 3)) Then
                '    Else
                '        SplashScreenManager.CloseForm(False)
                '        axCZKEM1.EnableDevice(iMachineNumber, True)
                '        axCZKEM1.Disconnect()
                '        XtraMessageBox.Show(ulf, "<size=10>Invalid Serial number " & McSrno & "</size>", "<size=9>Error</size>")
                '        DevExpress.XtraSplashScreen.SplashScreenManager.ShowForm(Me, GetType(WaitForm1), True, True, False)
                '        Continue For
                '    End If
                '    Dim LogResult As Boolean
                '    If type = "A" Then   'all logs
                '        LogResult = axCZKEM1.ReadGeneralLogData(iMachineNumber)
                '    ElseIf type = "N" Then   ' new logs
                '        LogResult = axCZKEM1.ReadNewGLogData(iMachineNumber)
                '    End If
                '    If LogResult Then 'read all the attendance records to the memory
                '        XtraMasterTest.LabelControlStatus.Text = "Downloading from " & IP & "..."
                '        Application.DoEvents()
                '        'get records from the memory
                '        Dim x As Integer = 0
                '        Dim startdate As DateTime
                '        Dim paycodelist As New List(Of String)()
                '        While axCZKEM1.SSR_GetGeneralLogData(iMachineNumber, sdwEnrollNumber, idwVerifyMode, idwInOutMode, idwYear, idwMonth, idwDay, idwHour, idwMinute, idwSecond, idwWorkcode)
                '            paycodelist.Add(sdwEnrollNumber)
                '            Dim punchdate = idwYear.ToString() & "-" + idwMonth.ToString("00") & "-" & idwDay.ToString("00") & " " & idwHour.ToString("00") & ":" & idwMinute.ToString("00") & ":" & idwSecond.ToString("00")
                '            If x = 0 Then
                '                startdate = punchdate
                '            End If
                '            com.funcGetGeneralLogDataZK(sdwEnrollNumber, idwVerifyMode, idwInOutMode, punchdate, idwWorkcode, Purpose, IN_OUT, ID_NO, datafile)
                '            x = x + 1
                '        End While
                '        Dim paycodeArray = paycodelist.Distinct.ToArray ' paycodelist.ToArray
                '        ds = New DataSet
                '        For i As Integer = 0 To paycodeArray.Length - 1
                '            Dim PRESENTCARDNO As String
                '            If IsNumeric(paycodeArray(i)) Then
                '                PRESENTCARDNO = Convert.ToInt64(paycodeArray(i)).ToString("000000000000")
                '            Else
                '                PRESENTCARDNO = paycodeArray(i)
                '            End If
                '            Dim sSqltmp As String = "select TblEmployee.PAYCODE, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK from TblEmployee, tblEmployeeShiftMaster where TblEmployee.PAYCODE = tblEmployeeShiftMaster.PAYCODE and TblEmployee.ACTIVE = 'Y' and TblEmployee.PRESENTCARDNO = '" & PRESENTCARDNO & "'"
                '            ds = New DataSet
                '            If Common.servername = "Access" Then
                '                adapA = New OleDbDataAdapter(sSqltmp, Common.con1)
                '                adapA.Fill(ds)
                '            Else
                '                adap = New SqlDataAdapter(sSqltmp, Common.con)
                '                adap.Fill(ds)
                '            End If
                '            If ds.Tables(0).Rows.Count > 0 Then
                '                cn.Remove_Duplicate_Punches(startdate, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim)
                '                If ds.Tables(0).Rows(0).Item("ISROUNDTHECLOCKWORK").ToString = "Y" Then
                '                    com.Process_AllRTC(startdate.AddDays(-1), Now, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim)
                '                Else
                '                    com.Process_AllnonRTC(startdate, Now, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim, ds.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim)
                '                End If
                '            End If
                '        Next
                '    Else
                '        Cursor = Cursors.Default
                '        axCZKEM1.GetLastError(idwErrorCode)
                '        If idwErrorCode <> 0 Then
                '            failIP.Add(IP.ToString)
                '        Else
                '            'MsgBox("No data from terminal returns!", MsgBoxStyle.Exclamation, "Error")
                '        End If
                '    End If
                '    axCZKEM1.EnableDevice(iMachineNumber, True)
                '    If clearLog = True Then
                '        axCZKEM1.ClearGLog(iMachineNumber)
                '    End If
                'Else
                '    axCZKEM1.GetLastError(idwErrorCode)
                '    failIP.Add(IP.ToString)
                '    Continue For
                'End If
                'axCZKEM1.Disconnect()
                'If Common.servername = "Access" Then
                '    If Common.con1.State <> ConnectionState.Open Then
                '        Common.con1.Open()
                '    End If
                '    cmd1 = New OleDbCommand("delete from Rawdata", Common.con1)
                '    cmd1.ExecuteNonQuery()
                '    If Common.con1.State <> ConnectionState.Closed Then
                '        Common.con1.Close()
                '    End If
                'Else
                '    If Common.con.State <> ConnectionState.Open Then
                '        Common.con.Open()
                '    End If
                '    cmd = New SqlCommand("delete from Rawdata", Common.con)
                '    cmd.ExecuteNonQuery()
                '    If Common.con.State <> ConnectionState.Closed Then
                '        Common.con.Close()
                '    End If
                'End If
            End If
        Next
        '"Connection fail" ' write logic to show connection fail devices
        SplashScreenManager.CloseForm(False)
        Dim failedIpArr() As String = failIP.ToArray
        Dim failIpStr As String = ""
        If failedIpArr.Length > 0 Then
            If failedIpArr.Length = 1 Then
                failIpStr = failedIpArr(0)
            Else
                For i As Integer = 0 To failedIpArr.Length - 1
                    failIpStr = failIpStr & ", " & failedIpArr(i)
                Next
            End If
            XtraMessageBox.Show(ulf, "<size=10>" & failIpStr.TrimStart(",") & " failed to download data</size>", "Failed")
        Else
            XtraMessageBox.Show(ulf, "<size=10>Download data success</size>", "Success")
        End If
        XtraMasterTest.LabelControlStatus.Text = ""
        Application.DoEvents()
    End Sub
    Private Sub BarButtonDeviceStatus_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonDeviceStatus.ItemClick
        'If GridView1.SelectedRowsCount = 0 Then
        '    XtraMessageBox.Show(ulf, "<size=10>Please select device</size>", "Failed")
        '    Exit Sub
        'End If
        'Dim RowHandles() As Integer = GridView1.GetSelectedRows
        Dim IP, ID_NO, DeviceType, A_R, Purpose As Object
        Dim HPassword As String, HLogin As String
        Dim cn As Common = New Common
        Dim Status As String = ""
        For rowHandle As Integer = 0 To GridView1.RowCount - 1
            IP = GridView1.GetRowCellValue(rowHandle, GridView1.Columns("LOCATION"))
            XtraMasterTest.LabelControlStatus.Text = "Connecting " & IP & "..."
            Application.DoEvents()

            ID_NO = GridView1.GetRowCellValue(rowHandle, GridView1.Columns("ID_NO"))
            DeviceType = GridView1.GetRowCellValue(rowHandle, GridView1.Columns("DeviceType"))
            A_R = GridView1.GetRowCellValue(rowHandle, GridView1.Columns("A_R"))
            HLogin = GridView1.GetRowCellValue(rowHandle, GridView1.Columns("HLogin"))
            HPassword = GridView1.GetRowCellValue(rowHandle, GridView1.Columns("HPassword"))

            Dim struDeviceInfoV40 As CHCNetSDK.NET_DVR_DEVICEINFO_V40 = New CHCNetSDK.NET_DVR_DEVICEINFO_V40
            Dim DeviceAdd As String = IP.Trim, userName As String = HLogin.Trim, pwd As String = HPassword.Trim
            Dim lUserID As Integer = -1
            Dim failReason
            Dim logistatus = cn.HikvisionLogin(DeviceAdd, userName, pwd, struDeviceInfoV40, lUserID, failReason)
            If logistatus = False Then
                GridView1.SetRowCellValue(rowHandle, "Status", "Offline")
                Status = "Offline"
                Application.DoEvents()
                Dim str As String = "update tblmachine set Status='" & Status & "' where location='" & IP & "'"
                If Common.servername = "Access" Then
                    If Common.con1.State <> ConnectionState.Open Then
                        Common.con1.Open()
                    End If
                    Try
                        Dim cmd1 As OleDbCommand = New OleDbCommand(str, Common.con1)
                        cmd1.ExecuteNonQuery()
                    Catch ex As Exception

                    End Try
                    If Common.con1.State <> ConnectionState.Closed Then
                        Common.con1.Close()
                    End If
                Else
                    If Common.con.State <> ConnectionState.Open Then
                        Common.con.Open()
                    End If
                    Try
                        Dim cmd As SqlCommand = New SqlCommand(str, Common.con)
                        cmd.ExecuteNonQuery()
                    Catch ex As Exception

                    End Try
                    If Common.con.State <> ConnectionState.Closed Then
                        Common.con.Close()
                    End If
                End If
            Else
                GridView1.SetRowCellValue(rowHandle, "Status", "Online")
                Status = "Online"
                'colorCells.Add(rowHandle)
                'GridView1.RefreshRow(rowHandle)
                Application.DoEvents()
                'Dim serialNo As String = System.Text.Encoding.UTF8.GetString(struDeviceInfoV40.struDeviceV30.sSerialNumber).TrimEnd(Microsoft.VisualBasic.ChrW(92))
                'TextEdit3.Text = serialNo.Trim

                Dim UpdateStr As String = ""

                Dim iUserNumber As Integer
                Dim facecount As Integer
                Dim cardCount As Integer
                Dim ptrOutBuf As IntPtr
                Dim ptrStatusBuffer As IntPtr
                Dim sUrl As String
                Dim ptrURL As IntPtr
                Dim ptrInput As IntPtr
                Dim ptrOuput As IntPtr
#Region "UserCount"
                Try
                    ptrOutBuf = Marshal.AllocHGlobal(1024)
                    ptrStatusBuffer = Marshal.AllocHGlobal(1024)

                    For i As Integer = 0 To 1024 - 1
                        Marshal.WriteByte(ptrOutBuf, i, 0)
                        Marshal.WriteByte(ptrStatusBuffer, i, 0)
                    Next

                    Dim struInput As CHCNetSDK.NET_DVR_XML_CONFIG_INPUT = New CHCNetSDK.NET_DVR_XML_CONFIG_INPUT()
                    Dim struOuput As CHCNetSDK.NET_DVR_XML_CONFIG_OUTPUT = New CHCNetSDK.NET_DVR_XML_CONFIG_OUTPUT()
                    sUrl = "GET /ISAPI/AccessControl/UserInfo/Count?format=json"
                    ptrURL = Marshal.StringToHGlobalAnsi(sUrl)
                    struInput.dwSize = CUInt(Marshal.SizeOf(struInput))
                    struInput.lpRequestUrl = ptrURL
                    struInput.dwRequestUrlLen = CUInt(sUrl.Length)
                    struOuput.dwSize = CUInt(Marshal.SizeOf(struOuput))
                    struOuput.lpOutBuffer = ptrOutBuf
                    struOuput.dwOutBufferSize = 1024
                    struOuput.lpStatusBuffer = ptrStatusBuffer
                    struOuput.dwStatusSize = 1024
                    ptrInput = Marshal.AllocHGlobal(Marshal.SizeOf(struInput))
                    Marshal.StructureToPtr(struInput, ptrInput, False)
                    ptrOuput = Marshal.AllocHGlobal(Marshal.SizeOf(struOuput))
                    Marshal.StructureToPtr(struOuput, ptrOuput, False)

                    If Not CHCNetSDK.NET_DVR_STDXMLConfig(lUserID, ptrInput, ptrOuput) Then
                        Dim listItem As ListViewItem = New ListViewItem()
                        listItem.Text = "Fail"
                        Dim strTemp As String = String.Format("Get User Info Count Fail,Error Code={0}", CHCNetSDK.NET_DVR_GetLastError())
                        listItem.SubItems.Add(strTemp)
                        'Me.AddList(listViewMessage, listItem)
                    Else
                        Dim listItem As ListViewItem = New ListViewItem()
                        listItem.Text = "Succ"
                        Dim strTemp As String = String.Format("Get User Info Count Success")
                        listItem.SubItems.Add(strTemp)
                        'Me.AddList(listViewMessage, listItem)
                        Dim strUserInfoCount As String = Marshal.PtrToStringAnsi(struOuput.lpOutBuffer)
                        Dim JsonUserInfoCount As CUserInfoCount = New CUserInfoCount()
                        JsonUserInfoCount = JsonConvert.DeserializeObject(Of CUserInfoCount)(strUserInfoCount)
                        iUserNumber = JsonUserInfoCount.UserInfoCount.userNumber
                        'textBoxUserInfoCount.Text = iUserNumber.ToString()
                        UpdateStr = "UserCount='" & iUserNumber & "'"
                    End If

                    Marshal.FreeHGlobal(ptrOutBuf)
                    Marshal.FreeHGlobal(ptrStatusBuffer)
                    Marshal.FreeHGlobal(ptrInput)
                    Marshal.FreeHGlobal(ptrOuput)
                    Marshal.FreeHGlobal(ptrURL)
                Catch ex As Exception
                    Try
                        Marshal.FreeHGlobal(ptrOutBuf)
                        Marshal.FreeHGlobal(ptrStatusBuffer)
                        Marshal.FreeHGlobal(ptrInput)
                        Marshal.FreeHGlobal(ptrOuput)
                        Marshal.FreeHGlobal(ptrURL)
                    Catch exU As Exception

                    End Try
                End Try
#End Region
#Region "FaceCount"
                Try
                    ptrOutBuf = Marshal.AllocHGlobal(1024)
                    ptrStatusBuffer = Marshal.AllocHGlobal(1024)

                    For i As Integer = 0 To 1024 - 1
                        Marshal.WriteByte(ptrOutBuf, i, 0)
                        Marshal.WriteByte(ptrStatusBuffer, i, 0)
                    Next

                    Dim struInputF As CHCNetSDK.NET_DVR_XML_CONFIG_INPUT = New CHCNetSDK.NET_DVR_XML_CONFIG_INPUT()
                    Dim struOuputF As CHCNetSDK.NET_DVR_XML_CONFIG_OUTPUT = New CHCNetSDK.NET_DVR_XML_CONFIG_OUTPUT()
                    sUrl = "GET /ISAPI/Intelligent/FDLib/Count?format=json&FDID=1&faceLibType=blackFD"
                    ptrURL = Marshal.StringToHGlobalAnsi(sUrl)
                    struInputF.dwSize = CUInt(Marshal.SizeOf(struInputF))
                    struInputF.lpRequestUrl = ptrURL
                    struInputF.dwRequestUrlLen = CUInt(sUrl.Length)
                    struOuputF.dwSize = CUInt(Marshal.SizeOf(struOuputF))
                    struOuputF.lpOutBuffer = ptrOutBuf
                    struOuputF.dwOutBufferSize = 1024
                    struOuputF.lpStatusBuffer = ptrStatusBuffer
                    struOuputF.dwStatusSize = 1024
                    ptrInput = Marshal.AllocHGlobal(Marshal.SizeOf(struInputF))
                    Marshal.StructureToPtr(struInputF, ptrInput, False)
                    ptrOuput = Marshal.AllocHGlobal(Marshal.SizeOf(struOuputF))
                    Marshal.StructureToPtr(struOuputF, ptrOuput, False)

                    If Not CHCNetSDK.NET_DVR_STDXMLConfig(lUserID, ptrInput, ptrOuput) Then
                        Dim listItem As ListViewItem = New ListViewItem()
                        listItem.Text = "Fail"
                        Dim strTemp As String = String.Format("Get Face Record Count Fail,Error Code={0}", CHCNetSDK.NET_DVR_GetLastError())
                        listItem.SubItems.Add(strTemp)
                        'Me.AddList(listViewMessage, listItem)
                    Else
                        Dim listItem As ListViewItem = New ListViewItem()
                        listItem.Text = "Succ"
                        Dim strTemp As String = String.Format("Get Face Record Count Success")
                        listItem.SubItems.Add(strTemp)
                        'Me.AddList(listViewMessage, listItem)
                        Dim strFaceRecordCount As String = Marshal.PtrToStringAnsi(struOuputF.lpOutBuffer)
                        Dim JsonFaceRecordCount As CFaceRecordCount = New CFaceRecordCount()
                        JsonFaceRecordCount = JsonConvert.DeserializeObject(Of CFaceRecordCount)(strFaceRecordCount)
                        facecount = JsonFaceRecordCount.recordDataNumber
                        'textBoxRecordDataNumber.Text = iRecordDataNumber.ToString()
                        UpdateStr = UpdateStr & ",FaceCount='" & facecount & "'"
                    End If

                    Marshal.FreeHGlobal(ptrOutBuf)
                    Marshal.FreeHGlobal(ptrStatusBuffer)
                    Marshal.FreeHGlobal(ptrInput)
                    Marshal.FreeHGlobal(ptrOuput)
                    Marshal.FreeHGlobal(ptrURL)
                Catch ex As Exception
                    Try
                        Marshal.FreeHGlobal(ptrOutBuf)
                        Marshal.FreeHGlobal(ptrStatusBuffer)
                        Marshal.FreeHGlobal(ptrInput)
                        Marshal.FreeHGlobal(ptrOuput)
                        Marshal.FreeHGlobal(ptrURL)
                    Catch exF As Exception

                    End Try
                End Try
#End Region
#Region "CardCount"
                Try
                    ptrOutBuf = Marshal.AllocHGlobal(1024)
                    ptrStatusBuffer = Marshal.AllocHGlobal(1024)

                    For i As Integer = 0 To 1024 - 1
                        Marshal.WriteByte(ptrOutBuf, i, 0)
                        Marshal.WriteByte(ptrStatusBuffer, i, 0)
                    Next

                    Dim struInputC As CHCNetSDK.NET_DVR_XML_CONFIG_INPUT = New CHCNetSDK.NET_DVR_XML_CONFIG_INPUT()
                    Dim struOuputC As CHCNetSDK.NET_DVR_XML_CONFIG_OUTPUT = New CHCNetSDK.NET_DVR_XML_CONFIG_OUTPUT()
                    sUrl = "GET /ISAPI/AccessControl/CardInfo/Count?format=json"
                    ptrURL = Marshal.StringToHGlobalAnsi(sUrl)
                    struInputC.dwSize = CUInt(Marshal.SizeOf(struInputC))
                    struInputC.lpRequestUrl = ptrURL
                    struInputC.dwRequestUrlLen = CUInt(sUrl.Length)
                    struOuputC.dwSize = CUInt(Marshal.SizeOf(struOuputC))
                    struOuputC.lpOutBuffer = ptrOutBuf
                    struOuputC.dwOutBufferSize = 1024
                    struOuputC.lpStatusBuffer = ptrStatusBuffer
                    struOuputC.dwStatusSize = 1024
                    ptrInput = Marshal.AllocHGlobal(Marshal.SizeOf(struInputC))
                    Marshal.StructureToPtr(struInputC, ptrInput, False)
                    ptrOuput = Marshal.AllocHGlobal(Marshal.SizeOf(struOuputC))
                    Marshal.StructureToPtr(struOuputC, ptrOuput, False)

                    If Not CHCNetSDK.NET_DVR_STDXMLConfig(lUserID, ptrInput, ptrOuput) Then
                        Dim listItem As ListViewItem = New ListViewItem()
                        listItem.Text = "Fail"
                        Dim strTemp As String = String.Format("Get Card Info Count Fail,Error Code={0}", CHCNetSDK.NET_DVR_GetLastError())
                        listItem.SubItems.Add(strTemp)
                        'Me.AddList(listViewMessage, listItem)
                    Else
                        Dim listItem As ListViewItem = New ListViewItem()
                        listItem.Text = "Succ"
                        Dim strTemp As String = String.Format("Get Card Info Count Success")
                        listItem.SubItems.Add(strTemp)
                        'Me.AddList(listViewMessage, listItem)
                        Dim strCardInfoCount As String = Marshal.PtrToStringAnsi(struOuputC.lpOutBuffer)
                        Dim JsonCardInfoCount As CCardInfoCount = New CCardInfoCount()
                        JsonCardInfoCount = JsonConvert.DeserializeObject(Of CCardInfoCount)(strCardInfoCount)
                        'textBoxCardInfoCount.Text = JsonCardInfoCount.CardInfoCount.cardNumber.ToString()
                        cardCount = JsonCardInfoCount.CardInfoCount.cardNumber.ToString()
                    End If

                    Marshal.FreeHGlobal(ptrOutBuf)
                    Marshal.FreeHGlobal(ptrStatusBuffer)
                    Marshal.FreeHGlobal(ptrInput)
                    Marshal.FreeHGlobal(ptrOuput)
                    Marshal.FreeHGlobal(ptrURL)
                Catch ex As Exception
                    Try
                        Marshal.FreeHGlobal(ptrOutBuf)
                        Marshal.FreeHGlobal(ptrStatusBuffer)
                        Marshal.FreeHGlobal(ptrInput)
                        Marshal.FreeHGlobal(ptrOuput)
                        Marshal.FreeHGlobal(ptrURL)
                    Catch exC As Exception

                    End Try
                End Try
#End Region
                logistatus = cn.HikvisionLogOut(lUserID)

                UpdateStr = UpdateStr.TrimStart(",")

                If UpdateStr.Trim <> "" Then

                    Dim str As String = "update tblmachine set " & UpdateStr & ", Status='" & Status & "' where location='" & IP & "'"
                    If Common.servername = "Access" Then
                        If Common.con1.State <> ConnectionState.Open Then
                            Common.con1.Open()
                        End If
                        Try
                            Dim cmd1 As OleDbCommand = New OleDbCommand(str, Common.con1)
                            cmd1.ExecuteNonQuery()
                        Catch ex As Exception

                        End Try
                        If Common.con1.State <> ConnectionState.Closed Then
                            Common.con1.Close()
                        End If
                    Else
                        If Common.con.State <> ConnectionState.Open Then
                            Common.con.Open()
                        End If
                        Try
                            Dim cmd As SqlCommand = New SqlCommand(str, Common.con)
                            cmd.ExecuteNonQuery()
                        Catch ex As Exception

                        End Try
                        If Common.con.State <> ConnectionState.Closed Then
                            Common.con.Close()
                        End If
                    End If
                    Common.loadDevice()
                End If
            End If
        Next
        XtraMasterTest.LabelControlStatus.Text = ""
        Application.DoEvents()
    End Sub
    Dim colorCells As List(Of Integer) = New List(Of Integer)
    Private Sub GridView1_RowStyle(sender As System.Object, e As DevExpress.XtraGrid.Views.Grid.RowStyleEventArgs) Handles GridView1.RowStyle
        If colorCells.Contains(e.RowHandle) Then
            e.Appearance.ForeColor = Color.Red
            e.HighPriority = True
        End If
    End Sub
    Private Sub GridView1_RowCellStyle(sender As System.Object, e As DevExpress.XtraGrid.Views.Grid.RowCellStyleEventArgs) Handles GridView1.RowCellStyle
        Dim view As GridView = TryCast(sender, GridView)
        Dim _mark As Boolean = CBool(view.GetRowCellValue(e.RowHandle, "Online"))
        If e.Column.FieldName = "Status" Then
            If IsNothing(e.CellValue) = False Then
                If e.CellValue.ToString = "Online" Then
                    e.Appearance.ForeColor = Color.Green 'If(_mark, Color.LightGreen, Color.LightSalmon)
                    e.Appearance.TextOptions.HAlignment = If(_mark, HorzAlignment.Center, HorzAlignment.Near)
                ElseIf e.CellValue.ToString = "Offline" Then
                    e.Appearance.ForeColor = Color.Red 'If(_mark, Color.LightGreen, Color.LightSalmon)
                    e.Appearance.TextOptions.HAlignment = If(_mark, HorzAlignment.Center, HorzAlignment.Near)
                End If
            End If
        End If
        If e.Column.FieldName = "LastDownloaded" Then
            e.Appearance.ForeColor = Color.Blue 'If(_mark, Color.LightGreen, Color.LightSalmon)
            e.Appearance.TextOptions.HAlignment = If(_mark, HorzAlignment.Center, HorzAlignment.Near)
        End If
    End Sub

    Private Sub BarButtonItem6_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem6.ItemClick
        Dim RowHandles() As Integer = GridView1.GetSelectedRows
        Dim IP, login, port, A_R, Purpose As Object
        Dim commkey As Integer
        Dim IN_OUT As String
        Dim cn As Common = New Common
        Dim failIP As New List(Of String)()
        For Each rowHandle As Integer In RowHandles
            IP = GridView1.GetRowCellValue(rowHandle, GridView1.Columns("LOCATION"))
            login = GridView1.GetRowCellValue(rowHandle, GridView1.Columns("HLogin"))
            port = GridView1.GetRowCellValue(rowHandle, GridView1.Columns("HPassword"))
            Dim liveVideo As XtraLiveVideo = New XtraLiveVideo(login, port, IP)
            liveVideo.Show()
        Next
    End Sub



    Private Sub GridControl1_Load(sender As System.Object, e As System.EventArgs) Handles GridControl1.Load
        For rowHandle As Integer = 0 To GridView1.RowCount - 1
            GridView1.SetRowCellValue(rowHandle, "Status", "")
        Next
    End Sub


#Region "SearchDevices"
    Private m_fnCallbackLink As SadpInterface.PDEVICE_FIND_CALLBACK_LINK = Nothing
    Private m_fnCallbackMulticast As SadpInterface.PDEVICE_FIND_CALLBACK_MULTICAST = Nothing
    Private m_lstDeviceInfo_Link As New List(Of SadpInterface.SADP_DEVICE_INFO_V40)()
    Private m_lstDeviceInfo_Multicast As New List(Of SadpInterface.SADP_DEVICE_INFO_V40)()
    Private m_lstDeviceInfo_All As New List(Of SadpInterface.SADP_DEVICE_INFO_V40)()
    Private m_struCurSelectedDevice_Multicast As New SadpInterface.SADP_DEVICE_INFO_V40()
    'private SadpInterface.SADP_DEVICE_INFO_V40 m_struCurSelectedDevice_Link = new SadpInterface.SADP_DEVICE_INFO_V40();
    Private m_bCompareStatus As Boolean = False
    Private m_csLock As New Object()
    Private Shared m_iIndex As Integer = 1

    Private Sub BarButtonItem7_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles BarButtonItem7.ItemClick

        GridControlSearch.DataSource = Nothing
        Dim dt As DataTable = New DataTable
        dt.Columns.Add("SerialNumber")
        dt.Columns.Add("IP")
        Dim datase As DataSet = New DataSet()
        datase.Tables.Add(dt)
        GridControlSearch.DataSource = dt

        SidePanelSearch.Visible = True
        SidePanelSearch.Height = SplitContainerControl1.Height * 30 / 100
        If m_fnCallbackLink Is Nothing Then
            m_fnCallbackLink = New SadpInterface.PDEVICE_FIND_CALLBACK_LINK(AddressOf CallbackLink)
        End If
        If m_fnCallbackMulticast Is Nothing Then
            m_fnCallbackMulticast = New SadpInterface.PDEVICE_FIND_CALLBACK_MULTICAST(AddressOf CallbackMulticast)
        End If
        SadpInterface.SADP_Start_V40(m_fnCallbackLink, m_fnCallbackMulticast, 0, New IntPtr())
    End Sub
    Public Sub CallbackLink(ByVal pData As IntPtr, ByVal pUserData As IntPtr)
        SyncLock m_csLock
            Debug.WriteLine("link")
            Dim tmp(2047) As Byte
            Dim struSadpDeviceInfoV40 As New SadpInterface.SADP_DEVICE_INFO_V40()
            Marshal.Copy(pData, tmp, 0, Marshal.SizeOf(struSadpDeviceInfoV40.GetType()))
            struSadpDeviceInfoV40 = DirectCast(BytesToStruct(tmp, struSadpDeviceInfoV40.GetType(), Marshal.SizeOf(struSadpDeviceInfoV40.GetType())), SadpInterface.SADP_DEVICE_INFO_V40)

            '更新到链路列表
            Dim isExist As Boolean = False
            Dim iIndex As Integer = -1
            For i As Integer = 0 To Me.m_lstDeviceInfo_Link.Count - 1
                If System.Text.Encoding.Default.GetString(Me.m_lstDeviceInfo_Link(i).struSadpDeviceInfo.szMAC).Equals(System.Text.Encoding.Default.GetString(struSadpDeviceInfoV40.struSadpDeviceInfo.szMAC)) Then
                    isExist = True
                    iIndex = i
                End If
            Next i

            If isExist Then
                Me.m_lstDeviceInfo_Link.RemoveAt(iIndex)
            End If
            Me.m_lstDeviceInfo_Link.Add(struSadpDeviceInfoV40)

            '更新到总链表
            isExist = False
            iIndex = -1
            For i As Integer = 0 To Me.m_lstDeviceInfo_All.Count - 1
                If System.Text.Encoding.Default.GetString(Me.m_lstDeviceInfo_All(i).struSadpDeviceInfo.szMAC).Equals(System.Text.Encoding.Default.GetString(struSadpDeviceInfoV40.struSadpDeviceInfo.szMAC)) Then
                    isExist = True
                    iIndex = i
                End If
            Next i

            If isExist Then
                Me.m_lstDeviceInfo_All.RemoveAt(iIndex)
            End If
            Me.m_lstDeviceInfo_All.Add(struSadpDeviceInfoV40)

        End SyncLock
        UpdateList()
    End Sub

    Public Sub CallbackMulticast(ByVal pData As IntPtr, ByVal pUserData As IntPtr)
        SyncLock m_csLock
            Debug.WriteLine("multicast")
            Dim tmp(2047) As Byte
            Dim struSadpDeviceInfoV40 As New SadpInterface.SADP_DEVICE_INFO_V40()
            Marshal.Copy(pData, tmp, 0, Marshal.SizeOf(struSadpDeviceInfoV40.GetType()))
            struSadpDeviceInfoV40 = DirectCast(BytesToStruct(tmp, struSadpDeviceInfoV40.GetType(), Marshal.SizeOf(struSadpDeviceInfoV40.GetType())), SadpInterface.SADP_DEVICE_INFO_V40)

            'Update MultiCast List
            Dim isExist As Boolean = False
            Dim iIndex As Integer = -1
            For i As Integer = 0 To Me.m_lstDeviceInfo_Multicast.Count - 1
                If System.Text.Encoding.Default.GetString(Me.m_lstDeviceInfo_Multicast(i).struSadpDeviceInfo.szMAC).Equals(System.Text.Encoding.Default.GetString(struSadpDeviceInfoV40.struSadpDeviceInfo.szMAC)) Then
                    isExist = True
                    iIndex = i
                End If
            Next i

            If isExist Then
                Me.m_lstDeviceInfo_Multicast.RemoveAt(iIndex)
            End If
            Me.m_lstDeviceInfo_Multicast.Add(struSadpDeviceInfoV40)

            isExist = False
            iIndex = -1
            For i As Integer = 0 To Me.m_lstDeviceInfo_All.Count - 1
                If System.Text.Encoding.Default.GetString(Me.m_lstDeviceInfo_All(i).struSadpDeviceInfo.szMAC).Equals(System.Text.Encoding.Default.GetString(struSadpDeviceInfoV40.struSadpDeviceInfo.szMAC)) Then
                    isExist = True
                    iIndex = i
                End If
            Next i

            If isExist Then
                Me.m_lstDeviceInfo_All.RemoveAt(iIndex)
            End If
            Me.m_lstDeviceInfo_All.Add(struSadpDeviceInfoV40)

        End SyncLock
        UpdateList()
    End Sub

    Private Sub UpdateList()
        If InvokeRequired Then
            Invoke(New MethodInvoker(AddressOf UpdateList))
            Return
        Else
            Dim SearchSerialArr() As String = SearchSerial.Distinct.ToArray
            SyncLock m_csLock
                'this.listView1.Items.Clear();
                For Each struDeviceInfo As SadpInterface.SADP_DEVICE_INFO_V40 In Me.m_lstDeviceInfo_All
                    If Not SearchSerialArr.Contains(System.Text.Encoding.Default.GetString(struDeviceInfo.struSadpDeviceInfo.szSerialNO).Trim(vbNullChar)) Then
                        GridViewSearch.AddNewRow()
                        Dim rowHandle As Integer = GridViewSearch.GetRowHandle(GridViewSearch.DataRowCount)
                    If GridViewSearch.IsNewItemRow(rowHandle) Then
                        SearchSerial.Add(System.Text.Encoding.Default.GetString(struDeviceInfo.struSadpDeviceInfo.szSerialNO).Trim(vbNullChar))
                            GridViewSearch.SetRowCellValue(rowHandle, GridViewSearch.Columns(0), System.Text.Encoding.Default.GetString(struDeviceInfo.struSadpDeviceInfo.szSerialNO).Trim(vbNullChar))
                            GridViewSearch.SetRowCellValue(rowHandle, GridViewSearch.Columns(1), System.Text.Encoding.Default.GetString(struDeviceInfo.struSadpDeviceInfo.szIPv4Address).Trim(vbNullChar))
                        End If
                    End If
                    'Dim bIsExist As Boolean = False
                    'For Each lvi As ListViewItem In Me.listView1.Items
                    '    If lvi.SubItems(2).Text.Equals(System.Text.Encoding.Default.GetString(struDeviceInfo.struSadpDeviceInfo.szMAC)) Then
                    '        bIsExist = True
                    '    End If
                    'Next lvi
                    'If Not bIsExist Then
                    '    Dim row As New ListViewItem(m_iIndex.ToString())
                    '    Dim item() As String = {System.Text.Encoding.Default.GetString(struDeviceInfo.struSadpDeviceInfo.szIPv4Address), System.Text.Encoding.Default.GetString(struDeviceInfo.struSadpDeviceInfo.szMAC), System.Text.Encoding.Default.GetString(struDeviceInfo.struSadpDeviceInfo.szSerialNO), struDeviceInfo.struSadpDeviceInfo.dwPort.ToString(), System.Text.Encoding.Default.GetString(struDeviceInfo.struSadpDeviceInfo.szDevDesc)}
                    '    row.SubItems.AddRange(item)
                    '    'Me.listView1.Items.Add(row)
                    '    m_iIndex += 1
                    '    If m_iIndex = 2 Then
                    '        row.Selected = True
                    '    End If
                    'End If
                Next struDeviceInfo
            End SyncLock
        End If
    End Sub

    ''' <summary>
    ''' 逐个字节进行比较
    ''' </summary>
    ''' <param name="b1"></param>
    ''' <param name="b2"></param>
    ''' <returns></returns>
    Private Function BytesCompare_Step(ByVal b1() As Byte, ByVal b2() As Byte) As Boolean
        If b1 Is Nothing OrElse b2 Is Nothing Then
            Return False
        End If
        If b1.Length <> b2.Length Then
            Return False
        End If
        For i As Integer = 0 To b1.Length - 1
            If b1(i) <> b2(i) Then
                Return False
            End If
        Next i
        Return True
    End Function

    Private Sub GridViewSearch_EditFormShowing(sender As Object, e As EditFormShowingEventArgs) Handles GridViewSearch.EditFormShowing
        If GridView1.IsNewItemRow(GridView1.FocusedRowHandle) = True Then
            If Common.DeviceAdd <> "Y" Then
                e.Allow = False
                Exit Sub
            End If
        Else
            If Common.DeviceModi <> "Y" Then
                e.Allow = False
                Exit Sub
            End If
        End If

        Dim row As System.Data.DataRow = GridViewSearch.GetDataRow(GridViewSearch.FocusedRowHandle)
        Try
            DeviceID = row("IP").ToString.Trim & "," & row("SerialNumber").ToString.Trim
        Catch ex As Exception
            DeviceID = ""
        End Try
        e.Allow = False

        XtraDeviceEdit.ShowDialog()
        Common.loadDevice()
        GridControl1.DataSource = Common.MachineNonAdmin
        For rowHandle As Integer = 0 To GridView1.RowCount - 1
            GridView1.SetRowCellValue(rowHandle, "Status", "")
        Next
    End Sub

    Public Shared Function BytesToStruct(ByVal bytes() As Byte, ByVal strcutType As Type, ByVal nSize As Integer) As Object
        If bytes Is Nothing Then
            Debug.WriteLine("null bytes!!!!!!!!!!!!!")
        End If
        'INSTANT VB NOTE: The variable size was renamed since Visual Basic does not handle local variables named the same as class members well:
        Dim size_Conflict As Integer = Marshal.SizeOf(strcutType)
        Dim buffer As IntPtr = Marshal.AllocHGlobal(nSize)
        'Debug.LogError("Type: " + strcutType.ToString() + "---TypeSize:" + size + "----packetSize:" + nSize);
        Try
            Marshal.Copy(bytes, 0, buffer, nSize)
            Return Marshal.PtrToStructure(buffer, strcutType)
        Catch ex As Exception
            Debug.WriteLine("Type: " & strcutType.ToString() & "---TypeSize:" & size_Conflict & "----packetSize:" & nSize)
            Return Nothing
        Finally
            Marshal.FreeHGlobal(buffer)
        End Try
    End Function

#End Region
End Class
Public Class CUserInfoCount
    Public Property UserInfoCount As CUserInfoCountContent
End Class
Public Class CUserInfoCountContent
    Public Property userNumber As Integer
End Class
Public Class CFaceRecordCount
    Public Property requestURL As String
    Public Property statusCode As Integer
    Public Property statusString As String
    Public Property subStatusCode As String
    Public Property errorCode As Integer
    Public Property errorMsg As String
    Public Property FDID As String
    Public Property faceLibType As String
    Public Property recordDataNumber As Integer
End Class
Public Class CCardInfoCount
    Public Property CardInfoCount() As CCardInfoCountContent
End Class
Public Class CCardInfoCountContent
    Public Property cardNumber() As Integer
End Class


