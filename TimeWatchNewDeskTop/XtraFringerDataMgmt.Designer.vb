﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraFringerDataMgmt
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim GridLevelNode1 As DevExpress.XtraGrid.GridLevelNode = New DevExpress.XtraGrid.GridLevelNode()
        Dim GridLevelNode2 As DevExpress.XtraGrid.GridLevelNode = New DevExpress.XtraGrid.GridLevelNode()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(XtraFringerDataMgmt))
        Me.SSSDBDataSet = New iAS.SSSDBDataSet()
        Me.TblMachineBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.TblMachineTableAdapter = New iAS.SSSDBDataSetTableAdapters.tblMachineTableAdapter()
        Me.TblMachine1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.tblMachine1TableAdapter()
        Me.GroupControl1 = New DevExpress.XtraEditors.GroupControl()
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colID_NO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLOCATION = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colbranch = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDeviceType = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colA_R = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colcommkey = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GroupControl2 = New DevExpress.XtraEditors.GroupControl()
        Me.PanelControl2 = New DevExpress.XtraEditors.PanelControl()
        Me.GridControl2 = New DevExpress.XtraGrid.GridControl()
        Me.FptableBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GridView2 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colEMachineNumber = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colEnrollNumber = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colUserName = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colFingerNumber = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPrivilege = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colPassword = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colTemplate = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCardnumber = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colcalid = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colauthority = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colcheck_type = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colopendoor_type = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colface_Data1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colface_data2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colface_data3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colface_data4 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colface_data5 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colface_data6 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colface_data7 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colface_data8 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colface_data9 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colface_data10 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colface_data11 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colface_data12 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colface_data13 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colface_data14 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colface_data15 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colface_data16 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colface_data17 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colface_data18 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colTemplate_Face = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colTemplate_Tw = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colVerifyMode = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colUltra800FId = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.btnSearch = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.PopupContainerEditDept = New DevExpress.XtraEditors.PopupContainerEdit()
        Me.PopupContainerControlDept = New DevExpress.XtraEditors.PopupContainerControl()
        Me.GridControlDept = New DevExpress.XtraGrid.GridControl()
        Me.GridViewDept = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colDEPARTMENTCODE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDEPARTMENTNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTimeEdit3 = New DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit()
        Me.PopupContainerEditLocation = New DevExpress.XtraEditors.PopupContainerEdit()
        Me.PopupContainerControlBranch = New DevExpress.XtraEditors.PopupContainerControl()
        Me.GridControlBranch = New DevExpress.XtraGrid.GridControl()
        Me.GridViewBranch = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colBRANCHCODE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBRANCHNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTimeEdit7 = New DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit()
        Me.RepositoryItemDateEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit()
        Me.PopupContainerControlShift = New DevExpress.XtraEditors.PopupContainerControl()
        Me.GridControlShift = New DevExpress.XtraGrid.GridControl()
        Me.GridViewShift = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colSHIFT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSTARTTIME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemDateEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit()
        Me.colENDTIME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTimeEdit5 = New DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit()
        Me.FptableTableAdapter = New iAS.SSSDBDataSetTableAdapters.fptableTableAdapter()
        Me.Fptable1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.fptable1TableAdapter()
        Me.btnDownloadFinger = New DevExpress.XtraEditors.SimpleButton()
        Me.ToggleDownloadAll = New DevExpress.XtraEditors.ToggleSwitch()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.TextSelectUser = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.btnUploadFinger = New DevExpress.XtraEditors.SimpleButton()
        Me.btnDeleteFrmDB = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.ToggleMakeAdmin = New DevExpress.XtraEditors.ToggleSwitch()
        Me.btnDeleteFrmDevice = New DevExpress.XtraEditors.SimpleButton()
        Me.btnClearAdmin = New DevExpress.XtraEditors.SimpleButton()
        Me.GroupControl3 = New DevExpress.XtraEditors.GroupControl()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.ToggleCreateEmp = New DevExpress.XtraEditors.ToggleSwitch()
        Me.GroupControl4 = New DevExpress.XtraEditors.GroupControl()
        Me.GroupControl5 = New DevExpress.XtraEditors.GroupControl()
        Me.SimpleButtonRestart = New DevExpress.XtraEditors.SimpleButton()
        Me.BtnDisable = New DevExpress.XtraEditors.SimpleButton()
        Me.BtnEnable = New DevExpress.XtraEditors.SimpleButton()
        Me.btnLockOpen = New DevExpress.XtraEditors.SimpleButton()
        Me.TextPassword = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.btnClearDeviceData = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.TblDepartment1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.tblDepartment1TableAdapter()
        Me.TblDepartmentTableAdapter = New iAS.SSSDBDataSetTableAdapters.tblDepartmentTableAdapter()
        Me.TblDepartmentBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.AxFP_CLOCK1 = New AxFP_CLOCKLib.AxFP_CLOCK()
        Me.colMAC_ADDRESS = New DevExpress.XtraGrid.Columns.GridColumn()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblMachineBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl1.SuspendLayout()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl2.SuspendLayout()
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl2.SuspendLayout()
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FptableBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.PopupContainerEditDept.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerControlDept, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PopupContainerControlDept.SuspendLayout()
        CType(Me.GridControlDept, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewDept, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTimeEdit3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerEditLocation.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerControlBranch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PopupContainerControlBranch.SuspendLayout()
        CType(Me.GridControlBranch, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewBranch, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTimeEdit7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit2.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerControlShift, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PopupContainerControlShift.SuspendLayout()
        CType(Me.GridControlShift, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewShift, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit1.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTimeEdit5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleDownloadAll.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextSelectUser.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToggleMakeAdmin.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl3.SuspendLayout()
        CType(Me.ToggleCreateEmp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl4.SuspendLayout()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupControl5.SuspendLayout()
        CType(Me.TextPassword.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblDepartmentBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AxFP_CLOCK1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SSSDBDataSet
        '
        Me.SSSDBDataSet.DataSetName = "SSSDBDataSet"
        Me.SSSDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'TblMachineBindingSource
        '
        Me.TblMachineBindingSource.DataMember = "tblMachine"
        Me.TblMachineBindingSource.DataSource = Me.SSSDBDataSet
        '
        'TblMachineTableAdapter
        '
        Me.TblMachineTableAdapter.ClearBeforeFill = True
        '
        'TblMachine1TableAdapter1
        '
        Me.TblMachine1TableAdapter1.ClearBeforeFill = True
        '
        'GroupControl1
        '
        Me.GroupControl1.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl1.AppearanceCaption.Options.UseFont = True
        Me.GroupControl1.Controls.Add(Me.GridControl1)
        Me.GroupControl1.Location = New System.Drawing.Point(12, 12)
        Me.GroupControl1.Name = "GroupControl1"
        Me.GroupControl1.Size = New System.Drawing.Size(611, 250)
        Me.GroupControl1.TabIndex = 1
        Me.GroupControl1.Text = "Device List"
        '
        'GridControl1
        '
        Me.GridControl1.DataSource = Me.TblMachineBindingSource
        Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControl1.EmbeddedNavigator.Buttons.Append.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.Edit.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.Remove.Visible = False
        GridLevelNode1.RelationName = "Level1"
        Me.GridControl1.LevelTree.Nodes.AddRange(New DevExpress.XtraGrid.GridLevelNode() {GridLevelNode1})
        Me.GridControl1.Location = New System.Drawing.Point(2, 23)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.Size = New System.Drawing.Size(607, 225)
        Me.GridControl1.TabIndex = 1
        Me.GridControl1.UseEmbeddedNavigator = True
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colID_NO, Me.colLOCATION, Me.colbranch, Me.colDeviceType, Me.colA_R, Me.colcommkey, Me.colMAC_ADDRESS})
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView1.OptionsBehavior.Editable = False
        Me.GridView1.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridView1.OptionsSelection.MultiSelect = True
        Me.GridView1.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        Me.GridView1.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colID_NO, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colID_NO
        '
        Me.colID_NO.Caption = "Controller Id"
        Me.colID_NO.FieldName = "ID_NO"
        Me.colID_NO.Name = "colID_NO"
        Me.colID_NO.Visible = True
        Me.colID_NO.VisibleIndex = 1
        '
        'colLOCATION
        '
        Me.colLOCATION.Caption = "Device IP"
        Me.colLOCATION.FieldName = "LOCATION"
        Me.colLOCATION.Name = "colLOCATION"
        Me.colLOCATION.Visible = True
        Me.colLOCATION.VisibleIndex = 2
        '
        'colbranch
        '
        Me.colbranch.Caption = "Location"
        Me.colbranch.FieldName = "branch"
        Me.colbranch.Name = "colbranch"
        Me.colbranch.Visible = True
        Me.colbranch.VisibleIndex = 3
        '
        'colDeviceType
        '
        Me.colDeviceType.Caption = "Device Type"
        Me.colDeviceType.FieldName = "DeviceType"
        Me.colDeviceType.Name = "colDeviceType"
        Me.colDeviceType.Visible = True
        Me.colDeviceType.VisibleIndex = 4
        '
        'colA_R
        '
        Me.colA_R.FieldName = "A_R"
        Me.colA_R.Name = "colA_R"
        '
        'colcommkey
        '
        Me.colcommkey.Caption = "GridColumn2"
        Me.colcommkey.FieldName = "commkey"
        Me.colcommkey.Name = "colcommkey"
        '
        'GroupControl2
        '
        Me.GroupControl2.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl2.AppearanceCaption.Options.UseFont = True
        Me.GroupControl2.Controls.Add(Me.PanelControl2)
        Me.GroupControl2.Controls.Add(Me.PanelControl1)
        Me.GroupControl2.Location = New System.Drawing.Point(12, 268)
        Me.GroupControl2.Name = "GroupControl2"
        Me.GroupControl2.Size = New System.Drawing.Size(611, 277)
        Me.GroupControl2.TabIndex = 2
        Me.GroupControl2.Text = "Fringer Print Details"
        '
        'PanelControl2
        '
        Me.PanelControl2.Controls.Add(Me.GridControl2)
        Me.PanelControl2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PanelControl2.Location = New System.Drawing.Point(2, 67)
        Me.PanelControl2.Name = "PanelControl2"
        Me.PanelControl2.Size = New System.Drawing.Size(607, 208)
        Me.PanelControl2.TabIndex = 3
        '
        'GridControl2
        '
        Me.GridControl2.DataSource = Me.FptableBindingSource
        Me.GridControl2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControl2.EmbeddedNavigator.Buttons.Append.Visible = False
        Me.GridControl2.EmbeddedNavigator.Buttons.CancelEdit.Visible = False
        Me.GridControl2.EmbeddedNavigator.Buttons.Edit.Visible = False
        Me.GridControl2.EmbeddedNavigator.Buttons.EndEdit.Visible = False
        Me.GridControl2.EmbeddedNavigator.Buttons.Remove.Visible = False
        GridLevelNode2.RelationName = "Level1"
        Me.GridControl2.LevelTree.Nodes.AddRange(New DevExpress.XtraGrid.GridLevelNode() {GridLevelNode2})
        Me.GridControl2.Location = New System.Drawing.Point(2, 2)
        Me.GridControl2.MainView = Me.GridView2
        Me.GridControl2.Name = "GridControl2"
        Me.GridControl2.Size = New System.Drawing.Size(603, 204)
        Me.GridControl2.TabIndex = 3
        Me.GridControl2.UseEmbeddedNavigator = True
        Me.GridControl2.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView2})
        '
        'FptableBindingSource
        '
        Me.FptableBindingSource.DataMember = "fptable"
        Me.FptableBindingSource.DataSource = Me.SSSDBDataSet
        '
        'GridView2
        '
        Me.GridView2.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colEMachineNumber, Me.colEnrollNumber, Me.colUserName, Me.colFingerNumber, Me.GridColumn1, Me.colPrivilege, Me.colPassword, Me.colTemplate, Me.colCardnumber, Me.colcalid, Me.colauthority, Me.colcheck_type, Me.colopendoor_type, Me.colface_Data1, Me.colface_data2, Me.colface_data3, Me.colface_data4, Me.colface_data5, Me.colface_data6, Me.colface_data7, Me.colface_data8, Me.colface_data9, Me.colface_data10, Me.colface_data11, Me.colface_data12, Me.colface_data13, Me.colface_data14, Me.colface_data15, Me.colface_data16, Me.colface_data17, Me.colface_data18, Me.colTemplate_Face, Me.colTemplate_Tw, Me.colVerifyMode, Me.colUltra800FId})
        Me.GridView2.GridControl = Me.GridControl2
        Me.GridView2.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always
        Me.GridView2.Name = "GridView2"
        Me.GridView2.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView2.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridView2.OptionsBehavior.Editable = False
        Me.GridView2.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridView2.OptionsSelection.MultiSelect = True
        Me.GridView2.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        Me.GridView2.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colEnrollNumber, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colEMachineNumber
        '
        Me.colEMachineNumber.Caption = "Controller Id"
        Me.colEMachineNumber.FieldName = "EMachineNumber"
        Me.colEMachineNumber.Name = "colEMachineNumber"
        Me.colEMachineNumber.Visible = True
        Me.colEMachineNumber.VisibleIndex = 1
        '
        'colEnrollNumber
        '
        Me.colEnrollNumber.FieldName = "EnrollNumber"
        Me.colEnrollNumber.Name = "colEnrollNumber"
        Me.colEnrollNumber.Visible = True
        Me.colEnrollNumber.VisibleIndex = 2
        '
        'colUserName
        '
        Me.colUserName.FieldName = "UserName"
        Me.colUserName.Name = "colUserName"
        Me.colUserName.Visible = True
        Me.colUserName.VisibleIndex = 3
        '
        'colFingerNumber
        '
        Me.colFingerNumber.FieldName = "FingerNumber"
        Me.colFingerNumber.Name = "colFingerNumber"
        Me.colFingerNumber.Visible = True
        Me.colFingerNumber.VisibleIndex = 4
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Device Type"
        Me.GridColumn1.FieldName = "DeviceType"
        Me.GridColumn1.FilterMode = DevExpress.XtraGrid.ColumnFilterMode.DisplayText
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.[True]
        Me.GridColumn1.OptionsFilter.FilterBySortField = DevExpress.Utils.DefaultBoolean.[True]
        Me.GridColumn1.SortMode = DevExpress.XtraGrid.ColumnSortMode.DisplayText
        Me.GridColumn1.UnboundType = DevExpress.Data.UnboundColumnType.[String]
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 5
        '
        'colPrivilege
        '
        Me.colPrivilege.FieldName = "Privilege"
        Me.colPrivilege.Name = "colPrivilege"
        '
        'colPassword
        '
        Me.colPassword.FieldName = "Password"
        Me.colPassword.Name = "colPassword"
        '
        'colTemplate
        '
        Me.colTemplate.FieldName = "Template"
        Me.colTemplate.Name = "colTemplate"
        '
        'colCardnumber
        '
        Me.colCardnumber.FieldName = "Cardnumber"
        Me.colCardnumber.Name = "colCardnumber"
        '
        'colcalid
        '
        Me.colcalid.FieldName = "calid"
        Me.colcalid.Name = "colcalid"
        '
        'colauthority
        '
        Me.colauthority.FieldName = "authority"
        Me.colauthority.Name = "colauthority"
        '
        'colcheck_type
        '
        Me.colcheck_type.FieldName = "check_type"
        Me.colcheck_type.Name = "colcheck_type"
        '
        'colopendoor_type
        '
        Me.colopendoor_type.FieldName = "opendoor_type"
        Me.colopendoor_type.Name = "colopendoor_type"
        '
        'colface_Data1
        '
        Me.colface_Data1.FieldName = "face_Data1"
        Me.colface_Data1.Name = "colface_Data1"
        '
        'colface_data2
        '
        Me.colface_data2.FieldName = "face_data2"
        Me.colface_data2.Name = "colface_data2"
        '
        'colface_data3
        '
        Me.colface_data3.FieldName = "face_data3"
        Me.colface_data3.Name = "colface_data3"
        '
        'colface_data4
        '
        Me.colface_data4.FieldName = "face_data4"
        Me.colface_data4.Name = "colface_data4"
        '
        'colface_data5
        '
        Me.colface_data5.FieldName = "face_data5"
        Me.colface_data5.Name = "colface_data5"
        '
        'colface_data6
        '
        Me.colface_data6.FieldName = "face_data6"
        Me.colface_data6.Name = "colface_data6"
        '
        'colface_data7
        '
        Me.colface_data7.FieldName = "face_data7"
        Me.colface_data7.Name = "colface_data7"
        '
        'colface_data8
        '
        Me.colface_data8.FieldName = "face_data8"
        Me.colface_data8.Name = "colface_data8"
        '
        'colface_data9
        '
        Me.colface_data9.FieldName = "face_data9"
        Me.colface_data9.Name = "colface_data9"
        '
        'colface_data10
        '
        Me.colface_data10.FieldName = "face_data10"
        Me.colface_data10.Name = "colface_data10"
        '
        'colface_data11
        '
        Me.colface_data11.FieldName = "face_data11"
        Me.colface_data11.Name = "colface_data11"
        '
        'colface_data12
        '
        Me.colface_data12.FieldName = "face_data12"
        Me.colface_data12.Name = "colface_data12"
        '
        'colface_data13
        '
        Me.colface_data13.FieldName = "face_data13"
        Me.colface_data13.Name = "colface_data13"
        '
        'colface_data14
        '
        Me.colface_data14.FieldName = "face_data14"
        Me.colface_data14.Name = "colface_data14"
        '
        'colface_data15
        '
        Me.colface_data15.FieldName = "face_data15"
        Me.colface_data15.Name = "colface_data15"
        '
        'colface_data16
        '
        Me.colface_data16.FieldName = "face_data16"
        Me.colface_data16.Name = "colface_data16"
        '
        'colface_data17
        '
        Me.colface_data17.FieldName = "face_data17"
        Me.colface_data17.Name = "colface_data17"
        '
        'colface_data18
        '
        Me.colface_data18.FieldName = "face_data18"
        Me.colface_data18.Name = "colface_data18"
        '
        'colTemplate_Face
        '
        Me.colTemplate_Face.FieldName = "Template_Face"
        Me.colTemplate_Face.Name = "colTemplate_Face"
        '
        'colTemplate_Tw
        '
        Me.colTemplate_Tw.FieldName = "Template_Tw"
        Me.colTemplate_Tw.Name = "colTemplate_Tw"
        '
        'colVerifyMode
        '
        Me.colVerifyMode.FieldName = "VerifyMode"
        Me.colVerifyMode.Name = "colVerifyMode"
        '
        'colUltra800FId
        '
        Me.colUltra800FId.Caption = "Ultra800FId"
        Me.colUltra800FId.FieldName = "Ultra800FId"
        Me.colUltra800FId.Name = "colUltra800FId"
        '
        'PanelControl1
        '
        Me.PanelControl1.Controls.Add(Me.btnSearch)
        Me.PanelControl1.Controls.Add(Me.LabelControl7)
        Me.PanelControl1.Controls.Add(Me.LabelControl6)
        Me.PanelControl1.Controls.Add(Me.PopupContainerEditDept)
        Me.PanelControl1.Controls.Add(Me.PopupContainerEditLocation)
        Me.PanelControl1.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanelControl1.Location = New System.Drawing.Point(2, 23)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(607, 44)
        Me.PanelControl1.TabIndex = 2
        '
        'btnSearch
        '
        Me.btnSearch.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.btnSearch.Appearance.Options.UseFont = True
        Me.btnSearch.Location = New System.Drawing.Point(527, 11)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(68, 23)
        Me.btnSearch.TabIndex = 3
        Me.btnSearch.Text = "Search"
        '
        'LabelControl7
        '
        Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.LabelControl7.Appearance.Options.UseFont = True
        Me.LabelControl7.Location = New System.Drawing.Point(5, 14)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(47, 16)
        Me.LabelControl7.TabIndex = 28
        Me.LabelControl7.Text = "Location"
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.LabelControl6.Appearance.Options.UseFont = True
        Me.LabelControl6.Location = New System.Drawing.Point(261, 14)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(67, 16)
        Me.LabelControl6.TabIndex = 27
        Me.LabelControl6.Text = "Department"
        '
        'PopupContainerEditDept
        '
        Me.PopupContainerEditDept.Location = New System.Drawing.Point(348, 12)
        Me.PopupContainerEditDept.Name = "PopupContainerEditDept"
        Me.PopupContainerEditDept.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditDept.Properties.Appearance.Options.UseFont = True
        Me.PopupContainerEditDept.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditDept.Properties.AppearanceDropDown.Options.UseFont = True
        Me.PopupContainerEditDept.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditDept.Properties.AppearanceFocused.Options.UseFont = True
        Me.PopupContainerEditDept.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.PopupContainerEditDept.Properties.PopupControl = Me.PopupContainerControlDept
        Me.PopupContainerEditDept.Size = New System.Drawing.Size(150, 20)
        Me.PopupContainerEditDept.TabIndex = 2
        Me.PopupContainerEditDept.ToolTip = "Leave blank if want for all Departments"
        '
        'PopupContainerControlDept
        '
        Me.PopupContainerControlDept.Controls.Add(Me.GridControlDept)
        Me.PopupContainerControlDept.Location = New System.Drawing.Point(196, 38)
        Me.PopupContainerControlDept.Name = "PopupContainerControlDept"
        Me.PopupContainerControlDept.Size = New System.Drawing.Size(300, 300)
        Me.PopupContainerControlDept.TabIndex = 27
        '
        'GridControlDept
        '
        Me.GridControlDept.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControlDept.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridControlDept.Location = New System.Drawing.Point(0, 0)
        Me.GridControlDept.MainView = Me.GridViewDept
        Me.GridControlDept.Name = "GridControlDept"
        Me.GridControlDept.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTimeEdit3})
        Me.GridControlDept.Size = New System.Drawing.Size(300, 300)
        Me.GridControlDept.TabIndex = 6
        Me.GridControlDept.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewDept})
        '
        'GridViewDept
        '
        Me.GridViewDept.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colDEPARTMENTCODE, Me.colDEPARTMENTNAME})
        Me.GridViewDept.GridControl = Me.GridControlDept
        Me.GridViewDept.Name = "GridViewDept"
        Me.GridViewDept.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewDept.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewDept.OptionsBehavior.Editable = False
        Me.GridViewDept.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridViewDept.OptionsSelection.MultiSelect = True
        Me.GridViewDept.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        Me.GridViewDept.OptionsView.ColumnAutoWidth = False
        Me.GridViewDept.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colDEPARTMENTCODE, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colDEPARTMENTCODE
        '
        Me.colDEPARTMENTCODE.Caption = "Depatment Code"
        Me.colDEPARTMENTCODE.FieldName = "DEPARTMENTCODE"
        Me.colDEPARTMENTCODE.Name = "colDEPARTMENTCODE"
        Me.colDEPARTMENTCODE.Visible = True
        Me.colDEPARTMENTCODE.VisibleIndex = 1
        Me.colDEPARTMENTCODE.Width = 100
        '
        'colDEPARTMENTNAME
        '
        Me.colDEPARTMENTNAME.Caption = "Name"
        Me.colDEPARTMENTNAME.FieldName = "DEPARTMENTNAME"
        Me.colDEPARTMENTNAME.Name = "colDEPARTMENTNAME"
        Me.colDEPARTMENTNAME.Visible = True
        Me.colDEPARTMENTNAME.VisibleIndex = 2
        Me.colDEPARTMENTNAME.Width = 120
        '
        'RepositoryItemTimeEdit3
        '
        Me.RepositoryItemTimeEdit3.AutoHeight = False
        Me.RepositoryItemTimeEdit3.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemTimeEdit3.Mask.EditMask = "HH:mm"
        Me.RepositoryItemTimeEdit3.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemTimeEdit3.Name = "RepositoryItemTimeEdit3"
        '
        'PopupContainerEditLocation
        '
        Me.PopupContainerEditLocation.EditValue = ""
        Me.PopupContainerEditLocation.Location = New System.Drawing.Point(63, 12)
        Me.PopupContainerEditLocation.Name = "PopupContainerEditLocation"
        Me.PopupContainerEditLocation.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditLocation.Properties.Appearance.Options.UseFont = True
        Me.PopupContainerEditLocation.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditLocation.Properties.AppearanceDropDown.Options.UseFont = True
        Me.PopupContainerEditLocation.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditLocation.Properties.AppearanceFocused.Options.UseFont = True
        Me.PopupContainerEditLocation.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.PopupContainerEditLocation.Properties.PopupControl = Me.PopupContainerControlBranch
        Me.PopupContainerEditLocation.Size = New System.Drawing.Size(150, 20)
        Me.PopupContainerEditLocation.TabIndex = 1
        Me.PopupContainerEditLocation.ToolTip = "Leave blank if want for all Locations"
        '
        'PopupContainerControlBranch
        '
        Me.PopupContainerControlBranch.Controls.Add(Me.GridControlBranch)
        Me.PopupContainerControlBranch.Controls.Add(Me.PopupContainerControlShift)
        Me.PopupContainerControlBranch.Location = New System.Drawing.Point(629, 490)
        Me.PopupContainerControlBranch.Name = "PopupContainerControlBranch"
        Me.PopupContainerControlBranch.Size = New System.Drawing.Size(300, 300)
        Me.PopupContainerControlBranch.TabIndex = 26
        '
        'GridControlBranch
        '
        Me.GridControlBranch.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControlBranch.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridControlBranch.Location = New System.Drawing.Point(0, 0)
        Me.GridControlBranch.MainView = Me.GridViewBranch
        Me.GridControlBranch.Name = "GridControlBranch"
        Me.GridControlBranch.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTimeEdit7, Me.RepositoryItemDateEdit2})
        Me.GridControlBranch.Size = New System.Drawing.Size(300, 300)
        Me.GridControlBranch.TabIndex = 6
        Me.GridControlBranch.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewBranch})
        '
        'GridViewBranch
        '
        Me.GridViewBranch.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colBRANCHCODE, Me.colBRANCHNAME})
        Me.GridViewBranch.GridControl = Me.GridControlBranch
        Me.GridViewBranch.Name = "GridViewBranch"
        Me.GridViewBranch.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewBranch.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewBranch.OptionsBehavior.Editable = False
        Me.GridViewBranch.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridViewBranch.OptionsSelection.MultiSelect = True
        Me.GridViewBranch.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        Me.GridViewBranch.OptionsView.ColumnAutoWidth = False
        Me.GridViewBranch.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colBRANCHCODE, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colBRANCHCODE
        '
        Me.colBRANCHCODE.Caption = "Location Code"
        Me.colBRANCHCODE.FieldName = "BRANCHCODE"
        Me.colBRANCHCODE.Name = "colBRANCHCODE"
        Me.colBRANCHCODE.Visible = True
        Me.colBRANCHCODE.VisibleIndex = 1
        Me.colBRANCHCODE.Width = 100
        '
        'colBRANCHNAME
        '
        Me.colBRANCHNAME.Caption = "Name"
        Me.colBRANCHNAME.FieldName = "BRANCHNAME"
        Me.colBRANCHNAME.Name = "colBRANCHNAME"
        Me.colBRANCHNAME.Visible = True
        Me.colBRANCHNAME.VisibleIndex = 2
        Me.colBRANCHNAME.Width = 120
        '
        'RepositoryItemTimeEdit7
        '
        Me.RepositoryItemTimeEdit7.AutoHeight = False
        Me.RepositoryItemTimeEdit7.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemTimeEdit7.Mask.EditMask = "HH:mm"
        Me.RepositoryItemTimeEdit7.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemTimeEdit7.Name = "RepositoryItemTimeEdit7"
        '
        'RepositoryItemDateEdit2
        '
        Me.RepositoryItemDateEdit2.AutoHeight = False
        Me.RepositoryItemDateEdit2.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit2.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit2.Mask.EditMask = "HH:mm"
        Me.RepositoryItemDateEdit2.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemDateEdit2.Name = "RepositoryItemDateEdit2"
        '
        'PopupContainerControlShift
        '
        Me.PopupContainerControlShift.Controls.Add(Me.GridControlShift)
        Me.PopupContainerControlShift.Location = New System.Drawing.Point(99, 123)
        Me.PopupContainerControlShift.Name = "PopupContainerControlShift"
        Me.PopupContainerControlShift.Size = New System.Drawing.Size(300, 300)
        Me.PopupContainerControlShift.TabIndex = 16
        '
        'GridControlShift
        '
        Me.GridControlShift.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControlShift.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridControlShift.Location = New System.Drawing.Point(0, 0)
        Me.GridControlShift.MainView = Me.GridViewShift
        Me.GridControlShift.Name = "GridControlShift"
        Me.GridControlShift.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTimeEdit5, Me.RepositoryItemDateEdit1})
        Me.GridControlShift.Size = New System.Drawing.Size(300, 300)
        Me.GridControlShift.TabIndex = 6
        Me.GridControlShift.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewShift})
        '
        'GridViewShift
        '
        Me.GridViewShift.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colSHIFT, Me.colSTARTTIME, Me.colENDTIME})
        Me.GridViewShift.GridControl = Me.GridControlShift
        Me.GridViewShift.Name = "GridViewShift"
        Me.GridViewShift.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewShift.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewShift.OptionsBehavior.Editable = False
        Me.GridViewShift.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridViewShift.OptionsSelection.MultiSelect = True
        Me.GridViewShift.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        Me.GridViewShift.OptionsView.ColumnAutoWidth = False
        Me.GridViewShift.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colSHIFT, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colSHIFT
        '
        Me.colSHIFT.Caption = "Shift"
        Me.colSHIFT.FieldName = "SHIFT"
        Me.colSHIFT.Name = "colSHIFT"
        Me.colSHIFT.Visible = True
        Me.colSHIFT.VisibleIndex = 1
        '
        'colSTARTTIME
        '
        Me.colSTARTTIME.Caption = "Start Time"
        Me.colSTARTTIME.ColumnEdit = Me.RepositoryItemDateEdit1
        Me.colSTARTTIME.FieldName = "STARTTIME"
        Me.colSTARTTIME.Name = "colSTARTTIME"
        Me.colSTARTTIME.Visible = True
        Me.colSTARTTIME.VisibleIndex = 2
        '
        'RepositoryItemDateEdit1
        '
        Me.RepositoryItemDateEdit1.AutoHeight = False
        Me.RepositoryItemDateEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit1.Mask.EditMask = "HH:mm"
        Me.RepositoryItemDateEdit1.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemDateEdit1.Name = "RepositoryItemDateEdit1"
        '
        'colENDTIME
        '
        Me.colENDTIME.Caption = "End Time"
        Me.colENDTIME.ColumnEdit = Me.RepositoryItemDateEdit1
        Me.colENDTIME.FieldName = "ENDTIME"
        Me.colENDTIME.Name = "colENDTIME"
        Me.colENDTIME.Visible = True
        Me.colENDTIME.VisibleIndex = 3
        '
        'RepositoryItemTimeEdit5
        '
        Me.RepositoryItemTimeEdit5.AutoHeight = False
        Me.RepositoryItemTimeEdit5.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemTimeEdit5.Mask.EditMask = "HH:mm"
        Me.RepositoryItemTimeEdit5.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemTimeEdit5.Name = "RepositoryItemTimeEdit5"
        '
        'FptableTableAdapter
        '
        Me.FptableTableAdapter.ClearBeforeFill = True
        '
        'Fptable1TableAdapter1
        '
        Me.Fptable1TableAdapter1.ClearBeforeFill = True
        '
        'btnDownloadFinger
        '
        Me.btnDownloadFinger.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.btnDownloadFinger.Appearance.Options.UseFont = True
        Me.btnDownloadFinger.Location = New System.Drawing.Point(21, 115)
        Me.btnDownloadFinger.Name = "btnDownloadFinger"
        Me.btnDownloadFinger.Size = New System.Drawing.Size(148, 23)
        Me.btnDownloadFinger.TabIndex = 18
        Me.btnDownloadFinger.Text = "Download Templates"
        '
        'ToggleDownloadAll
        '
        Me.ToggleDownloadAll.Location = New System.Drawing.Point(189, 29)
        Me.ToggleDownloadAll.Name = "ToggleDownloadAll"
        Me.ToggleDownloadAll.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleDownloadAll.Properties.Appearance.Options.UseFont = True
        Me.ToggleDownloadAll.Properties.OffText = "No"
        Me.ToggleDownloadAll.Properties.OnText = "Yes"
        Me.ToggleDownloadAll.Size = New System.Drawing.Size(95, 25)
        Me.ToggleDownloadAll.TabIndex = 19
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Location = New System.Drawing.Point(5, 63)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(65, 16)
        Me.LabelControl1.TabIndex = 20
        Me.LabelControl1.Text = "Select User"
        '
        'TextSelectUser
        '
        Me.TextSelectUser.Location = New System.Drawing.Point(147, 60)
        Me.TextSelectUser.Name = "TextSelectUser"
        Me.TextSelectUser.Properties.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextSelectUser.Properties.Mask.EditMask = "[0-9]*"
        Me.TextSelectUser.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.TextSelectUser.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextSelectUser.Properties.MaxLength = 15
        Me.TextSelectUser.Size = New System.Drawing.Size(113, 20)
        Me.TextSelectUser.TabIndex = 21
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.LabelControl2.Appearance.Options.UseFont = True
        Me.LabelControl2.Location = New System.Drawing.Point(5, 34)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(74, 16)
        Me.LabelControl2.TabIndex = 22
        Me.LabelControl2.Text = "Download All"
        '
        'btnUploadFinger
        '
        Me.btnUploadFinger.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.btnUploadFinger.Appearance.Options.UseFont = True
        Me.btnUploadFinger.Location = New System.Drawing.Point(21, 73)
        Me.btnUploadFinger.Name = "btnUploadFinger"
        Me.btnUploadFinger.Size = New System.Drawing.Size(148, 23)
        Me.btnUploadFinger.TabIndex = 23
        Me.btnUploadFinger.Text = "Upload Templates"
        '
        'btnDeleteFrmDB
        '
        Me.btnDeleteFrmDB.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.btnDeleteFrmDB.Appearance.Options.UseFont = True
        Me.btnDeleteFrmDB.Location = New System.Drawing.Point(8, 66)
        Me.btnDeleteFrmDB.Name = "btnDeleteFrmDB"
        Me.btnDeleteFrmDB.Size = New System.Drawing.Size(148, 23)
        Me.btnDeleteFrmDB.TabIndex = 2
        Me.btnDeleteFrmDB.Text = "Delete User From DB"
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.LabelControl3.Appearance.Options.UseFont = True
        Me.LabelControl3.Location = New System.Drawing.Point(9, 38)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(70, 16)
        Me.LabelControl3.TabIndex = 26
        Me.LabelControl3.Text = "Make Admin"
        '
        'ToggleMakeAdmin
        '
        Me.ToggleMakeAdmin.Location = New System.Drawing.Point(95, 33)
        Me.ToggleMakeAdmin.Name = "ToggleMakeAdmin"
        Me.ToggleMakeAdmin.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleMakeAdmin.Properties.Appearance.Options.UseFont = True
        Me.ToggleMakeAdmin.Properties.OffText = "No"
        Me.ToggleMakeAdmin.Properties.OnText = "Yes"
        Me.ToggleMakeAdmin.Size = New System.Drawing.Size(95, 25)
        Me.ToggleMakeAdmin.TabIndex = 25
        '
        'btnDeleteFrmDevice
        '
        Me.btnDeleteFrmDevice.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.btnDeleteFrmDevice.Appearance.Options.UseFont = True
        Me.btnDeleteFrmDevice.Location = New System.Drawing.Point(8, 37)
        Me.btnDeleteFrmDevice.Name = "btnDeleteFrmDevice"
        Me.btnDeleteFrmDevice.Size = New System.Drawing.Size(148, 23)
        Me.btnDeleteFrmDevice.TabIndex = 1
        Me.btnDeleteFrmDevice.Text = "Delete User From Device"
        '
        'btnClearAdmin
        '
        Me.btnClearAdmin.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.btnClearAdmin.Appearance.Options.UseFont = True
        Me.btnClearAdmin.Location = New System.Drawing.Point(8, 95)
        Me.btnClearAdmin.Name = "btnClearAdmin"
        Me.btnClearAdmin.Size = New System.Drawing.Size(148, 23)
        Me.btnClearAdmin.TabIndex = 3
        Me.btnClearAdmin.Text = "Clear Admin"
        '
        'GroupControl3
        '
        Me.GroupControl3.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl3.AppearanceCaption.Options.UseFont = True
        Me.GroupControl3.Controls.Add(Me.LabelControl4)
        Me.GroupControl3.Controls.Add(Me.ToggleCreateEmp)
        Me.GroupControl3.Controls.Add(Me.LabelControl2)
        Me.GroupControl3.Controls.Add(Me.ToggleDownloadAll)
        Me.GroupControl3.Controls.Add(Me.LabelControl1)
        Me.GroupControl3.Controls.Add(Me.TextSelectUser)
        Me.GroupControl3.Controls.Add(Me.btnDownloadFinger)
        Me.GroupControl3.Location = New System.Drawing.Point(629, 12)
        Me.GroupControl3.Name = "GroupControl3"
        Me.GroupControl3.Size = New System.Drawing.Size(342, 151)
        Me.GroupControl3.TabIndex = 3
        Me.GroupControl3.Text = "Download"
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.LabelControl4.Appearance.Options.UseFont = True
        Me.LabelControl4.Location = New System.Drawing.Point(5, 91)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(140, 16)
        Me.LabelControl4.TabIndex = 24
        Me.LabelControl4.Text = "Create Employee Master"
        '
        'ToggleCreateEmp
        '
        Me.ToggleCreateEmp.Location = New System.Drawing.Point(189, 86)
        Me.ToggleCreateEmp.Name = "ToggleCreateEmp"
        Me.ToggleCreateEmp.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ToggleCreateEmp.Properties.Appearance.Options.UseFont = True
        Me.ToggleCreateEmp.Properties.OffText = "No"
        Me.ToggleCreateEmp.Properties.OnText = "Yes"
        Me.ToggleCreateEmp.Size = New System.Drawing.Size(95, 25)
        Me.ToggleCreateEmp.TabIndex = 23
        '
        'GroupControl4
        '
        Me.GroupControl4.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl4.AppearanceCaption.Options.UseFont = True
        Me.GroupControl4.Controls.Add(Me.PopupContainerControlDept)
        Me.GroupControl4.Controls.Add(Me.LabelControl3)
        Me.GroupControl4.Controls.Add(Me.ToggleMakeAdmin)
        Me.GroupControl4.Controls.Add(Me.btnUploadFinger)
        Me.GroupControl4.Location = New System.Drawing.Point(629, 169)
        Me.GroupControl4.Name = "GroupControl4"
        Me.GroupControl4.Size = New System.Drawing.Size(342, 120)
        Me.GroupControl4.TabIndex = 4
        Me.GroupControl4.Text = "Upload"
        '
        'GroupControl5
        '
        Me.GroupControl5.AppearanceCaption.Font = New System.Drawing.Font("Tahoma", 10.0!, System.Drawing.FontStyle.Bold)
        Me.GroupControl5.AppearanceCaption.Options.UseFont = True
        Me.GroupControl5.Controls.Add(Me.SimpleButtonRestart)
        Me.GroupControl5.Controls.Add(Me.BtnDisable)
        Me.GroupControl5.Controls.Add(Me.BtnEnable)
        Me.GroupControl5.Controls.Add(Me.btnLockOpen)
        Me.GroupControl5.Controls.Add(Me.TextPassword)
        Me.GroupControl5.Controls.Add(Me.LabelControl5)
        Me.GroupControl5.Controls.Add(Me.btnClearDeviceData)
        Me.GroupControl5.Controls.Add(Me.btnDeleteFrmDevice)
        Me.GroupControl5.Controls.Add(Me.btnDeleteFrmDB)
        Me.GroupControl5.Controls.Add(Me.btnClearAdmin)
        Me.GroupControl5.Location = New System.Drawing.Point(629, 295)
        Me.GroupControl5.Name = "GroupControl5"
        Me.GroupControl5.Size = New System.Drawing.Size(342, 189)
        Me.GroupControl5.TabIndex = 5
        Me.GroupControl5.Text = "Delete"
        '
        'SimpleButtonRestart
        '
        Me.SimpleButtonRestart.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.SimpleButtonRestart.Appearance.Options.UseFont = True
        Me.SimpleButtonRestart.Location = New System.Drawing.Point(162, 95)
        Me.SimpleButtonRestart.Name = "SimpleButtonRestart"
        Me.SimpleButtonRestart.Size = New System.Drawing.Size(148, 23)
        Me.SimpleButtonRestart.TabIndex = 7
        Me.SimpleButtonRestart.Text = "Restart(ZK/BioPro/Eco)"
        '
        'BtnDisable
        '
        Me.BtnDisable.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.BtnDisable.Appearance.Options.UseFont = True
        Me.BtnDisable.Location = New System.Drawing.Point(162, 66)
        Me.BtnDisable.Name = "BtnDisable"
        Me.BtnDisable.Size = New System.Drawing.Size(148, 23)
        Me.BtnDisable.TabIndex = 6
        Me.BtnDisable.Text = "Disable User In Device"
        '
        'BtnEnable
        '
        Me.BtnEnable.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.BtnEnable.Appearance.Options.UseFont = True
        Me.BtnEnable.Location = New System.Drawing.Point(162, 37)
        Me.BtnEnable.Name = "BtnEnable"
        Me.BtnEnable.Size = New System.Drawing.Size(148, 23)
        Me.BtnEnable.TabIndex = 5
        Me.BtnEnable.Text = "Enable User In Device"
        '
        'btnLockOpen
        '
        Me.btnLockOpen.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.btnLockOpen.Appearance.Options.UseFont = True
        Me.btnLockOpen.Location = New System.Drawing.Point(162, 124)
        Me.btnLockOpen.Name = "btnLockOpen"
        Me.btnLockOpen.Size = New System.Drawing.Size(175, 23)
        Me.btnLockOpen.TabIndex = 8
        Me.btnLockOpen.Text = "Open Door ZK/BioPro/Eco"
        '
        'TextPassword
        '
        Me.TextPassword.Location = New System.Drawing.Point(120, 153)
        Me.TextPassword.Name = "TextPassword"
        Me.TextPassword.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextPassword.Properties.MaxLength = 20
        Me.TextPassword.Properties.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.TextPassword.Size = New System.Drawing.Size(138, 20)
        Me.TextPassword.TabIndex = 9
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.LabelControl5.Appearance.Options.UseFont = True
        Me.LabelControl5.Location = New System.Drawing.Point(8, 154)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(95, 16)
        Me.LabelControl5.TabIndex = 30
        Me.LabelControl5.Text = "Admin Password"
        '
        'btnClearDeviceData
        '
        Me.btnClearDeviceData.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.btnClearDeviceData.Appearance.Options.UseFont = True
        Me.btnClearDeviceData.Location = New System.Drawing.Point(8, 124)
        Me.btnClearDeviceData.Name = "btnClearDeviceData"
        Me.btnClearDeviceData.Size = New System.Drawing.Size(148, 23)
        Me.btnClearDeviceData.TabIndex = 4
        Me.btnClearDeviceData.Text = "Clear Device Data"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.SimpleButton1.Appearance.Options.UseFont = True
        Me.SimpleButton1.Location = New System.Drawing.Point(903, 508)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(68, 23)
        Me.SimpleButton1.TabIndex = 6
        Me.SimpleButton1.Text = "Close"
        '
        'TblDepartment1TableAdapter1
        '
        Me.TblDepartment1TableAdapter1.ClearBeforeFill = True
        '
        'TblDepartmentTableAdapter
        '
        Me.TblDepartmentTableAdapter.ClearBeforeFill = True
        '
        'TblDepartmentBindingSource
        '
        Me.TblDepartmentBindingSource.DataMember = "tblDepartment"
        Me.TblDepartmentBindingSource.DataSource = Me.SSSDBDataSet
        '
        'AxFP_CLOCK1
        '
        Me.AxFP_CLOCK1.Enabled = True
        Me.AxFP_CLOCK1.Location = New System.Drawing.Point(442, 253)
        Me.AxFP_CLOCK1.Name = "AxFP_CLOCK1"
        Me.AxFP_CLOCK1.OcxState = CType(resources.GetObject("AxFP_CLOCK1.OcxState"), System.Windows.Forms.AxHost.State)
        Me.AxFP_CLOCK1.Size = New System.Drawing.Size(60, 44)
        Me.AxFP_CLOCK1.TabIndex = 53
        Me.AxFP_CLOCK1.Visible = False
        '
        'colMAC_ADDRESS
        '
        Me.colMAC_ADDRESS.Caption = "MAC_ADDRESS"
        Me.colMAC_ADDRESS.FieldName = "MAC_ADDRESS"
        Me.colMAC_ADDRESS.Name = "colMAC_ADDRESS"
        '
        'XtraFringerDataMgmt
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(984, 557)
        Me.Controls.Add(Me.AxFP_CLOCK1)
        Me.Controls.Add(Me.PopupContainerControlBranch)
        Me.Controls.Add(Me.SimpleButton1)
        Me.Controls.Add(Me.GroupControl5)
        Me.Controls.Add(Me.GroupControl4)
        Me.Controls.Add(Me.GroupControl3)
        Me.Controls.Add(Me.GroupControl2)
        Me.Controls.Add(Me.GroupControl1)
        Me.LookAndFeel.SkinName = "iMaginary"
        Me.LookAndFeel.UseDefaultLookAndFeel = False
        Me.MaximizeBox = False
        Me.Name = "XtraFringerDataMgmt"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "User Setup"
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblMachineBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl1.ResumeLayout(False)
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl2.ResumeLayout(False)
        CType(Me.PanelControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl2.ResumeLayout(False)
        CType(Me.GridControl2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FptableBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.PopupContainerEditDept.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerControlDept, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PopupContainerControlDept.ResumeLayout(False)
        CType(Me.GridControlDept, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewDept, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTimeEdit3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerEditLocation.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerControlBranch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PopupContainerControlBranch.ResumeLayout(False)
        CType(Me.GridControlBranch, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewBranch, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTimeEdit7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit2.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerControlShift, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PopupContainerControlShift.ResumeLayout(False)
        CType(Me.GridControlShift, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewShift, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit1.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTimeEdit5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleDownloadAll.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextSelectUser.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToggleMakeAdmin.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl3.ResumeLayout(False)
        Me.GroupControl3.PerformLayout()
        CType(Me.ToggleCreateEmp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GroupControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl4.ResumeLayout(False)
        Me.GroupControl4.PerformLayout()
        CType(Me.GroupControl5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupControl5.ResumeLayout(False)
        Me.GroupControl5.PerformLayout()
        CType(Me.TextPassword.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblDepartmentBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AxFP_CLOCK1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SSSDBDataSet As iAS.SSSDBDataSet
    Friend WithEvents TblMachineBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents TblMachineTableAdapter As iAS.SSSDBDataSetTableAdapters.tblMachineTableAdapter
    Friend WithEvents TblMachine1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.tblMachine1TableAdapter
    Friend WithEvents GroupControl1 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colID_NO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLOCATION As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colbranch As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDeviceType As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GroupControl2 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GridControl2 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView2 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents FptableBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents FptableTableAdapter As iAS.SSSDBDataSetTableAdapters.fptableTableAdapter
    Friend WithEvents Fptable1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.fptable1TableAdapter
    Friend WithEvents colEMachineNumber As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colEnrollNumber As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colUserName As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colFingerNumber As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPrivilege As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colPassword As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colTemplate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCardnumber As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colcalid As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colauthority As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colcheck_type As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colopendoor_type As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colface_Data1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colface_data2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colface_data3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colface_data4 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colface_data5 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colface_data6 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colface_data7 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colface_data8 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colface_data9 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colface_data10 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colface_data11 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colface_data12 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colface_data13 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colface_data14 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colface_data15 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colface_data16 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colface_data17 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colface_data18 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colTemplate_Face As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colTemplate_Tw As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colVerifyMode As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents btnDownloadFinger As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents ToggleDownloadAll As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextSelectUser As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents btnUploadFinger As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnDeleteFrmDB As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ToggleMakeAdmin As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents btnDeleteFrmDevice As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btnClearAdmin As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GroupControl3 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupControl4 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents GroupControl5 As DevExpress.XtraEditors.GroupControl
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents colA_R As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colcommkey As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents ToggleCreateEmp As DevExpress.XtraEditors.ToggleSwitch
    Friend WithEvents btnClearDeviceData As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextPassword As DevExpress.XtraEditors.TextEdit
    Friend WithEvents btnLockOpen As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents BtnDisable As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents BtnEnable As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButtonRestart As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents PanelControl2 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents PopupContainerControlBranch As DevExpress.XtraEditors.PopupContainerControl
    Friend WithEvents GridControlBranch As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridViewBranch As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colBRANCHCODE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBRANCHNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemTimeEdit7 As DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit
    Friend WithEvents RepositoryItemDateEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents PopupContainerControlShift As DevExpress.XtraEditors.PopupContainerControl
    Friend WithEvents GridControlShift As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridViewShift As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colSHIFT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSTARTTIME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemDateEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents colENDTIME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemTimeEdit5 As DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit
    Friend WithEvents PopupContainerControlDept As DevExpress.XtraEditors.PopupContainerControl
    Friend WithEvents GridControlDept As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridViewDept As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colDEPARTMENTCODE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDEPARTMENTNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemTimeEdit3 As DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit
    Friend WithEvents TblDepartment1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.tblDepartment1TableAdapter
    Friend WithEvents TblDepartmentTableAdapter As iAS.SSSDBDataSetTableAdapters.tblDepartmentTableAdapter
    Friend WithEvents TblDepartmentBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PopupContainerEditLocation As DevExpress.XtraEditors.PopupContainerEdit
    Friend WithEvents PopupContainerEditDept As DevExpress.XtraEditors.PopupContainerEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents btnSearch As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents AxFP_CLOCK1 As AxFP_CLOCKLib.AxFP_CLOCK
    Friend WithEvents colUltra800FId As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colMAC_ADDRESS As DevExpress.XtraGrid.Columns.GridColumn
End Class
