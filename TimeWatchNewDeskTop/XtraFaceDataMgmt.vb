﻿Imports System.Resources
Imports System.Globalization
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports System.ComponentModel
Imports DevExpress.XtraGrid.Columns
Imports System.Data.SqlClient
Imports System.Text
Imports System.Data.OleDb
Imports DevExpress.XtraGrid.Views.Base
Imports iAS.AscDemo
Imports System.Runtime.InteropServices
Imports System.Threading
Imports Newtonsoft.Json

Public Class XtraFaceDataMgmt
    Public Delegate Sub invokeDelegate()
    'for getting user info (not having card number) added from staffmanagament.cs (Acs Demo)
    Dim m_lUserInfoSearchHandle As Integer = -1 'Hikvision
    Dim g_fUserInfoSearchCallback As CHCNetSDK.RemoteConfigCallback = Nothing 'Hikvision
    Dim m_JsonUserInfoSearch As CUserInfoSearch
    Public m_iSearchPosition As Integer = 0
    Public m_iUserCount As Integer = 0
    Public m_lUserInfoRecordHandle As Integer = -1
    Public m_lUserID As Int32 = -1
    Public m_lUserInfoDeleteHandle As Int32 = -1
    Public m_iUserInfoRecordIndex As Integer = 0
    Public m_bSearchAll As Boolean = True
    Public m_bDeleteAll As Boolean = True
    Public m_iUserInfoSettingHandle As Int32 = -1
    Dim lUserID As Integer = -1
    Dim paycodelist As New List(Of String)()
    Dim paycodeArray '= paycodelist.Distinct.ToArray ' paycodelist.ToArray
    'for getting user info (not having card number)
    ' Dim serialNo As String = ""
    Dim m_lGetCardCfgHandle As Int32 = -1 'Hikvision
    Dim g_fGetGatewayCardCallback As CHCNetSDK.RemoteConfigCallback = Nothing 'Hikvision
    Dim m_lGetFingerPrintCfgHandle As Integer = -1
    Dim HCardNos As List(Of String) = Nothing
    Dim iMaxCardNum As Integer = 1000
    Dim m_struFingerPrintOne As CHCNetSDK.NET_DVR_FINGER_PRINT_CFG_V50 = New CHCNetSDK.NET_DVR_FINGER_PRINT_CFG_V50
    Dim m_struDelFingerPrint As CHCNetSDK.NET_DVR_FINGER_PRINT_INFO_CTRL_BYCARD_V50 = New CHCNetSDK.NET_DVR_FINGER_PRINT_INFO_CTRL_BYCARD_V50
    Dim m_struRecordCardCfg() As CHCNetSDK.NET_DVR_FINGER_PRINT_CFG_V50 = New CHCNetSDK.NET_DVR_FINGER_PRINT_CFG_V50((iMaxCardNum) - 1) {}
    Dim m_struCardInfo() As CHCNetSDK.NET_DVR_CARD_CFG_V50 = New CHCNetSDK.NET_DVR_CARD_CFG_V50((iMaxCardNum) - 1) {}

    Dim g_fSetFingerPrintCallback As CHCNetSDK.RemoteConfigCallback = Nothing
    Dim g_fGetFingerPrintCallback As CHCNetSDK.RemoteConfigCallback = Nothing
    Dim g_fDelFingerPrintCallback As CHCNetSDK.RemoteConfigCallback = Nothing
    Dim g_fSetGatewayCardCallback As CHCNetSDK.RemoteConfigCallback = Nothing
    Dim m_lSetCardCfgHandle As Int32 = -1
    Dim m_bSendOne As Boolean = False
    Dim m_struSelSendCardCfg As CHCNetSDK.NET_DVR_CARD_CFG_V50
    Dim m_struNowSendCard As CHCNetSDK.NET_DVR_CARD_CFG_V50
    Dim m_BSendSel As Boolean = False
    Dim m_dwCardNum As UInteger = 0
    Dim m_dwSendIndex As UInteger = 0
    Dim m_lSetFingerPrintCfgHandle As Integer = -1
    Dim m_iSendIndex As Integer = -1

    Dim Downloading As Boolean = False

    Dim ulf As UserLookAndFeel
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand

    Const PWD_DATA_SIZE = 40
    Const FP_DATA_SIZE = 1680
    Const FACE_DATA_SIZE = 20080
    Const VEIN_DATA_SIZE = 3080
    Private mbytEnrollData(FACE_DATA_SIZE - 1) As Byte
    Private mbytPhotoImage() As Byte

    Private mnCommHandleIndex As Long
    Private mbGetState As Boolean
    Private mlngPasswordData As Long

    Dim vpszIPAddress As String
    Dim vpszNetPort As Long
    Dim vpszNetPassword As Long
    Dim vnTimeOut As Long
    Dim vnProtocolType As Long
    Dim strDateTime As String
    Dim vnResult As Long
    Dim vnLicense As Long


    '//=============== Backup Number Constant ===============//
    'Public Const BACKUP_FP_0 = 0                  ' Finger 0
    'Public Const BACKUP_FP_1 = 1                  ' Finger 1
    'Public Const BACKUP_FP_2 = 2                  ' Finger 2
    'Public Const BACKUP_FP_3 = 3                  ' Finger 3
    'Public Const BACKUP_FP_4 = 4                  ' Finger 4
    'Public Const BACKUP_FP_5 = 5                  ' Finger 5
    'Public Const BACKUP_FP_6 = 6                  ' Finger 6
    'Public Const BACKUP_FP_7 = 7                  ' Finger 7
    'Public Const BACKUP_FP_8 = 8                  ' Finger 8
    'Public Const BACKUP_FP_9 = 9                  ' Finger 9
    'Public Const BACKUP_PSW = 10                  ' Password
    Public Const BACKUP_CARD = 11                 ' Card
    Public Const BACKUP_FACE = 12                 ' Face
    Public Const BACKUP_VEIN_0 = 20               ' Vein 0


    Dim m_lGetFaceCfgHandle As Integer = -1
    Dim m_lSetFaceCfgHandle As Integer = -1
    Dim m_lCapFaceCfgHandle As Integer = -1

    Dim m_UserID As Integer = -1
    Dim m_lGetUserCfgHandle As Int32 = -1
    Dim m_lSetUserCfgHandle As Int32 = -1
    Dim m_lDelUserCfgHandle As Int32 = -1

    Public Sub New()
        InitializeComponent()
        Common.SetGridFont(GridView1, New Font("Tahoma", 10))
        Common.SetGridFont(GridView2, New Font("Tahoma", 10))
        m_struFingerPrintOne.Init()
        m_struDelFingerPrint.struByCard.byFingerPrintID = New Byte((CHCNetSDK.MAX_FINGER_PRINT_NUM) - 1) {}
    End Sub
    Private Sub setDefault()
        ToggleDownloadAll.IsOn = True
        If ToggleDownloadAll.IsOn = True Then
            LabelControl1.Visible = False
            TextSelectUser.Visible = False
        Else
            LabelControl1.Visible = True
            TextSelectUser.Visible = True
        End If
        TextSelectUser.Text = ""
        ToggleCreateEmp.IsOn = False
        GridView1.ClearSelection()
        GridView2.ClearSelection()
        TextPassword.Text = ""
        For Each c As Control In Me.Controls
            c.Enabled = True
        Next
        CheckFace.Checked = True
    End Sub
    Private Sub XtraFringerDataMgmt_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True

        'If Common.servername = "Access" Then
        '    Me.TblMachine1TableAdapter1.Fill(Me.SSSDBDataSet.tblMachine1)
        '    'GridControl1.DataSource = SSSDBDataSet.tblMachine1
        '    Me.Fptable1TableAdapter1.Fill(Me.SSSDBDataSet.fptable1)
        '    GridControl2.DataSource = SSSDBDataSet.fptable1
        'Else
        '    TblMachineTableAdapter.Connection.ConnectionString = Common.ConnectionString
        '    FptableTableAdapter.Connection.ConnectionString = Common.ConnectionString
        '    Me.TblMachineTableAdapter.Fill(Me.SSSDBDataSet.tblMachine)
        '    'GridControl1.DataSource = SSSDBDataSet.tblMachine
        '    Me.FptableTableAdapter.Fill(Me.SSSDBDataSet.fptable)
        '    GridControl2.DataSource = SSSDBDataSet.fptable
        'End If
        GridControl1.DataSource = Common.MachineNonAdminUltra
        LoadFPGrid()

        setDefault()
    End Sub
    Private Sub GridView2_CustomColumnDisplayText(sender As System.Object, e As DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs) Handles GridView2.CustomColumnDisplayText
        Try
            Dim row As System.Data.DataRow = GridView2.GetDataRow(GridView2.FocusedRowHandle)
            Dim view As ColumnView = TryCast(sender, ColumnView)
            If e.Column.FieldName = "FingerNumber" Then
                If view.GetListSourceRowCellValue(e.ListSourceRowIndex, "FingerNumber").ToString().Trim >= 0 And _
                    view.GetListSourceRowCellValue(e.ListSourceRowIndex, "FingerNumber").ToString().Trim < 10 Then
                    e.DisplayText = "FP-" & view.GetListSourceRowCellValue(e.ListSourceRowIndex, "FingerNumber").ToString().Trim
                ElseIf view.GetListSourceRowCellValue(e.ListSourceRowIndex, "FingerNumber").ToString().Trim = 12 Or view.GetListSourceRowCellValue(e.ListSourceRowIndex, "FingerNumber").ToString().Trim = 50 Then
                    e.DisplayText = "FACE"
                ElseIf view.GetListSourceRowCellValue(e.ListSourceRowIndex, "FingerNumber").ToString().Trim = 11 Then
                    e.DisplayText = "CARD"
                ElseIf view.GetListSourceRowCellValue(e.ListSourceRowIndex, "FingerNumber").ToString().Trim = 10 Then
                    e.DisplayText = "PASSWORD"
                End If
                'e.DisplayText = (view.GetListSourceRowCellValue(e.ListSourceRowIndex, "SHIFTDURATION").ToString().Trim \ 60).ToString("00") & ":" & (view.GetListSourceRowCellValue(e.ListSourceRowIndex, "SHIFTDURATION").ToString().Trim Mod 60).ToString("00")
            End If
        Catch
        End Try
        Try
            Dim adapA As OleDbDataAdapter
            Dim adap As SqlDataAdapter
            Dim ds As DataSet
            Dim sSql As String
            Dim row As System.Data.DataRow = GridView2.GetDataRow(GridView2.FocusedRowHandle)
            Dim view As ColumnView = TryCast(sender, ColumnView)
            If e.Column.FieldName = "UserName" Then
                Dim Enumber As String
                If IsNumeric(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "EnrollNumber").ToString().Trim) Then
                    Enumber = Convert.ToInt64(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "EnrollNumber").ToString().Trim).ToString("000000000000")
                Else
                    Enumber = view.GetListSourceRowCellValue(e.ListSourceRowIndex, "EnrollNumber").ToString().Trim
                End If
                sSql = "SELECT EMPNAME from TblEmployee where PRESENTCARDNO = '" & Enumber & "'"
                ds = New DataSet
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(ds)
                Else
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(ds)
                End If
                If ds.Tables(0).Rows.Count > 0 Then
                    e.DisplayText = ds.Tables(0).Rows(0).Item(0).ToString
                Else
                    '        e.DisplayText = ""
                End If
            End If

            If e.Column.Caption = "Device Type" Then
                'If view.GetListSourceRowCellValue(e.ListSourceRowIndex, "Template").ToString().Trim = "" Then
                '    If view.GetListSourceRowCellValue(e.ListSourceRowIndex, "Cardnumber").ToString().Trim <> "" Or view.GetListSourceRowCellValue(e.ListSourceRowIndex, "Password").ToString().Trim <> "" Then
                '        e.DisplayText = "ZK/Bio"
                '    Else
                '        e.DisplayText = "Bio"
                '    End If
                'ElseIf view.GetListSourceRowCellValue(e.ListSourceRowIndex, "Template_Tw").ToString().Trim = "" Then
                '    e.DisplayText = "ZK"
                'End If
                e.DisplayText = "HKSeries"
            End If
        Catch ex As Exception
        End Try
    End Sub
    Private Sub GridView1_CustomColumnDisplayText(sender As System.Object, e As DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs) Handles GridView1.CustomColumnDisplayText
        Try
            Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
            Dim view As ColumnView = TryCast(sender, ColumnView)
            If e.Column.FieldName = "DeviceType" Then
                If view.GetListSourceRowCellValue(e.ListSourceRowIndex, "DeviceType").ToString().Trim = "Bio-1/3/4/5/6/7/ATF-203/ATF-395/ACR-872" Then
                    e.DisplayText = "Bio"
                ElseIf view.GetListSourceRowCellValue(e.ListSourceRowIndex, "DeviceType").ToString().Trim = "ZK(TFT)" Or view.GetListSourceRowCellValue(e.ListSourceRowIndex, "DeviceType").ToString().Trim = "Bio-1Pro/ATF305Pro/ATF686Pro" Then
                    e.DisplayText = "ZK"
                End If
                e.DisplayText = (view.GetListSourceRowCellValue(e.ListSourceRowIndex, "SHIFTDURATION").ToString().Trim \ 60).ToString("00") & ":" & (view.GetListSourceRowCellValue(e.ListSourceRowIndex, "SHIFTDURATION").ToString().Trim Mod 60).ToString("00")
            End If
        Catch
        End Try
    End Sub
    Private Sub btnDownloadFinger_Click(sender As System.Object, e As System.EventArgs) Handles btnDownloadFinger.Click
        If GridView1.GetSelectedRows.Count = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>Please select Device</size>", "<size=9>Error</size>")
            Exit Sub
        Else
            If ToggleDownloadAll.IsOn = False Then
                If TextSelectUser.Text = "" Then
                    XtraMessageBox.Show(ulf, "<size=10>Please Enter User</size>", "<size=9>Error</size>")
                    Exit Sub
                End If
            End If

            If GridView1.SelectedRowsCount > 1 Then
                XtraMessageBox.Show(ulf, "<size=10>Please Select Single device at a time</size>", "<size=9>Error</size>")
                Exit Sub
            End If

            Me.Cursor = Cursors.WaitCursor
            For Each c As Control In Me.Controls
                c.Enabled = False
            Next
            Dim cn As Common = New Common
            Dim paycodelist As New List(Of String)()
            Dim AllUserList As New List(Of CUserInfoSearchCfg)()


            Dim paycodeArray '= paycodelist.Distinct.ToArray ' paycodelist.ToArray

            Dim commkey As Integer

            Dim selectedRows As Integer() = GridView1.GetSelectedRows()
            Dim result As Object() = New Object(selectedRows.Length - 1) {}
            For i As Integer = 0 To selectedRows.Length - 1
                Dim rowHandle As Integer = selectedRows(i)
                If Not GridView1.IsGroupRow(rowHandle) Then
                    result(i) = GridView1.GetRowCellValue(rowHandle, "ID_NO")
                    vpszIPAddress = Trim(GridView1.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim)

                    Dim Emachinenumber As Integer = result(i)
                    Dim A_R As String = GridView1.GetRowCellValue(rowHandle, "A_R") 'GridView1.GetRowCellValue(rowHandle, GridView1.Columns("A_R"))
                    If GridView1.GetRowCellValue(rowHandle, "DeviceType") = "Bio-1/3/4/5/6/7/ATF-203/ATF-395/ACR-872" Then

                    ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "ZK(TFT)" Or GridView1.GetRowCellValue(rowHandle, "DeviceType") = "Bio-1Pro/ATF305Pro/ATF686Pro" Then

                    ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "HKSeries" Then

                        XtraMasterTest.LabelControlStatus.Text = "Connecting " & vpszIPAddress & "..."
                        Application.DoEvents()
                        Dim struDeviceInfoV40 As CHCNetSDK.NET_DVR_DEVICEINFO_V40 = New CHCNetSDK.NET_DVR_DEVICEINFO_V40
                        Dim DeviceAdd As String = vpszIPAddress, userName As String = Trim(GridView1.GetRowCellValue(rowHandle, "HLogin").ToString.Trim), pwd As String = Trim(GridView1.GetRowCellValue(rowHandle, "HPassword").ToString.Trim)
                        Dim failReason As String = ""
                        Dim logistatus = cn.HikvisionLogin(DeviceAdd, userName, pwd, struDeviceInfoV40, lUserID, failReason)
                        If logistatus = False Then
                            If failReason <> "" Then
                                XtraMessageBox.Show(ulf, "<size=10>" & failReason & "</size>", "Failed")
                                XtraMasterTest.LabelControlStatus.Text = ""
                                Application.DoEvents()
                                Continue For
                            End If

                        Else
                            If failReason <> "" Then
                                XtraMessageBox.Show(ulf, "<size=10>" & failReason & "</size>", "Failed")
                                XtraMasterTest.LabelControlStatus.Text = ""
                                Application.DoEvents()
                                logistatus = cn.HikvisionLogOut(lUserID)
                                Continue For
                            End If
                            Downloading = True
                            m_lUserID = lUserID
                            If -1 <> m_lUserInfoSearchHandle Then
                                If CHCNetSDK.NET_DVR_StopRemoteConfig(m_lUserInfoSearchHandle) Then
                                    m_lUserInfoSearchHandle = -1
                                End If
                            End If

                            Dim sURL As String = "POST /ISAPI/AccessControl/UserInfo/Search?format=json"
                            Dim ptrURL As IntPtr = Marshal.StringToHGlobalAnsi(sURL)
                            m_lSetUserCfgHandle = CHCNetSDK.NET_DVR_StartRemoteConfig(lUserID, CHCNetSDK.NET_DVR_JSON_CONFIG, ptrURL, sURL.Length, Nothing, IntPtr.Zero)

                            If m_lSetUserCfgHandle < 0 Then
                                'MessageBox.Show("NET_DVR_StartRemoteConfig fail [url:POST /ISAPI/AccessControl/UserInfo/Search?format=json] error:" & CHCNetSDK.NET_DVR_GetLastError())
                                Marshal.FreeHGlobal(ptrURL)
                                Continue For
                            Else

                                Dim getUser As Boolean = True
                                Dim userStartPos As Integer = 0
                                While getUser
                                    Dim JsonUserInfoSearchCond As CUserInfoSearchCondCfg = New CUserInfoSearchCondCfg()
                                    JsonUserInfoSearchCond.UserInfoSearchCond = New CUserInfoSearchCond()
                                    Dim EmpNO As String = ""
                                    If Not ToggleDownloadAll.IsOn And TextSelectUser.Text.Trim <> "" Then
                                        EmpNO = TextSelectUser.Text.Trim
                                    End If


                                    If EmpNO.Length = 0 Then
                                        JsonUserInfoSearchCond.UserInfoSearchCond.searchID = "1"
                                        JsonUserInfoSearchCond.UserInfoSearchCond.searchResultPosition = userStartPos
                                        JsonUserInfoSearchCond.UserInfoSearchCond.maxResults = 10
                                    Else
                                        JsonUserInfoSearchCond.UserInfoSearchCond.searchID = "1"
                                        JsonUserInfoSearchCond.UserInfoSearchCond.searchResultPosition = 0
                                        JsonUserInfoSearchCond.UserInfoSearchCond.maxResults = 10
                                        JsonUserInfoSearchCond.UserInfoSearchCond.EmployeeNoList = New List(Of CEmployeeNoList)()
                                        Dim singleEmployeeNoList As CEmployeeNoList = New CEmployeeNoList()
                                        singleEmployeeNoList.employeeNo = EmpNO ' textBoxEmployeeNo.Text
                                        JsonUserInfoSearchCond.UserInfoSearchCond.EmployeeNoList.Add(singleEmployeeNoList)
                                    End If

                                    Dim strUserInfoSearchCondCfg As String = JsonConvert.SerializeObject(JsonUserInfoSearchCond)
                                    Dim ptrUserInfoSearchCondCfg As IntPtr = Marshal.StringToHGlobalAnsi(strUserInfoSearchCondCfg)
                                    Dim ptrJsonData As IntPtr = Marshal.AllocHGlobal(1024 * 10)

                                    For m As Integer = 0 To 1024 * 10 - 1
                                        Marshal.WriteByte(ptrJsonData, m, 0)
                                    Next

                                    Dim dwState As Integer = CInt(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_SUCCESS)
                                    Dim dwReturned As UInteger = 0

                                    While True
                                        dwState = CHCNetSDK.NET_DVR_SendWithRecvRemoteConfig(m_lSetUserCfgHandle, ptrUserInfoSearchCondCfg, CUInt(strUserInfoSearchCondCfg.Length), ptrJsonData, 1024 * 10, dwReturned)
                                        Dim strJsonData As String = Marshal.PtrToStringAnsi(ptrJsonData)

                                        If dwState = CInt(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_NEEDWAIT) Then
                                            Thread.Sleep(10)
                                            Continue While
                                        ElseIf dwState = CInt(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_FAILED) Then
                                            'MessageBox.Show("Get User Fail error:" & CHCNetSDK.NET_DVR_GetLastError())
                                        ElseIf dwState = CInt(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_SUCCESS) Then
                                            Dim JsonUserInfoSearchCfg As CUserInfoSearchCfg = New CUserInfoSearchCfg()
                                            JsonUserInfoSearchCfg = JsonConvert.DeserializeObject(Of CUserInfoSearchCfg)(strJsonData)

                                            If JsonUserInfoSearchCfg.UserInfoSearch Is Nothing Then
                                                Dim JsonResponseStatus As CResponseStatus = New CResponseStatus()
                                                JsonResponseStatus = JsonConvert.DeserializeObject(Of CResponseStatus)(strJsonData)

                                                If JsonResponseStatus.statusCode = 1 Then
                                                    'MessageBox.Show("Get User Success")
                                                Else
                                                    'MessageBox.Show("Get User Fail, ResponseStatus.statusCode" & JsonResponseStatus.statusCode)
                                                    getUser = False
                                                End If
                                            Else

                                                If JsonUserInfoSearchCfg.UserInfoSearch.totalMatches = 0 Then
                                                    'MessageBox.Show("no this EmployeeNo person")
                                                    getUser = False
                                                    Exit While
                                                End If

                                                If JsonUserInfoSearchCfg.UserInfoSearch.numOfMatches < 10 Then getUser = False

                                                For m As Integer = 0 To JsonUserInfoSearchCfg.UserInfoSearch.numOfMatches - 1
                                                    Try
                                                        paycodelist.Add(JsonUserInfoSearchCfg.UserInfoSearch.UserInfo(m).employeeNo & ";" & JsonUserInfoSearchCfg.UserInfoSearch.UserInfo(m).name)
                                                    Catch ex As Exception

                                                    End Try
                                                Next
                                                AllUserList.Add(JsonUserInfoSearchCfg)
                                                'saveHikvision(JsonUserInfoSearchCfg, Emachinenumber)
                                                userStartPos = userStartPos + 10
                                            End If

                                            Exit While
                                        ElseIf dwState = CInt(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_FINISH) Then
                                            'MessageBox.Show("Get User Finish")
                                            getUser = False
                                            Exit While
                                        ElseIf dwState = CInt(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_EXCEPTION) Then
                                            'MessageBox.Show("Get User Exception error:" & CHCNetSDK.NET_DVR_GetLastError())
                                            getUser = False
                                            Exit While
                                        Else
                                            'MessageBox.Show("unknown status error:" & CHCNetSDK.NET_DVR_GetLastError())
                                            getUser = False
                                            Exit While
                                        End If
                                    End While

                                    Marshal.FreeHGlobal(ptrUserInfoSearchCondCfg)
                                    Marshal.FreeHGlobal(ptrJsonData)
                                End While
                            End If
                            Dim AllUserArr() As CUserInfoSearchCfg = AllUserList.ToArray()
                            saveHikvision(AllUserArr, Emachinenumber)
                            cn.HikvisionLogOut(lUserID)

                            'm_iUserCount = 0
                            'm_iSearchPosition = 0
                            'serialNo = GridView1.GetRowCellValue(rowHandle, "MAC_ADDRESS").ToString.Trim
                            'g_fUserInfoSearchCallback = New CHCNetSDK.RemoteConfigCallback(AddressOf ProcessUserInfoSearchCallback)
                            ''Thread.Sleep(200)
                            'Dim sURL As String = "POST /ISAPI/AccessControl/UserInfo/Search?format=json"
                            'Dim ptrURL As IntPtr = Marshal.StringToHGlobalAnsi(sURL)
                            'm_lUserInfoSearchHandle = CHCNetSDK.NET_DVR_StartRemoteConfig(m_lUserID, CHCNetSDK.NET_DVR_JSON_CONFIG, ptrURL, sURL.Length, g_fUserInfoSearchCallback, Me.Handle)
                            ''Thread.Sleep(200)
                            'If (-1 = m_lUserInfoSearchHandle) Then
                            '    Dim listItem As ListViewItem = New ListViewItem
                            '    listItem.Text = "Fail"
                            '    Dim strTemp As String = String.Format("User Info Search Fail,Error Code={0}", CHCNetSDK.NET_DVR_GetLastError)
                            '    listItem.SubItems.Add(strTemp)
                            '    'Me.AddList(listViewMessage, listItem)
                            '    Marshal.FreeHGlobal(ptrURL)
                            '    Continue For
                            'Else
                            '    Dim listItem As ListViewItem = New ListViewItem
                            '    listItem.Text = "Succ"
                            '    listItem.SubItems.Add("User Info Search Success")
                            '    'Me.AddList(listViewMessage, listItem)
                            'End If

                            'Marshal.FreeHGlobal(ptrURL)
                            'If Not SearchUserInfo() Then
                            '    CHCNetSDK.NET_DVR_StopRemoteConfig(m_lUserInfoSearchHandle)
                            '    m_lUserInfoSearchHandle = -1
                            'Else
                            '    'XtraMessageBox.Show(ulf, "<size=10>No Data Available</size>", "ULtra")
                            '    'Me.Cursor = Cursors.Default
                            '    'For Each c As Control In Me.Controls
                            '    '    c.Enabled = True
                            '    'Next
                            '    'Continue For
                            'End If
                        End If
                    End If
                Else
                    result(0) = -1
                End If
            Next

            If ToggleCreateEmp.IsOn = True Then
                paycodeArray = paycodelist.Distinct.ToArray ' paycodelist.ToArray
                Common.CreateEmployee(paycodeArray, "Device")
                Common.loadEmp()
            End If

            For Each c As Control In Me.Controls
                c.Enabled = True
            Next

            'Dim msg As New Message("Downloading", "Download Started...")
            'XtraMaster.AlertControl1.Show(XtraMaster, msg.Caption, msg.Text, "", msg.Image, msg)
            XtraMasterTest.LabelControlStatus.Text = ""
            XtraMasterTest.LabelControlCount.Text = ""
            Application.DoEvents()
            Me.Cursor = Cursors.Default

            XtraMessageBox.Show(ulf, "<size=10>Task Completed</size>", "<size=9>Information</size>")
        End If
    End Sub

    Private Sub AddToCardList(ByVal struCardInfo As CHCNetSDK.NET_DVR_CARD_CFG_V50, ByVal strCardNo As String)
        Dim iItemIndex As Integer = 0 'GetExistItem(struCardInfo)
        If (-1 = iItemIndex) Then
            iItemIndex = 0 ' listViewGataManage.Items.Count
        End If
        'UpdateList(iItemIndex, strCardNo, struCardInfo)
        m_struCardInfo(iItemIndex) = struCardInfo
    End Sub
    'Public Delegate Sub SetTextCallbacksaveHikvision(ByVal userInfo As CUserInfoSearch)
    Private Sub saveHikvision(userInfoArr() As CUserInfoSearchCfg, serialNo As String)  'ByVal struCardCfg As CHCNetSDK.NET_DVR_CARD_CFG_V50
        'If InvokeRequired = True Then
        '    Dim d As SetTextCallbacksaveHikvision = New SetTextCallbacksaveHikvision(AddressOf Me.saveHikvision)
        '    Me.Invoke(d, New Object() {userInfo})
        '    'Dim tmp = IN_OUT
        'Else
        Dim cn As Common = New Common
        Dim con1 As OleDbConnection
        Dim con As SqlConnection
        If Common.servername = "Access" Then
            con1 = New OleDbConnection(Common.ConnectionString)
        Else
            con = New SqlConnection(Common.ConnectionString)
        End If
        'For i As Integer = 0 To userInfo.UserInfoSearch.numOfMatches - 1
        Dim tmpcount As Integer = 0
        For Each userInfo As CUserInfoSearchCfg In userInfoArr
            For i As Integer = 0 To userInfo.UserInfoSearch.UserInfo.Count - 1
                tmpcount = tmpcount + 1
                Dim CardNumber As String = "" 'System.Text.Encoding.UTF8.GetString(userInfo.UserInfoSearch.UserInfo(i).)
                ''HCardNos.Add(EnrollNumber)
                'GetHFp(CardNumber)
                Dim EnrollNumber = userInfo.UserInfoSearch.UserInfo(i).employeeNo.ToString()
                Dim Privilege As Integer
                If userInfo.UserInfoSearch.UserInfo(i).localUIRight = True Then
                    Privilege = 1
                Else
                    Privilege = 0
                End If
                'Dim del As invokeDelegate = Function()
                XtraMasterTest.LabelControlStatus.Text = "Downloading Template " & EnrollNumber
                XtraMasterTest.LabelControlCount.Text = "Downloading " & tmpcount & " of " & userInfo.UserInfoSearch.totalMatches.ToString
                Application.DoEvents()
                'End Sub
                '            Invoke(del)
                If CheckFP.Checked = True Then
                    GetHFp(EnrollNumber, userInfo.UserInfoSearch.UserInfo(i).name, Privilege, serialNo)
                End If
                'GetHFp(EnrollNumber)
                GetCard(EnrollNumber, userInfo.UserInfoSearch.UserInfo(i).name, Privilege, serialNo)
                If CheckFace.Checked Then
                    GetHFace(EnrollNumber, userInfo.UserInfoSearch.UserInfo(i).name, Privilege, userInfo.UserInfoSearch.UserInfo(i).password, serialNo)
                End If
                'temp open
                'If IsNumeric(EnrollNumber) Then
                '    EnrollNumber = Convert.ToDouble(EnrollNumber).ToString("000000000000")
                'End If
                Dim DeviceType As String = "HKSeries"
                Dim Password As String = userInfo.UserInfoSearch.UserInfo(i).password ' System.Text.Encoding.UTF8.GetString(struCardCfg.byCardPassword)
                Dim UserName As String = userInfo.UserInfoSearch.UserInfo(i).name

                If Password.Trim(vbNullChar) <> "" Then
                    Dim adap As SqlDataAdapter
                    Dim adapA As OleDbDataAdapter
                    Dim sSql As String = "select * from fptable where [EMachineNumber]='" & serialNo.Trim(vbNullChar) & "' and EnrollNumber ='" & EnrollNumber & "' and FingerNumber=10 and DeviceType='HKSeries'"
                    Dim dsRecord As DataSet = New DataSet
                    If Common.servername = "Access" Then
                        adapA = New OleDbDataAdapter(sSql, con1)
                        adapA.Fill(dsRecord)
                    Else
                        adap = New SqlDataAdapter(sSql, con)
                        adap.Fill(dsRecord)
                    End If
                    If dsRecord.Tables(0).Rows.Count = 0 Then
                        sSql = "insert into fptable (Emachinenumber, EnrollNumber, UserName, Password, FingerNumber, Privilege, DeviceType) values ('" & serialNo.Trim(vbNullChar) & "','" & EnrollNumber & "', '" & UserName.Trim(vbNullChar) & "','" & Password.Trim(vbNullChar) & "', 10, " & Privilege & ",'HKSeries')"
                    Else
                        sSql = "update fptable set Password='" & Password.Trim(vbNullChar) & "' , UserName='" & UserName.Trim(vbNullChar) & "', Privilege=" & Privilege & " where EnrollNumber='" & EnrollNumber & "' and Emachinenumber ='" & serialNo.Trim(vbNullChar) & "' and FingerNumber=10 and DeviceType='HKSeries'"
                    End If
                    If Common.servername = "Access" Then
                        If con1.State <> ConnectionState.Open Then
                            con1.Open()
                        End If
                        Try
                            Dim cmd As OleDbCommand
                            cmd = New OleDbCommand(sSql, con1)
                            cmd.ExecuteNonQuery()
                        Catch ex As Exception

                        End Try

                        If con1.State <> ConnectionState.Closed Then
                            con1.Close()
                        End If
                    Else
                        If con.State <> ConnectionState.Open Then
                            con.Open()
                        End If
                        Dim cmd As SqlCommand
                        cmd = New SqlCommand(sSql, con)
                        cmd.ExecuteNonQuery()
                        If con.State <> ConnectionState.Closed Then
                            con.Close()
                        End If
                    End If
                End If
                'End temp open
            Next
        Next
        paycodeArray = paycodelist.Distinct.ToArray
        Common.CreateEmployee(paycodeArray, "Device")
        'Dim cn As Common = New Common
        'cn.HikvisionLogOut(lUserID)

        'If Common.servername = "Access" Then
        '    Me.Fptable1TableAdapter1.Fill(Me.SSSDBDataSet.fptable1)
        '    GridControl2.DataSource = SSSDBDataSet.fptable1
        'Else
        '    FptableTableAdapter.Connection.ConnectionString = Common.ConnectionString
        '    Me.FptableTableAdapter.Fill(Me.SSSDBDataSet.fptable)
        '    GridControl2.DataSource = SSSDBDataSet.fptable
        'End If
        'GridView2.RefreshData()
        LoadFPGrid()
        Application.DoEvents()
        'For Each c As Control In Me.Controls
        '    c.Enabled = True
        'Next
        XtraMasterTest.LabelControlStatus.Text = ""
        Application.DoEvents()
        'XtraMessageBox.Show(ulf, "<size=10>Task Completed</size>", "<size=9>Information</size>")
        'CHCNetSDK.PostMessage(Nothing, 1104, 6, 0) ' all download done
        ' End If
    End Sub
    Private Function SendNextCard() As Boolean
        If (-1 = m_lSetCardCfgHandle) Then
            Return False
        End If
        m_dwSendIndex = (m_dwSendIndex + 1)
        If (m_dwSendIndex >= m_dwCardNum) Then
            'CHCNetSDK.NET_DVR_StopRemoteConfig(m_lSetCardCfgHandle);
            'this.StopRemoteCfg(ref m_lSetCardCfgHandle);
            'm_lSetCardCfgHandle = -1;
            Dim listItem2 As ListViewItem = New ListViewItem
            listItem2.Text = "SUCC"
            Dim strTemp2 As String = Nothing
            strTemp2 = String.Format("Send {0} card(s) over", m_dwCardNum)
            listItem2.SubItems.Add(strTemp2)
            'Me.AddList(listViewMessage, listItem2, True)
            Return True
        End If

        m_struNowSendCard = m_struCardInfo(m_dwSendIndex)
        Dim dwSize As UInteger = CType(Marshal.SizeOf(m_struNowSendCard), UInteger)
        Dim ptrSendCard As IntPtr = Marshal.AllocHGlobal(CType(dwSize, Integer))
        Marshal.StructureToPtr(m_struNowSendCard, ptrSendCard, False)
        If Not CHCNetSDK.NET_DVR_SendRemoteConfig(m_lSetCardCfgHandle, 3, ptrSendCard, dwSize) Then
            Marshal.FreeHGlobal(ptrSendCard)
            Dim listItem3 As ListViewItem = New ListViewItem
            listItem3.Text = "FAIL"
            Dim strTemp3 As String = Nothing
            strTemp3 = String.Format("Send Fail,CardNO:{0}", System.Text.Encoding.UTF8.GetString(m_struNowSendCard.byCardNo).TrimEnd(Microsoft.VisualBasic.ChrW(92)))
            listItem3.SubItems.Add(strTemp3)
            'Me.AddList(listViewMessage, listItem3, True)           
            Return False
        End If

        Marshal.FreeHGlobal(ptrSendCard)
        Return True
    End Function
    Private Sub GetTreeSel()
        Dim i As Integer = 0
        Do While (i < 512)
            If i = 0 Then
                m_struFingerPrintOne.byEnableCardReader(i) = 1
            Else
                m_struFingerPrintOne.byEnableCardReader(i) = 0
            End If
            i = (i + 1)
        Loop
        i = 0
        Do While (i < 256)
            m_struFingerPrintOne.byLeaderFP(i) = 0
            i = (i + 1)
        Loop
        i = 0
        Do While (i < 10)
            m_struDelFingerPrint.struByCard.byFingerPrintID(i) = 0
            i = (i + 1)
        Loop
    End Sub
    'Public Declare Sub CopyMemory Lib "kernel32" Alias "RtlMoveMemory" (Destination As Byte, Source As Byte, ByVal Length As Long)
    Public Declare Sub CopyMemory Lib "kernel32" Alias "RtlMoveMemory" (ByVal Destination As Long, ByVal Source As Long, ByVal Length As Long)
    Public Declare Sub ZeroMemory Lib "kernel32" Alias "RtlZeroMemory" (Destination As Byte, ByVal Length As Long)
    Public Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)
    Public Const gstrNoDevice = "No Device"

    Private Sub ConvFpDataToSaveInDbForCompatibility(ByRef abytSrc() As Byte, ByRef abytDest() As Byte)
        Dim nTempLen As Long, lenConvFpData As Long
        Dim bytConvFpData() As Byte
        Dim k As Int32, m As Int32
        Dim bytTemp() As Byte

        nTempLen = abytSrc.Length / 4
        lenConvFpData = nTempLen * 5
        ReDim bytConvFpData(lenConvFpData)

        For k = 0 To nTempLen - 1
            ReDim bytTemp(3)
            bytTemp(0) = abytSrc(k * 4)
            bytTemp(1) = abytSrc(k * 4 + 1)
            bytTemp(2) = abytSrc(k * 4 + 2)
            bytTemp(3) = abytSrc(k * 4 + 3)

            m = BitConverter.ToInt32(bytTemp, 0)
            bytConvFpData(k * 5) = 1
            If m < 0 Then
                If m = -2147483648 Then
                    bytConvFpData(k * 5) = 2
                    m = 2147483647
                Else
                    bytConvFpData(k * 5) = 0
                    m = -m
                End If
            End If
            bytTemp = BitConverter.GetBytes(m)
            bytConvFpData(k * 5 + 1) = bytTemp(3)
            bytConvFpData(k * 5 + 2) = bytTemp(2)
            bytConvFpData(k * 5 + 3) = bytTemp(1)
            bytConvFpData(k * 5 + 4) = bytTemp(0)
        Next

        abytDest = bytConvFpData
    End Sub
    Private Sub ToggleSwitch1_Toggled(sender As System.Object, e As System.EventArgs) Handles ToggleDownloadAll.Toggled
        If ToggleDownloadAll.IsOn = True Then
            LabelControl1.Visible = False
            TextSelectUser.Visible = False
        Else
            LabelControl1.Visible = True
            TextSelectUser.Visible = True
        End If
    End Sub
    Private Sub btnDeleteFrmDB_Click(sender As System.Object, e As System.EventArgs) Handles btnDeleteFrmDB.Click
        If GridView2.GetSelectedRows.Count = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>Please select User from Fring print table</size>", "<size=9>Error</size>")
            Exit Sub
        Else
            If TextPassword.Text.Trim = "" Then
                XtraMessageBox.Show(ulf, "<size=10>Please Enter Login Password</size>", "<size=9>Error</size>")
                TextPassword.Select()
                Exit Sub
            Else
                If TextPassword.Text.ToLower <> Common.PASSWORD.ToLower Then
                    XtraMessageBox.Show(ulf, "<size=10>Login Password Incorrect</size>", "<size=9>Error</size>")
                    TextPassword.Select()
                    Exit Sub
                End If
            End If
            If XtraMessageBox.Show(ulf, "<size=10>" & Common.res_man.GetString("askdelete", Common.cul) & "</size>", Common.res_man.GetString("confirmdeleteion", Common.cul), _
                              MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
                Me.Cursor = Cursors.WaitCursor
                For Each c As Control In Me.Controls
                    c.Enabled = False
                Next
                If Common.servername = "Access" Then
                    If Common.con1.State <> ConnectionState.Open Then
                        Common.con1.Open()
                    End If
                Else
                    If Common.con.State <> ConnectionState.Open Then
                        Common.con.Open()
                    End If
                End If
                Dim sSql As String = ""
                Dim selectedRows As Integer() = GridView2.GetSelectedRows()
                Dim result As Object() = New Object(selectedRows.Length - 1) {}
                Dim FingerNumber As String
                Dim EnrollNumber As String
                For i As Integer = 0 To selectedRows.Length - 1
                    Dim rowHandle As Integer = selectedRows(i)
                    If Not GridView2.IsGroupRow(rowHandle) Then
                        EnrollNumber = GridView2.GetRowCellValue(rowHandle, "EnrollNumber").ToString.Trim

                        XtraMasterTest.LabelControlStatus.Text = "Deleting Template " & EnrollNumber
                        Application.DoEvents()

                        FingerNumber = GridView2.GetRowCellValue(rowHandle, "FingerNumber").ToString.Trim
                        sSql = "delete from fptable where EnrollNumber='" & EnrollNumber & "' and FingerNumber='" & FingerNumber & "'"
                        If Common.servername = "Access" Then
                            sSql = "delete from fptable where EnrollNumber='" & EnrollNumber & "' and FingerNumber=" & FingerNumber & ""
                            cmd1 = New OleDbCommand(sSql, Common.con1)
                            cmd1.ExecuteNonQuery()
                        Else
                            cmd = New SqlCommand(sSql, Common.con)
                            cmd.ExecuteNonQuery()
                        End If
                    End If
                Next
                If Common.servername = "Access" Then
                    If Common.con1.State <> ConnectionState.Closed Then
                        Common.con1.Close()
                    End If
                Else
                    If Common.con.State <> ConnectionState.Closed Then
                        Common.con.Close()
                    End If
                End If
                For Each c As Control In Me.Controls
                    c.Enabled = True
                Next
                Me.Cursor = Cursors.Default
                XtraMasterTest.LabelControlStatus.Text = ""
                Application.DoEvents()
                'If Common.servername = "Access" Then
                '    Me.Fptable1TableAdapter1.Fill(Me.SSSDBDataSet.fptable1)
                '    GridControl2.DataSource = SSSDBDataSet.fptable1
                'Else
                '    FptableTableAdapter.Connection.ConnectionString = Common.ConnectionString
                '    Me.FptableTableAdapter.Fill(Me.SSSDBDataSet.fptable)
                '    GridControl2.DataSource = SSSDBDataSet.fptable
                'End If
                LoadFPGrid()
                TextPassword.Text = ""
                XtraMessageBox.Show(ulf, "<size=10>Task Completed</size>", "<size=9>Information</size>")
            End If
        End If
    End Sub
    Private Sub btnUploadFinger_Click(sender As System.Object, e As System.EventArgs) Handles btnUploadFinger.Click
        If GridView1.GetSelectedRows.Count = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>Please select the Machine</size>", "<size=9>Error</size>")
            Exit Sub
        End If

        If GridView2.GetSelectedRows.Count = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>Please select Employee</size>", "<size=9>Error</size>")
            Exit Sub
        End If
        Dim sSql As String = ""
        'Me.Cursor = Cursors.WaitCursor

        'For Each c As Control In Me.Controls
        '    c.Enabled = False
        'Next

        Dim selectedRows As Integer() = GridView1.GetSelectedRows()
        Dim result As Object() = New Object(selectedRows.Length - 1) {}
        Dim LstMachineId As String
        For i = 0 To selectedRows.Length - 1  'for device
            Dim rowHandle As Integer = selectedRows(i)
            If Not GridView1.IsGroupRow(rowHandle) Then
                LstMachineId = GridView1.GetRowCellValue(rowHandle, "ID_NO").ToString.Trim
                If GridView1.GetRowCellValue(rowHandle, "DeviceType") = "Bio-1/3/4/5/6/7/ATF-203/ATF-395/ACR-872" Then

                ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "ZK(TFT)" Or GridView1.GetRowCellValue(rowHandle, "DeviceType") = "Bio-1Pro/ATF305Pro/ATF686Pro" Then

                ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "HKSeries" Then
                    Dim cn As Common = New Common
                    Dim struDeviceInfoV40 As CHCNetSDK.NET_DVR_DEVICEINFO_V40 = New CHCNetSDK.NET_DVR_DEVICEINFO_V40
                    Dim DeviceAdd As String = GridView1.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim, userName As String = Trim(GridView1.GetRowCellValue(rowHandle, "HLogin").ToString.Trim), pwd As String = Trim(GridView1.GetRowCellValue(rowHandle, "HPassword").ToString.Trim)
                    Dim lUserID As Integer = -1
                    Dim failReason As String = ""
                    Dim logistatus = cn.HikvisionLogin(DeviceAdd, userName, pwd, struDeviceInfoV40, lUserID, failReason)
                    If logistatus = False Then
                        If failReason <> "" Then
                            XtraMessageBox.Show(ulf, "<size=10>" & failReason & "</size>", "Failed")
                            XtraMasterTest.LabelControlStatus.Text = ""
                            Application.DoEvents()
                            Continue For
                        End If

                    Else
                        If failReason <> "" Then
                            XtraMessageBox.Show(ulf, "<size=10>" & failReason & "</size>", "Failed")
                            XtraMasterTest.LabelControlStatus.Text = ""
                            Application.DoEvents()
                            logistatus = cn.HikvisionLogOut(lUserID)
                            Continue For
                        End If


                        XtraMasterTest.LabelControlStatus.Text = ""
                        Application.DoEvents()

                        If (-1 <> m_lSetCardCfgHandle) Then
                            If CHCNetSDK.NET_DVR_StopRemoteConfig(m_lSetCardCfgHandle) Then
                                m_lSetCardCfgHandle = -1
                            End If
                        End If
                        m_bSendOne = True
                        'loop for fingerprint
                        Dim selectedRowsEmp As Integer() = GridView2.GetSelectedRows()
                        Dim resultEmp As Object() = New Object(selectedRowsEmp.Length - 1) {}
                        'If axCZKEM1.BeginBatchUpdate(iMachineNumber, 1) Then
                        For x As Integer = 0 To selectedRowsEmp.Length - 1  'for user
                            'LstEmployeesTarget.ListIndex = e
                            Dim rowHandleEmp As Long = selectedRowsEmp(x)
                            If Not GridView2.IsGroupRow(rowHandleEmp) Then
                                Dim RsFp As DataSet = New DataSet
                                Dim mUser_ID As String = GridView2.GetRowCellValue(rowHandleEmp, "EnrollNumber").ToString.Trim()
                                Dim tmpUserId As String
                                If IsNumeric(mUser_ID) Then
                                    tmpUserId = Convert.ToDouble(mUser_ID).ToString("000000000000")
                                Else
                                    tmpUserId = mUser_ID
                                End If
                                XtraMasterTest.LabelControlStatus.Text = "Uploading User " & mUser_ID
                                Application.DoEvents()

                                'If GridView2.GetRowCellValue(rowHandleEmp, "FingerNumber").ToString.Trim() = "11" Then
                                'for user copy only
                                Dim s As String = "select EMPNAME, ValidityStartdate, ValidityEnddate from TblEmployee where PRESENTCARDNO = '" & tmpUserId & "'"
                                Dim ValidityStartdate As DateTime
                                Dim ValidityEnddate As DateTime
                                Dim adapZ As SqlDataAdapter
                                Dim adapAZ As OleDbDataAdapter
                                Dim dstmp As DataSet = New DataSet
                                If Common.servername = "Access" Then
                                    adapAZ = New OleDbDataAdapter(s, Common.con1)
                                    adapAZ.Fill(dstmp)
                                Else
                                    adapZ = New SqlDataAdapter(s, Common.con)
                                    adapZ.Fill(dstmp)
                                End If
                                Dim sName As String
                                Dim FpNo As String = GridView2.GetRowCellValue(rowHandleEmp, "FingerNumber").ToString.Trim()
                                Dim Privilege As Integer = 0
                                Dim Cardnumber As String = GridView2.GetRowCellValue(rowHandleEmp, "Cardnumber").ToString.Trim()
                                Dim Password As String = GridView2.GetRowCellValue(rowHandleEmp, "Password").ToString.Trim()
                                Try
                                    Privilege = GridView2.GetRowCellValue(rowHandleEmp, "Privilege")
                                Catch ex As Exception
                                End Try

                                Dim sdwEnrollNumber As String
                                If IsNumeric(mUser_ID) = True Then
                                    sdwEnrollNumber = Convert.ToDouble(mUser_ID)
                                Else
                                    sdwEnrollNumber = mUser_ID
                                End If
                                If dstmp.Tables(0).Rows.Count > 0 Then
                                    sName = dstmp.Tables(0).Rows(0)("EMPNAME").ToString().Trim()
                                    ValidityStartdate = Convert.ToDateTime(dstmp.Tables(0).Rows(0)("ValidityStartdate").ToString().Trim())
                                    ValidityEnddate = Convert.ToDateTime(dstmp.Tables(0).Rows(0)("ValidityEnddate").ToString().Trim())
                                Else
                                    sName = GridView2.GetRowCellValue(rowHandleEmp, "UserName").ToString.Trim()
                                    ValidityStartdate = Convert.ToDateTime("2018-01-01")
                                    ValidityEnddate = Convert.ToDateTime("2030-12-31")
                                End If

                                If (m_lSetUserCfgHandle <> -1) Then
                                    If CHCNetSDK.NET_DVR_StopRemoteConfig(m_lSetUserCfgHandle) Then
                                        m_lSetUserCfgHandle = -1
                                    End If
                                End If
                                'Dim sURL As String = "PUT /ISAPI/AccessControl/UserInfo/SetUp?format=json"
                                Dim sURL As String = "PUT /ISAPI/AccessControl/UserInfo/SetUp?format=json"
                                Dim ptrURL As IntPtr = Marshal.StringToHGlobalAnsi(sURL)
                                m_lSetUserCfgHandle = CHCNetSDK.NET_DVR_StartRemoteConfig(lUserID, CHCNetSDK.NET_DVR_JSON_CONFIG, ptrURL, sURL.Length, Nothing, IntPtr.Zero)
                                If (m_lSetUserCfgHandle < 0) Then
                                    'MessageBox.Show(("NET_DVR_StartRemoteConfig fail [url:PUT /ISAPI/AccessControl/UserInfo/SetUp?format=json] error:" + CHCNetSDK.NET_DVR_GetLastError))
                                    Marshal.FreeHGlobal(ptrURL)
                                    Continue For
                                Else
                                    SendUserInfo(sdwEnrollNumber, sName, ValidityStartdate, ValidityEnddate, Privilege, Cardnumber, Password)
                                    Marshal.FreeHGlobal(ptrURL)
                                End If



                                ''all values in m_struSelSendCardCfg
                                'Dim MachineCard As String = GridView2.GetRowCellValue(rowHandleEmp, "Cardnumber").ToString().Trim()
                                'm_struSelSendCardCfg = GetSelItem(rowHandleEmp, ValidityStartdate, ValidityEnddate, sName, MachineCard)


                                'Dim struCond As CHCNetSDK.NET_DVR_CARD_CFG_COND = New CHCNetSDK.NET_DVR_CARD_CFG_COND
                                'struCond.dwSize = CType(Marshal.SizeOf(struCond), UInteger)
                                'struCond.dwCardNum = 1
                                'struCond.wLocalControllerID = 0
                                'struCond.byCheckCardNo = 1
                                'm_BSendSel = True
                                'Dim dwSize As Integer = Marshal.SizeOf(struCond)
                                'Dim ptrStruCond As IntPtr = Marshal.AllocHGlobal(dwSize)
                                'Marshal.StructureToPtr(struCond, ptrStruCond, False)
                                'g_fSetGatewayCardCallback = New CHCNetSDK.RemoteConfigCallback(AddressOf ProcessSetGatewayCardCallback)
                                'Thread.Sleep(200)
                                'm_lSetCardCfgHandle = CHCNetSDK.NET_DVR_StartRemoteConfig(lUserID, CHCNetSDK.NET_DVR_SET_CARD_CFG_V50, ptrStruCond, dwSize, g_fSetGatewayCardCallback, Me.Handle)
                                'Thread.Sleep(200)
                                'If (-1 = m_lSetCardCfgHandle) Then
                                '    Dim listItem As ListViewItem = New ListViewItem
                                '    listItem.Text = "FAIL"
                                '    Dim strTemp As String = String.Format("NET_DVR_SET_CARD_CFG_V50 FAIL, ERROR CODE {0}", CHCNetSDK.NET_DVR_GetLastError)
                                '    listItem.SubItems.Add(strTemp)
                                '    'Me.AddList(listViewMessage, listItem, True)
                                '    Marshal.FreeHGlobal(ptrStruCond)
                                '    'Return
                                '    Continue For
                                'Else
                                '    Dim listItem As ListViewItem = New ListViewItem
                                '    listItem.Text = "SUCC"
                                '    listItem.SubItems.Add("NET_DVR_SET_CARD_CFG_V50")
                                '    'Me.AddList(listViewMessage, listItem, True)
                                'End If
                                'Marshal.FreeHGlobal(ptrStruCond)


                                'If m_BSendSel Then
                                '    m_dwCardNum = 1
                                '    m_dwSendIndex = 0
                                '    SendCardData(m_struSelSendCardCfg)

                                '    m_BSendSel = False
                                '    m_struSelSendCardCfg = New CHCNetSDK.NET_DVR_CARD_CFG_V50
                                '    'Return
                                '    Continue For
                                'End If
                                'End If
                            End If
                        Next


                        For x As Integer = 0 To selectedRowsEmp.Length - 1  'for Card
                            Dim rowHandleEmp As Long = selectedRowsEmp(x)
                            If Not GridView2.IsGroupRow(rowHandleEmp) Then
                                Dim RsFp As DataSet = New DataSet
                                Dim mUser_ID As String = GridView2.GetRowCellValue(rowHandleEmp, "EnrollNumber").ToString.Trim()
                                Dim cardNo As String = GridView2.GetRowCellValue(rowHandleEmp, "Cardnumber").ToString.Trim()
                                XtraMasterTest.LabelControlStatus.Text = "Uploading Card " & mUser_ID
                                Application.DoEvents()
                                If GridView2.GetRowCellValue(rowHandleEmp, "FingerNumber").ToString.Trim() = "11" Then
                                    If m_lSetCardCfgHandle <> -1 Then

                                        If CHCNetSDK.NET_DVR_StopRemoteConfig(m_lSetCardCfgHandle) Then
                                            m_lSetCardCfgHandle = -1
                                        End If
                                    End If

                                    Dim sURL As String = "PUT /ISAPI/AccessControl/CardInfo/SetUp?format=json"
                                    Dim ptrURL As IntPtr = Marshal.StringToHGlobalAnsi(sURL)
                                    m_lSetCardCfgHandle = CHCNetSDK.NET_DVR_StartRemoteConfig(lUserID, CHCNetSDK.NET_DVR_JSON_CONFIG, ptrURL, sURL.Length, Nothing, IntPtr.Zero)

                                    If m_lSetCardCfgHandle < 0 Then
                                        'MessageBox.Show("NET_DVR_StartRemoteConfig fail [url:PUT /ISAPI/AccessControl/CardInfo/SetUp?format=json] error:" & CHCNetSDK.NET_DVR_GetLastError())
                                        Marshal.FreeHGlobal(ptrURL)
                                        Return
                                    Else
                                        SendCardData(mUser_ID, cardNo)
                                        Marshal.FreeHGlobal(ptrURL)
                                    End If

                                End If
                            End If
                        Next

                        For x As Integer = 0 To selectedRowsEmp.Length - 1  'for fp template
                            'LstEmployeesTarget.ListIndex = e
                            Dim rowHandleEmp As Long = selectedRowsEmp(x)
                            If Not GridView2.IsGroupRow(rowHandleEmp) Then
                                Dim RsFp As DataSet = New DataSet
                                Dim mUser_ID As String = GridView2.GetRowCellValue(rowHandleEmp, "EnrollNumber").ToString.Trim()
                                XtraMasterTest.LabelControlStatus.Text = "Uploading Template " & mUser_ID & " In " & LstMachineId
                                Application.DoEvents()
                                If GridView2.GetRowCellValue(rowHandleEmp, "FingerNumber").ToString.Trim() < "11" And GridView2.GetRowCellValue(rowHandleEmp, "FingerNumber").ToString.Trim() >= "0" Then
                                    'for FP

                                    'new
                                    If m_lSetFingerPrintCfgHandle <> -1 Then
                                        CHCNetSDK.NET_DVR_StopRemoteConfig(CInt(m_lSetFingerPrintCfgHandle))
                                        m_lSetFingerPrintCfgHandle = -1
                                    End If
                                    If IsNumeric(mUser_ID) = True Then
                                        mUser_ID = Convert.ToDouble(mUser_ID)
                                    Else
                                        mUser_ID = mUser_ID
                                    End If
                                    'Dim sURL As String = "POST /ISAPI/AccessControl/FingerPrint/SetUp?format=json"
                                    Dim sURL As String = "POST /ISAPI/AccessControl/FingerPrintDownload?format=json"
                                    Dim ptrURL As IntPtr = Marshal.StringToHGlobalAnsi(sURL)
                                    m_lSetFingerPrintCfgHandle = CHCNetSDK.NET_DVR_StartRemoteConfig(lUserID, CHCNetSDK.NET_DVR_JSON_CONFIG, ptrURL, sURL.Length, Nothing, IntPtr.Zero)

                                    If m_lSetFingerPrintCfgHandle < 0 Then
                                        'MessageBox.Show("NET_DVR_StartRemoteConfig fail [url:POST /ISAPI/AccessControl/FingerPrint/SetUp?format=json] error:" & CHCNetSDK.NET_DVR_GetLastError())
                                        Marshal.FreeHGlobal(ptrURL)
                                        Return
                                    Else
                                        Dim JsonFingerPrintCfg As CFingerPrintCfgCfg = New CFingerPrintCfgCfg()
                                        JsonFingerPrintCfg.FingerPrintCfg = New CFingerPrintCfg()
                                        JsonFingerPrintCfg.FingerPrintCfg.employeeNo = mUser_ID
                                        JsonFingerPrintCfg.FingerPrintCfg.enableCardReader = New Integer(0) {}
                                        JsonFingerPrintCfg.FingerPrintCfg.enableCardReader(0) = 1
                                        JsonFingerPrintCfg.FingerPrintCfg.fingerPrintID = 1
                                        JsonFingerPrintCfg.FingerPrintCfg.fingerType = "normalFP"
                                        'Dim strPath As String = textBoxFingerData.Text

                                        'If Not File.Exists(strPath) Then
                                        '    MessageBox.Show("not find FingerData")
                                        '    Marshal.FreeHGlobal(ptrURL)
                                        '    Return
                                        'End If

                                        'Dim fs As FileStream = File.OpenRead(strPath)
                                        'Dim filelen As Integer = 0
                                        'filelen = CInt(fs.Length)
                                        'Dim byteFp As Byte() = New Byte(filelen - 1) {}
                                        'fs.Read(byteFp, 0, filelen)
                                        'fs.Close()
                                        Dim sFpData As String = Nothing
                                        If GridView2.GetRowCellValue(rowHandleEmp, "Template_Tw").ToString.Trim = "" Then
                                            Continue For
                                        End If
                                        Dim byteFp() As Byte = GridView2.GetRowCellValue(rowHandleEmp, "Template_Tw")
                                        Try
                                            sFpData = Convert.ToBase64String(byteFp)
                                        Catch
                                            sFpData = Nothing
                                        End Try

                                        JsonFingerPrintCfg.FingerPrintCfg.fingerData = sFpData
                                        Dim strJsonFingerPrintCfg As String = JsonConvert.SerializeObject(JsonFingerPrintCfg, Formatting.Indented, New JsonSerializerSettings With {
                                            .DefaultValueHandling = DefaultValueHandling.Ignore
                                        })
                                        Dim ptrJsonFingerPrintCfg As IntPtr = Marshal.StringToHGlobalAnsi(strJsonFingerPrintCfg)
                                        Dim ptrJsonData As IntPtr = Marshal.AllocHGlobal(1024)

                                        For j As Integer = 0 To 1024 - 1
                                            Marshal.WriteByte(ptrJsonData, j, 0)
                                        Next

                                        Dim dwState As Integer = 0
                                        Dim dwReturned As UInteger = 0

                                        While True
                                            dwState = CHCNetSDK.NET_DVR_SendWithRecvRemoteConfig(m_lSetFingerPrintCfgHandle, ptrJsonFingerPrintCfg, CUInt(strJsonFingerPrintCfg.Length), ptrJsonData, 1024, dwReturned)
                                            Dim strJsonData As String = Marshal.PtrToStringAnsi(ptrJsonData)

                                            If dwState = CInt(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_NEEDWAIT) Then
                                                Thread.Sleep(10)
                                                Continue While
                                            ElseIf dwState = CInt(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_FAILED) Then
                                                'MessageBox.Show("Set Card Fail error:" & CHCNetSDK.NET_DVR_GetLastError())
                                                Exit While
                                            ElseIf dwState = CInt(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_SUCCESS) Then
                                                Dim JsonFingerPrintStatusCfg As CFingerPrintStatusCfg = New CFingerPrintStatusCfg()
                                                JsonFingerPrintStatusCfg = JsonConvert.DeserializeObject(Of CFingerPrintStatusCfg)(strJsonData)

                                                If JsonFingerPrintStatusCfg.FingerPrintStatus Is Nothing Then
                                                    Dim JsonResponseStatus As CResponseStatus = New CResponseStatus()
                                                    JsonResponseStatus = JsonConvert.DeserializeObject(Of CResponseStatus)(strJsonData)

                                                    If JsonResponseStatus.statusCode = 1 Then
                                                        'MessageBox.Show("Set FingerPrint Success")
                                                    Else
                                                        'MessageBox.Show("Set FingerPrint Fail, ResponseStatus.statusCode:" & JsonResponseStatus.statusCode)
                                                    End If
                                                Else
                                                    For j As Integer = 0 To JsonFingerPrintStatusCfg.FingerPrintStatus.StatusList.Count - 1
                                                        If JsonFingerPrintStatusCfg.FingerPrintStatus.StatusList(j).cardReaderRecvStatus = 1 Then
                                                            'MessageBox.Show("Set FingerPrint Success")
                                                        Else
                                                            'MessageBox.Show("Set FingerPrint Fail, cardReaderRecvStatus:" & JsonFingerPrintStatusCfg.FingerPrintStatus.StatusList(i).cardReaderRecvStatus)
                                                        End If
                                                        Exit For
                                                    Next
                                                End If
                                                Exit While
                                            ElseIf dwState = CInt(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_EXCEPTION) Then
                                                'MessageBox.Show("Set FingerPrint Exception error:" & CHCNetSDK.NET_DVR_GetLastError())
                                                Exit While
                                            Else
                                                'MessageBox.Show("unknown Status error:" & CHCNetSDK.NET_DVR_GetLastError())
                                                Exit While
                                            End If
                                        End While

                                        If m_lSetFingerPrintCfgHandle <> -1 Then
                                            CHCNetSDK.NET_DVR_StopRemoteConfig(CInt(m_lSetFingerPrintCfgHandle))
                                            m_lSetFingerPrintCfgHandle = -1
                                        End If

                                        Marshal.FreeHGlobal(ptrJsonFingerPrintCfg)
                                        Marshal.FreeHGlobal(ptrJsonData)
                                        Marshal.FreeHGlobal(ptrURL)
                                    End If
                                    'end new




                                    'Dim struCond As CHCNetSDK.NET_DVR_FINGER_PRINT_INFO_COND_V50 = New CHCNetSDK.NET_DVR_FINGER_PRINT_INFO_COND_V50
                                    'struCond.byCardNo = New Byte((32) - 1) {}
                                    'struCond.byEmployeeNo = New Byte((32) - 1) {}
                                    'struCond.byEnableCardReader = New Byte((512) - 1) {}
                                    'struCond.dwSize = CType(Marshal.SizeOf(struCond), UInteger)
                                    'struCond.dwFingerPrintNum = 1
                                    'struCond.byCallbackMode = 0
                                    'Dim dwSize As Integer = Marshal.SizeOf(struCond)
                                    'Dim ptrStruCond As IntPtr = Marshal.AllocHGlobal(dwSize)

                                    'Dim byTempEmployeeNo() As Byte = System.Text.Encoding.UTF8.GetBytes("")
                                    'Dim y As Integer = 0
                                    'Do While (y < byTempEmployeeNo.Length)
                                    '    struCond.byEmployeeNo(y) = byTempEmployeeNo(y)
                                    '    y = (y + 1)
                                    'Loop
                                    'struCond.byFingerPrintID = 1
                                    'If IsNumeric(mUser_ID) = True Then
                                    '    mUser_ID = Convert.ToDouble(mUser_ID)
                                    'Else
                                    '    mUser_ID = mUser_ID
                                    'End If
                                    'Dim byTemp() As Byte = System.Text.Encoding.UTF8.GetBytes(mUser_ID)
                                    'y = 0
                                    'Do While (y < byTemp.Length)
                                    '    struCond.byCardNo(y) = byTemp(y)
                                    '    y = (y + 1)
                                    'Loop
                                    'm_struFingerPrintOne.byEnableCardReader(0) = 1
                                    'm_struFingerPrintOne.byLeaderFP(0) = 1
                                    'struCond.byEnableCardReader = m_struFingerPrintOne.byEnableCardReader
                                    'Marshal.StructureToPtr(struCond, ptrStruCond, False)
                                    'g_fSetFingerPrintCallback = New CHCNetSDK.RemoteConfigCallback(AddressOf ProcessSetFingerPrintCfgCallbackData)
                                    'Thread.Sleep(200)
                                    'm_lSetFingerPrintCfgHandle = CHCNetSDK.NET_DVR_StartRemoteConfig(lUserID, CHCNetSDK.NET_DVR_SET_FINGERPRINT_CFG_V50, ptrStruCond, dwSize, g_fSetFingerPrintCallback, Me.Handle)
                                    'Thread.Sleep(200)
                                    'If (-1 = m_lSetFingerPrintCfgHandle) Then
                                    '    Dim listItem As ListViewItem = New ListViewItem
                                    '    listItem.Text = "FAIL"
                                    '    Dim strTemp As String = String.Format("NET_DVR_SET_FINGERPRINT_CFG_V50 FAIL, ERROR CODE {0}", CHCNetSDK.NET_DVR_GetLastError)
                                    '    listItem.SubItems.Add(strTemp)
                                    '    'Me.AddList(listViewMessage, listItem)
                                    '    Marshal.FreeHGlobal(ptrStruCond)
                                    '    Return
                                    'Else
                                    '    Dim listItem As ListViewItem = New ListViewItem
                                    '    listItem.Text = "SUCC"
                                    '    listItem.SubItems.Add("NET_DVR_SET_FINGERPRINT_CFG_V50")
                                    '    'Me.AddList(listViewMessage, listItem)
                                    'End If
                                    'm_iSendIndex = 0
                                    ''If Not SendFirstCard Then
                                    'Dim struFingerPrintCfg As CHCNetSDK.NET_DVR_FINGER_PRINT_CFG_V50 = New CHCNetSDK.NET_DVR_FINGER_PRINT_CFG_V50
                                    'struFingerPrintCfg.byCardNo = New Byte((32) - 1) {}
                                    'struFingerPrintCfg.byEmployeeNo = New Byte((32) - 1) {}
                                    'struFingerPrintCfg.byEnableCardReader = New Byte((512) - 1) {}
                                    'struFingerPrintCfg.byLeaderFP = New Byte((256) - 1) {}
                                    'struFingerPrintCfg.byFingerData = New Byte((CHCNetSDK.MAX_FINGER_PRINT_LEN) - 1) {}
                                    ''Dim cardNumber As String
                                    'Dim FpNo As String = GridView2.GetRowCellValue(rowHandleEmp, "FingerNumber").ToString.Trim()
                                    'Dim FPath As String = GridView2.GetRowCellValue(rowHandleEmp, "HTemplatePath").ToString.Trim()

                                    'If FPath = "" Then
                                    '    Continue For
                                    'End If

                                    ''If IsNumeric(mUser_ID) = True Then
                                    ''    cardNumber = Convert.ToDouble(mUser_ID)
                                    ''Else
                                    ''    cardNumber = mUser_ID
                                    ''End If
                                    'UpdateFingerPrintCfg(struFingerPrintCfg, mUser_ID, FpNo, FPath)

                                    'Dim dwSize1 As UInteger = CType(Marshal.SizeOf(struFingerPrintCfg), UInteger)
                                    'Dim ptrNowSendCard As IntPtr = Marshal.AllocHGlobal(CType(dwSize1, Integer))
                                    'Marshal.StructureToPtr(struFingerPrintCfg, ptrNowSendCard, False)
                                    'If Not CHCNetSDK.NET_DVR_SendRemoteConfig(m_lSetFingerPrintCfgHandle, 3, ptrNowSendCard, dwSize1) Then
                                    '    Dim listItem As ListViewItem = New ListViewItem
                                    '    listItem.Text = "FAIL"
                                    '    Dim strTemp As String = String.Format("Send Fail,CardNo:{0},EmployeeNo:{1}", System.Text.Encoding.UTF8.GetString(struFingerPrintCfg.byCardNo).TrimEnd(Microsoft.VisualBasic.ChrW(92)), System.Text.Encoding.UTF8.GetString(m_struRecordCardCfg(m_iSendIndex).byEmployeeNo).TrimEnd(Microsoft.VisualBasic.ChrW(92)))
                                    '    listItem.SubItems.Add(strTemp)
                                    '    'Me.AddList(listViewMessage, listItem)
                                    '    Marshal.FreeHGlobal(ptrNowSendCard)
                                    '    'Return False
                                    'End If

                                    'Marshal.FreeHGlobal(ptrNowSendCard)
                                    ''End If
                                    'Marshal.FreeHGlobal(ptrStruCond)
                                End If
                            End If
                        Next
                        For x As Integer = 0 To selectedRowsEmp.Length - 1  'for face
                            Dim rowHandleEmp As Long = selectedRowsEmp(x)
                            If Not GridView2.IsGroupRow(rowHandleEmp) Then
                                Dim RsFp As DataSet = New DataSet
                                Dim mUser_ID As String = GridView2.GetRowCellValue(rowHandleEmp, "EnrollNumber").ToString.Trim()
                                XtraMasterTest.LabelControlStatus.Text = "Uploading Face Template " & mUser_ID
                                Application.DoEvents()
                                If GridView2.GetRowCellValue(rowHandleEmp, "FingerNumber").ToString.Trim() = "50" Then
                                    'for Face
                                    'Dim FPath As String = GridView2.GetRowCellValue(rowHandleEmp, "HTemplatePath").ToString.Trim()
                                    'Dim FTemlate As String = GridView2.GetRowCellValue(rowHandleEmp, "Template").ToString.Trim()
                                    'If FPath = "" Then
                                    '    Continue For
                                    'Else
                                    Dim sURL As String = "PUT /ISAPI/Intelligent/FDLib/FDSetUp?format=json"
                                    Dim ptrURL As IntPtr = Marshal.StringToHGlobalAnsi(sURL)
                                    m_lSetFaceCfgHandle = CHCNetSDK.NET_DVR_StartRemoteConfig(lUserID, CHCNetSDK.NET_DVR_FACE_DATA_RECORD, ptrURL, sURL.Length, Nothing, IntPtr.Zero)
                                    If (m_lSetFaceCfgHandle = -1) Then
                                        Marshal.FreeHGlobal(ptrURL)
                                        'MessageBox.Show(("NET_DVR_StartRemoteConfig fail [url:PUT /ISAPI/Intelligent/FDLib/FDSetUp?format=json] error:" + CHCNetSDK.NET_DVR_GetLastError))
                                        Return
                                    End If

                                    Marshal.FreeHGlobal(ptrURL)
                                    Dim JsonSetFaceDataCond As CSetFaceDataCond = New CSetFaceDataCond
                                    JsonSetFaceDataCond.faceLibType = "blackFD"
                                    JsonSetFaceDataCond.FDID = "1"
                                    JsonSetFaceDataCond.FPID = mUser_ID
                                    Dim strJsonSearchFaceDataCond As String = JsonConvert.SerializeObject(JsonSetFaceDataCond, Formatting.Indented, New JsonSerializerSettings With {.DefaultValueHandling = DefaultValueHandling.Ignore})
                                    Dim ptrJsonSearchFaceDataCond As IntPtr = Marshal.StringToHGlobalAnsi(strJsonSearchFaceDataCond)
                                    Dim struJsonDataCfg As CHCNetSDK.NET_DVR_JSON_DATA_CFG = New CHCNetSDK.NET_DVR_JSON_DATA_CFG
                                    struJsonDataCfg.dwSize = CType(Marshal.SizeOf(struJsonDataCfg), UInteger)
                                    struJsonDataCfg.lpJsonData = ptrJsonSearchFaceDataCond
                                    struJsonDataCfg.dwJsonDataSize = CType(strJsonSearchFaceDataCond.Length, UInteger)

                                    'Dim fs As FileStream = New FileStream(FPath, FileMode.OpenOrCreate)
                                    'If (0 = fs.Length) Then
                                    '    'MessageBox.Show("The picture is 0k,please input another picture!")
                                    '    Marshal.FreeHGlobal(ptrJsonSearchFaceDataCond)
                                    '    fs.Close()
                                    '    Return
                                    'End If

                                    'If ((200 * 1024) _
                                    '            < fs.Length) Then
                                    '    'MessageBox.Show("The picture is larger than 200k,please input another picture!")
                                    '    Marshal.FreeHGlobal(ptrJsonSearchFaceDataCond)
                                    '    fs.Close()
                                    '    Return
                                    'End If



                                    'struJsonDataCfg.dwPicDataSize = CType(fs.Length, UInteger)
                                    'Dim iLen As Integer = CType(struJsonDataCfg.dwPicDataSize, Integer)
                                    'Dim by() As Byte = New Byte((iLen) - 1) {}
                                    'struJsonDataCfg.lpPicData = Marshal.AllocHGlobal(iLen)
                                    'fs.Read(by, 0, iLen)
                                    'Marshal.Copy(by, 0, struJsonDataCfg.lpPicData, iLen)
                                    'fs.Close()



                                    'from template string 
                                    'If (0 = FTemlate.Length) Then
                                    '    'MessageBox.Show("The picture is 0k,please input another picture!")
                                    '    Marshal.FreeHGlobal(ptrJsonSearchFaceDataCond)
                                    '    Continue For
                                    'End If

                                    'Dim by() As Byte = GridView2.GetRowCellValue(rowHandleEmp, "Template_Face") 'System.Text.Encoding.ASCII.GetBytes(FTemlate)
                                    'struJsonDataCfg.dwPicDataSize = CType(by.Length, UInteger)
                                    'Dim iLen As Integer = CType(struJsonDataCfg.dwPicDataSize, Integer)
                                    ''Dim by() As Byte = New Byte((iLen) - 1) {}
                                    'struJsonDataCfg.lpPicData = Marshal.AllocHGlobal(iLen)
                                    ''fs.Read(by, 0, iLen)
                                    'Marshal.Copy(by, 0, struJsonDataCfg.lpPicData, iLen)

                                    Try
                                        struJsonDataCfg.dwPicDataSize = CType(GridView2.GetRowCellValue(rowHandleEmp, "Template_Face").Length, UInteger)
                                        Dim iLen As Integer = CType(struJsonDataCfg.dwPicDataSize, Integer)
                                        Dim by() As Byte = GridView2.GetRowCellValue(rowHandleEmp, "Template_Face") ' New Byte((iLen) - 1) {}
                                        struJsonDataCfg.lpPicData = Marshal.AllocHGlobal(iLen)
                                        'fs.Read(by, 0, iLen)
                                        Marshal.Copy(by, 0, struJsonDataCfg.lpPicData, iLen)

                                        'end from template string 

                                        'MsgBox(by.Length.ToString)
                                        Dim ptrJsonDataCfg As IntPtr = Marshal.AllocHGlobal(CType(struJsonDataCfg.dwSize, Integer))
                                        Marshal.StructureToPtr(struJsonDataCfg, ptrJsonDataCfg, False)
                                        Dim ptrJsonResponseStatus As IntPtr = Marshal.AllocHGlobal(1024)
                                        Dim m As Integer = 0
                                        Do While (m < 1024)
                                            Marshal.WriteByte(ptrJsonResponseStatus, m, 0)
                                            m = (m + 1)
                                        Loop

                                        Dim dwState As Integer = CType(CHCNetSDK.NET_SDK_GET_NEXT_STATUS_SUCCESS, Integer)
                                        Dim dwReturned As UInteger = 0

                                        While True
                                            dwState = CHCNetSDK.NET_DVR_SendWithRecvRemoteConfig(m_lSetFaceCfgHandle, ptrJsonDataCfg, struJsonDataCfg.dwSize, ptrJsonResponseStatus, 1024, dwReturned)
                                            'MsgBox("dwState " & dwState)
                                            Dim strResponseStatus As String = Marshal.PtrToStringAnsi(ptrJsonResponseStatus)
                                            If (dwState = CType(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_NEEDWAIT, Integer)) Then
                                                Thread.Sleep(10)
                                                'TODO: Warning!!! continue If
                                            ElseIf (dwState = CType(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_FAILED, Integer)) Then
                                                'MessageBox.Show(("Set Face Error:" + CHCNetSDK.NET_DVR_GetLastError))
                                                Exit While
                                            ElseIf (dwState = CType(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_SUCCESS, Integer)) Then
                                                Dim JsonResponseStatus As CResponseStatus = New CResponseStatus
                                                JsonResponseStatus = JsonConvert.DeserializeObject(Of CResponseStatus)(strResponseStatus)
                                                If (JsonResponseStatus.statusCode = 1) Then
                                                    'MessageBox.Show("Set Face Success")
                                                Else
                                                    'MessageBox.Show(("Set Face Fail, ResponseStatus.statusCode = " + JsonResponseStatus.statusCode))
                                                End If

                                                Exit While
                                            ElseIf (dwState = CType(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_EXCEPTION, Integer)) Then
                                                'MessageBox.Show(("Set Face Exception Error:" + CHCNetSDK.NET_DVR_GetLastError))
                                                Exit While
                                            Else
                                                'MessageBox.Show(("unknown Status Error:" + CHCNetSDK.NET_DVR_GetLastError))
                                                Exit While
                                            End If
                                        End While

                                        If (m_lSetFaceCfgHandle > 0) Then
                                            CHCNetSDK.NET_DVR_StopRemoteConfig(m_lSetFaceCfgHandle)
                                            m_lSetFaceCfgHandle = -1
                                        End If

                                        Marshal.FreeHGlobal(ptrJsonDataCfg)
                                        Marshal.FreeHGlobal(ptrJsonResponseStatus)
                                    Catch ex As Exception
                                        'MsgBox(ex.Message)
                                    End Try
                                    'End If
                                End If
                            End If

                        Next
                    End If
                End If
            End If
        Next i
        For Each c As Control In Me.Controls
            c.Enabled = True
        Next
        Me.Cursor = Cursors.Default
        XtraMasterTest.LabelControlStatus.Text = ""
        Application.DoEvents()
        ' Screen.MousePointer = vbNormal
        'MsgBox("Task Completed", vbOKOnly)
        XtraMessageBox.Show(ulf, "<size=10>Task Completed</size>", "<size=9>Information</size>")
    End Sub
    Private Sub SendUserInfo(sdwEnrollNumber As String, sName As String, ValidityStartdate As DateTime, ValidityEnddate As DateTime, Privilege As Integer, Cardnumber As String, Password As String)
        Dim JsonUserInfo As CUserInfoCfg = New CUserInfoCfg()
        JsonUserInfo.UserInfo = New CUserInfo()
        JsonUserInfo.UserInfo.employeeNo = sdwEnrollNumber
        JsonUserInfo.UserInfo.name = sName
        JsonUserInfo.UserInfo.userType = "normal"
        JsonUserInfo.UserInfo.Valid = New CValid()
        JsonUserInfo.UserInfo.Valid.enable = True
        JsonUserInfo.UserInfo.Valid.beginTime = ValidityStartdate.ToString("yyyy-MM-ddTHH:mm:ss") ' "2017-08-01T17:30:08"
        JsonUserInfo.UserInfo.Valid.endTime = ValidityEnddate.ToString("yyyy-MM-ddTHH:mm:ss") '"2020-08-01T17:30:08"
        JsonUserInfo.UserInfo.Valid.timeType = "local"
        JsonUserInfo.UserInfo.RightPlan = New List(Of CRightPlan)()
        Dim JsonRightPlan As CRightPlan = New CRightPlan()
        JsonRightPlan.doorNo = 1
        JsonRightPlan.planTemplateNo = 1 'textBoxRightPlan.Text
        JsonUserInfo.UserInfo.RightPlan.Add(JsonRightPlan)
        'JsonUserInfo.UserInfo.userVerifyMode = "card"
        JsonUserInfo.UserInfo.floorNumber = 0  'nitin
        JsonUserInfo.UserInfo.roomNumber = 0 'nitin
        JsonUserInfo.UserInfo.password = Password
        If Privilege = 1 Then
            JsonUserInfo.UserInfo.localUIRight = True  'admin rights
        Else
            JsonUserInfo.UserInfo.localUIRight = False   'admin rights
        End If

        JsonUserInfo.UserInfo.doorRight = "1" 'nitin
        Dim strJsonUserInfo As String = JsonConvert.SerializeObject(JsonUserInfo, Formatting.Indented, New JsonSerializerSettings With {
            .DefaultValueHandling = DefaultValueHandling.Ignore
        })
        Dim ptrJsonUserInfo As IntPtr = Marshal.StringToHGlobalAnsi(strJsonUserInfo)
        Dim ptrJsonData As IntPtr = Marshal.AllocHGlobal(1024)

        For i As Integer = 0 To 1024 - 1
            Marshal.WriteByte(ptrJsonData, i, 0)
        Next

        Dim dwState As Integer = CInt(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_SUCCESS)
        Dim dwReturned As UInteger = 0

        While True
            dwState = CHCNetSDK.NET_DVR_SendWithRecvRemoteConfig(m_lSetUserCfgHandle, ptrJsonUserInfo, CUInt(strJsonUserInfo.Length), ptrJsonData, 1024, dwReturned)
            Dim strJsonData As String = Marshal.PtrToStringAnsi(ptrJsonData)

            If dwState = CInt(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_NEEDWAIT) Then
                Thread.Sleep(10)
                Continue While
            ElseIf dwState = CInt(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_FAILED) Then
                'MessageBox.Show("Set User Fail error:" & CHCNetSDK.NET_DVR_GetLastError())
                Exit While
            ElseIf dwState = CInt(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_SUCCESS) Then
                Dim JsonResponseStatus As CResponseStatus = New CResponseStatus()
                JsonResponseStatus = JsonConvert.DeserializeObject(Of CResponseStatus)(strJsonData)

                If JsonResponseStatus.statusCode = 1 Then
                    'MessageBox.Show("Set User Success")
                Else
                    'MessageBox.Show("Set User Fail, ResponseStatus.statusCode" & JsonResponseStatus.statusCode)
                End If

                Exit While
            ElseIf dwState = CInt(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_FINISH) Then
                'MessageBox.Show("Set User Finish")
                Exit While
            ElseIf dwState = CInt(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_EXCEPTION) Then
                'MessageBox.Show("Set User Exception error:" & CHCNetSDK.NET_DVR_GetLastError())
                Exit While
            Else
                'MessageBox.Show("unknown Status error:" & CHCNetSDK.NET_DVR_GetLastError())
                Exit While
            End If
        End While

        If m_lSetUserCfgHandle <> -1 Then
            If CHCNetSDK.NET_DVR_StopRemoteConfig(m_lSetUserCfgHandle) Then
                m_lSetUserCfgHandle = -1
            End If
        End If

        Marshal.FreeHGlobal(ptrJsonUserInfo)
        Marshal.FreeHGlobal(ptrJsonData)
    End Sub
    Private Function GetSelItem(ByVal rowHandleEmp As Integer, ValidityStartdate As DateTime, ValidityEnddate As DateTime, sName As String, MachineCard As String) As CHCNetSDK.NET_DVR_CARD_CFG_V50
        Dim iPos As Integer = 0
        'If (rowHandleEmp >= 0) Then
        '    iPos = rowHandleEmp
        'End If

        Dim struCardInfo As CHCNetSDK.NET_DVR_CARD_CFG_V50 = New CHCNetSDK.NET_DVR_CARD_CFG_V50
        'If (iPos < 0) Then
        '    Return struCardInfo
        'End If

        struCardInfo.byCardNo = New Byte((32) - 1) {}
        struCardInfo.byName = New Byte((32) - 1) {}

        struCardInfo.dwModifyParamType = struCardInfo.dwModifyParamType Or &H80 'card password
        struCardInfo.byCardPassword = New Byte((8) - 1) {}

        struCardInfo.dwModifyParamType = struCardInfo.dwModifyParamType Or &H40 'Belongs to group parameters 
        struCardInfo.byBelongGroup = New Byte((128) - 1) {}

        struCardInfo.dwModifyParamType = struCardInfo.dwModifyParamType Or &H8  'DoorLimitParameter
        struCardInfo.byDoorRight = New Byte((256) - 1) {}
        struCardInfo.byLockCode = New Byte((8) - 1) {}
        struCardInfo.byRes2 = New Byte((3) - 1) {}
        struCardInfo.byRes3 = New Byte((83) - 1) {}
        struCardInfo.byRoomCode = New Byte((8) - 1) {}

        struCardInfo.dwModifyParamType = struCardInfo.dwModifyParamType Or &H100 'Card authority scheme
        struCardInfo.wCardRightPlan = New UShort((1024) - 1) {}
        struCardInfo.wCardRightPlan(0) = 1


        struCardInfo.struValid.byEnable = 1
        struCardInfo.struValid.byRes1 = New Byte((3) - 1) {}
        struCardInfo.struValid.byRes2 = New Byte((32) - 1) {}

        struCardInfo.dwModifyParamType = struCardInfo.dwModifyParamType Or &H2 'for validity
        struCardInfo.struValid.struBeginTime.wYear = ValidityStartdate.Year
        struCardInfo.struValid.struBeginTime.byMonth = ValidityStartdate.Month
        struCardInfo.struValid.struBeginTime.byDay = ValidityStartdate.Day
        struCardInfo.struValid.struBeginTime.byHour = ValidityStartdate.Hour
        struCardInfo.struValid.struBeginTime.byMinute = ValidityStartdate.Minute
        struCardInfo.struValid.struBeginTime.bySecond = ValidityStartdate.Second

        struCardInfo.struValid.struEndTime.wYear = ValidityEnddate.Year
        struCardInfo.struValid.struEndTime.byMonth = ValidityEnddate.Month
        struCardInfo.struValid.struEndTime.byDay = ValidityEnddate.Day
        struCardInfo.struValid.struEndTime.byHour = ValidityEnddate.Hour
        struCardInfo.struValid.struEndTime.byMinute = ValidityEnddate.Minute
        struCardInfo.struValid.struEndTime.bySecond = ValidityEnddate.Second


        struCardInfo.dwSize = CType(Marshal.SizeOf(struCardInfo), UInteger)
        Dim password As String = GridView2.GetRowCellValue(rowHandleEmp, "Password").ToString.Trim()
        Dim User_ID As String = GridView2.GetRowCellValue(rowHandleEmp, "EnrollNumber").ToString.Trim()
        Dim sdwEnrollNumber As String
        If IsNumeric(User_ID) = True Then
            sdwEnrollNumber = Convert.ToDouble(User_ID)
        Else
            sdwEnrollNumber = User_ID
        End If
        Dim byTemp() As Byte = System.Text.Encoding.UTF8.GetBytes(MachineCard)
        Dim y As Integer = 0
        Do While (y < byTemp.Length)
            struCardInfo.byCardNo(y) = byTemp(y)
            y = (y + 1)
        Loop

        byTemp = System.Text.Encoding.UTF8.GetBytes(sName)
        y = 0
        Do While (y < byTemp.Length)
            struCardInfo.byName(y) = byTemp(y)
            y = (y + 1)
        Loop

        byTemp = System.Text.Encoding.UTF8.GetBytes(password)
        y = 0
        Do While (y < byTemp.Length)
            struCardInfo.byCardPassword(y) = byTemp(y)
            y = (y + 1)
        Loop
        'struCardInfo.byCardType = 1
        struCardInfo.byCardValid = 1
        struCardInfo.byLeaderCard = 0
        struCardInfo.bySchedulePlanType = 2
        struCardInfo.dwEmployeeNo = sdwEnrollNumber
        struCardInfo.wDepartmentNo = 1
        struCardInfo.wSchedulePlanNo = 1
        struCardInfo.byDoorRight(0) = 1
        'new values hardcoded
        struCardInfo.dwModifyParamType = struCardInfo.dwModifyParamType Or &H4  'for card type
        struCardInfo.byCardType = CByte((1))

        Short.TryParse(0, struCardInfo.wFloorNumber)

        'struCardInfo.dwModifyParamType = struCardInfo.dwModifyParamType Or &H10 'FirstCardParameters
        'struCardInfo.byLeaderCard = 1

        'struCardInfo.dwModifyParamType = struCardInfo.dwModifyParamType Or &H20 'The maximum swiping card number
        'UInteger.TryParse(textBoxMaximumCreditCard.Text, m_struCardCfg.dwMaxSwipeTime)

        'struCardInfo.dwModifyParamType = struCardInfo.dwModifyParamType Or &H200  'Swiped number
        'UInteger.TryParse(textBoxreditCard.Text, m_struCardCfg.dwSwipeTime)

        m_struCardInfo(0) = struCardInfo
        Return m_struCardInfo(0)
    End Function
    Private Function UpdateFingerPrintCfg(ByRef struFingerPrintCfg As CHCNetSDK.NET_DVR_FINGER_PRINT_CFG_V50, ByVal cardNumber As String, ByVal FNumber As String, ByVal FPath As String) As Boolean
        struFingerPrintCfg.dwSize = CType(Marshal.SizeOf(struFingerPrintCfg), UInteger)
        Dim byTempCard() As Byte = System.Text.Encoding.UTF8.GetBytes(cardNumber)
        Dim i As Integer = 0
        Do While (i < byTempCard.Length)
            If (i > struFingerPrintCfg.byCardNo.Length) Then
                Return False
            End If

            struFingerPrintCfg.byCardNo(i) = byTempCard(i)
            i = (i + 1)
        Loop

        struFingerPrintCfg.dwFingerPrintLen = 512
        struFingerPrintCfg.byEnableCardReader = m_struFingerPrintOne.byEnableCardReader
        struFingerPrintCfg.byLeaderFP = m_struFingerPrintOne.byLeaderFP

        Byte.TryParse(FNumber, struFingerPrintCfg.byFingerPrintID)
        'struFingerPrintCfg.byFingerType = CType(comboBoxFingerprintType.SelectedIndex, Byte)
        struFingerPrintCfg.byFingerType = CType(0, Byte)
        Dim byTempEmployeeNo() As Byte = System.Text.Encoding.UTF8.GetBytes("")
        i = 0
        Do While (i < byTempEmployeeNo.Length)
            If (i > struFingerPrintCfg.byEmployeeNo.Length) Then
                Return False
            End If

            struFingerPrintCfg.byEmployeeNo(i) = byTempEmployeeNo(i)
            i = (i + 1)
        Loop
        Dim fs As FileStream = New FileStream(FPath, FileMode.OpenOrCreate)
        If (0 = fs.Length) Then
            struFingerPrintCfg.byFingerData(0) = 0
            fs.Close()
            Return True
        End If
        Dim objBinaryReader As BinaryReader = New BinaryReader(fs)
        If (struFingerPrintCfg.dwFingerPrintLen > CHCNetSDK.MAX_FINGER_PRINT_LEN) Then
            Return False
        End If
        i = 0
        Do While (i < struFingerPrintCfg.dwFingerPrintLen)
            If (i >= fs.Length) Then
                Exit Do
            End If
            struFingerPrintCfg.byFingerData(i) = objBinaryReader.ReadByte
            i = (i + 1)
        Loop
        fs.Close()
        Return True
    End Function
    Private Sub SendCardData(ByVal struCardCfg As CHCNetSDK.NET_DVR_CARD_CFG_V50, Optional ByVal dwDiffTime As UInteger = 0)
        If (-1 = m_lSetCardCfgHandle) Then
            Return
        End If
        Dim dwSize As UInteger = CType(Marshal.SizeOf(struCardCfg), UInteger)
        Dim ptrStruCard As IntPtr = Marshal.AllocHGlobal(CType(dwSize, Integer))
        Marshal.StructureToPtr(struCardCfg, ptrStruCard, False)
        If Not CHCNetSDK.NET_DVR_SendRemoteConfig(m_lSetCardCfgHandle, 3, ptrStruCard, dwSize) Then
            Marshal.FreeHGlobal(ptrStruCard)
            Return
        End If
        Marshal.FreeHGlobal(ptrStruCard)
        Return
    End Sub
    Private Sub ProcessSetGatewayCardCallback(ByVal dwType As UInteger, ByVal lpBuffer As IntPtr, ByVal dwBufLen As UInteger, ByVal pUserData As IntPtr)
        If (pUserData = Nothing) Then
            Return
        End If

        Dim dwStatus As UInteger = CType(Marshal.ReadInt32(lpBuffer), UInteger)
        If (dwStatus = CType(CHCNetSDK.NET_SDK_CALLBACK_STATUS_NORMAL.NET_SDK_CALLBACK_STATUS_PROCESSING, UInteger)) Then
            ' just example need refinement
            Dim listItem As ListViewItem = New ListViewItem
            listItem.Text = "SUCC"
            Dim strTemp As String = Nothing
            If m_bSendOne Then
                'strTemp = String.Format("Send SUCC,CardNO:{0}", System.Text.Encoding.UTF8.GetString(m_struCardInfo(m_iSelectIndex).byCardNo).TrimEnd(Microsoft.VisualBasic.ChrW(92)))
                m_bSendOne = False
            Else
                'strTemp = String.Format("Send SUCC,CardNO:{0}", System.Text.Encoding.UTF8.GetString(m_struCardInfo(m_dwSendIndex).byCardNo).TrimEnd(Microsoft.VisualBasic.ChrW(92)))
            End If

            'listItem.SubItems.Add(strTemp)
            'Me.AddList(listViewMessage, listItem, True)
            'next
            CHCNetSDK.PostMessage(pUserData, 1002, 0, 0)
        ElseIf (dwStatus = CType(CHCNetSDK.NET_SDK_CALLBACK_STATUS_NORMAL.NET_SDK_CALLBACK_STATUS_FAILED, UInteger)) Then
            Dim listItem As ListViewItem = New ListViewItem
            listItem.Text = "FAIL"
            Dim strTemp As String = Nothing
            Dim errorCode As UInteger = CType(Marshal.ReadInt32((lpBuffer + 4)), UInteger)
            Dim errorUserID As UInteger = CType(Marshal.ReadInt32((lpBuffer + 40)), UInteger)
            'strTemp = String.Format("Send FAILED,CardNO:{0},Error code{1},USER ID{2}", System.Text.Encoding.UTF8.GetString(m_struSelSendCardCfg.byCardNo).TrimEnd(Microsoft.VisualBasic.ChrW(92)), errorCode, errorUserID)
            'listItem.SubItems.Add(strTemp)
            'Me.AddList(listViewMessage, listItem, True)
            'next
            Dim x As Integer = CHCNetSDK.NET_DVR_GetLastError
            Dim tmp As String = m_struSelSendCardCfg.dwEmployeeNo
            CHCNetSDK.PostMessage(pUserData, 1002, 0, 0)
        ElseIf (dwStatus = CType(CHCNetSDK.NET_SDK_CALLBACK_STATUS_NORMAL.NET_SDK_CALLBACK_STATUS_SUCCESS, UInteger)) Then
            Dim listItem As ListViewItem = New ListViewItem
            listItem.Text = "SUCC"
            listItem.SubItems.Add("NET_DVR_SET_CARD_CFG_V50 Set finish")
            'Me.AddList(listViewMessage, listItem, True)
            CHCNetSDK.PostMessage(pUserData, 1001, 0, 0)
        ElseIf (dwStatus = CType(CHCNetSDK.NET_SDK_CALLBACK_STATUS_NORMAL.NET_SDK_CALLBACK_STATUS_EXCEPTION, UInteger)) Then
            Dim listItem As ListViewItem = New ListViewItem
            listItem.Text = "FAIL"
            listItem.SubItems.Add("NET_DVR_SET_CARD_CFG_V50 Set FAIL")
            'Me.AddList(listViewMessage, listItem, True)
            CHCNetSDK.PostMessage(pUserData, 1001, 0, 0)
        End If

        Return
    End Sub
    Private Sub ConvFpDataAfterReadFromDbForCompatibility(ByRef abytSrc() As Byte, ByRef abytDest() As Byte)
        Dim nTempLen As Long, lenConvFpData As Long
        Dim bytConvFpData() As Byte
        Dim k As Long, m As Long
        Dim bytTemp() As Byte

        nTempLen = abytSrc.Length / 5
        lenConvFpData = nTempLen * 4

        If lenConvFpData < FP_DATA_SIZE Then lenConvFpData = FP_DATA_SIZE
        ReDim bytConvFpData(lenConvFpData)

        For k = 0 To nTempLen - 1
            ReDim bytTemp(3)
            bytTemp(0) = abytSrc(k * 5 + 4)
            bytTemp(1) = abytSrc(k * 5 + 3)
            bytTemp(2) = abytSrc(k * 5 + 2)
            bytTemp(3) = abytSrc(k * 5 + 1)

            m = BitConverter.ToInt32(bytTemp, 0)
            If abytSrc(k * 5) = 0 Then
                m = -m
            ElseIf abytSrc(k * 5) = 2 Then
                m = -2147483648
            End If
            bytTemp = BitConverter.GetBytes(m)

            bytConvFpData(k * 4 + 3) = bytTemp(3)
            bytConvFpData(k * 4 + 2) = bytTemp(2)
            bytConvFpData(k * 4 + 1) = bytTemp(1)
            bytConvFpData(k * 4 + 0) = bytTemp(0)
        Next

        abytDest = bytConvFpData
    End Sub
    Private Sub ProcessSetFingerPrintCfgCallbackData(ByVal dwType As UInteger, ByVal lpBuffer As IntPtr, ByVal dwBufLen As UInteger, ByVal pUserData As IntPtr)
        If (pUserData = Nothing) Then
            Return
        End If

        If (dwType = CType(CHCNetSDK.NET_SDK_CALLBACK_TYPE.NET_SDK_CALLBACK_TYPE_STATUS, UInteger)) Then
            Dim dwStatus As UInteger = CType(Marshal.ReadInt32(lpBuffer), UInteger)
            If (dwStatus = CType(CHCNetSDK.NET_SDK_CALLBACK_STATUS_NORMAL.NET_SDK_CALLBACK_STATUS_PROCESSING, UInteger)) Then
                'some problem
                Dim listItem As ListViewItem = New ListViewItem
                listItem.Text = "SUCC"
                Dim strTemp As String = Nothing
                strTemp = String.Format("SetFingerPrint PROCESSING,CardNo:{0},EmployeeNo:{1}", System.Text.Encoding.UTF8.GetString(m_struRecordCardCfg(m_iSendIndex).byCardNo).TrimEnd(Microsoft.VisualBasic.ChrW(92)), System.Text.Encoding.UTF8.GetString(m_struRecordCardCfg(m_iSendIndex).byEmployeeNo).TrimEnd(Microsoft.VisualBasic.ChrW(92)))
                listItem.SubItems.Add(strTemp)
                'Me.AddList(listViewMessage, listItem)
                SendNextFingerPrint()
            ElseIf (dwStatus = CType(CHCNetSDK.NET_SDK_CALLBACK_STATUS_NORMAL.NET_SDK_CALLBACK_STATUS_FAILED, UInteger)) Then
                Dim listItem As ListViewItem = New ListViewItem
                listItem.Text = "FAIL"
                listItem.SubItems.Add("SetFingerPrint Err:NET_SDK_CALLBACK_STATUS_FAILED")
                'Me.AddList(listViewMessage, listItem)
                SendNextFingerPrint()
            ElseIf (dwStatus = CType(CHCNetSDK.NET_SDK_CALLBACK_STATUS_NORMAL.NET_SDK_CALLBACK_STATUS_SUCCESS, UInteger)) Then
                Dim listItem As ListViewItem = New ListViewItem
                listItem.Text = "SUCC"
                listItem.SubItems.Add("SetFingerPrint SUCCESS")
                'Me.AddList(listViewMessage, listItem)
                Dim listItem2 As ListViewItem = New ListViewItem
                listItem2.Text = "SUCC"
                listItem2.SubItems.Add("NET_DVR_SET_FINGERPRINT_CFG_V50 Set finish")
                'Me.AddList(listViewMessage, listItem2)
                CHCNetSDK.PostMessage(pUserData, 1001, 0, 0)
            ElseIf (dwStatus = CType(CHCNetSDK.NET_SDK_CALLBACK_STATUS_NORMAL.NET_SDK_CALLBACK_STATUS_EXCEPTION, UInteger)) Then
                Dim listItem As ListViewItem = New ListViewItem
                listItem.Text = "FAIL"
                listItem.SubItems.Add("SetFingerPrint EXCEPTION")
                'Me.AddList(listViewMessage, listItem)
                Dim listItem2 As ListViewItem = New ListViewItem
                listItem2.Text = "SUCC"
                listItem2.SubItems.Add("NET_DVR_SET_FINGERPRINT_CFG_V50 Set finish")
                'Me.AddList(listViewMessage, listItem2)
            Else
                Dim listItem As ListViewItem = New ListViewItem
                listItem.Text = "SUCC"
                listItem.SubItems.Add("SetFingerPrint SUCCESS")
                'Me.AddList(listViewMessage, listItem)
                Dim listItem2 As ListViewItem = New ListViewItem
                listItem2.Text = "SUCC"
                listItem2.SubItems.Add("NET_DVR_SET_FINGERPRINT_CFG_V50 Set finish")
                'Me.AddList(listViewMessage, listItem2)
            End If

        ElseIf (dwType = CType(CHCNetSDK.NET_SDK_CALLBACK_TYPE.NET_SDK_CALLBACK_TYPE_DATA, UInteger)) Then
            Dim struCfg As CHCNetSDK.NET_DVR_FINGER_PRINT_STATUS_V50 = New CHCNetSDK.NET_DVR_FINGER_PRINT_STATUS_V50
            struCfg = CType(Marshal.PtrToStructure(lpBuffer, GetType(CHCNetSDK.NET_DVR_FINGER_PRINT_STATUS_V50)), CHCNetSDK.NET_DVR_FINGER_PRINT_STATUS_V50)
            Dim bSendOK As Boolean = False
            If (struCfg.byRecvStatus = 0) Then
                Dim i As Integer = 0
                Do While (i < struCfg.byCardReaderRecvStatus.Length)
                    If (1 = struCfg.byCardReaderRecvStatus(i)) Then
                        bSendOK = True
                        Dim listItem As ListViewItem = New ListViewItem
                        listItem.Text = "SUCC"
                        Dim strTemp As String = Nothing
                        'strTemp = String.Format("SetFingerPrint PROCESSING,CardNo:{0},EmployeeNo:{1},CardReader:{2}", System.Text.Encoding.UTF8.GetString(m_struRecordCardCfg(m_iSendIndex).byCardNo).TrimEnd(Microsoft.VisualBasic.ChrW(92)), System.Text.Encoding.UTF8.GetString(m_struRecordCardCfg(m_iSendIndex).byEmployeeNo).TrimEnd(Microsoft.VisualBasic.ChrW(92)), (i + 1))
                        listItem.SubItems.Add(strTemp)
                        'Me.AddList(listViewMessage, listItem)
                    ElseIf (8 = struCfg.byCardReaderRecvStatus(i)) Then
                        'd:8�h�MnE�Sp�U�o

                    Else
                        bSendOK = False
                        Dim listItem As ListViewItem = New ListViewItem
                        listItem.Text = "FAIL"
                        Dim strTemp As String = Nothing
                        'strTemp = String.Format("SetFingerPrint PROCESSING,CardNo:{0},EmployeeNo:{1},CardReader:{2},Error Code:{3}", System.Text.Encoding.UTF8.GetString(m_struRecordCardCfg(m_iSendIndex).byCardNo).TrimEnd(Microsoft.VisualBasic.ChrW(92)), System.Text.Encoding.UTF8.GetString(m_struRecordCardCfg(m_iSendIndex).byEmployeeNo).TrimEnd(Microsoft.VisualBasic.ChrW(92)), (i + 1), struCfg.byCardReaderRecvStatus(i))
                        listItem.SubItems.Add(strTemp)
                        'Me.AddList(listViewMessage, listItem)
                    End If

                    i = (i + 1)
                Loop

            Else
                bSendOK = False
                Dim listItem As ListViewItem = New ListViewItem
                listItem.Text = "FAIL"
                Dim strTemp As String = Nothing
                If (struCfg.byRecvStatus = 1) Then
                    strTemp = String.Format("SetFingerPrint PROCESSING,CardNo:{0},EmployeeNo:{1},Error Finger ID", System.Text.Encoding.UTF8.GetString(m_struRecordCardCfg(m_iSendIndex).byCardNo).TrimEnd(Microsoft.VisualBasic.ChrW(92)), System.Text.Encoding.UTF8.GetString(m_struRecordCardCfg(m_iSendIndex).byEmployeeNo).TrimEnd(Microsoft.VisualBasic.ChrW(92)))
                ElseIf (struCfg.byRecvStatus = 2) Then
                    strTemp = String.Format("SetFingerPrint PROCESSING,CardNo:{0},EmployeeNo:{1},Error Finger Print Type", System.Text.Encoding.UTF8.GetString(m_struRecordCardCfg(m_iSendIndex).byCardNo).TrimEnd(Microsoft.VisualBasic.ChrW(92)), System.Text.Encoding.UTF8.GetString(m_struRecordCardCfg(m_iSendIndex).byEmployeeNo).TrimEnd(Microsoft.VisualBasic.ChrW(92)))
                ElseIf (struCfg.byRecvStatus = 3) Then
                    strTemp = String.Format("SetFingerPrint PROCESSING,CardNo:{0},EmployeeNo:{1},Illegal Card No", System.Text.Encoding.UTF8.GetString(m_struRecordCardCfg(m_iSendIndex).byCardNo).TrimEnd(Microsoft.VisualBasic.ChrW(92)), System.Text.Encoding.UTF8.GetString(m_struRecordCardCfg(m_iSendIndex).byEmployeeNo).TrimEnd(Microsoft.VisualBasic.ChrW(92)))
                ElseIf (struCfg.byRecvStatus = 4) Then
                    strTemp = String.Format("SetFingerPrint PROCESSING,CardNo:{0},EmployeeNo:{1},Finger Print Not Link Employee No Or Card No", System.Text.Encoding.UTF8.GetString(m_struRecordCardCfg(m_iSendIndex).byCardNo).TrimEnd(Microsoft.VisualBasic.ChrW(92)), System.Text.Encoding.UTF8.GetString(m_struRecordCardCfg(m_iSendIndex).byEmployeeNo).TrimEnd(Microsoft.VisualBasic.ChrW(92)))
                ElseIf (struCfg.byRecvStatus = 5) Then
                    strTemp = String.Format("SetFingerPrint PROCESSING,CardNo:{0},EmployeeNo:{1},Employee No Not Exist", System.Text.Encoding.UTF8.GetString(m_struRecordCardCfg(m_iSendIndex).byCardNo).TrimEnd(Microsoft.VisualBasic.ChrW(92)), System.Text.Encoding.UTF8.GetString(m_struRecordCardCfg(m_iSendIndex).byEmployeeNo).TrimEnd(Microsoft.VisualBasic.ChrW(92)))
                Else
                    strTemp = String.Format("SetFingerPrint PROCESSING,CardNo:{0},EmployeeNo:{1},Error Code:{2}", System.Text.Encoding.UTF8.GetString(m_struRecordCardCfg(m_iSendIndex).byCardNo).TrimEnd(Microsoft.VisualBasic.ChrW(92)), System.Text.Encoding.UTF8.GetString(m_struRecordCardCfg(m_iSendIndex).byEmployeeNo).TrimEnd(Microsoft.VisualBasic.ChrW(92)), struCfg.byRecvStatus)
                End If

                listItem.SubItems.Add(strTemp)
                'Me.AddList(listViewMessage, listItem)
            End If

            If Not bSendOK Then
                Dim listItem As ListViewItem = New ListViewItem
                listItem.Text = "FAIL"
                Dim strTemp As String = Nothing
                'strTemp = String.Format("SetFingerPrint Failed,CardNo:{0},EmployeeNo:{1}", System.Text.Encoding.UTF8.GetString(m_struRecordCardCfg(m_iSendIndex).byCardNo).TrimEnd(Microsoft.VisualBasic.ChrW(92)), System.Text.Encoding.UTF8.GetString(m_struRecordCardCfg(m_iSendIndex).byEmployeeNo).TrimEnd(Microsoft.VisualBasic.ChrW(92)))
                listItem.SubItems.Add(strTemp)
                'Me.AddList(listViewMessage, listItem)
            End If

            'if (0 == struCfg.byTotalStatus)
            '{
            '}
            SendNextFingerPrint()
        End If

    End Sub
    Private Function SendNextFingerPrint() As Boolean
        'IntPtr ihWnd = this.Handle;
        If (-1 = m_lSetFingerPrintCfgHandle) Then
            Return False
        End If

        m_iSendIndex = (m_iSendIndex + 1)
        Dim strTempText As String = "0" 'GetTextBoxText(textBoxNumber)
        Dim indexTmp As Integer = 0
        Integer.TryParse(strTempText, indexTmp)
        If (m_iSendIndex >= indexTmp) Then
            Return True
        End If

        Dim dwSize As UInteger = CType(Marshal.SizeOf(m_struRecordCardCfg(m_iSendIndex)), UInteger)
        Dim ptrNowSendCard As IntPtr = Marshal.AllocHGlobal(CType(dwSize, Integer))
        Marshal.StructureToPtr(m_struRecordCardCfg(m_iSendIndex), ptrNowSendCard, False)
        If Not CHCNetSDK.NET_DVR_SendRemoteConfig(m_lSetFingerPrintCfgHandle, 3, ptrNowSendCard, dwSize) Then
            Dim listItem As ListViewItem = New ListViewItem
            listItem.Text = "FAIL"
            Dim strTemp As String = String.Format("Send Fail,CardNo:{0},EmployeeNo:{1}", System.Text.Encoding.UTF8.GetString(m_struRecordCardCfg(m_iSendIndex).byCardNo).TrimEnd(Microsoft.VisualBasic.ChrW(92)), System.Text.Encoding.UTF8.GetString(m_struRecordCardCfg(m_iSendIndex).byEmployeeNo).TrimEnd(Microsoft.VisualBasic.ChrW(92)))
            listItem.SubItems.Add(strTemp)
            'Me.AddList(listViewMessage, listItem)
            Marshal.FreeHGlobal(ptrNowSendCard)
            Return False
        End If

        Marshal.FreeHGlobal(ptrNowSendCard)
        Return True
    End Function
    Private Sub btnDeleteFrmDevice_Click(sender As System.Object, e As System.EventArgs) Handles btnDeleteFrmDevice.Click
        Dim m As Integer
        'Dim e As Integer
        Dim lngMachineNum As Long
        Dim mCommKey As Long
        Dim lpszIPAddress As String
        Dim mKey As String
        Dim vnMachineNumber As Long
        Dim vnCommPort As Long
        Dim vnCommBaudrate As Long
        Dim vstrTelNumber As String
        Dim vnWaitDialTime As Long
        Dim vnLicense As Long
        Dim vpszIPAddress As String
        Dim vpszNetPort As Long
        Dim vpszNetPassword As Long
        Dim vnTimeOut As Long
        Dim vnProtocolType As Long
        Dim strDateTime As String
        Dim vRet As Long
        Dim vPrivilege As Long
        Dim mEmp As String
        Dim vFingerNumber As Long
        Dim vnResultCode As Long

        If GridView1.GetSelectedRows.Count = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>Please select the Machine</size>", "<size=9>Error</size>")
            Exit Sub
        End If
        If GridView2.GetSelectedRows.Count = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>Please select Employee</size>", "<size=9>Error</size>")
            Exit Sub
        End If
        If TextPassword.Text.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Please Enter Login Password</size>", "<size=9>Error</size>")
            TextPassword.Select()
            Exit Sub
        Else
            If TextPassword.Text <> Common.PASSWORD Then
                XtraMessageBox.Show(ulf, "<size=10>Login Password Incorrect</size>", "<size=9>Error</size>")
                TextPassword.Select()
                Exit Sub
            End If
        End If

        If XtraMessageBox.Show(ulf, "<size=10>" & Common.res_man.GetString("askdelete", Common.cul) & "</size>", Common.res_man.GetString("confirmdeleteion", Common.cul), _
                             MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
            For Each c As Control In Me.Controls
                c.Enabled = False
            Next
            Me.Cursor = Cursors.WaitCursor
            Dim sSql As String = ""
            Dim commkey As Integer
            Dim selectedRows As Integer() = GridView1.GetSelectedRows()
            Dim result As Object() = New Object(selectedRows.Length - 1) {}
            Dim LstMachineId As String
            For i = 0 To selectedRows.Length - 1
                Dim rowHandle As Integer = selectedRows(i)
                If Not GridView1.IsGroupRow(rowHandle) Then
                    LstMachineId = GridView1.GetRowCellValue(rowHandle, "ID_NO").ToString.Trim
                    'sSql = "select * from tblmachine where ID_NO='" & LstMachineId & "'"
                    Dim bConn As Boolean = False
                    vnMachineNumber = LstMachineId
                    vnLicense = 1261 '1789 '
                    lpszIPAddress = GridView1.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim 'Trim(rstm!Location)

                    vpszIPAddress = Trim(lpszIPAddress)
                    If GridView1.GetRowCellValue(rowHandle, "DeviceType") = "Bio-1/3/4/5/6/7/ATF-203/ATF-395/ACR-872" Then

                    ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "ZK(TFT)" Or GridView1.GetRowCellValue(rowHandle, "DeviceType") = "Bio-1Pro/ATF305Pro/ATF686Pro" Then

                    ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "HKSeries" Then
                        Dim cn As Common = New Common
                        Dim struDeviceInfoV40 As CHCNetSDK.NET_DVR_DEVICEINFO_V40 = New CHCNetSDK.NET_DVR_DEVICEINFO_V40
                        Dim DeviceAdd As String = GridView1.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim, userName As String = Trim(GridView1.GetRowCellValue(rowHandle, "HLogin").ToString.Trim), pwd As String = Trim(GridView1.GetRowCellValue(rowHandle, "HPassword").ToString.Trim)
                        Dim lUserID As Integer = -1
                        Dim failReason As String = ""
                        Dim logistatus = cn.HikvisionLogin(DeviceAdd, userName, pwd, struDeviceInfoV40, lUserID, failReason)
                        If logistatus = False Then
                            If failReason <> "" Then
                                XtraMessageBox.Show(ulf, "<size=10>" & failReason & "</size>", "Failed")
                                XtraMasterTest.LabelControlStatus.Text = ""
                                Application.DoEvents()
                                Continue For
                            End If
                        Else
                            If failReason <> "" Then
                                XtraMessageBox.Show(ulf, "<size=10>" & failReason & "</size>", "Failed")
                                XtraMasterTest.LabelControlStatus.Text = ""
                                Application.DoEvents()
                                logistatus = cn.HikvisionLogOut(lUserID)
                                Continue For
                            End If
                            XtraMasterTest.LabelControlStatus.Text = ""
                            Application.DoEvents()
                            Dim selectedRowsEmp As Integer() = GridView2.GetSelectedRows()
                            Dim resultemp As Object() = New Object(selectedRowsEmp.Length - 1) {}
                            For x As Integer = 0 To selectedRowsEmp.Length - 1
                                Dim rowHandleemp As Integer = selectedRowsEmp(x)
                                If Not GridView2.IsGroupRow(rowHandleemp) Then
                                    mEmp = GridView2.GetRowCellValue(rowHandleemp, "EnrollNumber").ToString.Trim
                                    XtraMasterTest.LabelControlStatus.Text = "Deleting Template " & mEmp
                                    Application.DoEvents()

                                    Dim ptrOutBuf As IntPtr = Marshal.AllocHGlobal(1024)
                                    Dim ptrStatusBuffer As IntPtr = Marshal.AllocHGlobal(1024)

                                    For n As Integer = 0 To 1024 - 1
                                        Marshal.WriteByte(ptrOutBuf, n, 0)
                                        Marshal.WriteByte(ptrStatusBuffer, n, 0)
                                    Next

                                    Dim struInput As CHCNetSDK.NET_DVR_XML_CONFIG_INPUT = New CHCNetSDK.NET_DVR_XML_CONFIG_INPUT()
                                    Dim struOuput As CHCNetSDK.NET_DVR_XML_CONFIG_OUTPUT = New CHCNetSDK.NET_DVR_XML_CONFIG_OUTPUT()
                                    Dim sUrl As String = "PUT /ISAPI/AccessControl/UserInfoDetail/Delete?format=json"
                                    Dim ptrURL As IntPtr = Marshal.StringToHGlobalAnsi(sUrl)
                                    struInput.dwSize = CUInt(Marshal.SizeOf(struInput))
                                    struInput.lpRequestUrl = ptrURL
                                    struInput.dwRequestUrlLen = CUInt(sUrl.Length)
                                    Dim JsonUserInfoDetailCfg As CUserInfoDetailCfg = New CUserInfoDetailCfg()
                                    JsonUserInfoDetailCfg.UserInfoDetail = New CUserInfoDetail()
                                    JsonUserInfoDetailCfg.UserInfoDetail.mode = "byEmployeeNo"
                                    JsonUserInfoDetailCfg.UserInfoDetail.EmployeeNoList = New List(Of CEmployeeNoList)()
                                    Dim singleEmployeeNoList As CEmployeeNoList = New CEmployeeNoList()
                                    singleEmployeeNoList.employeeNo = mEmp
                                    JsonUserInfoDetailCfg.UserInfoDetail.EmployeeNoList.Add(singleEmployeeNoList)
                                    Dim strUserInfoDetailCfg As String = JsonConvert.SerializeObject(JsonUserInfoDetailCfg)
                                    Dim ptrUserInfoDetailCfg As IntPtr = Marshal.StringToHGlobalAnsi(strUserInfoDetailCfg)
                                    struInput.lpInBuffer = ptrUserInfoDetailCfg
                                    struInput.dwInBufferSize = CUInt(strUserInfoDetailCfg.Length)
                                    struOuput.dwSize = CUInt(Marshal.SizeOf(struOuput))
                                    struOuput.lpOutBuffer = ptrOutBuf
                                    struOuput.dwOutBufferSize = 1024
                                    struOuput.lpStatusBuffer = ptrStatusBuffer
                                    struOuput.dwStatusSize = 1024
                                    Dim ptrInput As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(struInput))
                                    Marshal.StructureToPtr(struInput, ptrInput, False)
                                    Dim ptrOuput As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(struOuput))
                                    Marshal.StructureToPtr(struOuput, ptrOuput, False)

                                    If Not CHCNetSDK.NET_DVR_STDXMLConfig(lUserID, ptrInput, ptrOuput) Then
                                        'MessageBox.Show("NET_DVR_STDXMLConfig fail [url:PUT /ISAPI/AccessControl/UserInfoDetail/Delete?format=json] error:" & CHCNetSDK.NET_DVR_GetLastError())
                                        Marshal.FreeHGlobal(ptrOutBuf)
                                        Marshal.FreeHGlobal(ptrStatusBuffer)
                                        Marshal.FreeHGlobal(ptrUserInfoDetailCfg)
                                        Marshal.FreeHGlobal(ptrInput)
                                        Marshal.FreeHGlobal(ptrOuput)
                                        Marshal.FreeHGlobal(ptrURL)
                                        Return
                                    Else
                                        'Dim strResponseStatus As String = Marshal.PtrToStringAnsi(struOuput.lpOutBuffer)
                                        'Dim JsonResponseStatus As CResponseStatus = New CResponseStatus()
                                        'JsonResponseStatus = JsonConvert.DeserializeObject(Of CResponseStatus)(strResponseStatus)

                                        'If JsonResponseStatus.statusCode <> 1 Then
                                        '    'MessageBox.Show("NET_DVR_STDXMLConfig Return ResponseStatus.statusCode:" & JsonResponseStatus.statusCode)
                                        'End If

                                        Marshal.FreeHGlobal(ptrOutBuf)
                                        Marshal.FreeHGlobal(ptrStatusBuffer)
                                        Marshal.FreeHGlobal(ptrUserInfoDetailCfg)
                                        Marshal.FreeHGlobal(ptrInput)
                                        Marshal.FreeHGlobal(ptrOuput)
                                        Marshal.FreeHGlobal(ptrURL)
                                    End If

                                    If -1 <> m_lDelUserCfgHandle Then

                                        If CHCNetSDK.NET_DVR_StopRemoteConfig(m_lDelUserCfgHandle) Then
                                            m_lDelUserCfgHandle = -1
                                        End If
                                    End If

                                    Dim sUrlDeleteProcess As String = "GET /ISAPI/AccessControl/UserInfoDetail/DeleteProcess?format=json"
                                    Dim ptrUrlDeleteProcess As IntPtr = Marshal.StringToHGlobalAnsi(sUrlDeleteProcess)
                                    m_lDelUserCfgHandle = CHCNetSDK.NET_DVR_StartRemoteConfig(lUserID, CHCNetSDK.NET_DVR_JSON_CONFIG, ptrUrlDeleteProcess, sUrlDeleteProcess.Length, Nothing, IntPtr.Zero)

                                    If m_lDelUserCfgHandle < 0 Then
                                        'MessageBox.Show("NET_DVR_StartRemoteConfig fail [url:GET /ISAPI/AccessControl/UserInfoDetail/DeleteProcess?format=json] error:" & CHCNetSDK.NET_DVR_GetLastError())
                                        Marshal.FreeHGlobal(ptrUrlDeleteProcess)
                                        Continue For
                                    Else
                                        Dim ptrJsonData As IntPtr = Marshal.AllocHGlobal(1024)

                                        For n As Integer = 0 To 1024 - 1
                                            Marshal.WriteByte(ptrJsonData, n, 0)
                                        Next

                                        Dim dwState As Integer = CInt(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_SUCCESS)

                                        While True
                                            dwState = CHCNetSDK.NET_DVR_GetNextRemoteConfig(m_lDelUserCfgHandle, ptrJsonData, 1024)

                                            If dwState = -1 Then
                                                Dim a As UInteger = CHCNetSDK.NET_DVR_GetLastError()
                                            End If

                                            Dim strJsonData As String = Marshal.PtrToStringAnsi(ptrJsonData)

                                            If dwState = CInt(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_NEEDWAIT) Then
                                                Thread.Sleep(10)
                                                Continue While
                                            ElseIf dwState = CInt(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_FAILED) Then
                                                'MessageBox.Show("Get DelUser Process Fail error:" & CHCNetSDK.NET_DVR_GetLastError())
                                            ElseIf dwState = CInt(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_SUCCESS) Then
                                                Dim JsonUserInfoSearchCfg As CUserInfoDetailDeleteProcessCfg = New CUserInfoDetailDeleteProcessCfg()
                                                JsonUserInfoSearchCfg = JsonConvert.DeserializeObject(Of CUserInfoDetailDeleteProcessCfg)(strJsonData)

                                                If JsonUserInfoSearchCfg.UserInfoDetailDeleteProcess Is Nothing Then
                                                    Dim JsonResponseStatus As CResponseStatus = New CResponseStatus()
                                                    JsonResponseStatus = JsonConvert.DeserializeObject(Of CResponseStatus)(strJsonData)

                                                    If JsonResponseStatus.statusCode = 1 Then
                                                        'MessageBox.Show("Get DelUser Process Success")
                                                    Else
                                                        'MessageBox.Show("Get DelUser Process Fail, ResponseStatus.statusCode" & JsonResponseStatus.statusCode)
                                                    End If
                                                Else

                                                    If JsonUserInfoSearchCfg.UserInfoDetailDeleteProcess.status = "success" Then
                                                        'MessageBox.Show("Del User Success")
                                                    ElseIf JsonUserInfoSearchCfg.UserInfoDetailDeleteProcess.status = "failed" Then
                                                        'MessageBox.Show("Del User Failed")
                                                    ElseIf JsonUserInfoSearchCfg.UserInfoDetailDeleteProcess.status = "processing" Then
                                                        'MessageBox.Show("Del User processing")
                                                    End If
                                                End If
                                                Exit While
                                            ElseIf dwState = CInt(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_FINISH) Then
                                                'MessageBox.Show("Get DelUser Process Finish")
                                                Exit While
                                            ElseIf dwState = CInt(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_EXCEPTION) Then
                                                'MessageBox.Show("Get DelUser Process Exception error:" & CHCNetSDK.NET_DVR_GetLastError())
                                                Exit While
                                            Else
                                                'MessageBox.Show("unknown Status Error:" & CHCNetSDK.NET_DVR_GetLastError())
                                                Exit While
                                            End If
                                        End While
                                        Marshal.FreeHGlobal(ptrJsonData)
                                    End If
                                    If -1 <> m_lDelUserCfgHandle Then
                                        If CHCNetSDK.NET_DVR_StopRemoteConfig(m_lDelUserCfgHandle) Then
                                            m_lDelUserCfgHandle = -1
                                        End If
                                    End If
                                    Marshal.FreeHGlobal(ptrUrlDeleteProcess)
                                End If
                            Next
                        End If
                    End If
                End If
            Next
            For Each c As Control In Me.Controls
                c.Enabled = True
            Next
            TextPassword.Text = ""
            XtraMasterTest.LabelControlStatus.Text = ""
            Application.DoEvents()
            Me.Cursor = Cursors.Default
            XtraMessageBox.Show(ulf, "<size=10>Task Completed</size>", "<size=9>Information</size>")
        End If
    End Sub
    Private Sub btnClearAdmin_Click(sender As System.Object, e As System.EventArgs) Handles btnClearAdmin.Click
        Dim bConn As Boolean
        Dim lngMachineNum As Long
        Dim mCommKey As Long
        Dim lpszIPAddress As String
        Dim vEMachineNumber As Long
        Dim vEnrollNumber As Long
        Dim vFingerNumber As Long
        Dim vPrivilege As Long
        Dim vEnable As Long
        Dim vRet As Boolean
        Dim mKey As String
        Dim vnResultCode As Long
        Dim RsDevice As New DataSet 'ADODB.Recordset
        vnResultCode = 0
        Dim vnMachineNumber As Long

        Dim vstrTelNumber As String
        Dim vnWaitDialTime As Long
        Dim vnLicense As Long
        Dim vpszIPAddress As String
        Dim vpszNetPort As Long
        Dim vpszNetPassword As Long
        Dim vnCommPort As Long
        Dim vnCommBaudrate As Long

        If GridView1.GetSelectedRows.Count = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>Please select the Machine</size>", "<size=9>Error</size>")
            Exit Sub
        End If
        If TextPassword.Text.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Please Enter Login Password</size>", "<size=9>Error</size>")
            TextPassword.Select()
            Exit Sub
        Else
            If TextPassword.Text <> Common.PASSWORD Then
                XtraMessageBox.Show(ulf, "<size=10>Login Password Incorrect</size>", "<size=9>Error</size>")
                TextPassword.Select()
                Exit Sub
            End If
        End If
        If XtraMessageBox.Show(ulf, "<size=10>Are you sure to clear admin for selected machine</size>", Common.res_man.GetString("confirmdeleteion", Common.cul), _
                             MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then

            Me.Cursor = Cursors.WaitCursor
            For Each c As Control In Me.Controls
                c.Enabled = False
            Next
            Dim sSql As String = ""
            Dim selectedRows As Integer() = GridView1.GetSelectedRows()
            Dim result As Object() = New Object(selectedRows.Length - 1) {}
            Dim LstMachineId As String
            Dim mMachineNumber As Long
            Dim commkey As Integer
            For i = 0 To selectedRows.Length - 1
                Dim rowHandle As Integer = selectedRows(i)
                If Not GridView1.IsGroupRow(rowHandle) Then
                    LstMachineId = GridView1.GetRowCellValue(rowHandle, "ID_NO").ToString.Trim

                    'sSql = "select * from tblmachine where ID_NO='" & LstMachineId & "'"

                    bConn = False
                    vnMachineNumber = LstMachineId
                    vnLicense = 1261 '1789 '
                    lpszIPAddress = GridView1.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim 'Trim(rstm!Location)
                    vpszIPAddress = Trim(lpszIPAddress)

                    If GridView1.GetRowCellValue(rowHandle, "DeviceType") = "Bio-1/3/4/5/6/7/ATF-203/ATF-395/ACR-872" Then

                    ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "ZK(TFT)" Or GridView1.GetRowCellValue(rowHandle, "DeviceType") = "Bio-1Pro/ATF305Pro/ATF686Pro" Then

                    End If
                End If
            Next
            For Each c As Control In Me.Controls
                c.Enabled = True
            Next
            TextPassword.Text = ""
            Me.Cursor = Cursors.Default
            XtraMessageBox.Show(ulf, "<size=10>Task Completed</size>", "<size=9>Information</size>")
        End If
    End Sub
    Private Sub SimpleButton1_Click_1(sender As System.Object, e As System.EventArgs) Handles SimpleButton1.Click
        Me.Close()
    End Sub
    Private Sub btnClearDeviceData_Click(sender As System.Object, e As System.EventArgs) Handles btnClearDeviceData.Click
        Dim bConn As Boolean
        Dim lngMachineNum As Long
        Dim mCommKey As Long
        Dim lpszIPAddress As String
        Dim vEMachineNumber As Long
        Dim vEnrollNumber As Long
        Dim vFingerNumber As Long
        Dim vPrivilege As Long
        Dim vEnable As Long
        Dim vRet As Boolean
        Dim mKey As String
        Dim vnResultCode As Long
        Dim RsDevice As New DataSet 'ADODB.Recordset
        vnResultCode = 0
        Dim vnMachineNumber As Long

        Dim vstrTelNumber As String
        Dim vnWaitDialTime As Long
        Dim vnLicense As Long
        Dim vpszIPAddress As String
        Dim vpszNetPort As Long
        Dim vpszNetPassword As Long
        Dim vnCommPort As Long
        Dim vnCommBaudrate As Long

        If GridView1.GetSelectedRows.Count = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>Please select the Machine</size>", "<size=9>Error</size>")
            Exit Sub
        End If
        If TextPassword.Text.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Please Enter Login Password</size>", "<size=9>Error</size>")
            TextPassword.Select()
            Exit Sub
        Else
            If TextPassword.Text <> Common.PASSWORD Then
                XtraMessageBox.Show(ulf, "<size=10>Login Password Incorrect</size>", "<size=9>Error</size>")
                TextPassword.Select()
                Exit Sub
            End If
        End If
        If XtraMessageBox.Show(ulf, "<size=10>Are you sure to Clear Device Data for selected machine</size>", Common.res_man.GetString("confirmdeleteion", Common.cul), _
                             MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
            Me.Cursor = Cursors.WaitCursor
            For Each c As Control In Me.Controls
                c.Enabled = False
            Next
            Dim sSql As String = ""
            Dim selectedRows As Integer() = GridView1.GetSelectedRows()
            Dim result As Object() = New Object(selectedRows.Length - 1) {}
            Dim LstMachineId As String
            Dim mMachineNumber As Long
            Dim commkey As Integer
            For i = 0 To selectedRows.Length - 1
                Dim rowHandle As Integer = selectedRows(i)
                If Not GridView1.IsGroupRow(rowHandle) Then
                    LstMachineId = GridView1.GetRowCellValue(rowHandle, "ID_NO").ToString.Trim
                    bConn = False
                    vnMachineNumber = LstMachineId
                    vnLicense = 1261 '1789 '
                    lpszIPAddress = GridView1.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim 'Trim(rstm!Location)
                    vpszIPAddress = Trim(lpszIPAddress)

                    If GridView1.GetRowCellValue(rowHandle, "DeviceType") = "Bio-1/3/4/5/6/7/ATF-203/ATF-395/ACR-872" Then

                    ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "ZK(TFT)" Or GridView1.GetRowCellValue(rowHandle, "DeviceType") = "Bio-1Pro/ATF305Pro/ATF686Pro" Then

                    ElseIf GridView1.GetRowCellValue(rowHandle, "DeviceType") = "HKSeries" Then
                        Dim cn As Common = New Common()
                        Dim struDeviceInfoV40 As CHCNetSDK.NET_DVR_DEVICEINFO_V40 = New CHCNetSDK.NET_DVR_DEVICEINFO_V40
                        Dim DeviceAdd As String = vpszIPAddress, userName As String = Trim(GridView1.GetRowCellValue(rowHandle, "HLogin").ToString.Trim), pwd As String = Trim(GridView1.GetRowCellValue(rowHandle, "HPassword").ToString.Trim)
                        Dim lUserID As Integer = -1
                        Dim failReason As String = ""
                        Dim logistatus = cn.HikvisionLogin(DeviceAdd, userName, pwd, struDeviceInfoV40, lUserID, failReason)
                        If logistatus = False Then
                            If failReason <> "" Then
                                For Each c As Control In Me.Controls
                                    c.Enabled = True
                                Next
                                XtraMessageBox.Show(ulf, "<size=10>" & failReason & "</size>", "Failed")
                                XtraMasterTest.LabelControlStatus.Text = ""
                                Application.DoEvents()
                                Continue For
                            End If
                        Else
                            If failReason <> "" Then
                                XtraMessageBox.Show(ulf, "<size=10>" & failReason & "</size>", "Failed")
                                XtraMasterTest.LabelControlStatus.Text = ""
                                Application.DoEvents()
                                Me.Cursor = Cursors.Default
                                logistatus = cn.HikvisionLogOut(lUserID)
                                Continue For
                            End If
                            Dim struAcsParam As CHCNetSDK.NET_DVR_ACS_PARAM_TYPE = New CHCNetSDK.NET_DVR_ACS_PARAM_TYPE
                            struAcsParam.dwSize = CType(Marshal.SizeOf(struAcsParam), UInteger)
                            struAcsParam.dwParamType = 4096 ' (struAcsParam.dwParamType Or (1 + 12))
                            Dim dwSize As UInteger = CType(Marshal.SizeOf(struAcsParam), UInteger)
                            Dim ptrAcsParam As IntPtr = Marshal.AllocHGlobal(CType(dwSize, Integer))
                            Marshal.StructureToPtr(struAcsParam, ptrAcsParam, False)
                            If Not CHCNetSDK.NET_DVR_RemoteControl(lUserID, CHCNetSDK.NET_DVR_CLEAR_ACS_PARAM, ptrAcsParam, dwSize) Then
                                Dim listItem As ListViewItem = New ListViewItem
                                listItem.Text = "FAIL"
                                Dim strTemp As String = String.Format("NET_DVR_CLEAR_ACS_PARAM FAIL, ERROR CODE {0}", CHCNetSDK.NET_DVR_GetLastError)
                                listItem.SubItems.Add(strTemp)
                                'Me.AddList(listViewMessage, listItem, True)
                                XtraMessageBox.Show(ulf, "<size=10>Failed To Clear Device Data</size>", "Failed")
                            Else
                                Dim listItem As ListViewItem = New ListViewItem
                                listItem.Text = "SUCC"
                                listItem.SubItems.Add("NET_DVR_CLEAR_ACS_PARAM SUCC")
                                'Me.AddList(listViewMessage, listItem, True)
                                XtraMessageBox.Show(ulf, "<size=10>Task Completed</size>", "Success")
                            End If

                            Marshal.FreeHGlobal(ptrAcsParam)
                        End If
                    End If
                End If
            Next
            For Each c As Control In Me.Controls
                c.Enabled = True
            Next
            TextPassword.Text = ""
            Me.Cursor = Cursors.Default

        End If
    End Sub
    Private Sub btnLockOpen_Click(sender As System.Object, e As System.EventArgs) Handles btnLockOpen.Click
        Dim bConn As Boolean
        Dim lngMachineNum As Long
        Dim mCommKey As Long
        Dim lpszIPAddress As String
        Dim vEMachineNumber As Long
        Dim vEnrollNumber As Long
        Dim vFingerNumber As Long
        Dim vPrivilege As Long
        Dim vEnable As Long
        Dim vRet As Boolean
        Dim mKey As String
        Dim vnResultCode As Long
        Dim RsDevice As New DataSet 'ADODB.Recordset
        vnResultCode = 0
        Dim vnMachineNumber As Long

        Dim vstrTelNumber As String
        Dim vnWaitDialTime As Long
        Dim vnLicense As Long
        Dim vpszIPAddress As String
        Dim vpszNetPort As Long
        Dim vpszNetPassword As Long
        Dim vnCommPort As Long
        Dim vnCommBaudrate As Long

        If GridView1.GetSelectedRows.Count = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>Please select the Machine</size>", "<size=9>Error</size>")
            Exit Sub
        End If
        If TextPassword.Text.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Please Enter Login Password</size>", "<size=9>Error</size>")
            TextPassword.Select()
            Exit Sub
        Else
            If TextPassword.Text <> Common.PASSWORD Then
                XtraMessageBox.Show(ulf, "<size=10>Login Password Incorrect</size>", "<size=9>Error</size>")
                TextPassword.Select()
                Exit Sub
            End If
        End If
        If XtraMessageBox.Show(ulf, "<size=10>Are you sure to Open Door Lock for selected machine</size>", Common.res_man.GetString("confirmdeleteion", Common.cul), _
                             MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
            Me.Cursor = Cursors.WaitCursor
            For Each c As Control In Me.Controls
                c.Enabled = False
            Next
            Dim sSql As String = ""
            Dim selectedRows As Integer() = GridView1.GetSelectedRows()
            Dim result As Object() = New Object(selectedRows.Length - 1) {}
            Dim LstMachineId As String
            Dim mMachineNumber As Long
            Dim commkey As Integer
            For i = 0 To selectedRows.Length - 1
                Dim rowHandle As Integer = selectedRows(i)
                If Not GridView1.IsGroupRow(rowHandle) Then
                    LstMachineId = GridView1.GetRowCellValue(rowHandle, "ID_NO").ToString.Trim
                    bConn = False
                    vnMachineNumber = LstMachineId
                    vnLicense = 1261 '1789 '
                    lpszIPAddress = GridView1.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim 'Trim(rstm!Location)
                    vpszIPAddress = Trim(lpszIPAddress)

                    If GridView1.GetRowCellValue(rowHandle, "DeviceType") = "HKSeries" Then
                        Dim cn As Common = New Common()
                        Dim struDeviceInfoV40 As CHCNetSDK.NET_DVR_DEVICEINFO_V40 = New CHCNetSDK.NET_DVR_DEVICEINFO_V40
                        Dim DeviceAdd As String = vpszIPAddress, userName As String = Trim(GridView1.GetRowCellValue(rowHandle, "HLogin").ToString.Trim), pwd As String = Trim(GridView1.GetRowCellValue(rowHandle, "HPassword").ToString.Trim)
                        Dim lUserID As Integer = -1
                        Dim failReason As String = ""
                        Dim logistatus = cn.HikvisionLogin(DeviceAdd, userName, pwd, struDeviceInfoV40, lUserID, failReason)
                        If logistatus = False Then
                            If failReason <> "" Then
                                For Each c As Control In Me.Controls
                                    c.Enabled = True
                                Next
                                XtraMessageBox.Show(ulf, "<size=10>" & failReason & "</size>", "Failed")
                                XtraMasterTest.LabelControlStatus.Text = ""
                                Application.DoEvents()
                                Continue For
                            End If
                        Else
                            If failReason <> "" Then
                                XtraMessageBox.Show(ulf, "<size=10>" & failReason & "</size>", "Failed")
                                XtraMasterTest.LabelControlStatus.Text = ""
                                Application.DoEvents()
                                logistatus = cn.HikvisionLogOut(lUserID)
                                Continue For
                            End If
                            Dim struAcsParam As CHCNetSDK.NET_DVR_ACS_PARAM_TYPE = New CHCNetSDK.NET_DVR_ACS_PARAM_TYPE
                            struAcsParam.dwSize = CType(Marshal.SizeOf(struAcsParam), UInteger)
                            struAcsParam.dwParamType = 4096 ' (struAcsParam.dwParamType Or (1 + 12))
                            Dim dwSize As UInteger = CType(Marshal.SizeOf(struAcsParam), UInteger)
                            Dim ptrAcsParam As IntPtr = Marshal.AllocHGlobal(CType(dwSize, Integer))
                            Marshal.StructureToPtr(struAcsParam, ptrAcsParam, False)
                            If Not CHCNetSDK.NET_DVR_ControlGateway(lUserID, 1, 1) Then
                                Dim listItem As ListViewItem = New ListViewItem
                                listItem.Text = "FAIL"
                                Dim strTemp As String = String.Format("NET_DVR_CLEAR_ACS_PARAM FAIL, ERROR CODE {0}", CHCNetSDK.NET_DVR_GetLastError)
                                listItem.SubItems.Add(strTemp)
                                'Me.AddList(listViewMessage, listItem, True)
                                XtraMessageBox.Show(ulf, "<size=10>Failed To Open Door Lock</size>", "Failed")
                            Else
                                Dim listItem As ListViewItem = New ListViewItem
                                listItem.Text = "SUCC"
                                listItem.SubItems.Add("NET_DVR_CLEAR_ACS_PARAM SUCC")
                                'Me.AddList(listViewMessage, listItem, True)
                                XtraMessageBox.Show(ulf, "<size=10>Task Completed</size>", "Success")
                            End If

                            Marshal.FreeHGlobal(ptrAcsParam)
                        End If
                    End If
                End If
            Next
            For Each c As Control In Me.Controls
                c.Enabled = True
            Next
            TextPassword.Text = ""
            Me.Cursor = Cursors.Default
            XtraMessageBox.Show(ulf, "<size=10>Task Completed</size>", "<size=9>Information</size>")
        End If
    End Sub
    Private Sub btnLockClose_Click(sender As System.Object, e As System.EventArgs) Handles btnLockClose.Click
        Dim bConn As Boolean
        Dim lngMachineNum As Long
        Dim mCommKey As Long
        Dim lpszIPAddress As String
        Dim vEMachineNumber As Long
        Dim vEnrollNumber As Long
        Dim vFingerNumber As Long
        Dim vPrivilege As Long
        Dim vEnable As Long
        Dim vRet As Boolean
        Dim mKey As String
        Dim vnResultCode As Long
        Dim RsDevice As New DataSet 'ADODB.Recordset
        vnResultCode = 0
        Dim vnMachineNumber As Long

        Dim vstrTelNumber As String
        Dim vnWaitDialTime As Long
        Dim vnLicense As Long
        Dim vpszIPAddress As String
        Dim vpszNetPort As Long
        Dim vpszNetPassword As Long
        Dim vnCommPort As Long
        Dim vnCommBaudrate As Long

        If GridView1.GetSelectedRows.Count = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>Please select the Machine</size>", "<size=9>Error</size>")
            Exit Sub
        End If
        If TextPassword.Text.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Please Enter Login Password</size>", "<size=9>Error</size>")
            TextPassword.Select()
            Exit Sub
        Else
            If TextPassword.Text <> Common.PASSWORD Then
                XtraMessageBox.Show(ulf, "<size=10>Login Password Incorrect</size>", "<size=9>Error</size>")
                TextPassword.Select()
                Exit Sub
            End If
        End If
        If XtraMessageBox.Show(ulf, "<size=10>Are you sure to Close Door Lock for selected machine</size>", Common.res_man.GetString("confirmdeleteion", Common.cul),
                             MessageBoxButtons.YesNo, MessageBoxIcon.Question) = DialogResult.Yes Then
            Me.Cursor = Cursors.WaitCursor
            For Each c As Control In Me.Controls
                c.Enabled = False
            Next
            Dim sSql As String = ""
            Dim selectedRows As Integer() = GridView1.GetSelectedRows()
            Dim result As Object() = New Object(selectedRows.Length - 1) {}
            Dim LstMachineId As String
            Dim mMachineNumber As Long
            Dim commkey As Integer
            For i = 0 To selectedRows.Length - 1
                Dim rowHandle As Integer = selectedRows(i)
                If Not GridView1.IsGroupRow(rowHandle) Then
                    LstMachineId = GridView1.GetRowCellValue(rowHandle, "ID_NO").ToString.Trim
                    bConn = False
                    vnMachineNumber = LstMachineId
                    vnLicense = 1261 '1789 '
                    lpszIPAddress = GridView1.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim 'Trim(rstm!Location)
                    vpszIPAddress = Trim(lpszIPAddress)

                    If GridView1.GetRowCellValue(rowHandle, "DeviceType") = "HKSeries" Then
                        Dim cn As Common = New Common()
                        Dim struDeviceInfoV40 As CHCNetSDK.NET_DVR_DEVICEINFO_V40 = New CHCNetSDK.NET_DVR_DEVICEINFO_V40
                        Dim DeviceAdd As String = vpszIPAddress, userName As String = Trim(GridView1.GetRowCellValue(rowHandle, "HLogin").ToString.Trim), pwd As String = Trim(GridView1.GetRowCellValue(rowHandle, "HPassword").ToString.Trim)
                        Dim lUserID As Integer = -1
                        Dim failReason As String = ""
                        Dim logistatus = cn.HikvisionLogin(DeviceAdd, userName, pwd, struDeviceInfoV40, lUserID, failReason)
                        If logistatus = False Then
                            If failReason <> "" Then
                                XtraMessageBox.Show(ulf, "<size=10>" & failReason & "</size>", "Failed")
                                XtraMasterTest.LabelControlStatus.Text = ""
                                Application.DoEvents()
                                Continue For
                            End If
                        Else
                            If failReason <> "" Then
                                XtraMessageBox.Show(ulf, "<size=10>" & failReason & "</size>", "Failed")
                                XtraMasterTest.LabelControlStatus.Text = ""
                                Application.DoEvents()
                                logistatus = cn.HikvisionLogOut(lUserID)
                                Continue For
                            End If
                            Dim struAcsParam As CHCNetSDK.NET_DVR_ACS_PARAM_TYPE = New CHCNetSDK.NET_DVR_ACS_PARAM_TYPE
                            struAcsParam.dwSize = CType(Marshal.SizeOf(struAcsParam), UInteger)
                            struAcsParam.dwParamType = 4096 ' (struAcsParam.dwParamType Or (1 + 12))
                            Dim dwSize As UInteger = CType(Marshal.SizeOf(struAcsParam), UInteger)
                            Dim ptrAcsParam As IntPtr = Marshal.AllocHGlobal(CType(dwSize, Integer))
                            Marshal.StructureToPtr(struAcsParam, ptrAcsParam, False)
                            If Not CHCNetSDK.NET_DVR_ControlGateway(lUserID, 1, 0) Then
                                Dim listItem As ListViewItem = New ListViewItem
                                listItem.Text = "FAIL"
                                Dim strTemp As String = String.Format("NET_DVR_CLEAR_ACS_PARAM FAIL, ERROR CODE {0}", CHCNetSDK.NET_DVR_GetLastError)
                                listItem.SubItems.Add(strTemp)
                                'Me.AddList(listViewMessage, listItem, True)
                                XtraMessageBox.Show(ulf, "<size=10>Failed To Close Door Lock</size>", "Failed")
                            Else
                                Dim listItem As ListViewItem = New ListViewItem
                                listItem.Text = "SUCC"
                                listItem.SubItems.Add("NET_DVR_CLEAR_ACS_PARAM SUCC")
                                'Me.AddList(listViewMessage, listItem, True)
                                XtraMessageBox.Show(ulf, "<size=10>Task Completed</size>", "Success")
                            End If

                            Marshal.FreeHGlobal(ptrAcsParam)
                        End If
                    End If
                End If
            Next
            For Each c As Control In Me.Controls
                c.Enabled = True
            Next
            TextPassword.Text = ""
            Me.Cursor = Cursors.Default
            XtraMessageBox.Show(ulf, "<size=10>Task Completed</size>", "<size=9>Information</size>")
        End If
    End Sub
    'hikvision Nava logic
    'Private Sub ProcessUserInfoSearchCallback(ByVal dwType As UInteger, ByVal lpBuffer As IntPtr, ByVal dwBufLen As UInteger, ByVal pUserData As IntPtr)
    '    If (pUserData = Nothing) Then
    '        Return
    '    End If

    '    If (dwType = CType(CHCNetSDK.NET_SDK_CALLBACK_TYPE.NET_SDK_CALLBACK_TYPE_STATUS, UInteger)) Then
    '        Dim dwStatus As UInteger = CType(Marshal.ReadInt32(lpBuffer), UInteger)
    '        If (dwStatus = CType(CHCNetSDK.NET_SDK_CALLBACK_STATUS_NORMAL.NET_SDK_CALLBACK_STATUS_FAILED, UInteger)) Then
    '            CHCNetSDK.PostMessage(pUserData, 1004, 1, 0)
    '        ElseIf (dwStatus = CType(CHCNetSDK.NET_SDK_CALLBACK_STATUS_NORMAL.NET_SDK_CALLBACK_STATUS_EXCEPTION, UInteger)) Then
    '            CHCNetSDK.PostMessage(pUserData, 1004, 2, 0)
    '        ElseIf (dwStatus = CType(CHCNetSDK.NET_SDK_CALLBACK_STATUS_NORMAL.NET_SDK_CALLBACK_STATUS_PROCESSING, UInteger)) Then

    '        Else
    '            CHCNetSDK.PostMessage(pUserData, 1004, 3, 0)
    '        End If

    '    ElseIf (dwType = CType(CHCNetSDK.NET_SDK_CALLBACK_TYPE.NET_SDK_CALLBACK_TYPE_DATA, UInteger)) Then
    '        Dim bUserInfoSearch() As Byte = New Byte(((1024 * 10)) - 1) {}
    '        Marshal.Copy(lpBuffer, bUserInfoSearch, 0, bUserInfoSearch.Length)
    '        Dim strUserInfoSearch As String = System.Text.Encoding.UTF8.GetString(bUserInfoSearch)
    '        m_JsonUserInfoSearch = New CUserInfoSearch
    '        m_JsonUserInfoSearch = JsonConvert.DeserializeObject(Of CUserInfoSearch)(strUserInfoSearch)
    '        If (m_JsonUserInfoSearch.UserInfoSearch Is Nothing) Then
    '            CHCNetSDK.PostMessage(pUserData, 1004, 3, 0)
    '            Return
    '        End If

    '        If (m_JsonUserInfoSearch.UserInfoSearch.responseStatusStrg <> "NO MATCH") Then
    '            CHCNetSDK.PostMessage(pUserData, 1006, 0, 0)
    '            'CHCNetSDK.SendMessage(pUserData, 1006, 0, 0)
    '        End If

    '        If (m_JsonUserInfoSearch.UserInfoSearch.responseStatusStrg <> "MORE") Then
    '            CHCNetSDK.PostMessage(pUserData, 1004, 4, 0)
    '            Return
    '        End If

    '        m_iSearchPosition = (m_iSearchPosition + m_JsonUserInfoSearch.UserInfoSearch.numOfMatches)
    '        CHCNetSDK.PostMessage(pUserData, 1005, 0, 0)
    '    End If

    '    Return
    'End Sub
    'Public Delegate Sub DefWndProcCallback(ByRef m As System.Windows.Forms.Message)
    'Protected Overrides Sub DefWndProc(ByRef m As System.Windows.Forms.Message)
    '    If InvokeRequired = True Then
    '        Dim d As DefWndProcCallback = New DefWndProcCallback(AddressOf Me.DefWndProc)
    '        Me.Invoke(d, New Object() {m})
    '        'Dim tmp = IN_OUT
    '    Else
    '        Select Case (m.Msg)
    '            Case 1001
    '                Dim iErrorMsg As Integer = m.WParam.ToInt32

    '                If iErrorMsg = 1 Then
    '                    Dim listItem As ListViewItem = New ListViewItem()
    '                    listItem.Text = "Fail"
    '                    Dim strTemp As String = String.Format("NET_SDK_CALLBACK_STATUS_FAILED")
    '                    listItem.SubItems.Add(strTemp)
    '                    'Me.AddList(listViewMessage, listItem)
    '                ElseIf iErrorMsg = 2 Then
    '                    Dim listItem As ListViewItem = New ListViewItem()
    '                    listItem.Text = "Fail"
    '                    Dim strTemp As String = String.Format("NET_SDK_CALLBACK_STATUS_EXCEPTION")
    '                    listItem.SubItems.Add(strTemp)
    '                    'Me.AddList(listViewMessage, listItem)
    '                ElseIf iErrorMsg = 3 Then
    '                    Dim listItem As ListViewItem = New ListViewItem()
    '                    listItem.Text = "Fail"
    '                    Dim strTemp As String = String.Format("NET_SDK_CALLBACK_OTHER_ERROR")
    '                    listItem.SubItems.Add(strTemp)
    '                    'Me.AddList(listViewMessage, listItem)
    '                ElseIf iErrorMsg = 4 Then
    '                    Dim listItem As ListViewItem = New ListViewItem()
    '                    listItem.Text = "Succ"
    '                    Dim strTemp As String = String.Format("NET_SDK_CALLBACK_FINISH")
    '                    listItem.SubItems.Add(strTemp)
    '                    'Me.AddList(listViewMessage, listItem)
    '                End If

    '                If -1 <> m_lUserInfoRecordHandle Then

    '                    If CHCNetSDK.NET_DVR_StopRemoteConfig(m_lUserInfoRecordHandle) Then
    '                        m_lUserInfoRecordHandle = -1
    '                    End If
    '                End If

    '            Case 1002
    '                'SendUserInfo()
    '            Case 1003
    '                Dim iError As Integer = m.WParam.ToInt32()

    '                If iError = 1 Then
    '                    Dim listItem As ListViewItem = New ListViewItem()
    '                    listItem.Text = "Succ"
    '                    Dim strTemp As String = String.Format("Send Success")
    '                    listItem.SubItems.Add(strTemp)
    '                    'Me.AddList(listViewMessage, listItem)
    '                Else
    '                    Dim listItem As ListViewItem = New ListViewItem()
    '                    listItem.Text = "Fail"
    '                    Dim strTemp As String = String.Format("Send Failed")
    '                    listItem.SubItems.Add(strTemp)
    '                    'Me.AddList(listViewMessage, listItem)
    '                End If

    '            Case 1004
    '                Dim iErrorSer As Integer = m.WParam.ToInt32()

    '                If iErrorSer = 1 Then
    '                    Dim listItem As ListViewItem = New ListViewItem()
    '                    listItem.Text = "Fail"
    '                    Dim strTemp As String = String.Format("NET_SDK_CALLBACK_STATUS_FAILED")
    '                    listItem.SubItems.Add(strTemp)
    '                    'Me.AddList(listViewMessage, listItem)
    '                ElseIf iErrorSer = 2 Then
    '                    Dim listItem As ListViewItem = New ListViewItem()
    '                    listItem.Text = "Fail"
    '                    Dim strTemp As String = String.Format("NET_SDK_CALLBACK_STATUS_EXCEPTION")
    '                    listItem.SubItems.Add(strTemp)
    '                    'Me.AddList(listViewMessage, listItem)
    '                ElseIf iErrorSer = 3 Then
    '                    Dim listItem As ListViewItem = New ListViewItem()
    '                    listItem.Text = "Fail"
    '                    Dim strTemp As String = String.Format("NET_SDK_CALLBACK_OTHER_ERROR")
    '                    listItem.SubItems.Add(strTemp)
    '                    'Me.AddList(listViewMessage, listItem)
    '                ElseIf iErrorSer = 4 Then
    '                    Dim listItem As ListViewItem = New ListViewItem()
    '                    listItem.Text = "Succ"
    '                    Dim strTemp As String = String.Format("NET_SDK_CALLBACK_FINISH")
    '                    listItem.SubItems.Add(strTemp)
    '                    'Me.AddList(listViewMessage, listItem)
    '                End If

    '                'If -1 <> m_lUserInfoSearchHandle Then

    '                '    If CHCNetSDK.NET_DVR_StopRemoteConfig(m_lUserInfoSearchHandle) Then
    '                '        m_lUserInfoSearchHandle = -1
    '                '    End If
    '                'End If
    '                'Case 1104  ' all download done
    '                If -1 <> m_lUserInfoSearchHandle Then

    '                    If CHCNetSDK.NET_DVR_StopRemoteConfig(m_lUserInfoSearchHandle) Then
    '                        m_lUserInfoSearchHandle = -1
    '                    End If
    '                End If
    '                'paycodeArray = paycodelist.Distinct.ToArray
    '                'Common.CreateEmployee(paycodeArray)
    '                ''Dim cn As Common = New Common
    '                ''cn.HikvisionLogOut(lUserID)

    '                'If Common.servername = "Access" Then
    '                '    Me.Fptable1TableAdapter1.Fill(Me.SSSDBDataSet.fptable1)
    '                '    GridControl2.DataSource = SSSDBDataSet.fptable1
    '                'Else
    '                '    FptableTableAdapter.Connection.ConnectionString = Common.ConnectionString
    '                '    Me.FptableTableAdapter.Fill(Me.SSSDBDataSet.fptable)
    '                '    GridControl2.DataSource = SSSDBDataSet.fptable
    '                'End If
    '                'GridView2.RefreshData()
    '                'Application.DoEvents()
    '                'For Each c As Control In Me.Controls
    '                '    c.Enabled = True
    '                'Next
    '                'XtraMasterTest.LabelControlStatus.Text = ""
    '                'Application.DoEvents()
    '                'XtraMessageBox.Show(ulf, "<size=10>Task Completed</size>", "<size=9>Information</size>")
    '            Case 1005
    '                SearchUserInfo()
    '            Case 1006
    '                saveHikvision(m_JsonUserInfoSearch)
    '                For i As Integer = 0 To m_JsonUserInfoSearch.UserInfoSearch.numOfMatches - 1
    '                    '    paycodelist.Add(m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).employeeNo & ";" & m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).name)
    '                    ''listViewStaffManage.BeginUpdate()
    '                    'Dim listItem As ListViewItem = New ListViewItem()
    '                    'listItem.Text = (m_iUserCount + 1).ToString()
    '                    'listItem.SubItems.Add(m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).employeeNo.ToString())

    '                    'If m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).name IsNot Nothing Then
    '                    '    listItem.SubItems.Add(m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).name)
    '                    'Else
    '                    '    listItem.SubItems.Add("")
    '                    'End If

    '                    'If m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).userType IsNot Nothing Then
    '                    '    listItem.SubItems.Add(m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).userType)
    '                    'Else
    '                    '    listItem.SubItems.Add("")
    '                    'End If

    '                    'If m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).closeDelayEnabled = True Then
    '                    '    listItem.SubItems.Add("True")
    '                    'Else
    '                    '    listItem.SubItems.Add("False")
    '                    'End If

    '                    'If (m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).Valid Is Nothing) Then
    '                    '    listItem.SubItems.Add("")
    '                    '    listItem.SubItems.Add("")
    '                    'ElseIf m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).Valid.enable = True Then

    '                    '    If m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).Valid.beginTime IsNot Nothing Then
    '                    '        listItem.SubItems.Add(m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).Valid.beginTime)
    '                    '    Else
    '                    '        listItem.SubItems.Add("")
    '                    '    End If

    '                    '    If m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).Valid.endTime IsNot Nothing Then
    '                    '        listItem.SubItems.Add(m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).Valid.endTime)
    '                    '    Else
    '                    '        listItem.SubItems.Add("")
    '                    '    End If
    '                    'Else
    '                    '    listItem.SubItems.Add("")
    '                    '    listItem.SubItems.Add("")
    '                    'End If

    '                    'If m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).belongGroup IsNot Nothing Then
    '                    '    listItem.SubItems.Add(m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).belongGroup)
    '                    'Else
    '                    '    listItem.SubItems.Add("")
    '                    'End If

    '                    'If m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).password IsNot Nothing Then
    '                    '    listItem.SubItems.Add(m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).password)
    '                    'Else
    '                    '    listItem.SubItems.Add("")
    '                    'End If

    '                    'If m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).doorRight IsNot Nothing Then
    '                    '    listItem.SubItems.Add(m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).doorRight)
    '                    'Else
    '                    '    listItem.SubItems.Add("")
    '                    'End If

    '                    'If m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).RightPlan IsNot Nothing Then
    '                    '    Dim sRightPlan As String = ""

    '                    '    For j As Integer = 0 To m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).RightPlan.Count - 1
    '                    '        sRightPlan = sRightPlan & "Door" & m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).RightPlan(j).doorNo & ":" + m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).RightPlan(j).planTemplateNo & "-"
    '                    '    Next

    '                    '    listItem.SubItems.Add(sRightPlan)
    '                    'Else
    '                    '    listItem.SubItems.Add("")
    '                    'End If

    '                    'listItem.SubItems.Add(m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).maxOpenDoorTime.ToString())
    '                    'listItem.SubItems.Add(m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).roomNumber.ToString())
    '                    'listItem.SubItems.Add(m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).floorNumber.ToString())

    '                    'If m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).doubleLockRight = True Then
    '                    '    listItem.SubItems.Add("True")
    '                    'Else
    '                    '    listItem.SubItems.Add("False")
    '                    'End If

    '                    'If m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).alwaysOpenRight = True Then
    '                    '    listItem.SubItems.Add("True")
    '                    'Else
    '                    '    listItem.SubItems.Add("False")
    '                    'End If

    '                    'If m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).localUIRight = True Then
    '                    '    listItem.SubItems.Add("True")
    '                    'Else
    '                    '    listItem.SubItems.Add("False")
    '                    'End If

    '                    'If m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).userVerifyMode IsNot Nothing Then
    '                    '    listItem.SubItems.Add(m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).userVerifyMode)
    '                    'Else
    '                    '    listItem.SubItems.Add("")
    '                    'End If

    '                    'If (m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).Valid Is Nothing) Then
    '                    '    listItem.SubItems.Add("")
    '                    'ElseIf m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).Valid.enable = True Then

    '                    '    If m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).Valid.timeType IsNot Nothing Then
    '                    '        listItem.SubItems.Add(m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).Valid.timeType)
    '                    '    Else
    '                    '        listItem.SubItems.Add("")
    '                    '    End If
    '                    'Else
    '                    '    listItem.SubItems.Add("")
    '                    'End If

    '                    'listItem.SubItems.Add(m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).openDoorTime.ToString())
    '                    ''listViewStaffManage.Items.Add(listItem)
    '                    ''listViewStaffManage.EndUpdate()
    '                    m_iUserCount += 1
    '                Next

    '            Case 1007
    '                'Dim iErrorMsg2 As Integer = m.WParam.ToInt32()

    '                'If iErrorMsg2 = 1 Then
    '                '    Dim listItem As ListViewItem = New ListViewItem()
    '                '    listItem.Text = "Fail"
    '                '    Dim strTemp As String = String.Format("NET_SDK_CALLBACK_STATUS_FAILED")
    '                '    listItem.SubItems.Add(strTemp)
    '                '    'Me.AddList(listViewMessage, listItem)

    '                '    If -1 <> m_lUserInfoDeleteHandle Then

    '                '        If CHCNetSDK.NET_DVR_StopRemoteConfig(m_lUserInfoDeleteHandle) Then
    '                '            m_lUserInfoDeleteHandle = -1
    '                '        End If
    '                '    End If
    '                'ElseIf iErrorMsg2 = 2 Then
    '                '    Dim listItem As ListViewItem = New ListViewItem()
    '                '    listItem.Text = "Fail"
    '                '    Dim strTemp As String = String.Format("NET_SDK_CALLBACK_STATUS_EXCEPTION")
    '                '    listItem.SubItems.Add(strTemp)
    '                '    'Me.AddList(listViewMessage, listItem)

    '                '    If -1 <> m_lUserInfoDeleteHandle Then

    '                '        If CHCNetSDK.NET_DVR_StopRemoteConfig(m_lUserInfoDeleteHandle) Then
    '                '            m_lUserInfoDeleteHandle = -1
    '                '        End If
    '                '    End If
    '                'ElseIf iErrorMsg2 = 3 Then
    '                '    Dim listItem As ListViewItem = New ListViewItem()
    '                '    listItem.Text = "Fail"
    '                '    Dim strTemp As String = String.Format("NET_SDK_CALLBACK_OTHER_ERROR")
    '                '    listItem.SubItems.Add(strTemp)
    '                '    'Me.AddList(listViewMessage, listItem)

    '                '    If -1 <> m_lUserInfoDeleteHandle Then

    '                '        If CHCNetSDK.NET_DVR_StopRemoteConfig(m_lUserInfoDeleteHandle) Then
    '                '            m_lUserInfoDeleteHandle = -1
    '                '        End If
    '                '    End If
    '                'ElseIf iErrorMsg2 = 4 Then
    '                '    Dim listItem As ListViewItem = New ListViewItem()
    '                '    listItem.Text = "Succ"
    '                '    Dim strTemp As String = String.Format("Delete Processing...")
    '                '    listItem.SubItems.Add(strTemp)
    '                '    Me.AddList(listViewMessage, listItem)
    '                'ElseIf iErrorMsg2 = 5 Then
    '                '    Dim listItem As ListViewItem = New ListViewItem()
    '                '    listItem.Text = "Succ"
    '                '    Dim strTemp As String = String.Format("Delete Success...")
    '                '    listItem.SubItems.Add(strTemp)
    '                '    Me.AddList(listViewMessage, listItem)

    '                '    If -1 <> m_lUserInfoDeleteHandle Then

    '                '        If CHCNetSDK.NET_DVR_StopRemoteConfig(m_lUserInfoDeleteHandle) Then
    '                '            m_lUserInfoDeleteHandle = -1
    '                '        End If
    '                '    End If
    '                'ElseIf iErrorMsg2 = 6 Then
    '                '    Dim listItem As ListViewItem = New ListViewItem()
    '                '    listItem.Text = "Fail"
    '                '    Dim strTemp As String = String.Format("Delete Failed...")
    '                '    listItem.SubItems.Add(strTemp)
    '                '    Me.AddList(listViewMessage, listItem)

    '                '    If -1 <> m_lUserInfoDeleteHandle Then

    '                '        If CHCNetSDK.NET_DVR_StopRemoteConfig(m_lUserInfoDeleteHandle) Then
    '                '            m_lUserInfoDeleteHandle = -1
    '                '        End If
    '                '    End If
    '                'End If

    '            Case Else
    '                MyBase.DefWndProc(m)
    '        End Select
    '    End If
    'End Sub
    'Private Function SearchUserInfo() As Boolean
    '    If -1 = m_lUserInfoSearchHandle Then
    '        Return False
    '    End If

    '    Dim JsonUserInfoSearchCond As CUserInfoSearchCond = New CUserInfoSearchCond()
    '    JsonUserInfoSearchCond.UserInfoSearchCond = New CUserInfoSearchCondContent()
    '    JsonUserInfoSearchCond.UserInfoSearchCond.EmployeeNoList = New List(Of CEmployeeNoList)()
    '    JsonUserInfoSearchCond.UserInfoSearchCond.searchID = "1"
    '    JsonUserInfoSearchCond.UserInfoSearchCond.searchResultPosition = m_iSearchPosition
    '    JsonUserInfoSearchCond.UserInfoSearchCond.maxResults = 10

    '    If Not m_bSearchAll Then

    '        'For Each item As ListViewItem In Me.listViewEmployeeNo.Items
    '        '    If item.SubItems(1).Text <> "" Then
    '        '        Dim singleEmployeeNoList As CEmployeeNoList = New CEmployeeNoList()
    '        '        singleEmployeeNoList.employeeNo = item.SubItems(1).Text
    '        '        JsonUserInfoSearchCond.UserInfoSearchCond.EmployeeNoList.Add(singleEmployeeNoList)
    '        '    End If
    '        'Next
    '    End If

    '    Dim strUserInfoSearchCond As String = JsonConvert.SerializeObject(JsonUserInfoSearchCond)
    '    Dim ptrUserInfoSearchCond As IntPtr = Marshal.StringToHGlobalAnsi(strUserInfoSearchCond)

    '    If Not CHCNetSDK.NET_DVR_SendRemoteConfig(m_lUserInfoSearchHandle, CInt(CHCNetSDK.LONG_CFG_SEND_DATA_TYPE_ENUM.ENUM_SEND_JSON_DATA), ptrUserInfoSearchCond, CUInt(strUserInfoSearchCond.Length)) Then
    '        Dim listItem As ListViewItem = New ListViewItem()
    '        listItem.Text = "Fail"
    '        Dim strTemp As String = Nothing
    '        strTemp = String.Format("Search Fail")
    '        listItem.SubItems.Add(strTemp)
    '        'Me.AddList(listViewMessage, listItem)
    '        Marshal.FreeHGlobal(ptrUserInfoSearchCond)
    '        Return False
    '    End If

    '    Dim listItemSucc As ListViewItem = New ListViewItem()
    '    listItemSucc.Text = "Succ"
    '    Dim strTempSucc As String = Nothing
    '    strTempSucc = String.Format("Search Processing")
    '    listItemSucc.SubItems.Add(strTempSucc)
    '    'Me.AddList(listViewMessage, listItemSucc)
    '    Marshal.FreeHGlobal(ptrUserInfoSearchCond)
    '    Return True
    'End Function
    Private Sub GetHFace(ByVal CardNo As String, Name As String, Privilege As Integer, Password As String, serialNo As String)
        If (m_lGetFaceCfgHandle <> -1) Then
            CHCNetSDK.NET_DVR_StopRemoteConfig(m_lGetFaceCfgHandle)
            m_lGetFaceCfgHandle = -1
        End If

        'If (Not (pictureBoxFace.Image) Is Nothing) Then
        '    pictureBoxFace.Image.Dispose()
        '    pictureBoxFace.Image = Nothing
        'End If

        Dim sURL As String = "POST /ISAPI/Intelligent/FDLib/FDSearch?format=json"
        Dim ptrURL As IntPtr = Marshal.StringToHGlobalAnsi(sURL)
        m_lGetFaceCfgHandle = CHCNetSDK.NET_DVR_StartRemoteConfig(m_lUserID, CHCNetSDK.NET_DVR_FACE_DATA_SEARCH, ptrURL, sURL.Length, Nothing, IntPtr.Zero)
        If (m_lGetFaceCfgHandle = -1) Then
            Marshal.FreeHGlobal(ptrURL)
            'MessageBox.Show(("NET_DVR_StartRemoteConfig fail [url:PUT /ISAPI/AccessControl/UserInfo/SetUp?format=json] error:" + CHCNetSDK.NET_DVR_GetLastError))
            Return
        End If

        Marshal.FreeHGlobal(ptrURL)
        Dim JsonSearchFaceDataCond As CSearchFaceDataCond = New CSearchFaceDataCond
        JsonSearchFaceDataCond.searchResultPosition = 0
        JsonSearchFaceDataCond.maxResults = 1
        JsonSearchFaceDataCond.faceLibType = "blackFD"
        JsonSearchFaceDataCond.FDID = "1"
        JsonSearchFaceDataCond.FPID = CardNo
        'Dim strJsonSearchFaceDataCond As String = JsonConvert.SerializeObject(JsonSearchFaceDataCond, Formatting.Indented, New JsonSerializerSettings() {JsonSerializerSettings.NullValueHandling = NullValueHandling.Ignore})
        Dim strJsonSearchFaceDataCond As String = JsonConvert.SerializeObject(JsonSearchFaceDataCond, Formatting.Indented, New JsonSerializerSettings With {.NullValueHandling = NullValueHandling.Ignore})
        Dim ptrJsonSearchFaceDataCond As IntPtr = Marshal.StringToHGlobalAnsi(strJsonSearchFaceDataCond)
        Dim struJsonDataCfg As CHCNetSDK.NET_DVR_JSON_DATA_CFG = New CHCNetSDK.NET_DVR_JSON_DATA_CFG
        struJsonDataCfg.dwSize = CType(Marshal.SizeOf(struJsonDataCfg), UInteger)
        Dim ptrJsonDataCfg As IntPtr = Marshal.AllocHGlobal(CType(struJsonDataCfg.dwSize, Integer))
        Marshal.StructureToPtr(struJsonDataCfg, ptrJsonDataCfg, False)
        Dim dwState As Integer = CType(CHCNetSDK.NET_SDK_GET_NEXT_STATUS_SUCCESS, Integer)
        Dim dwReturned As UInteger = 0

        While True
            dwState = CHCNetSDK.NET_DVR_SendWithRecvRemoteConfig(m_lGetFaceCfgHandle, ptrJsonSearchFaceDataCond, CType(strJsonSearchFaceDataCond.Length, UInteger), ptrJsonDataCfg, CType(Marshal.SizeOf(struJsonDataCfg), UInteger), dwReturned)
            If (dwState = CHCNetSDK.NET_SDK_GET_NEXT_STATUS_SUCCESS) Then
                ProcessFaceData(ptrJsonDataCfg, CardNo, Name, Privilege, Password, serialNo)
                Exit While
            ElseIf (dwState = CHCNetSDK.NET_SDK_GET_NEXT_STATUS_FAILED) Then
                'MessageBox.Show(("FAILED error:" + CHCNetSDK.NET_DVR_GetLastError))
                Exit While
            Else
                'MessageBox.Show(("exception error:" + CHCNetSDK.NET_DVR_GetLastError))
                Exit While
            End If
        End While

        If (m_lGetFaceCfgHandle > 0) Then
            CHCNetSDK.NET_DVR_StopRemoteConfig(m_lGetFaceCfgHandle)
            m_lGetFaceCfgHandle = -1
        End If

        Marshal.FreeHGlobal(ptrJsonSearchFaceDataCond)
        Marshal.FreeHGlobal(ptrJsonDataCfg)
    End Sub
    Private Sub ProcessFaceData(ByVal lpBuffer As IntPtr, CardNo As String, name As String, Privilege As Integer, Password As String, serialNo As String)
        Dim m_struJsonDataCfg As CHCNetSDK.NET_DVR_JSON_DATA_CFG = CType(Marshal.PtrToStructure(lpBuffer, GetType(CHCNetSDK.NET_DVR_JSON_DATA_CFG)), CHCNetSDK.NET_DVR_JSON_DATA_CFG)
        Dim strSearchFaceDataReturn As String = Marshal.PtrToStringAnsi(CType(m_struJsonDataCfg.lpJsonData, IntPtr), CType(m_struJsonDataCfg.dwJsonDataSize, Integer))
        Dim m_JsonSearchFaceDataReturn As CSearchFaceDataReturn
        m_JsonSearchFaceDataReturn = JsonConvert.DeserializeObject(Of CSearchFaceDataReturn)(strSearchFaceDataReturn)
        'If (Not (pictureBoxFace.Image) Is Nothing) Then
        '    pictureBoxFace.Image.Dispose()
        '    pictureBoxFace.Image = Nothing
        'End If

        If (m_JsonSearchFaceDataReturn.totalMatches = 0) Then
            'MessageBox.Show("not exist face")
            Return
        End If

        Try
            Dim CardNumber As String = CardNo.Trim(vbNullChar)
            CardNumber = CardNumber.Trim(vbNullChar)
            'Dim imgPath As String = "UserFace\" & CardNumber.Trim(vbNullChar) & ".jpg"

            'Dim strpath As String = String.Format("FacePicture.jpg")
            'Dim fs As FileStream = New FileStream(imgPath, FileMode.OpenOrCreate)
            Dim FaceLen As Integer = CType(m_struJsonDataCfg.dwPicDataSize, Integer)
            Dim by() As Byte = New Byte((FaceLen) - 1) {}
            Marshal.Copy(m_struJsonDataCfg.lpPicData, by, 0, FaceLen)
            'StrFromPtr(m_struJsonDataCfg.lpPicData)
            'fs.Write(by, 0, FaceLen)
            'fs.Close()
            'Dim faceTmp As String = Convert.ToBase64String(by)
            Dim faceTmp As String = System.Text.Encoding.ASCII.GetString(by)
            Try
                Dim con1 As OleDbConnection
                Dim con As SqlConnection
                If Common.servername = "Access" Then
                    con1 = New OleDbConnection(Common.ConnectionString)
                Else
                    con = New SqlConnection(Common.ConnectionString)
                End If
                Dim ds As DataSet = New DataSet
                Dim adap As SqlDataAdapter
                Dim adapA As OleDbDataAdapter

                XtraMasterTest.LabelControlStatus.Text = "Downloading Template " & CardNumber
                Application.DoEvents()
                Dim FingerNumber As Integer = 50
                Dim SrTmp As String = serialNo.Trim(vbNullChar)
                Dim Sql1 As String = "select * from fptable Where  [EMachineNumber] = " & SrTmp & " and EnrollNumber ='" & CardNumber & "' and FingerNumber=" & FingerNumber & ""
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(Sql1, con1)
                    adapA.Fill(ds)
                Else
                    adap = New SqlDataAdapter(Sql1, con)
                    adap.Fill(ds)
                End If

                If ds.Tables(0).Rows.Count > 0 Then
                    Sql1 = "update fptable set UserName='" & name & "', Template_Face=@Template_Face, Privilege=" & Privilege & " , Password='" & Password & "' where EMachineNumber = '" & SrTmp & "' and EnrollNumber ='" & CardNumber & "' and FingerNumber=" & FingerNumber & " and DeviceType='HKSeries'"
                Else
                    Sql1 = "insert into fptable (Emachinenumber, EnrollNumber, UserName, CardNumber, FingerNumber, Template_Face,Privilege, Password, DeviceType) values ('" & SrTmp & "','" & CardNumber & "', '" & name & "','', " & FingerNumber & ",@Template_Face, " & Privilege & ",'" & Password & "','HKSeries')"
                End If
                If Common.servername = "Access" Then
                    If con1.State <> ConnectionState.Open Then
                        con1.Open()
                    End If
                    If ds.Tables(0).Rows.Count > 0 Then
                        Sql1 = "update fptable set UserName='" & name & "', Template_Face=@Template_Face, Privilege=" & Privilege & " where EMachineNumber = '" & SrTmp & "' and EnrollNumber ='" & CardNumber & "' and FingerNumber=" & FingerNumber & " and DeviceType='HKSeries'"
                    Else
                        Sql1 = "insert into fptable (Emachinenumber, EnrollNumber, UserName, CardNumber, FingerNumber, Template_Face,Privilege, DeviceType) values ('" & SrTmp & "','" & CardNumber & "', '" & name & "','', " & FingerNumber & ",@Template_Face, " & Privilege & ",'HKSeries')"
                    End If 'Using stream As New IO.MemoryStream
                    Dim cmd As OleDbCommand = New OleDbCommand(Sql1, con1)
                    cmd.Parameters.AddWithValue("@Template_Face", by)
                    'cmd.Parameters.AddWithValue("@Password", OleDb.OleDbType.VarChar).Value = Password
                    cmd.ExecuteNonQuery()
                    'End Using
                    If con1.State <> ConnectionState.Closed Then
                        con1.Close()
                    End If
                Else
                    If con.State <> ConnectionState.Open Then
                        con.Open()
                    End If
                    Dim cmd As SqlCommand
                    cmd = New SqlCommand(Sql1, con)
                    cmd.Parameters.AddWithValue("@Template_Face", by)
                    cmd.ExecuteNonQuery()
                    If con.State <> ConnectionState.Closed Then
                        con.Close()
                    End If
                End If

            Catch ex As Exception

            End Try
            'pictureBoxFace.Image = Image.FromFile(strpath)
            'textBoxFilePath.Text = String.Format("{0}\{1}", Environment.CurrentDirectory, strpath)
        Catch ex As System.Exception
            CHCNetSDK.NET_DVR_StopRemoteConfig(m_lGetFaceCfgHandle)
            'MessageBox.Show("ProcessFingerData failed", "Error", MessageBoxButtons.OK)
        End Try

    End Sub
    Private Sub GetCard(EmployeeNo As String, name As String, Privilege As Integer, serialNo As String)
        If m_lGetCardCfgHandle <> -1 Then

            If CHCNetSDK.NET_DVR_StopRemoteConfig(m_lGetCardCfgHandle) Then
                m_lGetCardCfgHandle = -1
            End If
        End If

        Dim sURL As String = "POST /ISAPI/AccessControl/CardInfo/Search?format=json"
        Dim ptrURL As IntPtr = Marshal.StringToHGlobalAnsi(sURL)
        m_lGetCardCfgHandle = CHCNetSDK.NET_DVR_StartRemoteConfig(m_lUserID, CHCNetSDK.NET_DVR_JSON_CONFIG, ptrURL, sURL.Length, Nothing, IntPtr.Zero)

        If m_lGetCardCfgHandle < 0 Then
            'MessageBox.Show("NET_DVR_StartRemoteConfig fail [url:POST /ISAPI/AccessControl/CardInfo/Search?format=json] error:" & CHCNetSDK.NET_DVR_GetLastError())
            Marshal.FreeHGlobal(ptrURL)
            Return
        Else
            Dim JsonCardInfoSearchCondCfg As CCardInfoSearchCondCfg = New CCardInfoSearchCondCfg()
            JsonCardInfoSearchCondCfg.CardInfoSearchCond = New CCardInfoSearchCond()
            JsonCardInfoSearchCondCfg.CardInfoSearchCond.searchID = "1"
            JsonCardInfoSearchCondCfg.CardInfoSearchCond.searchResultPosition = 0
            JsonCardInfoSearchCondCfg.CardInfoSearchCond.maxResults = 10
            JsonCardInfoSearchCondCfg.CardInfoSearchCond.EmployeeNoList = New List(Of CEmployeeNoList)()
            Dim singleEmployeeNoList As CEmployeeNoList = New CEmployeeNoList()
            singleEmployeeNoList.employeeNo = EmployeeNo
            JsonCardInfoSearchCondCfg.CardInfoSearchCond.EmployeeNoList.Add(singleEmployeeNoList)
            'Dim strCardInfoSearchCondCfg As String = JsonConvert.SerializeObject(JsonCardInfoSearchCondCfg, Formatting.Indented, New JsonSerializerSettings With {
            '    .DefaultValueHandling = DefaultValueHandling.Ignore
            '})
            Dim strCardInfoSearchCondCfg As String = JsonConvert.SerializeObject(JsonCardInfoSearchCondCfg, Formatting.Indented, New JsonSerializerSettings With {.NullValueHandling = NullValueHandling.Ignore})

            Dim ptrCardInfoSearchCondCfg As IntPtr = Marshal.StringToHGlobalAnsi(strCardInfoSearchCondCfg)
            Dim ptrJsonData As IntPtr = Marshal.AllocHGlobal(1024)

            For i As Integer = 0 To 1024 - 1
                Marshal.WriteByte(ptrJsonData, i, 0)
            Next

            Dim dwState As Integer = 0
            Dim dwReturned As UInteger = 0

            While True
                dwState = CHCNetSDK.NET_DVR_SendWithRecvRemoteConfig(m_lGetCardCfgHandle, ptrCardInfoSearchCondCfg, CUInt(strCardInfoSearchCondCfg.Length), ptrJsonData, 1024, dwReturned)
                Dim strJsonData As String = Marshal.PtrToStringAnsi(ptrJsonData)

                If dwState = CInt(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_NEEDWAIT) Then
                    Thread.Sleep(10)
                    Continue While
                ElseIf dwState = CInt(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_FAILED) Then
                    'MessageBox.Show("Get Card Fail error:" & CHCNetSDK.NET_DVR_GetLastError())
                    Exit While
                ElseIf dwState = CInt(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_SUCCESS) Then
                    Dim JsonCardInfoSearchCfg As CCardInfoSearchCfg = New CCardInfoSearchCfg()
                    JsonCardInfoSearchCfg = JsonConvert.DeserializeObject(Of CCardInfoSearchCfg)(strJsonData)

                    If JsonCardInfoSearchCfg.CardInfoSearch Is Nothing Then
                        Dim JsonResponseStatus As CResponseStatus = New CResponseStatus()
                        JsonResponseStatus = JsonConvert.DeserializeObject(Of CResponseStatus)(strJsonData)

                        If JsonResponseStatus.statusCode = 1 Then
                            'MessageBox.Show("Get Card Success")
                        Else
                            'MessageBox.Show("Get Card Fail, ResponseStatus.statusCode" & JsonResponseStatus.statusCode)
                        End If
                    Else

                        If JsonCardInfoSearchCfg.CardInfoSearch.totalMatches = 0 Then
                            'MessageBox.Show("There is no this card")
                            Exit While
                        End If
                        Dim SrTmp As String = serialNo.Trim(vbNullChar)
                        For i As Integer = 0 To JsonCardInfoSearchCfg.CardInfoSearch.numOfMatches - 1

                            Dim con1 As OleDbConnection
                            Dim con As SqlConnection
                            If Common.servername = "Access" Then
                                con1 = New OleDbConnection(Common.ConnectionString)
                            Else
                                con = New SqlConnection(Common.ConnectionString)
                            End If
                            Dim ds As DataSet = New DataSet
                            Dim adap As SqlDataAdapter
                            Dim adapA As OleDbDataAdapter
                            Dim sSql As String = "select * from fptable where [EMachineNumber]=" & SrTmp & " and EnrollNumber ='" & EmployeeNo & "' and FingerNumber=11"
                            Dim dsRecord As DataSet = New DataSet
                            If Common.servername = "Access" Then
                                adapA = New OleDbDataAdapter(sSql, con1)
                                adapA.Fill(dsRecord)
                            Else
                                adap = New SqlDataAdapter(sSql, con)
                                adap.Fill(dsRecord)
                            End If
                            Dim CardNumber As String = JsonCardInfoSearchCfg.CardInfoSearch.CardInfo(i).cardNo
                            If dsRecord.Tables(0).Rows.Count = 0 Then
                                sSql = "insert into fptable (Emachinenumber, EnrollNumber, UserName, CardNumber, FingerNumber, Privilege,DeviceType) values (" & serialNo.Trim(vbNullChar) & ",'" & EmployeeNo & "', '" & name & "','" & CardNumber.Trim(vbNullChar) & "', '11', " & Privilege & ",'HKSeries')"
                            Else
                                sSql = "update fptable set CardNumber='" & CardNumber.Trim(vbNullChar) & "' , UserName='" & name & "', Privilege=" & Privilege & " where EnrollNumber='" & EmployeeNo & "' and Emachinenumber =" & serialNo.Trim(vbNullChar) & " and FingerNumber=11 and DeviceType='HKSeries'"
                            End If
                            If Common.servername = "Access" Then
                                If con1.State <> ConnectionState.Open Then
                                    con1.Open()
                                End If
                                Dim cmd As OleDbCommand
                                cmd = New OleDbCommand(sSql, con1)
                                cmd.ExecuteNonQuery()
                                If con1.State <> ConnectionState.Closed Then
                                    con1.Close()
                                End If
                            Else
                                If con.State <> ConnectionState.Open Then
                                    con.Open()
                                End If
                                Dim cmd As SqlCommand
                                cmd = New SqlCommand(sSql, con)
                                cmd.ExecuteNonQuery()
                                If con.State <> ConnectionState.Closed Then
                                    con.Close()
                                End If
                            End If

                            'textBoxEmployeeNo.Text = JsonCardInfoSearchCfg.CardInfoSearch.CardInfo(i).employeeNo
                            'textBoxCardNo.Text = JsonCardInfoSearchCfg.CardInfoSearch.CardInfo(i).cardNo

                            'If JsonCardInfoSearchCfg.CardInfoSearch.CardInfo(i).cardType = "normalCard" Then
                            '    comboBoxCardType.SelectedIndex = 0
                            'ElseIf JsonCardInfoSearchCfg.CardInfoSearch.CardInfo(i).cardType = "patrolCard" Then
                            '    comboBoxCardType.SelectedIndex = 1
                            'ElseIf JsonCardInfoSearchCfg.CardInfoSearch.CardInfo(i).cardType = "hijackCard" Then
                            '    comboBoxCardType.SelectedIndex = 2
                            'ElseIf JsonCardInfoSearchCfg.CardInfoSearch.CardInfo(i).cardType = "superCard" Then
                            '    comboBoxCardType.SelectedIndex = 3
                            'ElseIf JsonCardInfoSearchCfg.CardInfoSearch.CardInfo(i).cardType = "dismissingCard" Then
                            '    comboBoxCardType.SelectedIndex = 4
                            'End If

                            'Exit For
                        Next

                        'MessageBox.Show("Get Card Success")
                    End If

                    Exit While
                ElseIf dwState = CInt(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_EXCEPTION) Then
                    'MessageBox.Show("Get Card Exception error:" & CHCNetSDK.NET_DVR_GetLastError())
                    Exit While
                Else
                    'MessageBox.Show("unknown Status Error:" & CHCNetSDK.NET_DVR_GetLastError())
                    Exit While
                End If
            End While

            If m_lGetCardCfgHandle <> -1 Then
                CHCNetSDK.NET_DVR_StopRemoteConfig(m_lGetCardCfgHandle)
                m_lGetCardCfgHandle = -1
            End If

            Marshal.FreeHGlobal(ptrCardInfoSearchCondCfg)
            Marshal.FreeHGlobal(ptrJsonData)
            Marshal.FreeHGlobal(ptrURL)
        End If
    End Sub
    Private Sub SendCardData(EmployeeNo As String, CardNo As String)
        Dim JsonCardInfo As CCardInfoCfg = New CCardInfoCfg()
        JsonCardInfo.CardInfo = New CCardInfo()
        JsonCardInfo.CardInfo.employeeNo = EmployeeNo
        JsonCardInfo.CardInfo.cardNo = CardNo
        JsonCardInfo.CardInfo.cardType = "normalCard" 'comboBoxCardType.Text
        'JsonCardInfo.CardInfo.deleteCard = False
        Dim strJsonCardInfo As String = JsonConvert.SerializeObject(JsonCardInfo, Formatting.Indented, New JsonSerializerSettings With {
            .DefaultValueHandling = DefaultValueHandling.Ignore
        })
        'Dim strJsonCardInfo As String = JsonConvert.SerializeObject(JsonCardInfo, Formatting.Indented, New JsonSerializerSettings With {.NullValueHandling = NullValueHandling.Ignore})

        Dim ptrJsonCardInfo As IntPtr = Marshal.StringToHGlobalAnsi(strJsonCardInfo)
        Dim ptrJsonData As IntPtr = Marshal.AllocHGlobal(1024)

        For i As Integer = 0 To 1024 - 1
            Marshal.WriteByte(ptrJsonData, i, 0)
        Next

        Dim dwState As Integer = 0
        Dim dwReturned As UInteger = 0

        While True
            dwState = CHCNetSDK.NET_DVR_SendWithRecvRemoteConfig(m_lSetCardCfgHandle, ptrJsonCardInfo, CUInt(strJsonCardInfo.Length), ptrJsonData, 1024, dwReturned)
            Dim strJsonData As String = Marshal.PtrToStringAnsi(ptrJsonData)

            If dwState = CInt(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_NEEDWAIT) Then
                Thread.Sleep(10)
                Continue While
            ElseIf dwState = CInt(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_FAILED) Then
                'MessageBox.Show("Set Card Fail error:" & CHCNetSDK.NET_DVR_GetLastError())
                Exit While
            ElseIf dwState = CInt(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_SUCCESS) Then
                Dim JsonResponseStatus As CResponseStatus = New CResponseStatus()
                JsonResponseStatus = JsonConvert.DeserializeObject(Of CResponseStatus)(strJsonData)

                If JsonResponseStatus.statusCode = 1 Then
                    'MessageBox.Show("Set Card Success")
                Else
                    'MessageBox.Show("Set Card Fail, ResponseStatus.statusCode:" & JsonResponseStatus.statusCode)
                End If

                Exit While
            ElseIf dwState = CInt(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_EXCEPTION) Then
                'MessageBox.Show("Set Card Exception error:" & CHCNetSDK.NET_DVR_GetLastError())
                Exit While
            Else
                'MessageBox.Show("unknown Status error:" & CHCNetSDK.NET_DVR_GetLastError())
                Exit While
            End If
        End While

        If m_lSetCardCfgHandle <> -1 Then

            If CHCNetSDK.NET_DVR_StopRemoteConfig(m_lSetCardCfgHandle) Then
                m_lSetCardCfgHandle = -1
            End If
        End If

        Marshal.FreeHGlobal(ptrJsonCardInfo)
        Marshal.FreeHGlobal(ptrJsonData)
    End Sub
    Private Sub GetHFp(ByVal EnrollNumber As String, name As String, ByVal Privilege As Integer, serialNo As String)
        'Thread.Sleep(5000)  '5000
        If (m_lGetFingerPrintCfgHandle <> -1) Then
            CHCNetSDK.NET_DVR_StopRemoteConfig(CType(m_lGetFingerPrintCfgHandle, Integer))
        End If

        Dim sURL As String = "POST /ISAPI/AccessControl/FingerPrintUpload?format=json"
        Thread.Sleep(500)
        Dim ptrURL As IntPtr = Marshal.StringToHGlobalAnsi(sURL)

        m_lGetFingerPrintCfgHandle = CHCNetSDK.NET_DVR_StartRemoteConfig(m_lUserID, CHCNetSDK.NET_DVR_JSON_CONFIG, ptrURL, sURL.Length, Nothing, IntPtr.Zero)
        If (m_lGetFingerPrintCfgHandle < 0) Then
            'MessageBox.Show(("NET_DVR_StartRemoteConfig fail [url:POST /ISAPI/AccessControl/FingerPrintUpload?format=json] error:" + CHCNetSDK.NET_DVR_GetLastError))
            Marshal.FreeHGlobal(ptrURL)
            Return
        Else
            Thread.Sleep(50)
            Dim JsonFingerPrintCondCfg As CFingerPrintCondCfg = New CFingerPrintCondCfg
            JsonFingerPrintCondCfg.FingerPrintCond = New CFingerPrintCond
            JsonFingerPrintCondCfg.FingerPrintCond.searchID = "1"
            JsonFingerPrintCondCfg.FingerPrintCond.employeeNo = EnrollNumber
            Dim cardReaderNo As Integer = 1 'hard code...
            JsonFingerPrintCondCfg.FingerPrintCond.cardReaderNo = cardReaderNo
            Dim figerPrintID As Integer = 1 'hard code... should be 0 to 9  
            JsonFingerPrintCondCfg.FingerPrintCond.fingerPrintID = figerPrintID
            'Dim strFingerPrintCondCfg As String = JsonConvert.SerializeObject(JsonFingerPrintCondCfg, Formatting.Indented, New JsonSerializerSettings() {DefaultValueHandling = DefaultValueHandling.Ignore})
            Dim strFingerPrintCondCfg As String = JsonConvert.SerializeObject(JsonFingerPrintCondCfg, Formatting.Indented, New JsonSerializerSettings With {
                .DefaultValueHandling = DefaultValueHandling.Ignore})
            Dim ptrFingerPrintCondCfg As IntPtr = Marshal.StringToHGlobalAnsi(strFingerPrintCondCfg)
            Dim ptrJsonData As IntPtr = Marshal.AllocHGlobal(1024)
            Dim j As Integer = 0
            Do While (j < 1024)
                Marshal.WriteByte(ptrJsonData, j, 0)
                j = (j + 1)
            Loop

            Dim dwState As Integer = 0
            Dim dwReturned As UInteger = 0

            While True
                dwState = CHCNetSDK.NET_DVR_SendWithRecvRemoteConfig(m_lGetFingerPrintCfgHandle, ptrFingerPrintCondCfg, CType(strFingerPrintCondCfg.Length, UInteger), ptrJsonData, 1024, dwReturned)
                Dim strJsonData As String = Marshal.PtrToStringAnsi(ptrJsonData)
                If (dwState = CType(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_NEEDWAIT, Integer)) Then
                    Thread.Sleep(10)
                    'TODO: Warning!!! continue If
                ElseIf (dwState = CType(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_FAILED, Integer)) Then
                    'MessageBox.Show(("Get Card Fail error:" + CHCNetSDK.NET_DVR_GetLastError))
                ElseIf (dwState = CType(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_SUCCESS, Integer)) Then
                    Dim JsonFingerPrintInfoCfg As CFingerPrintInfoCfg = New CFingerPrintInfoCfg
                    JsonFingerPrintInfoCfg = JsonConvert.DeserializeObject(Of CFingerPrintInfoCfg)(strJsonData)
                    If (JsonFingerPrintInfoCfg.FingerPrintInfo Is Nothing) Then
                        Dim JsonResponseStatus As CResponseStatus = New CResponseStatus
                        JsonResponseStatus = JsonConvert.DeserializeObject(Of CResponseStatus)(strJsonData)
                        If (JsonResponseStatus.statusCode = 1) Then
                            'MessageBox.Show("Get Finger Success")
                        Else
                            'MessageBox.Show(("Get FingerPrint Fail, ResponseStatus.statusCode" + JsonResponseStatus.statusCode))
                        End If

                    Else
                        If (JsonFingerPrintInfoCfg.FingerPrintInfo.status = "NoFP") Then
                            'MessageBox.Show("not exist fingerPrint")
                            Exit While
                        End If
                        Dim con1 As OleDbConnection
                        Dim con As SqlConnection
                        If Common.servername = "Access" Then
                            con1 = New OleDbConnection(Common.ConnectionString)
                        Else
                            con = New SqlConnection(Common.ConnectionString)
                        End If
                        Dim ds As DataSet = New DataSet
                        Dim adap As SqlDataAdapter
                        Dim adapA As OleDbDataAdapter
                        For i As Integer = 0 To i < JsonFingerPrintInfoCfg.FingerPrintInfo.FingerPrintList.Count - 1
                            Try
                                Dim bytes() As Byte = Convert.FromBase64String(JsonFingerPrintInfoCfg.FingerPrintInfo.FingerPrintList(i).fingerData)
                                'Dim FingerTmp As String = System.Text.Encoding.ASCII.GetString(bytes)
                                Dim FingerNumber As Integer = i

                                Dim SrTmp As String = serialNo.Trim(vbNullChar)
                                Dim Sql1 As String = "select * from fptable Where  [EMachineNumber] = '" & SrTmp & "' and EnrollNumber ='" & EnrollNumber & "' and FingerNumber=" & FingerNumber & ""
                                ds = New DataSet
                                If Common.servername = "Access" Then
                                    adapA = New OleDbDataAdapter(Sql1, con1)
                                    adapA.Fill(ds)
                                Else
                                    adap = New SqlDataAdapter(Sql1, con)
                                    adap.Fill(ds)
                                End If

                                If ds.Tables(0).Rows.Count > 0 Then
                                    Sql1 = "update fptable set UserName='" & name & "', Template_Tw=@Template_Tw, Privilege=" & Privilege & " where EMachineNumber = '" & SrTmp & "' and EnrollNumber ='" & EnrollNumber & "' and FingerNumber=" & FingerNumber & " and DeviceType='HKSeries'"
                                Else
                                    Sql1 = "insert into fptable (Emachinenumber, EnrollNumber, UserName, CardNumber, FingerNumber, Template_Tw, Privilege, DeviceType) values ('" & SrTmp & "','" & EnrollNumber & "', '" & name & "','', " & FingerNumber & ",@Template_Tw, " & Privilege & ",'HKSeries')"
                                End If
                                If Common.servername = "Access" Then
                                    If con1.State <> ConnectionState.Open Then
                                        con1.Open()
                                    End If
                                    Dim cmd As OleDbCommand
                                    cmd = New OleDbCommand(Sql1, con1)
                                    cmd.Parameters.AddWithValue("@Template_Tw", bytes)
                                    cmd.ExecuteNonQuery()

                                    If con1.State <> ConnectionState.Closed Then
                                        con1.Close()
                                    End If
                                Else
                                    If con.State <> ConnectionState.Open Then
                                        con.Open()
                                    End If
                                    Dim cmd As SqlCommand
                                    cmd = New SqlCommand(Sql1, con)
                                    cmd.Parameters.AddWithValue("@Template_Tw", bytes)
                                    cmd.ExecuteNonQuery()
                                    If con.State <> ConnectionState.Closed Then
                                        con.Close()
                                    End If
                                End If

                                'Dim strPath As String = String.Format("{0}\{1}_{2}_{3} fingerprint.dat", Environment.CurrentDirectory, textBoxEmployeeNo.Text, textBoxCardReaderNo.Text, textBoxFingerID.Text)
                                'textBoxFingerData.Text = strPath
                                'Try
                                '    Dim fs As FileStream = New FileStream(strPath, FileMode.OpenOrCreate)
                                '    If Not File.Exists(strPath) Then
                                '        MessageBox.Show("FingerPrint storage file creat failed")
                                '    End If
                                '    Dim objBinaryWrite As BinaryWriter = New BinaryWriter(fs)
                                '    fs.Write(bytes, 0, bytes.Length)
                                '    fs.Close()
                            Catch ex As System.Exception

                            End Try
                        Next
                        'MessageBox.Show("Get FingerPrint Success")
                    End If

                    Exit While
                ElseIf (dwState = CType(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_EXCEPTION, Integer)) Then
                    'MessageBox.Show(("Get FingerPrint Exception error:" + CHCNetSDK.NET_DVR_GetLastError))
                    Exit While
                Else
                    'MessageBox.Show(("unknown Status Error:" + CHCNetSDK.NET_DVR_GetLastError))
                    Exit While
                End If
            End While
            If (m_lGetFingerPrintCfgHandle <> -1) Then
                CHCNetSDK.NET_DVR_StopRemoteConfig(CType(m_lGetFingerPrintCfgHandle, Integer))
                m_lGetFingerPrintCfgHandle = -1
            End If
            Marshal.FreeHGlobal(ptrFingerPrintCondCfg)
            Marshal.FreeHGlobal(ptrJsonData)
            Marshal.FreeHGlobal(ptrURL)
        End If
    End Sub
    Private Sub LoadFPGrid()
        Dim fptable As New DataTable("fptable")
        Dim gridselet As String '= "select val(tblMachine.ID_NO), tblMachine.A_R, tblMachine.IN_OUT,tblMachine.DeviceType,tblMachine.LOCATION,tblMachine.branch,tblMachine.commkey,tblMachine.MAC_ADDRESS,tblMachine.Purpose,tblMachine.LastModifiedBy,tblMachine.LastModifiedDate from tblMachine"
        gridselet = "select * from fptable where DeviceType='HKSeries'"
        If Common.servername = "Access" Then
            Dim dataAdapter As New OleDbDataAdapter(gridselet, Common.con1)
            dataAdapter.Fill(fptable)
        Else
            Dim dataAdapter As New SqlClient.SqlDataAdapter(gridselet, Common.con)
            dataAdapter.Fill(fptable)
        End If
        GridControl2.DataSource = fptable
    End Sub
    'end hikvision Nava logic
    Private Declare Function lstrlen Lib "kernel32.dll" Alias "lstrlenA" (ByVal lpString As String) As Long
    Private Declare Function SysAllocStringByteLen Lib "oleaut32" (ByVal Ptr As Long, ByVal Length As Long) As String
End Class