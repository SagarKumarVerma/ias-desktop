﻿Imports System.Resources
Imports System.Globalization
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports System.ComponentModel
Imports DevExpress.XtraGrid.Columns
Imports System.Data.SqlClient
Imports System.Text
Imports System.Data.OleDb
Imports DevExpress.XtraGrid.Views.Base
Imports DevExpress.XtraSplashScreen
Imports System.Runtime.InteropServices
Imports ULtra.AscDemo
Imports System.Threading
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq

Public Class XtraDeviceOfflineActication

    Public Delegate Sub invokeDelegate()
    Dim serialNo As String = ""
    'Dim m_lGetCardCfgHandle As Int32 = -1 'Hikvision
    'Dim g_fGetGatewayCardCallback As CHCNetSDK.RemoteConfigCallback = Nothing 'Hikvision

    'for getting user info (not having card number) added from staffmanagament.cs (Acs Demo)
    Dim m_lUserInfoSearchHandle As Integer = -1 'Hikvision
    Dim g_fUserInfoSearchCallback As CHCNetSDK.RemoteConfigCallback = Nothing 'Hikvision
    Dim m_JsonUserInfoSearch As CUserInfoSearch
    Public m_iSearchPosition As Integer = 0
    Public m_iUserCount As Integer = 0
    Public m_lUserInfoRecordHandle As Integer = -1
    Public m_lUserID As Int32 = -1
    Public m_lUserInfoDeleteHandle As Int32 = -1
    Public m_iUserInfoRecordIndex As Integer = 0
    Public m_bSearchAll As Boolean = True
    Public m_bDeleteAll As Boolean = True
    Public m_iUserInfoSettingHandle As Int32 = -1
    Dim lUserID As Integer = -1
    Dim paycodelist As New List(Of String)()
    Dim paycodeArray '= paycodelist.Distinct.ToArray ' paycodelist.ToArray
    'for getting user info (not having card number)


    Dim m_lGetFingerPrintCfgHandle As Integer = -1
    Dim HCardNos As List(Of String) = Nothing
    Dim iMaxCardNum As Integer = 1000
    Dim m_struFingerPrintOne As CHCNetSDK.NET_DVR_FINGER_PRINT_CFG_V50 = New CHCNetSDK.NET_DVR_FINGER_PRINT_CFG_V50
    Dim m_struDelFingerPrint As CHCNetSDK.NET_DVR_FINGER_PRINT_INFO_CTRL_BYCARD_V50 = New CHCNetSDK.NET_DVR_FINGER_PRINT_INFO_CTRL_BYCARD_V50
    Dim m_struRecordCardCfg() As CHCNetSDK.NET_DVR_FINGER_PRINT_CFG_V50 = New CHCNetSDK.NET_DVR_FINGER_PRINT_CFG_V50((iMaxCardNum) - 1) {}
    Dim m_struCardInfo() As CHCNetSDK.NET_DVR_CARD_CFG_V50 = New CHCNetSDK.NET_DVR_CARD_CFG_V50((iMaxCardNum) - 1) {}
    Dim g_fSetFingerPrintCallback As CHCNetSDK.RemoteConfigCallback = Nothing
    Dim g_fGetFingerPrintCallback As CHCNetSDK.RemoteConfigCallback = Nothing
    Dim g_fDelFingerPrintCallback As CHCNetSDK.RemoteConfigCallback = Nothing
    Dim g_fSetGatewayCardCallback As CHCNetSDK.RemoteConfigCallback = Nothing
    Dim m_lSetCardCfgHandle As Int32 = -1
    Dim m_bSendOne As Boolean = False
    Dim m_struSelSendCardCfg As CHCNetSDK.NET_DVR_CARD_CFG_V50
    Dim m_struNowSendCard As CHCNetSDK.NET_DVR_CARD_CFG_V50
    Dim m_BSendSel As Boolean = False
    Dim m_dwCardNum As UInteger = 0
    Dim m_dwSendIndex As UInteger = 0
    Dim m_lSetFingerPrintCfgHandle As Integer = -1
    Dim m_iSendIndex As Integer = -1

    Dim Downloading As Boolean = False

    Dim ulf As UserLookAndFeel
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand

    Public m_lGetUserCfgHandle As Int32 = -1
    Public m_lSetUserCfgHandle As Int32 = -1

    Public Sub New()
        InitializeComponent()
        Common.SetGridFont(GridView1, New Font("Tahoma", 10))
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True

        m_struFingerPrintOne.Init()
        m_struDelFingerPrint.struByCard.byFingerPrintID = New Byte((CHCNetSDK.MAX_FINGER_PRINT_NUM) - 1) {}
    End Sub
    Private Sub XtraDeviceOfflineActication_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        GridControl1.DataSource = Common.MachineNonAdmin
        GridView1.ClearSelection()
    End Sub
    Private Sub btnUploadFinger_Click(sender As System.Object, e As System.EventArgs) Handles btnUploadFinger.Click
        If GridView1.GetSelectedRows.Count = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>Please select the Machine</size>", "<size=9>Error</size>")
            Exit Sub
        End If
        Try


            Dim RowHandles() As Integer = GridView1.GetSelectedRows
            'Dim IP, ID_NO, DeviceType, A_R, Purpose As Object
            Dim SerialNoStr As String = ""
            For Each rowHandle As Integer In RowHandles
                Dim SerialNo As String = GridView1.GetRowCellValue(rowHandle, GridView1.Columns("MAC_ADDRESS"))
                SerialNoStr = SerialNoStr & "," & SerialNoStr.Trim(vbNullChar).Substring(SerialNoStr.Trim(vbNullChar).Length - 9)
            Next
            SerialNoStr = SerialNoStr.TrimStart(",")

            If System.IO.File.Exists("Device.lic") Then
                My.Computer.FileSystem.DeleteFile("Device.lic")
            End If
            'File.Create("UserKey.lic").Dispose()
            Dim fs As FileStream = New FileStream("Device.lic", FileMode.Create, FileAccess.Write)
            Dim sw As StreamWriter = New StreamWriter(fs)
            Dim str As String = ""
            sw.WriteLine(EncyDcry.Encrypt(license.HDDSerialNo, True))
            sw.Write(SerialNoStr)
            sw.Flush()
            sw.Close()
            fs.Close()

            Dim client As New Net.WebClient
            Dim sfd As New SaveFileDialog
            sfd.FileName = "Device.lic"
            sfd.ShowDialog()
            client.DownloadFile("Device.lic", sfd.FileName)
            Try
                My.Computer.FileSystem.DeleteFile("Device.lic")
            Catch ex As Exception
            End Try
        Catch ex As Exception
            XtraMessageBox.Show(ulf, "<size=10>" & ex.Message.Trim & "</size>", "<size=9>Ultra</size>")
        End Try
    End Sub
    Private Sub ProcessGetGatewayCardCallback(ByVal dwType As UInteger, ByVal lpBuffer As IntPtr, ByVal dwBufLen As UInteger, ByVal pUserData As IntPtr)
        If (pUserData = Nothing) Then
            Return
        End If
        If (dwType = CType(CHCNetSDK.NET_SDK_CALLBACK_TYPE.NET_SDK_CALLBACK_TYPE_DATA, UInteger)) Then
            Dim struCardCfg As CHCNetSDK.NET_DVR_CARD_CFG_V50 = New CHCNetSDK.NET_DVR_CARD_CFG_V50
            struCardCfg = CType(Marshal.PtrToStructure(lpBuffer, GetType(CHCNetSDK.NET_DVR_CARD_CFG_V50)), CHCNetSDK.NET_DVR_CARD_CFG_V50)
            Dim strCardNo As String = System.Text.Encoding.UTF8.GetString(struCardCfg.byCardNo)
            Dim pCardInfo As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(struCardCfg))
            Marshal.StructureToPtr(struCardCfg, pCardInfo, True)
            CHCNetSDK.PostMessage(pUserData, 1003, CType(pCardInfo, Integer), 0)
            'AddToCardList(struCardCfg, strCardNo);
        ElseIf (dwType = CType(CHCNetSDK.NET_SDK_CALLBACK_TYPE.NET_SDK_CALLBACK_TYPE_STATUS, UInteger)) Then
            Dim dwStatus As UInteger = CType(Marshal.ReadInt32(lpBuffer), UInteger)
            If (dwStatus = CType(CHCNetSDK.NET_SDK_CALLBACK_STATUS_NORMAL.NET_SDK_CALLBACK_STATUS_SUCCESS, UInteger)) Then
                Dim listItem As ListViewItem = New ListViewItem
                listItem.Text = "SUCC"
                listItem.SubItems.Add("NET_DVR_GET_CARD_CFG_V50 Get finish")
                'Me.AddList(listViewMessage, listItem, True)
                CHCNetSDK.PostMessage(pUserData, 1002, 0, 0)
            ElseIf (dwStatus = CType(CHCNetSDK.NET_SDK_CALLBACK_STATUS_NORMAL.NET_SDK_CALLBACK_STATUS_FAILED, UInteger)) Then
                Dim dwErrorCode As UInteger = CType(Marshal.ReadInt32((lpBuffer + 1)), UInteger)
                Dim cardNumber As String = Marshal.PtrToStringAnsi((lpBuffer + 2))
                Dim listItem As ListViewItem = New ListViewItem
                listItem.Text = "FAIL"
                listItem.SubItems.Add(String.Format("NET_DVR_GET_CARD_CFG_V50 Get Failed,ErrorCode:{0},CardNo:{1}", dwErrorCode, cardNumber))
                'Me.AddList(listViewMessage, listItem, True)
                CHCNetSDK.PostMessage(pUserData, 1002, 0, 0)
            End If
        End If
        Return
    End Sub
    'Public Delegate Sub DefWndProcCallback(ByRef m As System.Windows.Forms.Message)
    'Protected Overrides Sub DefWndProc(ByRef m As System.Windows.Forms.Message)
    '    If InvokeRequired = True Then
    '        Dim d As DefWndProcCallback = New DefWndProcCallback(AddressOf Me.DefWndProc)
    '        Me.Invoke(d, New Object() {m})
    '        'Dim tmp = IN_OUT
    '    Else
    '        Select Case (m.Msg)
    '            Case 1001
    '                Dim iErrorMsg As Integer = m.WParam.ToInt32
    '                If (-1 <> m_lSetCardCfgHandle) Then
    '                    If CHCNetSDK.NET_DVR_StopRemoteConfig(m_lSetCardCfgHandle) Then
    '                        m_lSetCardCfgHandle = -1
    '                    End If
    '                End If
    '                If (-1 <> m_lGetCardCfgHandle) Then
    '                    If CHCNetSDK.NET_DVR_StopRemoteConfig(m_lGetCardCfgHandle) Then
    '                        m_lGetCardCfgHandle = -1

    '                    End If
    '                End If
    '            Case 1002
    '                If Downloading = True Then
    '                    Downloading = False
    '                    'Dim msg As New Message("Success", "Download Finished")
    '                    'XtraMaster.AlertControl1.Show(XtraMaster, msg.Caption, msg.Text, "", msg.Image, msg)
    '                    'XtraMasterTest.LabelControlStatus.Text = ""
    '                    'Application.DoEvents()
    '                    'If Common.servername = "Access" Then
    '                    '    Me.Fptable1TableAdapter1.Fill(Me.SSSDBDataSet.fptable1)
    '                    '    GridControl2.DataSource = SSSDBDataSet.fptable1
    '                    'Else
    '                    '    FptableTableAdapter.Connection.ConnectionString = Common.ConnectionString
    '                    '    Me.FptableTableAdapter.Fill(Me.SSSDBDataSet.fptable)
    '                    '    GridControl2.DataSource = SSSDBDataSet.fptable
    '                    'End If
    '                    'GridView2.RefreshData()
    '                    'Application.DoEvents()
    '                    XtraMessageBox.Show(ulf, "<size=10>Task Completed</size>", "<size=9>Information</size>")
    '                End If

    '                'SendNextCard()
    '            Case 1003
    '                Dim pCardInfo As IntPtr = CType(m.WParam.ToInt32, IntPtr)
    '                Dim struCardCfg As CHCNetSDK.NET_DVR_CARD_CFG_V50 = New CHCNetSDK.NET_DVR_CARD_CFG_V50
    '                struCardCfg = CType(Marshal.PtrToStructure(pCardInfo, GetType(CHCNetSDK.NET_DVR_CARD_CFG_V50)), CHCNetSDK.NET_DVR_CARD_CFG_V50)
    '                Dim strCardNo As String = System.Text.Encoding.UTF8.GetString(struCardCfg.byCardNo)
    '                'AddToCardList(struCardCfg, strCardNo)
    '                'Thread.Sleep(200)
    '                saveHikvision(struCardCfg)
    '            Case Else
    '                MyBase.DefWndProc(m)
    '        End Select
    '    End If
    'End Sub
    Private Sub AddToCardList(ByVal struCardInfo As CHCNetSDK.NET_DVR_CARD_CFG_V50, ByVal strCardNo As String)
        Dim iItemIndex As Integer = 0 'GetExistItem(struCardInfo)
        If (-1 = iItemIndex) Then
            iItemIndex = 0 ' listViewGataManage.Items.Count
        End If
        'UpdateList(iItemIndex, strCardNo, struCardInfo)
        m_struCardInfo(iItemIndex) = struCardInfo
    End Sub
    'Public Delegate Sub SetTextCallbacksaveHikvision(ByVal struCardCfg As CHCNetSDK.NET_DVR_CARD_CFG_V50)
    'Private Sub saveHikvision(ByVal struCardCfg As CHCNetSDK.NET_DVR_CARD_CFG_V50)
    '    If InvokeRequired = True Then
    '        Dim d As SetTextCallbacksaveHikvision = New SetTextCallbacksaveHikvision(AddressOf Me.saveHikvision)
    '        Me.Invoke(d, New Object() {struCardCfg})
    '        'Dim tmp = IN_OUT
    '    Else
    '        Dim cn As Common = New Common
    '        Dim con1 As OleDbConnection
    '        Dim con As SqlConnection
    '        If Common.servername = "Access" Then
    '            con1 = New OleDbConnection(Common.ConnectionString)
    '        Else
    '            con = New SqlConnection(Common.ConnectionString)
    '        End If

    '        Dim EnrollNumber As String = System.Text.Encoding.UTF8.GetString(struCardCfg.byCardNo)
    '        'HCardNos.Add(EnrollNumber)
    '        GetHFp(EnrollNumber)
    '        If IsNumeric(EnrollNumber) Then
    '            EnrollNumber = Convert.ToDouble(EnrollNumber).ToString("000000000000")
    '        End If
    '        Dim DeviceType As String = "HKSeries"
    '        Dim Password As String = System.Text.Encoding.UTF8.GetString(struCardCfg.byCardPassword)

    '        Dim del As invokeDelegate = Function()
    '                                        XtraMasterTest.LabelControlStatus.Text = "Downloading Template " & EnrollNumber
    '                                    End Function
    '        Invoke(del)
    '        Dim paycodelist As New List(Of String)()
    '        paycodelist.Add(EnrollNumber & ";" & System.Text.Encoding.UTF8.GetString(struCardCfg.byName).Trim)
    '        Dim paycodeArray = paycodelist.Distinct.ToArray
    '        Common.CreateEmployee(paycodeArray)

    '        'Dim x As String = serialNo.ToString.Trim 'Replace("-", "")
    '        'Dim adap As SqlDataAdapter
    '        'Dim adapA As OleDbDataAdapter
    '        'Dim sSql As String = "select * from fptable where [EMachineNumber]='" & x & "' and EnrollNumber ='" & EnrollNumber & "' and FingerNumber=11 "
    '        'Dim dsRecord As DataSet = New DataSet
    '        'If Common.servername = "Access" Then
    '        '    adapA = New OleDbDataAdapter(sSql, con1)
    '        '    adapA.Fill(dsRecord)
    '        'Else
    '        '    adap = New SqlDataAdapter(sSql, con)
    '        '    adap.Fill(dsRecord)
    '        'End If
    '        'If dsRecord.Tables(0).Rows.Count = 0 Then
    '        '    sSql = "insert into fptable (Emachinenumber, EnrollNumber, UserName, CardNumber, FingerNumber, Privilege) values ('" & serialNo.Trim & "','" & EnrollNumber & "', '','', '11', '0')"
    '        '    If Common.servername = "Access" Then
    '        '        If Common.con1.State <> ConnectionState.Open Then
    '        '            Common.con1.Open()
    '        '        End If
    '        '        Dim cmd As OleDbCommand
    '        '        cmd = New OleDbCommand(sSql, con1)
    '        '        cmd.ExecuteNonQuery()

    '        '        If con1.State <> ConnectionState.Closed Then
    '        '            con1.Close()
    '        '        End If
    '        '    Else
    '        '        If con.State <> ConnectionState.Open Then
    '        '            con.Open()
    '        '        End If
    '        '        Dim cmd As SqlCommand
    '        '        cmd = New SqlCommand(sSql, con)
    '        '        cmd.ExecuteNonQuery()
    '        '        If con.State <> ConnectionState.Closed Then
    '        '            con.Close()
    '        '        End If
    '        '    End If

    '        '    'GridView2.RefreshData()
    '        '    If Common.servername = "Access" Then
    '        '        Me.Fptable1TableAdapter1.Fill(Me.SSSDBDataSet.fptable1)
    '        '        GridControl2.DataSource = SSSDBDataSet.fptable1
    '        '    Else
    '        '        FptableTableAdapter.Connection.ConnectionString = Common.ConnectionString
    '        '        Me.FptableTableAdapter.Fill(Me.SSSDBDataSet.fptable)
    '        '        GridControl2.DataSource = SSSDBDataSet.fptable
    '        '    End If
    '        '    Application.DoEvents()
    '        'End If

    '        'If IsNumeric(EnrollNumber) Then
    '        '    EnrollNumber = Convert.ToDouble(EnrollNumber)
    '        'End If
    '        'GetHFp(EnrollNumber)
    '    End If
    'End Sub
    Private Function SendNextCard() As Boolean
        If (-1 = m_lSetCardCfgHandle) Then
            Return False
        End If
        m_dwSendIndex = (m_dwSendIndex + 1)
        If (m_dwSendIndex >= m_dwCardNum) Then
            'CHCNetSDK.NET_DVR_StopRemoteConfig(m_lSetCardCfgHandle);
            'this.StopRemoteCfg(ref m_lSetCardCfgHandle);
            'm_lSetCardCfgHandle = -1;
            Dim listItem2 As ListViewItem = New ListViewItem
            listItem2.Text = "SUCC"
            Dim strTemp2 As String = Nothing
            strTemp2 = String.Format("Send {0} card(s) over", m_dwCardNum)
            listItem2.SubItems.Add(strTemp2)
            'Me.AddList(listViewMessage, listItem2, True)
            Return True
        End If

        m_struNowSendCard = m_struCardInfo(m_dwSendIndex)
        Dim dwSize As UInteger = CType(Marshal.SizeOf(m_struNowSendCard), UInteger)
        Dim ptrSendCard As IntPtr = Marshal.AllocHGlobal(CType(dwSize, Integer))
        Marshal.StructureToPtr(m_struNowSendCard, ptrSendCard, False)
        If Not CHCNetSDK.NET_DVR_SendRemoteConfig(m_lSetCardCfgHandle, 3, ptrSendCard, dwSize) Then
            Marshal.FreeHGlobal(ptrSendCard)
            Dim listItem3 As ListViewItem = New ListViewItem
            listItem3.Text = "FAIL"
            Dim strTemp3 As String = Nothing
            strTemp3 = String.Format("Send Fail,CardNO:{0}", System.Text.Encoding.UTF8.GetString(m_struNowSendCard.byCardNo).TrimEnd(Microsoft.VisualBasic.ChrW(92)))
            listItem3.SubItems.Add(strTemp3)
            'Me.AddList(listViewMessage, listItem3, True)           
            Return False
        End If

        Marshal.FreeHGlobal(ptrSendCard)
        Return True
    End Function
    'Private Sub GetHFp(ByVal CardNo As String)
    '    Thread.Sleep(500)
    '    If (m_lGetFingerPrintCfgHandle <> -1) Then
    '        CHCNetSDK.NET_DVR_StopRemoteConfig(CType(m_lGetFingerPrintCfgHandle, Integer))
    '    End If
    '    Dim struCond As CHCNetSDK.NET_DVR_FINGER_PRINT_INFO_COND_V50 = New CHCNetSDK.NET_DVR_FINGER_PRINT_INFO_COND_V50
    '    struCond.byCardNo = New Byte((32) - 1) {}
    '    struCond.byEmployeeNo = New Byte((32) - 1) {}
    '    struCond.dwSize = CType(Marshal.SizeOf(struCond), UInteger)
    '    'UInteger.TryParse(textBoxNumber.Text, struCond.dwFingerPrintNum)
    '    struCond.dwFingerPrintNum = 0
    '    Dim byTempEmployeeNo() As Byte = System.Text.Encoding.UTF8.GetBytes("")
    '    Dim i As Integer = 0
    '    Do While (i < byTempEmployeeNo.Length)
    '        struCond.byEmployeeNo(i) = byTempEmployeeNo(i)
    '        i = (i + 1)
    '    Loop
    '    Byte.TryParse("1", struCond.byFingerPrintID)
    '    Dim byTemp() As Byte = System.Text.Encoding.UTF8.GetBytes(CardNo)
    '    i = 0
    '    Do While (i < byTemp.Length)
    '        struCond.byCardNo(i) = byTemp(i)
    '        i = (i + 1)
    '    Loop
    '    GetTreeSel()
    '    struCond.byEnableCardReader = m_struFingerPrintOne.byEnableCardReader
    '    Dim dwSize As Integer = Marshal.SizeOf(struCond)
    '    Dim ptrStruCond As IntPtr = Marshal.AllocHGlobal(dwSize)
    '    Marshal.StructureToPtr(struCond, ptrStruCond, False)
    '    'Thread.Sleep(200)  'test
    '    g_fGetFingerPrintCallback = New CHCNetSDK.RemoteConfigCallback(AddressOf ProcessGetFingerPrintCfgCallbackData)
    '    m_lGetFingerPrintCfgHandle = CHCNetSDK.NET_DVR_StartRemoteConfig(m_lUserID, CHCNetSDK.NET_DVR_GET_FINGERPRINT_CFG_V50, ptrStruCond, dwSize, g_fGetFingerPrintCallback, Me.Handle)
    '    If (-1 = m_lGetFingerPrintCfgHandle) Then
    '        Dim listItem As ListViewItem = New ListViewItem
    '        listItem.Text = "FAIL"
    '        Dim strTemp As String = String.Format("NET_DVR_GET_FINGERPRINT_CFG_V50 FAIL, ERROR CODE {0}", CHCNetSDK.NET_DVR_GetLastError)
    '        listItem.SubItems.Add(strTemp)
    '        'Me.AddList(listViewMessage, listItem)
    '        Marshal.FreeHGlobal(ptrStruCond)
    '        Return
    '    Else
    '        Dim listItem As ListViewItem = New ListViewItem
    '        listItem.Text = "SUCC"
    '        listItem.SubItems.Add("NET_DVR_GET_FINGERPRINT_CFG_V50")
    '        'Me.AddList(listViewMessage, listItem)
    '        Marshal.FreeHGlobal(ptrStruCond)
    '    End If

    'End Sub
    'Public Delegate Sub ProcessGetFingerPrintCfgCallbackDataCallback(ByVal dwType As UInteger, ByVal lpBuffer As IntPtr, ByVal dwBufLen As UInteger, ByVal pUserData As IntPtr)
    'Private Sub ProcessGetFingerPrintCfgCallbackData(ByVal dwType As UInteger, ByVal lpBuffer As IntPtr, ByVal dwBufLen As UInteger, ByVal pUserData As IntPtr)
    '    'If InvokeRequired = True Then
    '    '    Dim d As ProcessGetFingerPrintCfgCallbackDataCallback = New ProcessGetFingerPrintCfgCallbackDataCallback(AddressOf Me.ProcessGetFingerPrintCfgCallbackData)
    '    '    Me.Invoke(d, New Object() {dwType, lpBuffer, dwBufLen, pUserData})
    '    '    'Dim tmp = IN_OUT
    '    'Else

    '    Thread.Sleep(50)
    '    If (pUserData = Nothing) Then
    '        Return
    '    End If
    '    If (dwType = CType(CHCNetSDK.NET_SDK_CALLBACK_TYPE.NET_SDK_CALLBACK_TYPE_DATA, UInteger)) Then
    '        Dim strFingerPrintCfg As CHCNetSDK.NET_DVR_FINGER_PRINT_CFG_V50 = New CHCNetSDK.NET_DVR_FINGER_PRINT_CFG_V50
    '        strFingerPrintCfg = CType(Marshal.PtrToStructure(lpBuffer, GetType(CHCNetSDK.NET_DVR_FINGER_PRINT_CFG_V50)), CHCNetSDK.NET_DVR_FINGER_PRINT_CFG_V50)
    '        'Marshal.PtrToStructure(lpBuffer, strCardCfg);
    '        Dim bSendOK As Boolean = False
    '        Dim i As Integer = 0
    '        Do While (i < strFingerPrintCfg.byEnableCardReader.Length)
    '            If (1 = strFingerPrintCfg.byEnableCardReader(i)) Then
    '                bSendOK = True
    '            End If
    '            i = (i + 1)
    '        Loop

    '        If Not bSendOK Then
    '        End If
    '        bSendOK = False
    '        i = 0
    '        Do While (i < strFingerPrintCfg.byLeaderFP.Length)
    '            If (1 = strFingerPrintCfg.byLeaderFP(i)) Then
    '                bSendOK = True
    '            End If
    '            i = (i + 1)
    '        Loop
    '        If Not bSendOK Then
    '        End If
    '        If (0 = strFingerPrintCfg.dwSize) Then
    '            Return
    '        End If
    '        'AddToFingerPrintList(strFingerPrintCfg, False)
    '        Dim con1 As OleDbConnection
    '        Dim con As SqlConnection
    '        If Common.servername = "Access" Then
    '            con1 = New OleDbConnection(Common.ConnectionString)
    '        Else
    '            con = New SqlConnection(Common.ConnectionString)
    '        End If
    '        Dim ds As DataSet = New DataSet
    '        Dim adap As SqlDataAdapter
    '        Dim adapA As OleDbDataAdapter
    '        Dim Template As String = Encoding.ASCII.GetString(strFingerPrintCfg.byFingerData)
    '        Dim EnrollNumber As String = Convert.ToDouble(System.Text.Encoding.UTF8.GetString(strFingerPrintCfg.byCardNo))

    '        XtraMasterTest.LabelControlStatus.Text = "Downloading Template " & EnrollNumber
    '        Application.DoEvents()

    '        Dim FingerNumber As String = strFingerPrintCfg.byFingerPrintID.ToString
    '        serialNo = serialNo.Trim
    '        Dim strPath As String = System.Environment.CurrentDirectory & "\FTemplate\" & EnrollNumber.ToString.Trim & "_" & serialNo.Trim.Substring(0, 5) & "fingerprint.dat"
    '        Dim fs As FileStream = New FileStream(strPath, FileMode.OpenOrCreate)
    '        If Not File.Exists(strPath) Then
    '            MessageBox.Show("Fingerprint storage file creat failed")
    '        End If
    '        Dim objBinaryWrite As BinaryWriter = New BinaryWriter(fs)
    '        fs.Write(strFingerPrintCfg.byFingerData, 0, CType(strFingerPrintCfg.dwFingerPrintLen, Integer))
    '        fs.Close()

    '        'Dim EMachineNumber As String = serialNo
    '        If IsNumeric(EnrollNumber) Then
    '            EnrollNumber = Convert.ToDouble(EnrollNumber).ToString("000000000000")
    '        End If
    '        'serialNo = "DS-K1T804MF-120170911V010100ENC13360348"
    '        Dim SrTmp As String = serialNo.Trim
    '        Dim Sql1 As String = "select * from fptable Where  [EMachineNumber] = '" & SrTmp.Trim & "' and EnrollNumber ='" & EnrollNumber & "' and FingerNumber=" & FingerNumber & ""
    '        Dim HTemplate As String
    '        If Common.servername = "Access" Then
    '            adapA = New OleDbDataAdapter(Sql1, con1)
    '            adapA.Fill(ds)
    '        Else
    '            adap = New SqlDataAdapter(Sql1, con)
    '            adap.Fill(ds)
    '        End If


    '        If ds.Tables(0).Rows.Count > 0 Then
    '            Sql1 = "update fptable set HTemplatePath='" & strPath & "' where EMachineNumber = '" & serialNo.Trim & "' and EnrollNumber ='" & EnrollNumber & "' and FingerNumber='" & FingerNumber & "' "
    '        Else
    '            Sql1 = "insert into fptable (Emachinenumber, EnrollNumber, UserName, CardNumber, FingerNumber, HTemplatePath) values ('" & serialNo.Trim & "','" & EnrollNumber & "', '','', '" & FingerNumber & "', '" & strPath & "')"
    '        End If
    '        If Common.servername = "Access" Then
    '            If con1.State <> ConnectionState.Open Then
    '                con1.Open()
    '            End If
    '            Dim cmd As OleDbCommand
    '            cmd = New OleDbCommand(Sql1, con1)
    '            cmd.ExecuteNonQuery()

    '            If con1.State <> ConnectionState.Closed Then
    '                con1.Close()
    '            End If
    '        Else
    '            If con.State <> ConnectionState.Open Then
    '                con.Open()
    '            End If
    '            Dim cmd As SqlCommand
    '            cmd = New SqlCommand(Sql1, con)
    '            cmd.ExecuteNonQuery()
    '            If con.State <> ConnectionState.Closed Then
    '                con.Close()
    '            End If
    '        End If
    '    ElseIf (dwType = CType(CHCNetSDK.NET_SDK_CALLBACK_TYPE.NET_SDK_CALLBACK_TYPE_STATUS, UInteger)) Then
    '        Dim dwStatus As UInteger = 0
    '        dwStatus = CType(Marshal.ReadInt32(lpBuffer), UInteger)
    '        If (dwStatus = CType(CHCNetSDK.NET_SDK_CALLBACK_STATUS_NORMAL.NET_SDK_CALLBACK_STATUS_SUCCESS, UInteger)) Then
    '            Dim listItem As ListViewItem = New ListViewItem
    '            listItem.Text = "SUCC"
    '            listItem.SubItems.Add("NET_DVR_GET_FINGERPRINT_CFG_V50 Get finish")
    '        ElseIf (dwStatus = CType(CHCNetSDK.NET_SDK_CALLBACK_STATUS_NORMAL.NET_SDK_CALLBACK_STATUS_FAILED, UInteger)) Then
    '            Dim listItem As ListViewItem = New ListViewItem
    '            listItem.Text = "FAIL"
    '            listItem.SubItems.Add("NET_DVR_GET_FINGERPRINT_CFG_V50 Get FAIL")
    '            'Me.AddList(listViewMessage, listItem)
    '        End If
    '    End If
    '    'End If
    'End Sub
    'Private Sub GetTreeSel()
    '    Dim i As Integer = 0
    '    Do While (i < 512)
    '        If i = 0 Then
    '            m_struFingerPrintOne.byEnableCardReader(i) = 1
    '        Else
    '            m_struFingerPrintOne.byEnableCardReader(i) = 0
    '        End If
    '        i = (i + 1)
    '    Loop
    '    i = 0
    '    Do While (i < 256)
    '        m_struFingerPrintOne.byLeaderFP(i) = 0
    '        i = (i + 1)
    '    Loop
    '    i = 0
    '    Do While (i < 10)
    '        m_struDelFingerPrint.struByCard.byFingerPrintID(i) = 0
    '        i = (i + 1)
    '    Loop
    'End Sub

    Private Sub SimpleButton1_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton1.Click
        Me.Close()
    End Sub
    'Private Sub ProcessUserInfoSearchCallback(ByVal dwType As UInteger, ByVal lpBuffer As IntPtr, ByVal dwBufLen As UInteger, ByVal pUserData As IntPtr)
    '    If (pUserData = Nothing) Then
    '        Return
    '    End If

    '    If (dwType = CType(CHCNetSDK.NET_SDK_CALLBACK_TYPE.NET_SDK_CALLBACK_TYPE_STATUS, UInteger)) Then
    '        Dim dwStatus As UInteger = CType(Marshal.ReadInt32(lpBuffer), UInteger)
    '        If (dwStatus = CType(CHCNetSDK.NET_SDK_CALLBACK_STATUS_NORMAL.NET_SDK_CALLBACK_STATUS_FAILED, UInteger)) Then
    '            CHCNetSDK.PostMessage(pUserData, 1004, 1, 0)
    '        ElseIf (dwStatus = CType(CHCNetSDK.NET_SDK_CALLBACK_STATUS_NORMAL.NET_SDK_CALLBACK_STATUS_EXCEPTION, UInteger)) Then
    '            CHCNetSDK.PostMessage(pUserData, 1004, 2, 0)
    '        ElseIf (dwStatus = CType(CHCNetSDK.NET_SDK_CALLBACK_STATUS_NORMAL.NET_SDK_CALLBACK_STATUS_PROCESSING, UInteger)) Then

    '        Else
    '            CHCNetSDK.PostMessage(pUserData, 1004, 3, 0)
    '        End If

    '    ElseIf (dwType = CType(CHCNetSDK.NET_SDK_CALLBACK_TYPE.NET_SDK_CALLBACK_TYPE_DATA, UInteger)) Then
    '        Dim bUserInfoSearch() As Byte = New Byte(((1024 * 10)) - 1) {}
    '        Marshal.Copy(lpBuffer, bUserInfoSearch, 0, bUserInfoSearch.Length)
    '        Dim strUserInfoSearch As String = System.Text.Encoding.UTF8.GetString(bUserInfoSearch)
    '        m_JsonUserInfoSearch = New CUserInfoSearch
    '        m_JsonUserInfoSearch = JsonConvert.DeserializeObject(Of CUserInfoSearch)(strUserInfoSearch)
    '        If (m_JsonUserInfoSearch.UserInfoSearch Is Nothing) Then
    '            CHCNetSDK.PostMessage(pUserData, 1004, 3, 0)
    '            Return
    '        End If

    '        If (m_JsonUserInfoSearch.UserInfoSearch.responseStatusStrg <> "NO MATCH") Then
    '            CHCNetSDK.PostMessage(pUserData, 1006, 0, 0)
    '            'CHCNetSDK.SendMessage(pUserData, 1006, 0, 0)
    '        End If

    '        If (m_JsonUserInfoSearch.UserInfoSearch.responseStatusStrg <> "MORE") Then
    '            CHCNetSDK.PostMessage(pUserData, 1004, 4, 0)
    '            Return
    '        End If

    '        m_iSearchPosition = (m_iSearchPosition + m_JsonUserInfoSearch.UserInfoSearch.numOfMatches)
    '        CHCNetSDK.PostMessage(pUserData, 1005, 0, 0)
    '    End If

    '    Return
    'End Sub
    'Public Delegate Sub DefWndProcCallback(ByRef m As System.Windows.Forms.Message)
    'Protected Overrides Sub DefWndProc(ByRef m As System.Windows.Forms.Message)
    '    If InvokeRequired = True Then
    '        Dim d As DefWndProcCallback = New DefWndProcCallback(AddressOf Me.DefWndProc)
    '        Me.Invoke(d, New Object() {m})
    '        'Dim tmp = IN_OUT
    '    Else
    '        Select Case (m.Msg)
    '            Case 1001
    '                Dim iErrorMsg As Integer = m.WParam.ToInt32

    '                If iErrorMsg = 1 Then
    '                    Dim listItem As ListViewItem = New ListViewItem()
    '                    listItem.Text = "Fail"
    '                    Dim strTemp As String = String.Format("NET_SDK_CALLBACK_STATUS_FAILED")
    '                    listItem.SubItems.Add(strTemp)
    '                    'Me.AddList(listViewMessage, listItem)
    '                ElseIf iErrorMsg = 2 Then
    '                    Dim listItem As ListViewItem = New ListViewItem()
    '                    listItem.Text = "Fail"
    '                    Dim strTemp As String = String.Format("NET_SDK_CALLBACK_STATUS_EXCEPTION")
    '                    listItem.SubItems.Add(strTemp)
    '                    'Me.AddList(listViewMessage, listItem)
    '                ElseIf iErrorMsg = 3 Then
    '                    Dim listItem As ListViewItem = New ListViewItem()
    '                    listItem.Text = "Fail"
    '                    Dim strTemp As String = String.Format("NET_SDK_CALLBACK_OTHER_ERROR")
    '                    listItem.SubItems.Add(strTemp)
    '                    'Me.AddList(listViewMessage, listItem)
    '                ElseIf iErrorMsg = 4 Then
    '                    Dim listItem As ListViewItem = New ListViewItem()
    '                    listItem.Text = "Succ"
    '                    Dim strTemp As String = String.Format("NET_SDK_CALLBACK_FINISH")
    '                    listItem.SubItems.Add(strTemp)
    '                    'Me.AddList(listViewMessage, listItem)
    '                End If

    '                If -1 <> m_lUserInfoRecordHandle Then

    '                    If CHCNetSDK.NET_DVR_StopRemoteConfig(m_lUserInfoRecordHandle) Then
    '                        m_lUserInfoRecordHandle = -1
    '                    End If
    '                End If

    '            Case 1002
    '                'SendUserInfo()
    '            Case 1003
    '                Dim iError As Integer = m.WParam.ToInt32()

    '                If iError = 1 Then
    '                    Dim listItem As ListViewItem = New ListViewItem()
    '                    listItem.Text = "Succ"
    '                    Dim strTemp As String = String.Format("Send Success")
    '                    listItem.SubItems.Add(strTemp)
    '                    'Me.AddList(listViewMessage, listItem)
    '                Else
    '                    Dim listItem As ListViewItem = New ListViewItem()
    '                    listItem.Text = "Fail"
    '                    Dim strTemp As String = String.Format("Send Failed")
    '                    listItem.SubItems.Add(strTemp)
    '                    'Me.AddList(listViewMessage, listItem)
    '                End If

    '            Case 1004
    '                Dim iErrorSer As Integer = m.WParam.ToInt32()

    '                If iErrorSer = 1 Then
    '                    Dim listItem As ListViewItem = New ListViewItem()
    '                    listItem.Text = "Fail"
    '                    Dim strTemp As String = String.Format("NET_SDK_CALLBACK_STATUS_FAILED")
    '                    listItem.SubItems.Add(strTemp)
    '                    'Me.AddList(listViewMessage, listItem)
    '                ElseIf iErrorSer = 2 Then
    '                    Dim listItem As ListViewItem = New ListViewItem()
    '                    listItem.Text = "Fail"
    '                    Dim strTemp As String = String.Format("NET_SDK_CALLBACK_STATUS_EXCEPTION")
    '                    listItem.SubItems.Add(strTemp)
    '                    'Me.AddList(listViewMessage, listItem)
    '                ElseIf iErrorSer = 3 Then
    '                    Dim listItem As ListViewItem = New ListViewItem()
    '                    listItem.Text = "Fail"
    '                    Dim strTemp As String = String.Format("NET_SDK_CALLBACK_OTHER_ERROR")
    '                    listItem.SubItems.Add(strTemp)
    '                    'Me.AddList(listViewMessage, listItem)
    '                ElseIf iErrorSer = 4 Then
    '                    Dim listItem As ListViewItem = New ListViewItem()
    '                    listItem.Text = "Succ"
    '                    Dim strTemp As String = String.Format("NET_SDK_CALLBACK_FINISH")
    '                    listItem.SubItems.Add(strTemp)
    '                    'Me.AddList(listViewMessage, listItem)
    '                End If

    '                If -1 <> m_lUserInfoSearchHandle Then

    '                    If CHCNetSDK.NET_DVR_StopRemoteConfig(m_lUserInfoSearchHandle) Then
    '                        m_lUserInfoSearchHandle = -1
    '                    End If
    '                End If
    '                paycodeArray = paycodelist.Distinct.ToArray
    '                Common.CreateEmployee(paycodeArray)
    '                Dim cn As Common = New Common
    '                cn.HikvisionLogOut(lUserID)
    '                Me.Close()
    '            Case 1005
    '                SearchUserInfo()
    '            Case 1006

    '                For i As Integer = 0 To m_JsonUserInfoSearch.UserInfoSearch.numOfMatches - 1
    '                    paycodelist.Add(m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).employeeNo & ";" & m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).name)
    '                    ''listViewStaffManage.BeginUpdate()
    '                    'Dim listItem As ListViewItem = New ListViewItem()
    '                    'listItem.Text = (m_iUserCount + 1).ToString()
    '                    'listItem.SubItems.Add(m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).employeeNo.ToString())

    '                    'If m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).name IsNot Nothing Then
    '                    '    listItem.SubItems.Add(m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).name)
    '                    'Else
    '                    '    listItem.SubItems.Add("")
    '                    'End If

    '                    'If m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).userType IsNot Nothing Then
    '                    '    listItem.SubItems.Add(m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).userType)
    '                    'Else
    '                    '    listItem.SubItems.Add("")
    '                    'End If

    '                    'If m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).closeDelayEnabled = True Then
    '                    '    listItem.SubItems.Add("True")
    '                    'Else
    '                    '    listItem.SubItems.Add("False")
    '                    'End If

    '                    'If (m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).Valid Is Nothing) Then
    '                    '    listItem.SubItems.Add("")
    '                    '    listItem.SubItems.Add("")
    '                    'ElseIf m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).Valid.enable = True Then

    '                    '    If m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).Valid.beginTime IsNot Nothing Then
    '                    '        listItem.SubItems.Add(m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).Valid.beginTime)
    '                    '    Else
    '                    '        listItem.SubItems.Add("")
    '                    '    End If

    '                    '    If m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).Valid.endTime IsNot Nothing Then
    '                    '        listItem.SubItems.Add(m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).Valid.endTime)
    '                    '    Else
    '                    '        listItem.SubItems.Add("")
    '                    '    End If
    '                    'Else
    '                    '    listItem.SubItems.Add("")
    '                    '    listItem.SubItems.Add("")
    '                    'End If

    '                    'If m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).belongGroup IsNot Nothing Then
    '                    '    listItem.SubItems.Add(m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).belongGroup)
    '                    'Else
    '                    '    listItem.SubItems.Add("")
    '                    'End If

    '                    'If m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).password IsNot Nothing Then
    '                    '    listItem.SubItems.Add(m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).password)
    '                    'Else
    '                    '    listItem.SubItems.Add("")
    '                    'End If

    '                    'If m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).doorRight IsNot Nothing Then
    '                    '    listItem.SubItems.Add(m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).doorRight)
    '                    'Else
    '                    '    listItem.SubItems.Add("")
    '                    'End If

    '                    'If m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).RightPlan IsNot Nothing Then
    '                    '    Dim sRightPlan As String = ""

    '                    '    For j As Integer = 0 To m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).RightPlan.Count - 1
    '                    '        sRightPlan = sRightPlan & "Door" & m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).RightPlan(j).doorNo & ":" + m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).RightPlan(j).planTemplateNo & "-"
    '                    '    Next

    '                    '    listItem.SubItems.Add(sRightPlan)
    '                    'Else
    '                    '    listItem.SubItems.Add("")
    '                    'End If

    '                    'listItem.SubItems.Add(m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).maxOpenDoorTime.ToString())
    '                    'listItem.SubItems.Add(m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).roomNumber.ToString())
    '                    'listItem.SubItems.Add(m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).floorNumber.ToString())

    '                    'If m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).doubleLockRight = True Then
    '                    '    listItem.SubItems.Add("True")
    '                    'Else
    '                    '    listItem.SubItems.Add("False")
    '                    'End If

    '                    'If m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).alwaysOpenRight = True Then
    '                    '    listItem.SubItems.Add("True")
    '                    'Else
    '                    '    listItem.SubItems.Add("False")
    '                    'End If

    '                    'If m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).localUIRight = True Then
    '                    '    listItem.SubItems.Add("True")
    '                    'Else
    '                    '    listItem.SubItems.Add("False")
    '                    'End If

    '                    'If m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).userVerifyMode IsNot Nothing Then
    '                    '    listItem.SubItems.Add(m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).userVerifyMode)
    '                    'Else
    '                    '    listItem.SubItems.Add("")
    '                    'End If

    '                    'If (m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).Valid Is Nothing) Then
    '                    '    listItem.SubItems.Add("")
    '                    'ElseIf m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).Valid.enable = True Then

    '                    '    If m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).Valid.timeType IsNot Nothing Then
    '                    '        listItem.SubItems.Add(m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).Valid.timeType)
    '                    '    Else
    '                    '        listItem.SubItems.Add("")
    '                    '    End If
    '                    'Else
    '                    '    listItem.SubItems.Add("")
    '                    'End If

    '                    'listItem.SubItems.Add(m_JsonUserInfoSearch.UserInfoSearch.UserInfo(i).openDoorTime.ToString())
    '                    ''listViewStaffManage.Items.Add(listItem)
    '                    ''listViewStaffManage.EndUpdate()
    '                    m_iUserCount += 1
    '                Next

    '            Case 1007
    '                'Dim iErrorMsg2 As Integer = m.WParam.ToInt32()

    '                'If iErrorMsg2 = 1 Then
    '                '    Dim listItem As ListViewItem = New ListViewItem()
    '                '    listItem.Text = "Fail"
    '                '    Dim strTemp As String = String.Format("NET_SDK_CALLBACK_STATUS_FAILED")
    '                '    listItem.SubItems.Add(strTemp)
    '                '    'Me.AddList(listViewMessage, listItem)

    '                '    If -1 <> m_lUserInfoDeleteHandle Then

    '                '        If CHCNetSDK.NET_DVR_StopRemoteConfig(m_lUserInfoDeleteHandle) Then
    '                '            m_lUserInfoDeleteHandle = -1
    '                '        End If
    '                '    End If
    '                'ElseIf iErrorMsg2 = 2 Then
    '                '    Dim listItem As ListViewItem = New ListViewItem()
    '                '    listItem.Text = "Fail"
    '                '    Dim strTemp As String = String.Format("NET_SDK_CALLBACK_STATUS_EXCEPTION")
    '                '    listItem.SubItems.Add(strTemp)
    '                '    'Me.AddList(listViewMessage, listItem)

    '                '    If -1 <> m_lUserInfoDeleteHandle Then

    '                '        If CHCNetSDK.NET_DVR_StopRemoteConfig(m_lUserInfoDeleteHandle) Then
    '                '            m_lUserInfoDeleteHandle = -1
    '                '        End If
    '                '    End If
    '                'ElseIf iErrorMsg2 = 3 Then
    '                '    Dim listItem As ListViewItem = New ListViewItem()
    '                '    listItem.Text = "Fail"
    '                '    Dim strTemp As String = String.Format("NET_SDK_CALLBACK_OTHER_ERROR")
    '                '    listItem.SubItems.Add(strTemp)
    '                '    'Me.AddList(listViewMessage, listItem)

    '                '    If -1 <> m_lUserInfoDeleteHandle Then

    '                '        If CHCNetSDK.NET_DVR_StopRemoteConfig(m_lUserInfoDeleteHandle) Then
    '                '            m_lUserInfoDeleteHandle = -1
    '                '        End If
    '                '    End If
    '                'ElseIf iErrorMsg2 = 4 Then
    '                '    Dim listItem As ListViewItem = New ListViewItem()
    '                '    listItem.Text = "Succ"
    '                '    Dim strTemp As String = String.Format("Delete Processing...")
    '                '    listItem.SubItems.Add(strTemp)
    '                '    Me.AddList(listViewMessage, listItem)
    '                'ElseIf iErrorMsg2 = 5 Then
    '                '    Dim listItem As ListViewItem = New ListViewItem()
    '                '    listItem.Text = "Succ"
    '                '    Dim strTemp As String = String.Format("Delete Success...")
    '                '    listItem.SubItems.Add(strTemp)
    '                '    Me.AddList(listViewMessage, listItem)

    '                '    If -1 <> m_lUserInfoDeleteHandle Then

    '                '        If CHCNetSDK.NET_DVR_StopRemoteConfig(m_lUserInfoDeleteHandle) Then
    '                '            m_lUserInfoDeleteHandle = -1
    '                '        End If
    '                '    End If
    '                'ElseIf iErrorMsg2 = 6 Then
    '                '    Dim listItem As ListViewItem = New ListViewItem()
    '                '    listItem.Text = "Fail"
    '                '    Dim strTemp As String = String.Format("Delete Failed...")
    '                '    listItem.SubItems.Add(strTemp)
    '                '    Me.AddList(listViewMessage, listItem)

    '                '    If -1 <> m_lUserInfoDeleteHandle Then

    '                '        If CHCNetSDK.NET_DVR_StopRemoteConfig(m_lUserInfoDeleteHandle) Then
    '                '            m_lUserInfoDeleteHandle = -1
    '                '        End If
    '                '    End If
    '                'End If

    '            Case Else
    '                MyBase.DefWndProc(m)
    '        End Select
    '    End If
    'End Sub
    'Private Function SearchUserInfo() As Boolean
    '    If -1 = m_lUserInfoSearchHandle Then
    '        Return False
    '    End If

    '    Dim JsonUserInfoSearchCond As CUserInfoSearchCond = New CUserInfoSearchCond()
    '    JsonUserInfoSearchCond.UserInfoSearchCond = New CUserInfoSearchCondContent()
    '    JsonUserInfoSearchCond.UserInfoSearchCond.EmployeeNoList = New List(Of CEmployeeNoList)()
    '    JsonUserInfoSearchCond.UserInfoSearchCond.searchID = "1"
    '    JsonUserInfoSearchCond.UserInfoSearchCond.searchResultPosition = m_iSearchPosition
    '    JsonUserInfoSearchCond.UserInfoSearchCond.maxResults = 10

    '    If Not m_bSearchAll Then

    '        'For Each item As ListViewItem In Me.listViewEmployeeNo.Items
    '        '    If item.SubItems(1).Text <> "" Then
    '        '        Dim singleEmployeeNoList As CEmployeeNoList = New CEmployeeNoList()
    '        '        singleEmployeeNoList.employeeNo = item.SubItems(1).Text
    '        '        JsonUserInfoSearchCond.UserInfoSearchCond.EmployeeNoList.Add(singleEmployeeNoList)
    '        '    End If
    '        'Next
    '    End If

    '    Dim strUserInfoSearchCond As String = JsonConvert.SerializeObject(JsonUserInfoSearchCond)
    '    Dim ptrUserInfoSearchCond As IntPtr = Marshal.StringToHGlobalAnsi(strUserInfoSearchCond)

    '    If Not CHCNetSDK.NET_DVR_SendRemoteConfig(m_lUserInfoSearchHandle, CInt(CHCNetSDK.LONG_CFG_SEND_DATA_TYPE_ENUM.ENUM_SEND_JSON_DATA), ptrUserInfoSearchCond, CUInt(strUserInfoSearchCond.Length)) Then
    '        Dim listItem As ListViewItem = New ListViewItem()
    '        listItem.Text = "Fail"
    '        Dim strTemp As String = Nothing
    '        strTemp = String.Format("Search Fail")
    '        listItem.SubItems.Add(strTemp)
    '        'Me.AddList(listViewMessage, listItem)
    '        Marshal.FreeHGlobal(ptrUserInfoSearchCond)
    '        Return False
    '    End If

    '    Dim listItemSucc As ListViewItem = New ListViewItem()
    '    listItemSucc.Text = "Succ"
    '    Dim strTempSucc As String = Nothing
    '    strTempSucc = String.Format("Search Processing")
    '    listItemSucc.SubItems.Add(strTempSucc)
    '    'Me.AddList(listViewMessage, listItemSucc)
    '    Marshal.FreeHGlobal(ptrUserInfoSearchCond)
    '    Return True
    'End Function
End Class