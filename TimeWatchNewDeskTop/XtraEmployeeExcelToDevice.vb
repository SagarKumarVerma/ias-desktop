﻿Imports System.Resources
Imports System.Globalization
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports System.ComponentModel
Imports DevExpress.XtraGrid.Columns
Imports System.Data.SqlClient
Imports System.Text
Imports System.Data.OleDb
Imports DevExpress.XtraGrid.Views.Base
Imports iAS.HSeriesSampleCSharp
Imports System.Runtime.InteropServices
Imports CMITech.UMXClient
Imports iAS.AscDemo
Imports System.Threading
Imports Newtonsoft.Json
Imports Excel = Microsoft.Office.Interop.Excel
Imports System.Drawing.Imaging

Public Class XtraEmployeeExcelToDevice
    Dim APP As New Excel.Application
    Dim worksheet As Excel.Worksheet
    Dim workbook As Excel.Workbook
    Public Delegate Sub invokeDelegate()
    Dim serialNo As String = ""
    Dim lUserID As Integer = -1
    Dim m_lGetCardCfgHandle As Int32 = -1 'Hikvision
    Dim g_fGetGatewayCardCallback As CHCNetSDK.RemoteConfigCallback = Nothing 'Hikvision
    Dim m_lGetFingerPrintCfgHandle As Integer = -1
    Dim HCardNos As List(Of String) = Nothing
    Dim iMaxCardNum As Integer = 1000
    Dim m_struFingerPrintOne As CHCNetSDK.NET_DVR_FINGER_PRINT_CFG_V50 = New CHCNetSDK.NET_DVR_FINGER_PRINT_CFG_V50
    Dim m_struDelFingerPrint As CHCNetSDK.NET_DVR_FINGER_PRINT_INFO_CTRL_BYCARD_V50 = New CHCNetSDK.NET_DVR_FINGER_PRINT_INFO_CTRL_BYCARD_V50
    Dim m_struRecordCardCfg() As CHCNetSDK.NET_DVR_FINGER_PRINT_CFG_V50 = New CHCNetSDK.NET_DVR_FINGER_PRINT_CFG_V50((iMaxCardNum) - 1) {}
    Dim m_struCardInfo() As CHCNetSDK.NET_DVR_CARD_CFG_V50 = New CHCNetSDK.NET_DVR_CARD_CFG_V50((iMaxCardNum) - 1) {}
    Dim m_lUserID As Integer = -1
    Dim g_fSetFingerPrintCallback As CHCNetSDK.RemoteConfigCallback = Nothing
    Dim g_fGetFingerPrintCallback As CHCNetSDK.RemoteConfigCallback = Nothing
    Dim g_fDelFingerPrintCallback As CHCNetSDK.RemoteConfigCallback = Nothing
    Dim g_fSetGatewayCardCallback As CHCNetSDK.RemoteConfigCallback = Nothing
    Dim m_lSetCardCfgHandle As Int32 = -1
    Dim m_bSendOne As Boolean = False
    Dim m_struSelSendCardCfg As CHCNetSDK.NET_DVR_CARD_CFG_V50
    Dim m_struNowSendCard As CHCNetSDK.NET_DVR_CARD_CFG_V50
    Dim m_BSendSel As Boolean = False
    Dim m_dwCardNum As UInteger = 0
    Dim m_dwSendIndex As UInteger = 0
    Dim m_lSetFingerPrintCfgHandle As Integer = -1
    Dim m_iSendIndex As Integer = -1

    Dim Downloading As Boolean = False

    Dim ulf As UserLookAndFeel
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand

    Dim DC As New DateConverter()

    Const PWD_DATA_SIZE = 40
    Const FP_DATA_SIZE = 1680
    Const FACE_DATA_SIZE = 20080
    Const VEIN_DATA_SIZE = 3080
    Private mbytEnrollData(FACE_DATA_SIZE - 1) As Byte
    Private mbytPhotoImage() As Byte

    Private mnCommHandleIndex As Long
    Private mbGetState As Boolean
    Private mlngPasswordData As Long

    Dim vpszIPAddress As String
    Dim vpszNetPort As Long
    Dim vpszNetPassword As Long
    Dim vnTimeOut As Long
    Dim vnProtocolType As Long
    Dim strDateTime As String
    Dim vnResult As Long
    Dim vnLicense As Long
    '//=============== Backup Number Constant ===============//
    'Public Const BACKUP_FP_0 = 0                  ' Finger 0
    'Public Const BACKUP_FP_1 = 1                  ' Finger 1
    'Public Const BACKUP_FP_2 = 2                  ' Finger 2
    'Public Const BACKUP_FP_3 = 3                  ' Finger 3
    'Public Const BACKUP_FP_4 = 4                  ' Finger 4
    'Public Const BACKUP_FP_5 = 5                  ' Finger 5
    'Public Const BACKUP_FP_6 = 6                  ' Finger 6
    'Public Const BACKUP_FP_7 = 7                  ' Finger 7
    'Public Const BACKUP_FP_8 = 8                  ' Finger 8
    'Public Const BACKUP_FP_9 = 9                  ' Finger 9
    'Public Const BACKUP_PSW = 10                  ' Password
    Public Const BACKUP_CARD = 11                 ' Card
    Public Const BACKUP_FACE = 12                 ' Face
    Public Const BACKUP_VEIN_0 = 20               ' Vein 0

    Dim m_lSetFaceCfgHandle As Integer = -1
    Dim m_lSetUserCfgHandle As Int32 = -1
    Public Structure UserDetails
        Dim UserId As String
        Dim UserName As String
    End Structure
    Public Sub New()
        InitializeComponent()
        Common.SetGridFont(GridView1, New Font("Tahoma", 10))
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True
    End Sub
    Private Sub XtraEmployeeCopyToDevice_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        GridControl1.DataSource = Common.MachineNonAdmin
        GridView1.ClearSelection()
        TextEdit1.Text = ""
        CheckExcel.Checked = False
        If CheckExcel.Checked Then
            TextEdit1.Enabled = False
        Else
            TextEdit1.Enabled = True
        End If
    End Sub
    Private Sub SimpleButton1_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton1.Click
        Me.Close()
    End Sub
    Private Sub btnUploadFinger_Click(sender As System.Object, e As System.EventArgs) Handles btnUploadFinger.Click

        If TextEdit1.Text.Trim = "" And CheckExcel.Checked = False Then
            XtraMessageBox.Show(ulf, "<size=10>Please select the file to upload</size>", "<size>Error</size>")
            Exit Sub
        End If
        Dim vpszIPAddress As String
        Dim failIP As New List(Of String)()
        If GridView1.GetSelectedRows.Count = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>Please select the Machine</size>", "<size=9>Error</size>")
            Exit Sub
        End If
        Dim sSql As String = ""
        Me.Cursor = Cursors.WaitCursor


        '
        Dim UserInfoArr() As UserDetails
        Dim UserInfoList As New List(Of UserDetails)
        If CheckExcel.Checked = False Then
            Dim appWorldtmp As Excel.Application
            Dim wbWorld As Excel.Workbook
            Dim mPath As String = TextEdit1.Text.Trim
            Dim appWorld As Excel.Worksheet
            Dim intFile As Integer
            Dim bCardRepl As Boolean
            Dim i As Integer
            Dim rowcnt As Integer = 0
            Try
                appWorldtmp = GetObject(, "Excel.Application")  'look for a running copy of Excel
                If Err.Number <> 0 Then 'If Excel is not running then
                    appWorldtmp = CreateObject("Excel.Application") 'run it
                End If
                bCardRepl = False
                i = 0
                intFile = FreeFile()
                Err.Clear()
                wbWorld = appWorldtmp.Workbooks.Open(mPath)
                rowcnt = 2
            Catch ex As Exception
                XtraMessageBox.Show(ulf, "<size=10>" & ex.Message.ToString.Trim & "</size>", "<size=9>Error</size>")
                Exit Sub
            End Try
            Try
                appWorld = wbWorld.Worksheets(1)
                Do While appWorld.Cells(rowcnt, 1).value <> Nothing
                    Dim x As UserDetails = New UserDetails
                    x.UserId = (appWorld.Cells(rowcnt, 1).value.ToString)
                    If Trim(x.UserId) = "" Then GoTo c
                    x.UserName = Trim(appWorld.Cells(rowcnt, 2).value)
                    UserInfoList.Add(x)
c:                  rowcnt = rowcnt + 1
                Loop
                UserInfoArr = UserInfoList.ToArray
                wbWorld.Close()
                appWorldtmp.Quit()
            Catch ex As Exception
                appWorldtmp.Quit()
                Me.Cursor = Cursors.Default
                XtraMessageBox.Show(ulf, "<size=10>Sheet not found</size>", "<size=9>Success</size>")
                Exit Sub
            End Try
        Else
            Dim fileEntries As String() = Directory.GetFiles(My.Application.Info.DirectoryPath & "\EmpImages\", "*.jpg")
            'Dim extList() As String = {"*.jpg", "*.jpeg"}
            Dim extList() As String = {"*.jpg", "*.jpeg", ".png"}
            Dim fileList As New List(Of FileInfo)
            Dim fileListTmp As New List(Of String)
            For Each ext In extList
                'Returns only the filenames based on the directory that is selected
                Dim fi = From f In New IO.DirectoryInfo(My.Application.Info.DirectoryPath & "\EmpImages\").GetFiles(ext).Cast(Of IO.FileInfo)() Order By f.CreationTime Select f
                fileList.AddRange(fi)
            Next
            For Each fileInfo As System.IO.FileInfo In fileList
                fileListTmp.Add(fileInfo.Name.ToString)
            Next
            fileEntries = fileListTmp.Distinct.ToArray()
            For i As Integer = 0 To fileEntries.Length - 1
                Dim x As UserDetails = New UserDetails
                'x.UserId = fileEntries(i).Substring(0, fileEntries(i).Length - 4).Replace(My.Application.Info.DirectoryPath & "\EmpImages\", "")
                Dim UserIdTmp As String = fileEntries(i).Substring(0, fileEntries(i).IndexOf(".")).Replace(My.Application.Info.DirectoryPath & "\EmpImages\", "")
                If IsNumeric(UserIdTmp) Then
                    UserIdTmp = Convert.ToDouble(UserIdTmp)
                End If
                x.UserId = UserIdTmp ' fileEntries(i).Substring(0, fileEntries(i).IndexOf(".")).Replace(My.Application.Info.DirectoryPath & "\EmpImages\", "")
                'x.UserName = fileEntries(i).Substring(0, fileEntries(i).Length - 4).Replace(My.Application.Info.DirectoryPath & "\EmpImages\", "")
                x.UserName = UserIdTmp ' fileEntries(i).Substring(0, fileEntries(i).IndexOf(".")).Replace(My.Application.Info.DirectoryPath & "\EmpImages\", "")
                UserInfoList.Add(x)
            Next
            UserInfoArr = UserInfoList.ToArray
        End If
        Dim selectedRows As Integer() = GridView1.GetSelectedRows()
        Dim result As Object() = New Object(selectedRows.Length - 1) {}
        Dim LstMachineId As String
        For i = 0 To selectedRows.Length - 1
            Dim rowHandle As Integer = selectedRows(i)
            If Not GridView1.IsGroupRow(rowHandle) Then
                LstMachineId = GridView1.GetRowCellValue(rowHandle, "ID_NO").ToString.Trim
                vpszIPAddress = GridView1.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim
                If GridView1.GetRowCellValue(rowHandle, "DeviceType") = "HKSeries" Then
                    XtraMasterTest.LabelControlStatus.Text = "Connecting " & vpszIPAddress & " ..."
                    Application.DoEvents()
                    Dim cn As Common = New Common
                    Dim struDeviceInfoV40 As CHCNetSDK.NET_DVR_DEVICEINFO_V40 = New CHCNetSDK.NET_DVR_DEVICEINFO_V40
                    Dim DeviceAdd As String = GridView1.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim, userName As String = Trim(GridView1.GetRowCellValue(rowHandle, "HLogin").ToString.Trim), pwd As String = Trim(GridView1.GetRowCellValue(rowHandle, "HPassword").ToString.Trim)
                    Dim lUserID As Integer = -1
                    Dim failReason As String = ""
                    Dim logistatus = cn.HikvisionLogin(DeviceAdd, userName, pwd, struDeviceInfoV40, lUserID, failReason)
                    If logistatus = False Then
                        If failReason <> "" Then
                            XtraMessageBox.Show(ulf, "<size=10>" & failReason & "</size>", "Failed")
                            XtraMasterTest.LabelControlStatus.Text = ""
                            Application.DoEvents()
                            Continue For
                        End If
                    Else
                        If failReason <> "" Then
                            XtraMessageBox.Show(ulf, "<size=10>" & failReason & "</size>", "Failed")
                            XtraMasterTest.LabelControlStatus.Text = ""
                            Application.DoEvents()
                            Me.Cursor = Cursors.Default
                            logistatus = cn.HikvisionLogOut(lUserID)
                            Continue For
                        End If

                        XtraMasterTest.LabelControlStatus.Text = ""
                        Application.DoEvents()

                        If (-1 <> m_lSetCardCfgHandle) Then
                            If CHCNetSDK.NET_DVR_StopRemoteConfig(m_lSetCardCfgHandle) Then
                                m_lSetCardCfgHandle = -1
                            End If
                        End If
                        m_bSendOne = True
                        'loop for sheet 
                        Dim TempCardNo As String
                        Dim UName As String = ""

                        For Each uInfo As UserDetails In UserInfoArr
                            TempCardNo = uInfo.UserId
                            UName = uInfo.UserName
                            Dim RsFp As DataSet = New DataSet
                            Dim mUser_ID As String = TempCardNo
                            XtraMasterTest.LabelControlStatus.Text = "Uploading User " & mUser_ID
                            Application.DoEvents()
                            Dim ValidityStartdate As DateTime
                            Dim ValidityEnddate As DateTime
                            Dim sName As String = UName
                            Dim sdwEnrollNumber As String
                            If IsNumeric(mUser_ID) = True Then
                                sdwEnrollNumber = Convert.ToDouble(mUser_ID)
                            Else
                                sdwEnrollNumber = mUser_ID
                            End If
                            ValidityStartdate = Convert.ToDateTime("2018-01-01")
                            ValidityEnddate = Convert.ToDateTime("2030-12-31")
                            If (m_lSetUserCfgHandle <> -1) Then
                                If CHCNetSDK.NET_DVR_StopRemoteConfig(m_lSetUserCfgHandle) Then
                                    m_lSetUserCfgHandle = -1
                                End If
                            End If
                            Dim sURL As String = "PUT /ISAPI/AccessControl/UserInfo/SetUp?format=json"
                            Dim ptrURL As IntPtr = Marshal.StringToHGlobalAnsi(sURL)
                            m_lSetUserCfgHandle = CHCNetSDK.NET_DVR_StartRemoteConfig(lUserID, CHCNetSDK.NET_DVR_JSON_CONFIG, ptrURL, sURL.Length, Nothing, IntPtr.Zero)
                            If (m_lSetUserCfgHandle < 0) Then
                                'MessageBox.Show(("NET_DVR_StartRemoteConfig fail [url:PUT /ISAPI/AccessControl/UserInfo/SetUp?format=json] error:" + CHCNetSDK.NET_DVR_GetLastError))
                                Marshal.FreeHGlobal(ptrURL)
                                Continue For
                            Else
                                SendUserInfo(sdwEnrollNumber, sName, ValidityStartdate, ValidityEnddate)
                                Marshal.FreeHGlobal(ptrURL)
                                faceUpload(lUserID, sdwEnrollNumber)
                            End If
                        Next

                    End If
                End If
            End If
        Next i


        Me.Cursor = Cursors.Default
        XtraMasterTest.LabelControlStatus.Text = ""
        Application.DoEvents()

        Dim failedIpArr() As String = failIP.ToArray
        Dim failIpStr As String = ""
        If failedIpArr.Length > 0 Then
            For j As Integer = 0 To failedIpArr.Length - 1
                failIpStr = failIpStr & "," & failedIpArr(j)
            Next
            failIpStr = failIpStr.TrimStart(",")
            XtraMessageBox.Show(ulf, "<size=10>" & failIpStr & " failed to download data</size>", "<size=9>Failed</size>")
        Else
            XtraMessageBox.Show(ulf, "<size=10>Task Completed</size>", "<size=9>Success</size>")
        End If
        'XtraMessageBox.Show(ulf, "<size=10>Task Completed</size>", "<size=9>Information</size>")
    End Sub
    Private Sub SendUserInfo(sdwEnrollNumber As String, sName As String, ValidityStartdate As DateTime, ValidityEnddate As DateTime)
        Dim JsonUserInfo As CUserInfoCfg = New CUserInfoCfg()
        JsonUserInfo.UserInfo = New CUserInfo()
        JsonUserInfo.UserInfo.employeeNo = sdwEnrollNumber
        JsonUserInfo.UserInfo.name = sName
        JsonUserInfo.UserInfo.userType = "normal"
        JsonUserInfo.UserInfo.Valid = New CValid()
        JsonUserInfo.UserInfo.Valid.enable = True
        JsonUserInfo.UserInfo.Valid.beginTime = ValidityStartdate.ToString("yyyy-MM-ddTHH:mm:ss") ' "2017-08-01T17:30:08"
        JsonUserInfo.UserInfo.Valid.endTime = ValidityEnddate.ToString("yyyy-MM-ddTHH:mm:ss") '"2020-08-01T17:30:08"
        JsonUserInfo.UserInfo.Valid.timeType = "local"
        JsonUserInfo.UserInfo.RightPlan = New List(Of CRightPlan)()
        Dim JsonRightPlan As CRightPlan = New CRightPlan()
        JsonRightPlan.doorNo = 1
        JsonRightPlan.planTemplateNo = 1 'textBoxRightPlan.Text
        JsonUserInfo.UserInfo.RightPlan.Add(JsonRightPlan)
        'JsonUserInfo.UserInfo.userVerifyMode = "card"
        JsonUserInfo.UserInfo.floorNumber = 0  'nitin
        JsonUserInfo.UserInfo.roomNumber = 0 'nitin
        'If Privilege = 1 Then
        '    JsonUserInfo.UserInfo.localUIRight = True  'admin rights
        'Else
        JsonUserInfo.UserInfo.localUIRight = False   'admin rights
        'End If

        JsonUserInfo.UserInfo.doorRight = "1" 'nitin
        Dim strJsonUserInfo As String = JsonConvert.SerializeObject(JsonUserInfo, Formatting.Indented, New JsonSerializerSettings With {
            .DefaultValueHandling = DefaultValueHandling.Ignore
        })
        Dim ptrJsonUserInfo As IntPtr = Marshal.StringToHGlobalAnsi(strJsonUserInfo)
        Dim ptrJsonData As IntPtr = Marshal.AllocHGlobal(1024)

        For i As Integer = 0 To 1024 - 1
            Marshal.WriteByte(ptrJsonData, i, 0)
        Next

        Dim dwState As Integer = CInt(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_SUCCESS)
        Dim dwReturned As UInteger = 0

        While True
            dwState = CHCNetSDK.NET_DVR_SendWithRecvRemoteConfig(m_lSetUserCfgHandle, ptrJsonUserInfo, CUInt(strJsonUserInfo.Length), ptrJsonData, 1024, dwReturned)
            Dim strJsonData As String = Marshal.PtrToStringAnsi(ptrJsonData)

            If dwState = CInt(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_NEEDWAIT) Then
                Thread.Sleep(10)
                Continue While
            ElseIf dwState = CInt(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_FAILED) Then
                'MessageBox.Show("Set User Fail error:" & CHCNetSDK.NET_DVR_GetLastError())
                Exit While
            ElseIf dwState = CInt(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_SUCCESS) Then
                Dim JsonResponseStatus As CResponseStatus = New CResponseStatus()
                JsonResponseStatus = JsonConvert.DeserializeObject(Of CResponseStatus)(strJsonData)

                If JsonResponseStatus.statusCode = 1 Then
                    'MessageBox.Show("Set User Success")
                Else
                    'MessageBox.Show("Set User Fail, ResponseStatus.statusCode" & JsonResponseStatus.statusCode)
                End If

                Exit While
            ElseIf dwState = CInt(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_FINISH) Then
                'MessageBox.Show("Set User Finish")
                Exit While
            ElseIf dwState = CInt(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_EXCEPTION) Then
                'MessageBox.Show("Set User Exception error:" & CHCNetSDK.NET_DVR_GetLastError())
                Exit While
            Else
                'MessageBox.Show("unknown Status error:" & CHCNetSDK.NET_DVR_GetLastError())
                Exit While
            End If
        End While

        If m_lSetUserCfgHandle <> -1 Then
            If CHCNetSDK.NET_DVR_StopRemoteConfig(m_lSetUserCfgHandle) Then
                m_lSetUserCfgHandle = -1
            End If
        End If

        Marshal.FreeHGlobal(ptrJsonUserInfo)
        Marshal.FreeHGlobal(ptrJsonData)
    End Sub
    Private Sub CompressAndSaveImage(ByVal img As Image, ByVal fileName As String, ByVal quality As Long)
        Dim parameters As New EncoderParameters(1)
        parameters.Param(0) = New EncoderParameter(System.Drawing.Imaging.Encoder.Quality, quality)
        img.Save(fileName, GetCodecInfo("image/jpeg"), parameters)
    End Sub
    Private Shared Function GetCodecInfo(ByVal mimeType As String) As ImageCodecInfo
        For Each encoder As ImageCodecInfo In ImageCodecInfo.GetImageEncoders()
            If encoder.MimeType = mimeType Then
                Return encoder
            End If
        Next encoder
        Throw New ArgumentOutOfRangeException(String.Format("'{0}' not supported", mimeType))
    End Function
    Private Function faceUpload(ByVal m_UserID As Integer, empNo As String) As String
        XtraMasterTest.LabelControlStatus.Text = "Uploading Face " & empNo
        Application.DoEvents()
        Dim filePath As String = My.Application.Info.DirectoryPath & "\EmpImages\" & empNo & ".jpg"
        'Dim filePath1 As String = My.Application.Info.DirectoryPath & "\EmpImages\" & empNo & "_1.jpg"
        If Not My.Computer.FileSystem.FileExists(filePath) Then
            filePath = My.Application.Info.DirectoryPath & "\EmpImages\" & empNo & ".jpeg"
        End If
        If Not My.Computer.FileSystem.FileExists(filePath) Then
            filePath = My.Application.Info.DirectoryPath & "\EmpImages\" & empNo & ".png"
        End If
        If Not My.Computer.FileSystem.FileExists(filePath) Then
            Return "File Not Found"
        End If
        Dim currentImage As Image = Image.FromFile(filePath)

        If currentImage.Height <> 432 Or currentImage.Width <> 352 Then
            If currentImage.Width > currentImage.Height Then
                currentImage.RotateFlip(RotateFlipType.Rotate90FlipNone)
            End If
            Dim savedImage As New Bitmap(currentImage, 352, 432) 'for hikvisioncurrentImage     
            currentImage.Dispose()
            Dim fi As FileInfo = New FileInfo(filePath)
            If fi.Length > 200000 Then
                Dim selectedImage As Image = savedImage 'Image.FromFile(OpenFileDialog1.FileName)
                CompressAndSaveImage(selectedImage, filePath, 100)
            End If
        End If
        Try
            currentImage.Dispose()
        Catch ex As Exception

        End Try

        Dim sURL As String = "PUT /ISAPI/Intelligent/FDLib/FDSetUp?format=json"
        Dim ptrURL As IntPtr = Marshal.StringToHGlobalAnsi(sURL)
        m_lSetFaceCfgHandle = CHCNetSDK.NET_DVR_StartRemoteConfig(m_UserID, CHCNetSDK.NET_DVR_FACE_DATA_RECORD, ptrURL, sURL.Length, Nothing, IntPtr.Zero)
        If m_lSetFaceCfgHandle = -1 Then
            Marshal.FreeHGlobal(ptrURL)
            'MessageBox.Show("NET_DVR_StartRemoteConfig fail [url:PUT /ISAPI/Intelligent/FDLib/FDSetUp?format=json] error:" & CHCNetSDK.NET_DVR_GetLastError())
            Return "Connection Fail"
        End If
        Marshal.FreeHGlobal(ptrURL)

        Dim JsonSetFaceDataCond As New CSetFaceDataCond()
        JsonSetFaceDataCond.faceLibType = "blackFD"
        JsonSetFaceDataCond.FDID = "1"
        JsonSetFaceDataCond.FPID = empNo
        Dim strJsonSearchFaceDataCond As String = JsonConvert.SerializeObject(JsonSetFaceDataCond, Formatting.Indented, New JsonSerializerSettings With {.DefaultValueHandling = DefaultValueHandling.Ignore})
        Dim ptrJsonSearchFaceDataCond As IntPtr = Marshal.StringToHGlobalAnsi(strJsonSearchFaceDataCond)

        Dim struJsonDataCfg As New CHCNetSDK.NET_DVR_JSON_DATA_CFG()
        struJsonDataCfg.dwSize = CUInt(Marshal.SizeOf(struJsonDataCfg))
        struJsonDataCfg.lpJsonData = ptrJsonSearchFaceDataCond
        struJsonDataCfg.dwJsonDataSize = CUInt(strJsonSearchFaceDataCond.Length)
        If Not File.Exists(filePath) Then
            'MessageBox.Show("The picture does not exist!")
            Marshal.FreeHGlobal(ptrJsonSearchFaceDataCond)
            Return "The picture does not exist!"
        End If
        Dim fs As New FileStream(filePath, FileMode.OpenOrCreate)
        If 0 = fs.Length Then
            'MessageBox.Show("The picture is 0k,please input another picture!")
            Marshal.FreeHGlobal(ptrJsonSearchFaceDataCond)
            fs.Close()
            Return "The picture size is 0kb"
        End If
        If 200 * 1024 < fs.Length Then
            'MessageBox.Show("The picture is larger than 200k,please input another picture!")
            Marshal.FreeHGlobal(ptrJsonSearchFaceDataCond)
            fs.Close()
            Return "The picture is larger than 200k,please input another picture!"
        End If
        struJsonDataCfg.dwPicDataSize = CUInt(fs.Length)
        Dim iLen As Integer = CInt(struJsonDataCfg.dwPicDataSize)
        Dim by(iLen - 1) As Byte
        struJsonDataCfg.lpPicData = Marshal.AllocHGlobal(iLen)
        fs.Read(by, 0, iLen)
        Marshal.Copy(by, 0, struJsonDataCfg.lpPicData, iLen)
        fs.Close()
        Dim ptrJsonDataCfg As IntPtr = Marshal.AllocHGlobal(CInt(struJsonDataCfg.dwSize))
        Marshal.StructureToPtr(struJsonDataCfg, ptrJsonDataCfg, False)

        Dim ptrJsonResponseStatus As IntPtr = Marshal.AllocHGlobal(1024)
        For i As Integer = 0 To 1023
            Marshal.WriteByte(ptrJsonResponseStatus, i, 0)
        Next i
        Dim dwState As Integer = CInt(CHCNetSDK.NET_SDK_GET_NEXT_STATUS_SUCCESS)
        Dim dwReturned As UInteger = 0
        Dim returnValue As String = ""
        Do
            dwState = CHCNetSDK.NET_DVR_SendWithRecvRemoteConfig(m_lSetFaceCfgHandle, ptrJsonDataCfg, struJsonDataCfg.dwSize, ptrJsonResponseStatus, 1024, dwReturned)
            Dim strResponseStatus As String = Marshal.PtrToStringAnsi(ptrJsonResponseStatus)
            If dwState = CInt(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_NEEDWAIT) Then
                Thread.Sleep(10)
                Continue Do
            ElseIf dwState = CInt(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_FAILED) Then
                'MessageBox.Show("Set Face Error:" & CHCNetSDK.NET_DVR_GetLastError())
                returnValue = "Set Face Error:" & CHCNetSDK.NET_DVR_GetLastError()
                Exit Do
            ElseIf dwState = CInt(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_SUCCESS) Then
                Dim JsonResponseStatus As New CResponseStatus()
                JsonResponseStatus = JsonConvert.DeserializeObject(Of CResponseStatus)(strResponseStatus)
                If JsonResponseStatus.statusCode = 1 Then
                    'MessageBox.Show("Set Face Success")
                Else
                    'MessageBox.Show("Set Face Fail, ResponseStatus.statusCode = " & JsonResponseStatus.statusCode)
                    returnValue = "Set Face Fail, ResponseStatus.statusCode = " & JsonResponseStatus.statusCode
                End If
                Exit Do
            ElseIf dwState = CInt(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_EXCEPTION) Then
                'MessageBox.Show("Set Face Exception Error:" & CHCNetSDK.NET_DVR_GetLastError())
                returnValue = "Set Face Exception Error:" & CHCNetSDK.NET_DVR_GetLastError()
                Exit Do
            Else
                'MessageBox.Show("unknown Status Error:" & CHCNetSDK.NET_DVR_GetLastError())
                returnValue = "unknown Status Error:" & CHCNetSDK.NET_DVR_GetLastError()
                Exit Do
            End If
        Loop
        If m_lSetFaceCfgHandle > 0 Then
            CHCNetSDK.NET_DVR_StopRemoteConfig(m_lSetFaceCfgHandle)
            m_lSetFaceCfgHandle = -1
        End If
        Marshal.FreeHGlobal(ptrJsonDataCfg)
        Marshal.FreeHGlobal(ptrJsonResponseStatus)
        Return returnValue
    End Function
    Private Sub TextEdit1_Click(sender As System.Object, e As System.EventArgs) Handles TextEdit1.Click
        Dim dlg As New OpenFileDialog()
        ' Filter by Office Files
        dlg.Filter = "Office Files|*.xlsx;*.xls;"
        dlg.ShowDialog()
        TextEdit1.Text = dlg.FileName
    End Sub

    Private Sub CheckExcel_CheckedChanged(sender As Object, e As EventArgs) Handles CheckExcel.CheckedChanged
        If CheckExcel.Checked Then
            TextEdit1.Enabled = False
        Else
            TextEdit1.Enabled = True
        End If

    End Sub
End Class