﻿Imports System.Data.OleDb
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.Data.SqlClient
Imports System.IO
Imports System.Resources
Imports System.Globalization
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid.Views.Base

Public Class XtraHolidayEntry
    Dim ulf As UserLookAndFeel
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    Public Sub New()
        InitializeComponent()
        'for xtramessage box
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True

        'If Common.servername = "Access" Then
        '    Me.Holiday1TableAdapter1.Fill(Me.SSSDBDataSet.Holiday1)
        '    'GridControl1.DataSource = SSSDBDataSet.Holiday1
        'Else
        '    HolidayTableAdapter.Connection.ConnectionString = Common.ConnectionString '"Data Source=DESKTOP-PG6NDV8;Initial Catalog=SSSDB;Integrated Security=True"
        '    Me.HolidayTableAdapter.Fill(Me.SSSDBDataSet.Holiday)
        '    'GridControl1.DataSource = SSSDBDataSet.Holiday
        'End If
        Common.SetGridFont(GridView1, New Font("Tahoma", 11))
    End Sub
    Private Sub XtraHolidayEntry_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        'for xtramessage box
        Me.Width = Common.NavWidth 'Me.Parent.Width
        Me.Height = Common.NavHeight 'Me.Parent.Height
        'SplitContainerControl1.Width = Common.splitforMasterMenuWidth 'SplitContainerControl1.Parent.Width
        SplitContainerControl1.SplitterPosition = Common.SplitterPosition '(SplitContainerControl1.Parent.Width) * 85 / 100

        Dim sSql As String = "Select distinct(HDate),HOLIDAY,companycode from Holiday  order by Hdate asc"
        'If Common.servername = "Access" Then
        '    Dim tmp As OleDbCommand = New OleDbCommand(sSql)
        '    Holiday1TableAdapter1.Adapter.SelectCommand = tmp
        '    'Me.Holiday1TableAdapter1.Fill(Me.SSSDBDataSet.Holiday1)
        '    GridControl1.DataSource = Holiday1TableAdapter1.GetData 'SSSDBDataSet.Holiday1
        'Else
        '    Dim tmp As SqlCommand = New SqlCommand(sSql)
        '    HolidayTableAdapter.Adapter.SelectCommand = tmp
        '    HolidayTableAdapter.Adapter.Fill(HolidayTableAdapter.GetData)
        '    'Me.HolidayTableAdapter.Fill(Me.SSSDBDataSet.Holiday)
        '    GridControl1.DataSource = HolidayTableAdapter.GetData 'SSSDBDataSet.Holiday
        'End If
        setHolidayGrid()
    End Sub
    Private Sub setHolidayGrid()
        GridControl1.DataSource = Nothing
        Dim dt As DataTable = New DataTable
        dt.Columns.Add("Holiday Day")
        dt.Columns.Add("Holiday")
        dt.Columns.Add("Company")
        Dim sSql As String = "Select distinct(HDate),HOLIDAY,companycode from Holiday  order by Hdate asc"
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim ds As DataSet = New DataSet
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(ds)
        End If
        If ds.Tables(0).Rows.Count > 0 Then
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                dt.Rows.Add(ds.Tables(0).Rows(i).Item("HDate").ToString.Split(" ")(0), ds.Tables(0).Rows(i).Item("HOLIDAY").ToString, ds.Tables(0).Rows(i).Item("companycode").ToString)
            Next
        End If
        Dim datase As DataSet = New DataSet()
        datase.Tables.Add(dt)
        GridControl1.DataSource = dt
    End Sub
    Private Sub GridView1_EditFormShowing(sender As System.Object, e As DevExpress.XtraGrid.Views.Grid.EditFormShowingEventArgs) Handles GridView1.EditFormShowing
        e.Allow = False
        If GridView1.IsNewItemRow(GridView1.FocusedRowHandle) = True Then
            XtraHolidayEntryEdit.ShowDialog()
            setHolidayGrid()
            'If Common.servername = "Access" Then   'to refresh the grid
            '    Me.Holiday1TableAdapter1.Fill(Me.SSSDBDataSet.Holiday1)
            'Else
            '    Me.HolidayTableAdapter.Fill(Me.SSSDBDataSet.Holiday)
            'End If
        End If
    End Sub
    Private Sub GridControl1_EmbeddedNavigator_ButtonClick(sender As System.Object, e As DevExpress.XtraEditors.NavigatorButtonClickEventArgs) Handles GridControl1.EmbeddedNavigator.ButtonClick
        If e.Button.ButtonType = DevExpress.XtraEditors.NavigatorButtonType.Remove Then
            If XtraMessageBox.Show(ulf, "<size=10>" & Common.res_man.GetString("askdelete", Common.cul) & "</size>", Common.res_man.GetString("confirmdeleteion", Common.cul), _
                              MessageBoxButtons.YesNo, MessageBoxIcon.Question) <> DialogResult.Yes Then
                'Me.Validate()
                'e.Handled = True
                'MsgBox("Your records have been saved and updated successfully!")
            Else
                Me.Validate()
                e.Handled = True
                Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
                Dim HDate As DateTime = Convert.ToDateTime(row("Holiday Day").ToString.Trim)
                Dim companycode As String = row("Company").ToString.Trim
                If Common.servername = "Access" Then
                    If Common.con1.State <> ConnectionState.Open Then
                        Common.con1.Open()
                    End If
                    cmd1 = New OleDbCommand("Delete from Holiday WHERE format(HDate,'yyyy-MM-dd 00:00:00')='" & HDate.ToString("yyyy-MM-dd 00:00:00") & "' AND COMPANYCODE='" & companycode & "'", Common.con1)
                    cmd1.ExecuteNonQuery()
                    If Common.con1.State <> ConnectionState.Closed Then
                        Common.con1.Close()
                    End If
                Else
                    If Common.con.State <> ConnectionState.Open Then
                        Common.con.Open()
                    End If
                    cmd = New SqlCommand("Delete from Holiday WHERE HDate='" & HDate.ToString("yyyy-MM-dd 00:00:00") & "' AND COMPANYCODE='" & companycode & "'", Common.con)
                    cmd.ExecuteNonQuery()
                    If Common.con.State <> ConnectionState.Closed Then
                        Common.con.Close()
                    End If
                End If
                Common.LogPost("Holiday Delete; HDate=" & HDate.ToString("yyyy-MM-dd 00:00:00"))
                Dim comclass As Common = New Common

                Dim adap, adap1 As SqlDataAdapter
                Dim adapA, adapA1 As OleDbDataAdapter
                Dim ds As DataSet = New DataSet
                Dim ds1 As DataSet = New DataSet
                Dim s As String = "select TblEmployee.PAYCODE,EmployeeGroup.Id from TblEmployee, EmployeeGroup where EmployeeGroup.GroupId = TblEmployee.EmployeeGroupId" '"select PAYCODE from TblEmployee"
                If Common.servername = "Access" Then
                    adapA1 = New OleDbDataAdapter(s, Common.con1)
                    adapA1.Fill(ds1)
                Else
                    adap1 = New SqlDataAdapter(s, Common.con)
                    adap1.Fill(ds1)
                End If
                For i As Integer = 0 To ds1.Tables(0).Rows.Count - 1
                    Dim sSql As String = "select ISROUNDTHECLOCKWORK from tblEmployeeShiftMaster where PAYCODE = '" & ds1.Tables(0).Rows(i).Item("PAYCODE").Trim & "'"
                    ds = New DataSet
                    If Common.servername = "Access" Then
                        adapA = New OleDbDataAdapter(sSql, Common.con1)
                        adapA.Fill(ds)
                    Else
                        adap = New SqlDataAdapter(sSql, Common.con)
                        adap.Fill(ds)
                    End If
                    If ds.Tables(0).Rows(0).Item("ISROUNDTHECLOCKWORK").ToString = "Y" Then
                        If Common.PrcessMode = "M" Then
                            comclass.Process_AllnonRTCINOUT(HDate.AddDays(-1), HDate, ds1.Tables(0).Rows(i).Item("PAYCODE").Trim, ds1.Tables(0).Rows(i).Item("PAYCODE").Trim, ds1.Tables(0).Rows(i).Item("Id"), ds.Tables(0).Rows(0).Item("ISROUNDTHECLOCKWORK").ToString)
                        Else
                            comclass.Process_AllRTC(HDate.AddDays(-1), HDate, ds1.Tables(0).Rows(i).Item("PAYCODE").Trim, ds1.Tables(0).Rows(i).Item("PAYCODE").Trim, ds1.Tables(0).Rows(i).Item("Id"))
                        End If
                        'comclass.Process_AllRTC(HDate.AddDays(-1), HDate, ds1.Tables(0).Rows(i).Item("PAYCODE").Trim, ds1.Tables(0).Rows(i).Item("PAYCODE").Trim, ds1.Tables(0).Rows(i).Item("Id"))
                    Else
                        If Common.PrcessMode = "M" Then
                            comclass.Process_AllnonRTCINOUT(HDate, HDate, ds1.Tables(0).Rows(i).Item("PAYCODE").Trim, ds1.Tables(0).Rows(i).Item("PAYCODE").Trim, ds1.Tables(0).Rows(i).Item("Id"), ds.Tables(0).Rows(0).Item("ISROUNDTHECLOCKWORK").ToString)
                        Else
                            comclass.Process_AllnonRTC(HDate, HDate, ds1.Tables(0).Rows(i).Item("PAYCODE").Trim, ds1.Tables(0).Rows(i).Item("PAYCODE").Trim, ds1.Tables(0).Rows(i).Item("Id"))
                        End If
                        'comclass.Process_AllnonRTC(HDate, HDate, ds1.Tables(0).Rows(i).Item("PAYCODE").Trim, ds1.Tables(0).Rows(i).Item("PAYCODE").Trim, ds1.Tables(0).Rows(i).Item("Id"))
                        If Common.EmpGrpArr(ds1.Tables(0).Rows(i).Item("Id")).SHIFTTYPE = "M" Then
                            comclass.Process_AllnonRTCMulti(HDate, HDate, ds1.Tables(0).Rows(i).Item("PAYCODE").Trim, ds1.Tables(0).Rows(i).Item("PAYCODE").Trim, ds1.Tables(0).Rows(i).Item("Id"))
                        End If
                    End If
                Next
            End If
        End If
        XtraMasterTest.LabelControlStatus.Text = ""
        Application.DoEvents()
        setHolidayGrid()
    End Sub
    Private Sub GridView1_RowDeleted(sender As System.Object, e As DevExpress.Data.RowDeletedEventArgs) Handles GridView1.RowDeleted
        'Me.Holiday1TableAdapter1.Update(Me.SSSDBDataSet.Holiday1)
        'Me.HolidayTableAdapter.Update(Me.SSSDBDataSet.Holiday)
        setHolidayGrid()
        XtraMessageBox.Show(ulf, "<size=10>" & Common.res_man.GetString("deletesuccess", Common.cul) & "</size>", Common.res_man.GetString("msgsuccess", Common.cul))
    End Sub
    Private Sub GridView1_RowUpdated(sender As System.Object, e As DevExpress.XtraGrid.Views.Base.RowObjectEventArgs) Handles GridView1.RowUpdated
        'Me.Holiday1TableAdapter1.Update(Me.SSSDBDataSet.Holiday1)
        'Me.HolidayTableAdapter.Update(Me.SSSDBDataSet.Holiday)
    End Sub
    Private Sub GridView1_CustomColumnDisplayText(sender As System.Object, e As DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs) Handles GridView1.CustomColumnDisplayText
        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
        Dim view As ColumnView = TryCast(sender, ColumnView)
        If Common.IsNepali = "Y" Then
            Me.Cursor = Cursors.WaitCursor
            If e.Column.FieldName = "Holiday Day" Then
                Try
                    If row("Holiday Day").ToString.Trim <> "" Then
                        Dim DC As New DateConverter()
                        Dim dt As DateTime = Convert.ToDateTime(view.GetListSourceRowCellValue(e.ListSourceRowIndex, "Holiday Day").ToString().Trim)
                        Dim Vstart As String = DC.ToBS(New Date(dt.Year, dt.Month, dt.Day))
                        Dim dojTmp() As String = Vstart.Split("-")
                        e.DisplayText = dojTmp(2) & "-" & Common.NepaliMonth(dojTmp(1) - 1).ToString & "-" & dojTmp(0)
                    End If
                Catch ex As Exception
                End Try
            End If
            Me.Cursor = Cursors.Default
        End If
    End Sub
End Class
