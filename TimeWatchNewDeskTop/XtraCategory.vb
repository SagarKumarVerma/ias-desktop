﻿Imports System.Resources
Imports System.Globalization
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports System.ComponentModel
Imports DevExpress.XtraGrid.Columns
Imports DevExpress.XtraGrid.Views.Base
Imports DevExpress.Utils
Imports System.Data.OleDb
Imports System.Data.SqlClient

Public Class XtraCategory
    'Dim res_man As ResourceManager     'declare Resource manager to access to specific cultureinfo
    'Dim cul As CultureInfo     'declare culture info
    'Dim servername As String
    'Dim ConnectionString As String
    Dim ulf As UserLookAndFeel
    Public Shared CatId As String

    Public Sub New()
        InitializeComponent()
        If Common.servername = "Access" Then
            'ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=|DataDirectory|\TimeWatch.mdb;Persist Security Info=True;Jet OLEDB:Database Password=SSS"
            Me.TblCatagory1TableAdapter1.Fill(Me.SSSDBDataSet.tblCatagory1)
            GridControl1.DataSource = SSSDBDataSet.tblCatagory1
            Me.TblLeaveMaster1TableAdapter1.Fill(Me.SSSDBDataSet.tblLeaveMaster1)
            RepositoryItemGridLookUpEdit1.DataSource = SSSDBDataSet.tblLeaveMaster1
        Else
            'ConnectionString = "Data Source=" & servername & ";Initial Catalog=SSSDB;Integrated Security=True"
            TblCatagoryTableAdapter.Connection.ConnectionString = Common.ConnectionString '"Data Source=DESKTOP-PG6NDV8;Initial Catalog=SSSDB;Integrated Security=True"
            Me.TblCatagoryTableAdapter.Fill(Me.SSSDBDataSet.tblCatagory)
            GridControl1.DataSource = SSSDBDataSet.tblCatagory

            TblLeaveMasterTableAdapter.Connection.ConnectionString = Common.ConnectionString
            Me.TblLeaveMasterTableAdapter.Fill(Me.SSSDBDataSet.tblLeaveMaster)
            RepositoryItemGridLookUpEdit1.DataSource = SSSDBDataSet.tblLeaveMaster
        End If
        Common.SetGridFont(GridView1, New Font("Tahoma", 11))
    End Sub
    Private Sub XtraCategory_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        'for xtramessage box
        Me.Width = Common.NavWidth 'Me.Parent.Width
        Me.Height = Common.NavHeight 'Me.Parent.Height
        'SplitContainerControl1.Width = Common.splitforMasterMenuWidth 'SplitContainerControl1.Parent.Width
        SplitContainerControl1.SplitterPosition = Common.SplitterPosition '(SplitContainerControl1.Parent.Width) * 85 / 100

        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True

        'res_man = New ResourceManager("TimeWatchNewDeskTop.Res", GetType(XtraCategory).Assembly)
        'cul = CultureInfo.CreateSpecificCulture("en")

        'GridView1.Columns.Item(0).Caption = Common.res_man.GetString("gradecode", Common.cul)
        'GridView1.Columns.Item(1).Caption = Common.res_man.GetString("gradename", Common.cul)
        If Common.CatDel <> "Y" Then
            GridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = False
        Else
            GridControl1.EmbeddedNavigator.Buttons.Remove.Enabled = True
        End If
    End Sub
    Private Sub GridView1_InitNewRow(sender As System.Object, e As DevExpress.XtraGrid.Views.Grid.InitNewRowEventArgs) Handles GridView1.InitNewRow
        Dim view As GridView = CType(sender, GridView)
        view.SetRowCellValue(e.RowHandle, "LastModifiedDate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"))
        view.SetRowCellValue(e.RowHandle, "LastModifiedBy", "admin")
    End Sub
    Private Sub GridView1_RowDeleted(sender As System.Object, e As DevExpress.Data.RowDeletedEventArgs) Handles GridView1.RowDeleted
        Me.TblCatagoryTableAdapter.Update(Me.SSSDBDataSet.tblCatagory)
        Me.TblCatagory1TableAdapter1.Update(Me.SSSDBDataSet.tblCatagory1)
        XtraMessageBox.Show(ulf, "<size=10>" & Common.res_man.GetString("deletesuccess", Common.cul) & "</size>", Common.res_man.GetString("msgsuccess", Common.cul))
    End Sub
    Private Sub GridView1_RowUpdated(sender As System.Object, e As DevExpress.XtraGrid.Views.Base.RowObjectEventArgs) Handles GridView1.RowUpdated
        Me.TblCatagoryTableAdapter.Update(Me.SSSDBDataSet.tblCatagory)
        Me.TblCatagory1TableAdapter1.Update(Me.SSSDBDataSet.tblCatagory1)
    End Sub
    Private Sub GridControl1_EmbeddedNavigator_ButtonClick(sender As System.Object, e As DevExpress.XtraEditors.NavigatorButtonClickEventArgs) Handles GridControl1.EmbeddedNavigator.ButtonClick
        If e.Button.ButtonType = DevExpress.XtraEditors.NavigatorButtonType.Remove Then
            If XtraMessageBox.Show(ulf, "<size=10>" & Common.res_man.GetString("askdelete", Common.cul) & "</size>", Common.res_man.GetString("confirmdeleteion", Common.cul), _
                              MessageBoxButtons.YesNo, MessageBoxIcon.Question) <> DialogResult.Yes Then
                Me.Validate()
                e.Handled = True
                'MsgBox("Your records have been saved and updated successfully!")
            Else
                Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
                Dim CellId As String = row("CAT").ToString.Trim

                Dim adap As SqlDataAdapter
                Dim adapA As OleDbDataAdapter
                Dim ds As DataSet
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter("select count(*) from TblEmployee where CAT = '" & CellId & "'", Common.con1)
                    ds = New DataSet
                    adapA.Fill(ds)
                    If ds.Tables(0).Rows(0).Item(0).ToString > 0 Then
                        XtraMessageBox.Show(ulf, "<size=10>Category already assigned to Employee. Cannot delete.</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        Me.Validate()
                        e.Handled = True
                        Exit Sub
                    End If
                Else
                    adap = New SqlDataAdapter("select count(*) from TblEmployee where CAT = '" & CellId & "'", Common.con)
                    ds = New DataSet
                    adap.Fill(ds)
                    If ds.Tables(0).Rows(0).Item(0).ToString > 0 Then
                        XtraMessageBox.Show(ulf, "<size=10>Category already assigned to Employee. Cannot delete.</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        Me.Validate()
                        e.Handled = True
                        Exit Sub
                    End If
                End If
                Common.LogPost("Category Delete; Category Code='" & CellId)
            End If
        End If
    End Sub
    Private Sub GridView1_ValidateRow(sender As System.Object, e As DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs) Handles GridView1.ValidateRow
        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
        Dim CellId As String = row(0).ToString.Trim
        If CellId = "" Then
            e.Valid = False
            e.ErrorText = "<size=10>Category code cannot be empty " & Common.res_man.GetString("cannot_be_empty", Common.cul) & ","
        End If
        Dim cellname As String = row(1).ToString.Trim
        If cellname = "" Then
            e.Valid = False
            e.ErrorText = "<size=10>Category name cannot be empty " & Common.res_man.GetString("cannot_be_empty", Common.cul) & ","
        End If

        Dim view As GridView = CType(sender, GridView)
        view.SetRowCellValue(e.RowHandle, "LastModifiedDate", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"))
        view.SetRowCellValue(e.RowHandle, "LastModifiedBy", "admin")
    End Sub
    Private Sub GridView1_EditFormPrepared(sender As System.Object, e As DevExpress.XtraGrid.Views.Grid.EditFormPreparedEventArgs) Handles GridView1.EditFormPrepared
        For Each control As Control In e.Panel.Controls
            For Each button As Control In control.Controls
                If (button.Text = "Update") Then
                    button.Text = Common.res_man.GetString("save", Common.cul)
                End If
                If (button.Text = "Cancel") Then
                    button.Text = Common.res_man.GetString("cancel", Common.cul)
                End If
            Next
        Next
    End Sub
    Private Sub GridView1_CustomColumnDisplayText(sender As System.Object, e As DevExpress.XtraGrid.Views.Base.CustomColumnDisplayTextEventArgs) Handles GridView1.CustomColumnDisplayText
        Try
            Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
            Dim view As ColumnView = TryCast(sender, ColumnView)
            If e.Column.FieldName = "MaxLateDur" Then
                e.DisplayText = (view.GetListSourceRowCellValue(e.ListSourceRowIndex, "MaxLateDur").ToString().Trim \ 60).ToString("00") & ":" & (view.GetListSourceRowCellValue(e.ListSourceRowIndex, "MaxLateDur").ToString().Trim Mod 60).ToString("00")
            End If
            If e.Column.FieldName = "DeductFrom" Then
                Dim DeductFrom As String = view.GetListSourceRowCellValue(e.ListSourceRowIndex, "DeductFrom")
                If DeductFrom = "L" Then
                    e.DisplayText = "Leave"
                ElseIf DeductFrom = "A" Then
                    e.DisplayText = "Attendance"
                End If
            End If
        Catch
        End Try
    End Sub
    'Private Sub GridView1_EditFormShowing(sender As System.Object, e As DevExpress.XtraGrid.Views.Grid.EditFormShowingEventArgs) Handles GridView1.EditFormShowing

    '    Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
    '    Try

    '        Dim CellProtocol As String = row("LateVerification").ToString.Trim
    '        If row("LateVerification") = "Y" Then

    '            colDeductFrom.OptionsColumn.ReadOnly = False
    '            colEveryInterval.OptionsColumn.ReadOnly = False
    '            colFromLeave.OptionsColumn.ReadOnly = False
    '            colFromLeave1.OptionsColumn.ReadOnly = False
    '            colLateDays.OptionsColumn.ReadOnly = False
    '            colDeductDay.OptionsColumn.ReadOnly = False
    '            colMaxLateDur.OptionsColumn.ReadOnly = False
    '            GridView1.Columns("MaxLateDur").OptionsColumn.ReadOnly = False
    '            GridView1.Columns("MaxLateDur").OptionsColumn.AllowEdit = True
    '            MsgBox(GridView1.Columns("MaxLateDur").OptionsColumn.ReadOnly)
    '            MsgBox("Y")
    '        Else

    '            colDeductFrom.OptionsColumn.ReadOnly = True
    '            colEveryInterval.OptionsColumn.ReadOnly = True
    '            colFromLeave.OptionsColumn.ReadOnly = True
    '            colFromLeave1.OptionsColumn.ReadOnly = True
    '            colLateDays.OptionsColumn.ReadOnly = True
    '            colDeductDay.OptionsColumn.ReadOnly = True
    '            colMaxLateDur.OptionsColumn.ReadOnly = True
    '            GridView1.Columns("MaxLateDur").OptionsColumn.ReadOnly = True
    '            GridView1.Columns("MaxLateDur").OptionsColumn.AllowEdit = False
    '            MsgBox(GridView1.Columns("MaxLateDur").OptionsColumn.ReadOnly)
    '            MsgBox("N")
    '        End If
    '    Catch

    '        colDeductFrom.OptionsColumn.ReadOnly = False
    '        colEveryInterval.OptionsColumn.ReadOnly = False
    '        colFromLeave.OptionsColumn.ReadOnly = False
    '        colFromLeave1.OptionsColumn.ReadOnly = False
    '        colLateDays.OptionsColumn.ReadOnly = False
    '        colDeductDay.OptionsColumn.ReadOnly = False
    '        colMaxLateDur.OptionsColumn.ReadOnly = False
    '        GridView1.Columns("MaxLateDur").OptionsColumn.ReadOnly = False
    '        GridView1.Columns("MaxLateDur").OptionsColumn.AllowEdit = True
    '        MsgBox(GridView1.Columns("MaxLateDur").OptionsColumn.ReadOnly)
    '        MsgBox("catch")
    '    End Try
    'End Sub
    Private Sub GridView1_CellValueChanged(sender As System.Object, e As DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs) Handles GridView1.CellValueChanged
        Dim view As DevExpress.XtraGrid.Views.Grid.GridView = CType(GridControl1.FocusedView, GridView)
        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
        Try
            If e.Column.FieldName = "LateVerification" Then
                If row("LateVerification") = "Y" Then
                    MsgBox("n")
                    colDeductFrom.OptionsColumn.ReadOnly = False
                    colEveryInterval.OptionsColumn.ReadOnly = False
                    colFromLeave.OptionsColumn.ReadOnly = False
                    colFromLeave1.OptionsColumn.ReadOnly = False
                    colLateDays.OptionsColumn.ReadOnly = False
                    colDeductDay.OptionsColumn.ReadOnly = False
                    colMaxLateDur.OptionsColumn.ReadOnly = False
                Else
                    colDeductFrom.OptionsColumn.ReadOnly = True
                    colEveryInterval.OptionsColumn.ReadOnly = True
                    colFromLeave.OptionsColumn.ReadOnly = True
                    colFromLeave1.OptionsColumn.ReadOnly = True
                    colLateDays.OptionsColumn.ReadOnly = True
                    colDeductDay.OptionsColumn.ReadOnly = True
                    colMaxLateDur.OptionsColumn.ReadOnly = True
                End If

            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub GridView1_ShowingPopupEditForm(sender As System.Object, e As DevExpress.XtraGrid.Views.Grid.ShowingPopupEditFormEventArgs) Handles GridView1.ShowingPopupEditForm
        'Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
        'Try

        '    Dim CellProtocol As String = row("LateVerification").ToString.Trim
        '    If row("LateVerification") = "Y" Then

        '        colDeductFrom.OptionsColumn.ReadOnly = False
        '        colEveryInterval.OptionsColumn.ReadOnly = False
        '        colFromLeave.OptionsColumn.ReadOnly = False
        '        colFromLeave1.OptionsColumn.ReadOnly = False
        '        colLateDays.OptionsColumn.ReadOnly = False
        '        colDeductDay.OptionsColumn.ReadOnly = False
        '        colMaxLateDur.OptionsColumn.ReadOnly = False
        '        GridView1.Columns.Item("MaxLateDur").OptionsColumn.ReadOnly = False
        '        MsgBox(GridView1.Columns.Item("MaxLateDur").OptionsColumn.ReadOnly)
        '        MsgBox("Y")
        '    Else

        '        colDeductFrom.OptionsColumn.ReadOnly = True
        '        colEveryInterval.OptionsColumn.ReadOnly = True
        '        colFromLeave.OptionsColumn.ReadOnly = True
        '        colFromLeave1.OptionsColumn.ReadOnly = True
        '        colLateDays.OptionsColumn.ReadOnly = True
        '        colDeductDay.OptionsColumn.ReadOnly = True
        '        colMaxLateDur.OptionsColumn.ReadOnly = True
        '        GridView1.Columns.Item("MaxLateDur").OptionsColumn.ReadOnly = True
        '        MsgBox(GridView1.Columns.Item("MaxLateDur").OptionsColumn.ReadOnly)
        '        MsgBox("N")
        '    End If
        'Catch

        '    colDeductFrom.OptionsColumn.ReadOnly = False
        '    colEveryInterval.OptionsColumn.ReadOnly = False
        '    colFromLeave.OptionsColumn.ReadOnly = False
        '    colFromLeave1.OptionsColumn.ReadOnly = False
        '    colLateDays.OptionsColumn.ReadOnly = False
        '    colDeductDay.OptionsColumn.ReadOnly = False
        '    colMaxLateDur.OptionsColumn.ReadOnly = False
        '    GridView1.Columns.Item("MaxLateDur").OptionsColumn.ReadOnly = False
        '    MsgBox(GridView1.Columns.Item("MaxLateDur").OptionsColumn.ReadOnly)
        '    MsgBox("catch")
        'End Try

    End Sub
    Private Sub GridView1_EditFormShowing(sender As System.Object, e As DevExpress.XtraGrid.Views.Grid.EditFormShowingEventArgs) Handles GridView1.EditFormShowing

        If GridView1.IsNewItemRow(GridView1.FocusedRowHandle) = True Then
            If Common.CatAdd <> "Y" Then
                e.Allow = False
                Exit Sub
            End If
        Else
            If Common.CatModi <> "Y" Then
                e.Allow = False
                Exit Sub
            End If
        End If

        Dim row As System.Data.DataRow = GridView1.GetDataRow(GridView1.FocusedRowHandle)
        Try
            CatId = row("CAT").ToString.Trim
        Catch ex As Exception
            CatId = ""
        End Try
        e.Allow = False
        XtraCategoryEdit.ShowDialog()
        If Common.servername = "Access" Then   'to refresh the grid
            Me.TblCatagory1TableAdapter1.Fill(Me.SSSDBDataSet.tblCatagory1)
        Else
            Me.TblCatagoryTableAdapter.Fill(Me.SSSDBDataSet.tblCatagory)
        End If
    End Sub
End Class
