﻿Imports System.Resources
Imports System.Globalization
Public Class XtraReportMenu
    Public Sub New()
        InitializeComponent()
    End Sub

    Private Sub XtraReportMenu_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        NavigationPage1.Width = NavigationPage1.Parent.Width
        Me.Width = Me.Parent.Width
        Me.Height = Me.Parent.Height
        NavigationPane1.Width = NavigationPage1.Parent.Width
        'MsgBox(NavigationPane1.Width - NavigationPage1.Width)
        Common.NavHeight = NavigationPage1.Height
        Common.NavWidth = NavigationPage1.Width
    End Sub
    Private Sub NavigationPane1_SelectedPageIndexChanged(sender As System.Object, e As System.EventArgs) Handles NavigationPane1.SelectedPageIndexChanged
        If NavigationPane1.SelectedPageIndex = 0 Then
            NavigationPage1.Controls.Clear()
            Dim form As UserControl = New XtraReportsDaily
            form.Dock = DockStyle.Fill
            NavigationPage1.Controls.Add(form)
            form.Show()
        ElseIf NavigationPane1.SelectedPageIndex = 1 Then
            NavigationPage2.Controls.Clear()
            Dim form As UserControl = New XtraReportsMonthly
            form.Dock = DockStyle.Fill
            NavigationPage2.Controls.Add(form)
            form.Show()
            'ElseIf NavigationPane1.SelectedPageIndex = 2 Then
            '    NavigationPage3.Controls.Clear()
            '    Dim form As UserControl = New XtraDeparment
            '    form.Dock = DockStyle.Fill
            '    NavigationPage3.Controls.Add(form)
            '    form.Show()
        ElseIf NavigationPane1.SelectedPageIndex = 3 Then
            NavigationPage4.Controls.Clear()
            Dim form As UserControl = New XtraLeaveReport
            form.Dock = DockStyle.Fill
            NavigationPage4.Controls.Add(form)
            form.Show()
        ElseIf NavigationPane1.SelectedPageIndex = 4 Then
            NavigationPage5.Controls.Clear()
            Dim form As UserControl = New XtraCustomisedReportEdit
            form.Dock = DockStyle.Fill
            NavigationPage5.Controls.Add(form)
            form.Show()
            'ElseIf NavigationPane1.SelectedPageIndex = 5 Then
            '    NavigationPage6.Controls.Clear()
            '    Dim form As UserControl = New XtraEmployeeGroup
            '    form.Dock = DockStyle.Fill
            '    NavigationPage6.Controls.Add(form)
            '    form.Show()
            'ElseIf NavigationPane1.SelectedPageIndex = 6 Then
            '    NavigationPage7.Controls.Clear()
            '    Dim form As UserControl = New XtraBankMaster
            '    form.Dock = DockStyle.Fill
            '    NavigationPage7.Controls.Add(form)
            '    form.Show()
            'ElseIf NavigationPane1.SelectedPageIndex = 7 Then
            '    NavigationPage8.Controls.Clear()
            '    Dim form As UserControl = New XtraDespansary
            '    form.Dock = DockStyle.Fill
            '    NavigationPage8.Controls.Add(form)
            '    form.Show()
            'ElseIf NavigationPane1.SelectedPageIndex = 8 Then
            '    NavigationPage9.Controls.Clear()
            '    Dim form As UserControl = New XtraCategory
            '    form.Dock = DockStyle.Fill
            '    NavigationPage9.Controls.Add(form)
            '    form.Show()
        End If

    End Sub

End Class
