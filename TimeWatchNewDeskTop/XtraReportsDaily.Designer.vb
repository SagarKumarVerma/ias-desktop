﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraReportsDaily
    Inherits DevExpress.XtraEditors.XtraUserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(XtraReportsDaily))
        Me.SplitContainerControl1 = New DevExpress.XtraEditors.SplitContainerControl()
        Me.SidePanel3 = New DevExpress.XtraEditors.SidePanel()
        Me.PopupContainerControlEmp = New DevExpress.XtraEditors.PopupContainerControl()
        Me.GridControlEmp = New DevExpress.XtraGrid.GridControl()
        Me.TblEmployeeBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SSSDBDataSet = New iAS.SSSDBDataSet()
        Me.GridViewEmp = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colPAYCODE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colEMPNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTimeEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit()
        Me.PopupContainerControlDept = New DevExpress.XtraEditors.PopupContainerControl()
        Me.GridControlDept = New DevExpress.XtraGrid.GridControl()
        Me.TblDepartmentBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GridViewDept = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colDEPARTMENTCODE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDEPARTMENTNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTimeEdit3 = New DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit()
        Me.SimpleButton5 = New DevExpress.XtraEditors.SimpleButton()
        Me.PopupContainerControlComp = New DevExpress.XtraEditors.PopupContainerControl()
        Me.GridControlComp = New DevExpress.XtraGrid.GridControl()
        Me.TblCompanyBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GridViewComp = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colCOMPANYCODE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCOMPANYNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTimeEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit()
        Me.SimpleButton4 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton3 = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton2 = New DevExpress.XtraEditors.SimpleButton()
        Me.SidePanel2 = New DevExpress.XtraEditors.SidePanel()
        Me.PopupContainerControlGrade = New DevExpress.XtraEditors.PopupContainerControl()
        Me.GridControlGrade = New DevExpress.XtraGrid.GridControl()
        Me.TblGradeBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GridViewGrade = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.GridColumn1 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn2 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTimeEdit6 = New DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit()
        Me.PopupContainerControlShift = New DevExpress.XtraEditors.PopupContainerControl()
        Me.GridControlShift = New DevExpress.XtraGrid.GridControl()
        Me.TblShiftMasterBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GridViewShift = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colSHIFT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSTARTTIME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemDateEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit()
        Me.colENDTIME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTimeEdit5 = New DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit()
        Me.PopupContainerControlCat = New DevExpress.XtraEditors.PopupContainerControl()
        Me.GridControlCat = New DevExpress.XtraGrid.GridControl()
        Me.TblCatagoryBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GridViewCat = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colCAT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colCATAGORYNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTimeEdit4 = New DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit()
        Me.PanelControl1 = New DevExpress.XtraEditors.PanelControl()
        Me.CheckMachineRawPunchAll = New DevExpress.XtraEditors.CheckEdit()
        Me.PopupContainerControlBranch = New DevExpress.XtraEditors.PopupContainerControl()
        Me.GridControlBranch = New DevExpress.XtraGrid.GridControl()
        Me.TblbranchBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GridViewBranch = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colBRANCHCODE = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colBRANCHNAME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTimeEdit7 = New DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit()
        Me.RepositoryItemDateEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemDateEdit()
        Me.CheckContinuousLateArrival = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditVReport = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditTemperature = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckMultiShift = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckPunchImage = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckPhotoReport = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl13 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.TextToTime = New DevExpress.XtraEditors.TextEdit()
        Me.TextFromTime = New DevExpress.XtraEditors.TextEdit()
        Me.CheckInOut = New DevExpress.XtraEditors.CheckEdit()
        Me.PopupContainerEditDevice = New DevExpress.XtraEditors.PopupContainerEdit()
        Me.PopupContainerControlDevice = New DevExpress.XtraEditors.PopupContainerControl()
        Me.GridControlDevice = New DevExpress.XtraGrid.GridControl()
        Me.TblMachineBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GridViewDevice = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colID_NO = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLOCATION = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colbranch = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colDeviceType = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colA_R = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.GridColumn3 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colcommkey = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colIN_OUT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.CheckDeviceWise = New DevExpress.XtraEditors.CheckEdit()
        Me.TextEdit2 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.TextEdit1 = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.CheckDeptSkip = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckContinuousAbsenteeism = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckContinuousEarlyDeparture = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckCustomized = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckPresentReport = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckMissingAndReverificaton = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckManualPunchAudit = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckOutWork = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckMachineRawPunch = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckShiftChangeStatement = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckShiftWisePresence = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckOverTimeSummary = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEarlyArrival = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckOverTime = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckDepartmentSummary = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckTimeLoss = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckAttendance = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEarlyDeparture = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckAbsenteeism = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckDailyPerformance = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckLateArr = New DevExpress.XtraEditors.CheckEdit()
        Me.SidePanel1 = New DevExpress.XtraEditors.SidePanel()
        Me.ComboNepaliYearTo = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.SidePanelSelection = New DevExpress.XtraEditors.SidePanel()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.PopupContainerEditEmp = New DevExpress.XtraEditors.PopupContainerEdit()
        Me.PopupContainerEditGrade = New DevExpress.XtraEditors.PopupContainerEdit()
        Me.CheckEdit1 = New DevExpress.XtraEditors.CheckEdit()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.CheckEdit2 = New DevExpress.XtraEditors.CheckEdit()
        Me.PopupContainerEditLocation = New DevExpress.XtraEditors.PopupContainerEdit()
        Me.PopupContainerEditComp = New DevExpress.XtraEditors.PopupContainerEdit()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.PopupContainerEditShift = New DevExpress.XtraEditors.PopupContainerEdit()
        Me.PopupContainerEditDept = New DevExpress.XtraEditors.PopupContainerEdit()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.PopupContainerEditCat = New DevExpress.XtraEditors.PopupContainerEdit()
        Me.ComboNEpaliMonthTo = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.CheckPDF = New DevExpress.XtraEditors.CheckEdit()
        Me.ComboNepaliDateTo = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.CheckExcel = New DevExpress.XtraEditors.CheckEdit()
        Me.ComboNepaliYearFrm = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.CheckText = New DevExpress.XtraEditors.CheckEdit()
        Me.ComboNEpaliMonthFrm = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.ComboNepaliDateFrm = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.DateEdit2 = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.DateEdit1 = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.MemoEdit1 = New DevExpress.XtraEditors.MemoEdit()
        Me.TblDepartmentTableAdapter = New iAS.SSSDBDataSetTableAdapters.tblDepartmentTableAdapter()
        Me.TblEmployee1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.TblEmployee1TableAdapter()
        Me.TblEmployeeTableAdapter = New iAS.SSSDBDataSetTableAdapters.TblEmployeeTableAdapter()
        Me.TblCompany1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.tblCompany1TableAdapter()
        Me.TblDepartment1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.tblDepartment1TableAdapter()
        Me.TblCatagoryTableAdapter = New iAS.SSSDBDataSetTableAdapters.tblCatagoryTableAdapter()
        Me.TblGradeTableAdapter = New iAS.SSSDBDataSetTableAdapters.tblGradeTableAdapter()
        Me.TblShiftMaster1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.tblShiftMaster1TableAdapter()
        Me.TblShiftMasterTableAdapter = New iAS.SSSDBDataSetTableAdapters.tblShiftMasterTableAdapter()
        Me.TblCatagory1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.tblCatagory1TableAdapter()
        Me.TblCompanyTableAdapter = New iAS.SSSDBDataSetTableAdapters.tblCompanyTableAdapter()
        Me.TblGrade1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.tblGrade1TableAdapter()
        Me.TblbranchTableAdapter = New iAS.SSSDBDataSetTableAdapters.tblbranchTableAdapter()
        Me.Tblbranch1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.tblbranch1TableAdapter()
        Me.TblMachine1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.tblMachine1TableAdapter()
        Me.TblMachineTableAdapter1 = New iAS.SSSDBDataSetTableAdapters.tblMachineTableAdapter()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerControl1.SuspendLayout()
        Me.SidePanel3.SuspendLayout()
        CType(Me.PopupContainerControlEmp, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PopupContainerControlEmp.SuspendLayout()
        CType(Me.GridControlEmp, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblEmployeeBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewEmp, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTimeEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerControlDept, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PopupContainerControlDept.SuspendLayout()
        CType(Me.GridControlDept, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblDepartmentBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewDept, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTimeEdit3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerControlComp, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PopupContainerControlComp.SuspendLayout()
        CType(Me.GridControlComp, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblCompanyBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewComp, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTimeEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SidePanel2.SuspendLayout()
        CType(Me.PopupContainerControlGrade, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PopupContainerControlGrade.SuspendLayout()
        CType(Me.GridControlGrade, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblGradeBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewGrade, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTimeEdit6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerControlShift, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PopupContainerControlShift.SuspendLayout()
        CType(Me.GridControlShift, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblShiftMasterBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewShift, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit1.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTimeEdit5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerControlCat, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PopupContainerControlCat.SuspendLayout()
        CType(Me.GridControlCat, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblCatagoryBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewCat, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTimeEdit4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanelControl1.SuspendLayout()
        CType(Me.CheckMachineRawPunchAll.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerControlBranch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PopupContainerControlBranch.SuspendLayout()
        CType(Me.GridControlBranch, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblbranchBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewBranch, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTimeEdit7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemDateEdit2.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckContinuousLateArrival.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditVReport.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditTemperature.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckMultiShift.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckPunchImage.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckPhotoReport.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextToTime.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextFromTime.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckInOut.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerEditDevice.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerControlDevice, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PopupContainerControlDevice.SuspendLayout()
        CType(Me.GridControlDevice, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblMachineBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridViewDevice, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckDeviceWise.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckDeptSkip.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckContinuousAbsenteeism.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckContinuousEarlyDeparture.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckCustomized.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckPresentReport.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckMissingAndReverificaton.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckManualPunchAudit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckOutWork.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckMachineRawPunch.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckShiftChangeStatement.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckShiftWisePresence.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckOverTimeSummary.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEarlyArrival.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckOverTime.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckDepartmentSummary.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckTimeLoss.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckAttendance.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEarlyDeparture.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckAbsenteeism.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckDailyPerformance.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckLateArr.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SidePanel1.SuspendLayout()
        CType(Me.ComboNepaliYearTo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SidePanelSelection.SuspendLayout()
        CType(Me.PopupContainerEditEmp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerEditGrade.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerEditLocation.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerEditComp.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerEditShift.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerEditDept.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PopupContainerEditCat.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboNEpaliMonthTo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckPDF.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboNepaliDateTo.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckExcel.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboNepaliYearFrm.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckText.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboNEpaliMonthFrm.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboNepaliDateFrm.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit2.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit1.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SplitContainerControl1
        '
        Me.SplitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainerControl1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainerControl1.Name = "SplitContainerControl1"
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.SidePanel3)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.SidePanel2)
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.SidePanel1)
        Me.SplitContainerControl1.Panel1.Text = "Panel1"
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.MemoEdit1)
        Me.SplitContainerControl1.Panel2.Text = "Panel2"
        Me.SplitContainerControl1.Size = New System.Drawing.Size(1145, 568)
        Me.SplitContainerControl1.SplitterPosition = 1036
        Me.SplitContainerControl1.TabIndex = 2
        Me.SplitContainerControl1.Text = "SplitContainerControl1"
        '
        'SidePanel3
        '
        Me.SidePanel3.Controls.Add(Me.PopupContainerControlEmp)
        Me.SidePanel3.Controls.Add(Me.PopupContainerControlDept)
        Me.SidePanel3.Controls.Add(Me.SimpleButton5)
        Me.SidePanel3.Controls.Add(Me.PopupContainerControlComp)
        Me.SidePanel3.Controls.Add(Me.SimpleButton4)
        Me.SidePanel3.Controls.Add(Me.SimpleButton3)
        Me.SidePanel3.Controls.Add(Me.SimpleButton2)
        Me.SidePanel3.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.SidePanel3.Location = New System.Drawing.Point(0, 514)
        Me.SidePanel3.Name = "SidePanel3"
        Me.SidePanel3.Size = New System.Drawing.Size(1036, 54)
        Me.SidePanel3.TabIndex = 4
        Me.SidePanel3.Text = "SidePanel3"
        Me.SidePanel3.Visible = False
        '
        'PopupContainerControlEmp
        '
        Me.PopupContainerControlEmp.Controls.Add(Me.GridControlEmp)
        Me.PopupContainerControlEmp.Location = New System.Drawing.Point(793, 10)
        Me.PopupContainerControlEmp.Name = "PopupContainerControlEmp"
        Me.PopupContainerControlEmp.Size = New System.Drawing.Size(300, 300)
        Me.PopupContainerControlEmp.TabIndex = 10
        '
        'GridControlEmp
        '
        Me.GridControlEmp.DataSource = Me.TblEmployeeBindingSource
        Me.GridControlEmp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControlEmp.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridControlEmp.Location = New System.Drawing.Point(0, 0)
        Me.GridControlEmp.MainView = Me.GridViewEmp
        Me.GridControlEmp.Name = "GridControlEmp"
        Me.GridControlEmp.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTimeEdit1})
        Me.GridControlEmp.Size = New System.Drawing.Size(300, 300)
        Me.GridControlEmp.TabIndex = 6
        Me.GridControlEmp.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewEmp})
        '
        'TblEmployeeBindingSource
        '
        Me.TblEmployeeBindingSource.DataMember = "TblEmployee"
        Me.TblEmployeeBindingSource.DataSource = Me.SSSDBDataSet
        '
        'SSSDBDataSet
        '
        Me.SSSDBDataSet.DataSetName = "SSSDBDataSet"
        Me.SSSDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'GridViewEmp
        '
        Me.GridViewEmp.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colPAYCODE, Me.colEMPNAME})
        Me.GridViewEmp.GridControl = Me.GridControlEmp
        Me.GridViewEmp.Name = "GridViewEmp"
        Me.GridViewEmp.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewEmp.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewEmp.OptionsBehavior.Editable = False
        Me.GridViewEmp.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridViewEmp.OptionsSelection.MultiSelect = True
        Me.GridViewEmp.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        Me.GridViewEmp.OptionsView.ColumnAutoWidth = False
        '
        'colPAYCODE
        '
        Me.colPAYCODE.Caption = "Paycode"
        Me.colPAYCODE.FieldName = "PAYCODE"
        Me.colPAYCODE.Name = "colPAYCODE"
        Me.colPAYCODE.Visible = True
        Me.colPAYCODE.VisibleIndex = 1
        '
        'colEMPNAME
        '
        Me.colEMPNAME.Caption = "Name"
        Me.colEMPNAME.FieldName = "EMPNAME"
        Me.colEMPNAME.Name = "colEMPNAME"
        Me.colEMPNAME.Visible = True
        Me.colEMPNAME.VisibleIndex = 2
        Me.colEMPNAME.Width = 125
        '
        'RepositoryItemTimeEdit1
        '
        Me.RepositoryItemTimeEdit1.AutoHeight = False
        Me.RepositoryItemTimeEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemTimeEdit1.Mask.EditMask = "HH:mm"
        Me.RepositoryItemTimeEdit1.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemTimeEdit1.Name = "RepositoryItemTimeEdit1"
        '
        'PopupContainerControlDept
        '
        Me.PopupContainerControlDept.Controls.Add(Me.GridControlDept)
        Me.PopupContainerControlDept.Location = New System.Drawing.Point(487, 9)
        Me.PopupContainerControlDept.Name = "PopupContainerControlDept"
        Me.PopupContainerControlDept.Size = New System.Drawing.Size(300, 300)
        Me.PopupContainerControlDept.TabIndex = 14
        '
        'GridControlDept
        '
        Me.GridControlDept.DataSource = Me.TblDepartmentBindingSource
        Me.GridControlDept.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControlDept.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridControlDept.Location = New System.Drawing.Point(0, 0)
        Me.GridControlDept.MainView = Me.GridViewDept
        Me.GridControlDept.Name = "GridControlDept"
        Me.GridControlDept.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTimeEdit3})
        Me.GridControlDept.Size = New System.Drawing.Size(300, 300)
        Me.GridControlDept.TabIndex = 6
        Me.GridControlDept.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewDept})
        '
        'TblDepartmentBindingSource
        '
        Me.TblDepartmentBindingSource.DataMember = "tblDepartment"
        Me.TblDepartmentBindingSource.DataSource = Me.SSSDBDataSet
        '
        'GridViewDept
        '
        Me.GridViewDept.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colDEPARTMENTCODE, Me.colDEPARTMENTNAME})
        Me.GridViewDept.GridControl = Me.GridControlDept
        Me.GridViewDept.Name = "GridViewDept"
        Me.GridViewDept.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewDept.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewDept.OptionsBehavior.Editable = False
        Me.GridViewDept.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridViewDept.OptionsSelection.MultiSelect = True
        Me.GridViewDept.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        Me.GridViewDept.OptionsView.ColumnAutoWidth = False
        Me.GridViewDept.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colDEPARTMENTCODE, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colDEPARTMENTCODE
        '
        Me.colDEPARTMENTCODE.Caption = "Depatment Code"
        Me.colDEPARTMENTCODE.FieldName = "DEPARTMENTCODE"
        Me.colDEPARTMENTCODE.Name = "colDEPARTMENTCODE"
        Me.colDEPARTMENTCODE.Visible = True
        Me.colDEPARTMENTCODE.VisibleIndex = 1
        Me.colDEPARTMENTCODE.Width = 100
        '
        'colDEPARTMENTNAME
        '
        Me.colDEPARTMENTNAME.Caption = "Name"
        Me.colDEPARTMENTNAME.FieldName = "DEPARTMENTNAME"
        Me.colDEPARTMENTNAME.Name = "colDEPARTMENTNAME"
        Me.colDEPARTMENTNAME.Visible = True
        Me.colDEPARTMENTNAME.VisibleIndex = 2
        Me.colDEPARTMENTNAME.Width = 120
        '
        'RepositoryItemTimeEdit3
        '
        Me.RepositoryItemTimeEdit3.AutoHeight = False
        Me.RepositoryItemTimeEdit3.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemTimeEdit3.Mask.EditMask = "HH:mm"
        Me.RepositoryItemTimeEdit3.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemTimeEdit3.Name = "RepositoryItemTimeEdit3"
        '
        'SimpleButton5
        '
        Me.SimpleButton5.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButton5.Appearance.Options.UseFont = True
        Me.SimpleButton5.Location = New System.Drawing.Point(84, 10)
        Me.SimpleButton5.Name = "SimpleButton5"
        Me.SimpleButton5.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton5.TabIndex = 3
        Me.SimpleButton5.Text = "Excel"
        Me.SimpleButton5.Visible = False
        '
        'PopupContainerControlComp
        '
        Me.PopupContainerControlComp.Controls.Add(Me.GridControlComp)
        Me.PopupContainerControlComp.Location = New System.Drawing.Point(327, 10)
        Me.PopupContainerControlComp.Name = "PopupContainerControlComp"
        Me.PopupContainerControlComp.Size = New System.Drawing.Size(300, 300)
        Me.PopupContainerControlComp.TabIndex = 13
        '
        'GridControlComp
        '
        Me.GridControlComp.DataSource = Me.TblCompanyBindingSource
        Me.GridControlComp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControlComp.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridControlComp.Location = New System.Drawing.Point(0, 0)
        Me.GridControlComp.MainView = Me.GridViewComp
        Me.GridControlComp.Name = "GridControlComp"
        Me.GridControlComp.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTimeEdit2})
        Me.GridControlComp.Size = New System.Drawing.Size(300, 300)
        Me.GridControlComp.TabIndex = 6
        Me.GridControlComp.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewComp})
        '
        'TblCompanyBindingSource
        '
        Me.TblCompanyBindingSource.DataMember = "tblCompany"
        Me.TblCompanyBindingSource.DataSource = Me.SSSDBDataSet
        '
        'GridViewComp
        '
        Me.GridViewComp.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colCOMPANYCODE, Me.colCOMPANYNAME})
        Me.GridViewComp.GridControl = Me.GridControlComp
        Me.GridViewComp.Name = "GridViewComp"
        Me.GridViewComp.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewComp.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewComp.OptionsBehavior.Editable = False
        Me.GridViewComp.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridViewComp.OptionsSelection.MultiSelect = True
        Me.GridViewComp.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        Me.GridViewComp.OptionsView.ColumnAutoWidth = False
        Me.GridViewComp.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colCOMPANYCODE, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colCOMPANYCODE
        '
        Me.colCOMPANYCODE.Caption = "Company Code"
        Me.colCOMPANYCODE.FieldName = "COMPANYCODE"
        Me.colCOMPANYCODE.Name = "colCOMPANYCODE"
        Me.colCOMPANYCODE.Visible = True
        Me.colCOMPANYCODE.VisibleIndex = 1
        Me.colCOMPANYCODE.Width = 100
        '
        'colCOMPANYNAME
        '
        Me.colCOMPANYNAME.Caption = "Name"
        Me.colCOMPANYNAME.FieldName = "COMPANYNAME"
        Me.colCOMPANYNAME.Name = "colCOMPANYNAME"
        Me.colCOMPANYNAME.Visible = True
        Me.colCOMPANYNAME.VisibleIndex = 2
        Me.colCOMPANYNAME.Width = 120
        '
        'RepositoryItemTimeEdit2
        '
        Me.RepositoryItemTimeEdit2.AutoHeight = False
        Me.RepositoryItemTimeEdit2.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemTimeEdit2.Mask.EditMask = "HH:mm"
        Me.RepositoryItemTimeEdit2.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemTimeEdit2.Name = "RepositoryItemTimeEdit2"
        '
        'SimpleButton4
        '
        Me.SimpleButton4.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButton4.Appearance.Options.UseFont = True
        Me.SimpleButton4.Location = New System.Drawing.Point(246, 9)
        Me.SimpleButton4.Name = "SimpleButton4"
        Me.SimpleButton4.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton4.TabIndex = 2
        Me.SimpleButton4.Text = "PDF"
        Me.SimpleButton4.Visible = False
        '
        'SimpleButton3
        '
        Me.SimpleButton3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButton3.Appearance.Options.UseFont = True
        Me.SimpleButton3.Location = New System.Drawing.Point(3, 9)
        Me.SimpleButton3.Name = "SimpleButton3"
        Me.SimpleButton3.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton3.TabIndex = 1
        Me.SimpleButton3.Text = "Text"
        Me.SimpleButton3.Visible = False
        '
        'SimpleButton2
        '
        Me.SimpleButton2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButton2.Appearance.Options.UseFont = True
        Me.SimpleButton2.Location = New System.Drawing.Point(165, 9)
        Me.SimpleButton2.Name = "SimpleButton2"
        Me.SimpleButton2.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton2.TabIndex = 0
        Me.SimpleButton2.Text = "Excel"
        Me.SimpleButton2.Visible = False
        '
        'SidePanel2
        '
        Me.SidePanel2.Controls.Add(Me.PopupContainerControlGrade)
        Me.SidePanel2.Controls.Add(Me.PopupContainerControlShift)
        Me.SidePanel2.Controls.Add(Me.PopupContainerControlCat)
        Me.SidePanel2.Controls.Add(Me.PanelControl1)
        Me.SidePanel2.Dock = System.Windows.Forms.DockStyle.Top
        Me.SidePanel2.Location = New System.Drawing.Point(0, 202)
        Me.SidePanel2.Name = "SidePanel2"
        Me.SidePanel2.Size = New System.Drawing.Size(1036, 477)
        Me.SidePanel2.TabIndex = 3
        Me.SidePanel2.Text = "SidePanel2"
        '
        'PopupContainerControlGrade
        '
        Me.PopupContainerControlGrade.Controls.Add(Me.GridControlGrade)
        Me.PopupContainerControlGrade.Location = New System.Drawing.Point(225, 300)
        Me.PopupContainerControlGrade.Name = "PopupContainerControlGrade"
        Me.PopupContainerControlGrade.Size = New System.Drawing.Size(300, 300)
        Me.PopupContainerControlGrade.TabIndex = 23
        '
        'GridControlGrade
        '
        Me.GridControlGrade.DataSource = Me.TblGradeBindingSource
        Me.GridControlGrade.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControlGrade.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridControlGrade.Location = New System.Drawing.Point(0, 0)
        Me.GridControlGrade.MainView = Me.GridViewGrade
        Me.GridControlGrade.Name = "GridControlGrade"
        Me.GridControlGrade.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTimeEdit6})
        Me.GridControlGrade.Size = New System.Drawing.Size(300, 300)
        Me.GridControlGrade.TabIndex = 6
        Me.GridControlGrade.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewGrade})
        '
        'TblGradeBindingSource
        '
        Me.TblGradeBindingSource.DataMember = "tblGrade"
        Me.TblGradeBindingSource.DataSource = Me.SSSDBDataSet
        '
        'GridViewGrade
        '
        Me.GridViewGrade.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.GridColumn1, Me.GridColumn2})
        Me.GridViewGrade.GridControl = Me.GridControlGrade
        Me.GridViewGrade.Name = "GridViewGrade"
        Me.GridViewGrade.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewGrade.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewGrade.OptionsBehavior.Editable = False
        Me.GridViewGrade.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridViewGrade.OptionsSelection.MultiSelect = True
        Me.GridViewGrade.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        Me.GridViewGrade.OptionsView.ColumnAutoWidth = False
        Me.GridViewGrade.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.GridColumn1, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'GridColumn1
        '
        Me.GridColumn1.Caption = "Grade Code"
        Me.GridColumn1.FieldName = "GradeCode"
        Me.GridColumn1.Name = "GridColumn1"
        Me.GridColumn1.Visible = True
        Me.GridColumn1.VisibleIndex = 1
        Me.GridColumn1.Width = 100
        '
        'GridColumn2
        '
        Me.GridColumn2.Caption = "Name"
        Me.GridColumn2.FieldName = "GradeName"
        Me.GridColumn2.Name = "GridColumn2"
        Me.GridColumn2.Visible = True
        Me.GridColumn2.VisibleIndex = 2
        Me.GridColumn2.Width = 120
        '
        'RepositoryItemTimeEdit6
        '
        Me.RepositoryItemTimeEdit6.AutoHeight = False
        Me.RepositoryItemTimeEdit6.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemTimeEdit6.Mask.EditMask = "HH:mm"
        Me.RepositoryItemTimeEdit6.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemTimeEdit6.Name = "RepositoryItemTimeEdit6"
        '
        'PopupContainerControlShift
        '
        Me.PopupContainerControlShift.Controls.Add(Me.GridControlShift)
        Me.PopupContainerControlShift.Location = New System.Drawing.Point(837, 300)
        Me.PopupContainerControlShift.Name = "PopupContainerControlShift"
        Me.PopupContainerControlShift.Size = New System.Drawing.Size(300, 300)
        Me.PopupContainerControlShift.TabIndex = 16
        '
        'GridControlShift
        '
        Me.GridControlShift.DataSource = Me.TblShiftMasterBindingSource
        Me.GridControlShift.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControlShift.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridControlShift.Location = New System.Drawing.Point(0, 0)
        Me.GridControlShift.MainView = Me.GridViewShift
        Me.GridControlShift.Name = "GridControlShift"
        Me.GridControlShift.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTimeEdit5, Me.RepositoryItemDateEdit1})
        Me.GridControlShift.Size = New System.Drawing.Size(300, 300)
        Me.GridControlShift.TabIndex = 6
        Me.GridControlShift.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewShift})
        '
        'TblShiftMasterBindingSource
        '
        Me.TblShiftMasterBindingSource.DataMember = "tblShiftMaster"
        Me.TblShiftMasterBindingSource.DataSource = Me.SSSDBDataSet
        '
        'GridViewShift
        '
        Me.GridViewShift.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colSHIFT, Me.colSTARTTIME, Me.colENDTIME})
        Me.GridViewShift.GridControl = Me.GridControlShift
        Me.GridViewShift.Name = "GridViewShift"
        Me.GridViewShift.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewShift.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewShift.OptionsBehavior.Editable = False
        Me.GridViewShift.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridViewShift.OptionsSelection.MultiSelect = True
        Me.GridViewShift.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        Me.GridViewShift.OptionsView.ColumnAutoWidth = False
        Me.GridViewShift.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colSHIFT, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colSHIFT
        '
        Me.colSHIFT.Caption = "Shift"
        Me.colSHIFT.FieldName = "SHIFT"
        Me.colSHIFT.Name = "colSHIFT"
        Me.colSHIFT.Visible = True
        Me.colSHIFT.VisibleIndex = 1
        '
        'colSTARTTIME
        '
        Me.colSTARTTIME.Caption = "Start Time"
        Me.colSTARTTIME.ColumnEdit = Me.RepositoryItemDateEdit1
        Me.colSTARTTIME.FieldName = "STARTTIME"
        Me.colSTARTTIME.Name = "colSTARTTIME"
        Me.colSTARTTIME.Visible = True
        Me.colSTARTTIME.VisibleIndex = 2
        '
        'RepositoryItemDateEdit1
        '
        Me.RepositoryItemDateEdit1.AutoHeight = False
        Me.RepositoryItemDateEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit1.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit1.Mask.EditMask = "HH:mm"
        Me.RepositoryItemDateEdit1.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemDateEdit1.Name = "RepositoryItemDateEdit1"
        '
        'colENDTIME
        '
        Me.colENDTIME.Caption = "End Time"
        Me.colENDTIME.ColumnEdit = Me.RepositoryItemDateEdit1
        Me.colENDTIME.FieldName = "ENDTIME"
        Me.colENDTIME.Name = "colENDTIME"
        Me.colENDTIME.Visible = True
        Me.colENDTIME.VisibleIndex = 3
        '
        'RepositoryItemTimeEdit5
        '
        Me.RepositoryItemTimeEdit5.AutoHeight = False
        Me.RepositoryItemTimeEdit5.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemTimeEdit5.Mask.EditMask = "HH:mm"
        Me.RepositoryItemTimeEdit5.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemTimeEdit5.Name = "RepositoryItemTimeEdit5"
        '
        'PopupContainerControlCat
        '
        Me.PopupContainerControlCat.Controls.Add(Me.GridControlCat)
        Me.PopupContainerControlCat.Location = New System.Drawing.Point(531, 300)
        Me.PopupContainerControlCat.Name = "PopupContainerControlCat"
        Me.PopupContainerControlCat.Size = New System.Drawing.Size(300, 300)
        Me.PopupContainerControlCat.TabIndex = 15
        '
        'GridControlCat
        '
        Me.GridControlCat.DataSource = Me.TblCatagoryBindingSource
        Me.GridControlCat.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControlCat.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridControlCat.Location = New System.Drawing.Point(0, 0)
        Me.GridControlCat.MainView = Me.GridViewCat
        Me.GridControlCat.Name = "GridControlCat"
        Me.GridControlCat.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTimeEdit4})
        Me.GridControlCat.Size = New System.Drawing.Size(300, 300)
        Me.GridControlCat.TabIndex = 6
        Me.GridControlCat.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewCat})
        '
        'TblCatagoryBindingSource
        '
        Me.TblCatagoryBindingSource.DataMember = "tblCatagory"
        Me.TblCatagoryBindingSource.DataSource = Me.SSSDBDataSet
        '
        'GridViewCat
        '
        Me.GridViewCat.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colCAT, Me.colCATAGORYNAME})
        Me.GridViewCat.GridControl = Me.GridControlCat
        Me.GridViewCat.Name = "GridViewCat"
        Me.GridViewCat.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewCat.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewCat.OptionsBehavior.Editable = False
        Me.GridViewCat.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridViewCat.OptionsSelection.MultiSelect = True
        Me.GridViewCat.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        Me.GridViewCat.OptionsView.ColumnAutoWidth = False
        Me.GridViewCat.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colCAT, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colCAT
        '
        Me.colCAT.Caption = "Catagory Code"
        Me.colCAT.FieldName = "CAT"
        Me.colCAT.Name = "colCAT"
        Me.colCAT.Visible = True
        Me.colCAT.VisibleIndex = 1
        Me.colCAT.Width = 100
        '
        'colCATAGORYNAME
        '
        Me.colCATAGORYNAME.Caption = "Name"
        Me.colCATAGORYNAME.FieldName = "CATAGORYNAME"
        Me.colCATAGORYNAME.Name = "colCATAGORYNAME"
        Me.colCATAGORYNAME.Visible = True
        Me.colCATAGORYNAME.VisibleIndex = 2
        Me.colCATAGORYNAME.Width = 120
        '
        'RepositoryItemTimeEdit4
        '
        Me.RepositoryItemTimeEdit4.AutoHeight = False
        Me.RepositoryItemTimeEdit4.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemTimeEdit4.Mask.EditMask = "HH:mm"
        Me.RepositoryItemTimeEdit4.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemTimeEdit4.Name = "RepositoryItemTimeEdit4"
        '
        'PanelControl1
        '
        Me.PanelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple
        Me.PanelControl1.Controls.Add(Me.CheckMachineRawPunchAll)
        Me.PanelControl1.Controls.Add(Me.PopupContainerControlBranch)
        Me.PanelControl1.Controls.Add(Me.CheckEditVReport)
        Me.PanelControl1.Controls.Add(Me.CheckEditTemperature)
        Me.PanelControl1.Controls.Add(Me.CheckMultiShift)
        Me.PanelControl1.Controls.Add(Me.CheckPunchImage)
        Me.PanelControl1.Controls.Add(Me.CheckPhotoReport)
        Me.PanelControl1.Controls.Add(Me.LabelControl13)
        Me.PanelControl1.Controls.Add(Me.LabelControl12)
        Me.PanelControl1.Controls.Add(Me.TextToTime)
        Me.PanelControl1.Controls.Add(Me.TextFromTime)
        Me.PanelControl1.Controls.Add(Me.CheckInOut)
        Me.PanelControl1.Controls.Add(Me.PopupContainerEditDevice)
        Me.PanelControl1.Controls.Add(Me.CheckDeviceWise)
        Me.PanelControl1.Controls.Add(Me.TextEdit2)
        Me.PanelControl1.Controls.Add(Me.LabelControl11)
        Me.PanelControl1.Controls.Add(Me.TextEdit1)
        Me.PanelControl1.Controls.Add(Me.LabelControl10)
        Me.PanelControl1.Controls.Add(Me.CheckDeptSkip)
        Me.PanelControl1.Controls.Add(Me.CheckContinuousAbsenteeism)
        Me.PanelControl1.Controls.Add(Me.CheckContinuousEarlyDeparture)
        Me.PanelControl1.Controls.Add(Me.CheckCustomized)
        Me.PanelControl1.Controls.Add(Me.CheckPresentReport)
        Me.PanelControl1.Controls.Add(Me.CheckMissingAndReverificaton)
        Me.PanelControl1.Controls.Add(Me.CheckManualPunchAudit)
        Me.PanelControl1.Controls.Add(Me.CheckOutWork)
        Me.PanelControl1.Controls.Add(Me.CheckMachineRawPunch)
        Me.PanelControl1.Controls.Add(Me.CheckShiftChangeStatement)
        Me.PanelControl1.Controls.Add(Me.CheckShiftWisePresence)
        Me.PanelControl1.Controls.Add(Me.CheckOverTimeSummary)
        Me.PanelControl1.Controls.Add(Me.CheckEarlyArrival)
        Me.PanelControl1.Controls.Add(Me.CheckOverTime)
        Me.PanelControl1.Controls.Add(Me.CheckDepartmentSummary)
        Me.PanelControl1.Controls.Add(Me.CheckTimeLoss)
        Me.PanelControl1.Controls.Add(Me.CheckAttendance)
        Me.PanelControl1.Controls.Add(Me.CheckEarlyDeparture)
        Me.PanelControl1.Controls.Add(Me.CheckAbsenteeism)
        Me.PanelControl1.Controls.Add(Me.CheckDailyPerformance)
        Me.PanelControl1.Controls.Add(Me.CheckLateArr)
        Me.PanelControl1.Location = New System.Drawing.Point(16, 16)
        Me.PanelControl1.Name = "PanelControl1"
        Me.PanelControl1.Size = New System.Drawing.Size(923, 278)
        Me.PanelControl1.TabIndex = 12
        '
        'CheckMachineRawPunchAll
        '
        Me.CheckMachineRawPunchAll.Location = New System.Drawing.Point(15, 192)
        Me.CheckMachineRawPunchAll.Name = "CheckMachineRawPunchAll"
        Me.CheckMachineRawPunchAll.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckMachineRawPunchAll.Properties.Appearance.Options.UseFont = True
        Me.CheckMachineRawPunchAll.Properties.Caption = "Machine Raw Punch All"
        Me.CheckMachineRawPunchAll.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckMachineRawPunchAll.Properties.RadioGroupIndex = 0
        Me.CheckMachineRawPunchAll.Size = New System.Drawing.Size(168, 19)
        Me.CheckMachineRawPunchAll.TabIndex = 8
        Me.CheckMachineRawPunchAll.TabStop = False
        '
        'PopupContainerControlBranch
        '
        Me.PopupContainerControlBranch.Controls.Add(Me.GridControlBranch)
        Me.PopupContainerControlBranch.Controls.Add(Me.CheckContinuousLateArrival)
        Me.PopupContainerControlBranch.Location = New System.Drawing.Point(717, 194)
        Me.PopupContainerControlBranch.Name = "PopupContainerControlBranch"
        Me.PopupContainerControlBranch.Size = New System.Drawing.Size(300, 300)
        Me.PopupContainerControlBranch.TabIndex = 24
        '
        'GridControlBranch
        '
        Me.GridControlBranch.DataSource = Me.TblbranchBindingSource
        Me.GridControlBranch.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControlBranch.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.GridControlBranch.Location = New System.Drawing.Point(0, 0)
        Me.GridControlBranch.MainView = Me.GridViewBranch
        Me.GridControlBranch.Name = "GridControlBranch"
        Me.GridControlBranch.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTimeEdit7, Me.RepositoryItemDateEdit2})
        Me.GridControlBranch.Size = New System.Drawing.Size(300, 300)
        Me.GridControlBranch.TabIndex = 6
        Me.GridControlBranch.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewBranch})
        '
        'TblbranchBindingSource
        '
        Me.TblbranchBindingSource.DataMember = "tblbranch"
        Me.TblbranchBindingSource.DataSource = Me.SSSDBDataSet
        '
        'GridViewBranch
        '
        Me.GridViewBranch.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colBRANCHCODE, Me.colBRANCHNAME})
        Me.GridViewBranch.GridControl = Me.GridControlBranch
        Me.GridViewBranch.Name = "GridViewBranch"
        Me.GridViewBranch.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewBranch.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewBranch.OptionsBehavior.Editable = False
        Me.GridViewBranch.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridViewBranch.OptionsSelection.MultiSelect = True
        Me.GridViewBranch.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        Me.GridViewBranch.OptionsView.ColumnAutoWidth = False
        Me.GridViewBranch.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colBRANCHCODE, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colBRANCHCODE
        '
        Me.colBRANCHCODE.Caption = "Location Code"
        Me.colBRANCHCODE.FieldName = "BRANCHCODE"
        Me.colBRANCHCODE.Name = "colBRANCHCODE"
        Me.colBRANCHCODE.Visible = True
        Me.colBRANCHCODE.VisibleIndex = 1
        Me.colBRANCHCODE.Width = 100
        '
        'colBRANCHNAME
        '
        Me.colBRANCHNAME.Caption = "Name"
        Me.colBRANCHNAME.FieldName = "BRANCHNAME"
        Me.colBRANCHNAME.Name = "colBRANCHNAME"
        Me.colBRANCHNAME.Visible = True
        Me.colBRANCHNAME.VisibleIndex = 2
        Me.colBRANCHNAME.Width = 120
        '
        'RepositoryItemTimeEdit7
        '
        Me.RepositoryItemTimeEdit7.AutoHeight = False
        Me.RepositoryItemTimeEdit7.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemTimeEdit7.Mask.EditMask = "HH:mm"
        Me.RepositoryItemTimeEdit7.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemTimeEdit7.Name = "RepositoryItemTimeEdit7"
        '
        'RepositoryItemDateEdit2
        '
        Me.RepositoryItemDateEdit2.AutoHeight = False
        Me.RepositoryItemDateEdit2.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit2.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemDateEdit2.Mask.EditMask = "HH:mm"
        Me.RepositoryItemDateEdit2.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemDateEdit2.Name = "RepositoryItemDateEdit2"
        '
        'CheckContinuousLateArrival
        '
        Me.CheckContinuousLateArrival.Location = New System.Drawing.Point(-78, 192)
        Me.CheckContinuousLateArrival.Name = "CheckContinuousLateArrival"
        Me.CheckContinuousLateArrival.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckContinuousLateArrival.Properties.Appearance.Options.UseFont = True
        Me.CheckContinuousLateArrival.Properties.Caption = "Continuous LateArrival"
        Me.CheckContinuousLateArrival.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckContinuousLateArrival.Properties.RadioGroupIndex = 0
        Me.CheckContinuousLateArrival.Size = New System.Drawing.Size(168, 19)
        Me.CheckContinuousLateArrival.TabIndex = 29
        Me.CheckContinuousLateArrival.TabStop = False
        Me.CheckContinuousLateArrival.Visible = False
        '
        'CheckEditVReport
        '
        Me.CheckEditVReport.Location = New System.Drawing.Point(699, 42)
        Me.CheckEditVReport.Name = "CheckEditVReport"
        Me.CheckEditVReport.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditVReport.Properties.Appearance.Options.UseFont = True
        Me.CheckEditVReport.Properties.Caption = "Visitor Temperature Report"
        Me.CheckEditVReport.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEditVReport.Properties.RadioGroupIndex = 0
        Me.CheckEditVReport.Size = New System.Drawing.Size(179, 19)
        Me.CheckEditVReport.TabIndex = 45
        Me.CheckEditVReport.TabStop = False
        '
        'CheckEditTemperature
        '
        Me.CheckEditTemperature.Location = New System.Drawing.Point(699, 17)
        Me.CheckEditTemperature.Name = "CheckEditTemperature"
        Me.CheckEditTemperature.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditTemperature.Properties.Appearance.Options.UseFont = True
        Me.CheckEditTemperature.Properties.Caption = "Temperature Report"
        Me.CheckEditTemperature.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEditTemperature.Properties.RadioGroupIndex = 0
        Me.CheckEditTemperature.Size = New System.Drawing.Size(168, 19)
        Me.CheckEditTemperature.TabIndex = 44
        Me.CheckEditTemperature.TabStop = False
        '
        'CheckMultiShift
        '
        Me.CheckMultiShift.Location = New System.Drawing.Point(471, 92)
        Me.CheckMultiShift.Name = "CheckMultiShift"
        Me.CheckMultiShift.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckMultiShift.Properties.Appearance.Options.UseFont = True
        Me.CheckMultiShift.Properties.Caption = "MultiShift Report"
        Me.CheckMultiShift.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckMultiShift.Properties.RadioGroupIndex = 0
        Me.CheckMultiShift.Size = New System.Drawing.Size(168, 19)
        Me.CheckMultiShift.TabIndex = 26
        Me.CheckMultiShift.TabStop = False
        '
        'CheckPunchImage
        '
        Me.CheckPunchImage.Location = New System.Drawing.Point(471, 67)
        Me.CheckPunchImage.Name = "CheckPunchImage"
        Me.CheckPunchImage.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckPunchImage.Properties.Appearance.Options.UseFont = True
        Me.CheckPunchImage.Properties.Caption = "Punch Image Report"
        Me.CheckPunchImage.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckPunchImage.Properties.RadioGroupIndex = 0
        Me.CheckPunchImage.Size = New System.Drawing.Size(168, 19)
        Me.CheckPunchImage.TabIndex = 20
        Me.CheckPunchImage.TabStop = False
        '
        'CheckPhotoReport
        '
        Me.CheckPhotoReport.Location = New System.Drawing.Point(471, 42)
        Me.CheckPhotoReport.Name = "CheckPhotoReport"
        Me.CheckPhotoReport.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckPhotoReport.Properties.Appearance.Options.UseFont = True
        Me.CheckPhotoReport.Properties.Caption = "Photo Report"
        Me.CheckPhotoReport.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckPhotoReport.Properties.RadioGroupIndex = 0
        Me.CheckPhotoReport.Size = New System.Drawing.Size(168, 19)
        Me.CheckPhotoReport.TabIndex = 19
        Me.CheckPhotoReport.TabStop = False
        '
        'LabelControl13
        '
        Me.LabelControl13.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl13.Appearance.Options.UseFont = True
        Me.LabelControl13.Location = New System.Drawing.Point(603, 167)
        Me.LabelControl13.Name = "LabelControl13"
        Me.LabelControl13.Size = New System.Drawing.Size(15, 14)
        Me.LabelControl13.TabIndex = 43
        Me.LabelControl13.Text = "To"
        Me.LabelControl13.Visible = False
        '
        'LabelControl12
        '
        Me.LabelControl12.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl12.Appearance.Options.UseFont = True
        Me.LabelControl12.Location = New System.Drawing.Point(480, 167)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(27, 14)
        Me.LabelControl12.TabIndex = 42
        Me.LabelControl12.Text = "From"
        Me.LabelControl12.Visible = False
        '
        'TextToTime
        '
        Me.TextToTime.EditValue = "23:59"
        Me.TextToTime.Location = New System.Drawing.Point(624, 166)
        Me.TextToTime.Name = "TextToTime"
        Me.TextToTime.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextToTime.Properties.Appearance.Options.UseFont = True
        Me.TextToTime.Properties.Mask.EditMask = "HH:mm"
        Me.TextToTime.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TextToTime.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextToTime.Properties.MaxLength = 5
        Me.TextToTime.Size = New System.Drawing.Size(49, 20)
        Me.TextToTime.TabIndex = 23
        Me.TextToTime.Visible = False
        '
        'TextFromTime
        '
        Me.TextFromTime.EditValue = "00:00"
        Me.TextFromTime.Location = New System.Drawing.Point(515, 166)
        Me.TextFromTime.Name = "TextFromTime"
        Me.TextFromTime.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextFromTime.Properties.Appearance.Options.UseFont = True
        Me.TextFromTime.Properties.Mask.EditMask = "HH:mm"
        Me.TextFromTime.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.TextFromTime.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextFromTime.Properties.MaxLength = 5
        Me.TextFromTime.Size = New System.Drawing.Size(50, 20)
        Me.TextFromTime.TabIndex = 22
        Me.TextFromTime.Visible = False
        '
        'CheckInOut
        '
        Me.CheckInOut.Location = New System.Drawing.Point(471, 17)
        Me.CheckInOut.Name = "CheckInOut"
        Me.CheckInOut.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckInOut.Properties.Appearance.Options.UseFont = True
        Me.CheckInOut.Properties.Caption = "In/Out"
        Me.CheckInOut.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckInOut.Properties.RadioGroupIndex = 0
        Me.CheckInOut.Size = New System.Drawing.Size(168, 19)
        Me.CheckInOut.TabIndex = 18
        Me.CheckInOut.TabStop = False
        '
        'PopupContainerEditDevice
        '
        Me.PopupContainerEditDevice.Location = New System.Drawing.Point(471, 191)
        Me.PopupContainerEditDevice.Name = "PopupContainerEditDevice"
        Me.PopupContainerEditDevice.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditDevice.Properties.Appearance.Options.UseFont = True
        Me.PopupContainerEditDevice.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditDevice.Properties.AppearanceDropDown.Options.UseFont = True
        Me.PopupContainerEditDevice.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditDevice.Properties.AppearanceFocused.Options.UseFont = True
        Me.PopupContainerEditDevice.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.PopupContainerEditDevice.Properties.PopupControl = Me.PopupContainerControlDevice
        Me.PopupContainerEditDevice.Size = New System.Drawing.Size(225, 20)
        Me.PopupContainerEditDevice.TabIndex = 24
        Me.PopupContainerEditDevice.ToolTip = "Leave blank if want for all Devices"
        '
        'PopupContainerControlDevice
        '
        Me.PopupContainerControlDevice.Controls.Add(Me.GridControlDevice)
        Me.PopupContainerControlDevice.Location = New System.Drawing.Point(741, 18)
        Me.PopupContainerControlDevice.Name = "PopupContainerControlDevice"
        Me.PopupContainerControlDevice.Size = New System.Drawing.Size(340, 175)
        Me.PopupContainerControlDevice.TabIndex = 21
        '
        'GridControlDevice
        '
        Me.GridControlDevice.DataSource = Me.TblMachineBindingSource
        Me.GridControlDevice.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControlDevice.Location = New System.Drawing.Point(0, 0)
        Me.GridControlDevice.MainView = Me.GridViewDevice
        Me.GridControlDevice.Name = "GridControlDevice"
        Me.GridControlDevice.Size = New System.Drawing.Size(340, 175)
        Me.GridControlDevice.TabIndex = 0
        Me.GridControlDevice.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridViewDevice})
        '
        'TblMachineBindingSource
        '
        Me.TblMachineBindingSource.DataMember = "tblMachine"
        Me.TblMachineBindingSource.DataSource = Me.SSSDBDataSet
        '
        'GridViewDevice
        '
        Me.GridViewDevice.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colID_NO, Me.colLOCATION, Me.colbranch, Me.colDeviceType, Me.colA_R, Me.GridColumn3, Me.colcommkey, Me.colIN_OUT})
        Me.GridViewDevice.GridControl = Me.GridControlDevice
        Me.GridViewDevice.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always
        Me.GridViewDevice.Name = "GridViewDevice"
        Me.GridViewDevice.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewDevice.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.[False]
        Me.GridViewDevice.OptionsBehavior.Editable = False
        Me.GridViewDevice.OptionsSelection.CheckBoxSelectorColumnWidth = 50
        Me.GridViewDevice.OptionsSelection.MultiSelect = True
        Me.GridViewDevice.OptionsSelection.MultiSelectMode = DevExpress.XtraGrid.Views.Grid.GridMultiSelectMode.CheckBoxRowSelect
        Me.GridViewDevice.OptionsView.ColumnAutoWidth = False
        '
        'colID_NO
        '
        Me.colID_NO.Caption = "Controller Id"
        Me.colID_NO.FieldName = "ID_NO"
        Me.colID_NO.Name = "colID_NO"
        Me.colID_NO.Visible = True
        Me.colID_NO.VisibleIndex = 1
        '
        'colLOCATION
        '
        Me.colLOCATION.Caption = "Device IP"
        Me.colLOCATION.FieldName = "LOCATION"
        Me.colLOCATION.Name = "colLOCATION"
        Me.colLOCATION.Visible = True
        Me.colLOCATION.VisibleIndex = 3
        Me.colLOCATION.Width = 120
        '
        'colbranch
        '
        Me.colbranch.Caption = "Location"
        Me.colbranch.FieldName = "branch"
        Me.colbranch.Name = "colbranch"
        Me.colbranch.Visible = True
        Me.colbranch.VisibleIndex = 2
        '
        'colDeviceType
        '
        Me.colDeviceType.Caption = "GridColumn1"
        Me.colDeviceType.FieldName = "DeviceType"
        Me.colDeviceType.Name = "colDeviceType"
        '
        'colA_R
        '
        Me.colA_R.FieldName = "A_R"
        Me.colA_R.Name = "colA_R"
        '
        'GridColumn3
        '
        Me.GridColumn3.FieldName = "Purpose"
        Me.GridColumn3.Name = "GridColumn3"
        '
        'colcommkey
        '
        Me.colcommkey.Caption = "GridColumn2"
        Me.colcommkey.FieldName = "commkey"
        Me.colcommkey.Name = "colcommkey"
        '
        'colIN_OUT
        '
        Me.colIN_OUT.Caption = "In/Out"
        Me.colIN_OUT.FieldName = "IN_OUT"
        Me.colIN_OUT.Name = "colIN_OUT"
        Me.colIN_OUT.Visible = True
        Me.colIN_OUT.VisibleIndex = 4
        '
        'CheckDeviceWise
        '
        Me.CheckDeviceWise.Location = New System.Drawing.Point(471, 142)
        Me.CheckDeviceWise.Name = "CheckDeviceWise"
        Me.CheckDeviceWise.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckDeviceWise.Properties.Appearance.Options.UseFont = True
        Me.CheckDeviceWise.Properties.Caption = "Device Wise Report"
        Me.CheckDeviceWise.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckDeviceWise.Properties.RadioGroupIndex = 0
        Me.CheckDeviceWise.Size = New System.Drawing.Size(168, 19)
        Me.CheckDeviceWise.TabIndex = 21
        Me.CheckDeviceWise.TabStop = False
        '
        'TextEdit2
        '
        Me.TextEdit2.Location = New System.Drawing.Point(641, 250)
        Me.TextEdit2.Name = "TextEdit2"
        Me.TextEdit2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEdit2.Properties.Appearance.Options.UseFont = True
        Me.TextEdit2.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEdit2.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEdit2.Properties.MaxLength = 3
        Me.TextEdit2.Size = New System.Drawing.Size(55, 20)
        Me.TextEdit2.TabIndex = 36
        Me.TextEdit2.Visible = False
        '
        'LabelControl11
        '
        Me.LabelControl11.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl11.Appearance.Options.UseFont = True
        Me.LabelControl11.Location = New System.Drawing.Point(471, 253)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(145, 14)
        Me.LabelControl11.TabIndex = 35
        Me.LabelControl11.Text = "No. of days for continuous"
        Me.LabelControl11.Visible = False
        '
        'TextEdit1
        '
        Me.TextEdit1.EditValue = "58"
        Me.TextEdit1.Location = New System.Drawing.Point(110, 242)
        Me.TextEdit1.Name = "TextEdit1"
        Me.TextEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.TextEdit1.Properties.Appearance.Options.UseFont = True
        Me.TextEdit1.Properties.Mask.EditMask = "[0-9]*"
        Me.TextEdit1.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.TextEdit1.Properties.MaxLength = 2
        Me.TextEdit1.Size = New System.Drawing.Size(55, 20)
        Me.TextEdit1.TabIndex = 34
        Me.TextEdit1.Visible = False
        '
        'LabelControl10
        '
        Me.LabelControl10.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl10.Appearance.Options.UseFont = True
        Me.LabelControl10.Location = New System.Drawing.Point(15, 245)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(80, 14)
        Me.LabelControl10.TabIndex = 33
        Me.LabelControl10.Text = "Lines Per Page"
        Me.LabelControl10.Visible = False
        '
        'CheckDeptSkip
        '
        Me.CheckDeptSkip.Location = New System.Drawing.Point(264, 243)
        Me.CheckDeptSkip.Name = "CheckDeptSkip"
        Me.CheckDeptSkip.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckDeptSkip.Properties.Appearance.Options.UseFont = True
        Me.CheckDeptSkip.Properties.Caption = "Department Wise Skip"
        Me.CheckDeptSkip.Size = New System.Drawing.Size(186, 19)
        Me.CheckDeptSkip.TabIndex = 32
        Me.CheckDeptSkip.Visible = False
        '
        'CheckContinuousAbsenteeism
        '
        Me.CheckContinuousAbsenteeism.Location = New System.Drawing.Point(126, 259)
        Me.CheckContinuousAbsenteeism.Name = "CheckContinuousAbsenteeism"
        Me.CheckContinuousAbsenteeism.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckContinuousAbsenteeism.Properties.Appearance.Options.UseFont = True
        Me.CheckContinuousAbsenteeism.Properties.Caption = "Continuous Absenteeism"
        Me.CheckContinuousAbsenteeism.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckContinuousAbsenteeism.Properties.RadioGroupIndex = 0
        Me.CheckContinuousAbsenteeism.Size = New System.Drawing.Size(168, 19)
        Me.CheckContinuousAbsenteeism.TabIndex = 31
        Me.CheckContinuousAbsenteeism.TabStop = False
        Me.CheckContinuousAbsenteeism.Visible = False
        '
        'CheckContinuousEarlyDeparture
        '
        Me.CheckContinuousEarlyDeparture.Location = New System.Drawing.Point(297, 259)
        Me.CheckContinuousEarlyDeparture.Name = "CheckContinuousEarlyDeparture"
        Me.CheckContinuousEarlyDeparture.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckContinuousEarlyDeparture.Properties.Appearance.Options.UseFont = True
        Me.CheckContinuousEarlyDeparture.Properties.Caption = "Continuous Early Departure"
        Me.CheckContinuousEarlyDeparture.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckContinuousEarlyDeparture.Properties.RadioGroupIndex = 0
        Me.CheckContinuousEarlyDeparture.Size = New System.Drawing.Size(168, 19)
        Me.CheckContinuousEarlyDeparture.TabIndex = 30
        Me.CheckContinuousEarlyDeparture.TabStop = False
        Me.CheckContinuousEarlyDeparture.Visible = False
        '
        'CheckCustomized
        '
        Me.CheckCustomized.Location = New System.Drawing.Point(471, 117)
        Me.CheckCustomized.Name = "CheckCustomized"
        Me.CheckCustomized.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckCustomized.Properties.Appearance.Options.UseFont = True
        Me.CheckCustomized.Properties.Caption = "Customized Report "
        Me.CheckCustomized.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckCustomized.Properties.RadioGroupIndex = 0
        Me.CheckCustomized.Size = New System.Drawing.Size(168, 19)
        Me.CheckCustomized.TabIndex = 25
        Me.CheckCustomized.TabStop = False
        '
        'CheckPresentReport
        '
        Me.CheckPresentReport.Location = New System.Drawing.Point(15, 217)
        Me.CheckPresentReport.Name = "CheckPresentReport"
        Me.CheckPresentReport.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckPresentReport.Properties.Appearance.Options.UseFont = True
        Me.CheckPresentReport.Properties.Caption = "Present Report"
        Me.CheckPresentReport.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckPresentReport.Properties.RadioGroupIndex = 0
        Me.CheckPresentReport.Size = New System.Drawing.Size(168, 19)
        Me.CheckPresentReport.TabIndex = 9
        Me.CheckPresentReport.TabStop = False
        '
        'CheckMissingAndReverificaton
        '
        Me.CheckMissingAndReverificaton.Location = New System.Drawing.Point(227, 192)
        Me.CheckMissingAndReverificaton.Name = "CheckMissingAndReverificaton"
        Me.CheckMissingAndReverificaton.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckMissingAndReverificaton.Properties.Appearance.Options.UseFont = True
        Me.CheckMissingAndReverificaton.Properties.Caption = "Missing And Reverificaton"
        Me.CheckMissingAndReverificaton.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckMissingAndReverificaton.Properties.RadioGroupIndex = 0
        Me.CheckMissingAndReverificaton.Size = New System.Drawing.Size(168, 19)
        Me.CheckMissingAndReverificaton.TabIndex = 17
        Me.CheckMissingAndReverificaton.TabStop = False
        '
        'CheckManualPunchAudit
        '
        Me.CheckManualPunchAudit.Location = New System.Drawing.Point(227, 217)
        Me.CheckManualPunchAudit.Name = "CheckManualPunchAudit"
        Me.CheckManualPunchAudit.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckManualPunchAudit.Properties.Appearance.Options.UseFont = True
        Me.CheckManualPunchAudit.Properties.Caption = "Manual Punch Audit"
        Me.CheckManualPunchAudit.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckManualPunchAudit.Properties.RadioGroupIndex = 0
        Me.CheckManualPunchAudit.Size = New System.Drawing.Size(168, 19)
        Me.CheckManualPunchAudit.TabIndex = 9
        Me.CheckManualPunchAudit.TabStop = False
        Me.CheckManualPunchAudit.Visible = False
        '
        'CheckOutWork
        '
        Me.CheckOutWork.Location = New System.Drawing.Point(227, 167)
        Me.CheckOutWork.Name = "CheckOutWork"
        Me.CheckOutWork.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckOutWork.Properties.Appearance.Options.UseFont = True
        Me.CheckOutWork.Properties.Caption = "Out Work"
        Me.CheckOutWork.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckOutWork.Properties.RadioGroupIndex = 0
        Me.CheckOutWork.Size = New System.Drawing.Size(168, 19)
        Me.CheckOutWork.TabIndex = 16
        Me.CheckOutWork.TabStop = False
        '
        'CheckMachineRawPunch
        '
        Me.CheckMachineRawPunch.Location = New System.Drawing.Point(15, 167)
        Me.CheckMachineRawPunch.Name = "CheckMachineRawPunch"
        Me.CheckMachineRawPunch.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckMachineRawPunch.Properties.Appearance.Options.UseFont = True
        Me.CheckMachineRawPunch.Properties.Caption = "Machine Raw Punch"
        Me.CheckMachineRawPunch.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckMachineRawPunch.Properties.RadioGroupIndex = 0
        Me.CheckMachineRawPunch.Size = New System.Drawing.Size(168, 19)
        Me.CheckMachineRawPunch.TabIndex = 7
        Me.CheckMachineRawPunch.TabStop = False
        '
        'CheckShiftChangeStatement
        '
        Me.CheckShiftChangeStatement.Location = New System.Drawing.Point(227, 142)
        Me.CheckShiftChangeStatement.Name = "CheckShiftChangeStatement"
        Me.CheckShiftChangeStatement.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckShiftChangeStatement.Properties.Appearance.Options.UseFont = True
        Me.CheckShiftChangeStatement.Properties.Caption = "Shift Change Statement"
        Me.CheckShiftChangeStatement.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckShiftChangeStatement.Properties.RadioGroupIndex = 0
        Me.CheckShiftChangeStatement.Size = New System.Drawing.Size(168, 19)
        Me.CheckShiftChangeStatement.TabIndex = 15
        Me.CheckShiftChangeStatement.TabStop = False
        '
        'CheckShiftWisePresence
        '
        Me.CheckShiftWisePresence.Location = New System.Drawing.Point(15, 142)
        Me.CheckShiftWisePresence.Name = "CheckShiftWisePresence"
        Me.CheckShiftWisePresence.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckShiftWisePresence.Properties.Appearance.Options.UseFont = True
        Me.CheckShiftWisePresence.Properties.Caption = "Shift Wise Presence"
        Me.CheckShiftWisePresence.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckShiftWisePresence.Properties.RadioGroupIndex = 0
        Me.CheckShiftWisePresence.Size = New System.Drawing.Size(168, 19)
        Me.CheckShiftWisePresence.TabIndex = 6
        Me.CheckShiftWisePresence.TabStop = False
        '
        'CheckOverTimeSummary
        '
        Me.CheckOverTimeSummary.Location = New System.Drawing.Point(227, 117)
        Me.CheckOverTimeSummary.Name = "CheckOverTimeSummary"
        Me.CheckOverTimeSummary.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckOverTimeSummary.Properties.Appearance.Options.UseFont = True
        Me.CheckOverTimeSummary.Properties.Caption = "Over Time Summary"
        Me.CheckOverTimeSummary.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckOverTimeSummary.Properties.RadioGroupIndex = 0
        Me.CheckOverTimeSummary.Size = New System.Drawing.Size(168, 19)
        Me.CheckOverTimeSummary.TabIndex = 14
        Me.CheckOverTimeSummary.TabStop = False
        '
        'CheckEarlyArrival
        '
        Me.CheckEarlyArrival.Location = New System.Drawing.Point(15, 117)
        Me.CheckEarlyArrival.Name = "CheckEarlyArrival"
        Me.CheckEarlyArrival.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEarlyArrival.Properties.Appearance.Options.UseFont = True
        Me.CheckEarlyArrival.Properties.Caption = "Early Arrival"
        Me.CheckEarlyArrival.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEarlyArrival.Properties.RadioGroupIndex = 0
        Me.CheckEarlyArrival.Size = New System.Drawing.Size(168, 19)
        Me.CheckEarlyArrival.TabIndex = 5
        Me.CheckEarlyArrival.TabStop = False
        '
        'CheckOverTime
        '
        Me.CheckOverTime.Location = New System.Drawing.Point(227, 92)
        Me.CheckOverTime.Name = "CheckOverTime"
        Me.CheckOverTime.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckOverTime.Properties.Appearance.Options.UseFont = True
        Me.CheckOverTime.Properties.Caption = "Over Time"
        Me.CheckOverTime.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckOverTime.Properties.RadioGroupIndex = 0
        Me.CheckOverTime.Size = New System.Drawing.Size(168, 19)
        Me.CheckOverTime.TabIndex = 13
        Me.CheckOverTime.TabStop = False
        '
        'CheckDepartmentSummary
        '
        Me.CheckDepartmentSummary.Location = New System.Drawing.Point(15, 92)
        Me.CheckDepartmentSummary.Name = "CheckDepartmentSummary"
        Me.CheckDepartmentSummary.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckDepartmentSummary.Properties.Appearance.Options.UseFont = True
        Me.CheckDepartmentSummary.Properties.Caption = "Department Summary"
        Me.CheckDepartmentSummary.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckDepartmentSummary.Properties.RadioGroupIndex = 0
        Me.CheckDepartmentSummary.Size = New System.Drawing.Size(168, 19)
        Me.CheckDepartmentSummary.TabIndex = 4
        Me.CheckDepartmentSummary.TabStop = False
        '
        'CheckTimeLoss
        '
        Me.CheckTimeLoss.Location = New System.Drawing.Point(227, 67)
        Me.CheckTimeLoss.Name = "CheckTimeLoss"
        Me.CheckTimeLoss.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckTimeLoss.Properties.Appearance.Options.UseFont = True
        Me.CheckTimeLoss.Properties.Caption = "Time Loss"
        Me.CheckTimeLoss.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckTimeLoss.Properties.RadioGroupIndex = 0
        Me.CheckTimeLoss.Size = New System.Drawing.Size(168, 19)
        Me.CheckTimeLoss.TabIndex = 12
        Me.CheckTimeLoss.TabStop = False
        '
        'CheckAttendance
        '
        Me.CheckAttendance.Location = New System.Drawing.Point(15, 67)
        Me.CheckAttendance.Name = "CheckAttendance"
        Me.CheckAttendance.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckAttendance.Properties.Appearance.Options.UseFont = True
        Me.CheckAttendance.Properties.Caption = "Attendance"
        Me.CheckAttendance.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckAttendance.Properties.RadioGroupIndex = 0
        Me.CheckAttendance.Size = New System.Drawing.Size(168, 19)
        Me.CheckAttendance.TabIndex = 3
        Me.CheckAttendance.TabStop = False
        '
        'CheckEarlyDeparture
        '
        Me.CheckEarlyDeparture.Location = New System.Drawing.Point(227, 42)
        Me.CheckEarlyDeparture.Name = "CheckEarlyDeparture"
        Me.CheckEarlyDeparture.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEarlyDeparture.Properties.Appearance.Options.UseFont = True
        Me.CheckEarlyDeparture.Properties.Caption = "Early Departure"
        Me.CheckEarlyDeparture.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEarlyDeparture.Properties.RadioGroupIndex = 0
        Me.CheckEarlyDeparture.Size = New System.Drawing.Size(168, 19)
        Me.CheckEarlyDeparture.TabIndex = 11
        Me.CheckEarlyDeparture.TabStop = False
        '
        'CheckAbsenteeism
        '
        Me.CheckAbsenteeism.Location = New System.Drawing.Point(15, 42)
        Me.CheckAbsenteeism.Name = "CheckAbsenteeism"
        Me.CheckAbsenteeism.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckAbsenteeism.Properties.Appearance.Options.UseFont = True
        Me.CheckAbsenteeism.Properties.Caption = "Absenteeism"
        Me.CheckAbsenteeism.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckAbsenteeism.Properties.RadioGroupIndex = 0
        Me.CheckAbsenteeism.Size = New System.Drawing.Size(168, 19)
        Me.CheckAbsenteeism.TabIndex = 2
        Me.CheckAbsenteeism.TabStop = False
        '
        'CheckDailyPerformance
        '
        Me.CheckDailyPerformance.Location = New System.Drawing.Point(227, 17)
        Me.CheckDailyPerformance.Name = "CheckDailyPerformance"
        Me.CheckDailyPerformance.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckDailyPerformance.Properties.Appearance.Options.UseFont = True
        Me.CheckDailyPerformance.Properties.Caption = "Daily Performance"
        Me.CheckDailyPerformance.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckDailyPerformance.Properties.RadioGroupIndex = 0
        Me.CheckDailyPerformance.Size = New System.Drawing.Size(168, 19)
        Me.CheckDailyPerformance.TabIndex = 10
        Me.CheckDailyPerformance.TabStop = False
        '
        'CheckLateArr
        '
        Me.CheckLateArr.EditValue = True
        Me.CheckLateArr.Location = New System.Drawing.Point(15, 17)
        Me.CheckLateArr.Name = "CheckLateArr"
        Me.CheckLateArr.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckLateArr.Properties.Appearance.Options.UseFont = True
        Me.CheckLateArr.Properties.Caption = "Late Arrival "
        Me.CheckLateArr.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckLateArr.Properties.RadioGroupIndex = 0
        Me.CheckLateArr.Size = New System.Drawing.Size(168, 19)
        Me.CheckLateArr.TabIndex = 1
        '
        'SidePanel1
        '
        Me.SidePanel1.Controls.Add(Me.ComboNepaliYearTo)
        Me.SidePanel1.Controls.Add(Me.SidePanelSelection)
        Me.SidePanel1.Controls.Add(Me.ComboNEpaliMonthTo)
        Me.SidePanel1.Controls.Add(Me.CheckPDF)
        Me.SidePanel1.Controls.Add(Me.ComboNepaliDateTo)
        Me.SidePanel1.Controls.Add(Me.CheckExcel)
        Me.SidePanel1.Controls.Add(Me.ComboNepaliYearFrm)
        Me.SidePanel1.Controls.Add(Me.CheckText)
        Me.SidePanel1.Controls.Add(Me.ComboNEpaliMonthFrm)
        Me.SidePanel1.Controls.Add(Me.SimpleButton1)
        Me.SidePanel1.Controls.Add(Me.ComboNepaliDateFrm)
        Me.SidePanel1.Controls.Add(Me.DateEdit2)
        Me.SidePanel1.Controls.Add(Me.LabelControl3)
        Me.SidePanel1.Controls.Add(Me.DateEdit1)
        Me.SidePanel1.Controls.Add(Me.LabelControl2)
        Me.SidePanel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.SidePanel1.Location = New System.Drawing.Point(0, 0)
        Me.SidePanel1.Name = "SidePanel1"
        Me.SidePanel1.Size = New System.Drawing.Size(1036, 202)
        Me.SidePanel1.TabIndex = 2
        Me.SidePanel1.Text = "SidePanel1"
        '
        'ComboNepaliYearTo
        '
        Me.ComboNepaliYearTo.Location = New System.Drawing.Point(497, 16)
        Me.ComboNepaliYearTo.Name = "ComboNepaliYearTo"
        Me.ComboNepaliYearTo.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliYearTo.Properties.Appearance.Options.UseFont = True
        Me.ComboNepaliYearTo.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliYearTo.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNepaliYearTo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNepaliYearTo.Properties.Items.AddRange(New Object() {"2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037", "2038", "2039", "2040", "2041", "2042", "2043", "2044", "2045", "2046", "2047", "2048", "2049", "2050", "2051", "2052", "2053", "2054", "2055", "2056", "2057", "2058", "2059", "2060", "2061", "2062", "2063", "2064", "2065", "2066", "2067", "2068", "2069", "2070", "2071", "2072", "2073", "2074", "2075", "2076", "2077", "2078", "2079", "2080", "2081", "2082", "2083", "2084", "2085", "2086", "2087", "2088", "2089"})
        Me.ComboNepaliYearTo.Size = New System.Drawing.Size(61, 20)
        Me.ComboNepaliYearTo.TabIndex = 33
        '
        'SidePanelSelection
        '
        Me.SidePanelSelection.Controls.Add(Me.PopupContainerControlDevice)
        Me.SidePanelSelection.Controls.Add(Me.LabelControl1)
        Me.SidePanelSelection.Controls.Add(Me.LabelControl9)
        Me.SidePanelSelection.Controls.Add(Me.PopupContainerEditEmp)
        Me.SidePanelSelection.Controls.Add(Me.PopupContainerEditGrade)
        Me.SidePanelSelection.Controls.Add(Me.CheckEdit1)
        Me.SidePanelSelection.Controls.Add(Me.LabelControl8)
        Me.SidePanelSelection.Controls.Add(Me.CheckEdit2)
        Me.SidePanelSelection.Controls.Add(Me.PopupContainerEditLocation)
        Me.SidePanelSelection.Controls.Add(Me.PopupContainerEditComp)
        Me.SidePanelSelection.Controls.Add(Me.LabelControl7)
        Me.SidePanelSelection.Controls.Add(Me.LabelControl4)
        Me.SidePanelSelection.Controls.Add(Me.PopupContainerEditShift)
        Me.SidePanelSelection.Controls.Add(Me.PopupContainerEditDept)
        Me.SidePanelSelection.Controls.Add(Me.LabelControl6)
        Me.SidePanelSelection.Controls.Add(Me.LabelControl5)
        Me.SidePanelSelection.Controls.Add(Me.PopupContainerEditCat)
        Me.SidePanelSelection.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.SidePanelSelection.Location = New System.Drawing.Point(0, 44)
        Me.SidePanelSelection.Name = "SidePanelSelection"
        Me.SidePanelSelection.Size = New System.Drawing.Size(1036, 157)
        Me.SidePanelSelection.TabIndex = 21
        Me.SidePanelSelection.Text = "SidePanel4"
        '
        'LabelControl1
        '
        Me.LabelControl1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl1.Appearance.Options.UseFont = True
        Me.LabelControl1.Location = New System.Drawing.Point(16, 19)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(91, 14)
        Me.LabelControl1.TabIndex = 5
        Me.LabelControl1.Text = "Select Employee"
        '
        'LabelControl9
        '
        Me.LabelControl9.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl9.Appearance.Options.UseFont = True
        Me.LabelControl9.Location = New System.Drawing.Point(366, 66)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(70, 14)
        Me.LabelControl9.TabIndex = 20
        Me.LabelControl9.Text = "Select Grade"
        '
        'PopupContainerEditEmp
        '
        Me.PopupContainerEditEmp.Location = New System.Drawing.Point(113, 16)
        Me.PopupContainerEditEmp.Name = "PopupContainerEditEmp"
        Me.PopupContainerEditEmp.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditEmp.Properties.Appearance.Options.UseFont = True
        Me.PopupContainerEditEmp.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditEmp.Properties.AppearanceDropDown.Options.UseFont = True
        Me.PopupContainerEditEmp.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditEmp.Properties.AppearanceFocused.Options.UseFont = True
        Me.PopupContainerEditEmp.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.PopupContainerEditEmp.Properties.PopupControl = Me.PopupContainerControlEmp
        Me.PopupContainerEditEmp.Size = New System.Drawing.Size(225, 20)
        Me.PopupContainerEditEmp.TabIndex = 6
        Me.PopupContainerEditEmp.ToolTip = "Leave blank if want for all Employees"
        '
        'PopupContainerEditGrade
        '
        Me.PopupContainerEditGrade.Location = New System.Drawing.Point(501, 63)
        Me.PopupContainerEditGrade.Name = "PopupContainerEditGrade"
        Me.PopupContainerEditGrade.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditGrade.Properties.Appearance.Options.UseFont = True
        Me.PopupContainerEditGrade.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditGrade.Properties.AppearanceDropDown.Options.UseFont = True
        Me.PopupContainerEditGrade.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditGrade.Properties.AppearanceFocused.Options.UseFont = True
        Me.PopupContainerEditGrade.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.PopupContainerEditGrade.Properties.PopupControl = Me.PopupContainerControlGrade
        Me.PopupContainerEditGrade.Size = New System.Drawing.Size(225, 20)
        Me.PopupContainerEditGrade.TabIndex = 19
        Me.PopupContainerEditGrade.ToolTip = "Leave blank if want for all Grades"
        '
        'CheckEdit1
        '
        Me.CheckEdit1.EditValue = True
        Me.CheckEdit1.Location = New System.Drawing.Point(19, 115)
        Me.CheckEdit1.Name = "CheckEdit1"
        Me.CheckEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEdit1.Properties.Appearance.Options.UseFont = True
        Me.CheckEdit1.Properties.Caption = "Active Employee"
        Me.CheckEdit1.Size = New System.Drawing.Size(155, 19)
        Me.CheckEdit1.TabIndex = 7
        '
        'LabelControl8
        '
        Me.LabelControl8.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl8.Appearance.Options.UseFont = True
        Me.LabelControl8.Location = New System.Drawing.Point(16, 92)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(84, 14)
        Me.LabelControl8.TabIndex = 18
        Me.LabelControl8.Text = "Select Location"
        '
        'CheckEdit2
        '
        Me.CheckEdit2.EditValue = True
        Me.CheckEdit2.Location = New System.Drawing.Point(247, 115)
        Me.CheckEdit2.Name = "CheckEdit2"
        Me.CheckEdit2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEdit2.Properties.Appearance.Options.UseFont = True
        Me.CheckEdit2.Properties.Caption = "Inactive Employee"
        Me.CheckEdit2.Size = New System.Drawing.Size(155, 19)
        Me.CheckEdit2.TabIndex = 8
        '
        'PopupContainerEditLocation
        '
        Me.PopupContainerEditLocation.Location = New System.Drawing.Point(113, 89)
        Me.PopupContainerEditLocation.Name = "PopupContainerEditLocation"
        Me.PopupContainerEditLocation.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditLocation.Properties.Appearance.Options.UseFont = True
        Me.PopupContainerEditLocation.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditLocation.Properties.AppearanceDropDown.Options.UseFont = True
        Me.PopupContainerEditLocation.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditLocation.Properties.AppearanceFocused.Options.UseFont = True
        Me.PopupContainerEditLocation.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.PopupContainerEditLocation.Properties.PopupControl = Me.PopupContainerControlBranch
        Me.PopupContainerEditLocation.Size = New System.Drawing.Size(225, 20)
        Me.PopupContainerEditLocation.TabIndex = 17
        Me.PopupContainerEditLocation.ToolTip = "Leave blank if want for all Locations"
        '
        'PopupContainerEditComp
        '
        Me.PopupContainerEditComp.Location = New System.Drawing.Point(113, 42)
        Me.PopupContainerEditComp.Name = "PopupContainerEditComp"
        Me.PopupContainerEditComp.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditComp.Properties.Appearance.Options.UseFont = True
        Me.PopupContainerEditComp.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditComp.Properties.AppearanceDropDown.Options.UseFont = True
        Me.PopupContainerEditComp.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditComp.Properties.AppearanceFocused.Options.UseFont = True
        Me.PopupContainerEditComp.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.PopupContainerEditComp.Properties.PopupControl = Me.PopupContainerControlComp
        Me.PopupContainerEditComp.Size = New System.Drawing.Size(225, 20)
        Me.PopupContainerEditComp.TabIndex = 9
        Me.PopupContainerEditComp.ToolTip = "Leave blank if want for all Companies"
        '
        'LabelControl7
        '
        Me.LabelControl7.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl7.Appearance.Options.UseFont = True
        Me.LabelControl7.Location = New System.Drawing.Point(366, 43)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(63, 14)
        Me.LabelControl7.TabIndex = 16
        Me.LabelControl7.Text = "Select Shift"
        '
        'LabelControl4
        '
        Me.LabelControl4.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl4.Appearance.Options.UseFont = True
        Me.LabelControl4.Location = New System.Drawing.Point(16, 44)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(88, 14)
        Me.LabelControl4.TabIndex = 10
        Me.LabelControl4.Text = "Select Company"
        '
        'PopupContainerEditShift
        '
        Me.PopupContainerEditShift.Location = New System.Drawing.Point(501, 40)
        Me.PopupContainerEditShift.Name = "PopupContainerEditShift"
        Me.PopupContainerEditShift.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditShift.Properties.Appearance.Options.UseFont = True
        Me.PopupContainerEditShift.Properties.AppearanceDisabled.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditShift.Properties.AppearanceDisabled.Options.UseFont = True
        Me.PopupContainerEditShift.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditShift.Properties.AppearanceDropDown.Options.UseFont = True
        Me.PopupContainerEditShift.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditShift.Properties.AppearanceFocused.Options.UseFont = True
        Me.PopupContainerEditShift.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.PopupContainerEditShift.Properties.PopupControl = Me.PopupContainerControlShift
        Me.PopupContainerEditShift.Size = New System.Drawing.Size(225, 20)
        Me.PopupContainerEditShift.TabIndex = 15
        Me.PopupContainerEditShift.ToolTip = "Leave blank if want for all Shifts"
        '
        'PopupContainerEditDept
        '
        Me.PopupContainerEditDept.Location = New System.Drawing.Point(501, 15)
        Me.PopupContainerEditDept.Name = "PopupContainerEditDept"
        Me.PopupContainerEditDept.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditDept.Properties.Appearance.Options.UseFont = True
        Me.PopupContainerEditDept.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditDept.Properties.AppearanceDropDown.Options.UseFont = True
        Me.PopupContainerEditDept.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditDept.Properties.AppearanceFocused.Options.UseFont = True
        Me.PopupContainerEditDept.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.PopupContainerEditDept.Properties.PopupControl = Me.PopupContainerControlDept
        Me.PopupContainerEditDept.Size = New System.Drawing.Size(225, 20)
        Me.PopupContainerEditDept.TabIndex = 11
        Me.PopupContainerEditDept.ToolTip = "Leave blank if want for all Departments"
        '
        'LabelControl6
        '
        Me.LabelControl6.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl6.Appearance.Options.UseFont = True
        Me.LabelControl6.Location = New System.Drawing.Point(16, 68)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(86, 14)
        Me.LabelControl6.TabIndex = 14
        Me.LabelControl6.Text = "Select Catagory"
        '
        'LabelControl5
        '
        Me.LabelControl5.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl5.Appearance.Options.UseFont = True
        Me.LabelControl5.Location = New System.Drawing.Point(366, 18)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(104, 14)
        Me.LabelControl5.TabIndex = 12
        Me.LabelControl5.Text = "Select Department"
        '
        'PopupContainerEditCat
        '
        Me.PopupContainerEditCat.Location = New System.Drawing.Point(113, 66)
        Me.PopupContainerEditCat.Name = "PopupContainerEditCat"
        Me.PopupContainerEditCat.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditCat.Properties.Appearance.Options.UseFont = True
        Me.PopupContainerEditCat.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditCat.Properties.AppearanceDropDown.Options.UseFont = True
        Me.PopupContainerEditCat.Properties.AppearanceFocused.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.PopupContainerEditCat.Properties.AppearanceFocused.Options.UseFont = True
        Me.PopupContainerEditCat.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.PopupContainerEditCat.Properties.PopupControl = Me.PopupContainerControlCat
        Me.PopupContainerEditCat.Size = New System.Drawing.Size(225, 20)
        Me.PopupContainerEditCat.TabIndex = 13
        Me.PopupContainerEditCat.ToolTip = "Leave blank if want for all Catagory"
        '
        'ComboNEpaliMonthTo
        '
        Me.ComboNEpaliMonthTo.Location = New System.Drawing.Point(411, 16)
        Me.ComboNEpaliMonthTo.Name = "ComboNEpaliMonthTo"
        Me.ComboNEpaliMonthTo.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNEpaliMonthTo.Properties.Appearance.Options.UseFont = True
        Me.ComboNEpaliMonthTo.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNEpaliMonthTo.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNEpaliMonthTo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNEpaliMonthTo.Properties.Items.AddRange(New Object() {"Baishakh", "Jestha", "Asar", "Shrawan", "Bhadau", "Aswin", "Kartik", "Mansir", "Poush", "Magh", "Falgun", "Chaitra"})
        Me.ComboNEpaliMonthTo.Size = New System.Drawing.Size(80, 20)
        Me.ComboNEpaliMonthTo.TabIndex = 32
        '
        'CheckPDF
        '
        Me.CheckPDF.Location = New System.Drawing.Point(857, 14)
        Me.CheckPDF.Name = "CheckPDF"
        Me.CheckPDF.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckPDF.Properties.Appearance.Options.UseFont = True
        Me.CheckPDF.Properties.Caption = "PDF"
        Me.CheckPDF.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckPDF.Properties.RadioGroupIndex = 0
        Me.CheckPDF.Size = New System.Drawing.Size(63, 19)
        Me.CheckPDF.TabIndex = 24
        Me.CheckPDF.TabStop = False
        '
        'ComboNepaliDateTo
        '
        Me.ComboNepaliDateTo.Location = New System.Drawing.Point(363, 16)
        Me.ComboNepaliDateTo.Name = "ComboNepaliDateTo"
        Me.ComboNepaliDateTo.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliDateTo.Properties.Appearance.Options.UseFont = True
        Me.ComboNepaliDateTo.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliDateTo.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNepaliDateTo.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNepaliDateTo.Properties.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32"})
        Me.ComboNepaliDateTo.Size = New System.Drawing.Size(42, 20)
        Me.ComboNepaliDateTo.TabIndex = 31
        '
        'CheckExcel
        '
        Me.CheckExcel.Location = New System.Drawing.Point(756, 13)
        Me.CheckExcel.Name = "CheckExcel"
        Me.CheckExcel.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckExcel.Properties.Appearance.Options.UseFont = True
        Me.CheckExcel.Properties.Caption = "View"
        Me.CheckExcel.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckExcel.Properties.RadioGroupIndex = 0
        Me.CheckExcel.Size = New System.Drawing.Size(63, 19)
        Me.CheckExcel.TabIndex = 23
        Me.CheckExcel.TabStop = False
        '
        'ComboNepaliYearFrm
        '
        Me.ComboNepaliYearFrm.Location = New System.Drawing.Point(228, 16)
        Me.ComboNepaliYearFrm.Name = "ComboNepaliYearFrm"
        Me.ComboNepaliYearFrm.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliYearFrm.Properties.Appearance.Options.UseFont = True
        Me.ComboNepaliYearFrm.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliYearFrm.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNepaliYearFrm.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNepaliYearFrm.Properties.Items.AddRange(New Object() {"2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037", "2038", "2039", "2040", "2041", "2042", "2043", "2044", "2045", "2046", "2047", "2048", "2049", "2050", "2051", "2052", "2053", "2054", "2055", "2056", "2057", "2058", "2059", "2060", "2061", "2062", "2063", "2064", "2065", "2066", "2067", "2068", "2069", "2070", "2071", "2072", "2073", "2074", "2075", "2076", "2077", "2078", "2079", "2080", "2081", "2082", "2083", "2084", "2085", "2086", "2087", "2088", "2089"})
        Me.ComboNepaliYearFrm.Size = New System.Drawing.Size(61, 20)
        Me.ComboNepaliYearFrm.TabIndex = 30
        '
        'CheckText
        '
        Me.CheckText.EditValue = True
        Me.CheckText.Location = New System.Drawing.Point(663, 14)
        Me.CheckText.Name = "CheckText"
        Me.CheckText.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckText.Properties.Appearance.Options.UseFont = True
        Me.CheckText.Properties.Caption = "Text"
        Me.CheckText.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckText.Properties.RadioGroupIndex = 0
        Me.CheckText.Size = New System.Drawing.Size(63, 19)
        Me.CheckText.TabIndex = 22
        '
        'ComboNEpaliMonthFrm
        '
        Me.ComboNEpaliMonthFrm.Location = New System.Drawing.Point(142, 16)
        Me.ComboNEpaliMonthFrm.Name = "ComboNEpaliMonthFrm"
        Me.ComboNEpaliMonthFrm.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNEpaliMonthFrm.Properties.Appearance.Options.UseFont = True
        Me.ComboNEpaliMonthFrm.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNEpaliMonthFrm.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNEpaliMonthFrm.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNEpaliMonthFrm.Properties.Items.AddRange(New Object() {"Baishakh", "Jestha", "Asar", "Shrawan", "Bhadau", "Aswin", "Kartik", "Mansir", "Poush", "Magh", "Falgun", "Chaitra"})
        Me.ComboNEpaliMonthFrm.Size = New System.Drawing.Size(80, 20)
        Me.ComboNEpaliMonthFrm.TabIndex = 29
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButton1.Appearance.Options.UseFont = True
        Me.SimpleButton1.Location = New System.Drawing.Point(572, 15)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton1.TabIndex = 4
        Me.SimpleButton1.Text = "Generate"
        '
        'ComboNepaliDateFrm
        '
        Me.ComboNepaliDateFrm.Location = New System.Drawing.Point(94, 16)
        Me.ComboNepaliDateFrm.Name = "ComboNepaliDateFrm"
        Me.ComboNepaliDateFrm.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliDateFrm.Properties.Appearance.Options.UseFont = True
        Me.ComboNepaliDateFrm.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboNepaliDateFrm.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboNepaliDateFrm.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboNepaliDateFrm.Properties.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32"})
        Me.ComboNepaliDateFrm.Size = New System.Drawing.Size(42, 20)
        Me.ComboNepaliDateFrm.TabIndex = 28
        '
        'DateEdit2
        '
        Me.DateEdit2.EditValue = Nothing
        Me.DateEdit2.Location = New System.Drawing.Point(366, 16)
        Me.DateEdit2.Name = "DateEdit2"
        Me.DateEdit2.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.DateEdit2.Properties.Appearance.Options.UseFont = True
        Me.DateEdit2.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit2.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit2.Size = New System.Drawing.Size(100, 20)
        Me.DateEdit2.TabIndex = 3
        Me.DateEdit2.Visible = False
        '
        'LabelControl3
        '
        Me.LabelControl3.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl3.Appearance.Options.UseFont = True
        Me.LabelControl3.Location = New System.Drawing.Point(302, 19)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(45, 14)
        Me.LabelControl3.TabIndex = 2
        Me.LabelControl3.Text = "To Date"
        Me.LabelControl3.Visible = False
        '
        'DateEdit1
        '
        Me.DateEdit1.EditValue = Nothing
        Me.DateEdit1.Location = New System.Drawing.Point(94, 16)
        Me.DateEdit1.Name = "DateEdit1"
        Me.DateEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.DateEdit1.Properties.Appearance.Options.UseFont = True
        Me.DateEdit1.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit1.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit1.Size = New System.Drawing.Size(100, 20)
        Me.DateEdit1.TabIndex = 1
        '
        'LabelControl2
        '
        Me.LabelControl2.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl2.Appearance.Options.UseFont = True
        Me.LabelControl2.Location = New System.Drawing.Point(9, 19)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(57, 14)
        Me.LabelControl2.TabIndex = 0
        Me.LabelControl2.Text = "From Date"
        '
        'MemoEdit1
        '
        Me.MemoEdit1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.MemoEdit1.EditValue = resources.GetString("MemoEdit1.EditValue")
        Me.MemoEdit1.Location = New System.Drawing.Point(0, 0)
        Me.MemoEdit1.Name = "MemoEdit1"
        Me.MemoEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.MemoEdit1.Properties.Appearance.Options.UseFont = True
        Me.MemoEdit1.Properties.MaxLength = 100000
        Me.MemoEdit1.Properties.ReadOnly = True
        Me.MemoEdit1.Size = New System.Drawing.Size(101, 568)
        Me.MemoEdit1.TabIndex = 3
        '
        'TblDepartmentTableAdapter
        '
        Me.TblDepartmentTableAdapter.ClearBeforeFill = True
        '
        'TblEmployee1TableAdapter1
        '
        Me.TblEmployee1TableAdapter1.ClearBeforeFill = True
        '
        'TblEmployeeTableAdapter
        '
        Me.TblEmployeeTableAdapter.ClearBeforeFill = True
        '
        'TblCompany1TableAdapter1
        '
        Me.TblCompany1TableAdapter1.ClearBeforeFill = True
        '
        'TblDepartment1TableAdapter1
        '
        Me.TblDepartment1TableAdapter1.ClearBeforeFill = True
        '
        'TblCatagoryTableAdapter
        '
        Me.TblCatagoryTableAdapter.ClearBeforeFill = True
        '
        'TblGradeTableAdapter
        '
        Me.TblGradeTableAdapter.ClearBeforeFill = True
        '
        'TblShiftMaster1TableAdapter1
        '
        Me.TblShiftMaster1TableAdapter1.ClearBeforeFill = True
        '
        'TblShiftMasterTableAdapter
        '
        Me.TblShiftMasterTableAdapter.ClearBeforeFill = True
        '
        'TblCatagory1TableAdapter1
        '
        Me.TblCatagory1TableAdapter1.ClearBeforeFill = True
        '
        'TblCompanyTableAdapter
        '
        Me.TblCompanyTableAdapter.ClearBeforeFill = True
        '
        'TblGrade1TableAdapter1
        '
        Me.TblGrade1TableAdapter1.ClearBeforeFill = True
        '
        'TblbranchTableAdapter
        '
        Me.TblbranchTableAdapter.ClearBeforeFill = True
        '
        'Tblbranch1TableAdapter1
        '
        Me.Tblbranch1TableAdapter1.ClearBeforeFill = True
        '
        'TblMachine1TableAdapter1
        '
        Me.TblMachine1TableAdapter1.ClearBeforeFill = True
        '
        'TblMachineTableAdapter1
        '
        Me.TblMachineTableAdapter1.ClearBeforeFill = True
        '
        'XtraReportsDaily
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.SplitContainerControl1)
        Me.LookAndFeel.SkinName = "iMaginary"
        Me.LookAndFeel.UseDefaultLookAndFeel = False
        Me.Name = "XtraReportsDaily"
        Me.Size = New System.Drawing.Size(1145, 568)
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerControl1.ResumeLayout(False)
        Me.SidePanel3.ResumeLayout(False)
        CType(Me.PopupContainerControlEmp, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PopupContainerControlEmp.ResumeLayout(False)
        CType(Me.GridControlEmp, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblEmployeeBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewEmp, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTimeEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerControlDept, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PopupContainerControlDept.ResumeLayout(False)
        CType(Me.GridControlDept, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblDepartmentBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewDept, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTimeEdit3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerControlComp, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PopupContainerControlComp.ResumeLayout(False)
        CType(Me.GridControlComp, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblCompanyBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewComp, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTimeEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SidePanel2.ResumeLayout(False)
        CType(Me.PopupContainerControlGrade, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PopupContainerControlGrade.ResumeLayout(False)
        CType(Me.GridControlGrade, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblGradeBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewGrade, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTimeEdit6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerControlShift, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PopupContainerControlShift.ResumeLayout(False)
        CType(Me.GridControlShift, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblShiftMasterBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewShift, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit1.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTimeEdit5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerControlCat, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PopupContainerControlCat.ResumeLayout(False)
        CType(Me.GridControlCat, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblCatagoryBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewCat, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTimeEdit4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PanelControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanelControl1.ResumeLayout(False)
        Me.PanelControl1.PerformLayout()
        CType(Me.CheckMachineRawPunchAll.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerControlBranch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PopupContainerControlBranch.ResumeLayout(False)
        CType(Me.GridControlBranch, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblbranchBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewBranch, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTimeEdit7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit2.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemDateEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckContinuousLateArrival.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditVReport.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditTemperature.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckMultiShift.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckPunchImage.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckPhotoReport.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextToTime.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextFromTime.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckInOut.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerEditDevice.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerControlDevice, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PopupContainerControlDevice.ResumeLayout(False)
        CType(Me.GridControlDevice, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblMachineBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridViewDevice, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckDeviceWise.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TextEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckDeptSkip.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckContinuousAbsenteeism.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckContinuousEarlyDeparture.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckCustomized.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckPresentReport.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckMissingAndReverificaton.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckManualPunchAudit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckOutWork.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckMachineRawPunch.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckShiftChangeStatement.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckShiftWisePresence.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckOverTimeSummary.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEarlyArrival.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckOverTime.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckDepartmentSummary.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckTimeLoss.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckAttendance.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEarlyDeparture.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckAbsenteeism.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckDailyPerformance.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckLateArr.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SidePanel1.ResumeLayout(False)
        Me.SidePanel1.PerformLayout()
        CType(Me.ComboNepaliYearTo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SidePanelSelection.ResumeLayout(False)
        Me.SidePanelSelection.PerformLayout()
        CType(Me.PopupContainerEditEmp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerEditGrade.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerEditLocation.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerEditComp.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerEditShift.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerEditDept.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PopupContainerEditCat.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboNEpaliMonthTo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckPDF.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboNepaliDateTo.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckExcel.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboNepaliYearFrm.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckText.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboNEpaliMonthFrm.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboNepaliDateFrm.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit2.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit1.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SplitContainerControl1 As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents SidePanel3 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents PopupContainerControlEmp As DevExpress.XtraEditors.PopupContainerControl
    Friend WithEvents GridControlEmp As DevExpress.XtraGrid.GridControl
    Friend WithEvents TblEmployeeBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SSSDBDataSet As iAS.SSSDBDataSet
    Friend WithEvents GridViewEmp As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colPAYCODE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colEMPNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemTimeEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit
    Friend WithEvents PopupContainerControlDept As DevExpress.XtraEditors.PopupContainerControl
    Friend WithEvents GridControlDept As DevExpress.XtraGrid.GridControl
    Friend WithEvents TblDepartmentBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents GridViewDept As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colDEPARTMENTCODE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDEPARTMENTNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemTimeEdit3 As DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit
    Friend WithEvents SimpleButton5 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents PopupContainerControlComp As DevExpress.XtraEditors.PopupContainerControl
    Friend WithEvents GridControlComp As DevExpress.XtraGrid.GridControl
    Friend WithEvents TblCompanyBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents GridViewComp As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colCOMPANYCODE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCOMPANYNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemTimeEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit
    Friend WithEvents SimpleButton4 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton3 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SidePanel2 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents PopupContainerControlGrade As DevExpress.XtraEditors.PopupContainerControl
    Friend WithEvents GridControlGrade As DevExpress.XtraGrid.GridControl
    Friend WithEvents TblGradeBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents GridViewGrade As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GridColumn1 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn2 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemTimeEdit6 As DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit
    Friend WithEvents PopupContainerControlShift As DevExpress.XtraEditors.PopupContainerControl
    Friend WithEvents GridControlShift As DevExpress.XtraGrid.GridControl
    Friend WithEvents TblShiftMasterBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents GridViewShift As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colSHIFT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSTARTTIME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemDateEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents colENDTIME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemTimeEdit5 As DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit
    Friend WithEvents PopupContainerControlCat As DevExpress.XtraEditors.PopupContainerControl
    Friend WithEvents GridControlCat As DevExpress.XtraGrid.GridControl
    Friend WithEvents TblCatagoryBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents GridViewCat As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colCAT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colCATAGORYNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemTimeEdit4 As DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit
    Friend WithEvents PanelControl1 As DevExpress.XtraEditors.PanelControl
    Friend WithEvents CheckLateArr As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents SidePanel1 As DevExpress.XtraEditors.SidePanel
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PopupContainerEditGrade As DevExpress.XtraEditors.PopupContainerEdit
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PopupContainerEditLocation As DevExpress.XtraEditors.PopupContainerEdit
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PopupContainerEditShift As DevExpress.XtraEditors.PopupContainerEdit
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PopupContainerEditCat As DevExpress.XtraEditors.PopupContainerEdit
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PopupContainerEditDept As DevExpress.XtraEditors.PopupContainerEdit
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents PopupContainerEditComp As DevExpress.XtraEditors.PopupContainerEdit
    Friend WithEvents CheckEdit2 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEdit1 As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents PopupContainerEditEmp As DevExpress.XtraEditors.PopupContainerEdit
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents DateEdit2 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents DateEdit1 As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents MemoEdit1 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents TblDepartmentTableAdapter As iAS.SSSDBDataSetTableAdapters.tblDepartmentTableAdapter
    Friend WithEvents TblEmployee1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.TblEmployee1TableAdapter
    Friend WithEvents TblEmployeeTableAdapter As iAS.SSSDBDataSetTableAdapters.TblEmployeeTableAdapter
    Friend WithEvents TblCompany1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.tblCompany1TableAdapter
    Friend WithEvents TblDepartment1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.tblDepartment1TableAdapter
    Friend WithEvents TblCatagoryTableAdapter As iAS.SSSDBDataSetTableAdapters.tblCatagoryTableAdapter
    Friend WithEvents TblGradeTableAdapter As iAS.SSSDBDataSetTableAdapters.tblGradeTableAdapter
    Friend WithEvents TblShiftMaster1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.tblShiftMaster1TableAdapter
    Friend WithEvents TblShiftMasterTableAdapter As iAS.SSSDBDataSetTableAdapters.tblShiftMasterTableAdapter
    Friend WithEvents TblCatagory1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.tblCatagory1TableAdapter
    Friend WithEvents TblCompanyTableAdapter As iAS.SSSDBDataSetTableAdapters.tblCompanyTableAdapter
    Friend WithEvents TblGrade1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.tblGrade1TableAdapter
    Friend WithEvents CheckCustomized As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckPresentReport As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckMissingAndReverificaton As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckManualPunchAudit As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckOutWork As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckMachineRawPunch As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckShiftChangeStatement As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckShiftWisePresence As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckOverTimeSummary As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEarlyArrival As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckOverTime As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckDepartmentSummary As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckTimeLoss As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckAttendance As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEarlyDeparture As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckAbsenteeism As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckDailyPerformance As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckContinuousAbsenteeism As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckContinuousEarlyDeparture As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckContinuousLateArrival As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents TextEdit1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents CheckDeptSkip As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents SidePanelSelection As DevExpress.XtraEditors.SidePanel
    Friend WithEvents TextEdit2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents CheckExcel As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckText As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents PopupContainerControlBranch As DevExpress.XtraEditors.PopupContainerControl
    Friend WithEvents GridControlBranch As DevExpress.XtraGrid.GridControl
    Friend WithEvents TblbranchBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents GridViewBranch As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colBRANCHCODE As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colBRANCHNAME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents RepositoryItemTimeEdit7 As DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit
    Friend WithEvents RepositoryItemDateEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemDateEdit
    Friend WithEvents TblbranchTableAdapter As iAS.SSSDBDataSetTableAdapters.tblbranchTableAdapter
    Friend WithEvents Tblbranch1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.tblbranch1TableAdapter
    Friend WithEvents CheckPDF As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents ComboNepaliYearTo As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboNEpaliMonthTo As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboNepaliDateTo As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboNepaliYearFrm As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboNEpaliMonthFrm As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboNepaliDateFrm As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents CheckDeviceWise As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents TblMachine1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.tblMachine1TableAdapter
    Friend WithEvents TblMachineTableAdapter1 As iAS.SSSDBDataSetTableAdapters.tblMachineTableAdapter
    Friend WithEvents TblMachineBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PopupContainerControlDevice As DevExpress.XtraEditors.PopupContainerControl
    Friend WithEvents GridControlDevice As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridViewDevice As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colID_NO As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLOCATION As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colbranch As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colDeviceType As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colA_R As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents GridColumn3 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colcommkey As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colIN_OUT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents PopupContainerEditDevice As DevExpress.XtraEditors.PopupContainerEdit
    Friend WithEvents CheckInOut As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents LabelControl13 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents TextToTime As DevExpress.XtraEditors.TextEdit
    Friend WithEvents TextFromTime As DevExpress.XtraEditors.TextEdit
    Friend WithEvents CheckPhotoReport As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckPunchImage As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckMultiShift As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditTemperature As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditVReport As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckMachineRawPunchAll As DevExpress.XtraEditors.CheckEdit
End Class
