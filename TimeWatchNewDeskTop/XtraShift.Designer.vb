﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraShift
    Inherits DevExpress.XtraEditors.XtraUserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(XtraShift))
        Me.GridControl1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.colSHIFT = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTextEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.colSTARTTIME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTimeEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit()
        Me.colENDTIME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSHIFTDURATION = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLUNCHTIME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLUNCHDURATION = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTextEdit3 = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.colLUNCHENDTIME = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colORDERINF12 = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colOTDEDUCTAFTER = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colOTSTARTAFTER = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colOTDEDUCTHRS = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLUNCHDEDUCTION = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colSHIFTPOSITION = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemComboBox1 = New DevExpress.XtraEditors.Repository.RepositoryItemComboBox()
        Me.colShiftEarly = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colShiftLate = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLastModifiedBy = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.colLastModifiedDate = New DevExpress.XtraGrid.Columns.GridColumn()
        Me.RepositoryItemTextEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemTextEdit()
        Me.RepositoryItemTimeSpanEdit1 = New DevExpress.XtraEditors.Repository.RepositoryItemTimeSpanEdit()
        Me.RepositoryItemTimeEdit2 = New DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit()
        Me.TblShiftMasterBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SSSDBDataSet = New iAS.SSSDBDataSet()
        Me.TblShiftMasterTableAdapter = New iAS.SSSDBDataSetTableAdapters.tblShiftMasterTableAdapter()
        Me.TblShiftMaster1TableAdapter1 = New iAS.SSSDBDataSetTableAdapters.tblShiftMaster1TableAdapter()
        Me.SplitContainerControl1 = New DevExpress.XtraEditors.SplitContainerControl()
        Me.MemoEdit1 = New DevExpress.XtraEditors.MemoEdit()
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTextEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTimeEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTextEdit3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemComboBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTextEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTimeSpanEdit1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.RepositoryItemTimeEdit2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TblShiftMasterBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainerControl1.SuspendLayout()
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GridControl1
        '
        Me.GridControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GridControl1.EmbeddedNavigator.Buttons.Append.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.CancelEdit.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.Edit.Visible = False
        Me.GridControl1.EmbeddedNavigator.Buttons.EndEdit.Visible = False
        Me.GridControl1.Location = New System.Drawing.Point(0, 0)
        Me.GridControl1.MainView = Me.GridView1
        Me.GridControl1.Name = "GridControl1"
        Me.GridControl1.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.RepositoryItemTextEdit1, Me.RepositoryItemTimeEdit1, Me.RepositoryItemTextEdit2, Me.RepositoryItemTimeSpanEdit1, Me.RepositoryItemTimeEdit2, Me.RepositoryItemComboBox1, Me.RepositoryItemTextEdit3})
        Me.GridControl1.Size = New System.Drawing.Size(1036, 568)
        Me.GridControl1.TabIndex = 0
        Me.GridControl1.UseEmbeddedNavigator = True
        Me.GridControl1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.Appearance.TopNewRow.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.GridView1.Appearance.TopNewRow.ForeColor = System.Drawing.Color.Blue
        Me.GridView1.Appearance.TopNewRow.Options.UseFont = True
        Me.GridView1.Appearance.TopNewRow.Options.UseForeColor = True
        Me.GridView1.Columns.AddRange(New DevExpress.XtraGrid.Columns.GridColumn() {Me.colSHIFT, Me.colSTARTTIME, Me.colENDTIME, Me.colSHIFTDURATION, Me.colLUNCHTIME, Me.colLUNCHDURATION, Me.colLUNCHENDTIME, Me.colORDERINF12, Me.colOTDEDUCTAFTER, Me.colOTSTARTAFTER, Me.colOTDEDUCTHRS, Me.colLUNCHDEDUCTION, Me.colSHIFTPOSITION, Me.colShiftEarly, Me.colShiftLate, Me.colLastModifiedBy, Me.colLastModifiedDate})
        Me.GridView1.GridControl = Me.GridControl1
        Me.GridView1.Name = "GridView1"
        Me.GridView1.NewItemRowText = "Click here to add new Shift"
        Me.GridView1.OptionsBehavior.EditingMode = DevExpress.XtraGrid.Views.Grid.GridEditingMode.EditForm
        Me.GridView1.OptionsEditForm.EditFormColumnCount = 1
        Me.GridView1.OptionsEditForm.PopupEditFormWidth = 400
        Me.GridView1.OptionsView.ColumnAutoWidth = False
        Me.GridView1.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top
        Me.GridView1.OptionsView.ShowFooter = True
        Me.GridView1.SortInfo.AddRange(New DevExpress.XtraGrid.Columns.GridColumnSortInfo() {New DevExpress.XtraGrid.Columns.GridColumnSortInfo(Me.colSHIFT, DevExpress.Data.ColumnSortOrder.Ascending)})
        '
        'colSHIFT
        '
        Me.colSHIFT.ColumnEdit = Me.RepositoryItemTextEdit1
        Me.colSHIFT.FieldName = "SHIFT"
        Me.colSHIFT.Name = "colSHIFT"
        Me.colSHIFT.OptionsEditForm.Caption = "Shift Code: *"
        Me.colSHIFT.Visible = True
        Me.colSHIFT.VisibleIndex = 0
        Me.colSHIFT.Width = 80
        '
        'RepositoryItemTextEdit1
        '
        Me.RepositoryItemTextEdit1.AutoHeight = False
        Me.RepositoryItemTextEdit1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.RepositoryItemTextEdit1.MaxLength = 3
        Me.RepositoryItemTextEdit1.Name = "RepositoryItemTextEdit1"
        '
        'colSTARTTIME
        '
        Me.colSTARTTIME.Caption = "Start Time"
        Me.colSTARTTIME.ColumnEdit = Me.RepositoryItemTimeEdit1
        Me.colSTARTTIME.DisplayFormat.FormatString = "HH:mm"
        Me.colSTARTTIME.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.colSTARTTIME.FieldName = "STARTTIME"
        Me.colSTARTTIME.Name = "colSTARTTIME"
        Me.colSTARTTIME.OptionsEditForm.Caption = "Shift Start Time: *"
        Me.colSTARTTIME.Visible = True
        Me.colSTARTTIME.VisibleIndex = 1
        Me.colSTARTTIME.Width = 100
        '
        'RepositoryItemTimeEdit1
        '
        Me.RepositoryItemTimeEdit1.AutoHeight = False
        Me.RepositoryItemTimeEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemTimeEdit1.EditFormat.FormatString = "HH:mm"
        Me.RepositoryItemTimeEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.RepositoryItemTimeEdit1.Mask.EditMask = "HH:mm"
        Me.RepositoryItemTimeEdit1.MaxLength = 5
        Me.RepositoryItemTimeEdit1.Name = "RepositoryItemTimeEdit1"
        '
        'colENDTIME
        '
        Me.colENDTIME.Caption = "End Time"
        Me.colENDTIME.ColumnEdit = Me.RepositoryItemTimeEdit1
        Me.colENDTIME.DisplayFormat.FormatString = "HH:mm"
        Me.colENDTIME.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.colENDTIME.FieldName = "ENDTIME"
        Me.colENDTIME.Name = "colENDTIME"
        Me.colENDTIME.OptionsEditForm.Caption = "Shift End Time: *"
        Me.colENDTIME.Visible = True
        Me.colENDTIME.VisibleIndex = 2
        Me.colENDTIME.Width = 100
        '
        'colSHIFTDURATION
        '
        Me.colSHIFTDURATION.FieldName = "SHIFTDURATION"
        Me.colSHIFTDURATION.Name = "colSHIFTDURATION"
        Me.colSHIFTDURATION.OptionsColumn.ReadOnly = True
        Me.colSHIFTDURATION.Visible = True
        Me.colSHIFTDURATION.VisibleIndex = 3
        Me.colSHIFTDURATION.Width = 100
        '
        'colLUNCHTIME
        '
        Me.colLUNCHTIME.Caption = "Lunch Start Time"
        Me.colLUNCHTIME.ColumnEdit = Me.RepositoryItemTimeEdit1
        Me.colLUNCHTIME.FieldName = "LUNCHTIME"
        Me.colLUNCHTIME.Name = "colLUNCHTIME"
        Me.colLUNCHTIME.Visible = True
        Me.colLUNCHTIME.VisibleIndex = 4
        Me.colLUNCHTIME.Width = 110
        '
        'colLUNCHDURATION
        '
        Me.colLUNCHDURATION.Caption = "Lunch Duration"
        Me.colLUNCHDURATION.ColumnEdit = Me.RepositoryItemTextEdit3
        Me.colLUNCHDURATION.FieldName = "LUNCHDURATION"
        Me.colLUNCHDURATION.Name = "colLUNCHDURATION"
        Me.colLUNCHDURATION.Visible = True
        Me.colLUNCHDURATION.VisibleIndex = 5
        Me.colLUNCHDURATION.Width = 100
        '
        'RepositoryItemTextEdit3
        '
        Me.RepositoryItemTextEdit3.AutoHeight = False
        Me.RepositoryItemTextEdit3.Mask.EditMask = "([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]"
        Me.RepositoryItemTextEdit3.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.RegEx
        Me.RepositoryItemTextEdit3.Name = "RepositoryItemTextEdit3"
        '
        'colLUNCHENDTIME
        '
        Me.colLUNCHENDTIME.Caption = "Lunch End Time"
        Me.colLUNCHENDTIME.ColumnEdit = Me.RepositoryItemTimeEdit1
        Me.colLUNCHENDTIME.FieldName = "LUNCHENDTIME"
        Me.colLUNCHENDTIME.Name = "colLUNCHENDTIME"
        Me.colLUNCHENDTIME.OptionsColumn.ReadOnly = True
        Me.colLUNCHENDTIME.Visible = True
        Me.colLUNCHENDTIME.VisibleIndex = 6
        Me.colLUNCHENDTIME.Width = 100
        '
        'colORDERINF12
        '
        Me.colORDERINF12.FieldName = "ORDERINF12"
        Me.colORDERINF12.Name = "colORDERINF12"
        Me.colORDERINF12.Width = 101
        '
        'colOTDEDUCTAFTER
        '
        Me.colOTDEDUCTAFTER.ColumnEdit = Me.RepositoryItemTextEdit3
        Me.colOTDEDUCTAFTER.FieldName = "OTDEDUCTAFTER"
        Me.colOTDEDUCTAFTER.Name = "colOTDEDUCTAFTER"
        Me.colOTDEDUCTAFTER.Visible = True
        Me.colOTDEDUCTAFTER.VisibleIndex = 7
        Me.colOTDEDUCTAFTER.Width = 120
        '
        'colOTSTARTAFTER
        '
        Me.colOTSTARTAFTER.ColumnEdit = Me.RepositoryItemTextEdit3
        Me.colOTSTARTAFTER.FieldName = "OTSTARTAFTER"
        Me.colOTSTARTAFTER.Name = "colOTSTARTAFTER"
        Me.colOTSTARTAFTER.Visible = True
        Me.colOTSTARTAFTER.VisibleIndex = 8
        Me.colOTSTARTAFTER.Width = 120
        '
        'colOTDEDUCTHRS
        '
        Me.colOTDEDUCTHRS.ColumnEdit = Me.RepositoryItemTextEdit3
        Me.colOTDEDUCTHRS.FieldName = "OTDEDUCTHRS"
        Me.colOTDEDUCTHRS.Name = "colOTDEDUCTHRS"
        Me.colOTDEDUCTHRS.Visible = True
        Me.colOTDEDUCTHRS.VisibleIndex = 9
        Me.colOTDEDUCTHRS.Width = 120
        '
        'colLUNCHDEDUCTION
        '
        Me.colLUNCHDEDUCTION.ColumnEdit = Me.RepositoryItemTextEdit3
        Me.colLUNCHDEDUCTION.FieldName = "LUNCHDEDUCTION"
        Me.colLUNCHDEDUCTION.Name = "colLUNCHDEDUCTION"
        Me.colLUNCHDEDUCTION.Visible = True
        Me.colLUNCHDEDUCTION.VisibleIndex = 10
        Me.colLUNCHDEDUCTION.Width = 130
        '
        'colSHIFTPOSITION
        '
        Me.colSHIFTPOSITION.ColumnEdit = Me.RepositoryItemComboBox1
        Me.colSHIFTPOSITION.FieldName = "SHIFTPOSITION"
        Me.colSHIFTPOSITION.Name = "colSHIFTPOSITION"
        Me.colSHIFTPOSITION.Visible = True
        Me.colSHIFTPOSITION.VisibleIndex = 11
        Me.colSHIFTPOSITION.Width = 100
        '
        'RepositoryItemComboBox1
        '
        Me.RepositoryItemComboBox1.AutoHeight = False
        Me.RepositoryItemComboBox1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemComboBox1.Items.AddRange(New Object() {"DAY", "NIGHT", "HALFDAY"})
        Me.RepositoryItemComboBox1.Name = "RepositoryItemComboBox1"
        '
        'colShiftEarly
        '
        Me.colShiftEarly.Caption = "Shift Begin Duration"
        Me.colShiftEarly.ColumnEdit = Me.RepositoryItemTextEdit3
        Me.colShiftEarly.FieldName = "ShiftEarly"
        Me.colShiftEarly.Name = "colShiftEarly"
        Me.colShiftEarly.UnboundType = DevExpress.Data.UnboundColumnType.[String]
        Me.colShiftEarly.Visible = True
        Me.colShiftEarly.VisibleIndex = 12
        Me.colShiftEarly.Width = 100
        '
        'colShiftLate
        '
        Me.colShiftLate.Caption = "Shift End Duration"
        Me.colShiftLate.ColumnEdit = Me.RepositoryItemTextEdit3
        Me.colShiftLate.FieldName = "ShiftLate"
        Me.colShiftLate.Name = "colShiftLate"
        Me.colShiftLate.Visible = True
        Me.colShiftLate.VisibleIndex = 13
        Me.colShiftLate.Width = 100
        '
        'colLastModifiedBy
        '
        Me.colLastModifiedBy.FieldName = "LastModifiedBy"
        Me.colLastModifiedBy.Name = "colLastModifiedBy"
        '
        'colLastModifiedDate
        '
        Me.colLastModifiedDate.FieldName = "LastModifiedDate"
        Me.colLastModifiedDate.Name = "colLastModifiedDate"
        '
        'RepositoryItemTextEdit2
        '
        Me.RepositoryItemTextEdit2.AutoHeight = False
        Me.RepositoryItemTextEdit2.DisplayFormat.FormatString = "HH:mm"
        Me.RepositoryItemTextEdit2.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.RepositoryItemTextEdit2.EditFormat.FormatString = "HH:mm"
        Me.RepositoryItemTextEdit2.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.RepositoryItemTextEdit2.Mask.EditMask = "HH:mm"
        Me.RepositoryItemTextEdit2.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.DateTime
        Me.RepositoryItemTextEdit2.Name = "RepositoryItemTextEdit2"
        '
        'RepositoryItemTimeSpanEdit1
        '
        Me.RepositoryItemTimeSpanEdit1.AutoHeight = False
        Me.RepositoryItemTimeSpanEdit1.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemTimeSpanEdit1.DisplayFormat.FormatString = "HH:mm"
        Me.RepositoryItemTimeSpanEdit1.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.RepositoryItemTimeSpanEdit1.EditFormat.FormatString = "HH:mm"
        Me.RepositoryItemTimeSpanEdit1.EditFormat.FormatType = DevExpress.Utils.FormatType.Custom
        Me.RepositoryItemTimeSpanEdit1.Mask.EditMask = "HH:mm"
        Me.RepositoryItemTimeSpanEdit1.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.Custom
        Me.RepositoryItemTimeSpanEdit1.Name = "RepositoryItemTimeSpanEdit1"
        Me.RepositoryItemTimeSpanEdit1.TimeEditStyle = DevExpress.XtraEditors.Repository.TimeEditStyle.SpinButtons
        '
        'RepositoryItemTimeEdit2
        '
        Me.RepositoryItemTimeEdit2.AutoHeight = False
        Me.RepositoryItemTimeEdit2.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.RepositoryItemTimeEdit2.Mask.EditMask = "HH:mm"
        Me.RepositoryItemTimeEdit2.Mask.UseMaskAsDisplayFormat = True
        Me.RepositoryItemTimeEdit2.MaxLength = 5
        Me.RepositoryItemTimeEdit2.Name = "RepositoryItemTimeEdit2"
        '
        'TblShiftMasterBindingSource
        '
        Me.TblShiftMasterBindingSource.DataMember = "tblShiftMaster"
        Me.TblShiftMasterBindingSource.DataSource = Me.SSSDBDataSet
        '
        'SSSDBDataSet
        '
        Me.SSSDBDataSet.DataSetName = "SSSDBDataSet"
        Me.SSSDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'TblShiftMasterTableAdapter
        '
        Me.TblShiftMasterTableAdapter.ClearBeforeFill = True
        '
        'TblShiftMaster1TableAdapter1
        '
        Me.TblShiftMaster1TableAdapter1.ClearBeforeFill = True
        '
        'SplitContainerControl1
        '
        Me.SplitContainerControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainerControl1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainerControl1.Name = "SplitContainerControl1"
        Me.SplitContainerControl1.Panel1.Controls.Add(Me.GridControl1)
        Me.SplitContainerControl1.Panel1.Text = "Panel1"
        Me.SplitContainerControl1.Panel2.Controls.Add(Me.MemoEdit1)
        Me.SplitContainerControl1.Panel2.Text = "Panel2"
        Me.SplitContainerControl1.Size = New System.Drawing.Size(1145, 568)
        Me.SplitContainerControl1.SplitterPosition = 1036
        Me.SplitContainerControl1.TabIndex = 1
        Me.SplitContainerControl1.Text = "SplitContainerControl1"
        '
        'MemoEdit1
        '
        Me.MemoEdit1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.MemoEdit1.EditValue = resources.GetString("MemoEdit1.EditValue")
        Me.MemoEdit1.Location = New System.Drawing.Point(0, 0)
        Me.MemoEdit1.Name = "MemoEdit1"
        Me.MemoEdit1.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.MemoEdit1.Properties.Appearance.Options.UseFont = True
        Me.MemoEdit1.Properties.MaxLength = 100000
        Me.MemoEdit1.Properties.ReadOnly = True
        Me.MemoEdit1.Size = New System.Drawing.Size(101, 568)
        Me.MemoEdit1.TabIndex = 1
        '
        'XtraShift
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.SplitContainerControl1)
        Me.LookAndFeel.SkinName = "iMaginary"
        Me.LookAndFeel.UseDefaultLookAndFeel = False
        Me.Name = "XtraShift"
        Me.Size = New System.Drawing.Size(1145, 568)
        CType(Me.GridControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTextEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTimeEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTextEdit3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemComboBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTextEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTimeSpanEdit1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.RepositoryItemTimeEdit2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TblShiftMasterBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SSSDBDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SplitContainerControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainerControl1.ResumeLayout(False)
        CType(Me.MemoEdit1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GridControl1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents TblShiftMasterBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents SSSDBDataSet As iAS.SSSDBDataSet
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents colSHIFT As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSTARTTIME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colENDTIME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLUNCHTIME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLUNCHDURATION As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLUNCHENDTIME As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colORDERINF12 As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colOTSTARTAFTER As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colOTDEDUCTHRS As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLUNCHDEDUCTION As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSHIFTPOSITION As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colSHIFTDURATION As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colOTDEDUCTAFTER As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLastModifiedBy As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colLastModifiedDate As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents TblShiftMasterTableAdapter As iAS.SSSDBDataSetTableAdapters.tblShiftMasterTableAdapter
    Friend WithEvents TblShiftMaster1TableAdapter1 As iAS.SSSDBDataSetTableAdapters.tblShiftMaster1TableAdapter
    Friend WithEvents RepositoryItemTextEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents RepositoryItemTimeEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit
    Friend WithEvents RepositoryItemTextEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents SplitContainerControl1 As DevExpress.XtraEditors.SplitContainerControl
    Friend WithEvents MemoEdit1 As DevExpress.XtraEditors.MemoEdit
    Friend WithEvents RepositoryItemTimeSpanEdit1 As DevExpress.XtraEditors.Repository.RepositoryItemTimeSpanEdit
    Friend WithEvents RepositoryItemTimeEdit2 As DevExpress.XtraEditors.Repository.RepositoryItemTimeEdit
    Friend WithEvents RepositoryItemComboBox1 As DevExpress.XtraEditors.Repository.RepositoryItemComboBox
    Friend WithEvents RepositoryItemTextEdit3 As DevExpress.XtraEditors.Repository.RepositoryItemTextEdit
    Friend WithEvents colShiftEarly As DevExpress.XtraGrid.Columns.GridColumn
    Friend WithEvents colShiftLate As DevExpress.XtraGrid.Columns.GridColumn

End Class
