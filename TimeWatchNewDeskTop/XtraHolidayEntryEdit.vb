﻿Imports System.Data.OleDb
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.Data.SqlClient
Imports System.IO
Imports System.Resources
Imports System.Globalization
Imports DevExpress.XtraGrid.Views.Grid
Imports System.Text

Public Class XtraHolidayEntryEdit
    Dim ulf As UserLookAndFeel
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    Public Sub New()
        InitializeComponent()
        'for xtramessage box
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True

        If Common.servername = "Access" Then
            'Me.Holiday1TableAdapter1.Fill(Me.SSSDBDataSet.Holiday1)
            'GridControl1.DataSource = SSSDBDataSet.Holiday1
        Else
            TblCompanyTableAdapter.Connection.ConnectionString = Common.ConnectionString '"Data Source=DESKTOP-PG6NDV8;Initial Catalog=SSSDB;Integrated Security=True"
            TblDepartmentTableAdapter.Connection.ConnectionString = Common.ConnectionString
            TblbranchTableAdapter.Connection.ConnectionString = Common.ConnectionString
            'Me.HolidayTableAdapter.Fill(Me.SSSDBDataSet.Holiday)
            'GridControl1.DataSource = SSSDBDataSet.Holiday
        End If
        Common.SetGridFont(GridView1, New Font("Tahoma", 9))
        Common.SetGridFont(GridView2, New Font("Tahoma", 9))
        Common.SetGridFont(GridView3, New Font("Tahoma", 9))
    End Sub
    Private Sub XtraHolidayEntryEdit_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        If Common.servername = "Access" Then
            'Me.TblCompany1TableAdapter1.Fill(Me.SSSDBDataSet.tblCompany1)
            Me.TblDepartment1TableAdapter1.Fill(Me.SSSDBDataSet.tblDepartment1)
            'Me.Tblbranch1TableAdapter1.Fill(Me.SSSDBDataSet.tblbranch1)
            'GridControl1.DataSource = SSSDBDataSet.tblCompany1
            GridControl2.DataSource = SSSDBDataSet.tblDepartment1
            'GridControl3.DataSource = SSSDBDataSet.tblbranch1
        Else
            'Me.TblCompanyTableAdapter.Fill(Me.SSSDBDataSet.tblCompany)
            Me.TblDepartmentTableAdapter.Fill(Me.SSSDBDataSet.tblDepartment)
            'Me.TblbranchTableAdapter.Fill(Me.SSSDBDataSet.tblbranch)
            'GridControl1.DataSource = SSSDBDataSet.tblCompany
            GridControl2.DataSource = SSSDBDataSet.tblDepartment
            'GridControl3.DataSource = SSSDBDataSet.tblbranch
        End If

        GridControl3.DataSource = Common.LocationNonAdmin
        GridControl1.DataSource = Common.CompanyNonAdmin

        SetDefaultValue()
    End Sub
    'Private Sub PopupContainerEdit1_QueryPopUp(sender As System.Object, e As System.ComponentModel.CancelEventArgs) Handles PopupContainerEdit1.QueryPopUp
    '    Dim val As Object = PopupContainerEdit1.EditValue
    '    If (val Is Nothing) Then
    '        GridView1.ClearSelection()
    '    Else
    '        'Dim texts() As String = val.ToString.Split(Microsoft.VisualBasic.ChrW(44))
    '        Dim texts() As String = val.ToString.Split(",")
    '        For Each text As String In texts
    '            If text.Trim.Length = 1 Then
    '                text = text.Trim & "  "
    '            ElseIf text.Trim.Length = 2 Then
    '                text = text.Trim & " "
    '            End If
    '            'MsgBox(text & "  " & text.Length & " " & GridView1.LocateByValue("SHIFT", text))
    '            Dim rowHandle As Integer = GridView1.LocateByValue("COMPANYCODE", text)
    '            GridView1.SelectRow(rowHandle)
    '        Next
    '    End If
    'End Sub
    'Private Sub PopupContainerEdit2_QueryPopUp(sender As System.Object, e As System.ComponentModel.CancelEventArgs) Handles PopupContainerEdit2.QueryPopUp
    '    Dim val As Object = PopupContainerEdit1.EditValue
    '    If (val Is Nothing) Then
    '        GridView2.ClearSelection()
    '    Else
    '        'Dim texts() As String = val.ToString.Split(Microsoft.VisualBasic.ChrW(44))
    '        Dim texts() As String = val.ToString.Split(",")
    '        For Each text As String In texts
    '            If text.Trim.Length = 1 Then
    '                text = text.Trim & "  "
    '            ElseIf text.Trim.Length = 2 Then
    '                text = text.Trim & " "
    '            End If
    '            'MsgBox(text & "  " & text.Length & " " & GridView1.LocateByValue("SHIFT", text))
    '            Dim rowHandle As Integer = GridView2.LocateByValue("DEPARTMENTCODE", text)
    '            GridView2.SelectRow(rowHandle)
    '        Next
    '    End If
    'End Sub

    Private Sub PopupContainerEdit1_QueryResultValue(sender As System.Object, e As DevExpress.XtraEditors.Controls.QueryResultValueEventArgs) Handles PopupContainerEdit1.QueryResultValue
        Dim selectedRows() As Integer = GridView1.GetSelectedRows
        Dim sb As StringBuilder = New StringBuilder
        For Each selectionRow As Integer In selectedRows
            Dim a As System.Data.DataRowView = GridView1.GetRow(selectionRow)
            If (sb.ToString.Length > 0) Then
                sb.Append(", ")
            End If
            sb.Append(a.Item("COMPANYCODE"))
        Next
        e.Value = sb.ToString
    End Sub
    Private Sub PopupContainerEdit2_QueryResultValue(sender As System.Object, e As DevExpress.XtraEditors.Controls.QueryResultValueEventArgs) Handles PopupContainerEdit2.QueryResultValue
        Dim selectedRows() As Integer = GridView2.GetSelectedRows
        Dim sb As StringBuilder = New StringBuilder
        For Each selectionRow As Integer In selectedRows
            Dim a As System.Data.DataRowView = GridView2.GetRow(selectionRow)
            If (sb.ToString.Length > 0) Then
                sb.Append(", ")
            End If
            sb.Append(a.Item("DEPARTMENTCODE"))
        Next
        e.Value = sb.ToString
    End Sub
    Private Sub PopupContainerEdit3_QueryResultValue(sender As System.Object, e As DevExpress.XtraEditors.Controls.QueryResultValueEventArgs) Handles PopupContainerEdit3.QueryResultValue
        Dim selectedRows() As Integer = GridView3.GetSelectedRows
        Dim sb As StringBuilder = New StringBuilder
        For Each selectionRow As Integer In selectedRows
            Dim a As System.Data.DataRowView = GridView3.GetRow(selectionRow)
            If (sb.ToString.Length > 0) Then
                sb.Append(", ")
            End If
            sb.Append(a.Item("BRANCHCODE"))
        Next
        e.Value = sb.ToString
    End Sub
    Private Sub SetDefaultValue()
        DateEdit1.Text = ""
        DateEdit2.Text = ""
        TextEdit1.Text = ""
        TextEdit2.Text = ""
        PopupContainerEdit1.EditValue = ""
        PopupContainerEdit2.EditValue = ""
        PopupContainerEdit3.EditValue = ""
        'PopupContainerEdit1.Properties.PopupControl = PopupContainerControl1
        If Common.IsNepali = "Y" Then
            DateEdit1.Visible = False
            DateEdit2.Visible = False
            ComboNepaliDate.Visible = True
            ComboNepaliDate.EditValue = ""
            ComboNEpaliMonth.Visible = True
            ComboNEpaliMonth.EditValue = ""
            ComboNepaliYear.Visible = True
            ComboNepaliYear.EditValue = ""

            ComboNepaliDateAd.Visible = True
            ComboNepaliDateAd.EditValue = ""
            ComboNEpaliMonthAd.Visible = True
            ComboNEpaliMonthAd.EditValue = ""
            ComboNepaliYearAd.Visible = True
            ComboNepaliYearAd.EditValue = ""
        Else
            DateEdit1.Visible = True
            DateEdit2.Visible = True
            ComboNepaliDate.Visible = False
            ComboNEpaliMonth.Visible = False
            ComboNepaliYear.Visible = False
           
            ComboNepaliDateAd.Visible = False
            ComboNEpaliMonthAd.Visible = False
            ComboNepaliYearAd.Visible = False
        End If
    End Sub
    Private Sub CheckEdit1_CheckedChanged(sender As System.Object, e As System.EventArgs)
        'If CheckEdit1.Checked = True Then
        '    LabelControl5.Text = "Select Company"
        '    PopupContainerEdit1.Properties.PopupControl = PopupContainerControl1
        'Else
        '    LabelControl5.Text = "Select Department"
        '    PopupContainerEdit1.Properties.PopupControl = PopupContainerControl2
        'End If
    End Sub
    Private Sub SimpleButton1_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton1.Click
        If Common.IsNepali = "Y" Then
            Dim DC As New DateConverter()
            If ComboNepaliYear.EditValue.ToString.Trim <> "" And ComboNEpaliMonth.EditValue.ToString.Trim <> "" And ComboNepaliDate.EditValue.ToString.Trim <> "" Then
                Try
                    'DateEdit1.DateTime = DC.ToAD(New Date(ComboNepaliYear.EditValue, ComboNEpaliMonth.SelectedIndex + 1, ComboNepaliDate.EditValue))
                    DateEdit1.DateTime = DC.ToAD(ComboNepaliYear.EditValue & "-" & ComboNEpaliMonth.SelectedIndex + 1 & "-" & ComboNepaliDate.EditValue)
                Catch ex As Exception
                    XtraMessageBox.Show(ulf, "<size=10>Invalid Holiday Date</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    ComboNepaliDate.Select()
                    Exit Sub
                End Try
            End If
            If ComboNepaliYearAd.EditValue.ToString.Trim <> "" And ComboNEpaliMonthAd.EditValue.ToString.Trim <> "" And ComboNepaliDateAd.EditValue.ToString.Trim <> "" Then
                Try
                    'DateEdit2.DateTime = DC.ToAD(New Date(ComboNepaliYearAd.EditValue, ComboNEpaliMonthAd.SelectedIndex + 1, ComboNepaliDateAd.EditValue))
                    DateEdit2.DateTime = DC.ToAD(ComboNepaliYearAd.EditValue & "-" & ComboNEpaliMonthAd.SelectedIndex + 1 & "-" & ComboNepaliDateAd.EditValue)
                Catch ex As Exception
                    XtraMessageBox.Show(ulf, "<size=10>Invalid Adjustment Date</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    ComboNepaliDate.Select()
                    Exit Sub
                End Try
            End If
        End If
        If DateEdit1.Text = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Holiday Date cannot be empty</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            DateEdit1.Select()
            Exit Sub
        ElseIf TextEdit1.Text.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Holiday Reason cannot be empty</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            TextEdit1.Select()
            Exit Sub
        End If
        Dim HDate As DateTime = DateEdit1.DateTime
        Dim ADJUSTMENTHOLIDAY As DateTime = DateEdit2.DateTime
        Dim HOLIDAY As String = TextEdit1.Text.Trim
        Dim OT_FACTOR As Double = 0
        If TextEdit2.Text.Trim <> "" Then
            OT_FACTOR = TextEdit2.Text.Trim
        End If

        If PopupContainerEdit1.EditValue = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Please select Company</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            PopupContainerEdit1.Select()
            Exit Sub
        End If
        InsertHoliday()

        'Dim COMPANYCODE() As String = PopupContainerEdit1.EditValue.ToString.Split(",")
        'Dim DEPARTMENTCODE() As String = PopupContainerEdit2.EditValue.ToString.Split(",")
        'Dim Location() As String = PopupContainerEdit3.EditValue.ToString.Split(",")
        'If PopupContainerEdit1.EditValue = "" Then
        '    XtraMessageBox.Show(ulf, "<size=10>Please select Company</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        '    PopupContainerEdit1.Select()
        '    Exit Sub
        'End If



        'If PopupContainerEdit2.EditValue = "" Then
        '    If Common.servername = "Access" Then
        '        If Common.con1.State <> ConnectionState.Open Then
        '            Common.con1.Open()
        '        End If
        '    Else
        '        If Common.con.State <> ConnectionState.Open Then
        '            Common.con.Open()
        '        End If
        '    End If
        '    For i As Integer = 0 To COMPANYCODE.Length - 1
        '        Dim adap As SqlDataAdapter
        '        Dim adapA As OleDbDataAdapter
        '        Dim ds As DataSet = New DataSet
        '        Dim getDeptQuery As String = "select DEPARTMENTCODE from tblDepartment"
        '        If Common.servername = "Access" Then
        '            adapA = New OleDbDataAdapter(getDeptQuery, Common.con1)
        '            adapA.Fill(ds)
        '        Else
        '            adap = New SqlDataAdapter(getDeptQuery, Common.con)
        '            adap.Fill(ds)
        '        End If
        '        Dim sSql As String

        '        For j As Integer = 0 To ds.Tables(0).Rows.Count - 1
        '            'duplicate check
        '            Dim adapCheck As SqlDataAdapter
        '            Dim adapCheckA As OleDbDataAdapter
        '            Dim dsCheck As DataSet = New DataSet
        '            Dim sSqlCheck As String = "select HDate from Holiday where HDate = '" & HDate.ToString("yyyy-MM-dd 00:00:00") & "' and (COMPANYCODE = '" & COMPANYCODE(i).Trim & "'  and DEPARTMENTCODE = '" & ds.Tables(0).Rows(j).Item("DEPARTMENTCODE").ToString.Trim & "')"
        '            If Common.servername = "Access" Then
        '                sSqlCheck = "select HDate from Holiday where FORMAT(HDate, 'yyyy-MM-dd') = '" & HDate & "' and (COMPANYCODE = '" & COMPANYCODE(i).Trim & "'  and DEPARTMENTCODE = '" & ds.Tables(0).Rows(j).Item("DEPARTMENTCODE").ToString.Trim & "')"
        '                adapCheckA = New OleDbDataAdapter(sSqlCheck, Common.con1)
        '                adapCheckA.Fill(dsCheck)
        '            Else
        '                adapCheck = New SqlDataAdapter(sSqlCheck, Common.con)
        '                adapCheck.Fill(dsCheck)
        '            End If
        '            If dsCheck.Tables(0).Rows.Count > 0 Then
        '                Continue For
        '            End If
        '            'end duplicate check

        '            If DateEdit2.Text = "" Then
        '                If Common.servername = "Access" Then
        '                    sSql = "insert INTO Holiday (HDate, HOLIDAY, OT_FACTOR, COMPANYCODE, DEPARTMENTCODE, LastModifiedBy, LastModifiedDate) VALUES ('" & HDate & "', '" & HOLIDAY & "','" & OT_FACTOR & "','" & COMPANYCODE(i).Trim & "','" & ds.Tables(0).Rows(j).Item("DEPARTMENTCODE").ToString.Trim & "','admin', '" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "')"
        '                Else
        '                    sSql = "insert INTO Holiday (HDate, HOLIDAY, OT_FACTOR, COMPANYCODE, DEPARTMENTCODE, LastModifiedBy, LastModifiedDate) VALUES ('" & HDate.ToString("yyyy-MM-dd 00:00:00") & "', '" & HOLIDAY & "','" & OT_FACTOR & "','" & COMPANYCODE(i).Trim & "','" & ds.Tables(0).Rows(j).Item("DEPARTMENTCODE").ToString.Trim & "','admin', '" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "')"
        '                End If
        '            Else
        '                If Common.servername = "Access" Then
        '                    sSql = "insert INTO Holiday (HDate, HOLIDAY, ADJUSTMENTHOLIDAY, OT_FACTOR, COMPANYCODE, DEPARTMENTCODE, LastModifiedBy, LastModifiedDate) VALUES ('" & HDate & "', '" & HOLIDAY & "','" & ADJUSTMENTHOLIDAY & "','" & OT_FACTOR & "','" & COMPANYCODE(i).Trim & "','" & ds.Tables(0).Rows(j).Item("DEPARTMENTCODE").ToString.Trim & "','admin', '" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "')"
        '                Else
        '                    sSql = "insert INTO Holiday (HDate, HOLIDAY, ADJUSTMENTHOLIDAY, OT_FACTOR, COMPANYCODE, DEPARTMENTCODE, LastModifiedBy, LastModifiedDate) VALUES ('" & HDate.ToString("yyyy-MM-dd 00:00:00") & "', '" & HOLIDAY & "','" & ADJUSTMENTHOLIDAY.ToString("yyyy-MM-dd 00:00:00") & "','" & OT_FACTOR & "','" & COMPANYCODE(i).Trim & "','" & ds.Tables(0).Rows(j).Item("DEPARTMENTCODE").ToString.Trim & "','admin', '" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "')"
        '                End If
        '            End If
        '            Try
        '                If Common.servername = "Access" Then
        '                    cmd1 = New OleDbCommand(sSql, Common.con1)
        '                    cmd1.ExecuteNonQuery()
        '                Else
        '                    cmd = New SqlCommand(sSql, Common.con)
        '                    cmd.ExecuteNonQuery()
        '                End If
        '            Catch ex As Exception
        '                If ex.Message.ToString = "The changes you requested to the table were not successful because they would create duplicate values in the index, primary key, or relationship.  Change the data in the field or fields that contain duplicate data, remove the index, or redefine the index to permit duplicate entries and try again." Then
        '                    Continue For
        '                End If
        '            End Try
        '        Next
        '    Next
        '    If Common.servername = "Access" Then
        '        If Common.con1.State <> ConnectionState.Closed Then
        '            Common.con1.Close()
        '        End If
        '    Else
        '        If Common.con.State <> ConnectionState.Closed Then
        '            Common.con.Close()
        '        End If
        '    End If
        'ElseIf PopupContainerEdit1.EditValue = "" Then
        '    Dim sSql As String
        '    Dim adap As SqlDataAdapter
        '    Dim adapA As OleDbDataAdapter
        '    Dim ds As DataSet = New DataSet
        '    sSql = "select COMPANYCODE from tblCompany"
        '    If Common.servername = "Access" Then
        '        adapA = New OleDbDataAdapter(sSql, Common.con1)
        '        adapA.Fill(ds)
        '    Else
        '        adap = New SqlDataAdapter(sSql, Common.con)
        '        adap.Fill(ds)
        '    End If
        '    If Common.servername = "Access" Then
        '        If Common.con1.State <> ConnectionState.Open Then
        '            Common.con1.Open()
        '        End If
        '    Else
        '        If Common.con.State <> ConnectionState.Open Then
        '            Common.con.Open()
        '        End If
        '    End If
        '    For x As Integer = 0 To ds.Tables(0).Rows.Count - 1
        '        Dim dept() As String = PopupContainerEdit2.EditValue.ToString.Split(",")
        '        For i As Integer = 0 To dept.Length - 1
        '            'duplicate check
        '            Dim adapCheck As SqlDataAdapter
        '            Dim adapCheckA As OleDbDataAdapter
        '            Dim dsCheck As DataSet = New DataSet
        '            Dim sSqlCheck As String = "select HDate from Holiday where HDate = '" & HDate.ToString("yyyy-MM-dd 00:00:00") & "' and (COMPANYCODE = '" & ds.Tables(0).Rows(x).Item("COMPANYCODE").Trim & "'  and DEPARTMENTCODE = '" & dept(i).ToString.Trim & "')"
        '            If Common.servername = "Access" Then
        '                sSqlCheck = "select HDate from Holiday where FORMAT(HDate,'yyyy-MM-dd') = '" & HDate & "' and (COMPANYCODE = '" & ds.Tables(0).Rows(x).Item("COMPANYCODE").Trim & "'  and DEPARTMENTCODE = '" & dept(i).ToString.Trim & "')"
        '                adapCheckA = New OleDbDataAdapter(sSqlCheck, Common.con1)
        '                adapCheckA.Fill(dsCheck)
        '            Else
        '                adapCheck = New SqlDataAdapter(sSqlCheck, Common.con)
        '                adapCheck.Fill(dsCheck)
        '            End If
        '            If dsCheck.Tables(0).Rows.Count > 0 Then
        '                Continue For
        '            End If
        '            'end duplicate check

        '            If DateEdit2.Text = "" Then
        '                If Common.servername = "Access" Then
        '                    sSql = "insert INTO Holiday (HDate, HOLIDAY, OT_FACTOR, COMPANYCODE, DEPARTMENTCODE, LastModifiedBy, LastModifiedDate) VALUES ('" & HDate & "', '" & HOLIDAY & "','" & OT_FACTOR & "','" & ds.Tables(0).Rows(x).Item("COMPANYCODE").Trim & "','" & dept(i).ToString.Trim & "','admin', '" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "')"
        '                Else
        '                    sSql = "insert INTO Holiday (HDate, HOLIDAY, OT_FACTOR, COMPANYCODE, DEPARTMENTCODE, LastModifiedBy, LastModifiedDate) VALUES ('" & HDate.ToString("yyyy-MM-dd 00:00:00") & "', '" & HOLIDAY & "','" & OT_FACTOR & "','" & ds.Tables(0).Rows(x).Item("COMPANYCODE").Trim & "','" & dept(i).ToString.Trim & "','admin', '" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "')"
        '                End If
        '            Else
        '                If Common.servername = "Access" Then
        '                    sSql = "insert INTO Holiday (HDate, HOLIDAY, ADJUSTMENTHOLIDAY, OT_FACTOR, COMPANYCODE, DEPARTMENTCODE, LastModifiedBy, LastModifiedDate) VALUES ('" & HDate & "', '" & HOLIDAY & "','" & ADJUSTMENTHOLIDAY & "','" & OT_FACTOR & "','" & ds.Tables(0).Rows(x).Item("COMPANYCODE").Trim & "','" & dept(i) & "','admin', '" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "')"
        '                Else
        '                    sSql = "insert INTO Holiday (HDate, HOLIDAY, ADJUSTMENTHOLIDAY, OT_FACTOR, COMPANYCODE, DEPARTMENTCODE, LastModifiedBy, LastModifiedDate) VALUES ('" & HDate.ToString("yyyy-MM-dd 00:00:00") & "', '" & HOLIDAY & "','" & ADJUSTMENTHOLIDAY.ToString("yyyy-MM-dd 00:00:00") & "','" & OT_FACTOR & "','" & ds.Tables(0).Rows(x).Item("COMPANYCODE").Trim & "','" & dept(i) & "','admin', '" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "')"
        '                End If
        '            End If
        '            Try
        '                If Common.servername = "Access" Then
        '                    cmd1 = New OleDbCommand(sSql, Common.con1)
        '                    cmd1.ExecuteNonQuery()
        '                Else
        '                    cmd = New SqlCommand(sSql, Common.con)
        '                    cmd.ExecuteNonQuery()
        '                End If
        '            Catch ex As Exception
        '                If ex.Message.ToString = "The changes you requested to the table were not successful because they would create duplicate values in the index, primary key, or relationship.  Change the data in the field or fields that contain duplicate data, remove the index, or redefine the index to permit duplicate entries and try again." Then
        '                    Continue For
        '                End If
        '            End Try
        '        Next
        '    Next
        '    If Common.servername = "Access" Then
        '        If Common.con1.State <> ConnectionState.Closed Then
        '            Common.con1.Close()
        '        End If
        '    Else
        '        If Common.con.State <> ConnectionState.Closed Then
        '            Common.con.Close()
        '        End If
        '    End If
        'Else
        '    Dim sSql As String
        '    If Common.servername = "Access" Then
        '        If Common.con1.State <> ConnectionState.Open Then
        '            Common.con1.Open()
        '        End If
        '    Else
        '        If Common.con.State <> ConnectionState.Open Then
        '            Common.con.Open()
        '        End If
        '    End If
        '    For i As Integer = 0 To COMPANYCODE.Length - 1
        '        For j As Integer = 0 To DEPARTMENTCODE.Length - 1
        '            'duplicate check
        '            Dim adapCheck As SqlDataAdapter
        '            Dim adapCheckA As OleDbDataAdapter
        '            Dim dsCheck As DataSet = New DataSet
        '            Dim sSqlCheck As String = "select HDate from Holiday where HDate = '" & HDate.ToString("yyyy-MM-dd 00:00:00") & "' and (COMPANYCODE = '" & COMPANYCODE(i).Trim & "'  and DEPARTMENTCODE = '" & DEPARTMENTCODE(j).Trim & "')"
        '            If Common.servername = "Access" Then
        '                sSqlCheck = "select HDate from Holiday where FORMAT(HDate,'yyyy-MM-dd') = '" & HDate & "' and (COMPANYCODE = '" & COMPANYCODE(i).Trim & "'  and DEPARTMENTCODE = '" & DEPARTMENTCODE(j).Trim & "')"
        '                adapCheckA = New OleDbDataAdapter(sSqlCheck, Common.con1)
        '                adapCheckA.Fill(dsCheck)
        '            Else
        '                adapCheck = New SqlDataAdapter(sSqlCheck, Common.con)
        '                adapCheck.Fill(dsCheck)
        '            End If
        '            If dsCheck.Tables(0).Rows.Count > 0 Then
        '                Continue For
        '            End If
        '            'end duplicate check

        '            If DateEdit2.Text = "" Then
        '                If Common.servername = "Access" Then
        '                    sSql = "insert INTO Holiday (HDate, HOLIDAY, OT_FACTOR, COMPANYCODE, DEPARTMENTCODE, LastModifiedBy, LastModifiedDate) VALUES ('" & HDate & "', '" & HOLIDAY & "','" & OT_FACTOR & "','" & COMPANYCODE(i).Trim & "','" & DEPARTMENTCODE(j).Trim & "','admin', '" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "')"
        '                Else
        '                    sSql = "insert INTO Holiday (HDate, HOLIDAY, OT_FACTOR, COMPANYCODE, DEPARTMENTCODE, LastModifiedBy, LastModifiedDate) VALUES ('" & HDate.ToString("yyyy-MM-dd 00:00:00") & "', '" & HOLIDAY & "','" & OT_FACTOR & "','" & COMPANYCODE(i).Trim & "','" & DEPARTMENTCODE(j).Trim & "','admin', '" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "')"
        '                End If
        '            Else
        '                If Common.servername = "Access" Then
        '                    sSql = "insert INTO Holiday (HDate, HOLIDAY, ADJUSTMENTHOLIDAY, OT_FACTOR, COMPANYCODE, DEPARTMENTCODE, LastModifiedBy, LastModifiedDate) VALUES ('" & HDate & "', '" & HOLIDAY & "','" & ADJUSTMENTHOLIDAY & "','" & OT_FACTOR & "','" & COMPANYCODE(i).Trim & "','" & DEPARTMENTCODE(j).Trim & "','admin', '" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "')"
        '                Else
        '                    sSql = "insert INTO Holiday (HDate, HOLIDAY, ADJUSTMENTHOLIDAY, OT_FACTOR, COMPANYCODE, DEPARTMENTCODE, LastModifiedBy, LastModifiedDate) VALUES ('" & HDate.ToString("yyyy-MM-dd 00:00:00") & "', '" & HOLIDAY & "','" & ADJUSTMENTHOLIDAY.ToString("yyyy-MM-dd 00:00:00") & "','" & OT_FACTOR & "','" & COMPANYCODE(i).Trim & "','" & DEPARTMENTCODE(j).Trim & "','admin', '" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "')"
        '                End If
        '            End If
        '            Try
        '                If Common.servername = "Access" Then
        '                    cmd1 = New OleDbCommand(sSql, Common.con1)
        '                    cmd1.ExecuteNonQuery()
        '                Else
        '                    cmd = New SqlCommand(sSql, Common.con)
        '                    cmd.ExecuteNonQuery()
        '                End If
        '            Catch ex As Exception
        '                If ex.Message.ToString = "The changes you requested to the table were not successful because they would create duplicate values in the index, primary key, or relationship.  Change the data in the field or fields that contain duplicate data, remove the index, or redefine the index to permit duplicate entries and try again." Then
        '                    Continue For
        '                End If
        '            End Try
        '        Next
        '    Next
        '    If Common.servername = "Access" Then
        '        If Common.con1.State <> ConnectionState.Closed Then
        '            Common.con1.Close()
        '        End If
        '    Else
        '        If Common.con.State <> ConnectionState.Closed Then
        '            Common.con.Close()
        '        End If
        '    End If
        'End If




        'Dim ls As New List(Of String)()
        'For x As Integer = 0 To COMPANYCODE.Length - 1
        '    ls.Add(COMPANYCODE(x).Trim)
        'Next
        'Dim ls1 As New List(Of String)()
        'For x As Integer = 0 To DEPARTMENTCODE.Length - 1
        '    ls1.Add(DEPARTMENTCODE(x).Trim)
        'Next
        'Dim sSql1 As String = "select PAYCODE from TblEmployee where (COMPANYCODE IN ('" & String.Join("', '", ls.ToArray()) & "') or  DEPARTMENTCODE IN ('" & String.Join("', '", ls1.ToArray()) & "'))and ACTIVE = 'Y'"
        'Dim adapXX As SqlDataAdapter
        'Dim adapXXA As OleDbDataAdapter
        'Dim dsXX As DataSet = New DataSet
        'If Common.servername = "Access" Then
        '    adapXXA = New OleDbDataAdapter(sSql1, Common.con1)
        '    adapXXA.Fill(dsXX)
        'Else
        '    adapXX = New SqlDataAdapter(sSql1, Common.con)
        '    adapXX.Fill(dsXX)
        'End If
        ''MsgBox(dsXX.Tables(0).Rows.Count)
        'Dim comclass As Common = New Common
        'If dsXX.Tables(0).Rows.Count > 0 Then
        '    For i As Integer = 0 To dsXX.Tables(0).Rows.Count - 1
        '        Dim adap As SqlDataAdapter
        '        Dim adapA As OleDbDataAdapter
        '        Dim ds As DataSet = New DataSet
        '        Dim sSql As String = "select ISROUNDTHECLOCKWORK from tblEmployeeShiftMaster where PAYCODE = '" & dsXX.Tables(0).Rows(i).Item("PAYCODE").Trim & "'"
        '        If Common.servername = "Access" Then
        '            adapA = New OleDbDataAdapter(sSql, Common.con1)
        '            adapA.Fill(ds)
        '        Else
        '            adap = New SqlDataAdapter(sSql, Common.con)
        '            adap.Fill(ds)
        '        End If
        '        If ds.Tables(0).Rows(0).Item("ISROUNDTHECLOCKWORK").ToString = "Y" Then
        '            comclass.Process_AllRTC(HDate.AddDays(-1), HDate, dsXX.Tables(0).Rows(i).Item("PAYCODE").Trim, dsXX.Tables(0).Rows(i).Item("PAYCODE").Trim)
        '        Else
        '            comclass.Process_AllnonRTC(HDate, HDate, dsXX.Tables(0).Rows(i).Item("PAYCODE").Trim, dsXX.Tables(0).Rows(i).Item("PAYCODE").Trim)
        '        End If
        '    Next
        'End If
        'Me.Close()
    End Sub
    Private Sub InsertHoliday()

        Dim HDate As DateTime = DateEdit1.DateTime
        Dim ADJUSTMENTHOLIDAY As DateTime = DateEdit2.DateTime
        Dim HOLIDAY As String = TextEdit1.Text.Trim
        Dim OT_FACTOR As Double = 0
        If TextEdit2.Text.Trim <> "" Then
            OT_FACTOR = TextEdit2.Text.Trim
        End If

        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim dsLocation As DataSet
        Dim dsDept As DataSet
        Dim sSql As String = ""

        Dim COMPANYCODE() As String = PopupContainerEdit1.EditValue.ToString.Split(",")
        Dim DEPARTMENTCODE() As String = PopupContainerEdit2.EditValue.ToString.Split(",")
        Dim Location() As String = PopupContainerEdit3.EditValue.ToString.Split(",")

        Dim comclass As Common = New Common

        If Common.servername = "Access" Then
            If Common.con1.State <> ConnectionState.Open Then
                Common.con1.Open()
            End If
        Else
            If Common.con.State <> ConnectionState.Open Then
                Common.con.Open()
            End If
        End If
        For i As Integer = 0 To COMPANYCODE.Length - 1  'for company
            Dim CountLocation As Integer = 0
            If PopupContainerEdit3.EditValue.ToString.Trim = "" Then
                sSql = "select * from tblbranch	"
            Else
                Dim ls As New List(Of String)()
                For x As Integer = 0 To Location.Length - 1
                    ls.Add(Location(x).Trim)
                Next
                sSql = "select * from tblbranch	where BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "')"
            End If
            dsLocation = New DataSet
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(dsLocation)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(dsLocation)
            End If
            For j As Integer = 0 To dsLocation.Tables(0).Rows.Count - 1  'for location
                If PopupContainerEdit2.EditValue.ToString.Trim = "" Then
                    sSql = "select * from tblDepartment	"
                Else
                    Dim ls As New List(Of String)()
                    For x As Integer = 0 To DEPARTMENTCODE.Length - 1
                        ls.Add(DEPARTMENTCODE(x).Trim)
                    Next
                    sSql = "select * from tblDepartment	where DEPARTMENTCODE IN ('" & String.Join("', '", ls.ToArray()) & "')"
                End If
                dsDept = New DataSet
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(sSql, Common.con1)
                    adapA.Fill(dsDept)
                Else
                    adap = New SqlDataAdapter(sSql, Common.con)
                    adap.Fill(dsDept)
                End If
                For k As Integer = 0 To dsDept.Tables(0).Rows.Count - 1
                    If Common.servername = "Access" Then
                        If Common.con1.State <> ConnectionState.Open Then
                            Common.con1.Open()
                        End If
                        sSql = "insert INTO Holiday (HDate, HOLIDAY, ADJUSTMENTHOLIDAY, OT_FACTOR, COMPANYCODE, BRANCHCODE, DEPARTMENTCODE, LastModifiedBy, LastModifiedDate) VALUES ('" & HDate & "', '" & HOLIDAY & "','" & ADJUSTMENTHOLIDAY & "','" & OT_FACTOR & "','" & COMPANYCODE(i).Trim & "','" & dsLocation.Tables(0).Rows(j).Item("BRANCHCODE").ToString.Trim & "','" & dsDept.Tables(0).Rows(k).Item("DEPARTMENTCODE").Trim & "','admin', '" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "')"
                        cmd1 = New OleDbCommand(sSql, Common.con1)
                        cmd1.ExecuteNonQuery()
                        If Common.con1.State <> ConnectionState.Closed Then
                            Common.con1.Close()
                        End If
                    Else
                        If Common.con.State <> ConnectionState.Open Then
                            Common.con.Open()
                        End If
                        sSql = "insert INTO Holiday (HDate, HOLIDAY, ADJUSTMENTHOLIDAY, OT_FACTOR, COMPANYCODE, BRANCHCODE, DEPARTMENTCODE, LastModifiedBy, LastModifiedDate) VALUES ('" & HDate.ToString("yyyy-MM-dd 00:00:00") & "', '" & HOLIDAY & "','" & ADJUSTMENTHOLIDAY & "','" & OT_FACTOR & "','" & COMPANYCODE(i).Trim & "','" & dsLocation.Tables(0).Rows(j).Item("BRANCHCODE").ToString.Trim & "','" & dsDept.Tables(0).Rows(k).Item("DEPARTMENTCODE").Trim & "','admin', '" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "')"
                        cmd = New SqlCommand(sSql, Common.con)
                        cmd.ExecuteNonQuery()
                        If Common.con.State <> ConnectionState.Closed Then
                            Common.con.Close()
                        End If
                    End If

                    Dim ds As DataSet = New DataSet
                    Dim sSql1 As String = "select TblEmployee.PAYCODE, tblEmployeeShiftMaster.ISROUNDTHECLOCKWORK, EmployeeGroup.Id from tblEmployeeShiftMaster, TblEmployee, EmployeeGroup where tblEmployeeShiftMaster.PAYCODE=TblEmployee.PAYCODE and TblEmployee.COMPANYCODE = '" & COMPANYCODE(i).Trim & "' and TblEmployee.BRANCHCODE = '" & dsLocation.Tables(0).Rows(j).Item("BRANCHCODE").ToString.Trim & "' and TblEmployee.DEPARTMENTCODE = '" & dsDept.Tables(0).Rows(k).Item("DEPARTMENTCODE").Trim & "' and TblEmployee.EmployeeGroupId=EmployeeGroup.GroupId"
                    If Common.servername = "Access" Then
                        adapA = New OleDbDataAdapter(sSql1, Common.con1)
                        adapA.Fill(ds)
                    Else
                        adap = New SqlDataAdapter(sSql1, Common.con)
                        adap.Fill(ds)
                    End If
                    If ds.Tables(0).Rows.Count > 0 Then
                        For x As Integer = 0 To ds.Tables(0).Rows.Count - 1
                            If ds.Tables(0).Rows(x).Item("ISROUNDTHECLOCKWORK").ToString = "Y" Then
                                If Common.PrcessMode = "M" Then
                                    comclass.Process_AllnonRTCINOUT(HDate.AddDays(-1).ToString("yyyy-MM-dd"), HDate.ToString("yyyy-MM-dd"), ds.Tables(0).Rows(x).Item("PAYCODE").Trim, ds.Tables(0).Rows(x).Item("PAYCODE").Trim, ds.Tables(0).Rows(x).Item("Id"), ds.Tables(0).Rows(x).Item("ISROUNDTHECLOCKWORK").ToString)
                                Else
                                    comclass.Process_AllRTC(HDate.AddDays(-1).ToString("yyyy-MM-dd"), HDate.ToString("yyyy-MM-dd"), ds.Tables(0).Rows(x).Item("PAYCODE").Trim, ds.Tables(0).Rows(x).Item("PAYCODE").Trim, ds.Tables(0).Rows(x).Item("Id"))
                                End If
                                'comclass.Process_AllRTC(HDate.AddDays(-1).ToString("yyyy-MM-dd"), HDate.ToString("yyyy-MM-dd"), ds.Tables(0).Rows(x).Item("PAYCODE").Trim, ds.Tables(0).Rows(x).Item("PAYCODE").Trim, ds.Tables(0).Rows(x).Item("Id"))
                            Else
                                If Common.PrcessMode = "M" Then
                                    comclass.Process_AllnonRTCINOUT(HDate.ToString("yyyy-MM-dd"), HDate.ToString("yyyy-MM-dd"), ds.Tables(0).Rows(x).Item("PAYCODE").Trim, ds.Tables(0).Rows(x).Item("PAYCODE").Trim, ds.Tables(0).Rows(x).Item("Id"), ds.Tables(0).Rows(x).Item("ISROUNDTHECLOCKWORK").ToString)
                                Else
                                    comclass.Process_AllnonRTC(HDate.ToString("yyyy-MM-dd"), HDate.ToString("yyyy-MM-dd"), ds.Tables(0).Rows(x).Item("PAYCODE").Trim, ds.Tables(0).Rows(x).Item("PAYCODE").Trim, ds.Tables(0).Rows(x).Item("Id"))
                                End If
                                'comclass.Process_AllnonRTC(HDate.ToString("yyyy-MM-dd"), HDate.ToString("yyyy-MM-dd"), ds.Tables(0).Rows(x).Item("PAYCODE").Trim, ds.Tables(0).Rows(x).Item("PAYCODE").Trim, ds.Tables(0).Rows(x).Item("Id"))
                                If Common.EmpGrpArr(ds.Tables(0).Rows(x).Item("Id")).SHIFTTYPE = "M" Then
                                    comclass.Process_AllnonRTCMulti(HDate.ToString("yyyy-MM-dd"), HDate.ToString("yyyy-MM-dd"), ds.Tables(0).Rows(x).Item("PAYCODE").Trim, ds.Tables(0).Rows(x).Item("PAYCODE").Trim, ds.Tables(0).Rows(x).Item("Id"))
                                End If
                            End If
                        Next
                    End If
                Next
            Next
        Next
        Common.LogPost("Holiday Add; HDate='" & HDate.ToString("yyyy-MM-dd"))
        If Common.servername = "Access" Then
            If Common.con1.State <> ConnectionState.Closed Then
                Common.con1.Close()
            End If
        Else
            If Common.con.State <> ConnectionState.Closed Then
                Common.con.Close()
            End If
        End If
        XtraMasterTest.LabelControlStatus.Text = ""
        Application.DoEvents()
        Me.Close()
    End Sub
    Private Sub SimpleButton2_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton2.Click
        Me.Close()
    End Sub
End Class