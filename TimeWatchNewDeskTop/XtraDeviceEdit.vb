﻿Imports System.Resources
Imports System.Globalization
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports System.ComponentModel
Imports DevExpress.XtraGrid.Columns
Imports System.Data.SqlClient
Imports System.Text
Imports System.Data.OleDb
Imports System.Net
Imports System.Runtime.InteropServices
Imports iAS.B100Demo
Imports Riss.Devices 'bio2+
Imports iAS.HSeriesSampleCSharp
Imports CMITech.UMXClient
Imports Newtonsoft.Json.Linq
Imports Newtonsoft.Json

Public Class XtraDeviceEdit
    Dim ulf As UserLookAndFeel
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand

    Dim Did As String
    Dim DeviceValid As Boolean = False
    Dim DeviceDeactivate As Boolean = False
    Public axFP_CLOCK As AxFP_CLOCKLib.AxFP_CLOCK
    Private pOcxObject As AxFP_CLOCKLib.AxFP_CLOCK

    Public Class DeviceAPI
        Public Property ResultMsg As String
        Public Property Result As Boolean
    End Class
    Public Class ActivateSerialResultClass
        Public Property ActivateSerialResult As DeviceAPI = New DeviceAPI
    End Class
    Public Class DeActivateSerialResultClass
        Public Property DeActivateSerialResult As DeviceAPI = New DeviceAPI
    End Class
    Public Sub New()
        InitializeComponent()
        If Common.servername = "Access" Then
            Me.Tblbranch1TableAdapter1.Fill(Me.SSSDBDataSet.tblbranch1)
            GridLookUpEdit1.Properties.DataSource = SSSDBDataSet.tblbranch1
        Else
            TblbranchTableAdapter.Connection.ConnectionString = Common.ConnectionString '"Data Source=DESKTOP-PG6NDV8;Initial Catalog=SSSDB;Integrated Security=True"
            Me.TblbranchTableAdapter.Fill(Me.SSSDBDataSet.tblbranch)
            GridLookUpEdit1.Properties.DataSource = SSSDBDataSet.tblbranch
        End If
    End Sub
    Private Sub XtraDeviceEdit_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        GridLookUpEdit1.Properties.DataSource = Common.LocationNonAdmin
        DeviceValid = False
        'If Common.servername = "Access" Then
        '    Me.Tblbranch1TableAdapter1.Fill(Me.SSSDBDataSet.tblbranch1)
        '    GridLookUpEdit1.Properties.DataSource = SSSDBDataSet.tblbranch1
        'Else
        '    Me.TblbranchTableAdapter.Fill(Me.SSSDBDataSet.tblbranch)
        '    GridLookUpEdit1.Properties.DataSource = SSSDBDataSet.tblbranch
        'End If
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True

        If XtraDevice.DeviceID = "" Then
            setDefault()
        Else
            setData()
        End If
        setTCPUSB()
        'If (ComboBoxEdit3.SelectedIndex = 0 Or ComboBoxEdit3.SelectedIndex = 1 Or ComboBoxEdit3.SelectedIndex = 2) _
        '    And license.LicenseKey <> "" Then ' And Not (license.TrialPerioad = True And license.TrialExpired = False) Then
        If (ComboBoxEdit3.SelectedIndex = 0 Or ComboBoxEdit3.SelectedIndex = 1 Or ComboBoxEdit3.SelectedIndex = 2 Or ComboBoxEdit3.SelectedIndex = 10) Then
            ' And Not (license.TrialPerioad = True And license.TrialExpired = False) Then
            'BtnVlidate.Visible = True
            PanelControl1.Visible = True
            PanelControl2.Visible = True
        Else
            'BtnVlidate.Visible = False
            PanelControl1.Visible = False
            PanelControl2.Visible = False
        End If
    End Sub
    Private Sub setTCPUSB()
        If ComboBoxEdit1.SelectedIndex = 0 Then
            TextIP.Visible = True
            LabelControl5.Visible = True
            SimpleButtonGetSr.Visible = True
        Else
            TextIP.Visible = False
            LabelControl5.Visible = False
            If ComboBoxEdit3.EditValue.ToString.Trim = "Bio-1/3/4/5/6/7/ATF-203/ATF-395/ACR-872" Then
                SimpleButtonGetSr.Visible = True
            Else
                SimpleButtonGetSr.Visible = False
            End If
        End If
    End Sub
    Private Sub setDefault()
        TextConId.Enabled = True
        TextConId.Text = ""
        ComboBoxEdit1.SelectedIndex = 0
        ComboBoxEdit2.SelectedIndex = 0
        ComboBoxEdit3.SelectedIndex = 1
        TextIP.Text = ""
        GridLookUpEdit1.EditValue = GridLookUpEdit1.Properties.GetKeyValue(0)
        TextSrNo.Text = ""
        LabelControl9.Visible = False
        TextEditComm.Visible = False
        TextEditComm.Text = "0"
        TextLED.Text = ""
        BtnVlidate.Enabled = False
        BtnDeVlidate.Enabled = False
    End Sub
    Private Sub setData()
        Dim adap As SqlDataAdapter
        Dim adapA As OleDbDataAdapter
        Dim ds As DataSet = New DataSet
        Dim sSql As String = "select * from tblMachine WHERE ID_NO='" & XtraDevice.DeviceID & "'"
        If Common.servername = "Access" Then
            adapA = New OleDbDataAdapter(sSql, Common.con1)
            adapA.Fill(ds)
        Else
            adap = New SqlDataAdapter(sSql, Common.con)
            adap.Fill(ds)
        End If
        TextConId.Enabled = False
        TextConId.Text = ds.Tables(0).Rows(0).Item("ID_NO").ToString.Trim
        If ds.Tables(0).Rows(0).Item("A_R").ToString.Trim = "T" Then
            ComboBoxEdit1.SelectedIndex = 0
        Else
            ComboBoxEdit1.SelectedIndex = 1
        End If
        If ds.Tables(0).Rows(0).Item("IN_OUT").ToString.Trim = "I" Then
            ComboBoxEdit2.SelectedIndex = 0
        ElseIf ds.Tables(0).Rows(0).Item("IN_OUT").ToString.Trim = "O" Then
            ComboBoxEdit2.SelectedIndex = 1
        ElseIf ds.Tables(0).Rows(0).Item("IN_OUT").ToString.Trim = "B" Then
            ComboBoxEdit2.SelectedIndex = 2
        Else
            ComboBoxEdit2.SelectedIndex = 3
        End If
        ComboBoxEdit3.EditValue = ds.Tables(0).Rows(0).Item("DeviceType").ToString.Trim
        TextIP.Text = ds.Tables(0).Rows(0).Item("LOCATION").ToString.Trim
        GridLookUpEdit1.EditValue = ds.Tables(0).Rows(0).Item("branch").ToString.Trim
        TextSrNo.Text = ds.Tables(0).Rows(0).Item("MAC_ADDRESS").ToString.Trim
        TextEditComm.Text = ds.Tables(0).Rows(0).Item("commkey").ToString.Trim
        If ds.Tables(0).Rows(0).Item("Purpose").ToString.Trim = "A" Then
            ComboBoxEdit4.SelectedIndex = 0
        ElseIf ds.Tables(0).Rows(0).Item("Purpose").ToString.Trim = "C" Then
            ComboBoxEdit4.SelectedIndex = 1
        End If

        If ComboBoxEdit3.SelectedIndex = 0 Or ComboBoxEdit3.SelectedIndex = 2 Or ComboBoxEdit3.SelectedIndex = 4 Or ComboBoxEdit3.SelectedIndex = 10 Then
            TextEditComm.Visible = True
            LabelControl9.Visible = True
        Else
            TextEditComm.Visible = False
            LabelControl9.Visible = False
        End If
        TextLED.Text = ds.Tables(0).Rows(0).Item("LEDIP").ToString.Trim
        If TextSrNo.Text.Trim = "" Then
            BtnVlidate.Enabled = False
            BtnDeVlidate.Enabled = False
        Else
            BtnVlidate.Enabled = True
            BtnDeVlidate.Enabled = True
        End If
    End Sub
    Private Sub ComboBoxEdit1_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBoxEdit1.SelectedIndexChanged
        setTCPUSB()
    End Sub
    Private Sub SimpleButton2_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton2.Click
        Me.Close()
    End Sub
    Private Sub SimpleButton1_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton1.Click
        Dim ID_NO As String '= Convert.ToInt16(TextEdit1.Text.Trim)
        If TextConId.Text.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Controleer ID cannot be empty</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            TextConId.Select()
            Exit Sub
        End If
        ID_NO = Convert.ToInt16(TextConId.Text.Trim)
        Dim A_R As String
        Dim LOCATION As String
        If ComboBoxEdit1.SelectedIndex = 0 Then
            A_R = "T"
            If Common.IsValidIPAddress(TextIP.Text.Trim) Then
                LOCATION = TextIP.Text.Trim
            Else
                If ComboBoxEdit1.SelectedIndex = 0 Then
                    XtraMessageBox.Show(ulf, "<size=10>Invalid Device IP</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    TextIP.Select()
                    Exit Sub
                End If
            End If
        Else
            A_R = "S"
            LOCATION = ""
        End If
        Dim IN_OUT As String
        If ComboBoxEdit2.SelectedIndex = 0 Then
            IN_OUT = "I"
        ElseIf ComboBoxEdit2.SelectedIndex = 1 Then
            IN_OUT = "O"
        ElseIf ComboBoxEdit2.SelectedIndex = 2 Then
            IN_OUT = "B"
        Else
            IN_OUT = "D"
        End If
        Dim DeviceType As String = ComboBoxEdit3.EditValue
        '= TextEdit2.Text.Trim
        'Dim address As IPAddress = Nothing
        'If IPAddress.TryParse(TextEdit2.Text.Trim, address) Then

        Dim branch As String = GridLookUpEdit1.EditValue
        Dim MAC_ADDRESS As String = TextSrNo.Text.Trim
        Dim commkey As String = "0"
        If TextEditComm.Text.Trim = "" Then
            commkey = "0"
        Else
            commkey = TextEditComm.Text.Trim
        End If

        Dim Purpose As String
        If ComboBoxEdit4.SelectedIndex = 0 Then
            Purpose = "A"
        Else
            Purpose = "C"
        End If
        Dim LEDIP As String = TextLED.Text.Trim
        Dim sSql As String
        If XtraDevice.DeviceID = "" Then
            'insert
            Dim adap As SqlDataAdapter
            Dim adapA As OleDbDataAdapter
            Dim ds As DataSet = New DataSet
            Dim cquery As String = "select ID_NO from tblMachine WHERE ID_NO='" & ID_NO & "'"
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(cquery, Common.con1)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(cquery, Common.con)
                adap.Fill(ds)
            End If
            If ds.Tables(0).Rows.Count > 0 Then
                XtraMessageBox.Show(ulf, "<size=10>Duplicate Device ID</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                TextConId.Select()
                Exit Sub
            End If

            If ComboBoxEdit1.SelectedIndex = 0 Then
                ds = New DataSet
                cquery = "select LOCATION from tblMachine WHERE LOCATION='" & LOCATION & "' and A_R = 'T'"
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(cquery, Common.con1)
                    adapA.Fill(ds)
                Else
                    adap = New SqlDataAdapter(cquery, Common.con)
                    adap.Fill(ds)
                End If
                If ds.Tables(0).Rows.Count > 0 Then
                    XtraMessageBox.Show(ulf, "<size=10>Duplicate Device IP</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    TextIP.Select()
                    Exit Sub
                End If
            End If
            sSql = "INSERT INTO tblMachine (ID_NO, A_R, IN_OUT, DeviceType,LOCATION, branch,MAC_ADDRESS, LastModifiedBy, LastModifiedDate, Purpose, commkey, LEDIP ) VALUES('" & ID_NO & "', '" & A_R & "', '" & IN_OUT & "', '" & DeviceType & "','" & LOCATION & "', '" & branch & "', '" & MAC_ADDRESS & "', 'admin', '" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "','" & Purpose & "','" & commkey & "','" & LEDIP & "')"
            Common.LogPost("Device Insert; Device ID: " & ID_NO)
        Else
            If license.DeviceSerialNo.Contains(TextSrNo.Text.Trim) Then
                DeviceValid = True
            End If
            sSql = "update tblMachine SET A_R = '" & A_R & "' , IN_OUT ='" & IN_OUT & "', DeviceType = '" & DeviceType & "', LOCATION= '" & LOCATION & "', branch = '" & branch & "', MAC_ADDRESS='" & MAC_ADDRESS & "', LastModifiedBy='admin', LastModifiedDate= '" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "', Purpose='" & Purpose & "', commkey='" & commkey & "', LEDIP ='" & LEDIP & "' where ID_NO='" & ID_NO & "'"
            Common.LogPost("Device Update; Device ID: " & ID_NO)
            'update
        End If
        'MsgBox(sSql)

        'If (ComboBoxEdit3.SelectedIndex = 0 Or ComboBoxEdit3.SelectedIndex = 1 Or ComboBoxEdit3.SelectedIndex = 2) _
        '    And license.LicenseKey <> "" Then 'And Not (license.TrialPerioad = True And license.TrialExpired = False) Then
        If (ComboBoxEdit3.SelectedIndex = 0 Or ComboBoxEdit3.SelectedIndex = 1 Or ComboBoxEdit3.SelectedIndex = 2 Or ComboBoxEdit3.SelectedIndex = 10) Then 'And Not (license.TrialPerioad = True And license.TrialExpired = False) Then
            If DeviceValid = False Then
                XtraMessageBox.Show(ulf, "<size=10>Cannot Save. Unauthorized Serial Number</size>", "<size=9>iAS</size>")
                Exit Sub
            ElseIf DeviceValid = True Then
                Dim infoS As String = "select * from iASSystemInfo"
                Dim dsI As DataSet = New DataSet
                If Common.servername = "Access" Then
                    Dim adapA As OleDbDataAdapter = New OleDbDataAdapter(infoS, Common.con1)
                    adapA.Fill(dsI)
                Else
                    Dim adap As SqlDataAdapter = New SqlDataAdapter(infoS, Common.con)
                    adap.Fill(dsI)
                End If
                Dim serial As String
                If dsI.Tables(0).Rows(0)("DSerialNo").ToString.Trim <> "" Then
                    serial = EncyDcry.Encrypt(MAC_ADDRESS, True) & "," & dsI.Tables(0).Rows(0)("DSerialNo").ToString.Trim
                Else
                    serial = EncyDcry.Encrypt(MAC_ADDRESS, True)
                End If

                If DeviceDeactivate = True Then
                    serial = serial.Replace(EncyDcry.Encrypt(MAC_ADDRESS, True), "").Replace(",,", ",")
                    serial = serial.TrimStart(",").TrimEnd(",")
                End If

                infoS = "update iASSystemInfo set DSerialNo='" & serial & "'"
                    If Common.servername = "Access" Then
                        If Common.con1.State <> ConnectionState.Open Then
                            Common.con1.Open()
                        End If
                        cmd1 = New OleDbCommand(infoS, Common.con1)
                        cmd1.ExecuteNonQuery()
                        If Common.con1.State <> ConnectionState.Closed Then
                            Common.con1.Close()
                        End If
                    Else
                        If Common.con.State <> ConnectionState.Open Then
                            Common.con.Open()
                        End If
                        cmd = New SqlCommand(infoS, Common.con)
                        cmd.ExecuteNonQuery()
                        If Common.con.State <> ConnectionState.Open Then
                            Common.con.Close()
                        End If
                    End If
                    'update license.lic file
                    My.Computer.FileSystem.DeleteFile("License.lic")
                    Dim fs As FileStream = New FileStream("License.lic", FileMode.Create, FileAccess.Write)
                    Dim sw As StreamWriter = New StreamWriter(fs)
                    Dim str As String = ""
                    str = EncyDcry.Encrypt(license.HDDSerialNo, True)
                    sw.WriteLine(str)
                    sw.WriteLine(EncyDcry.Encrypt(license.InstallDate.ToString("yyyy-MM-dd HH:mm:ss"), True))
                    str = EncyDcry.Encrypt(license.NoOfUsers, True)
                    sw.WriteLine(str)
                    str = EncyDcry.Encrypt(license.NoOfDevices, True)
                    sw.WriteLine(str)
                    sw.WriteLine(license.LicenseKey)
                    sw.Write(serial)
                    sw.Flush()
                    sw.Close()
                    fs.Close()

                    license.getLicenseInfo()
                End If
            End If
        If Common.servername = "Access" Then
            If Common.con1.State <> ConnectionState.Open Then
                Common.con1.Open()
            End If
            cmd1 = New OleDbCommand(sSql, Common.con1)
            cmd1.ExecuteNonQuery()
            If Common.con1.State <> ConnectionState.Closed Then
                Common.con1.Close()
            End If
        Else
            If Common.con.State <> ConnectionState.Open Then
                Common.con.Open()
            End If
            'MsgBox(sSql)
            cmd = New SqlCommand(sSql, Common.con)
            cmd.ExecuteNonQuery()
            If Common.con.State <> ConnectionState.Open Then
                Common.con.Close()
            End If
        End If
        Me.Close()
    End Sub
    Private Sub SimpleButtonGetSr_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonGetSr.Click
        If TextIP.Text.Trim = "" And ComboBoxEdit1.SelectedIndex = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>IP Adress cannnot be empty</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            TextIP.Select()
            Exit Sub
        End If
        If TextEditComm.Text.Trim = "" Then
            TextEditComm.Text = "0"
        End If
        Dim a As DateTime
        Dim c As Common = New Common()
        Dim vnMachineNumber As Integer
        Dim vnLicense As Long
        Dim vpszIPAddress As String
        Dim vpszNetPort As Integer
        Dim vpszNetPassword As Integer
        Dim vnTimeOut As Long
        Dim vnProtocolType As Long
        Dim vnReadMark As Integer
        Dim IP, ID_NO, DeviceType, A_R As Object
        Dim cn As Common = New Common
        Dim failIP As New List(Of String)()
        Me.Cursor = Cursors.WaitCursor
        IP = TextIP.Text.Trim
        ID_NO = TextConId.Text.Trim
        DeviceType = ComboBoxEdit3.EditValue.ToString.Trim
        A_R = ComboBoxEdit1.EditValue.ToString.Trim
        Common.LogPost("Device Get Serial No; Device ID: " & ID_NO)
        If DeviceType = "Bio-1/3/4/5/6/7/ATF-203/ATF-395/ACR-872" Then
            vnMachineNumber = ID_NO.ToString  '1
            vpszIPAddress = IP.ToString '"192.168.0.111"
            vpszNetPort = 5005
            vpszNetPassword = 0
            vnTimeOut = 5000
            vnProtocolType = PROTOCOL_TCPIP
            vnLicense = 1261
            Dim gnCommHandleIndex As Long
            If A_R = "USB" Then
                gnCommHandleIndex = FK_ConnectUSB(vnMachineNumber, vnLicense)
            Else
                gnCommHandleIndex = c.ConnectToDevice(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
            End If
            If gnCommHandleIndex > 0 Then
                Dim vstrData As String = New String(CType(ChrW(32), Char), 256)
                Dim ret = FK_GetProductData(gnCommHandleIndex, CType(1, Integer), vstrData)
                TextSrNo.Text = vstrData
                'Dim statusIndex As Long = 1
                'Dim x As Long = FK_GetDeviceStatus(gnCommHandleIndex, statusIndex, GET_USERS)

                FK_DisConnect(gnCommHandleIndex)
            Else
                XtraMessageBox.Show(ulf, "<size=10>Connection Failed</size>", "Failed")
                Me.Cursor = Cursors.Default
                Exit Sub
            End If
        ElseIf DeviceType = "ZK(TFT)" Or DeviceType.ToString.Trim = "Bio-1Pro/ATF305Pro/ATF686Pro" Then
            Dim axCZKEM1tmp As New zkemkeeper.CZKEM
            Dim comZK As CommonZK = New CommonZK
            axCZKEM1tmp.SetCommPassword(Convert.ToInt32(Convert.ToInt32(TextEditComm.Text.Trim)))  'to check device commkey and db commkey matches

            Dim result As Boolean = comZK.connectZK(IP, 4370, axCZKEM1tmp)
            If result = True Then
                Dim iMachineNumber As Integer = 1
                Dim idwErrorCode As Integer

                Dim idwYear As Integer
                Dim idwMonth As Integer
                Dim idwDay As Integer
                Dim idwHour As Integer
                Dim idwMinute As Integer
                Dim idwSecond As Integer
                Dim sSN As String = ""
                If axCZKEM1tmp.GetSerialNumber(1, sSN) = True Then
                    TextSrNo.Text = sSN
                Else
                    axCZKEM1tmp.GetLastError(idwErrorCode)
                    XtraMessageBox.Show(ulf, "<size=10>Operation failed,ErrorCode=" & idwErrorCode.ToString() & "</size>", "Failed")
                End If
            Else
                XtraMessageBox.Show(ulf, "<size=10>Connection Failed</size>", "Failed")
                Me.Cursor = Cursors.Default
                Exit Sub
            End If
            comZK.DisconnectZK(axCZKEM1tmp)
        ElseIf DeviceType = "TW-IR102" Then
            Dim _IntPtr2 As IntPtr = Marshal.AllocHGlobal(200)
            If (B100API.F_GetDevSerial(IP, Integer.Parse(15001), _IntPtr2, 200) = 0) Then
                Dim temp() As Byte = New Byte((200) - 1) {}
                Marshal.Copy(_IntPtr2, temp, 0, 200)
                TextSrNo.Text = System.Text.Encoding.Default.GetString(temp)
            Else
                XtraMessageBox.Show(ulf, "<size=10>Connection Failed</size>", "Failed")
                Me.Cursor = Cursors.Default
                Exit Sub
            End If
            Marshal.FreeHGlobal(_IntPtr2)
        ElseIf DeviceType = "Bio2+" Then
            Dim deviceG2 As Riss.Devices.Device
            Dim deviceConnectionG2 As Riss.Devices.DeviceConnection
            deviceG2 = New Riss.Devices.Device()
            deviceG2.DN = 1 'CInt(nud_DN.Value)
            deviceG2.Password = TextEditComm.Text.Trim ' nud_Pwd.Value.ToString()
            deviceG2.Model = "ZDC2911"
            deviceG2.ConnectionModel = 5
            If ComboBoxEdit1.EditValue = "USB" Then
                deviceG2.CommunicationType = CommunicationType.Usb
            Else
                deviceG2.IpAddress = TextIP.Text.Trim()
                deviceG2.IpPort = 5500 'CInt(nud_Port.Value)
                deviceG2.CommunicationType = CommunicationType.Tcp
            End If
            deviceConnectionG2 = DeviceConnection.CreateConnection(deviceG2)
            If deviceConnectionG2.Open() > 0 Then
                Dim extraProperty As Object = New Object
                Dim extraData As Object = New Object
                extraData = Zd2911Utils.DeviceSerialNo
                Dim result As Boolean = deviceConnectionG2.GetProperty(DeviceProperty.SerialNo, extraProperty, deviceG2, extraData)
                If result Then
                    TextSrNo.Text = CType(extraData, String)
                Else
                    deviceConnectionG2.Close()
                    XtraMessageBox.Show(ulf, "<size=10>Connection Failed</size>", "Failed")
                    Me.Cursor = Cursors.Default
                    Exit Sub
                End If
                deviceConnectionG2.Close()
            Else
                XtraMessageBox.Show(ulf, "<size=10>Connection Failed</size>", "Failed")
                Me.Cursor = Cursors.Default
                Exit Sub
            End If
        ElseIf DeviceType = "Bio1Eco" Then
            Dim mOpenFlag As Boolean
            Dim mMK8001Device As Boolean 'TRUE:MK8001/8002Device;False:else

            Dim ret As Int32 = CType(SyLastError.sleInvalidParam, Int32)
            Dim equNo As UInt32 = Convert.ToUInt32(TextConId.Text.Trim)
            Dim gEquNo As UInt32 = equNo
            If A_R = "TCP/IP" Then
                Dim netCfg As SyNetCfg
                Dim pswd As UInt32 = 0
                Dim tmpPswd As String = TextEditComm.Text.Trim
                If (tmpPswd = "0") Then
                    tmpPswd = "FFFFFFFF"
                End If
                Try
                    pswd = UInt32.Parse(tmpPswd, NumberStyles.HexNumber)
                Catch ex As Exception
                    XtraMessageBox.Show(ulf, "<size=10>Incorrect Comm Key</size>", "Failed")
                    ret = CType(SyLastError.slePasswordError, Int32)
                End Try
                If (ret <> CType(SyLastError.slePasswordError, Int32)) Then
                    netCfg.mIsTCP = 1
                    netCfg.mPortNo = 8000 'Convert.ToUInt16(txtPortNo.Text)
                    netCfg.mIPAddr = New Byte((4) - 1) {}
                    Dim sArray() As String = IP.Split(Microsoft.VisualBasic.ChrW(46))
                    Dim j As Byte = 0
                    For Each i As String In sArray
                        Try
                            netCfg.mIPAddr(j) = Convert.ToByte(i.Trim)
                        Catch ex As System.Exception
                            netCfg.mIPAddr(j) = 255
                        End Try
                        j = (j + 1)
                        If j > 3 Then
                            Exit For
                        End If
                    Next
                End If

                Dim pnt As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(netCfg))
                Try
                    Marshal.StructureToPtr(netCfg, pnt, False)
                    '        If optWifiDevice.Checked Then
                    'ret = SyFunctions.CmdOpenDevice(3, pnt, equNo, pswd, Convert.ToByte(chkTranceIO.Checked?1, :, 0))
                    '        Else
                    ret = SyFunctions.CmdOpenDevice(2, pnt, equNo, pswd, Convert.ToByte(1))
                    '        End If
                    '                'TODO: Warning!!!, inline IF is not supported ?
                    '                chkTranceIO.Checked()
                    '0:
                    '                '
                Finally
                    Marshal.FreeHGlobal(pnt)
                End Try
            ElseIf A_R = "USB" Then
                Dim equtype As String = "Finger Module USB Device"
                Dim pswd As UInt32 = UInt32.Parse("FFFFFFFF", NumberStyles.HexNumber)
                ret = SyFunctions.CmdOpenDeviceByUsb(equtype, equNo, pswd, Convert.ToByte(1))
            End If
            If (ret = CType(SyLastError.sleSuss, Int32)) Then
                ret = SyFunctions.CmdTestConn2Device
                '
                If (ret = CType(SyLastError.sleSuss, Int32)) Then
                    mOpenFlag = True
                    mMK8001Device = False
                    Dim si As SySystemInfo = New SySystemInfo
                    si.cSerialNum = New Byte((20) - 1) {}
                    si.cManuName = New Byte((24) - 1) {}
                    si.cDevName = New Byte((24) - 1) {}
                    si.cAlgVer = New Byte((16) - 1) {}
                    si.cFirmwareVer = New Byte((24) - 1) {}
                    Dim rret As Integer = 66
                    Try
                        rret = SyFunctions.CmdGetSystemInfo(si)
                    Catch ex As Exception

                    End Try

                    If (rret = CType(SyLastError.sleSuss, Int32)) Then
                        Dim sFirmwareVer As String = System.Text.Encoding.Default.GetString(si.cFirmwareVer)
                        If sFirmwareVer.Contains("MK8001/8002") Then
                            mMK8001Device = True
                        Else
                            mMK8001Device = False
                        End If
                    End If
                    Dim gbk As Encoding = Encoding.GetEncoding(20936)
                    TextSrNo.Text = Encoding.Default.GetString(Encoding.Convert(gbk, Encoding.Default, si.cSerialNum, 0, 13))
                End If
                SyFunctions.CloseCom()
            End If
            If (ret <> CType(SyLastError.sleSuss, Int32)) Then
                XtraMessageBox.Show(ulf, "<size=10>Connection Failed</size>", "Failed")
                'XtraMessageBox.Show(ulf, "<size=10>" & (util.ErrorPrint(ret)) & "</size>", "Failed")
            End If
        ElseIf DeviceType = "EF45" Then
            Dim _client As Client = Nothing
            Dim connectOk As Boolean = False
            Try
                _client = cn.initClientEF45(IP)
                connectOk = _client.StealConnect() '_client.Connect()
                If connectOk Then
                    TextSrNo.Text = _client.GetConfigString("launcher.device.deviceinfo.deviceid")
                    _client.Disconnect()
                Else
                    XtraMessageBox.Show(ulf, "<size=10>Connection Failed</size>", "Failed")
                    Me.Cursor = Cursors.Default
                    Exit Sub
                End If
            Catch ex As Exception
                XtraMessageBox.Show(ulf, "<size=10>Connection Failed</size>", "Failed")
                Me.Cursor = Cursors.Default
                Exit Sub
            End Try
        ElseIf DeviceType = "F9" Then
            vnMachineNumber = ID_NO.ToString  '1
            vpszIPAddress = IP.ToString '"192.168.0.111"
            vpszNetPort = 5005
            vpszNetPassword = 0
            vnTimeOut = 5000
            vnProtocolType = PROTOCOL_TCPIP
            vnLicense = 7881
            Dim gnCommHandleIndex As Long
            Dim F9 As mdlPublic_f9 = New mdlPublic_f9
            If A_R = "USB" Then
                gnCommHandleIndex = F9.FK_ConnectUSB(vnMachineNumber, vnLicense)
            Else
                gnCommHandleIndex = F9.FK_ConnectNet(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
            End If
            If gnCommHandleIndex > 0 Then
                Dim vstrData As String = New String(CType(ChrW(32), Char), 256)
                Dim ret = F9.FK_GetProductData(gnCommHandleIndex, CType(1, Integer), vstrData)
                TextSrNo.Text = vstrData
                F9.FK_DisConnect(gnCommHandleIndex)
            Else
                XtraMessageBox.Show(ulf, "<size=10>Connection Failed</size>", "Failed")
                Me.Cursor = Cursors.Default
                Exit Sub
            End If
        ElseIf DeviceType = "ATF686n" Then
            vnMachineNumber = ID_NO.ToString  '1
            vpszIPAddress = IP.ToString '"192.168.0.111"
            vpszNetPort = 5005
            vpszNetPassword = 0
            vnTimeOut = 5000
            vnProtocolType = PROTOCOL_TCPIP
            vnLicense = 7881
            Dim gnCommHandleIndex As Long
            Dim atf686n As mdlFunction_Atf686n = New mdlFunction_Atf686n
            If A_R = "USB" Then
                gnCommHandleIndex = atf686n.ST_ConnectUSB(vnMachineNumber, vnLicense)
            Else
                gnCommHandleIndex = atf686n.ConnectNet(vpszIPAddress)
            End If
            If gnCommHandleIndex > 0 Then
                'Dim vstrData As String = New String(CType(ChrW(32), Char), 256)
                'Dim ret = atf686n.ST_GetDeviceInfo(gnCommHandleIndex, CInt(enumGetDeviceInfo.DI_MACHINENUM), vstrData)
                'TextSrNo.Text = vstrData
                atf686n.ST_DisConnect(gnCommHandleIndex)
            Else
                XtraMessageBox.Show(ulf, "<size=10>Connection Failed</size>", "Failed")
                Me.Cursor = Cursors.Default
                Exit Sub
            End If
        ElseIf DeviceType = "TF-01" Then
            Dim bRet As Boolean
            vnMachineNumber = ID_NO.ToString  '1
            vpszIPAddress = IP.ToString '"192.168.0.111"
            vpszNetPort = 5005
            vpszNetPassword = 0
            vnTimeOut = 5000
            vnProtocolType = PROTOCOL_TCPIP
            vnLicense = 7881
            Dim nPort As Integer = vpszNetPort
            Dim nPassword As Integer = vpszNetPassword
            Dim strIP As String = vpszIPAddress.ToString()

            Me.AxFP_CLOCK1.OpenCommPort(vnMachineNumber)

            bRet = AxFP_CLOCK1.SetIPAddress(strIP, nPort, nPassword)
            If Not bRet Then
                XtraMessageBox.Show(ulf, "<size=10>Connection Failed</size>", "Failed")
                Me.Cursor = Cursors.Default
                Exit Sub
            Else
                bRet = AxFP_CLOCK1.OpenCommPort(vnMachineNumber)
                bRet = DisableDevice(vnMachineNumber)

                If bRet Then
                    Dim str As String = ""
                    bRet = AxFP_CLOCK1.GetSerialNumber(vnMachineNumber, str)
                    If bRet Then
                        TextSrNo.Text = str
                    End If
                End If
            End If
            AxFP_CLOCK1.CloseCommPort()
        ElseIf DeviceType = "Ultra800" Then
            vnMachineNumber = ID_NO.ToString  '1
            vpszIPAddress = IP.ToString
            Try
                Dim link As String = "http://" & vpszIPAddress & ":8090/getDeviceKey"

                Dim proxy As WebClient = New WebClient
                Dim serviceURL As String = String.Format(link)
                Dim data() As Byte = proxy.DownloadData(serviceURL)
                Dim stream As Stream = New MemoryStream(data)
                Dim OutPut As String = System.Text.Encoding.UTF8.GetString(data)
                Dim UltraClass As UltraDeviceSerial = New UltraDeviceSerial()
                UltraClass = JsonConvert.DeserializeObject(Of UltraDeviceSerial)(OutPut)
                If UltraClass.success = True Then
                    Dim serialNo As String = UltraClass.data
                    TextSrNo.Text = serialNo
                Else
                    'TextSrNo.Text = ""
                    XtraMessageBox.Show(ulf, "<size=10>Connection Failed</size>", "Failed")
                    Me.Cursor = Cursors.Default
                    Exit Sub
                End If


#Region "Set Callback ip and port"

                'link = "http://192.168.1.122:8090/setIdentifyCallBack?pass=tipl9910&callbackurl=http://192.168.1.16:8888/lan/IdentifyCallBack"
                link = "http://" & vpszIPAddress & ":8090/setIdentifyCallBack?pass=" & TextEditComm.Text.Trim & "&callbackurl=http://" & Common.SystemIp & ":" & Common.Ultra800Port & "/lan/IdentifyCallBack&base64Enable=2"
                proxy = New WebClient
                Dim httpWebRequest As WebRequest = WebRequest.Create(link) 'Live
                httpWebRequest.ContentType = "application/json"
                httpWebRequest.Method = "POST"
                Dim httpResponse = CType(httpWebRequest.GetResponse(), HttpWebResponse)
                Using streamReader = New StreamReader(httpResponse.GetResponseStream())
                    'Dim streamReader = New StreamReader(httpResponse.GetResponseStream)
                    Dim result = streamReader.ReadToEnd()
                    Dim fs As FileStream = New FileStream("JsonResponse.txt", FileMode.OpenOrCreate, FileAccess.Write)
                    Dim sw As StreamWriter = New StreamWriter(fs)
                    sw.BaseStream.Seek(0, SeekOrigin.End)
                    sw.WriteLine(vbCrLf & link & ", " & result & "::" & DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"))
                    sw.Flush()
                    sw.Close()
                End Using


                link = "http://" & vpszIPAddress & ":8090/setDeviceHeartBeat?pass=" & TextEditComm.Text.Trim & "&url=http://" & Common.SystemIp & ":" & Common.Ultra800Port & "/lan/heartBeatCallback&Interval=6"
                proxy = New WebClient
                httpWebRequest = WebRequest.Create(link) 'Live
                httpWebRequest.ContentType = "application/json"
                httpWebRequest.Method = "POST"
                httpResponse = CType(httpWebRequest.GetResponse(), HttpWebResponse)
                Using streamReader = New StreamReader(httpResponse.GetResponseStream())
                    'Dim streamReader = New StreamReader(httpResponse.GetResponseStream)
                    Dim result = streamReader.ReadToEnd()
                    Dim fs As FileStream = New FileStream("JsonResponse.txt", FileMode.OpenOrCreate, FileAccess.Write)
                    Dim sw As StreamWriter = New StreamWriter(fs)
                    sw.BaseStream.Seek(0, SeekOrigin.End)
                    sw.WriteLine(vbCrLf & link & ", " & result & "::" & DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"))
                    sw.Flush()
                    sw.Close()
                End Using
#End Region
#Region "SetFtpUsernamPassword"
                link = "http://" & vpszIPAddress & ":8090/setFTP?pass=" & TextEditComm.Text.Trim & "&username=admin&password=" & TextEditComm.Text.Trim
                httpWebRequest = WebRequest.Create(link) 'Live
                httpWebRequest.ContentType = "application/json"
                httpWebRequest.Method = "POST"
                httpResponse = CType(httpWebRequest.GetResponse(), HttpWebResponse)
                Using streamReader = New StreamReader(httpResponse.GetResponseStream())
                    'Dim streamReader = New StreamReader(httpResponse.GetResponseStream)
                    Dim result = streamReader.ReadToEnd()
                    Dim fs As FileStream = New FileStream("JsonResponse.txt", FileMode.OpenOrCreate, FileAccess.Write)
                    Dim sw As StreamWriter = New StreamWriter(fs)
                    sw.BaseStream.Seek(0, SeekOrigin.End)
                    sw.WriteLine(vbCrLf & link & ", " & result & "::" & DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"))
                    sw.Flush()
                    sw.Close()
                End Using
#End Region

            Catch ex As Exception
                XtraMessageBox.Show(ulf, "<size=10>Connection Failed</size>", "Failed")
                Me.Cursor = Cursors.Default
                Exit Sub
            End Try
        End If
        Me.Cursor = Cursors.Default
    End Sub
    Public Class UltraDeviceSerial
        Public Property code As String
        Public Property data As String
        Public Property msg As String
        Public Property result As Integer
        Public Property success As Boolean
    End Class
    Private Function DisableDevice(vnMachineNumber) As Boolean

        Dim bRet As Boolean = AxFP_CLOCK1.EnableDevice(vnMachineNumber, 0)
        If bRet Then
            Return True
        Else
            Return False
        End If
    End Function
    Private Sub ComboBoxEdit3_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBoxEdit3.SelectedIndexChanged
        If ComboBoxEdit3.SelectedIndex = 0 Or ComboBoxEdit3.SelectedIndex = 2 Or ComboBoxEdit3.SelectedIndex = 4 Or ComboBoxEdit3.SelectedIndex = 5 Or ComboBoxEdit3.SelectedIndex = 10 Then
            TextEditComm.Visible = True
            LabelControl9.Visible = True
        Else
            TextEditComm.Visible = False
            LabelControl9.Visible = False
        End If
        If ComboBoxEdit3.SelectedIndex = 5 And ComboBoxEdit2.SelectedIndex = 3 Then
            ComboBoxEdit2.SelectedIndex = 2
        End If
        If ComboBoxEdit3.SelectedIndex = 0 Or ComboBoxEdit3.SelectedIndex = 1 Or ComboBoxEdit3.SelectedIndex = 2 Or ComboBoxEdit3.SelectedIndex = 10 Then
            'BtnVlidate.Visible = True
            PanelControl1.Visible = True
            PanelControl2.Visible = True
        Else
            'BtnVlidate.Visible = False
            PanelControl1.Visible = False
            PanelControl2.Visible = False
        End If
        If ComboBoxEdit3.SelectedIndex = 8 Then
            SimpleButtonGetSr.Visible = False
        Else
            SimpleButtonGetSr.Visible = True
        End If
    End Sub
    Private Sub ComboBoxEdit2_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBoxEdit2.SelectedIndexChanged
        If ComboBoxEdit3.SelectedIndex = 5 And ComboBoxEdit2.SelectedIndex = 3 Then
            ComboBoxEdit2.SelectedIndex = 2
        End If
    End Sub
    Private Sub TextSrNo_EditValueChanged(sender As System.Object, e As System.EventArgs) Handles TextSrNo.EditValueChanged
        If TextSrNo.Text.Trim <> "" Then
            BtnVlidate.Enabled = True
            BtnDeVlidate.Enabled = True
        End If
    End Sub
    Private Sub BtnVlidate_Click(sender As System.Object, e As System.EventArgs) Handles BtnVlidate.Click
        Try
            If ComboBoxEdit3.SelectedIndex = 0 Or ComboBoxEdit3.SelectedIndex = 1 Or ComboBoxEdit3.SelectedIndex = 2 Or ComboBoxEdit3.SelectedIndex = 10 Then
                Dim DeviceType As String
                If ComboBoxEdit3.SelectedIndex = 0 Or ComboBoxEdit3.SelectedIndex = 2 Then
                    DeviceType = "ZK"
                ElseIf ComboBoxEdit3.SelectedIndex = 1 Then
                    DeviceType = "Bio"
                Else
                    DeviceType = "All"
                End If

                Dim proxy As WebClient = New WebClient
                Dim serviceURL As String = String.Format("http://137.59.201.60:5011/Service1.svc/GetValidSerial/" & TextSrNo.Text.Trim(vbNullChar) & "," & DeviceType & "")
                Dim data() As Byte = proxy.DownloadData(serviceURL)
                Dim stream As Stream = New MemoryStream(data)
                Dim OutPut As String = System.Text.Encoding.UTF8.GetString(data)
                If OutPut.Contains("true") Then
                    DeviceValid = True
                    XtraMessageBox.Show(ulf, "<size=10>Serial Number Activated</size>", "<size=9>Ultra</size>")
                Else
                    DeviceValid = False
                    XtraMessageBox.Show(ulf, "<size=10>Invalid Serial Number</size>", "<size=9>Ultra</size>")
                End If

                '#Region "ActivateDevice"
                '                Dim proxy As WebClient = New WebClient
                '                Dim serviceURL As String = String.Format("http://137.59.201.60:5011/Service1.svc/ActivateSerial/" & TextSrNo.Text.Trim(vbNullChar) & "," & DeviceType & "," & license.HDDSerialNo)
                '                Dim data() As Byte = proxy.DownloadData(serviceURL)
                '                Dim stream As Stream = New MemoryStream(data)
                '                Dim OutPut As String = System.Text.Encoding.UTF8.GetString(data)
                '                Dim DApi As ActivateSerialResultClass = New ActivateSerialResultClass()
                '                DApi = JsonConvert.DeserializeObject(Of ActivateSerialResultClass)(OutPut)

                '                If DApi.ActivateSerialResult.Result = True Then
                '                    DeviceValid = True
                '                    DeviceDeactivate = False
                '                    XtraMessageBox.Show(ulf, "<size=10>Serial Number Activated</size>", "<size=9>Ultra</size>")
                '                Else
                '                    DeviceValid = False
                '                    DeviceDeactivate = False
                '                    XtraMessageBox.Show(ulf, "<size=10>" & DApi.ActivateSerialResult.ResultMsg & "</size>", "<size=9>Ultra</size>")
                '                End If
                '#End Region

            End If
        Catch ex As Exception
            XtraMessageBox.Show(ulf, "<size=10>" & ex.Message.Trim & "</size>", "<size=9>iAS</size>")
        End Try

    End Sub

    Private Sub SimpleButtonDownload_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonDownload.Click
        If TextSrNo.Text = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Invalid Serial Number</size>", "<size=9>iAS</size>")
            TextSrNo.Select()
            Exit Sub
        End If
        Try
            If System.IO.File.Exists("Device.lic") Then
                My.Computer.FileSystem.DeleteFile("Device.lic")
            End If
            'File.Create("UserKey.lic").Dispose()
            Dim fs As FileStream = New FileStream("Device.lic", FileMode.Create, FileAccess.Write)
            Dim sw As StreamWriter = New StreamWriter(fs)
            Dim str As String = ""
            sw.WriteLine(EncyDcry.Encrypt(license.HDDSerialNo, True))
            sw.Write(TextSrNo.Text.Trim)
            sw.Flush()
            sw.Close()
            fs.Close()

            Dim client As New Net.WebClient
            Dim sfd As New SaveFileDialog
            sfd.FileName = "Device.lic"
            sfd.ShowDialog()
            client.DownloadFile("Device.lic", sfd.FileName)
            Try
                My.Computer.FileSystem.DeleteFile("Device.lic")
            Catch ex As Exception
            End Try
        Catch ex As Exception
            XtraMessageBox.Show(ulf, "<size=10>" & ex.Message.Trim & "</size>", "<size=9>iAS</size>")
        End Try
    End Sub

    Private Sub SimpleButtonUpload_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonUpload.Click
        If TextEditUpload.Text.Trim <> "" Then
            Dim client As New Net.WebClient
            client.DownloadFile(TextEditUpload.Text, "Device.lic")
            Dim fs As FileStream = New FileStream("Device.lic", FileMode.Open, FileAccess.Read)
            Dim sr As StreamReader = New StreamReader(fs)
            Dim str() As String
            'Dim str1 As String
            Dim list As New List(Of String)()
            While (sr.Peek <> -1)
                list.Add(sr.ReadLine)
            End While
            sr.Close()
            fs.Close()
            str = list.ToArray
            If str.Length < 2 Then
                XtraMessageBox.Show(ulf, "<size=10>Incorrect file</size>", "<size=9>iAS</size>")
                TextEditUpload.Select()
                Exit Sub
            Else
                If EncyDcry.Decrypt(str(0), True) <> license.HDDSerialNo Then
                    XtraMessageBox.Show(ulf, "<size=10>Incorrect User Key</size>", "<size=9>iAS</size>")
                    TextEditUpload.Select()
                    Exit Sub
                End If
                Try
                    TextSrNo.Text = EncyDcry.Decrypt(str(1), True)
                    DeviceValid = True
                Catch ex As Exception
                    XtraMessageBox.Show(ulf, "<size=10>Incorrect file</size>", "<size=9>iAS</size>")
                    TextEditUpload.Select()
                    Exit Sub
                End Try

            End If
        End If
    End Sub

    Private Sub TextEditUpload_Click(sender As System.Object, e As System.EventArgs) Handles TextEditUpload.Click
        Dim dlg As New OpenFileDialog()
        ' Filter by Office Files
        dlg.Filter = "Office Files|*.lic;"
        dlg.ShowDialog()
        TextEditUpload.Text = dlg.FileName
    End Sub

    Private Sub BtnDeVlidate_Click(sender As Object, e As EventArgs) Handles BtnDeVlidate.Click
        Try
            If ComboBoxEdit3.SelectedIndex = 0 Or ComboBoxEdit3.SelectedIndex = 1 Or ComboBoxEdit3.SelectedIndex = 2 Or ComboBoxEdit3.SelectedIndex = 10 Then
                Dim DeviceType As String
                If ComboBoxEdit3.SelectedIndex = 0 Or ComboBoxEdit3.SelectedIndex = 2 Then
                    DeviceType = "ZK"
                ElseIf ComboBoxEdit3.SelectedIndex = 1 Then
                    DeviceType = "Bio"
                Else
                    DeviceType = "All"
                End If

                'Dim proxy As WebClient = New WebClient
                'Dim serviceURL As String = String.Format("http://137.59.201.60:5011/Service1.svc/GetValidSerial/" & TextSrNo.Text.Trim(vbNullChar) & "," & DeviceType & "")
                'Dim data() As Byte = proxy.DownloadData(serviceURL)
                'Dim stream As Stream = New MemoryStream(data)
                'Dim OutPut As String = System.Text.Encoding.UTF8.GetString(data)
                'If OutPut.Contains("true") Then
                '    DeviceValid = True
                '    XtraMessageBox.Show(ulf, "<size=10>Serial Number Activated</size>", "<size=9>Ultra</size>")
                'Else
                '    DeviceValid = False
                '    XtraMessageBox.Show(ulf, "<size=10>Invalid Serial Number</size>", "<size=9>Ultra</size>")
                'End If

#Region "ActivateDevice"
                Dim proxy As WebClient = New WebClient
                Dim serviceURL As String = String.Format("http://137.59.201.60:5011/Service1.svc/DeActivateSerial/" & TextSrNo.Text.Trim(vbNullChar) & "," & DeviceType & "," & license.HDDSerialNo)
                Dim data() As Byte = proxy.DownloadData(serviceURL)
                Dim stream As Stream = New MemoryStream(data)
                Dim OutPut As String = System.Text.Encoding.UTF8.GetString(data)
                Dim DApi As DeActivateSerialResultClass = New DeActivateSerialResultClass()
                DApi = JsonConvert.DeserializeObject(Of DeActivateSerialResultClass)(OutPut)

                If DApi.DeActivateSerialResult.Result = True Then
                    DeviceValid = True
                    DeviceDeactivate = True
                    XtraMessageBox.Show(ulf, "<size=10>Serial Number Deactivated</size>", "<size=9>Ultra</size>")
                Else
                    DeviceValid = False
                    DeviceDeactivate = False
                    XtraMessageBox.Show(ulf, "<size=10>" & DApi.DeActivateSerialResult.ResultMsg & "</size>", "<size=9>Ultra</size>")
                End If
#End Region

            End If
        Catch ex As Exception
            XtraMessageBox.Show(ulf, "<size=10>" & ex.Message.Trim & "</size>", "<size=9>iAS</size>")
        End Try
    End Sub
End Class