﻿Imports System.Resources
Imports System.Globalization
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports System.ComponentModel
Imports DevExpress.XtraGrid.Columns
Imports System.Data.SqlClient
Imports System.Text
Imports System.Data.OleDb
Imports System.Drawing.Imaging

Imports libzkfpcsharp
Imports System.Runtime.InteropServices
Imports System.Threading
Imports Sample
Imports DevExpress.XtraLayout.Customization
Imports iAS.AscDemo
Imports Newtonsoft.Json


Public Class XtraEmployeeEdit
    Dim EpId As String
    Dim ulf As UserLookAndFeel
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    Dim adap, adap1, adap2, adap3 As SqlDataAdapter
    Dim adapA, adapA1, adapA2 As OleDbDataAdapter
    Dim ds, ds1 As DataSet
    Dim ds2 As DataSet
    Dim ds3 As DataSet
    Public Shared UserNoForTemplate As String
    Public Shared NameForTemplate As String

    'for finger print
    Dim IsRegister As Boolean = False
    Dim RegisterCount As Integer = 0
    Dim cbRegTmp As Integer = 0
    Dim mDBHandle As IntPtr = IntPtr.Zero
    Dim mDevHandle As IntPtr = IntPtr.Zero
    Dim FormHandle As IntPtr = IntPtr.Zero
    Dim iFid As Integer = 1
    Dim RegTmps()() As Byte = New Byte(3 - 1)() {}
    Dim mfpWidth As Integer = 0
    Dim mfpHeight As Integer = 0
    Dim FPBuffer() As Byte
    Dim bIsTimeToDie As Boolean = False
    Dim cbCapTmp As Integer = 2048
    Dim CapTmp() As Byte = New Byte(2048 - 1) {}
    Dim RegTmp() As Byte = New Byte(2048 - 1) {}
    Dim REGISTER_FINGER_COUNT As Integer = 3
    Dim bIdentify As Boolean = True

    Dim MESSAGE_CAPTURED_OK As Integer = (1024 + 6)
    Dim strBase64 As String

    Public Shared Vimage As Image
    Public Shared VName As String
    Public Shared VCom As String
    Public Shared VToMeet As String
    Public Shared VDept As String
    Public Shared VDate As String
    Public Shared VTime As String
    Public Shared VSrNo As String
    Dim captureThread As Thread
    Dim isActive As String
    Dim DOL As String

    'for HK devices
    Dim m_lGetFaceCfgHandle As Integer = -1
    Dim m_lSetFaceCfgHandle As Integer = -1
    Dim m_lCapFaceCfgHandle As Integer = -1

    Dim m_UserID As Integer = -1
    Dim m_lGetUserCfgHandle As Int32 = -1
    Dim m_lSetUserCfgHandle As Int32 = -1
    Dim m_lDelUserCfgHandle As Int32 = -1

    Dim m_lSetCardCfgHandle As Int32 = -1
    Public Sub New()
        InitializeComponent()
        AllgridlookupLoad()
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True

        If Common.IsNepali = "Y" Then
            ComboBoxEditNepaliYear.Visible = True
            ComboBoxEditNEpaliMonth.Visible = True
            ComboBoxEditNepaliDate.Visible = True
            DateEditLeaving.Visible = False
            DateEditJoin.Visible = False
            DateEditBirth.Visible = False
            ComboBoxEditNepaliYearDOB.Visible = True
            ComboBoxEditNEpaliMonthDOB.Visible = True
            ComboBoxEditNepaliDateDOB.Visible = True

            ComboBoxEditNepaliYearLeave.Visible = True
            ComboBoxEditNEpaliMonthLeave.Visible = True
            ComboBoxEditNepaliDateLeave.Visible = True
        Else
            ComboBoxEditNepaliYear.Visible = False
            ComboBoxEditNEpaliMonth.Visible = False
            ComboBoxEditNepaliDate.Visible = False
            DateEditLeaving.Visible = True
            DateEditJoin.Visible = True
            DateEditBirth.Visible = True
            ComboBoxEditNepaliYearDOB.Visible = False
            ComboBoxEditNEpaliMonthDOB.Visible = False
            ComboBoxEditNepaliDateDOB.Visible = False
            ComboBoxEditNepaliYearLeave.Visible = False
            ComboBoxEditNEpaliMonthLeave.Visible = False
            ComboBoxEditNepaliDateLeave.Visible = False
        End If
        Common.SetGridFont(GridViewMachine, New Font("Tahoma", 10))
        Common.SetGridFont(GridViewZKDevice, New Font("Tahoma", 10))
    End Sub
    Private Sub AllgridlookupLoad()
        If Common.servername = "Access" Then
            'Me.TblCompany1TableAdapter1.Fill(Me.SSSDBDataSet.tblCompany1)
            'GridLookUpEdit1.Properties.DataSource = SSSDBDataSet.tblCompany1

            Me.TblDepartment1TableAdapter1.Fill(Me.SSSDBDataSet.tblDepartment1)
            GridLookUpEdit2.Properties.DataSource = SSSDBDataSet.tblDepartment1

            Me.TblCatagory1TableAdapter1.Fill(Me.SSSDBDataSet.tblCatagory1)
            GridLookUpEdit3.Properties.DataSource = SSSDBDataSet.tblCatagory1

            Me.EmployeeGroup1TableAdapter1.Fill(Me.SSSDBDataSet.EmployeeGroup1)
            GridLookUpEdit4.Properties.DataSource = SSSDBDataSet.EmployeeGroup1

            Me.TblGrade1TableAdapter1.Fill(Me.SSSDBDataSet.tblGrade1)
            GridLookUpEdit5.Properties.DataSource = SSSDBDataSet.tblGrade1

            Me.TblDESPANSARY1TableAdapter1.Fill(Me.SSSDBDataSet.tblDESPANSARY1)
            GridLookUpEdit6.Properties.DataSource = SSSDBDataSet.tblDESPANSARY1

            Me.TblBank1TableAdapter1.Fill(Me.SSSDBDataSet.tblBank1)
            GridLookUpEdit7.Properties.DataSource = SSSDBDataSet.tblBank1

            'Me.Tblbranch1TableAdapter1.Fill(Me.SSSDBDataSet.tblbranch1)
            'GridLookUpEdit8.Properties.DataSource = SSSDBDataSet.tblbranch1

            'Me.TblMachine1TableAdapter1.Fill(Me.SSSDBDataSet.tblMachine1)
            'GridControlMachine.DataSource = SSSDBDataSet.tblMachine1
            'GridControlZKDevice.DataSource = SSSDBDataSet.tblMachine1
        Else
            'TblCompanyTableAdapter.Connection.ConnectionString = Common.ConnectionString '"Data Source=DESKTOP-PG6NDV8;Initial Catalog=SSSDB;Integrated Security=True"
            'Me.TblCompanyTableAdapter.Fill(Me.SSSDBDataSet.tblCompany)
            'GridLookUpEdit1.Properties.DataSource = SSSDBDataSet.tblCompany

            TblDepartmentTableAdapter.Connection.ConnectionString = Common.ConnectionString
            Me.TblDepartmentTableAdapter.Fill(Me.SSSDBDataSet.tblDepartment)
            GridLookUpEdit2.Properties.DataSource = SSSDBDataSet.tblDepartment

            TblCatagoryTableAdapter.Connection.ConnectionString = Common.ConnectionString
            Me.TblCatagoryTableAdapter.Fill(Me.SSSDBDataSet.tblCatagory)
            GridLookUpEdit3.Properties.DataSource = SSSDBDataSet.tblCatagory

            EmployeeGroupTableAdapter.Connection.ConnectionString = Common.ConnectionString
            Me.EmployeeGroupTableAdapter.Fill(Me.SSSDBDataSet.EmployeeGroup)
            GridLookUpEdit4.Properties.DataSource = SSSDBDataSet.EmployeeGroup

            TblGradeTableAdapter.Connection.ConnectionString = Common.ConnectionString
            Me.TblGradeTableAdapter.Fill(Me.SSSDBDataSet.tblGrade)
            GridLookUpEdit5.Properties.DataSource = SSSDBDataSet.tblGrade

            TblDESPANSARYTableAdapter.Connection.ConnectionString = Common.ConnectionString
            Me.TblDESPANSARYTableAdapter.Fill(Me.SSSDBDataSet.tblDESPANSARY)
            GridLookUpEdit6.Properties.DataSource = SSSDBDataSet.tblDESPANSARY

            TblBankTableAdapter.Connection.ConnectionString = Common.ConnectionString
            Me.TblBankTableAdapter.Fill(Me.SSSDBDataSet.tblBank)
            GridLookUpEdit7.Properties.DataSource = SSSDBDataSet.tblBank

            'TblbranchTableAdapter.Connection.ConnectionString = Common.ConnectionString
            'Me.TblbranchTableAdapter.Fill(Me.SSSDBDataSet.tblbranch)
            'GridLookUpEdit8.Properties.DataSource = SSSDBDataSet.tblbranch

            TblMachineTableAdapter.Connection.ConnectionString = Common.ConnectionString
            'Me.TblMachineTableAdapter.Fill(Me.SSSDBDataSet.tblMachine)
            'GridControlMachine.DataSource = SSSDBDataSet.tblMachine
            'GridControlZKDevice.DataSource = SSSDBDataSet.tblMachine
        End If

        GridLookUpEdit8.Properties.DataSource = Common.LocationNonAdmin
        GridLookUpEdit1.Properties.DataSource = Common.CompanyNonAdmin

        GridControlMachine.DataSource = Common.MachineNonAdmin
        GridControlZKDevice.DataSource = Common.MachineNonAdmin

        If Common.USERTYPE <> "A" Then
            Dim emp() As String = Common.Auth_Branch.Split(",")
            Dim ls As New List(Of String)()
            For x As Integer = 0 To emp.Length - 1
                ls.Add(emp(x).Trim)
            Next
            Dim locatuontbl As DataTable = New DataTable("tblbranch")
            Dim gridselet As String = "select * from tblbranch where BRANCHCODE IN ('" & String.Join("', '", ls.ToArray()) & "')"
            If Common.servername = "Access" Then
                Dim dataAdapter As New OleDbDataAdapter(gridselet, Common.con1)
                dataAdapter.Fill(locatuontbl)
            Else
                Dim dataAdapter As New SqlClient.SqlDataAdapter(gridselet, Common.con)
                dataAdapter.Fill(locatuontbl)
            End If
            GridLookUpEdit8.Properties.DataSource = Common.LocationNonAdmin
            GridViewMachine.ClearSelection()
        End If
    End Sub
    Private Sub setDeviceGridZK()
        Dim gridtblregisterselet As String
        If Common.USERTYPE = "A" Then
            gridtblregisterselet = "select * from tblMachine where DeviceType = 'ZK(TFT)' or DeviceType='Bio-1Pro/ATF305Pro/ATF686Pro' "
        Else
            'gridtblregisterselet = "select * from tblMachine where DeviceType = 'ZK(TFT)' or DeviceType='Bio-1Pro/ATF305Pro/ATF686Pro' "
            Dim emp() As String = Common.Auth_Branch.Split(",")
            Dim ls As New List(Of String)()
            For x As Integer = 0 To emp.Length - 1
                ls.Add(emp(x).Trim)
            Next
            gridtblregisterselet = "select tblMachine.ID_NO, tblMachine.A_R, tblMachine.IN_OUT,tblMachine.DeviceType,tblMachine.LOCATION,tblMachine.branch,tblMachine.commkey,tblMachine.MAC_ADDRESS,tblMachine.Purpose,tblMachine.LastModifiedBy,tblMachine.LastModifiedDate from tblMachine, tblbranch where tblMachine.branch=tblbranch.BRANCHNAME and tblbranch.BRANCHCODE in ('" & String.Join("', '", ls.ToArray()) & "') and DeviceType = 'ZK(TFT)' or DeviceType='Bio-1Pro/ATF305Pro/ATF686Pro' "
        End If
        Dim WTDataTable As DataTable
        If Common.servername = "Access" Then
            Dim dataAdapter As New OleDbDataAdapter(gridtblregisterselet, Common.con1)
            WTDataTable = New DataTable("tblMachine")
            dataAdapter.Fill(WTDataTable)
        Else
            Dim dataAdapter As New SqlClient.SqlDataAdapter(gridtblregisterselet, Common.con)
            WTDataTable = New DataTable("tblMachine")
            dataAdapter.Fill(WTDataTable)
        End If
        'GridControlZKDevice.DataSource = WTDataTable
        GridControlMachine.DataSource = WTDataTable
    End Sub
    Private Sub setDeviceGridUltra()
        Dim gridtblregisterselet As String
        If Common.USERTYPE = "A" Then
            gridtblregisterselet = "select * from tblMachine where DeviceType = 'HTSeries'  "
        Else
            'gridtblregisterselet = "select * from tblMachine where DeviceType = 'ZK(TFT)' or DeviceType='Bio-1Pro/ATF305Pro/ATF686Pro' "
            Dim emp() As String = Common.Auth_Branch.Split(",")
            Dim ls As New List(Of String)()
            For x As Integer = 0 To emp.Length - 1
                ls.Add(emp(x).Trim)
            Next
            gridtblregisterselet = "select tblMachine.ID_NO, tblMachine.A_R, tblMachine.IN_OUT,tblMachine.DeviceType,tblMachine.LOCATION,tblMachine.branch,tblMachine.commkey,tblMachine.MAC_ADDRESS,tblMachine.Purpose,tblMachine.LastModifiedBy,tblMachine.LastModifiedDate from tblMachine, tblbranch where tblMachine.branch=tblbranch.BRANCHNAME and tblbranch.BRANCHCODE in ('" & String.Join("', '", ls.ToArray()) & "') and DeviceType = 'HTSeries' "
        End If
        Dim WTDataTable As DataTable
        If Common.servername = "Access" Then
            Dim dataAdapter As New OleDbDataAdapter(gridtblregisterselet, Common.con1)
            WTDataTable = New DataTable("tblMachine")
            dataAdapter.Fill(WTDataTable)
        Else
            Dim dataAdapter As New SqlClient.SqlDataAdapter(gridtblregisterselet, Common.con)
            WTDataTable = New DataTable("tblMachine")
            dataAdapter.Fill(WTDataTable)
        End If
        'GridControlZKDevice.DataSource = WTDataTable
        GridControlMachine.DataSource = WTDataTable
    End Sub
    Private Sub XtraEmployeeEdit_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        TabPane1.SelectedPage = TabNavigationPage1
        picFPImg.Image = Nothing
        FormHandle = Me.Handle  'used for finger print
        GroupControlFPscanner.Enabled = False
        'TextEditCard.Enabled = False
        TextEditCard.Text = ""
        CheckValidity.Checked = False

        Try
            zkfp2.Terminate()
        Catch ex As Exception

        End Try
        TextEditImage.Text = ""
        SimpleButtonInit.Enabled = True
        SimpleButtonStop.Enabled = False
        AllgridlookupLoad()
        'setDeviceGridZK()
        Dim xe As XtraEmployee = New XtraEmployee
        EpId = XtraEmployee.EmpId

        If ToggleSwitch1.IsOn Then
            DateEditLeaving.Enabled = False
            TextEdit6.Enabled = False
        ElseIf ToggleSwitch1.IsOn = False Then
            DateEditLeaving.Enabled = True
            TextEdit6.Enabled = True
        End If
        'setDeviceGrid()
        If Common.IsNepali = "Y" Then
            ComboBoxEditNepaliYear.Visible = True
            ComboBoxEditNEpaliMonth.Visible = True
            ComboBoxEditNepaliDate.Visible = True
            DateEditJoin.Visible = False
            DateEditBirth.Visible = False
            DateEditVStrt.Visible = False
            DateEditVEnd.Visible = False
            ComboBoxEditNepaliYearDOB.Visible = True
            ComboBoxEditNEpaliMonthDOB.Visible = True
            ComboBoxEditNepaliDateDOB.Visible = True

            ComboBoxEditNepaliYearLeave.Visible = True
            ComboBoxEditNEpaliMonthLeave.Visible = True
            ComboBoxEditNepaliDateLeave.Visible = True

            ComboNepaliVStartDate.Visible = True
            ComboNepaliVStartMonth.Visible = True
            ComboNepaliVStartYear.Visible = True

            ComboNepaliVEndDate.Visible = True
            ComboNepaliVEndMonth.Visible = True
            ComboNepaliVEndYear.Visible = True

            Dim DC As New DateConverter()
            Dim doj As String = DC.ToBS(New Date(Now.Year, Now.Month, 1))
            Dim dojTmp() As String = doj.Split("-")
            ComboBoxEditNepaliYear.EditValue = dojTmp(0)
            ComboBoxEditNEpaliMonth.SelectedIndex = dojTmp(1) - 1
            ComboBoxEditNepaliDate.EditValue = dojTmp(2)


            Dim Vstart As String = DC.ToBS(New Date(DateEditVStrt.DateTime.Year, DateEditVStrt.DateTime.Month, DateEditVStrt.DateTime.Day))
            dojTmp = Vstart.Split("-")
            ComboNepaliVStartYear.EditValue = dojTmp(0)
            ComboNepaliVStartMonth.SelectedIndex = dojTmp(1) - 1
            ComboNepaliVStartDate.EditValue = dojTmp(2)

            Dim VEnd As String = DC.ToBS(New Date(DateEditVEnd.DateTime.Year, DateEditVEnd.DateTime.Month, DateEditVEnd.DateTime.Day))
            dojTmp = VEnd.Split("-")
            ComboNepaliVEndYear.EditValue = dojTmp(0)
            ComboNepaliVEndMonth.SelectedIndex = dojTmp(1) - 1
            ComboNepaliVEndDate.EditValue = dojTmp(2)

        Else
            ComboBoxEditNepaliYear.Visible = False
            ComboBoxEditNEpaliMonth.Visible = False
            ComboBoxEditNepaliDate.Visible = False
            DateEditLeaving.Visible = True
            DateEditJoin.Visible = True
            DateEditBirth.Visible = True
            DateEditVStrt.Visible = True
            DateEditVEnd.Visible = True
            ComboBoxEditNepaliYearDOB.Visible = False
            ComboBoxEditNEpaliMonthDOB.Visible = False
            ComboBoxEditNepaliDateDOB.Visible = False
            ComboBoxEditNepaliYearLeave.Visible = False
            ComboBoxEditNEpaliMonthLeave.Visible = False
            ComboBoxEditNepaliDateLeave.Visible = False

            ComboNepaliVStartDate.Visible = False
            ComboNepaliVStartMonth.Visible = False
            ComboNepaliVStartYear.Visible = False

            ComboNepaliVEndDate.Visible = False
            ComboNepaliVEndMonth.Visible = False
            ComboNepaliVEndYear.Visible = False
        End If

        If EpId.Length = 0 Then
            SetDefaultValue()
        Else
            SetFormValue()
        End If
        If (Not System.IO.Directory.Exists(My.Application.Info.DirectoryPath & "\EmpImages")) Then
            System.IO.Directory.CreateDirectory(My.Application.Info.DirectoryPath & "\EmpImages")
        End If
        GridViewMachine.ClearSelection()
    End Sub
    Private Sub SetDefaultValue()
        If Common.IsNepali = "Y" Then
            ComboBoxEditNepaliYear.Visible = True
            ComboBoxEditNEpaliMonth.Visible = True
            ComboBoxEditNepaliDate.Visible = True
            DateEditJoin.Visible = False
            DateEditBirth.Visible = False
            DateEditVStrt.Visible = False
            DateEditVEnd.Visible = False
            ComboBoxEditNepaliYearDOB.Visible = True
            ComboBoxEditNEpaliMonthDOB.Visible = True
            ComboBoxEditNepaliDateDOB.Visible = True

            ComboBoxEditNepaliYearLeave.Visible = True
            ComboBoxEditNEpaliMonthLeave.Visible = True
            ComboBoxEditNepaliDateLeave.Visible = True

            ComboNepaliVStartDate.Visible = True
            ComboNepaliVStartMonth.Visible = True
            ComboNepaliVStartYear.Visible = True

            ComboNepaliVEndDate.Visible = True
            ComboNepaliVEndMonth.Visible = True
            ComboNepaliVEndYear.Visible = True

            Dim DC As New DateConverter()
            Dim doj As String = DC.ToBS(New Date(Now.Year, Now.Month, 1))
            Dim dojTmp() As String = doj.Split("-")
            ComboBoxEditNepaliYear.EditValue = dojTmp(0)
            ComboBoxEditNEpaliMonth.SelectedIndex = dojTmp(1) - 1
            ComboBoxEditNepaliDate.EditValue = dojTmp(2)


            Dim Vstart As String = DC.ToBS(New Date(DateEditVStrt.DateTime.Year, DateEditVStrt.DateTime.Month, DateEditVStrt.DateTime.Day))
            dojTmp = Vstart.Split("-")
            ComboNepaliVStartYear.EditValue = dojTmp(0)
            ComboNepaliVStartMonth.SelectedIndex = dojTmp(1) - 1
            ComboNepaliVStartDate.EditValue = dojTmp(2)


            Dim VEnd As String = DC.ToBS(New Date(DateEditVEnd.DateTime.Year, DateEditVEnd.DateTime.Month, DateEditVEnd.DateTime.Day))
            dojTmp = VEnd.Split("-")
            ComboNepaliVEndYear.EditValue = dojTmp(0)
            ComboNepaliVEndMonth.SelectedIndex = dojTmp(1) - 1
            ComboNepaliVEndDate.EditValue = dojTmp(2)
        Else
            ComboBoxEditNepaliYear.Visible = False
            ComboBoxEditNEpaliMonth.Visible = False
            ComboBoxEditNepaliDate.Visible = False
            DateEditLeaving.Visible = True
            DateEditJoin.Visible = True
            DateEditBirth.Visible = True
            DateEditVStrt.Visible = True
            DateEditVEnd.Visible = True
            ComboBoxEditNepaliYearDOB.Visible = False
            ComboBoxEditNEpaliMonthDOB.Visible = False
            ComboBoxEditNepaliDateDOB.Visible = False
            ComboBoxEditNepaliYearLeave.Visible = False
            ComboBoxEditNEpaliMonthLeave.Visible = False
            ComboBoxEditNepaliDateLeave.Visible = False

            ComboNepaliVStartDate.Visible = False
            ComboNepaliVStartMonth.Visible = False
            ComboNepaliVStartYear.Visible = False

            ComboNepaliVEndDate.Visible = False
            ComboNepaliVEndMonth.Visible = False
            ComboNepaliVEndYear.Visible = False
        End If
        TextEditUserNo.Enabled = True
        TextEditUserNo.Select()
        GridLookUpEdit1.EditValue = GridLookUpEdit1.Properties.GetKeyValue(0)
        GridLookUpEdit2.EditValue = GridLookUpEdit2.Properties.GetKeyValue(0)
        GridLookUpEdit3.EditValue = GridLookUpEdit3.Properties.GetKeyValue(0)
        GridLookUpEdit4.EditValue = GridLookUpEdit4.Properties.GetKeyValue(0)
        GridLookUpEdit5.EditValue = GridLookUpEdit5.Properties.GetKeyValue(0)
        GridLookUpEdit6.EditValue = GridLookUpEdit6.Properties.GetKeyValue(0)
        GridLookUpEdit7.EditValue = GridLookUpEdit7.Properties.GetKeyValue(0)
        GridLookUpEdit8.EditValue = GridLookUpEdit8.Properties.GetKeyValue(0)
        PopupContainerEdit1.EditValue = ""

        ToggleSwitch1.Enabled = False
        TextEdit6.Enabled = False
        DateEditLeaving.Enabled = False

        ToggleSwitch1.IsOn = True
        TextEditUserNo.Text = ""
        TextEdit2.Text = ""
        TextEditName.Text = ""
        TextEdit4.Text = ""
        TextEdit5.Text = ""
        DateEditLeaving.EditValue = ""
        TextEdit6.Text = ""
        TextEditESI.Text = ""
        TextEditPF1.Text = ""
        TextEditPF.Text = ""
        DateEditJoin.EditValue = ""
        DateEditBirth.EditValue = ""
        ToggleSwitch2.IsOn = False
        ComboBoxEdit1.EditValue = "N/A"
        TextEdit10.Text = ""
        TextEdit11.Text = ""
        ComboBoxEdit2.EditValue = "Male"
        TextEdit12.Text = ""
        TextEdit13.Text = ""
        TextEdit14.Text = ""
        TextEdit15.Text = ""
        TextEdit16.Text = ""
        TextEdit17.Text = ""
        TextEditUid.Text = ""
        TextEdit19.Text = ""
        DateEditVStrt.DateTime = "2018-01-01"
        DateEditVEnd.DateTime = "2029-12-31"
        MemoEdit1.Text = ""
        MemoEdit2.Text = ""
        ComboBoxEditNepaliYearDOB.EditValue = ""
        ComboBoxEditNEpaliMonthDOB.EditValue = ""
        ComboBoxEditNepaliDateDOB.EditValue = ""

        ComboBoxEditNepaliYearLeave.EditValue = ""
        ComboBoxEditNEpaliMonthLeave.EditValue = ""
        ComboBoxEditNepaliDateLeave.EditValue = ""
        CheckCopyEmpty.Checked = True
        EmpPictureEdit.Image = Nothing


        isActive = "Y"
    End Sub
    Private Sub SetFormValue()
        CheckCopyEmpty.Checked = True
        PopupContainerEdit1.EditValue = ""
        TextEditUserNo.Enabled = False
        ToggleSwitch1.Enabled = True
        ds1 = New DataSet
        If Common.servername = "Access" Then
            Dim a As OleDbDataAdapter = New OleDbDataAdapter("select * from TblEmployee where PAYCODE = '" & EpId & "' ", Common.con1)
            a.Fill(ds1)
        Else
            adap1 = New SqlDataAdapter("select * from TblEmployee where PAYCODE = '" & EpId & "' ", Common.con)
            adap1.Fill(ds1)
        End If
        If ds1.Tables(0).Rows(0).Item("ACTIVE").ToString.Trim = "Y" Then
            ToggleSwitch1.IsOn = True
        ElseIf ds1.Tables(0).Rows(0).Item("ACTIVE").ToString.Trim = "N" Then
            ToggleSwitch1.IsOn = False
        End If
        isActive = ds1.Tables(0).Rows(0).Item("ACTIVE").ToString.Trim
        DOL = ds1.Tables(0).Rows(0).Item("Leavingdate").ToString
        TextEditUserNo.Text = ds1.Tables(0).Rows(0).Item("PRESENTCARDNO").ToString.Trim
        TextEdit2.Text = ds1.Tables(0).Rows(0).Item("PAYCODE").ToString.Trim
        TextEditName.Text = ds1.Tables(0).Rows(0).Item("EMPNAME").ToString.Trim
        TextEdit4.Text = ds1.Tables(0).Rows(0).Item("GUARDIANNAME").ToString.Trim

        GridLookUpEdit1.EditValue = ds1.Tables(0).Rows(0).Item("COMPANYCODE").ToString
        GridLookUpEdit2.EditValue = ds1.Tables(0).Rows(0).Item("DEPARTMENTCODE").ToString
        GridLookUpEdit3.EditValue = ds1.Tables(0).Rows(0).Item("CAT").ToString
        GridLookUpEdit4.EditValue = ds1.Tables(0).Rows(0).Item("EmployeeGroupId").ToString
        GridLookUpEdit5.EditValue = ds1.Tables(0).Rows(0).Item("GradeCode").ToString
        GridLookUpEdit6.EditValue = ds1.Tables(0).Rows(0).Item("DESPANSARYCODE").ToString
        GridLookUpEdit7.EditValue = ds1.Tables(0).Rows(0).Item("bankCODE").ToString
        GridLookUpEdit8.EditValue = ds1.Tables(0).Rows(0).Item("BRANCHCODE").ToString

        TextEdit5.Text = ds1.Tables(0).Rows(0).Item("DESIGNATION").ToString.Trim
        TextEdit6.Text = ds1.Tables(0).Rows(0).Item("LeavingReason").ToString.Trim
        TextEditESI.Text = ds1.Tables(0).Rows(0).Item("ESINO").ToString.Trim 'ESINO

        If ds1.Tables(0).Rows(0).Item("PF_NO").ToString.Trim = "0" Then
            TextEditPF1.Text = ""
        Else
            TextEditPF1.Text = ds1.Tables(0).Rows(0).Item("PF_NO").ToString.Trim
        End If
        TextEditPF.Text = ds1.Tables(0).Rows(0).Item("PFNO").ToString.Trim

        Try
            If Convert.ToDateTime(ds1.Tables(0).Rows(0).Item("Leavingdate").ToString).ToString("yyyy-MM-dd 00:00:00") = "1800-01-01 00:00:00" Then
                DateEditLeaving.EditValue = ""
            Else
                DateEditLeaving.EditValue = Convert.ToDateTime(ds1.Tables(0).Rows(0).Item("Leavingdate").ToString.Trim)
            End If
        Catch ex As Exception
            DateEditLeaving.EditValue = ""
        End Try


        Try
            If Convert.ToDateTime(ds1.Tables(0).Rows(0).Item("DateOFJOIN").ToString).ToString("yyyy-MM-dd 00:00:00") = "1800-01-01 00:00:00" Then
                DateEditJoin.EditValue = ""
            Else
                DateEditJoin.EditValue = Convert.ToDateTime(ds1.Tables(0).Rows(0).Item("DateOFJOIN").ToString.Trim)
            End If
        Catch ex As Exception
            DateEditJoin.EditValue = ""
        End Try


        Try
            If Convert.ToDateTime(ds1.Tables(0).Rows(0).Item("DateOFBIRTH").ToString).ToString("yyyy-MM-dd 00:00:00") = "1800-01-01 00:00:00" Then
                DateEditBirth.EditValue = ""
            Else
                DateEditBirth.EditValue = Convert.ToDateTime(ds1.Tables(0).Rows(0).Item("DateOFBIRTH").ToString.Trim)
            End If
        Catch ex As Exception
            DateEditBirth.EditValue = ""
        End Try


        If ds1.Tables(0).Rows(0).Item("ISMARRIED").ToString.Trim = "Y" Then
            ToggleSwitch2.IsOn = True
        ElseIf ds1.Tables(0).Rows(0).Item("ISMARRIED").ToString.Trim = "N" Then
            ToggleSwitch2.IsOn = False
        End If
        ComboBoxEdit1.EditValue = ds1.Tables(0).Rows(0).Item("BLOODGROUP").ToString.Trim

        TextEdit10.Text = ds1.Tables(0).Rows(0).Item("QUALIFICATION").ToString.Trim
        TextEdit11.Text = ds1.Tables(0).Rows(0).Item("EXPERIENCE").ToString.Trim

        If ds1.Tables(0).Rows(0).Item("SEX").ToString.Trim = "M" Then
            ComboBoxEdit2.EditValue = "Male"
        ElseIf ds1.Tables(0).Rows(0).Item("SEX").ToString.Trim = "F" Then
            ComboBoxEdit2.EditValue = "Female"
        End If

        TextEdit12.Text = ds1.Tables(0).Rows(0).Item("BankAcc").ToString.Trim
        TextEdit13.Text = ds1.Tables(0).Rows(0).Item("E_MAIL1").ToString.Trim
        TextEdit14.Text = ds1.Tables(0).Rows(0).Item("BUS").ToString.Trim
        TextEdit15.Text = ds1.Tables(0).Rows(0).Item("VehicleNo").ToString.Trim

        Try
            If Convert.ToDateTime(ds1.Tables(0).Rows(0).Item("ValidityStartDate").ToString).ToString("yyyy-MM-dd 00:00:00") = "1800-01-01 00:00:00" Then
                DateEditVStrt.EditValue = ""
            Else
                DateEditVStrt.EditValue = Convert.ToDateTime(ds1.Tables(0).Rows(0).Item("ValidityStartDate").ToString.Trim)
            End If
        Catch ex As Exception
            DateEditVStrt.EditValue = ""
        End Try

        Try
            If Convert.ToDateTime(ds1.Tables(0).Rows(0).Item("ValidityEndDate").ToString).ToString("yyyy-MM-dd 00:00:00") = "1800-01-01 00:00:00" Then
                DateEditVEnd.EditValue = ""
            Else
                DateEditVEnd.EditValue = Convert.ToDateTime(ds1.Tables(0).Rows(0).Item("ValidityEndDate").ToString.Trim)
            End If
        Catch ex As Exception
            DateEditVEnd.EditValue = ""
        End Try

        Dim DC As New DateConverter()
        If Common.IsNepali = "Y" Then
            Dim doj As String = DC.ToBS(New Date(DateEditJoin.DateTime.Year, DateEditJoin.DateTime.Month, DateEditJoin.DateTime.Day))
            Dim dojTmp() As String = doj.Split("-")
            ComboBoxEditNepaliYear.EditValue = dojTmp(0)
            ComboBoxEditNEpaliMonth.SelectedIndex = dojTmp(1) - 1
            ComboBoxEditNepaliDate.EditValue = dojTmp(2)


            If Convert.ToDateTime(ds1.Tables(0).Rows(0).Item("DateOFBIRTH").ToString).ToString("yyyy-MM-dd 00:00:00") = "1800-01-01 00:00:00" Then
                ComboBoxEditNepaliYearDOB.EditValue = ""
                ComboBoxEditNEpaliMonthDOB.EditValue = ""
                ComboBoxEditNepaliDateDOB.EditValue = ""
            Else
                Dim doB As String = DC.ToBS(New Date(DateEditBirth.DateTime.Year, DateEditBirth.DateTime.Month, DateEditBirth.DateTime.Day))
                dojTmp = doB.Split("-")
                ComboBoxEditNepaliYearDOB.EditValue = dojTmp(0)
                ComboBoxEditNEpaliMonthDOB.SelectedIndex = dojTmp(1) - 1
                ComboBoxEditNepaliDateDOB.EditValue = dojTmp(2)

            End If
            If Convert.ToDateTime(ds1.Tables(0).Rows(0).Item("Leavingdate").ToString).ToString("yyyy-MM-dd 00:00:00") = "1800-01-01 00:00:00" Then
                ComboBoxEditNepaliYearLeave.EditValue = ""
                ComboBoxEditNEpaliMonthLeave.EditValue = ""
                ComboBoxEditNepaliDateLeave.EditValue = ""
            Else
                Dim doL As String = DC.ToBS(New Date(DateEditLeaving.DateTime.Year, DateEditLeaving.DateTime.Month, DateEditLeaving.DateTime.Day))
                dojTmp = doL.Split("-")
                ComboBoxEditNepaliYearLeave.EditValue = dojTmp(0)
                ComboBoxEditNEpaliMonthLeave.SelectedIndex = dojTmp(1) - 1
                ComboBoxEditNepaliDateLeave.EditValue = dojTmp(2)
            End If

            Dim Vstart As String = DC.ToBS(New Date(DateEditVStrt.DateTime.Year, DateEditVStrt.DateTime.Month, DateEditVStrt.DateTime.Day))
            dojTmp = Vstart.Split("-")
            ComboNepaliVStartYear.EditValue = dojTmp(0)
            ComboNepaliVStartMonth.SelectedIndex = dojTmp(1) - 1
            ComboNepaliVStartDate.EditValue = dojTmp(2)

            Dim VEnd As String = DC.ToBS(New Date(DateEditVEnd.DateTime.Year, DateEditVEnd.DateTime.Month, DateEditVEnd.DateTime.Day))
            dojTmp = VEnd.Split("-")
            ComboNepaliVEndYear.EditValue = dojTmp(0)
            ComboNepaliVEndMonth.SelectedIndex = dojTmp(1) - 1
            ComboNepaliVEndDate.EditValue = dojTmp(2)

        End If

        MemoEdit1.Text = ds1.Tables(0).Rows(0).Item("ADDRESS1").ToString.Trim
        MemoEdit2.Text = ds1.Tables(0).Rows(0).Item("ADDRESS2").ToString.Trim

        TextEdit16.Text = ds1.Tables(0).Rows(0).Item("PINCODE1").ToString.Trim
        TextEdit17.Text = ds1.Tables(0).Rows(0).Item("TELEPHONE1").ToString.Trim
        TextEdit19.Text = ds1.Tables(0).Rows(0).Item("PINCODE2").ToString.Trim
        TextEditUid.Text = ds1.Tables(0).Rows(0).Item("TELEPHONE2").ToString.Trim
        TextEditCard.Text = ds1.Tables(0).Rows(0).Item("MachineCard").ToString.Trim
        If System.IO.File.Exists("./EmpImages/" & TextEdit2.Text & ".jpg") Then
            'VisitorPictureEdit.Image = Image.FromFile("./EmpImages/" & TextEdit2.Text & ".jpg")
            EmpPictureEdit.LoadAsync("./EmpImages/" & TextEdit2.Text & ".jpg")
        Else
            EmpPictureEdit.Image = Nothing
        End If
    End Sub
    Private Sub PopupContainerEdit1_QueryPopUp(sender As System.Object, e As System.ComponentModel.CancelEventArgs) Handles PopupContainerEdit1.QueryPopUp
        Dim val As Object = PopupContainerEdit1.EditValue
        If (val Is Nothing) Then
            GridViewMachine.ClearSelection()
        Else
            'Dim texts() As String = val.ToString.Split(Microsoft.VisualBasic.ChrW(44))
            Dim texts() As String = val.ToString.Split(",")
            For Each text As String In texts
                If text.Trim.Length = 1 Then
                    text = text.Trim & "  "
                ElseIf text.Trim.Length = 2 Then
                    text = text.Trim & " "
                End If
                'MsgBox(text & "  " & text.Length & " " & GridView1.LocateByValue("SHIFT", text))
                Dim rowHandle As Integer = GridViewMachine.LocateByValue("ID_NO", text)
                GridViewMachine.SelectRow(rowHandle)
            Next
        End If
    End Sub
    Private Sub PopupContainerEdit1_QueryResultValue(sender As System.Object, e As DevExpress.XtraEditors.Controls.QueryResultValueEventArgs) Handles PopupContainerEdit1.QueryResultValue
        Dim selectedRows() As Integer = GridViewMachine.GetSelectedRows
        Dim sb As StringBuilder = New StringBuilder
        For Each selectionRow As Integer In selectedRows
            Dim a As System.Data.DataRowView = GridViewMachine.GetRow(selectionRow)
            If (sb.ToString.Length > 0) Then
                sb.Append(", ")
            End If
            sb.Append(a.Item("ID_NO"))
        Next
        e.Value = sb.ToString
    End Sub
    Private Sub SimpleButton2_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton2.Click
        Me.Close()
    End Sub
    Private Sub SimpleButtonSave_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonSave.Click
        If TextEditUserNo.Text = "" Then
            XtraMessageBox.Show(ulf, "<size=10>User No cannot be empty</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            TextEditUserNo.Select()
            Exit Sub
        End If
        If TextEdit2.Text = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Emp code cannot be empty</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            TextEdit2.Select()
            Exit Sub
        End If
        If TextEditName.Text.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Emp Name cannot be empty</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            TextEditName.Select()
            Exit Sub
        End If


        'If DateEdit2.Text = "" Then
        '    XtraMessageBox.Show(ulf, "<size=10>Join date cannot be empty</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        '    DateEdit2.Select()
        '    Exit Sub
        'End If
        Me.Cursor = Cursors.WaitCursor
        Dim ACTIVE As Char = ""
        If ToggleSwitch1.IsOn = True Then
            ACTIVE = "Y"
        ElseIf ToggleSwitch1.IsOn = False Then
            ACTIVE = "N"
        End If
        Dim PRESENTCARDNO As String = TextEditUserNo.Text.Trim
        Dim PAYCODE As String = TextEdit2.Text.Trim
        Dim EMPNAME As String = TextEditName.Text.Trim
        Dim GUARDIANNAME As String = TextEdit4.Text.Trim
        Dim COMPANYCODE As String = GridLookUpEdit1.EditValue
        Dim DEPARTMENTCODE As String = GridLookUpEdit2.EditValue
        Dim CAT As String = GridLookUpEdit3.EditValue
        Dim EmployeeGroupId As String = GridLookUpEdit4.EditValue
        Dim GradeCode As String = GridLookUpEdit5.EditValue
        Dim DESIGNATION As String = TextEdit5.Text.Trim

        'Dim DESPANSARYCODE As String = GridLookUpEdit6.EditValue
        Dim Leavingdate As String = Nothing


        If Common.IsNepali = "Y" Then
            Dim DC As New DateConverter()
            If ComboBoxEditNepaliYear.EditValue.ToString.Trim <> "" And ComboBoxEditNEpaliMonth.EditValue.ToString.Trim <> "" And ComboBoxEditNepaliDate.EditValue.ToString.Trim <> "" Then
                Try
                    'DateEdit2.DateTime = DC.ToAD(New Date(ComboBoxEditNepaliYear.EditValue, ComboBoxEditNEpaliMonth.SelectedIndex + 1, ComboBoxEditNepaliDate.EditValue))
                    DateEditJoin.DateTime = DC.ToAD(ComboBoxEditNepaliYear.EditValue & "-" & ComboBoxEditNEpaliMonth.SelectedIndex + 1 & "-" & ComboBoxEditNepaliDate.EditValue)
                Catch ex As Exception
                    XtraMessageBox.Show(ulf, "<size=10>Invalid Date Of Joining</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    ComboBoxEditNepaliDate.Select()
                    Exit Sub
                End Try
            End If
            If ComboBoxEditNepaliYearDOB.EditValue.ToString.Trim <> "" And ComboBoxEditNEpaliMonthDOB.EditValue.ToString.Trim <> "" And ComboBoxEditNepaliDateDOB.EditValue.ToString.Trim <> "" Then
                Try
                    'DateEdit3.DateTime = DC.ToAD(New Date(ComboBoxEditNepaliYearDOB.EditValue, ComboBoxEditNEpaliMonthDOB.SelectedIndex + 1, ComboBoxEditNepaliDateDOB.EditValue))
                    DateEditBirth.DateTime = DC.ToAD(ComboBoxEditNepaliYearDOB.EditValue & "-" & ComboBoxEditNEpaliMonthDOB.SelectedIndex + 1 & "-" & ComboBoxEditNepaliDateDOB.EditValue)
                Catch ex As Exception
                    XtraMessageBox.Show(ulf, "<size=10>Invalid Date Of Birth</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    ComboBoxEditNepaliDateDOB.Select()
                    Exit Sub
                End Try
            End If

            If ComboBoxEditNepaliYearLeave.EditValue.ToString.Trim <> "" And ComboBoxEditNEpaliMonthLeave.EditValue.ToString.Trim <> "" And ComboBoxEditNepaliDateLeave.EditValue.ToString.Trim <> "" Then
                Try
                    'DateEditLeaving.DateTime = DC.ToAD(New Date(ComboBoxEditNepaliYearLeave.EditValue, ComboBoxEditNEpaliMonthLeave.SelectedIndex + 1, ComboBoxEditNepaliDateLeave.EditValue))
                    DateEditLeaving.DateTime = DC.ToAD(ComboBoxEditNepaliYearLeave.EditValue & "-" & ComboBoxEditNEpaliMonthLeave.SelectedIndex + 1 & "-" & ComboBoxEditNepaliDateLeave.EditValue)
                Catch ex As Exception
                    XtraMessageBox.Show(ulf, "<size=10>Invalid Date Of Leaving</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    ComboBoxEditNepaliDateLeave.Select()
                    Exit Sub
                End Try
            End If

            If ComboNepaliVStartYear.EditValue.ToString.Trim <> "" And ComboNepaliVStartMonth.EditValue.ToString.Trim <> "" And ComboNepaliVStartDate.EditValue.ToString.Trim <> "" Then
                Try
                    'DateEdit4.DateTime = DC.ToAD(New Date(ComboNepaliVStartYear.EditValue, ComboNepaliVStartMonth.SelectedIndex + 1, ComboNepaliVStartDate.EditValue))
                    DateEditVStrt.DateTime = DC.ToAD(ComboNepaliVStartYear.EditValue & "-" & ComboNepaliVStartMonth.SelectedIndex + 1 & "-" & ComboNepaliVStartDate.EditValue)
                Catch ex As Exception
                    XtraMessageBox.Show(ulf, "<size=10>Invalid Validity Start Date</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    ComboNepaliVStartDate.Select()
                    Exit Sub
                End Try
            End If

            If ComboNepaliVEndYear.EditValue.ToString.Trim <> "" And ComboNepaliVEndMonth.EditValue.ToString.Trim <> "" And ComboNepaliVEndDate.EditValue.ToString.Trim <> "" Then
                Try
                    'DateEdit5.DateTime = DC.ToAD(New Date(ComboNepaliVEndYear.EditValue, ComboNepaliVEndMonth.SelectedIndex + 1, ComboNepaliVEndDate.EditValue))
                    DateEditVEnd.DateTime = DC.ToAD(ComboNepaliVEndYear.EditValue & "-" & ComboNepaliVEndMonth.SelectedIndex + 1 & "-" & ComboNepaliVEndDate.EditValue)
                Catch ex As Exception
                    XtraMessageBox.Show(ulf, "<size=10>Invalid Validity End Date</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    ComboNepaliVEndDate.Select()
                    Exit Sub
                End Try
            End If
        End If




        If DateEditLeaving.Text = "" Then
            Leavingdate = "1800-01-01 00:00:00"
        Else
            Leavingdate = DateEditLeaving.DateTime.ToString("yyyy-MM-dd HH:mm:ss")
        End If
        Dim LeavingReason As String = TextEdit6.Text.Trim
        Dim PFNO As String = TextEditPF.Text.Trim
        Dim PF_NO As Integer = Nothing
        If TextEditPF1.Text.Trim.Length > 0 Then
            PF_NO = TextEditPF1.Text.Trim()
        Else
            PF_NO = Nothing
        End If
        Dim ESINO As String = TextEditESI.Text.Trim
        Dim DESPANSARYCODE As String = GridLookUpEdit6.EditValue
        Dim DateOFJOIN As String = DateEditJoin.DateTime.ToString("yyyy-MM-dd HH:mm:ss")
        If DateEditJoin.Text = "" Then
            'DateOFJOIN = "1800-01-01 00:00:00"
            DateOFJOIN = Now.AddDays(-(Now.Day - 1)).ToString("yyyy-MM-dd HH:mm:ss")
        Else
            DateOFJOIN = DateEditJoin.DateTime.ToString("yyyy-MM-dd HH:mm:ss")
        End If
        Dim DateOFBIRTH As String = DateEditBirth.DateTime.ToString("yyyy-MM-dd HH:mm:ss")
        If DateEditBirth.Text = "" Then
            DateOFBIRTH = "1800-01-01 00:00:00"
        Else
            DateOFBIRTH = DateEditBirth.DateTime.ToString("yyyy-MM-dd HH:mm:ss")
        End If
        Dim ISMARRIED As Char = ""
        If ToggleSwitch2.IsOn = True Then
            ISMARRIED = "Y"
        ElseIf ToggleSwitch2.IsOn = False Then
            ISMARRIED = "N"
        End If
        Dim BLOODGROUP As String = ComboBoxEdit1.EditValue
        Dim QUALIFICATION As String = TextEdit10.Text.Trim
        Dim EXPERIENCE As String = TextEdit11.Text.Trim
        Dim SEX As Char = ComboBoxEdit2.EditValue.Substring(0, 1)
        Dim bankCODE As String = GridLookUpEdit7.EditValue
        Dim BRANCHCODE As String = GridLookUpEdit8.EditValue
        Dim BankAcc As String = TextEdit12.Text.Trim
        Dim E_MAIL1 As String = TextEdit13.Text.Trim
        Dim BUS As String = TextEdit14.Text.Trim
        Dim VehicleNo As String = TextEdit15.Text.Trim
        Dim ValidityStartDate As String = DateEditVStrt.DateTime.ToString("yyyy-MM-dd HH:mm:ss") '.EditValue
        If DateEditVStrt.Text = "" Then
            ValidityStartDate = "1800-01-01 00:00:00"
        Else
            ValidityStartDate = DateEditVStrt.DateTime.ToString("yyyy-MM-dd HH:mm:ss")
        End If
        Dim ValidityEndDate As String = DateEditVEnd.DateTime.ToString("yyyy-MM-dd HH:mm:ss") '.EditValue
        If DateEditVEnd.Text = "" Then
            ValidityEndDate = "1800-01-01 00:00:00"
        Else
            ValidityEndDate = DateEditVEnd.DateTime.ToString("yyyy-MM-dd HH:mm:ss")
        End If
        Dim ADDRESS1 As String = MemoEdit1.Text.Trim
        Dim PINCODE1 As String = TextEdit16.Text.Trim
        Dim TELEPHONE1 As String = TextEdit17.Text.Trim
        Dim ADDRESS2 As String = MemoEdit2.Text.Trim
        Dim PINCODE2 As String = TextEdit19.Text.Trim  'PAN
        Dim TELEPHONE2 As String = TextEditUid.Text.Trim  ' UID
        Dim MachineCard As String = TextEditCard.Text.Trim

        Dim EMPPHOTO As String = ""
        Dim currentImage As Image = EmpPictureEdit.Image
        Try
            Dim savedImage As New Bitmap(currentImage, EmpPictureEdit.ClientSize.Width, EmpPictureEdit.ClientSize.Height)
            Dim m As Bitmap = New Bitmap(savedImage)
            EMPPHOTO = "./EmpImages/" & TextEdit2.Text.Trim & ".jpg"
            'Try
            '    My.Computer.FileSystem.DeleteFile(EMPPHOTO)
            'Catch ex As Exception
            'End Try
            savedImage.Save(EMPPHOTO)
            savedImage.Dispose()
        Catch ex As Exception

        End Try

        If Common.servername = "Access" Then
            adapA1 = New OleDbDataAdapter("select * from EmployeeGroup where GroupId = '" & EmployeeGroupId & "' ", Common.con1)
            ds2 = New DataSet
            adapA1.Fill(ds2)
        Else
            adap2 = New SqlDataAdapter("select * from EmployeeGroup where GroupId = '" & EmployeeGroupId & "' ", Common.con)
            ds2 = New DataSet
            adap2.Fill(ds2)
        End If
        Dim g_Wo_Include As String = Common.EmpGrpArr(ds2.Tables(0).Rows(0).Item("Id")).g_Wo_Include
        Dim comclass As Common = New Common
        If EpId = "" Then
            'Insert
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter("select PAYCODE from TblEmployee where PAYCODE = '" & PAYCODE & "' ", Common.con1)
                ds1 = New DataSet
                adapA.Fill(ds1)
                If ds1.Tables(0).Rows.Count > 0 Then
                    XtraMessageBox.Show(ulf, "<size=10>Dupplicate User No.</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    TextEditUserNo.Select()
                    Exit Sub
                End If

                adapA = New OleDbDataAdapter("select PRESENTCARDNO from TblEmployee where PRESENTCARDNO = '" & PRESENTCARDNO & "' ", Common.con1)
                ds1 = New DataSet
                adapA.Fill(ds1)
                If ds1.Tables(0).Rows.Count > 0 Then
                    XtraMessageBox.Show(ulf, "<size=10>Dupplicate Emp Code</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    TextEdit2.Select()
                    Exit Sub
                End If

                If Common.con1.State <> ConnectionState.Open Then
                    Common.con1.Open()
                End If
                Try
insertA:            cmd1 = New OleDbCommand("insert into TblEmployee (ACTIVE, PRESENTCARDNO, PAYCODE, EMPNAME, GUARDIANNAME, COMPANYCODE, DEPARTMENTCODE, CAT, EmployeeGroupId, GradeCode, DESIGNATION, Leavingdate, LeavingReason, PFNO, PF_NO, ESINO, DESPANSARYCODE, DateOFJOIN, DateOFBIRTH, ISMARRIED, BLOODGROUP, QUALIFICATION, EXPERIENCE, SEX, bankCODE, BankAcc, E_MAIL1, BUS, VehicleNo, ValidityStartDate, ValidityEndDate, ADDRESS1, PINCODE1, TELEPHONE1, ADDRESS2, PINCODE2, TELEPHONE2, LastModifiedBy, LastModifiedDate, BRANCHCODE, EMPPHOTO, MachineCard) values ('" & ACTIVE & "','" & PRESENTCARDNO & "','" & PAYCODE & "','" & EMPNAME & "','" & GUARDIANNAME & "','" & COMPANYCODE & "','" & DEPARTMENTCODE & "','" & CAT & "','" & EmployeeGroupId & "','" & GradeCode & "','" & DESIGNATION & "','" & Leavingdate & "','" & LeavingReason & "','" & PFNO & "','" & PF_NO & "','" & ESINO & "','" & DESPANSARYCODE & "','" & DateOFJOIN & "','" & DateOFBIRTH & "','" & ISMARRIED & "','" & BLOODGROUP & "','" & QUALIFICATION & "','" & EXPERIENCE & "','" & SEX & "','" & bankCODE & "','" & BankAcc & "','" & E_MAIL1 & "','" & BUS & "','" & VehicleNo & "','" & ValidityStartDate & "','" & ValidityEndDate & "','" & ADDRESS1 & "','" & PINCODE1 & "','" & TELEPHONE1 & "','" & ADDRESS2 & "','" & PINCODE2 & "','" & TELEPHONE2 & "','" & Common.USER_R & "','" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "','" & BRANCHCODE & "','" & EMPPHOTO & "','" & MachineCard & "')", Common.con1)
                    cmd1.ExecuteNonQuery()
                Catch ex As Exception
                    If ex.Message.ToString.Trim = "The field is too small to accept the amount of data you attempted to add.  Try inserting or pasting less data." Then
                        Dim sSql As String = "Alter Table tblemployee" &
                               " ALTER COLUMN telephone1 longtext"
                        cmd1 = New OleDbCommand(sSql, Common.con1)
                        cmd1.ExecuteNonQuery()
                        GoTo insertA
                    End If
                End Try

                Common.con1.Close()


                'MsgBox(ds2.Tables(0).Rows.Count)
                If Common.con1.State <> ConnectionState.Open Then
                    Common.con1.Open()
                End If
                cmd1 = New OleDbCommand("insert into tblEmployeeShiftMaster (PAYCODE,CARDNO,SHIFT,SHIFTTYPE,SHIFTPATTERN,SHIFTREMAINDAYS,LASTSHIFTPERFORMED,INONLY,ISPUNCHALL,ISTIMELOSSALLOWED,ALTERNATE_OFF_DAYS,CDAYS,ISROUNDTHECLOCKWORK,ISOT,OTRATE,FIRSTOFFDAY,SECONDOFFTYPE,HALFDAYSHIFT,SECONDOFFDAY,PERMISLATEARRIVAL,PERMISEARLYDEPRT,ISAUTOSHIFT,ISOUTWORK,MAXDAYMIN,ISOS,AUTH_SHIFTS,TIME1,SHORT1,HALF,ISHALFDAY,ISSHORT,TWO, MARKMISSASHALFDAY, MSHIFT) values ( '" & PAYCODE & "','" & PRESENTCARDNO & "','" & ds2.Tables(0).Rows(0).Item("SHIFT").ToString() & "','" & ds2.Tables(0).Rows(0).Item("SHIFTTYPE").ToString() & "','" & ds2.Tables(0).Rows(0).Item("SHIFTPATTERN").ToString() & "','" & ds2.Tables(0).Rows(0).Item("SHIFTREMAINDAYS").ToString() & "','" & ds2.Tables(0).Rows(0).Item("LASTSHIFTPERFORMED").ToString() & "','" & ds2.Tables(0).Rows(0).Item("INONLY").ToString() & "','" & ds2.Tables(0).Rows(0).Item("ISPUNCHALL").ToString() & "','" & ds2.Tables(0).Rows(0).Item("ISTIMELOSSALLOWED").ToString() & "','" & ds2.Tables(0).Rows(0).Item("ALTERNATE_OFF_DAYS").ToString() & "','" & ds2.Tables(0).Rows(0).Item("CDAYS").ToString() & "','" & ds2.Tables(0).Rows(0).Item("ISROUNDTHECLOCKWORK").ToString() & "','" & ds2.Tables(0).Rows(0).Item("ISOT").ToString() & "','" & ds2.Tables(0).Rows(0).Item("OTRATE").ToString() & "','" & ds2.Tables(0).Rows(0).Item("FIRSTOFFDAY").ToString() & "','" & ds2.Tables(0).Rows(0).Item("SECONDOFFTYPE").ToString() & "','" & ds2.Tables(0).Rows(0).Item("HALFDAYSHIFT").ToString() & "','" & ds2.Tables(0).Rows(0).Item("SECONDOFFDAY").ToString() & "','" & ds2.Tables(0).Rows(0).Item("PERMISLATEARRIVAL").ToString() & "','" & ds2.Tables(0).Rows(0).Item("PERMISEARLYDEPRT").ToString() & "','" & ds2.Tables(0).Rows(0).Item("ISAUTOSHIFT").ToString() & "','" & ds2.Tables(0).Rows(0).Item("ISOUTWORK").ToString() & "','" & ds2.Tables(0).Rows(0).Item("MAXDAYMIN").ToString() & "','" & ds2.Tables(0).Rows(0).Item("ISOS").ToString() & "','" & ds2.Tables(0).Rows(0).Item("AUTH_SHIFTS").ToString() & "','" & ds2.Tables(0).Rows(0).Item("TIME1").ToString() & "','" & ds2.Tables(0).Rows(0).Item("SHORT1").ToString() & "','" & ds2.Tables(0).Rows(0).Item("HALF").ToString() & "','" & ds2.Tables(0).Rows(0).Item("ISHALFDAY").ToString() & "','" & ds2.Tables(0).Rows(0).Item("ISSHORT").ToString() & "','" & ds2.Tables(0).Rows(0).Item("TWO").ToString() & "', '" & ds2.Tables(0).Rows(0).Item("MARKMISSASHALFDAY").ToString() & "', '" & ds2.Tables(0).Rows(0).Item("MSHIFT").ToString() & "')", Common.con1)
                cmd1.ExecuteNonQuery()
                Common.con1.Close()

                'adapA2 = New OleDbDataAdapter("select * from tblSetup where SETUPID= (select MAX(SETUPID) from tblSetup)", Common.con1)
                'ds3 = New DataSet
                'adapA2.Fill(ds3)              
                'MsgBox("emp add done")
                Dim dateedit As DateTime
                If DateEditJoin.Text = "" Then
                    dateedit = Convert.ToDateTime(Now.ToString("01/MMM/yyyy"))
                Else
                    dateedit = DateEditJoin.DateTime
                End If
                'MsgBox(dateedit.ToString)
                Dim mmin_date As String = dateedit.ToString("dd/MMM/yyyy")
                If dateedit.Year < Now.Year Then
                    comclass.DutyRoster(PAYCODE, mmin_date, g_Wo_Include)
                    For i As Integer = dateedit.Year To Now.Year - 1
                        mmin_date = Now.ToString("01/Jan/" & i + 1 & "")
                        comclass.DutyRoster(PAYCODE, mmin_date, g_Wo_Include)
                    Next
                Else
                    comclass.DutyRoster(PAYCODE, dateedit.ToString("dd/MMM/yyyy"), g_Wo_Include)
                End If
            Else
                adap = New SqlDataAdapter("select PAYCODE from TblEmployee where PAYCODE = '" & PAYCODE & "' ", Common.con)
                ds = New DataSet
                adap.Fill(ds)
                If ds.Tables(0).Rows.Count > 0 Then
                    XtraMessageBox.Show(ulf, "<size=10>Dupplicate User No.</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    TextEditUserNo.Select()
                    Exit Sub
                End If

                adap = New SqlDataAdapter("select PRESENTCARDNO from TblEmployee where PRESENTCARDNO = '" & PRESENTCARDNO & "' ", Common.con)
                ds = New DataSet
                adap.Fill(ds)
                If ds.Tables(0).Rows.Count > 0 Then
                    XtraMessageBox.Show(ulf, "<size=10>Dupplicate Emp Code</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    TextEdit2.Select()
                    Exit Sub
                End If

                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                Try
                    'Dim TmpsSql As String = "insert into TblEmployee (ACTIVE, PRESENTCARDNO, PAYCODE, EMPNAME, GUARDIANNAME, COMPANYCODE, DEPARTMENTCODE, CAT, EmployeeGroupId, GradeCode, DESIGNATION, Leavingdate, LeavingReason, PFNO, PF_NO, ESINO, DESPANSARYCODE, DateOFJOIN, DateOFBIRTH, ISMARRIED, BLOODGROUP, QUALIFICATION, EXPERIENCE, SEX, bankCODE, BankAcc, E_MAIL1, BUS, VehicleNo, ValidityStartDate, ValidityEndDate, ADDRESS1, PINCODE1, TELEPHONE1, ADDRESS2, PINCODE2, TELEPHONE2, LastModifiedBy, LastModifiedDate, BRANCHCODE, EMPPHOTO,MachineCard) values ('" & ACTIVE & "','" & PRESENTCARDNO & "','" & PAYCODE & "','" & EMPNAME & "','" & GUARDIANNAME & "','" & COMPANYCODE & "','" & DEPARTMENTCODE & "','" & CAT & "','" & EmployeeGroupId & "','" & GradeCode & "','" & DESIGNATION & "','" & Leavingdate & "','" & LeavingReason & "','" & PFNO & "','" & PF_NO & "','" & ESINO & "','" & DESPANSARYCODE & "','" & DateOFJOIN & "','" & DateOFBIRTH & "','" & ISMARRIED & "','" & BLOODGROUP & "','" & QUALIFICATION & "','" & EXPERIENCE & "','" & SEX & "','" & bankCODE & "','" & BankAcc & "','" & E_MAIL1 & "','" & BUS & "','" & VehicleNo & "','" & ValidityStartDate & "','" & ValidityEndDate & "','" & ADDRESS1 & "','" & PINCODE1 & "','" & TELEPHONE1 & "','" & ADDRESS2 & "','" & PINCODE2 & "','" & TELEPHONE2 & "','" & Common.USER_R & "','" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "','" & BRANCHCODE & "','" & EMPPHOTO & "','" & MachineCard & "')"
                    'Clipboard.SetText(TmpsSql)
insert:             cmd = New SqlCommand("insert into TblEmployee (ACTIVE, PRESENTCARDNO, PAYCODE, EMPNAME, GUARDIANNAME, COMPANYCODE, DEPARTMENTCODE, CAT, EmployeeGroupId, GradeCode, DESIGNATION, Leavingdate, LeavingReason, PFNO, PF_NO, ESINO, DESPANSARYCODE, DateOFJOIN, DateOFBIRTH, ISMARRIED, BLOODGROUP, QUALIFICATION, EXPERIENCE, SEX, bankCODE, BankAcc, E_MAIL1, BUS, VehicleNo, ValidityStartDate, ValidityEndDate, ADDRESS1, PINCODE1, TELEPHONE1, ADDRESS2, PINCODE2, TELEPHONE2, LastModifiedBy, LastModifiedDate, BRANCHCODE, EMPPHOTO,MachineCard) values ('" & ACTIVE & "','" & PRESENTCARDNO & "','" & PAYCODE & "','" & EMPNAME & "','" & GUARDIANNAME & "','" & COMPANYCODE & "','" & DEPARTMENTCODE & "','" & CAT & "','" & EmployeeGroupId & "','" & GradeCode & "','" & DESIGNATION & "','" & Leavingdate & "','" & LeavingReason & "','" & PFNO & "','" & PF_NO & "','" & ESINO & "','" & DESPANSARYCODE & "','" & DateOFJOIN & "','" & DateOFBIRTH & "','" & ISMARRIED & "','" & BLOODGROUP & "','" & QUALIFICATION & "','" & EXPERIENCE & "','" & SEX & "','" & bankCODE & "','" & BankAcc & "','" & E_MAIL1 & "','" & BUS & "','" & VehicleNo & "','" & ValidityStartDate & "','" & ValidityEndDate & "','" & ADDRESS1 & "','" & PINCODE1 & "','" & TELEPHONE1 & "','" & ADDRESS2 & "','" & PINCODE2 & "','" & TELEPHONE2 & "','" & Common.USER_R & "','" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "','" & BRANCHCODE & "','" & EMPPHOTO & "','" & MachineCard & "')", Common.con)
                    cmd.ExecuteNonQuery()
                Catch ex As Exception
                    MsgBox(ex.Message.ToString)

                    If ex.Message.ToString.Trim = "String or binary data would be truncated." & vbCrLf & "The statement has been terminated." Then
                        Dim sSql As String = "Alter Table tblemployee" &
                                " ALTER COLUMN telephone1 varchar(200)"
                        cmd = New SqlCommand(sSql, Common.con)
                        cmd.ExecuteNonQuery()
                        GoTo insert
                    End If
                End Try

                Common.con.Close()


                'MsgBox(ds2.Tables(0).Rows.Count)
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                cmd = New SqlCommand("insert into tblEmployeeShiftMaster (PAYCODE,CARDNO,SHIFT,SHIFTTYPE,SHIFTPATTERN,SHIFTREMAINDAYS,LASTSHIFTPERFORMED,INONLY,ISPUNCHALL,ISTIMELOSSALLOWED,ALTERNATE_OFF_DAYS,CDAYS,ISROUNDTHECLOCKWORK,ISOT,OTRATE,FIRSTOFFDAY,SECONDOFFTYPE,HALFDAYSHIFT,SECONDOFFDAY,PERMISLATEARRIVAL,PERMISEARLYDEPRT,ISAUTOSHIFT,ISOUTWORK,MAXDAYMIN,ISOS,AUTH_SHIFTS,TIME,SHORT,HALF,ISHALFDAY,ISSHORT,TWO, MARKMISSASHALFDAY,MSHIFT) values ( '" & PAYCODE & "','" & PRESENTCARDNO & "','" & ds2.Tables(0).Rows(0).Item("SHIFT").ToString() & "','" & ds2.Tables(0).Rows(0).Item("SHIFTTYPE").ToString() & "','" & ds2.Tables(0).Rows(0).Item("SHIFTPATTERN").ToString() & "','" & ds2.Tables(0).Rows(0).Item("SHIFTREMAINDAYS").ToString() & "','" & ds2.Tables(0).Rows(0).Item("LASTSHIFTPERFORMED").ToString() & "','" & ds2.Tables(0).Rows(0).Item("INONLY").ToString() & "','" & ds2.Tables(0).Rows(0).Item("ISPUNCHALL").ToString() & "','" & ds2.Tables(0).Rows(0).Item("ISTIMELOSSALLOWED").ToString() & "','" & ds2.Tables(0).Rows(0).Item("ALTERNATE_OFF_DAYS").ToString() & "','" & ds2.Tables(0).Rows(0).Item("CDAYS").ToString() & "','" & ds2.Tables(0).Rows(0).Item("ISROUNDTHECLOCKWORK").ToString() & "','" & ds2.Tables(0).Rows(0).Item("ISOT").ToString() & "','" & ds2.Tables(0).Rows(0).Item("OTRATE").ToString() & "','" & ds2.Tables(0).Rows(0).Item("FIRSTOFFDAY").ToString() & "','" & ds2.Tables(0).Rows(0).Item("SECONDOFFTYPE").ToString() & "','" & ds2.Tables(0).Rows(0).Item("HALFDAYSHIFT").ToString() & "','" & ds2.Tables(0).Rows(0).Item("SECONDOFFDAY").ToString() & "','" & ds2.Tables(0).Rows(0).Item("PERMISLATEARRIVAL").ToString() & "','" & ds2.Tables(0).Rows(0).Item("PERMISEARLYDEPRT").ToString() & "','" & ds2.Tables(0).Rows(0).Item("ISAUTOSHIFT").ToString() & "','" & ds2.Tables(0).Rows(0).Item("ISOUTWORK").ToString() & "','" & ds2.Tables(0).Rows(0).Item("MAXDAYMIN").ToString() & "','" & ds2.Tables(0).Rows(0).Item("ISOS").ToString() & "','" & ds2.Tables(0).Rows(0).Item("AUTH_SHIFTS").ToString() & "','" & ds2.Tables(0).Rows(0).Item("TIME").ToString() & "','" & ds2.Tables(0).Rows(0).Item("SHORT").ToString() & "','" & ds2.Tables(0).Rows(0).Item("HALF").ToString() & "','" & ds2.Tables(0).Rows(0).Item("ISHALFDAY").ToString() & "','" & ds2.Tables(0).Rows(0).Item("ISSHORT").ToString() & "','" & ds2.Tables(0).Rows(0).Item("TWO").ToString() & "','" & ds2.Tables(0).Rows(0).Item("MARKMISSASHALFDAY").ToString() & "','" & ds2.Tables(0).Rows(0).Item("MSHIFT").ToString() & "')", Common.con)
                cmd.ExecuteNonQuery()
                Common.con.Close()

                'adap3 = New SqlDataAdapter("select * from tblSetup where SETUPID= (select MAX(SETUPID) from tblSetup)", Common.con)
                'ds3 = New DataSet
                'adap3.Fill(ds3)

                'Dim mmin_date As String = DateEdit2.DateTime.ToString("dd/MMM/yyyy")
                'If DateEdit2.DateTime.Year < Now.Year Then
                '    comclass.DutyRoster(PAYCODE, mmin_date, g_Wo_Include)
                '    For i As Integer = DateEdit2.DateTime.Year To Now.Year - 1
                '        mmin_date = Now.ToString("01/Jan/" & i + 1 & "")
                '        comclass.DutyRoster(PAYCODE, mmin_date, g_Wo_Include)
                '    Next
                'Else
                '    comclass.DutyRoster(PAYCODE, DateEdit2.DateTime.ToString("dd/MMM/yyyy"), g_Wo_Include)
                'End If

                Dim dateedit As DateTime
                If DateEditJoin.Text = "" Then
                    dateedit = Convert.ToDateTime(Now.ToString("01/MMM/yyyy"))
                Else
                    dateedit = DateEditJoin.DateTime
                End If
                Dim mmin_date As String = dateedit.ToString("dd/MMM/yyyy")
                If dateedit.Year < Now.Year Then
                    comclass.DutyRoster(PAYCODE, mmin_date, g_Wo_Include)
                    For i As Integer = dateedit.Year To Now.Year - 1
                        mmin_date = Now.ToString("01/Jan/" & i + 1 & "")
                        If Common.servername = "Access" Then
                            If Common.con1.State <> ConnectionState.Open Then
                                Common.con1.Open()
                            End If
                        Else
                            If Common.con.State <> ConnectionState.Open Then
                                Common.con.Open()
                            End If
                        End If
                        comclass.DutyRoster(PAYCODE, mmin_date, g_Wo_Include)
                        If Common.servername = "Access" Then
                            If Common.con1.State <> ConnectionState.Closed Then
                                Common.con1.Close()
                            End If
                        Else
                            If Common.con.State <> ConnectionState.Closed Then
                                Common.con.Close()
                            End If
                        End If
                    Next
                Else
                    comclass.DutyRoster(PAYCODE, dateedit.ToString("dd/MMM/yyyy"), g_Wo_Include)
                End If
            End If
            Common.LogPost("Employee Add;  Paycode='" & PAYCODE)
        Else
            'update
            If Common.servername = "Access" Then
                If Common.con1.State <> ConnectionState.Open Then
                    Common.con1.Open()
                End If
                Try
UpdateA:            cmd1 = New OleDbCommand("update TblEmployee set ACTIVE = '" & ACTIVE & "', PAYCODE = '" & PAYCODE & "',  EMPNAME = '" & EMPNAME & "',  GUARDIANNAME = '" & GUARDIANNAME & "',  COMPANYCODE = '" & COMPANYCODE & "',  DEPARTMENTCODE = '" & DEPARTMENTCODE & "',  CAT = '" & CAT & "',  EmployeeGroupId = '" & EmployeeGroupId & "',  GradeCode = '" & GradeCode & "',  DESIGNATION = '" & DESIGNATION & "',  Leavingdate = '" & Leavingdate & "',  LeavingReason = '" & LeavingReason & "',  PFNO = '" & PFNO & "',  PF_NO = '" & PF_NO & "',  ESINO = '" & ESINO & "',  DESPANSARYCODE = '" & DESPANSARYCODE & "',  DateOFJOIN = '" & DateOFJOIN & "',  DateOFBIRTH = '" & DateOFBIRTH & "',  ISMARRIED = '" & ISMARRIED & "',  BLOODGROUP = '" & BLOODGROUP & "',  QUALIFICATION = '" & QUALIFICATION & "',  EXPERIENCE = '" & EXPERIENCE & "',  SEX = '" & SEX & "',  bankCODE = '" & bankCODE & "',  BankAcc = '" & BankAcc & "',  E_MAIL1 = '" & E_MAIL1 & "',  BUS = '" & BUS & "',  VehicleNo = '" & VehicleNo & "',  ValidityStartDate = '" & ValidityStartDate & "',  ValidityEndDate = '" & ValidityEndDate & "',  ADDRESS1 = '" & ADDRESS1 & "',  PINCODE1 = '" & PINCODE1 & "',  TELEPHONE1 = '" & TELEPHONE1 & "',  ADDRESS2 = '" & ADDRESS2 & "',  PINCODE2 = '" & PINCODE2 & "',  TELEPHONE2 = '" & TELEPHONE2 & "',  LastModifiedBy = '" & Common.USER_R & "',  LastModifiedDate = '" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "', BRANCHCODE='" & BRANCHCODE & "',EMPPHOTO='" & EMPPHOTO & "', MachineCard='" & MachineCard & "' where PRESENTCARDNO='" & PRESENTCARDNO & "'", Common.con1)
                    cmd1.ExecuteNonQuery()
                Catch ex As Exception
                    If ex.Message.ToString.Trim = "The field is too small to accept the amount of data you attempted to add.  Try inserting or pasting less data." Then
                        Dim sSql As String = "Alter Table tblemployee" &
                               " ALTER COLUMN telephone1 longtext"
                        cmd1 = New OleDbCommand(sSql, Common.con1)
                        cmd1.ExecuteNonQuery()
                        GoTo UpdateA
                    End If
                End Try

                Common.con1.Close()

                adapA1 = New OleDbDataAdapter("select * from EmployeeGroup where GroupId = '" & EmployeeGroupId & "' ", Common.con1)
                ds2 = New DataSet
                adapA1.Fill(ds2)

                If Common.con1.State <> ConnectionState.Open Then
                    Common.con1.Open()
                End If
                cmd1 = New OleDbCommand("Update tblEmployeeShiftMaster set PAYCODE = '" & PAYCODE & "', SHIFT = '" & ds2.Tables(0).Rows(0).Item("SHIFT").ToString() & "', SHIFTTYPE = '" & ds2.Tables(0).Rows(0).Item("SHIFTTYPE").ToString() & "', SHIFTPATTERN = '" & ds2.Tables(0).Rows(0).Item("SHIFTPATTERN").ToString() & "', SHIFTREMAINDAYS = '" & ds2.Tables(0).Rows(0).Item("SHIFTREMAINDAYS").ToString() & "', LASTSHIFTPERFORMED  = '" & ds2.Tables(0).Rows(0).Item("LASTSHIFTPERFORMED").ToString() & "', INONLY = '" & ds2.Tables(0).Rows(0).Item("INONLY").ToString() & "', ISPUNCHALL = '" & ds2.Tables(0).Rows(0).Item("ISPUNCHALL").ToString() & "', ISTIMELOSSALLOWED = '" & ds2.Tables(0).Rows(0).Item("ISTIMELOSSALLOWED").ToString() & "', ALTERNATE_OFF_DAYS = '" & ds2.Tables(0).Rows(0).Item("ALTERNATE_OFF_DAYS").ToString() & "', CDAYS = '" & ds2.Tables(0).Rows(0).Item("CDAYS").ToString() & "', ISROUNDTHECLOCKWORK = '" & ds2.Tables(0).Rows(0).Item("ISROUNDTHECLOCKWORK").ToString() & "', ISOT = '" & ds2.Tables(0).Rows(0).Item("ISOT").ToString() & "', OTRATE = '" & ds2.Tables(0).Rows(0).Item("OTRATE").ToString() & "', FIRSTOFFDAY = '" & ds2.Tables(0).Rows(0).Item("FIRSTOFFDAY").ToString() & "', SECONDOFFTYPE = '" & ds2.Tables(0).Rows(0).Item("SECONDOFFTYPE").ToString() & "', HALFDAYSHIFT = '" & ds2.Tables(0).Rows(0).Item("HALFDAYSHIFT").ToString() & "', SECONDOFFDAY = '" & ds2.Tables(0).Rows(0).Item("SECONDOFFDAY").ToString() & "', PERMISLATEARRIVAL = '" & ds2.Tables(0).Rows(0).Item("PERMISLATEARRIVAL").ToString() & "', PERMISEARLYDEPRT = '" & ds2.Tables(0).Rows(0).Item("PERMISEARLYDEPRT").ToString() & "', ISAUTOSHIFT = '" & ds2.Tables(0).Rows(0).Item("ISAUTOSHIFT").ToString() & "', ISOUTWORK = '" & ds2.Tables(0).Rows(0).Item("ISOUTWORK").ToString() & "', MAXDAYMIN = '" & ds2.Tables(0).Rows(0).Item("MAXDAYMIN").ToString() & "', ISOS = '" & ds2.Tables(0).Rows(0).Item("ISOS").ToString() & "', AUTH_SHIFTS = '" & ds2.Tables(0).Rows(0).Item("AUTH_SHIFTS").ToString() & "', TIME1 = '" & ds2.Tables(0).Rows(0).Item("TIME1").ToString() & "', SHORT1 = '" & ds2.Tables(0).Rows(0).Item("SHORT1").ToString() & "', HALF = '" & ds2.Tables(0).Rows(0).Item("HALF").ToString() & "', ISHALFDAY = '" & ds2.Tables(0).Rows(0).Item("ISHALFDAY").ToString() & "', ISSHORT = '" & ds2.Tables(0).Rows(0).Item("ISSHORT").ToString() & "', TWO = '" & ds2.Tables(0).Rows(0).Item("TWO").ToString() & "', MARKMISSASHALFDAY='" & ds2.Tables(0).Rows(0).Item("MARKMISSASHALFDAY").ToString() & "', MSHIFT='" & ds2.Tables(0).Rows(0).Item("MSHIFT").ToString() & "' where CARDNO = '" & PRESENTCARDNO & "'", Common.con1)
                cmd1.ExecuteNonQuery()
                Common.con1.Close()
                If EpId <> PAYCODE Then
                    If Common.con1.State <> ConnectionState.Open Then
                        Common.con1.Open()
                    End If
                    cmd1 = New OleDbCommand("update tblTimeRegister set PAYCODE ='" & PAYCODE & "' where PAYCODE='" & EpId & "'", Common.con1)
                    cmd1.ExecuteNonQuery()


                    cmd1 = New OleDbCommand("update  tblLeaveLedger set PAYCODE ='" & PAYCODE & "' where PAYCODE='" & EpId & "'", Common.con1)
                    cmd1.ExecuteNonQuery()

                    cmd1 = New OleDbCommand("update  tblTimeRegister set PAYCODE ='" & PAYCODE & "' where PAYCODE='" & EpId & "'", Common.con1)
                    cmd1.ExecuteNonQuery()

                    cmd1 = New OleDbCommand("update  MachineRawPunch set PAYCODE ='" & PAYCODE & "' where PAYCODE='" & EpId & "'", Common.con1)
                    cmd1.ExecuteNonQuery()

                    cmd1 = New OleDbCommand("update  LeaveApplication set PAYCODE ='" & PAYCODE & "' where PAYCODE='" & EpId & "'", Common.con1)
                    cmd1.ExecuteNonQuery()

                    cmd1 = New OleDbCommand("update  Pay_Master set PAYCODE ='" & PAYCODE & "' where PAYCODE='" & EpId & "'", Common.con1)
                    cmd1.ExecuteNonQuery()

                    cmd1 = New OleDbCommand("update  PAY_RESULT set PAYCODE ='" & PAYCODE & "' where PAYCODE='" & EpId & "'", Common.con1)
                    cmd1.ExecuteNonQuery()
                    Common.con1.Close()
                End If
            Else
                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                Try
update:             cmd = New SqlCommand("update TblEmployee set ACTIVE = '" & ACTIVE & "', PAYCODE = '" & PAYCODE & "',  EMPNAME = '" & EMPNAME & "',  GUARDIANNAME = '" & GUARDIANNAME & "',  COMPANYCODE = '" & COMPANYCODE & "',  DEPARTMENTCODE = '" & DEPARTMENTCODE & "',  CAT = '" & CAT & "',  EmployeeGroupId = '" & EmployeeGroupId & "',  GradeCode = '" & GradeCode & "',  DESIGNATION = '" & DESIGNATION & "',  Leavingdate = '" & Leavingdate & "',  LeavingReason = '" & LeavingReason & "',  PFNO = '" & PFNO & "',  PF_NO = '" & PF_NO & "',  ESINO = '" & ESINO & "',  DESPANSARYCODE = '" & DESPANSARYCODE & "',  DateOFJOIN = '" & DateOFJOIN & "',  DateOFBIRTH = '" & DateOFBIRTH & "',  ISMARRIED = '" & ISMARRIED & "',  BLOODGROUP = '" & BLOODGROUP & "',  QUALIFICATION = '" & QUALIFICATION & "',  EXPERIENCE = '" & EXPERIENCE & "',  SEX = '" & SEX & "',  bankCODE = '" & bankCODE & "',  BankAcc = '" & BankAcc & "',  E_MAIL1 = '" & E_MAIL1 & "',  BUS = '" & BUS & "',  VehicleNo = '" & VehicleNo & "',  ValidityStartDate = '" & ValidityStartDate & "',  ValidityEndDate = '" & ValidityEndDate & "',  ADDRESS1 = '" & ADDRESS1 & "',  PINCODE1 = '" & PINCODE1 & "',  TELEPHONE1 = '" & TELEPHONE1 & "',  ADDRESS2 = '" & ADDRESS2 & "',  PINCODE2 = '" & PINCODE2 & "',  TELEPHONE2 = '" & TELEPHONE2 & "',  LastModifiedBy = '" & Common.USER_R & "',  LastModifiedDate = '" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "', BRANCHCODE='" & BRANCHCODE & "', EMPPHOTO='" & EMPPHOTO & "', MachineCard='" & MachineCard & "' where PRESENTCARDNO = '" & PRESENTCARDNO & "'", Common.con)
                    cmd.ExecuteNonQuery()
                Catch ex As Exception
                    If ex.Message.ToString.Trim = "String or binary data would be truncated." & vbCrLf & "The statement has been terminated." Then
                        Dim sSql As String = "Alter Table tblemployee" &
                                " ALTER COLUMN telephone1 varchar(200)"
                        cmd = New SqlCommand(sSql, Common.con)
                        cmd.ExecuteNonQuery()
                        GoTo update
                    End If

                End Try

                Common.con.Close()

                adap2 = New SqlDataAdapter("select * from EmployeeGroup where GroupId = '" & EmployeeGroupId & "' ", Common.con)
                ds2 = New DataSet
                adap2.Fill(ds2)


                If Common.con.State <> ConnectionState.Open Then
                    Common.con.Open()
                End If
                cmd = New SqlCommand("Update tblEmployeeShiftMaster set PAYCODE = '" & PAYCODE & "',  SHIFT = '" & ds2.Tables(0).Rows(0).Item("SHIFT").ToString() & "', SHIFTTYPE = '" & ds2.Tables(0).Rows(0).Item("SHIFTTYPE").ToString() & "', SHIFTPATTERN = '" & ds2.Tables(0).Rows(0).Item("SHIFTPATTERN").ToString() & "', SHIFTREMAINDAYS = '" & ds2.Tables(0).Rows(0).Item("SHIFTREMAINDAYS").ToString() & "', LASTSHIFTPERFORMED  = '" & ds2.Tables(0).Rows(0).Item("LASTSHIFTPERFORMED").ToString() & "', INONLY = '" & ds2.Tables(0).Rows(0).Item("INONLY").ToString() & "', ISPUNCHALL = '" & ds2.Tables(0).Rows(0).Item("ISPUNCHALL").ToString() & "', ISTIMELOSSALLOWED = '" & ds2.Tables(0).Rows(0).Item("ISTIMELOSSALLOWED").ToString() & "', ALTERNATE_OFF_DAYS = '" & ds2.Tables(0).Rows(0).Item("ALTERNATE_OFF_DAYS").ToString() & "', CDAYS = '" & ds2.Tables(0).Rows(0).Item("CDAYS").ToString() & "', ISROUNDTHECLOCKWORK = '" & ds2.Tables(0).Rows(0).Item("ISROUNDTHECLOCKWORK").ToString() & "', ISOT = '" & ds2.Tables(0).Rows(0).Item("ISOT").ToString() & "', OTRATE = '" & ds2.Tables(0).Rows(0).Item("OTRATE").ToString() & "', FIRSTOFFDAY = '" & ds2.Tables(0).Rows(0).Item("FIRSTOFFDAY").ToString() & "', SECONDOFFTYPE = '" & ds2.Tables(0).Rows(0).Item("SECONDOFFTYPE").ToString() & "', HALFDAYSHIFT = '" & ds2.Tables(0).Rows(0).Item("HALFDAYSHIFT").ToString() & "', SECONDOFFDAY = '" & ds2.Tables(0).Rows(0).Item("SECONDOFFDAY").ToString() & "', PERMISLATEARRIVAL = '" & ds2.Tables(0).Rows(0).Item("PERMISLATEARRIVAL").ToString() & "', PERMISEARLYDEPRT = '" & ds2.Tables(0).Rows(0).Item("PERMISEARLYDEPRT").ToString() & "', ISAUTOSHIFT = '" & ds2.Tables(0).Rows(0).Item("ISAUTOSHIFT").ToString() & "', ISOUTWORK = '" & ds2.Tables(0).Rows(0).Item("ISOUTWORK").ToString() & "', MAXDAYMIN = '" & ds2.Tables(0).Rows(0).Item("MAXDAYMIN").ToString() & "', ISOS = '" & ds2.Tables(0).Rows(0).Item("ISOS").ToString() & "', AUTH_SHIFTS = '" & ds2.Tables(0).Rows(0).Item("AUTH_SHIFTS").ToString() & "', TIME = '" & ds2.Tables(0).Rows(0).Item("TIME").ToString() & "', SHORT = '" & ds2.Tables(0).Rows(0).Item("SHORT").ToString() & "', HALF = '" & ds2.Tables(0).Rows(0).Item("HALF").ToString() & "', ISHALFDAY = '" & ds2.Tables(0).Rows(0).Item("ISHALFDAY").ToString() & "', ISSHORT = '" & ds2.Tables(0).Rows(0).Item("ISSHORT").ToString() & "', TWO = '" & ds2.Tables(0).Rows(0).Item("TWO").ToString() & "', MARKMISSASHALFDAY='" & ds2.Tables(0).Rows(0).Item("MARKMISSASHALFDAY").ToString() & "', MSHIFT='" & ds2.Tables(0).Rows(0).Item("MSHIFT").ToString() & "'  where CARDNO = '" & PRESENTCARDNO & "'", Common.con)
                cmd.ExecuteNonQuery()
                Common.con.Close()

                If EpId <> PAYCODE Then
                    If Common.con.State <> ConnectionState.Open Then
                        Common.con.Open()
                    End If
                    cmd = New SqlCommand("update tblTimeRegister set PAYCODE ='" & PAYCODE & "' where PAYCODE='" & EpId & "'", Common.con)
                    cmd.ExecuteNonQuery()

                    cmd = New SqlCommand("update  tblLeaveLedger set PAYCODE ='" & PAYCODE & "' where PAYCODE='" & EpId & "'", Common.con)
                    cmd.ExecuteNonQuery()

                    cmd = New SqlCommand("update  tblTimeRegister set PAYCODE ='" & PAYCODE & "' where PAYCODE='" & EpId & "'", Common.con)
                    cmd.ExecuteNonQuery()

                    cmd = New SqlCommand("update  MachineRawPunch set PAYCODE ='" & PAYCODE & "' where PAYCODE='" & EpId & "'", Common.con)
                    cmd.ExecuteNonQuery()

                    cmd = New SqlCommand("update  LeaveApplication set PAYCODE ='" & PAYCODE & "' where PAYCODE='" & EpId & "'", Common.con)
                    cmd.ExecuteNonQuery()

                    cmd = New SqlCommand("update  Pay_Master set PAYCODE ='" & PAYCODE & "' where PAYCODE='" & EpId & "'", Common.con)
                    cmd.ExecuteNonQuery()

                    cmd = New SqlCommand("update  PAY_RESULT set PAYCODE ='" & PAYCODE & "' where PAYCODE='" & EpId & "'", Common.con)
                    cmd.ExecuteNonQuery()

                    Common.con.Close()
                End If

            End If
            Common.LogPost("Employee Update;  Paycode='" & PAYCODE)
        End If

        If isActive = "Y" And ACTIVE = "N" Then 'delete roster id marked as inactive
            Dim sSql As String = "delete from tblTimeRegister where PAYCODE='" & PAYCODE & "' and DateOFFICE > '" & DateEditLeaving.DateTime.ToString("yyyy-MM-dd 00:00:00") & "'"
            If Common.servername = "Access" Then
                sSql = sSql.Replace("DateOFFICE", "FORMAT(DateOFFICE,'yyyy-MM-dd HH:mm:ss')")
                Common.con1.Open()
                cmd1 = New OleDbCommand(sSql, Common.con1)
                cmd1.ExecuteNonQuery()
                Common.con1.Close()
            Else
                Common.con.Open()
                cmd = New SqlCommand(sSql, Common.con)
                cmd.ExecuteNonQuery()
                Common.con.Close()
            End If
        ElseIf isActive = "N" And ACTIVE = "Y" Then 'create roster if marked as active after inactive
            Try
                Dim dateedit As DateTime = Convert.ToDateTime(DOL)
                comclass.DutyRoster(PAYCODE, dateedit.ToString("dd/MMM/yyyy"), g_Wo_Include)
            Catch ex As Exception

            End Try

        End If

        Me.Close()
        Me.Cursor = Cursors.Default
    End Sub
    Private Sub FPCopy() 'Handles SimpleButtonFPCopy.Click
        Dim selectedRows As Integer() = GridViewMachine.GetSelectedRows()
        Dim result As Object() = New Object(selectedRows.Length - 1) {}
        Dim LstMachineId As String
        For i = 0 To selectedRows.Length - 1
            Dim rowHandle As Integer = selectedRows(i)
            If Not GridViewMachine.IsGroupRow(rowHandle) Then
                LstMachineId = GridViewMachine.GetRowCellValue(rowHandle, "ID_NO").ToString.Trim
                Dim sdwEnrollNumber As String
                Dim sName As String = ""
                Dim sPassword As String = ""
                Dim iPrivilege As Integer
                Dim idwFingerIndex As Integer
                Dim sTmpData As String = ""
                Dim sEnabled As String = ""
                Dim bEnabled As Boolean = False
                Dim iflag As Integer

                Dim lpszIPAddress As String = GridViewMachine.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim 'Trim(rstm!Location)
                Dim vpszIPAddress As String = Trim(lpszIPAddress)

                Dim bIsConnected = False
                Dim iMachineNumber As Integer = result(i)
                Dim idwErrorCode As Integer
                Dim com As Common = New Common

                If GridViewMachine.GetRowCellValue(rowHandle, "DeviceType").ToString.Trim = "Bio-1/3/4/5/6/7/ATF-203/ATF-395/ACR-872" Then
                    Dim bConn As Boolean = False
                    Dim vnLicense As Long = 1261 '1789 '
                    lpszIPAddress = GridViewMachine.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim 'Trim(rstm!Location)
                    vpszIPAddress = Trim(lpszIPAddress)
                    Dim vpszNetPort As Long = CLng("5005")
                    Dim vpszNetPassword As Long = CLng("0")
                    Dim vnTimeOut As Long = CLng("5000")
                    Dim vnProtocolType As Long = 0 'PROTOCOL_TCPIP
                    Dim vRet As Long
                    'bConn = FK623Attend.ConnectNet(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
                    If GridViewMachine.GetRowCellValue(rowHandle, "A_R").ToString.Trim = "S" Then
                        vRet = FK_ConnectUSB(iMachineNumber, vnLicense)
                    Else
                        vpszIPAddress = Trim(lpszIPAddress)
                        vpszNetPort = CLng("5005")
                        vpszNetPassword = CLng("0")
                        vnTimeOut = CLng("5000")
                        vnProtocolType = 0
                        vRet = FK_ConnectNet(iMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
                    End If
                    If vRet > 0 Then
                        bConn = True
                    Else
                        bConn = False
                    End If
                    If bConn Then
                        Dim selectedRowsEmp As Integer() = GridView2.GetSelectedRows()
                        Dim resultEmp As Object() = New Object(selectedRowsEmp.Length - 1) {}

                        Dim vStrEnrollNumber As String = Trim(TextEditUserNo.Text)
                        Dim vEnrollNumber As Long = 0 ' Convert.ToUInt32(vStrEnrollNumber)
                        If IsNumeric(vStrEnrollNumber) Then
                            vEnrollNumber = Convert.ToUInt32(vStrEnrollNumber)
                            vStrEnrollNumber = Convert.ToUInt32(vStrEnrollNumber)
                        Else
                            vStrEnrollNumber = Trim(TextEditUserNo.Text)
                        End If
                        If vEnrollNumber = 0 Then
                            XtraMessageBox.Show(ulf, "<size=10>Device No: " & LstMachineId & " Does not support User No. " & Trim(TextEditUserNo.Text) & "</size>", "Information")
                            FK_DisConnect(vRet)
                            Continue For
                        End If
                        If FK_GetIsSupportStringID(vRet) = RUN_SUCCESS Then
                            vRet = FK_SetUserName_StringID(vRet, vStrEnrollNumber, Trim(TextEditName.Text))
                        Else
                            vRet = FK_SetUserName(vRet, vEnrollNumber, Trim(TextEditName.Text))
                        End If

                        If CheckValidity.Checked = True Then
                            'finger validity
                            vRet = FK_EnableDevice(vRet, 0)
                            If (vRet <> CType(RUN_SUCCESS, Integer)) Then
                                Continue For
                            End If
                            Dim vSize As Integer = (20 + 264)
                            Dim bytExtCmd_USERDOORINFO() As Byte = New Byte((vSize) - 1) {}
                            Dim vExtCmd_USERDOORINFO As Common.ExtCmd_USERDOORINFO = New Common.ExtCmd_USERDOORINFO
                            vStrEnrollNumber = vEnrollNumber.ToString.Trim
                            'vEnrollNumber = FKAttendDLL.GetInt(vStrEnrollNumber)
                            vExtCmd_USERDOORINFO.Init("ECMD_SetUserDoorInfo", vEnrollNumber)
                            vExtCmd_USERDOORINFO.StartYear = CType(DateEditVStrt.DateTime.ToString("yyyy"), Short)
                            vExtCmd_USERDOORINFO.StartMonth = CType(DateEditVStrt.DateTime.ToString("MM"), Byte)
                            vExtCmd_USERDOORINFO.StartDay = CType(DateEditVStrt.DateTime.ToString("dd"), Byte)
                            vExtCmd_USERDOORINFO.EndYear = CType(DateEditVEnd.DateTime.ToString("yyyy"), Short)
                            vExtCmd_USERDOORINFO.EndMonth = CType(DateEditVEnd.DateTime.ToString("MM"), Byte)
                            vExtCmd_USERDOORINFO.EndDay = CType(DateEditVEnd.DateTime.ToString("dd"), Byte)
                            ConvertStructureToByteArray(vExtCmd_USERDOORINFO, bytExtCmd_USERDOORINFO)
                            vRet = FK_ExtCommand(vRet, bytExtCmd_USERDOORINFO)
                            'vRet = FK_EnableDevice(vRet, 1)
                            'End finger validity



                            'TimeZone Sart 
                            Dim bytUserPassTime(SIZE_USER_WEEK_PASS_TIME_STRUCT - 1) As Byte
                            Dim vUserPassTime As USER_WEEK_PASS_TIME
                            Dim nFKRetCode As Integer
                            vUserPassTime.Init()
                            GetUserWeekPassTimeValue(vStrEnrollNumber, vUserPassTime)
                            'nFKRetCode = FK_EnableDevice(vRet, 0)
                            If nFKRetCode <> RUN_SUCCESS Then
                                Return
                            End If
                            ConvertStructureToByteArray(vUserPassTime, bytUserPassTime)
                            nFKRetCode = FK_HS_SetUserWeekPassTime(vRet, bytUserPassTime)
                            If nFKRetCode = RUN_SUCCESS Then
                                'lblMessage.Text = "Success !"
                            Else
                                'lblMessage.Text = ReturnResultPrint(nFKRetCode)
                                ' XtraMessageBox.Show(ulf, "<size=10>" & ReturnResultPrint(nFKRetCode) & "</size>", "Error")
                                Exit Sub
                            End If
                            'TimeZone End
                            FK_EnableDevice(vRet, 1)
                        End If
                        'cmdSetAllEnrollData_bio_All(iMachineNumber, TextEdit1.Text.Trim, 0)
                    Else
                        XtraMessageBox.Show(ulf, "<size=10>Device No: " & LstMachineId & " Not connected..</size>", "Information")
                        Continue For
                    End If
                    'FK623Attend.Disconnect
                    FK_DisConnect(vRet)
                ElseIf GridViewMachine.GetRowCellValue(rowHandle, "DeviceType").ToString.Trim = "ZK(TFT)" Or GridViewMachine.GetRowCellValue(rowHandle, "DeviceType").ToString.Trim = "Bio-1Pro/ATF305Pro/ATF686Pro" Then
                    Dim axCZKEM1 As New zkemkeeper.CZKEM
                    bIsConnected = axCZKEM1.Connect_Net(vpszIPAddress, 4370)
                    If bIsConnected = True Then
                        axCZKEM1.EnableDevice(iMachineNumber, False)
                        XtraMasterTest.LabelControlStatus.Text = "Uploading Template " & TextEditUserNo.Text.Trim
                        Application.DoEvents()
                        Dim strCardno = ""
                        Try
                            sdwEnrollNumber = Convert.ToDouble(TextEditUserNo.Text.Trim)
                        Catch ex As Exception
                            sdwEnrollNumber = TextEditUserNo.Text.Trim
                        End Try

                        sEnabled = "true" 'RsFp.Tables(0).Rows(0)(6).ToString().Trim()
                        Dim ret = axCZKEM1.SSR_GetUserInfo(iMachineNumber, sdwEnrollNumber, sName, sPassword, iPrivilege, bEnabled)  'get the user info if user is already in device
                        If ret = False Then
                            sPassword = "" 'RsFp.Tables(0).Rows(0)("Password").ToString().Trim()
                        Else
                            axCZKEM1.GetStrCardNumber(strCardno)
                            If strCardno = "0" Then
                                strCardno = ""
                            End If
                        End If
                        sTmpData = "" 'strBase64 'Encoding.UTF8.GetString(dsRecord.Tables(0).Rows(0).Item("vFingerImage"), 0, dsRecord.Tables(0).Rows(0).Item("vFingerImage").Length) 'dsRecord.Tables(0).Rows(0).Item("vFingerImage")
                        iPrivilege = 0 'Convert.ToInt32(RsFp.Tables(0).Rows(0)("Privilege").ToString().Trim()) 'told by ajitesh(used for admin)
                        idwFingerIndex = 6 ' Convert.ToInt32(RsFp.Tables(0).Rows(0)("FingerNumber").ToString().Trim())
                        sName = TextEditName.Text.Trim ' dstmp.Tables(0).Rows(0)("EMPNAME").ToString().Trim()
                        iflag = 0 'Convert.ToInt32(RsFp.Tables(0).Rows(0)(7).ToString())' as per old s/w  
                        'Dim icard As String = "" 'RsFp.Tables(0).Rows(0)("CardNumber").ToString().Trim()
                        If sEnabled.ToString().ToLower() = "true" Then
                            bEnabled = True
                        Else
                            bEnabled = False
                        End If
                        Dim iBackupNumber As String
                        Dim rs As Integer
                        If TextEditCard.Text.Trim <> "" Then
                            axCZKEM1.SetStrCardNumber(TextEditCard.Text.Trim)
                        Else
                            axCZKEM1.SetStrCardNumber(strCardno)
                        End If
                        If axCZKEM1.SSR_SetUserInfo(iMachineNumber, sdwEnrollNumber, sName, sPassword, iPrivilege, bEnabled) Then 'upload user information to the device
                            rs = axCZKEM1.SetUserTmpExStr(iMachineNumber, sdwEnrollNumber, idwFingerIndex, iflag, sTmpData) 'upload templates information to the device
                            If CheckValidity.Checked = True Then
                                Try
                                    Dim x = axCZKEM1.SetUserValidDate(iMachineNumber, sdwEnrollNumber, 1, 0, DateEditVStrt.DateTime.ToString("yyyy-MM-dd HH:mm:00"), DateEditVEnd.DateTime.ToString("yyyy-MM-dd HH:mm:59"))  'add valdity
                                    axCZKEM1.RefreshData(iMachineNumber)
                                Catch ex As Exception
                                End Try
                            End If
                        Else
                            axCZKEM1.GetLastError(idwErrorCode)
                            axCZKEM1.EnableDevice(iMachineNumber, True)
                            axCZKEM1.Disconnect()
                            Return
                        End If
                        axCZKEM1.EnableDevice(iMachineNumber, True)
                        axCZKEM1.Disconnect()
                    Else
                        axCZKEM1.GetLastError(idwErrorCode)
                        axCZKEM1.Disconnect()
                        Continue For
                    End If
                ElseIf GridViewMachine.GetRowCellValue(rowHandle, "DeviceType").ToString.Trim = "F9" Then
                    Dim bConn As Boolean = False
                    Dim vnLicense As Long = 7881 '1789 '
                    lpszIPAddress = GridViewMachine.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim 'Trim(rstm!Location)
                    vpszIPAddress = Trim(lpszIPAddress)
                    Dim vpszNetPort As Long = CLng("5005")
                    Dim vpszNetPassword As Long = CLng("0")
                    Dim vnTimeOut As Long = CLng("5000")
                    Dim vnProtocolType As Long = 0 'PROTOCOL_TCPIP
                    Dim vRet As Long
                    Dim F9 As mdlPublic_f9 = New mdlPublic_f9
                    'bConn = FK623Attend.ConnectNet(vnMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
                    If GridViewMachine.GetRowCellValue(rowHandle, "A_R").ToString.Trim = "S" Then
                        vRet = F9.FK_ConnectUSB(iMachineNumber, vnLicense)
                    Else
                        vpszIPAddress = Trim(lpszIPAddress)
                        vpszNetPort = CLng("5005")
                        vpszNetPassword = CLng("0")
                        vnTimeOut = CLng("5000")
                        vnProtocolType = 0
                        vRet = F9.FK_ConnectNet(iMachineNumber, vpszIPAddress, vpszNetPort, vnTimeOut, vnProtocolType, vpszNetPassword, vnLicense)
                    End If
                    If vRet > 0 Then
                        bConn = True
                    Else
                        bConn = False
                    End If
                    If bConn Then
                        Dim selectedRowsEmp As Integer() = GridView2.GetSelectedRows()
                        Dim resultEmp As Object() = New Object(selectedRowsEmp.Length - 1) {}

                        Dim vStrEnrollNumber As String = Trim(TextEditUserNo.Text)
                        Dim vEnrollNumber As Long = 0 ' Convert.ToUInt32(vStrEnrollNumber)
                        If IsNumeric(vStrEnrollNumber) Then
                            vEnrollNumber = Convert.ToUInt32(vStrEnrollNumber)
                            vStrEnrollNumber = Convert.ToUInt32(vStrEnrollNumber)
                        Else
                            vStrEnrollNumber = Trim(TextEditUserNo.Text)
                        End If
                        If vEnrollNumber = 0 Then
                            XtraMessageBox.Show(ulf, "<size=10>Device No: " & LstMachineId & " Does not support User No. " & Trim(TextEditUserNo.Text) & "</size>", "Information")
                            F9.FK_DisConnect(vRet)
                            Continue For
                        End If
                        If F9.FK_GetIsSupportStringID(vRet) = RUN_SUCCESS Then
                            vRet = F9.FK_SetUserName_StringID(vRet, vStrEnrollNumber, Trim(TextEditName.Text))
                        Else
                            vRet = F9.FK_SetUserName(vRet, vEnrollNumber, Trim(TextEditName.Text))
                        End If

                        If CheckValidity.Checked = True Then
                            'finger validity
                            vRet = F9.FK_EnableDevice(vRet, 0)
                            If (vRet <> CType(RUN_SUCCESS, Integer)) Then
                                Continue For
                            End If
                            Dim vSize As Integer = (20 + 264)
                            Dim bytExtCmd_USERDOORINFO() As Byte = New Byte((vSize) - 1) {}
                            Dim vExtCmd_USERDOORINFO As Common.ExtCmd_USERDOORINFO = New Common.ExtCmd_USERDOORINFO
                            vStrEnrollNumber = vEnrollNumber.ToString.Trim
                            'vEnrollNumber = FKAttendDLL.GetInt(vStrEnrollNumber)
                            vExtCmd_USERDOORINFO.Init("ECMD_SetUserDoorInfo", vEnrollNumber)
                            vExtCmd_USERDOORINFO.StartYear = CType(DateEditVStrt.DateTime.ToString("yyyy"), Short)
                            vExtCmd_USERDOORINFO.StartMonth = CType(DateEditVStrt.DateTime.ToString("MM"), Byte)
                            vExtCmd_USERDOORINFO.StartDay = CType(DateEditVStrt.DateTime.ToString("dd"), Byte)
                            vExtCmd_USERDOORINFO.EndYear = CType(DateEditVEnd.DateTime.ToString("yyyy"), Short)
                            vExtCmd_USERDOORINFO.EndMonth = CType(DateEditVEnd.DateTime.ToString("MM"), Byte)
                            vExtCmd_USERDOORINFO.EndDay = CType(DateEditVEnd.DateTime.ToString("dd"), Byte)
                            ConvertStructureToByteArray(vExtCmd_USERDOORINFO, bytExtCmd_USERDOORINFO)
                            vRet = F9.FK_ExtCommand(vRet, bytExtCmd_USERDOORINFO)
                            vRet = F9.FK_EnableDevice(vRet, 1)
                            'End finger validity
                        End If
                        'cmdSetAllEnrollData_bio_All(iMachineNumber, TextEdit1.Text.Trim, 0)
                    Else
                        XtraMessageBox.Show(ulf, "<size=10>Device No: " & LstMachineId & " Not connected..</size>", "Information")
                        Continue For
                    End If
                    'FK623Attend.Disconnect
                    F9.FK_DisConnect(vRet)
                ElseIf GridViewMachine.GetRowCellValue(rowHandle, "DeviceType").ToString.Trim = "TF-01" Then
                    Dim bConn As Boolean = False
                    Dim vnLicense As Long = 7881 '1789 '
                    lpszIPAddress = GridViewMachine.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim 'Trim(rstm!Location)
                    vpszIPAddress = Trim(lpszIPAddress)
                    Dim vpszNetPort As Long = CLng("5005")
                    Dim vpszNetPassword As Long = CLng("0")
                    Dim vnTimeOut As Long = CLng("5000")
                    Dim vnProtocolType As Long = 0 'PROTOCOL_TCPIP
                    Dim vRet As Long

                    bConn = AxFP_CLOCK1.SetIPAddress(lpszIPAddress, vpszNetPort, vpszNetPassword)
                    Me.AxFP_CLOCK1.OpenCommPort(LstMachineId)
                    If bConn Then
                        Dim selectedRowsEmp As Integer() = GridView2.GetSelectedRows()
                        Dim resultEmp As Object() = New Object(selectedRowsEmp.Length - 1) {}
                        bConn = AxFP_CLOCK1.EnableDevice(LstMachineId, False)
                        Dim vStrEnrollNumber As String = Trim(TextEditUserNo.Text)
                        Dim vEnrollNumber As Long = 0 ' Convert.ToUInt32(vStrEnrollNumber)
                        If IsNumeric(vStrEnrollNumber) Then
                            vEnrollNumber = Convert.ToUInt32(vStrEnrollNumber)
                            vStrEnrollNumber = Convert.ToUInt32(vStrEnrollNumber)
                        Else
                            vStrEnrollNumber = Trim(TextEditUserNo.Text)
                        End If
                        If vEnrollNumber = 0 Then
                            XtraMessageBox.Show(ulf, "<size=10>Device No: " & LstMachineId & " Does not support User No. " & Trim(TextEditUserNo.Text) & "</size>", "Information")
                            AxFP_CLOCK1.EnableDevice(LstMachineId, True)
                            AxFP_CLOCK1.CloseCommPort()
                            Continue For
                        End If

                        Dim obj As Object
                        obj = New System.Runtime.InteropServices.VariantWrapper(Trim(TextEditName.Text))
                        vStrEnrollNumber = vEnrollNumber
                        bConn = AxFP_CLOCK1.SetUserName(0, LstMachineId, vStrEnrollNumber, LstMachineId, obj)
                        If CheckValidity.Checked = True Then
                            Dim dtSart As DateTime = DateEditVStrt.DateTime
                            Dim dtEnd As DateTime = DateEditVEnd.DateTime
                            bConn = AxFP_CLOCK1.SetUserCtrl(LstMachineId, vStrEnrollNumber, 0, 0, dtSart.Year, dtSart.Month, dtSart.Day, dtEnd.Year, dtEnd.Month, dtEnd.Day)

                        End If

                    Else
                        XtraMessageBox.Show(ulf, "<size=10>Device No: " & LstMachineId & " Not connected..</size>", "Information")
                        Continue For
                    End If
                    'FK623Attend.Disconnect
                    AxFP_CLOCK1.EnableDevice(LstMachineId, True)
                    AxFP_CLOCK1.CloseCommPort()

                ElseIf GridViewMachine.GetRowCellValue(rowHandle, "DeviceType").ToString.Trim = "HTSeries" Then
                    LstMachineId = GridViewMachine.GetRowCellValue(rowHandle, "ID_NO").ToString.Trim
                    Dim bConn = False
                    Dim vnMachineNumber = LstMachineId
                    Dim vnLicense = 1261 '1789 '
                    lpszIPAddress = GridViewMachine.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim 'Trim(rstm!Location)
                    vpszIPAddress = Trim(lpszIPAddress)
                    Dim struDeviceInfoV40 As CHCNetSDK.NET_DVR_DEVICEINFO_V40 = New CHCNetSDK.NET_DVR_DEVICEINFO_V40
                    Dim DeviceAdd As String = vpszIPAddress
                    Dim userName As String = Trim(GridViewMachine.GetRowCellValue(rowHandle, "HLogin").ToString.Trim)
                    Dim pwd As String = Trim(GridViewMachine.GetRowCellValue(rowHandle, "HPassword").ToString.Trim)
                    Dim lUserID As Integer = -1
                    Dim failReason As String = ""
                    Dim logistatus = com.HikvisionLogin(DeviceAdd, userName, pwd, struDeviceInfoV40, lUserID, failReason)
                    If logistatus = False Then
                        For Each c As Control In Me.Controls
                            c.Enabled = True
                        Next
                        If failReason <> "" Then
                            XtraMessageBox.Show(ulf, "<size=10>" & failReason & "</size>", "Failed")
                            XtraMasterTest.LabelControlStatus.Text = ""
                            Application.DoEvents()
                            Me.Cursor = Cursors.Default
                            Exit Sub
                        End If
                    Else
                        If failReason <> "" Then
                            XtraMessageBox.Show(ulf, "<size=10>" & failReason & "</size>", "Failed")
                            XtraMasterTest.LabelControlStatus.Text = ""
                            Application.DoEvents()
                            Me.Cursor = Cursors.Default
                            logistatus = com.HikvisionLogOut(lUserID)
                            Exit Sub
                        End If
                        Dim mUser_ID As String = TextEditUserNo.Text.Trim 'GridView2.GetRowCellValue(rowHandleEmp, "EnrollNumber").ToString.Trim()
                        'Dim tmpUserId As String
                        If IsNumeric(mUser_ID) Then
                            mUser_ID = Convert.ToDouble(mUser_ID) '.ToString("000000000000")
                        Else
                            mUser_ID = mUser_ID
                        End If
                        XtraMasterTest.LabelControlStatus.Text = "Uploading User " & mUser_ID
                        Application.DoEvents()
                        'for user
                        'If GridView2.GetRowCellValue(rowHandleEmp, "FingerNumber").ToString.Trim() = "11" Then
                        'for user copy only
                        Dim ValidityStartdate As DateTime
                        Dim ValidityEnddate As DateTime
                        Dim adapZ As SqlDataAdapter
                        Dim adapAZ As OleDbDataAdapter

                        'Dim sName As String
                        'Dim FpNo As String = GridView2.GetRowCellValue(rowHandleEmp, "FingerNumber").ToString.Trim()
                        Dim Privilege As Integer = 0
                        Dim Cardnumber As String = TextEditCard.Text.Trim 'GridView2.GetRowCellValue(rowHandleEmp, "Cardnumber").ToString.Trim()
                        Dim Password As String = TextEditPassword.Text.Trim 'GridView2.GetRowCellValue(rowHandleEmp, "Password").ToString.Trim()
                        Try
                            Privilege = ComboBoxEditPriV.SelectedIndex 'GridView2.GetRowCellValue(rowHandleEmp, "Privilege")
                        Catch ex As Exception
                        End Try

                        'Dim sdwEnrollNumber As String
                        If IsNumeric(mUser_ID) = True Then
                            sdwEnrollNumber = Convert.ToDouble(mUser_ID)
                        Else
                            sdwEnrollNumber = mUser_ID
                        End If

                        sName = TextEditName.Text.Trim ' dstmp.Tables(0).Rows(0)("EMPNAME").ToString().Trim()
                        ValidityStartdate = DateEditVStrt.DateTime 'Convert.ToDateTime(dstmp.Tables(0).Rows(0)("ValidityStartdate").ToString().Trim())
                        ValidityEnddate = DateEditVEnd.DateTime 'Convert.ToDateTime(dstmp.Tables(0).Rows(0)("ValidityEnddate").ToString().Trim())

                        If (m_lSetUserCfgHandle <> -1) Then
                            If CHCNetSDK.NET_DVR_StopRemoteConfig(m_lSetUserCfgHandle) Then
                                m_lSetUserCfgHandle = -1
                            End If
                        End If
                        Dim sURL As String = "PUT /ISAPI/AccessControl/UserInfo/SetUp?format=json"
                        Dim ptrURL As IntPtr = Marshal.StringToHGlobalAnsi(sURL)
                        m_lSetUserCfgHandle = CHCNetSDK.NET_DVR_StartRemoteConfig(lUserID, CHCNetSDK.NET_DVR_JSON_CONFIG, ptrURL, sURL.Length, Nothing, IntPtr.Zero)
                        If (m_lSetUserCfgHandle < 0) Then
                            'MessageBox.Show(("NET_DVR_StartRemoteConfig fail [url:PUT /ISAPI/AccessControl/UserInfo/SetUp?format=json] error:" + CHCNetSDK.NET_DVR_GetLastError))
                            Marshal.FreeHGlobal(ptrURL)
                            Continue For
                        Else
                            SendUserInfo(sdwEnrollNumber, sName, ValidityStartdate, ValidityEnddate, Privilege, Cardnumber, Password)
                            Marshal.FreeHGlobal(ptrURL)
                        End If
                        'user end

                        'card
                        If TextEditCard.Text.Trim <> "" Then
                            If m_lSetCardCfgHandle <> -1 Then
                                If CHCNetSDK.NET_DVR_StopRemoteConfig(m_lSetCardCfgHandle) Then
                                    m_lSetCardCfgHandle = -1
                                End If
                            End If

                            sURL = "PUT /ISAPI/AccessControl/CardInfo/SetUp?format=json"
                            ptrURL = Marshal.StringToHGlobalAnsi(sURL)
                            m_lSetCardCfgHandle = CHCNetSDK.NET_DVR_StartRemoteConfig(lUserID, CHCNetSDK.NET_DVR_JSON_CONFIG, ptrURL, sURL.Length, Nothing, IntPtr.Zero)

                            If m_lSetCardCfgHandle < 0 Then
                                'MessageBox.Show("NET_DVR_StartRemoteConfig fail [url:PUT /ISAPI/AccessControl/CardInfo/SetUp?format=json] error:" & CHCNetSDK.NET_DVR_GetLastError())
                                Marshal.FreeHGlobal(ptrURL)
                                Return
                            Else
                                SendCardData(mUser_ID, Cardnumber)
                                Marshal.FreeHGlobal(ptrURL)
                            End If
                        End If
                        'end card

                        'If CheckEditTemplate.Checked = True Then
                        '    EnrollFace(lUserID, mUser_ID)
                        'End If
                        'disconnect
                        com.HikvisionLogOut(lUserID)
                    End If
                End If
                If TextEditCard.Text.Trim <> "" Then
                    Dim sqlinsert As String = "select Emachinenumber from fptable where EnrollNumber='" & TextEditUserNo.Text.Trim & "' and Emachinenumber='" & LstMachineId & "' and FingerNumber ='11'"
                    ds = New DataSet
                    If Common.servername = "Access" Then
                        adapA = New OleDbDataAdapter(sqlinsert, Common.con1)
                        adapA.Fill(ds)
                    Else
                        adap = New SqlDataAdapter(sqlinsert, Common.con)
                        adap.Fill(ds)
                    End If
                    If ds.Tables(0).Rows.Count > 0 Then
                        sqlinsert = "update fptable set CardNumber='" & TextEditCard.Text.Trim & "' where EnrollNumber='" & TextEditUserNo.Text.Trim & "' and Emachinenumber='" & LstMachineId & "' and FingerNumber ='11'"
                        If Common.servername = "Access" Then
                            Common.con1.Open()
                            cmd1 = New OleDbCommand(sqlinsert, Common.con1)
                            cmd1.ExecuteNonQuery()
                            Common.con1.Close()
                        Else
                            Common.con.Open()
                            cmd = New SqlCommand(sqlinsert, Common.con)
                            cmd.ExecuteNonQuery()
                            Common.con.Close()
                        End If
                    Else
                        sqlinsert = "insert into fptable (Emachinenumber, EnrollNumber, UserName, CardNumber, FingerNumber, Privilege) values ('" & LstMachineId & "','" & TextEditUserNo.Text.Trim & "', '" & sName.ToString() & "','" & TextEditCard.Text.Trim & "', '11', '" & iPrivilege.ToString() & "')"
                        If Common.servername = "Access" Then
                            Common.con1.Open()
                            cmd1 = New OleDbCommand(sqlinsert, Common.con1)
                            cmd1.ExecuteNonQuery()
                            Common.con1.Close()
                        Else
                            Common.con.Open()
                            cmd = New SqlCommand(sqlinsert, Common.con)
                            cmd.ExecuteNonQuery()
                            Common.con.Close()
                        End If
                    End If

                End If
            End If

        Next
        XtraMasterTest.LabelControlStatus.Text = ""
        Application.DoEvents()
    End Sub
    Private Sub GetUserWeekPassTimeValue(ByVal EnrollNumber As String, ByRef aUserWeekPassTime As USER_WEEK_PASS_TIME)
        aUserWeekPassTime.UserId = EnrollNumber 'GetInt(txtEnrollNumber.Text)
        aUserWeekPassTime.WeekPassTime(0) = CByte(GetInt("1"))
        aUserWeekPassTime.WeekPassTime(1) = CByte(GetInt("1"))
        aUserWeekPassTime.WeekPassTime(2) = CByte(GetInt("1"))
        aUserWeekPassTime.WeekPassTime(3) = CByte(GetInt("1"))
        aUserWeekPassTime.WeekPassTime(4) = CByte(GetInt("1"))
        aUserWeekPassTime.WeekPassTime(5) = CByte(GetInt("1"))
        aUserWeekPassTime.WeekPassTime(6) = CByte(GetInt("1"))
    End Sub
    Private Sub cmdSetAllEnrollData_bio_All(mMachine_ID As Long, mUser_ID As String, FpNo As Long)
        Dim mnCommHandleIndex As Long
        Dim vEnrollNumber As Long
        Dim vEMachineNumber As Long
        Dim vFingerNumber As Long
        Dim vPrivilege As Long
        Dim vFlag As Boolean
        Dim vRet As Long
        Dim vnIsSuppoterd As Long
        'Dim RsFp As New DataSet 'ADODB.Recordset
        Dim vbytConvFpData() As Byte
        Dim vbytEnrollData() As Byte
        Dim vStrEnrollNumber As String
        Dim vnResultCode As Long
        Dim verifymode As Long
        Dim Uname As String
        Dim mMachineNumber As String = mMachine_ID

        Const PWD_DATA_SIZE = 40
        Const FP_DATA_SIZE = 1680
        Const FACE_DATA_SIZE = 20080
        Const VEIN_DATA_SIZE = 3080
        Dim mbytEnrollData(FACE_DATA_SIZE - 1) As Byte
        Dim mlngPasswordData As Long = 0


        vFlag = False
        'Dim adap As SqlDataAdapter
        'Dim adapA As OleDbDataAdapter
        vEMachineNumber = mMachine_ID 'RsFp.Tables(0).Rows(0).Item("EMachineNumber").ToString.Trim '!EMachineNumber
        vEnrollNumber = mUser_ID ' RsFp.Tables(0).Rows(0).Item("EnrollNumber").ToString.Trim '!EnrollNumber
        vFingerNumber = FpNo ' RsFp.Tables(0).Rows(0).Item("FingerNumber").ToString.Trim '!FingerNumber
        vPrivilege = 0

        'verifymode = IIf(IsNull(!verifymode), 0, !verifymode)
        verifymode = 0

        'If vFingerNumber = BACKUP_PSW Or vFingerNumber = BACKUP_CARD Then
        '    vbytEnrollData = RsFp.Tables(0).Rows(0).Item("Template_Tw")
        '    Array.Copy(vbytEnrollData, mbytEnrollData, PWD_DATA_SIZE)
        'ElseIf vFingerNumber >= BACKUP_FP_0 And vFingerNumber <= BACKUP_FP_9 Then
        '    vbytConvFpData = RsFp.Tables(0).Rows(0).Item("Template_Tw")
        '    ReDim vbytEnrollData(FP_DATA_SIZE - 1)
        '    ConvFpDataAfterReadFromDbForCompatibility(vbytConvFpData, vbytEnrollData)
        '    Array.Copy(vbytEnrollData, mbytEnrollData, FP_DATA_SIZE)
        'ElseIf vFingerNumber = BACKUP_FACE Then
        '    vbytEnrollData = RsFp.Tables(0).Rows(0).Item("Template_Tw")
        '    Array.Copy(vbytEnrollData, mbytEnrollData, FACE_DATA_SIZE)
        'ElseIf vFingerNumber = BACKUP_VEIN_0 Then
        '    vbytEnrollData = RsFp.Tables(0).Rows(0).Item("Template_Tw")
        '    Array.Copy(vbytEnrollData, mbytEnrollData, VEIN_DATA_SIZE)
        'End If

        'Array.Copy(vbytEnrollData, mbytEnrollData, FP_DATA_SIZE)
        vnResultCode = FK_EnableDevice(mnCommHandleIndex, 0)
        If vnResultCode <> RUN_SUCCESS Then
            'lblMessage.Caption = gstrNoDevice
            'cmdSetAllEnrollData.Enabled = True
            Exit Sub
        End If

        FK_IsSupportedEnrollData(mnCommHandleIndex, vFingerNumber, vnIsSuppoterd)
        If vnIsSuppoterd = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>Template Not supported</size>", "<size=9>Information</size>")
            vRet = FK_EnableDevice(mnCommHandleIndex, 1)
            GoTo EEE
        End If
        vnResultCode = FK_PutEnrollData(mnCommHandleIndex,
                                              vEnrollNumber,
                                              vFingerNumber,
                                              vPrivilege,
                                              mbytEnrollData,
                                              mlngPasswordData)
        If vnResultCode = RUN_SUCCESS Then
            'lblMessage.Caption = "Saving..."
            'DoEvents
            vnResultCode = FK_SaveEnrollData(mnCommHandleIndex)
            If vnResultCode = RUN_SUCCESS Then
                'lblMessage.Caption = "SetAllEnrollData OK"
            Else
                'lblMessage.Caption = ReturnResultPrint(vnResultCode)
            End If
        Else
            GoTo LLL
        End If
        vnResultCode = 0
        'vnResultCode = FK623Attend.SetUserName(vEnrollNumber, Trim(!UserName))
        Uname = TextEditName.Text.Trim 'RsFp.Tables(0).Rows(0).Item("Username") 'Trim(!Username)
        vStrEnrollNumber = vEnrollNumber
        If FK_GetIsSupportStringID(mnCommHandleIndex) = RUN_SUCCESS Then
            vnResultCode = FK_SetUserName_StringID(mnCommHandleIndex, vStrEnrollNumber, Uname)
        Else
            vnResultCode = FK_SetUserName(mnCommHandleIndex, vEnrollNumber, Uname)
        End If
LLL:
        'End If
EEE:
        FK_EnableDevice(mnCommHandleIndex, 1)
    End Sub
    Private Sub ToggleSwitch1_Toggled(sender As System.Object, e As System.EventArgs) Handles ToggleSwitch1.Toggled
        If ToggleSwitch1.IsOn Then
            DateEditLeaving.Enabled = False
            TextEdit6.Enabled = False
        ElseIf ToggleSwitch1.IsOn = False Then
            DateEditLeaving.Enabled = True
            TextEdit6.Enabled = True
        End If
    End Sub
    Private Sub TextEdit1_Leave(sender As System.Object, e As System.EventArgs) Handles TextEditUserNo.Leave
        If TextEditUserNo.Text.Trim <> "" Then
            If IsNumeric(TextEditUserNo.Text) Then
                Dim x As Long = TextEditUserNo.Text
                TextEditUserNo.Text = x.ToString("000000000000")
                TextEdit2.Text = Convert.ToDouble(TextEditUserNo.Text)
            Else
                TextEdit2.Text = TextEditUserNo.Text
            End If
        End If
    End Sub
    Private Sub GridLookUpEdit1_CustomDisplayText(sender As System.Object, e As DevExpress.XtraEditors.Controls.CustomDisplayTextEventArgs) Handles GridLookUpEdit1.CustomDisplayText
        Try
            Dim gridLookUpEdit = CType(sender, GridLookUpEdit)
            Dim dataRowView = CType(gridLookUpEdit.Properties.GetRowByKeyValue(e.Value), DataRowView)
            Dim row = CType(dataRowView.Row, DataRow)
            e.DisplayText = (row("COMPANYCODE").ToString + ("  " + row("COMPANYNAME").ToString))
        Catch
            'MsgBox("Catch")
        End Try
    End Sub
    Private Sub GridLookUpEdit2_CustomDisplayText(sender As System.Object, e As DevExpress.XtraEditors.Controls.CustomDisplayTextEventArgs) Handles GridLookUpEdit2.CustomDisplayText
        Try
            Dim gridLookUpEdit = CType(sender, GridLookUpEdit)
            Dim dataRowView = CType(gridLookUpEdit.Properties.GetRowByKeyValue(e.Value), DataRowView)
            Dim row = CType(dataRowView.Row, DataRow)
            e.DisplayText = (row("DEPARTMENTCODE").ToString + ("  " + row("DEPARTMENTNAME").ToString))
        Catch
            'MsgBox("Catch")
        End Try

        'Dim edit As GridLookUpEdit = CType(sender, GridLookUpEdit)
        'Dim theIndex As Integer = edit.Properties.GetIndexByKeyValue(edit.EditValue)
        'If edit.Properties.View.IsDataRow(theIndex) Then
        '    Dim row As DataRow = edit.Properties.View.GetDataRow(theIndex)
        '    e.DisplayText = (row("DEPARTMENTCODE").ToString + ("  " + row("DEPARTMENTNAME").ToString))
        'End If
    End Sub
    Private Sub GridLookUpEdit3_CustomDisplayText(sender As System.Object, e As DevExpress.XtraEditors.Controls.CustomDisplayTextEventArgs) Handles GridLookUpEdit3.CustomDisplayText
        Try
            Dim gridLookUpEdit = CType(sender, GridLookUpEdit)
            Dim dataRowView = CType(gridLookUpEdit.Properties.GetRowByKeyValue(e.Value), DataRowView)
            Dim row = CType(dataRowView.Row, DataRow)
            e.DisplayText = (row("CAT").ToString + ("  " + row("CATAGORYNAME").ToString))
        Catch
            'MsgBox("Catch")
        End Try
    End Sub
    Private Sub GridLookUpEdit4_CustomDisplayText(sender As System.Object, e As DevExpress.XtraEditors.Controls.CustomDisplayTextEventArgs) Handles GridLookUpEdit4.CustomDisplayText
        Try
            Dim gridLookUpEdit = CType(sender, GridLookUpEdit)
            Dim dataRowView = CType(gridLookUpEdit.Properties.GetRowByKeyValue(e.Value), DataRowView)
            Dim row = CType(dataRowView.Row, DataRow)
            e.DisplayText = (row("GroupId").ToString + ("  " + row("GroupName").ToString))
        Catch
            'MsgBox("Catch")
        End Try
    End Sub
    Private Sub GridLookUpEdit5_CustomDisplayText(sender As System.Object, e As DevExpress.XtraEditors.Controls.CustomDisplayTextEventArgs) Handles GridLookUpEdit5.CustomDisplayText
        Try
            Dim gridLookUpEdit = CType(sender, GridLookUpEdit)
            Dim dataRowView = CType(gridLookUpEdit.Properties.GetRowByKeyValue(e.Value), DataRowView)
            Dim row = CType(dataRowView.Row, DataRow)
            e.DisplayText = (row("GradeCode").ToString + ("  " + row("GradeName").ToString))
        Catch
            'MsgBox("Catch")
        End Try
    End Sub
    Private Sub GridLookUpEdit6_CustomDisplayText(sender As System.Object, e As DevExpress.XtraEditors.Controls.CustomDisplayTextEventArgs) Handles GridLookUpEdit6.CustomDisplayText
        Try
            Dim gridLookUpEdit = CType(sender, GridLookUpEdit)
            Dim dataRowView = CType(gridLookUpEdit.Properties.GetRowByKeyValue(e.Value), DataRowView)
            Dim row = CType(dataRowView.Row, DataRow)
            e.DisplayText = (row("DESPANSARYCODE").ToString + ("  " + row("DESPANSARYNAME").ToString))
        Catch
            'MsgBox("Catch")
        End Try
    End Sub
    Private Sub GridLookUpEdit7_CustomDisplayText(sender As System.Object, e As DevExpress.XtraEditors.Controls.CustomDisplayTextEventArgs) Handles GridLookUpEdit7.CustomDisplayText
        Try
            Dim gridLookUpEdit = CType(sender, GridLookUpEdit)
            Dim dataRowView = CType(gridLookUpEdit.Properties.GetRowByKeyValue(e.Value), DataRowView)
            Dim row = CType(dataRowView.Row, DataRow)
            e.DisplayText = (row("BANKCODE").ToString + ("  " + row("BANKNAME").ToString))
        Catch
            'MsgBox("Catch")
        End Try
    End Sub
    Private Sub GridLookUpEdit8_CustomDisplayText(sender As System.Object, e As DevExpress.XtraEditors.Controls.CustomDisplayTextEventArgs) Handles GridLookUpEdit8.CustomDisplayText
        Try
            Dim gridLookUpEdit = CType(sender, GridLookUpEdit)
            Dim dataRowView = CType(gridLookUpEdit.Properties.GetRowByKeyValue(e.Value), DataRowView)
            Dim row = CType(dataRowView.Row, DataRow)
            e.DisplayText = (row("BRANCHCODE").ToString + ("  " + row("BRANCHNAME").ToString))
        Catch
            'MsgBox("Catch")
        End Try
    End Sub
    'Private Sub setDeviceGrid()
    '    Dim gridtblregisterselet As String
    '    gridtblregisterselet = "select * from tblMachine where DeviceType = 'ZK(TFT)' "
    '    Dim WTDataTable As DataTable
    '    If Common.servername = "Access" Then
    '        Dim dataAdapter As New OleDbDataAdapter(gridtblregisterselet, Common.con1)
    '        WTDataTable = New DataTable("tblMachine")
    '        dataAdapter.Fill(WTDataTable)
    '    Else
    '        Dim dataAdapter As New SqlClient.SqlDataAdapter(gridtblregisterselet, Common.con)
    '        WTDataTable = New DataTable("tblMachine")
    '        dataAdapter.Fill(WTDataTable)
    '    End If
    '    GridControlMachine.DataSource = WTDataTable
    'End Sub
    Private Sub CheckCopyEmpty_CheckedChanged(sender As System.Object, e As System.EventArgs)
        If CheckCopyEmpty.Checked = True Then
            PopupContainerEdit1.Enabled = True
        Else
            PopupContainerEdit1.Enabled = False
        End If
    End Sub
    Private Sub VisitorPictureEdit_DoubleClick(sender As System.Object, e As System.EventArgs) Handles EmpPictureEdit.DoubleClick
        InvokeDefaultCameraDialog()
    End Sub
    Public Sub InvokeDefaultCameraDialog()
        Dim dialog As DevExpress.XtraEditors.Camera.TakePictureDialog = New DevExpress.XtraEditors.Camera.TakePictureDialog
        If dialog.ShowDialog = DialogResult.OK Then
            Dim image As System.Drawing.Image = dialog.Image
            Using stream As MemoryStream = New MemoryStream
                image.Save(stream, ImageFormat.Jpeg)
                EmpPictureEdit.EditValue = stream.ToArray
            End Using
        End If
    End Sub
    Private Sub TextEditImage_Click(sender As System.Object, e As System.EventArgs) Handles TextEditImage.Click
        Dim dlg As New OpenFileDialog()
        'Filter by Office Files
        dlg.Filter = "Office Files|*.jpg;*.jpeg;"
        dlg.ShowDialog()
        TextEditImage.Text = dlg.FileName
        EmpPictureEdit.Image = Nothing
        If TextEditImage.Text <> "" Then
            Try
                EmpPictureEdit.Image = Image.FromFile(TextEditImage.Text)
            Catch ex As Exception
                XtraMessageBox.Show(ulf, "<size=10>" & ex.Message.ToString & "</size>", "<size=9>Error</size>")
            End Try
        End If
    End Sub



    Private Sub SimpleButtonInit_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonInit.Click
        Dim ret As Integer '= zkfperrdef.ZKFP_ERR_OK
        Try
            ret = zkfp2.Init()
        Catch ex As Exception
            XtraMessageBox.Show(ulf, "<size=10>Driver Not Found!</size>", "<size=9>Error</size>")
            Exit Sub
        End Try

        'If (ret = zkfp2.Init()) = zkfperrdef.ZKFP_ERR_OK Then
        If ret = zkfperrdef.ZKFP_ERR_OK Then
            Dim nCount As Integer = zkfp2.GetDeviceCount()
            If nCount = 0 Then
                zkfp2.Terminate()
                XtraMessageBox.Show(ulf, "<size=10>No device connected!</size>", "<size=9>Error</size>")
            Else
                bnOpen()
                bnEnroll()
                SimpleButtonInit.Enabled = False
                SimpleButtonStop.Enabled = True
            End If
        Else
            XtraMessageBox.Show(ulf, "<size=10>No Device Found!</size>", "<size=9>Error</size>")
            'XtraMessageBox.Show(ulf, "<size=10>Initialize fail, ret=" & ret & " !</size>", "<size=9>Error</size>")
        End If
    End Sub
    <DllImport("user32.dll", EntryPoint:="SendMessageA")>
    Public Shared Function SendMessage(ByVal hwnd As IntPtr, ByVal wMsg As Integer, ByVal wParam As IntPtr, ByVal lParam As IntPtr) As Integer
    End Function
    Private Sub bnOpen()
        Dim ret As Integer = zkfp.ZKFP_ERR_OK
        mDevHandle = zkfp2.OpenDevice(0)
        If (IntPtr.Zero = mDevHandle) Then '0 is the index of device connected, we have only one device so 0
            XtraMessageBox.Show(ulf, "<size=10>OpenDevice fail</size>", "<size=9>Error</size>")
            Return
        End If
        mDBHandle = zkfp2.DBInit()
        If (IntPtr.Zero = mDBHandle) Then
            XtraMessageBox.Show(ulf, "<size=10>Init DB fail</size>", "<size=9>Error</size>")
            zkfp2.CloseDevice(mDevHandle)
            mDevHandle = IntPtr.Zero
            Return
        End If

        RegisterCount = 0
        cbRegTmp = 0
        iFid = 1

        'Do While (i < 3)
        '    RegTmps(i) = New Byte(2048 - 1) {}
        '    i = (i + 1)
        'Loop
        For i = 0 To 2
            RegTmps(i) = New Byte(2048 - 1) {}
        Next
        Dim paramValue() As Byte = New Byte(4 - 1) {}
        Dim size As Integer = 4
        zkfp2.GetParameters(mDevHandle, 1, paramValue, size)
        zkfp2.ByteArray2Int(paramValue, mfpWidth)
        size = 4
        zkfp2.GetParameters(mDevHandle, 2, paramValue, size)
        zkfp2.ByteArray2Int(paramValue, mfpHeight)
        FPBuffer = New Byte((mfpWidth * mfpHeight) - 1) {}
        captureThread = New Thread(New ThreadStart(AddressOf DoCapture))
        captureThread.IsBackground = True
        captureThread.Start()
        bIsTimeToDie = False
        textRes.Text = "Open succ"
    End Sub
    Private Sub DoCapture()
        While Not bIsTimeToDie
            cbCapTmp = 2048
            Try
                Dim ret As Integer = zkfp2.AcquireFingerprint(mDevHandle, FPBuffer, CapTmp, cbCapTmp)
                If (ret = zkfp.ZKFP_ERR_OK) Then
                    SendMessage(FormHandle, MESSAGE_CAPTURED_OK, IntPtr.Zero, IntPtr.Zero)
                End If
            Catch ex As Exception

            End Try
            Thread.Sleep(200)
        End While

    End Sub
    Private Sub bnEnroll()
        If Not IsRegister Then
            IsRegister = True
            RegisterCount = 0
            cbRegTmp = 0
            textRes.Text = "Please press your finger 3 times!"
        End If
    End Sub
    Protected Overrides Sub DefWndProc(ByRef m As System.Windows.Forms.Message)
        Select Case (m.Msg)
            Case MESSAGE_CAPTURED_OK
                Dim ms As System.IO.MemoryStream = New System.IO.MemoryStream
                Sample.BitmapFormat.GetBitmap(FPBuffer, mfpWidth, mfpHeight, ms)
                Dim bmp As Bitmap = New Bitmap(ms)
                Me.picFPImg.Image = bmp
                'Me.PictureBox1.Image = bmp
                If IsRegister Then
                    Dim ret As Integer = zkfp.ZKFP_ERR_OK
                    Dim score As Integer = 0
                    Dim fid As Integer = 0
                    ret = zkfp2.DBIdentify(mDBHandle, CapTmp, fid, score)
                    If (zkfp.ZKFP_ERR_OK = ret) Then
                        textRes.Text = ("This finger was already register by " & (fid & "!"))
                        Return
                    End If

                    'If ((RegisterCount > 0) And (zkfp2.DBMatch(mDBHandle, CapTmp, RegTmps((RegisterCount - 1))) <= 0)) Then
                    If (RegisterCount > 0) Then
                        If zkfp2.DBMatch(mDBHandle, CapTmp, RegTmps(RegisterCount - 1)) <= 0 Then
                            textRes.Text = "Please press the same finger 3 times for the enrollment"
                            Return
                        End If
                    End If

                    Array.Copy(CapTmp, RegTmps(RegisterCount), cbCapTmp)
                    strBase64 = zkfp2.BlobToBase64(CapTmp, cbCapTmp)
                    Dim blob() As Byte = zkfp2.Base64ToBlob(strBase64)
                    RegisterCount = (RegisterCount + 1)
                    If (RegisterCount >= REGISTER_FINGER_COUNT) Then
                        RegisterCount = 0
                        If ((zkfp.ZKFP_ERR_OK = zkfp2.DBMerge(mDBHandle, RegTmps(0), RegTmps(1), RegTmps(2), RegTmp, cbRegTmp)) _
                                    AndAlso (zkfp.ZKFP_ERR_OK = zkfp2.DBAdd(mDBHandle, iFid, RegTmp))) Then
                            iFid = (iFid + 1)
                            textRes.Text = "enroll succ"
                            'SimpleButtonFPCopy.Enabled = True
                        Else
                            textRes.Text = ("enroll fail, error code=" & ret)
                        End If

                        IsRegister = False
                        Return
                    Else
                        textRes.Text = ("You need to press the " & ((REGISTER_FINGER_COUNT - RegisterCount) & " times fingerprint"))
                    End If
                Else
                    If (cbRegTmp <= 0) Then
                        textRes.Text = "Please register your finger first!"
                        Return
                    End If

                    If bIdentify Then
                        Dim ret As Integer = zkfp.ZKFP_ERR_OK
                        Dim score As Integer = 0
                        Dim fid As Integer = 0
                        ret = zkfp2.DBIdentify(mDBHandle, CapTmp, fid, score)
                        If (zkfp.ZKFP_ERR_OK = ret) Then
                            textRes.Text = ("Identify succ, fid= " & fid & ",score=" & score & "!")
                            Return
                        Else
                            textRes.Text = ("Identify fail, ret= " & ret)
                            Return
                        End If

                    Else
                        Dim ret As Integer = zkfp2.DBMatch(mDBHandle, CapTmp, RegTmp)
                        If (0 < ret) Then
                            textRes.Text = ("Match finger succ, score=" & (ret & "!"))
                            Return
                        Else
                            textRes.Text = ("Match finger fail, ret= " & ret)
                            Return
                        End If
                    End If
                End If
                'Me.picFPImg.EditValue = RegTmps(RegisterCount) 'CapTmp   'nitin
            Case Else
                MyBase.DefWndProc(m)
        End Select
    End Sub


    Private Sub SimpleButtonStop_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonStop.Click
        zkfp2.Terminate()
        SimpleButtonInit.Enabled = True
        SimpleButtonStop.Enabled = False
    End Sub
    Private Sub FPCopyTemplate() 'Handles SimpleButtonFPCopy.Click
        If TextFingerNo.Text.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Finger No cannot be Empty!</size>", "<size=9>Error</size>")
            Exit Sub
        End If
        If strBase64 = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Finger Template cannot be Empty!</size>", "<size=9>Error</size>")
            Exit Sub
        End If
        If GridViewMachine.SelectedRowsCount = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>Please Select Device from Device List!</size>", "<size=9>Error</size>")
            Exit Sub
        End If
        Dim selectedRows As Integer() = GridViewMachine.GetSelectedRows()
        Dim result As Object() = New Object(selectedRows.Length - 1) {}
        Dim LstMachineId As String
        For i = 0 To selectedRows.Length - 1
            Dim rowHandle As Integer = selectedRows(i)
            If Not GridViewMachine.IsGroupRow(rowHandle) Then
                LstMachineId = GridViewMachine.GetRowCellValue(rowHandle, "ID_NO").ToString.Trim
                Dim sdwEnrollNumber As String
                Dim sName As String = ""
                Dim sPassword As String = ""
                Dim iPrivilege As Integer
                Dim idwFingerIndex As Integer
                Dim sTmpData As String = ""
                Dim sEnabled As String = ""
                Dim bEnabled As Boolean = False
                Dim iflag As Integer

                Dim lpszIPAddress As String = GridViewMachine.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim 'Trim(rstm!Location)
                Dim vpszIPAddress As String = Trim(lpszIPAddress)

                Dim bIsConnected = False
                Dim iMachineNumber As Integer = result(i)
                Dim idwErrorCode As Integer
                Dim com As Common = New Common
                Dim axCZKEM1 As New zkemkeeper.CZKEM
                bIsConnected = axCZKEM1.Connect_Net(vpszIPAddress, 4370)
                If bIsConnected = True Then
                    axCZKEM1.EnableDevice(iMachineNumber, False)
                    XtraMasterTest.LabelControlStatus.Text = "Uploading Template " & TextEdit2.Text.Trim
                    Application.DoEvents()
                    If IsNumeric(TextEditUserNo.Text.Trim) Then
                        sdwEnrollNumber = Convert.ToDouble(TextEditUserNo.Text.Trim)
                    Else
                        sdwEnrollNumber = TextEditUserNo.Text.Trim
                    End If
                    Dim strCardno = ""

                    Dim ret = axCZKEM1.SSR_GetUserInfo(iMachineNumber, sdwEnrollNumber, sName, sPassword, iPrivilege, bEnabled)  'get the user info if user is already in device
                    If ret = False Then
                        sPassword = "" 'RsFp.Tables(0).Rows(0)("Password").ToString().Trim()
                    Else
                        axCZKEM1.GetStrCardNumber(strCardno)
                        If strCardno = "0" Then
                            strCardno = ""
                        End If
                    End If
                    sName = TextEditName.Text.Trim 'XtraEmployeeEdit.NameForTemplate 'TextEditName.Text.Trim ' dstmp.Tables(0).Rows(0)("EMPNAME").ToString().Trim()
                    idwFingerIndex = TextFingerNo.Text '6 ' Convert.ToInt32(RsFp.Tables(0).Rows(0)("FingerNumber").ToString().Trim())
                    sTmpData = strBase64 'Encoding.UTF8.GetString(dsRecord.Tables(0).Rows(0).Item("vFingerImage"), 0, dsRecord.Tables(0).Rows(0).Item("vFingerImage").Length) 'dsRecord.Tables(0).Rows(0).Item("vFingerImage")
                    'Dim sTmpDataByte() As Byte = dsRecord.Tables(0).Rows(0).Item("vFingerImage")
                    iPrivilege = 0 'Convert.ToInt32(RsFp.Tables(0).Rows(0)("Privilege").ToString().Trim()) 'told by ajitesh(used for admin)

                    sEnabled = "true" 'RsFp.Tables(0).Rows(0)(6).ToString().Trim()
                    iflag = 0 'Convert.ToInt32(RsFp.Tables(0).Rows(0)(7).ToString())' as per old s/w
                    'Dim icard As String = "" 'RsFp.Tables(0).Rows(0)("CardNumber").ToString().Trim()
                    If sEnabled.ToString().ToLower() = "true" Then
                        bEnabled = True
                    Else
                        bEnabled = False
                    End If
                    'Dim iBackupNumber As String

                    If TextEditCard.Text.Trim <> "" Then
                        axCZKEM1.SetStrCardNumber(TextEditCard.Text.Trim)
                    Else
                        axCZKEM1.SetStrCardNumber(strCardno)
                    End If

                    Dim rs As Integer
                    If axCZKEM1.SSR_SetUserInfo(iMachineNumber, sdwEnrollNumber, sName, sPassword, iPrivilege, bEnabled) Then 'upload user information to the device
                        rs = axCZKEM1.SetUserTmpExStr(iMachineNumber, sdwEnrollNumber, idwFingerIndex, iflag, sTmpData) 'upload templates information to the device
                        'axCZKEM1.SetUserValidDate(iMachineNumber, sdwEnrollNumber, 1, 0, DateEditStart.DateTime.ToString("yyyy-MM-dd HH:mm:00"), DateEditEnd.DateTime.ToString("yyyy-MM-dd HH:mm:59"))  'add valdity
                        If CheckValidity.Checked = True Then
                            Try
                                axCZKEM1.SetUserValidDate(iMachineNumber, sdwEnrollNumber, 1, 0, DateEditVStrt.DateTime.ToString("yyyy-MM-dd HH:mm:00"), DateEditVEnd.DateTime.ToString("yyyy-MM-dd HH:mm:59"))  'add valdity
                                axCZKEM1.RefreshData(iMachineNumber)
                            Catch ex As Exception
                            End Try
                        End If
                    Else
                        axCZKEM1.GetLastError(idwErrorCode)
                        XtraMessageBox.Show(ulf, "<size=10>Operation failed,ErrorCode=" & idwErrorCode.ToString() & "</size>", "<size=9>Error<size=9>")
                        axCZKEM1.EnableDevice(iMachineNumber, True)
                        'Cursor = Cursors.Default
                        Return
                    End If
                    axCZKEM1.EnableDevice(iMachineNumber, True)
                Else
                    axCZKEM1.GetLastError(idwErrorCode)
                    axCZKEM1.Disconnect()
                    XtraMessageBox.Show(ulf, "<size=10>Operation failed for " & lpszIPAddress & " </size>", "<size=9>Error<size=9>")
                    Continue For
                End If

                Try 'save to fp table
                    Dim sSql As String = "select * from fptable where Enrollnumber='" & TextEditUserNo.Text.Trim & "' and FingerNumber=" & TextFingerNo.Text.Trim
                    Dim dsRecord As DataSet = New DataSet
                    If Common.servername = "Access" Then
                        adapA = New OleDbDataAdapter(sSql, Common.con1)
                        adapA.Fill(dsRecord)
                    Else
                        adap = New SqlDataAdapter(sSql, Common.con)
                        adap.Fill(dsRecord)
                    End If
                    If dsRecord.Tables(0).Rows.Count > 0 Then
                        sSql = "DELETE FROM FPTABLE WHERE Enrollnumber='" & sdwEnrollNumber & "' and FingerNumber=" & TextFingerNo.Text.Trim
                        If Common.servername = "Access" Then
                            cmd1 = New OleDbCommand(sSql, Common.con1)
                            cmd1.ExecuteNonQuery()
                        Else
                            cmd = New SqlCommand(sSql, Common.con)
                            cmd.ExecuteNonQuery()
                        End If
                        Dim sqlinsert As String = "insert into fptable (Emachinenumber, EnrollNumber, UserName, Template, FingerNumber, Privilege) values ('" & LstMachineId & "','" & TextEditUserNo.Text.Trim & "', '" & sName.ToString() & "','" & sTmpData.ToString() & "', '" & TextFingerNo.Text.Trim & "', '" & iPrivilege.ToString() & "')"
                        If Common.servername = "Access" Then
                            cmd1 = New OleDbCommand(sqlinsert, Common.con1)
                            cmd1.ExecuteNonQuery()
                        Else
                            cmd = New SqlCommand(sqlinsert, Common.con)
                            cmd.ExecuteNonQuery()
                        End If
                    Else
                        Dim sqlinsert As String = "insert into fptable (Emachinenumber, EnrollNumber, UserName, Template, FingerNumber, Privilege) values ('" & LstMachineId & "','" & TextEditUserNo.Text.Trim & "', '" & sName.ToString() & "','" & sTmpData.ToString() & "', '" & TextFingerNo.Text.Trim & "', '" & iPrivilege.ToString() & "')"
                        If Common.servername = "Access" Then
                            cmd1 = New OleDbCommand(sqlinsert, Common.con1)
                            cmd1.ExecuteNonQuery()
                        Else
                            cmd = New SqlCommand(sqlinsert, Common.con)
                            cmd.ExecuteNonQuery()
                        End If
                    End If
                Catch ex As Exception

                End Try
            End If
        Next
        XtraMasterTest.LabelControlStatus.Text = ""
        Application.DoEvents()
        'Me.Close()
    End Sub
    Private Sub EnrollFinger()
        If GridViewMachine.SelectedRowsCount = 0 Then
            XtraMessageBox.Show(ulf, "<size=10>Please Select Device from Device List!</size>", "<size=9>Error</size>")
            Exit Sub
        End If
        Dim sdwEnrollNumber As String
        Dim sName As String = ""
        If IsNumeric(TextEditUserNo.Text.Trim) Then
            sdwEnrollNumber = Convert.ToDouble(TextEditUserNo.Text.Trim)
        Else
            XtraMessageBox.Show(ulf, "<size=10>User No. Should be Numeric</size>", "<size=9>Error<size=9>")
            Exit Sub
        End If
        If sdwEnrollNumber.Length > 10 Then
            XtraMessageBox.Show(ulf, "<size=10>User No. Should not be more than 10 digits</size>", "<size=9>Error<size=9>")
            Exit Sub
        End If
        Dim selectedRows As Integer() = GridViewMachine.GetSelectedRows()
        Dim result As Object() = New Object(selectedRows.Length - 1) {}
        Dim LstMachineId As String
        For i = 0 To selectedRows.Length - 1
            Dim rowHandle As Integer = selectedRows(i)
            If Not GridViewMachine.IsGroupRow(rowHandle) Then
                LstMachineId = GridViewMachine.GetRowCellValue(rowHandle, "ID_NO").ToString.Trim
                Dim sPassword As String = ""
                Dim iPrivilege As Integer
                Dim idwFingerIndex As Integer = Convert.ToInt16(TextFingerNo.Text.Trim)
                If CheckEnrollFace.Checked = True Then
                    idwFingerIndex = 111
                End If
                Dim sTmpData As String = ""
                Dim sEnabled As String = ""
                Dim bEnabled As Boolean = False
                Dim iflag As Integer

                Dim lpszIPAddress As String = GridViewMachine.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim 'Trim(rstm!Location)
                Dim vpszIPAddress As String = Trim(lpszIPAddress)

                Dim bIsConnected = False
                Dim iMachineNumber As Integer = result(i)
                Dim idwErrorCode As Integer
                Dim com As Common = New Common
                If GridViewMachine.GetRowCellValue(rowHandle, "DeviceType").ToString.Trim = "ZK(TFT)" Or GridViewMachine.GetRowCellValue(rowHandle, "DeviceType").ToString.Trim = "Bio-1Pro/ATF305Pro/ATF686Pro" Then
                    Dim axCZKEM1 As New zkemkeeper.CZKEM
                    bIsConnected = axCZKEM1.Connect_Net(vpszIPAddress, 4370)
                    If bIsConnected = True Then
                        axCZKEM1.CancelOperation()
                        'If the specified index of user's templates has existed ,delete it first
                        axCZKEM1.SSR_DelUserTmpExt(iMachineNumber, sdwEnrollNumber, idwFingerIndex)
                        If axCZKEM1.StartEnrollEx(sdwEnrollNumber, idwFingerIndex, 0) Then
                            If axCZKEM1.StartIdentify() Then
                            Else
                                axCZKEM1.Disconnect()
                                XtraMessageBox.Show(ulf, "<size=10>Operation failed for " & lpszIPAddress & " </size>", "<size=9>Error<size=9>")
                                Continue For
                            End If
                        Else
                            axCZKEM1.Disconnect()
                            XtraMessageBox.Show(ulf, "<size=10>Operation failed for " & lpszIPAddress & " </size>", "<size=9>Error<size=9>")
                            Continue For
                        End If
                    Else
                        axCZKEM1.GetLastError(idwErrorCode)
                        axCZKEM1.Disconnect()
                        XtraMessageBox.Show(ulf, "<size=10>Operation failed for " & lpszIPAddress & " </size>", "<size=9>Error<size=9>")
                        Continue For
                    End If
                ElseIf GridViewMachine.GetRowCellValue(rowHandle, "DeviceType").ToString.Trim = "HTSeries" Then
                    LstMachineId = GridViewMachine.GetRowCellValue(rowHandle, "ID_NO").ToString.Trim
                    Dim bConn = False
                    Dim vnMachineNumber = LstMachineId
                    Dim vnLicense = 1261 '1789 '
                    lpszIPAddress = GridViewMachine.GetRowCellValue(rowHandle, "LOCATION").ToString.Trim 'Trim(rstm!Location)
                    vpszIPAddress = Trim(lpszIPAddress)
                    Dim struDeviceInfoV40 As CHCNetSDK.NET_DVR_DEVICEINFO_V40 = New CHCNetSDK.NET_DVR_DEVICEINFO_V40
                    Dim DeviceAdd As String = vpszIPAddress
                    Dim userName As String = Trim(GridViewMachine.GetRowCellValue(rowHandle, "HLogin").ToString.Trim)
                    Dim pwd As String = Trim(GridViewMachine.GetRowCellValue(rowHandle, "HPassword").ToString.Trim)
                    Dim lUserID As Integer = -1
                    Dim failReason As String = ""
                    Dim logistatus = com.HikvisionLogin(DeviceAdd, userName, pwd, struDeviceInfoV40, lUserID, failReason)
                    If logistatus = False Then
                        For Each c As Control In Me.Controls
                            c.Enabled = True
                        Next
                        If failReason <> "" Then
                            XtraMessageBox.Show(ulf, "<size=10>" & failReason & "</size>", "Failed")
                            XtraMasterTest.LabelControlStatus.Text = ""
                            Application.DoEvents()
                            Me.Cursor = Cursors.Default
                            Exit Sub
                        End If
                    Else
                        If failReason <> "" Then
                            XtraMessageBox.Show(ulf, "<size=10>" & failReason & "</size>", "Failed")
                            XtraMasterTest.LabelControlStatus.Text = ""
                            Application.DoEvents()
                            Me.Cursor = Cursors.Default
                            logistatus = com.HikvisionLogOut(lUserID)
                            Exit Sub
                        End If
                        Dim mUser_ID As String = TextEditUserNo.Text.Trim 'GridView2.GetRowCellValue(rowHandleEmp, "EnrollNumber").ToString.Trim()
                        'Dim tmpUserId As String
                        If IsNumeric(mUser_ID) Then
                            mUser_ID = Convert.ToDouble(mUser_ID) '.ToString("000000000000")
                        Else
                            mUser_ID = mUser_ID
                        End If
                        XtraMasterTest.LabelControlStatus.Text = "Uploading User " & mUser_ID
                        Application.DoEvents()
                        'for user
                        'If GridView2.GetRowCellValue(rowHandleEmp, "FingerNumber").ToString.Trim() = "11" Then
                        'for user copy only
                        Dim ValidityStartdate As DateTime
                        Dim ValidityEnddate As DateTime
                        Dim adapZ As SqlDataAdapter
                        Dim adapAZ As OleDbDataAdapter

                        'Dim sName As String
                        'Dim FpNo As String = GridView2.GetRowCellValue(rowHandleEmp, "FingerNumber").ToString.Trim()
                        Dim Privilege As Integer = 0
                        Dim Cardnumber As String = TextEditCard.Text.Trim 'GridView2.GetRowCellValue(rowHandleEmp, "Cardnumber").ToString.Trim()
                        Dim Password As String = TextEditPassword.Text.Trim 'GridView2.GetRowCellValue(rowHandleEmp, "Password").ToString.Trim()
                        Try
                            Privilege = ComboBoxEditPriV.SelectedIndex 'GridView2.GetRowCellValue(rowHandleEmp, "Privilege")
                        Catch ex As Exception
                        End Try

                        'Dim sdwEnrollNumber As String
                        If IsNumeric(mUser_ID) = True Then
                            sdwEnrollNumber = Convert.ToDouble(mUser_ID)
                        Else
                            sdwEnrollNumber = mUser_ID
                        End If

                        sName = TextEditName.Text.Trim ' dstmp.Tables(0).Rows(0)("EMPNAME").ToString().Trim()
                        ValidityStartdate = DateEditVStrt.DateTime 'Convert.ToDateTime(dstmp.Tables(0).Rows(0)("ValidityStartdate").ToString().Trim())
                        ValidityEnddate = DateEditVEnd.DateTime 'Convert.ToDateTime(dstmp.Tables(0).Rows(0)("ValidityEnddate").ToString().Trim())

                        If (m_lSetUserCfgHandle <> -1) Then
                            If CHCNetSDK.NET_DVR_StopRemoteConfig(m_lSetUserCfgHandle) Then
                                m_lSetUserCfgHandle = -1
                            End If
                        End If
                        Dim sURL As String = "PUT /ISAPI/AccessControl/UserInfo/SetUp?format=json"
                        Dim ptrURL As IntPtr = Marshal.StringToHGlobalAnsi(sURL)
                        m_lSetUserCfgHandle = CHCNetSDK.NET_DVR_StartRemoteConfig(lUserID, CHCNetSDK.NET_DVR_JSON_CONFIG, ptrURL, sURL.Length, Nothing, IntPtr.Zero)
                        If (m_lSetUserCfgHandle < 0) Then
                            'MessageBox.Show(("NET_DVR_StartRemoteConfig fail [url:PUT /ISAPI/AccessControl/UserInfo/SetUp?format=json] error:" + CHCNetSDK.NET_DVR_GetLastError))
                            Marshal.FreeHGlobal(ptrURL)
                            Continue For
                        Else
                            SendUserInfo(sdwEnrollNumber, sName, ValidityStartdate, ValidityEnddate, Privilege, Cardnumber, Password)
                            Marshal.FreeHGlobal(ptrURL)
                        End If
                        'user end

                        'enroll face
                        If m_lCapFaceCfgHandle <> -1 Then
                            CHCNetSDK.NET_DVR_StopRemoteConfig(m_lCapFaceCfgHandle)
                            m_lCapFaceCfgHandle = -1
                        End If

                        If EmpPictureEdit.Image IsNot Nothing Then
                            EmpPictureEdit.Image.Dispose()
                            EmpPictureEdit.Image = Nothing
                        End If

                        Dim struCond As NET_DVR_CAPTURE_FACE_COND = New NET_DVR_CAPTURE_FACE_COND()
                        struCond.init()
                        struCond.dwSize = Marshal.SizeOf(struCond)
                        Dim dwInBufferSize As Integer = struCond.dwSize
                        Dim ptrStruCond As IntPtr = Marshal.AllocHGlobal(dwInBufferSize)
                        Marshal.StructureToPtr(struCond, ptrStruCond, False)
                        m_lCapFaceCfgHandle = CHCNetSDK.NET_DVR_StartRemoteConfig(lUserID, CHCNetSDK.NET_DVR_CAPTURE_FACE_INFO, ptrStruCond, dwInBufferSize, Nothing, IntPtr.Zero)

                        If -1 = m_lCapFaceCfgHandle Then
                            Marshal.FreeHGlobal(ptrStruCond)
                            MessageBox.Show("NET_DVR_CAP_FACE_FAIL, ERROR CODE" & CHCNetSDK.NET_DVR_GetLastError().ToString(), "Error", MessageBoxButtons.OK)
                            Return
                        End If

                        Dim struFaceCfg As NET_DVR_CAPTURE_FACE_CFG = New NET_DVR_CAPTURE_FACE_CFG()
                        struFaceCfg.init()
                        Dim dwStatus As Integer = 0
                        Dim dwOutBuffSize As Integer = Marshal.SizeOf(struFaceCfg)
                        Dim Flag As Boolean = True

                        Dim ptr As IntPtr = Marshal.AllocHGlobal(1024)
                        For n As Integer = 0 To 1024 - 1
                            Marshal.WriteByte(ptr, n, 0)
                        Next

                        While Flag
                            dwStatus = CHCNetSDK.NET_DVR_GetNextRemoteConfig(m_lCapFaceCfgHandle, ptr, dwOutBuffSize)
                            Select Case dwStatus
                                Case CHCNetSDK.NET_SDK_GET_NEXT_STATUS_SUCCESS
                                    struFaceCfg = Marshal.PtrToStructure(ptr, GetType(NET_DVR_CAPTURE_FACE_CFG))
                                    ProcessCapFaceData(struFaceCfg, Flag)
                                Case CHCNetSDK.NET_SDK_GET_NEXT_STATUS_NEED_WAIT
                                Case CHCNetSDK.NET_SDK_GET_NEXT_STATUS_FAILED
                                    CHCNetSDK.NET_DVR_StopRemoteConfig(m_lCapFaceCfgHandle)
                                    MessageBox.Show("NET_SDK_GET_NEXT_STATUS_FAILED" & CHCNetSDK.NET_DVR_GetLastError().ToString(), "Error", MessageBoxButtons.OK)
                                    Flag = False
                                Case CHCNetSDK.NET_SDK_GET_NEXT_STATUS_FINISH
                                    CHCNetSDK.NET_DVR_StopRemoteConfig(m_lCapFaceCfgHandle)
                                    Flag = False
                                Case Else
                                    MessageBox.Show("NET_SDK_GET_STATUS_UNKOWN" & CHCNetSDK.NET_DVR_GetLastError().ToString(), "Error", MessageBoxButtons.OK)
                                    Flag = False
                                    CHCNetSDK.NET_DVR_StopRemoteConfig(m_lCapFaceCfgHandle)
                            End Select
                        End While
                        Marshal.FreeHGlobal(ptrStruCond)
                        EnrollFace(lUserID, mUser_ID)
                    End If
                End If

            End If
        Next
        FPCopy()
        XtraMasterTest.LabelControlStatus.Text = ""
        Application.DoEvents()
    End Sub

    Private Sub CheckEditTemplateUltra_CheckedChanged(sender As Object, e As EventArgs) Handles CheckEditTemplateUltra.CheckedChanged
        templateCheckChange()
    End Sub

    Private Sub btnTransfer_Click(sender As Object, e As EventArgs) Handles btnTransfer.Click
        If TextEditUserNo.Text = "" Then
            XtraMessageBox.Show(ulf, "<size=10>User No cannot be empty</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            TextEditUserNo.Select()
            Exit Sub
        End If
        If TextEdit2.Text = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Emp code cannot be empty</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            TextEdit2.Select()
            Exit Sub
        End If
        If TextEditName.Text.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Emp Name cannot be empty</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            TextEditName.Select()
            Exit Sub
        End If

        If CheckCopyEmpty.Checked = True Then
            FPCopy()
        ElseIf CheckEditTemplate.Checked = True Then
            UserNoForTemplate = TextEditUserNo.Text.Trim
            NameForTemplate = TextEditName.Text.Trim
            'XtraEmployeeTemplateCopy.ShowDialog()
            FPCopyTemplate()
        ElseIf CheckEnroll.Checked = True Or CheckEnrollFace.Checked = True Then
            EnrollFinger()
        ElseIf CheckEditTemplateUltra.Checked Then

        End If

    End Sub

    Private Sub XtraEmployeeEdit_FormClosing(sender As System.Object, e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        Try
            zkfp2.Terminate()
            captureThread.Abort()
        Catch ex As Exception

        End Try
    End Sub
    Private Sub CheckCopyEmpty_CheckedChanged_1(sender As System.Object, e As System.EventArgs) Handles CheckCopyEmpty.CheckedChanged
        templateCheckChange()
    End Sub
    Private Sub CheckEditTemplate_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckEditTemplate.CheckedChanged
        templateCheckChange()
    End Sub
    Private Sub CheckEnroll_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckEnroll.CheckedChanged
        templateCheckChange()
    End Sub
    Private Sub templateCheckChange()
        If CheckCopyEmpty.Checked = True Then
            'PopupContainerEdit1.Enabled = False
            GroupControlFPscanner.Enabled = False
            'TextEditCard.Enabled = False
            'If Common.servername = "Access" Then
            '    Me.TblMachine1TableAdapter1.Fill(Me.SSSDBDataSet.tblMachine1)
            '    GridControlMachine.DataSource = SSSDBDataSet.tblMachine1
            'Else
            '    TblMachineTableAdapter.Connection.ConnectionString = Common.ConnectionString
            '    Me.TblMachineTableAdapter.Fill(Me.SSSDBDataSet.tblMachine)
            '    GridControlMachine.DataSource = SSSDBDataSet.tblMachine
            'End If
            GridControlMachine.DataSource = Common.MachineNonAdminAll
        ElseIf CheckEditTemplate.Checked = True Then
            GroupControlFPscanner.Enabled = True
            'TextEditCard.Enabled = True
            'PopupContainerEdit1.Enabled = True
            setDeviceGridZK()
        ElseIf CheckEnroll.Checked = True Or CheckEnrollFace.Checked = True Then
            GroupControlFPscanner.Enabled = False
            Dim gridtblregisterselet As String
            If Common.USERTYPE = "A" Then
                gridtblregisterselet = "select * from tblMachine where DeviceType = 'ZK(TFT)' or DeviceType='Bio-1Pro/ATF305Pro/ATF686Pro' or DeviceType = 'HTSeries' "
            Else
                'gridtblregisterselet = "select * from tblMachine where DeviceType = 'ZK(TFT)' or DeviceType='Bio-1Pro/ATF305Pro/ATF686Pro' "
                Dim emp() As String = Common.Auth_Branch.Split(",")
                Dim ls As New List(Of String)()
                For x As Integer = 0 To emp.Length - 1
                    ls.Add(emp(x).Trim)
                Next
                gridtblregisterselet = "select tblMachine.ID_NO, tblMachine.A_R, tblMachine.IN_OUT,tblMachine.DeviceType,tblMachine.LOCATION,tblMachine.branch,tblMachine.commkey,tblMachine.MAC_ADDRESS,tblMachine.Purpose,tblMachine.LastModifiedBy,tblMachine.LastModifiedDate from tblMachine, tblbranch where tblMachine.branch=tblbranch.BRANCHNAME and tblbranch.BRANCHCODE in ('" & String.Join("', '", ls.ToArray()) & "') and DeviceType = 'ZK(TFT)' or DeviceType='Bio-1Pro/ATF305Pro/ATF686Pro'  or DeviceType = 'HTSeries'"
            End If
            Dim WTDataTable As DataTable
            If Common.servername = "Access" Then
                Dim dataAdapter As New OleDbDataAdapter(gridtblregisterselet, Common.con1)
                WTDataTable = New DataTable("tblMachine")
                dataAdapter.Fill(WTDataTable)
            Else
                Dim dataAdapter As New SqlClient.SqlDataAdapter(gridtblregisterselet, Common.con)
                WTDataTable = New DataTable("tblMachine")
                dataAdapter.Fill(WTDataTable)
            End If
            'GridControlZKDevice.DataSource = WTDataTable
            GridControlMachine.DataSource = WTDataTable

        ElseIf CheckEditTemplateUltra.Checked = True Then
            GroupControlFPscanner.Enabled = False
            setDeviceGridUltra()
        End If
    End Sub
    Private Sub CheckEnrollFace_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles CheckEnrollFace.CheckedChanged
        templateCheckChange()
    End Sub
    Private Sub SendUserInfo(sdwEnrollNumber As String, sName As String, ValidityStartdate As DateTime, ValidityEnddate As DateTime, Privilege As Integer, Cardnumber As String, Password As String)
        Dim JsonUserInfo As CUserInfoCfg = New CUserInfoCfg()
        JsonUserInfo.UserInfo = New CUserInfo()
        JsonUserInfo.UserInfo.employeeNo = sdwEnrollNumber
        JsonUserInfo.UserInfo.name = sName
        JsonUserInfo.UserInfo.userType = "normal"
        JsonUserInfo.UserInfo.Valid = New CValid()
        JsonUserInfo.UserInfo.Valid.enable = True
        JsonUserInfo.UserInfo.Valid.beginTime = ValidityStartdate.ToString("yyyy-MM-ddTHH:mm:ss") ' "2017-08-01T17:30:08"
        JsonUserInfo.UserInfo.Valid.endTime = ValidityEnddate.ToString("yyyy-MM-ddTHH:mm:ss") '"2020-08-01T17:30:08"
        JsonUserInfo.UserInfo.Valid.timeType = "local"
        JsonUserInfo.UserInfo.RightPlan = New List(Of CRightPlan)()
        Dim JsonRightPlan As CRightPlan = New CRightPlan()
        JsonRightPlan.doorNo = 1
        JsonRightPlan.planTemplateNo = 1 'textBoxRightPlan.Text
        JsonUserInfo.UserInfo.RightPlan.Add(JsonRightPlan)
        'JsonUserInfo.UserInfo.userVerifyMode = "card"
        JsonUserInfo.UserInfo.floorNumber = 0  'nitin
        JsonUserInfo.UserInfo.roomNumber = 0 'nitin
        JsonUserInfo.UserInfo.password = Password
        If Privilege = 1 Then
            JsonUserInfo.UserInfo.localUIRight = True  'admin rights
        Else
            JsonUserInfo.UserInfo.localUIRight = False   'admin rights
        End If

        JsonUserInfo.UserInfo.doorRight = "1" 'nitin
        Dim strJsonUserInfo As String = JsonConvert.SerializeObject(JsonUserInfo, Formatting.Indented, New JsonSerializerSettings With {
            .DefaultValueHandling = DefaultValueHandling.Ignore
        })
        Dim ptrJsonUserInfo As IntPtr = Marshal.StringToHGlobalAnsi(strJsonUserInfo)
        Dim ptrJsonData As IntPtr = Marshal.AllocHGlobal(1024)

        For i As Integer = 0 To 1024 - 1
            Marshal.WriteByte(ptrJsonData, i, 0)
        Next

        Dim dwState As Integer = CInt(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_SUCCESS)
        Dim dwReturned As UInteger = 0

        While True
            dwState = CHCNetSDK.NET_DVR_SendWithRecvRemoteConfig(m_lSetUserCfgHandle, ptrJsonUserInfo, CUInt(strJsonUserInfo.Length), ptrJsonData, 1024, dwReturned)
            Dim strJsonData As String = Marshal.PtrToStringAnsi(ptrJsonData)

            If dwState = CInt(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_NEEDWAIT) Then
                Thread.Sleep(10)
                Continue While
            ElseIf dwState = CInt(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_FAILED) Then
                'MessageBox.Show("Set User Fail error:" & CHCNetSDK.NET_DVR_GetLastError())
                Exit While
            ElseIf dwState = CInt(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_SUCCESS) Then
                Dim JsonResponseStatus As CResponseStatus = New CResponseStatus()
                JsonResponseStatus = JsonConvert.DeserializeObject(Of CResponseStatus)(strJsonData)

                If JsonResponseStatus.statusCode = 1 Then
                    'MessageBox.Show("Set User Success")
                Else
                    'MessageBox.Show("Set User Fail, ResponseStatus.statusCode" & JsonResponseStatus.statusCode)
                End If

                Exit While
            ElseIf dwState = CInt(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_FINISH) Then
                'MessageBox.Show("Set User Finish")
                Exit While
            ElseIf dwState = CInt(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_EXCEPTION) Then
                'MessageBox.Show("Set User Exception error:" & CHCNetSDK.NET_DVR_GetLastError())
                Exit While
            Else
                'MessageBox.Show("unknown Status error:" & CHCNetSDK.NET_DVR_GetLastError())
                Exit While
            End If
        End While

        If m_lSetUserCfgHandle <> -1 Then
            If CHCNetSDK.NET_DVR_StopRemoteConfig(m_lSetUserCfgHandle) Then
                m_lSetUserCfgHandle = -1
            End If
        End If

        Marshal.FreeHGlobal(ptrJsonUserInfo)
        Marshal.FreeHGlobal(ptrJsonData)
    End Sub
    Private Sub SendCardData(EmployeeNo As String, CardNo As String)
        Dim JsonCardInfo As CCardInfoCfg = New CCardInfoCfg()
        JsonCardInfo.CardInfo = New CCardInfo()
        JsonCardInfo.CardInfo.employeeNo = EmployeeNo
        JsonCardInfo.CardInfo.cardNo = CardNo
        JsonCardInfo.CardInfo.cardType = "normalCard" 'comboBoxCardType.Text
        'JsonCardInfo.CardInfo.deleteCard = False
        Dim strJsonCardInfo As String = JsonConvert.SerializeObject(JsonCardInfo, Formatting.Indented, New JsonSerializerSettings With {
            .DefaultValueHandling = DefaultValueHandling.Ignore
        })
        'Dim strJsonCardInfo As String = JsonConvert.SerializeObject(JsonCardInfo, Formatting.Indented, New JsonSerializerSettings With {.NullValueHandling = NullValueHandling.Ignore})

        Dim ptrJsonCardInfo As IntPtr = Marshal.StringToHGlobalAnsi(strJsonCardInfo)
        Dim ptrJsonData As IntPtr = Marshal.AllocHGlobal(1024)

        For i As Integer = 0 To 1024 - 1
            Marshal.WriteByte(ptrJsonData, i, 0)
        Next

        Dim dwState As Integer = 0
        Dim dwReturned As UInteger = 0

        While True
            dwState = CHCNetSDK.NET_DVR_SendWithRecvRemoteConfig(m_lSetCardCfgHandle, ptrJsonCardInfo, CUInt(strJsonCardInfo.Length), ptrJsonData, 1024, dwReturned)
            Dim strJsonData As String = Marshal.PtrToStringAnsi(ptrJsonData)

            If dwState = CInt(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_NEEDWAIT) Then
                Thread.Sleep(10)
                Continue While
            ElseIf dwState = CInt(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_FAILED) Then
                'MessageBox.Show("Set Card Fail error:" & CHCNetSDK.NET_DVR_GetLastError())
                Exit While
            ElseIf dwState = CInt(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_SUCCESS) Then
                Dim JsonResponseStatus As CResponseStatus = New CResponseStatus()
                JsonResponseStatus = JsonConvert.DeserializeObject(Of CResponseStatus)(strJsonData)

                If JsonResponseStatus.statusCode = 1 Then
                    'MessageBox.Show("Set Card Success")
                Else
                    'MessageBox.Show("Set Card Fail, ResponseStatus.statusCode:" & JsonResponseStatus.statusCode)
                End If

                Exit While
            ElseIf dwState = CInt(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_EXCEPTION) Then
                'MessageBox.Show("Set Card Exception error:" & CHCNetSDK.NET_DVR_GetLastError())
                Exit While
            Else
                'MessageBox.Show("unknown Status error:" & CHCNetSDK.NET_DVR_GetLastError())
                Exit While
            End If
        End While

        If m_lSetCardCfgHandle <> -1 Then

            If CHCNetSDK.NET_DVR_StopRemoteConfig(m_lSetCardCfgHandle) Then
                m_lSetCardCfgHandle = -1
            End If
        End If

        Marshal.FreeHGlobal(ptrJsonCardInfo)
        Marshal.FreeHGlobal(ptrJsonData)
    End Sub
    Private Sub ProcessCapFaceData(ByRef struFaceCfg As NET_DVR_CAPTURE_FACE_CFG, ByRef flag As Boolean)
        If 0 = struFaceCfg.dwFacePicSize Then
            Return
        End If

        Dim strpath As String = Nothing
        Dim dt As DateTime = DateTime.Now
        strpath = String.Format("capture" & Now.ToString("HHmmss") & ".jpg", Environment.CurrentDirectory)
        Dim fs As FileStream = New FileStream(strpath, FileMode.OpenOrCreate)
        Try
            Dim FaceLen As Integer = struFaceCfg.dwFacePicSize
            Dim by As Byte() = New Byte(FaceLen - 1) {}
            Marshal.Copy(struFaceCfg.pFacePicBuffer, by, 0, FaceLen)
            fs.Write(by, 0, FaceLen)
            fs.Close()
            'End Using
            EmpPictureEdit.Image = Image.FromFile(strpath)
            'textBoxFilePath.Text = String.Format("{0}\{1}", Environment.CurrentDirectory, strpath)
            'MessageBox.Show("Capture succeed", "SUCCESSFUL", MessageBoxButtons.OK)
            My.Computer.FileSystem.DeleteFile(strpath)
        Catch
            fs.Close()
            flag = False
            Try
                My.Computer.FileSystem.DeleteFile(strpath)
            Catch ex As Exception
            End Try
            'XtraMessageBox.Show(ulf, "<size=10>Enroll Failed</size>", "<size=9>Error</size>")
        End Try
    End Sub
    Private Sub EnrollFace(lUserID As Integer, mUser_ID As Integer)
        'face

        Dim sURL As String = "PUT /ISAPI/Intelligent/FDLib/FDSetUp?format=json"
        Dim ptrURL As IntPtr = Marshal.StringToHGlobalAnsi(sURL)
        m_lSetFaceCfgHandle = CHCNetSDK.NET_DVR_StartRemoteConfig(lUserID, CHCNetSDK.NET_DVR_FACE_DATA_RECORD, ptrURL, sURL.Length, Nothing, IntPtr.Zero)
        If (m_lSetFaceCfgHandle = -1) Then
            Marshal.FreeHGlobal(ptrURL)
            'MessageBox.Show(("NET_DVR_StartRemoteConfig fail [url:PUT /ISAPI/Intelligent/FDLib/FDSetUp?format=json] error:" + CHCNetSDK.NET_DVR_GetLastError))
            Return
        End If

        Marshal.FreeHGlobal(ptrURL)
        Dim JsonSetFaceDataCond As CSetFaceDataCond = New CSetFaceDataCond
        JsonSetFaceDataCond.faceLibType = "blackFD"
        JsonSetFaceDataCond.FDID = "1"
        JsonSetFaceDataCond.FPID = mUser_ID
        Dim strJsonSearchFaceDataCond As String = JsonConvert.SerializeObject(JsonSetFaceDataCond, Formatting.Indented, New JsonSerializerSettings With {.DefaultValueHandling = DefaultValueHandling.Ignore})
        Dim ptrJsonSearchFaceDataCond As IntPtr = Marshal.StringToHGlobalAnsi(strJsonSearchFaceDataCond)
        Dim struJsonDataCfg As CHCNetSDK.NET_DVR_JSON_DATA_CFG = New CHCNetSDK.NET_DVR_JSON_DATA_CFG
        struJsonDataCfg.dwSize = CType(Marshal.SizeOf(struJsonDataCfg), UInteger)
        struJsonDataCfg.lpJsonData = ptrJsonSearchFaceDataCond
        struJsonDataCfg.dwJsonDataSize = CType(strJsonSearchFaceDataCond.Length, UInteger)

        Try
            Dim by() As Byte
            Using mStream As New MemoryStream()
                Dim img As Image = EmpPictureEdit.Image
                img.Save(mStream, img.RawFormat)
                by = mStream.ToArray()
            End Using
            struJsonDataCfg.dwPicDataSize = CType(by.Length, UInteger)
            Dim iLen As Integer = CType(struJsonDataCfg.dwPicDataSize, Integer)
            struJsonDataCfg.lpPicData = Marshal.AllocHGlobal(iLen)
            'fs.Read(by, 0, iLen)
            Marshal.Copy(by, 0, struJsonDataCfg.lpPicData, iLen)

            'end from template string 

            'MsgBox(by.Length.ToString)
            Dim ptrJsonDataCfg As IntPtr = Marshal.AllocHGlobal(CType(struJsonDataCfg.dwSize, Integer))
            Marshal.StructureToPtr(struJsonDataCfg, ptrJsonDataCfg, False)
            Dim ptrJsonResponseStatus As IntPtr = Marshal.AllocHGlobal(1024)
            Dim m As Integer = 0
            Do While (m < 1024)
                Marshal.WriteByte(ptrJsonResponseStatus, m, 0)
                m = (m + 1)
            Loop

            Dim dwState As Integer = CType(CHCNetSDK.NET_SDK_GET_NEXT_STATUS_SUCCESS, Integer)
            Dim dwReturned As UInteger = 0

            While True
                dwState = CHCNetSDK.NET_DVR_SendWithRecvRemoteConfig(m_lSetFaceCfgHandle, ptrJsonDataCfg, struJsonDataCfg.dwSize, ptrJsonResponseStatus, 1024, dwReturned)
                'MsgBox("dwState " & dwState)
                Dim strResponseStatus As String = Marshal.PtrToStringAnsi(ptrJsonResponseStatus)
                If (dwState = CType(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_NEEDWAIT, Integer)) Then
                    Thread.Sleep(10)
                    'TODO: Warning!!! continue If
                ElseIf (dwState = CType(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_FAILED, Integer)) Then
                    'MessageBox.Show(("Set Face Error:" + CHCNetSDK.NET_DVR_GetLastError))
                    Exit While
                ElseIf (dwState = CType(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_SUCCESS, Integer)) Then
                    Dim JsonResponseStatus As CResponseStatus = New CResponseStatus
                    JsonResponseStatus = JsonConvert.DeserializeObject(Of CResponseStatus)(strResponseStatus)
                    If (JsonResponseStatus.statusCode = 1) Then
                        'MessageBox.Show("Set Face Success")
                    Else
                        'MessageBox.Show(("Set Face Fail, ResponseStatus.statusCode = " + JsonResponseStatus.statusCode))
                    End If

                    Exit While
                ElseIf (dwState = CType(CHCNetSDK.NET_SDK_SENDWITHRECV_STATUS.NET_SDK_CONFIG_STATUS_EXCEPTION, Integer)) Then
                    'MessageBox.Show(("Set Face Exception Error:" + CHCNetSDK.NET_DVR_GetLastError))
                    Exit While
                Else
                    'MessageBox.Show(("unknown Status Error:" + CHCNetSDK.NET_DVR_GetLastError))
                    Exit While
                End If
            End While

            If (m_lSetFaceCfgHandle > 0) Then
                CHCNetSDK.NET_DVR_StopRemoteConfig(m_lSetFaceCfgHandle)
                m_lSetFaceCfgHandle = -1
            End If

            Marshal.FreeHGlobal(ptrJsonDataCfg)
            Marshal.FreeHGlobal(ptrJsonResponseStatus)
        Catch ex As Exception
            'MsgBox(ex.Message)
        End Try
        'end face
    End Sub
End Class