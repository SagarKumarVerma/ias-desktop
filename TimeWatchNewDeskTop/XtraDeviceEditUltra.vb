﻿Imports System.Resources
Imports System.Globalization
Imports DevExpress.LookAndFeel
Imports DevExpress.XtraEditors
Imports System.IO
Imports DevExpress.XtraGrid.Views.Grid
Imports DevExpress.XtraGrid
Imports System.ComponentModel
Imports DevExpress.XtraGrid.Columns
Imports System.Data.SqlClient
Imports System.Text
Imports System.Data.OleDb
Imports System.Net
Imports System.Runtime.InteropServices
Imports iAS.B100Demo
Imports iAS.AscDemo

Public Class XtraDeviceEditUltra
    Dim ulf As UserLookAndFeel
    Dim cmd As New SqlCommand
    Dim cmd1 As New OleDbCommand
    Dim Did As String
    Dim m_CenterNum As Integer = 0
    Dim DeviceValid As Boolean = False
    Public Sub New()
        InitializeComponent()
        'If Common.servername = "Access" Then
        '    Me.Tblbranch1TableAdapter1.Fill(Me.SSSDBDataSet.tblbranch1)
        '    GridLookUpEdit1.Properties.DataSource = SSSDBDataSet.tblbranch1
        'Else
        '    TblbranchTableAdapter.Connection.ConnectionString = Common.ConnectionString '"Data Source=DESKTOP-PG6NDV8;Initial Catalog=SSSDB;Integrated Security=True"
        '    Me.TblbranchTableAdapter.Fill(Me.SSSDBDataSet.tblbranch)
        '    GridLookUpEdit1.Properties.DataSource = SSSDBDataSet.tblbranch
        'End If
    End Sub
    Private Sub XtraDeviceEdit_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        GridLookUpEdit1.Properties.DataSource = Common.LocationNonAdmin
        'If Common.servername = "Access" Then
        '    Me.Tblbranch1TableAdapter1.Fill(Me.SSSDBDataSet.tblbranch1)
        '    GridLookUpEdit1.Properties.DataSource = SSSDBDataSet.tblbranch1
        'Else
        '    Me.TblbranchTableAdapter.Fill(Me.SSSDBDataSet.tblbranch)
        '    GridLookUpEdit1.Properties.DataSource = SSSDBDataSet.tblbranch
        'End If
        ulf = New UserLookAndFeel(Me)
        ulf.SetSkinStyle("iMaginary")
        DevExpress.XtraEditors.XtraMessageBox.AllowHtmlText = True
        XtraMessageBox.AllowCustomLookAndFeel = True

        If XtraDeviceUltra.DeviceID = "" Then
            setDefault()
        Else
            setData()
        End If
        setTCPUSB()
        TabPane1.SelectedPageIndex = 0
        'GroupCenterCombox.Properties.Items.Clear()
        GroupCenterCombox.SelectedIndex = 0
        'GroupCenterCom.Properties.Items.Clear()
        GroupCenterCom.SelectedIndex = 0
        ComboServerType.SelectedIndex = 1

        TextServerIP.Text = ""
        TextServerPort.Text = "7660"
        ComboISUPV.SelectedIndex = 3
        TextAcc.Text = ""
        If TextSrNo.Text.Trim <> "" Then
            Try
                TextAcc.Text = TextSrNo.Text.Trim(vbNullChar).Substring(TextSrNo.Text.Trim(vbNullChar).Length - 9)
            Catch ex As Exception
                TextAcc.Text = ""
            End Try
        End If
        TextAccPwd.Text = "ehome111"
        TextEditUpload.Text = ""
        TextEditComm.Visible = False
        LabelControl9.Visible = False

        'If license.TrialPerioad = True Then
        '    PanelControl1.Enabled = False
        '    PanelControl2.Enabled = False
        'End If
    End Sub
    Private Sub setTCPUSB()
        If ComboBoxEdit1.SelectedIndex = 0 Then
            TextEdit2.Visible = True
            LabelControl5.Visible = True
            SimpleButtonGetSr.Visible = True
        Else
            TextEdit2.Visible = False
            LabelControl5.Visible = False
            SimpleButtonGetSr.Visible = False
        End If
    End Sub
    Private Sub setDefault()
        TextEdit1.Enabled = True
        TextEdit1.Text = ""
        ComboBoxEdit1.SelectedIndex = 0
        ComboBoxEdit2.SelectedIndex = 2
        ComboBoxEdit3.SelectedIndex = 0
        TextEdit2.Text = ""
        GridLookUpEdit1.EditValue = GridLookUpEdit1.Properties.GetKeyValue(0)
        TextSrNo.Text = ""
        LabelControl9.Visible = False
        TextEditComm.Visible = False
        TextEditComm.Text = "0"
        TextLED.Text = ""
        TexHikLogin.Text = ""
        TextHikPass.Text = ""
        BtnVlidate.Enabled = False
    End Sub
    Private Sub setData()

        If XtraDeviceUltra.DeviceID.Contains(",") Then
            setDefault()
            Dim adap As SqlDataAdapter
            Dim adapA As OleDbDataAdapter
            Dim ds As DataSet = New DataSet
            Dim sSql As String = "select max(ID_NO) from tblMachine"
            If Common.servername = "Access" Then
                sSql = " Select ID_NO from tblMachine where ID_NO =(Select CVar(Max(CInt(ID_NO))) from tblMachine )"
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
            If ds.Tables(0).Rows.Count > 0 Then
                If ds.Tables(0).Rows(0).Item(0).ToString.Trim = "" Or ds.Tables(0).Rows(0).Item(0).ToString.Trim = "0" Then 'If IsNull(rscat(0)) Then
                    TextEdit1.Text = "1"
                Else
                    TextEdit1.Text = (Convert.ToInt16(ds.Tables(0).Rows(0).Item(0).ToString) + 1) 'Format(Val(rscat(0)) + 1, "000")
                End If
            Else
                TextEdit1.Text = "1"
            End If
            Dim tmp() As String = XtraDeviceUltra.DeviceID.Split(",")
            TextEdit2.Text = tmp(0)
            TextSrNo.Text = tmp(1)
            TextAcc.Text = TextSrNo.Text.Trim(vbNullChar).Substring(TextSrNo.Text.Trim(vbNullChar).Length - 9)
        Else
            Dim adap As SqlDataAdapter
            Dim adapA As OleDbDataAdapter
            Dim ds As DataSet = New DataSet
            Dim sSql As String = "select * from tblMachine WHERE ID_NO='" & XtraDeviceUltra.DeviceID & "'"
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(sSql, Common.con1)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(sSql, Common.con)
                adap.Fill(ds)
            End If
            TextEdit1.Enabled = False
            TextEdit1.Text = ds.Tables(0).Rows(0).Item("ID_NO").ToString.Trim
            If ds.Tables(0).Rows(0).Item("A_R").ToString.Trim = "T" Then
                ComboBoxEdit1.SelectedIndex = 0
            Else
                ComboBoxEdit1.SelectedIndex = 1
            End If
            If ds.Tables(0).Rows(0).Item("IN_OUT").ToString.Trim = "I" Then
                ComboBoxEdit2.SelectedIndex = 0
            ElseIf ds.Tables(0).Rows(0).Item("IN_OUT").ToString.Trim = "O" Then
                ComboBoxEdit2.SelectedIndex = 1
            Else
                ComboBoxEdit2.SelectedIndex = 2
            End If
            ComboBoxEdit3.EditValue = ds.Tables(0).Rows(0).Item("DeviceType").ToString.Trim
            TextEdit2.Text = ds.Tables(0).Rows(0).Item("LOCATION").ToString.Trim
            GridLookUpEdit1.EditValue = ds.Tables(0).Rows(0).Item("branch").ToString.Trim
            TextSrNo.Text = ds.Tables(0).Rows(0).Item("MAC_ADDRESS").ToString.Trim
            TextEditComm.Text = ds.Tables(0).Rows(0).Item("commkey").ToString.Trim
            If ds.Tables(0).Rows(0).Item("Purpose").ToString.Trim = "A" Then
                ComboBoxEdit4.SelectedIndex = 0
            ElseIf ds.Tables(0).Rows(0).Item("Purpose").ToString.Trim = "C" Then
                ComboBoxEdit4.SelectedIndex = 1
            End If

            If ComboBoxEdit3.SelectedIndex = 0 Or ComboBoxEdit3.SelectedIndex = 2 Then
                TextEditComm.Visible = True
                LabelControl9.Visible = True
            Else
                TextEditComm.Visible = False
                LabelControl9.Visible = False
            End If
            TextLED.Text = ds.Tables(0).Rows(0).Item("LEDIP").ToString.Trim
            TexHikLogin.Text = ds.Tables(0).Rows(0).Item("HLogin").ToString.Trim
            TextHikPass.Text = ds.Tables(0).Rows(0).Item("HPassword").ToString.Trim
            If TextSrNo.Text.Trim = "" Then
                BtnVlidate.Enabled = False
            Else
                BtnVlidate.Enabled = True
            End If
        End If
    End Sub
    Private Sub ComboBoxEdit1_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBoxEdit1.SelectedIndexChanged
        setTCPUSB()
    End Sub
    Private Sub SimpleButton2_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton2.Click
        Me.Close()
    End Sub
    Private Sub SimpleButton1_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton1.Click
        Dim ID_NO As String '= Convert.ToInt16(TextEdit1.Text.Trim)
        If TextEdit1.Text.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Controleer ID cannot be empty</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            TextEdit1.Select()
            Exit Sub
        End If
        ID_NO = Convert.ToInt16(TextEdit1.Text.Trim)
        Dim A_R As String
        Dim LOCATION As String
        If ComboBoxEdit1.SelectedIndex = 0 Then
            A_R = "T"
            If Common.IsValidIPAddress(TextEdit2.Text.Trim) Then
                LOCATION = TextEdit2.Text.Trim
            Else
                If ComboBoxEdit1.SelectedIndex = 0 Then
                    XtraMessageBox.Show(ulf, "<size=10>Invalid Device IP</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    TextEdit2.Select()
                    Exit Sub
                End If
            End If
        Else
            A_R = "S"
            LOCATION = ""
        End If
        Dim IN_OUT As String
        If ComboBoxEdit2.SelectedIndex = 0 Then
            IN_OUT = "I"
        ElseIf ComboBoxEdit2.SelectedIndex = 1 Then
            IN_OUT = "O"
        Else
            IN_OUT = "B"
        End If
        Dim DeviceType As String = ComboBoxEdit3.EditValue
        '= TextEdit2.Text.Trim
        'Dim address As IPAddress = Nothing
        'If IPAddress.TryParse(TextEdit2.Text.Trim, address) Then

        Dim branch As String = GridLookUpEdit1.EditValue
        Dim MAC_ADDRESS As String = TextSrNo.Text.Trim
        Dim commkey As Integer
        If TextEditComm.Text.Trim = "" Then
            commkey = 0
        Else
            commkey = TextEditComm.Text.Trim
        End If

        Dim Purpose As String
        If ComboBoxEdit4.SelectedIndex = 0 Then
            Purpose = "A"
        Else
            Purpose = "C"
        End If
        Purpose = "A" 'for HIKVISION
        Dim LEDIP As String = TextLED.Text.Trim

        Dim HLogin As String = TexHikLogin.Text.Trim
        Dim HPassword As String = TextHikPass.Text.Trim
        If ComboBoxEdit3.SelectedIndex = 5 Then
            If HLogin = "" Then
                XtraMessageBox.Show(ulf, "<size=10>Login Id cannot be empty</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                TexHikLogin.Select()
                Exit Sub
            End If
            If HPassword = "" Then
                XtraMessageBox.Show(ulf, "<size=10>Password cannot be empty</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                TextHikPass.Select()
                Exit Sub
            End If
        End If
        Dim sSql As String
        If XtraDeviceUltra.DeviceID = "" Or XtraDeviceUltra.DeviceID.Contains(",") Then
            'insert
            Dim adap As SqlDataAdapter
            Dim adapA As OleDbDataAdapter
            Dim ds As DataSet = New DataSet
            Dim cquery As String = "select ID_NO from tblMachine WHERE ID_NO='" & ID_NO & "'"
            If Common.servername = "Access" Then
                adapA = New OleDbDataAdapter(cquery, Common.con1)
                adapA.Fill(ds)
            Else
                adap = New SqlDataAdapter(cquery, Common.con)
                adap.Fill(ds)
            End If
            If ds.Tables(0).Rows.Count > 0 Then
                XtraMessageBox.Show(ulf, "<size=10>Duplicate Controller ID</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                TextEdit1.Select()
                Exit Sub
            End If

            If ComboBoxEdit1.SelectedIndex = 0 Then
                ds = New DataSet
                cquery = "select LOCATION from tblMachine WHERE LOCATION='" & LOCATION & "' and A_R = 'T'"
                If Common.servername = "Access" Then
                    adapA = New OleDbDataAdapter(cquery, Common.con1)
                    adapA.Fill(ds)
                Else
                    adap = New SqlDataAdapter(cquery, Common.con)
                    adap.Fill(ds)
                End If
                If ds.Tables(0).Rows.Count > 0 Then
                    XtraMessageBox.Show(ulf, "<size=10>Duplicate Device IP</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    TextEdit2.Select()
                    Exit Sub
                End If
            End If
            sSql = "INSERT INTO tblMachine (ID_NO, A_R, IN_OUT, DeviceType,LOCATION, branch,MAC_ADDRESS, LastModifiedBy, LastModifiedDate, Purpose, commkey, LEDIP, HLogin, HPassword) VALUES('" & ID_NO & "', '" & A_R & "', '" & IN_OUT & "', '" & DeviceType & "','" & LOCATION & "', '" & branch & "', '" & MAC_ADDRESS & "', 'admin', '" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "','" & Purpose & "'," & commkey & ",'" & LEDIP & "','" & HLogin & "','" & HPassword & "')"
        Else
            If license.DeviceSerialNo.Contains(TextSrNo.Text.Trim) Then
                DeviceValid = True
            End If
            sSql = "update tblMachine SET A_R = '" & A_R & "' , IN_OUT ='" & IN_OUT & "', DeviceType = '" & DeviceType & "', LOCATION= '" & LOCATION & "', branch = '" & branch & "', MAC_ADDRESS='" & MAC_ADDRESS & "', LastModifiedBy='admin', LastModifiedDate= '" & Now.ToString("yyyy-MM-dd HH:mm:ss") & "', Purpose='" & Purpose & "', commkey=" & commkey & ", LEDIP ='" & LEDIP & "', HLogin='" & HLogin & "', HPassword='" & HPassword & "' where ID_NO='" & ID_NO & "'"
            'update
        End If
        'MsgBox(sSql)

        'If Common.SerialNo.Contains(TextSrNo.Text.Trim) Then
        'Else
        '    XtraMessageBox.Show(ulf, "<size=10>Unauthorized Serial Number.</size>", "<size=9>Error</size>")
        '    Exit Sub
        'End If



        ''If license.LicenseKey <> "" Then 'And Not (license.TrialPerioad = True And license.TrialExpired = False) Then
        'If license.TrialPerioad = False Then 'allow all devices in trial perioad
        If DeviceValid = False Then
                XtraMessageBox.Show(ulf, "<size=10>Cannot Save. Unauthorized Serial Number</size>", "<size=9>ULtra</size>")
                'If XtraMessageBox.Show(ulf, "<size=10>Unauthorized Serial Number" & vbCrLf & "Do you still want to save?  </size>", "<size=9>ULtra</size>", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                '    GoTo SaveDevice
                'End If
                Exit Sub
            ElseIf DeviceValid = True Then
                Dim infoS As String = "select * from iASSystemInfo"
                Dim dsI As DataSet = New DataSet
                If Common.servername = "Access" Then
                    Dim adapA As OleDbDataAdapter = New OleDbDataAdapter(infoS, Common.con1)
                    adapA.Fill(dsI)
                Else
                    Dim adap As SqlDataAdapter = New SqlDataAdapter(infoS, Common.con)
                    adap.Fill(dsI)
                End If
                Dim serial As String
                If dsI.Tables(0).Rows(0)("DSerialNo").ToString.Trim <> "" Then
                    serial = EncyDcry.Encrypt(TextSrNo.Text.Trim(vbNullChar).Substring(TextSrNo.Text.Trim(vbNullChar).Length - 9), True) & "," & dsI.Tables(0).Rows(0)("DSerialNo").ToString.Trim
                Else
                    serial = EncyDcry.Encrypt(TextSrNo.Text.Trim(vbNullChar).Substring(TextSrNo.Text.Trim(vbNullChar).Length - 9), True)
                End If

                infoS = "update iASSystemInfo set DSerialNo='" & serial & "'"
                If Common.servername = "Access" Then
                    If Common.con1.State <> ConnectionState.Open Then
                        Common.con1.Open()
                    End If
                    cmd1 = New OleDbCommand(infoS, Common.con1)
                    cmd1.ExecuteNonQuery()
                    If Common.con1.State <> ConnectionState.Closed Then
                        Common.con1.Close()
                    End If
                Else
                    If Common.con.State <> ConnectionState.Open Then
                        Common.con.Open()
                    End If
                    cmd = New SqlCommand(infoS, Common.con)
                    cmd.ExecuteNonQuery()
                    If Common.con.State <> ConnectionState.Closed Then
                        Common.con.Close()
                    End If
                End If
                'update license.lic file
                My.Computer.FileSystem.DeleteFile("License.lic")
                Dim fs As FileStream = New FileStream("License.lic", FileMode.Create, FileAccess.Write)
                Dim sw As StreamWriter = New StreamWriter(fs)
                Dim str As String = ""
                str = EncyDcry.Encrypt(license.HDDSerialNo, True)
                sw.WriteLine(str)
                sw.WriteLine(EncyDcry.Encrypt(license.InstallDate.ToString("yyyy-MM-dd HH:mm:ss"), True))
                str = EncyDcry.Encrypt("500", True)
                sw.WriteLine(str)
                str = EncyDcry.Encrypt("10", True)
                sw.WriteLine(str)
                sw.WriteLine(license.LicenseKey)
                sw.Write(serial)
                sw.Flush()
                sw.Close()
                fs.Close()

                license.getLicenseInfo()


                If Common.servername = "Access" Then
                    If Common.con1.State <> ConnectionState.Open Then
                        Common.con1.Open()
                    End If
                    cmd1 = New OleDbCommand(sSql, Common.con1)
                    cmd1.ExecuteNonQuery()
                    If Common.con1.State <> ConnectionState.Closed Then
                        Common.con1.Close()
                    End If
                Else
                    If Common.con.State <> ConnectionState.Open Then
                        Common.con.Open()
                    End If
                    'MsgBox(sSql)
                    cmd = New SqlCommand(sSql, Common.con)
                    cmd.ExecuteNonQuery()
                    If Common.con.State <> ConnectionState.Open Then
                        Common.con.Close()
                    End If
                End If
            End If
        '        Else
        'SaveDevice: If Common.servername = "Access" Then
        '                If Common.con1.State <> ConnectionState.Open Then
        '                    Common.con1.Open()
        '                End If
        '                cmd1 = New OleDbCommand(sSql, Common.con1)
        '                cmd1.ExecuteNonQuery()
        '                If Common.con1.State <> ConnectionState.Closed Then
        '                    Common.con1.Close()
        '                End If
        '            Else
        '                If Common.con.State <> ConnectionState.Open Then
        '                    Common.con.Open()
        '                End If
        '                'MsgBox(sSql)
        '                cmd = New SqlCommand(sSql, Common.con)
        '                cmd.ExecuteNonQuery()
        '                If Common.con.State <> ConnectionState.Open Then
        '                    Common.con.Close()
        '                End If
        '            End If
        '        End If
        '        'End If

        Me.Close()
    End Sub
    Private Sub SimpleButtonGetSr_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButtonGetSr.Click
        If TextEdit2.Text.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>IP Adress cannnot be empty</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            TextEdit2.Select()
            Exit Sub
        End If

        Dim c As Common = New Common()
        Dim IP, ID_NO, DeviceType, A_R As Object
        Dim cn As Common = New Common
        Dim failIP As New List(Of String)()
        Me.Cursor = Cursors.WaitCursor
        IP = TextEdit2.Text.Trim
        ID_NO = TextEdit1.Text.Trim
        DeviceType = ComboBoxEdit3.EditValue.ToString.Trim
        A_R = ComboBoxEdit1.EditValue.ToString.Trim
        If DeviceType = "Bio-1/3/4/5/6/7/ATF-203/ATF-395/ACR-872" Then

        ElseIf DeviceType = "ZK(TFT)" Or DeviceType.ToString.Trim = "Bio-1Pro/ATF305Pro/ATF686Pro" Then

        ElseIf DeviceType = "HKSeries" Then
            Dim struDeviceInfoV40 As CHCNetSDK.NET_DVR_DEVICEINFO_V40 = New CHCNetSDK.NET_DVR_DEVICEINFO_V40
            Dim DeviceAdd As String = TextEdit2.Text.Trim, userName As String = TexHikLogin.Text.Trim, pwd As String = TextHikPass.Text.Trim
            Dim lUserID As Integer = -1
            Dim failReason As String = ""
            Dim logistatus = cn.HikvisionLogin(DeviceAdd, userName, pwd, struDeviceInfoV40, lUserID, failReason)
            If logistatus = False Then
                If failReason <> "" Then
                    XtraMessageBox.Show(ulf, "<size=10>" & failReason & "</size>", "Failed")
                    XtraMasterTest.LabelControlStatus.Text = ""
                    Application.DoEvents()
                    Me.Cursor = Cursors.Default
                    Exit Sub
                End If
            Else
                'If failReason <> "" Then
                '    XtraMessageBox.Show(ulf, "<size=10>" & failReason & "</size>", "Failed")
                '    XtraMasterTest.LabelControlStatus.Text = ""
                '    Application.DoEvents()
                '    Me.Cursor = Cursors.Default
                '    logistatus = cn.HikvisionLogOut(lUserID)
                '    Exit Sub
                'End If

                Dim serialNo As String = System.Text.Encoding.UTF8.GetString(struDeviceInfoV40.struDeviceV30.sSerialNumber).TrimEnd(Microsoft.VisualBasic.ChrW(92))
                TextSrNo.Text = serialNo.Trim
                'TextAcc.Text = serialNo.Trim(vbNullChar).Substring(serialNo.Trim(vbNullChar).Length - 9)
                Try
                    TextAcc.Text = TextSrNo.Text.Trim(vbNullChar).Substring(TextSrNo.Text.Trim(vbNullChar).Length - 9)
                Catch ex As Exception
                    TextAcc.Text = ""
                End Try

                logistatus = cn.HikvisionLogOut(lUserID)
            End If
        End If
        Me.Cursor = Cursors.Default

    End Sub
    Private Sub ComboBoxEdit3_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBoxEdit3.SelectedIndexChanged
        'If ComboBoxEdit3.SelectedIndex = 0 Or ComboBoxEdit3.SelectedIndex = 2 Then
        '    TextEditComm.Visible = True
        '    LabelControl9.Visible = True
        'Else
        '    TextEditComm.Visible = False
        '    LabelControl9.Visible = False
        'End If
        'If ComboBoxEdit3.SelectedIndex = 4 Then
        '    TextHikPass.Visible = True
        '    TexHikLogin.Visible = True
        '    LabelControl11.Visible = True
        '    LabelControl12.Visible = True
        'Else
        '    TextHikPass.Visible = False
        '    TexHikLogin.Visible = False
        '    TextHikPass.Text = ""
        '    TexHikLogin.Text = ""
        '    LabelControl11.Visible = False
        '    LabelControl12.Visible = False
        'End If
    End Sub
    Private Sub btnGet_Click(sender As Object, e As EventArgs) Handles btnGet.Click
        Dim c As Common = New Common()
        Dim IP, ID_NO, DeviceType, A_R As Object
        Dim cn As Common = New Common
        Dim failIP As New List(Of String)()
        Me.Cursor = Cursors.WaitCursor
        IP = TextEdit2.Text.Trim
        ID_NO = TextEdit1.Text.Trim
        DeviceType = ComboBoxEdit3.EditValue.ToString.Trim
        A_R = ComboBoxEdit1.EditValue.ToString.Trim
        If DeviceType = "Bio-1/3/4/5/6/7/ATF-203/ATF-395/ACR-872" Then

        ElseIf DeviceType = "ZK(TFT)" Or DeviceType.ToString.Trim = "Bio-1Pro/ATF305Pro/ATF686Pro" Then

        ElseIf DeviceType = "HKSeries" Then
            Dim struDeviceInfoV40 As CHCNetSDK.NET_DVR_DEVICEINFO_V40 = New CHCNetSDK.NET_DVR_DEVICEINFO_V40
            Dim DeviceAdd As String = TextEdit2.Text.Trim, userName As String = TexHikLogin.Text.Trim, pwd As String = TextHikPass.Text.Trim
            Dim lUserID As Integer = -1
            Dim failReason As String = ""
            Dim logistatus = cn.HikvisionLogin(DeviceAdd, userName, pwd, struDeviceInfoV40, lUserID, failReason)
            If logistatus = False Then
                If failReason <> "" Then
                    XtraMessageBox.Show(ulf, "<size=10>" & failReason & "</size>", "Failed")
                    XtraMasterTest.LabelControlStatus.Text = ""
                    Application.DoEvents()
                    Me.Cursor = Cursors.Default
                    Exit Sub
                End If
            Else
                'GroupCenterCombox
                Dim m_struAbility As CHCNetSDK.NET_DVR_ALARMHOST_ABILITY = New CHCNetSDK.NET_DVR_ALARMHOST_ABILITY()
                m_struAbility.Init()
                Dim AbilityCfg As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(m_struAbility))
                Marshal.StructureToPtr(m_struAbility, AbilityCfg, True)
                Dim dwSize As UInteger = CUInt(Marshal.SizeOf(m_struAbility))
                Dim InBuffer As IntPtr = IntPtr.Zero
                GroupCenterCombox.Properties.Items.Clear()
                If Not CHCNetSDK.NET_DVR_GetDeviceAbility(lUserID, CHCNetSDK.ALARMHOST_ABILITY, InBuffer, 0, AbilityCfg, dwSize) Then
                    For i As Integer = 0 To 1 - 1
                        Dim CenterInfo As String = String.Format("CENTER{0}", i + 1)
                        GroupCenterCombox.Properties.Items.Add(CenterInfo)
                    Next
                Else
                    For i As Integer = 0 To m_struAbility.byNetNum - 1
                        Dim CenterInfo As String = String.Format("CENTER{0}", i + 1)
                        GroupCenterCombox.Properties.Items.Add(CenterInfo)
                    Next
                End If
                'end GroupCenterCombox

                ''GroupCenterCom
                Dim struAbility As CHCNetSDK.NET_DVR_ALARMHOST_ABILITY = New CHCNetSDK.NET_DVR_ALARMHOST_ABILITY()
                struAbility.dwSize = CUInt(Marshal.SizeOf(struAbility))
                InBuffer = IntPtr.Zero
                Dim struAbilityCfg As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(struAbility))
                Marshal.StructureToPtr(struAbility, struAbilityCfg, True)
                If Not CHCNetSDK.NET_DVR_GetDeviceAbility(lUserID, CHCNetSDK.ALARMHOST_ABILITY, InBuffer, 0, struAbilityCfg, struAbility.dwSize) Then
                    m_CenterNum = 2
                Else
                    m_CenterNum = struAbility.byCenterGroupNum
                End If
                'GroupCenterCom.Properties.Items.Clear()
                'For i As Integer = 0 To m_CenterNum - 1
                '    Dim csStr As String = String.Format("CENTER{0}", i + 1)
                '    GroupCenterCom.Properties.Items.Add(csStr)
                'Next
                ''end GroupCenterCom


                Dim m_struNetCfgInV50 As CHCNetSDK.NET_DVR_ALARMHOST_NETCFG_V50 = New CHCNetSDK.NET_DVR_ALARMHOST_NETCFG_V50()
                m_struNetCfgInV50.Init()
                Dim dwReturn As UInteger = 0
                Dim iNetType As Integer = 1
                m_struNetCfgInV50.dwSize = CUInt(Marshal.SizeOf(m_struNetCfgInV50))
                Dim IPV6 As String = String.Empty
                Dim IpV4 As String = String.Empty
                Dim Account As String = String.Empty
                Dim ISUPKey As String = String.Empty
                Dim DoMianName As String = String.Empty

                For i As Integer = 0 To CHCNetSDK.MAX_CENTERNUM - 1
                    m_struNetCfgInV50.struNetCenter(i).struIP.Init()
                    m_struNetCfgInV50.struNetCenter(i).struIP.sIpV4 = IpV4.Trim().PadRight(16, vbNullChar)
                    m_struNetCfgInV50.struNetCenter(i).struIP.byIPv6 = System.Text.Encoding.[Default].GetBytes(IPV6.Trim().PadRight(128, vbNullChar).ToCharArray())
                    m_struNetCfgInV50.struNetCenter(i).byDevID = System.Text.Encoding.[Default].GetBytes(Account.PadRight(32, vbNullChar).ToCharArray())
                    m_struNetCfgInV50.struNetCenter(i).byEHomeKey = System.Text.Encoding.[Default].GetBytes(ISUPKey.PadRight(32, vbNullChar).ToCharArray())
                    m_struNetCfgInV50.struNetCenter(i).byDomainName = System.Text.Encoding.[Default].GetBytes(DoMianName.PadRight(64, vbNullChar).ToCharArray())
                Next

                Dim RES1 As String = String.Empty
                m_struNetCfgInV50.byRes1 = System.Text.Encoding.[Default].GetBytes(RES1.Trim().PadRight(128, vbNullChar))
                Dim struNetCfgV50 As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(m_struNetCfgInV50))
                Marshal.StructureToPtr(m_struNetCfgInV50, struNetCfgV50, True)

                If Not CHCNetSDK.NET_DVR_GetDVRConfig(lUserID, CHCNetSDK.NET_DVR_GET_ALARMHOST_NETCFG_V50, iNetType, struNetCfgV50, CUInt(Marshal.SizeOf(m_struNetCfgInV50)), dwReturn) Then
                    logistatus = cn.HikvisionLogOut(lUserID)
                    XtraMessageBox.Show(ulf, "<size=10>Get Server Details Failed: " & CHCNetSDK.NET_DVR_GetLastError() & "</size>", "Failed")
                    XtraMasterTest.LabelControlStatus.Text = ""
                    Application.DoEvents()
                    Me.Cursor = Cursors.Default
                    Exit Sub
                Else
                    Dim m_struNetCfgOutV50 As CHCNetSDK.NET_DVR_ALARMHOST_NETCFG_V50 = New CHCNetSDK.NET_DVR_ALARMHOST_NETCFG_V50()
                    m_struNetCfgOutV50 = CType(Marshal.PtrToStructure(struNetCfgV50, GetType(CHCNetSDK.NET_DVR_ALARMHOST_NETCFG_V50)), CHCNetSDK.NET_DVR_ALARMHOST_NETCFG_V50)
                    GroupCenterCombox.SelectedIndex = 0
                    ComboServerType.SelectedIndex = m_struNetCfgOutV50.struNetCenter(0).byAddressType
                    TextServerIP.Text = m_struNetCfgOutV50.struNetCenter(0).struIP.sIpV4
                    TextServerPort.Text = Convert.ToString(m_struNetCfgOutV50.struNetCenter(0).wPort)
                    'ProtocolTypeCombox.SelectedIndex = m_struNetCfgOutV50.struNetCenter(0).byReportProtocol - 1
                    ComboISUPV.SelectedIndex = m_struNetCfgOutV50.struNetCenter(0).byProtocolVersion
                    TextAcc.Text = System.Text.Encoding.[Default].GetString(m_struNetCfgOutV50.struNetCenter(0).byDevID)
                    TextAccPwd.Text = System.Text.Encoding.[Default].GetString(m_struNetCfgOutV50.struNetCenter(0).byEHomeKey)

                    'MessageBox.Show("Get Isup Net Param Success")
                End If
                Marshal.FreeHGlobal(struNetCfgV50)
                logistatus = cn.HikvisionLogOut(lUserID)
            End If
        End If
        Me.Cursor = Cursors.Default
    End Sub
    Private Sub btnServerSave_Click(sender As Object, e As EventArgs) Handles btnServerSave.Click
        If TextServerIP.Text.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Server IP cannnot be empty</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            TextServerIP.Select()
            Exit Sub
        End If
        If TextServerPort.Text.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Server Port cannnot be empty</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            TextServerPort.Select()
            Exit Sub
        End If
        If TextAcc.Text.Trim = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Account Name cannnot be empty</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            TextAcc.Select()
            Exit Sub
        End If
        'If TextAccPwd.Text.Trim = "" Then
        '    XtraMessageBox.Show(ulf, "<size=10>Account Password cannnot be empty</size>", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        '    TextAccPwd.Select()
        '    Exit Sub
        'End If





        Dim c As Common = New Common()
        Dim IP, ID_NO, DeviceType, A_R As Object
        Dim cn As Common = New Common
        Dim failIP As New List(Of String)()
        Me.Cursor = Cursors.WaitCursor
        IP = TextEdit2.Text.Trim
        ID_NO = TextEdit1.Text.Trim
        DeviceType = ComboBoxEdit3.EditValue.ToString.Trim
        A_R = ComboBoxEdit1.EditValue.ToString.Trim
        If DeviceType = "HKSeries" Then

            GetEhome()

            Dim struDeviceInfoV40 As CHCNetSDK.NET_DVR_DEVICEINFO_V40 = New CHCNetSDK.NET_DVR_DEVICEINFO_V40
            Dim DeviceAdd As String = TextEdit2.Text.Trim, userName As String = TexHikLogin.Text.Trim, pwd As String = TextHikPass.Text.Trim
            Dim lUserID As Integer = -1
            Dim failReason As String = ""
            Dim logistatus = cn.HikvisionLogin(DeviceAdd, userName, pwd, struDeviceInfoV40, lUserID, failReason)
            If logistatus = False Then
                If failReason <> "" Then
                    XtraMessageBox.Show(ulf, "<size=10>" & failReason & "</size>", "Failed")
                    XtraMasterTest.LabelControlStatus.Text = ""
                    Application.DoEvents()
                    Me.Cursor = Cursors.Default
                    Exit Sub
                End If
            Else


                Dim EhomeKey As String = "ehome111" ' TextAccPwd.Text.Trim

                Dim m_struParamV50 As CHCNetSDK.NET_DVR_ALARMHOST_NETPARAM_V50 = New CHCNetSDK.NET_DVR_ALARMHOST_NETPARAM_V50()
                m_struParamV50.Init()
                m_struParamV50.struIP.Init()
                Dim IPV6 As String = String.Empty
                m_struParamV50.struIP.sIpV4 = TextServerIP.Text.Trim().PadRight(16, vbNullChar)
                m_struParamV50.struIP.byIPv6 = System.Text.Encoding.[Default].GetBytes(IPV6.Trim().PadRight(128, vbNullChar).ToCharArray())
                Dim Port As Integer = Integer.Parse(TextServerPort.Text.Trim)
                m_struParamV50.wPort = CUShort(Port)
                m_struParamV50.byReportProtocol = Convert.ToByte(3) 'Ehome 'Convert.ToByte(ProtocolTypeCombox.SelectedIndex + 1)
                m_struParamV50.byDevID = System.Text.Encoding.[Default].GetBytes(TextAcc.Text.Trim().PadRight(32, vbNullChar).ToCharArray())
                m_struParamV50.byEHomeKey = System.Text.Encoding.[Default].GetBytes(EhomeKey.Trim().PadRight(32, vbNullChar).ToCharArray())
                m_struParamV50.byAddressType = Convert.ToByte(ComboServerType.SelectedIndex)

                If ComboServerType.EditValue = "Domain Name" Then
                    m_struParamV50.byDomainName = System.Text.Encoding.[Default].GetBytes(TextServerIP.Text.Trim().PadRight(64, vbNullChar).ToCharArray())
                Else
                    m_struParamV50.byDomainName = System.Text.Encoding.[Default].GetBytes("".PadRight(64, vbNullChar).ToCharArray())
                End If

                m_struParamV50.byEnable = Convert.ToByte(1)
                m_struParamV50.byProtocolVersion = ComboISUPV.SelectedIndex ' Convert.ToByte(3)
                Dim struParamCfgV50 As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(m_struParamV50))
                Marshal.StructureToPtr(m_struParamV50, struParamCfgV50, True)
                Dim m_struNetCfgV50 As CHCNetSDK.NET_DVR_ALARMHOST_NETCFG_V50 = New CHCNetSDK.NET_DVR_ALARMHOST_NETCFG_V50()
                m_struNetCfgV50.Init()
                m_struNetCfgV50.struNetCenter = New CHCNetSDK.NET_DVR_ALARMHOST_NETPARAM_V50(3) {}
                m_struNetCfgV50.struNetCenter(GroupCenterCombox.SelectedIndex) = CType(Marshal.PtrToStructure(struParamCfgV50, GetType(CHCNetSDK.NET_DVR_ALARMHOST_NETPARAM_V50)), CHCNetSDK.NET_DVR_ALARMHOST_NETPARAM_V50)
                m_struNetCfgV50.dwSize = CUInt(Marshal.SizeOf(m_struNetCfgV50))
                Marshal.FreeHGlobal(struParamCfgV50)
                Dim NetCfgV50 As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(m_struNetCfgV50))
                Marshal.StructureToPtr(m_struNetCfgV50, NetCfgV50, True)
                Dim iNetType As Integer = 1

                If Not CHCNetSDK.NET_DVR_SetDVRConfig(lUserID, CHCNetSDK.NET_DVR_SET_ALARMHOST_NETCFG_V50, iNetType, NetCfgV50, m_struNetCfgV50.dwSize) Then
                    'MessageBox.Show("Set Isup Param  Failed: " & CHCNetSDK.NET_DVR_GetLastError())
                    XtraMessageBox.Show(ulf, "<size=10>Set Isup Details Failed: " & CHCNetSDK.NET_DVR_GetLastError() & "</size>", "Failed")
                    Marshal.FreeHGlobal(NetCfgV50)
                    GoTo logout
                Else
                    'Me.Cursor = Cursors.Default
                    'XtraMessageBox.Show(ulf, "<size=10>Set Server Details Success</size>", "ULtra")
                End If

                Marshal.FreeHGlobal(NetCfgV50)

                'Set Upload mode
                Dim dwCenterIndex As UInteger = CUInt(GroupCenterCombox.SelectedIndex) 'GroupCenterCom
                Dim struReportMode As CHCNetSDK.NET_DVR_ALARMHOST_REPORT_CENTER_CFG_V40 = New CHCNetSDK.NET_DVR_ALARMHOST_REPORT_CENTER_CFG_V40()
                Dim byRes As String = String.Empty
                Dim byDealFailCenter As String = String.Empty
                Dim ZoneReport As String = String.Empty
                Dim NoneZoneReport As String = String.Empty
                Dim byAlarmNetCard As String = String.Empty
                Dim byRes2 As String = String.Empty
                Dim byChanAlarmMode As String = String.Empty
                struReportMode.byRes = System.Text.Encoding.[Default].GetBytes(byRes.PadRight(2, vbNullChar).ToCharArray())
                struReportMode.byChanAlarmMode = System.Text.Encoding.[Default].GetBytes(byChanAlarmMode.PadRight(4, vbNullChar).ToCharArray())
                struReportMode.byDealFailCenter = System.Text.Encoding.[Default].GetBytes(byDealFailCenter.PadRight(16, vbNullChar).ToCharArray())
                struReportMode.byZoneReport = System.Text.Encoding.[Default].GetBytes(ZoneReport.PadRight(512, vbNullChar).ToCharArray())
                struReportMode.byNonZoneReport = System.Text.Encoding.[Default].GetBytes(NoneZoneReport.PadRight(32, vbNullChar).ToCharArray())
                struReportMode.byAlarmNetCard = System.Text.Encoding.[Default].GetBytes(byAlarmNetCard.PadRight(4, vbNullChar).ToCharArray())
                struReportMode.byRes2 = System.Text.Encoding.[Default].GetBytes(byRes2.PadRight(252, vbNullChar).ToCharArray())
                struReportMode.dwSize = CUInt(Marshal.SizeOf(struReportMode))
                struReportMode.byDataType = 0
                struReportMode.byValid = 1 'EnableUploadMode
                struReportMode.byDealFailCenter(0) = 0
                struReportMode.byDealFailCenter(1) = 0
                struReportMode.byDealFailCenter(2) = 0
                struReportMode.byDealFailCenter(3) = 0
                struReportMode.byDealFailCenter(4) = 0
                struReportMode.byDealFailCenter(5) = 0

                For i As Integer = 0 To 7 - 1
                    struReportMode.byNonZoneReport(i) = 0
                Next
                Dim m_pStruReportCenter As CHCNetSDK.NET_DVR_ALARMHOST_REPORT_CENTER_CFG_V40() = New CHCNetSDK.NET_DVR_ALARMHOST_REPORT_CENTER_CFG_V40(6) {}
                For i As Integer = 0 To 7 - 1
                    m_pStruReportCenter(i).Init()
                Next
                For i As Integer = 0 To m_CenterNum - 1
                    m_pStruReportCenter(i).dwSize = CUInt(Marshal.SizeOf(struReportMode))
                Next
                Dim strReportModeCfg As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(struReportMode))
                Marshal.StructureToPtr(struReportMode, strReportModeCfg, True)
                m_pStruReportCenter(dwCenterIndex) = CType(Marshal.PtrToStructure(strReportModeCfg, GetType(CHCNetSDK.NET_DVR_ALARMHOST_REPORT_CENTER_CFG_V40)), CHCNetSDK.NET_DVR_ALARMHOST_REPORT_CENTER_CFG_V40)
                Marshal.FreeHGlobal(strReportModeCfg)
                Dim iCenterIndex As Integer = 0
                While iCenterIndex < m_CenterNum
                    If iCenterIndex = 0 Then
                        m_pStruReportCenter(iCenterIndex).byChanAlarmMode(0) = Convert.ToByte(3) 'Convert.ToByte(MainChannelCom.SelectedIndex)
                        m_pStruReportCenter(iCenterIndex).byChanAlarmMode(1) = 0
                        m_pStruReportCenter(iCenterIndex).byChanAlarmMode(2) = 0
                        m_pStruReportCenter(iCenterIndex).byChanAlarmMode(3) = 0
                        m_pStruReportCenter(iCenterIndex).byAlarmNetCard(0) = 0
                        m_pStruReportCenter(iCenterIndex).byAlarmNetCard(1) = 0
                        m_pStruReportCenter(iCenterIndex).byAlarmNetCard(2) = 0
                        m_pStruReportCenter(iCenterIndex).byAlarmNetCard(3) = 0
                    Else
                        For i As Integer = 0 To 4 - 1
                            m_pStruReportCenter(iCenterIndex).byChanAlarmMode(i) = 0
                            m_pStruReportCenter(iCenterIndex).byAlarmNetCard(i) = 0
                        Next
                    End If
                    iCenterIndex += 1
                End While
                Dim NetIndex As CHCNetSDK.NET_DVR_INDEX = New CHCNetSDK.NET_DVR_INDEX()
                NetIndex.Init()
                Dim NetIndexCfg As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(NetIndex))
                Marshal.StructureToPtr(NetIndex, NetIndexCfg, True)
                Dim InBufferSize As UInteger = CUInt(Marshal.SizeOf(NetIndex))
                Dim InBuff As CHCNetSDK.NET_DVR_INBUFF = New CHCNetSDK.NET_DVR_INBUFF()
                InBuff.Init()
                Dim StatusPtr As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(InBuff))
                Marshal.StructureToPtr(InBuff, StatusPtr, True)
                Dim ReportCenterSize As Integer = Marshal.SizeOf(m_pStruReportCenter(0))
                Dim ReportCenterCfg As IntPtr = Marshal.AllocHGlobal(ReportCenterSize)
                Marshal.StructureToPtr(m_pStruReportCenter(0), ReportCenterCfg, True)
                Dim ParamSize As Integer = m_CenterNum * Marshal.SizeOf(m_pStruReportCenter(0))

                If Not CHCNetSDK.NET_DVR_SetDeviceConfig(lUserID, CHCNetSDK.NET_DVR_SET_ALARMHOST_REPORT_CENTER_V40, 1, NetIndexCfg, InBufferSize, StatusPtr, ReportCenterCfg, CUInt(ParamSize)) Then
                    'MessageBox.Show("Set UpLoad Mode Failed, " & CHCNetSDK.NET_DVR_GetLastError())
                    XtraMessageBox.Show(ulf, "<size=10>Set Isup Details Failed: " & CHCNetSDK.NET_DVR_GetLastError() & "</size>", "Failed")
                Else
                    'MessageBox.Show("Set UpLoad Mode Success! ")
                    Me.Cursor = Cursors.Default
                    XtraMessageBox.Show(ulf, "<size=10>Set Server Details Success</size>", "ULtra")
                End If

                Marshal.FreeHGlobal(ReportCenterCfg)
                Marshal.FreeHGlobal(StatusPtr)
                Marshal.FreeHGlobal(NetIndexCfg)
                'End Set upload mode

logout:         logistatus = cn.HikvisionLogOut(lUserID)
            End If
        End If
        Me.Cursor = Cursors.Default
    End Sub
    Private Sub GetEhome()
        Dim c As Common = New Common()
        Dim IP, ID_NO, DeviceType, A_R As Object
        Dim cn As Common = New Common
        Dim failIP As New List(Of String)()
        Me.Cursor = Cursors.WaitCursor
        IP = TextEdit2.Text.Trim
        ID_NO = TextEdit1.Text.Trim
        DeviceType = ComboBoxEdit3.EditValue.ToString.Trim
        A_R = ComboBoxEdit1.EditValue.ToString.Trim
        If DeviceType = "Bio-1/3/4/5/6/7/ATF-203/ATF-395/ACR-872" Then

        ElseIf DeviceType = "ZK(TFT)" Or DeviceType.ToString.Trim = "Bio-1Pro/ATF305Pro/ATF686Pro" Then

        ElseIf DeviceType = "HKSeries" Then
            Dim struDeviceInfoV40 As CHCNetSDK.NET_DVR_DEVICEINFO_V40 = New CHCNetSDK.NET_DVR_DEVICEINFO_V40
            Dim DeviceAdd As String = TextEdit2.Text.Trim, userName As String = TexHikLogin.Text.Trim, pwd As String = TextHikPass.Text.Trim
            Dim lUserID As Integer = -1
            Dim failReason As String = ""
            Dim logistatus = cn.HikvisionLogin(DeviceAdd, userName, pwd, struDeviceInfoV40, lUserID, failReason)
            If logistatus = False Then
                If failReason <> "" Then
                    XtraMessageBox.Show(ulf, "<size=10>" & failReason & "</size>", "Failed")
                    XtraMasterTest.LabelControlStatus.Text = ""
                    Application.DoEvents()
                    Me.Cursor = Cursors.Default
                    Exit Sub
                End If
            Else
                'GroupCenterCombox
                Dim m_struAbility As CHCNetSDK.NET_DVR_ALARMHOST_ABILITY = New CHCNetSDK.NET_DVR_ALARMHOST_ABILITY()
                m_struAbility.Init()
                Dim AbilityCfg As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(m_struAbility))
                Marshal.StructureToPtr(m_struAbility, AbilityCfg, True)
                Dim dwSize As UInteger = CUInt(Marshal.SizeOf(m_struAbility))
                Dim InBuffer As IntPtr = IntPtr.Zero
                GroupCenterCombox.Properties.Items.Clear()
                If Not CHCNetSDK.NET_DVR_GetDeviceAbility(lUserID, CHCNetSDK.ALARMHOST_ABILITY, InBuffer, 0, AbilityCfg, dwSize) Then
                    For i As Integer = 0 To 1 - 1
                        Dim CenterInfo As String = String.Format("CENTER{0}", i + 1)
                        GroupCenterCombox.Properties.Items.Add(CenterInfo)
                    Next
                Else
                    For i As Integer = 0 To m_struAbility.byNetNum - 1
                        Dim CenterInfo As String = String.Format("CENTER{0}", i + 1)
                        GroupCenterCombox.Properties.Items.Add(CenterInfo)
                    Next
                End If
                'end GroupCenterCombox

                ''GroupCenterCom
                Dim struAbility As CHCNetSDK.NET_DVR_ALARMHOST_ABILITY = New CHCNetSDK.NET_DVR_ALARMHOST_ABILITY()
                struAbility.dwSize = CUInt(Marshal.SizeOf(struAbility))
                InBuffer = IntPtr.Zero
                Dim struAbilityCfg As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(struAbility))
                Marshal.StructureToPtr(struAbility, struAbilityCfg, True)
                If Not CHCNetSDK.NET_DVR_GetDeviceAbility(lUserID, CHCNetSDK.ALARMHOST_ABILITY, InBuffer, 0, struAbilityCfg, struAbility.dwSize) Then
                    m_CenterNum = 2
                Else
                    m_CenterNum = struAbility.byCenterGroupNum
                End If
                'GroupCenterCom.Properties.Items.Clear()
                'For i As Integer = 0 To m_CenterNum - 1
                '    Dim csStr As String = String.Format("CENTER{0}", i + 1)
                '    GroupCenterCom.Properties.Items.Add(csStr)
                'Next
                ''end GroupCenterCom


                Dim m_struNetCfgInV50 As CHCNetSDK.NET_DVR_ALARMHOST_NETCFG_V50 = New CHCNetSDK.NET_DVR_ALARMHOST_NETCFG_V50()
                m_struNetCfgInV50.Init()
                Dim dwReturn As UInteger = 0
                Dim iNetType As Integer = 1
                m_struNetCfgInV50.dwSize = CUInt(Marshal.SizeOf(m_struNetCfgInV50))
                Dim IPV6 As String = String.Empty
                Dim IpV4 As String = String.Empty
                Dim Account As String = String.Empty
                Dim ISUPKey As String = String.Empty
                Dim DoMianName As String = String.Empty

                For i As Integer = 0 To CHCNetSDK.MAX_CENTERNUM - 1
                    m_struNetCfgInV50.struNetCenter(i).struIP.Init()
                    m_struNetCfgInV50.struNetCenter(i).struIP.sIpV4 = IpV4.Trim().PadRight(16, vbNullChar)
                    m_struNetCfgInV50.struNetCenter(i).struIP.byIPv6 = System.Text.Encoding.[Default].GetBytes(IPV6.Trim().PadRight(128, vbNullChar).ToCharArray())
                    m_struNetCfgInV50.struNetCenter(i).byDevID = System.Text.Encoding.[Default].GetBytes(Account.PadRight(32, vbNullChar).ToCharArray())
                    m_struNetCfgInV50.struNetCenter(i).byEHomeKey = System.Text.Encoding.[Default].GetBytes(ISUPKey.PadRight(32, vbNullChar).ToCharArray())
                    m_struNetCfgInV50.struNetCenter(i).byDomainName = System.Text.Encoding.[Default].GetBytes(DoMianName.PadRight(64, vbNullChar).ToCharArray())
                Next

                Dim RES1 As String = String.Empty
                m_struNetCfgInV50.byRes1 = System.Text.Encoding.[Default].GetBytes(RES1.Trim().PadRight(128, vbNullChar))
                Dim struNetCfgV50 As IntPtr = Marshal.AllocHGlobal(Marshal.SizeOf(m_struNetCfgInV50))
                Marshal.StructureToPtr(m_struNetCfgInV50, struNetCfgV50, True)

                If Not CHCNetSDK.NET_DVR_GetDVRConfig(lUserID, CHCNetSDK.NET_DVR_GET_ALARMHOST_NETCFG_V50, iNetType, struNetCfgV50, CUInt(Marshal.SizeOf(m_struNetCfgInV50)), dwReturn) Then
                    logistatus = cn.HikvisionLogOut(lUserID)
                    XtraMessageBox.Show(ulf, "<size=10>Get Server Details Failed: " & CHCNetSDK.NET_DVR_GetLastError() & "</size>", "Failed")
                    XtraMasterTest.LabelControlStatus.Text = ""
                    Application.DoEvents()
                    Me.Cursor = Cursors.Default
                    Exit Sub
                Else
                    Dim m_struNetCfgOutV50 As CHCNetSDK.NET_DVR_ALARMHOST_NETCFG_V50 = New CHCNetSDK.NET_DVR_ALARMHOST_NETCFG_V50()
                    m_struNetCfgOutV50 = CType(Marshal.PtrToStructure(struNetCfgV50, GetType(CHCNetSDK.NET_DVR_ALARMHOST_NETCFG_V50)), CHCNetSDK.NET_DVR_ALARMHOST_NETCFG_V50)
                    'GroupCenterCombox.SelectedIndex = 0
                    'ComboServerType.SelectedIndex = m_struNetCfgOutV50.struNetCenter(0).byAddressType
                    'TextServerIP.Text = m_struNetCfgOutV50.struNetCenter(0).struIP.sIpV4
                    'TextServerPort.Text = Convert.ToString(m_struNetCfgOutV50.struNetCenter(0).wPort)
                    'ProtocolTypeCombox.SelectedIndex = m_struNetCfgOutV50.struNetCenter(0).byReportProtocol - 1
                    'ComboISUPV.SelectedIndex = m_struNetCfgOutV50.struNetCenter(0).byProtocolVersion
                    'TextAcc.Text = System.Text.Encoding.[Default].GetString(m_struNetCfgOutV50.struNetCenter(0).byDevID)
                    'TextAccPwd.Text = System.Text.Encoding.[Default].GetString(m_struNetCfgOutV50.struNetCenter(0).byEHomeKey)

                    'MessageBox.Show("Get Isup Net Param Success")
                End If
                Marshal.FreeHGlobal(struNetCfgV50)
                logistatus = cn.HikvisionLogOut(lUserID)
            End If
        End If
        Me.Cursor = Cursors.Default
    End Sub

    Private Sub SimpleButtonDownload_Click(sender As Object, e As EventArgs) Handles SimpleButtonDownload.Click
        If TextSrNo.Text = "" Then
            XtraMessageBox.Show(ulf, "<size=10>Invalid Serial Number</size>", "<size=9>Ultra</size>")
            TextSrNo.Select()
            Exit Sub
        End If
        Try
            'If license.TrialPerioad = True Then
            '    XtraMessageBox.Show(ulf, "<size=10>No need to Activate Device in Trial Period</size>", "<size=9>Ultra</size>")
            '    TextSrNo.Select()
            '    Exit Sub
            'End If

            If System.IO.File.Exists("Device.lic") Then
                My.Computer.FileSystem.DeleteFile("Device.lic")
            End If
            'File.Create("UserKey.lic").Dispose()
            Dim fs As FileStream = New FileStream("Device.lic", FileMode.Create, FileAccess.Write)
            Dim sw As StreamWriter = New StreamWriter(fs)
            Dim str As String = ""
            sw.WriteLine(EncyDcry.Encrypt(license.HDDSerialNo, True))
            sw.Write(TextSrNo.Text.Trim(vbNullChar).Substring(TextSrNo.Text.Trim(vbNullChar).Length - 9))
            sw.Flush()
            sw.Close()
            fs.Close()

            Dim client As New Net.WebClient
            Dim sfd As New SaveFileDialog
            sfd.FileName = "Device.lic"
            sfd.ShowDialog()
            client.DownloadFile("Device.lic", sfd.FileName)
            Try
                My.Computer.FileSystem.DeleteFile("Device.lic")
            Catch ex As Exception
            End Try
        Catch ex As Exception
            XtraMessageBox.Show(ulf, "<size=10>" & ex.Message.Trim & "</size>", "<size=9>Ultra</size>")
        End Try
    End Sub

    Private Sub SimpleButtonUpload_Click(sender As Object, e As EventArgs) Handles SimpleButtonUpload.Click
        If TextEditUpload.Text.Trim <> "" Then
            Dim client As New Net.WebClient
            client.DownloadFile(TextEditUpload.Text, "Device.lic")
            Dim fs As FileStream = New FileStream("Device.lic", FileMode.Open, FileAccess.Read)
            Dim sr As StreamReader = New StreamReader(fs)
            Dim str() As String
            'Dim str1 As String
            Dim list As New List(Of String)()
            While (sr.Peek <> -1)
                list.Add(sr.ReadLine)
            End While
            sr.Close()
            fs.Close()
            str = list.ToArray
            If str.Length < 2 Then
                XtraMessageBox.Show(ulf, "<size=10>Incorrect file</size>", "<size=9>Ultra</size>")
                TextEditUpload.Select()
                Exit Sub
            Else
                If EncyDcry.Decrypt(str(0), True) <> license.HDDSerialNo Then
                    XtraMessageBox.Show(ulf, "<size=10>Incorrect User Key</size>", "<size=9>Ultra</size>")
                    TextEditUpload.Select()
                    Exit Sub
                End If
                Try
                    TextSrNo.Text = EncyDcry.Decrypt(str(1), True)
                    DeviceValid = True

                    Dim cn As Common = New Common
                    Dim struDeviceInfoV40 As CHCNetSDK.NET_DVR_DEVICEINFO_V40 = New CHCNetSDK.NET_DVR_DEVICEINFO_V40
                    Dim DeviceAdd As String = TextEdit2.Text.Trim, userName As String = TexHikLogin.Text.Trim, pwd As String = TextHikPass.Text.Trim
                    Dim lUserID As Integer = -1
                    Dim failReason As String = ""
                    Dim logistatus = cn.HikvisionLogin(DeviceAdd, userName, pwd, struDeviceInfoV40, lUserID, failReason)
                    If logistatus Then
                        Dim serialNo As String = System.Text.Encoding.UTF8.GetString(struDeviceInfoV40.struDeviceV30.sSerialNumber).TrimEnd(Microsoft.VisualBasic.ChrW(92))
                        TextSrNo.Text = serialNo.Trim
                        'TextAcc.Text = serialNo.Trim(vbNullChar).Substring(serialNo.Trim(vbNullChar).Length - 9)
                        Try
                            TextAcc.Text = TextSrNo.Text.Trim(vbNullChar).Substring(TextSrNo.Text.Trim(vbNullChar).Length - 9)
                        Catch ex As Exception
                            TextAcc.Text = ""
                        End Try
                        logistatus = cn.HikvisionLogOut(lUserID)
                    End If
                    XtraMessageBox.Show(ulf, "<size=10>Serial Number Authorized</size>", "<size=9>Ultra</size>")
                Catch ex As Exception
                    XtraMessageBox.Show(ulf, "<size=10>Incorrect file</size>", "<size=9>Ultra</size>")
                    TextEditUpload.Select()
                    Exit Sub
                End Try

            End If
        End If
    End Sub

    Private Sub BtnVlidate_Click(sender As Object, e As EventArgs) Handles BtnVlidate.Click
        Try
            If ComboBoxEdit3.SelectedIndex = 0 Or ComboBoxEdit3.SelectedIndex = 1 Or ComboBoxEdit3.SelectedIndex = 2 Then

                'If license.TrialPerioad = True Then
                '    XtraMessageBox.Show(ulf, "<size=10>No need to Activate Device in Trial Period</size>", "<size=9>Ultra</size>")
                '    TextSrNo.Select()
                '    Exit Sub
                'End If

                Dim DeviceType As String
                If ComboBoxEdit3.SelectedIndex = 0 Or ComboBoxEdit3.SelectedIndex = 2 Then
                    DeviceType = "ZK"
                ElseIf ComboBoxEdit3.SelectedIndex = 1 Then
                    DeviceType = "Bio"
                End If


                Dim proxy As WebClient = New WebClient
                Dim serviceURL As String = String.Format("http://137.59.201.60:5011/Service1.svc/GetValidSerial/" & TextSrNo.Text.Trim(vbNullChar).Substring(TextSrNo.Text.Trim(vbNullChar).Length - 9) & ",HKSeries")
                Dim data() As Byte = proxy.DownloadData(serviceURL)
                Dim stream As Stream = New MemoryStream(data)
                Dim OutPut As String = System.Text.Encoding.UTF8.GetString(data)

                If OutPut.Contains("true") Then
                    DeviceValid = True
                    XtraMessageBox.Show(ulf, "<size=10>Serial Number Activated</size>", "<size=9>Ultra</size>")
                Else
                    DeviceValid = False
                    XtraMessageBox.Show(ulf, "<size=10>Invalid Serial Number</size>", "<size=9>Ultra</size>")
                End If

                'Dim adap As SqlDataAdapter
                'Dim ds As DataSet = New DataSet
                'Dim ConnectionString = "Data Source=137.59.201.60;Initial Catalog=iASVersionCheck;User Id=sa;Password=sss;"
                'Dim con As SqlConnection = New SqlConnection(ConnectionString)
                'Dim sSql As String = "select count (*) from DeviceSerialNo where DeviceType='HKSeries' and SerialNo='" & TextSrNo.Text.Trim(vbNullChar).Substring(TextSrNo.Text.Trim(vbNullChar).Length - 9) & "'"
                'adap = New SqlDataAdapter(sSql, con)
                'adap.Fill(ds)
                'If ds.Tables(0).Rows(0)(0).ToString.Trim > 0 Then
                '    DeviceValid = True
                '    XtraMessageBox.Show(ulf, "<size=10>Serial Number Activated</size>", "<size=9>Ultra</size>")
                'Else
                '    DeviceValid = False
                '    XtraMessageBox.Show(ulf, "<size=10>Invalid Serial Number</size>", "<size=9>Ultra</size>")
                'End If
            End If
        Catch ex As Exception
            XtraMessageBox.Show(ulf, "<size=10>" & ex.Message.Trim & vbCrLf & "Try Offline Mode" & "</size>", "<size=9>Ultra</size>")
        End Try
    End Sub
    Private Sub TextSrNo_EditValueChanged(sender As System.Object, e As System.EventArgs) Handles TextSrNo.EditValueChanged
        If TextSrNo.Text.Trim <> "" Then
            BtnVlidate.Enabled = True
        End If
    End Sub

    Private Sub TextEditUpload_Click(sender As System.Object, e As System.EventArgs) Handles TextEditUpload.Click
        Dim dlg As New OpenFileDialog()
        ' Filter by Office Files
        dlg.Filter = "Office Files|*.lic;"
        dlg.ShowDialog()
        TextEditUpload.Text = dlg.FileName
    End Sub
End Class