﻿Imports System.Data.SqlClient
Imports System.Data.OleDb
Imports DevExpress.XtraEditors
Imports System
Imports System.Collections.Generic
Imports System.Data
Imports System.Linq
Imports System.Windows.Forms
Imports DevExpress.XtraGrid.Columns
Imports iAS.HorizontalMerging

Public Class XtraForm1
    Private ViewHandler As MyGridViewHandler = Nothing
    Public Sub New()
        InitializeComponent()      
    End Sub
    Private Sub XtraForm1_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub SimpleButton1_Click(sender As System.Object, e As System.EventArgs) Handles SimpleButton1.Click
        Dim ConnectionString As String = "server = 'DESKTOP-PG6NDV8' ;Initial Catalog= 'master';User Id=sa;Password=sss;"
        Dim connection As SqlConnection = New SqlConnection(ConnectionString)
        Dim cmd As New SqlClient.SqlCommand
        Dim objReader As System.IO.StreamReader

        connection.Open()
        cmd = New SqlCommand("CREATE DATABASE SSSDB_Temp", connection)
        cmd.ExecuteNonQuery()
        connection.Close()


        ConnectionString = "server = 'DESKTOP-PG6NDV8' ;Initial Catalog= 'SSSDB_Temp';User Id=sa;Password=sss;"
        connection = New SqlConnection(ConnectionString)
        connection.Open()
        cmd.CommandType = CommandType.Text
        cmd.Connection = connection

        objReader = New System.IO.StreamReader("F:\temp.sql")
        cmd.CommandText = objReader.ReadToEnd
        cmd.ExecuteNonQuery()
        connection.Close()


        'ConnectionString = "server = 'DESKTOP-PG6NDV8' ;Initial Catalog= 'SSSDB_Temp';User Id=sa;Password=sss;"
        'connection = New SqlConnection(ConnectionString)
        'connection.Open()
        'cmd.CommandType = CommandType.Text
        'cmd.Connection = connection
        'objReader = New System.IO.StreamReader("F:\Temp_pro.sql")
        'cmd.CommandText = objReader.ReadToEnd
        'cmd.ExecuteNonQuery()
        'connection.Close()

        Dim sSql As String = "CREATE PROCEDURE [dbo].[usp_update_device_conn_status] " & vbCrLf & _
"	-- Add the parameters for the stored procedure here " & vbCrLf & _
"	@dev_id varchar(24), " & vbCrLf & _
"	@dev_name varchar(24), " & vbCrLf & _
"	@tm_last_update datetime, " & vbCrLf & _
"	@fktm_last_update datetime, " & vbCrLf & _
"	@dev_info varchar(256) " & vbCrLf & _
"AS " & vbCrLf & _
"BEGIN " & vbCrLf & _
"	SET NOCOUNT ON; " & vbCrLf & _
"	declare @dev_registered int " & vbCrLf & _
"	if len(@dev_id) < 1  " & vbCrLf & _
"		return -1 " & vbCrLf & _
"	if len(@dev_name) < 1  " & vbCrLf & _
"		return -1 " & vbCrLf & _
"	begin transaction " & vbCrLf & _
"	SELECT @dev_registered = COUNT(device_id) from tbl_fkdevice_status WHERE device_id=@dev_id " & vbCrLf & _
"	if  @dev_registered = 0 " & vbCrLf & _
"	begin " & vbCrLf & _
"		INSERT INTO tbl_fkdevice_status(  " & vbCrLf & _
"				device_id,  " & vbCrLf & _
"				device_name,  " & vbCrLf & _
"				connected,  " & vbCrLf & _
"				last_update_time, " & vbCrLf & _
"				last_update_fk_time,  " & vbCrLf & _
"				device_info) " & vbCrLf & _
"			VALUES( " & vbCrLf & _
"				@dev_id, " & vbCrLf & _
"				@dev_name,  " & vbCrLf & _
"				1, " & vbCrLf & _
"				@tm_last_update, " & vbCrLf & _
"				@fktm_last_update, " & vbCrLf & _
"				@dev_info) " & vbCrLf & _
"	end	 " & vbCrLf & _
"	else -- if @@ROWCOUNT = 0 " & vbCrLf & _
"	begin " & vbCrLf & _
"		UPDATE tbl_fkdevice_status SET  " & vbCrLf & _
"				device_id=@dev_id,  " & vbCrLf & _
"				device_name=@dev_name,  " & vbCrLf & _
"				connected=1, " & vbCrLf & _
"				last_update_time=@tm_last_update, " & vbCrLf & _
"				last_update_fk_time=@fktm_last_update, " & vbCrLf & _
"				device_info=@dev_info " & vbCrLf & _
"			WHERE  " & vbCrLf & _
"				device_id=@dev_id " & vbCrLf & _
"	end " & vbCrLf & _
"	if @@error <> 0 " & vbCrLf & _
"	begin " & vbCrLf & _
"		rollback transaction " & vbCrLf & _
"		return -2 " & vbCrLf & _
"	end " & vbCrLf & _
"	commit transaction " & vbCrLf & _
"	return 0 " & vbCrLf & _
"END "
        ConnectionString = "server = 'DESKTOP-PG6NDV8' ;Initial Catalog= 'SSSDB_Temp';User Id=sa;Password=sss;"
        connection = New SqlConnection(ConnectionString)
        connection.Open()
        cmd = New SqlCommand(sSql, connection)
        cmd.ExecuteNonQuery()

        sSql = "CREATE PROCEDURE [dbo].[usp_set_cmd_result] " & vbCrLf & _
"	@dev_id varchar(24), " & vbCrLf & _
"	@trans_id varchar(16), " & vbCrLf & _
"	@return_code varchar(128), " & vbCrLf & _
"	@cmd_result_bin varbinary(max) " & vbCrLf & _
"AS " & vbCrLf & _
"BEGIN " & vbCrLf & _
"	SET NOCOUNT ON; " & vbCrLf & _
"	if @dev_id is null or len(@dev_id) = 0 " & vbCrLf & _
"		return -1 " & vbCrLf & _
"	if @trans_id is null or len(@trans_id) = 0 " & vbCrLf & _
"		return -1 " & vbCrLf & _
"	begin transaction " & vbCrLf & _
"	BEGIN TRY " & vbCrLf & _
"		select trans_id from tbl_fkcmd_trans where trans_id = @trans_id and status='RUN' " & vbCrLf & _
"		if @@ROWCOUNT != 1 " & vbCrLf & _
"		begin " & vbCrLf & _
"			return -2 " & vbCrLf & _
"		end " & vbCrLf & _
"		delete from tbl_fkcmd_trans_cmd_result where trans_id=@trans_id " & vbCrLf & _
"		if len(@cmd_result_bin) > 0 " & vbCrLf & _
"		begin " & vbCrLf & _
"			insert into tbl_fkcmd_trans_cmd_result (trans_id, device_id, cmd_result) values(@trans_id, @dev_id, @cmd_result_bin) " & vbCrLf & _
"		end " & vbCrLf & _
"		update tbl_fkcmd_trans set status='RESULT', return_code=@return_code, update_time = GETDATE() where trans_id=@trans_id and device_id=@dev_id and status='RUN' " & vbCrLf & _
"	END TRY " & vbCrLf & _
"    BEGIN CATCH " & vbCrLf & _
"		rollback transaction " & vbCrLf & _
"		return -3 " & vbCrLf & _
"   END CATCH " & vbCrLf & _
"	 " & vbCrLf & _
"	if @@error <> 0 " & vbCrLf & _
"	begin " & vbCrLf & _
"		rollback transaction " & vbCrLf & _
"		return -3 " & vbCrLf & _
"	end " & vbCrLf & _
"	commit transaction " & vbCrLf & _
"	return 0 " & vbCrLf & _
"END"
        cmd = New SqlCommand(sSql, connection)
        cmd.ExecuteNonQuery()

        sSql = "CREATE PROCEDURE [dbo].[usp_receive_cmd]  " & vbCrLf & _
"	@dev_id varchar(24), " & vbCrLf & _
"	@trans_id varchar(16) output, " & vbCrLf & _
"	@cmd_code varchar(32) output, " & vbCrLf & _
"	@cmd_param_bin varbinary(max) output " & vbCrLf & _
"AS " & vbCrLf & _
"BEGIN " & vbCrLf & _
"	SET NOCOUNT ON; " & vbCrLf & _
"	select @trans_id = '' " & vbCrLf & _
"	if @dev_id is null or len(@dev_id) = 0 " & vbCrLf & _
"		return -1	 " & vbCrLf & _
"	begin transaction " & vbCrLf & _
"	BEGIN TRY " & vbCrLf & _
"		declare @trans_id_tmp as varchar(16) " & vbCrLf & _
"		declare @csrTransId as cursor " & vbCrLf & _
"		set @csrTransId = Cursor For " & vbCrLf & _
"			 select trans_id " & vbCrLf & _
"			 from tbl_fkcmd_trans " & vbCrLf & _
"			 where device_id=@dev_id AND status='RUN' " & vbCrLf & _
"		Open @csrTransId " & vbCrLf & _
"		Fetch Next From @csrTransId	Into @trans_id_tmp " & vbCrLf & _
"		While(@@FETCH_STATUS = 0) " & vbCrLf & _
"		begin " & vbCrLf & _
"			DELETE FROM tbl_fkcmd_trans_cmd_param WHERE trans_id=@trans_id_tmp " & vbCrLf & _
"			DELETE FROM tbl_fkcmd_trans_cmd_result WHERE trans_id=@trans_id_tmp " & vbCrLf & _
"			Fetch Next From @csrTransId	Into @trans_id_tmp " & vbCrLf & _
"		end " & vbCrLf & _
"		close @csrTransId " & vbCrLf & _
"	END TRY " & vbCrLf & _
"    BEGIN CATCH " & vbCrLf & _
"		rollback transaction " & vbCrLf & _
"		select @trans_id='' " & vbCrLf & _
"		return -2 " & vbCrLf & _
"    END CATCH " & vbCrLf & _
"	UPDATE tbl_fkcmd_trans SET status='CANCELLED', update_time = GETDATE() WHERE device_id=@dev_id AND status='RUN' " & vbCrLf & _
"	if @@error <> 0 " & vbCrLf & _
"	begin " & vbCrLf & _
"		rollback transaction " & vbCrLf & _
"		return -2 " & vbCrLf & _
"	end " & vbCrLf & _
"	commit transaction " & vbCrLf & _
"		BEGIN TRY " & vbCrLf & _
"		SELECT @trans_id=trans_id, @cmd_code=cmd_code FROM tbl_fkcmd_trans " & vbCrLf & _
"		WHERE device_id=@dev_id AND status='WAIT' ORDER BY update_time DESC		 " & vbCrLf & _
"		if @@ROWCOUNT = 0 " & vbCrLf & _
"		begin " & vbCrLf & _
"			select @trans_id='' " & vbCrLf & _
"			return -3 " & vbCrLf & _
"		end " & vbCrLf & _
"		select @cmd_param_bin=cmd_param from tbl_fkcmd_trans_cmd_param " & vbCrLf & _
"		where trans_id=@trans_id " & vbCrLf & _
"		UPDATE tbl_fkcmd_trans SET status='RUN', update_time = GETDATE() WHERE trans_id=@trans_id " & vbCrLf & _
"	END TRY " & vbCrLf & _
"    BEGIN CATCH " & vbCrLf & _
"   	select @trans_id='' " & vbCrLf & _
"		return -2 " & vbCrLf & _
"	END CATCH " & vbCrLf & _
"	return 0 " & vbCrLf & _
"END"
        cmd = New SqlCommand(sSql, connection)
        cmd.ExecuteNonQuery()

        sSql = "CREATE PROCEDURE [dbo].[usp_check_reset_fk_cmd] " & vbCrLf & _
"	@dev_id varchar(24), " & vbCrLf & _
"	@trans_id varchar(16) output " & vbCrLf & _
"AS " & vbCrLf & _
"BEGIN " & vbCrLf & _
"	SET NOCOUNT ON; " & vbCrLf & _
"	select @trans_id='' " & vbCrLf & _
"   if @dev_id is null or len(@dev_id) = 0 " & vbCrLf & _
"		return -1 " & vbCrLf & _
"   SELECT @trans_id=trans_id FROM tbl_fkcmd_trans where device_id=@dev_id AND cmd_code='RESET_FK' AND status='WAIT' " & vbCrLf & _
"	if @@ROWCOUNT = 0 " & vbCrLf & _
"		return -2  " & vbCrLf & _
"	begin transaction " & vbCrLf & _
"	BEGIN TRY " & vbCrLf & _
"		declare @trans_id_tmp as varchar(16) " & vbCrLf & _
"		declare @csrTransId as cursor " & vbCrLf & _
"		set @csrTransId = Cursor For " & vbCrLf & _
"			 select trans_id " & vbCrLf & _
"			 from tbl_fkcmd_trans " & vbCrLf & _
"			 where device_id=@dev_id AND status='RUN' " & vbCrLf & _
"		Open @csrTransId " & vbCrLf & _
"		Fetch Next From @csrTransId	Into @trans_id_tmp " & vbCrLf & _
"		While(@@FETCH_STATUS = 0) " & vbCrLf & _
"		begin " & vbCrLf & _
"			DELETE FROM tbl_fkcmd_trans_cmd_param WHERE trans_id=@trans_id_tmp " & vbCrLf & _
"			DELETE FROM tbl_fkcmd_trans_cmd_result WHERE trans_id=@trans_id_tmp " & vbCrLf & _
"			Fetch Next From @csrTransId	Into @trans_id_tmp " & vbCrLf & _
"		end " & vbCrLf & _
"		close @csrTransId " & vbCrLf & _
"		UPDATE tbl_fkcmd_trans SET status='CANCELLED', update_time = GETDATE() WHERE device_id=@dev_id AND status='RUN' " & vbCrLf & _
"		UPDATE tbl_fkcmd_trans SET status='RESULT', update_time = GETDATE() WHERE device_id=@dev_id AND cmd_code='RESET_FK' " & vbCrLf & _
"	END TRY " & vbCrLf & _
"   BEGIN CATCH " & vbCrLf & _
"		rollback transaction " & vbCrLf & _
"		select @trans_id='' " & vbCrLf & _
"		return -2 " & vbCrLf & _
"    END CATCH " & vbCrLf & _
"	commit transaction " & vbCrLf & _
"	return 0 " & vbCrLf & _
"END"
        cmd = New SqlCommand(sSql, connection)
        cmd.ExecuteNonQuery()
        connection.Close()

    End Sub
End Class