﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class XtraLeaveIncrement
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.ComboBoxEditNepaliYear = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.ComboBoxEditNEpaliMonth = New DevExpress.XtraEditors.ComboBoxEdit()
        Me.DateEdit = New DevExpress.XtraEditors.DateEdit()
        Me.LabelControl17 = New DevExpress.XtraEditors.LabelControl()
        Me.SimpleButtonSave = New DevExpress.XtraEditors.SimpleButton()
        Me.SimpleButton1 = New DevExpress.XtraEditors.SimpleButton()
        Me.CheckEditM = New DevExpress.XtraEditors.CheckEdit()
        Me.CheckEditY = New DevExpress.XtraEditors.CheckEdit()
        CType(Me.ComboBoxEditNepaliYear.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComboBoxEditNEpaliMonth.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DateEdit.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditM.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CheckEditY.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ComboBoxEditNepaliYear
        '
        Me.ComboBoxEditNepaliYear.Location = New System.Drawing.Point(268, 33)
        Me.ComboBoxEditNepaliYear.Name = "ComboBoxEditNepaliYear"
        Me.ComboBoxEditNepaliYear.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEditNepaliYear.Properties.Appearance.Options.UseFont = True
        Me.ComboBoxEditNepaliYear.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEditNepaliYear.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboBoxEditNepaliYear.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEditNepaliYear.Properties.Items.AddRange(New Object() {"2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037", "2038", "2039", "2040", "2041", "2042", "2043", "2044", "2045", "2046", "2047", "2048", "2049", "2050", "2051", "2052", "2053", "2054", "2055", "2056", "2057", "2058", "2059", "2060", "2061", "2062", "2063", "2064", "2065", "2066", "2067", "2068", "2069", "2070", "2071", "2072", "2073", "2074", "2075", "2076", "2077", "2078", "2079", "2080", "2081", "2082", "2083", "2084", "2085", "2086", "2087", "2088", "2089"})
        Me.ComboBoxEditNepaliYear.Size = New System.Drawing.Size(61, 20)
        Me.ComboBoxEditNepaliYear.TabIndex = 2
        '
        'ComboBoxEditNEpaliMonth
        '
        Me.ComboBoxEditNEpaliMonth.Location = New System.Drawing.Point(196, 33)
        Me.ComboBoxEditNEpaliMonth.Name = "ComboBoxEditNEpaliMonth"
        Me.ComboBoxEditNEpaliMonth.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEditNEpaliMonth.Properties.Appearance.Options.UseFont = True
        Me.ComboBoxEditNEpaliMonth.Properties.AppearanceDropDown.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.ComboBoxEditNEpaliMonth.Properties.AppearanceDropDown.Options.UseFont = True
        Me.ComboBoxEditNEpaliMonth.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.ComboBoxEditNEpaliMonth.Properties.Items.AddRange(New Object() {"Baishakh", "Jestha", "Asar", "Shrawan", "Bhadau", "Aswin", "Kartik", "Mansir", "Poush", "Magh", "Falgun", "Chaitra"})
        Me.ComboBoxEditNEpaliMonth.Size = New System.Drawing.Size(66, 20)
        Me.ComboBoxEditNEpaliMonth.TabIndex = 1
        '
        'DateEdit
        '
        Me.DateEdit.EditValue = Nothing
        Me.DateEdit.Location = New System.Drawing.Point(167, 33)
        Me.DateEdit.Name = "DateEdit"
        Me.DateEdit.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.DateEdit.Properties.Appearance.Options.UseFont = True
        Me.DateEdit.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.DateEdit.Properties.Mask.EditMask = "MM/yyyy"
        Me.DateEdit.Properties.Mask.UseMaskAsDisplayFormat = True
        Me.DateEdit.Size = New System.Drawing.Size(135, 20)
        Me.DateEdit.TabIndex = 1
        '
        'LabelControl17
        '
        Me.LabelControl17.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.LabelControl17.Appearance.Options.UseFont = True
        Me.LabelControl17.Location = New System.Drawing.Point(12, 36)
        Me.LabelControl17.Name = "LabelControl17"
        Me.LabelControl17.Size = New System.Drawing.Size(140, 14)
        Me.LabelControl17.TabIndex = 34
        Me.LabelControl17.Text = "Update Leave Balance for"
        '
        'SimpleButtonSave
        '
        Me.SimpleButtonSave.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButtonSave.Appearance.Options.UseFont = True
        Me.SimpleButtonSave.Location = New System.Drawing.Point(187, 77)
        Me.SimpleButtonSave.Name = "SimpleButtonSave"
        Me.SimpleButtonSave.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButtonSave.TabIndex = 3
        Me.SimpleButtonSave.Text = "Process"
        '
        'SimpleButton1
        '
        Me.SimpleButton1.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.SimpleButton1.Appearance.Options.UseFont = True
        Me.SimpleButton1.Location = New System.Drawing.Point(268, 77)
        Me.SimpleButton1.Name = "SimpleButton1"
        Me.SimpleButton1.Size = New System.Drawing.Size(75, 23)
        Me.SimpleButton1.TabIndex = 35
        Me.SimpleButton1.Text = "Close"
        '
        'CheckEditM
        '
        Me.CheckEditM.EditValue = True
        Me.CheckEditM.Location = New System.Drawing.Point(167, 8)
        Me.CheckEditM.Name = "CheckEditM"
        Me.CheckEditM.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditM.Properties.Appearance.Options.UseFont = True
        Me.CheckEditM.Properties.Caption = "Monthly"
        Me.CheckEditM.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEditM.Properties.RadioGroupIndex = 0
        Me.CheckEditM.Size = New System.Drawing.Size(71, 19)
        Me.CheckEditM.TabIndex = 36
        '
        'CheckEditY
        '
        Me.CheckEditY.Location = New System.Drawing.Point(257, 8)
        Me.CheckEditY.Name = "CheckEditY"
        Me.CheckEditY.Properties.Appearance.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.CheckEditY.Properties.Appearance.Options.UseFont = True
        Me.CheckEditY.Properties.Caption = "Yearly"
        Me.CheckEditY.Properties.CheckStyle = DevExpress.XtraEditors.Controls.CheckStyles.Radio
        Me.CheckEditY.Properties.RadioGroupIndex = 0
        Me.CheckEditY.Size = New System.Drawing.Size(85, 19)
        Me.CheckEditY.TabIndex = 37
        Me.CheckEditY.TabStop = False
        '
        'XtraLeaveIncrement
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(373, 126)
        Me.ControlBox = False
        Me.Controls.Add(Me.CheckEditY)
        Me.Controls.Add(Me.CheckEditM)
        Me.Controls.Add(Me.SimpleButton1)
        Me.Controls.Add(Me.SimpleButtonSave)
        Me.Controls.Add(Me.ComboBoxEditNepaliYear)
        Me.Controls.Add(Me.ComboBoxEditNEpaliMonth)
        Me.Controls.Add(Me.DateEdit)
        Me.Controls.Add(Me.LabelControl17)
        Me.LookAndFeel.SkinName = "iMaginary"
        Me.LookAndFeel.UseDefaultLookAndFeel = False
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "XtraLeaveIncrement"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Leave Increment"
        CType(Me.ComboBoxEditNepaliYear.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComboBoxEditNEpaliMonth.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit.Properties.CalendarTimeProperties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DateEdit.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditM.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CheckEditY.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ComboBoxEditNepaliYear As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents ComboBoxEditNEpaliMonth As DevExpress.XtraEditors.ComboBoxEdit
    Friend WithEvents DateEdit As DevExpress.XtraEditors.DateEdit
    Friend WithEvents LabelControl17 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents SimpleButtonSave As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents SimpleButton1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents CheckEditM As DevExpress.XtraEditors.CheckEdit
    Friend WithEvents CheckEditY As DevExpress.XtraEditors.CheckEdit
End Class
